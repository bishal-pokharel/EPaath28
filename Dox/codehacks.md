/**********************************************************************
****** START: styling to override fieldset and legend styles 
		from bootstrap and recover its default style   ***********
**********************************************************************/
<code>
    fieldset{
    	display: block;
        margin-left: 2px;
        margin-right: 2px;
        padding-top: 0.35em;
        padding-bottom: 0.625em;
        padding-left: 0.75em;
        padding-right: 0.75em;
        border: 2px groove grey;
    }

    legend{
    	display: block;
        padding-left: 1%;
        padding-right: 1%;
        margin: 0;
        padding-top: 0;
        padding-bottom: 0;
        border:none;
        width:auto;
    }
</code>

/**********************************************************************
****** STOP: styling to override fieldset and legend styles 
        from bootstrap and recover its default style   ***********
**********************************************************************/
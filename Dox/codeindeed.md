/**********
********
******for the next page button to shake at last, duration is set in mytemplate .myfooter next and prev > span 
********
******
***/

<code> 	

$(".footer-next > span").addClass('animated infinite shake');

element {

-webkit-animation-duration: 2s;
-moz-animation-duration: 2s;
animation-duration: 2s;

}

</code>

/**********
********
******
for close button use this class in span tag and the following css
********
******
***/
<code>

<span class="glyphicon glyphicon-remove-circle"></span>

element {

cursor:pointer;
color:darkred;
font-size: 2em;	
text-shadow:-1px -1px 0 #fff,1px -1px 0 #fff,-1px 1px 0 #fff,1px 1px 0 #fff;

}

</code>


/**********
********
******
for next button- <!-- for showing texts --> use this class in span tag and the following css
********
******
***/

<code>
	
<span id="nxtList" class="glyphicon glyphicon-arrow-right"></span>

element {

cursor: pointer;
color:black;
font-size:2.5em;	
text-shadow:-1px -1px 0 #fff,1px -1px 0 #fff,-1px 1px 0 #fff,1px 1px 0 #fff;

}

</code>

/**********
********
******
for next button- <!-- for showing different slides inside a page --> use this class in span tag and the following css
********
******
***/

<code>

<!-- for travelling right -->
<span class="glyphicon glyphicon-circle-arrow-right"></span>

<!-- for travelling left -->
<span class="glyphicon glyphicon-circle-arrow-left"></span>

{

cursor: pointer;
color:black;
font-size:3em;	
text-shadow:-1px -1px 0 #fff,1px -1px 0 #fff,-1px 1px 0 #fff,1px 1px 0 #fff;

}
</code>


/**********
********
******
for next button- <!-- for showing different slides inside a page --> use this class in span tag and the following css
********
******
***/

<code>

<span class="glyphicon glyphicon-repeat"></span>
	
{

cursor: pointer;
color:black;
font-size:3em;	
text-shadow:-1px -1px 0 #fff,1px -1px 0 #fff,-1px 1px 0 #fff,1px 1px 0 #fff;

}

</code>
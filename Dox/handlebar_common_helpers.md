<h3>Commonly used <mark>HANDLEBAR HELPERS</mark> that can be used in individual activity script files.</h3>

<ol>	
	<li>
	<h3>Helper to call item index number from base 1</h3>
		<em>Helper :</em>
		<pre>
			Handlebars.registerHelper('indexbase1', number) {
    			return number + 1;
			});
		</pre>
		<em>Call : <code>{{indexbase1 @index}}</code></em>
	</li>
	<br>
	<hr>
	
</ol>
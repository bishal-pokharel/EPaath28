<h1>Commonly used ole library "ole.lib.js" function calls and usage</h1>


<ol>	
	<h3># Function calls for notification, for page navigation</h3>
	<li>
		<pre><h4>ole.footerNotificationHandler.pageEndSetNotification();</h4></pre>
		<em>Usage: On calling this function, a notification with already set message is seen. <mark>Continue</mark> button thus seen allows the user to navigate to next page either in lesson or in exercise. The circular arrow <mark>Reload</mark> button allows the user to iterate the page.</em>
	</li>
	<br>
	<hr>
	<li>		
		<pre><h4>ole.footerNotificationHandler.lessonEndSetNotification();</h4></pre>
		<em>Usage: On calling this function, a notification with already set message is seen.<mark>Continue</mark> button thus seen allows the user to navigate to first page of exercise. The circular arrow <mark>Reload</mark> button allows the user to iterate the page.</em>
	</li>
	<br>
	<hr>
	<li>		
		<pre><h4>ole.footerNotificationHandler.pageEndSetCustomNotification(usermessage);</h4></pre>
		<em>Usage: On calling this function, a notification with "usermessage" is seen. <mark>Continue</mark> button thus seen allows the user to navigate to next page either in lesson or in exercise. The circular arrow <mark>Reload</mark> button allows the user to iterate the page.</em>
	</li>
	<br>
	<hr>
	<li>		
		<pre><h4>ole.footerNotificationHandler.lessonEndSetCustomNotification(usermessage);</h4></pre>
		<em>Usage: On calling this function, a notification with "usermessage" is seen. <mark>Continue</mark> button thus seen allows the user to navigate to first page of exercise. The circular arrow <mark>Reload</mark> button allows the user to iterate the page.</em>
	</li>
	<br>
	<hr>
	<li>		
		<pre><h4>ole.footerNotificationHandler.hideNotification();</h4></pre>
		<em>Usage: On calling this function, current notification is hidden from the notification panel. This includes the reload and continue button and the message as well.</em>
	</li>
	<br>
	<hr>
	<li>		
		<pre><h4>ole.activityComplete.finishingcall();</h4></pre>
		<em>Usage: On calling this function, a <mark>congratulations page</mark> is called. You <mark>can navigate</mark> back to the <mark>current lesson or exercise</mark> or to the <mark>main menu</mark> for the current subject you are working on.</em>
	</li>
	<br>
	<hr>
</ol>
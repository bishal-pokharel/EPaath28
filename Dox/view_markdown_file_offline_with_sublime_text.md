<h4>To view the markdown files here on browser:</h4>

<b>Method 1: <mark>With Sublime Text Editor</mark> </b>
<ul>
	<li><em>Step 0: </em>Install <mark>Markdown Preview</mark> package from package control sublime text. If you need instructions on how to install this package on sublime text <a href="https://github.com/revolunet/sublimetext-markdown-preview#using-package-control-recommended"> goto this link</a>.</li>
	<li><em>Step 1: </em> Open a <mark>*.md</mark> file with sublime text.</li>
	<li><em>Step 2: </em> Open <mark>package control</mark> from either Menu > Preferences > Package Control , or, use the shortcut <mark>Ctrl+Shift+P</mark></li>
	<li><em>Step 3: </em> Type <mark>markdown</mark> in the search box and select the second option <mark>Markdown Preview : Preview in Browser</mark> from the dropdown menu and press enter.</li>
	<li><em>Step 4: </em>Select the second option <mark>markdown</mark> from the dropdown menu and press enter.</li>
	<li><em>Step 5: </em>Go to browser and view the markdown file. Cheers!</li>
</ul>

<em>Note : Please add any other methods if you know on previewing markdown file in browser.</em>
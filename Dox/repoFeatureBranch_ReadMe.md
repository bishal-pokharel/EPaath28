<h1>Feature branches for the Epaath Html Repository</h1>

<ul>
	<li>
		<h3><mark>simplefeatures/featureaddition</mark></h3>
		<span>commit all the feature addition required for: small and common feature addition</span>
	</li>

	<li>
		<h3><mark>lessontaxonomy/featureaddition</mark></h3>
		<span>commit all the feature addition required for categorization and view of the lessons</span>
	</li>

	<li>
		<h3><mark>requirejs/featureaddition</mark></h3>
		<span>commit all the feature addition required for: Testing Require JS on making the Epaath more modular and weighing on if it gives a significant added advantage.</span>
	</li>

	<li>
		<h3><mark>class7mathreview</mark></h3>
		<span>commit all the feature addition required for: review changes and updates for grade 7 math activities</span>
	</li>

	<li>
		<h3><mark>class7sciencereview</mark></h3>
		<span>commit all the feature addition required for: review changes and updates for grade 7 science activities</span>
	</li>	

	<li>
		<h3><mark>generic : [lessonname]/[subject]/[grade]</mark></h3>
		<span>a branch for each activity with above branchname syntax, all changes are done to above branch for particular activity</span><br>
		<span>Also add all the review changes to this activity in this very branch. Name the commit with the syntax : [bug number from redmine][what bug was resolved]</span>
	</li>

	<li>
		<h3><mark>bugfix/activity/epaath</mark></h3>
		<span>branch for bug fixes in epaath for activities</span><br>
		<span>Branch for bug fixes in epaath for activities <mark>for those activities whose specific branches are not generated</mark></span>
	</li>

	<li>
		<h3><mark>docsNtemplates/epaath</mark></h3>
		<span>branch for bug fixes in epaath for activities</span><br>
		<span>Branch for bug fixes in epaath for activities <mark>for those activities whose specific branches are not generated</mark></span>
	</li>
</ul>

<h1>Specific activity branches</h1>
<ul>
	<h3>Grade 7 : </h3>	
	<li>Mixture | Science | 7 | <em>branchname:</em> <b><mark>mixtureroshan</mark></b></li>
	<li>Rocks | Science | 7 | <em>branchname:</em> <b><mark>strofearth/roshan</mark></b></li>

	<h3>Grade 8 : </h3>
	<li>Reading A Map | English | 8 | <em>branchname:</em> <b><mark>readingA
		map/English8</mark></b></li>
	<li>Microbes | Science | 8 | <em>branchname:</em> <b><mark>microbes/science/grade8</mark></b></li>
	<li>Mirror | Science | 8 | <em>branchname:</em> <b><mark>mirror/science/grade8</mark></b></li>
	<li>Minerals | Science | 8 | <em>branchname:</em> <b><mark>minerals/science/grade8</mark></b></li>
	<li>Soil | Science | 8 | <em>branchname:</em> <b><mark>soil/science/grade8</mark></b></li>
	<li>Soil | Science | 8 | <em>branchname:</em> <b><mark>raydigram/science/grade8</mark></b></li>
	<li>Periodic_table | Science | 8 | <em>branchname:</em> <b><mark>periodic_table/science/grade8</mark></b></li>
	<li>Measurement | Science | 8 | <em>branchname:</em> <b><mark>measurement/science/grade8</mark></b></li>
	<li>Acid Base Salt | Science | 8 | <em>branchname:</em> <b><mark>acid_base_salt/science/grade8</mark></b></li>
	<li>Chemical Reaction | Science | 8 | <em>branchname:</em> <b><mark>chemical_reaction/science/grade8</mark></b></li>
	<li>Electricity | Science | 8 | <em>branchname:</em> <b><mark>electricity/science/grade8</mark></b></li>
	<li>LifeProcesses | Science | 8 | <em>branchname:</em> <b><mark>lifeprocesses/science/grade8</mark></b></li>
	<li>Lever | Science | 8 | <em>branchname:</em> <b><mark>lever/science/grade8</mark></b></li>
	<li>Pressure | Science | 8 | <em>branchname:</em> <b><mark>pressure/science/grade8</mark></b></li>
	<li>Velocity_and_acceleration | Science | 8 | <em>branchname:</em> <b><mark>velocityNacceleration/science/grade8</mark></b></li>
	<li>Sound | Science | 8 | <em>branchname:</em> <b><mark>sound/science/grade8</mark></b></li>
	<li>Bearing | Math | 8 | <em>branchname:</em> <b><mark>bearing/science/grade8</mark></b></li>
</ul>
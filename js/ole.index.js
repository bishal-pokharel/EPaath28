(function($) {
  var $_GET = {}; /*get object to parse and put url decoded value*/
  var $subject = ""; /*subject store variable initialized*/
  var $currentgrade =
    "2"; /*currentgradestore variable initialized default grade 2*/
  $_GET["lang"] = "en"; /*get key value for language*/
  var $lang = ""; /*initialize lang variable to store language*/

  // parse the url and decode it to get key value pairs for the parameters passed in url
  document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function() {
    function decode(s) {
      return decodeURIComponent(s.split("+").join(" "));
    }

    /*now call the decode function to find and store the key value
		pair in url params*/
    $_GET[decode(arguments[1])] = decode(arguments[2]);
  });

  // set the language as decoded from the url
  console.log("lang = " + $_GET["lang"]);
  // set the language as given in the url
  $lang = $_GET["lang"];

  $("body").attr("lang", $lang); //create attribute "lang" on body tag
  $(".mainBox").attr("lang", $lang);

  // extract data from configsubjects.json and display subjects tab
  // jqxhrdatajson is a jqXHR object storage although it is vestigial for
  // now it can be used later sometimes for development, similarly for the
  // done, fail and always methods
  var jqxhrconfigsubjectsjson = $.getJSON(
          "config/configsubjects.json",
          function(subjectsdata) {
            console.log("configsubjects.json get success");

            var subjectsData = []; /*stores data to be passed for subjects */
            var i = 0; /*counter*/

            // search and update the subjects data in a variable to be later
            // passed to handlebars here subjectsData
            $.each(subjectsdata, function(key) {
              console.log(subjectsdata[key][$lang]);
              // key == "english" ?
              // ( $link = "subjects.html?sub="+key+"&lang=en&grade="+$currentgrade ) :
              // ( $link = "subjects.html?sub="+key+"&lang="+$lang+"&grade="+$currentgrade );

              if (key == "english") {
                $link =
                  "subjects.html?sub=" + key + "&lang=en&grade=" + $currentgrade;
              } else if (key == "nepali") {
                $link =
                  "subjects.html?sub=" + key + "&lang=np&grade=" + $currentgrade;
              } else {
                $link =
                  "subjects.html?sub=" +
                  key +
                  "&lang=" +
                  $lang +
                  "&grade=" +
                  $currentgrade;
              }

              subjectsData[i] = {
                name: subjectsdata[key][$lang],
                image: subjectsdata[key].img,
                classname: "imghov" + i,
                link: $link
              };
              i++;
            });

            // load the subjects data into the handlebars template
            var source = $("#subjects-template").html();
            var template = Handlebars.compile(source);
            var html = template(subjectsData);
            $(".indexsubjectrow").append(html);
          }
        )
    .done(function() {
      console.log("configsubjects.json get done");

      $(".imghov0").hover(
        function() {
          $(".imghov0 img").attr("src", "images/scienceHover.png");
        },
        function() {
          $(".imghov0 img").attr("src", "images/science.png");
        }
      );

      $(".imghov1").hover(
        function() {
          $(".imghov1 img").attr("src", "images/mathHover.png");
        },
        function() {
          $(".imghov1 img").attr("src", "images/math.png");
        }
      );

      $(".imghov2").hover(
        function() {
          $(".imghov2 img").attr("src", "images/englishHover.png");
        },
        function() {
          $(".imghov2 img").attr("src", "images/english.png");
        }
      );

      $(".imghov3 img").attr("src", "images/nepali_thumnail.png");
      $(".imghov3").hover(
        function() {
          $(".imghov3 img").attr("src", "images/nepali_thumnail_hover.png");
        },
        function() {
          $(".imghov3 img").attr("src", "images/nepali_thumnail.png");
        }
      );
    })
    .fail(function(data, textstatus, errortype) {
      alert(
        "get configsubjects.json failed, status: " +
          textstatus +
          ", error: " +
          errortype
      );
      location.reload();
    })
    .always(function() {
      console.log("configsubjects.json complete, appears on complete");
    });

  // extract data from data json and display the about text
  // jqxhrdatajson is a jqXHR object storage although it is vestigial for
  // now it can used later sometimes for development, similarly for the
  // done, fail and always methods
  var jqxhrdatajson = $.getJSON("config/data.json", function(generaldata) {
    console.log("data.json get success");

    // store the generaldata gradeselect text to buttongradedata
    var buttongradedata = generaldata.gradeselect[$lang];

    // store the generaldata gradess text to gradestext
    var gradetext = generaldata.gradess[$lang];

    // dropdown template data to be passed to handlebars
    var gradesData = {
      gradetextdata: buttongradedata,
      allgrades: []
    };

    // parse all grade levels and push into all grades array
    $.each(generaldata.allgrades[$lang], function(classindex, classval) {
      /* iterate through allgrades array for current language*/
      var temporarygradeobject = {
        specificgrade:
          generaldata.allgrades["en"][
            classindex
          ] /*this value should always be in english since it is backend data*/,
        specificgradetext: gradetext + " " + classval
      };
      gradesData.allgrades.push(temporarygradeobject);
    });

    // put the grades data extracted above into the dropdown
    //handlebars template
    var source = $("#classdropdown-template").html();
    var template = Handlebars.compile(source);
    var html = template(gradesData);
    $(".dropdowncontainer").append(html);

    // find text to be displayed in first para about and display
    var $indexExpText = generaldata.indexText[$lang];
    $(".indexExpText").html($indexExpText);
    // find text to be displayed in first para about and display
    var $indexExpTexta = generaldata.indexTexta[$lang];
    $(".indexExpText_a").html($indexExpTexta);
    // find text to be displayed in first para about and display
    var $indexExpTextb = generaldata.indexTextb[$lang];
    $(".indexExpText_b").html($indexExpTextb);

    // find text to be displayed in second para about and display
    var $indexExpText2 = generaldata.indexTextSecond[$lang];
    $(".indexExpText2").html($indexExpText2);

    // find text to be displayed in below para about and display
    var $indexTextBelow = generaldata.indexTextBelow[$lang];
    $(".indexTextBelow").html($indexTextBelow);

    // find text to be displayed in below footerApprovalText about and display
    var $footerApprovalText = generaldata.footerApprovalText[$lang];
    $(".footerApprovalText").html($footerApprovalText);

    // find text to be displayed in footer and display
    var valOf = generaldata.footerText[$lang];
    $(".footerCC")
      .find(".design")
      .html(valOf);

    // find text to be displayed for support by
    var SupportMe = generaldata.SupportText[$lang];
    $(".footerCC")
      .find(".gni-class")
      .html(SupportMe);

    // find text to be displayed by choose grade and choose subject
    var gradeSelect = generaldata.chooseGrade[$lang];
    $(".chooseGrade p").html(gradeSelect);
    var subjectSelect = generaldata.chooseSubject[$lang];
    $(".chooseSubject p").html(subjectSelect);

    // find text to be displayed by FRONT GRADE
    $(".rowDivClass div:nth-of-type(1) p").html(
      generaldata.allgrades[$lang][0]
    );
    $(".rowDivClass div:nth-of-type(2) p").html(
      generaldata.allgrades[$lang][1]
    );
    $(".rowDivClass div:nth-of-type(3) p").html(
      generaldata.allgrades[$lang][2]
    );
    $(".rowDivClass div:nth-of-type(4) p").html(
      generaldata.allgrades[$lang][3]
    );
    $(".rowDivClass div:nth-of-type(5) p").html(
      generaldata.allgrades[$lang][4]
    );
    $(".rowDivClass div:nth-of-type(6) p").html(
      generaldata.allgrades[$lang][5]
    );
    $(".rowDivClass div:nth-of-type(7) p").html(
      generaldata.allgrades[$lang][6]
    );
    $(".rowDivClass div:nth-of-type(8) p").html(
      generaldata.allgrades[$lang][7]
    );

    // find index page alert text and display
    var indexalerttext = generaldata.indexTextAlert[$lang];
    $("span.alerttext").html(indexalerttext);

    $(".indexExpText2 span").click(function() {
      // $(this).attr('http://pustakalaya.org/epaath/', '_blank');
      window.open("http://pustakalaya.org/epaath/");
    });
  })
    .done(function() {
      console.log("data.json get done, second success");

      // cache alertbox jquery dom object
      var $alertbox = $("div.alertbox");

      // cache jquery dom object for subjets container
      var $indexsubjectrow = $("div.indexsubjectrow");

      // cache jquery dom object for subjets anchor tag
      var $subjectsanchortag = $indexsubjectrow.find("div a");

      // event handler for subjects anchor tag
      $subjectsanchortag.on("click", function(event) {
        event.preventDefault(); /*so that the subjects are
									not clickable until grade selection is done*/

        /* Act on the event */
        $alertbox.show(0);
      });

      // // on success add event handler to the class lists on the dropdown menu
      // $("div.dropdowncontainer").on('click', 'ul > li > a', function(event) {
      // 	event.preventDefault(); /*keep this intact*/
      // 	/* Act on the event */
      //
      // 	// make subject anchor tags act its default - follow the link assigned
      // 	$subjectsanchortag.off("click");
      //
      // 	/*put default grade into a temp var before udpating the selected
      // 	current grade*/
      // 	var defaultgrade = $currentgrade;
      //
      // 	// jquery object for button grade display data
      // 	$buttongradedata = $("span.buttongradedata");
      //
      // 	$buttongradedata.text($(this).text());
      //
      // 	// update the selected current grade
      // 	$currentgrade = $(this).attr("data-grade");
      //
      // 	// regex that finds grade param in subjects tab href attribute
      // 	var regex = new RegExp("&grade="+defaultgrade,"gi");
      //
      // 	// update the anchor tag href attribute of each subject types
      // 	$.each($subjectsanchortag, function(index, val) {
      // 		/* iterate through array or object */
      // 		$(this).attr("href", $(this).attr("href").replace(regex,"&grade="+$currentgrade));
      // 	});
      // });

      // on success add event handler to the grade row
      $(".insideGradeDivs").click(function(event) {
        event.preventDefault(); /*keep this intact*/
        /* Act on the event */

        //change the clicked class
        $(".insideGradeDivs").removeClass("insideGradeDivs_active");
        $(this).addClass("insideGradeDivs_active");
        // make subject anchor tags act its default - follow the link assigned
        $subjectsanchortag.off("click");

        /*put default grade into a temp var before udpating the selected
			current grade*/
        var defaultgrade = $currentgrade;

        // update the selected current grade
        $currentgrade = $(this).data("grades");

        // regex that finds grade param in subjects tab href attribute
        var regex = new RegExp("&grade=" + defaultgrade, "gi");

        // update the anchor tag href attribute of each subject types
        $.each($subjectsanchortag, function(index, val) {
          /* iterate through array or object */
          $(this).attr(
            "href",
            $(this)
              .attr("href")
              .replace(regex, "&grade=" + $currentgrade)
          );

          // change the link to direct to another folder for grade 7 and grade 8 starts
          // if($(this).attr("href").includes("EPaath7-8/subjects.html?")){
          // 	$(this).attr("href", $(this).attr("href").replace("EPaath7-8/subjects.html?","subjects.html?"));
          // }
          //
          // if($currentgrade==7||$currentgrade==8){
          // 	$(this).attr("href", $(this).attr("href").replace("subjects.html?","EPaath7-8/subjects.html?"));
          // }
          // console.log($(this).attr("href"));
          // change the link to direct to another folder for grade 7 and grade 8 emds
        });
      });
    })
    .fail(function(data, textstatus, errortype) {
      alert(
        "get data.json failed, status: " + textstatus + ", error: " + errortype
      );
      // reload the page on json fail
      location.reload();
    })
    .always(function() {
      console.log("data.json complete, appears on complete");
    });
})(jQuery);

/*if (navigator.userAgent.indexOf('Firefox') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Firefox') + 8)) >= 3.6){//Firefox
 //Allow
 } else {
 	alert("please use firefox for better view, thank you");
 }*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
	    extratextblock:[
		{
			datahighlightflag : 'true',
			datahighlightcustomclass : "bold",
			textclass: "chapter_title",
			textdata: data.string.p1text1
		}
		],

	imageblock:[{
		imagestoshow:[
			{
				imgclass: "bg_full",
				imgid : 'coverpage',
				imgsrc: ""
			}
		]
	}]
},

// slide1
{
	contentblockadditionalclass:"grey",
		extratextblock:[
		{
			textclass: "top_text",
			textdata: data.string.p1text3
		},
		{
			textclass: "",
			textdata: data.string.p1text4
		}
	],
	},

	// slide2
	{
		contentblockadditionalclass:"white",
			extratextblock:[
			{
				textclass: "instruction_bot",
				textdata: data.string.p1text5
			},
			{
				textclass: "top_text_instruc",
				textdata: data.string.p1text6
			}
		],

	imageblock:[{
		imagestoshow:[
			{
				imgclass: "middle_img",
				imgid : 'step1',
				imgsrc: ""
			}
		]
	}]
},

// slide3
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text7
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step10',
			imgsrc: ""
		}
	]
}]
},

// slide4
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text8
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step2',
			imgsrc: ""
		}
	]
}]
},

// slide5
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text9
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step3',
			imgsrc: ""
		}
	]
}]
},

// slide6
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text10
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step4',
			imgsrc: ""
		}
	]
}]
},

// slide7
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text11
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step5',
			imgsrc: ""
		}
	]
}]
},

// slide8
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text12
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step13',
			imgsrc: ""
		}
	]
}]
},

// slide9
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text13
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step6',
			imgsrc: ""
		}
	]
}]
},

// slide10
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text14
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step7',
			imgsrc: ""
		}
	]
}]
},

// slide11
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text15
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step8',
			imgsrc: ""
		}
	]
}]
},

// slide12
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text16
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step9',
			imgsrc: ""
		}
	]
}]
},

// slide14
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text17
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step12',
			imgsrc: ""
		}
	]
}]
},

// slide15
{
	contentblockadditionalclass:"white",
		extratextblock:[
		{
			textclass: "instruction_bot",
			textdata: data.string.p1text18
		},
		{
			textclass: "top_text_instruc",
			textdata: data.string.p1text6
		}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "middle_img",
			imgid : 'step11',
			imgsrc: ""
		}
	]
}]
},

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "coverpage", src: imgpath+"frame.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "step1", src: imgpath+"step-01_paper_cutting.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "step2", src: imgpath+"step-02_paper_cutting.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "step3", src: imgpath+"step-03-paper-cutting.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "step4", src: imgpath+"step-04-paper-cutting.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "step5", src: imgpath+"step-05-paper-cutting.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "step6", src: imgpath+"step-06-paper-cutting.png", type: createjs.AbstractLoader.IMAGE},
			{id: "step7", src: imgpath+"step-07-paper-cutting.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "step8", src: imgpath+"step-08-paper-cutting.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "step9", src: imgpath+"step-09-paper-cutting.png", type: createjs.AbstractLoader.IMAGE},
			{id: "step10", src: imgpath+"step-010_paper_cutting.png", type: createjs.AbstractLoader.IMAGE},
			{id: "step11", src: imgpath+"step-11-paper-cutting.png", type: createjs.AbstractLoader.IMAGE},
			{id: "step12", src: imgpath+"step-10-paper-cutting.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "step13", src: imgpath+"step-05a-paper-cutting.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_1", src: soundAsset+"p1_s1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		$nextBtn.hide(0);
		nav_button_controls(1000);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(10):'';
		});
	}
	// function sound_player1(sound_id, next){
	// 	createjs.Sound.stop();
	// 	current_sound = createjs.Sound.play(sound_id);
	// 	current_sound.play();
	// 	// current_sound.on('complete', function(){
	// 	// 	if(next == null)
	// 	// 	navigationcontroller();
	// 	// });
	// }

	// function sound_player_duo(sound_id, sound_id_2){
	// 	createjs.Sound.stop();
	// 	current_sound = createjs.Sound.play(sound_id);
	// 	//current_sound_2 = createjs.Sound.play(sound_id_2);
	// 	current_sound.play();
	// 	current_sound.on('complete', function(){
	// 		$(".dotext").show(0);
	// 		sound_player(sound_id_2);
	// 	});
    //
	// }

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

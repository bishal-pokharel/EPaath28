var imgpath = $ref+"/images/Page02/";
var soundAsset = $ref+"/sounds/page_2/";
//
var s2_p1 = new buzz.sound((soundAsset + "s2_p1.ogg"));
var s2_p2 = new buzz.sound((soundAsset + "s2_p2.ogg"));
var s2_p3 = new buzz.sound((soundAsset + "s2_p3-2.ogg"));
var s2_p4 = new buzz.sound((soundAsset + "s2_p4.ogg"));
var s2_p5 = new buzz.sound((soundAsset + "s2_p5.ogg"));
var s2_p6 = new buzz.sound((soundAsset + "s2_p6.ogg"));
var s2_p7 = new buzz.sound((soundAsset + "s2_p7.ogg"));
var s2_p8 = new buzz.sound((soundAsset + "s2_p8.ogg"));
var s2_p9 = new buzz.sound((soundAsset + "s2_p9.ogg"));
var s2_p10 = new buzz.sound((soundAsset + "s2_p10.ogg"));
var s2_p11 = new buzz.sound((soundAsset + "s2_p11.ogg"));
var s2_p12 = new buzz.sound((soundAsset + "s2_p12.ogg"));
var s2_p13 = new buzz.sound((soundAsset + "s2_p13.ogg"));
var s2_p14 = new buzz.sound((soundAsset + "s2_p14.ogg"));
// var s2_p15 = new buzz.sound((soundAsset + "s2_p15.ogg"));
// var s2_p1 = new buzz.sound((soundAsset + "s2_p1.ogg"));
// var s2_p10 = new buzz.sound((soundAsset + "s2_p10.ogg"));
// var s2_p10 = new buzz.sound((soundAsset + "s2_p10.ogg"));

var soundArray = [s2_p1, s2_p2, s2_p3, s2_p4, s2_p5, s2_p6,
                  s2_p7, s2_p8, s2_p9, s2_p10, s2_p11, s2_p12,
                  s2_p13, s2_p14];
var currentSound =s2_p1;

var content=[
// slide1
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig7
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02a.png"
      }]
    }]
  },
// slide2
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig8
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02b.png"
      },{
        imgclass: "bubbleImg",
        imgsrc: imgpath+ "roti.png"
      }]
    }]
  },
// slide3
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig9
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02c.png"
      },{
        imgclass: "bubbleImg",
        imgsrc: imgpath+ "dhido.png"
      }]
    }]
  },
// slide4
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig10
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02d.png"
      }]
    }]
  },
// slide5
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig11
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02e.png"
      }]
    }]
  },
// slide6
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig12
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02f.png"
      }]
    }]
  },
// slide7
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig13
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02g.png"
      }]
    }]
  },
// slide8
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig14
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02h.png"
      }]
    }]
  },
// slide9
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig15
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02i.png"
      }]
    }]
  },
// slide10
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig16
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02j.png"
      }]
    }]
  },
// slide11
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig17
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02k.png"
      }]
    }]
  },
// slide12
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig18
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02l.png"
      }]
    }]
  },
// slide13
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig19
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02m.png"
      }]
    }]
  },
// slide14
  {
    extratextblock:[{
      textclass: "btmDialogues",
      textdata: data.string.dig20
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass: "coverpage",
        imgsrc: imgpath+ "p02n.png"
      }]
    }]
  },
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 1;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				if(countNext == $total_page - 1)
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 1000);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		switch(countNext){
      case 1:
      case 2:
        $nextBtn.hide(0);
        countNext==1?$(".bubbleImg").delay(1500).fadeIn(1000):$(".bubbleImg").delay(5500).fadeIn(1000);
        soundPlayer(soundArray[countNext], 1);
      break;
			default:
        $nextBtn.hide(0);
        soundPlayer(soundArray[countNext], 1);
        // navigationcontroller();
			break;
		}

		// splitintofractions($(".fractionblock"));
  }

  function soundPlayer (soundId, showNextBtn){
    currentSound = soundId;
    currentSound.play();
    currentSound.bind("ended", function(){
      if (showNextBtn)
        navigationcontroller();
        // countNext<5?$nextBtn.show():ole.footerNotificationHandler.pageEndSetNotification();
    });
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generaltemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
    currentSound.stop();
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
    currentSound.stop();
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

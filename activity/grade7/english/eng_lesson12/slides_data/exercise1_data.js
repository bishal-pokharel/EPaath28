var id = 1;
var charID = 0;
var dialougeImage = 0;
var answer123 = [];
var pos = 0;
var dialogId = 9;
var cache;
var characterDialouges = [
  {
    diaouges: data.string.order1
  }, //
  {
    diaouges: data.string.order2
  }, //

  {
    diaouges: data.string.order3
  }, //

  {
    diaouges: data.string.order4
  }, //

  {
    diaouges: data.string.order5
  }, //

  {
    diaouges: data.string.order6
  }, //

  {
    diaouges: data.string.order7
  }, //

  {
    diaouges: data.string.order8
  }, //

  {
    diaouges: data.string.order9
  }, //

  {
    diaouges: data.string.order10
  } //
];
$.fn.shuffle = function() {
  return this.each(function() {
    var items = $(this)
      .children()
      .clone(true);
    return items.length ? $(this).html($.shuffle(items)) : this;
  });
};

$.shuffle = function(arr) {
  for (
    var j, x, i = arr.length;
    i;
    j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x
  );
  return arr;
};

$(document).ready(function() {
  loadTimelineProgress(1, 1);
  //console.log(characterDialouges);
  characterDialouge();
  sort();
  //this will need a sortable function as well
});
function sort() {
  $("#sortable").sortable({
    stop: function(event, ui) {
      var cache = $("#sortable").html();
      pos = ui.item.index();
      correctans = answer123[pos];
      useranswer = $.trim(ui.item.text());
      // console.log(pos);
      // console.log(correctans);
      //console.log(useranswer);
    }
  });
}
$("#sortable").disableSelection();
function characterDialouge() {
  if (dialogId == 10) {
    $("#order").html("");
    $(".title")
      .html("Congratulations on finishing your exercise.")
      .css({
        position: "absolute",
        top: "48%",
        transform: "translateY(-50%)",
        left: "0",
        width: "100%"
      });
    $("#submit").hide(0);
    return true;
  }
  answered = false;
  attemptcount = 0;
  orders = new Array();
  values = characterDialouges[dialogId];
  //var orders =$.each(characterDialouges,function(key,values){
  id++;
  chDialouges = values.diaouges;
  //console.log(chDialouges);
  var splitSentence = chDialouges.trim().split(/\s+/);
  console.log(splitSentence);

  answer123 = splitSentence;
  //console.log(answer123);

  $.each(answer123, function(i, val) {
    i = i + 1;
    orders[i] =
      '<li class="ui-state-default sortable submit_answer" id="' +
      i +
      '" data="' +
      val +
      '">' +
      val +
      "</li>";
  });
  orders = $.shuffle(orders);

  //});
  appendOrder =
    '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sortable_drag">';
  appendOrder += '<ul id="sortable">';
  appendOrder += orders.join("");
  appendOrder += "</ul>";
  appendOrder += "</div>";
  $("#order").html(appendOrder);
  dialogId++;
}

var answered = false;
var attemptcount = 0;

var totalq = characterDialouges.length;
var correctlyanswered = 0;
$("#activity-page-next-btn-enabled").show(0);
$("#activity-page-next-btn-enabled").click(function() {
  //$("#correct_icon").hide(0);
  if (answered) {
    return answered;
  }
  attemptcount++;
  var answer_submit = [];
  $(".submit_answer").each(function(abcd, dsa) {
    answer_submit.push($(dsa).attr("data"));
  });
  console.log(answer123);
  console.log("answer_submitted" + answer_submit);
  var is_same =
    answer123.length == answer_submit.length &&
    answer123.every(function(element, index) {
      return element === answer_submit[index];
    });

  console.log(is_same);

  if (is_same == true) {
    ole.footerNotificationHandler.pageEndSetNotification();
    if (attemptcount == 1) {
      correctlyanswered++;
    }
    answered = true;
    play_correct_incorrect_sound(true);
    characterDialouge();
    sort();
    $("#incorrect_icon").hide(0);
    //$("#correct_icon").show(0);
  } else {
    $("#incorrect_icon").show(0);
    play_correct_incorrect_sound(false);
  }
});

//http://yelotofu.com/labs/jquery/snippets/shuffle/demo.html

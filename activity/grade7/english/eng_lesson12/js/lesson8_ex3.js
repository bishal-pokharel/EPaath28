//$(document).ready(function(){
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var questionCount =0;
	var correctlyanswered = 0;
	var $total_page = 6;
	loadTimelineProgress($total_page,questionCount+1);

	$('.title').text(data.string.exerciseTitle);
	var content = [
	{
		question : data.string.q7,
		answers: [
		{ans : data.string.name19},
		{ans : data.string.name20,
		correct : "correct"},
		{ans : data.string.name21},
		{ans : data.string.name22}
		],
		img : $ref+"/exercise_images/pelts.jpg"
	},
	{
		question : data.string.q8,
		answers: [
		{ans : data.string.name23},
		{ans : data.string.name24},
		{ans : data.string.name25},
		{ans : data.string.name26,
			correct : "correct"}
		],
		img : $ref+"/exercise_images/snow_leopard.jpg"
	},
	{
		question : data.string.q9,
		answers: [
		{ans : data.string.name27},
	
		{ans : data.string.name28},
		{ans : data.string.name29,
			correct : "correct"},
		{ans : data.string.name30}
		],
		img : $ref+"/exercise_images/rhino.jpg"
	},
	{
		question : data.string.q10,
		answers: [
		{ans : data.string.name31,
			correct : "correct"},
		{ans : data.string.name32
		},
		{ans : data.string.name33},
		{ans : data.string.name34}
		],
		img : $ref+"/exercise_images/extinct.jpg"
	},
	{
		question : data.string.q11,
		answers: [
		{ans : data.string.name35},
		{ans : data.string.name36},
		{ans : data.string.name37,
			correct : "correct"},
		{ans : data.string.name38}
		],
		img : $ref+"/exercise_images/rhino.jpg"
	}
	];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}

	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	$board.on('click','.neutral',function () {
		// console.log("what");
		if(answered){
			return answered;
		}
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			attemptcount++;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			console.log(attemptcount);
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
		}
		else {
			attemptcount++;
			$this.addClass('wrong').removeClass('neutral');
			play_correct_incorrect_sound(false);
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
	loadTimelineProgress($total_page,questionCount+1);
	if(questionCount<5){
			qA();
		}
		else if (questionCount==5){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').hide(0);
			// .html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				// "position": "absolute",
				// "top": "48%",
				// "transform": "translateY(-50%)"
			// });
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);

			$(".closebtn").on("click",function(){
				ole.activityComplete.finishingcall();
			});
		}
	});

})(jQuery);

var imgpath = $ref+"/images/";

var audio1 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/1.ogg");
var audio2 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/2.ogg");
var audio3 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/3.ogg");
var audio4 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/4.ogg");
var audio5 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/5.ogg");
var audio6 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/6.ogg");
var audio7 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/7.ogg");
var audio8 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/8.ogg");
var audio9 = new Audio("activity/grade8/english/_grade8_documenting_experiences/sounds/page2/9.ogg");

var playList = [audio1, audio2, audio3, audio4, audio5, audio6, audio7, audio8, audio9];
var playThisDialog = playList[0];

var content=[
	{	
		uppertextblock:[
			{
				textclass:"title",
				textdata: "When a whole species of animals is gone, that particular species will then <span style='color:#333;font-weight:bold;'>definitely</span> become extinct. "
			},
			
			{
				textclass:"question1",
				textdata: " Tasmanian Tiger and West African Rhinoceros are some of the animals that are now extinct. "
			}
			
		],
		
		
		
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"subwrapper",
						imgsrc: imgpath+"extinct.png"
					}
				]
			}
		],

	}
	
	
	
	

	

	
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	
	
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		

		//call instruction block controller
		// instructionblockcontroller($board);
		$(".yes").on("click", function(){ 
			$(this).addClass("correct");
			$nextBtn.show(0);
		});

		$(".no").on("click", function(){ 
			$(this).addClass("incorrect");
		});


	}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		/*OR, call templates like this if you have more than one template
		to call*/

		if($.isArray(playList[countNext])){
			if(typeof(playThisDialog) != 'undefined') {
				playThisDialog.pause();
			}
			playThisDialog = playList[countNext];
			if (playThisDialog.currentTime !== 0 && (playThisDialog.currentTime > 0 && playThisDialog.currentTime < playThisDialog.duration)) {
    playThisDialog.currentTime = 0;
}
			playThisDialog.play();
			playThisDialog.addEventListener('ended', function(){
				// done playing
				$nextBtn.show(0);
			});
		}
		else if(!$.isArray(playList[countNext])){
			if(typeof(playThisDialog) != 'undefined') {
				playThisDialog.pause();
			}
			playThisDialog = playList[countNext];
			if (playThisDialog.currentTime !== 0 && (playThisDialog.currentTime > 0 && playThisDialog.currentTime < playThisDialog.duration)) {
    playThisDialog.currentTime = 0;
}
			playThisDialog.play();
			playThisDialog.addEventListener('ended', function(){
				// done playing
				$nextBtn.show(0);
			});
		}
		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();
		console.log(countNext);
		//$nextBtn.hide(0);
		
			
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */
		/*countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;*/			
		
		
	});

/*=====  End of Templates Controller Block  ======*/
});
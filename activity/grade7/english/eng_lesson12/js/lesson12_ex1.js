$("document").ready(function(){

	function allowDrop(ev)
	{
	    ev.preventDefault();
	}

	function drag(ev)
	{
	    console.log('drag');
	    ev.dataTransfer.setData("Text",ev.target.id);
	}

	function drop(ev)
	{
	    console.log('drop');
	    ev.preventDefault();
	    var data=ev.dataTransfer.getData("Text");
	  
	    var currentText = $(ev.target).text();
	    $(ev.target).text($('#' + data).text() + ' ' + currentText);
	    $('#' + data).remove();
	}
});
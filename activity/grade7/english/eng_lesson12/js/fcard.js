$(".title").text(data.string.title);
$(document).ready(function() {
  $(".title").text(data.string.title);
  var content = [
    {
      words: data.string.word1,
      description: data.string.des1,
      img: $ref + "/exercise_images/probably.jpg"
    }, //
    {
      words: data.string.word2,
      description: data.string.des2,
      img: $ref + "/exercise_images/den.jpg"
    }, //

    {
      words: data.string.word3,
      description: data.string.des3,
      img: $ref + "/exercise_images/ferocious.jpg"
    },

    {
      words: data.string.word4,
      description: data.string.des4,
      img: $ref + "/exercise_images/terrible.jpg"
    }, //
    {
      words: data.string.word5,
      description: data.string.des5,
      img: $ref + "/exercise_images/endangered.jpg"
    }, //
    {
      words: data.string.word6,
      description: data.string.des6,
      img: $ref + "/exercise_images/species.jpg"
    }, //
    {
      words: data.string.word7,
      description: data.string.des7,
      img: $ref + "/exercise_images/extinction.jpeg"
    }, //
    {
      words: data.string.word8,
      description: data.string.des8,
      img: $ref + "/exercise_images/extinct.jpg"
    }, //
    {
      words: data.string.word9,
      description: data.string.des9,
      img: $ref + "/exercise_images/eventually.png"
    }, //
    {
      words: data.string.word10,
      description: data.string.des10,
      img: $ref + "/exercise_images/dwindle.jpg"
    }, //
    {
      words: data.string.word11,
      description: data.string.des11,
      img: $ref + "/exercise_images/particular.jpg"
    }, //
    {
      words: data.string.word12,
      description: data.string.des12,
      img: $ref + "/exercise_images/illegal.jpg"
    }, //
    {
      words: data.string.word13,
      description: data.string.des13,
      img: $ref + "/exercise_images/pelts.jpg"
    }, //
    {
      words: data.string.word14,
      description: data.string.des14,
      img: $ref + "/exercise_images/medicinalpurposes.jpg"
    }, //
    {
      words: data.string.word15,
      description: data.string.des15,
      img: $ref + "/exercise_images/certainly.jpg"
    }, //
    {
      words: data.string.word16,
      description: data.string.des16,
      img: $ref + "/exercise_images/habitat.jpg"
    }, //
    {
      words: data.string.word17,
      description: data.string.des17,
      img: $ref + "/exercise_images/originally.jpg"
    }, //
    {
      words: data.string.word18,
      description: data.string.des18,
      img: $ref + "/exercise_images/suitable.jpg"
    }, //
    {
      words: data.string.word19,
      description: data.string.des19,
      img: $ref + "/exercise_images/native.jpg"
    }, //
    {
      words: data.string.word20,
      description: data.string.des20,
      img: $ref + "/exercise_images/indigenous.gif"
    }, //
    {
      words: data.string.word21,
      description: data.string.des21,
      img: $ref + "/exercise_images/aware.jpg"
    }, //
    {
      words: data.string.word22,
      description: data.string.des22,
      img: $ref + "/exercise_images/possibly.png"
    }, //
    {
      words: data.string.word23,
      description: data.string.des23,
      img: $ref + "/exercise_images/conscious.png"
    }, //
    {
      words: data.string.word24,
      description: data.string.des24,
      img: $ref + "/exercise_images/destroy.jpg"
    }, //
    {
      words: data.string.word25,
      description: data.string.des25,
      img: $ref + "/exercise_images/clumsy.gif"
    }, //
    {
      words: data.string.word26,
      description: data.string.des26,
      img: $ref + "/exercise_images/wonder.jpg"
    }, //
    {
      words: data.string.word27,
      description: data.string.des27,
      img: $ref + "/exercise_images/exist.jpg"
    }, //
    {
      words: data.string.word28,
      description: data.string.des28,
      img: $ref + "/exercise_images/mauritius.png"
    }, //
    {
      words: data.string.word29,
      description: data.string.des29,
      img: $ref + "/exercise_images/century.jpg"
    }, //
    {
      words: data.string.word30,
      description: data.string.des30,
      img: $ref + "/exercise_images/fascinating.jpg"
    }, //
    {
      words: data.string.word31,
      description: data.string.des31,
      img: $ref + "/exercise_images/slang.jpg"
    }, //
    {
      words: data.string.word32,
      description: data.string.des32,
      img: $ref + "/exercise_images/commonsense.jpg"
    }, //
    {
      words: data.string.word33,
      description: data.string.des33,
      img: $ref + "/exercise_images/flattering.jpg"
    }, //
    {
      words: data.string.word34,
      description: data.string.des34,
      img: $ref + "/exercise_images/nickname.jpg"
    } //
  ];
  console.log(content);
  displaycontent();
  function displaycontent() {
    var words = $.each(content, function(key, value) {
      words = value.words;
      description = value.description;
      image = value.img;
      // console.log(description);
      appendTab = "<li>";
      appendTab +=
        '<a href="#' + words + '" data-toggle="tab"  >' + words + "</a>";
      appendTab += "</li>";
      $("#myTab").append(appendTab);

      appendcontent = '<div id="' + words + '" class="tab-pane ">';
      appendcontent += '<div class="f1_container">';
      appendcontent += '<div class="shadow f1_card">';
      appendcontent += '<div class="front face">';
      appendcontent += "<p>" + words + "</p>";
      appendcontent += "</div>";
      appendcontent += '<div class="back face center">';
      appendcontent +=
        '<img src="' + image + '" class="tab_image img-responsive"/>';
      appendcontent += '<p class="textroll">';
      appendcontent += description;
      appendcontent += "</p>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      $("#myTabContent").append(appendcontent);
    });
  }
  /*
   * function qA() { var source = $('#qA-templete').html(); var
   * template = Handlebars.compile(source); var html =
   * template(content); //$board.html(html); // console.log(html); }
   *
   * qA();
   */
});
$(document).ready(function() {
  loadTimelineProgress(1, 1);
  $(".f1_container").click(function() {
    //alert("ok");
    $(this).toggleClass("active");
  });
  ole.footerNotificationHandler.pageEndSetNotification();
});

imageAsset = $ref+"/slides_images/";
soundAsset = $ref+"/sounds/slide3/";

var dialog1 = new buzz.sound(soundAsset+"1.ogg");

var dialog2part1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"3.ogg");
var dialog2 = [dialog2part1,dialog2part2];

var dialog3 = new buzz.sound(soundAsset+"4.ogg");
var dialog4 = new buzz.sound(soundAsset+"5.ogg");
var dialog5 = new buzz.sound(soundAsset+"6.ogg");
var dialog6 = new buzz.sound(soundAsset+"7.ogg");
var dialog7 = new buzz.sound(soundAsset+"8.ogg");

var dialog8part1 = new buzz.sound(soundAsset+"9.ogg");
var dialog8part2 = new buzz.sound(soundAsset+"10.ogg");
var dialog8 = [dialog8part1,dialog8part2];

var soundcontent = [dialog1, dialog2, dialog3, dialog4, dialog5, dialog6,dialog7,dialog8];

var content=[
	{
		bgImgSrc : imageAsset+"15.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			}
			
		],
		lineCountDialog : [data.string.digs3_1],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"zoo.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			}
		],
		lineCountDialog : [data.string.digs3_2part1,
						   data.string.digs3_2part2],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"16.jpg",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			}
		],
			lineCountDialog : [data.string.digs3_3],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"17.jpg",
		forwhichdialog : "dialog4",
		talkHeadImages : [
		{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			}
			
		],
		lineCountDialog : [data.string.digs3_4],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"18.jpg",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			}
		
		],
		lineCountDialog : [data.string.digs3_5],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"19.jpg",
		forwhichdialog : "dialog6",
		talkHeadImages : [
						{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			}
			
		],
		lineCountDialog : [data.string.digs3_6],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"20.jpg",
		forwhichdialog : "dialog7",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			}
			
		],
		lineCountDialog : [data.string.digs3_7],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"21.jpg",
		forwhichdialog : "dialog8",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			}
			
		],
		lineCountDialog : [data.string.digs3_8part1,
						   data.string.digs3_8part2],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 8;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		/*this function binds appropriate events handlers to the 
		audio as required*/
		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
var correctCards = 0;
	$(init);

	function init() {

		  // Hide the success message
		  $('#successMessage').hide(0);
		  $('#successMessage').css( {
		    left: '580px',
		    top: '250px',
		    width: 0,
		    height: 0
		  } );

		  // Reset the game
		  correctCards = 0;
		  $('#cardPile').html( '' );
		  $('#cardSlots').html( '' );

		  // Create the pile of shuffled cards
		  var words = [ 'Perhaps ', 'the', 'dinosaurs', 'could', 'have', 'been', 'saved'];
		 // var numbers = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
		  words.sort( function() { return Math.random() - .5 } );
		  for ( var i=0; i<8; i++ ) {
		    $('<div>' + words[i] + '</div>').data( 'number', words[i] ).attr( 'id', 'card'+words[i] ).appendTo( '#cardPile' ).draggable( {
		      containment: '#content',
		      stack: '#cardPile div',
		      cursor: 'move',
		      revert: true
		    } );
		  }

		  // Create the card slots
		  var numbers = [ 1, 2, 3, 4, 5, 6, 7,8 ];
		//  var words = [ 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten' ];
		  for ( var i=1; i<=8; i++ ) {
		    $('<div>' + numbers[i-1] + '</div>').data( 'number', i ).appendTo( '#cardSlots' ).droppable( {
		      accept: '#cardPile div',
		      hoverClass: 'hovered',
		      drop: handleCardDrop
		    } );
		  }

		}
	function handleCardDrop( event, ui ) {
		  var slotNumber = $(this).data( 'number' );
		  var cardNumber = ui.draggable.data( 'number' );
          console.log(slotNumber);
          console.log(cardNumber);
		  // If the card was dropped to the correct slot,
		  // change the card colour, position it directly
		  // on top of the slot, and prevent it being dragged
		  // again

		  if ( slotNumber == cardNumber ) {
		    ui.draggable.addClass( 'correct' );
		    ui.draggable.draggable( 'disable' );
		    $(this).droppable( 'disable' );
		    ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
		    ui.draggable.draggable( 'option', 'revert', false );
		    correctCards++;
		  } 
		  
		  // If all the cards have been placed correctly then display a message
		  // and reset the cards for another go

		  if ( correctCards == 10 ) {
		    $('#successMessage').show(0);
		    $('#successMessage').animate( {
		      left: '380px',
		      top: '200px',
		      width: '400px',
		      height: '100px',
		      opacity: 1
		    } );
		  }

		}
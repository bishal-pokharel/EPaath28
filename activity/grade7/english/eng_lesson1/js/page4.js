imageAsset = $ref+"/slide_images/";
soundAsset = $ref+"/sounds/slide4/";
background_image = $ref+"/images/page4/"


var dialog1 = new buzz.sound(soundAsset+"lesson1_slide4_1.ogg");
var dialog2 = new buzz.sound(soundAsset+"lesson1_slide4_2.ogg");
var dialog3 = new buzz.sound(soundAsset+"lesson1_slide4_3.ogg");
var dialog4 = new buzz.sound(soundAsset+"lesson1_slide4_4.ogg");
var dialog5 = new buzz.sound(soundAsset+"lesson1_slide4_5.ogg");


//This will have all the sound 
var soundcontent = [dialog1, dialog2, dialog3, dialog4,
					dialog5]; 

var content=[
	{
		bgImgSrc : background_image+"1b.jpg",//image
		forwhichdialog : "dialog1", //audio
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
		],
		lineCountDialog : [data.string.dig15],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : background_image+"1b.jpg",//image
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.dig16,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,

	},
	
	{
		bgImgSrc :  background_image+"blocks.jpg",//image
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.dig18,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc :  background_image+"building.gif",//image
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
		],
		lineCountDialog : [data.string.dig18_1,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc :  background_image+"drawing.png",//image
		forwhichdialog : "dialog5",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.dig18_2,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 5;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}
			playerbinder();
	}

	slide1();

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);




//$(document).ready(function(){
(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  var content = [
    {
      question: data.string.q1,
      answers: [
        { ans: data.string.name1 },
        { ans: data.string.name2, correct: "correct" },
        { ans: data.string.name3 },
        { ans: data.string.name4 }
      ],
      img: $ref + "/exercise_image/india.jpg"
    },
    {
      question: data.string.q2,
      answers: [
        { ans: data.string.name5, correct: "correct" },
        { ans: data.string.name6 },
        { ans: data.string.name7 },
        { ans: data.string.name8 }
      ],
      img: $ref + "/exercise_image/engineer.jpg"
    },
    {
      question: data.string.q3,
      answers: [
        { ans: data.string.name9 },

        { ans: data.string.name10 },
        { ans: data.string.name11, correct: "correct" },
        { ans: data.string.name12 }
      ],
      img: $ref + "/exercise_image/kolkata1.jpg"
    },
    {
      question: data.string.q4,
      answers: [
        { ans: data.string.name13 },
        { ans: data.string.name14, correct: "correct" },
        { ans: data.string.name15 },
        { ans: data.string.name16 }
      ],
      img: $ref + "/exercise_image/china.jpg"
    },
    {
      question: data.string.q5,
      answers: [
        { ans: data.string.name17 },
        { ans: data.string.name18 },
        { ans: data.string.name19, correct: "correct" },
        { ans: data.string.name20 }
      ],
      img: $ref + "/exercise_image/solukhumbhu.jpg"
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }

  qA();
  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);
  var correctlyanswered = 0;

  $board.on("click", ".neutral", function() {
    // console.log("what");
    if (answered) {
      return answered;
    }
    attemptcount++;
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      play_correct_incorrect_sound(true);
    } else {
      $this.addClass("wrong").removeClass("neutral");
      play_correct_incorrect_sound(false);
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 5) {
      qA();
    } else if (questionCount == 5) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title")
        .html(
          "Congratulations on finishing your exercise <br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .css({
          position: "absolute",
          top: "48%",
          transform: "translateY(-50%)"
        });
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
})(jQuery);

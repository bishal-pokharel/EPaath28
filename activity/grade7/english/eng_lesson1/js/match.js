var correctCards = 0;
$(init);

function init() {
  loadTimelineProgress(1, 1);
  // Hide the success message
  $("#successMessage").hide(0);
  $("#successMessage").css({
    left: "580px",
    top: "250px",
    width: 0,
    height: 0
  });

  // Reset the game
  correctCards = 0;
  $("#cardPile").html("");
  $("#cardSlots").html("");

  // Create the pile of shuffled cards
  /*var numbers = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
		 numbers.sort( function() { return Math.random() - .5 } );*/
  var fruits = [
    "Laborer",
    "Doctor",
    "Police",
    "Civil engineer",
    "Farmer",
    "Journalist",
    "Musician",
    "Teacher",
    "Mountaineer"
  ];
  var orgFruits = fruits;
  fruits.sort(function() {
    return Math.random() - 0.5;
  });
  // var fruit="apple.jpg";

  for (var i = 1; i <= 9; i++) {
    $(
      '<div id="drag' +
        orgFruits[i - 1].replace(/ /g, "") +
        '">' +
        orgFruits[i - 1] +
        "</div>"
    )
      .data("orgFruits", i)
      .appendTo("#cardPile")
      .draggable({
        containment: "#wrapper",
        stack: "#cardPile div",
        cursor: "move",
        revert: true
      });
  }

  // Create the card slots
  // var words = [ 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten' ];
  orgFruits.sort(function() {
    return Math.random() - 0.5;
  });

  for (var i = 0; i < 9; i++) {
    var appendText =
      "<div id='" +
      fruits[i].replace(/ /g, "") +
      "' class='col-xs-4 col-sm-4 col-md-4 col-lg-4 droppable'>";
    appendText +=
      "<img src='activity/grade7/english/eng_lesson1/images/" +
      fruits[i].replace(/ /g, "").toLowerCase() +
      ".jpg' class='image_style img-responsive'>";
    appendText += "</img>";
    appendText += "</div>";
    //$("#cardSlots").append(appendText);
    $(appendText)
      .data("fruits", fruits[i])
      .attr("id", fruits[i].replace(/ /g, ""))
      .appendTo("#cardSlots")
      .droppable({
        accept: "#cardPile div",
        hoverClass: "hovered",
        drop: handleCardDrop
      });
  }
}

function handleCardDrop(event, ui) {
  // var slotNumber = $(this).data( 'fruits' );
  //var cardNumber = ui.draggable.data( 'fruits' );
  // console.log(slotNumber);
  //console.log(cardNumber);
  var slotNumber = $(this).attr("id");
  var cardNumber = ui.draggable.text();

  // If the card was dropped to the correct slot,
  // change the card colour, position it directly
  // on top of the slot, and prevent it being dragged
  // again

  if (slotNumber == cardNumber.replace(/ /g, "")) {
    ui.draggable.addClass("correct");
    ui.draggable.draggable("disable");
    ui.draggable.css("font-size", "20px");
    $(this).droppable("disable");
    // cardnumber.hide(0);
    ui.draggable.position({
      of: $(this),
      my: "left top",
      at: "left top"
    });
    ui.draggable.draggable("option", "revert", false);
    correctCards++;
    //To place the draggable inside of the droppable
    $("#cardPile")
      .find("#drag" + slotNumber)
      .remove();
    $("#cardPile")
      .find("#drag" + cardNumber)
      .remove();
    //$(this).find("img").css('height','60px');
    $(this).append(
      "<div class='col-md-12 col-sm-12 col-xs-12 end'>" + cardNumber + "</div>"
    );
  }

  // If all the cards have been placed correctly then display a message
  // and reset the cards for another go

  if (correctCards == 9) {
    $(".title").text("Congratulations on finishing up your exercise.");

    $(".finishtxt").show(0);
  }

  $(".closebtn").click(function() {
    ole.activityComplete.finishingcall();
  });
}

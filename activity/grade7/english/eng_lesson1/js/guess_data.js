var id = 0;
$(document).ready(function() {
  var content = [
    {
      professionDesc: data.string.profD1,
      profession: data.string.prof1,
      img: $ref + "/images/civilengineer.jpg"
    }, //
    {
      professionDesc: data.string.profD2,
      profession: data.string.prof2,
      img: $ref + "/images/doctor.jpg"
    }, //

    {
      professionDesc: data.string.profD3,
      profession: data.string.prof3,
      img: $ref + "/images/musician.jpg"
    }, //

    {
      professionDesc: data.string.profD4,
      profession: data.string.prof4,
      img: $ref + "/exercise_image/journalist.png"
    }, //

    {
      professionDesc: data.string.profD5,
      profession: data.string.prof5,
      img: $ref + "/images/mountaineer.jpg"
    }, //

    {
      professionDesc: data.string.profD6,
      profession: data.string.prof6,
      img: $ref + "/images/teacher.jpg"
    }, //
    {
      professionDesc: data.string.profD8,
      profession: data.string.prof8,
      img: $ref + "/images/farmer.jpg"
    }, //
    {
      professionDesc: data.string.profD9,
      profession: data.string.prof9,
      img: $ref + "/images/labour.jpg"
    }, //
    {
      professionDesc: data.string.profD10,
      profession: data.string.prof10,
      img: $ref + "/images/police.jpg"
    } //
  ];
  // console.log(content);

  displayContents();

  function displayContents() {
    loadTimelineProgress(content.length + 1, 1);
    var context = $.each(content, function(key, values) {
      id++;
      desc = values.professionDesc;
      prof = values.profession;
      image = values.img;

      appendContents = '<div class="question' + id + '">';
      appendContents += '<div class="row">';
      appendContents += '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';

      appendContents += '<section class="main" style="height:0 !important;">'; // This will have the image section
      appendContents += '<ul class="ch-grid">';
      appendContents += "<li>";
      appendContents += '<div class="ch-item">';
      appendContents += '<div class="ch-info">';
      appendContents += '<div class="ch-info-front ch-img-1">';

      appendContents +=
        '<img src="' + image + '" class="question_image img-responsive"/>';
      appendContents += "</div>";
      appendContents += "</div>";
      appendContents += "</div>";
      appendContents += "</li>";
      appendContents += "</ul>";
      appendContents += "</section>";
      appendContents += "</div>";

      appendContents += '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
      appendContents += '<section class="hints">'; //Thie will have the hint click option
      appendContents += '<div class="hint">';
      appendContents += desc;
      appendContents += "</div>";
      appendContents += "</section>";

      appendContents += '<section class="check_answer">';
      appendContents += '<div class="answer1">';
      appendContents += "Check answer";
      appendContents += "</div>";
      appendContents += "</section>";

      appendContents += '<section class="profession">';
      appendContents += '<div class="answer text_align_center">';
      appendContents += prof;
      appendContents += "</div>";
      appendContents += "</section>";

      appendContents += "</div>";
      appendContents += " </div>";
      appendContents += " </div>";

      $(".contents_guess").append(appendContents);
    });
  }
});

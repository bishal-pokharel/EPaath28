var buttonID = 1;
var id_num = 1;
$(document).ready(function() {
  $(".sitemakers").click(function() {
    $(".hint").fadeIn(3000);
    $(".check_answer").fadeIn(3000);
  });

  $(".check_answer").click(function() {
    $(".answer").show(0);
    $("#activity-page-next-btn-enabled").show(0);
  });

  $("#activity-page-next-btn-enabled").click(function() {
    $("#activity-page-next-btn-enabled").hide(0);
    console.log("ok");
    var id_num = buttonID;
    loadTimelineProgress(10, id_num + 1);
    buttonID = buttonID + 1;
    var next_id = buttonID;
    console.log(id_num);
    console.log(next_id);
    $(".question" + id_num).hide(0);
    $(".question" + next_id).show(0);

    //hide the answer only

    $(".answer").hide(0);
    $(".hint").hide(0);
    $(".check_answer").hide(0);

    if (next_id == 10) {
      $(".title").text("Congratulations on finishing up your exercise");
      $(".exercise_button").hide(0);
      $("#activity-page-next-btn-enabled").hide(0);
      $(".sitemakers").hide(0);

      ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
  $(".exercise_button").click(function() {
    location.reload();
  });
});

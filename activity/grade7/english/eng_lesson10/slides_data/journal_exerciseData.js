var journalId =0;
$(document).ready(function(){
	$(".title").text(data.string.anne);
	var journalTexts = [
    {
       journal : "",
       day : "",
       img1 : $ref+"/exercise_images/writing_a_diary_anne_frank.png"
    },
	{
		journal : data.string.journal1,
		day : data.string.st1,
		img : $ref+"/exercise_images/annefrank_diary.jpg"
	},//
	{
		journal : data.string.journal2,
		day : data.string.st2,
		img : $ref+"/exercise_images/family.jpg"
	},
	{
		journal : data.string.journal3,
		day : data.string.st3,
		img : $ref+"/exercise_images/camp.jpg"
	},
	{
		journal : data.string.journal4,
		day : data.string.st4,
		img : $ref+"/exercise_images/miep.jpg"
	},
	{
		journal : data.string.journal5,
		day : data.string.st5,
		img : $ref+"/exercise_images/room.jpg"
	},
	{
		journal : data.string.journal6,
		day : data.string.st6,
		img : $ref+"/exercise_images/jews.jpg"
	},
	{
		journal : data.string.journal7,
		day : data.string.st7,
		img : $ref+"/exercise_images/peace.jpg"
	},
	{
		journal : data.string.journal8,
		day : data.string.st8,
		img : $ref+"/exercise_images/prisoners.jpg"
	},
	{
		journal : data.string.journal9,
		day : data.string.st9,
		img : $ref+"/exercise_images/war.jpg"
	},
	{
		journal : data.string.journal10,
		day : data.string.st10,
		img : $ref+"/exercise_images/auschwitz.jpg"
	},

	];
 console.log(journalTexts);
    loadTimelineProgress(11,1);
    journalContents();
	function journalContents(){
		var jour =$.each(journalTexts,function(key,values){
			journalId++;
			contents = values.journal;
			days =values.day;
			image = values.img;
            image1=values.img1;
            //console.log(contents);
			//console.log(days);
			appendDate = '<p class="para_date' + journalId + '">';
			appendDate += days;
			appendDate +='</p>';
		  $(".para_dates").append(appendDate);

		  appendContents = '<p class="para_content' + journalId + '">';
		  appendContents += contents;
		  appendContents +='</p>';
		  appendImages ='<img src="'+ image + '" class="annne_images img-responsive" id="anne_img'+journalId +'"/>';
		  appendImages1 ='<img src="'+ image1 + '" class="img-responsive" id="anne_img'+journalId +'"/>';

		  $(".images_jhammak").append(appendImages);
            journalId==1?$(".coverpage").append(appendImages1):"";
            $(".paragraph_contents").append(appendContents);
		//console.log(appendJournal);
		//$(".journal_contents").append(appendJournal);
		});
	}
	// function to add the journal
});

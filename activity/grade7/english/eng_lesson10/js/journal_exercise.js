var id=1;
var buttonId =1;

$(document).ready(function(){
var soundAsset = $ref+"/sounds/";
var s1_p1 = new buzz.sound((soundAsset + "s1_p1.ogg"));
var s1_p2 = new buzz.sound((soundAsset + "s1_p2.ogg"));
var s1_p3 = new buzz.sound((soundAsset + "s1_p3.ogg"));
var s1_p4 = new buzz.sound((soundAsset + "s1_p4.ogg"));
var s1_p5 = new buzz.sound((soundAsset + "s1_p5.ogg"));
var s1_p6 = new buzz.sound((soundAsset + "s1_p6.ogg"));
var s1_p7 = new buzz.sound((soundAsset + "s1_p7.ogg"));
var s1_p8 = new buzz.sound((soundAsset + "s1_p8.ogg"));
var s1_p9 = new buzz.sound((soundAsset + "s1_p9.ogg"));
var s1_p10 = new buzz.sound((soundAsset + "s1_p10.ogg"));
var s1_p11 = new buzz.sound((soundAsset + "s1_p11.ogg"));
var soundArray = [s1_p1, s1_p2, s1_p3, s1_p4, s1_p5, s1_p6,
									s1_p7, s1_p8, s1_p9, s1_p10, s1_p11];
var currentSound = s1_p1;
currentSound.play();
	$("#activity-page-next-btn-enabled").click(function(){
		var journalid = buttonId;
		buttonId = buttonId + 1;
		soundPlayer(soundArray[buttonId-1], journalid);
      loadTimelineProgress(11,buttonId);
      var next_id = buttonId;
			// console.log(soundArray[journalid]);
      $(".coverpage").addClass("coverpage1").removeClass("coverpage");

		$(".para_date" + journalid).hide(0);
		$(".para_content" + journalid).hide(0);
		$("#anne_img" + journalid).hide(0);
		$("#anne_img12").hide(0);


		$("#anne_img" + next_id).show(0);
		$(".para_date" + next_id).show(0);
		$(".para_content" + next_id).show(0);
		$("#activity-page-prev-btn-enabled").show(0);

		//need to function the button clicks
	   $(".para_date1").hide(0);
	   $(".para_content1").hide(0);
	   // if(journalid >= 10) {
		 //   $("#activity-page-next-btn-enabled").hide(0);
		 //   $("#activity-page-prev-btn-enabled").show(0);
			//  $(".images_jhammak").append('<img src='+$ref+'/exercise_images/anne_frank_car.jpg class="annne_images img-responsive">');
		 //   //$(".para_dates").hide(0);
		 //   ole.footerNotificationHandler.lessonEndSetNotification();
	   // }
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		   $(".para_dates").show(0);
		var journalid = buttonId;
		buttonId = buttonId - 1;
		soundPlayer(soundArray[buttonId-1], journalid);
        loadTimelineProgress(11,buttonId);
        var next_id = buttonId;
		console.log(journalid);
		console.log(next_id);
		 $("#activity-page-next-btn-enabled").show(0);
		$(".para_date" + journalid).hide(0);
		$(".para_content" + journalid).hide(0);
		$("#anne_img" + journalid).hide(0);

		$("#anne_img" + next_id).show(0);
		$(".para_date" + next_id).show(0);
		$(".para_content" + next_id).show(0);
		//$("#activity-page-prev-btn-enabled").show(0);

		//need to function the button clicks
	   if(journalid ==2) {
           $(".coverpage1").addClass("coverpage").removeClass("coverpage1");
		   $("#activity-page-prev-btn-enabled").hide(0);
		   $("#activity-page-next-btn-enabled").show(0);
	   }
	});

	$("#activity-page-next-btn-enabled").hide(0);
	function soundPlayer(soundId, journalid){
		$("#activity-page-next-btn-enabled, #activity-page-prev-btn-enabled").hide(0);
		currentSound.stop();
		currentSound = soundId;
		currentSound.play();
		currentSound.bind("ended", function(){
			 if(journalid >= 10) {
				 $("#activity-page-next-btn-enabled").hide(0);
				 $("#activity-page-prev-btn-enabled").show(0);
				 ole.footerNotificationHandler.lessonEndSetNotification();

			 }else{
				 $("#activity-page-next-btn-enabled, #activity-page-prev-btn-enabled").show(0);
			 }
		});
	}

	$(".closebtn").click(function(){
		currentSound.stop();
			$("#activity-page-next-btn-enabled").show(0);
	});
});

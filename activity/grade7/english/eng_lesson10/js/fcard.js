$(".title").text(data.string.title);
$(document).ready(function() {
  $(".title").text(data.string.exerciseTitle2);
  var content = [
    {
      words: data.string.word1,
      description: data.string.des1,
      img: $ref + "/exercise_images/invaded.jpg"
    }, //
    {
      words: data.string.word2,
      description: data.string.des2,
      img: $ref + "/exercise_images/subjugate.jpg"
    }, //

    {
      words: data.string.word3,
      description: data.string.des3,
      img: $ref + "/exercise_images/secret.jpg"
    }, //

    {
      words: data.string.word4,
      description: data.string.des4,
      img: $ref + "/exercise_images/diary.png"
    }, //
    {
      words: data.string.word5,
      description: data.string.des5,
      img: $ref + "/exercise_images/argument.jpg"
    }, //
    {
      words: data.string.word6,
      description: data.string.des6,
      img: $ref + "/exercise_images/adventure.jpg"
    }, //
    {
      words: data.string.word7,
      description: data.string.des7,
      img: $ref + "/exercise_images/worldwar.jpg"
    }, //
    {
      words: data.string.word8,
      description: data.string.des8,
      img: $ref + "/exercise_images/camp.jpg"
    }, //
    {
      words: data.string.word9,
      description: data.string.des9,
      img: $ref + "/exercise_images/suffering.jpg"
    }, //
    {
      words: data.string.word10,
      description: data.string.des10,
      img: $ref + "/exercise_images/wine.jpg"
    }, //
    {
      words: data.string.word11,
      description: data.string.des11,
      img: $ref + "/exercise_images/annexe.jpg"
    } //
  ];
  console.log(content);
  displaycontent();
  function displaycontent() {
    var words = $.each(content, function(key, value) {
      words = value.words;
      description = value.description;
      image = value.img;
      // console.log(description);
      appendTab = "<li>";
      appendTab +=
        '<a href="#' + words + '" data-toggle="tab"  >' + words + "</a>";
      appendTab += "</li>";
      $("#myTab").append(appendTab);

      appendcontent = '<div id="' + words + '" class="tab-pane ">';
      appendcontent += '<div class="f1_container">';
      appendcontent += '<div class="shadow f1_card">';
      appendcontent += '<div class="front face">';
      appendcontent += "<p>" + words + "</p>";
      appendcontent += "</div>";
      appendcontent += '<div class="back face center">';
      appendcontent +=
        '<img src="' + image + '" class="tab_image img-responsive"/>';
      appendcontent += '<p class="textroll">';
      appendcontent += description;
      appendcontent += "</p>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      $("#myTabContent").append(appendcontent);
    });
  }
  /*
   * function qA() { var source = $('#qA-templete').html(); var
   * template = Handlebars.compile(source); var html =
   * template(content); //$board.html(html); // console.log(html); }
   *
   * qA();
   */
});
$(document).ready(function() {
  loadTimelineProgress(1, 1);
  $(".f1_container").click(function() {
    //alert("ok");
    $(this).toggleClass("active");
  });
  ole.footerNotificationHandler.pageEndSetNotification();
});

//$('.title').text(data.string.exerciseTitle);
var id = 0;
$(document).ready(function() {
	$("#activity-page-next-btn-enabled").show(0);
	var ex = [ {
		text : data.string.ex1
	}, {
		text : data.string.ex2
	}, {
		text : data.string.ex3
	}, {
		text : data.string.ex4
	}, {
		text : data.string.ex5
	}

	];
	// console.log(ex);
	totalq = ex.length;
	displayContents();
	function displayContents() {
		var context = $.each(ex, function(key, values) {
			id++;
			text1 = values.text;
			//console.log(text1);
			appendContents = '<div class="question' + id + '">';
			appendContents += text1;
			appendContents += '</div>';
			//console.log(appendContents);
			 console.log(appendContents);
		 	$(".contents_choose").append(appendContents);
		});
	}
	$("#activity-page-next-btn-enabled").hide(0);
});

var answered = false;
var attemptcount = 0;

var totalq;
var correctlyanswered = 0;

function correct() {
	if(answered){
		return answered;
	}
	attemptcount++;
	if(attemptcount == 1){
		correctlyanswered++;
	}
	answered = true;
	play_correct_incorrect_sound(true);
	if(buttonID==5){
		$(".question"+buttonID).hide(0);
		$(".title").html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
		$(".exercise_button").show(0);
		$("#activity-page-next-btn-enabled").hide(0);
		ole.footerNotificationHandler.pageEndSetNotification();
	} else {
		$("#activity-page-next-btn-enabled").show(0);
	}
	$(".yes").css("color","#33FF33");
}
function incorrect() {
	if(answered){
		return answered;
	}
	attemptcount++;
	play_correct_incorrect_sound(false);
	$(".no").css("color","#D00000 ");
}

//$(document).ready(function(){
(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  $(".title").text(data.string.exerciseTitle);

  var content = [
    {
      question: data.string.j1,
      answers: [
        { ans: data.string.ans1 },
        { ans: data.string.ans2 },
        { ans: data.string.ans3, correct: "correct" },
        { ans: data.string.ans4 }
      ],

      img: $ref + "/exercise_images/clothes.jpg"
    },
    {
      question: data.string.j2,
      answers: [
        { ans: data.string.ans5 },
        { ans: data.string.ans6 },
        { ans: data.string.ans7, correct: "correct" },
        { ans: data.string.ans8 }
      ],
      img: $ref + "/exercise_images/food.png"
    },
    {
      question: data.string.j3,
      answers: [
        { ans: data.string.ans9 },
        { ans: data.string.ans10, correct: "correct" },
        { ans: data.string.ans11 },
        { ans: data.string.ans12 }
      ],
      img: $ref + "/exercise_images/astronaut.png"
    },
    {
      question: data.string.j4,
      answers: [
        { ans: data.string.ans13 },
        { ans: data.string.ans14, correct: "correct" },
        { ans: data.string.ans15 },
        { ans: data.string.ans16 }
      ],
      img: $ref + "/exercise_images/everest.jpg"
    },
    {
      question: data.string.j5,
      answers: [
        { ans: data.string.ans17 },
        { ans: data.string.ans18, correct: "correct" },
        { ans: data.string.ans19 },
        { ans: data.string.ans20 }
      ],
      img: $ref + "/exercise_images/fair.jpg"
    },
    {
      question: data.string.j6,
      answers: [
        { ans: data.string.ans21 },
        { ans: data.string.ans22 },
        { ans: data.string.ans23, correct: "correct" },
        { ans: data.string.ans24 }
      ],
      img: $ref + "/exercise_images/granny.jpg"
    },

    {
      question: data.string.j7,
      answers: [
        { ans: data.string.ans25, correct: "correct" },
        { ans: data.string.ans26 },
        { ans: data.string.ans27 },
        { ans: data.string.ans28 }
      ],
      img: $ref + "/exercise_images/principal.jpg"
    },
    {
      question: data.string.j8,
      answers: [
        { ans: data.string.ans29 },
        { ans: data.string.ans30 },
        { ans: data.string.ans31 },
        { ans: data.string.ans32, correct: "correct" }
      ],
      img: $ref + "/exercise_images/wound.png"
    },
    {
      question: data.string.j9,
      answers: [
        { ans: data.string.ans33 },
        { ans: data.string.ans34 },
        { ans: data.string.ans35, correct: "correct" },
        { ans: data.string.ans36 }
      ],
      img: $ref + "/exercise_images/toy.jpg"
    },
    {
      question: data.string.j10,
      answers: [
        { ans: data.string.ans37 },
        { ans: data.string.ans38 },
        { ans: data.string.ans39, correct: "correct" },
        { ans: data.string.ans40 }
      ],
      img: $ref + "/exercise_images/walk.png"
    },
    {
      question: data.string.j11,
      answers: [
        { ans: data.string.ans41, correct: "correct" },
        { ans: data.string.ans42 },
        { ans: data.string.ans43 },
        { ans: data.string.ans44 }
      ],
      img: $ref + "/exercise_images/come.jpg"
    },
    {
      question: data.string.j12,
      answers: [
        { ans: data.string.ans45 },
        { ans: data.string.ans46 },
        { ans: data.string.ans47 },
        { ans: data.string.ans48, correct: "correct" }
      ],
      img: $ref + "/exercise_images/pencil.gif"
    },
    {
      question: data.string.j13,
      answers: [
        { ans: data.string.ans49 },
        { ans: data.string.ans50, correct: "correct" },
        { ans: data.string.ans51 },
        { ans: data.string.ans52 }
      ],
      img: $ref + "/exercise_images/leadp.png"
    },
    {
      question: data.string.j14,
      answers: [
        { ans: data.string.ans53 },
        { ans: data.string.ans54 },
        { ans: data.string.ans55, correct: "correct" },
        { ans: data.string.ans56 }
      ],
      img: $ref + "/exercise_images/game.jpg"
    },
    {
      question: data.string.j15,
      answers: [
        { ans: data.string.ans57, correct: "correct" },
        { ans: data.string.ans58 },
        { ans: data.string.ans59 },
        { ans: data.string.ans60 }
      ],
      img: $ref + "/exercise_images/game2.jpg"
    },
    {
      question: data.string.j16,
      answers: [
        { ans: data.string.ans61 },
        { ans: data.string.ans62 },
        { ans: data.string.ans63 },
        { ans: data.string.ans64, correct: "correct" }
      ],
      img: $ref + "/exercise_images/homework.gif"
    },
    {
      question: data.string.j17,
      answers: [
        { ans: data.string.ans65 },
        { ans: data.string.ans66, correct: "correct" },
        { ans: data.string.ans67 },
        { ans: data.string.ans68 }
      ],
      img: $ref + "/exercise_images/girlplay.jpeg"
    },
    {
      question: data.string.j18,
      answers: [
        { ans: data.string.ans69 },
        { ans: data.string.ans70 },
        { ans: data.string.ans71 },
        { ans: data.string.ans72, correct: "correct" }
      ],
      img: $ref + "/exercise_images/plane.jpeg"
    },
    {
      question: data.string.j19,
      answers: [
        { ans: data.string.ans73 },
        { ans: data.string.ans74 },
        { ans: data.string.ans75 },
        { ans: data.string.ans76, correct: "correct" }
      ],
      img: $ref + "/exercise_images/man.png"
    },
    {
      question: data.string.j20,
      answers: [
        { ans: data.string.ans77, correct: "correct" },
        { ans: data.string.ans78 },
        { ans: data.string.ans79 },
        { ans: data.string.ans80 }
      ],
      img: $ref + "/exercise_images/guitar.jpg"
    },
    {
      question: data.string.j21,
      answers: [
        { ans: data.string.ans81 },
        { ans: data.string.ans82, correct: "correct" },

        { ans: data.string.ans83 },
        { ans: data.string.ans84 }
      ],
      img: $ref + "/exercise_images/drum.png"
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $total_page = 21;
  loadTimelineProgress($total_page + 1, questionCount + 1);

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();
  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  var correctlyanswered = 0;
  $board.on("click", ".neutral", function() {
    // console.log("what");
    if (answered) {
      return answered;
    }
    attemptcount++;
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      $this.addClass("right").removeClass("neutral");
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      $nextBtn.fadeIn();
      play_correct_incorrect_sound(true);
    } else {
      play_correct_incorrect_sound(false);
      $this.addClass("wrong").removeClass("neutral");
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress($total_page + 1, questionCount + 1);
    if (questionCount < 21) {
      qA();
    } else if (questionCount == 21) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title").hide(0);
      // .html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
      // "position": "absolute",
      // "top": "48%",
      // "transform": "translateY(-50%)"
      // });
      $(".finishtxt")
        .append(
          "<br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .show(0);
    }
  });
  $(".closebtn").click(function() {
    ole.activityComplete.finishingcall();
  });
})(jQuery);

//$(document).ready(function(){
(function () {
	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var questionCount =0;
	var $total_page = 12;
	loadTimelineProgress($total_page,questionCount+1);
	$('.title').text(data.string.exerciseTitle);
	var content = [
	{
		question : data.string.j1,
		answers: [
		{ans : data.string.ans1,
			correct : "correct"},
		{ans : data.string.ans2},
		{ans : data.string.ans3},
		{ans : data.string.ans4},
		],
		
	},
	{
		question : data.string.j2,
		answers: [
		  		{ans : data.string.ans5},
		  		{ans : data.string.ans6},
		  		{ans : data.string.ans7},
		  		{ans : data.string.ans8,
		  			correct : "correct"},
		  		],
		
	},
	{
		question : data.string.j3,
		answers: [
		  		{ans : data.string.ans9,
		  			correct : "correct"},
		  		{ans : data.string.ans10},
		  		{ans : data.string.ans11},
		  		{ans : data.string.ans12},
		  		],
	},
	{
		question : data.string.j4,
		answers: [
		  		{ans : data.string.ans13},
		  		{ans : data.string.ans14},
		  		{ans : data.string.ans15,
		  			correct : "correct"},
		  		{ans : data.string.ans16},
		  		],
				
	},
	{
		question : data.string.j5,
		answers: [
		  		{ans : data.string.ans17},
		  		{ans : data.string.ans18},
		  		{ans : data.string.ans19,
		  			correct : "correct"},
		  		{ans : data.string.ans20},
		  		],
	},
	{
		question : data.string.j6,
		answers: [
		  		{ans : data.string.ans21},
		  		{ans : data.string.ans22,
		  			correct : "correct"},
		  		{ans : data.string.ans23},
		  		{ans : data.string.ans24},
		  		],
		
	},
	
	{
		question : data.string.j7,
		answers: [
		  		{ans : data.string.ans25},
		  		{ans : data.string.ans26,
		  			correct : "correct"},
		  		{ans : data.string.ans27},
		  		{ans : data.string.ans28}
		  		],
	},
	{
		question : data.string.j8,
		answers: [
		  		{ans : data.string.ans29,
		  			correct : "correct"},
		  		{ans : data.string.ans30},
		  		{ans : data.string.ans31},
		  		{ans : data.string.ans32},
		  		],
		
	},
	{
		question : data.string.j9,
		answers: [
		  		{ans : data.string.ans33},
		  		{ans : data.string.ans34},
		  		{ans : data.string.ans35,
		  			correct : "correct"},
		  		{ans : data.string.ans36},
		  		],
		
	},
	{
		question : data.string.j10,
		answers: [
		  		{ans : data.string.ans37},
		  		{ans : data.string.ans38,
		  			correct : "correct"},
		  		{ans : data.string.ans39},
		  		{ans : data.string.ans40},
		  		],
	},
	{
	question : data.string.j11,
	answers: [
	  		{ans : data.string.ans41},
	  		{ans : data.string.ans42,
	  			correct : "correct"},
	  		{ans : data.string.ans43},
	  		{ans : data.string.ans44},
	  		],
	
	}
];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		// console.log("what");
		if(answered){
			return answered;
		}
		attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
		}
		else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		loadTimelineProgress($total_page,questionCount+1);
	if(questionCount<11){
			qA();
		}
		else if (questionCount==11){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

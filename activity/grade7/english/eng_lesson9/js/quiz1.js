//$(document).ready(function(){
(function () {
	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.quiztitle);
	var content = [
	{
		question : data.string.quiz1_01,
		answers: [
		{ans : data.string.quiz1_ans1},
		{ans : data.string.quiz1_ans2},
		{ans : data.string.quiz1_ans3,
			correct : "correct"},
		
		],
		
	},
	{
		question : data.string.quiz1_02,
		answers: [
		{ans : data.string.quiz1_ans4},
		{ans : data.string.quiz1_ans5},
		{ans : data.string.quiz1_ans6,
			correct : "correct"},
		
		],
		
	}

	
];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			attemptcount = 0;
			answered = false;
			// console.log(html);
	}
	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		// console.log("what");
		if(answered){
			return answered;
		}
		attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$(".correcttxt").show(0);
			$(".incorrecttxt").hide(0);
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
			
			$nextBtn.fadeIn();
		}
		
		else {
			$this.addClass('wrong').removeClass('neutral');
			play_correct_incorrect_sound(false);
			
			$(".incorrecttxt").show(0);
			$(".correcttxt").hide(0);
			
		}
	});
	$nextBtn.on('click',function () {
		$(".incorrecttxt").hide(0);
		$(".correcttxt").hide(0);
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<2){
			qA();
		}
		else if (questionCount==2){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

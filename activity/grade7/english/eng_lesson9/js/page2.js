imageAsset = $ref + "/slide_images/";
soundAsset = $ref + "/sounds/slide2/";

var dialog1part1 = new buzz.sound(soundAsset + "1.ogg");
var dialog1part2 = new buzz.sound(soundAsset + "2.ogg");
var dialog1 = [dialog1part1, dialog1part2];

var dialog2part1 = new buzz.sound(soundAsset + "3.ogg");
var dialog2part2 = new buzz.sound(soundAsset + "4.ogg");
var dialog2 = [dialog2part1, dialog2part2];

var dialog3 = new buzz.sound(soundAsset + "5.ogg");
var dialog4 = new buzz.sound(soundAsset + "6.ogg");

var dialog5part1 = new buzz.sound(soundAsset + "7.ogg");
var dialog5part2 = new buzz.sound(soundAsset + "8.ogg");
var dialog5 = [dialog5part1, dialog5part2];

var dialog6 = new buzz.sound(soundAsset + "9.ogg");

var dialog7part1 = new buzz.sound(soundAsset + "10.ogg");
var dialog7part2 = new buzz.sound(soundAsset + "11.ogg");
var dialog7 = [dialog7part1, dialog7part2];

var dialog8part1 = new buzz.sound(soundAsset + "12.ogg");
var dialog8part2 = new buzz.sound(soundAsset + "13.ogg");
var dialog8 = [dialog8part1, dialog8part2];

var dialog9part1 = new buzz.sound(soundAsset + "14.ogg");
var dialog9part2 = new buzz.sound(soundAsset + "15.ogg");
var dialog9 = [dialog9part1, dialog9part2];

var dialog10 = new buzz.sound(soundAsset + "16.ogg");
var dialog11 = new buzz.sound(soundAsset + "17.ogg");

var dialog12part1 = new buzz.sound(soundAsset + "18.ogg");
var dialog12part2 = new buzz.sound(soundAsset + "19.ogg");
var dialog12 = [dialog12part1, dialog12part2];

var dialog13 = new buzz.sound(soundAsset + "20.ogg");

var dialog14part1 = new buzz.sound(soundAsset + "21.ogg");
var dialog14part2 = new buzz.sound(soundAsset + "22.ogg");
var dialog14 = [dialog14part1, dialog14part2];

var dialog15 = new buzz.sound(soundAsset + "23.ogg");
var dialog16 = new buzz.sound(soundAsset + "24.ogg");
var dialog17 = new buzz.sound(soundAsset + "25.ogg");

var dialog18part1 = new buzz.sound(soundAsset + "26.ogg");
var dialog18part2 = new buzz.sound(soundAsset + "27.ogg");
var dialog18part3 = new buzz.sound(soundAsset + "28.ogg");
var dialog18part4 = new buzz.sound(soundAsset + "29.ogg");
var dialog18 = [dialog18part1, dialog18part2, dialog18part3, dialog18part4];

var dialog19part1 = new buzz.sound(soundAsset + "30.ogg");
var dialog19part2 = new buzz.sound(soundAsset + "31.ogg");
var dialog19part3 = new buzz.sound(soundAsset + "32.ogg");
var dialog19 = [dialog19part1, dialog19part2, dialog19part3];

var dialog20 = new buzz.sound(soundAsset + "33.ogg");

var dialog21part1 = new buzz.sound(soundAsset + "34.ogg");
var dialog21part2 = new buzz.sound(soundAsset + "35.ogg");
var dialog21part3 = new buzz.sound(soundAsset + "36.ogg");
var dialog21 = [dialog21part1, dialog21part2, dialog21part3];

var dialog22part1 = new buzz.sound(soundAsset + "37.ogg");
var dialog22part2 = new buzz.sound(soundAsset + "38.ogg");
var dialog22part3 = new buzz.sound(soundAsset + "39.ogg");
var dialog22 = [dialog22part1, dialog22part2, dialog22part3];

var dialog23part1 = new buzz.sound(soundAsset + "40.ogg");
var dialog23part2 = new buzz.sound(soundAsset + "41.ogg");
var dialog23 = [dialog23part1, dialog23part2];

var dialog24part1 = new buzz.sound(soundAsset + "42.ogg");
var dialog24part2 = new buzz.sound(soundAsset + "43.ogg");
var dialog24part3 = new buzz.sound(soundAsset + "44.ogg");
var dialog24part4 = new buzz.sound(soundAsset + "45.ogg");
var dialog24 = [dialog24part1, dialog24part2, dialog24part3, dialog24part4];

var dialog25part1 = new buzz.sound(soundAsset + "46.ogg");
var dialog25part2 = new buzz.sound(soundAsset + "47.ogg");
var dialog25part3 = new buzz.sound(soundAsset + "48.ogg");
var dialog25 = [dialog25part1, dialog25part2, dialog25part3];

var dialog26part1 = new buzz.sound(soundAsset + "49.ogg");
var dialog26part2 = new buzz.sound(soundAsset + "50.ogg");
var dialog26 = [dialog26part1, dialog26part2];

var dialog27part1 = new buzz.sound(soundAsset + "51.ogg");
var dialog27part2 = new buzz.sound(soundAsset + "52.ogg");
var dialog27 = [dialog27part1, dialog27part2];

var dialog28 = new buzz.sound(soundAsset + "53.ogg");
var dialog29 = new buzz.sound(soundAsset + "54.ogg");

var dialog30part1 = new buzz.sound(soundAsset + "55.ogg");
var dialog30part2 = new buzz.sound(soundAsset + "56.ogg");
var dialog30 = [dialog30part1, dialog30part2];

var dialog31 = new buzz.sound(soundAsset + "57.ogg");

var soundcontent = [
  dialog1,
  dialog2,
  dialog3,
  dialog4,
  dialog5,
  dialog6,
  dialog7,
  dialog8,
  dialog9,
  dialog10,
  dialog11,
  dialog12,
  dialog13,
  dialog14,
  dialog15,
  dialog16,
  dialog17,
  dialog18,
  dialog19,
  dialog20,
  dialog21,
  dialog22,
  dialog23,
  dialog24,
  dialog25,
  dialog26,
  dialog27,
  dialog28,
  dialog29,
  dialog30,
  dialog31
];

var content = [
  {
    bgImgSrc: imageAsset + "9-a.jpg",
    forwhichdialog: "dialog1",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_1part1, data.string.digs2_1part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_2.jpg",
    forwhichdialog: "dialog2",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_2part1, data.string.digs2_2part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_3.jpg",
    forwhichdialog: "dialog3",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_3],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_2.jpg",
    forwhichdialog: "dialog4",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_4],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_4.jpg",
    forwhichdialog: "dialog5",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_5part1, data.string.digs2_5part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_5.jpg",
    forwhichdialog: "dialog6",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_6],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_6.jpg",
    forwhichdialog: "dialog7",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_7part1, data.string.digs2_7part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_7.jpg",
    forwhichdialog: "dialog8",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_8part1, data.string.digs2_8part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_8.jpg",
    forwhichdialog: "dialog9",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_9part1, data.string.digs2_9part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_9.jpg",
    forwhichdialog: "dialog10",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_10],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_10.jpg",
    forwhichdialog: "dialog11",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_11],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_11.jpg",
    forwhichdialog: "dialog12",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_12part1, data.string.digs2_12part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_12.jpg",
    forwhichdialog: "dialog13",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_13],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_13.jpg",
    forwhichdialog: "dialog14",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_14part1, data.string.digs2_14part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_14.jpg",
    forwhichdialog: "dialog15",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_15],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_15.jpg",
    forwhichdialog: "dialog16",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_16],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_16.jpg",
    forwhichdialog: "dialog17",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_17],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_17.jpg",
    forwhichdialog: "dialog18",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [
      data.string.digs2_18part1,
      data.string.digs2_18part2,
      data.string.digs2_18part3,
      data.string.digs2_18part4
    ],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_18.jpg",
    forwhichdialog: "dialog19",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [
      data.string.digs2_19part1,
      data.string.digs2_19part2,
      data.string.digs2_19part3
    ],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_19.jpg",
    forwhichdialog: "dialog20",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_20],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_20.jpg",
    forwhichdialog: "dialog21",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [
      data.string.digs2_21part1,
      data.string.digs2_21part2,
      data.string.digs2_21part3
    ],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_21.jpg",
    forwhichdialog: "dialog22",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [
      data.string.digs2_22part1,
      data.string.digs2_22part2,
      data.string.digs2_22part3
    ],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_22.jpg",
    forwhichdialog: "dialog23",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_23part1, data.string.digs2_23part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_23.jpg",
    forwhichdialog: "dialog24",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [
      data.string.digs2_24part1,
      data.string.digs2_24part2,
      data.string.digs2_24part3,
      data.string.digs2_24part4
    ],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_24.jpg",
    forwhichdialog: "dialog25",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [
      data.string.digs2_25part1,
      data.string.digs2_25part2,
      data.string.digs2_25part3
    ],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_25.jpg",
    forwhichdialog: "dialog26",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_26part1, data.string.digs2_26part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_26.jpg",
    forwhichdialog: "dialog27",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_27part1, data.string.digs2_27part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_27.jpg",
    forwhichdialog: "dialog28",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_28],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_28.jpg",
    forwhichdialog: "dialog29",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_29],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "3_29.jpg",
    forwhichdialog: "dialog30",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_30part1, data.string.digs2_30part2],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  },

  {
    bgImgSrc: imageAsset + "9-a.jpg",
    forwhichdialog: "dialog31",
    talkHeadImages: [
      {
        talkHeadImgSrc: imageAsset + "5.png",
        nameofwhosehead: "bhuwan",
        highlightFlag: "highlight"
      },
      {
        talkHeadImgSrc: imageAsset + "3.png",
        nameofwhosehead: "nidhi",
        highlightFlag: "" //hightlight characters according to them speaking
      },
      {
        talkHeadImgSrc: imageAsset + "4.png",
        nameofwhosehead: "nidhi1",
        highlightFlag: ""
      }
    ],
    lineCountDialog: [data.string.digs2_31],
    speakerImgSrc: $ref + "/slide_images/speaker.png",
    listenAgainText: data.string.listenAgainData
  }
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = 31;
  loadTimelineProgress($total_page, countNext + 1);

  function slide1() {
    var source = $("#slide1-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    var $slide = $board.children("div");
    var $dialogcontainer = $slide.children("div.dialogcontainer");
    var $listenAgainButton = $dialogcontainer
      .children("p")
      .children("img.listenAgainButton");
    var $paralines = $dialogcontainer.children("p").children("span");
    var $paralinestohideonListenAgain = $dialogcontainer
      .children("p")
      .children("span:nth-of-type(n+2)");

    if ($.isArray(soundcontent[countNext])) {
      playThisDialog = soundcontent[countNext];
      playThisDialog[0].play();
    } else if (!$.isArray(soundcontent[countNext])) {
      playThisDialog = [soundcontent[countNext]];
      playThisDialog[0].play();
    }

    $listenAgainButton.on("click", function() {
      /* Act on the event */
      $paralinestohideonListenAgain.css("display", "none");
      playThisDialog[0].play();
      $nextBtn.hide(0);
      $prevBtn.hide(0);
      $listenAgainButton
        .removeClass("enableListenAgain")
        .addClass("disableListenAgain");
    });

    /*this function binds appropriate events handlers to the
		audio as required*/
    function playerbinder() {
      $.each(playThisDialog, function(index, entry) {
        if (index < playThisDialog.length - 1) {
          entry.bind("ended", function() {
            // alert(index +"sound ended");
            $paralines.eq(index + 1).fadeIn(400);
            playThisDialog[index + 1].play();
          });
        }

        if (index == playThisDialog.length - 1) {
          entry.bind("ended", function() {
            $listenAgainButton
              .removeClass("disableListenAgain")
              .addClass("enableListenAgain");

            if (countNext > 0 && countNext < $total_page - 1) {
              $nextBtn.show(0);
              $prevBtn.show(0);
            } else if (countNext < 1) {
              $nextBtn.show(0);
            } else if (countNext >= $total_page - 1) {
              $prevBtn.show(0);
              ole.footerNotificationHandler.pageEndSetNotification();
            }
          });
        }
      });
    }

    playerbinder();
  }

  slide1((countNext += 0));

  $nextBtn.on("click", function() {
    $(this).css("display", "none");
    $prevBtn.css("display", "none");
    slide1(++countNext);
    loadTimelineProgress($total_page, countNext + 1);
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    $(this).css("display", "none");
    $nextBtn.css("display", "none");
    slide1(--countNext);
    ole.footerNotificationHandler.hideNotification();
    loadTimelineProgress($total_page, countNext + 1);
  });
})(jQuery);

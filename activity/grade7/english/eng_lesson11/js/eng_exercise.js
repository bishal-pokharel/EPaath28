//$(document).ready(function(){
(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  var content = [
    {
      question: data.string.q1,
      answers: [
        { ans: data.string.name1, correct: "correct" },
        { ans: data.string.name2 },
        { ans: data.string.name3 }
      ],
      img: $ref + "/exercise_image/q08.jpg"
    },
    {
      question: data.string.q2,
      answers: [
        { ans: data.string.name4 },
        { ans: data.string.name5, correct: "correct" },
        { ans: data.string.name6 }
      ],
      img: $ref + "/exercise_image/q08.jpg"
    },
    {
      question: data.string.q3,
      answers: [
        { ans: data.string.name7, correct: "correct" },
        { ans: data.string.name8 },
        { ans: data.string.name9 }
      ],
      img: $ref + "/exercise_image/q07.jpg"
    },
    {
      question: data.string.q4,
      answers: [
        { ans: data.string.name10 },
        { ans: data.string.name11 },
        { ans: data.string.name12, correct: "correct" }
      ],
      img: $ref + "/exercise_image/q07.jpg"
    },
    {
      question: data.string.q5,
      answers: [
        { ans: data.string.name13 },
        { ans: data.string.name14, correct: "correct" },
        { ans: data.string.name15 }
      ],
      img: $ref + "/exercise_image/globalwarming.jpg"
    },
    {
      question: data.string.q6,
      answers: [
        { ans: data.string.name16, correct: "correct" },
        { ans: data.string.name17 },
        { ans: data.string.name18 }
      ],
      img: $ref + "/exercise_image/kolkata.jpg"
    },
    {
      question: data.string.q7,
      answers: [
        { ans: data.string.name19 },
        { ans: data.string.name20 },
        { ans: data.string.name21, correct: "correct" }
      ],
      img: $ref + "/exercise_image/involve.jpg"
    },
    {
      question: data.string.q8,
      answers: [
        { ans: data.string.name22, correct: "correct" },
        { ans: data.string.name23 },
        { ans: data.string.name24 }
      ],
      img: $ref + "/exercise_image/q04.jpg"
    },
    {
      question: data.string.q9,
      answers: [
        { ans: data.string.name25 },
        { ans: data.string.name26, correct: "correct" },
        { ans: data.string.name27 }
      ],
      img: $ref + "/exercise_image/q03.jpg"
    },
    {
      question: data.string.q10,
      answers: [
        { ans: data.string.name28 },
        { ans: data.string.name29 },
        { ans: data.string.name30, correct: "correct" }
      ],
      img: $ref + "/exercise_image/q04.jpg"
    },
    {
      question: data.string.q11,
      answers: [
        { ans: data.string.name31 },
        { ans: data.string.name32, correct: "correct" },
        { ans: data.string.name33 }
      ],
      img: $ref + "/exercise_image/q01.jpg"
    },
    {
      question: data.string.q12,
      answers: [
        { ans: data.string.name34, correct: "correct" },
        { ans: data.string.name35 },
        { ans: data.string.name36 }
      ],
      img: $ref + "/exercise_image/q02.jpg"
    },
    {
      question: data.string.q13,
      answers: [
        { ans: data.string.name37 },
        { ans: data.string.name38, correct: "correct" },
        { ans: data.string.name39 }
      ],
      img: $ref + "/exercise_image/q06.jpg"
    },
    {
      question: data.string.q14,
      answers: [
        { ans: data.string.name40 },
        { ans: data.string.name41, correct: "correct" },
        { ans: data.string.name42 }
      ],
      img: $ref + "/exercise_image/dashain.jpg"
    },
    {
      question: data.string.q15,
      answers: [
        { ans: data.string.name43 },
        { ans: data.string.name44 },
        { ans: data.string.name45, correct: "correct" }
      ],
      img: $ref + "/exercise_image/chhath.jpg"
    },
    {
      question: data.string.q16,
      answers: [
        { ans: data.string.name46, correct: "correct" },
        { ans: data.string.name47 },
        { ans: data.string.name48 }
      ],
      img: $ref + "/exercise_image/chhath.jpg"
    },
    {
      question: data.string.q17,
      answers: [
        { ans: data.string.name49 },
        { ans: data.string.name50, correct: "correct" },
        { ans: data.string.name51 }
      ],
      img: $ref + "/exercise_image/chhath.jpg"
    },
    {
      question: data.string.q18,
      answers: [
        { ans: data.string.name52, correct: "correct" },
        { ans: data.string.name53 },
        { ans: data.string.name54 }
      ],
      img: $ref + "/exercise_image/chhath.jpg"
    },
    {
      question: data.string.q19,
      answers: [
        { ans: data.string.name55 },
        { ans: data.string.name56, correct: "correct" },
        { ans: data.string.name57 }
      ],
      img: $ref + "/exercise_image/chhath.jpg"
    },
    {
      question: data.string.q20,
      answers: [
        { ans: data.string.name58 },
        { ans: data.string.name59 },
        { ans: data.string.name60, correct: "correct" }
      ],
      img: $ref + "/exercise_image/singing.png"
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }

  qA();
  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);

  var correctlyanswered = 0;
  $board.on("click", ".neutral", function() {
    // console.log("what");
    if (answered) {
      return answered;
    }
    attemptcount++;
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      play_correct_incorrect_sound(true);
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();
    } else {
      play_correct_incorrect_sound(false);
      $this.addClass("wrong").removeClass("neutral");
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 20) {
      qA();
    } else if (questionCount == 20) {
      // $(".result").show(0);
      $(".question").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".title").hide(0);
      // html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
      // "position": "absolute",
      // "top": "48%",
      // "transform": "translateY(-50%)"
      // });
      $(".finishtxt")
        .append(
          "<br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .show(0);

      $(".closebtn").on("click", function() {
        ole.activityComplete.finishingcall();
      });
    }
  });
})(jQuery);

$(document).ready(function() {
  loadTimelineProgress(1, 1);
  var lesson1_Dialouge = new buzz.sound($ref + "/audio/Lesson13.ogg");
  $("#showdil").click(function() {
    $("#showdil").hide(0);
    $("#start").trigger("click");
  });
  $("#start").click(function() {
    $("#start").hide(0);
    $("#showdil").hide(0);
    $("#diag1").fadeIn(1500);
    $("#diag1")
      .delay(500)
      .fadeOut(500);

    $("#diag2")
      .delay(2500)
      .fadeIn(500);
    $("#diag2")
      .delay(500)
      .fadeOut(500);

    $("#diag3")
      .delay(3500)
      .fadeIn(500);
    $("#diag3")
      .delay(1000)
      .fadeOut(500);

    $("#diag4")
      .delay(6000)
      .fadeIn(1000);
    $("#diag4")
      .delay(500)
      .fadeOut(500);

    $("#diag5")
      .delay(8000)
      .fadeIn(1000);
    $("#diag5")
      .delay(500)
      .fadeOut(500);

    $("#diag6")
      .delay(10000)
      .fadeIn(1000);
    $("#diag6")
      .delay(2500)
      .fadeOut(500);

    $("#diag7")
      .delay(14000)
      .fadeIn(1000);
    $("#diag7")
      .delay(500)
      .fadeOut(500);

    $("#diag8")
      .delay(16000)
      .fadeIn(1000);
    $("#diag8")
      .delay(1000)
      .fadeOut(1000);

    $("#diag9")
      .delay(19000)
      .fadeIn(1000);
    $("#diag9")
      .delay(6000)
      .fadeOut(1000);

    $("#diag10")
      .delay(27000)
      .fadeIn(1000);
    $("#diag10")
      .delay(6000)
      .fadeOut(1000);

    $("#diag11")
      .delay(35000)
      .fadeIn(1000);
    $("#diag11")
      .delay(1000)
      .fadeOut(1000);

    $("#diag12")
      .delay(38000)
      .fadeIn(1000);
    $("#diag12")
      .delay(2000)
      .fadeOut(1000);

    $("#diag13")
      .delay(42000)
      .fadeIn(1000);
    $("#diag13")
      .delay(1000)
      .fadeOut(200);

    $("#diag14")
      .delay(44500)
      .fadeIn(1000);
    $("#diag14")
      .delay(500)
      .fadeOut(500);

    $("#diag15")
      .delay(46500)
      .fadeIn(500);
    $("#diag15")
      .delay(1000)
      .fadeOut(500);

    $("#diag16")
      .delay(48500)
      .fadeIn(500);
    $("#diag16")
      .delay(2000)
      .fadeOut(500);

    $("#diag17")
      .delay(51500)
      .fadeIn(500);
    $("#diag17")
      .delay(500)
      .fadeOut(500);

    $("#diag18")
      .delay(53000)
      .fadeIn(500);
    $("#diag18")
      .delay(1000)
      .fadeOut(500);

    $("#showdil")
      .delay(55500)
      .fadeIn(1000);
    $("#start")
      .delay(54500)
      .fadeIn(1000);
    setTimeout(function() {
      ole.footerNotificationHandler.pageEndSetNotification();
    }, 55500);
  });

  $("#start").click(function() {
    lesson1_Dialouge.play();
  });
});

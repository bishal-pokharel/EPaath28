$(".title").text(data.string.title);
$(document).ready(function() {
  $(".title").text(data.string.exerciseTitle2);
  var content = [
    {
      words: data.string.word1,
      description: data.string.des1,
      img: $ref + "/images/rsvp.jpg"
    }, //
    {
      words: data.string.word2,
      description: data.string.des2,
      img: $ref + "/images/reg.jpg"
    }, //

    {
      words: data.string.word3,
      description: data.string.des3,
      img: $ref + "/images/formals.jpg"
    }, //

    {
      words: data.string.word4,
      description: data.string.des4,
      img: $ref + "/images/informals.jpg"
    }, //
    {
      words: data.string.word5,
      description: data.string.des5,
      img: $ref + "/images/casual.jpg"
    } //
  ];
  console.log(content);
  displaycontent();
  function displaycontent() {
    var words = $.each(content, function(key, value) {
      words = value.words;
      var refwords = value.words.toString().replace(" ","");
      description = value.description;
      image = value.img;
      // console.log(description);
      appendTab = "<li>";
      appendTab +=
        '<a href="#' + refwords + '" data-toggle="tab"  >' + words + "</a>";
      appendTab += "</li>";
      $("#myTab").append(appendTab);

      appendcontent = '<div id="' + refwords + '" class="tab-pane ">';
      appendcontent += '<div class="f1_container">';
      appendcontent += '<div class="shadow f1_card">';
      appendcontent += '<div class="front face">';
      appendcontent += "<p>" + words + "</p>";
      appendcontent += "</div>";
      appendcontent += '<div class="back face center">';
      appendcontent +=
        '<img src="' + image + '" class="tab_image img-responsive"/>';
      appendcontent += '<p class="textroll">';
      appendcontent += description;
      appendcontent += "</p>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      $("#myTabContent").append(appendcontent);
    });
  }
  /*
   * function qA() { var source = $('#qA-templete').html(); var
   * template = Handlebars.compile(source); var html =
   * template(content); //$board.html(html); // console.log(html); }
   *
   * qA();
   */
});
$(document).ready(function() {
  loadTimelineProgress(1, 1);
  $(".f1_container").click(function() {
    //alert("ok");
    $(this).toggleClass("active");
  });
  ole.footerNotificationHandler.pageEndSetNotification();
});

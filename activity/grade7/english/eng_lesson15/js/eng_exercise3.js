//$(document).ready(function(){
var imgpath = $ref+"/images/";
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.exerciseTitle);
	var content = [
	{
		question : data.string.j6,
		answers: [
		{ans : data.string.ans17},
		{ans : data.string.ans18},
		{ans : data.string.ans19,
		correct : "correct"}
		],
		img: imgpath + "longhair.png"
	},
	{
		question : data.string.j7,
		answers: [
		{ans : data.string.ans20,
		correct : "correct"},
		{ans : data.string.ans21},
		{ans : data.string.ans22}
		],
		img: imgpath + "longhair.png"
	},
	{
		question : data.string.j8,
		answers: [
		{ans : data.string.ans23,
			correct : "correct"},
		{ans : data.string.ans24},
		{ans : data.string.ans25},
		{ans : data.string.ans26}
		],
		img: imgpath + "longhair.png"
	},
	{
		question : data.string.j9,
		answers: [
		{ans : data.string.ans31},
		{ans : data.string.ans32},
		{ans : data.string.ans33,
			correct : "correct"}
		],
		img: imgpath + "helicopter.gif"
	},
	{
		question : data.string.j10,
		answers: [
		{ans : data.string.ans34,
		correct : "correct"},
		{ans : data.string.ans35},
		{ans : data.string.ans36}
		],
		img: imgpath + "longhair.png"
	},
	{
		question : data.string.j11,
		answers: [
		{ans : data.string.ans37,
		correct : "correct"},
		{ans : data.string.ans38},
		{ans : data.string.ans39}
		],
		img: imgpath + "teasing.jpg"
	},
	{
		question : data.string.j12,
		answers: [
		{ans : data.string.ans40},
		{ans : data.string.ans41,
			correct : "correct"},
		{ans : data.string.ans42}
		],
		img: imgpath + "longhair.png"
	},
	{
		question : data.string.j13,
		answers: [
		{ans : data.string.ans43},
		{ans : data.string.ans44,
			correct : "correct"},
		{ans : data.string.ans45}
		],
		img: imgpath + "longhair.png"
	},
	{
		question : data.string.j14,
		answers: [
		{ans : data.string.ans46},
		{ans : data.string.ans47,
			correct : "correct"},
		{ans : data.string.ans48}
		],
		img: imgpath + "longhair.png"
	},
	{
		question : data.string.j15,
		answers: [
		{ans : data.string.ans49},
		{ans : data.string.ans50,
			correct : "correct"},
		{ans : data.string.ans51}
		],
		img: imgpath + "longhair.png"
	}
	];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}

	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		// console.log("what");
		if(answered){
			return answered;
		}
		attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
			play_correct_incorrect_sound(false);
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<10){
			qA();
		}
		else if (questionCount==10){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
var buttonID = 1;
var id_num = 1;
$(document).ready(function() {
  var $nextBtn = $("#activity-page-next-btn-enabled");

  // $("#activity-page-next-btn-enabled").show(0);

  loadTimelineProgress(11, buttonID);
  // s2_p0.play();
  $("#activity-page-next-btn-enabled").show().click(function() {
    var id_num = buttonID;
    buttonID = buttonID + 1;
    var next_id = buttonID;
    $(".question" + id_num).fadeOut();
    $(".question" + id_num).css("display", "none");
    $(".question" + next_id).fadeIn();
    $(".question" + next_id).css("display", "block");
    loadTimelineProgress(11, buttonID);
    // console.log(soundArray[buttonID])
    
    if (next_id == 11) {
      // $(".title").text("Congratulations on finishing up your exercise");
      $(".instruction").hide(0);
      $(".button_next").hide(0);
      $("#activity-page-next-btn-enabled").hide();
      // ole.activityComplete.finishingcall();
      ole.activityComplete.finishingcall();
    }
  });
});

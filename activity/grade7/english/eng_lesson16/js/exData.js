var id = 0;
$(document).ready(function() {
  var content = [
    {
      professionDesc: data.string.q1,
      profession: data.string.st1,
      img: $ref + "/slides_images/q01.png"
    }, //
    {
      professionDesc: data.string.q2,
      profession: data.string.st1,
      img: $ref + "/slides_images/q02.png"
    }, //

    {
      professionDesc: data.string.q3,
      profession: data.string.st1,
      img: $ref + "/slides_images/q04.png"
    }, //

    {
      professionDesc: data.string.q4,
      profession: data.string.st2,
      img: $ref + "/slides_images/q044.gif"
    }, //

    {
      professionDesc: data.string.q5,
      profession: data.string.st3,
      img: $ref + "/slides_images/q05.png"
    }, //

    {
      professionDesc: data.string.q6,
      profession: data.string.st4,
      img: $ref + "/slides_images/q06.png"
    }, //
    {
      professionDesc: data.string.q7,
      profession: data.string.st5,
      img: $ref + "/slides_images/q07.png"
    }, //
    {
      professionDesc: data.string.q8,
      profession: data.string.st6,
      img: $ref + "/slides_images/q08.png"
    }, //
    {
      professionDesc: data.string.q9,
      profession: data.string.st7,
      img: $ref + "/slides_images/q09.png"
    }, //

    {
      professionDesc: data.string.q10,
      profession: data.string.st8,
      img: $ref + "/slides_images/q10.png"
    }
  ];
  //console.log(content);
  displayContents();
  function displayContents() {
    var context = $.each(content, function(key, values) {
      id++;
      desc = values.professionDesc;
      prof = values.profession;
      image = values.img;
      appendContents = '<div class="display_ques question' + id + '">';
      appendContents += '<div class="row content_image">';

      appendContents +=
        '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 instructions">';
      appendContents += '<div class="row">';
      appendContents +=
        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description">';
      appendContents += desc;
      appendContents += "</div>"; //end of the desc

      appendContents += "</div>"; //end of row inside instruction div
      appendContents += "</div>"; //end of instruction div

      appendContents += '<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">'; //image
      appendContents +=
        '<img src="' +
        image +
        '" class="question_image' +
        id +
        ' img-responsive"/>';
      appendContents += "</div>"; //end of image

      appendContents += "</div>"; //end of row/content image
      appendContents += "</div>"; //end of question div

      $(".exercise_contents").append(appendContents);
      $(".display_ques").css("display", "none");
      $(".question1").css("display", "block");
      setTimeout(function() {}, 1000);
    });
  }
});

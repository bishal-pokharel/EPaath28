$(document).ready(function() {
  soundAsset = $ref+"/sounds/section_1/";
  var s1_p0 = new buzz.sound(soundAsset+"s1_intro.ogg");
  var s1_p1 = new buzz.sound(soundAsset+"s1_p1.ogg");
  var s1_p2 = new buzz.sound(soundAsset+"s1_p2.ogg");
  var s1_p3 = new buzz.sound(soundAsset+"s1_p3.ogg");
  var s1_p4 = new buzz.sound(soundAsset+"s1_p4.ogg");
  var s1_p5 = new buzz.sound(soundAsset+"s1_p5.ogg");
  var s1_p6 = new buzz.sound(soundAsset+"s1_p6.ogg");
  var s1_p7 = new buzz.sound(soundAsset+"s1_p7.ogg");
  var s1_p8 = new buzz.sound(soundAsset+"s1_p8.ogg");
  var s1_p9 = new buzz.sound(soundAsset+"title.ogg");
  var soundArray = [s1_p0,s1_p1, s1_p2, s1_p3, s1_p4, s1_p5, s1_p6, s1_p7, s1_p8, s1_p9];
  var currentSound = s1_p0;
  var countNext = 0;

  var $total_page = 9;
  loadTimelineProgress($total_page, countNext + 1);
  $("#showdil").click(function() {
    $("#showdil").hide(0);
    $("#start").hide(0);
    $("#start").trigger("click");
  });
  function showcontent(closebtn) {
    //$("#showdil").hide(0);
    $nextBtn.hide(0);
    switch (countNext) {
      case 0:
        $("#diag1").show(0);
        $("#diImg1").show(0);
        $("#diag2").hide(0);
        // soundPlayer(s1_p0, 1);
        var soundid = closebtn? soundArray[9]:soundArray[countNext];
        soundPlayer(soundid, 1);
        $prevBtn.hide(0);
        // $nextBtn.show(0);
        break;
      case 1:
        $("#diag1").hide(0);
        $("#diImg1").show(0);
        $("#diag2").show(0);
        $("#diImg2").hide(0);
        $("#diag3").hide(0);
        soundPlayer(soundArray[countNext], 1);
        $prevBtn.show(0);
        break;
      case 2:
        $("#diImg1").hide(0);
        $("#diag2").hide(0);
        $("#diImg2").show(0);
        $("#diag3").show(0);
        $("#diImg3").hide(0);
        $("#diag4").hide(0);
        soundPlayer(soundArray[countNext], 1);
        break;
      case 3:
        $("#diImg2").hide(0);
        $("#diag3").hide(0);
        $("#diImg3").show(0);
        $("#diag4").show(0);
        $("#diImg4").hide(0);
        $("#diag5").hide(0);
        soundPlayer(soundArray[countNext], 1);
        break;
      case 4:
        $("#diImg3").hide(0);
        $("#diag4").hide(0);
        $("#diImg4").show(0);
        $("#diag5").show(0);
        $("#diImg5").hide(0);
        $("#diag6").hide(0);
        soundPlayer(soundArray[countNext], 1);
        break;
      case 5:
        $("#diImg4").hide(0);
        $("#diag5").hide(0);
        $("#diImg5").show(0);
        $("#diag6").show(0);
        $("#diImg6").hide(0);
        $("#diag7").hide(0);
        soundPlayer(soundArray[countNext], 1);
        break;
      case 6:
        $("#diImg5").hide(0);
        $("#diag6").hide(0);
        $("#diImg6").show(0);
        $("#diag7").show(0);
        $("#diImg7").hide(0);
        $("#diag8").hide(0);
        soundPlayer(soundArray[countNext], 1);
        break;
      case 7:
        $("#diImg6").hide(0);
        $("#diag7").hide(0);
        $("#diImg7").show(0);
        $("#diag8").show(0);
        soundPlayer(soundArray[countNext], 1);
        break;
      case 8:
        $("#diImg7").hide(0);
        $("#diag8").hide(0);
        $("#diImg8").show(0);
        $("#diag9").show(0);
        soundPlayer(soundArray[countNext], 1);
        $nextBtn.hide(0);
        setTimeout(function() {
          ole.footerNotificationHandler.pageEndSetNotification();
        }, 1000);
        break;
      default:
        break;
    }

  }

  function soundPlayer(soundId, showNextBtn){
    console.log(soundId);
    currentSound = soundId;
    currentSound.play();
    currentSound.bind("ended", function(){
      if(showNextBtn)
        countNext<8?$nextBtn.show(0):ole.footerNotificationHandler.pageEndSetNotification();
    });
  }

  var $nextBtn = $("#activity-page-next-btn-enabled");
  $nextBtn.hide(0);
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  $prevBtn.hide(0);
  var countNext = 0;
  $("#diImg1").show(0);
  $("#diag1").show(0);
  $nextBtn.click(function() {
    currentSound.stop();
    countNext++;
    loadTimelineProgress($total_page, countNext + 1);
    showcontent();
  });

  $prevBtn.click(function() {
    countNext--;
    countNext < 7 ? ole.footerNotificationHandler.hideNotification() : null;
    showcontent();
  });

  currentSound.play();
  currentSound.bind("ended", function(){
    $(".closebtn").show(0);
    $(".closebtn").click(function() {
      showcontent(true);
    });
  });
});

var buttonID = 1;
var id_num = 1;
$(document).ready(function() {
  soundAsset = $ref+"/sounds/section_2/";
  var s2_p0 = new buzz.sound(soundAsset+"s2_p1.ogg");
  var s2_p1 = new buzz.sound(soundAsset+"s2_p2.ogg");
  var s2_p2 = new buzz.sound(soundAsset+"s2_p3.ogg");
  var s2_p3 = new buzz.sound(soundAsset+"s2_p4.ogg");
  var s2_p4 = new buzz.sound(soundAsset+"s2_p5.ogg");
  var s2_p5 = new buzz.sound(soundAsset+"s2_p6.ogg");
  var s2_p6 = new buzz.sound(soundAsset+"s2_p7.ogg");
  var s2_p7 = new buzz.sound(soundAsset+"s2_p8.ogg");
  var s2_p8 = new buzz.sound(soundAsset+"s2_p9.ogg");
  var s2_p9 = new buzz.sound(soundAsset+"s2_p10.ogg");
  var s2_p10 = new buzz.sound(soundAsset+"s2_p11.ogg");
  var s2_p11 = new buzz.sound(soundAsset+"s2_p12.ogg");
  var soundArray = [s2_p0,s2_p1, s2_p2, s2_p3, s2_p4, s2_p5, s2_p6, s2_p7, s2_p8,s2_p9,s2_p10,s2_p11];
  var currentSound = s2_p0;
  var $nextBtn = $("#activity-page-next-btn-enabled");

  function soundPlayer(soundId, showNextBtn){
    // console.log(soundId);
    currentSound = soundId;
    currentSound.play();
    currentSound.bind("ended", function(){
      if(showNextBtn)
        buttonID<13?$nextBtn.show(0):ole.footerNotificationHandler.lessonEndSetNotification();
          });
  }
  // $("#activity-page-next-btn-enabled").show(0);
  $(".exercise_button").click(function() {
    location.reload();
  });

  loadTimelineProgress(12, buttonID);
  // s2_p0.play();
  soundPlayer(soundArray[(buttonID-1)], 1);
  $("#activity-page-next-btn-enabled").click(function() {
    console.log("CurrentBtnId---->"+buttonID);
    currentSound.stop();
    var id_num = buttonID;
    buttonID = buttonID + 1;
    var next_id = buttonID;
    console.log(id_num);
    console.log(next_id);
    $(".question" + id_num).fadeOut();
    $(".question" + id_num).css("display", "none");
    $(".question" + next_id).fadeIn();
    $(".question" + next_id).css("display", "block");
    loadTimelineProgress(12, buttonID);
    // console.log(soundArray[buttonID])
    soundPlayer(soundArray[(buttonID-1)], 1);
    if (next_id == 12) {
      // $(".title").text("Congratulations on finishing up your exercise");
      $(".instruction").hide(0);
      $(".button_next").hide(0);
      $("#activity-page-next-btn-enabled").hide();
      // ole.activityComplete.finishingcall();
      ole.footerNotificationHandler.lessonEndSetNotification();
    }
  });
  // $('.bg_image').on('click', ".button_next", function(){
  // 	console.log("ok");
  // 	var id_num = buttonID;
  // 	buttonID = buttonID + 1;
  // 	var next_id = buttonID;
  // 	console.log(id_num);
  // 	console.log(next_id);
  // 	$(".question" + id_num).fadeOut();
  // 	$(".question" + id_num).css("display","none");
  // 	$(".question" + next_id).fadeIn();
  // 	$(".question" + next_id).css("display","block");
  // 	loadTimelineProgress(next_id, 13);
  // 	if(next_id==13){
  // 		// $(".title").text("Congratulations on finishing up your exercise");
  // 		$(".instruction").hide(0);
  // 		$(".button_next").hide(0);
  // 		// ole.activityComplete.finishingcall();
  // 		ole.footerNotificationHandler.lessonEndSetNotification();
  //
  // 	}
  // });
});

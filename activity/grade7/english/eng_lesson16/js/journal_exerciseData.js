var id = 0;
$(document).ready(function() {
  var content = [
    {
    	professionDesc : data.string.dig9,
		profession:data.string.st1,
		img : $ref + "/slides_images1/step-01_paper_cutting.gif"
	},//
	{
		professionDesc : data.string.dig10,
		profession:data.string.st1,
		img : $ref + "/slides_images1/step-010_paper_cutting.png"
	},//

  {
		professionDesc : data.string.dig12,
		profession:data.string.st2,
		img : $ref + "/slides_images1/step-03-paper-cutting.gif"
	},//
	
	{
		professionDesc : data.string.dig13,
		profession:data.string.st3,
		img : $ref + "/slides_images1/step-04-paper-cutting.gif"
	},//
	
	{
		professionDesc : data.string.dig14,
		profession:data.string.st4,
		img : $ref + "/slides_images1/step-05-paper-cutting.gif"
	},//
	{
		professionDesc : data.string.dig15,
		profession:data.string.st5,
		img : $ref + "/slides_images1/step-05a-paper-cutting.png"
	},//
	{
		professionDesc : data.string.dig16,
		profession:data.string.st6,
		img : $ref + "/slides_images1/step-06-paper-cutting.png"
	},//
	{
		professionDesc : data.string.dig17,
		profession:data.string.st7,
		img : $ref + "/slides_images1/step-07-paper-cutting.gif"
	},//
	
	{
		professionDesc : data.string.dig18,
		profession:data.string.st8,
		img : $ref + "/slides_images1/step-08-paper-cutting.gif"
	},//
	
	{
		professionDesc : data.string.dig19,
		profession:data.string.st9,
		img : $ref + "/slides_images1/step-09-paper-cutting.png"
	},//
	
	{
		professionDesc : data.string.dig20,
		profession:data.string.st10,
		img : $ref + "/slides_images1/step-10-paper-cutting.gif"
	},//
	
	{
		professionDesc : data.string.dig21,
		img : $ref + "/slides_images1/step-11-paper-cutting.png"
	},//

  ];
  //console.log(content);
  displayContents();
  function displayContents() {
    var context = $.each(content, function(key, values) {
      id++;
      desc = values.professionDesc;
      prof = values.profession;
      image = values.img;
      appendContents = '<div class="display_ques question' + id + '">';
      appendContents += '<div class="row content_image">';

      appendContents +=
        '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 instructions">';
      appendContents += '<div class="row">';
      appendContents +=
        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description">';
      appendContents += desc;
      appendContents += "</div>"; //end of the desc

      appendContents += "</div>"; //end of row inside instruction div
      appendContents += "</div>"; //end of instruction div

      appendContents += '<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">'; //image
      appendContents +=
        '<img src="' +
        image +
        '" class="question_image' +
        id +
        ' img-responsive"/>';
      appendContents += "</div>"; //end of image

      appendContents += "</div>"; //end of row/content image
      appendContents += "</div>"; //end of question div

      $(".exercise_contents").append(appendContents);
      $(".display_ques").css("display", "none");
      $(".question1").css("display", "block");
      setTimeout(function() {}, 1000);
    });
  }
});

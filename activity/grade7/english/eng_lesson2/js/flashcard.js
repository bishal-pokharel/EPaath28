var correctanswers = 0;
var totalquestions = $(".choiceoptions").length;

$(document).ready(function() {
  $(".title").text(data.string.exerciseTitle2);
  var content = [
    {
      words: data.string.word1,
      description: data.string.des1,
      img: $ref + "/exercise_images/introduce.jpg"
    }, //
    {
      words: data.string.word2,
      description: data.string.des2,
      img: $ref + "/exercise_images/skip.jpg"
    }, //

    {
      words: data.string.word3,
      description: data.string.des3,
      img: $ref + "/exercise_images/humid.gif"
    }, //

    {
      words: data.string.word4,
      description: data.string.des4,
      img: $ref + "/exercise_images/favourite.jpg"
    }, //
    {
      words: data.string.word5,
      description: data.string.des5,
      img: $ref + "/exercise_images/leave.jpg"
    }, //
    {
      words: data.string.word6,
      description: data.string.des6,
      img: $ref + "/exercise_images/mountains.jpg"
    }, //
    {
      words: data.string.word7,
      description: data.string.des7,
      img: $ref + "/exercise_images/cool.jpg"
    }, //
    {
      words: data.string.word8,
      description: data.string.des8,
      img: $ref + "/exercise_images/shut.png"
    }, //
    {
      words: data.string.word9,
      description: data.string.des9,
      img: $ref + "/exercise_images/snow.jpg"
    }, //
    {
      words: data.string.word10,
      description: data.string.des10,
      img: $ref + "/exercise_images/melt.jpg"
    }, //
    {
      words: data.string.word11,
      description: data.string.des11,
      img: $ref + "/exercise_images/freeze.jpg"
    }, //
    {
      words: data.string.word12,
      description: data.string.des12,
      img: $ref + "/exercise_images/hot.jpg"
    }, //
    {
      words: data.string.word13,
      description: data.string.des13,
      img: $ref + "/exercise_images/unbearable.gif"
    }, //
    {
      words: data.string.word14,
      description: data.string.des14,
      img: $ref + "/exercise_images/excited.jpg"
    }, //
    {
      words: data.string.word15,
      description: data.string.des15,
      img: $ref + "/exercise_images/trip.jpg"
    }, //
    {
      words: data.string.word16,
      description: data.string.des16,
      img: $ref + "/exercise_images/journey.jpg"
    }, //
    {
      words: data.string.word17,
      description: data.string.des17,
      img: $ref + "/exercise_images/journal.jpg"
    } //
  ];
  console.log(content);
  displaycontent();
  function displaycontent() {
    var words = $.each(content, function(key, value) {
      words = value.words;
      description = value.description;
      image = value.img;
      // console.log(description);
      appendTab = "<li>";
      appendTab +=
        '<a href="#' + words + '" data-toggle="tab"  >' + words + "</a>";
      appendTab += "</li>";
      $("#myTab").append(appendTab);

      appendcontent = '<div id="' + words + '" class="tab-pane ">';
      appendcontent += '<div class="f1_container">';
      appendcontent += '<div class="shadow f1_card">';
      appendcontent += '<div class="front face">';
      appendcontent += "<p>" + words + "</p>";
      appendcontent += "</div>";
      appendcontent += '<div class="back face center">';
      appendcontent +=
        '<img src="' + image + '" class="tab_image img-responsive"/>';
      appendcontent += '<p class="textroll">';
      appendcontent += description;
      appendcontent += "</p>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      $("#myTabContent").append(appendcontent);
      ole.footerNotificationHandler.pageEndSetNotification();
    });
  }
});
$(document).ready(function() {
  loadTimelineProgress(1, 1);
  $(".f1_container").click(function() {
    //alert("ok");
    $(this).toggleClass("active");
  });

  $("#myTab > li:last-child").click(function() {
    //alert("ok");
    $(".finishtxt").show(0);
  });

  $(".closebtn").click(function() {
    //ole.activityComplete.finishingcall();
  });
});

var id=1;
var buttonId =1;
$(document).ready(function(){
	var $total_page = 3;
	loadTimelineProgress($total_page,buttonId);
	$("#activity-page-next-btn-enabled").hide(0);
	$("#activity-page-next-btn-enabled").click(function(){
		var journalid = buttonId;
		buttonId = buttonId + 1;
		loadTimelineProgress($total_page,buttonId);
		var next_id = buttonId;
		console.log(journalid);
		console.log(next_id);

		$(".para_date" + journalid).hide(0);
		$(".para_content" + journalid).hide(0);

		$(".para_date" + next_id).show(0);
		$(".para_content" + next_id).show(0);
		$("#activity-page-next-prev-enabled").show(0);

		//need to function the button clicks
	   $(".para_date1").hide(0);
	   $(".para_content1").hide(0);
	   if(journalid >= 2) {
		   $("#activity-page-next-btn-enabled").hide(0);
		   $("#activity-page-next-prev-enabled").hide(0);
		   
		   $(".finishtxt1").show(0);
		   ole.footerNotificationHandler.pageEndSetNotification();
	   }
	});

	$("#activity-page-next-prev-enabled").click(function(){
		var journalid = buttonId;
		buttonId = buttonId - 1;
		loadTimelineProgress($total_page,buttonId);
		var next_id = buttonId;
		console.log(journalid);
		console.log(next_id);
		 $("#activity-page-next-btn-enabled").show(0);
		$(".para_date" + journalid).hide(0);
		$(".para_content" + journalid).hide(0);

		$(".para_date" + next_id).show(0);
		$(".para_content" + next_id).show(0);
		//$("#activity-page-next-prev-enabled").show(0);

		//need to function the button clicks
	   if(journalid == 2) {
		   $("#activity-page-next-prev-enabled").hide(0);
		   $("#activity-page-next-btn-enabled").show(0);
	   }
	});

	$(".closebtn").click(function(){
		$(this).parent().css("display", "none");
		if(buttonId == 1){
			$("#activity-page-next-btn-enabled").show(0);
		}else{
			$("#activity-page-next-prev-enabled").show(0);
		}
	});
});

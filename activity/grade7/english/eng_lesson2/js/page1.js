imageAsset = $ref+"/slide_images/";
soundAsset = $ref+"/sounds/slide1/";

var dialog0 = new buzz.sound(soundAsset+"introduction.ogg");
var dialog1 = new buzz.sound(soundAsset+"1.ogg");
var dialog2part1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"3.ogg");
var dialog2 = [dialog2part1,dialog2part2];

var dialog3part1 = new buzz.sound(soundAsset+"4.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"5.ogg");
var dialog3 = [dialog3part1,dialog3part2];
var dialog4 = new buzz.sound(soundAsset+"6.ogg");

var soundcontent = [dialog0,dialog1, dialog2, dialog3, dialog4];

var content=[
    {
        bgImgSrc : imageAsset+"introduction_of_a_new_student.png",
        forwhichdialog : "dialog0",
        lineCountDialog : [data.string.dig0],
        speakerImgSrc : $ref+"/slide_images/speaker.png",
        listenAgainText : data.string.listenAgainData,
    },
	{
		bgImgSrc : imageAsset+"2a.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.dig1],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"2a.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "",
			},
			
		],
		lineCountDialog : [data.string.dig111,
		                   data.string.dig111_1,
		                   ],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"2a.jpg",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
			lineCountDialog : [data.string.dig222,
		                  	   data.string.dig222_2,
		                   ],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"2a.jpg",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "",
			},
			
		],
		lineCountDialog : [data.string.dig333,],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 5;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(prevBtn){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
		if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
			}
			
			
			if(countNext == 0 && prevBtn==false){
				$('#startsound').on('click',function () {
					playThisDialog[0].play();
					
				});
			}	else {
				playThisDialog[0].play();
			}

			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		/*this function binds appropriate events handlers to the 
		audio as required*/
		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		slide1(true);
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
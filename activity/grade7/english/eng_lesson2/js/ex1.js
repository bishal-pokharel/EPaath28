//$(document).ready(function(){
(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  $(".title").text(data.string.exerciseTitle);
  var content = [
    {
      question: data.string.j1,
      answers: [
        { ans: data.string.ans1 },
        { ans: data.string.ans2, correct: "correct" },
        { ans: data.string.ans3 }
      ],
      img: $ref + "/exercise_images/bad.jpg"
    },
    {
      question: data.string.j2,
      answers: [
        { ans: data.string.ans4 },
        { ans: data.string.ans5, correct: "correct" },
        { ans: data.string.ans6 }
      ],
      img: $ref + "/exercise_images/athens.jpg"
    },
    {
      question: data.string.j3,
      answers: [
        { ans: data.string.ans7, correct: "correct" },
        { ans: data.string.ans8 },
        { ans: data.string.ans9 }
      ],
      img: $ref + "/exercise_images/athena.jpg"
    },
    {
      question: data.string.j4,
      answers: [
        { ans: data.string.ans10 },
        { ans: data.string.ans11, correct: "correct" },
        { ans: data.string.ans12 }
      ],
      img: $ref + "/exercise_images/museum.jpg"
    },
    {
      question: data.string.j5,
      answers: [
        { ans: data.string.ans13 },
        { ans: data.string.ans14, correct: "correct" },
        { ans: data.string.ans15 }
      ],
      img: $ref + "/exercise_images/bronzeage.jpg"
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $total_page = 6;
  loadTimelineProgress($total_page, questionCount + 1);
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();
  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  var correctlyanswered = 0;
  $board.on("click", ".neutral", function() {
    // console.log("what");
    if (answered) {
      return answered;
    }
    attemptcount++;
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      play_correct_incorrect_sound(true);
    } else {
      $this.addClass("wrong").removeClass("neutral");
      play_correct_incorrect_sound(false);
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress($total_page, questionCount + 1);
    if (questionCount < 5) {
      qA();
    } else if (questionCount == 5) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title")
        .html(
          "Congratulations on finishing your exercise <br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .css({
          position: "absolute",
          top: "48%",
          transform: "translateY(-50%)"
        });
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
})(jQuery);

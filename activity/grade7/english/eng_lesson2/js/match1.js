//$(document).ready(function(){
(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");

  $('.title').text(data.string.exerciseTitle3);
  var content = [
  {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8,correct : "correct"},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/baitadi.png"
  },
  

    {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9,correct : "correct"},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/bajhang.png"
  },
  

  {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5,correct : "correct"},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/chitwan.png"
  },

    {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7,correct : "correct"},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/dhankuta.png"
  },

    {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6,correct : "correct"},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/jhapa.png"
  },

    {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2,correct : "correct"},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/kapilvastu.png"
  },
   
   {
    
    answers: [
    {ans : data.string.cityname1,correct : "correct"},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/kathmandu.png"
  },


  {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3,correct : "correct"},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/mugu.png"
  },

  {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10,correct : "correct"}
    ],
    img : $ref+"/image/mustang.png"
  },

  {
    
    answers: [
    {ans : data.string.cityname1},
    {ans : data.string.cityname2},
    {ans : data.string.cityname3},
    {ans : data.string.cityname4,correct : "correct"},
    {ans : data.string.cityname5},
    {ans : data.string.cityname6},
    {ans : data.string.cityname7},
    {ans : data.string.cityname8},
    {ans : data.string.cityname9},
    {ans : data.string.cityname10}
    ],
    img : $ref+"/image/solukhumbu.png"
  }


     
  
    
  ];

  $nextBtn.fadeOut();

  // console.log(content);
  
  var questionCount = 0;
  
	var $total_page = 11;
	loadTimelineProgress($total_page,questionCount+1);
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");

  
  console.log(content);
  function  qA() {
      var source = $('#qA-templete').html();
      var template = Handlebars.compile(source);
      var html = template(content[questionCount]);
      $board.html(html);
		attemptcount = 0;
		answered = false;
      // console.log(html);
  }

  qA();


	var answered = false;
	var attemptcount = 0;
		
	var totalq = content.length;
	var correctlyanswered = 0;
	


  $board.on('click','.neutral',function () {
    // console.log("what");
	if(answered){
		return answered;
	}
	attemptcount++;
    var $this = $(this);
    var isCorrect = $(this).data('correct');
    if(isCorrect=== "correct") {
		if(attemptcount == 1){
			correctlyanswered++;
		}
		answered = true;
      $this.addClass('right').removeClass('neutral');
      $nextBtn.fadeIn();
		play_correct_incorrect_sound(true); 
	} else {
		play_correct_incorrect_sound(false);
	      $this.addClass('wrong').removeClass('neutral');
    }
  });

  $nextBtn.on('click',function () {
    $nextBtn.fadeOut();
    questionCount++;
	loadTimelineProgress($total_page,questionCount+1);
  if(questionCount<10){
      qA();
    }
    else if (questionCount==10){
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $('.title').hide(0);
      //$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.");
      $(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
    }
  });

  $( ".closebtn" ).click(function() {
        ole.activityComplete.finishingcall();
      });

})(jQuery);


imageAsset = $ref+"/slides_images/";
soundAsset = $ref+"/sounds/slide1/";


var dialog0 = new buzz.sound(soundAsset+"kiteflying.ogg");
var dialog1part1 = new buzz.sound(soundAsset+"1.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"2.ogg");
var dialog1 = [dialog1part1,dialog1part2];

var dialog2 = new buzz.sound(soundAsset+"3.ogg");
var dialog3 = new buzz.sound(soundAsset+"4.ogg");
var dialog4 = new buzz.sound(soundAsset+"5.ogg");
var dialog5 = new buzz.sound(soundAsset+"6.ogg");
var dialog6 = new buzz.sound(soundAsset+"7.ogg");

var dialog7part1 = new buzz.sound(soundAsset+"8.ogg");
var dialog7part2 = new buzz.sound(soundAsset+"9.ogg");
var dialog7 = [dialog7part1,dialog7part2];

var dialog8 = new buzz.sound(soundAsset+"10.ogg");
var dialog9 = new buzz.sound(soundAsset+"11.ogg");
var dialog10 = new buzz.sound(soundAsset+"12.ogg");
var dialog11 = new buzz.sound(soundAsset+"13.ogg");
var dialog12 = new buzz.sound(soundAsset+"14.ogg");
var dialog13 = new buzz.sound(soundAsset+"15.ogg");
var dialog14 = new buzz.sound(soundAsset+"16.ogg");
var dialog15= new buzz.sound(soundAsset+"17.ogg");
var dialog16 = new buzz.sound(soundAsset+"18.ogg");
var dialog17 = new buzz.sound(soundAsset+"19.ogg");
var dialog18 = new buzz.sound(soundAsset+"20.ogg");
var dialog19 = new buzz.sound(soundAsset+"21.ogg");
var dialog20 = new buzz.sound(soundAsset+"22.ogg");

var dialog21part1 = new buzz.sound(soundAsset+"23.ogg");
var dialog21part2 = new buzz.sound(soundAsset+"24.ogg");
var dialog21 = [dialog21part1,dialog21part2];

var dialog22part1 = new buzz.sound(soundAsset+"25.ogg");
var dialog22part2 = new buzz.sound(soundAsset+"26.ogg");
var dialog22 = [dialog22part1,dialog22part2];

var dialog23 = new buzz.sound(soundAsset+"27.ogg");
var dialog24 = new buzz.sound(soundAsset+"28.ogg");

var soundcontent = [dialog0,dialog1, dialog2, dialog3, dialog4,dialog5,dialog6,dialog7,dialog8,dialog9,dialog10,dialog11,dialog12,dialog13,dialog14,dialog15,dialog16,dialog17,dialog18,dialog19,dialog20,dialog21,dialog22,dialog23,dialog24];

var content=[
    {
        bgImgSrc : imageAsset+"kite_flying.png",
        forwhichdialog : "dialog0",
        lineCountDialog : [data.string.digs0],
        speakerImgSrc : $ref+"/slides_images/speaker.png",
        listenAgainText : data.string.listenAgainData,
    },
	{
		bgImgSrc : imageAsset+"17a.png",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_1part1,data.string.digs1_1part2],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new5.png",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_2],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"bg_kites.png",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_3],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new2.png",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_4],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new2.png",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_5],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new3.png",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_6],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	

	{
		bgImgSrc : imageAsset+"bg_kites2.png",
		forwhichdialog : "dialog7",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_7part1,data.string.digs1_7part2],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"bg_kites2.png",
		forwhichdialog : "dialog8",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_8],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new6.png",
		forwhichdialog : "dialog9",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_9],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new7.png",
		forwhichdialog : "dialog10",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_10],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new8.png",
		forwhichdialog : "dialog11",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_11],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"tangle.gif",
		forwhichdialog : "dialog12",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_12],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new4.png",
		forwhichdialog : "dialog13",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_13],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},	

	{
		bgImgSrc : imageAsset+"17c.png",
		forwhichdialog : "dialog14",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_14],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},	

	{
		bgImgSrc : imageAsset+"new6.png",
		forwhichdialog : "dialog15",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_15],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"bg_kites2.png",
		forwhichdialog : "dialog16",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_16],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},	

	{
		bgImgSrc : imageAsset+"new4.png",
		forwhichdialog : "dialog17",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_17],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},	

	{
		bgImgSrc : imageAsset+"new3.png",
		forwhichdialog : "dialog18",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_18],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},	

	{
		bgImgSrc : imageAsset+"bg_kites.png",
		forwhichdialog : "dialog19",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_19],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"17c.png",
		forwhichdialog : "dialog20",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_20],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},


{
		bgImgSrc : imageAsset+"da.png",
		forwhichdialog : "dialog21",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_21part1,data.string.digs1_21part2],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"wa.png",
		forwhichdialog : "dialog22",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_22part1,data.string.digs1_22part2],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"17b.png",
		forwhichdialog : "dialog23",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digs1_23],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"new2.png",
		forwhichdialog : "dialog24",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digs1_24],
		speakerImgSrc : $ref+"/slides_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 25;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(prevBtn){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			
		if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
			}
			
			
			if(countNext == 0 && prevBtn==false){
				$('#startsound').on('click',function () {
					playThisDialog[0].play();
					
				});
			}	else {
				playThisDialog[0].play();
			}

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		/*this function binds appropriate events handlers to the 
		audio as required*/
		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.lessonEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		console.log(countNext);
		if(countNext == 2)
		{
			$(".kite_red").show(0);
		}
		if(countNext == 3)
		{
			$(".kite_red").hide(0);
		}

		if(countNext == 6)
		{
			$(".kite_soar").show(0);
			$(".kite_ex1").show(0);
			$(".kite_ex2").show(0);
			$(".kite_ex3").show(0);
		}

		if(countNext == 8)
		{
			$(".kite_soar").hide(0);
			$(".kite_ex1").hide(0);
			$(".kite_ex2").hide(0);
			$(".kite_ex3").hide(0);
		}

		if(countNext == 15)
		{
			$(".kite_soar").show(0);
			$(".kite_ex1").show(0);
			$(".kite_ex2").show(0);
			$(".kite_ex3").show(0);
		}

		if(countNext == 16)
		{
			$(".kite_soar").show(0);
			$(".kite_ex1").show(0);
			$(".kite_ex2").show(0);
			$(".kite_ex3").show(0);
		}

		if(countNext == 17)
		{
			$(".kite_blue").show(0);
			
		}
		if(countNext == 18)
		{
			$(".kite_soar").show(0);
			$(".kite_ex1").show(0);
			$(".kite_ex2").show(0);
			$(".kite_ex3").show(0);
			
		}

		if(countNext == 19)
		{
			$(".kite_soar").hide(0);
			$(".kite_ex1").hide(0);
			$(".kite_ex2").hide(0);
			$(".kite_ex3").hide(0);
			$(".kite_blue").hide(0);
			
		}


		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		slide1(true);
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
$('.title').text(data.string.title);
$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
			var content = [ {
				words : data.string.word1,
				description : data.string.des1,
				img : $ref + "/exercise_image/spiral.jpg",
			},//
			{
				words : data.string.word2,
				description : data.string.des2,
				img : $ref + "/exercise_image/lattai.jpg",
			},//

			{
				words : data.string.word3,
				description : data.string.des3,
				img : $ref + "/exercise_image/soar.jpg",
			},//

			{
				words : data.string.word4,
				description : data.string.des4,
				img : $ref + "/exercise_image/tangled.jpg",
			},//
			{
				words : data.string.word5,
				description : data.string.des5,
				img : $ref + "/exercise_image/release.jpg",
			},//
			
		
			];
			console.log(content);
			displaycontent();
			function displaycontent() {
				var words = $.each(content, function(key, value) {
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image img-responsive"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);

				});
			}
			/*
			 * function qA() { var source = $('#qA-templete').html(); var
			 * template = Handlebars.compile(source); var html =
			 * template(content); //$board.html(html); // console.log(html); }
			 * 
			 * qA();
			 */

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		//alert("ok");
		$(this).toggleClass('active');
	}); 
	ole.footerNotificationHandler.pageEndSetNotification();
});
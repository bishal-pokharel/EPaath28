$(document).ready(function(){
	var lesson1_Dialouge = new buzz.sound($ref+"/audio/lesson17_slide.ogg");
	$("#showdil").click(function(){
		$("#showdil").hide(0);
		$("#start").hide(0);
		$('#start').trigger('click');
	});
	$("#start").click(function() {
		$("#showdil").hide(0);
		$("#start").hide(0);
		
		$("#diImg1").fadeIn(1500);
		$("#diImg2").fadeIn(1500);
		$("#diImg3").fadeIn(1500);
		$("#diImg4").fadeIn(1500);
		$("#diImg5").fadeIn(1500);
		$("#character2").delay(1500).fadeTo("slow", 0.5, function() {
		});
		$("#diag1").fadeIn(1500);
		$("#diag1").delay(10000).fadeOut(800);
		
		
		$("#character1").delay(12000).fadeTo("slow", 0.5, function() {
		});
		$("#character2").delay(8000).fadeTo("slow", 10, function() {
		});
		$("#diag2").delay(12300).fadeIn(800);
		$("#diag2").delay(14000).fadeOut(800);
		
		
		$("#character1").delay(15000).fadeTo("slow", 10, function() {
		});
		$("#character2").delay(15000).fadeTo("slow", 0.5, function() {
		});
		$("#diag3").delay(27900).fadeIn(800);
		$("#diag3").delay(7500).fadeOut(800);
		
		
		$("#character1").delay(8000).fadeTo("slow", 0.5, function() {
		});
		$("#character2").delay(10000).fadeTo("slow", 10, function() {
		});
		$("#diag4").delay(37000).fadeIn(800);
		$("#diag4").delay(1000).fadeOut(800);
		
		
		$("#character1").delay(2000).fadeTo("slow", 10, function() {
		});
		$("#character2").delay(2000).fadeTo("slow", 0.5, function() {
		});
		$("#diag5").delay(39600).fadeIn(800);
		$("#diag5").delay(2000).fadeOut(800);
		
		
		$("#character1").delay(3000).fadeTo("slow", 0.5, function() {
		});
		$("#character2").delay(3000).fadeTo("slow", 10, function() {
		});
		$("#diag6").delay(43200).fadeIn(800);
		$("#diag6").delay(10000).fadeOut(800);
		$("#diImg1").delay(43200).fadeOut(1500);
		$("#diImg2").delay(43200).fadeOut(1500);
		$("#diImg3").delay(43200).fadeOut(1500);
		$("#diImg4").delay(43200).fadeOut(1500);
		$("#diImg5").delay(43200).fadeOut(1500);
		
		
		
		
		
		$("#diImg6").delay(45000).fadeIn(1500);
		$("#diImg7").delay(45000).fadeIn(1500);
		
		$("#character1").delay(12000).fadeTo("slow", 10, function() {
		});
		$("#character2").delay(12000).fadeTo("slow", 0.5, function() {
		});
		$("#diag7").delay(54800).fadeIn(800);
		$("#diag7").delay(10000).fadeOut(800);
		
		
		$("#character1").delay(8000).fadeTo("slow", 0.5, function() {
		});
		$("#character2").delay(8000).fadeTo("slow", 10, function() {
		});
		$("#diag8").delay(66400).fadeIn(800);
		$("#diag8").delay(3000).fadeOut(800);
		
		
		$("#diImg6").delay(35000).fadeOut(1500);
		$("#diImg7").delay(35000).fadeOut(1500);
		
		
		
		
		//kite gets tangled
		$("#diImg8").delay(83000).fadeIn(1500);
		$("#diImg9").delay(83000).fadeIn(1500);
		
		$("#diImg2").delay(67000).fadeIn(800);
		$("#character1").delay(5000).fadeTo("slow", 10, function() {
		});
		$("#character2").delay(5000).fadeTo("slow", 0.5, function() {
		});
		$("#diag9").delay(71000).fadeIn(800);
		$("#diag9").delay(8000).fadeOut(800);
		
		$("#diImg8").delay(15000).fadeOut(1500);
		$("#diImg9").delay(15000).fadeOut(1500);
		
		
		
		
		
		//blue kite challenge
		$("#diImg10").delay(100000).fadeIn(1500);
		$("#diImg11").delay(100000).fadeIn(1500);
		
		$("#character1").delay(8000).fadeTo("slow", 0.5, function() {
		});
		$("#character2").delay(8000).fadeTo("slow", 10, function() {
		});
		$("#diag10").delay(80600).fadeIn(800);
		$("#diag10").delay(8000).fadeOut(800);
		
		
		$("#character1").delay(10000).fadeTo("slow", 10, function() {
		});
		$("#character2").delay(10000).fadeTo("slow", 0.5, function() {
		});
		$("#diag11").delay(90200).fadeIn(800);
		$("#diag11").delay(8000).fadeOut(800);
		
		
		$("#character1").delay(5000).fadeTo("slow", 0.5, function() {
		});
		$("#character2").delay(5000).fadeTo("slow", 10, function() {
		});
		$("#diag11").delay(99800).fadeIn(800);
		$("#diag11").delay(8000).fadeOut(800);
		
		
		
		$("#diag12").delay(102200).fadeIn(800);
		$("#diag12").delay(5000).fadeOut(800);
		
		$("#diag13").delay(108800).fadeIn(800);
		$("#diag13").delay(12000).fadeOut(800);
		
		$("#diag14").delay(122400).fadeIn(800);
		$("#diag14").delay(8000).fadeOut(800);
		
		$("#diag15").delay(132000).fadeIn(800);
		$("#diag15").delay(8000).fadeOut(800);
		
		$("#diag16").delay(141600).fadeIn(800);
		$("#diag16").delay(5000).fadeOut(800);
		
		$("#diag17").delay(148200).fadeIn(800);
		$("#diag17").delay(5000).fadeOut(800);
		
		$("#diag18").delay(154800).fadeIn(800);
		$("#diag18").delay(5000).fadeOut(800);
		
		//blue kite challenge
		$("#diImg10").delay(40000).fadeOut(1500);
		$("#diImg11").delay(40000).fadeOut(1500);
		
		
		
		
		$("#diImg12").delay(140000).fadeIn(1500);
		$("#diag19").delay(161400).fadeIn(800);
		$("#diag19").delay(8000).fadeOut(800);
		
		
		$("#diag20").delay(171000).fadeIn(800);
		$("#diag20").delay(15000).fadeOut(800);
		
		$("#diag21").delay(187600).fadeIn(800);
		$("#diag21").delay(21000).fadeOut(800);
		
		$("#diag22").delay(210200).fadeIn(800);
		$("#diag22").delay(5000).fadeOut(800);
		
		$("#diag23").delay(216800).fadeIn(800);
		$("#diag23").delay(5000).fadeOut(800);
		$("#diImg12").delay(75000).fadeOut(1500);
		
		$("#showdil").delay(223400).fadeIn(2500);
		$("#start").delay(223400).fadeIn(2500);
	});
	$("#start").click(function() {
		lesson1_Dialouge.play();
	});
});
//$(document).ready(function(){
var imgpath = $ref+"/images/";
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.exerciseTitle);
	var content = [
	{
		question : data.string.q1,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3,
			correct : "correct"},
		],
		img: imgpath + "q01.png"
	},
	{
		question : data.string.q2,
		answers: [
		{ans : data.string.name4},
		{ans : data.string.name5,
			correct : "correct"},
		{ans : data.string.name6},
		],
		img: imgpath + "q02.png"
	},
	{
		question : data.string.q3,
		answers: [
		{ans : data.string.name7,
			correct : "correct"},
		{ans : data.string.name8},
		{ans : data.string.name9},
		],
		img: imgpath + "q03.png"
	},
	{
		question : data.string.q4,
		answers: [
		{ans : data.string.name10},
		{ans : data.string.name11 ,
			correct : "correct"},
		{ans : data.string.name12},
		],
		img: imgpath + "q04.png"
	},
	{
		question : data.string.q5,
		answers: [
		{ans : data.string.name13,
			correct : "correct"},
		{ans : data.string.name14},
		{ans : data.string.name15},
		],
		img: imgpath + "q05.png"
	},
	
	{
		question : data.string.q6,
		answers: [
		{ans : data.string.name16},
		{ans : data.string.name17},
		{ans : data.string.name18,
			correct : "correct"},
		],
		img: imgpath + "q06.png"
	},
	
	{
		question : data.string.q7,
		answers: [
		{ans : data.string.name19},
		{ans : data.string.name20,
			correct : "correct"},
		{ans : data.string.name21},
		],
		img: imgpath + "q07.png"
	},
	
	{
		question : data.string.q8,
		answers: [
		{ans : data.string.name22},
		{ans : data.string.name23},
		{ans : data.string.name24,
			correct : "correct"},
		],
		img: imgpath + "q08.png"
	},
	
	{
		question : data.string.q9,
		answers: [
		{ans : data.string.name25},
		{ans : data.string.name26,
			correct : "correct"},
		{ans : data.string.name27},
		],
		img: imgpath + "q09.png"
	},
	
	{
		question : data.string.q10,
		answers: [
		{ans : data.string.name28},
		{ans : data.string.name29,
			correct : "correct"},
		],
		img: imgpath + "q10.png"
	},

	{
		question : data.string.q11,
		answers: [
		{ans : data.string.name30,
			correct : "correct"},
		{ans : data.string.name31},
		],
		img: imgpath + "q01.png"
	}
];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}

	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		// console.log("what");
		if(answered){
			return answered;
		}
		attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
			play_correct_incorrect_sound(false);
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<11){
			qA();
		}
		else if (questionCount==11){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

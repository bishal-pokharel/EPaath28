var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";
//
var sound_ting = new buzz.sound((soundAsset + "ding.ogg"));
var s1_p1 = new buzz.sound((soundAsset + "mahakallibridge.ogg"));
var s1_p2 = new buzz.sound((soundAsset + "s1_p2.ogg"));
var s1_p3 = new buzz.sound((soundAsset + "s1_p3.ogg"));
var s1_p4 = new buzz.sound((soundAsset + "s1_p4.ogg"));
var s1_p5 = new buzz.sound((soundAsset + "s1_p5(1).ogg"));
var s1_p5_1 = new buzz.sound((soundAsset + "s1_p5(2).ogg"));
var s1_p6 = new buzz.sound((soundAsset + "s1_p6(1).ogg"));
var s1_p6_1 = new buzz.sound((soundAsset + "s1_p6(2).ogg"));

var soundArray = [s1_p1, s1_p2, s1_p3, s1_p4, s1_p5, s1_p6];
var currentSound =s1_p1;

var content=[
    {
        // slide0
        contentnocenteradjust: true,
        uppertextblock:[{
            textclass: "covering_topic1",
            textdata: data.string.p1_s0
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass: "coverpage",
                imgsrc: imgpath+ "mahakali_bridge.png"
            }]
        }]
    },
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p1_s0_title
		}, {
			textclass: "totallength",
			textdata: data.string.p1_s7
		}, {
			textclass: "additional_info",
			textdata: data.string.p1_s8
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "the_bridge.jpg"
			},{
				imgclass: "bridge1",
				imgsrc: imgpath+ "bridge_1.jpg"
			},{
				imgclass: "bridge2",
				imgsrc: imgpath+ "bridge_2.jpg"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p1_s1
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p1_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "batuwa_on_the_bridge.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p1_s2
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p1_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge scaledown",
				imgsrc: imgpath+ "the_bridge.jpg"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock2",
		lowertextblock:[{
			textdata: data.string.p1_s3
		},{
			tripleinputfields: true
		},{
			textdata: data.string.p1_s3_ans
		},{
			textdata: data.string.p1_s4
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p1_s0_title
		}],
		imageblock:[{
			imageblockclass: "foursegments",
			imagestoshow:[{
				imgclass: "bridge_width_four",
				imgsrc: imgpath+ "the_bridge_foursegs.png"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel_info fadein1",
				imagelabeldata: data.string.p1_s5
			},{
				imagelabelclass: "imagelabel_number1_width fadein2",
				imagelabeldata: "1"
			},{
				imagelabelclass: "imagelabel_number2_width fadein3",
				imagelabeldata: "2"
			},{
				imagelabelclass: "imagelabel_number3_width fadein4",
				imagelabeldata: "3"
			},{
				imagelabelclass: "imagelabel_number4_width fadein5",
				imagelabeldata: "4"
			}]
		},{
			imageblockclass: "onesegment",
			imagestoshow:[{
				imgclass: "bridge_width_one fadein6",
				imgsrc: imgpath+ "the_bridge_oneseg.png"
			},{
				imgclass: "bridge_width_one_line fadein7",
				imgsrc: imgpath+ "mark02.png"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel_info fadein6",
				imagelabeldata: data.string.p1_s9
			},{
				imagelabelclass: "imagelabel_single_width fadein7",
				imagelabeldata: "363 m"
			}]
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p1_s0_title
		}],
		imageblock:[{
			imageblockclass: "foursegments",
			imagestoshow:[{
				imgclass: "bridge_width_four",
				imgsrc: imgpath+ "the_bridge_foursegs.png"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel_info fadein1",
				imagelabeldata: data.string.p1_s6
			},{
				imagelabelclass: "imagelabel_number1_height fadein2",
				imagelabeldata: "1"
			},{
				imagelabelclass: "imagelabel_number2_height fadein3",
				imagelabeldata: "2"
			},{
				imagelabelclass: "imagelabel_number3_height fadein4",
				imagelabeldata: "3"
			},{
				imagelabelclass: "imagelabel_number4_height fadein5",
				imagelabeldata: "4"
			},{
				imagelabelclass: "imagelabel_number5_height fadein6",
				imagelabeldata: "5"
			},{
				imagelabelclass: "imagelabel_number6_height fadein7",
				imagelabeldata: "6"
			},{
				imagelabelclass: "imagelabel_number7_height fadein8",
				imagelabeldata: "7"
			},{
				imagelabelclass: "imagelabel_number8_height fadein9",
				imagelabeldata: "8"
			}]
		},{
			imageblockclass: "onesegment",
			imagestoshow:[{
				imgclass: "bridge_width_one fadein10",
				imgsrc: imgpath+ "the_bridge_oneseg.png"
			},{
				imgclass: "bridge_height_one_line fadein11",
				imgsrc: imgpath+ "mark01.png"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel_info fadein10",
				imagelabeldata: data.string.p1_s10
			},{
				imagelabelclass: "imagelabel_single_height fadein11",
				imagelabeldata: "32.72 m"
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
				// if(countNext == $total_page - 1)
					// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 1000);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		switch(countNext){
			case 0:
        $nextBtn.hide();
				$(".coverpage").css("width","101%");
        soundPlayer(s1_p1, 1);
				// s1_p1.play().bind("ended", function(){
        //             $nextBtn.show(0);
        //         });
				break;
			case 1:
				var value = 0;
				var $length = $(".length");
				var interval;
				$(".totallength").append("<img class='arrowpointing' src='"+imgpath+"/arrow.png'>");

				setTimeout(function(){
					$(".bridge").addClass("animatebridge");
					interval = setInterval(function(){
								value += 7;
								if(value < 1453){
									$length.html(value);
								}else{
									$length.html(1453);
									clearInterval(interval);
								}
							}, 100);
				}, 1600);
				$nextBtn.hide(0);
				$(".bridge1, .bridge2").hide(0);
				setTimeout(function(){
					sound_ting.play();
					$(".bridge1, .bridge2").delay(2000).show(0);
					$(".arrowpointing").show(0);
					$(".additional_info").show(0);
					sound_ting.bind("ended", function(){
						$nextBtn.delay(1500).show(0);
					});
				}, 22600);

        soundPlayer(soundArray[countNext], 0);
				break;
			case 2:
      $nextBtn.hide(0);
			// $nextBtn.show(0);
        soundPlayer(soundArray[countNext], 1);
			break;
			case 3:
				$nextBtn.hide(0);
				var p = $(".lowertextblock2 > p");
				setTimeout(function(){
					$(p[0]).show(0);
					$(".inputcontainer").show(0);
          soundPlayer(soundArray[countNext], 0);
				}, 5100);

				$(".hint").click(function(){
					$(p[2]).show(0);
				});

				$(".check").click(function(){
					var input = parseFloat($(".input_value").val());
					if(input >= 20 && input <= 25){
						$(".input_value").removeClass("incorrect").addClass("correct");
						$(p[1]).show(0);
						$nextBtn.show(0);
						$(".input_value").attr('disabled','disabled');
					}else{
						$(".input_value").addClass("incorrect");
					}
				});

				$(".input_value").keydown(function(evt) {
					var charVal = parseInt(evt.key);
					var prevValue = parseInt(evt.target.value) * 10 + charVal;
					var charCode = (evt.which) ? evt.which : evt.keyCode;
					if(charCode == 110 || charCode == 190){
						if((evt.target.value).indexOf(".") == -1){
							return true;
						}
					}
					if ((charCode > 31 && (charCode < 48 || charCode > 57)) && isNaN(charVal)) {
						// console.log("inside");
						// console.log(evt.target.value);
						return false;
					}

					if (prevValue > 9999) {
						return false;
					}
					return true;
				});

				break;
			case 4:
				$nextBtn.hide(0);
				// $nextBtn.delay(8000).show(0);
        soundPlayer(soundArray[countNext], 1);
        currentSound.stop();
        currentSound = s1_p5;
        currentSound.play();
        currentSound.bind("ended", function(){
          currentSound.stop();
          currentSound = s1_p5_1;
          currentSound.play();
          currentSound.bind("ended", function(){
            // $nextBtn.show(0);
          });
        });
				break;
			case 5:
				// setTimeout(function(){
				// 	if(countNext == $total_page - 1)
				// 		ole.footerNotificationHandler.pageEndSetNotification();
				// }, 12000);
        currentSound.stop();
        currentSound = s1_p6;
        currentSound.play();
        currentSound.bind("ended", function(){
          setTimeout(function(){
            currentSound.stop();
            currentSound = s1_p6_1;
            currentSound.play();
            currentSound.bind("ended", function(){
              // $nextBtn.show(0);
  						ole.footerNotificationHandler.pageEndSetNotification();
            });
          }, 9500);
        });
				break;
			default:
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

  function soundPlayer (soundId, showNextBtn){
    currentSound = soundId;
    currentSound.play();
    currentSound.bind("ended", function(){
      if (showNextBtn)
        countNext<5?$nextBtn.show():ole.footerNotificationHandler.pageEndSetNotification();
    });
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generaltemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
    currentSound.stop();
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

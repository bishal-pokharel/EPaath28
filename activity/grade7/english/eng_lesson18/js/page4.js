var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var s4_p1 = new buzz.sound((soundAsset + "s4_p1.ogg"));
var s4_p2 = new buzz.sound((soundAsset + "s4_p2.ogg"));
var s4_p3 = new buzz.sound((soundAsset + "s4_p3(1).ogg"));
var s4_p3_1 = new buzz.sound((soundAsset + "s4_p3(2).ogg"));
var s4_p4 = new buzz.sound((soundAsset + "s4_p4(1).ogg"));
var s4_p4_1 = new buzz.sound((soundAsset + "s4_p4(2).ogg"));
var s4_p5 = new buzz.sound((soundAsset + "s4_p5(1).ogg"));
var s4_p5_1 = new buzz.sound((soundAsset + "s4_p5(2)_retake.ogg"));
var s4_p6 = new buzz.sound((soundAsset + "s4_p6(1)_retake.ogg"));
var s4_p6_1 = new buzz.sound((soundAsset + "s4_p6(2)_retake.ogg"));
var s4_p7 = new buzz.sound((soundAsset + "s4_p7(1).ogg"));
var s4_p7_1 = new buzz.sound((soundAsset + "s4_p7(2).ogg"));
var s4_p8 = new buzz.sound((soundAsset + "s4_p8(1).ogg"));
var s4_p8_1 = new buzz.sound((soundAsset + "s4_p8(2).ogg"));
var s4_p9 = new buzz.sound((soundAsset + "s4_p9.ogg"));
var s4_p10 = new buzz.sound((soundAsset + "s4_p10.ogg"));
var s4_p11 = new buzz.sound((soundAsset + "s4_p11.ogg"));
var s4_p12 = new buzz.sound((soundAsset + "s4_p12.ogg"));

var soundArray = [s4_p1, s4_p2, s4_p3, s4_p4, s4_p5, s4_p6,
									s4_p7, s4_p8, s4_p9, s4_p10, s4_p11, s4_p12];
var currentSound =s4_p1;

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "kalapani_nepal.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s2
		}]
	},
	{
		// slide1
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "worldmap.png"
			},{
				imgclass: "bridge fadein0",
				imgsrc: imgpath+ "worldmap-amazon.png"
			},{
				imgclass: "bridge fadein1",
				imgsrc: imgpath+ "worldmap-nile.png"
			},{
				imgclass: "bridge fadein2",
				imgsrc: imgpath+ "worldmap-yangtze.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textclass: "textalignleft",
			textdata: data.string.p4_s3
		},{
			textclass: "textalignleft fadein0",
			textdata: data.string.p4_s5
		},{
			textclass: "textalignleft fadein1",
			textdata: data.string.p4_s4
		},{
			textclass: "textalignleft fadein2",
			textdata: data.string.p4_s6
		}]
	},
	{
		// slide2
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s7
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "police04 position1_2",
				imgsrc: imgpath+ "tree.png"
			},{
				imgclass: "police04 position2_2 tree",
				imgsrc: imgpath+ "tallest-tree.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s8
		},{
			findanswer: true
		},{
			textdata: data.string.p4_s8_ans
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s7
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "police04 position1_2",
				imgsrc: imgpath+ "dharara.png"
			},{
				imgclass: "police04 position2_2",
				imgsrc: imgpath+ "burj_khalifa.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s9
		},{
			findanswer: true
		},{
			textdata: data.string.p4_s9_ans
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s7
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "challenger_deep.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s10
		},{
			findanswer: true
		},{
			textdata: data.string.p4_s10_ans
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s7
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "challenger_deep.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s10_ans1
		},{
			findanswer: true
		},{
			textdata: data.string.p4_s10_ans2
		},{
			textdata: data.string.p4_s10_ans3
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s7
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "fearofhights.jpg"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s11
		},{
			findanswer: true
		},{
			textdata: data.string.p4_s12,
			datahighlightflag: true

		}]
	},{
		// slide7
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s7
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "tallest_suspension_bridge_in-nepal.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s13
		},{
			findanswer: true
		},{
			textdata: data.string.p4_s14,
			datahighlightflag: true
		},{
			textdata: data.string.p4_s15
		},{
			textdata: data.string.p4_s16
		}]
	},{
		// slide8
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s17
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "brain1",
				imgsrc: imgpath+ "brain.png"
			},{
				imgclass: "brain2",
				imgsrc: imgpath+ "humanbody.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s18
		},{
			textdata: data.string.p4_s19
		}]
	},{
		// slide9
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s17
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "brain1",
				imgsrc: imgpath+ "brain.png"
			},{
				imgclass: "brain2",
				imgsrc: imgpath+ "humanbody.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock2",
		lowertextblock:[{
			textdata: data.string.p4_s18
		},{
			textdata: data.string.p4_s20
		},{
			doubleinputfield: true
		},{
			textdata: data.string.p4_s21
		}]
	},{
		// slide10
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s17
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "body.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p4_s22
		},{
			textdata: data.string.p4_s23
		}]
	},{
		// slide11
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p4_s0_title + data.string.p4_s1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car2",
				imgsrc: imgpath+ "body.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock2",
		lowertextblock:[{
			textdata: data.string.p4_s22
		},{
			textdata: data.string.p4_s24
		},{
			doubleinputfield: true
		},{
			textdata: data.string.p4_s25
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
				// if(countNext == $total_page - 1)
					// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 1000);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		switch(countNext){
			case 0:
			$nextBtn.hide(0);
			// $nextBtn.delay(2000).show(0);
				soundPlayer(soundArray[countNext], 1);
				break;
			case 1:
			$nextBtn.hide(0);
			// $nextBtn.delay(2000).show(0);
				soundPlayer(soundArray[countNext], 1);
				break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				$nextBtn.hide(0);
					soundPlayer(soundArray[countNext], 0);
				$(".lowertextblock>p:nth-of-type(2)").css('visibility', 'hidden');
				$(".lowertextblock>p:nth-of-type(3)").css('visibility', 'hidden');
				$(".ans").click(function(){
					if(countNext == 2){
						$(".position1_2").addClass("scaledown_anim");
						$(".position2_2").delay(2000).show(0);
					}else if(countNext == 3){
						$(".position1_2").addClass("scaledown_anim2");
						$(".position2_2").delay(2000).show(0);
					}
					$(".lowertextblock>p:nth-of-type(2)").css('visibility', 'visible');
					$(".lowertextblock>p:nth-of-type(3)").css('visibility', 'visible');
					countNext==2?soundPlayer(s4_p3_1, 1):
					countNext==3?soundPlayer(s4_p4_1, 1):
					countNext==4?soundPlayer(s4_p5_1, 1):
					countNext==5?soundPlayer(s4_p6_1, 1):
					countNext==6?soundPlayer(s4_p7_1, 1):'';

					// $nextBtn.delay(1500).show(0);
				});
				break;
			case 7:
				$nextBtn.hide(0);
				$(".lowertextblock>p:nth-of-type(2)").css('visibility', 'hidden');
				$(".lowertextblock>p:nth-of-type(3)").css('visibility', 'hidden');
				$(".lowertextblock>p:nth-of-type(4)").css('visibility', 'hidden');
				$(".ans").click(function(){
					$(".lowertextblock>p:nth-of-type(2)").css('visibility', 'visible');
					$(".lowertextblock>p:nth-of-type(3)").css('visibility', 'visible');
					$(".lowertextblock>p:nth-of-type(4)").css('visibility', 'visible');
					// $nextBtn.show(0);
					soundPlayer(s4_p8_1, 1);
				});
					soundPlayer(soundArray[countNext], 0);
				break;
				break;
			case 8:
				$nextBtn.hide(0);
				soundPlayer(soundArray[countNext], 1);
				break;
			case 9:
			case 11:
				var $p = $(".lowertextblock2 > p");
				var $finalanswer = $($p[2]);
				$finalanswer.hide(0);
				$nextBtn.hide(0);
				$(".check").click(function(){
					var input = parseFloat($(".input_value").val());
					if(input > 0){
						$(".input_value").removeClass("incorrect").addClass("correct");
						$finalanswer.show(0);
						if(countNext == 11){
							$(".calculated_weight").html((input*0.4).toFixed(3));
							ole.footerNotificationHandler.lessonEndSetNotification();
						}else{
							$(".calculated_weight").html((input*0.02).toFixed(3));
							$nextBtn.show(0);
						}
					}else{
						$(".input_value").addClass("incorrect");
					}
				});

				$(".input_value").keydown(function(evt) {
					var charVal = parseInt(evt.key);
					var prevValue = parseInt(evt.target.value) * 10 + charVal;
					var charCode = (evt.which) ? evt.which : evt.keyCode;
					if(charCode == 110 || charCode == 190){
						if((evt.target.value).indexOf(".") == -1){
							return true;
						}
					}
					if ((charCode > 31 && (charCode < 48 || charCode > 57)) && isNaN(charVal)) {
						// console.log("inside");
						// console.log(evt.target.value);
						return false;
					}

					if (prevValue > 9999999) {
						return false;
					}
					return true;
				});
					soundPlayer(soundArray[countNext], 0);
				break;
			default:
			// navigationcontroller();
			$nextBtn.hide(0);
				soundPlayer(soundArray[countNext], 1);
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/

function soundPlayer (soundId, showNextBtn){
	currentSound.stop();
	currentSound = soundId;
	currentSound.play();
	currentSound.bind("ended", function(){
		if (showNextBtn)
			countNext<11?$nextBtn.show():ole.footerNotificationHandler.pageEndSetNotification();
	});
}
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generaltemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		currentSound.stop();
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		currentSound.stop();
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

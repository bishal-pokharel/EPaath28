var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_ting = new buzz.sound((soundAsset + "ding.ogg"));
var s2_p1 = new buzz.sound((soundAsset + "s2_p1.ogg"));
var s2_p2 = new buzz.sound((soundAsset + "s2_p2.ogg"));
var s2_p3 = new buzz.sound((soundAsset + "s2_p3(incorrect).ogg"));
var s2_p4 = new buzz.sound((soundAsset + "s2_p4(incorrect).ogg"));
var s2_p5 = new buzz.sound((soundAsset + "s2_p5.ogg"));
var s2_p6 = new buzz.sound((soundAsset + "s2_p6.ogg"));
var s2_p7 = new buzz.sound((soundAsset + "s2_p7 (Incomplete).ogg"));
var s2_p8 = new buzz.sound((soundAsset + "s2_p8.ogg"));
var s2_p9 = new buzz.sound((soundAsset + "s2_p9.ogg"));
var s2_p10 = new buzz.sound((soundAsset + "s2_p10.ogg"));
var s2_p11 = new buzz.sound((soundAsset + "s2_p11.ogg"));
var s2_p12 = new buzz.sound((soundAsset + "s2_p12.ogg"));

var soundArray = [s2_p1, s2_p2, s2_p3, s2_p4, s2_p5, s2_p6,
									s2_p7, s2_p8, s2_p9, s2_p10, s2_p11, s2_p12];
var currentSound =s2_p1;

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "bridge_1.jpg"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s1
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "dodhara-bridge.gif"
			}],
			imagelabels: [{
				imagelabelclass: "dodhara_label",
				imagelabeldata: data.string.p2_s18
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s2
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "newmap01.png"
			},{
				imgclass: "mahendranagar_pointer",
				imgsrc: imgpath+ "markred.png"
			}],
			imagelabels: [{
				imagelabelclass: "mahendranagar_label",
				imagelabeldata: data.string.p2_s19
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s4
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "newmap01.png"
			},{
				imgclass: "dam_pointer",
				imgsrc: imgpath+ "markgreen.png"
			}],
			imagelabels: [{
				imagelabelclass: "dam_label",
				imagelabeldata: data.string.p2_s20
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s5
		},{
			textdata: data.string.p2_s6
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "newmap01.png"
			},{
				imgclass: "bridge",
				imgsrc: imgpath+ "path01.png"
			},{
				imgclass: "mahendranagar_pointer",
				imgsrc: imgpath+ "markred.png"
			},{
				imgclass: "dam_pointer",
				imgsrc: imgpath+ "markgreen.png"
			}],
			imagelabels: [{
				imagelabelclass: "mahendranagar_label",
				imagelabeldata: data.string.p2_s19
			},{
				imagelabelclass: "dam_label",
				imagelabeldata: data.string.p2_s20
			},{
				imagelabelclass: "dodhara_label",
				imagelabeldata: data.string.p2_s18
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s3
		},{
			textdata: data.string.p2_s3_add
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "newmap01.png"
			},{
				imgclass: "bridge",
				imgsrc: imgpath+ "path02.png"
			},{
				imgclass: "mahendranagar_pointer",
				imgsrc: imgpath+ "markred.png"
			}],
			imagelabels: [{
				imagelabelclass: "mahendranagar_label",
				imagelabeldata: data.string.p2_s19
			},{
				imagelabelclass: "dodhara_label",
				imagelabeldata: data.string.p2_s18
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s21
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge2",
				imgsrc: imgpath+ "map-of-nepal.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock2",
		lowertextblock:[{
			textdata: data.string.p2_s7
		},{
			textdata: data.string.p2_s8
		},{
			tripleinputfields: true
		},{
			textdata: data.string.p2_s9_add
		},{
			textdata: data.string.p2_s9
		}]
	},{
		// slide7
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge3",
				imgsrc: imgpath+ "eastwesthighway.png"
			},{
				imgclass: "mahendranagar_pointer2",
				imgsrc: imgpath+ "markred.png"
			}],
			imagelabels: [{
				imagelabelclass: "mahendranagar_label2",
				imagelabeldata: data.string.p2_s19
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s10
		}]
	},{
		// slide8
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge2",
				imgsrc: imgpath+ "eastwesthighway.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock2",
		lowertextblock:[{
			textdata: data.string.p2_s11
		},{
			textclass: "yes option",
			textdata: data.string.p2_s12
		},{
			textclass: "option",
			textdata: data.string.p2_s13
		},{
			textclass: "option",
			textdata: data.string.p2_s14
		},{
			textclass: "option",
			textdata: data.string.p2_s15
		}]
	},{
		// slide9
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge3",
				imgsrc: imgpath+ "eastwesthighway.png"
			},{
				imgclass: "bridge3",
				imgsrc: imgpath+ "midwesthighway.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s16
		}]
	},{
		// slide10
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge3",
				imgsrc: imgpath+ "eastwesthighway.png"
			},{
				imgclass: "bridge3",
				imgsrc: imgpath+ "midwesthighway.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock2",
		lowertextblock:[{
			textdata: data.string.p2_s24
		},{
			textclass: "yes option",
			textdata: data.string.p2_s25
		},{
			textclass: "option",
			textdata: data.string.p2_s26
		},{
			textclass: "option",
			textdata: data.string.p2_s27
		},{
			textclass: "option",
			textdata: data.string.p2_s28
		}]
	},{
		// slide11
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p2_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge4",
				imgsrc: imgpath+ "hillyroad.jpg"
			},{
				imgclass: "bridge5",
				imgsrc: imgpath+ "terairoad.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "imagelabel_info1",
				imagelabeldata: data.string.p2_s22
			},{
				imagelabelclass: "imagelabel_info2",
				imagelabeldata: data.string.p2_s23
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p2_s17
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				if(countNext == $total_page - 1)
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 1000);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		switch(countNext){
			case 0:
			case 2:
			case 3:
			case 4:
			case 5:
			$nextBtn.hide(0);
				soundPlayer(soundArray[countNext], 1);
				// $nextBtn.hide(0).delay(1500).show(0);
				break;
			case 1:
				$(".dodhara_label").hide(0);
				$nextBtn.hide(0);
				setTimeout(function(){
					$(".dodhara_label").show(0);
					// $nextBtn.show(0);
				}, 2500);
				soundPlayer(soundArray[countNext], 1);
				break;
			case 6:
				$nextBtn.hide(0);
				var p = $(".lowertextblock2 > p");
				$(p[0]).show(0);
				setTimeout(function(){
					$(p[1]).show(0);
					$(".inputcontainer").show(0);
				}, 1000);

				$(".hint").click(function(){
					$(p[3]).show(0);
				});

				$(".check").click(function(){
					var input = parseFloat($(".input_value").val());
					if(input >= 15 && input <= 20){
						$(".input_value").removeClass("incorrect").addClass("correct");
						$(p[2]).show(0);
						$nextBtn.show(0);
					}else{
						$(".input_value").addClass("incorrect");
					}
				});

				$(".input_value").keydown(function(evt) {
					var charVal = parseInt(evt.key);
					var prevValue = parseInt(evt.target.value) * 10 + charVal;
					var charCode = (evt.which) ? evt.which : evt.keyCode;
					if(charCode == 110 || charCode == 190){
						if((evt.target.value).indexOf(".") == -1){
							return true;
						}
					}
					if ((charCode > 31 && (charCode < 48 || charCode > 57)) && isNaN(charVal)) {
						// console.log("inside");
						// console.log(evt.target.value);
						return false;
					}

					if (prevValue > 9999) {
						return false;
					}
					return true;
				});
				soundPlayer(soundArray[countNext], 0);
			 	break;
			 case 7:
			 $nextBtn.hide(0);
			 	soundPlayer(soundArray[countNext], 1);
			 	break;
			 case 8:
			 case 10:
			 $(".lowertextblock2>p").show(0);
			 var $p = $(".lowertextblock2>p");
			 var $ltb = $(".lowertextblock2");
			 $ltb.html("");
			 $ltb.html($p[0]);
			 $p.splice(0, 1);
			 var random_idx = 0;
			 while ($p.length > 1){
			 	random_idx = Math.floor(Math.random() * $p.length);
			 	$ltb.append($p[random_idx]);
			 	$p.splice(random_idx, 1);
			 }
			 $ltb.append($p[0]);

			 	var answered = false;
			 	$nextBtn.hide(0);
			 	$(".option").click(function(){
			 		if(answered){
			 			return answered;
			 		}
			 		var $this = $(this);
			 		if($this.hasClass("yes")){
			 			$this.addClass("correct");
			 			answered = true;
			 			$nextBtn.show(0);
			 		} else {
			 			$this.addClass("incorrect");
			 		}
			 	});
 			 	soundPlayer(soundArray[countNext], 0);
			 	break;
			 case 9:
			 	$nextBtn.hide(0);
			 	var $span = $(".lowertextblock span");
			 	for (var i = 0; i < $span.length; i++){
			 		$($span[i]).addClass("fadein"+i);
			 	}
 			 	soundPlayer(soundArray[countNext], 1);
			 	break;
			default:
				// countNext<11?$nextBtn.show():ole.footerNotificationHandler.pageEndSetNotification();
					soundPlayer(soundArray[countNext], 1);
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/
function soundPlayer (soundId, showNextBtn){
	currentSound = soundId;
	currentSound.play();
	currentSound.bind("ended", function(){
		if (showNextBtn)
			countNext<11?$nextBtn.show():ole.footerNotificationHandler.pageEndSetNotification();
	});
}
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generaltemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		currentSound.stop();
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

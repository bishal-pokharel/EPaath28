var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";
var s3_p1 = new buzz.sound((soundAsset + "s3_p1.ogg"));
var s3_p2 = new buzz.sound((soundAsset + "s3_p2.ogg"));
var s3_p3 = new buzz.sound((soundAsset + "s3_p3(1).ogg"));
var s3_p3_1 = new buzz.sound((soundAsset + "s3_p3(2).ogg"));
var s3_p4 = new buzz.sound((soundAsset + "s3_p4(1).ogg"));
var s3_p4_1 = new buzz.sound((soundAsset + "s3_p4(2).ogg"));
var s3_p5 = new buzz.sound((soundAsset + "s3_p5(1).ogg"));
var s3_p5_1 = new buzz.sound((soundAsset + "s3_p5(2).ogg"));
var s3_p6 = new buzz.sound((soundAsset + "s3_p6(1).ogg"));
var s3_p6_1 = new buzz.sound((soundAsset + "s3_p6(2).ogg"));
var s3_p7 = new buzz.sound((soundAsset + "s3_p7.ogg"));
var s3_p8 = new buzz.sound((soundAsset + "s3_p8.ogg"));
var s3_p9 = new buzz.sound((soundAsset + "s3_p9(1).ogg"));
var s3_p9_1 = new buzz.sound((soundAsset + "s3_p9(2)incorrect.ogg"));
var s3_p10 = new buzz.sound((soundAsset + "s3_p10(1)incorrect).ogg"));
var s3_p10_1 = new buzz.sound((soundAsset + "s3_p10(2).ogg"));
//
var soundArray = [s3_p1, s3_p2, s3_p3, s3_p4, s3_p5, s3_p6,
									s3_p7, s3_p8, s3_p9, s3_p10];
var currentSound =s3_p1;

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "cali_bridge.gif"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p3_s1
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "batuwa_on_the_bridge.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p3_s2
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "car.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p3_s3
		},{
			findanswer: true
		},{
			textdata: data.string.p3_s4
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "motorbike.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p3_s5
		},{
			findanswer: true
		},{
			textdata: data.string.p3_s6
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "cycle.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p3_s7
		},{
			findanswer: true
		},{
			textdata: data.string.p3_s8
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "police04",
				imgsrc: imgpath+ "police04.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p3_s9
		},{
			findanswer: true
		},{
			textdata: data.string.p3_s10
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imageblockclass: "container1 fadein1",
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "car.png"
			}],
			imagelabels: [{
				imagelabelclass: "imagelabel_info",
				imagelabeldata: data.string.p3_s4_add
			}]
		},{
			imageblockclass: "container2 fadein2",
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "motorbike.png"
			}],
			imagelabels: [{
				imagelabelclass: "imagelabel_info",
				imagelabeldata:data.string.p3_s6_add
			}]
		},{
			imageblockclass: "container3 fadein3",
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "cycle.png"
			}],
			imagelabels: [{
				imagelabelclass: "imagelabel_info",
				imagelabeldata:data.string.p3_s8_add
			}]
		},{
			imageblockclass: "container4 fadein4",
			imagestoshow:[{
				imgclass: "police04",
				imgsrc: imgpath+ "police04.png"
			}],
			imagelabels: [{
				imagelabelclass: "imagelabel_info",
				imagelabeldata:data.string.p3_s10_add
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock fadein5",
		lowertextblock:[{
			textdata: data.string.p3_s11
		}]
	},{
		// slide7
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge",
				imgsrc: imgpath+ "the_bridge_oneseg.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p3_s12
		}]
	},{
		// slide8
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "bridge2",
				imgsrc: imgpath+ "the_bridge_oneseg.png"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock2",
		lowertextblock:[{
			textdata: data.string.p3_s13
		},{
			textdata: data.string.p3_s14
		},{
			findanswer: true
		},{
			textdata: data.string.p3_s18
		},{
			textdata: data.string.p3_s15
		}]
	},{
		// slide9
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "covering_topic",
			textdata: data.string.p3_s0_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "car1",
				imgsrc: imgpath+ "truck-n-bridge-width.jpg"
			}]
		}],
		lowertextblockadditionalclass: "lowertextblock",
		lowertextblock:[{
			textdata: data.string.p3_s16
		},{
			findanswer: true
		},{
			textdata: data.string.p3_s17
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
				// if(countNext == $total_page - 1)
					// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 1000);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		switch(countNext){
			case 0:
			case 1:
			// $nextBtn.hide(0).delay(2000).show(0);
				$nextBtn.hide(0);
			 	var $span = $(".lowertextblock span");
			 	for (var i = 0; i < $span.length; i++){
			 		$($span[i]).addClass("fadein_txt"+i);
			 	}
				soundPlayer(soundArray[countNext], 1);
				break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 9:
				$nextBtn.hide(0);
				$(".lowertextblock>p:nth-of-type(2)").css('visibility', 'hidden');
				$(".ans").click(function(){
					$(".lowertextblock>p:nth-of-type(2)").css('visibility', 'visible');
					countNext==2?soundPlayer(s3_p3_1, 1):
					countNext==3?soundPlayer(s3_p4_1, 1):
					countNext==4?soundPlayer(s3_p5_1, 1):
					countNext==5?soundPlayer(s3_p6_1, 1):
					countNext==9?soundPlayer(s3_p10_1, 1):'';

				// 	if(countNext == 9){
				// 		setTimeout(function(){
				// 			ole.footerNotificationHandler.pageEndSetNotification();
				// 		}, 1500);
				// 	}else{
				// 		$nextBtn.delay(1500).show(0);
				// 	}
				});
				soundPlayer(soundArray[countNext], 0);
				break;
			case 6:
				$nextBtn.hide(0);
				var $label = $(".imagelabel_info");
				setTimeout(function(){
					// $nextBtn.delay(1500).show(0);
					soundPlayer(soundArray[countNext], 1);
					$($label[0]).addClass("imagelabel_info2");
					$($label[2]).addClass("imagelabel_info2");
				}, 5000);
				break;
			case 7:
				$nextBtn.hide(0);
					soundPlayer(soundArray[countNext], 1);
				break;
			case 8:
				var $p = $(".lowertextblock2 > p");
				var $hint = $($p[2]);
				var $finalanswer = $($p[3]);
				$hint.hide(0);
				$nextBtn.hide(0);
				$finalanswer.hide(0);
				$(".ans").click(function(){
					$hint.show(0);
					$finalanswer.delay(1000).show(0);
					soundPlayer(s3_p9_1, 1);
					// $nextBtn.delay(2000).show(0);
				});
						soundPlayer(soundArray[countNext], 0);
				break;
			default:
				navigationcontroller();
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/
function soundPlayer (soundId, showNextBtn){
	currentSound.stop();
	currentSound = soundId;
	currentSound.play();
	currentSound.bind("ended", function(){
		if (showNextBtn)
			countNext<9?$nextBtn.show():ole.footerNotificationHandler.pageEndSetNotification();
	});
}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generaltemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		currentSound.stop();
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		currentSound.stop();
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

$(document).ready(function() {
  var dialog0 = new buzz.sound($ref + "/audio/dialog0.ogg");
  var dialog1 = new buzz.sound($ref + "/audio/dialog1.ogg");
  var dialog2 = new buzz.sound($ref + "/audio/dialog3.ogg");
  var dialog3 = new buzz.sound($ref + "/audio/dialog4.ogg");
  var dialog4 = new buzz.sound($ref + "/audio/dialog5.ogg");

  $("#showdil").click(function() {
    $("#showdil").hide(0);
    $("#start").hide(0);
    $("#start").trigger("click");
  });
  $("#start").click(function() {
    loadTimelineProgress(5, 1);
    $nextBtn.hide(0);
    $prevBtn.hide(0);
    $("#showdil").hide(0);
    $("#start").hide(0);

    $("#diImg1").fadeIn(1500);
    $("#diag1").fadeIn(1500);
    dialog0.play();
    dialog0.bind("ended", function() {
      $nextBtn.show(0);
    });
  });

  var count = -1;
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");

  $nextBtn.hide(0);
  $prevBtn.hide(0);

  $prevBtn.click(function() {
    count--;
    ole.footerNotificationHandler.hideNotification();
    if (count == 0) {
      $("#start").trigger("click");
    } else {
      tracker();
    }
  });

  $nextBtn.click(function() {
    count++;
    if (count == 0) {
      $("#start").trigger("click");
    } else {
      console.log(count);
      tracker();
    }
  });

  function tracker() {
    loadTimelineProgress(5, count + 1);
    switch (count) {
      case 1:
        $nextBtn.hide(0);
        $prevBtn.hide(0);
        $("#diag1").hide(0);
        $("#diImg1").hide(0);
        $("#diImg2").fadeIn(500);
        $("#diag2").fadeIn(500);
        dialog1.play();
        dialog1.bind("ended", function() {
          $nextBtn.show(0);
          $prevBtn.show(0);
        });
        break;
      case 2:
        $nextBtn.hide(0);
        $prevBtn.hide(0);
        $("#diag2").hide(0);
        $("#diImg2").hide(0);
        $("#diImg3").fadeIn(500);
        $("#diag3").fadeIn(500);
        dialog2.play();
        dialog2.bind("ended", function() {
          $nextBtn.show(0);
          $prevBtn.show(0);
        });
        break;
      case 3:
        $nextBtn.hide(0);
        $prevBtn.hide(0);
        $("#diag3").hide(0);
        $("#diImg3").hide(0);
        $("#diImg4").fadeIn(500);
        $("#diag4").fadeIn(500);
        dialog3.play();
        dialog3.bind("ended", function() {
          $nextBtn.show(0);
          $prevBtn.show(0);
        });
        break;
      case 4:
        $nextBtn.hide(0);
        $prevBtn.hide(0);
        $("#diag4").hide(0);
        $("#diImg4").hide(0);
        $("#diImg5").fadeIn(500);
        $("#diag5").fadeIn(500);

        dialog4.play();
        dialog4.bind("ended", function() {
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        });
        break;

      default:
        break;
    }
  }

  $("#start").hide(0);
  $nextBtn.trigger("click");

  // $("#start").click(function() {
  // lesson1_Dialouge.play();
  // });
});

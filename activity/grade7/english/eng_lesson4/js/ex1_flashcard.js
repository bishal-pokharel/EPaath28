$(".title").text(data.string.title);
$(document).ready(function() {
  $(".title").text(data.string.exerciseTitle2);
  var content = [
    {
      words: data.string.word1,
      description: data.string.des1,
      img: $ref + "/exercise_image/flu.png"
    }, //
    {
      words: data.string.word2,
      description: data.string.des2,
      img: $ref + "/exercise_image/participate.png"
    }, //

    {
      words: data.string.word3,
      description: data.string.des3,
      img: $ref + "/exercise_image/perform.png"
    }, //

    {
      words: data.string.word4,
      description: data.string.des4,
      img: $ref + "/exercise_image/practice.png"
    }, //
    {
      words: data.string.word5,
      description: data.string.des5,
      img: $ref + "/exercise_image/international.png"
    }, //
    {
      words: data.string.word6,
      description: data.string.des6,
      img: $ref + "/exercise_image/argentina.png"
    }, //
    {
      words: data.string.word7,
      description: data.string.des7,
      img: $ref + "/exercise_image/nightmare.png"
    }, //
    {
      words: data.string.word8,
      description: data.string.des8,
      img: $ref + "/exercise_image/defender.png"
    }, //
    {
      words: data.string.word9,
      description: data.string.des9,
      img: $ref + "/exercise_image/glide.png"
    }, //
    {
      words: data.string.word10,
      description: data.string.des10,
      img: $ref + "/exercise_image/easy.png"
    }, //
    {
      words: data.string.word11,
      description: data.string.des11,
      img: $ref + "/exercise_image/afford.png"
    }, //
    {
      words: data.string.word12,
      description: data.string.des12,
      img: $ref + "/exercise_image/rags.png"
    }, //
    {
      words: data.string.word13,
      description: data.string.des13,
      img: $ref + "/exercise_image/brazil.png"
    }, //
    {
      words: data.string.word14,
      description: data.string.des14,
      img: $ref + "/exercise_image/sao.png"
    }, //
    {
      words: data.string.word15,
      description: data.string.des15,
      img: $ref + "/exercise_image/tournament.png"
    }, //
    {
      words: data.string.word16,
      description: data.string.des16,
      img: $ref + "/exercise_image/confident.png"
    }, //
    {
      words: data.string.word17,
      description: data.string.des17,
      img: $ref + "/exercise_image/inspiration.png"
    }, //
    {
      words: data.string.word18,
      description: data.string.des18,
      img: $ref + "/exercise_image/undeterred.png"
    }, //
    {
      words: data.string.word19,
      description: data.string.des19,
      img: $ref + "/exercise_image/rare.png"
    }, //
    {
      words: data.string.word20,
      description: data.string.des20,
      img: $ref + "/exercise_image/skill.png"
    }, //
    {
      words: data.string.word21,
      description: data.string.des21,
      img: $ref + "/exercise_image/odds.png"
    } //
  ];
  console.log(content);
  displaycontent();
  function displaycontent() {
    var words = $.each(content, function(key, value) {
      words = value.words;
      var refwords = value.words.toString().replace(" ", "");
      console.log(">>>>>>>>" + refwords);
      description = value.description;
      image = value.img;
      // console.log(description);
      appendTab = "<li>";
      appendTab +=
        '<a href="#' + refwords + '" data-toggle="tab"  >' + words + "</a>";
      appendTab += "</li>";
      $("#myTab").append(appendTab);

      appendcontent = '<div id="' + refwords + '" class="tab-pane ">';
      appendcontent += '<div class="f1_container">';
      appendcontent += '<div class="shadow f1_card">';
      appendcontent += '<div class="front face">';
      appendcontent += "<p>" + words + "</p>";
      appendcontent += "</div>";
      appendcontent += '<div class="back face center">';
      appendcontent +=
        '<img src="' + image + '" class="tab_image img-responsive"/>';
      appendcontent += '<p class="textroll">';
      appendcontent += description;
      appendcontent += "</p>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      $("#myTabContent").append(appendcontent);
    });
  }
  /*
   * function qA() { var source = $('#qA-templete').html(); var
   * template = Handlebars.compile(source); var html =
   * template(content); //$board.html(html); // console.log(html); }
   *
   * qA();
   */
});
$(document).ready(function() {
  loadTimelineProgress(1, 1);
  $(".f1_container").click(function() {
    //alert("ok");
    $(this).toggleClass("active");
  });
});

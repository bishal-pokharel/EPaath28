//$(document).ready(function(){
(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  $(".title").text(data.string.exerciseTitle);
  var content = [
    {
      question: data.string.q1,
      answers: [
        { ans: data.string.name1 },
        { ans: data.string.name2 },
        { ans: data.string.name3, correct: "correct" },
        { ans: data.string.name4 }
      ],
      img: $ref + "/exercise_image/flue.png"
    },
    {
      question: data.string.q2,
      answers: [
        { ans: data.string.name5 },
        { ans: data.string.name6 },
        { ans: data.string.name7, correct: "correct" },
        { ans: data.string.name8 }
      ],
      img: $ref + "/exercise_image/worldcup.jpg"
    },
    {
      question: data.string.q3,
      answers: [
        { ans: data.string.name9 },
        { ans: data.string.name10, correct: "correct" },
        { ans: data.string.name11 },
        { ans: data.string.name12 }
      ],
      img: $ref + "/exercise_image/football-match.jpg"
    },
    {
      question: data.string.q4,
      answers: [
        { ans: data.string.name13, correct: "correct" },
        { ans: data.string.name14 },
        { ans: data.string.name15 },
        { ans: data.string.name16 }
      ],
      img: $ref + "/exercise_image/nightmare.jpg"
    },
    {
      question: data.string.q5,
      answers: [
        { ans: data.string.name17 },
        { ans: data.string.name18 },
        { ans: data.string.name19, correct: "correct" },
        { ans: data.string.name20 }
      ],
      img: $ref + "/exercise_image/torn.png"
    },
    {
      question: data.string.q6,
      answers: [
        { ans: data.string.name21 },
        { ans: data.string.name22 },
        { ans: data.string.name23 },
        { ans: data.string.name24, correct: "correct" }
      ],
      img: $ref + "/exercise_image/argentina.png"
    },
    {
      question: data.string.q7,
      answers: [
        { ans: data.string.name25 },
        { ans: data.string.name26, correct: "correct" },
        { ans: data.string.name27 },
        { ans: data.string.name28 }
      ],
      img: $ref + "/exercise_image/swimming.png"
    },
    {
      question: data.string.q8,
      answers: [
        { ans: data.string.name29 },
        { ans: data.string.name30, correct: "correct" },
        { ans: data.string.name31 },
        { ans: data.string.name32 }
      ],
      img: $ref + "/exercise_image/confedence.jpg"
    },
    {
      question: data.string.q9,
      answers: [
        { ans: data.string.name33 },
        { ans: data.string.name34, correct: "correct" },
        { ans: data.string.name35 },
        { ans: data.string.name36 }
      ],
      img: $ref + "/exercise_image/surkhet.jpg"
    },
    {
      question: data.string.q10,
      answers: [
        { ans: data.string.name37, correct: "correct" },
        { ans: data.string.name38 },
        { ans: data.string.name39 },
        { ans: data.string.name40 }
      ],
      img: $ref + "/exercise_image/practice.jpg"
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();
  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);
  var correctlyanswered = 0;
  $board.on("click", ".neutral", function() {
    // console.log("what");
    if (answered) {
      return answered;
    }
    attemptcount++;
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect == "correct") {
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      play_correct_incorrect_sound(true);
    } else {
      play_correct_incorrect_sound(false);
      $this.addClass("wrong").removeClass("neutral");
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 10) {
      qA();
    } else if (questionCount == 10) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title").hide(0);
      // .text("Congratulation on finishing your exercise").css({
      // "position": "absolute",
      // "top": "48%",
      // "transform": "translateY(-50%)"
      // });
      //ole.footerNotificationHandler.pageEndSetNotification();
      //lesson end
      $(".finishtxt")
        .append(
          "<br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .show(0);

      $(".closebtn").on("click", function() {
        ole.activityComplete.finishingcall();
      });
    }
  });
})(jQuery);

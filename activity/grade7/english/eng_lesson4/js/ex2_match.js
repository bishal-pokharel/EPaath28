var correctCards = 0;
$(init);
loadTimelineProgress(1, 1);

function init() {
  // Hide the success message
  $("#successMessage").hide(0);
  $("#successMessage").css({
    left: "580px",
    top: "250px",
    width: 0,
    height: 0
  });

  // Reset the game
  correctCards = 0;
  $("#cardPile").html("");
  $("#cardSlots").html("");

  // Create the pile of shuffled cards
  /*var numbers = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
		 numbers.sort( function() { return Math.random() - .5 } );*/
  var fruits = [
    "ball",
    "trophy",
    "whistle",
    "shorts",
    "shirt",
    "goalkeeper",
    "pitch",
    "football",
    "player"
  ];
  var orgFruits = fruits;
  fruits.sort(function() {
    return Math.random() - 0.5;
  });
  // var fruit="apple.jpg";

  for (var i = 1; i <= 9; i++) {
    $('<div id="drag' + orgFruits[i - 1] + '">' + orgFruits[i - 1] + "</div>")
      .data("orgFruits", i)
      .appendTo("#cardPile")
      .draggable({
        containment: "#wrapper",
        stack: "#cardPile div",
        cursor: "move",
        revert: true
      });
  }

  // Create the card slots
  // var words = [ 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten' ];
  orgFruits.sort(function() {
    return Math.random() - 0.5;
  });

  for (var i = 0; i < 9; i++) {
    var appendText =
      "<div id='" +
      fruits[i] +
      "' class='col-xs-4 col-sm-4 col-md-4 col-lg-4 droppable'>";
    appendText +=
      "<img src='activity/grade7/english/eng_lesson4/image/" +
      fruits[i] +
      ".jpg' class='image_style'>";
    appendText += "</img>";
    appendText += "</div>";
    //$("#cardSlots").append(appendText);
    $(appendText)
      .data("fruits", fruits[i])
      .attr("id", fruits[i])
      .appendTo("#cardSlots")
      .droppable({
        accept: "#cardPile div",
        hoverClass: "hovered",
        drop: handleCardDrop
      });
  }
}

function handleCardDrop(event, ui) {
  // var slotNumber = $(this).data( 'fruits' );
  //var cardNumber = ui.draggable.data( 'fruits' );
  // console.log(slotNumber);
  //console.log(cardNumber);
  var slotNumber = $(this).attr("id");
  var cardNumber = ui.draggable.text();

  // If the card was dropped to the correct slot,
  // change the card colour, position it directly
  // on top of the slot, and prevent it being dragged
  // again

  if (slotNumber == cardNumber) {
    ui.draggable.addClass("correct");
    ui.draggable.draggable("disable");
    ui.draggable.css("font-size", "20px");
    $(this).droppable("disable");
    // cardnumber.hide(0);
    ui.draggable.position({
      of: $(this),
      my: "left top",
      at: "left top"
    });
    ui.draggable.draggable("option", "revert", false);
    correctCards++;
    //To place the draggable inside of the droppable
    $("#cardPile")
      .find("#drag" + cardNumber)
      .remove();
    //$(this).find("img").css('height','60px');
    $(this).append(
      "<div class='col-md-10 col-sm-11 col-xs-12 end'>" + cardNumber + "</div>"
    );
  }

  // If all the cards have been placed correctly then display a message
  // and reset the cards for another go

  if (correctCards == 9) {
    ole.footerNotificationHandler.pageEndSetNotification();
    $("#successMessage").show(0);
    $("#successMessage").animate({
      //left : '20%',
      //right:'20%',
      //top : '50%',
      //width : '40%',
      //height : '100px',
      left: "46.9%",
      top: "44%",
      width: "15%",
      height: "22%",
      opacity: 1
    });
  }
}

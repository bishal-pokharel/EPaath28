var id = 0;
var charID = 0;
var dialougeImage = 0;
$(document).ready(function() {
  var characterDialouges = [
    {
      diaouges: data.string.dig33
    }, //
    {
      diaouges: data.string.dig34
    }, //

    {
      diaouges: data.string.dig35
    }, //

    {
      diaouges: data.string.dig36
    }, //

    {
      diaouges: data.string.dig37
    } //
  ];
  var dialougeImages = [
    {
      image: $ref + "/slide_images/pele.jpg"
    },
    {
      image: $ref + "/slide_images/pele2.jpg"
    },
    {
      image: $ref + "/slide_images/pele3.jpg"
    },

    {
      image: $ref + "/slide_images/pele4.jpg"
    },

    {
      image: $ref + "/slide_images/pele5.jpg",
      className: "imglast"
    }
  ];
  // var characterImages = [
  //   {
  //     character: $ref + "/image/pele2.jpg"
  //   }
  // ];
  //console.log(content);
  //console.log(dialougeImages);
  //console.log(characterImages);
  //first display the characters
  // displayCharacters();
  displayRelatedImages();
  characterDialouge();
  function displayCharacters() {
    var images = $.each(characterImages, function(key, values) {
      charID++;
      characters = values.character;
      appendCharacters = '<div class="col-lg-3 col-md-3 col-sm-3">';
      appendCharacters +=
        '<img src =" ' +
        characters +
        '" class="img-circle img_rounded character_image_modify" id="character' +
        charID +
        '">';
      appendCharacters += "</div>";
      console.log(appendCharacters);
      $(".characters_images").append(appendCharacters);
    });
  }
  function displayRelatedImages() {
    var relatedImages = $.each(dialougeImages, function(key, values) {
      dialougeImage++;
      dialouge_images = values.image;
      className = values.className;
      appendImages =
        '<img src ="' +
        dialouge_images +
        '" id="diImg' +
        dialougeImage +
        '" class="dialouge_images ' +
        className +
        '">';
      $(".related_images").append(appendImages);
    });
  }
  function characterDialouge() {
    appendDialouge =
      '<div class="col-lg-12 col-md-12 col-sm-12 footer_dialouges">';
    var dialouges = $.each(characterDialouges, function(key, values) {
      id++;
      chDialouges = values.diaouges;

      appendDialouge += '<p id ="diag' + id + '" class="character_dialouges">';
      appendDialouge += chDialouges;
      appendDialouge += "</p>";
    });
    appendDialouge += "</div>";
    $("#dialouge").append(appendDialouge);
  }
});

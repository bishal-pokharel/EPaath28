var buttonID = 1;
var id_num = 1;
$(document).ready(function() {
  $(".exercise_button").click(function() {
    location.reload();
  });
  $("#activity-page-next-btn-enabled").click(function() {
    $(".yes").css("color", "black");
    $(".no").css("color", "black");
    console.log("ok");
    var id_num = buttonID;
    buttonID = buttonID + 1;
    loadTimelineProgress(15, buttonID);
    var next_id = buttonID;
    $(".question" + id_num).hide(0);
    $(".question" + buttonID).show(0);
    $("#activity-page-next-btn-enabled").hide(0);
    answered = false;
    attemptcount = 0;
  });
  $(".closebtn").click(function() {
    ole.activityComplete.finishingcall();
  });
});

//$('.title').text(data.string.exerciseTitle);
var id = 0;
$(document).ready(function() {
  var ex = [
    {
      text: data.string.ex1
    },
    {
      text: data.string.ex2
    },
    {
      text: data.string.ex3
    },
    {
      text: data.string.ex4
    },
    {
      text: data.string.ex5
    },
    {
      text: data.string.ex6
    },
    {
      text: data.string.ex7
    },
    {
      text: data.string.ex8
    },
    {
      text: data.string.ex9
    },
    {
      text: data.string.ex10
    },
    {
      text: data.string.ex11
    },
    {
      text: data.string.ex12
    },
    {
      text: data.string.ex13
    },
    {
      text: data.string.ex14
    },
    {
      text: data.string.ex15
    }
  ];
  // console.log(ex);
  totalq = ex.length;
  loadTimelineProgress(totalq, 1);

  $("#activity-page-next-btn-enabled").hide(0);
  displayContents();
  function displayContents() {
    var context = $.each(ex, function(key, values) {
      id++;
      text1 = values.text;
      appendContents = '<div class="question' + id + '">';
      appendContents += text1;
      appendContents += "</div>";
      console.log(appendContents);
      // console.log(appendContents);
      $(".contents_choose").append(appendContents);
    });
  }
});

var answered = false;
var attemptcount = 0;

var totalq;
var correctlyanswered = 0;

function correct() {
  if (answered) {
    return answered;
  }
  attemptcount++;

  $(".yes").css("color", "#009933");
  play_correct_incorrect_sound(true);
  if (attemptcount == 1) {
    correctlyanswered++;
  }
  answered = true;
  if (buttonID == 15) {
    //x$(".title").html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.");
    //$(".exercise_button").show(0);
    $("#activity-page-next-btn-enabled").hide(0);

    $(".finishtxt").show(0);
  } else {
    $("#activity-page-next-btn-enabled").show(0);
  }
}
function incorrect() {
  if (answered) {
    return answered;
  }
  play_correct_incorrect_sound(false);
  $(".no").css("color", "#D00000");
}

var id_num = 1;
var buttonID = 1;
var id=0;
$(document).ready(function() {
	var questionCount =0;
	var $total_page = 1;
	loadTimelineProgress($total_page,questionCount+1);
	$("#play_again").click(function(){
		location.reload();
	});
	$('#successMessage').hide(0);
	
	var questionContent = [ {
		question : data.string.label1,
		
	},//	
	{
		question : data.string.label2
	},//
	{
		question : data.string.label3
	},//
	{
		question : data.string.label4
	},//
	{
		question : data.string.label5
	},//
	];
	
	var pathology = [
	                 {
	                	 path : data.string.dept1
	                 },
	                 ];
	
	var middleSection = [
	                     {
	                    	peo : data.string.dept2
	                     },
	                     {
	                    	peo : data.string.dept3 
	                     },
	                     {
	                    	 peo : data.string.dept4
	                     },
	                     ];
	var ambulance =[
	                {
	                	ambu : data.string.dept5
	                },
	                ];

	var characters =[
	                 {
	                	img :  $ref + "/exercise_images/yay.png" 
	                 },
	                 {
		                img  : $ref + "/exercise_images/ch.png" 
		              },
	                 {
		                img  : $ref + "/exercise_images/emergency.png" 
		              },
	                 {
		                img : $ref + "/exercise_images/opd.png" 
		              },
	                 {
		                img : $ref + "/exercise_images/ambulance.png" 
		              },
	                 ];
	pathalogyBlock();
	middleBlock();
	ambulanceBlock();
	imageSection();
	initialize();
	function pathalogyBlock(){
		var context =$.each(pathology,function(key,values){
			pharmacy = values.path;
					
			appendContents ='<div class="cardslots">';
			appendContents +='<div id="slot_1" class="test" data-number="1">';
			//appendContents +=pharmacy;
			appendContents +='</div>';
			appendContents +='</div>';
			$(".patho").append(appendContents);
		});
	}
	
	function middleBlock(){
		var context =$.each(middleSection,function(key,values){
			wards = values.peo;
			id_num++;
			console.log(id_num);
			appendMiddle='<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">';
			appendMiddle +='<div class="cardSlots">';
			appendMiddle +='<div id="slot_'+ id_num +'" class="test" data-number="'+id_num+'">';
			//appendMiddle +='<p>' + wards +'</p>';
			appendMiddle +='</div>';
			appendMiddle +='</div>';
			appendMiddle +='</div>';
			if(id_num==5){				
			}
			$(".middle").append(appendMiddle);
		});
	}

	function ambulanceBlock(){
		var context =$.each(ambulance,function(key,values){
			amb = values.ambu;
			appendMiddle ='<div class="cardSlots">';
			appendMiddle +='<div id="slot_5" class="test" data-number="5">';
			//appendMiddle += amb;
			appendMiddle +='</div>';
			appendMiddle +='</div>';
			$(".ambulance").append(appendMiddle);

		});
	}
	
	function imageSection(){
		var context =$.each(characters,function(key,values){
			image = values.img;
			id++;
			
			appendCharacters ='<div class="cardPile">';
			appendCharacters +='<div id="element_'+ id +'" data-number="'+id+'">';
			appendCharacters += '<img src="' + image +'" class="tt img-responsive" id="image'+ id +'">';
			appendCharacters +='</div>';
			appendCharacters +='</div>';
			$(".characters").append(appendCharacters);
			if(id==6){
				
			}
			
			
		});
	}
	
questions();
function questions(){
	id = 0;
	var context =$.each(questionContent,function(key,values){
	id++;
	que=values.question;
	appendQuestion='<div id="question' + id +'">';
	appendQuestion+=que;
	appendQuestion +='</div>';
	$(".questions").append(appendQuestion);
	console.log("id"+id);

	
	});
}

function initialize() {
	/*id = 1;
	while(id < 6) {
		$("#element_" + id).draggable({
			containment: '#content',
			stack:'.cardSlots div',
			cursor : 'move',
			revert : 'invalid'
		});//element end

		$("#slot_"+ id).droppable({
			accept : '#element_'+id,
			hoverClass : 'hovered',
			//drop : handleCardDrop
		});//slot end
		id++;

	}*/
	$('.cardPile div').draggable({
		stack: '.cardSlots div',
		containment : '#content',
		cursor : 'move',
		revert : function() {
			if($(this).data('number') != $(this).parents('div.ui-droppable').data('number')) {
				return true;
			} else {
				return false;
			}
		}
	});
	
	$('.cardSlots div').droppable({
		hoverClass : 'hovered',
		drop : handleCardDrop
	});

}



function handleCardDrop(event, ui) {
	var slotNumber = $(this).data('number');
	var cardNumber = ui.draggable.data('number');
	if (slotNumber == cardNumber) {
		ui.draggable.addClass('correct');
		//ui.draggable.draggable('disable');
		ui.draggable.clone(true).css('position', 'inherit').appendTo($(this));
	    ui.draggable.remove();
		$(this).droppable('disable');
		//$(this).html(ui.draggable);
		
		
		
		var id_num = buttonID;
		buttonID = buttonID + 1;
		var next_id = buttonID;
		console.log(id_num);
		console.log(next_id);
		$("#question" + id_num).hide(0);
		$("#question" + next_id).show(0);
		if(next_id==6){
			//alert("congratulations you did it!");
			$('#successMessage').show(0);
			
		}
		
	}
}
});
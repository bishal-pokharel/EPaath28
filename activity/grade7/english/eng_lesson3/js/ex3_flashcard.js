$('.title').text(data.string.exerciseTitle);
$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
			var content = [ {
				words : data.string.word1,
				description : data.string.des1,
				img : $ref + "/images/enormous.jpg",
			},//
			{
				words : data.string.word2,
				description : data.string.des2,
				img : $ref + "/images/chariot.jpg",
			},//

			{
				words : data.string.word3,
				description : data.string.des3,
				img : $ref + "/images/idol.jpg",
			},//

			{
				words : data.string.word4,
				description : data.string.des4,
				img : $ref + "/images/features.jpg",
			},//
			{
				words : data.string.word5,
				description : data.string.des5,
				img : $ref + "/images/assemble.png",
			},//
			{
				words : data.string.word6,
				description : data.string.des6,
				img : $ref + "/images/materials.jpg",
			},//
			{
				words : data.string.word7,
				description : data.string.des7,
				img : $ref + "/images/toppel.jpg",
			},//
			{
				words : data.string.word8,
				description : data.string.des8,
				img : $ref + "/images/Omen.jpg",
			},//
			{
				words : data.string.word9,
				description : data.string.des9,
				img : $ref + "/images/steady.jpg",
			},//
			{
				words : data.string.word10,
				description : data.string.des10,
				img : $ref + "/images/lane.jpg",
			},//
			{
				words : data.string.word11,
				description : data.string.des11,
				img : $ref + "/images/conclude.jpg",
			},//
			{
				words : data.string.word12,
				description : data.string.des12,
				img : $ref + "/images/ceremony.jpg",
			},//
			{
				words : data.string.word13,
				description : data.string.des13,
				img : $ref + "/images/legend.jpg",
			},//
			{
				words : data.string.word14,
				description : data.string.des14,
				img : $ref + "/images/suffer.jpg",
			},//
			{
				words : data.string.word15,
				description : data.string.des15,
				img : $ref + "/images/prolonged.jpg",
			},//
			{
				words : data.string.word16,
				description : data.string.des16,
				img : $ref + "/images/spell.jpg",
			},//
			{
				words : data.string.word17,
				description : data.string.des17,
				img : $ref + "/images/drought.jpg",
			},//
			{
				words : data.string.word18,
				description : data.string.des18,
				img : $ref + "/images/start.jpg",
			},//
			{
				words : data.string.word19,
				description : data.string.des19,
				img : $ref + "/images/plea.jpg",
			},//
			{
				words : data.string.word20,
				description : data.string.des20,
				img : $ref + "/images/plentiful.jpg",
			},//
			{
				words : data.string.word21,
				description : data.string.des21,
				img : $ref + "/images/harvest.jpg",
			},//
			{
				words : data.string.word22,
				description : data.string.des22,
				img : $ref + "/images/prosperity.jpg",
			},//
			{
				words : data.string.word23,
				description : data.string.des23,
				img : $ref + "/images/collective.jpg",
			},//
			{
				words : data.string.word24,
				description : data.string.des24,
				img : $ref + "/images/revelry.jpg",
			},//
			{
				words : data.string.word25,
				description : data.string.des25,
				img : $ref + "/images/onlookers.jpg",
			},//
			{
				words : data.string.word26,
				description : data.string.des26,
				img : $ref + "/images/cheering.jpg",
			},//
			{
				words : data.string.word27,
				description : data.string.des27,
				img : $ref + "/images/accompained.jpg",
			},//
			{
				words : data.string.word28,
				description : data.string.des28,
				img : $ref + "/images/cymbals.jpg",
			},//
			{
				words : data.string.word29,
				description : data.string.des29,
				img : $ref + "/images/experience.jpg",
			},//
			
			];
			console.log(content);
			displaycontent();
			function displaycontent() {
				var words = $.each(content, function(key, value) {
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image img-responsive"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);

				});
			}
			/*
			 * function qA() { var source = $('#qA-templete').html(); var
			 * template = Handlebars.compile(source); var html =
			 * template(content); //$board.html(html); // console.log(html); }
			 * 
			 * qA();
			 */

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		//alert("ok");
		$(this).toggleClass('active');
	}); 
	ole.footerNotificationHandler.pageEndSetNotification();
});
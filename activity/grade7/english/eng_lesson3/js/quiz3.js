//$(document).ready(function(){
(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  $(".title").text(data.string.quiztitle);
  var content = [
    {
      question: data.string.quiz3_01,
      answers: [
        { ans: data.string.quiz3_ans1 },
        { ans: data.string.quiz3_ans2, correct: "correct" },
        { ans: data.string.quiz3_ans3 }
      ],
      img: $ref + "/images/bhotojatra.jpg"
    }
  ];

  $nextBtn.fadeOut();

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();
  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);

  var correctlyanswered = 0;
  $board.on("click", ".neutral", function() {
    // console.log("what");
    if (answered) {
      return answered;
    }
    attemptcount++;
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      play_correct_incorrect_sound(true);
      $(".question").text(
        eval("data.string.quiz3_0" + (questionCount + 1) + "a")
      );
      $this.addClass("right").removeClass("neutral");
      $(".correcttxt").show(0);
      $(".incorrecttxt").hide(0);

      $nextBtn.fadeIn();
    } else {
      $this.addClass("wrong").removeClass("neutral");
      play_correct_incorrect_sound(false);

      $(".incorrecttxt").show(0);
      $(".correcttxt").hide(0);
    }
  });

  $nextBtn.on("click", function() {
    $(".incorrecttxt").hide(0);
    $(".correcttxt").hide(0);
    $nextBtn.fadeOut();
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 1) {
      qA();
    } else if (questionCount == 1) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title")
        .html(
          "Congratulations on finishing your exercise <br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .css({
          position: "absolute",
          top: "48%",
          transform: "translateY(-50%)"
        });
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
})(jQuery);

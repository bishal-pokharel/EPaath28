$('.title').text(data.string.title);
$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
	    loadTimelineProgress(1,1);
			
			var ex = [
			          {
			        	text : data.string.ex1
			          }
			          ]
			console.log(ex);
			var content = [ {
				words : data.string.word1,
				description : data.string.des1,
				img : $ref + "/images/midst.jpg",
			},//
			{
				words : data.string.word2,
				description : data.string.des2,
				img : $ref + "/images/excerpt.jpg",
			},//

			{
				words : data.string.word3,
				description : data.string.des3,
				img : $ref + "/images/autographical.png",
			},//

			{
				words : data.string.word4,
				description : data.string.des4,
				img : $ref + "/images/essay.jpg",
			},//
			{
				words : data.string.word5,
				description : data.string.des5,
				img : $ref + "/images/translated.jpg",
			},//
			{
				words : data.string.word6,
				description : data.string.des6,
				img : $ref + "/images/transcend.jpg",
			},//
			{
				words : data.string.word7,
				description : data.string.des7,
				img : $ref + "/images/cerebral.jpg",
			},//
			{
				words : data.string.word8,
				description : data.string.des8,
				img : $ref + "/images/author.jpg",
			},//
			{
				words : data.string.word9,
				description : data.string.des9,
				img : $ref + "/images/columinist.jpg",
			},//
			{
				words : data.string.word10,
				description : data.string.des10,
				img : $ref + "/images/recipient.jpg",
			},//
			{
				words : data.string.word11,
				description : data.string.des11,
				img : $ref + "/images/crippled.jpg",
			},//
			{
				words : data.string.word12,
				description : data.string.des12,
				img : $ref + "/images/deprived.jpg",
			},//
			{
				words : data.string.word13,
				description : data.string.des13,
				img : $ref + "/images/denied.jpg",
			},//
			{
				words : data.string.word14,
				description : data.string.des14,
				img : $ref + "/images/desire.jpg",
			},//
			{
				words : data.string.word15,
				description : data.string.des15,
				img : $ref + "/images/fulfilled.jpg",
			},//
			{
				words : data.string.word16,
				description : data.string.des16,
				img : $ref + "/images/financial.jpg",
			},//
			{
				words : data.string.word17,
				description : data.string.des17,
				img : $ref + "/images/ignore.jpg",
			},//
			{
				words : data.string.word18,
				description : data.string.des18,
				img : $ref + "/images/mastery.jpg",
			},//
			{
				words : data.string.word19,
				description : data.string.des19,
				img : $ref + "/images/twig.jpg",
			},//
			{
				words : data.string.word20,
				description : data.string.des20,
				img : $ref + "/images/memorize.jpg",
			},//
			{
				words : data.string.word21,
				description : data.string.des21,
				img : $ref + "/images/means.jpg",
			},//
			{
				words : data.string.word22,
				description : data.string.des22,
				img : $ref + "/images/attemp.jpg",
			},//
			{
				words : data.string.word23,
				description : data.string.des23,
				img : $ref + "/images/practice.jpg",
			},//
			{
				words : data.string.word24,
				description : data.string.des24,
				img : $ref + "/images/composition.jpg",
			},//
			{
				words : data.string.word25,
				description : data.string.des25,
				img : $ref + "/images/habituated.jpg",
			},//
			{
				words : data.string.word26,
				description : data.string.des26,
				img : $ref + "/images/crawl.jpg",
			},//
			{
				words : data.string.word27,
				description : data.string.des27,
				img : $ref + "/images/illusion.jpg",
			},//
			{
				words : data.string.word28,
				description : data.string.des28,
				img : $ref + "/images/complete.jpg",
			},//
			{
				words : data.string.word29,
				description : data.string.des29,
				img : $ref + "/images/bruise.jpg",
			},//
			{
				words : data.string.word30,
				description : data.string.des30,
				img : $ref + "/images/innumerable.jpg",
			},//
			{
				words : data.string.word31,
				description : data.string.des31,
				img : $ref + "/images/dewdrops.jpg",
			},//
			{
				words : data.string.word32,
				description : data.string.des32,
				img : $ref + "/images/wipedout.jpg",
			},//
			{
				words : data.string.word33,
				description : data.string.des33,
				img : $ref + "/images/existence.jpg",
			},//
			{
				words : data.string.word34,
				description : data.string.des34,
				img : $ref + "/images/distress.jpg",
			},//
			{
				words : data.string.word35,
				description : data.string.des35,
				img : $ref + "/images/expression.jpg",
			},//
			{
				words : data.string.word36,
				description : data.string.des36,
				img : $ref + "/images/resentment.jpg",
			},//
			{
				words : data.string.word37,
				description : data.string.des37,
				img : $ref + "/images/rumors.jpg",
			},//
			{
				words : data.string.word38,
				description : data.string.des38,
				img : $ref + "/images/adolescent.jpg",
			},//
			{
				words : data.string.word39,
				description : data.string.des39,
				img : $ref + "/images/literary.jpg",
			},//
			{
				words : data.string.word40,
				description : data.string.des40,
				img : $ref + "/images/lyrics.jpg",
			},//
			{
				words : data.string.word41,
				description : data.string.des41,
				img : $ref + "/images/perodical.jpg",
			},//
			{
				words : data.string.word42,
				description : data.string.des42,
				img : $ref + "/images/obliged.jpg",
			},//
			{
				words : data.string.word43,
				description : data.string.des43,
				img : $ref + "/images/appearance.jpg",
			},//

			];
			//console.log(content);
			displaycontent();

			function displaycontent() {
				var words = $.each(content, function(key, value) {
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image img-responsive"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);

				});

			}

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		//alert("ok");
		$(this).toggleClass('active');
	});
	ole.footerNotificationHandler.pageEndSetNotification();
});

var id=1;
var buttonId =1;
$(document).ready(function(){
	var soundAsset = $ref+"/sounds/";
	var s1_p1 = new buzz.sound((soundAsset + "s1_p1.ogg"));
	var s1_p2 = new buzz.sound((soundAsset + "s1_p2.ogg"));
	var s1_p3 = new buzz.sound((soundAsset + "s1_p3.ogg"));
	var s1_p4 = new buzz.sound((soundAsset + "s1_p4.ogg"));
	var s1_p5 = new buzz.sound((soundAsset + "s1_p5.ogg"));
	var s1_p6 = new buzz.sound((soundAsset + "s1_p6.ogg"));
	var soundArray = [s1_p1, s1_p2, s1_p3, s1_p4, s1_p5, s1_p6];
	var currentSound = s1_p1;
	currentSound.play();
	$("#activity-page-next-btn-enabled").click(function(){
		var journalid = buttonId;
		buttonId = buttonId + 1;
		var next_id = buttonId;
		soundPlayer(soundArray[buttonId-1], journalid);
		console.log(journalid);
		console.log(next_id);
    loadTimelineProgress(6,buttonId);
    $(".coverpage").addClass("coverpage1").removeClass("coverpage");
		$(".para_date" + journalid).hide(0);
		$(".para_content" + journalid).hide(0);
		$("#jhammak_img" + journalid).hide(0);

		$(".para_date" + next_id).show(0);
		$("#jhammak_img" + next_id).show(0);
		$(".para_content" + next_id).show(0);
		$("#activity-page-prev-btn-enabled").show(0);

		//need to function the button clicks
	   $(".para_date1").hide(0);
	   $(".para_content1").hide(0);
	   // if(journalid >= 5) {
		 //   $("#activity-page-next-btn-enabled").hide(0);
		 //   $("#activity-page-prev-btn-enabled").show(0);
			//  ole.footerNotificationHandler.lessonEndSetNotification();
	   // }
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		var journalid = buttonId;
		buttonId = buttonId - 1;
		soundPlayer(soundArray[buttonId-1], journalid);
		var next_id = buttonId;
		console.log(journalid);
		console.log(next_id);
		 $("#activity-page-next-btn-enabled").show(0);
		$(".para_date" + journalid).hide(0);
		$(".para_content" + journalid).hide(0);
		$("#jhammak_img" + journalid).hide(0);

		$(".para_date" + next_id).show(0);
		$(".para_content" + next_id).show(0);
		$("#jhammak_img" + next_id).show(0);
		//$("#activity-page-prev-btn-enabled").show(0);

		//need to function the button clicks
	   if(journalid == 2) {
       $(".coverpage1").addClass("coverpage").removeClass("coverpage1");
       $("#activity-page-prev-btn-enabled").hide(0);
		   $("#activity-page-next-btn-enabled").show(0);
	   }
	});

	$("#activity-page-next-btn-enabled").hide(0);

	function soundPlayer(soundId, journalid){
		$("#activity-page-next-btn-enabled, #activity-page-prev-btn-enabled").hide(0);
		currentSound.stop();
		currentSound = soundId;
		currentSound.play();
		currentSound.bind("ended", function(){
			 if(journalid >= 5) {
				 $("#activity-page-next-btn-enabled").hide(0);
				 $("#activity-page-prev-btn-enabled").show(0);
				 ole.footerNotificationHandler.lessonEndSetNotification();

			 }else{
				 $("#activity-page-next-btn-enabled, #activity-page-prev-btn-enabled").show(0);
			 }
		});
	}
	$(".closebtn").click(function(){
		currentSound.stop();
		$("#activity-page-next-btn-enabled").show(0);
	});
});

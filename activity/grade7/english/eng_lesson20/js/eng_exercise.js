//$(document).ready(function(){
	var imgpath = $ref+"/exercise_image/";
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.exerciseTitle);
	var content = [
	{
		question : data.string.q1,
		answers: [
		{ans : data.string.name1,
			correct : "correct"},
		{ans : data.string.name2},
		{ans : data.string.name3},
		{ans : data.string.name4}
		],
		img: imgpath+ "q01.jpg"
	},
	{
		question : data.string.q2,
		answers: [
		{ans : data.string.name5},
		{ans : data.string.name6},
		{ans : data.string.name7,
			correct : "correct"},
		{ans : data.string.name8}
		],
		img: imgpath+ "q02.jpg"
	},
	{
		question : data.string.q3,
		answers: [
		{ans : data.string.name9,
			correct : "correct"},
		{ans : data.string.name10},
		{ans : data.string.name11},
		{ans : data.string.name12}
		],
		img: imgpath+ "q03.jpg"
	},
	{
		question : data.string.q4,
		answers: [
		{ans : data.string.name13},
		{ans : data.string.name14 ,
			correct : "correct"},
		{ans : data.string.name15},
		{ans : data.string.name16}
		],
		img: imgpath+ "q04.jpg"
	},
	{
		question : data.string.q5,
		answers: [
		{ans : data.string.name17},
		{ans : data.string.name18},
		{ans : data.string.name19,
			correct : "correct"},
		{ans : data.string.name20}
		],
		img: imgpath+ "q05.jpg"
	},
	{
		question : data.string.q6,
		answers: [
		{ans : data.string.name21},
		{ans : data.string.name22},
		{ans : data.string.name23},
		{ans : data.string.name24,
			correct : "correct"}
		],
		img: imgpath+ "q06.jpg"
	},
	{
		question : data.string.q7,
		answers: [
		{ans : data.string.name25,
			correct : "correct"},
		{ans : data.string.name26},
		{ans : data.string.name27},
		{ans : data.string.name28}
		],
		img: imgpath+ "q07.jpg"
	},
	{
		question : data.string.q8,
		answers: [
		{ans : data.string.name29},
		{ans : data.string.name30,
			correct : "correct"},
		{ans : data.string.name31},
		{ans : data.string.name32}
		],
		img: imgpath+ "q08.jpg"
	},
	{
		question : data.string.q9,
		answers: [
		{ans : data.string.name33},
		{ans : data.string.name34,
			correct : "correct"},
		{ans : data.string.name35},
		{ans : data.string.name36}
		],
		img: imgpath+ "q09.jpg"
	},
	{
		question : data.string.q10,
		answers: [
		{ans : data.string.name37},
		{ans : data.string.name38},
		{ans : data.string.name39,
			correct : "correct"},
		{ans : data.string.name40}
		],
		img: imgpath+ "q10.jpg"
	},
	{
		question : data.string.q11,
		answers: [
		{ans : data.string.name41,
			correct : "correct"},
		{ans : data.string.name42},
		{ans : data.string.name43},
		{ans : data.string.name44}
		],
		img: imgpath+ "q11.jpg"
	},
	{
		question : data.string.q12,
		answers: [
		{ans : data.string.name45},
		{ans : data.string.name46,
			correct : "correct"},
		{ans : data.string.name47},
		{ans : data.string.name48}
		],
		img: imgpath+ "q12.jpg"
	},
	{
		question : data.string.q13,
		answers: [
		{ans : data.string.name49},
		{ans : data.string.name50},
		{ans : data.string.name51,
			correct : "correct"},
		{ans : data.string.name52}
		],
		img: imgpath+ "q13.jpg"
	},
	{
		question : data.string.q14,
		answers: [
		{ans : data.string.name53},
		{ans : data.string.name54,
			correct : "correct"},
		{ans : data.string.name55},
		{ans : data.string.name56}
		],
		img: imgpath+ "q14.png"
	}
	];

	$nextBtn.hide(0);

	// console.log(content);

	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	loadTimelineProgress(content.length, questionCount + 1);


	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();
	var answered = false;
	var attemptcount = 0;

	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		// console.log("what");
		if(answered){
			return answered;
		}
		attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true);
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
			play_correct_incorrect_sound(false);
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<14){
			qA();
			loadTimelineProgress(content.length, questionCount + 1);
		}
		else if (questionCount==14){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

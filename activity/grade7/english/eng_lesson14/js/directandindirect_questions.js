var id=0;
$(document).ready(function() {
	var content = [ {
		professionDesc : data.string.DQ1,
		profession:data.string.RQ1,
		img : $ref + "/slide_images/card.jpg"
	},//
	{
		professionDesc : data.string.DQ2,
		profession:data.string.RQ2,
		img : $ref + "/slide_images/lunch.jpg"
	},//

	{
		professionDesc : data.string.DQ3,
		profession:data.string.RQ3,
		img : $ref + "/slide_images/happy.jpg"
	},//

	{
		professionDesc : data.string.DQ4,
		profession:data.string.RQ4,
		img : $ref + "/slide_images/rashmi.jpg"
	},//
	

	];
	//console.log(content);
	displayContents();
	function displayContents(){
		var context =$.each(content,function(key,values){
		    id++;
			desc = values.professionDesc;
			prof = values.profession;
			image=values.img;
			//console.log(desc);
			//console.log(prof);
			//console.log(image);
			appendContents='<div class="question' + id +'">';
			appendContents+='<div class="row content_image">';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> </div>';
			appendContents +='<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 text_align_center">';
			appendContents +='<img src="'+ image + '" class="question_image"/>';
			appendContents +='</div>';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> </div>';
			appendContents +='</div>';	
			appendContents +='<div class="row content_answerSection">';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> </div>';	
			appendContents +='<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 text_align_center">';	
			appendContents +='<div class="hint_button">';
			appendContents +='Direct Question';		
			appendContents +='</div>';	
			appendContents +='<div class="hint">';	
			appendContents += desc;
			appendContents +='</div>';
			appendContents +='</div>';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> </div>';
			appendContents +='</div>';
			appendContents +='<div class="row answer_section">';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> </div>';	
			appendContents +='	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text_align_center">';
			appendContents +='<div class="answer1">';	
			appendContents +='Reported Question';		
			appendContents +='</div>';
			appendContents +='<div class="answer text_align_center">';	
			appendContents += prof;
			appendContents +='</div>';	
			appendContents +='</div>';
			appendContents +='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
			appendContents +='<button id="button1" class="button_next">';
			appendContents +='Next';	
			appendContents +='</button>';	
			appendContents +='</div>';
			appendContents +='</div>';
			appendContents +='</div>';
			//console.log(appendContents);
			$(".exercise_contents").append(appendContents);
		});
	}
	
});
var id=0;
$(document).ready(function() {
	var content = [ {
		professionDesc : data.string.profD1,
		profession:data.string.prof1,
		img : $ref + "/slide_images/dog.jpg"
	},//
	{
		professionDesc : data.string.profD2,
		profession:data.string.prof2,
		img : $ref + "/slide_images/play_dogs.jpg"
	},//

	{
		professionDesc : data.string.profD3,
		profession:data.string.prof3,
		img : $ref + "/slide_images/vdc.jpg"
	},//

	{
		professionDesc : data.string.profD4,
		profession:data.string.prof4,
		img : $ref + "/slide_images/vdc2.jpg"
	},//
	
	{
		professionDesc : data.string.profD5,
		profession:data.string.prof5,
		img : $ref + "/slide_images/vdc3.jpg"
	},//
	
	{
		professionDesc : data.string.profD6,
		profession:data.string.prof6,
		img : $ref + "/slide_images/school.jpg"
	},//
	{
		professionDesc : data.string.profD8,
		profession:data.string.prof8,
		img : $ref + "/slide_images/school1.jpg"
	},//
	{
		professionDesc : data.string.profD9,
		profession:data.string.prof9,
		img : $ref + "/slide_images/school2.jpg"
	},//
	{
		professionDesc : data.string.profD10,
		profession:data.string.prof10,
		img : $ref + "/slide_images/run.jpg"
	},//
	{
		professionDesc : data.string.profD11,
		profession:data.string.prof10,
		img : $ref + "/slide_images/run1.jpg"
	},
	];
	//console.log(content);
	displayContents();
	function displayContents(){
		var context =$.each(content,function(key,values){
		    id++;
			desc = values.professionDesc;
			prof = values.profession;
			image=values.img;
			//console.log(desc);
			//console.log(prof);
			//console.log(image);
			appendContents='<div class="question' + id +'">';
			appendContents+='<div class="row content_image">';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> </div>';
			appendContents +='<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 ">';
			appendContents +='<img src="'+ image + '" class="question_image img-responsive"/>';
			appendContents +='</div>';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> </div>';
			appendContents +='</div>';	
			appendContents +='<div class="row content_answerSection">';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> </div>';	
			appendContents +='<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 text_align_center">';	
			appendContents +='<div class="hint_button">';
			appendContents +='Quoted Speech';		
			appendContents +='</div>';	
			appendContents +='<div class="hint">';	
			appendContents += desc;
			appendContents +='</div>';
			appendContents +='</div>';
			appendContents +='<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> </div>';
			appendContents +='</div>';
			appendContents +='<div class="row answer_section">';
			appendContents +='<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> </div>';	
			appendContents +='	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 text_align_center">';
			appendContents +='<div class="answer1">';	
			appendContents +='Reported Speech';		
			appendContents +='</div>';
			appendContents +='<div class="answer text_align_center">';	
			appendContents += prof;
			appendContents +='</div>';
			appendContents +='</div>';
			appendContents +='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
			appendContents +='<button id="button1" class="button_next">';
			appendContents +='Next';	
			appendContents +='</button>';	
			appendContents +='</div>';
			appendContents +='</div>';
			appendContents +='</div>';
			//console.log(appendContents);
			$(".exercise_contents").append(appendContents);
		});
	}
	
});
//$(document).ready(function(){
var imgpath = $ref+"/exercise_image/";
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.qq1,
		mainQuestion : data.string.q1,
		answers: [
		{ans : data.string.ans1,
			correct : "correct"},
		{ans : data.string.ans2},
		{ans : data.string.ans3},
		{ans : data.string.ans4}
		],
		img: imgpath+"q01.jpg"
	},
	{
		question : data.string.qq2,
		mainQuestion : data.string.q2,
		answers: [
		{ans : data.string.ans5},
		{ans : data.string.ans6,
			correct : "correct"},
		{ans : data.string.ans7},
		{ans : data.string.ans8}
		],
		img: imgpath+"q02.jpg"
	},
	{
		question : data.string.qq3,
		mainQuestion : data.string.q3,
		answers: [
		{ans : data.string.ans9},
	
		{ans : data.string.ans10},
		{ans : data.string.ans11,
			correct : "correct"},
		{ans : data.string.ans12}
		],
		img: imgpath+"q03.jpg"
	},
	{
		question : data.string.qq4,
		mainQuestion : data.string.q4,
		answers: [
		{ans : data.string.ans13},
		{ans : data.string.ans14},
		{ans : data.string.ans15},
		{ans : data.string.ans16,
			correct : "correct"}
		],
		img: imgpath+"q04.jpg"
	},
	
	{
		question : data.string.qq5,
		mainQuestion : data.string.q5,
		answers: [
		  		{ans : data.string.ans17},
		  		{ans : data.string.ans18,correct : "correct"},
		  		],
		img: imgpath+"q05.jpg"
	},
	{
		question : data.string.qq6,
		mainQuestion : data.string.q6,
		answers: [
		  		{ans : data.string.ans19},
		  		{ans : data.string.ans20,
		  			correct : "correct"}
		  		],
		img: imgpath+"q04.jpg"
	}
		
	];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}

	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		// console.log("what");
		if(answered){
			return answered;
		}
		attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
			play_correct_incorrect_sound(false);
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<6){
			qA();
		}
		else if (questionCount==6){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$(".title").hide(0);
			//$('.title').text("Congratulation on finishing your exercise").css({				"position": "absolute",				"top": "48%",				"transform": "translateY(-50%)"			});
			
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
			$(".closebtn").on("click",function(){
				ole.activityComplete.finishingcall();
			});
		}
	});
	
	
		
	
	

})(jQuery);

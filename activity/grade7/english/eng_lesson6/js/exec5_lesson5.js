//$(document).ready(function(){
(function () {
	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	           	{
	           		question : data.string.j1,
	           		answers: [
	           		{ans : data.string.ans1},
	           		{ans : data.string.ans2,
	           			correct : "correct"},
	           		{ans : data.string.ans3},
	           		{ans : data.string.ans4},
	           		{ans : data.string.ans5},
	           		{ans : data.string.ans6},
	           		{ans : data.string.ans7},
	           		{ans : data.string.ans8},
	           		{ans : data.string.ans9},
	           		{ans : data.string.ans10},
	           		],
	           		
	           	},
	           	{
	           		question : data.string.j2,
	           		answers: [
	           		  		{ans : data.string.ans11},
	           		  		{ans : data.string.ans12},
	           		  		{ans : data.string.ans13},
	           		  		{ans : data.string.ans14},
	           		  		{ans : data.string.ans15},
	           		  		{ans : data.string.ans16,
	           		  			correct : "correct"},
	           		  		{ans : data.string.ans17},
	           		  		{ans : data.string.ans18},
	           		  		{ans : data.string.ans19},
	          
	           		  		],
	           		
	           	},
	           	{
	           		question : data.string.j3,
	           		answers: [
	           		  		{ans : data.string.ans21},
	           		  		{ans : data.string.ans22},
	           		  		{ans : data.string.ans23},
	           		  		{ans : data.string.ans24},
	           		  		{ans : data.string.ans25},
	           		  		{ans : data.string.ans26},
	           		  		{ans : data.string.ans27},
	           		  		{ans : data.string.ans28,
	           		  			correct : "correct"},
	           		  		{ans : data.string.ans29},
	           		  		{ans : data.string.ans30},
	           		  		],
	           	},
	           	{
	           		question : data.string.j4,
	           		answers: [
	           		  		{ans : data.string.ans31},
	           		  		{ans : data.string.ans32},
	           		  		{ans : data.string.ans33},
	           		  		{ans : data.string.ans34},
	           		  		{ans : data.string.ans35},
	           		  		{ans : data.string.ans36},
	           		  		{ans : data.string.ans37},
	           		  		{ans : data.string.ans38},
	           		  		{ans : data.string.ans39,
	           		  			correct : "correct"},
	           		  		{ans : data.string.ans40},
	           		  		],
	           				
	           	},
	           	{
	           		question : data.string.j5,
	           		answers: [
	           		  		{ans : data.string.ans41},
	           		  		{ans : data.string.ans42},
	           		  		{ans : data.string.ans43,
	           		  			correct : "correct"},
	           		  		{ans : data.string.ans44},
	           		  		{ans : data.string.ans45},
	           		  		{ans : data.string.ans46},
	           		  		{ans : data.string.ans47},
	           		  		{ans : data.string.ans48},
	           		  		{ans : data.string.ans49},
	           		  		{ans : data.string.ans50},
	           		  		],
	           	},
	           	{
	           		question : data.string.j6,
	           		answers: [
	           		  		{ans : data.string.ans51},
	           		  		{ans : data.string.ans52},
	           		  		{ans : data.string.ans53},
	           		  		{ans : data.string.ans54},
	           		  		{ans : data.string.ans55},
	           		  		{ans : data.string.ans56},
	           		  		{ans : data.string.ans57},
	           		  		{ans : data.string.ans58},
	           		  		{ans : data.string.ans59},
	           		  		{ans : data.string.ans60,
	           		  			correct : "correct"},
	           		  		],
	           		
	           	},
	           	
	           	{
	           		question : data.string.j7,
	           		answers: [
	           		  		{ans : data.string.ans61},
	           		  		{ans : data.string.ans62},
	           		  		{ans : data.string.ans63},
	           		  		{ans : data.string.ans64,
	           		  			correct : "correct"},
	           		  		{ans : data.string.ans65},
	           		  		{ans : data.string.ans66},
	           		  		{ans : data.string.ans67},
	           		  		{ans : data.string.ans68},
	           		  		{ans : data.string.ans69},
	           		  		{ans : data.string.ans70},
	           		  		],
	           	},
	           	{
	           		question : data.string.j8,
	           		answers: [
	           		  		{ans : data.string.ans71},
	           		  		{ans : data.string.ans72},
	           		  		{ans : data.string.ans73},
	           		  		{ans : data.string.ans74},
	           		  		{ans : data.string.ans75},
	           		  		{ans : data.string.ans76},
	           		  		{ans : data.string.ans77,
	           		  			correct : "correct"},
	           		  		{ans : data.string.ans78},
	           		  		{ans : data.string.ans79},
	           		  		{ans : data.string.ans80},
	           		  		],
	           		
	           	},
	           	{
	           		question : data.string.j9,
	           		answers: [
	           		  		{ans : data.string.ans81},
	           		  		{ans : data.string.ans82},
	           		  		{ans : data.string.ans83},
	           		  		{ans : data.string.ans84},
	           		  		{ans : data.string.ans85,
	           		  			correct : "correct"},
	           		  		{ans : data.string.ans86},
	           		  		{ans : data.string.ans87},
	           		  		{ans : data.string.ans88},
	           		  		{ans : data.string.ans89},
	           		  		{ans : data.string.ans90},
	           		  		],
	           		
	           	},
	           	{
	           		question : data.string.j10,
	           		answers: [
	           		  		{ans : data.string.ans91,
	           		  			correct : "correct"},
	           		  		{ans : data.string.ans92},
	           		  		{ans : data.string.ans93},
	           		  		{ans : data.string.ans94},
	           		  		{ans : data.string.ans95},
	           		  		{ans : data.string.ans96},
	           		  		{ans : data.string.ans97},
	           		  		{ans : data.string.ans98},
	           		  		{ans : data.string.ans99},
	           		  		{ans : data.string.ans100},
	           		  		],
	           		
	           	}
	           	];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		// console.log("what");
	if(answered){
		return answered;
	}
	attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
			$nextBtn.fadeIn();
		}
		else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<10){
			qA();
		}
		else if (questionCount==10){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

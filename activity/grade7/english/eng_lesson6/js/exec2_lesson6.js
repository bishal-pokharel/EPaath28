//$(document).ready(function(){
(function () {
	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.exerciseTitle);
	var content = [
	{
		question : data.string.j11,
		answers: [
		{ans : data.string.ans101},
		{ans : data.string.ans102,
			correct : "correct"},
		{ans : data.string.ans103},
		],
		img : $ref+"/exercise_images/krishna.jpg"
	},
	{
		question : data.string.j12,
		answers: [
		  		{ans : data.string.ans104,
		  			correct : "correct"},
		  		{ans : data.string.ans105},
		  		{ans : data.string.ans106},
		  		],
		  		img : $ref+"/exercise_images/tika.jpg"
	},
	{
		question : data.string.j13,
		answers: [
			  		{ans : data.string.ans107},
			  		{ans : data.string.ans108},
			  		{ans : data.string.ans109,
			  			correct : "correct"},
			  		],
			  		img : $ref+"/exercise_images/mid.png"
	},
	{
		question : data.string.j14,
		answers: [
		  		{ans : data.string.ans110},
		  		{ans : data.string.ans111,
		  			correct : "correct"},
		  		{ans : data.string.ans112},
		  		],
		  		img : $ref+"/exercise_images/shivaparvati.jpg"
				
	},
	{
		question : data.string.j15,
		answers: [
		  		{ans : data.string.ans113},
		  		{ans : data.string.ans114},
		  		{ans : data.string.ans115,
		  		 correct : "correct"},
		  		],
		  		img : $ref+"/exercise_images/mahabharat.jpg"
	}
	];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $total_page = 5;
	loadTimelineProgress($total_page,questionCount+1);
	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		// console.log("what");
		if(answered){
			return answered;
		}
		attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
		}
		else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
	loadTimelineProgress($total_page,questionCount+1);
	if(questionCount<5){
			qA();
		}
		else if (questionCount==5){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').hide(0);
			// .text("Congratulation on finishing your exercise").css({
				// "position": "absolute",
				// "top": "48%",
				// "transform": "translateY(-50%)"
			// });
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
			//ole.footerNotificationHandler.pageEndSetNotification();
		}
		$('.closebtn').click(function(){
			ole.activityComplete.finishingcall();
		  });
	});
	

})(jQuery);

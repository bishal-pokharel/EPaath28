//$(document).ready(function(){
(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  $(".title").text(data.string.exerciseTitle);
  var content = [
    {
      question: data.string.j10,
      answers: [
        { ans: data.string.ans37, correct: "correct" },
        { ans: data.string.ans38 },
        { ans: data.string.ans39 },
        { ans: data.string.ans40 }
      ],
      img: $ref + "/image/ques_10.jpg"
    }, //
    {
      question: data.string.j1,
      answers: [
        { ans: data.string.ans1 },
        { ans: data.string.ans2, correct: "correct" },
        { ans: data.string.ans3 },
        { ans: data.string.ans4 }
      ],
      img: $ref + "/image/rara_lake.jpg"
    }, //

    {
      question: data.string.j2,
      answers: [
        { ans: data.string.ans5 },
        { ans: data.string.ans6 },
        { ans: data.string.ans7 },
        { ans: data.string.ans8, correct: "correct" }
      ],
      img: $ref + "/image/manasarovar.jpg"
    }, //

    {
      question: data.string.j3,
      answers: [
        { ans: data.string.ans9 },
        { ans: data.string.ans10, correct: "correct" },
        { ans: data.string.ans11 },
        { ans: data.string.ans12 }
      ],
      img: $ref + "/image/phoksundo-lake.jpg"
    }, //

    {
      question: data.string.j4,
      answers: [
        { ans: data.string.ans13 },
        { ans: data.string.ans14 },
        { ans: data.string.ans15, correct: "correct" },
        { ans: data.string.ans16 }
      ],
      img: $ref + "/image/bon-po.jpg"
    }, //

    {
      question: data.string.j5,
      answers: [
        { ans: data.string.ans17, correct: "correct" },
        { ans: data.string.ans18 },
        { ans: data.string.ans19 },
        { ans: data.string.ans20 }
      ],
      img: $ref + "/image/danphe_rara.jpg"
    }, //
    {
      question: data.string.j6,
      answers: [
        { ans: data.string.ans21 },
        { ans: data.string.ans22 },
        { ans: data.string.ans23, correct: "correct" },
        { ans: data.string.ans24 }
      ],
      img: $ref + "/image/ques_6.jpg"
    }, //
    {
      question: data.string.j7,
      answers: [
        { ans: data.string.ans25 },
        { ans: data.string.ans26 },
        { ans: data.string.ans27, correct: "correct" },
        { ans: data.string.ans28 }
      ],
      img: $ref + "/image/ques_7.jpg"
    }, //
    {
      question: data.string.j8,
      answers: [
        { ans: data.string.ans29 },
        { ans: data.string.ans30 },
        { ans: data.string.ans31 },
        { ans: data.string.ans32, correct: "correct" }
      ],
      img: $ref + "/image/ques_8.jpg"
    }, //
    {
      question: data.string.j9,
      answers: [
        { ans: data.string.ans33 },
        { ans: data.string.ans34 },
        { ans: data.string.ans35 },
        { ans: data.string.ans36, correct: "correct" }
      ],
      img: $ref + "/image/ques_9.jpg"
    } //
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();
  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);

  var correctlyanswered = 0;
  $board.on("click", ".neutral", function() {
    // console.log("what");
    if (answered) {
      return answered;
    }
    attemptcount++;
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      play_correct_incorrect_sound(true);
    } else {
      $this.addClass("wrong").removeClass("neutral");
      play_correct_incorrect_sound(false);
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 10) {
      qA();
    } else if (questionCount == 10) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title").hide(0);

      // .html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
      // "position": "absolute",
      // "top": "48%",
      // "transform": "translateY(-50%)"
      // });
      $(".finishtxt")
        .append(
          "<br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .show(0);

      $(".closebtn").on("click", function() {
        ole.activityComplete.finishingcall();
      });
    }
  });
})(jQuery);

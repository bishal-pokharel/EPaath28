$(document).ready(function() {
	// $('.title').text(data.string.exerciseTitle);
	$('.title').text(data.string.ex1title);
	var content = [ {
		lakes : data.string.td1,
		altitudes : data.string.tdvalues1,
		information : data.string.inf1
	},//
	{
		lakes : data.string.td2,
		altitudes : data.string.tdvalues2,
		information : data.string.inf2
	},//

	{
		lakes : data.string.td3,
		altitudes : data.string.tdvalues3,
		information : data.string.inf3
	},//

	{
		lakes : data.string.td4,
		altitudes : data.string.tdvalues4,
		information : data.string.inf4
	},//
	{
		lakes : data.string.td5,
		altitudes : data.string.tdvalues5,
		information : data.string.inf5
	},//
	];
	console.log(content);
	displayDatas();
	function displayDatas()
     {
		appendTable='<tr>';
		appendBody='<tr>';
    	 var datas=$.each(content , function (key,value){
    		 lake=value.lakes;
    		 altitudes=value.altitudes;
    		 information=value.information;
    		 console.log(information);
    		 console.log(lake);
    		 console.log(altitudes);
    		 //now we will start keeping the files into respective location-->append
    		 appendTable+='<th scope="col">' + lake + '</th>';
    		 appendBody+='<td>' + altitudes + '</td>';
    		 //console.log(appendBody);
    		 
    		 //make the information section here
    		 appendInformation_Lakes='<div id="' + lake +'_lake" class="info_bar">';
    		 appendInformation_Lakes+='<p class="lake_information">';
    		 appendInformation_Lakes+=information;
    		 appendInformation_Lakes+'</p>';
    		 appendInformation_Lakes+='</div>';
    		 $(".lakes_information").append(appendInformation_Lakes);
    		 
    	 });
    	 appendTable+='</tr>';
    	 appendBody+='</tr>';
    	 $("#list_lakes").append(appendTable);
    	 $("#lakes_altitudes").append(appendBody);
    	
    	 console.log(appendInformation_Lakes);
     }

});

var dd = 0;
$(document).ready(function() {
  loadTimelineProgress(1, 1);
  $("#Rara_lake").hide(0);
  $("#Mansarovar_lake").hide(0);
  $("#Phewa_lake").hide(0);
  $("#Phoksundo_lake").hide(0);
  $("#Tilicho_lake").hide(0);
  createGraph("#data-table", ".chart");
  function createGraph(data, container) {
    var bars = [];
    var figureContainer = $('<div id="figure"></div>');
    var graphContainer = $('<div class="graph"></div>');
    var barContainer = $('<div class="bars"></div>');
    var data = $(data);
    var container = $(container);
    var chartData;
    var chartYMax;
    var columnGroups;
    var barTimer;
    var graphTimer;

    var tableData = {
      chartData: function() {
        var chartData = [];
        data.find("tbody td").each(function() {
          chartData.push($(this).text());
        });
        return chartData;
      },
      chartHeading: function() {
        var chartHeading = data.find("caption").text();
        return chartHeading;
      },
      chartLegend: function() {
        var chartLegend = [];
        data.find("tbody th").each(function() {
          chartLegend.push($(this).text());
        });
        return chartLegend;
      },
      chartYMax: function() {
        var chartData = this.chartData();
        // Round off the value
        var chartYMax =
          Math.ceil(Math.max.apply(Math, chartData) / 1000) * 1000;
        return chartYMax;
      },
      yLegend: function() {
        var chartYMax = this.chartYMax();
        var yLegend = [];
        var yAxisMarkings = 5;
        for (var i = 0; i < yAxisMarkings; i++) {
          yLegend.unshift((chartYMax * i) / (yAxisMarkings - 1) / 1000);
        }
        return yLegend;
      },
      xLegend: function() {
        var xLegend = [];
        data.find("thead th").each(function() {
          xLegend.push($(this).text());
        });
        return xLegend;
      },
      columnGroups: function() {
        var columnGroups = [];
        var columns = data.find("tbody tr:eq(0) td").length;
        for (var i = 0; i < columns; i++) {
          columnGroups[i] = [];
          data.find("tbody tr").each(function() {
            columnGroups[i].push(
              $(this)
                .find("td")
                .eq(i)
                .text()
            );
          });
        }
        return columnGroups;
      }
    };

    chartData = tableData.chartData();
    chartYMax = tableData.chartYMax();
    columnGroups = tableData.columnGroups();
    $.each(columnGroups, function(i) {
      var barGroup = $('<div class="bar-group"></div>');
      for (var j = 0, k = columnGroups[i].length; j < k; j++) {
        dd++;
        var barObj = {};
        barObj.label = this[j];
        barObj.height = Math.floor((barObj.label / chartYMax) * 100) + "%";
        barObj.bar = $(
          '<div class="bar fig' +
            j +
            '" id=' +
            dd +
            "><span>" +
            barObj.label +
            "</span></div>"
        ).appendTo(barGroup);
        bars.push(barObj);
      }

      barGroup.appendTo(barContainer);
    });

    var chartHeading = tableData.chartHeading();
    var heading = $("<h4>" + chartHeading + "</h4>");
    heading.appendTo(figureContainer);
    var chartLegend = tableData.chartLegend();
    var legendList = $('<ul class="legend"></ul>');
    $.each(chartLegend, function(i) {
      console.log(legendList);
      var listItem = $(
        '<li><span class="icon fig' + i + '"></span>' + this + "</li>"
      ).appendTo(legendList);
    });
    legendList.appendTo(figureContainer);

    var xLegend = tableData.xLegend();
    var xAxisList = $('<ul class="x-axis"></ul>');
    $.each(xLegend, function(i) {
      var listItem = $("<li><span>" + this + "</span></li>").appendTo(
        xAxisList
      );
    });
    xAxisList.appendTo(graphContainer);

    var yLegend = tableData.yLegend();
    var yAxisList = $('<ul class="y-axis"></ul>');
    $.each(yLegend, function(i) {
      var listItem = $("<li><span>" + this + "</span></li>").appendTo(
        yAxisList
      );
    });
    yAxisList.appendTo(graphContainer);

    barContainer.appendTo(graphContainer);

    graphContainer.appendTo(figureContainer);
    figureContainer.appendTo(container);
    function displayGraph(bars, i) {
      if (i < bars.length) {
        $(bars[i].bar).animate(
          {
            height: bars[i].height
          },
          1500
        );
        barTimer = setTimeout(function() {
          i++;
          displayGraph(bars, i);
        }, 1000);
      }
    }

    function resetGraph() {
      $.each(bars, function(i) {
        $(bars[i].bar)
          .stop()
          .css("height", 0);
      });

      clearTimeout(barTimer);
      clearTimeout(graphTimer);

      graphTimer = setTimeout(function() {
        displayGraph(bars, 0);
      }, 200);
    }

    $("#reset-graph-button").click(function() {
      resetGraph();
      return false;
    });

    resetGraph();
  }
  $(".fig0").hover(function() {
    console.log($(this).attr("id"));
    if ($(this).attr("id") == 1) {
      $("#1").css("background-color", "white");
      $(".overflow").css(
        "background-image",
        "url(activity/grade8/english/eng_lesson7/image/rara.jpg)"
      );
      $("#Rara_lake").show(0);
      $(".fig0").mouseout(function() {
        $("#Rara_lake").hide(0);
        $("#1").css("background-color", "orange");
        $(".overflow").css(
          "background-image",
          "url(activity/grade8/english/eng_lesson7/image/lakes.jpg)"
        );
        $("#rara_lake").fadeOut(800);
      });
    } //
    else if ($(this).attr("id") == 2) {
      $("#2").css("background-color", "white");
      $(".overflow").css(
        "background-image",
        "url(activity/grade8/english/eng_lesson7/image/phoksondo.jpg)"
      );
      $("#Phoksundo_lake").show(0);
      $(".fig0").mouseout(function() {
        $("#Phoksundo_lake").hide(0);
        $("#2").css("background-color", "orange");
        $(".overflow").css(
          "background-image",
          "url(activity/grade8/english/eng_lesson7/image/lakes.jpg)"
        );
        $("#Phoksundo_lake").fadeOut(800);
      });
    } //
    else if ($(this).attr("id") == 3) {
      $("#3").css("background-color", "white");
      $(".overflow").css(
        "background-image",
        "url(activity/grade8/english/eng_lesson7/image/tilicho.jpg)"
      );
      $("#Tilicho_lake").show(0);
      $(".fig0").mouseout(function() {
        $("#Tilicho_lake").hide(0);
        $("#3").css("background-color", "orange");
        $(".overflow").css(
          "background-image",
          "url(activity/grade8/english/eng_lesson7/image/lakes.jpg)"
        );
        $("#Tilicho_lake").fadeOut(800);
      });
    } //
    else if ($(this).attr("id") == 4) {
      $("#4").css("background-color", "white");
      $(".overflow").css(
        "background-image",
        "url(activity/grade8/english/eng_lesson7/image/phewa.jpg)"
      );
      $("#Phewa_lake").show(0);
      $(".fig0").mouseout(function() {
        $("#Phewa_lake").hide(0);
        $("#4").css("background-color", "orange");
        $(".overflow").css(
          "background-image",
          "url(activity/grade8/english/eng_lesson7/image/lakes.jpg)"
        );
        $("#Phewa_lake").fadeOut(800);
      });
    } //
    else if ($(this).attr("id") == 5) {
      $("#5").css("background-color", "white");
      $(".overflow").css(
        "background-image",
        "url(activity/grade8/english/eng_lesson7/image/manasovar.jpg)"
      );
      $("#Mansarovar_lake").show(0);
      $(".fig0").mouseout(function() {
        $("#Mansarovar_lake").hide(0);
        $("#5").css("background-color", "orange");
        $(".overflow").css(
          "background-image",
          "url(activity/grade8/english/eng_lesson7/image/lakes.jpg)"
        );
        $("#Mansarovar_lake").fadeOut(800);
        // $('#activity-page-continue-btn').show(0);
        // $('#activity-page-continue-btn').click(function(){
        ole.footerNotificationHandler.pageEndSetNotification();
        // });
      });
    } //
  });
});

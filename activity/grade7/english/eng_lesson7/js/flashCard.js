$(".title").text(data.string.exerciseTitle);
$(document).ready(function() {
  $(".title").text(data.string.exerciseTitle2);
  var content = [
    {
      words: data.string.word1,
      description: data.string.des1,
      img: $ref + "/image/sea_level.jpg"
    }, //
    {
      words: data.string.word2,
      description: data.string.des2,
      img: $ref + "/image/species.jpg"
    }, //

    {
      words: data.string.word3,
      description: data.string.des3,
      img: $ref + "/image/wild_fowl.jpg"
    }, //

    {
      words: data.string.word4,
      description: data.string.des4,
      img: $ref + "/image/migration.jpg"
    }, //
    {
      words: data.string.word5,
      description: data.string.des5,
      img: $ref + "/image/resident.jpg"
    }, //
    {
      words: data.string.word6,
      description: data.string.des6,
      img: $ref + "/image/altitude.jpg"
    }, //
    {
      words: data.string.word7,
      description: data.string.des7,
      img: $ref + "/image/alipine.jpg"
    }, //
    {
      words: data.string.word8,
      description: data.string.des8,
      img: $ref + "/image/pre.jpg"
    }, //
    {
      words: data.string.word9,
      description: data.string.des9,
      img: $ref + "/image/sacred.jpg"
    }, //
    {
      words: data.string.word10,
      description: data.string.des10,
      img: $ref + "/image/glacial.jpg"
    }, //
    {
      words: data.string.word11,
      description: data.string.des11,
      img: $ref + "/image/trans.jpg"
    }, //
    {
      words: data.string.word12,
      description: data.string.des12,
      img: $ref + "/image/pilgrim.jpg"
    }, //
    {
      words: data.string.word13,
      description: data.string.des13,
      img: $ref + "/image/epic.jpg"
    }, //
    {
      words: data.string.word14,
      description: data.string.des14,
      img: $ref + "/image/water.jpg"
    }, //
    {
      words: data.string.word15,
      description: data.string.des15,
      img: $ref + "/image/ref.jpg"
    }, //
    {
      words: data.string.word16,
      description: data.string.des16,
      img: $ref + "/image/Barahi.jpg"
    }, //
    {
      words: data.string.word17,
      description: data.string.des17,
      img: $ref + "/image/shore.jpg"
    }, //
    {
      words: data.string.word18,
      description: data.string.des18,
      img: $ref + "/image/densely.jpg"
    }, //
    {
      words: data.string.word19,
      description: data.string.des19,
      img: $ref + "/image/scriptures.jpg"
    }, //
    {
      words: data.string.word20,
      description: data.string.des20,
      img: $ref + "/image/manifest.jpg"
    }, //
    {
      words: data.string.word21,
      description: data.string.des21,
      img: $ref + "/image/sin.jpg"
    }, //
    {
      words: data.string.word22,
      description: data.string.des22,
      img: $ref + "/image/ceremony.jpg"
    } //
  ];
  console.log(content);
  displaycontent();
  function displaycontent() {
    var words = $.each(content, function(key, value) {
      words = value.words;
      description = value.description;
      image = value.img;
      // console.log(description);
      appendTab = "<li>";
      appendTab +=
        '<a href="#' + words + '" data-toggle="tab"  >' + words + "</a>";
      appendTab += "</li>";
      $("#myTab").append(appendTab);

      appendcontent = '<div id="' + words + '" class="tab-pane ">';
      appendcontent += '<div class="f1_container">';
      appendcontent += '<div class="shadow f1_card">';
      appendcontent += '<div class="front face">';
      appendcontent += "<p>" + words + "</p>";
      appendcontent += "</div>";
      appendcontent += '<div class="back face center">';
      appendcontent +=
        '<img src="' + image + '" class="tab_image img-responsive"/>';
      appendcontent += '<p class="textroll">';
      appendcontent += description;
      appendcontent += "</p>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      $("#myTabContent").append(appendcontent);
    });
  }
  /*
   * function qA() { var source = $('#qA-templete').html(); var
   * template = Handlebars.compile(source); var html =
   * template(content); //$board.html(html); // console.log(html); }
   *
   * qA();
   */
});
$(document).ready(function() {
  loadTimelineProgress(1, 1);
  $(".f1_container").click(function() {
    //alert("ok");
    $(this).toggleClass("active");
  });
  ole.footerNotificationHandler.pageEndSetNotification();
});

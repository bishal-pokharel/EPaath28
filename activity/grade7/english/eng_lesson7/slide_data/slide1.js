$(document).ready(function() {
  soundAsset = $ref+"/sounds/";
  var s1Audio = new buzz.sound(soundAsset+"s1_p1.ogg");

  var lakedesc = [
    {
      lakes: data.string.lake1
    } //
  ];
  console.log(lakedesc);
  displaylake();
  loadTimelineProgress(1, 1);
  function displaylake() {
    var images = $.each(lakedesc, function(key, values) {
      lake_desc = values.lakes;

      appendLakes = '<p class="paragraph_rara">';
      appendLakes += lake_desc;
      appendLakes += "</p>";

      $(".rara").append(appendLakes);
    });
    $(".rara").hide(0);
    $(".rara_heading").text("Lakes");
  }

  $(".closebtn").click(function() {
    $(".rara_heading").text("Rara Lake");
    $(this).parent().hide(0);
    s1Audio.play();
    s1Audio.bind('ended', function(){
      ole.footerNotificationHandler.pageEndSetNotification();
    });
    // setTimeout(function() {
    //   ole.footerNotificationHandler.pageEndSetNotification();
    // }, 1000);
    $(".rara").show(0);
  });
});

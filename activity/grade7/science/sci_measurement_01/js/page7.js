$(function()
{
	
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	$(".titleMe2").html(data.string.p7_1);

	var $weight=[5,15,30];

	var $quest=1;
	getFunction($quest);
	
	$("#activity-page-next-btn-enabled").click(function(){
		$quest++;
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);

		getFunction($quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		$quest--;
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		getFunction($quest);
	});

	$(".wholebx").on("click","#start",function(){
		$(this).fadeOut(10);
		var rand_weight=Math.floor((Math.random()*3)+1); 

		var gift_weight=$weight[(rand_weight-1)];
		
		showGift(gift_weight);
	});//btnclick

	$(".wholebx").on("click","#restart",function(){
		$(this).fadeOut(10);

		var rand_weight=Math.floor((Math.random()*3)+1); 
		var gift_weight=$weight[(rand_weight-1)];
		showGift(gift_weight);
	});
	$(".allbck").on("click","#totalCross",function(){
		var url = 'activity.html?id=scimes01&lang='+$lang+'&pg=5&scId=0';
		window.location.href = url;
	});
});

function getFunction(qno)
{
	loadTimelineProgress(2,qno);
	switch(qno)
	{
		case 1:
		{
			
			case1();
			break;
		}
		case 2:
		{
		
			case2();
			break;
		}
	}
}

function case1()
{
	//$("#totalCross").fadeOut(10);
	var datavar={txt1:data.string.p7_2, txt2:(data.string.p7_3 + '<span style="color:#A00">'+data.string.p7_3_extra+'</span>')};
	

	var source   = $("#template-1").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		$("#text1").delay(200).fadeIn(10,function()
		{
			$(this).addClass("animated fadeInLeftBig");
			$("#text2").delay(1500).fadeIn(10,function()
			{
				$(this).addClass("animated fadeInLeftBig");
				
				$("#activity-page-next-btn-enabled").fadeIn(10);
			});
		});
	});
}

function case2()
{
	$("#activity-page-prev-btn-enabled").fadeIn(10);
	//$("#totalCross").fadeIn(10);

	var datavar={txt3:data.string.p7_4,
		start:data.string.p7_5,
		restart:data.string.p7_6
	};
	

	var source   = $("#template-2").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		
		
	});
}

function showGift(weight)
{
	
	$('#gift div').html(weight + " Kg");
	$('#gift').fadeIn(10,function(){

		$("#leftbalance").animate({'top':'65%'},1000);
		$("#rightbalance").animate({'top':'59%'},1000);

		
		$("#dropbox").animate({'bottom':'38%'},1000);
		
		$("#scalebalance").addClass('rotateLeft');
		$("#gift").animate({'top':'51%'},1000);
		$("#wtlist").delay(1000).fadeIn(10,function(){
			$(this).addClass('draggables')
		});
	});
	
	
	

	$("#wtlist img").draggable({ revert: "invalid", helper: "clone",});

	$("#dropbox").droppable({
        drop: function( event, ui ) {
        	var id=ui.draggable.attr('id');

        	var src=ui.draggable.attr('src');
        	$(this).html("<img src='"+src+"' />");
        	if(parseInt(id)==parseInt(weight))
        	{
        		$("#wtlist").fadeOut(10);
        		$("#leftbalance").animate({'top':'62%'},1000);
        		$("#rightbalance").animate({'top':'62%'},1000);
        		$("#gift").animate({'top':'48%'},1000);
        		$("#dropbox").animate({'bottom':'35%'},1000);

        		$("#wtlist").removeClass('draggables');

        		$("#scalebalance").removeClass('rotateLeft');
        		$("#scalebalance").removeClass('rotateRight');
        		
        		setTimeout(function(){
        			$('#gift').fadeOut(10);
        			
        			$("#dropbox").html("<img src='"+$ref+"/images/page7/8.png' />");
        			$("#restart").fadeIn(10);
        		},2000);
        		ole.footerNotificationHandler.pageEndSetNotification();
        	}
        	else if(parseInt(id)<parseInt(weight))
        	{
        		$("#leftbalance").animate({'top':'65%'},1000);
        		$("#rightbalance").animate({'top':'59%'},1000);

        		$("#gift").animate({'top':'51%'},1000);
        		$("#dropbox").animate({'bottom':'38%'},1000);
        		$("#scalebalance").removeClass('rotateRight');
        		$("#scalebalance").addClass('rotateLeft');

        	}
        	else if(parseInt(id)>parseInt(weight))
        	{
        		$("#leftbalance").animate({'top':'59%'},1000);
        		$("#rightbalance").animate({'top':'65%'},1000);
        		
        		$("#gift").animate({'top':'45%'},1000);
        		$("#dropbox").animate({'bottom':'33%'},1000);
        		$("#scalebalance").removeClass('rotateLeft');
        		$("#scalebalance").addClass('rotateRight');
        	}
        }	 
        	
    });
}

$(function()
{
	
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);
	
	$(".titleMe2").html(data.string.p6_1);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		quest++;
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);

		getFunction(quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		quest--;
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		getFunction(quest);
	});

	$(".wholebx").on("click","#totalCross",function(){

		var url = 'activity.html?id=scimes01&lang='+$lang+'&pg=5&scId=0';
		window.location.href = url;
	});
});


function getFunction(qno)
{
	loadTimelineProgress(7,qno);
	switch(qno)
	{
		case 1:
		{
			$(".titleMe2").html(data.string.p6_1);
			case1();
			break;
		}
		case 2:
		case 3:
		case 4:
		case 6:
		{
			$(".titleMe2").fadeIn(10);
			case2(qno);
			break;
		}
		case 5:
		{
			$(".titleMe2").fadeIn(10);
			case5();
			break;
		}
		case 7:
		{

			case7();
			break;
		}
		
	}
}

function case1()
{
	var datavar={};
	

	var source   = $("#template-1").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

			$(".wholebx").find("#p6_2").html(data.string.p6_2);
			$(".wholebx").find("#p6_2_a").html(data.string.p6_2_a);
			$(".wholebx").find("#p6_2_b").html(data.string.p6_2_b);
			$(".wholebx").find("#p6_2_c").html(data.string.p6_2_c);

			for(var i=3;i<=6;i++)
			{
				$(".wholebx").find("#p6_"+i).html(data.string["p6_"+i]);
				$(".wholebx").find("#p6_"+i+"_b").html(data.string["p6_"+i+"_b"]);
			}

			$("#activity-page-next-btn-enabled").fadeIn(10);
	});
}


function case2(qno)
{
	var datavar;
	if(qno==2)
	{
		$(".titleMe2").html(data.string.p6_3);
		datavar={
		
			millidesc:data.string.p6_3_a,
			imags33:$ref+"/images/page6/03.png",
			class1:"class1",
			class2:"imgs"
		};
	}
	else if(qno==3)
	{
		$(".titleMe2").html(data.string.p6_4);
		datavar={
			millidesc:data.string.p6_4_a,
			imags33:$ref+"/images/page6/04.png",
			class1:"class1",
			class2:"imgss"
		};
	}
	else if(qno==4)
	{
		$(".titleMe2").html(data.string.p6_5);
		datavar={
			millidesc:data.string.p6_5_a,
			imags33:$ref+"/images/page6/02.png",
			millidesc2:data.string.p6_5_c,
			imags34:$ref+"/images/page6/01.png",
			class1:"class2",
			class2:"imgs1"
		};
	}
	else if(qno==6)
	{
		$(".titleMe2").html(data.string.p6_6);
		datavar={
			millidesc:data.string.p6_6_a,
			imags33:$ref+"/images/page6/height/4.png",
			class1:"class1",
			class2:"imgs"
		};
	}
	

	var source   = $("#template-2").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);

	}).fadeIn(10,function(){

		$("#desc").delay(100).fadeIn(10,function(){
			$(this).addClass('animated bounceIn');
			if(qno==4)
			{
				$("#desc2").delay(1500).fadeIn(10,function(){
					$(this).addClass('animated bounceIn');
					
					$("#activity-page-next-btn-enabled").delay(1500).fadeIn(10);
					$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10);
				});
			}
			else
			{
				$("#activity-page-next-btn-enabled").delay(1500).fadeIn(10);

				$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10);
			}

		});	
	});
}

function case5()
{
	$(".titleMe2").html(data.string.p6_5);
	var datavar={
			bgimg:$ref+"/images/page6/height/1.png",
			htImg1:$ref+"/images/page6/height/2.png",
			htImg2:$ref+"/images/page6/height/3.png",
			httxt1:data.string.p6_5_d,
			submit:data.string.p6_5_h,
			httxt2:data.string.p6_5_e
		};

	var source   = $("#template-3").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		$("#htbtn").click(function(){
	
			$(".htAns").fadeOut(10);
			$(".htAns2").fadeOut(10);
			var pattern=/^\d*\.?\d*$/;



			var val=$("#htTxtbx").val();

			var txtval=$("#htTxtbx").val();
			
			if(txtval.charAt(0)==".") txtval=0+txtval;

			if(!val.match(pattern))
			{
				$("#htTxtbx").val('');
			}
			else if(txtval=="" || txtval=='0')
			{
				
				$(".htAns").html(data.string.p6_5_i);
				$(".htAns").fadeIn(10);	
					
			}
			else
			{

				$(this).fadeOut();
				
				var txts=parseFloat(txtval);

				var boywd,scbottom;
				
				if(txts<0.5)
				{
					if(txts==(0.5/2))
					{
						boywd=6.5;
						scbottom=15;
					}
					else if(txts<(0.5/2))
					{
						boywd=6;
						scbottom=14;
					}
					else if(txts>(0.5/2))
					{
						boywd=9;
						scbottom=19;
					}
				}
				else if(txts==0.5)
				{	
					scbottom=24;
					boywd=11.5;
				}
				else if(txts<1)
				{
					if(txts==(0.75))
					{
						
						boywd=17;
						scbottom=33;
					}
					else if(txts<(0.75))
					{
						boywd=12;
						scbottom=25;
					}
					else if(txts>(0.75))
					{
						boywd=19;
						scbottom=36;
						
					}
				}
				else if(txts==1)
				{
					scbottom=42;
					boywd=22;
					
				}
				else if(txts<1.5)
				{
					
					if(txts==(1.25))
					{
						boywd=27;
						scbottom=51;
						
					}
					else if(txts<(1.25))
					{
						boywd=25;
						scbottom=47.5;
						
					}
					else if(txts>(1.25))
					{
						boywd=29;
						scbottom=54;
						
					}
				}
				else if(txts==1.5)
				{
					boywd=32.5;
						scbottom=60.5;
					
				}
				else if(txts<2)
				{
					if(txts==(1.75))
					{
						boywd=37;
						scbottom=68;
						
					}
					else if(txts<(1.75))
					{
						boywd=35;
						scbottom=64;
						
					}
					else if(txts>(1.75))
					{
						boywd=39;
						scbottom=72;
						
					}
				}
				else if(txts==2)
				{
					boywd=43;
					scbottom=79;
				}
				else if(txts>2)
				{
					boywd=45;
					scbottom=82;
				}
				
				$(".htImg").animate({'bottom': scbottom+"%"}, 500,function(){
					$(".htImg2").css({'width':boywd+"%"});
					$(".htAns").html(data.string.p6_5_f);
					ole.parseToolTip(".htAns",'YYYYYY','',false,val);
					$(".htAns2").html(data.string.p6_5_g);

					$(".htAns").fadeIn(10);	
					$(".htAns2").fadeIn(10,function(){
						$("#htbtn").fadeIn(10);	
						$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
						$("#activity-page-prev-btn-enabled").delay(500).fadeIn(10);
					});	

				});
			}
		});//click func
	});
}


function case7()
{
	var datavar={
				maptitle:data.string.p6_6_c,
				mapimg:$ref+"/images/page6/height/4.png",
				mapcar:$ref+"/images/page6/height/5.png",
				p6_6_d:data.string.p6_6_d,
				p6_6_e:data.string.p6_6_e,
				p6_6_f:data.string.p6_6_f,
				p6_6_h:data.string.p6_6_h,
				p6_6_g:data.string.p6_6_g,
				km:data.string.p6_6_i
	};
	var source   = $("#template-4").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){
		$("#activity-page-prev-btn-enabled").delay(500).fadeIn(10);
		//$("#totalCross").delay(500).fadeIn(10);

		
		$(".titleMe2").fadeOut(10);



		var mid_hill_district_name = new Array('Dandeldhura', 'Silgadhi', 'Dailekh', 'Baglung', 'Pokhara', 'Gorkha', 'Dhulikhel', 'Diktel', 'Phidim');
	var mid_hill_distances = [
		[0, 70, 422, 735, 662, 689, 797, 1060, 1223],
		[70, 0, 486, 796, 726, 753, 861, 1124, 1287],
		[422, 486, 0, 618, 545, 572, 681, 943, 1106],
		[735, 796, 618, 0, 73, 182, 306, 640, 802],
		[662, 726, 545, 73, 0, 109, 233, 567, 730],
		[689, 753, 572, 182, 109, 0, 173, 507, 670],
		[797, 861, 681, 306, 233, 173, 0, 475, 638],
		[1060, 1124, 943, 640, 567, 507, 475, 0, 480], 
		[1223, 1287, 1106, 802, 730, 670, 638, 480, 0]
 	];

 var east_west_district_name = new Array('Mahendranagar', 'Dhangadhi', 'Nepalgunj', 'Ghorahi', 'Butwal', 'Parasi', 'Hetaunda', 'Kalaiya', 'Jaleshwar', 'Siraha', 'Ineruwa', 'Chandragadhi');
	 var east_west_distances = [
	 	[0, 57, 209, 324, 434, 465, 625, 688, 796, 821, 911, 1015],
	 	[57, 0, 181, 296, 407, 437, 597, 660, 768, 793, 883, 987],
	 	[209, 181, 0, 141, 252, 282, 443, 505, 614, 639, 728, 833],
	 	[324, 296, 141, 0, 151, 181, 342, 404, 513, 538, 627, 732], 
	 	[434, 407, 252, 151, 0, 30, 191, 253, 362, 387, 476, 581],
		[465, 437, 282, 181, 30, 0, 178, 241, 349, 375, 464, 568],
	 	[625, 597, 443, 342, 191, 178, 0, 63, 171, 196, 286, 390],
		[688, 660, 505, 404, 253, 241, 63, 0, 175, 201, 290, 394],
	[796, 768, 614, 513, 362, 349, 171, 175, 0, 91, 180, 284],
	 	[821, 793, 639, 538, 387, 375, 196, 201, 91, 0, 125, 230],
	 	[911, 883, 728, 627, 476, 464, 286, 290, 180, 125, 0, 104],
		[1015, 987, 833, 732, 581, 568, 390, 394, 284, 230, 104, 0]
	 ]

	distance_cal(mid_hill_district_name, mid_hill_distances,east_west_district_name, east_west_distances);
			
	});

ole.footerNotificationHandler.pageEndSetNotification();
		
}
var count = 0;
	var click1;
	var highway1,highway2;

function countdistanceval(val, distance)
{
	var current = val;
	
	current = current + 5; 
	
	$("#distval").html(current);
	if(current > distance){
		$("#distval").html(distance);
		$("#distance").html(distance);
		$('.districtName').show(0);
	} else {    
		setTimeout(function(){countdistanceval(current,distance)}, 10);
	}
}   

function reset(){
		$('#distance').html(0);
		
		$("#car_dist").removeClass("flipcar");
		$("#distval").removeClass("flipcar");
		$("#distval").html("");
						
		$("#car_dist").hide(0);

		$('#from_district').html(data.string.p6_6_h);
		$('#to_district').html(data.string.p6_6_h);
		
		$('.districtName').show(0);
		count = 0;
		click1 = "";
		highway1="";
		highway2="";
}
function distance_cal(district_name, distances,district_name2, distances2){
	
	var leftdst,leftdst1;
	var topdst,topdst1;
	var dur;
	
	var valtop,shwtop , leftint,leftint2;
	
	

	$('.districtName').bind('click', function(e){

		var leftPoint=$(this).css('left');
		var topPoint=$(this).css('top');
	
		if(count>=2){
			reset();
		}

		count++;
		if(count == 1)
		{
			var clicked=$(this).attr('id');		

			var txtVal=data.string[clicked]		
			$('#from_district').html(txtVal);
			

			leftBx1=parseInt(leftPoint);
			topPoint1=parseInt(topPoint);

			click1=district_name.indexOf(clicked);

			if(click1==-1)
			{

				click1=district_name2.indexOf(clicked);
				highway1=2;
			}
			else highway1=1;
			var showPoint=(parseInt(topPoint)-40)+"px";

			$("#car_dist").css({"left":leftPoint,"top":showPoint}).show(0);	
		}
		else if (count == 2)
		{
			$('.districtName').hide(0);
			var clicked=$(this).attr('id');
			var click2=district_name.indexOf(clicked);
			highway2=1;
			

			if(click2==-1)
			{
				highway2=2;
				click2=district_name2.indexOf(clicked);
				
			
				if(click2==-1) reset();
				else if(highway2!=highway1) reset();
				else
				{ 
				 	
				 	var txtVal2=data.string[clicked];

				 	$("#to_district").html(txtVal2);
					
					var distanceval=distances2[click1][click2];
					
					
					leftBx2=parseInt(leftPoint);
					topPoint2=parseInt(topPoint);
						
					if(leftBx2<leftBx1)
					{
						$("#car_dist").addClass("flipcar");
						$("#distval").addClass("flipcar");
					}
					else
					{	
						$("#car_dist").removeClass("flipcar");
						$("#distval").removeClass("flipcar");
					}
					if(distanceval<500) dur=1000;
					else dur=5000;
					
					var showPoint=(parseInt(topPoint)-40)+"px";
					$("#car_dist").animate({"top":showPoint,"left":leftPoint},dur);
					
					countdistanceval(5, distanceval);
				}
			}//click2
			else
			{
				highway2=1;		
				
				if(highway2!=highway1) reset();
				else
				{
					var txtVal3=data.string[clicked];
					$("#to_district").html(txtVal3);
					var distanceval=distances[click1][click2];
					
					leftBx2=parseInt(leftPoint);
					topPoint2=parseInt(topPoint);			
					 

					countdistanceval(5, distanceval);	
					if(leftBx2<leftBx1)
					{
						$("#car_dist").addClass("flipcar");
						$("#distval").addClass("flipcar");
					
					}
					else
					{	
						$("#car_dist").removeClass("flipcar");
						$("#distval").removeClass("flipcar");
					}
					if(distanceval<500) dur=1000;
					else dur=5000;
					
					var showPoint=(parseInt(topPoint)-40)+"px";
					$("#car_dist").animate({"top":showPoint,"left":leftPoint},dur,function(){
					
						//$("#totalCross").fadeIn(10);
					});
				}
			}
		}
	});
}
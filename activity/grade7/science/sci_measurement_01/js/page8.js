$(function()
{
	
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	$(".titleMe2").html(data.string.p8_1);

	var $quest=1;

	loadTimelineProgress(2,1);
	getFunction($quest);
	
	$("#activity-page-next-btn-enabled").click(function(){
		$quest++;
		loadTimelineProgress(2,$quest);
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);

		getFunction($quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		$quest--;
		loadTimelineProgress(2,$quest);
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		getFunction($quest);
	});
	$(".allbck").on("click","#totalCross",function(){
		var url = 'activity.html?id=scimes01&lang='+$lang+'&pg=5&scId=0';
		window.location.href = url;
	});


	$(".wholebx").on("click",".refresh",function(){
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);
		getFunction(2);
	});


	$(".wholebx").on("click","#btnval",function(){

		var pattern=/^\d*\.?\d*$/;
		
		var val=$("#heightval").val();
		var newValme=parseFloat(val);
		if(!val.match(pattern))
		{
			$("#heightval").val('');
		}
		else if(val=="")
		{
			
		}
		else if(newValme>25)
		{
			alert(newValme+"s "+data.string.p8_13);
		}
		else
		{
			var millis=val*1000;
			if(millis>9580)
			{

				
				var changeText=((millis-9580)/1000)+"s";
				$("#conc2").html(data.string["p8_11_2"]);

				ole.parseToolTip("#conc2","YYYYYYY",'',false,changeText);

			}
			else if(millis==9580)
			{
				
				$("#conc2").html(data.string["p8_11_3"]);
			}
			else if(millis<9580)
			{
				
				$("#conc2").html(data.string["p8_11_1"]);
			}
			$(this).fadeOut(10);
			$("#heightval").attr('disabled','disabled');
			$(".track").fadeIn(10,function(){

				$(this).addClass('animated fadeInDown');

				$(".runner").delay(1500).fadeIn(10,function(){

					var sec=parseFloat(val)*1000;	
					$(".runner").delay(200).animate({"left":"86%"},{
						duration: sec,
						start:function(){
							$(".runner").find('img').attr('src',$ref+'/images/page8/2.gif');
						},
						complete:function(){
							$(".runner").find('img').attr('src',$ref+'/images/page8/11.gif');

							$(".summary").delay(1000).fadeIn(10,function(){
								$(".runner").fadeOut(10);

								$("#refreshId").html(getReloadBtn());
								$(".refresh").delay(500).fadeIn(10);
								$("#activity-page-prev-btn-enabled").delay(500).fadeIn(10);
								//$("#totalCross").delay(500).fadeIn(10);
							});
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					});
				});
			});
		}
	});
	
});

function getFunction(qno)
{
	switch(qno)
	{
		case 1:
		{
			
			case1();
			break;
		}
		case 2:
		{
		
			case2();
			break;
		}
	}
}

function case1()
{
	var datavar={txt1:data.string.p8_2, 
		txt2:data.string.p8_3,
		image1:$ref+"/images/page8/clockface.jpg",
		image2:$ref+"/images/page8/sechand.png",
		image3:$ref+"/images/page8/hourhand.png",
		image4:$ref+"/images/page8/minhand.png",
		text3:data.string.p8_4,
		text4:data.string.p8_5,
		text5:data.string.p8_6,
		text6:data.string.p8_7,
		text7:data.string.p8_8
	};
	
	RunClock();
	var source   = $("#template-1").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		$("#text1").delay(200).fadeIn(10,function()
		{
			$(this).addClass("animated bounceInDown");
			$("#clockid").delay(1500).fadeIn(10,function()
			{
				$(this).addClass("animated bounceInDown");
				$("#text2").delay(1500).fadeIn(10,function()
				{
					$(this).addClass("animated bounceInDown");
					
					$("#activity-page-next-btn-enabled").fadeIn(10);
				});
			});
		});
	});
}


function RunClock(){


	setInterval( function() {
      var seconds = new Date().getSeconds();
      var sdegree = seconds * 6;
      var srotate = "rotate(" + sdegree + "deg)";
      
      $("#sec").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});
          
    }, 1000 );
      
 
      setInterval( function() {
      var hours = new Date().getHours();
      var mins = new Date().getMinutes();
      var hdegree = hours * 30 + (mins / 2);
      var hrotate = "rotate(" + hdegree + "deg)";
      
      $("#hour").css({"-moz-transform" : hrotate, "-webkit-transform" : hrotate});
          
      }, 1000 );


      setInterval( function() {
      var mins = new Date().getMinutes();
      var mdegree = mins * 6;
      var mrotate = "rotate(" + mdegree + "deg)";
      
      $("#min").css({"-moz-transform" : mrotate, "-webkit-transform" : mrotate});
          
      }, 1000 );
}

function case2()
{
	var datavar={
			txt1:data.string.p8_9,
			done:data.string.p8_12,
			imag1:$ref+"/images/page8/3.png",
			runner:$ref+"/images/page8/1.png",
			bolt:$ref+"/images/page8/4.png",
			conc1:data.string.p8_10
		};
	
	
	var source   = $("#template-2").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);


	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){
		// ole.footerNotificationHandler.pageEndSetNotification();
		$('#heightval').on('change keyup', function() {
		var sanitized = $(this).val().replace(/[^0-9]/g, '');
	  	$(this).val(sanitized);
		});
	});
}
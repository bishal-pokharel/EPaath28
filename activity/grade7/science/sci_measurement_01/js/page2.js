$(function(){

	$(".titleMe2").html(data.string.p2_1);
	var quet=1;

	getFunction(quet);

	$("#whatPop").on('click','.yesid',function(){

		$(this).addClass("rightbx").removeClass('yesid');
		quet++;
		
		if(quet<5)
		{
			setTimeout(function(){
				getFunction(quet);
			},400);
		}
		else
		{
			getFunction2();
		}
	});

	$("#whatPop").on('click','#repeatId',function(){
		location.reload();
	});

	$("#whatPop").on('click','.noid',function(){

		$(this).addClass("wrongbx").removeClass('noid');
	});

});

function getFunction(ques)
{
	loadTimelineProgress(5,ques);
	var opt;
	if((ques%2)==0)
	{
		opt=[{options:data.string["p2_1_"+ques+"_b"],yesval:'noid'},
			{options:data.string["p2_1_"+ques+"_a"],yesval:'yesid'}
		];
	}
	else
	{
		opt=[{options:data.string["p2_1_"+ques+"_a"],yesval:'yesid'},
			{options:data.string["p2_1_"+ques+"_b"],yesval:'noid'}
		];
	}

	var textvals=data.string["p2_1_"+ques];
	var datavar={textbx:textvals,
		imgsrc:$ref+"/images/page2/0"+ques+".png",
		optionval:opt
	}


	var source   = $("#template-1").html();
	
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$("#whatPop").fadeOut(10,function(){
		$(this).html(html);
	}).delay(500).fadeIn(10,function(){

	});
}


function getFunction2()
{
	loadTimelineProgress(5,5);
	$(".titleMe2").fadeOut(10);
	var imgRepeat=getReloadBtn();

	var datavar={
		optionval:
		[
			{
				textbx:data.string.p2_1_1,
				imgsrc:$ref+"/images/page2/01.png",
				options:data.string.p2_1_1_a
			},
			{	
				textbx:data.string.p2_1_2,
				imgsrc:$ref+"/images/page2/02.png",
				options:data.string.p2_1_2_a 
			},
			{	
				textbx:data.string.p2_1_3,
				imgsrc:$ref+"/images/page2/03.png",
				options:data.string.p2_1_3_a 								
			},
			{	
				textbx:data.string.p2_1_4,
				imgsrc:$ref+"/images/page2/04.png",
				options:data.string.p2_1_4_a							 	
			}
		],
		imgRepeat:imgRepeat
	};



	var source   = $("#template-2").html();
	
	var template = Handlebars.compile(source);
	
	var html=template(datavar);

	$("#whatPop").fadeOut(10,function(){
		$(this).html(html);
		$(this).css("font-size","1.5em");
	}).delay(500).fadeIn(10,function(){
		ole.footerNotificationHandler.pageEndSetNotification();
		
	});
}
$(function(){


	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	$(".titleMe").html(data.string.title1);
	$("#p1_2").html(data.string.p1_2);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		quest++;
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);

		getFunction(quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		quest--;
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		getFunction(quest);
	});

});

function getFunction(qno)
{
	loadTimelineProgress(5,qno);
	switch(qno)
	{
		case 1:
		{
			case0();
			break;
		}
		case 2:
		{
			case1();
			break;
		}
		case 3:
		{
			case2();
			break;
		}
		case 4:
		{
			case3();
			$("#p1_2").html(data.string.p1_2);
			break;
		}
		case 5:
		{
			case4();
			break;
		}
	}
}

function case0()
{
	var datavar={
		middle_text: data.lesson.chapter,
		img1s:$ref+"/images/page1/measurement.png"
	}

	var source   = $("#template-0").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);
	$(".allbck").html(html);
	$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
}

function case1()
{
	var datavar={
		p1_3_1: data.string.p1_3_1,
		img1s:$ref+"/images/page1/boy.png",
		p1_3_2: data.string.p1_3_2,
		img2s:$ref+"/images/page1/girl.png",
		p1_3_3: data.string.p1_3_3,
		p1_3_4: data.string.p1_3_4
	}

	var source   = $("#template-1").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".allbck").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		$("#p1_3_1").delay(100).fadeIn(10,function(){

			$(".f3_1").delay(100).fadeIn(10,function(){
				$(this).addClass('animated bounceInLeft');

				$(".f3_2").delay(1500).fadeIn(10,function(){
					$(this).addClass('animated bounceInLeft');
					$("#p1_3_4").delay(1500).fadeIn(10,function(){
						$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
					});
				});

			});

		});
	});
}

function case2()
{
	var datavar={
		p1_4_1: data.string.p1_4_1,
		img1s:$ref+"/images/page1/window.png",
		p1_4_2: data.string.p1_4_2,
		p1_4_3: data.string.p1_4_3
	}

	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".allbck").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		$("#p1_4_1").delay(100).fadeIn(10,function(){

			$("#p1_4_2").delay(100).fadeIn(10,function(){
				$(this).addClass('animated fadeInLeft');

				$(".img4").delay(1500).fadeIn(10,function(){
					$(this).addClass('animated fadeInLeft');

					$("#p1_4_3").delay(1500).fadeIn(10,function(){
						$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
						$("#activity-page-prev-btn-enabled").delay(500).fadeIn(10);
					});
				});

			});

		});

	});
}


function case3()
{
	var datavar={
		p1_5_1: data.string.p1_5_1,
		img1s:$ref+"/images/page1/oil-with-jug.gif",
		p1_5_2: data.string.p1_5_2
	}

	var source   = $("#template-3").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".allbck").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		$("#p1_5_1").delay(100).fadeIn(10,function(){


			$(".img5").delay(100).fadeIn(10,function(){
				$(this).addClass('animated fadeInleft');

				$("#p1_5_2").delay(1500).fadeIn(10,function(){
					$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
					$("#activity-page-prev-btn-enabled").delay(500).fadeIn(10);
				});
			});

		});

	});
}


function case4()
{
	var datavar={
		p1_6_1: data.string.p1_6_1,
		p1_6_2: data.string.p1_6_2,
		p1_6_3: data.string.p1_6_3,
		p1_6_4: data.string.p1_6_4
	}

	var source   = $("#template-4").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".allbck").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		$("#p1_2").html(data.string.p1_1);
		$(".allbck").append("<p class='titleQuestion'>"+data.string.p1_6_0+"</p>");
		$("#p1_6").delay(100).fadeIn(10,function(){
			$(this).addClass('animated fadeInLeft');

			$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10,function(){
				ole.footerNotificationHandler.pageEndSetNotification();

			});

		});

	});
}

var l1, h1, b1, u1;
var Countclick = 0;
$(function() {
  $(".titleMe2").html(data.string.p13_1);
  $(".text1").html(data.string.p13_2);
  $(".text2").html(data.string.p13_3);
  $(".text3 div").html(data.string.p13_4);
  $("#r2 div").html(data.string.p13_5);
  var cnt = 0;
  var $quest = 1;
  var regular = false;
  var irregular = false;
  var count = 1;
  loadTimelineProgress(1, 1);
  $(".text3").click(function() {
    var id = $(this).attr("id");
    $("#activityNextBtn").css("opacity", 0);
    if (id == "r1") {
      loadTimelineProgress(2, 1);
      getFunction(1);
      regular = true;
      irregular = false;
    } else if (id == "r2") {
      $("#activityNextBtn").hide();
      loadTimelineProgress(4, 2);
      getFunction(2);
      irregular = true;
      regular = false;
      count = 2;
    }
  });

  $("#activity-page-next-btn-enabled").click(function() {
    $("#activityNextBtn").css("opacity", 0);
    if (regular) {
      loadTimelineProgress(2, 2);
      case1a();
      $("#activityNextBtn").css("opacity", 0);
    } else if (irregular && count == 2) {
      loadTimelineProgress(4, 3);
      getFunction(3);
      count = 3;
    } else if (irregular && count == 3) {
      loadTimelineProgress(4, 4);
      getFunction(5);
      count = 4;
      $("#activity-page-next-btn-enabled").hide();
    }
    $("#activityNextBtn").css("opacity", 0);
  });

  $("#activity-page-prev-btn-enabled").click(function() {
    // $(".allbck2").on("click","#activity-page-prev-btn-enabled",function(){
    if (regular) {
      loadTimelineProgress(2, 1);
      case1(1);
    } else if (irregular && count == 3) {
      loadTimelineProgress(4, 2);
      getFunction(2);
      count = 2;
    } else if (irregular && count == 4) {
      loadTimelineProgress(4, 3);
      getFunction(3);
      count = 3;
    }
  });

  $(".allbck2").on("click", "#closeme", function() {
    loadTimelineProgress(1, 1);
    Countclick++;
    if (Countclick == 2) {
      ole.footerNotificationHandler.lessonEndSetNotification();
    }
    $(".allbck2").fadeOut(10, function() {
      $(".allbck").fadeIn(10);
    });
  });

  // $("#activity-page-next-btn-enabled").click(function(){
  // // $(".allbck2").on("click","#activity-page-next-btn-enabled",function(){
  // 	loadTimelineProgress(4,2);
  // 	getFunction(3)
  // });

  $(".allbck2").on("click", ".hintbtn", function() {
    $(this).addClass("clickedDone");
    $(".allhint").fadeIn(10, function() {});
  });

  $(".allbck2").on("click", ".clickedDone", function() {
    $(this).removeClass("clickedDone");
    $(".allhint").fadeOut(10);
  });

  $(".allbck2").on("click", ".myval", function() {
    var $id = $(".myval").attr("id");
    var corr;
    if ($id == "whatis3") {
      corr = "44";
    } else {
      corr = "22.28";
    }
    var pattern = /^\d*\.?\d*$/;

    var val = $("#whatis").val();
    if (val == "") {
      $("#whatis").val("");
    }
    if (!val.match(pattern)) {
      $("#whatis").val("");
    } else {
      $("#rite").fadeOut(10);
      $("#wrong").fadeOut(10);
      if (val == corr) {
        $("#rite").fadeIn(10);
        $("#activity-page-next-btn-enabled").fadeIn(10);
        $("#activity-page-next-btn-enabled").fadeIn(10);
      } else {
        $("#wrong").fadeIn(10);
      }
    }
  });

  //   $(".allbck2").on("click", "#activity-page-prev-btn-enabled", function() {
  //     loadTimelineProgress(4, 1);
  //     getFunction(2);
  //     count = 2;
  //   });

  //   $("#activity-page-next-btn-enabled").click(function() {
  //     //   $(".allbck2").on("click","#activity-page-next-btn-enabled",function(){
  //     if (irregular && count == 2) {
  //       loadTimelineProgress(4, 3);
  //       getFunction(3);
  //       count = 3;
  //     }
  //   });

  // $("#activity-page-prev-btn-enabled").click(function() {
  //   // $(".allbck2").on("click","#activity-page-prev-btn-enabled",function(){
  //   if (irregular && count == 2) {
  //     loadTimelineProgress(4, 2);
  // 	getFunction(3);
  // 	count = 3;
  //   }
  // });

  //   $("#activity-page-next-btn-enabled").click(function() {
  //     // $(".allbck2").on("click","#activity-page-next-btn-enabled",function(){
  //     if (irregular && count == 3) {
  //       loadTimelineProgress(4, 4);
  //       getFunction(5);
  //       count = 4;
  //     }
  //   });

  // $("#activity-page-prev-btn-enabled").click(function() {
  //   // $(".allbck2").on("click","#activity-page-prev-btn-enabled",function(){
  //   if (irregular) {
  //     loadTimelineProgress(4, 3);
  //     getFunction(4);
  //   }
  // });
});

function getFunction(qno) {
  switch (qno) {
    case 1: {
      case1(qno);
      break;
    }
    case 2: {
      case1(qno);
      break;
    }
    case 3: {
      case2(qno);
      break;
    }
    case 4: {
      case2(qno);
      break;
    }
    case 5: {
      case3();
      break;
    }
  }
}

function case1(qno) {
  var datavar;

  var $whatnxtbtn = getSubpageMoveButton($lang, "next");

  if (qno == 1) {
    datavar = {
      txt1: data.string.p13_4,
      m1: data.string.p13_6,
      txt3: data.string.p13_8,
      txt2: data.string.p13_7,
      img1: $ref + "/images/page13/1.png",
      img2: $ref + "/images/page13/2.png",
      // activity-page-next-btn-enabled: "activity-page-next-btn-enabled",
      imgsrcnext: $whatnxtbtn
    };
  } else if (qno == 2) {
    datavar = {
      txt1: data.string.p13_5,
      m1: data.string.p13_18,
      txt3: "",
      txt2: "",
      img1: $ref + "/images/page13/3.png",
      img2: $ref + "/images/page13/4.png",
      // activity-page-next-btn-enabled:"activity-page-next-btn-enabled",
      imgsrcnext: $whatnxtbtn
    };
  }

  var source = $("#template-1").html();

  var template = Handlebars.compile(source);

  var html = template(datavar);
  $("#activityNextBtn").css("opacity", 0);

  $(".allbck").fadeOut(10, function() {
    $(".allbck2")
      .fadeOut(10, function() {
        $(this).html(html);
      })
      .fadeIn(10, function() {
        if (qno == 1) {
          $("#activity-page-next-btn-enabled").fadeIn(10);
        } else {
          $("#activity-page-next-btn-enabled").fadeIn(10);
        }
      });
  });
}
function case1a() {
  var $whatprevbtn = getSubpageMoveButton($lang, "prev");

  var closeMeImgSrc = getCloseBtn();

  var datavar = {
    c1: data.string.p13_4,
    c2: data.string.p13_9,
    c3: data.string.p13_10,
    c4: data.string.p13_11,
    c5: data.string.p13_12,
    c6: data.string.p13_13,
    c7: data.string.p13_13_1,
    cal1: data.string.p13_14,
    cal2: data.string.p13_15,
    cal3: data.string.p13_16,
    cal4: data.string.p13_17,
    Calculate: data.string.p13_29,
    imgsrcprev: $whatprevbtn,
    closeMeImgSrc: closeMeImgSrc
  };

  var source = $("#template-3").html();

  var template = Handlebars.compile(source);

  var html = template(datavar);
  $("#activityNextBtn").css("opacity", 0);

  $(".allbck").fadeOut(10, function() {
    $(".allbck2")
      .fadeOut(10, function() {
        $(this).html(html);
      })
      .fadeIn(10, function() {
        $("#activity-page-prev-btn-enabled").fadeIn(10);

        l1 = parseInt($("#cube1").val());
        b1 = parseInt($("#cube2").val());
        h1 = parseInt($("#cube3").val());
        u1 = $("#cube4").val();

        $(".slideMe").slider({
          value: 15,
          min: 5,
          max: 20,
          step: 5,
          slide: function(event, ui) {
            var $id = $(this).attr("id");

            var whatUnit;

            if ($id == "slider1") $("#cube1").val(ui.value);
            else if ($id == "slider2") $("#cube2").val(ui.value);
            else if ($id == "slider3") $("#cube3").val(ui.value);
            else if ($id == "slider4") {
              if (ui.value == 15) whatUnit = " m";
              else if (ui.value == 20) whatUnit = " km";
              else if (ui.value == 10) whatUnit = " cm";
              else if (ui.value == 5) whatUnit = " mm";

              $("#cube4").val(whatUnit);
            }

            l1 = parseInt($("#cube1").val());
            b1 = parseInt($("#cube2").val());
            h1 = parseInt($("#cube3").val());
            u1 = $("#cube4").val();

            /*$("#ans1").html("= "+l1+" X " + b1);
					$("#ans2").html("= "+(l1*b1)+ " "+ u1 + "<sup>2</sup>");

					$("#ans3").html("= "+l1+" X " + b1+ " X "+ h1);
					$("#ans4").html("= "+(l1*b1*h1) +" "+ u1 + "<sup>3</sup>");*/
          }
        }); //slide

        $("#getCal").click(function() {
          $(".m7")
            .fadeOut(10, function() {
              $("#ans1").html("= " + l1 + " X " + b1);
              $("#ans2").html("= " + l1 * b1 + " " + u1 + "<sup>2</sup>");

              $("#ans3").html("= " + l1 + " X " + b1 + " X " + h1);
              $("#ans4").html("= " + l1 * b1 * h1 + " " + u1 + "<sup>3</sup>");
            })
            .delay(100)
            .fadeIn(10);
        }); //click
      }); //fadein
  }); //fadeout
}

function case2(qno) {
  var datavar;
  var $whatprevbtn = getSubpageMoveButton($lang, "prev");

  var $whatnextbtn = getSubpageMoveButton($lang, "next");

  if (qno == 3) {
    datavar = {
      rr1: data.string.p13_19,
      rr2: data.string.p13_20_1,
      rr3: data.string.p13_20_2,
      rr4: data.string.p13_20_3,
      rrimg: $ref + "/images/page13/3.png",
      rr5: data.string.p13_20,
      hint: data.string.p11_8,
      hint1: data.string.p13_20_3a,
      hint2: data.string.p13_20_4,
      ht: [data.string.p13_20_5, data.string.p13_20_6, data.string.p13_20_7],
      hint3: data.string.p13_20_8,
      ht1: [
        data.string.p13_20_9,
        data.string.p13_20_10,
        data.string.p13_20_11,
        data.string.p13_20_12
      ],
      tot: data.string.p13_20_13,
      hint4: data.string.p13_20_14,
      hint5: data.string.p13_20_15,
      hint6: data.string.p13_20_16,
      submit: data.string.p8_12,
      // activity-page-next-btn-enabled:"activity-page-next-btn-enabled",
      // activity-page-prev-btn-enabled:"activity-page-prev-btn-enabled3",
      whatis2: "whatis2",
      htcls: "ht1",
      htcls2: "ht2",
      imgsrcprev: "",
      imgsrcnext: ""
    };
  } else {
    datavar = {
      rr1: data.string.p13_19,
      rr2: data.string.p13_20_1,
      rr3: data.string.p13_21_21,
      rr4: data.string.p13_21_2,
      rr3a: data.string.p13_21_3_1,
      rrimg: $ref + "/images/page13/4.png",
      rr5: data.string.p13_20,
      hint: data.string.p11_8,
      hint1: data.string.p13_21_3a,
      hint2: data.string.p13_21_4,
      ht: [
        data.string.p13_21_5,
        data.string.p13_21_6,
        data.string.p13_21_7,
        data.string.p13_21_8
      ],
      hint3: data.string.p13_21_9,
      ht1: [
        data.string.p13_21_10,
        data.string.p13_21_11,
        data.string.p13_21_12
      ],
      hint3a: data.string.p13_21_13,
      ht2: [
        data.string.p13_21_14,
        data.string.p13_21_15,
        data.string.p13_21_16,
        data.string.p13_21_17
      ],
      tot: data.string.p13_21_18,
      hint4: data.string.p13_21_19,
      hint5: data.string.p13_21_20,
      hint6: "",
      submit: data.string.p8_12,
      // activity-page-prev-btn-enabled:"activity-page-prev-btn-enabled4",
      closeme: "closeme",
      // activity-page-next-btn-enabled:"activity-page-next-btn-enabled",
      whatis2: "whatis3",
      htcls: "ht3",
      htcls2: "ht4",
      imgsrcprev: "",
      imgsrcnext: ""
    };
  }

  var source = $("#template-2").html();

  var template = Handlebars.compile(source);

  var html = template(datavar);
  $("#activityNextBtn").css("opacity", 0);

  $(".allbck").fadeOut(10, function() {
    $(".allbck2")
      .fadeOut(10, function() {
        $(this).html(html);
      })
      .fadeIn(10, function() {
        if (qno == 3) {
          // $("#activity-page-next-btn-enabled").fadeIn(10);
          $("#activity-page-prev-btn-enabled").fadeIn(10);
        } else {
          $("#activity-page-prev-btn-enabled").fadeIn(10);
          // $("#activity-page-next-btn-enabled").fadeIn(10);
        }
      });
  });
}

function case3(qno) {
  var $whatprevbtn = getSubpageMoveButton($lang, "prev");
  var closeMeImgSrc = getCloseBtn();

  var datavar = {
    rr3: data.string.p13_22,
    imgzrc: $ref + "/images/page13/7.png",
    crc1: data.string.p13_23,
    txtMe: data.string.p13_24,
    crc2: data.string.p13_25,
    imgzrc2: $ref + "/images/page13/6.png",
    crc3: data.string.p13_26,
    imgzrc3: $ref + "/images/page13/5.gif",
    ltxt: data.string.p13_27,
    ltxt2: data.string.p13_28,
    imgsrcprev: "",
    closeMeImgSrc: closeMeImgSrc
  };
  var source = $("#template-4").html();

  var template = Handlebars.compile(source);

  var html = template(datavar);
  $("#activityNextBtn").css("opacity", 0);

  $(".allbck").fadeOut(10, function() {
    $(".allbck2")
      .fadeOut(10, function() {
        $(this).html(html);
      })
      .fadeIn(10, function() {
        $("#activity-page-prev-btn-enabled").fadeIn(10);
        $("#closeme").fadeIn(10);
      });
  });
}

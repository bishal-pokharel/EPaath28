$(function()
{

	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	$(".titleMe2").html(data.string.p12_1);

	loadTimelineProgress(2,1);
	var cnt=0;
	var $quest=1;
	getFunction($quest);


	$(".allbck").on("click","#totalCross",function(){
		var url = 'activity.html?id=scimes01&lang='+$lang+'&pg=5&scId=0';
		window.location.href = url;
	});
	$("#activity-page-next-btn-enabled").click(function(){
		$quest++;
		loadTimelineProgress(2,$quest);
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);

		getFunction($quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		$quest--;
		loadTimelineProgress(2,$quest);
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		getFunction($quest);
	});

	$(".wholebx").on('click','.arrow1' ,function(){

		var pattern=/^\d*$/;
		var arrId=$(this).attr('id');

		if(arrId=="arrows")
		{
			var val=$("#val1").val();

			if(!val.match(pattern))
			{
				$("#val1").val('');
			}
			else if(val=="")
			{
				$("#val1").val('');
			}
			else
			{
				if(parseInt(val)==5)
				{
					$(this).fadeOut(10);
					$("#val1").attr('disabled','disabled');
					$(".wholebx").find("#lastbxx").fadeIn(10,function()
					{
						$(this).addClass("animated fadeInDown");
					});
				}
				else
				{
					$("#val1").val('');
				}
			}

		}
		else if(arrId=="arrows1")
		{
			var val1=$("#val2").val();

			if(!val1.match(pattern))
			{
				$("#val2").val('');
			}
			else
			{
				if(parseInt(val1)==10)
				{
					$(this).fadeOut(10);
					$("#val2").attr('disabled','disabled');
					$(".wholebx").find(".text11").fadeIn(10,function()
					{
						$(this).addClass("animated fadeInDown");
						$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10);
						ole.footerNotificationHandler.pageEndSetNotification();
						//$("#totalCross").delay(1500).fadeIn(10);
					});
				}
				else
				{
					$("#val2").val('');
				}
			}

		}


	});

});

function getFunction(qno)
{
	loadTimelineProgress(2,qno);
	switch(qno)
	{
		case 1:
		{

			case1();
			break;
		}
		case 2:
		{

			case2();
			break;
		}
	}
}

function case1()
{
	var datavar={txt1:data.string.p12_2,
		txt2:data.string.p12_3,
		img1:$ref+"/images/page12/1.png"
	};


	var source   = $("#template-1").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){
		$(".text1").delay(200).fadeIn(10,function()
		{
			$(this).addClass("animated fadeInDown");
			$(".text2").delay(1500).fadeIn(10,function()
			{
				$(this).addClass("animated fadeInDown");

				$(".img").delay(1500).fadeIn(10,function()
				{
					$(this).addClass("animated fadeInDown");
					$("#activity-page-next-btn-enabled").fadeIn(10);


				});
			});
		});

	});
}
function case2()
{
	var datavar={txt3:data.string.p12_6,
		txt4:data.string.p12_7,
		txt5:data.string.p12_8,
		txt6:data.string.p12_9_1,
		txt7:data.string.p12_9_2,
		txt8:data.string.p12_10,
		txt9:data.string.p12_11,
		txt10:data.string.p12_12,
		txt11:data.string.p12_13
	};


	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".wholebx").fadeOut(10,function(){
		$(this).html(html);
	}).fadeIn(10,function(){

		$(".firstcls").delay(200).fadeIn(10,function()
		{
			$(this).addClass("animated fadeInDown");
			$(".text5").delay(2000).fadeIn(10,function()
			{
				$(this).addClass("animated fadeInDown");

				$(".firstbxx").delay(1500).fadeIn(10,function()
				{
					$(this).addClass("animated fadeInDown");
					ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);


				});
			});
		});
	});
}

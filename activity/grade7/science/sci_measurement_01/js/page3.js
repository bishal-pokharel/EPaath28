$(function(){

	// var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	// $("#activity-page-next-btn-enabled").html($whatnxtbtn);

	// var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	// $("#activity-page-prev-btn-enabled").html($whatprevbtn);

	$(".titleMe2").html(data.string.p3_a);
	var quet=1;

	getFunction(quet);


	$("#activity-page-next-btn-enabled").on('click',function(){

		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);

		quet++;

		if(quet<5)
		{
			/*setTimeout(function(){
				getFunction(quet);
			},400);*/
			getFunction(quet);

		}
		else
		{
			getFunction2();
		}
	});

	$("#activity-page-prev-btn-enabled").on('click',function(){
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		quet--;
		/*setTimeout(function(){
			*/	getFunction(quet);
		/*},400);*/

	});


	$(".allbck").on('click','#repeatId',function(){
		location.reload();
	});



});

function getFunction(ques)
{
	loadTimelineProgress(5,ques);
	var opt;

	var datavar={textbx:data.string["p3_"+ques]+" : ",
		txt11:data.string["p3_"+ques+"_D"],
		optionval:
		[
			{
				pdesc:data.string["p3_5"],
				imgsrc:$ref+"/images/page3/01.png",
				options:data.string["p3_"+ques+"_1"],
				options2:data.string["p3_"+ques+"_a"],
				options3:data.string["p3_"+ques+"_1a"],
				ggg:"g1"
			},
			{	pdesc:data.string["p3_6"],
				imgsrc:$ref+"/images/page3/02.png",
				options:data.string["p3_"+ques+"_2"],
				options2:data.string["p3_"+ques+"_b"],
				options3:data.string["p3_"+ques+"_1b"],
				ggg:"g2"
			},
			{	pdesc:data.string["p3_7"],
				imgsrc:$ref+"/images/page3/03.png",
				options:data.string["p3_"+ques+"_3"],
				options2:data.string["p3_"+ques+"_c"],
				options3:data.string["p3_"+ques+"_1c"],
				ggg:"g3"
			}
		]
	};

	var source   = $("#template-1").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$("#whatPop").fadeOut(10,function(){
		$(this).html(html);
	}).delay(500).fadeIn(10,function(){

		$(".g1").fadeIn(10,function()
		{
			$(this).addClass("animated bounceInDown");
			$(".g2").delay(1500).fadeIn(10,function()
			{
				$(this).addClass("animated bounceInDown");

				$(".g3").delay(1500).fadeIn(10,function()
				{
					$(this).addClass("animated bounceInDown");
					if(ques!=1)
					{
						$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10);
					}

					if(ques!=5)
					{
						$("#activity-page-next-btn-enabled").delay(1500).fadeIn(10);
					}
				});

			});
		});

	});
}


function getFunction2()
{
	loadTimelineProgress(5,5);
	var datavar={head1:data.string.p3_8,
		head2:data.string.p3_1,
		head3:data.string.p3_2,
		head4:data.string.p3_3,
		head5:data.string.p3_4,
		pdesc1:data.string.p3_9,
		imgsrc1:$ref+"/images/page3/01.png",
		l1:data.string.p3_1_1+" "+data.string["p3_1_a"],
		l2:data.string.p3_2_1+" "+data.string["p3_2_a"],
		l3:data.string.p3_3_1+" "+data.string["p3_3_a"],
		l4:data.string.p3_4_1+" "+data.string["p3_4_a"],
		pdesc2:data.string.p3_10,
		imgsrc2:$ref+"/images/page3/02.png",
		p1:data.string.p3_1_2+" "+data.string["p3_1_b"],
		p2:data.string.p3_2_2+" "+data.string["p3_2_b"],
		p3:data.string.p3_3_2+" "+data.string["p3_3_b"],
		p4:data.string.p3_4_2+" "+data.string["p3_4_b"],
		pdesc3:data.string.p3_11,
		imgsrc3:$ref+"/images/page3/03.png",
		s1:data.string.p3_1_3+" "+data.string["p3_1_c"],
		s2:data.string.p3_2_3+" "+data.string["p3_2_c"],
		s3:data.string.p3_3_3+" "+data.string["p3_3_c"],
		s4:data.string.p3_4_3+" "+data.string["p3_4_c"]
	};



	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$("#whatPop").fadeOut(10,function(){
		$(this).html(html);
	}).delay(500).fadeIn(10,function(){
		$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10,function()
			{
				ole.footerNotificationHandler.pageEndSetNotification();

			});

	});
}


$(function()
{

	$(".titleMe2").html(data.string.p9_1);
	$(".text2").html(data.string.p9_2);
	$(".text3").html(data.string.p9_3);
	$("#heatme").html(data.string.p9_4);

	loadTimelineProgress(1,1);
	var str=data.string.p9_5; 
	$(".text4").html(ole.degree(str));
	
	$(".allbck").on("click","#totalCross",function(){
		var url = 'activity.html?id=scimes01&lang='+$lang+'&pg=5&scId=0';
		window.location.href = url;
	});
	
	$("#heatme").click(function(){
		$(this).fadeOut(10);
		$("#flame").fadeIn(10);
		setTimeout(function(){
			$("#air").fadeIn(10,function(){
				$("#air").addClass('steamfly');
			});
			$(".bubbles").delay(4000).fadeIn(10,function(){
				allvibrate();
			});
			
			$("#mercury").animate({'height':'31%'},8000,function(){
				
				$(".text4").fadeIn(10,function(){
					$(".text4").addClass('animated bounceInLeft');
					$("#restart").html(getReloadBtn()).delay(1500).fadeIn(10,function(){
						ole.footerNotificationHandler.pageEndSetNotification();
		
					});
				});
				
			});
		},1000);
		
	});
	$("#restart").click(function(){
		$(this).fadeOut(10);
		stopVibrateMe();
		$(".text4").fadeOut(10,function(){
			$(".text4").removeClass('animated bounceInLeft');
		});

		$("#flame").fadeOut(10);
		$("#air").fadeOut(10,function(){
			$("#air").removeClass('steamfly');
		});
		$(".bubbles").fadeOut(10);
		$("#mercury").css({'height':'2%'});
		$("#heatme").fadeIn(10);
	});
});

function allvibrate()
{
	$("#bubble1").vibrateSingle(5,5,100,15);
	$("#bubble2").vibrateSingle(30,30,100,15);
	$("#bubble3").vibrateSingle(60,50,100,15);
	$("#bubble4").vibrateSingle(30,75,100,15);
	$("#bubble5").vibrateSingle(20,60,100,15);
}


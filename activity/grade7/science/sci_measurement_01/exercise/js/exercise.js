
var countIng=0;
var countEggs=0;
var optionId;
var countAll=0;
var clickno=0;
var moveUp=0;
var replace1,replace2;
var colorcode;

var phase2=0;
$(function(){



	firstThis();


	$(".popUp").find(".btnA").click(function(){
		$(".popUp").html("").fadeOut(10);
		secondThis();
	});

	$(".mySideBox").on('click',"#libottle",function(){

		if($(this).hasClass('clickBottle'))
		{

			var $this=$(this);
			clickno++;

			$(this).removeClass('clickBottle');

			$(this).find('img').attr('src',replace2);
			$("#liquidflow").css('background',colorcode);
			$("#liquidflow").addClass('raise'+clickno);
			if(clickno>1)
			{
				$("#liquidflow").addClass('raise'+(clickno-1));
			}

			setTimeout(function(){
				if(clickno<moveUp)
				{
					$this.find('img').attr('src',replace1);

					$this.addClass('clickBottle');

				}
				else
				{
					$this.find('img').hide(0);
					setTimeout(function(){
						$(".mySideBox").html('');
						 clickno=0;
						 moveUp=0;
						if(countAll==6)pourMe();
						else pourMe();
					},1000);
				}
			},2000);
		}
	});




});

function firstThis()
{

	var datavar={
		titlepop: data.string.exe_data1,
		text1: data.string.exe_data2,
		btntext: data.string.exe_data3,
		imgchief:$ref+"/exercise/images/chef.png"
	};

	var source   = $("#firstPopUp").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".popUp").html(html).fadeIn(10);
	ole.vCenter(".popUp .innerPop",3);

}

function secondThis()
{

	$(".smallTitle").html(data.string.exe_in0);
	$("#helpMe").addClass('helpme1').html(data.string.exe_data4);

	for(var i=1;i<=5;i++)
	{
		$("#ing"+i).html(data.string["exe_in"+i]);
	}

	countIng++;


	moveIngredient(countIng);

	eggshow();



}

function moveIngredient(countIng,bol)
{


	for(var i=1;i<=5;i++)
	{
		if ( $(".ingnext").parents("#ing"+i).length == 1 )
		{
			$(".ingnext").remove();
		}
	}

	var spanme="<span class='glyphicon glyphicon glyphicon-hand-left ingnext' id='ingnext"+countIng+"'></span>";
	$("#ing"+countIng).append(spanme);

	if(bol==undefined)
	{
		$("#helpMe").html(data.string["exe_hp"+countIng]);
	}
	else if(bol==true)
	{

		var helpval=countIng+5;

		$("#helpMe").html(data.string["exe_hp"+helpval]);
	}




}

function eggshow()
{
	countAll++;
	var datavar={
		eggcrate: $ref+"/exercise/images/egg-crate.png",
		eggs1:"eggs1",
		eggs2:"eggs2",
		eggs3:"eggs3",
		eggs4:"eggs4",
		eggs5:"eggs5",
		eggs6:"eggs6",
		eggs7:"eggs7",
		eggs8:"eggs8",
		eggme: [$ref+"/exercise/images/egg.png"],
		imgsDrops:[$ref+"/exercise/images/egg-break.gif"]
	};

	var source   = $("#eggpop").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".mySideBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(10,function(){
		dragAndDrop();

	});


}

function dragAndDrop()
{
	$("#helpMe").addClass().show(0);
	$("#egglist").show(0);
	$(".eggss").draggable({ revert: "invalid",
		start: function() {

		}
	});


	$("#dropEgg").droppable({
        drop: function( event, ui ) {
        	$("#coverme").show(0);
        	ui.draggable.hide(0);

        	var timestramp=new Date().getTime();
        	var urls=$ref+"/exercise/images/egg-break.gif?"+timestramp;
        	$("#dropEgg").find('img').attr('src',urls).show(0);

        	countEggs++;

			setTimeout(function(){
				$("#dropEgg").find('img').hide(0);

				// count till eggs are 6 and go to next step
				if(countEggs>=6)
				{
					setTimeout(function(){
						$(".mySideBox").html("");
						setTimeout(function(){
							questionMe();
						},1500);
					},700);

				}
				else
				{
					$("#coverme").hide(0);
				}

			},1500);
        }
    });


}

function questionMe()
{
	countAll++;

	var datavar;
	$("#helpMe").removeClass().addClass('helpme2').html(data.string["exe_data6"]);

	if(countAll==2)
	{

		datavar={
			question: data.string.exe_qu1,
			option1: data.string.exe_q1_o1,
			corr1:"no",
			opt1:"opt1",
			remove1:"glyphicon-remove",
			what1:"whatx",
			option2: data.string.exe_q1_o2,
			corr2:"no",
			opt2:"opt2",
			remove2:"glyphicon-remove",
			what2:"whatx",
			option3: data.string.exe_q1_o3,
			corr3:"yes",
			opt3:"opt3",
			remove3:"glyphicon-ok",
			what3:"whatr",
			submitMe: data.string.exe_data5,
			expme:data.string.exp_1
		};
	}
	else if(countAll==5)
	{
		datavar={
			question: data.string.exe_qu2,
			option1: data.string.exe_q2_o1,
			corr1:"yes",
			opt1:"opt3",
			remove1:"glyphicon-ok",
			what1:"whatr",
			option2: data.string.exe_q2_o2,
			corr2:"no",
			opt2:"opt1",
			remove2:"glyphicon-remove",
			what2:"whatx",
			option3: data.string.exe_q2_o3,
			corr3:"no",
			opt3:"opt2",
			remove3:"glyphicon-remove",
			what3:"whatx",
			submitMe: data.string.exe_data5,
			expme:data.string.exp_2
		};
	}
	else if(countAll==8)
	{
		datavar={
			question: data.string.exe_qu3,
			option1: data.string.exe_q3_o1,
			corr1:"no",
			opt1:"opt1",
			remove1:"glyphicon-remove",
			what1:"whatx",
			option2: data.string.exe_q3_o2,
			corr2:"yes",
			opt2:"opt3",
			remove2:"glyphicon-ok",
			what2:"whatr",
			option3: data.string.exe_q3_o3,
			corr3:"no",
			opt3:"opt2",
			remove3:"glyphicon-remove",
			what3:"whatx",
			submitMe: data.string.exe_data5,
			expme:data.string.exp_3
		};
	}
	else if(countAll==100)
	{
		datavar={
			question: data.string.exe_qu4,
			option1: data.string.exe_q4_o1,
			corr1:"no",
			opt1:"opt1",
			remove1:"glyphicon-remove",
			what1:"whatx",
			option2: data.string.exe_q4_o2,
			corr2:"no",
			opt2:"opt2",
			remove2:"glyphicon-remove",
			what2:"whatx",
			option3: data.string.exe_q4_o3,
			corr3:"yes",
			opt3:"opt3",
			remove3:"glyphicon-ok",
			what3:"whatr",
			submitMe: data.string.exe_data5,
			expme:data.string.exp_4
		};
	}

	var source   = $("#questionBox").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);


	$(".mySideBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(10,function(){

		$(".mySideBox").find("input[name=radioMe]:radio").click(function(){

			optionId=$(this).parent().parent().attr('id');
			$(".submitMe").show(0);
		});

		$(".mySideBox").find(".submitMe").click(function(){

			$("#explanation").show(0);

			if(optionId=="opt3")
			{
				$(this).hide(0);
				setTimeout(function(){
					$(".mySideBox").html("");

					if(countAll==2)
						sugarMeBox();
					else if(countAll==5)
						liquidMeBox();
					else if(countAll==8)
					{
						sugarMeBox();
					}else if(countAll==100)
					{
						$(".mySideBox").html('');
						cakeInOven();
					}
				},2500);
			}
			$("#"+optionId).find("span").show(0);
		});
	});


}

function sugarMeBox()
{
	countIng++;
	countAll++;
	moveIngredient(countIng);
	var rotateInt=0;
	var datavar;
	var howmuchrotate=0;
	var howMuchtry=0;


	if(countAll==3)
	{
		datavar={
			sgimg: $ref+"/exercise/images/sugar.png",
			mchimg:$ref+"/exercise/images/machine.png",
			ndimg: $ref+"/exercise/images/niddle.png",
			fimg: $ref+"/exercise/images/flour-rise.png"
		};
		howmuchrotate=3;
		howMuchtry=3;
	}
	else if(countAll==9)
	{
		datavar={
			sgimg: $ref+"/exercise/images/flour.png",
			mchimg:$ref+"/exercise/images/machine.png",
			ndimg: $ref+"/exercise/images/niddle.png",
			fimg: $ref+"/exercise/images/flour-rise.png"
		};
		howmuchrotate=5;
		howMuchtry=5;
	}


	var source   = $("#sugarBox").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);
	$(".mySideBox").html(html);
	$("#helpMe").removeClass().addClass('helpme3');

	setTimeout(function(){
		$("#helpMe").show(0);
		$("#s1").addClass("sugarclick");
	},10);

	var sugarclickcount=0;
	var riseFlour=38;

	$(".sugarsack").click(function(){
		if($(this).hasClass('sugarclick'))
		{
			sugarclickcount++;
			var sackid=$(this).attr('id');
			var sackId="#"+sackid;


			/*if(countAll==3)
			{
				rotateInt+=2;
			}
			else if(countAll==9)
			{
				rotateInt+=1;
			}*/
				rotateInt++;

			$(sackId).addClass('pourscale');
			$(sackId).removeClass('sugarclick');

			var timestramp=new Date().getTime();

			if(countAll==3)
			{
				var urlss=$ref+"/exercise/images/psugar.png?"+timestramp;
				var newurls=$ref+"/exercise/images/psugar-empty.png?"+timestramp;
			}
			else if(countAll==9)
			{
				var urlss=$ref+"/exercise/images/pflour.png?"+timestramp;
				var newurls=$ref+"/exercise/images/pflour-empty.png?"+timestramp;
			}



			if(rotateInt<=howmuchrotate)
			{
				$(sackId).find('img').attr('src',urlss);

				if(rotateInt<=2)
				{

					$(".riseflour").delay(1000).fadeIn(10);

				}

				if(rotateInt==howmuchrotate)
					$(this).removeClass('sugarclick');

				$(".niddle").addClass("nidcls"+rotateInt);

				riseFlour=riseFlour-(rotateInt/2);

				$(".riseflour").css({"top":riseFlour+"%"});

				if(rotateInt>1)
				{
					$(".niddle").removeClass("nidcls"+(rotateInt-1));
				}

				setTimeout(function(){
					$(sackId).find('img').attr('src',newurls);

				},2400);

				setTimeout(function(){
					$(sackId).hide(0);
					if(sugarclickcount<howMuchtry)
					{
						$("#s"+(sugarclickcount+1)).show(0);
						$("#s"+(sugarclickcount+1)).addClass("sugarclick");
					}
					if(rotateInt==howmuchrotate)
					{
						setTimeout(function(){
							$(".mySideBox").html("");

							setTimeout(function(){
								pourMe();

							},500);
						},1200);
					}
				},3000);

			}
		}
	});
}


function pourMe()
{
	$("#helpMe").html(data.string.exe_pouringredient);
	/*make bowlimage appear in front of mysidebox div so that the
	pouring into bowl images go back of the bowl*/
	$(".bowlimg").css('z-index', '10');

	var datavar,dval;
	var timestramp=new Date().getTime();
	countAll++;
	if(countAll==4)
	{
		dval={pourBox:"pourBox"};
		datavar=$ref+"/exercise/images/sugar-and-flour.png?"+timestramp;
	}
	else if(countAll==10)
	{
		dval={pourBox:"pourBox"};
		datavar=$ref+"/exercise/images/sugar-and-flour.png?"+timestramp;
	}
	else if(countAll==7)
	{
		dval={pourBox:"pourBoxch"};
		datavar=$ref+"/exercise/images/beeker-withmilk.png?"+timestramp;
	}
	else if(countAll==12)
	{
		dval={pourBox:"pourBoxch"};
		datavar=$ref+"/exercise/images/beeker-with-chocolate.png?"+timestramp;
	}
	else if(countAll==13)
	{
		dval={spoon:"spoon"};
		datavar=$ref+"/exercise/images/spoon.png?"+timestramp;
	}


	var source   = $("#pourBox").html();

	var template = Handlebars.compile(source);

	var html=template(dval);

	$(".mySideBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(10,function(){

		$(this).find("#pourbxx").attr('src',datavar);


			setTimeout(function(){
				// now let the bowl image go back of the sidebox
				$(".bowlimg").css('z-index', '2');

				$(".mySideBox").html('');
				if(countAll==4){ questionMe(); }
				else if(countAll==7){questionMe(); }
				else if(countAll==10){ liquidMeBox(); }
				else if(countAll==12){receipeshow()}

			},3000); /*3000*/

	});



}

function liquidMeBox()
{
	countIng++;
	moveIngredient(countIng);
	countAll++;




	var datavar;

	if(countAll==6)
	{
		datavar={
			libottle:$ref+"/exercise/images/milkbottle1.png",
			imgbreaker:$ref+"/exercise/images/beaker.png"
		};
		replace1=$ref+"/exercise/images/milkbottle1.png";
		replace2=$ref+"/exercise/images/milkbottle-01.png";
		moveUp=5;
		colorcode="#fff";
	}
	else
	{
		datavar={
			libottle:$ref+"/exercise/images/chocolatebottle1.png",
			imgbreaker:$ref+"/exercise/images/beaker.png"
		};

		replace1=$ref+"/exercise/images/chocolatebottle1.png";
		replace2=$ref+"/exercise/images/chocolatebottle-01.png";
		moveUp=2;
		colorcode="#8C3509";
	}


	var source   = $("#pourLiquid").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);
	$(".mySideBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(10,function(){

		$("#helpMe").removeClass().addClass('helpme4');
		$("#libottle").addClass('clickBottle');

	});

}

function receipeshow()
{
	countIng=0;
	$(".smallTitle").html(data.string.exe_in00);


	for(var i=6,j=1;i<=11,j<=6;i++,j++)
	{

		$("#ing"+j).html(data.string["exe_in"+i]);
	}

	countIng++;

	moveIngredient(countIng,true);

	stillImage();

}
function stillImage()
{
	phase2++;

	var datavar;
	var timestramp=new Date().getTime();
	var source;


	if(phase2==1)
	{
		datavar={
			pourBox:"pourBox2",
			pimg:$ref+"/exercise/images/spoon2.png",
			spoon:$ref+"/exercise/images/spoon.png"
		};


		 source   = $("#stillImage").html();
	}
	else if(phase2==3)
	{
		datavar={
			pourBox:"pourBox3",
			pimg:$ref+"/exercise/images/ready-for-cook.png"
		};


		 source   = $("#stillImage").html();
	}

	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".mySideBox").fadeOut(10,function(){
		$(this).html(html);

	}).delay(100).fadeIn(10,function(){
		$("#helpMe").show(0);

		if(phase2==1)
		{

			$('.pourBox2').click(function(){
				$(this).hide(0);

				$("#spoon").show(0);

				setTimeout(function(){
					$(".mySideBox").html('');
						cakeInPan();
				},2000);
			});
		}
		else if(phase2==3)
		{

			setTimeout(function(){
				$(".mySideBox").html('');
				$("#helpMe").html(data.string.exe_data6).show(0);
				countAll=99;
				questionMe();
			},3000);



		}


	});

}

function cakeInPan()
{
	phase2++;

	countIng++;

	moveIngredient(countIng,true);
	$(".imgBck").attr("src",$ref+"/exercise/images/back-with-chef.png");
	$(".bowlimg").css('display', 'none');

	stillImage();
}

function cakeInOven()
{

	phase2++;

	countIng++;

	var cntTimer=0;
	var cntTemp=0;

	moveIngredient(countIng,true);
	$(".imgBck").attr("src",$ref+"/exercise/images/back-oven-cake.png");


	var datavar={
			amp:$ref+"/exercise/images/oven-niddle.png",
			amplable:data.string.exe_data7,
			tempimg:$ref+"/exercise/images/oven-switch.png",
			templable:data.string.exe_data8,
			timerimg:$ref+"/exercise/images/oven-switch.png",
			timerlable:data.string.exe_data9,
			caketop:$ref+"/exercise/images/cake-top.png",
			steamingup:$ref+"/exercise/images/cake-steam.png",
			expme:data.string.exp_7,
		};
	var  source   = $("#ovenId").html();
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".mySideBox").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){
		$("#helpMe").removeClass().addClass('helpme5');

		setTimeout(function(){
			countIng++;

			moveIngredient(countIng,true);

			$("#timerhere").addClass('blinkOven');


		},6000);

		$("#timerhere").click(function(){
			if($(this).hasClass('blinkOven'))
			{
				cntTimer++;

				$(this).addClass('time'+cntTimer);
				if(cntTimer>1)
				{
					$(this).removeClass('time'+(cntTimer-1));
				}

				if(cntTimer==2)
				{
					$(this).removeClass('blinkOven');
					innerQuestion(1);

				}
			}

		});

		$("#temprature").click(function(){
			$("#explanation").hide(0);
			if($(this).hasClass('blinkOven'))
			{
				cntTemp++;

				$(this).addClass('time'+cntTemp);
				if(cntTemp>1)
				{
					$(this).removeClass('time'+(cntTemp-1));
				}

				if(cntTemp==4)
				{
					$(this).removeClass('blinkOven');
					innerQuestion(2);

				}
			}

		});


		var optionId;
		var whtCntInner=0;
		$(".mySideBox").find("input[name=radioMe]:radio").click(function(){

			optionId=$(this).parent().parent().attr('id');
			$("#innerClickBtn").show(0);

		});

		$("#innerClickBtn").click(function(){


			$("#explanation").show(0);
			if(optionId=="inopt2")
			{
				whtCntInner++;
				$(this).hide(0);
				if(whtCntInner==1)
				{
					setTimeout(function(){
						$("#whatquestionForme").hide(0);
						countIng++;

						moveIngredient(countIng,true);

						$("#helpMe").removeClass().addClass('helpme5');
						$("#temprature").addClass('blinkOven');
					},2000);
				}
				else if(whtCntInner==2)
				{
					setTimeout(function(){
						$("#temprature").removeClass('blinkOven');
						$("#whatquestionForme").hide(0);
						$("#helpMe").removeClass().addClass('helpme7').html(data.string.exe_data10);

						$("#cakeTop").delay(2000).fadeIn(500,function(){
							$("#steamingup").delay(900).fadeIn(500,function(){
								setTimeout(function(){
									finalCake();
								},1000);

							});
						});


					},2000);
				}



			}
			$("#"+optionId).find("span").show(0);
		});
	});

}

function innerQuestion(cntInner)
{


	$("#helpMe").removeClass().addClass('helpme6').html(data.string.exe_data6);
	if(cntInner==1)
	{
		$("#innerquestion").html(data.string.exe_qu5);
		$("#opts1").html(data.string.exe_q5_o1);
		$("#opts2").html(data.string.exe_q5_o2);
		$("#innerClickBtn").html(data.string.exe_data5);

		$("#explanation").html(data.string.exp_5);

	}
	else
	{
		$("#innerquestion").html(data.string.exe_qu6);
		$("#opts1").html(data.string.exe_q6_o1);
		$("#opts2").html(data.string.exe_q6_o2);
		$("#innerClickBtn").html(data.string.exe_data5);
		$(".inneroption").find(".glyphicon").hide(0);
		$("input[name=radioMe]:radio").removeAttr("checked");

		$("#explanation").html(data.string.exp_6);
	}





	$("#whatquestionForme").show(0);


}

function finalCake()
{

	phase2++;
	$(".mySideBox").html('');
	$(".imgBck").attr("src",$ref+"/exercise/images/back-no-chief.png");

	$("#helpMe").removeClass().addClass('helpme8').html(data.string.exe_data11);

	var numCandel=0;
	var datavar={
			imgcandel:$ref+"/exercise/images/candle01.png",
			imgcake:$ref+"/exercise/images/cake.png",
			textDn:data.string.exe_data12,
			congrats:data.string.exe_data14,
			imgcon:$ref+"/exercise/images/firework01.png",
			questIn:data.string.exe_qu8,
			area:data.string.exe_q8_o1,
			volume:data.string.exe_q8_o2,
			submitMe:data.string.exe_data5,
			expme2:data.string.exp_8
		};
	var  source   = $("#cakeMe").html();
	var template = Handlebars.compile(source);

	var html=template(datavar);

	$(".mySideBox").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){

		setTimeout(function(){
			$("#helpMe").removeClass().addClass('helpme9').html(data.string.exe_data15);


			$("#whatquestionForme2").show(0);
			$("#innerClickBtn2").show(0);
		},1500);


		var okCount=0;
		$("#innerClickBtn2").click(function(){
			$("#explanation2").show(0);
			var tst1=parseInt($("#textopt1").val());
			var tst2=parseInt($("#textopt2").val());
			okCount++;

			if((tst1==600) && (tst2==6000))
			{
				$(this).hide(0);
				$("#txtA1 .whatx").show(0);
				$("#txtA2 .whatx").show(0);
				$("#txtA1 .whatr").hide(0);
				$("#txtA2 .whatr").hide(0);

				$("#textopt1").attr('disbaled','disbaled');
				$("#textopt2").attr('disbaled','disbaled');

				setTimeout(function(){
					$("#whatquestionForme2").hide(0);
					$("#candel").show(0);
					countIng++;

					moveIngredient(countIng,true);
					$("#candel").draggable({ revert: "invalid",helper:"clone"});
				},2000);

			}
			else if(okCount<3)
			{
				if(tst1==600)
				{
					$("#txtA1 .whatx").show(0);
					$("#txtA1 .whatr").hide(0);
					$("#txtA2 .whatx").hide(0);
					$("#txtA2 .whatr").show(0);
				}
				else if(tst2==6000)
				{
					$("#txtA1 .whatx").hide(0);
					$("#txtA1 .whatr").show(0);
					$("#txtA2 .whatx").show(0);
					$("#txtA2 .whatr").hide(0);
				}
				else
				{

					$("#txtA1 .whatx").hide(0);
					$("#txtA1 .whatr").show(0);
					$("#txtA2 .whatx").hide(0);
					$("#txtA2 .whatr").show(0);
				}
			}
			else
			{
				$("#textopt1").val(600).attr('disbaled','disbaled');
				$("#textopt2").val(6000).attr('disbaled','disbaled');
				$(this).hide(0);
				alert(data.string.exe_data16);
				$("#txtA1 .whatx").show(0);
				$("#txtA2 .whatx").show(0);
				$("#txtA1 .whatr").hide(0);
				$("#txtA2 .whatr").hide(0);

				setTimeout(function(){
					$("#whatquestionForme2").hide(0);
					$("#candel").show(0);
					countIng++;


					moveIngredient(countIng,true);
					$("#candel").draggable({ revert: "invalid",helper:"clone"});
				},3000);
			}



		});

		$("#dropcandel").droppable({
	        drop: function( event, ui ) {
	        	numCandel++;

	        	$(this).append('<img src="'+$ref+'/exercise/images/candle02.png" class="cd'+numCandel+'" />');
	        	if(numCandel==1)
	        	{

	        		$("#submitDn").show(0);
	        	}
	        	if(numCandel==9)
	        	{
	        		$("#candel").hide(0);
	        	}
	        }
		});


		$("#submitDn").click(function(){

			$(this).hide(0);
			$("#candel").hide(0);
			$("#helpMe").removeClass().addClass('helpme10').html(data.string.exe_data6);

			$("#innerquestion").html(data.string.exe_qu7);
			$("#opts1").html((numCandel+2)+ " cd");
			$("#opts2").html(numCandel + " cd");
			$("#innerClickBtn").html(data.string.exe_data5);
			$("#whatquestionForme").show(0);

			$("#explanation").html(data.string.exp_7);
		});

		var optionId;
		$(".mySideBox").find("input[name=radioMe]:radio").click(function(){

			optionId=$(this).parent().parent().attr('id');
			$("#innerClickBtn").show(0);

		});

		$("#innerClickBtn").click(function(){


			$("#explanation").show(0);
			if(optionId=="inopt2")
			{
				$(this).hide(0);
				setTimeout(function()
				{
					$("#whatquestionForme").hide(0);
					$("#helpMe").removeClass().addClass('helpme11').html(data.string.exe_data13);
					$("#congrats").show(0);
					$("#refreshMe").show(0);
					ole.activityComplete.finishingcall();
				},2000);
			}
			$("#"+optionId).find("span").show(0);
		}); //circle


		$("#refreshMe").click(function(){
			location.reload();
		});

	});




}

(function ($) {

	var $nextBtn = $("#activity-page-next-btn-enabled");
/***
* most common variables :D
**/
	var $exWidth = $(".wepEx").width(); /*width of the exercise in total*/
		$refImg = $ref+"/exercise/images/",
		$board = $('.board'),
		$juice =$(".juice"),
		$life =$(".life"),
		$popUp = $('.popUp'),
		$textbox = $(".textbox"),
		$bush = $('.bush'),
		$labelDiv = $(".label"),
		$goToGame = $(".gameOn"),
		$mainChar = $('.mainChar'),
		$mainCharStand = $mainChar.find('.stand'),
		$mainCharWalk = $mainChar.find('.walk'),
		$jumpboy =$(".jumpboy"),
		$animEnv = $(".animEnvironment"),
		$studyroom =$(".studyroom"),
		$bathroom=$(".bathroom"),
		$nameInput = $(".label > p > input[type=text]"),
		$scene = "",
		subSceneCount = 0,
		countNext = 0,
		flagLast ="",
		objCount = 0, /*which object will come*/
		animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
		wronganscount=0;

// load these data at first
function firstLoadData(){
	$(".label > p").text(data.string.e1);
	$(".label > em:nth-of-type(1)").text(data.string.e2);
	$(".label > em:nth-of-type(2)").text(data.string.e4);
	$(".label > em:nth-of-type(3)").text(data.string.e3);
	$(".label > em:nth-of-type(4)").text(data.string.e5);
	$(".label > b > em").text(data.string.e6);
	$(".progresslabel > em:nth-of-type(1)").text(data.string.e9);
	$(".progresslabel > em:nth-of-type(2)").text(data.string.e10);
	$(".progresslabel > em:nth-of-type(3)").text(data.string.e11);
	$(".yourreport").text(data.string.e12);
	// $(".rules > em:nth-of-type(1)").text(data.string.e7);
}



$juice.css({'height':'100%', "background":"rgba(0,255,5,0.8)"});

$nameInput.focus();


/*************************
* defining objects
*************************/
	var Objects = function () {
		this.name = "object";
		this.currentPosition = $exWidth;
		this.positionA = $exWidth;
		this.positionB = -100;
		this.positionM = $exWidth*(2/5);
		this.iD = "#objId";
		this.image = $refImg + "404_not_found.jpg";
		this.instructionTime = false;
		this.check = 0;
		this.state = "start";

		this.moveRight = function() {
			if (this.check===0) {
				this.firstSet();
				this.check = 1;
			};
			this.currentPosition-=1.5;
			// move the object
			$(".objClass").css({left: this.currentPosition+"px"});
			//move the background bush
			$(".bush").css({"background-position":"-=1.5px 0px"});
		},
		this.moveLeft = function() {
			this.currentPosition+=2;
			$(".objClass").css({left: this.currentPosition+"px"});
		}
		this.changeImg = function () {
			// console.log(this.image);
			$(".objClass").attr("src",this.image)
		}
		this.firstSet = function () {
			$(".objClass").attr('id',this.iD);
			$(".objClass").animate({"left":this.positionA+"px"},0);
			this.changeImg();
		}
	}

	var arrayOfObj = ["wheelbag","tree",'tank','house'];

	for (var i = 0; i < arrayOfObj.length; i++) {
		var item = arrayOfObj[i];
		arrayOfObj[i]=new Objects();
		arrayOfObj[i].image = $refImg+item+".png";
		arrayOfObj[i].iD = item;

		//change the mid position of the objects default is 40% from left
		switch(arrayOfObj[i].iD){
			case "tree":arrayOfObj[i].positionM=$exWidth*(3/10);break;
			default:break;
		}
		// console.log(arrayOfObj[i].iD);
	};

/*************************
* defining person
*************************/
var person = {
	fullname : "hello",
	steps : 0,
	energy : 100,
	energyRate : 0.05,
	life : 3,
	lastMoveClicked : 60,
	state : "rest",
	walk : function () {
		$mainCharWalk.show(0);
		$mainCharStand.hide(0);
	},

	changeCharImage : function (charsource, changewidth, changeheight) {
		console.log(charsource);
		$mainCharWalk.attr("src",$refImg+charsource+".gif");
		$mainCharStand.attr("src",$refImg+charsource+".png");
		if (typeof changewidth === "undefined" && typeof changeheight === "undefined") {
			//do nothing
		} else {
			$mainChar.css({"width":changewidth,"height":changeheight});
		}
	},
	stand : function () {
		$mainCharStand.show(0);
		$mainCharWalk.hide(0);
	},
	moveLeft : function () {
		this.steps--;
		this.energy--;
		this.energyBar(this.energy);
	},
	moveRight : function () {
		this.steps++;
		this.energy-=this.energyRate;
		this.energyBar(this.energy);
		this.lastMoveClicked = new Date().getTime();
		if (this.state === "rest") {
			this.walk();
			this.state = "motion";
		};
	},

	energyBar : function (energy) {
		// console.log("this "+energy);
		var red,green,blue =5;
		if (energy > 100){
			person.energy=100; 
		}
		else if (energy>80) {
			green = Math.floor((energy)*255/100);;
			red = Math.floor((100-energy)*100/400);
			// console.log("red"+red);
		} else if (energy>50) {
			green = energy* 255/100;
			red = 255;
		}else if (energy>20) {
			 green = energy* 255/100;
			 red = energy* 100/255-30;
		} else if(energy<20){
			red = 255;
			green = 16;
		}

		if (energy<=0) {
			this.life--;

			if (this.life<0) {
				alert("dead");
			} else {
				this.energy = 100;
				$juice.css({'height':'100%', "background":"rgba(0,255,5,0.8)"});
			}

			switch(this.life){
				case 2: $life.children('span:nth-of-type(3)').css({'opacity':0});break;
				case 1: $life.children('span:nth-of-type(2)').css({'opacity':0});break;
				case 0: $life.children('span:nth-of-type(1)').css({'opacity':0});break;
				default:break;
			}

			// alert("life "+this.life);

		};

		// console.log(red+" "+green);
		$juice.css({'height': energy+'%', "background":"rgba("+red+","+green+","+blue+",1)"});
	},

	motionStop : function (stop) {
		if (typeof stop === "undefined") {
			stop = 0;
		} else {
			stop = 1;
		}
		person.state = "rest";
		person.stand();
	}
}

/*************************
* on key press space the method of GAMELOOP below removes the delay on kepress
and starts the events instantaneously - the delay is caused because of OS...

...search on google for details
*************************/
	var keys = {};
	var manMove = true;
	var gameLoopVariable;

	function gameLoop() {
	    // speed for motion
	    if (keys[32] && manMove) {
	       if (arrayOfObj[objCount].currentPosition<=arrayOfObj[objCount].positionM && arrayOfObj[objCount].state==="start") {

				subSceneCount =0;

				switch (objCount) {
					case 0:
					$scene = "scene0";
					instConvChecker();
					break;

					case 1:
					$scene = "scene1";
					instConvChecker();
					break;

					case 2:
					$scene = "scene3";
					instConvChecker();
					break;

					case 3:
					$scene = "scene4";
					instConvChecker();
					break;

					default:break;
				}
				arrayOfObj[objCount].state = "mid";

			}
			else if (typeof arrayOfObj[objCount].extraStop!== "undefined" && arrayOfObj[objCount].currentPosition<=arrayOfObj[objCount].extraStop && arrayOfObj[objCount].state==="mid") {
				subSceneCount =0;

				switch (objCount) {
					case 2:
					$scene = "scene3Extra";
					instConvChecker();
					break;

					default:
					break;
				}

				arrayOfObj[objCount].state = "midExtra";

			} else if(arrayOfObj[objCount].currentPosition<=arrayOfObj[objCount].positionB && (arrayOfObj[objCount].state==="mid" || arrayOfObj[objCount].state==="midExtra")){
				// alert("reached end");
				arrayOfObj[objCount].state = "end";

			} else if (arrayOfObj[objCount].state === "end") {
				if (objCount<arrayOfObj.length) {
					objCount++;
				};
			} else {
				person.moveRight();
				arrayOfObj[objCount].moveRight();
				// console.log(person.energy);
			}
	    }

	    else if(!keys[32]) {
	    	person.motionStop();
	    }

	    // events for other keys come here

	    // set loop every 20 milliseconds this checks for the keydown event every 20 seconds
	   gameLoopVariable = setTimeout(gameLoop, 20);
	}


$(document).on("keydown",function(event){
	    keys[event.which] = true;
	  });
$(document).on("keyup",function(event){
	 	 keys[event.which] = false;
	  });

/***************
* text box template select
****************/

var textBoxContent = {
	goAhead : {
			type : "instruction",
			content : {
				instructionlist :[
					{instruction:data.string.etd1}
				] }
		},
	sceneIntro : [
		{
			type : "instruction",
			content : {
				instructionlist :[
					{instruction:data.string.etd2}
				] }
		},
		{
			type : "instruction",
			content : {
				instructionlist :[
					{instruction:data.string.etd3},
					{instruction:data.string.etd4}
				] }
		},
		{
			type : "instruction",
			content : {
				instructionlist :[
					{instruction:data.string.etd5},
					{instruction:data.string.etd6}
				] }
		}

	],
	scene0 : [
		{
			type : "question",
			content : {
				question: data.string.etd7,
				options : [
				{correct : "correct", op:data.string.etd8},
				{correct : "incorrect", op:data.string.etd9},
				{correct : "incorrect", op:data.string.etd10},
				{correct : "incorrect", op:data.string.etd11}
				],
				explanation:data.string.etd12
			}
		},
		{
			type : "instruction",
			content : {
				instructionlist :[
					{instruction:data.string.etd13}
				] }
		},
		{
			type : "conversation",
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{person1 : data.string.etd14}
				] }
		},
		{
			type : "question",
			callFunc : afterwheelbag,
			content : {
				question: data.string.etd15,
				options : [
				{correct : "incorrect", op:data.string.etd16},
				{correct : "incorrect", op:data.string.etd17},
				{correct : "correct", op:data.string.etd18},
				{correct : "incorrect", op:data.string.etd19}
				],
				explanation:data.string.etd20
			}
		}

		],
	scene1 : [
		{
			type : "conversation",
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{person1 :data.string.etd21}
				] }
		},
		{
			type : "question",
			callFunc : beforemango,
			content : {
				question: data.string.etd22,
				options : [
					{correct : "correct", op:data.string.etd23},
					{correct : "incorrect", op:data.string.etd24},
					{correct : "incorrect", op:data.string.etd25},
					{correct : "incorrect", op:data.string.etd26}

				],
				explanation:data.string.etd27
			}
		},
		{
			type : "question",
			content : {
				question:data.string.etd28,
				options : [
					{correct : "incorrect", op:data.string.etd29},
					{correct : "incorrect", op:data.string.etd30},
					{correct : "incorrect", op:data.string.etd31},
					{correct : "correct", op:data.string.etd32}
				],
				explanation:data.string.etd33
			}
		},
		{
			type : "conversation",
			callFunc : aftermango,
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{
					person1 : data.string.etd34
					}
				] }
		},
		{
			type : "question",
			content : {
				question:data.string.etd81,
				options : [
					{correct : "incorrect", op:data.string.etd82},
					{correct : "incorrect", op:data.string.etd83},
					{correct : "correct", op:data.string.etd84},
					{correct : "incorrect", op:data.string.etd85}					
				],
				explanation:data.string.etd86
			}
		}
	],
	scene3 : [
		{
			type : "conversation",
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{
					person1 : data.string.etd35,
					person2 : data.string.etd36
					},
					{
					person2 : data.string.etd37
					},
					{
					person1 : data.string.etd38
					}
				] }
		},
		{
			type : "question",
			callFunc : pullKey,
			content : {
				question: data.string.etd39,
				options : [
				{correct : "correct", op:data.string.etd40},
				{correct : "incorrect", op:data.string.etd41},
				{correct : "incorrect", op:data.string.etd42},
				{correct : "incorrect", op:data.string.etd43},
				],
				explanation:data.string.etd44
			},


		},
		{
			type : "conversation",
			callFunc : keyMagnet,
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{
					person2 : data.string.etd45
					},
					{
					person1 : data.string.etd46
					},
					{
					person2 : data.string.etd47
					}
				] }
		}
	],
	scene3Extra : [
		{
			type : "conversation",
			callFunc : turnGirl,
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
				{
					person2 :data.string.etd48
				}
				]
			}
		},
		{
			type : "conversation",
			callFunc : doublePullBag,
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
				{
					person1 :data.string.etd49
				}
				]
			}
		}
	],
	scene4 : [
		{
			type : "question",
			content : {
				question: data.string.etd50,
				options : [				
				{correct : "incorrect", op:data.string.etd51},
				{correct : "incorrect", op:data.string.etd52},
				{correct : "correct", op:data.string.etd53},
				{correct : "incorrect", op:data.string.etd54}
				],
				explanation:data.string.etd55
			},

		},
		{
			type : "conversation",
			callFunc : lastHouse,
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
				{
					person2 : data.string.etd56
				},
				{
					person1 : data.string.etd57
				},
				{
					person2 : data.string.etd58
				},
				{
					person1 : data.string.etd59
				}

				] }
		}
	],
	lastHouse : {
		instructionlist : [
			{instruction:data.string.etd60}
		]
	},
	sceneHome : [
		{
			type : "conversation",
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{
					person1 : data.string.etd61
					}
				] }
		},
		{
			type : "conversation",
			callFunc:insideBathroom,
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{
					person1 : data.string.etd62
					}
				] }
		},
		{
			type : "conversation",
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{
					person1 : data.string.etd63
					}
				] }
		},
		{
			type : "question",
			content : {
				question: data.string.etd64,
				options : [
					{correct : "correct", op:data.string.etd65},
					{correct : "incorrect", op:data.string.etd66},
					{correct : "incorrect", op:data.string.etd67},
					{correct : "incorrect", op:data.string.etd68}
				],
				explanation:data.string.etd69
			}
		},
		{
			type : "conversation",
			callFunc:insideStudyroom,
			content : {
				malePic :$refImg+"male.png",
				femalePic :$refImg+"female.png",
				conversationlist : [
					{
					person1 : data.string.etd70
					},
					{
					person1 : data.string.etd71
					},
					{
					person1 : data.string.etd72
					}
				] }
		},
		{
			type : "instruction",
			content : {
				instructionlist :[
					{instruction:data.string.etd73}
				] }
		},
		{
			type : "question",
			content : {
				question: data.string.etd74,
				options : [
					{correct : "incorrect", op:data.string.etd75},
					{correct : "correct", op:data.string.etd76},
					{correct : "incorrect", op:data.string.etd77},
					{correct : "incorrect", op:data.string.etd78},
				],
				explanation:data.string.etd79
			}			
		},
		{
			type : "instruction",
			callFunc : finishflagon,
			content: {
				instructionlist : [
					{ instruction : data.string.etd80}
				]}
		}
	],
	gameEnd :{
		instructionlist : [
			{instruction:data.string.etd80}
		]
	}
};


/*
* find the scene and the text display type for particular scene
*/
function instConvChecker () {
	var scene = textBoxContent[$scene],
		len = scene.length;
	if (subSceneCount>=len) {
		if(flagLast=== "house"){
			clickHouseSay();
		} else if (flagLast === "finish") {
			gameEndsHere();
		} else {
			manMove=true;
			myTemplate("instruction",textBoxContent.goAhead.content,goAhead);
		}

	} else {
		person.stand();
		manMove=false;
		var type = scene[subSceneCount].type,
			hasFunc = scene[subSceneCount].callFunc,
			content = scene[subSceneCount].content;
		switch (type) {
			case "conversation":
				myTemplate("conversation",content,myConversation);
			break;

			case "instruction" :
				myTemplate("instruction",content,myinstruction);
			break;

			case "question" :
				myTemplate("qop",content,myquestion);
			break;
			case "questionFig" :
				myTemplate("qopfig",content,myfigquestion);
			break;
		}

		// if a function call is in between it is called
		if (typeof hasFunc != "undefined") {
			hasFunc();
		};
		subSceneCount++;

	}
}

// function for updating template
function myTemplate(templateType,content,funCall){
	// console.log(content);
	var source = $("#"+templateType+"-template").html(); //why need one extra var :P
	var template = Handlebars.compile(source);
	var html;
	switch(templateType){
		case "qop":
		html = template(content);
		break;

		case "qopfig":
		html = template(content);
		break;

		case "conversation":
		html = template(content);
		break;

		case "instruction":
		html = template(content);
		break;

		default:break;
	}

	// console.log(html);
	$textbox.html(html);
	// console.log(content);
	if (typeof funCall === "undefined") {
		//do nothing
	} else {
		funCall();
	}
}

/*
* function to check the options
*/
function optionSelectCheck () {
	var $qbank = $textbox.children(".qbank"),
		$currentquestion = $qbank.children('.questions'),
		$explain = $qbank.children('.explain'),
		$areyoucorrect = $explain.children('.areyoucorrect'),
		$correctsign = $currentquestion.children('.correctsign'),
		$currentOptionBox = $qbank.children('.opt'),
		$currentOptionBoxType = $currentOptionBox.attr("data-opt");

	var inCorrectCss={"cursor":"default","background":"#EA2E49"};
	var correctCss={"cursor":"default","background":"lawngreen"};

	// if question is figure type call this handler
	if($currentOptionBoxType == "figopt"){

		$currentOptionGroup = $currentOptionBox.children("img");
		$currentOptionBox.on('click', 'img', function() {
			var selected =$(this);
			functioncheck(selected,$currentOptionGroup);
		});
	}

	// else if question is para type call this handler
	else if($currentOptionBoxType == "paraopt"){
		$currentOptionGroup = $currentOptionBox.children("p");
		$currentOptionBox.on('click', 'p', function() {
			var selected =$(this);
			functioncheck(selected,$currentOptionGroup);
		});
	}

	// function to check if clicked option is correct or not
	function functioncheck(selected, currentOptionGroup) {
		var myOption = selected.attr("data-check");
		if(myOption != "correct"){
			// all the color codes and information
			$correctsign.addClass('glyphicon-remove').css({"color":"#EA2E49"});
			if($currentOptionBoxType == "paraopt"){
				selected.css(inCorrectCss);
			}
			currentOptionGroup.css({"pointer-events":"none", "cursor":"default"});
			$areyoucorrect.html(data.string.e7);
			$explain.show(0);
			$textbox.find(".playOn").show(0);
			person.energyBar(person.energy-=20);
			// increase wrong ans count
			wronganscount++;
		}

		else if(myOption == "correct"){
			// all the color codes and information
			$correctsign.addClass('glyphicon-ok').css({"color":"lawngreen"});
			if($currentOptionBoxType == "paraopt"){
				selected.css(correctCss);
			}
			currentOptionGroup.css({"pointer-events":"none", "cursor":"default"});
			$areyoucorrect.html(data.string.e8);
			$explain.show(0);
			$textbox.find(".playOn").show(0);
		}
	}
};

/*
* function is called to display conversation
*/
function myConversation(){
	var $conversation = $textbox.children('.conversation').attr("class");
	var numberOfConversation = $("."+$conversation+"> p").length;
	var counterInstruction = 1;
	$("."+$conversation).children('p:nth-of-type('+counterInstruction+')').show(0).addClass("animated fadeIn");
	counterInstruction++;
	var intervalSetting = setInterval(function () {
		$("."+$conversation).children('p:nth-of-type('+counterInstruction+')').show(0).addClass("animated fadeIn");
		counterInstruction++;
		if (counterInstruction>numberOfConversation) {
			clearInterval(intervalSetting);
			$textbox.find(".playOn").show(0);
		};
	},1000);
};

/*
* function is called to display instruction
*/
function myinstruction(){
	// console.log("i reached here after");
	var $instruction = $textbox.children('.instruction').attr("class");
	var numberOfInstruction = $("."+$instruction+"> p").length;
	var counterInstruction = 1;
	var intervalSetting = setInterval(function () {
		$("."+$instruction).children('p:nth-of-type('+counterInstruction+')').fadeIn(400);
		counterInstruction++;
		if (counterInstruction>numberOfInstruction) {
			clearInterval(intervalSetting);
				$textbox.find(".playOn").show(0);
		};
	}, 200);
};


/*
* function is called to display question with text options
*/
function myquestion(){
	optionSelectCheck();
};

/*
* function is called to display question with image options
*/
function myfigquestion(){
	optionSelectCheck();
};

/*
* key and magnet loading
*/
function pullKey () {
	$mainChar.hide(0);
	$(".objClass").hide(0);
	$animEnv.html("<img id='pullKey' src='"+$refImg+"pullkey"+".gif' >").show(0);
}

function keyMagnet () {
	$animEnv.html(" ").hide(0);
	$mainChar.show(0);
	$(".objClass").show(0);
	arrayOfObj[2].extraStop = 10;
	console.log(arrayOfObj[objCount]);
	$("#tank").attr('src',$refImg+"tank2.png");
	person.energyRate=0.03;
}

function turnGirl () {
	$("#tank").attr('src',$refImg+"tank3.png");
}

function doublePullBag () {
	$("#tank").attr('src',$refImg+"tank4.png");
	person.changeCharImage("mainchar3");
	person.energyRate=0.02;
}

/*
* function is called at end of each scene to write go ahead
*/
function goAhead () {
	var $instruction = $textbox.children('.instruction');
	$instruction.children('p').show(200);
};

/*
* functions are called at end of each scene to write go ahead
*/
function afterwheelbag(){
	$("#wheelbag").attr('src',$refImg+"bag.png");
	person.changeCharImage("mainchar2");
	person.energyRate=0.03;
};

function beforemango(){
	$mainChar.css("display","none");
	$jumpboy.show(0);
};

function aftermango(){
	$jumpboy.css("display","none");
	$mainChar.show(0);
	person.energyBar(person.energy+=30);
};

function insideBathroom(){
	$studyroom.css({"left":"-100%"});
	$bathroom.css({"left":"0%"});
};

function insideStudyroom(){
	$studyroom.attr('src', $refImg+'studyroom1.png');
	$studyroom.css({"left":"0%"});
	$bathroom.css({"left":"100%"});
};

/*
*last msg for house
*/
function lastHouse () {
	flagLast = "house";
};

/*
* click
*/
function clickHouseSay () {
	myTemplate("instruction",textBoxContent.lastHouse,houseClick);
};

function houseClick () {
	var $instruction = $textbox.children('.instruction').attr("class");
	$("."+$instruction).children('p').fadeIn(400);
	$("#house").css("cursor","pointer");
	$("#house").on("click",function () {
		clearInterval(gameLoopVariable);
		$("document").off("keydown");
		$("document").off("keyup");
		$(".untilHome").fadeOut();

		$(".home").fadeIn(function (){
			// reset the subSceneCount to 0
			subSceneCount =0;
			$scene = "sceneHome";
			instConvChecker();
		})

	})
};

function finishflagon(){
	flagLast = "finish";
	$bathroom.attr('src', $refImg+'bathroom2.png');
	$studyroom.css({"left":"-100%"});
	$bathroom.css({"left":"0%"});
};

function ohfinal(){
	var $instruction = $textbox.children('.instruction').attr("class");
	$("."+$instruction).children('p').fadeIn(400);
	$bathroom.attr('src', $refImg+'bathroom2.png');
	$studyroom.css({"left":"-100%"});
	$bathroom.css({"left":"0%"});
	// setTimeout(function(){
		$(".gamefinaldiv").css('top', '0%');
			setTimeout(function(){
				var color;
				var correctanspercent= ((10-wronganscount)/10)*100;
				if(correctanspercent <33){
					color="rgba(255, 38, 56,0.8)";
				}
				else if(correctanspercent > 33 && correctanspercent < 66){
					color="rgba(255, 81, 0,0.8)";
				}
				else if(correctanspercent > 66){
					color="rgba(44, 98, 23,0.8)";
				}
				$(".progressreport").css({'height':correctanspercent+"%","background":color});
					$nextBtn.show(0);
				}, 1000);
	// },1);
};

function gameEndsHere(){	
	myTemplate("instruction",textBoxContent.gameEnd,ohfinal);
};



/*
* on click playon
*/
$textbox.on('click','.playOn',function () {
	$(this).hide(0);
	instConvChecker();
});


/*
* call all the first load functions below
*/
function callInitialFunc () {	
	gameLoop();
	$scene = "sceneIntro";
	instConvChecker();
};


// on input name
$nameInput.on("keyup",function() {		
		// get the input value and change it to toLowerCase
		var inputlength = $(this).val().length;
		// check for the input answer
		if(inputlength >= 4){
			$goToGame.show(0);
		}

		else if(inputlength <= 4){
			$goToGame.css('display', 'none');
		}
			
	});

/*************************
* events to occur on go to game button click
*************************/
$goToGame.on("click",function() {
	person.fullname = $nameInput.val();
	$goToGame.css('display', 'none');
	$nameInput.off("keyup");
	// $nameInput.prop("disabled",true);
	$goToGame.off("click");
	$labelDiv.css('display', 'none');
	$labelDiv.remove();
	callInitialFunc();
});

// call the function for first data load data
firstLoadData();

/*on next button click*/
$nextBtn.on('click',function() {
	ole.activityComplete.finishingcall();
});

})(jQuery)   

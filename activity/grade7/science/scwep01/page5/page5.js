// function for animation of slideB
function slideBanim(){
	animSlideB = false;
	$("#sun").fadeIn(500,function(){
		$("#food").fadeIn(500,function(){
			$("#river").fadeIn(500,function(){
				$("#toSlideC").show(0);//.addClass('blinkEffect');
				$("#toSlideA").show(0);
			});
		});
	});
}

// function for animation of slideC
function slideCanim(){
	animSlideC = false;
	/*$(".pull-right > a").addClass('nextPageBlink');*/
	ole.footerNotificationHandler.pageEndSetNotification();
	$("#toSlideBfromC").show(0);
}

// variables for animating the slides for only first entry
var animSlideB=true;
var animSlideC=true;

$(document).ready(function() {

	loadTimelineProgress(3,1);

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#toSlideB").html($whatnextbtn);
	$("#toSlideC").html($whatnextbtn);

	var $whatnprevbtn=getSubpageMoveButton($lang,"prev");

	$("#toSlideA").html($whatnprevbtn);
	$("#toSlideBfromC").html($whatnprevbtn);




	// pull all data
	$("#energyDef").text(data.string.pg5s1);
	$("#forExample").text(data.string.pg5s2);
	$("#lightBulb > p").text(data.string.pg5s3);
	$("#manCarry > p").text(data.string.pg5s4);
	$("#manPlay > p").text(data.string.pg5s5);
	$("#tableFan > p").text(data.string.pg5s16);

	$("#slideBtext").text(data.string.pg5s6);
	$("#sun > p").text(data.string.pg5s17);
	$("#food > p").text(data.string.pg5s18);
	$("#river > p").text(data.string.pg5s19);

	$("#slideCtext").text(data.string.pg5s7);

	$("#heatEnergy > p").text(data.string.pg5s8);
	$("#soundEnergy > p").text(data.string.pg5s9);
	$("#gravitationalEnergy > p").text(data.string.pg5s10);
	$("#electricalEnergy > p").text(data.string.pg5s11);
	$("#lightEnergy > p").text(data.string.pg5s12);
	$("#magneticEnergy > p").text(data.string.pg5s13);
	$("#chemicalEnergy > p").text(data.string.pg5s14);
	$("#atomicEnergy > p").text(data.string.pg5s15);


	// at first on page load
	$("#energyDef").addClass('slideTextEffect');
	$("#energyDef").on('animationend webkitAnimationEnd', function(){
		$("#forExample").show(500,function(){
			// display the four examples one by one each
			$("#lightBulb").fadeIn(1000);
			$("#manCarry").delay(700).fadeIn(1000);
			$("#manPlay").delay(1500).fadeIn(1000,function(){
				$("#tableFan > img:nth-of-type(2)").addClass('fanRotate');
			});
			$("#tableFan").delay(2200).fadeIn(500,function(){
				$("#toSlideB").show(0);;//.addClass('blinkEffect');
			});

		});
	});


	//to show the answer on clicking the showAns button
	$("#toSlideB").on('click', function() {


		loadTimelineProgress(3,2);
		//$("#toSlideB").removeClass('blinkEffect');

		$("#lab5slideA").hide(0);
		$("#lab5slideC").hide(0);
		$("#lab5slideB").show(0);

		// if first time on slideB do following animations
		if(animSlideB){
			slideBanim();
		}
	});


	// on clicking to slideA
	$("#toSlideA").on('click', function() {

		loadTimelineProgress(3,1);
		ole.footerNotificationHandler.hideNotification();
		$("#lab5slideA").show(0);
		$("#lab5slideC").hide(0);
		$("#lab5slideB").hide(0);

		});

	// on clicking toSlideC
	//to show the answer on clicking the showAns button
	$("#toSlideC").on('click', function() {

		loadTimelineProgress(3,3);
		//$("#toSlideB").removeClass('blinkEffect');

		$("#lab5slideA").hide(0);
		$("#lab5slideC").show(0);
		$("#lab5slideB").hide(0);

		// if first time on slideB do following animations
		if(animSlideC){
			slideCanim();
		}
		else{
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

// going to slideB from slideC
	$("#toSlideBfromC").on('click', function() {
		ole.footerNotificationHandler.hideNotification();
		loadTimelineProgress(3,2);
		$("#lab5slideA").hide(0);
		$("#lab5slideC").hide(0);
		$("#lab5slideB").show(0);
	});
});

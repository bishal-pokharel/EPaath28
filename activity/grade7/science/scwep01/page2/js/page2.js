
$(document).ready(function() {

	loadTimelineProgress(3,1);

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#enterGravity").html($whatnextbtn);
	$("#conclusionSpeech").html($whatnextbtn);

	// pull all data
	$("#workDef").text(data.string.pg2s1);
	$("#wl2instruction").text(data.string.pg2s2);
	$("#whatHappenFrictionPart1").text(data.string.pg2s3);
	$("#whatHappenFrictionPart2").text(data.string.pg2s4);
	$("#whatHappenFrictionPart3").text(data.string.pg2s5);

	$("#whatHappenGravityPart1").text(data.string.pg2s8);
	$("#whatHappenGravityPart2").text(data.string.pg2s9);
	$("#whatHappenGravityPart3").text(data.string.pg2s10);

	$("#conclusionWork").text(data.string.pg2s12);
	$("#workAgainstFrictionDef").text(data.string.pg2s6);
	$("#workAgainstGravityDef").text(data.string.pg2s11);

	$("#forceLabel").text(data.string.pg2s13);
	$("#frictionLabel").text(data.string.pg2s14);
	$("#forceAgainstGravityLabel").text(data.string.pg2s13);
	$("#gravityLabel").text(data.string.pg2s15);

	// for highlighting the required text
	ole.parseToolTip("#workDef","दुरी");
	ole.parseToolTip("#workDef","बलको");
	ole.parseToolTip("#workDef","कार्य");
	ole.parseToolTip("#workDef","जुल");

	$("#workDef").children('span:nth-of-type(n+1)').css({"color":"brown"});

	// continiously blinking hand
	$("#pushFinger").addClass("blinkEffect");

	// what to do on clicking the finger
	$("#pushFinger").on('click', function(){
		// disable clicking of the pushFinger image after it is clicked once

			$("#pushFinger").off("click");
			$("#pushFinger").removeClass("blinkEffect");

			// hide the instruction1
			$("#wl2instruction").hide(0);

			// $("#stickyNotes2, #instruction1").effect("explode",500);
			$("#pushFinger").animate({"opacity":"1"},0,function(){

				// show the arrow and its animation
				$("#pushForce").show(0);
				$("#pushFriction").show(0);
				//make the arrows blink
				$("#forceArrow, #frictionArrow").addClass("blinkEffect");


					$("#pushFinger").animate({"left":"63%"},6000,"linear",function(){
						// display this text
						$("#whatHappenFrictionPart1").delay(1000).fadeIn(500);
						// display this text
						$("#whatHappenFrictionPart2").delay(2500).fadeIn(500);

						$("#pushBoxLeft").fadeIn(500);
						$("#whatHappenFrictionPart3").delay(4000).fadeIn(500);

							// remove blinkEffect in arrow
						$("#forceArrow, #frictionArrow").removeClass("blinkEffect");

						// show the enterGravity button
						$("#enterGravity").delay(4500).fadeIn(0).addClass("blinkEffect");

				});
				$("#pushBoxRight").animate({"left":"73%"},6000,"linear"); //this is moved along with finger
				$("#pushForce").animate({"left":"53%"},6000,"linear");
				$("#pushFriction").animate({"left":"72%"},6000,"linear");
			});
	});


	//what to do on clicking enterGravity arrow
	$("#enterGravity").on('click', function() {

		loadTimelineProgress(3,2);
		$("#enterGravity").off('click');
		$("#enterGravity").fadeOut(0).removeClass("blinkEffect");
		$("#pushDiv, #whatHappenFrictionPart1, #whatHappenFrictionPart2, #whatHappenFrictionPart3").delay(300).fadeOut(0,function(){
			$("#gravityDiv").fadeIn(500,function(){
					$("#openHand").addClass("blinkEffect");
					$("#wl2instruction").text(data.string.pg2s7).css('left', '40%').show(0);
				});

		});
	});

	// on clicking the openHand image on gravity div
	$("#openHand").on('click', function() {
		$("#openHand").off('click');
		$("#wl2instruction").hide(0);
		$("#openHand").removeClass("blinkEffect");

		// let the hand go upto to bucket first
		$("#openHand").animate({"top":"40%"},2000, "linear", function() {
			// display the arrows
			$("#forceAgainstGravity, #gravity").fadeIn(1000);

			//make arrows blink
			$("#forceAgainstGravityArrow, #gravityArrow").addClass("blinkEffect");
			// catch the bucket now
			$("#openHand, #bucket").animate({"opacity":"0"},300);
			$("#pickBucket").animate({"opacity":"1"},300);

				// make the bucket go up
				$("#pickBucket").animate({"top":"0%"},3000,"linear",function(){
					// display this text
					$("#whatHappenGravityPart1").fadeIn(500);
					$("#bucket").animate({"opacity":"0.4"},1000,function(){
							$("#whatHappenGravityPart2").fadeIn(1000,function(){

								$("#conclusionSpeech").delay(1000).fadeIn("500").addClass("blinkEffect");
								$("#whatHappenGravityPart3").show(2000,function(){

								});
							});
					});
				});
				// together the with bucket the arrows move up
			$("#forceAgainstGravity, #gravity").animate({"top":"32%"},2500,"linear",function(){
				//remove blink from arrows
			$("#forceAgainstGravityArrow, #gravityArrow").removeClass("blinkEffect");
			});
		});
	});


	//on clicking conclusion speech
	$("#conclusionSpeech").on('click', function() {

			loadTimelineProgress(3,3);
		// disable click on this element
		$("#conclusionSpeech").off('click').hide(0);
		// hide gravity div and associated text
		$("#gravityDiv, #whatHappenGravityPart1,#whatHappenGravityPart2,#whatHappenGravityPart3").hide(0);
		//display this text
		$("#conclusionWork").fadeIn(1000,function(){
			// hide all arrow labels
			$("#forceAgainstGravityLabel, #gravityLabel, #forceLabel, #frictionLabel").hide(0);

			// make push and gravity div small and place it as screenshots
			$("#pushDiv").css({"top":"20%","left":"18%"}).show(0);

			$("#gravityDiv").css({"top":"20%","left":"-10%"}).show(0);

			$("#workAgainstFrictionDef, #workAgainstGravityDef").delay(1000).fadeIn(1000,function(){
				/*$(".pull-right > a").addClass('nextPageBlink');*/
				ole.footerNotificationHandler.pageEndSetNotification();
			});
		});

	});
});

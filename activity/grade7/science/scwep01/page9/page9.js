// execute this function when all the items have been dropped to their proper places
function rotateFan(){
	$("#dropRiver > span").show(1000,function(){
		$("#dropTransmission > span").show(1000,function(){
			$("#window").fadeIn(2000,function(){
				$("#dropHome > span").show(1000,function(){
				$("#fan").addClass('fanRotateFast');
				$("#lab9conclusion").delay(1000).fadeIn(0).addClass('blurryText');
				$("#lab9conclusion2").delay(2000).fadeIn(0).addClass('blurryText');
				$("#lab9instr,#lab9instr2").fadeOut(1000,function(){
					$("#electricEnergy").show(1000,function(){
						/*$(".pull-right > a").addClass('nextPageBlink');*/
						ole.footerNotificationHandler.pageEndSetNotification();
					});					
				});
				
			});
			});
			
		});
	});
}

$(document).ready(function() {	

	loadTimelineProgress(1,1);
	
	// pull all data
	$("#lab9instr").text(data.string.pg9s1);
	$("#lab9instr2").text(data.string.pg9s2);
	$("#lab9conclusion").text(data.string.pg9s3);
	$("#lab9conclusion2").text(data.string.pg9s4);
	$("#electricEnergy").text(data.string.pg9s5);

	// for the tooltips
	// $("#dragRiver").attr('title',data.string.pg9s6);

	// variable counter for counting number of items dropped if total is 4 then rotate fan
	droppedItems=0;

	// define dragGrp images as draggable
	$("#dragGrp > img").draggable({"containment":"#wepLab9", "revert":"invalid","opacity":"0.7"});

	//defing the droppable div and what to do 
	// for river
	$("#dropRiver").droppable({"accept":"#dragRiver",
								over: function() {
									$("#dropRiver").addClass('glowAnim');
								},
								out: function() {
									$("#dropRiver").removeClass('glowAnim');
								},
								drop: function() {
									// on dropping the battery do the following
									droppedItems++;

									$("#dropRiver").droppable("disable").removeClass('glowAnim');
									$("#dragRiver").draggable("disable").hide(0);

									$("#river").fadeIn(500);

									if(droppedItems==4){
										rotateFan();
									}

								},
								});
	// for transmission
	$("#dropTransmission").droppable({"accept":"#dragTransmission",
								over: function() {
									$("#dropTransmission").addClass('glowAnim');
								},
								out: function() {
									$("#dropTransmission").removeClass('glowAnim');
								},
								drop: function() {
									// on dropping the battery do the following
									droppedItems++;

									$("#dropTransmission").droppable("disable").removeClass('glowAnim');
									$("#dragTransmission").draggable("disable").hide(0);

									$("#transmission").fadeIn(500);

									if(droppedItems==4){
										rotateFan();
									}

								},
								});
	// for plug
	$("#dropHome").droppable({"accept":"#dragHome",
								over: function() {
									$("#dropHome").addClass('glowAnim');
								},
								out: function() {
									$("#dropHome").removeClass('glowAnim');
								},
								drop: function() {
									// on dropping the battery do the following
									droppedItems++;

									$("#dropHome").droppable("disable").removeClass('glowAnim');
									$("#dragHome").draggable("disable").hide(0);

									$("#home").fadeIn(500);

									if(droppedItems==4){
										rotateFan();
									}

								},
								});

	// for fan
	$("#dropFan").droppable({"accept":"#dragFan",
								over: function() {
									$("#dropFan").addClass('glowAnim');
								},
								out: function() {
									$("#dropFan").removeClass('glowAnim');
								},
								drop: function() {
									// on dropping the battery do the following
									droppedItems++;

									$("#dropFan").droppable("disable").removeClass('glowAnim');
									$("#dragFan").draggable("disable").hide(0);

									$("#fanBody, #fan").fadeIn(500);

									if(droppedItems==4){
										rotateFan();
									}

								},
								});
});
				
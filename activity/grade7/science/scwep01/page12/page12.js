$(document).ready(function() {	

	loadTimelineProgress(1,1);
	
	// pull all data
	$("#whatHappened").text(data.string.pg12s1);
	$("#wepLab12Q").text(data.string.pg12s2);
	$("#firstOption").text(data.string.pg12s3);
	$("#secondOption").text(data.string.pg12s4);
	$("#conclusionGravity").text(data.string.pg12s7);
	$("#gravityEnergy").text(data.string.pg12s8);

	// what to do on selecting the options
		$("#firstOption, #allText > input:nth-of-type(1)").on('click', function() {
			$("#allText > input:nth-of-type(1)").prop('checked', true);
			$("#firstOption").css({"text-decoration":"line-through","color":"darkred"});
			// disable click once it is clicked
			$("#firstOption, #allText > input:nth-of-type(1)").off("click");
			// show correct or incorrect option
			$("#checkOption").show(0).text(data.string.pg12s6).css("color","darkred");
		});

		$("#secondOption, #allText > input:nth-of-type(2)").on('click', function() {
			$("#allText > input:nth-of-type(2)").prop('checked', true);
			$("#secondOption").css({"color":"darkgreen"});

			// disable all radio options click
			$("#firstOption, #secondOption, #allText > input:nth-of-type(n+1)").off("click");
			$("#checkOption").show(0).text(data.string.pg12s5).css("color","darkgreen");

			$("#conclusionGravity").show(1000,function(){
				$("#wepLab12Q, #allText > input, #allText > span, #checkOption").slideUp(100,function(){
					/*$(".pull-right > a").addClass('nextPageBlink');*/
					$("#gravityEnergy").show(100,function(){
						ole.footerNotificationHandler.pageEndSetNotification();
					});
					
				});
			})
		});

	// rock fall animation
	$("#rock").addClass('rockBlinkEffect');
	$("#rock").on('animationend webkitAnimationEnd', function(){
		$("#rock").addClass('shakeRock');
		$("#rock").on('animationend webkitAnimationEnd', function(){
 			$("#rock").removeClass('shakeRock').addClass("startFallRock");

 				$("#rock").on('animationend webkitAnimationEnd', function(){
 					$("#rock").addClass("fallingRock");

 						$("#rock").on('animationend webkitAnimationEnd', function(){
 									$("#stone").hide(0);
 									$("#brokeStone").show(0);
 									$("#whatHappened").delay(2000).fadeIn(1000,function(){
 										$("#wepLab12Q, #allText > input, #allText > span").fadeIn(1000);
 									});
 								});
 					});
 			});
		});
});
				
// global variable for if you can click
var canIclick=true;
var clickCount = 0;
$(document).ready(function() {

	ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg1n1);
	loadTimelineProgress(1,1);

	// pull required data at first
	$("#workTopic").text(data.string.pg1s1);
	$("#whatIsWork").text(data.string.pg1s2);
	$("#eg1Caption").text(data.string.pg1s3);
	$("#eg2Caption").text(data.string.pg1s4);
	$("#eg3Caption").text(data.string.pg1s5);
	$("#eg4Caption").text(data.string.pg1s6);
	$("#girlText").text(data.string.pg1s7);
	$("#speech").text(data.string.pg1s11);

	// on mouseover circle the images with orange

	$("#eg1,#eg2,#eg3,#eg4").on('mouseover', function() {
		$(this).addClass('workEgHoverEffect');
	});

	$("#eg1,#eg2,#eg3,#eg4").on('mouseout', function() {
		$(this).removeClass('workEgHoverEffect');
	});

	// on clicking each eg do following
	$("#eg1").on('click', function() {

		if(canIclick)
		{

			canIclick=false;
		//change cursor as indicating u cant click =)
			$("#eg1, #eg2,#eg3,#eg4").css({"cursor":"default"});

		//hide speechBubble at first
		$("#speech, #speechPoint").hide(0);

		// hide all other and show the clicked example explanation
		$("#eg2Explain,#eg3Explain,#eg4Explain").hide(0);
		$("#eg1Explain").show(0);
		// set to default all the animated images and divs
		$("#womanSit").css({'opacity':'1'});
		$("#womanUp").css({'opacity': '0'});
		$("#womanWalk").css({'opacity': '0','left':"16%"});
		$("#arrow1").css({'opacity': '0','width':"0%"});

		//remove the click effect on all the other images except the clicked one
		$("#eg2,#eg3,#eg4").removeClass('workEgClickEffect');
		$("#eg1").addClass('workEgClickEffect');

		$("#womanSit").delay(1000).animate({"opacity":"0"},200,function(){
			$("#womanWalk").animate({"opacity":"1"},200,function(){
				$("#womanWalk").animate({"left":"45%"},5000,"linear",function(){
					$("#womanWalk").animate({"opacity":"0"},100,function(){
						$("#womanUp").animate({"opacity":"1"},100,function(){
							$("#womanSit").animate({"opacity":"0.5"},100,function(){
								// change what cartoon says
								$("#speech").text(data.string.pg1s7);
								// display speechBubble
								$("#speech, #speechPoint").fadeIn(500);
								$("#arrow1").delay(500).animate({"opacity":"0.8","width":"35%"},1000,function(){
									canIclick=true;
									$("#eg1, #eg2, #eg3, #eg4").css({"cursor":"pointer"});
									clickCount++;
									console.log(clickCount);
									if(clickCount==4){
										ole.footerNotificationHandler.pageEndSetNotification();
									}
									ole.footerNotificationHandler.setNotificationShowNextPagebutton("thap udhaharan hernu lai click gara, natra aarko page jao");
								});
							});
						});

					});

				});

			});
		});
	}
	});

	$("#eg2").on('click', function() {
		if(canIclick)
		{

			canIclick=false;
			$("#eg1, #eg2, #eg3, #eg4").css({"cursor":"default"});

		// hide all other and show the clicked example explanation
		$("#eg1Explain,#eg3Explain,#eg4Explain").hide(0);
		$("#eg2Explain").show(0);

		//hide speechBubble at first
		$("#speech, #speechPoint").hide(0);

		// set to default all the animated images and divs
		$("#bulbOn").css({'opacity':'0'});


		//remove the click effect on all the other images except the clicked one
		$("#eg1,#eg3,#eg4").removeClass('workEgClickEffect');
		$("#eg2").addClass('workEgClickEffect');

		// animate the bulb

		$("#bulbOn").delay(1000).animate({"opacity":"1"},500,function(){

						// change what cartoon says
						$("#speech").text(data.string.pg1s8);
						// display speechBubble
						$("#speech, #speechPoint").fadeIn(500);

						canIclick=true;
						$("#eg1, #eg2, #eg3, #eg4").css({"cursor":"pointer"});
						clickCount++;
						console.log(clickCount);
						if(clickCount==4){
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					});

	}
});

$("#eg3").on('click', function() {
		if(canIclick)
		{

			canIclick=false;
			$("#eg1, #eg2, #eg3, #eg4").css({"cursor":"default"});

		// hide all other and show the clicked example explanation
		$("#eg1Explain,#eg2Explain,#eg4Explain").hide(0);
		$("#eg3Explain").show(0);

		//hide speechBubble at first
		$("#speech, #speechPoint").hide(0);

		// set to default all the animated images and divs
		$("#boxLeft").css({'opacity':'1'});
		$("#boxRight").css({'opacity': '0'});
		$("#boxWalk").css({"left":"5%","opacity":"0"});
		$("#arrow3").css({'opacity': '0','width':"0%"});

		//remove the click effect on all the other images except the clicked one
		$("#eg1,#eg2,#eg4").removeClass('workEgClickEffect');
		$("#eg3").addClass('workEgClickEffect');

		$("#boxLeft").delay(1000).animate({"opacity":"0"},50,function(){
			$("#boxWalk").animate({"opacity":"1"},50,function(){
				$("#boxWalk").animate({"left":"45%"},7000,"linear",function(){
					$("#boxWalk").animate({"opacity":"0"},100);
					$("#boxRight").delay(100).animate({"opacity":"1"},100,function(){
						$("#boxLeft").animate({"opacity":"0.4"},500);
						// change what cartoon says
						$("#speech").text(data.string.pg1s9);
						// display speechBubble
						$("#speech, #speechPoint").fadeIn(500);
						$("#arrow3").delay(500).animate({"opacity":"0.8","width":"40%"},1000,function(){
						canIclick=true;
						$("#eg1, #eg2, #eg3, #eg4").css({"cursor":"pointer"});
						clickCount++;
						console.log(clickCount);
						if(clickCount==4){
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					});
					});

				});

			});
		});
	}
});

$("#eg4").on('click', function() {
		if(canIclick)
		{

			canIclick=false;
			$("#eg1, #eg2, #eg3, #eg4").css({"cursor":"default"});

		// hide all other and show the clicked example explanation
		$("#eg1Explain,#eg2Explain,#eg3Explain").hide(0);
		$("#eg4Explain").show(0);

		//hide speechBubble at first
		$("#speech, #speechPoint").hide(0);


		//remove the click effect on all the other images except the clicked one
		$("#eg1,#eg2,#eg3").removeClass('workEgClickEffect');
		$("#eg4").addClass('workEgClickEffect');

			// replay magnet gif
             var timestamp = new Date().getTime();
             var src = $("#magnetEx").attr("src")+'?'+timestamp;
             $("#magnetEx").attr("src",src);

			$("#speech").text(data.string.pg1s10);
			// display speechBubble
				$("#speech, #speechPoint").delay(3000).fadeIn(1000,function(){
						canIclick=true;
				$("#eg1, #eg2, #eg3, #eg4").css({"cursor":"pointer"});
				clickCount++;
				console.log(clickCount);
				if(clickCount==4){
					ole.footerNotificationHandler.pageEndSetNotification();
				}
				});
		}
});

});

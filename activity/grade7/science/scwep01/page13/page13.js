// dropping all items function
setTimeout(function(){
	ole.footerNotificationHandler.pageEndSetNotification();
},10000);
function dropAllItems(){
	$("#bolt").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#brick").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#clip").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#key").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#mirror").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#stick").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#paper").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#juice").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#nail").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
		$("#pin").show(90).delay(500).animate({"top":"110%", "opacity":"0"},{duration: 500,specialEasing: {"top":"easeInQuad","opacity":"easeInExpo"},complete: function(){
			$("#nonMagneticBasket, #magneticBasket").addClass('glowBox').show(0);
			$("#allMaterial > img").css({"top":"0%","display":"none","opacity":"1"});
				if(!allItemsDroppedOnce){
					ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg13n1);
					allItemsDroppedOnce = true;
				}
			dropAllItems();
	}});
	}});
	}});
	}});
	}});
	}});
	}});
	}});
	}});
	}});
}

/*first all item dropped flag*/
allItemsDroppedOnce = false;

$(document).ready(function() {

	loadTimelineProgress(1,1);
	// pull all data
	$("#textMagneticMaterial").text(data.string.pg13s1);
	$("#textNonMagneticMaterial").text(data.string.pg13s2);
	$("#magneticEnergy").text(data.string.pg13s3);
	$("#seeMagnetWork").text(data.string.pg13s4);
	$("#whatSeparatorDoes1").text(data.string.pg13s5);
	$("#whatSeparatorDoes2").text(data.string.pg13s6);



	// do this at first

	// display the magnet gif replay button after 6seconds
	$("#replayMagnet").delay(6000).fadeIn(0);

	// replay the magnet gif on clicking this replay button
    $("#replayMagnet").on('click',function () {
    			// hide replay magnet button
    		$("#replayMagnet").fadeOut(0).removeClass('blinkEffect');
    				// replay gif
                    var timestamp = new Date().getTime();

                    var src = $("#magnetEx").attr("src")+'?'+timestamp;
                    $("#magnetEx").attr("src",src);
             // show the magnetEx replay button after 6 seconds
        	$("#replayMagnet").delay(6000).fadeIn(0);
            })

    // after 6 seconds i.e when the gif animation completes show the border and then the conveyor belt animation
    $("#border").delay(7000).fadeIn(1000, function() {
    	$("#whatSeparatorDoes1").fadeIn(1000,function(){
    		$("#conveyor, #allMaterial").fadeIn(1000,function(){
    			dropAllItems();
    			$("#whatSeparatorDoes2").delay(4000).fadeIn(1000,function(){

    			});
    		});
    	});
    });

	// after the baskets are clickable do the  following
	// for magnetic collection Basket what to do on hover
	$("#magneticBasket").hover(function() {
		$("#magnetCollect, #textMagneticMaterial").show(0);
		$("#conveyor, #whatSeparatorDoes2").css({"opacity":"0.3"});
	}, function() {
		$("#magnetCollect, #textMagneticMaterial").hide(0);
		$("#conveyor, #whatSeparatorDoes2").css({"opacity":"1"});
	});

	// for non magnetic basket what to do on hover
	$("#nonMagneticBasket").hover(function() {
		$("#nonMagnetCollect, #textNonMagneticMaterial").show(0);
		$("#conveyor, #whatSeparatorDoes2").css({"opacity":"0.3"});
	}, function() {
		$("#nonMagnetCollect, #textNonMagneticMaterial").hide(0);
		$("#conveyor, #whatSeparatorDoes2").css({"opacity":"1"});
	});


});

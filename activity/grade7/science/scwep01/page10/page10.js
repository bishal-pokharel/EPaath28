$(document).ready(function() {	
	loadTimelineProgress(1,1);
	
	// pull all data
	$("#pointOne").text(data.string.pg10s1);
	$("#pointTwo").text(data.string.pg10s2);
	$("#pointThree").text(data.string.pg10s3);
	$("#lightEnergy").text(data.string.pg10s4);
	$("#reference").text(data.string.pg10s5);
	
	$("#sun").fadeIn(2000,function(){
		$("#sun").animate({"width":"10%","left":"20%","top":"5%"},3000, function() {
			$("#pointOne").fadeIn(500);
			$("#stalk").addClass('makeBig');

			//when this animation completes
			$("#stalk").on('animationend webkitAnimationEnd', function(){
 							$("#bud").addClass('makeBig');
 							$("#pointThree").fadeIn(500);
 						});

			$("#bud").on('animationend webkitAnimationEnd', function(){
 							$("#pointTwo").fadeIn(500);

 							$("#bud").fadeOut(3000);

 							$("#flower").fadeIn(3000,function(){
 								 // show reference text
 								 $("#reference").fadeIn(500);

 								$("#lightEnergy").fadeIn(1000,function(){
 									/*$(".pull-right > a").addClass('nextPageBlink');*/
 									ole.footerNotificationHandler.pageEndSetNotification();
 								});
 							});
 						});
		});
	});
		
});
				
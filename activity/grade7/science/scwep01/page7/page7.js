function foodAnim(){
	$("#sackCarry").fadeIn(1000,function(){
		// first show the text because it is hidden and then give the blurry effect
		$("#explain1").show(0).addClass('blurryText');
		// even for the span tag in explain1 div
		$("#explain1 > span").show(0).addClass('blurryText');

		$("#explain2").delay(3000).show(0,function(){
			$("#plate").show(0).addClass('plateRotate');
			$("#explain2").addClass('blurryText');
		});

		$("#explain3").delay(5000).show(0,function(){
			$("#explain3").addClass('blurryText');
			$("#chemEnergy").fadeIn(1000);
			/*$(".pull-right > a").addClass('nextPageBlink');*/
			ole.footerNotificationHandler.pageEndSetNotification();
		});				
			
	});
	
}


$(document).ready(function() {	

	loadTimelineProgress(1,1);
	// pull all data
	$("#explain1").text(data.string.pg7s1);
	$("#explain2").text(data.string.pg7s2);
	$("#explain3").text(data.string.pg7s3);
	$("#chemEnergy").text(data.string.pg7s4);

	ole.parseToolTip("#explain1","खाना");
	ole.parseToolTip("#explain2","रसायनिक शक्ति");
	$("#explain1, #explain2").children('span:nth-of-type(n+1)').css({'color':"brown","border":"none"});

	// animation for wepLab7

	foodAnim();
	
	
});
				
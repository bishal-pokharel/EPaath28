function solarAnim(){
	$("#sun").addClass('sunRotate').fadeIn(2000,function(){
		$("#sun").animate({"width":"8%","left":"10%","top":"8%"},1000,"linear",function(){});
		$("#house").delay(900).fadeIn(2000,function(){
				$("#ray").fadeIn(500,function(){
					$("#ray").addClass('blinkEffect');
					$("#house").fadeOut(1000);
					$("#houseAnim").fadeIn(1000,function(){
						$("#heatEnergy").show(1000,function(){
							/*$(".pull-right > a").addClass('nextPageBlink');*/
							ole.footerNotificationHandler.pageEndSetNotification();
						});						
					});					
				});
			});
		});
}


$(document).ready(function() {	

	loadTimelineProgress(1,1);

	// pull all data
	$("#solarText").text(data.string.pg6s1);
	$("#heatEnergy").text(data.string.pg6s2);

	solarAnim();
});
				
// donkey animation
function donkeyRuns(){
	$("#donkeyStandFirst").fadeOut(100);
	$("#donkeyMove").fadeIn(100);
	$("#donkeyMove").fadeIn(100,function(){
		$("#powerIntro").fadeIn(1000);
	}).animate({"left":"50%"},
		{duration:10000, specialEasing: {"left":"linear"},complete:function(){
		$("#donkeyMove").fadeOut(100);
		$("#donkeyStandLast").fadeIn(100,function(){
			$("#concludePower1").show(0).addClass('blurryText');
				$("#concludePower1").on('animationend webkitAnimationEnd', function(){
					$("#concludePower2").show(0).addClass('blurryText');
						$("#concludePower2").on('animationend webkitAnimationEnd', function(){
							$("#concludePower3").show(0).addClass('blurryText');
							ole.footerNotificationHandler.lessonEndSetNotification();
						});
				});
		});
	}});
}

// bus animation
function busRuns(){
	$("#wheelFront, #wheelBack").addClass('wheelRotate');
	$("#bus").animate({"left":"50%"},{duration:3000, specialEasing:{"left":"linear"},complete: function(){
		$("#wheelFront, #wheelBack").removeClass('wheelRotate');
	}});
}

$(document).ready(function() {

loadTimelineProgress(1,1);
	
	// pull all data
	$("#powerIntro").text(data.string.pg17s1);
	$("#concludePower1").text(data.string.pg17s2);
	$("#concludePower2").text(data.string.pg17s3);
	$("#concludePower3").text(data.string.pg17s4);
	
	// start the race on clicking the race button
	$("#race").on("click",function(){
		$("#race").removeClass('startRaceEffect').hide(0);
		donkeyRuns();
		busRuns();

	});
	
	// on clicking the restart button do the following
	$("#powerReplay").on('click', function() {
		location.reload();
		console.log(1);
	});

});
				
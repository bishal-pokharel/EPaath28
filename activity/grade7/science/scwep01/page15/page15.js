$(document).ready(function() {	
	
	loadTimelineProgress(1,1);
	
	// pull all data
	$("#earText").text(data.string.pg15s1);
	$("#sonarText1").text(data.string.pg15s2);
	$("#sonarText2").text(data.string.pg15s3);
	$("#soundEnergy").text(data.string.pg15s4);
	
	// for tabla sound
	var mySoundDrum = new buzz.sound($ref+"/page15/sound/tabla.ogg");

	// play sound at first
	buzz.all().stop();	
	mySoundDrum.play();

	// replay the gif on clicking this replay button
    $("#replayEar").on('click',function () {
    				// replay sound
    				buzz.all().stop();	
					mySoundDrum.play();

    				// replay gif
                    var timestamp = new Date().getTime();                    
                   
                    var src = $("#soundHear").attr("src")+'?'+timestamp;
                    $("#soundHear").attr("src",src);                  
            })

    // display replayEar
    $("#replayEar").delay(2000).fadeIn(100,function(){
    	$("#replayEar").addClass('blinkEffect');
    	$("#earText").show(0).addClass('blurryText');

    	$("#replayEar").on('animationend webkitAnimationEnd', function(){
 				$("#sonarEx").addClass('flipPage');
	 			$("#sonarEx").on('animationend webkitAnimationEnd', function(){
	 				$("#soundEnergy").show(0).addClass('blurryText');
	 				/*$(".pull-right > a").addClass('nextPageBlink');*/
	 				ole.footerNotificationHandler.pageEndSetNotification();
	 			});
 			});
    });

   

});
				
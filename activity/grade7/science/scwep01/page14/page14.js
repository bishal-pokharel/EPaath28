// check for the first click 
var firstClick = true;
var canIclick = true;

// var callWindowAnim=true;

/*// function for window animation
function windowAnim(){
	$("#window1").fadeIn(1000,function(){
		$("#window1").fadeOut(1000);
		$("#window2").fadeIn(1000,function(){
			$("#window2").fadeOut(1000);
			$("#window3").fadeIn(1000,function(){
				$("#window3").fadeOut(1000);
				$("#window4").fadeIn(1000,function(){
					$("#window4").fadeOut(500,function(){callWindowAnim=true;});
				});
			});
		});
	});

}*/

function nuclearAnim(){
	// nuclearFission animation
		$("#nuclearFission").fadeIn(500,function(){
			$("#neutron").animate({"left":"5%"},300,function(){
				$("#neutron > p, #mainAtom > p").fadeIn(500);
				$("#neutron").animate({"left":"20%"},3000,function(){
					$("#neutron > p, #mainAtom > p").fadeOut(200);
					$("#neutron").animate({"left":"35%"},300,function(){
							$("#mainAtom").fadeOut(50);
							$("#splitAtomTop, #splitAtomBottom, #splitNeutronTop, #splitNeutronBottom").fadeIn(50,function(){
								$("#splitAtomTop").animate({"top":"10%","left":"90%"},300);
								$("#splitAtomBottom").animate({"top":"80%","left":"80%"},300);
								$("#neutron").animate({"left":"80%"},300);
								$("#splitNeutronTop").animate({"top":"5%","left":"70%"},300);
								$("#splitNeutronBottom").animate({"top":"70%","left":"60%"},300);
								$("#energyShow").fadeIn(600);								
							});

		});
		});
		});
		});

		// fade out after some time interval but do not place it inside fadeIn function
		$("#nuclearFission").delay(5000).fadeOut(1000,function(){
		// call function to set the initial positions of atoms and neutrons
		$("#neutron").css({"top":"44%","left":"0%"});
		$("#splitNeutronTop, #splitNeutronBottom").css({"top":"44%","left":"33.5%"});
		$("#mainAtom").css({"top":"35%","left":"30%"});	
		$("#splitAtomTop").css({"top":"33%","left":"34%"});
		$("#splitAtomBottom").css({"top":"50%","left":"34%"});
		$("#mainAtom").show(0);
		$("#splitNeutronTop, #splitNeutronBottom, #splitAtomTop, #splitAtomBottom, #energyShow").hide(0);

		// show the notify div
		$("#notify").show(0,function(){
			$("#notify").css({"cursor":"pointer"});
			canIclick=true;
			ole.footerNotificationHandler.pageEndSetNotification();
			});			
		});
}

// here is the call for animation
function callForAnimation(){
		$("#notify").unbind('click');
		$("#notify").css({"cursor":"default"});
		// stop the glowing of the notify div and hide it
		$("#notify").hide(0);

		$("#notify").removeClass('glowCircle');
		

		// animation for nuclear fission
		nuclearAnim();	
}



$(document).ready(function() {	
	
	loadTimelineProgress(1,1);
	
// pull all data
$("#neutron > p").text(data.string.pg14s1);
$("#mainAtom > p").text(data.string.pg14s2);
$("#energyShow > p").text(data.string.pg14s3);
$("#info1").text(data.string.pg14s4);
$("#info2").text(data.string.pg14s5);
$("#atomicEnergy").text(data.string.pg14s6);
$("#fact1").text(data.string.pg14s7);

$("#reactorChamber > p").text(data.string.pg14s8);
$("#condensor > p").text(data.string.pg14s9);
$("#turbine > p").text(data.string.pg14s10);
$("#generator > p").text(data.string.pg14s11);
$("#transformer > p").text(data.string.pg14s12);

// these text comes on mouse hover
// $("#reactorChamber").attr("title", data.string.pg14s13);
// $("#condensor").attr("title", data.string.pg14s14);
// $("#turbine").attr("title", data.string.pg14s15);
// $("#generator").attr("title", data.string.pg14s16);
// $("#transformer").attr("title", data.string.pg14s17);

$("#caution > p").text(data.string.pg14s18);


/*// start window animation
setInterval(function(){
	if(callWindowAnim)
	{
		callWindowAnim=false;
		windowAnim();
	}
},4600);*/

// display info1
$("#info2").delay(500).fadeIn(1000);

// after 3 seconds display info2
$("#info1").delay(3000).fadeIn(1000);

// after 5 seconds display info2
$("#caution").delay(5000).show(1000);

//after 4 seconds of page load notify that there is something to click on 
$("#notify").delay(4000).show(0,function(){
	$("#notify").addClass('glowCircle');
});

// what to do on clicking the neuclear chamber--notify div here =)
$("#notify").on('click', function() {
	if(canIclick)
	{
		canIclick=false;
		$("#notify").css({"cursor":"default"});
		// stop the glowing of the notify div and hide it
		$("#notify").hide(0);

		// if it is firstclick remove glowcircle class else do no execute this
		if(firstClick)
		{
			firstClick=false;
			$("#notify").removeClass('glowCircle');
			console.log(1);
		}

		// animation for nuclear fission
		nuclearAnim();
	}
		});
	
	// function for moreInfo tooltip 
	$("#moreInfo > div").hover(function() {
		
		$(this).after('<p></p>');
		var selected = "#"+$(this).attr("id");

		// switch case to select specific data for parts in power plant
			switch(selected)
				{
				case "#reactorChamber":
				  var moreData = data.string.pg14s13;
				  break;
				case "#condensor":
				  var moreData = data.string.pg14s14;
				  break;
				case "#turbine":
				  var moreData = data.string.pg14s15;
				  break;
				case "#generator":
				  var moreData = data.string.pg14s16;
				  break;
				case "#transformer":
				  var moreData = data.string.pg14s17;
				  break;				  
				}

		$(this).next().text(moreData).css({
			"position": 'absolute',
			"top":"50%",
			"width": '100%',
			"color":"darkblue",
			"text-align":"center",
			"background-color":"rgba(255,255,255,0.9)",
			"border":"0.1em solid rgba(0,0,0,0.5)",
			"border-radius":"5%"
		});
	}, function() {
		$(this).next().remove();
	});
});

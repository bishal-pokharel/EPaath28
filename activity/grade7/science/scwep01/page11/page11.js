$(document).ready(function() {	

	loadTimelineProgress(1,1);
	// pull all data
	$("#roomLightEnergy").text(data.string.pg11s3);
	$("#lab11Instruction1").text(data.string.pg11s1);
	
	// blink the switch at the start
	$("#switch").addClass('blinkEffect');

	//do the following on mouse over switch
	$("#switch").on('mouseover', function() {
		$("#switch").removeClass('blinkEffect');
		$("#switch").css({"opacity":"0"});
		$("#switchOff").css({"opacity":"1"});
	});
		
	//do the following on mouse out from switch
	$("#switch").on('mouseout', function() {
		$("#switch").addClass('blinkEffect');
		$("#switchOff").css({"opacity":"0"});
		$("#switch").css({"opacity":"1"});
	});

	// do the following on clicking the switch
	$("#switch").on('click', function() {
		$("#switch").removeClass('blinkEffect'); 
		 // do this to remove switch and switchOff from the dom so that they are unclickable
		 $("#lab11Instruction1").hide(0).css({"top":"68%", "left":"21%","width":"50%", "color":"darkgreen", "text-align":"center"});
		 $("#lab11Instruction1").show(0).addClass('blurryText').text(data.string.pg11s2); //show conclusion now
		$("#switch, #switchOff").css({"display":"none"});
		$("#switchOn").css({"opacity":"1"});
		$("#room").attr('src', $ref+'/page11/image/brightRoom.png');
		$("#room").animate({"left":"20%"},1000);
		$("#switchBox").animate({"left":"40%"},1000);
		$("#switchBox").delay(1000).animate({"opacity":"0"},0,function(){
			ole.footerNotificationHandler.pageEndSetNotification();
		});
	});

	// // do the following on clicking the switch
	// $("#switchOn").on('click', function() {
	// 	 // do this to remove switch and switchOff from the dom so that they are unclickable
	// 	$("#switch").show(0).css({"opacity":"1"});
	// 	$("#switchOff").show(0).css({"opacity":"0"});
	// 	$("#switchOn").css({"opacity":"0"});
	// 	$("#room").attr('src', $ref+'/page11/image/darkRoom.png');
	// 	// $("#switchBox").css({"display":"none"});
	// });
});
				
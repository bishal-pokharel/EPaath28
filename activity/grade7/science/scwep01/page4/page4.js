$(document).ready(function() {


	loadTimelineProgress(2,1);

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var $whatnprevbtn=getSubpageMoveButton($lang,"prev");

	$("#activity-page-prev-btn-enabled").html($whatnprevbtn);
	// pull all data
	$("#energyWork").text(data.string.pg4s1);
	$("#reasonEnergy").text(data.string.pg4s2);
	$("#slideBQ1").text(data.string.pg4s3);
	$("#slideBQ2").text(data.string.pg4s4);
	$("#slideBreasonStrong > p").text(data.string.pg4s5);
	$("#slideBreasonWeak > p").text(data.string.pg4s6);
	$("#pointDescribeStrong").text(data.string.pg4s7);
	$("#pointDescribeWeak").text(data.string.pg4s8);
	$("#whatIsEnergy").text(data.string.pg4s9);

	// $("#energyWork").effect("slide");

	$("#slideAbody").delay(1000).animate({"opacity":"0"},500);
	$("#slideAbodyMotion").delay(1000).animate({"opacity":"1"},500,function(){
		$("#reasonEnergy").addClass("startRaceEffect");
		$("#activity-page-next-btn-enabled").delay(1000).fadeIn(500);
	});

	//when going to slide B hide slide A and show slide B
	$("#activity-page-next-btn-enabled").on('click',function() {

		loadTimelineProgress(2,2);

	//	$("#activity-page-next-btn-enabled").removeClass("blinkEffect");
	// $("#slideA").effect('puff');
		$("#slideA").hide(0);
		$("#slideB").delay(500).fadeIn(500, function() {
			// $("#startRace").addClass('startRaceEffect');
			$("#activity-page-prev-btn-enabled").show(0);
		});
	});

	// on clicking startRace button do following
	$("#startRace").on('click', function() {
		$("#startRace").removeClass("startRaceEffect").fadeOut(0);

		// animate the strong one
		$("#slideBstrong").animate({"opacity":"0"},500);
		$("#strongMove").animate({"opacity":"1"},500,function(){
			$("#strongMove").animate({"left":"65%"},8000,"linear",function(){
				$("#strongMove").animate({"opacity":"0"},500);
				$("#slideBstrongLast").animate({"opacity":"1"},500);
			});
		});

		// animate the weak one
		$("#slideBweak").animate({"opacity":"0"},500);
		$("#weakMove").animate({"opacity":"1"},500,function(){
			$("#weakMove").animate({"left":"46%"},8000,"linear",function(){
				$("#weakMove").animate({"opacity":"0"},500);
				$("#slideBweakLast").animate({"opacity":"1"},500,function(){
					$("#slideBQ1").fadeOut(500);
					$("#slideBQ2").fadeIn(500,function(){
						$("#land1Mark2, #land2Mark2").fadeIn(1000,function(){
							$("#pointDescribeStrong,#pointDescribeWeak").fadeIn(500,function(){
								$("#howThisHappen").addClass('blinkEffect').fadeIn(500);
							});

						});
					});
				});
			});
		});
	});

 	// what happens on clicking how this happens
 	$("#howThisHappen").on('click',function() {
 		$("#howThisHappen").removeClass('blinkEffect').fadeOut(0);
 		$("#slideBreasonStrong > img, #slideBreasonWeak > img").addClass('rotateEffect');
 			$("#slideBreasonStrong, #slideBreasonWeak").fadeIn(1000,function(){
 				$("#whatIsEnergy").delay(3000).fadeIn(1000,function(){
 					/*$(".pull-right > a").addClass('nextPageBlink');*/
 					ole.footerNotificationHandler.pageEndSetNotification();
 				});
 			});
 	});

 	// what to do on clicking lab4Replay
 	$("#activity-page-prev-btn-enabled").on('click', function() {
 		location.reload();
 	});
 });

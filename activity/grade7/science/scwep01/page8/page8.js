$(document).ready(function() {	

	loadTimelineProgress(1,1);
	
	// pull all data
	$("#instructLab8").text(data.string.pg8s1);
	$("#conclusionLab8").text(data.string.pg8s2);
	$("#conclusionLab8second").text(data.string.pg8s3);
	$("#chemEnergy").text(data.string.pg8s4);

	// highlight the required text
	ole.parseToolTip("#conclusionLab8","ब्याट्री");
	ole.parseToolTip("#conclusionLab8","रसायनिक शक्ति");
	$("#conclusionLab8").children('span:nth-of-type(n+1)').css({'color':"brown","border":"none"});
	
	// make the battery blinking at
	$("#batteryOut").addClass('blinkEffect');

	// battery out side the torch div is draggable
	$("#batteryOut").draggable({"containment":"parent", "revert":"invalid","opacity":"0.7",
								start: function() {
									$("#batteryOut").removeClass('blinkEffect');
								},
								stop: function() {
									$("#batteryOut").addClass('blinkEffect');
								}
							});
		
	//make the batteryDrop div droppable for the batteryOut image
	$("#batteryDrop").droppable({"accept":"#batteryOut",
								over: function() {
									$("#batteryDrop").addClass('batteryDropGlow');
								},
								out: function() {
									$("#batteryDrop").removeClass('batteryDropGlow');
								},
								drop: function() {
									// on dropping the battery do the following
									$("#batteryOut").fadeOut(0);
									$("#batteryOut").draggable("disable");
									$("#batteryDrop").removeClass('batteryDropGlow');
									$("#batteryIn").fadeIn(500,function(){
										$("#instructLab8").fadeOut(100);
										$("#torchOff").fadeOut(500);
										$("#torchOn").fadeIn(500,function(){
											$("#rays").fadeIn(500,function(){
												$("#conclusionLab8,#conclusionLab8second,#pointBattery").fadeIn(1000,function(){
													$("#chemEnergy").fadeIn(1000,function(){
														/*$(".pull-right > a").addClass('nextPageBlink');*/
														ole.footerNotificationHandler.pageEndSetNotification();
													});
												})
											});
										});
									});
								},
								});
});
				
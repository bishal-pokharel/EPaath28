var $imgPath = $ref+"/images/exercise1/";
/*content object*/
var content=[
	{
		instructionData : data.string.e1s0,
		mixtureCollection:[
			{
				mixtureNameData : "oilwater",
				mixtureImgSrc : $imgPath+"oilwater.png",
				mixItemCaption : data.string.e1s1
			},
			{
				mixtureNameData : "iron",
				mixtureImgSrc : $imgPath+"iron.png",
				mixItemCaption : data.string.e1s2
			},
			{
				mixtureNameData : "lemontea",
				mixtureImgSrc : $imgPath+"lemontea.png",
				mixItemCaption : data.string.e1s3
			},
			{
				mixtureNameData : "air",
				mixtureImgSrc : $imgPath+"air.png",
				mixItemCaption : data.string.e1s4
			},
			{
				mixtureNameData : "gold",
				mixtureImgSrc : $imgPath+"gold.png",
				mixItemCaption : data.string.e1s5
			},
			{
				mixtureNameData : "rocks",
				mixtureImgSrc : $imgPath+"rocks.png",
				mixItemCaption : data.string.e1s6
			},
			{
				mixtureNameData : "sandwater",
				mixtureImgSrc : $imgPath+"sandwater.png",
				mixItemCaption : data.string.e1s7
			},
			{
				mixtureNameData : "silver",
				mixtureImgSrc : $imgPath+"silver.png",
				mixItemCaption : data.string.e1s8
			},
			{
				mixtureNameData : "sugarmilk",
				mixtureImgSrc : $imgPath+"sugarmilk.png",
				mixItemCaption : data.string.e1s9
			},
			{
				mixtureNameData : "saltwater",
				mixtureImgSrc : $imgPath+"saltwater.png",
				mixItemCaption : data.string.e1s10
			},
			{
				mixtureNameData : "coffee",
				mixtureImgSrc : $imgPath+"coffee.png",
				mixItemCaption : data.string.e1s11
			},
			{
				mixtureNameData : "aluminiumfoil",
				mixtureImgSrc : $imgPath+"aluminiumfoil.png",
				mixItemCaption : data.string.e1s12
			},
			{
				mixtureNameData : "brass",
				mixtureImgSrc : $imgPath+"brass.png",
				mixItemCaption : data.string.e1s13
			},
			{
				mixtureNameData : "bronze",
				mixtureImgSrc : $imgPath+"bronze.png",
				mixItemCaption : data.string.e1s14
			}

		]	
	}
];

/*css styles for conclusion paragraph*/
var correctConclusion = {
	"background-color" : "rgba(208,244,119,0.5)"
}

var incorrectConclusion = {
	"background-color" : "rgba(255,25,0,0.1)"
}

$(function () {	
	var $board = $('.board');	
	var countNext = 0;

	var $total_page = 1;
	loadTimelineProgress($total_page,countNext+1);

/*
* selectMixture
*/
	function selectMixture() {
		var source = $("#selectMixture-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	

		var selectedMixtureCount = 0;
		var selectedMixtureType;
		var selectionConclusionData;
		var mixtureName;
		var $instruction = $board.children('.instruction');
		var $selectionConclusion = $board.children('.selectionConclusion');
		var $selectionAreaFigure = $board.children('.selectionArea').children('figure');

		/*on clicking mixture types*/
		$selectionAreaFigure.on('click',function() {
			selectedMixtureType = $(this).data("mixturename");

			if(selectedMixtureCount == 0){
				$instruction.css('visibility', 'hidden');
			}

			switch(selectedMixtureType){
				case "oilwater":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;
						selectionConclusionData = data.string.e1s_right + data.string.e1_oilwater_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				case "iron":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectionConclusionData = data.string.e1s_wrong + data.string.e1_iron_reason;
						$selectionConclusion.css(incorrectConclusion);
						$(this).children('div').addClass('incorrectStyle');
					break;
				case "lemontea":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;
						selectionConclusionData = data.string.e1s_right + data.string.e1_lemontea_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				case "air":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;
						selectionConclusionData = data.string.e1s_right + data.string.e1_air_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				case "gold":						
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectionConclusionData = data.string.e1s_wrong + data.string.e1_gold_reason;
						$selectionConclusion.css(incorrectConclusion);
						$(this).children('div').addClass('incorrectStyle');
					break;
				case "rocks":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectionConclusionData = data.string.e1s_wrong + data.string.e1_rocks_reason;
						$selectionConclusion.css(incorrectConclusion);
						$(this).children('div').addClass('incorrectStyle');
					break;
				case "sandwater":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;
						selectionConclusionData = data.string.e1s_right + data.string.e1_sandwater_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				case "silver":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectionConclusionData = data.string.e1s_wrong + data.string.e1_silver_reason;
						$selectionConclusion.css(incorrectConclusion);
						$(this).children('div').addClass('incorrectStyle');
					break;
				case "sugarmilk":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;
						selectionConclusionData = data.string.e1s_right + data.string.e1_milksugar_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				case "saltwater":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;
						selectionConclusionData = data.string.e1s_right + data.string.e1_saltwater_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				case "coffee":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;						
						selectionConclusionData = data.string.e1s_right + data.string.e1_coffee_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				case "aluminiumfoil":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectionConclusionData = data.string.e1s_wrong + data.string.e1_aluminium_reason;
						$selectionConclusion.css(incorrectConclusion);
						$(this).children('div').addClass('incorrectStyle');
					break;
				case "brass":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;						
						selectionConclusionData = data.string.e1s_right + data.string.e1_brass_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				case "bronze":
						$(this).off("click");
						$(this).children("div").children("img").css("cursor","default");
						selectedMixtureCount++;
						selectionConclusionData = data.string.e1s_right + data.string.e1_bronze_reason;
						$selectionConclusion.css(correctConclusion);
						$(this).children('div').addClass('correctStyle');
					break;
				default:break;
			}

			$selectionConclusion.html(selectionConclusionData);

			if(selectedMixtureCount > 8){
				$selectionAreaFigure.off("click");
				$selectionConclusion.html(data.string.e1_congratulation);
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		});
	}	
	
	/*first call to solutionExample*/
	selectMixture();

});
/*function onImgErrorSmall(source)
	{
	source.remove();
	// disable onerror to prevent endless loop
	source.onerror = "";
	return true;
	}*/

/*image path to exercise 3 folder*/
var $refImg = $ref+"/images/exercise4/";
var disabledSubmitButtonImageSrc = $refImg+"disabledsubmit.png";
var enabledSubmitButtonImageSrc = $refImg+"enabledsubmit.png";
 
/*content object*/
var content=[
	{	
		questionData : data.string.e4q1,
		conclusionFirstText : data.string.e4commoninstruction,
		dragobject:[
					{
					   acceptdrag : "process5",
					   dragImgSrc : $refImg+"q1p5.png",
					   dragCaption : data.string.e4q1p5
					},
					{
					   acceptdrag : "process2",
					   dragImgSrc : $refImg+"q1p2.png",
					   dragCaption : data.string.e4q1p2
					},
					{
					   acceptdrag : "process4",
					   dragImgSrc : $refImg+"q1p4.png",
					   dragCaption : data.string.e4q1p4
					},					
					{
					   acceptdrag : "process3",
					   dragImgSrc : $refImg+"q1p3.png",
					   dragCaption : data.string.e4q1p3
					},
					
					{
					   acceptdrag : "process1",
					   dragImgSrc : $refImg+"q1p1.png",
					   dragCaption : data.string.e4q1p1
					}					
				   ],
		dropobject:[
					{
					   acceptdrop : "process1",
					   displayProcessArrow : true,
					   arrowimgsrc : $refImg+"processarrow.png"
					},
					{
					   acceptdrop : "process2",
					   displayProcessArrow : true,
					   arrowimgsrc : $refImg+"processarrow.png"
					},
					{
					   acceptdrop : "process3",
					   displayProcessArrow : false
					}
				   ],
		rawmaterials : [
					{
						material : data.string.e4q1materialsand,
						displayplus : true
					},
					{
						material : data.string.e4q1materialsalt,
						displayplus : false
					}
					],
		rawMaterialfontSizeCheckClass : "rawMaterialNormalFont",
		productImgSrc : $refImg+"q1product.png",
		submitData: data.string.e4s1,
		finalProductImgSrc : $refImg+"q1finalproduct.png",
		finalProductText : data.string.e4q1finalproducttext
	},
	{	
		questionData : data.string.e4q2,
		conclusionFirstText : data.string.e4commoninstruction,
		dragobject:[
					{
					   acceptdrag : "process5",
					   dragImgSrc : $refImg+"q1p5.png",
					   dragCaption : data.string.e4q1p5
					},
					{
					   acceptdrag : "process2",
					   dragImgSrc : $refImg+"q2p2.png",
					   dragCaption : data.string.e4q2p2
					},
					{
					   acceptdrag : "process4",
					   dragImgSrc : $refImg+"q1p2.png",
					   dragCaption : data.string.e4q1p2
					},					
					{
					   acceptdrag : "process3",
					   dragImgSrc : $refImg+"q2p3.png",
					   dragCaption : data.string.e4q2p3
					},
					
					{
					   acceptdrag : "process1",
					   dragImgSrc : $refImg+"q2p1.png",
					   dragCaption : data.string.e4q2p1
					}					
				   ],
		dropobject:[
					{
					   acceptdrop : "process1",
					   displayProcessArrow : true,
					   arrowimgsrc : $refImg+"processarrow.png"
					},
					{
					   acceptdrop : "process2",
					   displayProcessArrow : true,
					   arrowimgsrc : $refImg+"processarrow.png"
					},
					{
					   acceptdrop : "process3",
					   displayProcessArrow : false
					}
				   ],
		rawmaterials : [
					{
						material : data.string.e4q2materialcolor,
						displayplus : false
					}
					],
		rawMaterialfontSizeCheckClass : "rawMaterialNormalFont",
		productImgSrc : $refImg+"q2p1.png",
		submitData: data.string.e4s1,
		finalProductImgSrc : $refImg+"q2p3.png",
		finalProductText : data.string.e4q2finalproducttext
	},
	{	
		questionData : data.string.e4q3,
		conclusionFirstText : data.string.e4commoninstruction,
		dragobject:[
					{
					   acceptdrag : "process1",
					   dragImgSrc : $refImg+"q3p1.png",
					   dragCaption : data.string.e4q3p3
					},
					{
					   acceptdrag : "process2",
					   dragImgSrc : $refImg+"q3p2.png",
					   dragCaption : data.string.e4q3p2
					},
					{
					   acceptdrag : "process4",
					   dragImgSrc : $refImg+"q1p3.png",
					   dragCaption : data.string.e4q1p3
					},					
					{
					   acceptdrag : "process5",
					   dragImgSrc : $refImg+"q1p2.png",
					   dragCaption : data.string.e4q1p2
					},					
					{
					   acceptdrag : "process3",
					   dragImgSrc : $refImg+"q3p3.png",
					   dragCaption : data.string.e4q3p1
					}					
				   ],
		dropobject:[
					{
					   acceptdrop : "process1",
					   displayProcessArrow : true,
					   arrowimgsrc : $refImg+"processarrow.png"
					},
					{
					   acceptdrop : "process2",
					   displayProcessArrow : true,
					   arrowimgsrc : $refImg+"processarrow.png"
					},
					{
					   acceptdrop : "process3",
					   displayProcessArrow : false
					}
				   ],
		rawmaterials : [
					{
						material : data.string.e4q3materialmuddywater,
						displayplus : false
					}
					],
		rawMaterialfontSizeCheckClass : "rawMaterialNormalFont",
		productImgSrc : $refImg+"q3product.png",
		submitData: data.string.e4s1,
		finalProductImgSrc : $refImg+"q3finalproduct.png",
		finalProductText : data.string.e4q3finalproducttext
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	/*total number of questions*/
	var totalQuestions = content.length;	
/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $labarea, $conclusion, $draggables, $droppables;
		var correctdropcount=0, processStepCount, totalempty;

		var $labarea = $board.children('div.labarea');
		var $submitButton = $labarea.children('button.submit');
		var $conclusion = $labarea.children('p.conclusion');
		var $draggables = $labarea.children('div.dragarea').children('figure');
		var $processingArea = $labarea.children('div.processingArea');
		var $finalProduct = $processingArea.children('figure.finalProduct');
		var $finalProductImg = $finalProduct.children('img');
		var $finalProductText = $finalProduct.children('figcaption');
		var $dropArea = $processingArea.children('div.droparea');
		var $droppables = $dropArea.children('figure');

		/*length of process step is the number of droppables*/
		processStepCount = $droppables.length;
		/*at first all the droppables are empty obvious hoi*/
		totalempty=processStepCount;
		
		$draggables.draggable({
			helper: "clone",
			revert: "invalid",
			containment:".board"
		});

		$droppables.droppable({
			tolerance : "pointer",
    		drop: function(event, ui){
    					totalempty--;
    					$(this).children('img').attr('src', $(ui.draggable).children('img').attr("src"));
    			   		$(this).children('figcaption').text($(ui.draggable).children('figcaption').text());
    					$(this).droppable("disable");
    					$(ui.draggable).css('opacity', '0');
    					ui.helper.remove();
    					if($(this).data('acceptdrop') != $(ui.draggable).data('acceptdrag')){    						
    			   			/*flag these draggables and droppables with classes to be called together later on*/
    			   			$(ui.draggable).addClass('showDraggables');
    			   			$(this).addClass("showDroppables");
    					}
    					else if($(this).data('acceptdrop') == $(ui.draggable).data('acceptdrag')){
    						$(this).droppable("destroy");
    						correctdropcount++;
    					}

		    			/*show the submit button disabled or enabled state*/
			    		if(totalempty > 0){
			    			$submitButton.toggleClass('enableSubmit',false);
			    		}
			    		else if(totalempty <=0 ){
			    			$submitButton.toggleClass('enableSubmit',true);
			    			/*remove any message if previously shown*/
			    			$conclusion.text("");
						}
    				}
		});

		$submitButton.on('click', function() {
			/* Act on the event */
			if(totalempty > 0){
				$conclusion.text(data.string.e4empty);
			}
			else if(totalempty <= 0){
				/*give message according to total process steps and correctdropcount*/
				switch(processStepCount){
					case 2: switch(correctdropcount){
							case 0 : $conclusion.text(data.string.e4correct0);break;
							case 1 : $conclusion.text(data.string.e4correct1);break;
							case 2 : switch(countNext){
										case 0: $conclusion.text(data.string.e4q4correct);break;
										case 1: $conclusion.text(data.string.e4q2correct);break;
										case 2: $conclusion.text(data.string.e4q3correct);break;
										case 3: $conclusion.text(data.string.e4q4correct);break;
										case 4: $conclusion.text(data.string.e4q5correct);break;
										default:break;
									 };
									 $nextBtn.show(0);
									 $submitButton.css('display', 'none');
									 /*change final product image and text*/
									 $finalProductImg.attr('src', content[countNext].finalProductImgSrc);
									 $finalProductText.text(content[countNext].finalProductText);
									 /*now we can destroy the draggables and droppables when answer is correct*/
									 $draggables.draggable("destroy");
									 $draggables.css('cursor', 'default');
									 $droppables.droppable("destroy");									 
									 break;
							default:break;
						};
						break;
					case 3: switch(correctdropcount){
							case 0 : $conclusion.text(data.string.e4correct0);break;
							case 1 : $conclusion.text(data.string.e4correct1);break;
							case 2 : $conclusion.text(data.string.e4correct2);break;
							case 3 : switch(countNext){
										case 0: $conclusion.text(data.string.e4q1correct);break;
										case 1: $conclusion.text(data.string.e4q2correct);break;
										case 2: $conclusion.text(data.string.e4q3correct);break;
										case 3: $conclusion.text(data.string.e4q4correct);break;
										case 4: $conclusion.text(data.string.e4q5correct);break;
										default:break;
									 };
									 $nextBtn.show(0);
									 $submitButton.css('display', 'none');
									 /*change final product image and text*/
									 $finalProduct.addClass('finalProductGlow');
									 $finalProductImg.attr('src', content[countNext].finalProductImgSrc);
									 $finalProductText.text(content[countNext].finalProductText);
									 /*now we can destroy the draggables and droppables when answer is correct*/
									 $draggables.draggable("destroy");
									 $draggables.css('cursor', 'default');
									 $droppables.droppable("destroy");									 
									 break;
							default:break;
						};
						break;
					default:break;
				}
				

				/*reload the droppables and draggables*/
				$labarea.children('div.dragarea').children('figure.showDraggables').css('opacity', '1');
				$dropArea.children('figure.showDroppables').children('img').attr('src', 'images/404.png');
				$dropArea.children('figure.showDroppables').children('figcaption').text("");
				$dropArea.children('figure.showDroppables').droppable("enable");
				$draggables.removeClass('showDraggables');
	    		$droppables.removeClass("showDroppables");
	    		totalempty = processStepCount - correctdropcount;

	    		/*show the submit button disabled or enabled state after the total empty has been counted above*/
	    		if(totalempty > 0){
	    			$submitButton.toggleClass('enableSubmit',false);
	    		}
	    		else if(totalempty <=0 ){
	    			$submitButton.toggleClass('enableSubmit',true);
				}			
			}
		});
	}	

	/*first call to first*/
	first();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;
		switch(countNext){
			case 1: first();
					break;
			case 2: first();
					break;
			case 3: ole.activityComplete.finishingcall();
					break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});
});
var arrowImage = $ref+'/images/page3/arrowgreen.png';
var arrowImageHtml = "<img class='arrowimageInstr' src='"+arrowImage+"'>";

var plusImage = $ref+'/images/page3/plus.png';
var plusImageHtml = "<img class='plusimageInstr' src='"+plusImage+"'>";

var slide1instruction = ole.textSR(data.string.p3s1,"#imageplaceholder#",arrowImageHtml);
var slide2instruction = ole.textSR(data.string.p3s2,"#imageplaceholder#",arrowImageHtml);
var slide3instruction = ole.textSR(data.string.p3s3,"#imageplaceholder#",plusImageHtml);
var slide6instructionmakai = ole.textSR(data.string.p3s18,"#imageplaceholder#",plusImageHtml);
var slide6instructiondalmoth = ole.textSR(data.string.p3s15,"#imageplaceholder#",plusImageHtml);

//array of image
	var definitionImg = [
	"images/diy/diy1.png",
	"images/diy/diy2.png",
	"images/diy/diy3.png",
	"images/diy/diy4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);
/*content object*/
var content=[
	{	
		diyImageSource : definitionImg[randomImageNumeral],
		diyTextData : data.string.p3s0,
		diyInstructionData : data.string.p3s01
	},
	{
		smalldiy: {	
				diyImageSource : definitionImg[randomImageNumeral],
				diyTextData : data.string.p3s0,
				diyInstructionData : slide2instruction
		},
		mixItemXSource : $ref+"/images/page3/sugar.png",
		mixerSource : $ref+"/images/page3/arrowgreen.png",
		mixItemYSource : $ref+"/images/page3/milk-glass.png",
		mixtureSource : $ref+"/images/page3/milk-glass.png",
		mixItemXClass : "mixItemX1",
		mixItemXLabelClass : "mixItemX1Label", 
		mixItemXLabelData : data.string.p3s7,
		mixerClass : "mixer1 bounceItem",
		mixItemYClass : "mixItemY1",
		mixItemYLabelClass : "mixItemY1Label",
		mixItemYLabelData : data.string.p3s8,
		mixtureClass : "mixture1",
		mixtureLabelClass : "mixture1Label",
		mixtureLabelData : data.string.p3s9
	},
	{	
		smalldiy: {	
				diyImageSource : definitionImg[randomImageNumeral],
				diyTextData : data.string.p3s0,
				diyInstructionData : slide1instruction
		},
		mixItemXSource : $ref+"/images/page3/tea.png",
		mixerSource : $ref+"/images/page3/arrowgreen.png",
		mixerInstructionClass : "arrowButton",
		mixerButtonInstruction : data.string.p3s23,
		mixItemYSource : $ref+"/images/page3/water-glass.png",
		mixtureSource : $ref+"/images/page3/tea-glass.png",
		mixItemXClass : "mixItemX1",
		mixItemXLabelClass : "mixItemX1Label", 
		mixItemXLabelData : data.string.p3s4,
		mixerClass : "mixer1 bounceItem",
		mixItemYClass : "mixItemY1",
		mixItemYLabelClass : "mixItemY1Label",
		mixItemYLabelData : data.string.p3s5,
		mixtureClass : "mixture1",
		mixtureLabelClass : "mixture1Label",
		mixtureLabelData : data.string.p3s6
	},
	{	
		smalldiy: {	
				diyImageSource : definitionImg[randomImageNumeral],
				diyTextData : data.string.p3s0,
				diyInstructionData : slide3instruction
		},
		mixItemXSource : $ref+"/images/page3/tea-glass.png",
		mixerSource : $ref+"/images/page3/plus.png",
		mixItemYSource : $ref+"/images/page3/milk-glass.png",
		mixtureSource : $ref+"/images/page3/teawithstar.png",
		mixItemXClass : "mixItemX3",
		mixItemXLabelClass : "mixItemX3Label", 
		mixItemXLabelData : data.string.p3s6,
		mixerClass : "mixer3 bounceItemInOut",
		mixItemYClass : "mixItemY3",
		mixItemYLabelClass : "mixItemY3Label",
		mixItemYLabelData : data.string.p3s9,
		mixtureClass : "mixture3",
		mixtureLabelClass : "mixture3Label",
		mixtureLabelData : data.string.p3s10,
		vapourImageSource : $ref+"/images/page3/vapour.png"
	},
	{
		smalldiy: {	
				diyImageSource : definitionImg[randomImageNumeral],
				diyTextData : data.string.p3s0,
				diyInstructionData :data.string.p3s12
		},
		
		snackItemXLabelClass : "snackItemX1Label",
		snackItemXLabelData : data.string.p3s13,
		snackItemYLabelClass : "snackItemY1Label",
		snackItemYLabelData : data.string.p3s14		
	},
	{
		smalldiy: {	
				diyImageSource : definitionImg[randomImageNumeral],
				diyTextData : data.string.p3s0,
				diyInstructionData :""
		},
		mixItemXSource : "",
		mixerSource : $ref+"/images/page3/plus.png",
		mixItemYSource : "",
		mixtureSource : "",
		mixItemXClass : "mixItemX5",
		mixItemXLabelClass : "mixItemX5Label", 
		mixItemXLabelData : "",
		mixerClass : "mixer3 bounceItemInOut",
		mixItemYClass : "mixItemY5",
		mixItemYLabelClass : "mixItemY5Label",
		mixItemYLabelData : "",
		mixtureClass : "mixture5",
		mixtureLabelClass : "mixture5Label",
		mixtureLabelData : "",
		instructionDataChange : "",
		whatSnacks: ""
	},
	{
		smalldiy: {	
				diyImageSource : definitionImg[randomImageNumeral],
				diyTextData : data.string.p3s0,
				diyInstructionData :data.string.p3s24
		},
		
		teaSource : $ref+"/images/page3/teawithstar.png",
		vapourImageSource : $ref+"/images/page3/vapour.png",
		snacksSource : "",
		snackItemYLabelData : data.string.p3s14		
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 7;
	loadTimelineProgress($total_page,countNext+1);

/*
* diyLandingPage
*/
	function diyLandingPage () {
		var source = $("#diyLandingPage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$nextBtn.show(0);	
	}

/*
* mixThat
*/
	function mixThat(slideNum) {
		var source = $("#mixThat-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[slideNum]);
		$board.html(html);

		$board.on('click', '.mixer1', function() {
			$(this).css('display', 'none');
			$(this).siblings('.mixture1').css("opacity","1");
			$(this).siblings('.mixItemX1').attr("src",$ref+"/images/page3/spoon-blank.png");
			$board.children('.mixingArea').children('p:nth-of-type(-n+2)').css('display', 'none');
			$board.children('.mixingArea').children('p:nth-of-type(3)').show(0);
			$board.children('.diySmall').children('p').css('visibility', 'hidden');
			$nextBtn.show(0);		
		});

		$board.on('click', '.mixer3', function() {
			$(this).css('display', 'none');
			$(this).siblings('.mixture3 , .mixture5').css({"opacity":"1","z-index":"1"});
			$(this).siblings('.mixItemX3, .mixItemY3, .mixItemX5, .mixItemY5').css('display','none');
			$board.children('.mixingArea').children('p:nth-of-type(-n+2)').css('display', 'none');
			$board.children('.mixingArea').children('p:nth-of-type(3)').show(0);			

			/*the text when mixture is prepared on slide 2 and 4*/			
			if(content[slideNum].whatSnacks === "dalmothchiura"){
				$board.children('.diySmall').children('p').html(data.string.p3s21);
			}
			else if(content[slideNum].whatSnacks === "makaibhatmas"){
				$board.children('.diySmall').children('p').html(data.string.p3s22);
			}
			else{
				$board.children('.diySmall').children('p').text(data.string.p3s11);
			}

			/*vapour effect in slide 2*/
			if(slideNum == 2){
				$board.children('.mixingArea').children('.vapourImage').show(0).addClass('vapourRise');
			}
			else if(slideNum == 5){
				$board.children('.mixingArea').children('.vapourImage').css('display', 'none');
			}

			$nextBtn.show(0);	
		});
	}	

	/*function for snack choosing slide*/
	function chooseSnack(slideNum) {
		var source = $("#chooseSnack-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[slideNum]);
		$board.html(html);

		$board.on('click', '.choosingArea > p', function() {
			if($(this).attr("class") == "snackItemX1Label"){
				$(this).addClass('snackLabelSelected');
				$(this).siblings("p").css('display', 'none');
				content[5].smalldiy.diyInstructionData = slide6instructiondalmoth;
				content[5].mixItemXSource = $ref+"/images/page3/dalmoth.png";
				content[5].mixItemYSource = $ref+"/images/page3/chiura.png";
				content[5].mixtureSource = $ref+"/images/page3/dalmothChiura.png";
				content[6].snacksSource = $ref+"/images/page3/dalmothChiura.png";
				content[5].mixItemXLabelData = data.string.p3s16;
				content[5].mixItemYLabelData = data.string.p3s17;
				content[5].mixtureLabelData = data.string.p3s13;
				content[5].instructionDataChange = data.string.p3s21;
				content[5].whatSnacks = "dalmothchiura";
			}
			else{
				$(this).addClass('snackLabelSelected');
				$(this).siblings("p").css('display', 'none');
				content[5].smalldiy.diyInstructionData = slide6instructionmakai;
				content[5].mixItemXSource = $ref+"/images/page3/makai.png";
				content[5].mixItemYSource = $ref+"/images/page3/bhatmas.png";
				content[5].mixtureSource = $ref+"/images/page3/makaibhatmas.png";
				content[6].snacksSource = $ref+"/images/page3/makaibhatmas.png";
				content[5].mixItemXLabelData = data.string.p3s19;
				content[5].mixItemYLabelData = data.string.p3s20;
				content[5].mixtureLabelData = data.string.p3s14;
				content[5].instructionDataChange = data.string.p3s22;
				content[5].whatSnacks = "makaibhatmas";
			}
			$board.children('.diySmall').children('p').css('visibility', 'hidden');
			
			$nextBtn.show(0);				
		});
	}

/*
* snacksOnTable
*/
	function snacksOnTable() {
		var source = $("#snacksOnTable-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$board.children('div.snacksOnTableArea').children('div.tea').children('img.teaVapourImage').show(0).addClass('vapourRise');
		ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*first call to mixThat*/
	diyLandingPage();
	// snacksOnTable(countNext+=6);

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		countNext++;
		switch (countNext) {
			case 1:				
				mixThat(countNext);
				break;
			case 2:				
				mixThat(countNext);
				break;
			case 3:
				mixThat(countNext);
				break;
			case 4:
				chooseSnack(countNext);
				break;
			case 5:
				mixThat(countNext);
				break;
			case 6:
				snacksOnTable();
				break;
			default:break;
		}		
		loadTimelineProgress($total_page,countNext+1);
	});

});
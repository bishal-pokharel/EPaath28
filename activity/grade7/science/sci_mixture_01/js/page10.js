/*definition image path*/
$definitionImgSrc1 = "images/definitionpage/definition1.png",
$definitionImgSrc2 = "images/definitionpage/definition2.png",
$definitionImgSrc3 = "images/definitionpage/definition3.png",
$definitionImgSrc4 = "images/definitionpage/definition4.png"

var content=[
	{	
		recapImageSource : $definitionImgSrc1,
		recapTextData : data.string.p2s0,
		definitionData : data.string.p2s1
	},
	{	
		definitionImageSource : $definitionImgSrc2,
		definitionTextData : data.string.p2s0,
		definitionData : data.string.p2s1
	},
	{	
		definitionImageSource : $definitionImgSrc3,
		definitionTextData : data.string.p2s0,
		definitionData : data.string.p2s1
	},
	{	
		definitionImageSource : $definitionImgSrc4,
		definitionTextData : data.string.p2s0,
		definitionData : data.string.p2s1
	}
];


$(function () {	
	var $board = $('.board');
	var countNext = 0;
	var $total_page = 1;
	loadTimelineProgress($total_page,countNext+1);

	var randomImageNumeral = ole.getRandom(1,3,0);

/*
* recapLandingPage
*/
	function recapLandingPage() {
		var source = $("#recapLandingPage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);	
		ole.footerNotificationHandler.lessonEndSetNotification();
	}	

	/*call the definition landing page function*/
	recapLandingPage();
});
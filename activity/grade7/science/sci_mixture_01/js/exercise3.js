var $refImg = $ref+"/images/exercise3/";

var content=[
	{
		instruction:[data.string.e3s1,data.string.e3s2]
	},
	{
		processData :"evaporation",
		mixtureCaption : data.string.e3_saltwater,
		mixtureImgSrc :$refImg+"saltwater.png",
		questionData :data.string.e3_question_saltwater
	},
	{
		processData :"sublimation",
		mixtureCaption : data.string.e3_sandiodine,
		mixtureImgSrc :$refImg+"sandiodine.png",
		questionData :data.string.e3_question_sandiodine
	},
	{
		processData :"crystallization",
		mixtureCaption : data.string.e3_cuso4,
		mixtureImgSrc :$refImg+"cuso4.png",
		questionData :data.string.e3_question_cuso4
	},
	{
		processData :"centrifusion",
		mixtureCaption : data.string.e3_curd,
		mixtureImgSrc :$refImg+"curd.png",
		questionData :data.string.e3_question_curd
	},
	{
		processData :"chromatography",
		mixtureCaption : data.string.e3_color,
		mixtureImgSrc :$refImg+"color.png",
		questionData :data.string.e3_question_color
	},
	{
		congratulationData : data.string.e2s15
	}
]

$(function () {
	var $board = $('.board');
	var $expert = $('div.expert');
	// var $nextBtn = $(".nextBtn");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 6;
	loadTimelineProgress($total_page,countNext+1);

	$("div.sublimation").children('p.expertLabel').text(data.string.p9s7);
	$("div.crystallization").children('p.expertLabel').text(data.string.p9s9);
	$("div.chromatography").children('p.expertLabel').text(data.string.p9s10);
	$("div.evaporation").children('p.expertLabel').text(data.string.p9s6);
	$("div.centrifusion").children('p.expertLabel').text(data.string.p9s8);
/*
* gameInstruction
*/
	function gameInstruction() {
		var source = $("#gameInstruction-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show();
	}

/*
* mixtureSample
*/
	function mixtureSample() {
		var source = $("#mixtureSample-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		var $mixtureSample = $board.children('div.mixtureSample');
		var $currentExpertChild;
		var expertProcess;
		var questionProcess;
		var dialogText;
		var dialogPart1;
		var dialogPart2;

		/*on clicking the process expert divs*/
		$expert.on('click', function() {
			$expert.removeClass('expertClicked expertCorrect');
			$expert.children('div.dialog').css('display', 'none');
			expertProcess = $(this).data("process");
			questionProcess = $mixtureSample.data("processname");

			switch(expertProcess){
				case "evaporation": dialogPart2 = data.string.e3_dialog_saltwater;
						break;
				case "sublimation": dialogPart2 = data.string.e3_dialog_sandiodine;
						break;
				case "crystallization": dialogPart2 = data.string.e3_dialog_cuso4;
						break;
				case "centrifusion": dialogPart2 = data.string.e3_dialog_curd;
						break;
				case "chromatography": dialogPart2 = data.string.e3_dialog_color;
						break;
				default:break;
			}


			if(expertProcess === questionProcess){
				$nextBtn.show();
				$(this).addClass('expertCorrect');
				$currentExpertChild = $(this).children('div.dialog');
				/*check the dialog box is left orient or right and add and remove
				correct dialog box style accordingly*/
				if($currentExpertChild.hasClass('dialogafter')){
					$currentExpertChild.removeClass('dialogafter');
					$currentExpertChild.addClass('dialogafterCorrect');
				}
				else if($currentExpertChild.hasClass('dialogbefore')){
					$currentExpertChild.removeClass('dialogbefore');
					$currentExpertChild.addClass('dialogbeforeCorrect');
				}
				dialogPart1 = data.string.e3_right;
				$nextBtn.show(0);
				$expert.css('cursor', 'default');
				$expert.off('click');
			}

			else if(expertProcess != questionProcess){
				$(this).addClass('expertClicked');
				dialogPart1 = data.string.e3_wrong;
			}

			dialogText = dialogPart1 + dialogPart2;
			$(this).children('div.dialog').children('p.dialogText').html(dialogText);
			$(this).children('div.dialog').show(0);

		});
	}

/*
* congratulation
*/
	function congratulation() {
		var source = $("#congratulation-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$expert.css('cursor', 'pointer');
		ole.footerNotificationHandler.pageEndSetNotification();
	}

gameInstruction();

	/*on next button click*/
	$nextBtn.on('click',function () {
		countNext++;
		$expert.children('div.dialogbeforeCorrect').addClass('dialogbefore').removeClass('dialogbeforeCorrect');
		$expert.children('div.dialogafterCorrect').addClass('dialogafter').removeClass('dialogafterCorrect');
		$expert.children('div.dialog').css('display', 'none');
		$expert.css('cursor', 'pointer');
		$expert.removeClass('expertClicked expertCorrect');
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				mixtureSample();
				break;
			case 2:
				$(this).css("display","none");
				mixtureSample();
				break;
			case 3:
				$(this).css("display","none");
				mixtureSample();
				break;
			case 4:
				$(this).css("display","none");
				mixtureSample();
				break;
			case 5:
				$(this).css("display","none");
				mixtureSample();
				break;
			case 6:
				$(this).css("display","none");
				congratulation();
				break;
			default:break;
		}

		countNext<$total_page?loadTimelineProgress($total_page,countNext+1):loadTimelineProgress($total_page,countNext);
	});

});

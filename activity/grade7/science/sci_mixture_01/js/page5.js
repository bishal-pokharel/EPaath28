/*content object*/
var content=[
	{
		topicTextData : data.string.p5s1,
		mixtureSource1 : $ref+"/images/page1/gems.png",
		mixtureSource1Caption : data.string.p5s4,
		mixtureSource2 : $ref+"/images/page1/makaibhatmas.png",
		mixtureSource2Caption : data.string.p5s5,
		mixtureSource3 : $ref+"/images/page3/dalmothChiura.png",
		mixtureSource3Caption : data.string.p5s6,
		mixtureTypeTextData : data.string.p5s2,
		mixtureClass : "firstSlideImages",
		individualMixtureClass1 : "",
		bgColorClass : "bgBlue"
	},
	{
		topicTextData : "",
		mixtureSource1 : $ref+"/images/page4/masala.png",
		mixtureSource1Caption : data.string.p5s9,
		mixtureSource2 : $ref+"/images/page3/milk-glass.png",
		mixtureSource2Caption : data.string.p5s7,		
		mixtureSource3 : $ref+"/images/page3/tea-cup.png",
		mixtureSource3Caption : data.string.p5s8,
		mixtureTypeTextData : data.string.p5s3,
		mixtureClass : "",
		individualMixtureClass1 : "spice",
		bgColorClass : "bgGreen"
	}	
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

/*
* mixtureType
*/
	function mixtureType() {
		var source = $("#mixtureType-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		switch(countNext){
			case 0: var selector = $board.children('p:nth-of-type(2)');
					ole.parseToolTip(selector, data.string.p5s2_selectme);
					break;
			case 1: var selector = $board.children('p:nth-of-type(2)');
					ole.parseToolTip(selector, data.string.p5s3_selectme);
					break;
			default:break;
		}

		

		if(countNext < 1){
			$nextBtn.show(0);
		}
		else{
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
		
	}	

	/*first call to mixtureType*/
	mixtureType();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		countNext++;
		switch (countNext) {
			case 1:				
				mixtureType();
				break;
			default:break;
		}		
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		switch(countNext) {
			case 0:				
				mixtureType();
				ole.footerNotificationHandler.hideNotification();
				break;
			default:break;
		}		
		loadTimelineProgress($total_page,countNext+1);
	});

});
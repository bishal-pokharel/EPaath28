/*image path*/
$refImg = $ref+"/images/page9/";

/*content object*/
var content=[
	{
		firstLine : data.string.p9s0,
		technique1 : data.string.p9s1,
		technique2 : data.string.p9s2,
		technique3 : data.string.p9s3,
		technique4 : data.string.p9s4,
		technique5 : data.string.p9s5
	},
	[
		{
            next:data.string.goNext,
            prev:data.string.goBack,
            headingDisplayClass : "",
			headingData : data.string.p9s13,
			imageDisplayClass : "",
			imageSrc : $refImg+"evaporation1.png",
			explainImageData : data.string.p9s14	
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"evaporation2.png",
			explainImageData : data.string.p9s15
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"evaporation3.png",
			explainImageData : data.string.p9s16
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "displayProcessImage",
			explainTextClass:"techniqueDefinition",
			explainImageData : data.string.p9s17
		}
	],
	[
		{
			headingDisplayClass : "",
			headingData : data.string.p9s18,
			imageDisplayClass : "",
			imageSrc : $refImg+"sublimation1.png",
			explainImageData : data.string.p9s19
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"sublimation2.png",
			explainImageData : data.string.p9s20
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"sublimation3.png",
			explainImageData : data.string.p9s21
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "displayProcessImage",
			explainTextClass:"techniqueDefinition",
			explainImageData : data.string.p9s22
		}
	],
	[
		{
			headingDisplayClass : "",
			headingData : data.string.p9s23,
			imageDisplayClass : "",
			imageSrc : $refImg+"centrifusion1.png",
			explainImageData : data.string.p9s24
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"centrifusion2.png",
			explainImageData : data.string.p9s25
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"centrifusion3.png",
			explainImageData : data.string.p9s26
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "displayProcessImage",
			explainTextClass:"techniqueDefinition",
			explainImageData : data.string.p9s27
		}
	],
	[
		{
			headingDisplayClass : "",
			headingData : data.string.p9s28,
			imageDisplayClass : "",
			imageSrc : $refImg+"crystallization1.png",
			explainImageData : data.string.p9s29
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"crystallization2.png",
			explainImageData : data.string.p9s30
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"crystallization3.png",
			explainImageData : data.string.p9s31
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"crystallization4.png",
			explainImageData : data.string.p9s32
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"crystallization5.png",
			explainImageData : data.string.p9s33
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "displayProcessImage",
			explainTextClass:"techniqueDefinition",
			explainImageData : data.string.p9s34
		}
	],
	[
		{
			headingDisplayClass : "",
			headingData : data.string.p9s35,
			imageDisplayClass : "",
			imageSrc : $refImg+"chromatography1.png",
			explainImageData : data.string.p9s36
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"chromatography2.png",
			explainImageData : data.string.p9s37
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"chromatography3.png",
			explainImageData : data.string.p9s38
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"chromatography4.png",
			explainImageData : data.string.p9s39
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"chromatography5.png",
			explainImageData : data.string.p9s40
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "",
			imageSrc : $refImg+"chromatography6.png",
			explainImageData : data.string.p9s41
		},
		{
			headingDisplayClass : "displayHeading",
			headingData : "",
			imageDisplayClass : "displayProcessImage",
			explainTextClass:"techniqueDefinition",
			explainImageData : data.string.p9s42
		}
	],
	{
		firstLine : data.string.p9s11
	}
];


	var $board = $('.board');
	var $goToNextTechnique = $(".separationTechniquesWrapper").children('em:nth-of-type(1)');
	var $tabContainer = $(".tabContainer");
	var $tabEvaporationLabel = $tabContainer.children('div:nth-of-type(1)').children('label');
	var $tabSublimationLabel = $tabContainer.children('div:nth-of-type(2)').children('label');
	var $tabCentrifusionLabel = $tabContainer.children('div:nth-of-type(3)').children('label');
	var $tabCrystallizationLabel = $tabContainer.children('div:nth-of-type(4)').children('label');
	var $tabChromatographyLabel = $tabContainer.children('div:nth-of-type(5)').children('label');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var processStepCount = 0;

	/*technique id number to select from the content array
		evaporation = 1, sublimation = 2, centrifusion = 3,
		crystallization = 4 and chromatography =5.
	initialize the variable with 0*/
	var techniqueIdnumber =1;

	/*keep track of how many techiques are clicked and visited till last*/
	var numberOfTechniquesVisited = 0;


	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

	/*just to make sure the function below are initialized before they are used as they are called form html file =)*/
	var techniques, techniquesExplained, eachTechnique, eachTechniqueLab;

	/*variable to keep track if a technique is fully seen*/
	var evaporationVisited = sublimationVisited = centrifusionVisited = false;
    var crystallizationVisited = chromatographyVisited = false;

	/*add data to the labels*/
	$tabEvaporationLabel.text(data.string.p9s6);
	$tabSublimationLabel.text(data.string.p9s7);
	$tabCentrifusionLabel.text(data.string.p9s8);
	$tabCrystallizationLabel.text(data.string.p9s9);
	$tabChromatographyLabel.text(data.string.p9s10);
	$goToNextTechnique.text(data.string.p9s12);
/*
* techniques
*/
	function techniques() {
		var source = $("#techniques-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$tabContainer.css('display', 'none');
		$nextBtn.show(0);
	}

/*
* techniquesExplained
*/
	function techniquesExplained() {
		var source = $("#techniques-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[6]);
		$board.html(html);
		$tabContainer.show(0);
		$prevBtn.show(0);
	}

/*
* eachTechnique - while clicking each technique tab
*/
function eachTechnique(thisTechnique){

	var source = $("#eachTechnique-template").html();
	var template = Handlebars.compile(source);
	var html = template();
	$board.html(html);

	$prevBtn.css('display', 'none');
	$goToNextTechnique.css('display', 'none');
	var $nextInTechniquesBtn = $(".nextInTechniquesBtn");
    $(".nextInTechniquesBtn").html(data.string.goNext);
	var $prevInTechniquesBtn = $(".prevInTechniquesBtn");
    $(".prevInTechniquesBtn").html(data.string.goBack);
	var $eachTechniqueLab = $board.children('.eachTechnique').children('.eachTechniqueLab');
	processStepCount = 0;

	switch(thisTechnique){
		case "evaporation": techniqueIdnumber = 1; break;
		case "sublimation": techniqueIdnumber = 2; break;
		case "centrifusion": techniqueIdnumber = 3; break;
		case "crystallization": techniqueIdnumber = 4; break;
		case "chromatography":  techniqueIdnumber = 5; break;
		default:break;
	}

	$nextInTechniquesBtn.show(0);

	eachTechniqueLab($eachTechniqueLab);

	$nextInTechniquesBtn.on('click',function () {
		$(this).css("display","none");
		$prevInTechniquesBtn.css('display', 'none');
		processStepCount++;
		eachTechniqueLab($eachTechniqueLab);
		switch(techniqueIdnumber){
			case 1:
			case 2:
			case 3: switch(processStepCount){
						case 0:
						case 1:
						case 2: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.show(0);
								break;
						case 3: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.show(0);
								$goToNextTechnique.show(0);
								switch(techniqueIdnumber){
									case 1: var selector = $eachTechniqueLab.children('.techniqueDefinition');
											ole.parseToolTip(selector, data.string.p9s17_selectme);
											if(evaporationVisited == false){
													evaporationVisited = true;
													if(evaporationVisited){$tabEvaporationLabel.siblings('img').show(0);}
												if(++numberOfTechniquesVisited == 5){
													ole.footerNotificationHandler.pageEndSetNotification();
												}
											}
											break;
									case 2: var selector = $eachTechniqueLab.children('.techniqueDefinition');
											ole.parseToolTip(selector, data.string.p9s22_selectme);
											if(sublimationVisited == false){
													sublimationVisited = true;
													if(sublimationVisited){$tabSublimationLabel.siblings('img').show(0);}
												if(++numberOfTechniquesVisited == 5){
													ole.footerNotificationHandler.pageEndSetNotification();
												}
											}
											break;
									case 3: var selector = $eachTechniqueLab.children('.techniqueDefinition');
											ole.parseToolTip(selector, data.string.p9s27_selectme);
											if(centrifusionVisited == false){
													centrifusionVisited = true;
													if(centrifusionVisited){$tabCentrifusionLabel.siblings('img').show(0);}
												if(++numberOfTechniquesVisited == 5){
													ole.footerNotificationHandler.pageEndSetNotification();
												}
											}
											 break;
									default : break;
								}
								break;
						case 4: switch(techniqueIdnumber){
									case 1: $tabSublimationLabel.siblings('input').prop("checked", true);
											eachTechnique("sublimation");
											break;
									case 2: $tabCentrifusionLabel.siblings('input').prop("checked", true);
											eachTechnique("centrifusion");
											break;
									case 3: $tabCrystallizationLabel.siblings('input').prop("checked", true);
											eachTechnique("crystallization");
											 break;
									default : break;
								}
								break;
						default:break;
					}
					break;
			case 4: switch(processStepCount){
						case 0:
						case 1:
						case 2:
						case 3:
						case 4: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.show(0);
								break;
						case 5: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.show(0);
								$goToNextTechnique.show(0);

								var selector = $eachTechniqueLab.children('.techniqueDefinition');
											ole.parseToolTip(selector, data.string.p9s34_selectme);

								if(crystallizationVisited == false){
										crystallizationVisited = true;
										if(crystallizationVisited){$tabCrystallizationLabel.siblings('img').show(0);}
									if(++numberOfTechniquesVisited == 5){
										ole.footerNotificationHandler.pageEndSetNotification();
									}
								}
								break;
						case 6: $tabChromatographyLabel.siblings('input').prop("checked", true);
								eachTechnique("chromatography");
								break;
						default:break;
					}
					break;
			case 5: switch(processStepCount){
						case 0:
						case 1:
						case 2:
						case 3:
						case 4:
						case 5: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.show(0);
								break;
						case 6: $nextInTechniquesBtn.css('display', 'none');
								$prevInTechniquesBtn.show(0);
								var selector = $eachTechniqueLab.children('.techniqueDefinition');
											ole.parseToolTip(selector, data.string.p9s42_selectme);
								if(chromatographyVisited == false){
										chromatographyVisited = true;
										if(chromatographyVisited){$tabChromatographyLabel.siblings('img').show(0);}
									if(++numberOfTechniquesVisited == 5){
										ole.footerNotificationHandler.pageEndSetNotification();
									}
								}
								break;
						default:break;
					}
					break;
		    default: break;
		}

	});

	$prevInTechniquesBtn.on('click',function () {
		$(this).css("display","none");
		$nextInTechniquesBtn.css('display', 'none');
		processStepCount--;
		eachTechniqueLab($eachTechniqueLab);
		switch(techniqueIdnumber){
			case 1:
			case 2:
			case 3:switch(processStepCount){
						case 0: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.css('display', 'none');
								break;
						case 1:
						case 2: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.show(0);
								break;
						default:break;
					}
					break;
			case 4:switch(processStepCount){
						case 0: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.css('display', 'none');
								break;
						case 1:
						case 2:
						case 3:
						case 4: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.show(0);
								break;
						default:break;
					}
					break;
			case 5:switch(processStepCount){
						case 0: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.css('display', 'none');
								break;
						case 1:
						case 2:
						case 3:
						case 4:
						case 5: $nextInTechniquesBtn.show(0);
								$prevInTechniquesBtn.show(0);
								break;
						default:break;
					}
					break;
			default:break;

		}
	});
}

/*the inside handlebar for eachTechniqueLab div*/
function eachTechniqueLab($eachTechniqueLab){
	var sourceInside = $("#eachTechniqueLab-template").html();
	var template = Handlebars.compile(sourceInside);
	var html = template(content[techniqueIdnumber][processStepCount]);
	$eachTechniqueLab.html(html);
}

	/*first call to techniques*/
	techniques();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		countNext++;
		switch (countNext) {
			case 1:
				techniquesExplained();
				break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		switch (countNext) {
			case 0:
				techniques();
				break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});

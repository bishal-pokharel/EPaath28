// images path
$refImg = $ref+"/images/page4/"

/*content object*/
var content=[
	{	
		instructionText : data.string.p4concreteInstruction,
		compoundmixtureImgSrc : $refImg+"concrete.png" ,
		clickMeImgSrc : "images/clickme_icon.png",
		ingredientLeftimages : [
				{
					imgSrc : $refImg+"sand.png",
					imgcaption :data.string.p4sand
				},
				{
					imgSrc : $refImg+"water.png",
					imgcaption :data.string.p4water
				}
		],
		ingredientRightimages : [
				{
					imgSrc : $refImg+"ballast.png",
					imgcaption :data.string.p4ballast
				},
				{
					imgSrc : $refImg+"cement.png",
					imgcaption :data.string.p4cement
				}
		],
		explainTextList : [
				data.string.p4concreteExplainText1
		]
	},
	{	
		instructionText : data.string.p4spicesInstruction,
		compoundmixtureImgSrc : $refImg+"masala.png" ,
		clickMeImgSrc : "images/clickme_icon.png",
		ingredientLeftimages : [
				{
					imgSrc : $refImg+"cardamom.png",
					imgcaption :data.string.p4cardamom
				},
				{
					imgSrc : $refImg+"cinnamon.png",
					imgcaption :data.string.p4cinnamon
				},
				{
					imgSrc : $refImg+"cloves.png",
					imgcaption :data.string.p4cloves
				}
		],
		ingredientRightimages : [
				{
					imgSrc : $refImg+"coriander.png",
					imgcaption :data.string.p4coriander
				},
				{
					imgSrc : $refImg+"cumin.png",
					imgcaption :data.string.p4cumin
				},
				{
					imgSrc : $refImg+"pepper.png",
					imgcaption :data.string.p4pepper
				}
		],
		explainTextList : [
				data.string.p4spiceExplainText1,data.string.p4spiceExplainText2
		]
	}
]

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

/*
* compoundmixture*/
	function compoundmixture() {
		var source = $("#compoundmixture-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		
		var $compoundmixture = $board.children('div.compoundmixture');
		var $allImages = $compoundmixture.children('div.allImages');
		var $finalMixture = $allImages.children('div.finalMixture');
		var $ingredients = $allImages.children('div.ingredient').children('figure');
		var $explainText = $compoundmixture.children('div.explainText').children('p');

		$finalMixture.on('click',function() {
			$(this).children('img.clickMeIconImage').remove();
			$finalMixture.off("click");
			$finalMixture.css('cursor', 'default');
				$ingredients.fadeIn(1000,function(){

					/*if explain text has only one p tag that means it is first slide*/					
					if($explainText.length == 1){
						$explainText.eq(0).fadeIn(500,function(){
							$nextBtn.show(0);
						});
					}

					/*if explain text has two p tags that means it is second slide*/
					else if($explainText.length > 1){
						$explainText.eq(0).fadeIn(500,function(){
                            $explainText.eq(1).fadeIn(2000, function() {
                                $prevBtn.show(0);
                                ole.footerNotificationHandler.pageEndSetNotification();
                            });
						});
					}
				});			
		});

	}

	compoundmixture();

	$nextBtn.on('click',function () {
		countNext++;
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		switch(countNext){
			case 1:
				compoundmixture();
				break;
			default:break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		switch(countNext){
			case 0:
				ole.footerNotificationHandler.hideNotification();
				compoundmixture();
				break;
			default:break;
		}
	});

});
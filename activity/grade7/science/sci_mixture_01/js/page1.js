var content=[
	{
		topicText : data.lesson.chapter,
		textclass : 'titletext',
		imageclass : 'bg_full',
		imageSrc : $ref+"/images/page1/mixture.png"

	},
	{
		imageSrc : $ref+"/images/page1/gems.png",
		imageAlt : data.string.p1s2,
		exampleText : data.string.p1s2
	},
	{
		imageSrc : $ref+"/images/page1/makaibhatmas.png",
		imageAlt : data.string.p1s3,
		exampleText : data.string.p1s3
	},
	{
		topicText : data.string.p1s4,
	},
	{
		topicText : data.string.p1s5,
	},
	{
		topicText : data.string.p1s8,
		imageSrc : $ref+"/images/page1/milksugar.png",
		imageAlt : data.string.p1s7,
		exampleText : data.string.p1s7
	},
	{
		imageSrc : $ref+"/images/page1/blacktea.png",
		imageAlt : data.string.p1s6,
		exampleText : data.string.p1s6
	}
]


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 7;
	loadTimelineProgress($total_page,countNext+1);

	$(".mixture_ex > p").text(data.string.p1s1);

/*
* first
*/
	function first () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		if(countNext < 7){
			$nextBtn.css('display', '');
		}
		else{
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

/*
* second
*/
	function second () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show(0);


		switch(countNext){
			case 3: var selector = $board.children('p:nth-of-type(1)');
					ole.parseToolTip(selector, data.string.p1s4_selectme);
					break;
			case 4: var selector = $board.children('p:nth-of-type(1)');
					ole.parseToolTip(selector, data.string.p1s5_selectme);
					break;
			default:break;
		}
	}
	first();

	$nextBtn.on('click',function () {
		countNext++;
		switch (countNext) {
			case 0:
				$(this).css("display","none");
				first();
				break;
			case 1:
				$(this).css("display","none");
				first();
				break;
			case 2:
				$(this).css("display","none");
				first();
				break;
			case 3:
				$(this).css("display","none");
				second();
				break;
			case 4:
				$(this).css("display","none");
				second();
				break;
			case 5:
				$(this).css("display","none");
				first();
				break;
			case 6:
				$(this).css("display","none");
				first();
				ole.footerNotificationHandler.pageEndSetNotification();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});

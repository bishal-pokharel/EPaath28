/*content object*/
var content=[
	{
		instructionData : data.string.p7s1,
		mixItem1Source : $ref+"/images/page7/water.png",
		mixItem1Caption : data.string.p7s2,
		plusSource : $ref+"/images/page3/plus.png",
		mixItem2Source : $ref+"/images/page7/sugar.png",
		mixItem2Caption : data.string.p7s3,
		equalSource : $ref+"/images/page7/equal.png", 
		mixtureSource : $ref+"/images/page7/sugarmix.png",
		mixtureCaption : data.string.p7s4		
	},
	{
        next:data.string.goNext,
        instructionData : data.string.p7s5,
		mixItem1Data: "mixture1",
		mixItem1Source : $ref+"/images/page7/lemon-water.png",
		mixItem1Caption : data.string.p7s6,
		mixItem2Data: "mixture2",
		mixItem2Source : $ref+"/images/page7/salt-water.png",
		mixItem2Caption : data.string.p7s7,
		mixItem3Data: "mixture3",
		mixItem3Source : $ref+"/images/page7/sand-water.png",
		mixItem3Caption : data.string.p7s8,
		mixItem4Data: "mixture4",
		mixItem4Source : $ref+"/images/page7/milk-sugar.png",
		mixItem4Caption : data.string.p7s9,
		mixItem5Data: "mixture5",
		mixItem5Source : $ref+"/images/page7/oil-water.png",
		mixItem5Caption : data.string.p7s10,
		mixItem6Data: "mixture6",
		mixItem6Source : $ref+"/images/page7/orange-juice.png",
		mixItem6Caption : data.string.p7s11			
	},
	{
		next:data.string.goNext,
		instructionData : data.string.p7s16,
		mixtureUse1 : data.string.p7s22,
		mixtureUse2: data.string.p7s23,
		mixtureUse3 : data.string.p7s24,
		mixtureUse4 : data.string.p7s25,
		mixtureUse5: data.string.p7s26,
		mixtureNeed : data.string.p7s27
	}
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

/*
* solutionExample
*/
	function solutionExample() {
		var source = $("#solutionExample-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.show(0);		
	}	


/*
* selectMixture
*/
	function selectMixture() {
		var source = $("#selectMixture-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	

		var selectedMixture;
		var selectionConclusionData;
		var mixtureName;
		var mixture1selected = mixture2selected = mixture4selected = mixture6selected = false;
		var $selectionConclusion = $board.children('.selectionConclusion');
		var $selectionAreaFigure = $board.children('.selectionArea').children('figure');

		/*on clicking mixture types*/
		$selectionAreaFigure.on('click',function() {
			selectedMixture = $(this).data("mixturetype");
			mixtureName = $(this).children('figcaption').text();
			$mixtureLabel = $(this).children('figcaption');
			switch(selectedMixture){
				case "mixture1":
						mixture1selected = true;
						selectionConclusionData = data.string.p7s12 + mixtureName + data.string.p7s13;
						$selectionConclusion.html(selectionConclusionData);
						$selectionConclusion.css('background-color', 'rgba(208,244,119,0.5)');
						$(this).children('div').addClass('correctStyle');
					break;
				case "mixture2":
						mixture2selected = true;
						selectionConclusionData = data.string.p7s12 + mixtureName + data.string.p7s13;
						$selectionConclusion.html(selectionConclusionData);
						$selectionConclusion.css('background-color', 'rgba(208,244,119,0.5)');
						$(this).children('div').addClass('correctStyle');
					break;
				case "mixture3":
						selectionConclusionData = data.string.p7s14 + mixtureName + data.string.p7s15;
						$selectionConclusion.html(selectionConclusionData);
						$selectionConclusion.css('background-color', 'rgba(255,25,0,0.1)');
						$(this).children('div').addClass('incorrectStyle');
					break;
				case "mixture4":
						mixture4selected = true;
						selectionConclusionData = data.string.p7s12 + mixtureName + data.string.p7s13;
						$selectionConclusion.html(selectionConclusionData);
						$selectionConclusion.css('background-color', 'rgba(208,244,119,0.5)');
						$(this).children('div').addClass('correctStyle');
					break;
				case "mixture5":						
						selectionConclusionData = data.string.p7s14 + mixtureName + data.string.p7s15;
						$selectionConclusion.html(selectionConclusionData);
						$selectionConclusion.css('background-color', 'rgba(255,25,0,0.1)');
						$(this).children('div').addClass('incorrectStyle');
					break;
				case "mixture6":
						mixture6selected = true;
						selectionConclusionData = data.string.p7s12 + mixtureName + data.string.p7s13;
						$selectionConclusion.html(selectionConclusionData);
						$selectionConclusion.css('background-color', 'rgba(208,244,119,0.5)');
						$(this).children('div').addClass('correctStyle');
					break;
				default:break;
			}

			if(mixture1selected && mixture2selected && mixture4selected && mixture6selected){
				$nextBtn.show(0);
				$prevBtn.show(0);
			}
		});
	}	

/*
* mixtureUses
*/
	function mixtureUses() {
		var source = $("#mixtureUses-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $usesArea = $board.children('.usesArea');
		var $nextUsesButton =  $board.children('.nextUse');
		var $usesUnorderedList = $usesArea.children('ul');
		var nextUseCount = 0;

		$nextUsesButton.show(0);
		
		$nextUsesButton.on('click', function() {
			nextUseCount++;
			$usesUnorderedList.children('li:nth-of-type('+nextUseCount+')').css('opacity', '1');
			if(nextUseCount == 6){
				$(this).css('display', 'none').off("click");
				$usesArea.children('p').css('opacity', '1');
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		});		
	}
	
	/*first call to solutionExample*/
	solutionExample();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		countNext++;
		switch(countNext) {
			case 1:				
				selectMixture();
				break;
			case 2:				
				mixtureUses();
				break;
			
			default:break;
		}		
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		switch(countNext) {
			case 0:				
				solutionExample();
				break;
			case 1:				
				selectMixture();
				break;			
			default:break;
		}		
		loadTimelineProgress($total_page,countNext+1);
	});

});
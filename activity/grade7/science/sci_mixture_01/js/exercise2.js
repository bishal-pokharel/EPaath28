
var $imgPath = $ref+"/images/exercise2/";

var content=[
	{
		mixtureCollection:[
			{
				mixtureTypeData : "homogeneous",
				mixtureNameData : "water",
				mixtureImgSrc : $imgPath+"water.png",
				mixItemCaption : data.string.e2s1
			},
			{
				mixtureTypeData : "heterogeneous",
				mixtureNameData : "pizza",
				mixtureImgSrc : $imgPath+"pizza.png",
				mixItemCaption : data.string.e2s2
			},
			{
				mixtureTypeData : "homogeneous",
				mixtureNameData : "blood",
				mixtureImgSrc : $imgPath+"blood.png",
				mixItemCaption : data.string.e2s3
			},
			{
				mixtureTypeData : "heterogeneous",
				mixtureNameData : "soil",
				mixtureImgSrc : $imgPath+"soil.png",
				mixItemCaption : data.string.e2s4
			},
			{
				mixtureTypeData : "homogeneous",
				mixtureNameData : "coffee",
				mixtureImgSrc : $imgPath+"coffee.png",
				mixItemCaption : data.string.e2s6
			},
			{
				mixtureTypeData : "homogeneous",
				mixtureNameData : "air",
				mixtureImgSrc : $imgPath+"air.png",
				mixItemCaption : data.string.e2s7
			},
			{
				mixtureTypeData : "heterogeneous",
				mixtureNameData : "sand",
				mixtureImgSrc : $imgPath+"sand.png",
				mixItemCaption : data.string.e2s8
			},
			{
				mixtureTypeData : "homogeneous",
				mixtureNameData : "perfume",
				mixtureImgSrc : $imgPath+"perfume.png",
				mixItemCaption : data.string.e2s9
			},
			{
				mixtureTypeData : "heterogeneous",
				mixtureNameData : "gems",
				mixtureImgSrc : $imgPath+"gems.png",
				mixItemCaption : data.string.e2s10
			},
			{
				mixtureTypeData : "heterogeneous",
				mixtureNameData : "concrete",
				mixtureImgSrc : $imgPath+"concrete.png",
				mixItemCaption : data.string.e2s11
			}
		],
		homogeneousData : data.string.e2s12,
		homogeneousCollection:[{},{},{},{},{}],		
		heterogeneousData : data.string.e2s13,
		heterogeneousCollection:[{},{},{},{},{}],
		conclusionData : ""
	}
];

/*css style for correct and wrong conclusion data*/
var correctConclusion = {
	"background-color" : "rgba(208,244,119,0.5)"
}

var incorrectConclusion = {
	"background-color" : "rgba(255,25,0,0.1)"
}

$(function () {	
	var countNext = 0;

	var $total_page = 1;
	loadTimelineProgress($total_page,countNext+1);
	var $board = $(".board");


/*
* separateMixture
*/
	function separateMixture() {
		var source = $("#separateMixture-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $lastMsg = $board.children('b');
		var $selectionArea = $board.children('.selectionArea');
		var $clickSack = $board.children('div.clickSack');
		var $homogeneousCollector = $board.children('.homogeneousCollector');	
		var $homogeneousDiv = $homogeneousCollector.children('div.homogeneousDiv');
		var $heterogeneousCollector = $board.children('.heterogeneousCollector');
		var $heterogeneousDiv = $heterogeneousCollector.children('div.heterogeneousDiv');		
		var $selectionConclusion = $(".selectionConclusion");
		var $currentSubstance;
		var $nextSubstance;
		var $homogeneousImg; /*which homogeneous image to show*/	
		var $heterogeneousImg; /*which heterogeneous image to show*/
		var substanceCount = 0;
		var totalSubstance = 10;
		var homogeneousTotalCount = 5;
		var heteregeneousTotalCount = 5;
		var nthFigureCount;
		var nextNthFigureCount;
		var currentSubstanceName;
		var currentSubstanceType;

		var conclusionData;
		var wrongData = data.string.e2_wrong;
		var correctData = data.string.e2_correct;
		var reasonData;

		$selectionConclusion.html(data.string.e2s14);
		$lastMsg.html(data.string.e2s15);

		$homogeneousCollector.on('click', function() {
			$selectionConclusion.removeClass('instructionStyle');
			nthFigureCount = totalSubstance - substanceCount;
			nextNthFigureCount = nthFigureCount-1;
			$currentSubstance = $selectionArea.children('figure:nth-of-type('+nthFigureCount+')');
			currentSubstanceName = $currentSubstance.data("mixturename");
			currentSubstanceType = $currentSubstance.data("mixturetype");
			
			$nextSubstance = $selectionArea.children('figure:nth-of-type('+nextNthFigureCount+')');

			switch(currentSubstanceName){
				case "water": reasonData = data.string.e2_water;
					break;
				case "blood": reasonData = data.string.e2_blood;
					break;
				case "coffee": reasonData = data.string.e2_coffee;
					break;
				case "air": reasonData = data.string.e2_air;
					break;
				case "perfume": reasonData = data.string.e2_perfume;
					break;
				case "pizza": reasonData = data.string.e2_pizza;
					break;
				case "soil": reasonData = data.string.e2_soil;
					break;
				case "sand": reasonData = data.string.e2_sand;
					break;
				case "gems": reasonData = data.string.e2_gems;
					break;
				case "concrete": reasonData = data.string.e2_concrete;
					break;
				default:break;
			}

			if(currentSubstanceType === "homogeneous"){
				/*make the sack not respond to click when answer is correct till animation completes*/
				$clickSack.css('pointer-events', 'none');
				$homogeneousImg = $homogeneousDiv.children('img:nth-of-type('+homogeneousTotalCount+')');
				$homogeneousImg.attr("src",$imgPath+currentSubstanceName+".png");

				$currentSubstance.children('figcaption').css('display', 'none');
				$currentSubstance.addClass('flyIntoHomogeneousSack');
				$currentSubstance.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
						/*remove current element- not required*/
						$(this).remove();					
						$homogeneousImg.fadeIn(300,function(){
							$nextSubstance.delay(100).fadeIn(300,function(){
								$clickSack.css('pointer-events', 'auto');	
								});
							});			
						});								
				homogeneousTotalCount--;
				substanceCount++;
				conclusionData = correctData+reasonData;
				$selectionConclusion.css(correctConclusion);
			}

			else if(currentSubstanceType === "heterogeneous"){
				$currentSubstance.addClass('shakeItem').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      				$(this).removeClass();
    			});
    			conclusionData = wrongData+reasonData;
    			$selectionConclusion.css(incorrectConclusion);
			}

			$selectionConclusion.html(conclusionData);

			if(substanceCount == totalSubstance){
				$homogeneousCollector.off("click");
				$homogeneousCollector.css("cursor","default");
				$heterogeneousCollector.off("click");
				$heterogeneousCollector.css('cursor', 'default');
				$lastMsg.fadeIn(100);
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		});

		$heterogeneousCollector.on('click', function() {
			$selectionConclusion.removeClass('instructionStyle');
			nthFigureCount = totalSubstance - substanceCount;
			nextNthFigureCount = nthFigureCount-1;
			$currentSubstance = $selectionArea.children('figure:nth-of-type('+nthFigureCount+')');
			currentSubstanceName = $currentSubstance.data("mixturename");
			currentSubstanceType = $currentSubstance.data("mixturetype");
			
			$nextSubstance = $selectionArea.children('figure:nth-of-type('+nextNthFigureCount+')');

			switch(currentSubstanceName){
				case "water": reasonData = data.string.e2_water;
					break;
				case "blood": reasonData = data.string.e2_blood;
					break;
				case "coffee": reasonData = data.string.e2_coffee;
					break;
				case "air": reasonData = data.string.e2_air;
					break;
				case "perfume": reasonData = data.string.e2_perfume;
					break;
				case "pizza": reasonData = data.string.e2_pizza;
					break;
				case "soil": reasonData = data.string.e2_soil;
					break;
				case "sand": reasonData = data.string.e2_sand;
					break;
				case "gems": reasonData = data.string.e2_gems;
					break;
				case "concrete": reasonData = data.string.e2_concrete;
					break;
				default:break;
			}

			if(currentSubstanceType === "heterogeneous"){
				/*make the sack not respond to click when answer is correct till animation completes*/
				$clickSack.css('pointer-events', 'none');
				$heterogeneousImg = $heterogeneousDiv.children('img:nth-of-type('+heteregeneousTotalCount+')');
				$heterogeneousImg.attr("src",$imgPath+currentSubstanceName+".png");

				$currentSubstance.children('figcaption').css('display', 'none');
				$currentSubstance.addClass('flyIntoHeterogeneousSack');
				$currentSubstance.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					/*remove current element- not required*/
					$(this).remove();
					$heterogeneousImg.fadeIn(300,function(){
					$nextSubstance.delay(100).fadeIn(300,function(){
						$clickSack.css('pointer-events', 'auto');
						});
					});
				});				
				heteregeneousTotalCount--;
				substanceCount++;
				conclusionData = correctData+reasonData;
				$selectionConclusion.css(correctConclusion);
			}

			else if(currentSubstanceType === "homogeneous"){
				$currentSubstance.addClass('shakeItem').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      				$(this).removeClass();
    			});
    			conclusionData = wrongData+reasonData;
    			$selectionConclusion.css(incorrectConclusion);
			}

			$selectionConclusion.html(conclusionData);

			if(substanceCount == totalSubstance){
				$homogeneousCollector.off("click");
				$homogeneousCollector.css("cursor","default");
				$heterogeneousCollector.off("click");
				$heterogeneousCollector.css('cursor', 'default');
				$lastMsg.fadeIn(100);
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		});
		
	}	

	/*call to separateMixture*/
	separateMixture();

});
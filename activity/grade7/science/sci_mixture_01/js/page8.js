var arrowImage = $ref+'/images/page3/arrowgreen.png';
var arrowImageHtml = "<img src='"+arrowImage+"'>";
var slide1instruction1 = ole.textSR(data.string.p8s5,"#imageplaceholder#",arrowImageHtml);
var slide1instruction2 = ole.textSR(data.string.p8s11_addup,"#imageplaceholder#",arrowImageHtml);

/*content object*/
var content=[
	{
		firstLine : data.string.p8s1,
		list1 : data.string.p8s2,
		list2 : data.string.p8s3,
		list3 : data.string.p8s4
	},
	{
        next:data.string.goNext,
        instructionData : slide1instruction1,
		mixItemXSource : $ref+"/images/page3/sugar.png",
		mixerSource : $ref+"/images/page3/arrowgreen.png",
		mixItemYSource : $ref+"/images/page7/water.png",
		mixItemXClass : "mixItemX1",
		mixItemXLabelClass : "mixItemX1Label", 
		mixItemXLabelData : data.string.p8s6,
		mixerClass : "mixer1 bounceItem",
		mixItemYClass : "mixItemY1",
		mixItemYLabelClass : "mixItemY1Label",
		mixItemYLabelData : data.string.p3s5
	},
	{
        next:data.string.goNext,
        instructionData : data.string.p8s20,
		mixItemXSource : $ref+"/images/page3/sugar.png",
		mixerSource : $ref+"/images/page3/arrowgreen.png",
		mixItemYSource : $ref+"/images/page8/sugarwaterunmix.png",
		mixItemXClass : "mixItemX1",
		mixItemXLabelClass : "mixItemX1Label", 
		mixItemXLabelData : data.string.p8s6,
		mixerClass : "mixer1 bounceItem",
		mixItemYClass : "mixItemY1",
		mixItemYLabelClass : "mixItemY1Label",
		mixItemYLabelData : data.string.p3s5,
		fireSource : $ref+"/images/page8/fire.png"
	},
	{
		conclusionHeading : data.string.p8solutionconclusion,
		solutions : [
				{
					solutionClass : "",
					solutionImgSrc : $ref+"/images/page8/defunsaturated.png",
					solutionDefinition : data.string.p8s15
				},
				{
					solutionClass : "",
					solutionImgSrc : $ref+"/images/page8/defsaturated.png",
					solutionDefinition : data.string.p8s11
				},
				{
					solutionClass : "",
					solutionImgSrc : $ref+"/images/page8/defsupersaturated.png",
					solutionDefinition : data.string.p8supersaturatedDefinition
				}
		]
	}
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 4;
	loadTimelineProgress($total_page,countNext+1);	

	var mixDuration = 2700; /*this is the time taken by the animation of sugarmix*/

/***
**slide1
***/
	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show(0);
	}

/***
**slide2
***/
	function slide2(){
		var source = $("#slide2-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var sugarSpoonCount = 0;
		var $topInstruction = $board.children('.instruction');
		var $mixingArea = $board.children('.mixingArea');
		var $mixItemX = $mixingArea.children('.mixItemX1');
		var $mixItemY = $mixingArea.children('.mixItemY1');
		var $mixer = $mixingArea.children('.mixer1');
		var $mixingReport = $board.children('p:nth-of-type(2)');
		var $slidenextBtn = $board.children('.slidenextBtn');

		$board.on('click', '.mixer1', function() {	
			$(this).css('display', 'none');		
			switch(++sugarSpoonCount){
				case 1:	
						$topInstruction.css('display', 'none');
						$mixItemY.attr('src',$ref+"/images/page8/sugarwatermix.gif");
						$mixItemX.attr("src",$ref+"/images/page3/spoon-blank.png");
						$mixingArea.children('p:nth-of-type(-n+2)').css('display', 'none');						
							/*delay for some seconds*/
						setTimeout(
  							function() 
  								{	//do something special
  									$mixingArea.children('p:nth-of-type(2)').html(data.string.p8s8).show(0);
    								$mixItemX.attr("src",$ref+"/images/page3/sugar.png");
									$mixer.show(0);
									$mixingReport.html(data.string.p8s7);
  								}, mixDuration);
						
					break;

				case 2:	
						$board.children('.instruction').css('display', 'none');
						$mixItemY.attr('src',$ref+"/images/page8/sugarwatermix.gif");
						$mixItemX.attr("src",$ref+"/images/page3/spoon-blank.png");
						$mixingArea.children('p:nth-of-type(-n+2)').css('display', 'none');	
						$mixer.css('display', 'none');					
							/*delay for some seconds*/
						setTimeout(
  							function() 
  								{	//do something special
  									$mixingArea.children('p:nth-of-type(2)').html(data.string.p8s8).show(0);
    								$mixItemX.attr("src",$ref+"/images/page3/sugar.png");
									$mixingReport.html(data.string.p8s9);
									$slidenextBtn.show(0);
  								}, mixDuration);

						var slideNextCountCase2 = 0;
						$slidenextBtn.on('click', function() {							
							$(this).css('display', 'none');
							switch(++slideNextCountCase2){
								case 1: $mixingReport.html(data.string.p8s15).css('background-color', 'rgba(255,25,255,0.1)');
										ole.parseToolTip($mixingReport, data.string.p8s15_selectme);
										$slidenextBtn.show(0);
										break;
								case 2: $mixingReport.html(data.string.p8s15_addup).css('background-color', 'none');
										ole.parseToolTip($mixingReport, data.string.p8s15_addup_selectme);
										$slidenextBtn.show(0);
										break;
								case 3: $mixingReport.html(slide1instruction2).css('background-color', 'none');
										$mixer.show(0);
										$(this).off("click");
										break;
								default:break;
							}
							
						});						
					break;

				case 3:	$mixingReport.css('display', 'none');
						$board.children('.instruction').css('display', 'none');
						$mixItemY.attr('src',$ref+"/images/page8/sugarwatermix.gif");
						$mixItemX.attr("src",$ref+"/images/page3/spoon-blank.png");
						/*delay for some seconds*/
						setTimeout(
  							function() 
  								{	//do something special
    								$mixItemX.attr("src",$ref+"/images/page3/sugar.png");
									$mixingReport.html(data.string.p8s9_addup).show(0);
									$mixer.show(0);
  								}, mixDuration);
					break;

				case 4:	$mixingReport.css('display', 'none');
						$board.children('.instruction').css('display', 'none');
						$mixItemY.attr('src',$ref+"/images/page8/sugarwaterunmix.gif");
						$mixItemX.attr("src",$ref+"/images/page3/spoon-blank.png");
						/*delay for some seconds*/
						setTimeout(
  							function() 
  								{	//do something special
									$mixingReport.html(data.string.p8s10).show(0);
									$slidenextBtn.show(0);
  								}, mixDuration);

						var slideNextCountCase3 = 0;
						$slidenextBtn.on('click', function() {
							$(this).off("click");
							$(this).css('display', 'none');
							$mixingReport.html(data.string.p8s11).css('background-color', 'rgba(255,25,255,0.1)');	
							ole.parseToolTip($mixingReport, data.string.p8s11_selectme);
							$nextBtn.show(0);	
						});
					break;
				default:break;
			}				
		});		
	}
/***
**slide3
***/
	function slide3(){
		var source = $("#slide2-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var slidenextBtnCount = 0;
		var sugarSpoonCount = 0;
		var $mixingArea = $board.children('.mixingArea');
		var $mixItemX = $mixingArea.children('.mixItemX1');
		var $mixItemY = $mixingArea.children('.mixItemY1');
		var $mixer = $mixingArea.children('.mixer1');
		var $mixingAreaLabel1 = $mixingArea.children('p:nth-of-type(1)');
		var $mixtureLabel = $mixingArea.children('p:nth-of-type(2)');
		var $mixingReport = $board.children('p:nth-of-type(2)');
		var $slidenextBtn = $board.children('.slidenextBtn');
		var $fire = $mixingArea.children('.fireClass');

		$slidenextBtn.show(0);
		$mixItemX.css('display', 'none');
		$mixer.css('display', 'none');							
		$mixingAreaLabel1.css('display', 'none');
		$mixtureLabel.html(data.string.p8s8);
		$fire.show(0);

		/*on slide button click*/
			$slidenextBtn.on('click', function() {
				$(this).css('display', 'none');
				switch(++slidenextBtnCount){
					case 1: $mixingReport.html(data.string.p8s21);
							$slidenextBtn.show(0);
						break;
					case 2: $board.children('.instruction').css('display', 'none');
							$mixItemY.attr('src',$ref+"/images/page8/sugarwatermix.gif");
							$mixingReport.css('display', 'none');
							setTimeout(
		  						function() 
				  					{	//do something special
				  						$mixingReport.html(data.string.p8s12).show(0);
				  						$slidenextBtn.show(0);
				  					}, mixDuration);
							
						break;
					case 3:	$mixingReport.css('display', 'none');
							$mixItemX.show(0);	
				  			$mixer.show(0);					
							$mixingReport.html(data.string.p8s22).show(0);
						break;
					case 4: $mixingReport.html(data.string.p8s18);							
							$slidenextBtn.show(0);
						break;
					case 5: $mixingReport.html(data.string.p8s19).css('background-color', 'rgba(255,25,255,0.1)');
							ole.parseToolTip($mixingReport, data.string.p8s19_selectme);
							$prevBtn.show(0);
							$nextBtn.show(0);
						break;
					default:break;
				}
			});

		$board.on('click', '.mixer1', function() {	
			$(this).css('display', 'none');		
			switch(++sugarSpoonCount){
					case 1:	
						$mixingReport.css('display', 'none');
						$mixItemY.attr('src',$ref+"/images/page8/sugarwatermix.gif");
						$mixItemX.attr("src",$ref+"/images/page3/spoon-blank.png");						
							/*delay for some seconds*/
						setTimeout(
  							function() 
  								{	//do something special
    								$mixItemX.attr("src",$ref+"/images/page3/sugar.png");
									$mixingReport.html(data.string.p8s23).show(0);
									$mixer.show(0);
  								}, mixDuration);						
					break;
				
				case 2:	$mixingReport.css('display', 'none');
						$board.children('.instruction').css('display', 'none');
						$mixItemY.attr('src',$ref+"/images/page8/sugarwaterunmix.gif");
						$mixItemX.attr("src",$ref+"/images/page3/spoon-blank.png");
						/*delay for some seconds*/
						setTimeout(
  							function() 
  								{	//do something special
									$mixingReport.html(data.string.p8s17).show(0);
									$slidenextBtn.show(0);
  								}, mixDuration);
					break;
				default:break;
			}
		});		
	}

/***
**slide4
***/
	function slide4(){
		var source = $("#slide4-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);		

		var $conclusionBoard = $board.children('div.solutionConclusion').children('div.conclusionImages')
		var $unsaturatedDefinition = $conclusionBoard.children('figure:nth-of-type(1)').children('figcaption');
		var $saturatedDefinition = $conclusionBoard.children('figure:nth-of-type(2)').children('figcaption');
		var $superSaturatedDefinition = $conclusionBoard.children('figure:nth-of-type(3)').children('figcaption');
		
		ole.parseToolTip($unsaturatedDefinition, data.string.p8s15_selectme);
		ole.parseToolTip($saturatedDefinition, data.string.p8s11_selectme);
		ole.parseToolTip($superSaturatedDefinition, data.string.p8s19_selectme);
		$prevBtn.show(0);
		ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*first call to slide1*/
	slide1();
	// slide4(countNext+=3);

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		countNext++;
		switch (countNext) {
			case 1:				
				slide2();
				break;
			case 2:				
				slide3();
				break;
			case 3:				
				slide4();
				break;
			default:break;
		}		
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		switch (countNext) {
			case 0:			
				slide1();
				break;
			case 1:			
				slide2();
				break;
			case 2:	
				ole.footerNotificationHandler.hideNotification();		
				slide3();
				break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});


$(function(){
	
	loadTimelineProgress(1,1);			
	$(".p1_title").html(data.string.s1);
	
	$(".p6_ques_title").html(data.string.s14);
	$(".p6_insidetext").html(data.string.s14a);

	$(".p6_click_board").addClass('clickMeToo');

	ole.footerNotificationHandler.pageEndSetNotification();
	
	

	$(".p6_click_board").click(function(){


		if($(this).hasClass('clickMeToo'))
		{
			$(".p6_click_board").removeClass('clickMeToo');
			var myId=$(this).attr('id');
			resetAll();
			if(myId=='click_1')
			{
				$(".distanceToA").animate({'height':'70%', 'top': '15%'},1000,function(){

					$(".distanceAToB").animate({'width':'60%'},1000);

					$("#clicks_1").removeClass('glyphicon-ok');

					$("#clicks_1").addClass('glyphicon-remove');

					$("#clicks_1").removeClass('glyphicon-asterisk');
					$(".p6_click_board").addClass('clickMeToo');

				});
			}
			else if(myId=='click_2')
			{
				$(".distanceToC").animate({'width':'60%'},1000,function(){

					$(".distanceCToB").animate({'height':'70%', 'top':'15%'},1000);

					$("#clicks_2").removeClass('glyphicon-ok');

					$("#clicks_2").addClass('glyphicon-remove');

					$("#clicks_2").removeClass('glyphicon-asterisk');
					$(".p6_click_board").addClass('clickMeToo');
				});
			}
			else if(myId=='click_3')
			{
				$(".p6_diagonal").animate({'height':'115.5%','left': '78%','top': '16.5%'},1000,function(){


					$("#clicks_3").addClass('glyphicon-ok');
			
					$("#clicks_3").removeClass('glyphicon-remove');

					$("#clicks_3").removeClass('glyphicon-asterisk');
					$(".p6_click_board").addClass('clickMeToo');
				});
			}
		}
	});
	
	function resetAll()
	{
		$(".distanceToA").css({'height':'0%','top':'84%'});

		$(".distanceAToB").css({'width':'0%'});
		$(".distanceToC").css({'width':'0%'});

		$(".distanceCToB").css({'height':'0%','top':'84%'});

		$(".p6_diagonal").css({'height':'0%','left':'20%','top': '85%'});

		$(".glyphicon").removeClass('glyphicon-ok');
		$(".glyphicon").removeClass('glyphicon-remove');

		$(".glyphicon").addClass('glyphicon-asterisk');
	}	
	
		
});
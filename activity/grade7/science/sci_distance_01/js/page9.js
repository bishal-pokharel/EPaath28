

$(function(){
	
	loadTimelineProgress(1,1);	
	
	$(".p1_title").html(data.string.s26);
	$(".p2_ques_title").html(data.string.s25);


	$(".s17").html(data.string.s26);
	$(".s17a").html(data.string.s26a);


	$(".s18").html(data.string.s18);

	$(".s19").html(data.string.s19);
	$(".s19a").html(data.string.s19a);
	$(".s20").html(data.string.s27);


	$(".formula_box div").hide(0);

	$(".formula_box").hide(0);


	$(".formula_box").delay(1000).slideDown(500,'linear',function(){
		$(".s17").fadeIn(1000,'linear');
		$(".s17a").delay(1000).fadeIn(1000,'linear');
		
		$(".s18").delay(2000).fadeIn(1000,'linear');
		
		$(".s19").delay(3000).fadeIn(1000,'linear');
		$(".s19a").delay(4000).fadeIn(1000,'linear');
		
		$(".s20").delay(5000).fadeIn(1000,'linear',function(){
			ole.footerNotificationHandler.pageEndSetNotification();
		});

	});
			
		

});

$(function(){
	
	loadTimelineProgress(1,1);	
	$(".p1_title").html(data.string.s60);
	$(".p14_note").html(data.string.s61);
	$(".p14_replay_btn").html(getReloadBtn());
	$(".p2_note").hide(0);
	
	for(var $i=1;$i<=8;$i++)
	{
		$(".p15_divrow"+$i+" .coldiv1").html(data.string["p16_"+$i+"a"]);
		$(".p15_divrow"+$i+" .p15_coldiv2").html(data.string["p16_"+$i+"b"]);
	}
	
	$(".s62").html(data.string.s62);
	
	/*$(".p14_change0").html(data.string.s58+": ");
	$(".p14_change2").html(data.string.s57+": ");
	$(".p14_change4").html(data.string.s59+" : ");*/
	
	 carAnimation();
	
	

		
	$(".p14_replay_btn").click(function()
	{
		$(this).fadeOut(10);
		$(".p14_box div").show(0);
		$(".p14_insidetext").hide(0);
		
		$( ".p14_change1" ).html("0m");
		$( ".p14_change3" ).html("0s");
		$( ".p14_change5" ).html("0m/s");
		carAnimation();
	});
});


function carAnimation()
{
	$(".p14_box").hide(0);
	$(".p15_divtable").hide(0);
	
	$(".p14_car").css({'left':'-13%'});
	
	$(".p14_car").animate({'left':'-3%'},{
			duration: 1000,
			easing:'linear',
			complete: function(now,fx){
				$(".s62a").html(data.string.s62a+"= <font style='color:#CCFFFF'>10m/s "+data.string.s62_1+"</font>");
				$(".s62a").show(0);
				$(".p14_box").show(0);
			}
	}).animate({'left':'13%'},{
			duration: 500,
			easing:'linear',
			complete: function(now,fx){
				$(".s62a").html(data.string.s62b+"= <font style='color:#CCFFFF'>20m/s "+data.string.s62_1+"</font>");
				
				
			}
	}).animate({'left':'30%'},{
			duration: 2000,
			easing:'linear',
			complete: function(now,fx){
				$(".s62a").html(data.string.s62c+"= <font style='color:#CCFFFF'>5m/s "+data.string.s62_1+"</font>");
				
				
			}
	}).animate({'left':'45%'},{
			duration: 2000,
			easing:'linear',
			complete: function(now,fx){
				$(".s62a").html(data.string.s62d+"= <font style='color:#CCFFFF'>5m/s "+data.string.s62_1+"</font>");
				
				
			}
	}).animate({'left':'62%'},{
			duration: 500,
			easing:'linear',
			complete: function(now,fx){
				$(".s62a").html(data.string.s62e+"= <font style='color:#CCFFFF'>20m/s "+data.string.s62_1+"</font>");
				
				
			}
	}).animate({'left':'77%'},{
			duration: 1000,
			easing:'linear',
			complete: function(now,fx){
				$(".s62a").html(data.string.s62f+"= <font style='color:#CCFFFF'>10m/s "+data.string.s62_1+"</font>");
				
				
			}
	}).animate({'left':'100%'},{
			duration: 2000,
			easing:'linear',
			complete: function(now,fx){
				$(".s62a").html(data.string.s62g+"= <font style='color:#CCFFFF'>5m/s "+data.string.s62_1+"</font>");
				
				$(".p14_box div").delay(1500).fadeOut(500,'linear',function(){
					$(".s62a").fadeOut(500);
					$(".s62").fadeIn(500,'linear',function(){
					
						$(".p15_divtable").show(0);
						$(".p2_note").delay(1000).slideDown(500,'linear');
						
						
						$(".p14_replay_btn").delay(2000).fadeIn(500);
						ole.footerNotificationHandler.pageEndSetNotification();
					});
				});
				
			}
	});
	
}
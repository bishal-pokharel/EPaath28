

$(function(){

		var arrowbtn=getArrowBtn();
		// $("#activity-page-next-btn-enabled").html(arrowbtn);

		loadTimelineProgress(1,1);

		$(".p1_title").html(data.string.s1);
		$(".p5_ques_title").html(data.string.s10);



		var count=0;
		var mouse = $(".p5_mouse");
		var main_tl = new TimelineMax({repeat:0, onComplete:getPopUp});

		$("#activity-page-next-btn-enabled").show(0);



		var tween1=TweenMax.to(mouse, 5, {bezier:{values:[{ left: '10%', top: '42%' },{ left: '20%', top: '55%' },{ left: '40%', top: '56%' },{ left: '50%', top: '43%' },{ left: '53%', top: '33%' },{ left: '55%', top: '25%' },{ left: '58%', top: '20%' },{ left: '68%', top: '15%' },{ left: '87%', top: '23%' }], autoRotate:true}, ease:Power1.easeInOut});


		main_tl.add(tween1);



		function getPopUp()
		{

			$(".p5_insidetext").html(data.string.s11);
			$(".p5_slideDown").slideDown(500,'linear');
			$(".p5_mouse_path").show(0);
		}


		$("#activity-page-next-btn-enabled").click(function()
		{

			$(".p5_slideDown").slideUp(500,'linear',function(){

				if(count==0)
				{
					$(".p5_arrow").fadeIn(2000,'linear',function(){
						count++;
						$(".p5_insidetext").html(data.string.s12);
						$(".p5_slideDown").delay(500).slideDown(500,'linear');

					});
				}
				else if(count==1)
				{
					count++;
					$(".p5_insidetext").html(data.string.s13);
					$(".p5_slideDown").delay(500).slideDown(500,'linear');
				}
				else if(count==2)
				{

					ole.footerNotificationHandler.pageEndSetNotification();
				}


			});



		});

		$(".p5_replay_btn").click(function()
		{
			$(this).hide(0);
			$(".p5_arrow").hide(0);
			count=0;
			main_tl.restart();
		});

});


$(function(){

	var arrowbtn=getArrowBtn();
	$("#activity-page-next-btn-enabled").html(arrowbtn);
	$("#activity-page-next-btn-enabled").html(arrowbtn);



	loadTimelineProgress(1,1);
	$(".p1_title").html(data.string.s15);
	$(".p12_change0").html(data.string.s43+" : ");
	$(".p12_change2").html(data.string.s42+" : ");
	$(".p12_distance1").html(data.string.s43);
	$(".p12_time1").html(data.string.s42);
	$(".p12_speed1").html(data.string.s44);
	$(".p12_velocity1").html(data.string.s45);
	$(".p12_speed2").html(data.string.s43a);

	$(".p12_replay_btn").html(getReloadBtn());






	animate();

	$("#activity-page-next-btn-enabled").click(function(){

		$(this).hide(0);

		$(".p12_slidezone").slideUp(500,'linear',function(){

				animate2();
		}); //slideup
	});

	$("#activity-page-next-btn-enabled").click(function(){

		$(this).hide(0);

		$(".p12_slidezone").slideUp(500,'linear',function(){

				$(".p12_replay_btn").show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
		}); //slideup
	});


	$(".p12_replay_btn").click(function(){

		$(this).hide(0);
		$(".p12_slidezone").hide(0);
		animate();
	});

});
function animate()
{

	$(".p12_cycle1").css({ 'left': '80%','top': '38%'});
	$(".p12_cycle1").show(0);
	$(".p12_cycle2").hide(0);
	$(".p12_slidezone div").hide(0);


	var currentTime=0, currentDistance=0,count=0;
	var time = 5000;
	var distance = 70;

	var totalDis=0, totaltime=0;

	$(".p12_wheel_1").transition({rotate: '-360deg' },5000);
	$(".p12_wheel_2").transition({rotate: '-360deg' },5000);


	$(".p12_cycle1").animate({'left':'0%','top':'15%'},{
			duration: 5000,
			easing:'linear',
			step: function(now){

				if((count%2)==0)
				{
					var whatTime=100-now;

					currentTime = Math.round((whatTime*time)/(distance));

					currentDistance=(Math.round(currentTime/1000))*10;


					$( ".p12_change1" ).html(currentDistance+"m");

					$( ".p12_change3" ).html(Math.round(currentTime/1000)+"s");

					totalDis=currentDistance;
					totaltime=Math.round(currentTime/1000);
				}
				count++;
			},
			complete: function(){

				$(".p12_slidezone").slideDown(500,'linear',function(){

						$(".p12_distance2").html(totalDis+ "m");
						$(".p12_time2").html(totaltime+ "s");

						$(".p12_speed3").html(totalDis+ " / "+ totaltime);

						$(".p12_speed4").html((totalDis/totaltime)+"m/s");

						$(".p12_velocity2").html((totalDis/totaltime)+"m/s <font style='color:#fff'>" + data.string.s45_3+"</font>");


						$(".p12_distance1").fadeIn(500,'linear');
						$(".p12_distance1a").delay(300).fadeIn(500,'linear');
						$(".p12_distance2").delay(800).fadeIn(500,'linear');


						$(".p12_time1").delay(1200).fadeIn(500,'linear');
						$(".p12_time1a").delay(1500).fadeIn(500,'linear');
						$(".p12_time2").delay(1800).fadeIn(500,'linear');

						$(".p12_speed1").delay(2200).fadeIn(500,'linear');
						$(".p12_speed1a").delay(2500).fadeIn(500,'linear');
						$(".p12_speed2").delay(2800).fadeIn(500,'linear');
						$(".p12_speed2a").delay(3100).fadeIn(500,'linear');
						$(".p12_speed3").delay(3400).fadeIn(500,'linear');

						$(".p12_speed3a").delay(3700).fadeIn(500,'linear');
						$(".p12_speed4").delay(4000).fadeIn(500,'linear');


						$(".p12_velocity1").delay(4300).fadeIn(500,'linear');
						$(".p12_velocity1a").delay(4600).fadeIn(500,'linear');
						$(".p12_velocity2").delay(4900).fadeIn(500,'linear');


						$("#activity-page-next-btn-enabled").delay(5100).fadeIn(500,'linear');



				});/*.delay(7000).slideUp(500,'linear',function(){

					animate2();
				});*/
			}

	});



}

function animate2()
{
	$(".p12_cycle2").css({ 'left': '57%','top': '0%', 'height': '26%', 'width': '12%'});
	$(".p12_cycle1").hide(0);
	$(".p12_cycle2").show(0);
	$(".p12_slidezone div").hide(0);
	$(".p12_closThis").removeClass('animat1_me');
		$(".p12_closThis").removeClass('animat2_me');

	var currentTime=0, currentDistance=0,count=0;
	var time = 5000;
	var distance = 72;

	var totalDis=0, totaltime=0;

		$(".p12_cycle2").animate({height: '38%',width: '18%',top:'72%',left:'30%',},{
			duration: 5000,
			easing:'linear',
			step: function(now,fx){
				count++;


				if((count%30)==0)
				{
					$(".p12_headlight,.p12_headlight2").hide(0);
				}
				else if((count%100)==0)
				{
					$(".p12_headlight, .p12_headlight2").show(0);
				}


				 if (fx.prop == "top")
				 {





					currentTime = Math.round((now*time)/(distance));

					 currentDistance=(Math.round(currentTime/1000))*5;
					 var test=Math.round(currentTime/1000);
					if(test==0)
					{
						test=0.5;
						currentDistance=2.5;
					}

					$( ".p12_change1" ).html(currentDistance+"m");

					$( ".p12_change3" ).html(test+"s");

					totalDis=currentDistance;
					totaltime=Math.round(currentTime/1000);
				}



			},complete: function(){

				$(".p12_slidezone").slideDown(500,'linear',function(){

						$(".p12_distance2").html(totalDis+ "m");
						$(".p12_time2").html(totaltime+ "s");

						$(".p12_speed3").html(totalDis+ " / "+ totaltime);

						$(".p12_speed4").html((totalDis/totaltime)+"m/s");

						$(".p12_velocity2").html((totalDis/totaltime)+"m/s <font style='color:#000000'>" + data.string.s45_1+"</font>");


						$(".p12_distance1").fadeIn(500,'linear');
						$(".p12_distance1a").delay(300).fadeIn(500,'linear');
						$(".p12_distance2").delay(800).fadeIn(500,'linear');


						$(".p12_time1").delay(1200).fadeIn(500,'linear');
						$(".p12_time1a").delay(1500).fadeIn(500,'linear');
						$(".p12_time2").delay(1800).fadeIn(500,'linear');

						$(".p12_speed1").delay(2200).fadeIn(500,'linear');
						$(".p12_speed1a").delay(2500).fadeIn(500,'linear');
						$(".p12_speed2").delay(2800).fadeIn(500,'linear');
						$(".p12_speed2a").delay(3100).fadeIn(500,'linear');
						$(".p12_speed3").delay(3400).fadeIn(500,'linear');

						$(".p12_speed3a").delay(3700).fadeIn(500,'linear');
						$(".p12_speed4").delay(4000).fadeIn(500,'linear');


						$(".p12_velocity1").delay(4300).fadeIn(500,'linear');
						$(".p12_velocity1a").delay(4600).fadeIn(500,'linear');
						$(".p12_velocity2").delay(4900).fadeIn(500,'linear');


						$("#activity-page-next-btn-enabled").delay(5100).fadeIn(500,'linear');



				});
			}

		});
}

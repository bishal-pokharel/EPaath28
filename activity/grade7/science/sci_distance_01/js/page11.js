

$(function(){
	
	loadTimelineProgress(1,1);	
	$(".p1_title").html(data.string.s15);
	$(".p11_title").html(data.string.s38);
	$(".p11_slidezone").html(data.string.s40);
	$(".p11_replay_btn").html(getReloadBtn());
	
	animate3();	
	

});



function animate3()
{
	
	$(".p11_boat").css({'left':'100%','top':'15%', 'cursor':'pointer'});
	$(".p11_direction").hide(0);
	$(".p11_arrow").hide(0);
	$(".p11_boat").addClass('p11_boat_wave');
	$(".p11_replay_btn").hide(0);
	
	
	var count=0;
	
	$(".p11_boat").animate({'left':'40%','top':'70%'},{
		duration: 10000,
		easing:'linear',
		step: function(now){
			count++;
			if((count%100)==0)
			{
				$(this).css({'background':'url(activity/grade7/science/sci_distance_01/images/p11_6.png) no-repeat'});
			}
			else if((count%120)==0)
			{
				$(this).css({'background':'url(activity/grade7/science/sci_distance_01/images/p11_5.png) no-repeat'});
			}
		},
		complete: function(){
			$(".p11_direction").delay(600).fadeIn(500,'linear',function(){
				$(".p11_arrow").show(0);
			
				$(".p11_slidezone").slideDown(500,'linear',function(){
					
					
					$(".p11_replay_btn").show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				});
			
			});
		 	
		}
	
	});
	
	
	$(".p11_replay_btn").click(function(){
		/*$(".p11_slidezone").slideUp(500,'linear',function(){
				animate3();
		});*/
		location.reload();
		
	});
}


$(function(){
	
	loadTimelineProgress(1,1);	
	$(".p1_title").html(data.string.s49);
	$(".p2_note").html(data.string.s50);
	$(".p14_replay_btn").html(getReloadBtn());
	$(".p14_change0").html(data.string.s58+" : ");
	$(".p14_change2").html(data.string.s57+" : ");
	$(".p14_change4").html(data.string.s59+" : ");
	$(".s56").html(data.string.s56);
	$(".p2_note").hide(0);
	
	
	
	carAnimation();
	
		
	$(".p14_replay_btn").click(function()
	{
		$(".p14_replay_btn").hide(0);
		$(".p14_box div").show(0);
		$(".p14_insidetext").hide(0);
		carAnimation();
	});
});


function carAnimation()
{
	var time = 7000;
	var distance = 100;
	var currentTime = 0;
	
	$(".p14_car").css({'left':'-13%'});
	
	$(".p14_car").animate({'left':distance+'%'},{
			duration: 5000,
			easing:'linear',
			step: function(now,fx){
				
				currentTime = Math.round((now*time)/(distance));
    			
				var val=(Math.round(currentTime/1000))*10;
				var vls=Math.round(currentTime/1000);
				
				if(val<=0)
				{
					val=5;
					vls=0.5;
				}
				$( ".p14_change1" ).html(val+"m");
				
				$( ".p14_change3" ).html(vls+"s");
				
				
				var velo=val/vls;
				
				if(isNaN(velo))
				{
					$( ".p14_change5" ).html("");
				}
				else $( ".p14_change5" ).html(val+" / "+vls+" = "+velo+ "m/s <font style='color:#CCFFFF'>"+data.string.s45_2+"</font>");
			},
			complete: function()
			{
			
				$(".p14_box div").delay(1500).fadeOut(500,'linear',function(){
					$(".p14_insidetext").fadeIn(500,'linear',function(){
					
						$(".p2_note").delay(1000).slideDown(500,'linear');
						$(".p14_replay_btn").show(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					});
				});
				
			}
	});
}
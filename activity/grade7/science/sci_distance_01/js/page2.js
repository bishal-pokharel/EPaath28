

$(function(){

		var arrowbtn=getArrowBtn();
		$("#activity-page-next-btn-enabled").html(arrowbtn);

		var count=0;
		loadTimelineProgress(1,1);
		$(".p1_title").html(data.string.s1);
		$(".p2_note").html(data.string.s3);

		$(".p2_note").hide(0);
		$(".p2_ques1").hide(0);

		$(".s4").html(data.string.s4);


		$(".p2_insidetext").html(data.string.s4);


		var bee1 = $(".bug");
		var main_tl = new TimelineMax({repeat:0,onComplete:showall});


		var tween1=TweenMax.to(bee1, 5, {bezier:{values:[{left:'77%', top:'70%'},{left:'60%', top:'40%'},{left:'52%', top:'30%'},{left:'42%', top:'22%'},{left:'38%', top:'20%'},{left:'28%', top:'20%'},{left:'23%', top:'23%'}]}, ease:Power1.easeInOut});


		main_tl.add(tween1);



		$(".p2_ques1").click(function(){
			$("#p2_slide_add").fadeIn(500,'linear',function(){


				$("#p2_textbox, .p2_insidetext").show(0);
				if(count==0)
				{

					$(".p2_insidetext1").html(data.string.s5a);
					$(".p2_insidetext2").html(data.string.s5b);
					$(".p2_insidetext3").html(data.string.s5c);
					$(".p2_insidetext4").html(data.string.s5d);
					$(".p2_insidetext5").html(data.string.s5);

				}
				else
				{
					$(".p2_insidetext1").html('');
					$(".p2_insidetext2").html(data.string.s9a);
					$(".p2_insidetext3").html('');
					$(".p2_insidetext4").html('');
					$(".p2_insidetext5").html(data.string.s9);
				}

				$(".p2_insidetext1").delay(100).fadeIn(500,'linear');
				$(".p2_insidetext2").delay(400).fadeIn(500,'linear');
				$(".p2_insidetext3").delay(700).fadeIn(500,'linear');
				$(".p2_insidetext4").delay(1000).fadeIn(500,'linear');
				$(".p2_insidetext5").delay(1300).fadeIn(500,'linear');
				$('#activity-page-next-btn-enabled').delay(1600).fadeIn(500,'linear');
			});

		});// click


		$('#activity-page-next-btn-enabled').click(function(){


			$("#p2_slide_add").fadeOut(500,'linear',function(){

				$("#p2_slide_add div, #p2_textbox div, .p2_ques1").hide(0);

				$(".p2_note").fadeOut(500,'linear',function(){


					if(count==0)
					{
						count++;
						$(".p2_note").html(data.string.s7);
						$(".point2, .point4").hide(0);
						$(".bug_arrow").show(0);

						$(".p2_note").fadeIn(500,'linear',function(){
							$(".p2_scale").delay(1000).fadeIn(500,'linear',function(){
								$(".p2_ques1").show(0);
							});

						});

					}
					else
					{

						$(".p2_note").html(data.string.s3);



						ole.footerNotificationHandler.pageEndSetNotification();
					}

				});//fadeout



			});//slidedown



		});//click


		$(".p2_replay").click(function(){
			count=0;$(this).hide(0);
				$(".point2, .point4").show(0);

				$(".bug_arrow, .p2_scale").hide(0);

				main_tl.restart();
		});//click



	function showall()
	{



			$(".bug_path").show(0);
			$(".p2_note").fadeIn(500,'linear',function(){
				$(".p2_ques1").show(0);
			});

	}
});



$(function(){

		var arrowbtn=getArrowBtn();
		// $("#activity-page-next-btn-enabled").html(arrowbtn);
		loadTimelineProgress(1,1);
		$(".p1_title").html(data.string.s1);

		$(".p1_note1").html(data.string.s2);
		$(".p1_note").hide(0);

		$(".p1_text1").html(data.string.s2a);
		$(".p3_text1").html(data.string.s8);

		$("#activity-page-next-btn-enabled").show(0);

		//$("#activity-page-next-btn-enabled").html(data.string.s1b);

		var countMe=0;

		var testArray = [0,1,2];
		var NewArry=Shuffle(testArray);
		var newval=NewArry[countMe];


		var patharray=Array();

		patharray[0]=[{left:'15%', top:'45%'},{left:'25%', top:'35%'},{left:'35%', top:'35%'},{left:'48%', top:'43%'},{left:'53%', top:'49%'},{left:'63%', top:'53%'},{left:'70%', top:'49%'},{left:'72%', top:'48%'},{left:'85%', top:'17%'}];


		 patharray[1]=[{left:'15%', top:'66%'},{left:'25%', top:'68%'},{left:'40%', top:'65%'},{left:'47%', top:'59%'},{left:'70%', top:'69%'},{left:'75%', top:'65%'},{left:'85%', top:'19%'}];

		patharray[2]=[{left:'22%', top:'82%'},{left:'28%', top:'85%'},{left:'45%', top:'85%'},{left:'50%', top:'80%'},{left:'62%', top:'70%'},{left:'69%', top:'60%'},{left:'73%', top:'50%'},{left:'77%', top:'40%'},{left:'85%', top:'19%'}];

		var bee1 = $(".p1_bee");
		var main_tl = new TimelineMax({repeat:0, onComplete:showAll});


		 var tween1=TweenMax.to(bee1, 5, {bezier:patharray[newval], ease:Power1.easeInOut});
		main_tl.add(tween1);



		$(".p1_replay").click(function()
		{
			$(this).hide(0);
			$(".p1_divpath, .p1_text1, .p3_divpath, .p3_text1,.p1_divpath2, .p1_divpath3").hide(0);
			$(".p1_note1").html(data.string.s2);
			$("#activity-page-next-btn-enabled").show(0);
			$(".p1_note").hide(0);


			if(countMe==0)
			{
				countMe++;
				$(".p1_bee").css({'left': '9%','top': '64%'});

				newval=NewArry[countMe];

				var main_t2 = new TimelineMax({repeat:0, onComplete:showAll});
				var tween2=TweenMax.to(bee1, 5, {bezier:patharray[newval], ease:Power1.easeInOut});


				main_t2.add(tween2);
			}
			else if(countMe==1)
			{

				countMe++;
				$(".p1_bee").css({'left': '9%','top': '64%'});


				newval=NewArry[countMe];
				var main_t3 = new TimelineMax({repeat:0, onComplete:showAll});
				var tween3=TweenMax.to(bee1, 5, {bezier:patharray[newval], ease:Power1.easeInOut});


				main_t3.add(tween3);
			}
			else
			{

				countMe=0;

				 newval=NewArry[countMe];
			 	main_tl.restart();
			}
		});


		function showAll()
		{

			if(newval==0)
			{
				$(".p1_divpath").fadeIn(500,'linear');
				$(".p1_text1").css({'top': '30%'});
			}
			else if(newval==1)
			{
				$(".p1_divpath2").fadeIn(500,'linear');
				$(".p1_text1").css({'top': '70%'});
			}
			else if(newval==2)
			{
				$(".p1_divpath3").fadeIn(500,'linear');
				$(".p1_text1").css({'top': '87%'});
			}


			$(".p1_text1").fadeIn(500,'linear');
			$(".p1_note").delay(500).slideDown(500,'linear');
		}

		$("#activity-page-next-btn-enabled").click(function(){

			$(".p1_note").slideUp(500,'linear',function(){
				$(".p3_divpath").fadeIn(500,'linear');
				$(".p3_text1").fadeIn(500,'linear');
				$(".p1_note1").html(data.string.s6);
				$("#activity-page-next-btn-enabled").hide(0);
			}).delay(1000).slideDown(500,'linear',function(){



				ole.footerNotificationHandler.pageEndSetNotification();

			});

		});





});

function Shuffle(o) {
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};


$(function(){

	loadTimelineProgress(1,1);
	var moveObject = $(".p10_car");
	$(".p10_slideDown").html(data.string.p10_slideDown);
	var t_duration=15;
	var count=0;

	$(".p1_title").html(data.string.s15);
	$(".p2_ques_title").html(data.string.s16);
	$(".p10_replay_btn").html(getReloadBtn());
	$(".p10_replay_btn").hide(0);

	var main_t2,tween1;

	var $newarr=['p10_7.png','p10_8.png','p10_9.png','p10_10.png','p10_11.png'];
	var $newarr2=['p10_2.png','p10_4.png','p10_5.png','p10_3.png','p10_2.png'];
	var $newarr3=[data.string.s28,data.string.s29,data.string.s30,data.string.s31,data.string.s32];
	var $newarr4=[data.string.s34,data.string.s35,data.string.s36,data.string.s37,data.string.s34];

	var $newarr5=["p10_directionImage","p10_directionImage2","p10_directionImage3","p10_directionImage4","p10_directionImage"];
	var $newarr6=["p10_divtext","p10_divtext2","p10_divtext3","p10_divtext4","p10_divtext"];



	var $data;

	var source,template,html;

	$(".p10_car").click(function()
	{

		if($(".p10_car").hasClass('car_tweening'))
		{

			main_t2.restart();
			//click not allowed
		}
		else
		{
			if($(".p10_car").hasClass('car_tweening2'))
			{
				main_t2.restart();
			}
			else
			{
				$(".p10_slideDown").hide(0);
				$(".p10_car").addClass('car_tweening');
				$(".p10_car").addClass('car_tweening2')
				main_t2 = new TimelineMax({repeat:0,onUpdate:updateFn, onComplete:showBtn});



				tween1=TweenMax.to(moveObject, t_duration, {bezier:{values:[{left:'48%', top:'19%'},{left:'60%', top:'20%'},{left:'68%', top:'25%'},{left:'75%', top:'32%'},{left:'81%', top:'42%'},{left:'81%', top:'45%'},{left:'81%', top:'52%'},{left:'81%', top:'62%'},{left:'75%', top:'70%'},{left:'65%', top:'77%'},{left:'45%', top:'80%'},{left:'25%', top:'74%'},{left:'15%', top:'62%'},{left:'12%', top:'52%'},{left:'13%', top:'42%'},{left:'24%', top:'27%'},{left:'35%', top:'21%'}], autoRotate:true},ease:Linear.easeNone});

				main_t2.add(tween1);
				ole.footerNotificationHandler.pageEndSetNotification();
			}


		}

	});
	function updateFn()
	{


		var leftval=parseInt(moveObject[0].style.left);
		var topval=parseInt(moveObject[0].style.top);

		$popUp = $(".popUp");



		if(leftval==48 && topval==18 && count==0)
		{


			$data = {	title : data.string.s26,
						textme: $newarr3[count],
						images1: "activity/grade7/science/sci_distance_01/images/"+$newarr[count],
						images2: "activity/grade7/science/sci_distance_01/images/"+$newarr2[count],
						dirtext: $newarr4[count],
						p10_directionImage:$newarr5[count],
						p10_divtext:$newarr6[count]
			}

			count++;
			main_t2.pause();
			source   = $("#my-template").html();
			template = Handlebars.compile(source);
			html = template($data);

			$popUp.html(html);
			$("#p10_pop_clo").show(0);



			$popUp.fadeIn(10,function(){
				$("#p10_pop_clo").html(getCloseBtn());

			});


		}

		else if(leftval==81 && topval==45 && count==1)
		{

			$data = {	title : data.string.s26,
						textme: $newarr3[count],
						images1: $ref+"/images/"+$newarr[count],
						images2: $ref+"/images/"+$newarr2[count],
						dirtext: $newarr4[count],
						p10_directionImage:$newarr5[count],
						p10_divtext:$newarr6[count]
					}
			count++;

			main_t2.pause();


			source   = $("#my-template").html();
			template = Handlebars.compile(source);
			html = template($data);

			$popUp.html(html);
			$("#p10_pop_clo").show(0);
			$popUp.fadeIn(10,function(){
				$("#p10_pop_clo").html(getCloseBtn());

			});



		}
		else if(leftval==48 && topval==79 && count==2)
		{

			$data = {	title : data.string.s26,
						textme: $newarr3[count],
						images1: $ref+"/images/"+$newarr[count],
						images2: $ref+"/images/"+$newarr2[count],
						dirtext: $newarr4[count],
						p10_directionImage:$newarr5[count],
						p10_divtext:$newarr6[count]
			}
			count++;

			main_t2.pause();

			source   = $("#my-template").html();
			template = Handlebars.compile(source);
			html = template($data);

			$popUp.html(html);
			$("#p10_pop_clo").show(0);
			$popUp.fadeIn(10,function(){
				$("#p10_pop_clo").html(getCloseBtn());
			});



		}
		else if(leftval==12 && topval==52 && count==3)
		{
			$data = {	title : data.string.s26,
						textme: $newarr3[count],
						images1: $ref+"/images/"+$newarr[count],
						images2: $ref+"/images/"+$newarr2[count],
						dirtext: $newarr4[count],
						p10_directionImage:$newarr5[count],
						p10_divtext:$newarr6[count]
			}
			count++;

			main_t2.pause();

			source   = $("#my-template").html();
			template = Handlebars.compile(source);
			html = template($data);

			$popUp.html(html);
			$("#p10_pop_clo").show(0);
			$popUp.fadeIn(10,function(){
				$("#p10_pop_clo").html(getCloseBtn());
			});



		}



		$("#p10_pop_clo").click(function(){

			$(".popUp").empty().fadeOut();
			main_t2.resume();


		});
	}//function  update


	function showBtn()
	{
		$popUp = $(".popUp");


		$data = {	title : data.string.s26,
						textme: $newarr3[4],
						images1: $ref+"/images/"+$newarr[4],
						images2: $ref+"/images/"+$newarr2[4],
						dirtext: $newarr4[4],
						p10_directionImage:$newarr5[4],
						p10_divtext:$newarr6[4]
					}
		source   = $("#my-template").html();
		template = Handlebars.compile(source);
		html = template($data);

		$popUp.html(html);
		$(".closeNewBtn").show(0);

		$(".s33").html(data.string.s33);
		$(".s33a").html(data.string.s33a);

		$(".s33").show(0);
		$(".s33a").show(0);


		$popUp.fadeIn(10,function(){
			$("#p10_pop_clo").html(getCloseBtn());
		});




		$("#p10_pop_clo").click(function(){
			$(".popUp").empty().hide(0);
			$(".p10_car").removeClass('car_tweening');

			$(".p10_replay_btn").show(0);
			ole.footerNotificationHandler.pageEndSetNotification();

		});
	}//showbtn


	$(".p10_replay_btn").click(function(){

				location.reload();

	});

});

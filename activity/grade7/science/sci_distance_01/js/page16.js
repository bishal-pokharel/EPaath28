
var countThis=1;
$(function(){


		$(".p1_title").html(data.string.s63);

		var whatnext= getSubpageMoveButton($lang,"next");

		$("#activity-page-next-btn-enabled").append(whatnext);

		$(".p16_replay_btn").html(getReloadBtn());


		var lessoncount=1;
		var lastlesson=3; //totalnumber of lessons



		startlesson(lessoncount,lastlesson);

		/*$(".p16tabs").click(function(){

				var thisId=$(this).attr('id');
				var countThis=1;

				$(".p16tabs").removeClass('p16tabsactive');
				$(".ans_path").removeClass('disableClick');
				$("#opt1").removeClass('p16_ansactive');

				$(".p16_ans").hide(0);
				$("#p16_correct").hide(0);
				$("#p16_incorrect").hide(0);
				$("#p16_hint").hide(0);

				if(thisId=="tab_1")countThis=1;
				else if(thisId=="tab_2")countThis=2;
				else if(thisId=="tab_3")countThis=3;

				startlesson(countThis,lastlesson);
		});
		*/



		$("#activity-page-next-btn-enabled").click(function(){
			$(this).hide(0);
			var thisId=$(this).attr('id');
				countThis++;

				$(".p16tabs").removeClass('p16tabsactive');
				$(".ans_path").removeClass('disableClick');
				$("#opt1").removeClass('p16_ansactive');

				$(".p16_ans").hide(0);
				$("#p16_correct").hide(0);
				$("#p16_incorrect").hide(0);
				$("#p16_hint").hide(0);



				startlesson(countThis,lastlesson);
		});



		$(".ans_path").click(function(){

			var whatisIt=$(this).attr('isIt');

			Correction(whatisIt);
	});


	$('.p16_animation').on('click',".p14_replay_btn",function(){
		startlesson(3,3);
	});

	$(".p16_replay_btn").click(function(){
		location.reload();
	});

});

function startlesson(lessoncount,lastlesson )
{

	loadTimelineProgress(lastlesson,lessoncount);

	$("#tab_"+lessoncount).addClass('p16tabsactive');

	$(".p16_animation").empty();
	$(".p16_ques").html(data.string['s64_ques_'+lessoncount]);
	$("#p16_hint").html(data.string['s64_hint_'+lessoncount]);

	$("#opt1").html(data.string['s64_ans_'+lessoncount+"_a"]);
	$("#opt2").html(data.string['s64_ans_'+lessoncount+"_b"]);

	$('.p16_animation').load($ref+"/page16_"+lessoncount+".html");



}

function Correction(showright)
{

	if(!$(".ans_path").hasClass('disableClick'))
	{

		if(showright=="right")
		{
			$("#p16_correct").show(0);
			$("#p16_incorrect").hide(0);

			$("#p16_hint").hide(0);
			$(".ans_path").addClass('disableClick');

			$("#opt1").addClass('p16_ansactive');

			//alert(countThis);
			if(countThis<3)
			{
				$("#activity-page-next-btn-enabled").show(0);
				$(".p16_replay_btn").hide(0);
			}
			else if(countThis>=3)
			{
				$(".p16_replay_btn").show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			}


		}
		else
		{
			$("#p16_incorrect").show(0);
			$("#p16_correct").hide(0);
			$("#p16_hint").show(0);

		}
	}
}



$(function(){

	loadTimelineProgress(1,1);
	$(".p1_title").html(data.string.s15);
	$(".p2_ques_title").html(data.string.s16);




	var moveObject = $(".p8_car");

	var t_duration=10;

	var duration_val=6;

	var main_t2 = new TimelineMax({repeat:0, onComplete:calculate});

	$( "#slider" ).slider({
		value:6,
		min: 3,
		max: 12,
		step: 3,
		slide: function( event, ui ) {
				duration_val=ui.value;
			main_t2.duration( ui.value).restart();
			$(this).slider('disable');
		}
	});
	var tween1=TweenMax.to(".p8_car", t_duration, {bezier:{values:[{left:'60%', top:'20%'},{left:'70%', top:'25%'},{left:'78%', top:'32%'},{left:'83%', top:'42%'},{left:'84%', top:'52%'},{left:'81%', top:'62%'},{left:'75%', top:'70%'},{left:'65%', top:'77%'},{left:'45%', top:'80%'},{left:'25%', top:'74%'},{left:'15%', top:'62%'},{left:'13%', top:'52%'},{left:'14%', top:'42%'},{left:'24%', top:'27%'},{left:'35%', top:'21%'}], autoRotate:true},ease:Linear.easeNone});
	setTimeout(function(){
		ole.footerNotificationHandler.pageEndSetNotification();
	},t_duration);
	main_t2.add(tween1);



	function calculate()
	{


		$popUp = $(".popUp")

		$htmlval='	';



		$popUp.load('activity/grade7/science/sci_distance_01/page8_pop.html',function(){

			var txtfont='<font  style="color:#000000">'+data.string.s23a+'</font>';

			var txt= data.string.s23b;
			txt='<font  style="color:#000000">'+ txt.replace("YYYYYY", duration_val)+'</font>';


			var txt2= data.string.s23;
			txt2= txt2.replace("ZZZZZZ", txtfont);
			$(".s23").html(txt2+txt);

			$(".calculation1").html("= " + 360 + "/ "+ duration_val);
			var clacu=360/duration_val;
			$(".calculation2").html("= "+ clacu+ " m/s");

			var conc=data.string.s24a;
			conc= conc.replace("YYYYYY", clacu);

			$(".s24a").html(conc);

		});




		$popUp.show(0);
		$( "#slider" ).slider('enable');
	}


	$( "#p8_speed" ).html(data.string.s21);


});

$(function(){


	$("#toDo").html(data.string.e_1);
	$("#repeatId").html(getReloadBtn());

	var whatnext= getSubpageMoveButton($lang,"next");

	// $("#activity-page-next-btn-enabled").append(whatnext);

	var counter=0;
	var totalCounter=10;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);

	//var newarray=shuffleArray(arrayquestion);
	var newarray=arrayquestion;
	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;


	myquestion(newarray[counter],counter);

	$(".allquestion").on("click",".ans",function(){

		if((newarray[counter]==8) || (newarray[counter]==9))
		{
			$("#allhint").show(0);

			$("#showImg").hide(0);
		}

		var isCorrect=parseInt($(this).attr('corr'));

		$("#showAns div").removeClass('ans').addClass('done');

		$("#showAns div[corr='1']").addClass('corrans');

		if(isCorrect==1)
		{
			$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);


			rightcounter++;
		}
		else
		{
			$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);


			wrongCounter++;
		}

		$("#activity-page-next-btn-enabled").fadeIn(10,function(){
			//$(this).addClass('animated shake');
		});

	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0).removeClass('animated shake');
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		} else if (counter>totalCounter) {
			ole.activityComplete.finishingcall();
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);
			// ole.activityComplete.finishingcall();
		}
	});

	$(".allquestion").on("click","#showhint",function(){

		$(".allquestion").find("#hint").toggle();
	});


	$(".allquestion").on("click",".imgseven",function(){

		var whatclk=$(this).attr('id');
		if($(this).hasClass('clickimg'))
		{
			$(".imgseven").removeClass('clickimg');

			$("#imgSpd2").addClass('corrans');
			if(whatclk=="imgSpd2")
			{
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);


				rightcounter++;
			}
			else
			{
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);


				wrongCounter++;
			}


			$("#activity-page-next-btn-enabled").fadeIn(10,function(){
				//$(this).addClass('animated shake');
			});
		}
	});


	$("#repeatId").click(function(){
		location.reload();
	});
});


function myquestion(questionNo,counter)
{
	loadTimelineProgress(11,(counter+1));
	var source   = $("#label-template").html();

	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter);
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#Imgshow").fadeOut(10);

	}).delay(100).fadeIn(10,function(){

		if(questionNo==1)
		{
			$("#img1_1").append("<div id='road2' class='roads'><img src='"+$ref+"/exercise/images/road02.png' /></div><div id='road1'  class='roads'><img src='"+$ref+"/exercise/images/road01.png' /></div>");

			$("#opt_1_1").fadeOut(10);
			$("#opt_1_2").fadeOut(10);

			$(".roads").click(function(){

				$("#quest span").html(data.string.exe_1+"<br/>"+data.string.exe_12);
				var $id=$(this).attr('id');
				//opt_1_1
				if($id=="road2")
				{
					$("#opt_1_1").attr('corr',1);
					$("#opt_1_2").attr('corr',0);
				}
				else
				{
					$("#opt_1_1").attr('corr',0);
					$("#opt_1_2").attr('corr',1);

				}
				$("#opt_1_1").fadeIn(10);
				$("#opt_1_2").fadeIn(10);

				$("#img1_1").html('<img src="'+$ref+'/exercise/images/main.png"/>');
			});

		}

		if(questionNo==7)
		{
			$("#opt_7_1").fadeOut(10);
			$("#opt_7_2").fadeOut(10);
			$("#showAns").css({'height':'auto'});
			$("#showAns").addClass('changeShow');
			$("#showAns").append("<div id='imgSpd' class='imgseven clickimg'><img src='"+$ref+"/exercise/images/cyclying.png' /></div><div id='imgSpd2'  class='imgseven clickimg'><img src='"+$ref+"/exercise/images/cyclying_east_"+$lang+".png' /></div>");
		}
	});
}

function myquestion2(right,wrong)
{
	loadTimelineProgress(11,11);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var $dataval={
		rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:data.string.exe_1, qans: data.string.opt_1_2,quesno:"ques_1"},
			{ques:data.string.exe_2, qans: data.string.opt_2_1,quesno:"ques_2"},
			{ques:data.string.exe_3, qans: data.string.opt_3_1,quesno:"ques_3"},
			{ques:data.string.exe_4, qans: data.string.opt_4_1,quesno:"ques_4"},
			{ques:data.string.exe_5, qans: data.string.opt_5_2,quesno:"ques_5"},
			{ques:data.string.exe_6, qans: data.string.opt_6_2,quesno:"ques_6"},

			{ques:data.string.exe_8, qans: data.string.opt_8_1,quesno:"ques_8"},
			{ques:data.string.exe_9, qans: data.string.opt_9_2,quesno:"ques_9"},
			{ques:data.string.exe_10, qans: data.string.opt_10_2,quesno:"ques_10"}

		]


	};
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.exe3);
		$("#Imgshow").fadeOut(10);


	}).delay(100).fadeIn(10,function(){

		$("#activity-page-next-btn-enabled").delay(100).fadeIn(10);

	});
}

function getQuestion($quesNo,counter)
{
	var quesList;
	var nep=ole.nepaliNumber(counter+1);

	var whatri=whatCorr($quesNo);
	var imgObjA=[];
	var hint,showhint,allhint;
	if($quesNo==1)
	{
		 imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/bg.png"}];
	}
	else if($quesNo==2)
	{
		 imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/car-moving.gif"}];
	}
	else if($quesNo==3)
	{
		 imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/car-donkey.gif"}];
	}
	else if($quesNo==4)
	{
		 imgObjA=[{mul:'smllimg',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/car1_"+$lang+".png"},
		 {mul:'smllimg',imgObj1:"img"+$quesNo+"_2", imgObjval:$ref+"/exercise/images/car1.png"}
		 ];
	}
	else if($quesNo==5)
	{
		 imgObjA=[{mul:'smllimg',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/car-arrow2.png"},
		 {mul:'smllimg',imgObj1:"img"+$quesNo+"_2", imgObjval:$ref+"/exercise/images/car-arrow1.png"}
		 ];
	}
	else if($quesNo==8)
	{
		hint=data.string.opt_8_hint;
		showhint=data.string.exe4;
		 imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/car-calc.png"}
		 ];
	}
	else if($quesNo==9)
	{
		hint=data.string.opt_9_hint;
		showhint=data.string.exe4;

		 imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/horse-"+$lang+".png"}
		 ];

	}
	else if($quesNo==10)
	{
		 imgObjA=[{mul:'smllimg',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/velocity1.png"},
		 {mul:'smllimg',imgObj1:"img"+$quesNo+"_2", imgObjval:$ref+"/exercise/images/velocity2.png"}
		 ];
	}

	if($quesNo>5 && $quesNo!=7 && $quesNo!=10) allhint="yes";

	quesList={
		myQuesion:data.string["exe_"+$quesNo],
		img22:$ref+"/exercise/images/blank.png",
		imgobj:imgObjA,

		hint:hint,
		allhint:allhint,
		optObj:[
				{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"]},
				{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"]}
				]
	};

	return quesList;
}

function whatCorr($quesNo)
{
	var whatis=new Array();
	switch($quesNo)
	{
		case 1: 	whatis[0]=0;	whatis[1]=1;	  	break;
		case 2: 	whatis[0]=1;	whatis[1]=0;		break;
		case 3: 	whatis[0]=0;	whatis[1]=1;	  	break;
		case 4: 	whatis[0]=1;	whatis[1]=0;		break;
		case 5: 	whatis[0]=1;	whatis[1]=0;		break;
		case 6: 	whatis[0]=0;	whatis[1]=1;		break;
		case 7: 	whatis[0]=1;	whatis[1]=0;		break;
		case 8: 	whatis[0]=1;	whatis[1]=0;	 	break;
		case 9: 	whatis[0]=0;	whatis[1]=1;	 	break;
		case 10: 	whatis[0]=0;	whatis[1]=1;		break;
	}
	return whatis;
}

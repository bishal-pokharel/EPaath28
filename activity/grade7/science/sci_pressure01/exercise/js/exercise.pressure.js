(function () {

	var $wahtar=getSubpageMoveButton($lang,"next");

	var $board = $('.board');
	$nextBtn = $("#activity-page-next-btn-enabled"),
	$title = $('.title'),
	animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
	$refImg = $ref+"/exercise/images/";

	$nextBtn.html($wahtar);

	var tortoise = {
		pos : 0,
		initalPos : 5,
		finalPos : 90,
	}

	var question = {
		count : 0,
		range : 10,
		total : 10,
		correct : []
	}
	var randomVals = ole.getRandom(question.range,question.total-1);
	console.log(randomVals);
	$board.html(randomVals)
/*
 _              _   _
| |_ _   _ _ __| |_| | ___
| __| | | | '__| __| |/ _ \
| |_| |_| | |  | |_| |  __/
 \__|\__,_|_|   \__|_|\___|

*/
	function goTurtle (len,nextFunc) {
		// console.log('tortoise.pos= '+tortoise.pos);
		var lastpos = tortoise.pos, i=lastpos;
		// tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(question.count-1)+tortoise.initalPos);
		tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(len-1)+tortoise.initalPos);
		var newGo = setInterval(function () {
			// console.log('lastpos = '+lastpos+" newPos="+tortoise.pos+" i="+i);
			if(i%2===0) {
				$('#tortoise2').show(0);
				$('#tortoise1').hide(0);
			} else {
				$('#tortoise1').show(0);
				$('#tortoise2').hide(0);
			}
			$('.tortoise').animate({'left' : i+"%"},100);
			i++;
			if (i>tortoise.pos) {
				clearInterval(newGo);
				// nextFunc();
			};
		},100);
	}

/*
* turtle out
*/



	var content = [
	{
		type:'',
		question : data.string.q1,
		answers: [
		{ans : data.string.a1_1},
		{ans : data.string.a1_2},
		{ans : data.string.a1_3,
		correct : "correct"},
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q2,
		answers: [
		{ans : data.string.a2_1},
		{ans : data.string.a2_2,
		correct : "correct"},
		{ans : data.string.a2_3},
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q3,
		answers: [
		{ans : data.string.a3_1},
		{ans : data.string.a3_2,
		correct : "correct"},
		{ans : data.string.a3_3}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q4,
		answers: [
		{ans : data.string.a4_1,
		correct : "correct"},
		{ans : data.string.a4_2},
		{ans : data.string.a4_3}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q5,
		answers: [
		{ans : data.string.a5_1},
		{ans : data.string.a5_2},
		{ans : data.string.a5_3,
		correct : "correct"}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q6,
		answers: [
		{ans : data.string.a6_1},
		{ans : data.string.a6_2,
		correct : "correct"},
		{ans : data.string.a6_3},
		],
		img : ''
	},
	{
		type : '',
		question : data.string.q7,
		answers: [
		{ans : data.string.a7_1,
		correct : "correct"},
		{ans : data.string.a7_2},
		{ans : data.string.a7_3}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q8,
		answers: [
		{ans : data.string.a8_1,
		correct : "correct"},
		{ans : data.string.a8_2},
		{ans : data.string.a8_3}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q9,
		answers: [
		{ans : data.string.a9_1},
		{ans : data.string.a9_2},
		{ans : data.string.a9_3,
		correct : "correct"},
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q10,
		answers: [
		{ans : data.string.a10_1},
		{ans : data.string.a10_2,
		correct : "correct"},
		{ans : data.string.a10_3},
		],
		img : ''
	}

	]

	function firstStoryTeller () {
		var source = $('#first-story-template').html();
		var template = Handlebars.compile(source);
		var storyContent = {
			sun : $refImg+"sun.png",
			story : data.string.story,
			enter : data.string.press,
			storyImg1 : $refImg+"first.png",
			storyImg2 : $refImg+"flag.png",
		}
		var html = template(storyContent);
		$board.html(html);
	}

	firstStoryTeller();
	// content[0].question =ole.textSR(content[0].question,'sup2','<sup>2</sup>')
	// console.log(content[0].question);
	function  qA () {
		$title.text(data.string.exerciseTitle).show(0);
		$('.advancement').show(0);
		var counter = randomVals[question.count];

		loadTimelineProgress(10,question.count+1); 
		content[counter].correctImg = $refImg+"correct.png";
		content[counter].wrongImg = $refImg+"wrong.png";
		content[counter].question =ole.textSR(content[counter].question,'sup2','<sup>2</sup>');
			if (content[counter].type==='multipleImg') {
				// alert();
				var source = $('#qAImage-templete').html();
			} else {
				var source = $('#qA-templete').html();
			}

			var template = Handlebars.compile(source);
			var html = template(content[counter]);
			$board.html(html);
	}

	// qA();

/*
* results
*/
	function results () {
		$title.hide(0);
		var len = question.correct.length;
		if(len < question.total) {
			$('.tortoise').animate({

			},600,function () {
				$(this).html("<img src='"+$refImg+"laydown.png' >")
				$(this).css({
					'width' : '20%',
					'bottom' : "-10%",
				})
			})

			$('#flag').css({
				'width' : '15%',
			},1000);
			if(len === 9){
				var endContent = {
					story : data.string.missedOne
				}
			} else if(len === 0){
				var endContent = {
					story : data.string.noTry
				}
			} else if(len <5){
				var endContent = {
					story : data.string.needMore
				}
			} else if(len <9){
				var endContent = {
					story : data.string.someMore
				}
			}
		} else {
			$('.tortoise').html("<img src='"+$refImg+"dance.gif' >").animate({
				'left' : '80%',
				'width' : '15%',
			},1000);
			$('#flag').animate({
				'right' : '35%',
				'width' : '15%',
			},1000);
			var endContent = {
				story : data.string.successStory
			}
		}

		var source = $('#results-template').html();
		var template = Handlebars.compile(source);
		var html = template(endContent);
		$board.html(html);
		$board.find('.endBoard').addClass('animated bounceInDown').one(animationEnd,function () {
			$('.repeat').show(0).addClass('animated bounceInDown');
		});

	}

	$board.on('click','.neutral',function () {
		// console.log("what");
		var $this = $(this);
		var isCorrect = $this.data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right');
			$('.neutral').removeClass('neutral');
			$nextBtn.fadeIn();
			$('.imgCrot .correctImg').show(0);
			question.correct.push(question.count);
			console.log(question.correct);
			var len = question.correct.length;
			goTurtle (len);
		}
		else {
			$this.addClass('wrong')
			$('.neutral').not('.correct').addClass('wrong').removeClass('neutral');
			$('.correct').addClass('right animated  tada');
			$('.neutral').removeClass('neutral');
			$('.imgCrot .wrongImg').show(0);
			$nextBtn.fadeIn();

		}
	})

	$nextBtn.on('click',function () {

		$nextBtn.fadeOut();
		question.count++;

		if(question.count<question.total){
			// goTurtle (qA);
			qA();
		}
		else if (question.count===question.total) {
			// goTurtle(results);
			// alert('finish');
			$nextBtn.fadeIn();
			results();
		} else {
			ole.activityComplete.finishingcall();
		}
	})

	$board.on('click','.storyWrapper .enter',qA);

	$('.repeat').on('click',function () {
		location.reload();
	})

})(jQuery)
$(function(){

	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		quest++;
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);
		if(quest<3){
			getFunction(quest);
		}
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		quest--;
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		getFunction(quest);
	});

});

function getFunction(qno)
{
	loadTimelineProgress(2,qno);
	switch(qno)
	{
		case 1:
		{
			case0();
			break;
		}
		case 2:
		{
			case1();
			break;
		}
	}
}

function case0()
{
	var datavar={
		middle_text: data.lesson.chapter,
		coverpage:$ref+"/images/pressure.png"
	}

	var source   = $("#firstpage-template").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);
	$(".frontpage").html(html);
	$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
}

	function case1()
	{
		$(".frontpage").hide(0);
	var $wahtar=getSubpageMoveButton($lang,"next");
	$(".classroom .next").html($wahtar);

	$("#activity-page-next-btn-enabled").show(0);

	var $bubble1= $(".bubble1");
	var $bubble2 = $(".bubble2");
	$board = $("#board")
	$board.find('#title h1').text(data.string.title);
	$board.find('.force h3').text(data.string.force);
	$board.find('.area h3').text(data.string.area);
	$board.find('.pressure h3').text(data.string.pressure);
	$('.definationPart').text(data.string.pressureDesc);
	$('.definationPart2').text(data.string.pressureSIUnit);

	var boardOut = function () {
						$('.lastMsg').html(data.string.dailyWork);
						$('.lastMsg').delay(3000).fadeIn(100);
	}

	var fillLog = function () {

	}

	var nextCount =0;

	$("#activity-page-next-btn-enabled").on('click',function () {
		if (nextCount===0) {
			bubble1func();
		} else if (nextCount===1) {
			bubble2func();
		}
	})

	function bubble1func () {
		$bubble1.hide(0);
		$bubble2.find('span').html(data.string.dialog2);
		$bubble2.show(0);
		nextCount = 1;
		$("#activity-page-next-btn-enabled").show(0);
	}

	function bubble2func () {
		$bubble2.fadeOut(100,function(){
				$("#teacher").fadeOut(400);
				$board.delay(400).animate({
					width : "100%",
					height : "100%"
				},2000,function(){
					// $('.desc').find('img').hide(0);
					$('.logOnBoard').attr('src',$ref+"/images/log_final.png");
					$('.supply').show(0)

					// ole.stringShow(".definationPart",data.string.pressureDesc, true, function(){
					$('.definationPart').animate({
						"margin-left" : "0%"
					},1000,function () {
						$('.definationPart2').animate({
						"margin-left" : "0%"
					},1000, function () {

						ole.parseToolTip(".definationPart2","SI",data.string.siUnit);
						ole.parseToolTip(".definationPart2",data.string.newtonTerm,data.string.newton);
						ole.footerNotificationHandler.pageEndSetNotification();
						boardOut();

					});


					})

				});
			});
	}

		setTimeout(function () {
			$bubble1.find('span').html(data.string.dialog1);
			$bubble1.show(0);
			$(".next").show(0);
		},1000);
}

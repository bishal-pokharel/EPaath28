(function  () {
	loadTimelineProgress(1,1); 
	
	$ul = $(".board").find('ul');
	console.log($ul);
	$values = ['now','if','f','a','p','dp'];

	$('#force').text(data.string.force);
	$('#area').text(data.string.area);
	$('#pressure').text(data.string.pressure);
	
	var count = 0;
	var displayLi = setInterval(function  () {
		$datasLi = "<li>"+data.string[$values[count]]+"</li>"
		$ul.append($datasLi);
		count++;
		console.log($datasLi);
		console.log(count);
		if (count === $values.length) {
			clearInterval(displayLi);
			ask1();
		};
	},1000);

	var ask1 = function () {
		setTimeout(function () {
			$bubble = $("#firstbubble");
			$bubble.find('.textFront').text(data.string.textFront1);
			$bubble.find('.textBack').text(data.string.textBack1);
			$bubble.show(0);
		},2000)	
	}
	$bubble1 = $('#firstbubble');
	$bubble1.on('click','.choice',function () {
		var $getData= $(this).data('choice');
		if ($getData=== "f") {
			 $(".board").find('ul  li').last().html("<span style='color: yellow;'>"+data.string.correct+"</span> "+data.string.pfRel)
			 $bubble1.hide(0);
			 setTimeout(function  () {
			 	ask2();	
			 },1000)
		}
		else {
			$bubble1.find('h3').text(data.string.wrong);
		}
	});

	$bubble2 = $('#secondbubble');
	var ask2 = function  () {
		$ul.append("<li>"+data.string.ip+"</li>")
		setTimeout(function  () {
			$bubble2.find(".textBack").text(data.string.textBack2);
			$bubble2.show(0);
		},2000);
	}

	$bubble2.on('click','.choice',function () {
		var $getData= $(this).data('choice');
		if ($getData=== "a") {
			 $(".board").find('ul  li').last().html("<span style='color: yellow;'>"+data.string.correct+"</span> "+data.string.paRel)
			 $bubble2.hide(0);
			 setTimeout(function  () {
			 	conclusion();
			 },2000)
			 
		}
		else {
			$bubble2.find('h3').text(data.string.wrong);
		}
	});

	function conclusion() {
		var $values = ['so','PAFrel','where','f','a','p']
		$ul.html("");
		var count = 0;
		$('.picture').animate({
			// "margin-top": "10%"
		},1000)
		// ole.stringShow($ul,data.string.fullpfaRel,function () {
		$ul.html(data.string.fullpfaRel);
			var displayLi = setInterval(function  () {
			$datasLi = "<li>"+data.string[$values[count]]+"</li>"
			$ul.append($datasLi);
			count++;
			console.log($datasLi);
			console.log(count);
			if (count === $values.length) {
				clearInterval(displayLi);
				ole.footerNotificationHandler.pageEndSetNotification();
			};
			},1000);
		
	}

	function popDesc (name) {
		setTimeout(function  () {
			name = name + "Defn";
			$dataDefn = data.string[name];

		},2000);
		
	}
})(jQuery);
(function ($) {

	loadTimelineProgress(1,1);



	// console.log("hellow for cloud");
	var goNextCount =0;
	var $data ="";
	count = 0;
	$popUp = $('.cloudFull .popUp');
	$cloud = $('.cloud');
	$bubble = $('.bubble');

	$bubble.html(data.string.why).delay(100).fadeIn(100,function(){
		sideText();
	});

	/**
	* title
	*/
	$('.title').text(data.string.paRelation);

	/**
	* sidetext funtion actually made to slide from side but :P
	* it is shown in middle
	*/
	$sidetext =$(".sidetext");
	var sideText = function () {
			$cloud.find('.objects').delay(10000).fadeIn(100,function(){
				$bubble.hide(0);

				$('.teacher').delay(1000).fadeOut(500,function () {
					$('.cloud').css({
						width: "100%",
						height: "100%"
					})
					$(".cloudFull").css({
						background : "white",
						border : "10px solid #235DA0"
					})
				})

				$sidetext.html(data.string.sideText).delay(400).animate({
				"margin-right": "20%",
				}, 5000);
			});
	}

	// console.log(data.string.why);
	var valueOfClick = "";
	var image = new Image();
	$cloud.on('click',".objects",function  () {
		firstPopUp($(this));
	});

	/**
	* first popUp
	*/

	function firstPopUp ($this) {

		var $wahtar=getArrowBtn();


		var timestamp = new Date().getTime();
		valueOfClick = $this.data('value');
		$question = data.string[valueOfClick+"Q"];
		$image = $ref+"/images/gif/"+valueOfClick+".gif?"+timestamp;
		image.src = $image;
		var source = $('#pop-template1').html();
		var template = Handlebars.compile(source);

		var $data = {
			id1 : valueOfClick,
			forceCenter : data.string.forceCenter,
			forceOver : data.string.forceOver,
			areaD1 : data.string.areaD1,
			pressureD1: data.string.pressureD1,
			areaD2 : data.string.areaD2,
			pressureD2 : data.string.pressureD2,
			question : $question,
			image : $image,
			gonext:$wahtar
		}

		goNextCount=1;

		var html = template($data);
		$popUp.html(html).show(0);
		$popUp.find('.popCenter').delay(100).fadeIn(100,function () {
			// ole.vCenter(".popCenter");
		});

		showPointsTime(valueOfClick);
	}

	/**
	* showPointsTime = show datas on arrow head acc to time counted by me on hands lol
	*/
	var showPointsTime = function (argument) {
		$popUp.find('.descc').hide(0);
		var $time = 0;
		switch (valueOfClick) {
			case "pin":
			$time = 3100;
			break;

			case "tractor":
			$time = 10000;
			break;

			case "log":
			$time = 2000;
			break;

			case "halo":
			$time = 6000;
			break;

			case "sandel":
			$time = 7000;
			break;

			case "":
			$time = 0;
			break;
		}

		setTimeout(function () {
			$popUp.find('.descc').show(0);
			$popUp.find('.goNext').addClass('blinky');
		},$time);
	}

	$popUp.on('click','.repeat',function () {
		showPointsTime(valueOfClick);
		var timestamp = new Date().getTime();
		var $src = image.src+'?'+timestamp;
		$popUp.find('.image img').attr("src",$src)
	})

	$popUp.on('click','.goNext',function () {
		if (goNextCount=== 1) {
			secondPop();
		}
		else if (goNextCount===2) {
			thirdPop();
		}

	})

	function secondPop () {
		var $wahtar=getArrowBtn();
		var $this = valueOfClick;
		$question = data.string[$this+"Q"];
		$answer = data.string.allAnswers;
		$image = $ref+"/images/"+$this+"_area.png";
		var $data = {
			id1 : valueOfClick,
			question : $question,
			answer : $answer,
			image : $image,
			gonext:$wahtar
		}

		var source = $('#pop-template3').html();
		var template = Handlebars.compile(source);

		var html = template($data);
		$popUp.html(html).show(0);
		$popUp.find('.popCenter').delay(100).fadeIn(100,function () {
			// ole.vCenter(".popCenter");
		});

		setTimeout(function () {
			$popUp.find('.template3Pic').css({
			'visibility': 'visible'
			});
			$popUp.find('.answer').delay(400).fadeIn();
		},700);
		goNextCount = 2;
	}

	function thirdPop () {
		var $this = valueOfClick;
		$question = data.string[$this+"Q"];
		$answer = data.string[$this];
		$image = $ref+"/images/"+$this+".gif";

		var source = $('#pop-template2').html();
		var template = Handlebars.compile(source);

		var $data = {

			question : $question,
			answer : $answer,
			image : $image
		}

		var html = template($data);
		$popUp.html(html).show(0);
		$popUp.find('.popCenter').delay(100).fadeIn(100,function () {
			// ole.vCenter(".popCenter");
		});
		count++;
	}

	$popUp.on('click','.closebtn',function () {
		$popUp.hide(0);
		if (count >= 5) {
			$bubble.hide(0);
			setTimeout(function () {
				$sidetext.hide(0);
				$bubble.html(data.string.conclusionSay).show(0);
			},1000);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

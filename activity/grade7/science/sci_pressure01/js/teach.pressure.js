(function ($) {

	loadTimelineProgress(1,1);

	var area=0,force=0,animal=0,wood=0, checkSlide=0 ;
	// var $".calculation");
	var countAnimalClick = 0, countLogClick=0;
	var $listArea = $('.listArea')
	var $bench = $(".bench"), $animal = $(".animal");
	$('.question h1').text(data.string.learnTitle);
	var $popUp=$('.maths .popUp');
	var $lineBox = $('.lineBox');
	var pressureShow = 0;

	//mesg parts
	$('.help').text(data.string.instruction);
	$('.bubble').text(data.string.funnySay);
	$('.msgForce').find('p').text(data.string.msgForce);
	$('.msgArea').find('p').text(data.string.msgArea);
	$('.msgMath').find('p').text(data.string.msgMath);
	$('.msgAreaPressure').find('p').text(data.string.msgAreaPressure);
	$lineBox.find('.active').text(data.string.activeArea);

	/**
	* help function... first popUp to show what to do
	*/
	var help = function () {
		var helpData = {
			title : data.string.learnTitle,
			formula : data.string.lformula,
			imgLog : $ref+"/images/wood_1.png",
			pLog1 : data.string.pLog1,
			pLog2 : data.string.pLog2,
			pAnimal : data.string.pAnimal,
			imgAnimal : $ref+"/images/animal_1.png",
			pNote : data.string.pNote,
			pMath : data.string.pMath
		}

		var source   = $("#pop-template").html();
		var template = Handlebars.compile(source);
		var html    = template(helpData);
		$popUp.html(html).show(0);
	}
	help(); // call the help

	$('.help').on('click',function () {
		help();
	})

	/**
	* close help
	*/
	$popUp.on('click','.closeHelp',function () {
		$popUp.hide(0);
	})

	/**
	* pressure calculation and changes
	*/
	var pressure = function($this) {
		if (countAnimalClick=== 0) {
			// $('.msgMath').show(0);
			$('.msgAreaPressure').show(0);
			countAnimalClick++;
		} else {
			$('.msgMath').hide(0);
		}

		if(pressureShow > 5){
			var $conclusion = data.string.conclusionTeach;
			// $popUp.show(0).find('.popPressure').show(0);
			var conc = {
				conclusion : $conclusion
			}

			var source   = $("#conclusion-template").html();
			var template = Handlebars.compile(source);
			var html    = template(conc);
			$popUp.find('.popPressure').html(html);
			$popUp.show(0).find('.popPressure').show(0);
			ole.footerNotificationHandler.lessonEndSetNotification();
		}

		pressureShow++;
		console.log(pressureShow);
		$('.pressure').show(0);
		$('.pressure-ans').css({
			"color" : "#B800F6"
		}).delay(2000).animate({
			"color" : "black"
		},500);
		$('.mainImg').find('.bubble').hide(0);
		var p =Math.floor(force/area * 1000) / 1000;

		$step1 = "&nbsp; &nbsp; = "+force+"/"+area;

		var $data1 = {
			force1 : force,
			area1 : area,
			step1 : $step1,
			ans : "&nbsp; &nbsp; = "+p + " pascal "
		}

		var source   = $("#calculation-template").html();
		var template = Handlebars.compile(source);
		var html    = template($data1);
		$('.calculation').find('ul').html(html);

		$('.pressure-ans').html("P = "+ p+" Pascal");

			var speed = 600;
			if (wood===1) {
				var slidedown = 100/21000*p;
			}
			else {
				var slidedown = 100/15000*p;
			}

			$('.mainBox').off('click');
			$animal.delay(speed).stop(true,false).animate({
				bottom: "-="+slidedown
			},1000)
			$bench.delay(speed).stop(true,false).animate({
				bottom: "-="+slidedown
			},1000)
	}

	/**
	* mouse hover on wooden log
	*/

	$listArea.on('mouseenter','li',function(){
		var area = $(this).data('area');
		var $width,$height;
		switch (area) {
			case 5:
			$width = "50%",
			$height="20%"
			$sideMsg = data.string.smallArea;
			$area = "A = 5m<sup>2</sup>";
			break;

			case 10:
			$width= "70%";
			$height= "40%";
			$sideMsg = data.string.bigArea;
			$area = "A = 10m<sup>2</sup>";

			break;
		}

		$lineBox.find('.side').text($sideMsg);
		$lineBox.find('.boxArea').css({
			width: $width,
			height: $height
		})
		$lineBox.find('.area').html($area);

		$('#line').show(0);
		$lineBox.show(0);
	}).on('mouseleave',function () {
		$('#line').hide(0);
		$lineBox.hide(0);
	})


	/**
	* area images
	*/

	$listArea.on('click','li',function(){
		if (countLogClick===0) {
			$('.msgArea').hide(0);
		};
		area = $(this).data('area');
		$animal.removeAttr("style");
		$bench.removeAttr("style");
		var $img
		switch (area) {
			case 5:
			wood=0;
			$img = $ref+"/images/wood_1.png";
			break;

			case 10:
			wood=0;
			$img = $ref+"/images/wood_4.png";
			break;
		}

		$bench.html("<img src="+$img+" width='100%'>");
		$('.areaMsg').html("A = "+area+" m<sup>2</sup>");
		$('.areaMsg').css({
			"color" : "#B800F6",

		}).delay(2000).animate({
			"color" : "black"
		},500);

		/**
		* animal jump try
		*/
		$animal.stop(true,false).animate({
			"bottom" : "55%",
			"transform" : "rotate(90deg)"
		},500).animate({
			bottom: "47%",
			"transform" : "rotate(90deg)"
		},400).animate({
			bottom: "50%"
		},100).animate({
			bottom: "47%"
		},100,function () {
			if(area!=0 && force!=0) {
			pressure($(this));
			}
		});


	})

	/**
	*	list of force being clicked
	*/

	$('.listForce').on('click','li',function(){
		if(countAnimalClick===0){
			$('.msgForce').hide(0);
		}
		$animal.removeAttr("style");
		$bench.removeAttr("style");
		force = $(this).data('force');
		$('.force').html("F = "+force+ " N");
		$('.force').css({
			"color" : "#B800F6"
		}).delay(2000).animate({
			"color" : "black"
		},500);
		var $img,$width,$margin;
		switch (force) {
			case 10000:
			animal= 1;
			$img = $ref+"/images/animal_1.png";
			$width = "100%";
			$margin1 = "0";
			break;

			case 3000:
			animal= 2;
			$img = $ref+"/images/animal_2.png";
			$width= "80%";
			$margin1 = "10%";
			break;

			case 100:
			animal= 3;
			$img = $ref+"/images/animal_3.png";
			$width= "60%";
			$margin1="20%";
            ole.footerNotificationHandler.lessonEndSetNotification();
            break;
		}

		$animal.find('.fAnimal').html("<img src="+$img+" width='"+$width+"' style='margin-left:"+$margin1+";'>");
		$animal.hide(0).css({
			bottom : "55%"
		}).show(0).stop(true,false).animate({
			bottom: "40%"
		},400,function () {
			if(area!=0 && force!=0) {
				pressure($(this));
			}
			else if (area=== 0) {
				$('.mainImg').find('.bubble').show(0);
			}
		});


	});

	/**
	* revert wooden log
	*/

	$('.revert').on('click',function () {
		var $attr = $listArea.find('li').data('area');
		console.log("val = "+$attr);
		// console.log("value of attr = "+$listArea.find('li').data('attr'));
		if ($attr === 5) {
			$attr = 10;
			var $img = $ref+ "/images/box_2.gif";
		} else {
			$attr = 5;
			var $img = $ref+ "/images/box_1.gif";
		}
		$listArea.find('li').data('area',$attr);
		$listArea.find('img').attr('src',$img);

	});

	$popUp.on('click','.repeat',function () {
		location.reload();
	})

})(jQuery);

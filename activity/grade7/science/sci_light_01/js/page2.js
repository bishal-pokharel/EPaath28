
$(document).ready(function()
{

	var whatnext= getSubpageMoveButton($lang,"next");

	$("#activity-page-next-btn-enabled").append(whatnext);

	$(".gameTitle").html(data.string.s3);
	$("#lightbtn").html(data.string.s4c);
	$("#nolightbtn").html(data.string.s4d);

	$("#startbtn").html(data.string.p_1);
	$(".scorebtn").html(data.string.p_1);
	$("#restartbtn").html(getReloadBtn());

	var counter=0;
	var totalCounter=13;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10,11,12,13);

	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;
	loadTimelineProgress(14,(counter+1));


	$("#startbtn").click(function(){

		$(this).hide(0);
		myquestion(newarray[counter],counter);
	});



	$(".answerbtn").click(function(){

		if(!$(this).hasClass('almostDone'))
		{

		//$(".answerbtn").hide(0);
			var myclckId=$(this).attr('id');
			var checkboolean;

			$(".my_template_btn").removeClass('answerbtn').addClass('almostDone');;

			if(($("#whatid").hasClass('true')) && myclckId=="lightbtn")
			{
				checkboolean=true;
			}
			else if(($("#whatid").hasClass('false')) && myclckId=="nolightbtn")
			{
				checkboolean=true;
			}
			else checkboolean=false;


			if(checkboolean==true)
			{
				$(".rightbck").show(0);
				rightcounter++;
				$(this).addClass('corranswer');
				$(".my_template_btn").not(this).addClass('incorranswer');
			}
			else
			{
				$(".wrongbck").show(0);
				$(this).addClass('incorranswer');
				$(".my_template_btn").not(this).addClass('corranswer');

			}


			$("#activity-page-next-btn-enabled").show(0);
		}

	});


	$("#activity-page-next-btn-enabled").click(function(){

		$(this).hide(0);

		$(".rightbck").hide(0);
		$(".wrongbck").hide(0);
		counter++;
		loadTimelineProgress(14,(counter+1));

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
			$(".my_template_btn").addClass('answerbtn').removeClass('almostDone').removeClass('corranswer').removeClass('incorranswer');
		}
		else
		{
			$(".my_template_btn").hide(0);
			var txt=searchNreplace(data.string.s4ans, 'YYYYY',rightcounter)

			$(".scorebtn").html(txt+"/ 13");
			$(".scorebtn").show(0);
			$("#restartbtn").show(0);
			$(".whatimg").html('');

			ole.footerNotificationHandler.pageEndSetNotification();

		}
	});


	$("#restartbtn").click(function(){
		location.reload();
	});
});


function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function myquestion(questionNo,counter)
{


	var source1   = $("#template1").html();

	var template = Handlebars.compile(source1);
	var whatelse="true";

	if(questionNo>=7)
		whatelse="false";
	else whatelse="true";

	var myimgclass1="myimgclass";

	if(questionNo==1  ||questionNo==7)
	{
		myimgclass1="myimgclass1";
	}
	else if(questionNo==6)
	{
		myimgclass1="myimgclass2";
	}
	else if(questionNo==4)
	{
		myimgclass1="myimgclass3";
	}
	else if(questionNo==9)
	{
		myimgclass1="myimgclass4";
	}
	else myimgclass1="myimgclass";

	var $dataval={
		imgval:$ref+"/images/p2_"+questionNo+".png",
		imgcls:whatelse,
		myimgclass:myimgclass1
	}
	var html=template($dataval);

	$(".whatimg").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){
		$(".answerbtn").show(0);
	});
}


function searchNreplace(text, searchthis,replacewith)
{

	text=text.replace(searchthis, replacewith);
	return text;
}

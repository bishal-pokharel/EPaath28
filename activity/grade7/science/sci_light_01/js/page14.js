var content=[
	{	
		text_1_1:data.string.s14,
		text_1_2:data.string.s15	
	},
	{
		text_1_1:data.string.s14,
		text_1_2:data.string.s16,
		text_1_3:data.string.s17
	},	
	{
		text_1_1:data.string.s14,
		text_1_2:data.string.s18,
		text_1_3:data.string.s18a
	}
];

var source=["#first-template-1",
			"#first-template-2",
			"#first-template-3"];

(function ($) {
	var $value = 1;
		$clickcount = 0,
		$board = $('.board'),
		$nextbtn = $("#activity-page-next-btn-enabled"),
		$prevbtn = $(".prevButton"),
		countNext = 1,
		$total_page = 3;



	
	$nextbtn.html(getSubpageMoveButton($lang,"next"));
	$prevbtn.html(getSubpageMoveButton($lang,"prev"));


	getHtml(countNext)

	$nextbtn.click(function(){
		$(this).hide(0);
		countNext++;
		getHtml();

	});


	$prevbtn.click(function(){
		$(this).hide(0);
		countNext--;
		getHtml();

	});

	

	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		 var sourceHtml = $(source[countNext-1]).html();
		
		var template = Handlebars.compile(sourceHtml);
		var getDatas= content[countNext-1];

		var html = template(getDatas);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){

			
			$(".text_1_2").delay(600).fadeIn(400,function(){
				

				if(countNext==1)
				{
					animateMe(countNext,$total_page);
				}
				else if(countNext>1)
				{
					$(".text_1_3").delay(600).fadeIn(400,function(){
						animateMe(countNext,$total_page);
					});
			
				}
				
			});
			
			

		});

	}
	function animateMe(countNext,$total_page)
	{
		if(countNext>1)
		{
			$prevbtn.delay(500).fadeIn(10);

		}

		if(countNext<$total_page)
		{
			$nextbtn.delay(500).fadeIn(10);
		}
		if(countNext==$total_page)
		{
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	}
	
	

	
})(jQuery);




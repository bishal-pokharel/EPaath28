$(function() {
	
	var arrowbtn=getArrowBtn();
	$("#next1, #next2, #next3, #next4").html(arrowbtn);

	loadTimelineProgress(1,1);	
	$(".p1_title").html(data.string.s37);
	
	$(".p13_btn").html(getReloadBtn());
	
	
	for(i=38;i<=47;i++)
	{
		$(".s"+i).html(searchNreplace(data.string['s'+i], "YYYYY", "&deg;"));
	}

	$("#next1").click(function(){
		$(this).fadeOut(10);
		$(".s38").fadeOut(1000,function(){

			$(".p13_frame2").fadeIn(500,'linear',function(){
				$(".p13_blinking2").fadeIn(500,'linear');
			});//fadein

			$(".s44").delay(2500).fadeIn(500,'linear',function(){
				$("#next2").delay(2000).fadeIn(10,function(){
					//$(this).addClass('animated shake');

				});
			});

		});//fadeout

	});//click
	
	$("#next2").click(function(){
			$(this).fadeOut(10);
		$(".s44").fadeOut(500,'linear',function(){
			$(".p13_ray2").fadeIn(500,'linear',function(){
				$(".s45").delay(2000).fadeIn(500,'linear',function(){
					$("#next3").delay(2000).fadeIn(10,function(){
						//$(this).addClass('animated shake');

					});
				});
			});
			
		});

	});//click


	$("#next3").click(function(){
		$(this).fadeOut(10);
		$(".s45").fadeOut(500,'linear',function(){

			$(".p13_ray3").fadeIn(500,'linear',function(){
				$(".s46").delay(2000).fadeIn(500,'linear',function(){
					$("#next4").delay(2000).fadeIn(10,function(){
							//$(this).addClass('animated shake');

						});
				});
			});
		});
	});

	$("#next4").click(function(){
			$(this).fadeOut(10);
		$(".s46").fadeOut(500,'linear',function(){

			$(".p13_ray4").fadeIn(500,'linear',function(){
				$(".s47").delay(2000).fadeIn(500,'linear',function(){
					$(".p13_btn").fadeIn(500,'linear');
					ole.footerNotificationHandler.lessonEndSetNotification();
	
				});
			});
		});
	});
	
	
	
	$(".p13_btn").click(function(){
	
			location.reload(); 
	});
	
});
function searchNreplace(text, searchthis,replacewith)
{
	
	text=text.replace(searchthis, replacewith);
	return text;
}

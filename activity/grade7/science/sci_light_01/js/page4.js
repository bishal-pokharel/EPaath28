
$(document).ready(function()
{
	loadTimelineProgress(1,1);	
	
	$(".def_700").html(data.string.s13c);
	$(".def_7").html(data.string.s13b);
	$(".def_7b").html(data.string.s13a);
	

	ole.footerNotificationHandler.pageEndSetNotification();
	
	
	
	var counts=0;
	
	
	$("#torch_7").click(function(){
	
		if(!$(this).hasClass('dragging'))
		{
			$(this).addClass('dragging');
			$("#ray_7").hide(0);
			$(".shadow_7").hide(0);
			
			if($(this).hasClass('dragged'))
			{
				$(this).transit({x:'0%'},function(){
					
					$("#ray_7").find('img').attr('src','activity/grade7/science/sci_light_01/images/p4_6.png');
					
					$(".shadow_7").find('img').css({'height':'80%'});
					
					$(".shadow_7").delay(200).fadeIn(1);
					
					$("#ray_7").transit({x:'0%',y:'0%'},100).fadeIn(100,function(){
						
						$("#torch_7").removeClass('dragged');
						
						$("#torch_7").removeClass('dragging');
					
					});//transit
					
				});//transit
				
			}//if
			else
			{
				$(this).transit({x:'-25%'},function(){
					
					$("#ray_7").find('img').attr('src',$ref+'/images/p4_12.png');
					
					$(".shadow_7").find('img').css({'height':'100%'});
					
					$(".shadow_7").delay(200).fadeIn(1);
					
					$("#ray_7").transit({x:'-5%',y:'-11%'},100).fadeIn(100,function(){
					
						$("#torch_7").addClass('dragged');
					
						$("#torch_7").removeClass('dragging');
					
					});//transit
				});//transit
				
			}//else
		}//hasclass
	});//click
	
	$(".selector_7").click(function()
	{
		var $src=$(this).find('img').attr('src');
		$(".item_7").find('img').attr('src',$src);
		
		var whatNumbers =$src.split('/');
		var len=whatNumbers.length;
		whatNumbers =whatNumbers[len-1].split('_');
		
		var Numbers=parseInt(whatNumbers[1])+1;
		var $newsrc=$ref+'/images/p4_'+Numbers+'.png';
		$(".shadow_7").find('img').attr('src',$newsrc);
		
		
	});
	
});


$(function() {
	
	loadTimelineProgress(1,1);	
	var whatChange=0;
	
	$(".p5_title").html(data.string.s16);
	$(".s15").html(data.string.s15);
	$("#nomirror1").html(data.string.s18a);
	$("#innerbtn").html(getCloseBtn());
	
	ole.footerNotificationHandler.pageEndSetNotification();
	
	
	
	$("input[name='y-mirror']").attr('checked',false);
	
	
	$("input[name='y-mirror']").change(function()
	{
		
		
		if($(this).attr('id')=="nomirror") whatChange=1;
		else if($(this).attr('id')=="mirror1") whatChange=2;
		else if($(this).attr('id')=="mirror2") whatChange=3;
		
		$("#page5_conclusion").fadeIn(1000);
		$("#innerbtn").show(0);
		if(whatChange==1)
		{
			hideAll();
			$(".light_ray1").show(0);
			$("#page5_conclusion .page5_content").html(data.string.s18);
		}
		else if(whatChange==2)
		{
			hideAll();
			$(".light_ray2").show(0);
			$("#first_mirror").show(0);
			
			$(".light_ray3").css({'width':'15%'});
			$(".light_ray3").show(0);
			$("#page5_conclusion .page5_content").html(data.string.s17+"<br/>"+data.string.s18);
			
		}
		else if(whatChange==3)
		{
			hideAll();
			$(".light_ray2").show(0);
			$("#first_mirror").show(0);
			
			$(".light_ray3").css({'width':'11%'});
			$(".light_ray3").show(0);
			$("#second_mirror").show(0);
			$(".light_ray4").show(0);
			$("#page5_conclusion .page5_content").html(data.string.s17+"<br/>"+data.string.s18);
		}
		
		//
		
	});
	
	$("#innerbtn").click(function()
	{
		$("#page5_conclusion").fadeOut(1000);
	});
});
function hideAll()
{

	$(".light_ray1").hide(0);
	$(".light_ray2").hide(0);
	$(".light_ray3").hide(0);
	$(".light_ray4").hide(0);
	$("#first_mirror").hide(0);
	
	$("#second_mirror").hide(0);

}
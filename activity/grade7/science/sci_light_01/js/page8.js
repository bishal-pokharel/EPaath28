
$(document).ready(function()
{
	loadTimelineProgress(1,1);
	$(".p1_title").html(data.string.s25);
	$(".s25a").html(data.string.s25a);
	$(".s26").html(data.string.s26);
	$(".s27").html(data.string.s27);
	$(".normal_txt").html(data.string.s27c);
	$(".ray_txt").html(data.string.s27a);
	$(".ref_txt").html(data.string.s27b);

	$("#Reset").html(getReloadBtn());




	resetAnimation();


	$(".reflectionGame #Reset").click(function(){
		location.reload();

	});
});

function resetAnimation()
{
	$(".reflectionGame #Reset").hide(0);

	$(".reflectionLi").hide(0);
	$(".ray3").css({'height':'0'});

	$(".protractor").hide(0);

	$(".ray1").hide(0);
	$(".ray2").hide(0);
	$(".arrow").hide(0);
	$(".arrow2").hide(0);
	$(".deg1").hide(0);
	$(".deg2").hide(0);

	$(".ray_txt").css({'left':'10%','top':'25%'});
	$(".ref_txt").css({'left':'60%','top':'25%'})
	$(".ray_txt").hide(0);
	$(".normal_txt").hide(0);
	$(".ref_txt").hide(0);

	$(".ray4").hide(0);
	$(".ray5").hide(0);
	$(".arrow3").hide(0);
	$(".arrow4").hide(0);
	$(".deg3").hide(0);
	$(".deg4").hide(0);

	$(".ray6").hide(0);
	$(".ray7").hide(0);
	$(".arrow5").hide(0);
	$(".arrow6").hide(0);
	$(".deg5").hide(0);
	$(".deg6").hide(0);

	$(".ray1").delay(1000).fadeIn(600).delay(4000).fadeOut();
	$(".ray2").delay(1500).fadeIn(800).delay(3500).fadeOut();
	$(".ray3").delay(2000).animate({'height':'73%'},1000);

	$(".normal_txt").delay(2500).fadeIn(500);

	$(".protractor").delay(3000).fadeIn(1000,function(){
		$(".arrow, .arrow2, .deg1, .deg2").show(0);



	});





	$(".ray_txt").delay(2500).fadeIn(500).delay(3000).animate({'left':'30%','top':'35%'},500).delay(3000).animate({'left':'10%','top':'50%'},500);

	$(".ref_txt").delay(2500).fadeIn(500).delay(3000).animate({'left':'62%','top':'35%'},500).delay(3000).animate({'left':'65%','top':'50%'},500);

	$(".arrow, .arrow2, .deg1, .deg2").delay(5500).fadeOut();
	$(".ray5").delay(6000).fadeIn(800).delay(2000).fadeOut();

	$(".ray4").delay(6500).fadeIn(600,function(){

		$(".arrow3, .arrow4, .deg3, .deg4").show(0);
	}).delay(1500).fadeOut();

	$(".arrow3, .arrow4, .deg3, .deg4").delay(8500).fadeOut();


	$(".ray6").delay(9000).fadeIn(800);
	$(".ray7").delay(9500).fadeIn(600,function(){

		$(".arrow5, .arrow6, .deg5, .deg6").show(0);
		$(".s25a").fadeOut(100);
		$(".reflectionLi").fadeIn(1000,'linear',function(){

			$(".reflectionGame #Reset").fadeIn();
			ole.footerNotificationHandler.pageEndSetNotification();

		});

	});


}

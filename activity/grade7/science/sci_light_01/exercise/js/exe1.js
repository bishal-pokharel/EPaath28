$(function(){


	$("#toDo").html(data.string.e_1);


	// var whatnext= getSubpageMoveButton($lang,"next");

	// $("#activity-page-next-btn-enabled").append(whatnext);


	var counter=0;
	var totalCounter=10;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);

	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;


	myquestion(newarray[counter],counter);

	$(".allquestion").on("click",".ans",function(){


		var isCorrect=parseInt($(this).attr('corr'));

		$("#showAns div").removeClass('ans');
		$("#showAns div[corr='1']").addClass('corrans');
		if(isCorrect==1)
		{
			$("#img22").attr('src',$ref+'/exercise/images/correct.png');
			$("#img22").addClass('Ok');

			rightcounter++;
		}
		else
		{
			$("#img22").attr('src',$ref+'/exercise/images/incorrect.png');
			$("#img22").addClass('notOk');

			wrongCounter++;
		}

		$("#activity-page-next-btn-enabled").fadeIn(10,function(){
			//$(this).addClass('animated shake');
		});

	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0).removeClass('animated shake');
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}

		if(counter > totalCounter){
			ole.activityComplete.finishingcall();
		}
	});

	/*$(".allquestion").on("click","#repeatIdThis",function(){

		location.reload();
	});*/

});


function myquestion(questionNo,counter)
{
	loadTimelineProgress(11,(counter+1));
	var source   = $("#label-template").html();

	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter)
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){

	});
}

function myquestion2(right,wrong)
{
	loadTimelineProgress(11,11);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var $dataval={rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:data.string.exe_1, qans: data.string.opt_1_2,quesno:"ques_1"},
			{ques:data.string.exe_2, qans: data.string.opt_2_1,quesno:"ques_2"},
			{ques:data.string.exe_3, qans: data.string.opt_3_2,quesno:"ques_3"},
			{ques:data.string.exe_4, qans: data.string.opt_4_1,quesno:"ques_4"},
			{ques:data.string.exe_5, qans: data.string.opt_5_2,quesno:"ques_5"},
			{ques:data.string.exe_6, qans: data.string.opt_6_2,quesno:"ques_6"},
			{ques:data.string.exe_7, qans: data.string.opt_7_1,quesno:"ques_7"},
			{ques:data.string.exe_8, qans: data.string.opt_8_1,quesno:"ques_8"},
			{ques:data.string.exe_9, qans: data.string.opt_9_2,quesno:"ques_9"},
			{ques:data.string.exe_10, qans: data.string.opt_10_2,quesno:"ques_10"}

		]


	}
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.exe3);

	}).delay(100).fadeIn(10,function(){
		// $("#repeatIdThis").html(getReloadBtn());
		$("#activity-page-next-btn-enabled").show(0);

	});
}

function getQuestion($quesNo,counter)
{
	var quesList;
	var nep=ole.nepaliNumber(counter+1);

	var whatri=whatCorr($quesNo);


	quesList={
		myQuesion:data.string["exe_"+$quesNo],
		img22:$ref+"/exercise/images/blank.png",
		optObj:[
				{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"]},
				{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"]}
				]
	}

	return quesList;
}

function whatCorr($quesNo)
{
	var whatis=new Array();
	switch($quesNo)
	{
		case 1: whatis[0]=0;whatis[1]=1;  break;
		case 2: whatis[0]=1;whatis[1]=0; break;
		case 3: whatis[0]=0;whatis[1]=1;  break;
		case 4: whatis[0]=1;whatis[1]=0; break;
		case 5: whatis[0]=0;whatis[1]=1;break;
		case 6: whatis[0]=0;whatis[1]=1; break;
		case 7: whatis[0]=1;whatis[1]=0; break;
		case 8: whatis[0]=1;whatis[1]=0; break;
		case 9: whatis[0]=0;whatis[1]=1; break;
		case 10: whatis[0]=0;whatis[1]=1; break;
	}
	return whatis;
}

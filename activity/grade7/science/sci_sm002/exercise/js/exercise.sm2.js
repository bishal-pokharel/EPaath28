(function ($) {

/***
* change timing of any css animation -test phase
element- is the class or id passed to function
duration- is the new time to be set
**/
function setAnimSpeed(element, duration){
	$(element).css({
		"animation-duration": duration+"s",
		"-webkit-animation-duration": duration+"s"
	});
}
/***
* most common variables :D
**/
var $refImg = $ref+"/exercise/images/",
$board = $('.board'),
$title = $('.title'),
$popUp = $('.popUp'),
$scene = "",
$currentScene,$currentToolBox,$currentDialogBox,
countNext = 0,
animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
smth = "";

/*****************************
* data , content.num // and call it model ;)
******************************/

var content = [
	{
		tools : [
			{tool : $refImg+'options/plane.png',
			toolName:"hammer1"},
			{tool : $refImg+'options/scooty.png',
			toolName:"hammer2",
			correct : "correct"},
			{tool : $refImg+'options/lever.png',
			toolName:"hammer3"}
		],
		dialog : data.string.walkDiaglog,
		stickMan : $refImg+'walking.gif',
	},
	{
		tools : [
			{tool : $refImg+'options/lever.png',
			toolName:"lever"},
			{tool : $refImg+'options/screw.png',
			toolName:"screw",
			correct : "correct"},
			{tool : $refImg+'options/pulley.png',
			toolName:"pulley"}
		],
		dialog : data.string.ex11,
		stickMan : $refImg+'scooty.gif',
		bus1: $refImg+'gadi.png'
	},
	{
		tools : [
			{tool : $refImg+'options/axe.png',
			toolName:"axe",
			correct : "correct"},
			{tool : $refImg+'options/scooty.png',
			toolName:"wheel & axle"},
			{tool : $refImg+'options/plane.png',
			toolName:"inclined plane"}
		],
		dialog : data.string.ex13,
		stickMan : $refImg+'scooty.gif',
		log2: $refImg+'log.png'
	},
	{
		tools : [
			{tool : $refImg+'options/scooty.png',
			toolName:"wheel & axle"},
			{tool : $refImg+'options/plane.png',
			toolName:"inclined plane",
			correct : "correct"},
			{tool : $refImg+'options/pulley.png',
			toolName:"pulley"}
		],
		dialog : data.string.ex15,
		stickMan : $refImg+'scooty.gif',
		onlyScooter:$refImg+"onlyScooty.png"
	},
	{
		tools : [
			{tool : $refImg+'options/pulley.png',
			toolName:"hammer1",
			correct : "correct"},
			{tool : $refImg+'options/screw.png',
			toolName:"hammer2"},
			{tool : $refImg+'options/plane.png',
			toolName:"hammer3"}
		],
		dialog : data.string.giftPulleyDiaglog,
		stickMan : $refImg+'stand.png',
		stickWoman : $refImg+'stickwoman.png',
		giftBox : $refImg+"gift_box.png",
		pulley: $refImg+'pulley_pull.gif',
	},

	{
		tools : [
			{tool : $refImg+'options/lever.png',
			toolName:"hammer1",
			correct : "correct"},
			{tool : $refImg+'options/scooty.png',
			toolName:"hammer2"},
			{tool : $refImg+'options/axe.png',
			toolName:"hammer3"}
		],
		dialog : data.string.giftDiaglog,
		leverWithBox : $refImg+'leverwithbox.png',
		leverThrow : $refImg+'lever_throw.gif',
		stickMan : $refImg+'stand.png',
		stickWoman : $refImg+'stickwoman.png',
		giftBox : $refImg+"gift_box.png",
		flower : $refImg+"flower.png",
	},
	{
		dialog : data.string.omgDialog,
		stickMan : $refImg+'stand.png',
		stickWoman : $refImg+'stickwome_flower.png',
		love : $refImg+"love.png",
	}
]



/***************
* Lets call it View // which template to select
****************/
function myTemplate(num,funCall){
		var source = $("#scene"+num+"-template").html(); //why need one extra var :P
		var template = Handlebars.compile(source);
		// update content data with sceneNumber
		content[num].sceneNumber = num;
		var html = template(content[num]);
		$board.html(html);

		// define the currentScene, currentDialogBox, & currentToolBox
		$currentScene = $board.children('.scenes');
		$currentDialogBox = $currentScene.children('.dialog');
		$currentToolBox = $currentScene.children('.toolBox');
		// hide toolBox and dialog at default
		$currentToolBox.hide(0);
		$currentDialogBox.hide(0);
		toolSelectCheck()
		funCall();
	}

/**
* after selection of correct tool in
	each scene these functions are executed which are scene specific
	*/
	function scene1afterToolSelect(){
		$currentToolBox.hide(0);
		$currentDialogBox.children("p").text(data.string.ex12);
		$(".bus").attr("src",$refImg+"wheelchange.gif");
		$(".bus").on('load',function() {
		// at around 3 seconds the above loaded gif completes

		setTimeout(function () {
			$(".bus").attr("src",$refImg+"Gadi.gif");
				// ASAP above gif loads
				$(".bus").on('load',function() {
					$(".bus").animate({"left":"100%"},200,"linear",function(){
						$(".stickMan").attr("src",$refImg+"scooty.gif");
						setAnimSpeed(".cityMove",200);
						$currentDialogBox.show(0);
						$(".last").show(0);
						$(".last").on('click', function() {
							$currentDialogBox.css({"display":"none"});
							setAnimSpeed(".cityMove",20);
							$(".stickMan").animate({"left":"100%"},500,"linear",function(){
								myTemplate(2,func2);
							});
						});
					});
				});
			},3000);
	});
	}

	function scene2afterToolSelect(){
		$currentToolBox.hide(0);
		$currentDialogBox.children("p").text(data.string.ex14);
		$(".log").attr("src",$refImg+"log.gif");
		$(".log").on('load',function() {
		// at around 3 seconds the above loaded gif completes
		setTimeout(function () {
				$(".stickMan").attr("src",$refImg+"scooty.gif");
				setAnimSpeed(".cityMove",200);
				$(".log").animate({"left":"-20%"},2000,"linear");
				$currentDialogBox.show(0);
				$(".last").show(0);
				$(".last").on('click', function() {
					$currentDialogBox.css({"display":"none"});
					setAnimSpeed(".cityMove",20);

					$(".stickMan").animate({"left":"100%"},500,"linear",function(){
						myTemplate(3,func3);
					});
				});


		},4000);
	});

	}

	function scene3afterToolSelect(){
		$currentDialogBox.css({"left":"65%","width":"30%","bottom":"70%"});
		$currentDialogBox.children("p").text(data.string.ex16);
		$(".last").show(0);
		$currentToolBox.hide(0);
		$("#inclinedPlane").show(0);
		$(".scooterOnly").show(0);
		$(".stickMan").attr('src', $refImg+'walking.gif');
		$(".stickMan").css({"left":"26%"});
	 // move the stickMan up the wall asa it starts walking
	 $(".stickMan").animate({"left":"51%","bottom":"30%"},5000,"linear",function(){
	 	$(".stickMan").animate({"left":"60%"},2000,"linear",function(){
	 		$(".stickMan").stop();
	 		$(".stickMan").attr('src', $refImg+'stand.png');
	 		$currentDialogBox.show(0);
	 		$(".last").on('click', function() {
	 			$currentDialogBox.css({"display":"none"});
	 			setAnimSpeed(".cityMove",800);
	 			$(".stickMan").attr('src', $refImg+'walking.gif');
	 			$(".stickMan").animate({"left":"100%"},4000,"linear",function(){
	 				myTemplate(4,func4);
	 			});
	 		});
	 	});
	 });
	}

/**
* on clicking toolBox images
*/
function toolSelectCheck () {
	$currentToolBox.on('click', 'img', function() {
		var selected =$(this);
		var myTool = selected.attr("class");
		if(myTool != "correct"){
			selected.addClass('animated hinge');
		}

		else if(myTool == "correct"){
			selected.addClass('animated zoomOutDown').one(animationEnd,function(){
 			// get scene Number and change it to number type
 			$currentToolBox.hide(0);
 			var whichScene = $currentScene.data("scenenumber");
 			console.log(whichScene);
 			switch(whichScene){
 				case 0:
 				scene0Works();
 				break;
 				case 1:
 				scene1afterToolSelect();
 				break;
 				case 2:
 				scene2afterToolSelect();
 				break;
 				case 3:
 				scene3afterToolSelect();
 				break;
 				case 4:
 				scene4works();
 				break;
 				case 5:
 				scene5works();
 				break;
 			}
 		});
		}
	});
}

/********
* functions to be loaded after template has been loaded
*********/

	// function for scene 0
	function func0 () {
		var dialogCount=1;
		$scene = $board.find('.scene0');
		setAnimSpeed(".cityMove", 500);

		$scene.find('.stickMan').animate({
			'left' : '15%'
		},3500,function () {
			setAnimSpeed(".cityMove", 200);
			$scene.find('.dialog').show(0);
		})

		$scene.on('click','.send',function () {
			if (dialogCount===1) {
				$scene.find('.dialog').hide(0);
				$scene.find('.stickMan').attr('src',$refImg+'stand.png');
				setAnimSpeed(".cityMove", 1000);
				$scene.find('.toolBox').show(0)
				dialogCount++;
			} else {
				$scene.find('.dialog').hide(0);
				$scene.find('.stickMan').animate({
					'left' : '100%' },1000,function (){
						myTemplate(1,func1);
					}
					);
			}

		})



	}

	// function for scene 1
	function func1 () {
		$currentDialogBox.css({"display":"none","left":"15%"});
		setAnimSpeed(".cityMove",100);
		$(".stickMan").animate({"left":"10%"},5000,"linear",function(){
			$(".stickMan").attr({"src":$refImg+"standScooty.png"});
			$currentDialogBox.show(0);
			setAnimSpeed(".cityMove",0);
		});

		$currentScene.on('click','.send',function () {
			$currentDialogBox.hide(0);
			$currentToolBox.show(0);
		})
	}

	function func4 () {
		setAnimSpeed(".cityMove", 70000);
		dialogCount=1;
		$currentDialogBox.show(0);
		$currentDialogBox.on('click','.send',function () {
			if (dialogCount===1) {
				$currentDialogBox.hide(0);
				$currentToolBox.show(0)
				dialogCount++;
			} else {
				myTemplate(5,func5);
			}


		})
	}

/*****************
*
******************/
function func5 () {
	setAnimSpeed(".cityMove", 70000);
	$scene = $board.find('.scene5');
	showGiftLine();
	setTimeout(function () {
		$scene.find('.svgHolder').show(0);
	},1000);

	$scene.on('click','.send',function () {
		$scene.find('.dialog').hide(0);
		$scene.find('.toolBox').show(0);
		$scene.find('.svgHolder').hide(0);
	})



}

/*****************
*funciton for scene 2
******************/
function func2 () {

	$currentDialogBox.css({"display":"none","left":"15%"});
	$currentToolBox.css({"left":"15%"});
	setAnimSpeed(".cityMove",100);
	$(".stickMan").animate({"left":"8%"},5000,"linear",function(){
		$(".stickMan").attr({"src":$refImg+"standScooty.png"});
		console.log($currentDialogBox);
		$currentDialogBox.show(0);
		setAnimSpeed(".cityMove",0);
	});

	$currentScene.on('click','.send',function () {
		$currentDialogBox.hide(0);
		$currentToolBox.show(0);
	})
}

/*****************
*funciton for scene 3
******************/
function func3() {
	$currentDialogBox.css({"display":"none","left":"30%"});
	$currentToolBox.css({"display":"none","left":"25%"});
	setAnimSpeed(".cityMove",500);
	$(".stickMan").animate({"left":"24%"},5000,"linear",function(){
		$(".stickMan").attr({"src":$refImg+"standScooty.png"});
		console.log($currentDialogBox);
		$currentDialogBox.show(0);
		setAnimSpeed(".cityMove",0);
	});

	$currentScene.on('click','.send',function () {
		$currentDialogBox.hide(0);
		$currentToolBox.show(0);
	})
}


/*
* func6
*/
function func6 () {
	var snapC = Snap("#sendLine");
		// var myPath = "M800,50 l-740,0";
		var myPath = "M800.01,81.263C731.52,79.08,671.9,42.883,603.765,25.849c-19.164-4.259-40.457-6.388-59.621-4.259c-10.646,2.129-23.421,4.259-34.067,8.518c-42.585,17.034-85.171,29.81-129.885,44.714c-40.456,12.775-85.171,8.517-123.497-6.388c-68.137-27.68-140.532-55.36-214.079-40.3";

		var myPathC = snapC.path(myPath).attr({
			id: "squiggle",
			fill: "none",
			strokeWidth: "4",
			// stroke: "#ffffff",
			strokeMiterLimit: "10",
			strokeDasharray: "12 6",
			strokeDashOffset: "180"
		});

		var Triangle = snapC.image($refImg+"love.png", 10, 50, 30, 30);

		initTriangle();

		function initTriangle()
		{
			var triangleGroup = snapC.g( Triangle );
			movePoint = myPathC.getPointAtLength(length);
			triangleGroup.transform( 't' + parseInt(movePoint.x - 15) + ',' + parseInt( movePoint.y - 15) + 'r' + (0));
		}
		var lenC = myPathC.getTotalLength();

		myPathC.attr({
			display: "none"
		});

		function animateSVG() {

			var triangleGroup = snapC.g( Triangle );

			Snap.animate(0, lenC, function( value ) {

				movePoint = myPathC.getPointAtLength( value );

				triangleGroup.transform( 't' + parseInt(movePoint.x - 15) + ',' + parseInt( movePoint.y - 15) + 'r' + (10));

			}, 2500,mina.easeinout, function(){
				// boy falls fun :D
				/*$currentScene.find('.stickMan').animate({
					'transform' : 'rotate(270deg)',
					"bottom": "30%",
					"left": "25%",
				}, {
					duration: 1000,
					step: function() {
						$(this).css('-webkit-transform','rotate(270deg)');
						$(this).css('-moz-transform','rotate(270deg)');
						$currentScene.find('.svgHolder').hide(0);
						$currentDialogBox.show(0);
					}})*/
			$currentDialogBox.show(0);
			$currentScene.find('.svgHolder').hide(0);
			});
		}

		animateSVG();
}

/*****************************************
* svgssss
******************************************/
function showGiftLine () {
	var snapC = Snap("#sendLine");
	var myPathC = snapC.path("M4.572,508.04	c0,0,49.323-502.411,620.027-502.411S1100,508.04,1200,350").attr({
    id: "squiggle",
    fill: "none",
    strokeWidth: "4",
    stroke: "#ffffff",
    strokeMiterLimit: "10",
    strokeDasharray: "9 9",
    strokeDashOffset: "988.01"
  });

	var Triangle = snapC.polyline("0, 30, 15, 0, 30, 30");
	Triangle.attr({
		id: "plane",
		fill: "#006400"
	});

		initTriangle();

		function initTriangle()
		{
			var triangleGroup = snapC.g( Triangle );
			movePoint = myPathC.getPointAtLength(length);
			triangleGroup.transform( 't' + parseInt(movePoint.x - 15) + ',' + parseInt( movePoint.y - 15) + 'r' + (movePoint.alpha - 90));
		}
		var lenC = myPathC.getTotalLength();

		myPathC.attr({
			display: "none"
		});

		var gr = snapC.g(myPathC);



		function animateSVG() {
			var triangleGroup = snapC.g( Triangle );
			var flight = gr.path().attr({
				fill: "none",
				stroke: "red",
				strokeWidth: 3,
				strokeDasharray: "10 5"
			}).insertBefore(triangleGroup);

			Snap.animate(0, lenC, function( value ) {

				movePoint = myPathC.getPointAtLength( value );
				flight.attr({
					d: myPathC.getSubpath(0, value)
				});

				triangleGroup.transform( 't' + parseInt(movePoint.x - 15) + ',' + parseInt( movePoint.y - 15) + 'r' + (movePoint.alpha - 90));

			}, 4500,mina.easeinout, function(){
				$currentDialogBox.show(0);
			});
		}

		animateSVG();
	}

	function throwGift () {
		var snapC = Snap("#sendLine2");
		var myPath = "M5,200c0,0,158.998-290.021,588.293-260.019 c429.294,30.003,473.813,260.019,473.813,260.019";

		var myPathC = snapC.path(myPath).attr({
			id: "squiggle",
			fill: "none",
			strokeWidth: "4",
			// stroke: "#ffffff",
			strokeMiterLimit: "10",
			strokeDasharray: "12 6",
			strokeDashOffset: "180"
		});

		var Triangle = snapC.image($refImg+"gift_box.png", 10, 50, 60, 60);

		initTriangle();

		function initTriangle()
		{
			var triangleGroup = snapC.g( Triangle );
			movePoint = myPathC.getPointAtLength(length);
			triangleGroup.transform( 't' + parseInt(movePoint.x - 15) + ',' + parseInt( movePoint.y - 15) + 'r' + (0));
		}
		var lenC = myPathC.getTotalLength();

		myPathC.attr({
			display: "none"
		});

		function animateSVG() {

			var triangleGroup = snapC.g( Triangle );

			Snap.animate(0, lenC, function( value ) {

				movePoint = myPathC.getPointAtLength( value );

				triangleGroup.transform( 't' + parseInt(movePoint.x - 15) + ',' + parseInt( movePoint.y - 15) + 'r' + (10));

			}, 2500,mina.easeinout, function(){
				showGiftNextSide();
				$currentScene.find('.svgHolder2').hide(0);
			});
		}

		animateSVG();
	}

	function scene5works () {
		$currentScene.find('.giftBox').hide(0);
		$currentScene.find('.leverTime').show(0);

		$currentScene.on('click','.bounce',function () {


			$currentScene.find('.leverThrow').html("<img src='"+$refImg+'lever_throw.gif?3343'+"'>").show(function () {
				setTimeout(function () {
					throwGift();
					$currentScene.find('.svgHolder2').show(0);
				},1300);

			});
			$currentScene.find('.leverTime').hide(0);
			$currentScene.find('.stickMan').hide(0);
		})
	}

	function showGiftNextSide () {
		$currentScene.find('.giftBox2').show(0);
		$currentScene.find('.leverThrow').hide(0);
		$currentScene.find('.stickMan').show(0);
		$currentScene.find('.flower').delay(800).fadeIn().animate({
			"bottom" : '44%'
		},800,function () {
			$currentScene.find('.stickWoman').animate({
				'right' : '7%'
			},800,function () {
					$currentScene.find('.stickWoman').attr('src',$refImg+'stickwoman2.png');
					setTimeout(myTemplate(6,func6),600);
					$("#activity-page-next-btn-enabled").show(0);
			})

		})
	}

	function scene0Works () {
		$currentScene.find('.stickMan').attr('src',$refImg+'scooty.gif');
		setAnimSpeed(".cityMove", 70);
		setTimeout(function () {
			$currentScene.find('.dialog p').html(data.string.gotScooty+"<span class='glyphicon glyphicon-arrow-right send'></span>");
			$currentScene.find('.dialog').show(0);
		},1000)
	}

	function scene4works () {
		$currentScene.find('.stickMan').hide(0);
		$currentScene.find('.giftBox').hide(0);
		$currentScene.find('.pulley').html("<img src='"+$refImg+'pulley_pull.gif'+"' alt='pulley pull'>")
		setTimeout(function () {
			$currentScene.find('.dialog p').html(data.string.pulleyHurrah+"<span class='glyphicon glyphicon-arrow-right send'></span>");
			$currentScene.find('.dialog').show(0);
		},3000);
	}

	myTemplate(0,func0);

	$("#activity-page-next-btn-enabled").on('click',function () {
		ole.activityComplete.finishingcall();
	})

})(jQuery)
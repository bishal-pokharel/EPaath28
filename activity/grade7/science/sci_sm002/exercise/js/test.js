var snapC = Snap("#svg");
var path1 = snapC.select("#path1");
  // SVG C - "Squiggly" Path
  var myPathC = snapC.path("M 100 400 l 100 -300 l 100 150 l 100 -150 l 100 300").attr({
    id: "squiggle",
    fill: "none",
    strokeWidth: "4",
    stroke: "#ffffff",
    strokeMiterLimit: "10",
    strokeDasharray: "12 6",
    strokeDashOffset: "180"
  });

  // SVG C - Triangle (As Polyline)
  var Triangle = snapC.polyline("0, 30, 15, 0, 30, 30");
  Triangle.attr({
    id: "plane",
    fill: "#005400"
  });

  initTriangle();

  // Initialize Triangle on Path
  function initTriangle(){
    var triangleGroup = snapC.g( Triangle ); // Group polyline
    movePoint = myPathC.getPointAtLength(length);
    triangleGroup.transform( 't' + parseInt(movePoint.x - 15) + ',' + parseInt( movePoint.y - 15) + 'r' + (movePoint.alpha - 90));
  }

  // SVG C - Draw Path
  var lenC = myPathC.getTotalLength();

  // SVG C - Animate Path
  function animateSVG() {

    myPathC.attr({
      stroke: '#000',
      strokeWidth: 4,
      fill: 'none',
      // Draw Path
      "stroke-dasharray": lenC+" "+lenC,
      "stroke-dashoffset": lenC
    }).animate({"stroke-dashoffset": 10}, 4500,mina.easeinout);

    var triangleGroup = snapC.g( Triangle ); // Group polyline

    setTimeout( function() {
      Snap.animate(0, lenC, function( value ) {

        movePoint = myPathC.getPointAtLength( value );

        triangleGroup.transform( 't' + parseInt(movePoint.x - 15) + ',' + parseInt( movePoint.y - 15) + 'r' + (movePoint.alpha - 90));

      }, 4500,mina.easeinout, function(){
        alertEnd();
      });
    });

  }

      animateSVG();


      // html part
      <svg viewBox = "0 0 1100 400" version = "1.1" id="svg">
    <defs>
    <path id="path1" d = 'M 700 50 ' stroke = 'blue' stroke-width = '3'/>
    </defs>
</svg>
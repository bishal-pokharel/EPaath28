$(function(){


	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		quest++;
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);

		getFunction(quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		quest--;
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		getFunction(quest);
	});

});


function getFunction(qno)
{
	loadTimelineProgress(2,qno);
	switch(qno)
	{
		case 1:
		{
			case0();
			break;
		}
		case 2:
		{
			case1();
			break;
		}
	}
}
function case0()
{
	var datavar={
		middle_text: data.lesson.chapter,
		coverpage:$ref+"/images/utilities_of_simple_machine.png"
	}

	var source   = $("#firstpage-template").html();

	var template = Handlebars.compile(source);

	var html=template(datavar);
	$(".frontpage").html(html);
	$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
}

function case1(){
	$(".frontpage").hide(0);
	var d= new Date();
	var lastTime = d.getTime();
	var $popUp = $('.popUpInside'),$msg1 = $('.msg1');
	var continueCount = 0,coutPlayit=0;
	var neClick = 0,clickLever=0,clickPulley=0,clickScrew=0,clickPlane=0,clickWheel=0,clickWedge=0;
	var nameSm="";

	$('.tabOver').text(data.string.hintClickMe);
	/**
	* load ropeway and ropeway calls, clicks
	*/
	function loadRopeWay () {
		var ropeway = Snap(".ropeway");
		Snap.load($ref+"/images/ropeway.svg", function(f) {

			/**
			* clickables
			*/
			var wheel = f.select('#wheelRight');
			var wheel2 = f.select('#wheelLeft');
			var pulley1 = f.select('#pulley1Back');
			var pulley2 = f.select('#pulley2Back');
			var pulley3 = f.select('#pulley1Front');
			var pulley4 = f.select('#pulley2Front');
			var screw = f.select('#screw');
			var lever = f.select('#break');
			var screw2 = f.select('#screw2');


			var bucketBack = f.select("#bucketBack");
			var bucketFront = f.select("#bucketFront");
			var myPathB = f.select('#stringWheel');
			var path1 = f.select('#path1');
			var pathUp = f.select('#path2up');
			var food = f.select('#foodInBucket');
			var len = path1.getTotalLength();
			var len2 = pathUp.getTotalLength();

			console.log(f.selectAll(".wheel"));

			food.drag();
			bucketBack.click(function() {

			});

			bucketFront = f.select("#bucketFront");

			/**
			* wheel rotation
			*/
			function rotateWheel () {
				var countRotate;
				(function wheelAnimation(){
					if (typeof countRotate === 'undefined' || countRotate > 4) {
						countRotate=0;
						console.log('it came here');
					}
					console.log('countRotate = '+countRotate);
					wheel.stop().animate(
							{ transform: 'r360,976.521,519.271'}, // Basic rotation around a point. No frills.
							1000, // Nice slow turning rays

							function(){
								wheel.attr({ transform: 'rotate(0 976.521,519.271)'}); // Reset the position of the rays.
								 // Repeat this animation so it appears infinite.
								 if(countRotate >= 4){
								 	wheel.stop()
								 } else {
								 	wheelAnimation();
								 }
								 countRotate++;
								}

								);

				})()

				var countRotate2;
				(function wheelAnimation(){
					if (typeof countRotate2 === 'undefined' || countRotate2 > 4) {
						countRotate2=0;
					}
					wheel2.stop().animate(
							{ transform: 'r360,289.389,219.352'}, // Basic rotation around a point. No frills.
							1000, // Nice slow turning rays

							function(){
								wheel2.attr({ transform: 'rotate(0 289.389,219.352)'}); // Reset the position of the rays.
								 // Repeat this animation so it appears infinite.
								 if(countRotate2 >= 4){
								 	wheel2.stop()
								 } else {
								 	wheelAnimation();
								 }
								 countRotate2++;
								}

								);

				})()
			}


			function callAnimate () {
				setTimeout( function() {
					Snap.animate(0, len, function( value ) {
						movePoint = path1.getPointAtLength( value );
						// console.log(movePoint);
						bucketFront.transform( 't' + parseInt(movePoint.x) + ',' + parseInt( movePoint.y+10) );
					}, 5000,mina.easeinout);
				});
				setTimeout( function() {
					var myRotate = 360;




					Snap.animate(0, len2, function( value ) {
						movePoint2 = pathUp.getPointAtLength( value );
						// console.log(movePoint2);
						bucketBack.transform( 't' + parseInt(movePoint2.x) + ',' + parseInt( movePoint2.y+10) );
					}, 5000,mina.easeinout);
				});
			}


			$('.playIt').on('click',function () {
				$('.helper').hide(0);
				callAnimate();
				rotateWheel();
				$msg1.fadeOut(); //hide msg1 box
				if(coutPlayit===0){
					setTimeout(msgBox2,2500); //show msg2 box
					$('.tabOver').delay(3000).fadeIn();

				}
				coutPlayit++;
			})

			/**
			* click handles
			*/
			wheel.click(function () {
				nameSm = "pulley2";
				popUp2(nameSm);
				clickPulley=1;
			})
			wheel2.click(function () {
				nameSm = "pulley2";
				popUp2(nameSm);
				clickPulley=1;
			})
			pulley1.click(function () {
				nameSm = "pulley";
				popUp2(nameSm);
				clickPulley=1;
			})
			pulley2.click(function () {
				nameSm = "pulley";
				popUp2(nameSm);
				clickPulley=1;
			})
			pulley3.click(function () {
				nameSm = "pulley";
				popUp2(nameSm);
				clickPulley=1;
			})
			pulley4.click(function () {
				nameSm = "pulley";
				popUp2(nameSm);
				clickPulley=1;
			})
			screw.click(function () {
				nameSm = "screw";
				popUp2(nameSm);
				clickScrew=1;
			})
			screw2.click(function () {
				nameSm = "screw";
				popUp2(nameSm);
				clickScrew=1;
			})

			lever.click(function () {
				nameSm = "lever";
				popUp2(nameSm);
				clickLever=1;
			})

			/**
			* mouseover handles
			*/
			wheel.mouseover(function () {
				newImage = "pulley2.png";
				text1 = data.string.name2;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})
			wheel2.mouseover(function () {
				newImage = "pulley2.png";
				text1 = data.string.name2;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})
			pulley1.mouseover(function () {
				newImage = "pulley.png";
				text1 = data.string.name2;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})
			pulley2.mouseover(function () {
				newImage = "pulley.png";
				text1 = data.string.name2;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})
			pulley3.mouseover(function () {
				newImage = "pulley.png";
				text1 = data.string.name2;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})
			pulley4.mouseover(function () {
				newImage = "pulley.png";
				text1 = data.string.name2;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})
			screw.mouseover(function () {
				newImage = 'screw.png';
				text1 = data.string.name5;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})
			screw2.mouseover(function () {
				newImage = 'screw.png';
				text1 = data.string.name5;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})

			lever.mouseover(function () {
				newImage = 'breakhandle.png';
				text1 = data.string.name1;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})

			/**
			* mouseover ends
			*/
			ropeway.append(f);
		});

	};

	loadRopeWay();

	/**
	* messages starts
	*/

	(function msgBox1 () {
	  var source=$('#msgBox1-template').html();
	  var template = Handlebars.compile(source);
	  var content = {
	    text1 : data.string.playButtonDesc,
	  }
	  var html = template(content);
	  $('.msg1').html(html);
	})();

	function msgBox2 () {
	  var source=$('#msgBox2-template').html();
	  var template = Handlebars.compile(source);
	  var content = {
	    text1 : data.string.thisIsGravityRopeWay,
	    text2 : data.string.halfOfThis,
	    text3 : data.string.findThem,
	  }
	  var html = template(content);
	  $('.homePop').html(html).show(0);
	}

	function msgBox3 () {
	  var source=$('#msgBox1-template').html();
	  var template = Handlebars.compile(source);
	  var content = {
	    text1 : data.string.goNextPage,
	  }
	  var html = template(content);
	  $('.msg1').html(html).show(0);
	}

	function msgBox4 () {
	  var source=$('#msgBox3-template').html();
	  var template = Handlebars.compile(source);
	  var content = {
	    text1 : data.string.thisIsGaadi,
	    text3 : data.string.clickGaadi,
	  }
	  var html = template(content);
	  $('.homePop').html(html).show(0);
	}

	$('.clickMe').on('click',function () {
	  ropeWayDes();
	})

	/**
	* exit msg box
	*/
	$('.homePop').on('click','.exitMsg',function () {
	  $('.homePop').hide(0);
	})


	/*
	* popUp data
	*/
	CORRECT = data.string.correct;
	WRONG = data.string.wrong;

	function anstype1 (name) {
	  answers = [];
	  $name = name+"Type1Ans";
	  random = Math.floor(Math.random()*10)%3;
	  var count = 1;
	  for (var i = 0; i < 4; i++) {
	    if (random === i) {
	      $id1 = $name+"Correct";
	      answers[i]= "<li class='type1' data-value='correct'>"+data.string[$id1]+"</li>";
	    }
	    else {

	      $id1 = $name+count;
	      answers[i]="<li class='type1'>"+data.string[$id1]+"</li>";
	      count++;
	    }
	  };
	  return answers;
	  // console.log(answers);
	}



	/*
	* array of ans ==>
	*/
	function anstype2 (no) {
	  var answers = [],count;
	  for (var i = 0; i < 6; i++) {
	    count=i+1;
	    $name = "name"+count;
	    if (count === no){
	      answers[i]= "<li class='type1' data-value='correct'>"+data.string[$name]+"</li>";
	    }
	    else {
	      answers[i]= "<li class='type1'>"+data.string[$name]+"</li>";
	    }

	  };
	return answers;
	}
	$quest = data.string.q1;
	var dataPop1 = {
		screw : {
			questions: $quest,
			images : $ref+"/images/screw.png",
			answersGiven: anstype1("screw"),
			continue1 : data.string.continue1
		},
		lever : {
			questions: $quest,
			images : $ref+"/images/breakhandle.png",
			answersGiven: anstype1("lever"),
			continue1 : data.string.continue1
		},
		pulley : {
			questions: $quest,
			images : $ref+"/images/pulley.png",
			answersGiven: anstype1("pulley"),
			continue1 : data.string.continue1
		},
		pulley2 : {
			questions: $quest,
			images : $ref+"/images/pulley2.png",
			answersGiven: anstype1("pulley"),
			continue1 : data.string.continue1
		},
		wheel : {
			questions: $quest,
			images : $ref+"/images/wheel.png",
			answersGiven: anstype1("wheel"),
			continue1 : data.string.continue1
		},
		wedge : {
			questions: $quest,
			images : $ref+"/images/wedge1.png",
			answersGiven: anstype1("wedge"),
			continue1 : data.string.continue1
		},
		plane : {
			questions: $quest,
			images : $ref+"/images/plane.png",
			answersGiven: anstype1("plane"),
			continue1 : data.string.continue1
		},

	};
	console.log("data = ");
	console.log(dataPop1);
	$quest2 = data.string.q2;
	var dataPop2 = {
		lever : {
			questions: $quest2,
			images : $ref+"/images/breakhandle.png",
			answersGiven: anstype2(1),
			continue1 : data.string.continue1
		},
		pulley : {
			questions: $quest2,
			images : $ref+"/images/pulley.png",
			answersGiven: anstype2(2),
			continue1 : data.string.continue1
		},
		pulley2 : {
			questions: $quest2,
			images : $ref+"/images/pulley2.png",
			answersGiven: anstype2(2),
			continue1 : data.string.continue1
		},
		wheel : {
			questions: $quest2,
			images : $ref+"/images/wheel.png",
			answersGiven: anstype2(3),
			continue1 : data.string.continue1
		},
		plane : {
			questions: $quest2,
			images : $ref+"/images/plane.png",
			answersGiven: anstype2(4),
			continue1 : data.string.continue1
		},
		screw : {
			questions: $quest2,
			images : $ref+"/images/screw.png",
			answersGiven: anstype2(5),
			continue1 : data.string.continue1
		},
		wedge : {
			questions: $quest2,
			images : $ref+"/images/wedge1.png",
			answersGiven: anstype2(6),
			continue1 : data.string.continue1
		},
	}

	var dataPop3 = {
		lever : {
			title : data.string.name1,
			defination: data.string.definition1,
			images : $ref+"/images/lever1.gif",
			typeTitle : data.string.typeLever,
			types: [
			data.string.lever1,
			data.string.lever2,
			data.string.lever3,
			]
		},
		pulley : {
			title : data.string.name2,
			defination: data.string.definition2,
			images : $ref+"/images/pulley1.gif",
			types: []
		},
		pulley2 : {
			title : data.string.name2,
			defination: data.string.definition2,
			images : $ref+"/images/pulley1.gif",
			types: []
		},
		wheel : {
			title : data.string.name3,
			defination: data.string.definition3,
			images : $ref+"/images/kite.png",
		},
		plane : {
			title : data.string.name4,
			defination: data.string.definition4,
			images : $ref+"/images/plane.gif",

		},
		screw : {
			title : data.string.name5,
			defination: data.string.definition5,
			images : $ref+"/images/screw.png"
		},
		wedge : {
			title : data.string.name6,
			defination: data.string.definition6,
			images : $ref+"/images/wedge.png"
		}
	}

	$back = data.string.back;
	$effort  = data.string.effort;
	$load = data.string.load;
	$fulcrum = data.string.fulcrum;

	var dataPop4 = {
		lever0 : {
			type : "first",
			title : data.string.lever1,
			defination: data.string.lever1definition,
			images : $ref+"/images/lever01.png",
			back : $back,
			effort : $effort,
			load : $load,
			fulcrum : $fulcrum
		},
		lever1 : {
			type : "second",
			title : data.string.lever2,
			defination: data.string.lever2definition,
			images : $ref+"/images/lever02.png",
			back : $back,
			effort : $effort,
			load : $load,
			fulcrum : $fulcrum
		},
		lever2 : {
			type : "third",
			title : data.string.lever3,
			defination: data.string.lever3definition,
			images : $ref+"/images/lever03.png",
			back : $back,
			effort : $effort,
			load : $load,
			fulcrum : $fulcrum
		},
	}

	/*
	* popUp function template call
	*/
	function popUp(nameGiven) {
		var source   = $("#popup1").html();
		var template = Handlebars.compile(source);
		var popup1 = template(dataPop1[nameGiven]);
		$popUp.html(popup1);
		$popUp.show(0);
	}

	function popUp2(nameGiven) {
		console.log("i am here");
		console.log(nameGiven);
		var source   = $("#popup1").html();
		try {
			var template = Handlebars.compile(source);
			var popup1 = template(dataPop2[nameGiven]);
			console.log(popup1);
		}
		catch (e) {
			console.log(e);
		}

		$popUp.html(popup1);
		$popUp.show(0);
	}

	    function popUp3(nameGiven) {
	      if(nameGiven==='pulley' || nameGiven === 'pulley2'){
	        var source = $("#defination-template3").html();
	      } else if(nameGiven === 'lever'){
	        var source = $("#defination-template").html();
	      }
	      else {
	        var source   = $("#defination-template2").html();
	      }

	      var template = Handlebars.compile(source);
	      var popup1 = template(dataPop3[nameGiven]);
	      $popUp.html(popup1);
	      $popUp.show(0);
	    }

	    function popUp4(number) {
	      var source   = $("#defination-template2").html();
	      var template = Handlebars.compile(source);
	      name = "lever"+number
	       var popup1 = template(dataPop4[name]);
	      $popUp.html(popup1);
	      $popUp.show(0);
	      $popUp.find('.closeBtn').hide(0);
	    }

	/*
	* click calls for popUp's
	*/

	$popUp.on('click',".type1",function  () {
		$this = $(this);
		value = $this.data("value");
		if(value==="correct"){
			$popUp.find(".ansCheck").html(CORRECT);
			$popUp.find(".continue").show(0);
		}
		else {
			$popUp.find(".ansCheck").html(WRONG);
		}
	});

	$popUp.on('click',".continue",function  () {
		console.log("your count is = "+continueCount);
		if (continueCount=== 0) {
			console.log(nameSm);
			popUp(nameSm);
			continueCount++;
		}
		else if (continueCount === 1) {
			popUp3(nameSm);
			continueCount= 0;
		};

	});

	$popUp.on('click',".back",function  () {
		popUp3(nameSm);
	});

	$popUp.on('click','.types li', function () {
		popUp4($(this).data("value"));
	});

	$popUp.on("click",".closeBtn",function  () {
		$popUp.html("");
		$popUp.hide(0);


		if (clickLever===1 && clickScrew===1 && clickPulley===1 && neClick===0) {
			$('.btn2').show(0);
			$('.msgBox2').hide(0);
			neClick++;
			msgBox3();
		};
		if (clickWedge===1 && clickPlane===1 && clickWheel===1) {
			$('.clickMe').show(0);
			$('.msgBox3').hide(0);
			neClick++;
		};
	})

	/**
	* slide left or right,btn
	*/
	var prevNextCount = 0,clickNext=0;
	$('.slideBtn').on('click',".btn",function  () {
		$('.msg1').hide(0);
		$('.helper').hide(0);
		$('.tabSm').hide(0);
		$('.tabOver').show(0);
		var check = $(this).data("value");
		if (check === "prev" && prevNextCount === 1) {
			$('.ropewayall').find('.ropemain').animate({
				"margin-left" : "+=42%"
			},2500);
			prevNextCount = 0;
			$('.btn2,.playIt').fadeIn();
			$('.btn1').fadeOut();
			$('.msgBox4,.msgBox3').fadeOut();
			$('.tabSm').find('.tab1').show(0);
			$('.tabSm').find('.tab2').hide(0);


		}
		else if (check === "next" && prevNextCount === 0) {
			$('.ropewayall').find('.ropemain').animate({
				"margin-left" : "-=42%"
			},2500);
			prevNextCount = 1;
			$('.btn1').fadeIn();
			$('.btn2,.playIt').fadeOut();
			if (clickNext===0) {
				setTimeout(function () {
					msgBox4();
				},2000)

				clickNext++;
			};
			$('.tabSm').find('.tab2').show(0);
			$('.tabSm').find('.tab1').hide(0);
		}
	});

	/**
	* gaadi
	*/

	function gaadiFunc () {
		var gaadi = Snap(".gaadi");
		Snap.load($ref+"/images/gaadi.svg", function(f) {

			/**
			* clickables
			*/
			var wedge = f.select('#wedge');
			var wheels = f.select('#wheels');
			var plane = f.select('#plane');

			/**
			* click handles
			*/
			wheels.click(function () {
				nameSm = "wheel";
				popUp2(nameSm);
				clickWheel=1;
			})

			plane.click(function () {
				nameSm = "plane";
				popUp2(nameSm);
				clickPlane=1;
			})

			/**
			* mouseover handles
			*/
			wheels.mouseover(function () {
				newImage = "wheel.png";
				text1 = data.string.name3;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})

			plane.mouseover(function () {
				newImage = "plane.png";
				text1 = data.string.name4;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})

			/**
			* mouseover ends
			*/
			gaadi.append(f);
		});

	};

	gaadiFunc();


	/**
	* gaadi
	*/

	function wedgeFunc () {
		var wedge = Snap(".wedge");
		Snap.load($ref+"/images/wedge.svg", function(f) {
			var wedges = f.select('#wedge');
			wedges.click(function () {
				nameSm = "wedge";
				popUp2(nameSm);
				clickWedge=1;
			})
			wedges.mouseover(function () {
				newImage = "wedge.png";
				text1 = data.string.name6;
				$('.zoomer').find('img').attr('src',$ref+"/images/"+newImage);
				$('.bottomZoom').find('.title').html("<h2 class='titles'>"+text1+"</h2>");
			})
			wedge.append(f);
		});

	};

	wedgeFunc();


	/**
	* tabs
	*/

	(function firstTabs () {
		var source= $('#listSm-template').html();
		var template = Handlebars.compile(source);
		var content = {
			tabClass : 'tab1',
			name : [data.string.name1,data.string.name2,data.string.name5]
		}

		var content2 = {
			tabClass : 'tab2',
			name : [data.string.name3,data.string.name4,data.string.name6]
		}

		var html = template(content);
		var html2 = template(content2);

		$('.tabSm').html(html+html2);
		$('.tabSm').find('.tab2').hide(0);
	})();

	$('.tabSm').on('click','.tab1 li',function () {
		var $val = $(this).data('value');
		$('.helper').hide(0);
		switch($val){
			case 0:
			$('.lever').show(0);
			break;

			case 1:
			$('.pulley').show(0);
			break;

			case 2:
			$('.screw').show(0);
			break;
		}
	})

	$('.tabSm').on('click','.tab2 li',function () {
		var $val = $(this).data('value');
		$('.helper').hide(0);
		switch($val){
			case 0:
			$('.wheelImg').show(0);
			break;

			case 1:
			$('.plane').show(0);
			break;

			case 2:
			$('.wedge1').show(0);
			break;
		}
	})

	$('.helper').on('click',function () {
		var $types = $(this).data('types');
		$('.helper').hide(0);
		switch ($types) {
			case 'pulley2':
				nameSm = "pulley2";
				popUp2(nameSm);
				clickPulley=1;
			break;

			case 'pulley':
				nameSm = "pulley";
				popUp2(nameSm);
				clickPulley=1;
			break;

			case 'screw':
				nameSm = "screw";
				popUp2(nameSm);
				clickScrew=1;
			break;

			case 'lever':
				nameSm = "lever";
				popUp2(nameSm);
				clickLever=1;
			break;

			case 'wheel':
				nameSm = "wheel";
				popUp2(nameSm);
				clickWheel=1;
			break;

			case 'plane':
				nameSm = "plane";
				popUp2(nameSm);
				clickPlane=1;
			break;

			case 'wedge':
				nameSm = "wedge";
				popUp2(nameSm);
				clickWedge=1;
			break;
		}
	})

	$('.helper').on('mouseenter',function () {
		var $types = $(this).data('types');
		switch ($types) {
			case 'pulley2':

			break;

			case 'pulley':

			break;

			case 'screw':

			break;

			case 'lever':

			break;

			case 'wheel':

			break;

			case 'plane':

			break;

			case 'wedge':

			break;
		}
	})

	$('.clickMe').text(data.string.clickMe);

	/**
	* ropeway description
	*/

	function ropeWayDes () {
		descCloseCount = 1;
		$desc = data.string.ropeWayDesc1+"<br>"+data.string.ropeWayDesc2
		dataRopeWay = {
			title : data.string.ropeWayTitle,
			desc : $desc,
			heading : data.string.ropeWayParts,
			ropewayb : [
			{title:data.string.ropeWayParts1,
				desc: data.string.ropeWayParts1desc},
				{title:data.string.ropeWayParts2,
					desc: data.string.ropeWayParts2desc},
					{title:data.string.ropeWayParts3,
						desc: data.string.ropeWayParts3desc},
						{title:data.string.ropeWayParts4,
						desc: data.string.ropeWayParts4desc}
						],
						image : $ref+"/images/ropeway.png",

					}

					var source   = $("#ropeWay-template").html();
					var template = Handlebars.compile(source);
					var ropeWayPop = template(dataRopeWay);

	  $popUp.html(ropeWayPop);
	  $popUp.show(0);
	}

	$('.clickMe').on('click',function () {
	  ropeWayDes();
	  neClick++;
	})

	$popUp.on('click','.aCloseBtn',function () {
		$popUp.hide(0);
		ole.footerNotificationHandler.lessonEndSetNotification();
	})

	$('.tabOver').on('click',function () {
		$('.tabSm').show(0)
		$(this).hide(0);
	})

	if (navigator.userAgent.indexOf('Firefox') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Firefox') + 8)) >= 3.6){//Firefox
	 //Allow
	 } else {
	 	// alert("please use firefox for good view, thank you");
	 }
}

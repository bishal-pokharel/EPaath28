$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	$(".exe_t").html(data.string.exe_t);
	var counter=0;

	var arrayquestion=new Array(1,2,3,4,5);

	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;


	myquestion(newarray[counter],counter);

	$(".allquestion").on("click",".optObj",function(){


		var isCorrect=parseInt($(this).attr('corr'));

		$(".page p").removeClass('optObj').addClass('optObj2');

		$(".page p[corr='1']").addClass('corrans');
		if(isCorrect==1)
		{
			$(this).append('<img src="'+$ref+'/exercise/images/correct.png" class="Ok" />');
			rightcounter++;
		}
		else
		{
			$(this).append('<img src="'+$ref+'/exercise/images/incorrect.png" class="notOk" />');
			wrongCounter++;
		}

		$("#activity-page-next-btn-enabled").fadeIn(10,function(){

		});

	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;

		if(counter<5)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}

		if(counter>5){
			ole.activityComplete.finishingcall();
		}
	});

});

function myquestion(questionNo,counter)
{
	loadTimelineProgress(6,(counter+1));
	var source   = $("#label-template").html();

	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter)
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(10,function(){

	});
}

function myquestion2(right,wrong)
{
	loadTimelineProgress(6,6);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var $dataval={rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:ole.nepaliNumber(1)+") "+data.string.exe_1, qans: data.string.exe_1_opt1},
			{ques:ole.nepaliNumber(2)+") "+data.string.exe_2, qans: data.string.exe_2_opt3},
			{ques:ole.nepaliNumber(3)+") "+data.string.exe_3, qans: data.string.exe_3_opt1},
			{ques:ole.nepaliNumber(4)+") "+data.string.exe_4, qans: data.string.exe_4_opt3},
			{ques:ole.nepaliNumber(5)+") "+data.string.exe_5, qans: data.string.exe_5_opt2}

		]


	}
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);

	}).delay(100).fadeIn(10,function(){
		$("#activity-page-next-btn-enabled").show(0);
	});
}

function getQuestion($quesNo,counter)
{
	var quesList;
	if($lang=="np")
		var nep=ole.nepaliNumber(counter+1);
	else
		var nep=(counter+1);

	var whatri=whatCorr($quesNo);


	quesList={
		myQuesion:nep+")  "+data.string["exe_"+$quesNo],
		optObj:[{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["exe_"+$quesNo+"_opt1"]},
				{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["exe_"+$quesNo+"_opt2"]},
				{optObj1:"opt_"+$quesNo+"_3", yesno:whatri[2],optObjval:data.string["exe_"+$quesNo+"_opt3"]}
				]
	}

	return quesList;
}

function whatCorr($quesNo)
{
		var whatis=new Array();
	switch($quesNo)
	{
		case 1: whatis[0]=1;whatis[1]=0; whatis[2]=0; break;
		case 2: whatis[0]=0;whatis[1]=0; whatis[2]=1; break;
		case 3: whatis[0]=1;whatis[1]=0; whatis[2]=0; break;
		case 4: whatis[0]=0;whatis[1]=0; whatis[2]=1; break;
		case 5: whatis[0]=0;whatis[1]=1; whatis[2]=0; break;
	}
	return whatis;
}

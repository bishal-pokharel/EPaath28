var $counter=0;
$.fn.dropMe=function ()
{
	var $this=$(this);

	var $id=$this.attr('id');
	
	var $num=$id.split("_");

	var $accept="#drag_"+$num[1];


	var what=3+parseInt($num[1]);
	$("#"+$id).droppable({
        accept: $accept,
        drop: function( event, ui ) {
			ui.draggable.hide(0);
			$("#"+$id).html(data.string['p3_'+what]);
			$counter++;

			if($counter>=7)
			{
				ole.footerNotificationHandler.pageEndSetNotification();
			}
        }
    });

};

$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);
	$("#repeatId").html(getReloadBtn());
	

	$("#p3_1_a").html(data.string.p3_1_a);
	for(var $it=1;$it<=3;$it++)
	{
		$("#p3_"+$it).html(data.string["p3_"+$it]);
	}

	for(var $it=1, $ij=4;$it<=7;$it++, $ij++)
	{
		$("#drag_"+$it).html(data.string["p3_"+$ij]);
	}


	loadTimelineProgress(2,1);
	$("#p3_1").addClass('animated bounceInDown');
	
	setTimeout(function(){
		$("#p3_1").removeClass('animated bounceInDown');
		
		$("#p3_1_a").fadeIn(500,function(){
			$("#activity-page-next-btn-enabled").fadeIn(100,function(){
				
			});
		});

		/**/
	},1000);


	$('.flotcls').draggable({ revert: "invalid"});

	for(var $kk=1;$kk<=7;$kk++)
	{
		$('#drop_'+$kk).dropMe();
	}
	

	$("#repeatId").click(function(){
		location.reload();
	});

	$("#activity-page-next-btn-enabled").click(function(){
		loadTimelineProgress(2,2);
		$(this).fadeOut(10);
		$("#p3_1_a").fadeOut(10,function(){
		
			$("#p3_3").fadeIn(10).addClass('animated pulse');
			setTimeout(function(){

				$("#p3_3").removeClass('animated pulse');

				$("#animationBx").fadeIn(1000);
			},1500);
		});
	});//click

});

var countclick=0;
$(function(){
	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);
	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	for(var $it=1;$it<=10;$it++)
	{
		$("#p3_"+$it).html(data.string["p3_"+$it]);
	}

	var counter=1;



	$("#p3_1").addClass('animated bounceInDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		$(this).removeClass('animated bounceInDown');
		$("#p3_2").fadeIn(10).addClass('animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

			$(this).removeClass('animated pulse');

			$("#animationBx").fadeIn(1000,function(){

				showAnimation(counter);

			});


		});
	});


	$("#activity-page-next-btn-enabled").click(function(){
		counter++;


		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);

		showAnimation(counter);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		counter--;
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		showAnimation(counter);
	});


	$("#newTextwhat").click(function(){
		$(this).fadeOut(0);
		countclick++;

		if(countclick%2===0)
		{
			$('#newgif').delay(10).animate({'width':'5%','left':'50%','top':'50%'},1000).delay(10).fadeOut(10,function(){
				$("#newTextwhat").find("span").removeClass('glyphicon-minus-sign').addClass('glyphicon-plus-sign');
				$("#newTextwhat").fadeIn(10);
			});

		}
		else
		{
			$('#newgif').fadeIn(10).delay(10).animate({'width':'100%','left':'0%','top':'0%'},1000,function(){
				var timestand=new Date().getTime();
				$('#newgif').attr('src',$ref+'/images/page4/3_a.gif?'+timestand);

				$("#newTextwhat").find("span").removeClass('glyphicon-plus-sign').addClass('glyphicon-minus-sign');
				$("#newTextwhat").fadeIn(10);
			});

		}

	});

});

function showAnimation($c)
{
	loadTimelineProgress(5,$c);
	$(".dropAt").removeClass('boxBlinking');
	var timestramp=new Date().getTime();
	switch ($c)
	{
		case 1:

			$("#gifimg").find('img:nth-child(1)').attr('src',$ref+'/images/page4/1.png');
			$("#gifimg").find('.smallGif').attr('src',$ref+'/images/page4/1.gif');
			$("#p3_4, #p3_5, #p3_6").addClass('boxBlinking');

			$("#explanationTxt").fadeOut(10,function(){
				$(this).html(data.string.p4_1).css({'top':'84%'});
			}).delay(1000).fadeIn(100,function(){

				$("#activity-page-next-btn-enabled").fadeIn(10);

			});

			break;
		case 2:


			$("#gifimg").find('img:nth-child(1)').attr('src',$ref+'/images/page4/2.png');
			$("#gifimg").find('.smallGif').attr('src',$ref+'/images/page4/2.gif');
			$("#newTextwhat").fadeOut(10);
			$("#newgif").fadeOut(10);


				$("#p3_7").addClass('boxBlinking');

				$("#explanationTxt").fadeOut(10,function(){
					$(this).html(data.string.p4_2).css({'top':'84%'});
				}).delay(1000).fadeIn(100,function(){

					$("#activity-page-next-btn-enabled").fadeIn(10);
					$("#activity-page-prev-btn-enabled").fadeIn(10);

				});



			break;
		case 3:
			$("#gifimg").find('img:nth-child(1)').attr('src',$ref+'/images/page4/3.png');
			$("#gifimg").find('.smallGif').attr('src',$ref+'/images/page4/3.gif');
			$("#p3_9").addClass('boxBlinking');
			countclick=0;

			$("#explanationTxt").fadeOut(10,function(){
				$(this).html(data.string.p4_3).css({'top':'84%'});
			}).delay(1000).fadeIn(100,function(){

				$("#activity-page-next-btn-enabled").fadeIn(10);
				$("#activity-page-prev-btn-enabled").fadeIn(10);

				$("#newTextwhat").find("span").removeClass('glyphicon-minus-sign').addClass('glyphicon-plus-sign');
				$("#newTextwhat").fadeIn(10);
			});
			break;
		case 4:
			ole.footerNotificationHandler.hideNotification();
			$("#newTextwhat").fadeOut(10);
			$("#newgif").fadeOut(10);
			$("#gifimg").find('img:nth-child(1)').attr('src',$ref+'/images/page4/4.png');
			$("#gifimg").find('.smallGif').attr('src',$ref+'/images/page4/4.gif');

			$("#p3_8").addClass('boxBlinking');

			$("#explanationTxt").fadeOut(10,function(){
				$(this).html(data.string.p4_4).css({'top':'84%'});
			}).delay(1000).fadeIn(100,function(){
				$("#activity-page-next-btn-enabled").fadeIn(10);
				$("#activity-page-prev-btn-enabled").fadeIn(10);


			});


			break;
		case 5:
			$("#gifimg").find('img:nth-child(1)').attr('src',$ref+'/images/page4/5.gif?'+timestramp);
			$("#gifimg").find('.smallGif').fadeOut(10);
			$("#p3_10").addClass('boxBlinking');
			$("#explanationTxt").fadeOut(10,function(){
				$(this).html(data.string.p4_5).css({'top':'84%'});
			}).delay(50).fadeIn(5000,function(){
				$("#activity-page-prev-btn-enabled").fadeIn(10);
				ole.footerNotificationHandler.pageEndSetNotification();
			});


			break;
	}
}

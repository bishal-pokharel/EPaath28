$(function(){
	var $wahtar=getSubpageMoveButton($lang,"next");
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		 quest++;
		 $(this).fadeOut(10);
		 $("#activity-page-prev-btn-enabled").fadeOut(10);
		 getFunction(quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		 quest--;
		 $(this).fadeOut(10);
		 $("#activity-page-next-btn-enabled").fadeOut(10);
		 getFunction(quest);
	 });

	 function getFunction(qno){
			loadTimelineProgress(3,qno);
			switch(qno)
			{
					case 1:
						$("#activity-page-next-btn-enabled").fadeIn(10);
						$('#lifeContent').prepend('<img class="bg_full" src='+$ref+'/images/page1/animal_life_process.png /><p class="front_page_title">'+data.lesson.chapter+'</p>');
						break;


					case 2:
						$('.bg_full,.front_page_title').hide(0);
						$("#activity-page-next-btn-enabled").fadeIn(10);
						break;
					case 3:
						case3();
						break;
			}
		}


	var countCng=0;



	for(var $it=1;$it<=10;$it++)
	{
		$("#p1_"+$it).html(data.string["p1_"+$it]);
	}

	$("#p1_52").html(data.string["p1_52"]);

	$("#p1_2_a").html(data.string["p1_2_a"]);

	// loadTimelineProgress(2,1);


	$(".radio").change(function() {
		countCng++;



		$id=$(this).attr('id');

		$whatImg=$id.split("_");

		var $dataVal=getDataVal(parseInt($whatImg[1]));

		var $otherval = {
				labelObj:$dataVal
			}

		$("#img_left").find('img').attr('src',$ref+'/images/page1/'+$whatImg[1]+".png");


		var source=$("#label-template").html();
		var template=Handlebars.compile(source);

		var html=template($otherval);


		$("div").remove('.labelsObj');
		$("#img_left").append(html);
		if(countCng==8)
		{
			ole.footerNotificationHandler.pageEndSetNotification();

		}


	});


	function case3(){
		$("#p1_2_a").fadeOut(10);
		$("#p1_2").fadeOut(10,function(){


			$("#animationBx").fadeIn(500,function(){
				$("#p1_52").fadeIn(10);
			});

		});
	}

	function getDataVal($i)
	{

		var $dataVal;

		switch($i)
		{

			case 1:
				$dataVal=[
						{"labelId":'lab1_a' , "labels":data.string.p1_11},
						{"labelId":'lab1_b' , "labels":data.string.p1_12},
						{"labelId":'lab1_c' , "labels":data.string.p1_13}
					];
					break;
			case 2:
				$dataVal=[
						{"labelId":'lab2_a' , "labels":data.string.p1_14},
						{"labelId":'lab2_b' , "labels":data.string.p1_15},
						{"labelId":'lab2_c' , "labels":data.string.p1_16},
						{"labelId":'lab2_d' , "labels":data.string.p1_17},
						{"labelId":'lab2_e' , "labels":data.string.p1_18},
						{"labelId":'lab2_f' , "labels":data.string.p1_19},
						{"labelId":'lab2_g' , "labels":data.string.p1_20},
						{"labelId":'lab2_h' , "labels":data.string.p1_21}
					];
					break;
			case 3:
				$dataVal=[
						{"labelId":'lab3_a' , "labels":data.string.p1_22},
						{"labelId":'lab3_b' , "labels":data.string.p1_23},
						{"labelId":'lab3_c' , "labels":data.string.p1_24},
						{"labelId":'lab3_d' , "labels":data.string.p1_25}
					];
					break;
			case 4:
				$dataVal=[
						{"labelId":'lab4_a' , "labels":data.string.p1_26},
						{"labelId":'lab4_b' , "labels":data.string.p1_27}
					];
					break;
			case 5:
				$dataVal=[
						{"labelId":'lab5_a' , "labels":data.string.p1_28},
						{"labelId":'lab5_b' , "labels":data.string.p1_29},
						{"labelId":'lab5_c' , "labels":data.string.p1_30},
						{"labelId":'lab5_d' , "labels":data.string.p1_31},
						{"labelId":'lab5_e' , "labels":data.string.p1_32},
						{"labelId":'lab5_f' , "labels":data.string.p1_33},
						{"labelId":'lab5_g' , "labels":data.string.p1_34}
					];
					break;
			case 6:
				$dataVal=[
						{"labelId":'lab6_a' , "labels":data.string.p1_35},
						{"labelId":'lab6_b' , "labels":data.string.p1_36},
						{"labelId":'lab6_c' , "labels":data.string.p1_37},
						{"labelId":'lab6_d' , "labels":data.string.p1_38}
					];
					break;
			case 7:
				$dataVal=[
						{"labelId":'lab7_a' , "labels":data.string.p1_39},
						{"labelId":'lab7_b' , "labels":data.string.p1_40},
						{"labelId":'lab7_c' , "labels":data.string.p1_41},
						{"labelId":'lab7_d' , "labels":data.string.p1_42}
					];
					break;
			case 8:
				$dataVal=[
						{"labelId":'lab8_a' , "labels":data.string.p1_43},
						{"labelId":'lab8_b' , "labels":data.string.p1_44},
						{"labelId":'lab8_c' , "labels":data.string.p1_45},
						{"labelId":'lab8_d' , "labels":data.string.p1_46},
						{"labelId":'lab8_e' , "labels":data.string.p1_47},
						{"labelId":'lab8_f' , "labels":data.string.p1_48},
						{"labelId":'lab8_g' , "labels":data.string.p1_49},
						{"labelId":'lab8_h' , "labels":data.string.p1_50},
						{"labelId":'lab8_i' , "labels":data.string.p1_51}
					];
					break;

		}


		return $dataVal;
	}

});

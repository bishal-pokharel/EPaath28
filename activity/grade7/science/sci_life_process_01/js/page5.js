$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	for(var $it=1;$it<=3;$it++)
	{
		$("#p5_"+$it).html(data.string["p5_"+$it]);
	}

	var counter=0;
	var cntcnt=1;

	$("#p5_1").addClass('animated bounceInDown');

	loadTimelineProgress(6,cntcnt);
	setTimeout(function(){
		$(this).removeClass('animated bounceInDown');
		$("#p5_2").fadeIn(500,function(){
			$("#activity-page-next-btn-enabled").fadeIn(100,function(){});
		});
	},1500);
	//
	$("#activity-page-next-btn-enabled").click(function(){
		cntcnt++;
		loadTimelineProgress(6,cntcnt);
		console.log("cntCnt=="+cntcnt)
		$(this).removeClass('animated bounce').fadeOut(10);
		$("#p5_2").fadeOut();
			$("#p5_3").fadeIn(10);
			setTimeout(function(){
				$("#animationBx").fadeIn(100,function(){
				});
			},1500);
	});

	//
	$("#activity-page-next-btn-enabled").click(function(){
		counter++;
		console.log("counter=="+counter)
		loadTimelineProgress(6,(counter+1));
		$(this).fadeOut(10);
		showAnimation(counter);
	});

});

function showAnimation($c)
{

	switch ($c)
	{
		case 1:


			$("#titleTxt1").fadeOut(10,function(){

				$(this).html(data.string.p5_4);
				$(this).removeClass('animated pulse');

			}).delay(10).fadeIn(100,function(){

				$(this).addClass('animated pulse');
			});

			$("#expText").delay(10).fadeOut(10,function(){
				$(this).html(data.string.p5_5);
			}).delay(1500).fadeIn(100,function(){
				$("#activity-page-next-btn-enabled").fadeIn(10);
			});


			break;
		case 2:

			$("#titleTxt1").fadeOut(10,function(){

				$(this).html(data.string.p5_6);
				$(this).removeClass('animated pulse');

			}).delay(10).fadeIn(100,function(){

				$(this).addClass('animated pulse');
			});

			$("#dropImg").removeClass('imgclass1').addClass('imgclass2');
			$("#dropImg").find('img').attr('src',$ref+'/images/page2/9.gif');



			$("#expText").delay(10).fadeOut(10,function(){

				$(this).html(data.string.p5_7);

			}).delay(1500).fadeIn(100,function(){
				$("#activity-page-next-btn-enabled").fadeIn(10);
			});

			break;
		case 3:
			$("#dropImg").removeClass('imgclass2').addClass('imgclass3');
			$("#dropImg").find('img').attr('src',$ref+'/images/page5/1.gif');

			$("#titleTxt1").fadeOut(10,function(){

				$(this).html(data.string.p5_8);
				$(this).removeClass('animated pulse');

			}).delay(10).fadeIn(100,function(){

				$(this).addClass('animated pulse');
			});

			$("#expText").delay(10).fadeOut(10,function(){
				$(this).html(data.string.p5_9);
			}).delay(1500).fadeIn(100,function(){
				$("#activity-page-next-btn-enabled").fadeIn(10);
			});

			break;
		case 4:

			$("#dropImg").removeClass('imgclass3').addClass('imgclass4');
			$("#dropImg").find('img').attr('src',$ref+'/images/page5/2.gif');

			$("#titleTxt1").fadeOut(10,function(){

				$(this).html(data.string.p5_10);
				$(this).removeClass('animated pulse');

			}).delay(10).fadeIn(100,function(){

				$(this).addClass('animated pulse');
			});

			$("#expText").delay(10).fadeOut(10,function(){
				$(this).html(data.string.p5_11);
			}).delay(1500).fadeIn(100,function(){
				$("#activity-page-next-btn-enabled").fadeIn(10);
			});


			break;

		case 5:

			$("#dropImg").hide(0);


			$("#titleTxt1").fadeOut(10);

			$("#expText").delay(10).fadeOut(10,function(){
				$(this).html(data.string.p5_12);
				$(this).addClass('extraCss');
			}).delay(1500).fadeIn(100,function(){
				ole.footerNotificationHandler.lessonEndSetNotification();
			});
			break;


	}
}

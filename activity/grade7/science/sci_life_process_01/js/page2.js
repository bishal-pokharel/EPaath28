$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);
	// $("#nextidBtn").html($whatnextbtn);
	$("#reloadidBtn").html(getReloadBtn());


	var totalpage=4,pageCounter=1;
	var html;
	var mybigpageCounter=1;

	for(var $it=1;$it<=3;$it++)
	{
		$("#p2_"+$it).html(data.string["p2_"+$it]);
	}
	$("#p2_1_a").html(data.string.p2_1_a);
	$("#p2_1_b").html(data.string.p2_1_b);


	$("#p2_1").addClass('animated bounceInDown');

	loadTimelineProgress(5,1);

	setTimeout(function(){
		$("#p2_1").removeClass('animated bounceInDown');

		$("#p2_1_a").fadeIn(500,function(){

			$("#p2_1_b").fadeIn(10);

			$("#activity-page-next-btn-enabled").fadeIn(100,function(){

			});
		});
	},1500);

	$("#activity-page-next-btn-enabled").click(function(){

		loadTimelineProgress(5,(mybigpageCounter+1));
		$(this).fadeOut(10);
		$("#p2_1_b").fadeOut(10);
		$("#p2_1_a").fadeOut(10,function(){


			$("#p2_2").fadeIn(10).addClass('animated pulse');
			setTimeout(function(){

				$("#p2_2").removeClass('animated pulse');
				 allThis(pageCounter,totalpage);

			},1500);
		});

	});

	$("#lifeContent").on('click',"#nextidBtn",function(){


		$(this).fadeOut(1).removeClass('animated wobble');
		pageCounter++;

		loadTimelineProgress(5,(pageCounter+1));

		$("#p2_1").html(data.string.p2_2);

		var etcs;
		if(pageCounter==2)
		{
			etcs=data.string.p2_6;
		}
		else if(pageCounter==3)
		{etcs=data.string.p2_7;
		}
		else if(pageCounter==4)
		{
				etcs=data.string.p2_8;
		}

		$("#explain_this").fadeOut(0,function(){
			$(this).html(etcs);
		}).delay(1000).fadeIn(10);




		if(pageCounter==2)
		{
			$("#fullimg").fadeOut(10,function(){
				$(this).html("<img src='"+$ref+"/images/page2/9.gif' class='allimg' />");
			}).delay(1000).fadeIn(10);

			$("#p2_2").html(data.string.p2_3).addClass('extrafont animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$(this).removeClass('animated pulse');
			});
		}
		else if(pageCounter==3)
		{


			$("#p2_2").html(data.string.p2_4).addClass('extrafont animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$(this).removeClass('animated pulse')


			});
		}
		else if(pageCounter==4)
		{


			$("#p2_2").html(data.string.p2_5).addClass('extrafont animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$(this).removeClass('animated pulse')


			});
		}
		 getPagignator2(pageCounter,totalpage,4000);



	});


	$("#lifeContent").on('click',"#reloadidBtn",function(){
		 location.reload();
	 });


});
function allThis(pageCounter,totalpage)
{

	html=getThisPage(pageCounter);
	$("#animationBx").html(html).delay(100).fadeIn(10,function()
	{
		getAnimation(pageCounter,totalpage);
	});
}
function getThisPage($pnum)
{


	var datas=getDataValue($pnum);

	if($pnum>=2)
		var source=$("#template-2").html();
	else
	var source=$("#template-"+$pnum).html();

	var template=Handlebars.compile(source);
	var html=template(datas);


	return html;


}
function getDataValue($pnum)
{

	var dataValue;

	switch($pnum)
	{
		case 1:
			dataValue={
				p2_3:data.string.p2_3,
				p2_4:data.string.p2_4,
				p2_5:data.string.p2_5,
				img1:$ref+"/images/page2/1.png",
				img2:$ref+"/images/page2/2.png",
				img4:$ref+"/images/page2/8.png",
				img3:$ref+"/images/page2/3.png"
			}
			break;
		case 2:

			break;

	}

	return dataValue;

}

function getAnimation($pnum,$totalPage)
{
	switch($pnum)
	{
		case 1:
			page_1($pnum,$totalPage);
			break;
	}


}

function page_1($pnum,$totalPage)
{
	$("#animationBx").find("#face img").delay(1000).animate({"left": "362%","top": "26%"},1000,function()
	{

		$("#fullimg").append("<img src='"+$ref+"/images/page2/4.png' class='face1' />");
		$(this).fadeOut(10).removeAttr('style').fadeIn(10,function(){

			$("#animationBx").find("#pipe img").delay(1000).animate({"left": "350%","top": "-55%"},1000,function()
			{
				$("#fullimg").append("<img src='"+$ref+"/images/page2/5.png' />");
				$(this).fadeOut(10).removeAttr('style').fadeIn(10,function(){

					$("#animationBx").find("#lungs img").delay(1000).animate({"left": "310%","top": "-50%"},1000,function()
					{
						$("#fullimg").append("<img src='"+$ref+"/images/page2/6.png' />");
						$(this).fadeOut(10).removeAttr('style').fadeIn(10,function(){

							$("#fullimg").find('.face1').attr('src',$ref+'/images/page2/7.png');

							$("#fullimg").delay(1100).fadeOut(10,function(){
								$(this).html("<img src='"+$ref+"/images/page2/8.png' class='allimg' />")
							}).fadeIn(10,function(){
								getPagignator2($pnum,$totalPage,1000);
							});




						});//fadein
					});//animate


				});//fadeIn
			});//animate


		});//fadein
	});//face animate


}


function getPagignator2($pnum,$totalPage,$delayVal)
{


	if($pnum<$totalPage)
	{
		$("#lifeContent").find('#nextidBtn').delay($delayVal).fadeIn(10).html(data.string.learnmore);

	}
	else
	{
			$("#lifeContent").find('#reloadidBtn').delay($delayVal).fadeIn(10,function(){
				ole.footerNotificationHandler.pageEndSetNotification();
			});

	}
}

$(function(){

  var body = $("#mysample"),
      universe = $("#universe"),
      solarsys = $("#solar-system");

  
   
    body.removeClass('opening').addClass("view-3D").delay(2000).queue(function() {
      $(this).removeClass('hide-UI').addClass("set-speed");
      $(this).dequeue();
    });

    $( ".planet" ).each(function( index ) {
      var planetName= $( this ).parent().parent('.orbit').attr('id');
          $(this).find('dt').html(data.string["p7_1_"+(index+1)]);
    });

    $("#sun").find('dt').html(data.string["p7_1_0"]);
  
  
  $(".orbit, #sun").click(function(e)
  {
      var ref = $(this).attr('id');
      solarsys.removeClass().addClass(ref);
      $(".orbit, #sun").find('.infos').css({'opacity':0});
      $(this).find('.infos').css({'opacity':1});

      e.preventDefault();

      var getMydata=getData(ref);
      var source = $("#first-template").html();
      var template = Handlebars.compile(source);
      var html = template(getMydata);

      
      $(".myInfoBox").html(html).fadeIn(500,function()
      {
          //$(".absolutecls").hide(0);
      });
       
  });

  $(".planet").click(function(e)
  {
      var ref = $(this).parent().parent('.orbit').attr('id');
      solarsys.removeClass().addClass(ref);
      $(".orbit, #sun").find('.infos').css({'opacity':0});
      $(this).find('.infos').css({'opacity':1});
      
      e.preventDefault();

      var getMydata=getData(ref);
      var source = $("#first-template").html();
      var template = Handlebars.compile(source);
      var html = template(getMydata);

      
      $(".myInfoBox").html(html).fadeIn(500,function()
      {
         // $(".absolutecls").hide(0);
      });
      

      
  });



/*
  $(".myInfoBox").on('click','.mybtnbx',function(){
    
    var idVal=$(this).attr('id');
    var showWhat, hideWhat;


    if(idVal=="absoluteShow")
    {
      showWhat="absolutecls";
      hideWhat="relativecls";
      $(this).addClass('myBxActive');
       $("#relativeShow").removeClass('myBxActive');
    }  
    else {
      showWhat="relativecls";
      hideWhat="absolutecls";
      $(this).addClass('myBxActive');
       $("#absoluteShow").removeClass('myBxActive');
    }

    $("."+showWhat).show(0);
    $("."+hideWhat).hide(0);
      e.preventDefault();
  });*/


  $(".myInfoBox").on('click','.closeBtn',function(){
     $(".myInfoBox").fadeOut(500);
      $(".orbit, #sun").find('.infos').css({'opacity':0});
      solarsys.removeClass();
       e.preventDefault()
  });
  

});


function getData($cnt)
{
  var datVals=[];
  switch($cnt)
  {
    case "sun":
      datVals={
        imgarcs:$ref+"/images/page13/real/sun.png",
        relativeVal:"yes",
        relativeMe: data.string["p_13_1"],
        absoluteMe:data.string["p_13_2"],
        myRealVal:[
            {topic:data.string["p_13_3"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_1_1"],absolutecls:"absolutecls",absolute:data.string["p_13_1_1a"]},
            {topic:data.string["p_13_4"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_1_2"],absolutecls:"absolutecls",absolute:data.string["p_13_1_2a"]},
            {topic:data.string["p_13_10"],colcls:2, relativecls:"firstshow", relative:data.string["p_13_1_3"]},
            {topic:data.string["p_13_7"],colcls:2, relativecls:"firstshow", relative:data.string["p_13_1_4"]}

        ]
      };
      break;
    case "mercury":
        datVals={
          imgarcs:$ref+"/images/page13/real/mercury.png",
          relativeVal:"yes",
          relativeMe:data.string["p_13_1"],
          absoluteMe:data.string["p_13_2"],
         myRealVal:[
              {topic:data.string["p_13_3"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_1"],absolutecls:"absolutecls",absolute:data.string["p_13_2_1a"]},
              {topic:data.string["p_13_4"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_2"],absolutecls:"absolutecls",absolute:data.string["p_13_2_2a"]},
              {topic:data.string["p_13_5"],colcls:2, relativecls:"firstshow", relative:data.string["p_13_2_3"]},
              {topic:data.string["p_13_6"],colcls:2, relativecls:"firstshow", relative:data.string["p_13_2_4"]},
              {topic:data.string["p_13_7"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_5"],absolutecls:"absolutecls",absolute:data.string["p_13_2_5a"]},
              {topic:data.string["p_13_8"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_6"],absolutecls:"absolutecls",absolute:data.string["p_13_2_6a"]},
              {topic:data.string["p_13_9"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_7"],absolutecls:"absolutecls",absolute:data.string["p_13_2_7a"]}
          ]
        };
        break;   

    case "venus":
        datVals={
          imgarcs:$ref+"/images/page13/real/venus.png",
          relativeVal:"yes",
          relativeMe:data.string["p_13_1"],
          absoluteMe:data.string["p_13_2"],
         myRealVal:[
              {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_1"],absolutecls:"absolutecls",absolute:data.string["p_13_3_1a"]},
              {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_2"],absolutecls:"absolutecls",absolute:data.string["p_13_3_2a"]},
              {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_3_3"]},
              {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_4"],absolutecls:"absolutecls",absolute:data.string["p_13_3_4a"]},
              {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_5"],absolutecls:"absolutecls",absolute:data.string["p_13_3_5a"]},
              {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_6"],absolutecls:"absolutecls",absolute:data.string["p_13_3_6a"]}
          ]
        };
        break;  

    case "earth":
          datVals={
            imgarcs:$ref+"/images/page13/real/earth.png",
            relativeVal:"yes",
            relativeMe:data.string["p_13_1a"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
                {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_4_1"],absolutecls:"absolutecls",absolute:data.string["p_13_4_1a"]},
                {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_4_2"],absolutecls:"absolutecls",absolute:data.string["p_13_4_2a"]},
                {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_4_3"]},
                {topic:data.string["p_13_7"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_4_4"]},
                {topic:data.string["p_13_8"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_4_5"]},
                {topic:data.string["p_13_9"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_4_6"]}
            ]
          };
          break; 
    case "mars":
          datVals={
            imgarcs:$ref+"/images/page13/real/mars.png",
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
                {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_1"],absolutecls:"absolutecls",absolute:data.string["p_13_5_1a"]},
                {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_2"],absolutecls:"absolutecls",absolute:data.string["p_13_5_2a"]},
                {topic:data.string["p_13_11"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_5_3"]},
                {topic:data.string["p_13_12"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_5_4"]},
                {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_5"],absolutecls:"absolutecls",absolute:data.string["p_13_5_5a"]},
                {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_6"],absolutecls:"absolutecls",absolute:data.string["p_13_5_6a"]},
                {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_7"],absolutecls:"absolutecls",absolute:data.string["p_13_5_7a"]}
            ]
          };
          break; 
    case "jupiter":
          datVals={
            imgarcs:$ref+"/images/page13/real/jupiter.png",
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
                {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_1"],absolutecls:"absolutecls",absolute:data.string["p_13_6_1a"]},
                {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_2"],absolutecls:"absolutecls",absolute:data.string["p_13_6_2a"]},
                {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_6_3"]},
                {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_4"],absolutecls:"absolutecls",absolute:data.string["p_13_6_4a"]},
                {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_5"],absolutecls:"absolutecls",absolute:data.string["p_13_6_5a"]},
                {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_6"],absolutecls:"absolutecls",absolute:data.string["p_13_6_6a"]}
            ]
          };
          break; 

    case "saturn":
          datVals={
            imgarcs:$ref+"/images/page13/real/saturn.png",
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
                {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_1"],absolutecls:"absolutecls",absolute:data.string["p_13_7_1a"]},
                {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_2"],absolutecls:"absolutecls",absolute:data.string["p_13_7_2a"]},
                {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_7_3"]},
                {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_4"],absolutecls:"absolutecls",absolute:data.string["p_13_7_4a"]},
                {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_5"],absolutecls:"absolutecls",absolute:data.string["p_13_7_5a"]},
                {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_6"],absolutecls:"absolutecls",absolute:data.string["p_13_7_6a"]}
            ]
          };
          break;
    case "uranus":
          datVals={
            imgarcs:$ref+"/images/page13/real/uranus.png",
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
                {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_1"],absolutecls:"absolutecls",absolute:data.string["p_13_8_1a"]},
                {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_2"],absolutecls:"absolutecls",absolute:data.string["p_13_8_2a"]},
                {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_8_3"]},
                {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_4"],absolutecls:"absolutecls",absolute:data.string["p_13_8_4a"]},
                {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_5"],absolutecls:"absolutecls",absolute:data.string["p_13_8_5a"]},
                {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_6"],absolutecls:"absolutecls",absolute:data.string["p_13_8_6a"]}
            ]
          };
          break;   
    case "neptune":
          datVals={
            imgarcs:$ref+"/images/page13/real/neptune.png",
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
                {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_1"],absolutecls:"absolutecls",absolute:data.string["p_13_9_1a"]},
                {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_2"],absolutecls:"absolutecls",absolute:data.string["p_13_9_2a"]},
                {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_9_3"]},
                {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_4"],absolutecls:"absolutecls",absolute:data.string["p_13_9_4a"]},
                {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_5"],absolutecls:"absolutecls",absolute:data.string["p_13_9_5a"]},
                {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_6"],absolutecls:"absolutecls",absolute:data.string["p_13_9_6a"]}
            ]
          };
          break;                 
  }
  
  return datVals;

}
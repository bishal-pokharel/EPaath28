(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevbtn = $(".prevButton"),
		countNext = 1,
		$total_page = 3;

	
	$nextBtn.html(getSubpageMoveButton($lang,"next"));
	$prevbtn.html(getSubpageMoveButton($lang,"prev"));


	getHtml(countNext)

	$nextBtn.click(function(){
		$(this).hide(0);
		countNext++;
		getHtml(countNext);

	});

	$prevbtn.click(function(){
		$(this).hide(0);
		countNext--;
		getHtml(countNext);

	});


	

	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		 var source = $("#first-template").html()
		
		var template = Handlebars.compile(source);
		var content= getdata();

		var html = template(content);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){
			if(countNext>1)
			{
				$prevbtn.show(0);

			}
			if(countNext<$total_page)
			{
				$nextBtn.show(0);
			}
			else
			{
				ole.footerNotificationHandler.pageEndSetNotification();
			}

		});

	}
	function getdata()
	{
		var content;
		switch(countNext)
		{
			case 1:
				content = { 
							headtitle:data.string.p5_1,
							text_1_1_cls:"text_1_1",
							text_1_1:data.string.p5_2,
							text_1_2:data.string.p5_3,
							text_1_3:data.string.p5_3_1,
							img1:$ref+"/images/page5/5.jpg",
							headtitlecls:"another_head"
					}

				break;
			case 2:
				content = { 
							
							imgsrc:$ref+"/images/page5/1.jpg",
							fullimg:"fullimg2",							
							text_1_1:data.string.p5_4,
							text_1_1_cls:"text_1_12"
					}
				break;
			case 3:

				content = { 
							headtitle:data.string.p5_1,
							defination:data.string.p5_5,
							imgsrc:$ref+"/images/page5/4.jpg",
							fullimg:"fullimg",
							labelObj:[{name:"label1",labVal:data.string.p5_6,labellang:"label-"+$lang},
							{name:"label2",labVal:data.string.p5_7,labellang:"label-"+$lang},
							{name:"label3",labVal:data.string.p5_8,labellang:"label-"+$lang},
							{name:"label4",labVal:data.string.p5_9,labellang:"label-"+$lang},
							{name:"label5",labVal:data.string.p5_10,labellang:"label-"+$lang},
							{name:"label6",labVal:data.string.p5_11,labellang:"label-"+$lang},
							{name:"label7",labVal:data.string.p5_12,labellang:"label-"+$lang},
							{name:"label8",labVal:data.string.p5_13,labellang:"label-"+$lang}]	
					}

				
				break;
			

		}

		return content
	}


	
})(jQuery);




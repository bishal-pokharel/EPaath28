(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevbtn = $(".prevButton"),
		countNext = 1,
		$total_page = 6;

	
	$nextBtn.html(getSubpageMoveButton($lang,"next"));
	$prevbtn.html(getSubpageMoveButton($lang,"prev"));


	getHtml(countNext)

	$nextBtn.click(function(){
		$(this).hide(0);
		countNext++;
		getHtml(countNext);

	});
	$prevbtn.click(function(){
		$(this).hide(0);
		countNext--;
		getHtml(countNext);

	});

	

	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		 var source = $("#first-template").html()
		
		var template = Handlebars.compile(source);
		var content= getdata();

		var html = template(content);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){
			if(countNext>1)
			{
				$prevbtn.show(0);

			}
			if(countNext<$total_page)
			{
				$nextBtn.show(0);
			}
			else
			{
				ole.footerNotificationHandler.pageEndSetNotification();
			}

		});

	}
	function getdata()
	{
		var content;
		switch(countNext)
		{
			case 1:
				content = { 
							headtitle:data.string.p2_1,
							imgsrc:$ref+"/images/page2/1.jpg",
							fullimg:"fullimg",
							text_1_1:data.string.p2_1_1,
							text_1_2:data.string.p2_1_2,
							text_1_3:data.string.p2_1_3,
							text_1_1_cls:"text_1_1"
					}

				break;
			case 2:
				content = { 
							headtitle:data.string.p2_1_0,
							diffbetween:"yes",
							diffsrc1:$ref+"/images/page2/2.jpg",
							diffimg:"diffimg",
							diffsrc2:$ref+"/images/page2/3.jpg",
							imgclass:"myExSrc1",
							dBetween:[
								{star:data.string.p2_2_1,planet:data.string.p2_3_1}
							]
					}
				break;
			case 3:

				content = { 
							headtitle:data.string.p2_1_0,
							diffbetween:"yes",
							diffsrc1:$ref+"/images/page2/4.jpg",
							diffimg:"diffimg",
							diffsrc2:$ref+"/images/page2/5.jpg",
							imgclass:"myExSrc1",
							dBetween:[
								{star:data.string.p2_2_2,planet:data.string.p2_3_2}
							]
					}

				
				break;
			case 4:
				content = { 
							headtitle:data.string.p2_1_0,
							diffbetween:"yes",
							diffsrc1:$ref+"/images/page2/6.jpg",
							diffimg:"diffimg",
							diffsrc2:$ref+"/images/page2/8.jpg",
							imgclass:"myExSrc1",
							dBetween:[
								{star:data.string.p2_2_3,planet:data.string.p2_3_3}
								
							]
					}
					break;

			case 5:
				content = { 
							headtitle:data.string.p2_1_0,
							diffbetween:"yes",
							diffsrc1:$ref+"/images/page2/6.jpg",
							diffimg:"diffimg",
							diffsrc2:$ref+"/images/page2/7.jpg",
							imgclass:"myExSrc2",
							dBetween:[
								{star:data.string.p2_2_4,planet:data.string.p2_3_4},
								{star:data.string.p2_2_5,planet:data.string.p2_3_5}
							]
					}
					break;
			break;

			case 6:
				content = { 
							headtitle:data.string.p2_1_0,
							text_1_1:data.string.p2_3_6,
							text_1_2:data.string.p2_3_7,
							text_1_1_cls:"text_1_0"
					}

				break;
			

		}

		return content
	}


	
})(jQuery);




(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevbtn = $(".prevButton"),
		countNext = 1,
		$total_page = 5;

	
	$nextBtn.html(getSubpageMoveButton($lang,"next"));
	$prevbtn.html(getSubpageMoveButton($lang,"prev"));


	getHtml(countNext)

	$nextBtn.click(function(){
		$(this).hide(0);
		countNext++;
		getHtml(countNext);

	});
	$prevbtn.click(function(){
		$(this).hide(0);
		countNext--;
		getHtml(countNext);

	});


	

	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		 var source = $("#first-template").html()
		
		var template = Handlebars.compile(source);
		var content= getdata();

		var html = template(content);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){
			if(countNext>1)
			{
				$prevbtn.show(0);

			}

			if(countNext<$total_page)
			{
				$("#activity-page-next-btn-enabled").show(0);
			}
			else
			{
				ole.footerNotificationHandler.pageEndSetNotification();
			}

		});

	}
	function getdata()
	{
		var content;
		switch(countNext)
		{
			/*case 1:
				content = { 
						text_1_1:data.string.p9_1,
						text_1_1_cls:"text_in_middle"
				}

				break;*/
			case 1:
				content = { 
							text_div:data.string.p9_2,
							topSrc:"images/timetothink/timetothink1.png",
							think:data.string.p9_4,
							imgsrc:$ref+"/images/page9/cloud01.jpg",							
							fullimg:"fullimg",
							text_1_2_cls:"text_1_2_hide",
							text_1_2:data.string.p9_3
					}
				break;
			case 2:
				content = { 
							text_div:data.string.p9_2,
							topSrc:"images/timetothink/timetothink1.png",
							think:data.string.p9_4,
							imgsrc:$ref+"/images/page9/cloud02.jpg",							
							fullimg:"fullimg",
							text_1_2_cls:"text_1_2_hide",
							text_1_2:data.string.p9_5
					}
				break;
			case 3:
				content = { 
							text_1_1:data.string.p9_6,
							imgsrc:$ref+"/images/page9/img1.jpg",			
							text_1_1_cls:"text_1_1",
							fullimg:"fullimg2" 
					}
			break;

			case 4:
				content = { imgsrc:$ref+"/images/page9/img1.jpg",			
							fullimg:"fullimg2",
							text_1_2_cls:"text_1_2_hide",
							text_1_2:data.string.p9_7
					}
			break;

			case 5:
				content = { text_1_1:data.string.p9_8,
							text_1_1_cls:"text_1_3",
							imgsrc:$ref+"/images/page9/img2.jpg",						
							fullimg:"fullimg",
							text_1_2_cls:"text_1_2_hide",
							text_1_2:data.string.p9_9
					}
			break;

			

		}

		return content
	}


	
})(jQuery);




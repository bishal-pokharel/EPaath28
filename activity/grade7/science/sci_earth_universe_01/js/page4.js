/*make page specific image paths if needed*/
/*var $refImg = $ref+"/images/page4/";*/

var content=[
	{	
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		diyInstructionData : data.string.p4_3
	},
	{
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		text_1_1 :  data.string.p4_2,
		earth:"earth-"+$lang,
		sun:"mysun_"+$lang,
		erth:$ref+"/images/page4/1.png",
		sunny:$ref+"/images/page4/2.png",
		finalMyText: data.string.p4_4,
		case1:data.string.p4_5_1,
		case2:data.string.p4_5_2,
		case3:data.string.p4_5_3,
		case4:data.string.p4_5_4,
		finalMyText2: data.string.p4_6

	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var mySunClass="mysun_"+$lang;

	$nextBtn.html(getSubpageMoveButton($lang,"next"));

	

	var countNext = 0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);


/*
* diyLandingPage
*/
	function diyLandingPage () {
		var source = $("#diyLandingPage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$nextBtn.show(0);	
	}

	/*
* sampleSecondSlide
*/
	function sampleSecondSlide() {
		var source = $("#sampleSecondSlide-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.fadeOut(10,function(){ 
			$(this).html(html); 
		}).delay(10).fadeIn(10,function(){

			$(".dragable" ).draggable({ revert: "invalid",
				start: function() {
					$(".dropBox").css({'border':'#0895D6 2px solid'});
				}
			});
			

			$(".dropBox").droppable({
				drop: function( event, ui ) {

					var draggableId=ui.draggable.attr('id');

					var $dropMeObj=$(this);
					var droppableId = $(this).attr("id");
					var whatprevHtml=$(this).html();
					var dataVal = ui.draggable.attr("dataVal");

					var mywth=$("#"+droppableId).width();
					var myhgt=$("#"+droppableId).height();			

					var backup=	$("#"+draggableId);	
				
					//
					$("#"+droppableId).html('');

					$(".dropBox").css({'border':'#0895D6 2px solid'});

					$dropMeObj.css({'border':'none'}).append($("#"+draggableId).html());

					$("#"+draggableId).hide(0);



					if(droppableId==dataVal){					

						$("#"+draggableId).removeClass(mySunClass).addClass('dropSunClass');
						$("#"+draggableId).draggable( 'disable' );	
						$("#finalMyText").show(0);

						ole.footerNotificationHandler.pageEndSetNotification();
					}
					else
					{
						$("#finalMyText2").show(0);
						setTimeout(function(){

							$dropMeObj.removeAttr('style').html('');
							$("#"+droppableId).html(whatprevHtml);
							$("#"+draggableId).removeAttr('style').show(0);

							$("#finalMyText2").hide(0);
						},1000);
					}

				}
			});
		});			
	}

	diyLandingPage();

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				sampleSecondSlide();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});

	
});



(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board2'),
		$nextbtn = $("#activity-page-next-btn-enabled"),
		$prevbtn = $(".prevButton"),
		countNext = 1,
		$total_page = 9;


	$nextbtn.html(getSubpageMoveButton($lang,"next"));
	$prevbtn.html(getSubpageMoveButton($lang,"prev"));



	$nextbtn.click(function(){
		$(this).hide(0);
		countNext++;
		getHtml(countNext);

	});


	$prevbtn.click(function(){
		$(this).hide(0);
		countNext--;
		getHtml(countNext);

	});


	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		 var source = $("#first-template-1").html()

		var template = Handlebars.compile(source);
		var content= getdata();

		var html = template(content);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){

			if(countNext>1)
			{
				$prevbtn.show(0);

			}
			if(countNext<$total_page)
			{
				$nextbtn.show(0);
			}
			else
			{
				ole.footerNotificationHandler.pageEndSetNotification();
			}

		});

	}
	function getdata()
	{
		var content;
		switch(countNext)
		{
			case 1:
				content = {
					text_1_1:data.string.p1_0,
					text_1_1_cls:"text_1_1",
					bckImg:"backtg2"
				}

				break;
			case 2:
				content = {
							text_1_1:data.string.p1_1,
							imgsrc:$ref+"/images/page1/1.jpg",
							text_1_1_cls:"text_1_1",
							bckImg:"backtg1"
					}
				break;
			case 3:
				content = {
							text_1_1:data.string.p1_2,
							imgsrc:$ref+"/images/page1/2.jpg",
							text_1_1_cls:"text_1_1",
							bckImg:"backtg2"
					}
				break;
			/*case 4:
				content = {
							text_1_1:data.string.p1_3,
							imgsrc:$ref+"/images/page1/3.png",
							text_1_1_cls:"text_1_1",
							bckImg:"backtg2"

					}
			break;*/
			case 4:
				content = {
							text_1_1:data.string.p1_3,
							imgsrc:$ref+"/images/page1/3.jpg",
							text_1_1_cls:"text_1_1",
							bckImg:"backtg2",
							whatval:data.string.p1_3_1

					}
			break;
			case 5:
				content = {
							text_1_1:data.string.p1_4,
							imgsrc:$ref+"/images/page1/3.jpg",
							text_1_1_cls:"text_1_1",
							bckImg:"backtg2"

					}
			break;

			case 6:
				content = {
							text_1_1:data.string.p1_5,
							imgsrc:$ref+"/images/page1/4.jpg",
							text_1_1_cls:"text_1_1",
							bckImg:"backtg2"
					}
			break;

			case 7:
				content = {
							text_1_1:data.string.p1_6,
							imgsrc:$ref+"/images/page1/5.jpg",
							text_1_1_cls:"text_1_1a",
							bckImg:"backtg3"
					}
			break;
			case 8:
				content = {
							text_1_1:data.string.p1_7,
							imgsrc:$ref+"/images/page1/5.jpg",
							text_1_1_cls:"text_1_1a",
							bckImg:"backtg3"
					}
			break;

			case 9:
				content = {
							text_1_1:data.string.p1_8,
							text_1_1_cls:"text_in_middle2",
							whatval:data.string.p1_9,
							bckImg:"backtg4"
					}
			break;


		}

		return content
	}
	getHtml(countNext);



})(jQuery);

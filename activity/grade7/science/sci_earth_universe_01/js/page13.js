$(function(){

  var body = $("#mysample"),
      universe = $("#universe"),
      solarsys = $("#solar-system");
	var clickCountMyPlanet=0;

    var whatClicked, solarPages;
    var countSolarpages=1;
    var getMydata;

    $(".myCaptionBox").html(data.string.p_13_29);


    body.removeClass('opening').addClass("view-3D").delay(2000).queue(function() {
      $(this).removeClass('hide-UI').addClass("set-speed");
      $(this).dequeue();
    });

    $( ".planet" ).each(function( index ) {
      var planetName= $( this ).parent().parent('.orbit').attr('id');
          $(this).find('dt').html(data.string["p7_1_"+(index+1)]);
    });

    $("#sun").find('dt').html(data.string["p7_1_0"]);

    loadTimelineProgress(1,1);
  $( ".orbit").hover(
    function(e) {
       e.stopPropagation();
      $(this).addClass('planetstoprotating');
      $(this).find('.pos').addClass('planetstoprotating');
      $(this).find('.planet').addClass('planetstoprotating');

      $(this).find('.orbit').addClass('moonstoprotating');
      $(this).find('.orbit').find('.pos').removeClass('planetstoprotating').addClass('moonstoprotating');
    },
    function(e) {
       e.stopPropagation();
      $(this).removeClass('planetstoprotating');
      $(this).find('.pos').removeClass('planetstoprotating');
      $(this).find('.planet').removeClass('planetstoprotating');

      $(this).find('.orbit').removeClass('moonstoprotating');
      $(this).find('.orbit').find('.pos').removeClass('moonstoprotating');
    }
  );

  $( "#earth .orbit").hover(
    function(e) {
       e.stopPropagation();
      $("#earth.orbit").addClass('moonstoprotating');
      $("#earth.orbit").find('.pos').addClass('moonstoprotating');
      $("#earth.orbit").find('.planet').addClass('moonstoprotating');

      $(this).addClass('planetstoprotating');
      $(this).find('.pos').removeClass('moonstoprotating').addClass('planetstoprotating');

    },
    function(e) {
      e.stopPropagation();
      $("#earth.orbit").removeClass('moonstoprotating');
      $("#earth.orbit").find('.pos').removeClass('moonstoprotating');
      $("#earth.orbit").find('.planet').removeClass('moonstoprotating');

      $(this).removeClass('planetstoprotating');
      $(this).find('.pos').removeClass('planetstoprotating');

    }
  );

  /**click moon**/
  $( "#earth .orbit").click(function(e)
   {
      e.stopPropagation();
	  clickCountMyPlanet++;
      whatClicked="moon";

      e.preventDefault();

      getMydata=getData(whatClicked);
      var source = $("#first-template").html();
      var template = Handlebars.compile(source);
      var html = template(getMydata);
      $(".myInfoBox").html(html).fadeIn(500,function()
      {
         // $(".absolutecls").hide(0);
      });

	  if(clickCountMyPlanet==4)
	  {
		  ole.footerNotificationHandler.pageEndSetNotification();
	  }
   });



  $(".orbit, #sun").click(function(e)
  {
      var ref = $(this).attr('id');
      whatClicked=ref;
	  clickCountMyPlanet++;
      solarsys.removeClass().addClass(ref);
      e.preventDefault();

      getMydata=getData(ref);
      var source = $("#first-template").html();
      var template = Handlebars.compile(source);
      var html = template(getMydata);

      $(".myInfoBox").html(html).fadeIn(500,function()
      {
          //$(".absolutecls").hide(0);
      });

	  if(clickCountMyPlanet==5)
	  {
		  ole.footerNotificationHandler.pageEndSetNotification();
	  }

  });

  $(".planet").click(function(e)
  {
      var ref = $(this).parent().parent('.orbit').attr('id');
      whatClicked=ref;
	  clickCountMyPlanet++;
      solarsys.removeClass().addClass(ref);

      e.preventDefault();

      getMydata=getData(ref);
      var source = $("#first-template").html();
      var template = Handlebars.compile(source);
      var html = template(getMydata);
      $(".myInfoBox").html(html).fadeIn(500,function()
      {
         // $(".absolutecls").hide(0);
      });

		if(clickCountMyPlanet==5)
	  {
		  ole.footerNotificationHandler.pageEndSetNotification();
	  }

  });

  /****   datatable click    ***/
  $(".myInfoBox").on('click','#spaceNext',function(){
    $("#spacePrev").removeClass('activePrevNext').addClass('noprevnext');
    $("#spaceNext").addClass('activePrevNext').removeClass('noprevnext');

    $(".myInfoBox").find(".solarDefinition").hide(0);
    $(".myInfoBox").find('.relativeBox').show(0);
    $(".myInfoBox").find('.explanationMe').hide(0);
    $(".myInfoBox").find('.explanation2Me').hide(0);
     $(".myInfoBox").find(".whatIconVal").hide(0);

    $(".whatIconVal").fadeOut(10);
  });

   /****   basics click    ***/
  $(".myInfoBox").on('click','#spacePrev',function(){
    $("#spaceNext").removeClass('activePrevNext').addClass('noprevnext');
    $("#spacePrev").addClass('activePrevNext').removeClass('noprevnext');
    $(".myInfoBox").find(".solarDefinition").show(0);
    $(".myInfoBox").find('.relativeBox').hide(0);
    $(".myInfoBox").find('.explanationMe').show(0);
    $(".myInfoBox").find('.explanation2Me').hide(0);
     $(".myInfoBox").find(".whatIconVal").hide(0);

  });




  /****   icons click    ***/
  $(".myInfoBox").on('click','.iconClass',function(){

      var id=$(this).attr('id');

      var txtVal="";
      var imgVal=$ref+"/images/page13/";
      var imgsrcval="";
      var textforicon="";
      if(id=="icon01")
      {
        txtVal=data.string.p_13_3;
        imgsrcval=imgVal+"icon01.png";
        textforicon= data.string.p_13_3_def;
      }
      else if(id=="icon02")
      {
        txtVal=data.string.p_13_4;
        imgsrcval=imgVal+"icon02.png";
        textforicon= data.string.p_13_4_def;
      }
      else if(id=="icon03")
      {
        txtVal=data.string.p_13_10;
        imgsrcval=imgVal+"icon03.png";
        textforicon= data.string.p_13_10_def;
      }
      else if(id=="icon03a")
      {
        txtVal=data.string.p_13_5;
        imgsrcval=imgVal+"icon03.png";
        textforicon= data.string.p_13_5_def;
      }
      else if(id=="icon03b")
      {
        txtVal=data.string.p_13_6;
        imgsrcval=imgVal+"icon03b.png";
        textforicon= data.string.p_13_6_def;
      }
      else if(id=="icon03c")
      {
        txtVal=data.string.p_13_11;
        imgsrcval=imgVal+"p_13_10_def.png";
        textforicon= data.string.p_13_11_def;
      }
      else if(id=="icon03d")
      {
        txtVal=data.string.p_13_12;
        imgsrcval=imgVal+"icon03b.png";
        textforicon= data.string.p_13_12_def;
      }
      else if(id=="icon04")
      {
        txtVal=data.string.p_13_7;
        imgsrcval=imgVal+"icon04.png";
         textforicon= data.string.p_13_7_def;
      }
      else if(id=="icon05")
      {
        txtVal=data.string.p_13_8;
        imgsrcval=imgVal+"icon05.png";
         textforicon= data.string.p_13_8_def;
      }
      else if(id=="icon06")
      {
        txtVal=data.string.p_13_9;
        imgsrcval=imgVal+"icon06.png";
         textforicon= data.string.p_13_9_def;
      }
      else if(id=="icon07")
      {
        txtVal=data.string.p_13_13;
        imgsrcval=imgVal+"icon07.png";
         textforicon= data.string.p_13_13_def;
      }

      var divs="<div>"+txtVal+"</div> <div><img src='"+imgsrcval+"' /></div><div>"+textforicon+"</div><div id='linkBack'><img src='images/arrows/arrow_prev.png'/></div>";

      $(".whatIconVal").html(divs).show(0);
      $(".relativeBox").hide(0);


  });

  $(".myInfoBox").on('click','#linkBack',function(){
     $(".whatIconVal").hide(0);
      $(".relativeBox").show(0);

  });

  $(".myInfoBox").on('click','.closeBtn',function(e){
    $(".myInfoBox").fadeOut(500);
    solarsys.removeClass();
    countSolarpages=1;
    e.preventDefault();
  });



	/*** moon icon clicked on earth  */

	$(".myInfoBox").on('click','.iconClassformoon',function(e){

		e.stopPropagation();

		whatClicked="moon";

		e.preventDefault();

		getMydata=getData(whatClicked);
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(getMydata);
		$(".myInfoBox").html(html).fadeIn(500,function()
		{
		// $(".absolutecls").hide(0);
		});
	});
});


function getData($cnt)
{
  var datVals=[];
  var imgsrcValIt=$ref+"/images/page13/";
  switch($cnt)
  {
    case "sun":
      datVals={
        imgarcs:$ref+"/images/page13/real/sun.png",
        planetName:data.string.p7_1_0,
        solarDefinition:"yes",
        solarDef:[data.string.p6_def_1],
        relativeVal:"yes",
        relativeMe: data.string["p_13_1"],
        absoluteMe:data.string["p_13_2"],
        defNition:data.string.p_13_15,
        deftable:data.string.p_13_16,
        myRealVal:[
          {topic:data.string["p_13_3"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_1_1"],absolutecls:"absolutecls",absolute:data.string["p_13_1_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
          {topic:data.string["p_13_4"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_1_2"],absolutecls:"absolutecls",absolute:data.string["p_13_1_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
          {topic:data.string["p_13_10"],colcls:2, relativecls:"firstshow", relative:data.string["p_13_1_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03"},
          {topic:data.string["p_13_7"],colcls:2, relativecls:"firstshow", relative:data.string["p_13_1_4"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"}
        ],
        pageNumber:[
          {active:"activeSolar",number:1},
          {active:"",number:2}
        ],
        explanation1:"yes",
        solarexps:[data.string.p6_1_1,data.string.p6_1_2, data.string.p6_1_3, data.string.p6_1_4, data.string.p6_1_5]
      };
    break;
    case "mercury":
        datVals={
          imgarcs:$ref+"/images/page13/real/mercury.png",
          planetName:data.string.p7_1_1,
          solarDefinition:"yes",
          solarDef:[data.string.p6_def_2],
          relativeVal:"yes",
          defNition:data.string.p_13_15,
          deftable:data.string.p_13_16,
          relativeMe:data.string["p_13_1"],
          absoluteMe:data.string["p_13_2"],
          myRealVal:[
            {topic:data.string["p_13_3"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_1"],absolutecls:"absolutecls",absolute:data.string["p_13_2_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
            {topic:data.string["p_13_4"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_2"],absolutecls:"absolutecls",absolute:data.string["p_13_2_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
            {topic:data.string["p_13_5"],colcls:2, relativecls:"firstshow", relative:data.string["p_13_2_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03a"},
            {topic:data.string["p_13_6"],colcls:2, relativecls:"firstshow", relative:data.string["p_13_2_4"],relativeImg:imgsrcValIt+"icon03b.png", relativeId:"icon03b"},
            {topic:data.string["p_13_7"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_5"],absolutecls:"absolutecls",absolute:data.string["p_13_2_5a"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
            {topic:data.string["p_13_8"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_6"],absolutecls:"absolutecls",absolute:data.string["p_13_2_6a"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
            {topic:data.string["p_13_9"],colcls:1, relativecls:"relativecls",relative:data.string["p_13_2_7"],absolutecls:"absolutecls",absolute:data.string["p_13_2_7a"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"}
          ],
          pageNumber:[
            {active:"activeSolar",number:1},
            {active:"",number:2}
          ],
          explanation1:"yes",
          solarexps:[data.string.p6_2_1,data.string.p6_2_2, data.string.p6_2_3, data.string.p6_2_4, data.string.p6_2_5, data.string.p6_2_6]
        };
        break;

    case "venus":
        datVals={
          imgarcs:$ref+"/images/page13/real/venus.png",
          planetName:data.string.p7_1_2,
          solarDefinition:"yes",
          solarDef:[data.string.p6_def_3],
          relativeVal:"yes",
          defNition:data.string.p_13_15,
          deftable:data.string.p_13_16,
          relativeMe:data.string["p_13_1"],
          absoluteMe:data.string["p_13_2"],
          myRealVal:[
            {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_1"],absolutecls:"absolutecls",absolute:data.string["p_13_3_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
            {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_2"],absolutecls:"absolutecls",absolute:data.string["p_13_3_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
            {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_3_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03"},
            {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_4"],absolutecls:"absolutecls",absolute:data.string["p_13_3_4a"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
            {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_5"],absolutecls:"absolutecls",absolute:data.string["p_13_3_5a"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
            {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_3_6"],absolutecls:"absolutecls",absolute:data.string["p_13_3_6a"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"}

          ],
          pageNumber:[
            {active:"activeSolar",number:1},
            {active:"",number:2}
          ],
          explanation1:"yes",
          solarexps:[data.string.p6_3_2, data.string.p6_3_3, data.string.p6_3_4, data.string.p6_3_5,data.string.p6_3_1],
        };

        break;

    case "earth":
        datVals={
          imgarcs:$ref+"/images/page13/real/earth.png",
          planetName:data.string.p7_1_3,
          solarDefinition:"yes",
          solarDef:[data.string.p6_def_4],
          defNition:data.string.p_13_15,
          deftable:data.string.p_13_16,
          relativeVal:"yes",
          relativeMe:data.string["p_13_1a"],
          absoluteMe:data.string["p_13_2"],
          myRealVal:[
            {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_4_1"],absolutecls:"absolutecls",absolute:data.string["p_13_4_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
            {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_4_2"],absolutecls:"absolutecls",absolute:data.string["p_13_4_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
            {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_4_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03"},
            {topic:data.string["p_13_7"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_4_4"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
            {topic:data.string["p_13_8"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_4_5"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
            {topic:data.string["p_13_9"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_4_6"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"},
            {topic:data.string["p_13_13"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_4_moon"],moonrelativeImg:imgsrcValIt+"icon07.png", relativeId:"icon07",}
          ],
          pageNumber:[
            {active:"activeSolar",number:1},
            {active:"",number:2}
          ],
          explanation1:"yes",
          solarexps:[data.string.p6_4_1, data.string.p6_4_2, data.string.p6_4_3, data.string.p6_4_4]
        };
        break;
    case "mars":
          datVals={
            imgarcs:$ref+"/images/page13/real/mars.png",
            planetName:data.string.p7_1_4,
            solarDefinition:"yes",
            solarDef:[data.string.p6_def_5],
            defNition:data.string.p_13_15,
            deftable:data.string.p_13_16,
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
              {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_1"],absolutecls:"absolutecls",absolute:data.string["p_13_5_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
              {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_2"],absolutecls:"absolutecls",absolute:data.string["p_13_5_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
              {topic:data.string["p_13_11"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_5_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03c"},
              {topic:data.string["p_13_12"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_5_4"],relativeImg:imgsrcValIt+"icon03b.png", relativeId:"icon03d"},
              {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_5"],absolutecls:"absolutecls",absolute:data.string["p_13_5_5a"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
              {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_6"],absolutecls:"absolutecls",absolute:data.string["p_13_5_6a"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
              {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_5_7"],absolutecls:"absolutecls",absolute:data.string["p_13_5_7a"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"},
              {topic:data.string["p_13_13"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_5_moon"],relativeImg:imgsrcValIt+"icon07.png", relativeId:"icon07"}
            ],
            pageNumber:[
              {active:"activeSolar",number:1},
              {active:"",number:2}
            ],
            explanation1:"yes",
            solarexps:[data.string.p6_5_1, data.string.p6_5_2, data.string.p6_5_3, data.string.p6_5_4,data.string.p6_5_5]

          };
          break;
    case "jupiter":
          datVals={
            imgarcs:$ref+"/images/page13/real/jupiter.png",
            planetName:data.string.p7_1_5,
            solarDefinition:"yes",
            solarDef:[data.string.p6_def_6],
            defNition:data.string.p_13_15,
            deftable:data.string.p_13_16,
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
              {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_1"],absolutecls:"absolutecls",absolute:data.string["p_13_6_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
              {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_2"],absolutecls:"absolutecls",absolute:data.string["p_13_6_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
              {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_6_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03"},
              {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_4"],absolutecls:"absolutecls",absolute:data.string["p_13_6_4a"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
              {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_5"],absolutecls:"absolutecls",absolute:data.string["p_13_6_5a"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
              {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_6_6"],absolutecls:"absolutecls",absolute:data.string["p_13_6_6a"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"},
              {topic:data.string["p_13_13"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_6_moon"],relativeImg:imgsrcValIt+"icon07.png", relativeId:"icon07"}
            ],
            pageNumber:[
              {active:"activeSolar",number:1},
              {active:"",number:2}
            ],
            explanation1:"yes",
            solarexps:[data.string.p6_6_1, data.string.p6_6_2, data.string.p6_6_3, data.string.p6_6_4]
           };
          break;

    case "saturn":
          datVals={
            imgarcs:$ref+"/images/page13/real/saturn.png",
            planetName:data.string.p7_1_6,
            solarDefinition:"yes",
            solarDef:[data.string.p6_def_7],
            defNition:data.string.p_13_15,
            deftable:data.string.p_13_16,
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
              {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_1"],absolutecls:"absolutecls",absolute:data.string["p_13_7_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
              {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_2"],absolutecls:"absolutecls",absolute:data.string["p_13_7_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
              {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_7_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03"},
              {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_4"],absolutecls:"absolutecls",absolute:data.string["p_13_7_4a"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
              {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_5"],absolutecls:"absolutecls",absolute:data.string["p_13_7_5a"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
              {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_7_6"],absolutecls:"absolutecls",absolute:data.string["p_13_7_6a"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"},
              {topic:data.string["p_13_13"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_7_moon"],relativeImg:imgsrcValIt+"icon07.png", relativeId:"icon07"}
            ],
            pageNumber:[
              {active:"activeSolar",number:1},
              {active:"",number:2}
            ],
            explanation1:"yes",
            solarexps:[data.string.p6_7_1, data.string.p6_7_2, data.string.p6_7_3, data.string.p6_7_4]

          };
          break;
    case "uranus":
          datVals={
            imgarcs:$ref+"/images/page13/real/uranus.png",
            planetName:data.string.p7_1_7,
            solarDefinition:"yes",
            solarDef:[data.string.p6_def_8],
            defNition:data.string.p_13_15,
            deftable:data.string.p_13_16,
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
              {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_1"],absolutecls:"absolutecls",absolute:data.string["p_13_8_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
              {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_2"],absolutecls:"absolutecls",absolute:data.string["p_13_8_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
              {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_8_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03"},
              {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_4"],absolutecls:"absolutecls",absolute:data.string["p_13_8_4a"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
              {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_5"],absolutecls:"absolutecls",absolute:data.string["p_13_8_5a"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
              {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_8_6"],absolutecls:"absolutecls",absolute:data.string["p_13_8_6a"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"},
              {topic:data.string["p_13_13"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_8_moon"],relativeImg:imgsrcValIt+"icon07.png", relativeId:"icon07"}
            ],
            pageNumber:[
              {active:"activeSolar",number:1},
              {active:"",number:2}
            ],
            explanation1:"yes",
            solarexps:[data.string.p6_8_2, data.string.p6_8_3,data.string.p6_8_4]

          };
          break;
    case "neptune":
          datVals={
            imgarcs:$ref+"/images/page13/real/neptune.png",
            planetName:data.string.p7_1_8,
            solarDefinition:"yes",
            solarDef:[data.string.p6_def_9],
            defNition:data.string.p_13_15,
            deftable:data.string.p_13_16,
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
              {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_1"],absolutecls:"absolutecls",absolute:data.string["p_13_9_1a"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
              {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_2"],absolutecls:"absolutecls",absolute:data.string["p_13_9_2a"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
              {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_9_3"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03"},
              {topic:data.string["p_13_7"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_4"],absolutecls:"absolutecls",absolute:data.string["p_13_9_4a"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
              {topic:data.string["p_13_8"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_5"],absolutecls:"absolutecls",absolute:data.string["p_13_9_5a"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
              {topic:data.string["p_13_9"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_9_6"],absolutecls:"absolutecls",absolute:data.string["p_13_9_6a"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"},
              {topic:data.string["p_13_13"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_9_moon"],relativeImg:imgsrcValIt+"icon07.png", relativeId:"icon07"}
            ],
            pageNumber:[
              {active:"activeSolar",number:1},
              {active:"",number:2}
            ],
            explanation1:"yes",
            solarexps:[data.string.p6_9_1,data.string.p6_9_2, data.string.p6_9_3, data.string.p6_9_4]
          };
          break;
    case "moon":
          datVals={
            imgarcs:$ref+"/images/page13/real/moon.png",
            planetName:data.string.p_13_17,
            solarDefinition:"yes",
            solarDef:[data.string.p_13_18],
            defNition:data.string.p_13_15,
            deftable:data.string.p_13_16,
            relativeVal:"yes",
            relativeMe:data.string["p_13_1"],
            absoluteMe:data.string["p_13_2"],
            myRealVal:[
              {topic:data.string["p_13_3"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_23_1"],absolutecls:"absolutecls",absolute:data.string["p_13_23_2"],relativeImg:imgsrcValIt+"icon01.png", relativeId:"icon01"},
              {topic:data.string["p_13_4"],  colcls:1, relativecls:"relativecls",relative:data.string["p_13_24_1"],absolutecls:"absolutecls",absolute:data.string["p_13_24_2"],relativeImg:imgsrcValIt+"icon02.png", relativeId:"icon02"},
              {topic:data.string["p_13_10"], colcls:2, relativecls:"firstshow", relative:data.string["p_13_25"],relativeImg:imgsrcValIt+"icon03.png", relativeId:"icon03"},
              {topic:data.string["p_13_7"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_26"],relativeImg:imgsrcValIt+"icon04.png", relativeId:"icon04"},
              {topic:data.string["p_13_8"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_27"],relativeImg:imgsrcValIt+"icon05.png", relativeId:"icon05"},
              {topic:data.string["p_13_9"],  colcls:2, relativecls:"firstshow",relative:data.string["p_13_28"],relativeImg:imgsrcValIt+"icon06.png", relativeId:"icon06"}
            ],
            pageNumber:[
              {active:"activeSolar",number:1},
              {active:"",number:2}
            ],
            explanation1:"yes",
            solarexps:[data.string.p_13_19,data.string.p_13_20, data.string.p_13_21, data.string.p_13_22]
          };
          break;
  }

  return datVals;

}

function  getPages(dataVals)
{

  var whatvals=dataVals['pageNumber'].length;
  return whatvals;
}

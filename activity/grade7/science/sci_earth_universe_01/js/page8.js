/*make page specific image paths if needed*/
/*var $refImg = $ref+"/images/page4/";*/


var content=[
	{	
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		diyInstructionData : data.string.p8_1
	},	
	{
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		text_1_1 :  data.string.p8_2,
		src:$ref+"/images/page8/bk.png",
		src1:$ref+"/images/page8/1.png",
		src2:$ref+"/images/page8/1.gif",
		src3:$ref+"/images/page8/2.png",
		corr1:0,
		corr2:1,
		corr3:0,
		myTextMe:data.string.p8_3,
		clickMe1:"clickMe1",
		clickMe2:"clickMe2",
		clickMe3:"clickMe3"
	},	
	{
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		text_1_1 :  data.string.p8_4,
		src:$ref+"/images/page8/bk.png",
		src1:$ref+"/images/page8/1.png",
		src2:$ref+"/images/page8/1.gif",
		src3:$ref+"/images/page8/2.png",
		corr1:1,
		corr2:0,
		corr3:0,
		myTextMe:data.string.p8_5,
		clickMe1:"clickMe1",
		clickMe2:"clickMe2",
		clickMe3:"clickMe3"
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var mySunClass="mysun_"+$lang;

	$nextBtn.html(getSubpageMoveButton($lang,"next"));

	

	var countNext = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);



	function diyLandingPage () {
		var source = $("#diyLandingPage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$nextBtn.fadeIn(10);	
	}

	function sampleSecondSlide() {
		
		
		var source = $("#sampleSecondSlide-template").html();
		

		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.fadeOut(10,function(){ 
			$(this).html(html); 
		}).delay(10).fadeIn(10,function(){});			
	}

	diyLandingPage();




	$nextBtn.on('click',function () {
		countNext++;	
		$(this).fadeOut(10);	
		
		sampleSecondSlide();
		

		loadTimelineProgress($total_page,countNext+1);
	});


	$(".board").on('click',".clickme",function(){

			var corrs=$(this).attr('corr');

			if(corrs==1)
			{
				$(".clickme").removeClass('roundme');
				$("#textMe").fadeIn(10);
				if(countNext==2)
				{
					ole.footerNotificationHandler.pageEndSetNotification();
				}
				else
				{
					$nextBtn.fadeIn(10);
				}
			}
			else
			{
				$(this).addClass('roundme');
				$("#textMe").fadeOut(10);
			}
	});
	

});


(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevbtn = $(".prevButton"),
		countNext = 1,
		$total_page = 9;

	
	var $whatNext=(getSubpageMoveButton($lang,"next"));
	var $newPrev=(getSubpageMoveButton($lang,"prev"));


	var $solar=[
				{names:"sun",img:$ref+"/images/page6/sun.png",activeMe:"activeMe"},
				{names:"mercury",img:$ref+"/images/page6/mercury.png"},
				{names:"venus",img:$ref+"/images/page6/venus.png"},
				{names:"earth",img:$ref+"/images/page6/earth.png"},
				{names:"mars",img:$ref+"/images/page6/mars.png"},
				{names:"jupiter",img:$ref+"/images/page6/jupiter.png"},
				{names:"saturn",img:$ref+"/images/page6/saturn.png"},
				{names:"uranus",img:$ref+"/images/page6/uranus.png"},
				{names:"neptune",img:$ref+"/images/page6/neptune.png"}
			];

	$.each($solar, function (index){

		if(index==0)activeme="activeMe";
		else activeme="";


		var $innserHtml="<div id="+$solar[index].names+" class="+activeme+"><img src='"+$solar[index].img+"'  /></div>";
		
		$("#solarsystem").append($innserHtml);
		
	});


	getHtml(countNext)

	
	$('.board').on('click','.nextBtns',function(){
		$(this).hide(0);
		countNext++;
		getHtml(countNext);

	});

	$('.board').on('click','.prevBtns',function(){
		$(this).hide(0);
		countNext--;
		getHtml(countNext);

	});


	

	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		var source = $("#first-template").html()
		
		var template = Handlebars.compile(source);
		var content= getdata();

		var html = template(content);
		

		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){
			
			$('.board').animate({scrollTop : 0},10);
			$("#solarsystem>div").removeClass("activeMe");

			$("#solarsystem div:nth-child("+countNext+")").addClass("activeMe");

			if(countNext>1)
			{
				$('.prevBtns').show(0);

			}

			if(countNext<$total_page)
			{
				
				$('.nextBtns').show(0);
			}
			else
			{
				ole.footerNotificationHandler.pageEndSetNotification();
			}

		});

	}
	function getdata()
	{
		var content;
		switch(countNext)
		{
			case 1:
				content = { 
						headtitle:data.string.p6_1,
						defination:data.string.p6_def_1,
						/*points:[data.string.p6_1_1,data.string.p6_1_2,data.string.p6_1_3,data.string.p6_1_4,data.string.p6_1_5],
						*/newSection:$whatNext,
						prevSection:$newPrev

					};
				break;

			case 2:
				
				content = { 
						headtitle:data.string.p6_2,
						defination:data.string.p6_def_2,
						/*points:[data.string.p6_2_1,data.string.p6_2_2,data.string.p6_2_3,data.string.p6_2_4,data.string.p6_2_5,data.string.p6_2_6],
						*/wholeDef:[
							{types:data.string.p6_opt_1, values:data.string.p6_2o_1},
							{types:data.string.p6_opt_2, values:data.string.p6_2o_2},
							{types:data.string.p6_opt_3, values:data.string.p6_2o_3},
							{types:data.string.p6_opt_4, values:data.string.p6_2o_4},
							{types:data.string.p6_opt_5, values:data.string.p6_2o_5},
							{types:data.string.p6_opt_6, values:data.string.p6_2o_6},
						],
						newSection:$whatNext,
						prevSection:$newPrev							
					}

				break;

			case 3:
				
				content = { 
						headtitle:data.string.p6_3,
						defination:data.string.p6_def_3,
						/*points:[data.string.p6_3_1,data.string.p6_3_2,data.string.p6_3_3,data.string.p6_3_4,data.string.p6_3_5],
						*/wholeDef:[
							{types:data.string.p6_opt_1, values:data.string.p6_3o_1},
							{types:data.string.p6_opt_2, values:data.string.p6_3o_2},
							{types:data.string.p6_opt_3, values:data.string.p6_3o_3},
							{types:data.string.p6_opt_4, values:data.string.p6_3o_4},
							{types:data.string.p6_opt_5, values:data.string.p6_3o_5},
							{types:data.string.p6_opt_6, values:data.string.p6_3o_6},
						],
						
						newSection:$whatNext,
						prevSection:$newPrev							
					}

				break;

			case 4:
				
				content = { 
						headtitle:data.string.p6_4,
						defination:data.string.p6_def_4,
						/*points:[data.string.p6_4_1,data.string.p6_4_2,data.string.p6_4_3,data.string.p6_4_4],
						*/wholeDef:[
							{types:data.string.p6_opt_1, values:data.string.p6_4o_1},
							{types:data.string.p6_opt_2, values:data.string.p6_4o_2},
							{types:data.string.p6_opt_3, values:data.string.p6_4o_3},
							{types:data.string.p6_opt_4, values:data.string.p6_4o_4},
							{types:data.string.p6_opt_5, values:data.string.p6_4o_5},
							{types:data.string.p6_opt_6, values:data.string.p6_4o_6}
						],
						newSection:$whatNext,
						prevSection:$newPrev							
					}

				break;

			case 5:
				
				content = { 
						headtitle:data.string.p6_5,
						defination:data.string.p6_def_5,
						/*points:[data.string.p6_5_1,data.string.p6_5_2,data.string.p6_5_3,data.string.p6_5_4],
						*/wholeDef:[
							{types:data.string.p6_opt_1, values:data.string.p6_5o_1},
							{types:data.string.p6_opt_2, values:data.string.p6_5o_2},
							{types:data.string.p6_opt_3, values:data.string.p6_5o_3},
							{types:data.string.p6_opt_4, values:data.string.p6_5o_4},
							{types:data.string.p6_opt_5, values:data.string.p6_5o_5},
							{types:data.string.p6_opt_6, values:data.string.p6_5o_6}
						],
						newSection:$whatNext,
						prevSection:$newPrev							
					}

				break;
			case 6:
				
				content = { 
						headtitle:data.string.p6_6,
						defination:data.string.p6_def_6,
						/*points:[data.string.p6_6_1,data.string.p6_6_2,data.string.p6_6_3,data.string.p6_6_4],
						*/wholeDef:[
							{types:data.string.p6_opt_1, values:data.string.p6_6o_1},
							{types:data.string.p6_opt_2, values:data.string.p6_6o_2},
							{types:data.string.p6_opt_3, values:data.string.p6_6o_3},
							{types:data.string.p6_opt_9, values:data.string.p6_6o_9},
							{types:data.string.p6_opt_4, values:data.string.p6_6o_4},
							{types:data.string.p6_opt_5, values:data.string.p6_6o_5},
							{types:data.string.p6_opt_6, values:data.string.p6_6o_6},
							{types:data.string.p6_opt_7, values:data.string.p6_6o_7},
						],
						newSection:$whatNext,
						prevSection:$newPrev							
					}

				break;
			case 7:
				
				content = { 
						headtitle:data.string.p6_7,
						defination:data.string.p6_def_7,
						/*points:[data.string.p6_7_1,data.string.p6_7_2,data.string.p6_7_3,data.string.p6_7_4],
						*/wholeDef:[
							{types:data.string.p6_opt_1, values:data.string.p6_7o_1},
							{types:data.string.p6_opt_2, values:data.string.p6_7o_2},
							{types:data.string.p6_opt_3, values:data.string.p6_7o_3},
							{types:data.string.p6_opt_4, values:data.string.p6_7o_4},
							{types:data.string.p6_opt_5, values:data.string.p6_7o_5},
							{types:data.string.p6_opt_6, values:data.string.p6_7o_6},
							{types:data.string.p6_opt_7, values:data.string.p6_7o_7},
						],
						newSection:$whatNext,
						prevSection:$newPrev							
					}

				break;
			case 8:
				
				content = { 
						headtitle:data.string.p6_8,
						defination:data.string.p6_def_8,
						/*points:[data.string.p6_8_1,data.string.p6_8_2,data.string.p6_8_3],
						*/wholeDef:[
							{types:data.string.p6_opt_1, values:data.string.p6_8o_1},
							{types:data.string.p6_opt_2, values:data.string.p6_8o_2},
							{types:data.string.p6_opt_3, values:data.string.p6_8o_3},
							{types:data.string.p6_opt_4, values:data.string.p6_8o_4},
							{types:data.string.p6_opt_5, values:data.string.p6_8o_5},
							{types:data.string.p6_opt_6, values:data.string.p6_8o_6},
							{types:data.string.p6_opt_7, values:data.string.p6_8o_7}
						],
						newSection:$whatNext,
						prevSection:$newPrev							
					}

				break;
			case 9:
				
				content = { 
						headtitle:data.string.p6_9,
						defination:data.string.p6_def_9,
						/*points:[data.string.p6_9_1,data.string.p6_9_2,data.string.p6_9_3,data.string.p6_9_4],
						*/wholeDef:[
							{types:data.string.p6_opt_1, values:data.string.p6_9o_1},
							{types:data.string.p6_opt_2, values:data.string.p6_9o_2},
							{types:data.string.p6_opt_3, values:data.string.p6_9o_3},
							{types:data.string.p6_opt_4, values:data.string.p6_9o_4},
							{types:data.string.p6_opt_5, values:data.string.p6_9o_5},
							{types:data.string.p6_opt_6, values:data.string.p6_9o_6},
							{types:data.string.p6_opt_7, values:data.string.p6_9o_7},
						],
						newSection:$whatNext,
						prevSection:$newPrev							
					}

				break;
		}

		return content;
	}


	
})(jQuery);




/*make page specific image paths if needed*/
/*var $refImg = $ref+"/images/page4/";*/

var plantesArray=["mercury","venus","earth","mars","jupiter","saturn","uranus","neptune"];
var extraArray=["jupiter","saturn","uranus","neptune","earth","venus","mars","mercury"];
var getIndexedArray=reArrangeArray(extraArray);

var newQuestion=[1,2,3,4,5,6,7,8];
var etcQuestion=shuffleArray(newQuestion);

var getQuestion=getRandomQuestion(etcQuestion,plantesArray);
var content=[
	{	
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		diyInstructionData : data.string.p7_1
	},
	{
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		text_1_1 :  data.string.p7_2,
		boxes:["mercury","mars","venus","earth","neptune","uranus","saturn","jupiter"],
		dragimgs:getIndexedArray,
		dropBox:"dropBox",
		conclusion:data.string.p7_3_1
	},
	{
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		text_1_1 :  data.string.p7_3,
		sun:$ref+"/images/page7/sun.png",
		sunObj:data.string.p7_1_0,
		boxes2:["mercury","venus","earth","mars","jupiter","saturn","uranus","neptune"],
		dragimgs:getIndexedArray,
		dropBox:"dropBox2",
		conclusion:data.string.p7_3_1
	},
	{
		diyImageSource : "images/diy/diy1.png",
		diyTextData : data.string.p4_1,
		text_1_1 :  data.string.p7_4,
		boxes:getQuestion,
		dragimgs:getIndexedArray,
		conclusion:data.string.p7_3_1
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var mySunClass="mysun_"+$lang;

	$nextBtn.html(getSubpageMoveButton($lang,"next"));

	

	var countNext = 0;

	var $total_page = 4;
	loadTimelineProgress($total_page,countNext+1);



	function diyLandingPage () {
		var source = $("#diyLandingPage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$nextBtn.fadeIn(10);	
	}

	function sampleSecondSlide() {
		
		var countdrop=0;
		var source = $("#sampleSecondSlide-template").html();
		if(countNext==3)
			source = $("#sampleThirdSlide-template").html();


		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.fadeOut(10,function(){ 
			$(this).html(html); 
		}).delay(10).fadeIn(10,function(){

			$(".dragable" ).draggable({ revert: "invalid"});
			

			$(".dropBox, .dropBox2, .dropBox3").droppable({
				accept:function (e) { 

					if (e.attr('id')== $(this).attr('dataval')) return true; 

				},
				drop: function( event, ui ) {

					var dragId=ui.draggable.attr('id');
					var $drag=$("#"+dragId);
					var innerhtml=$drag.html();

					var myDrop=$(this);


					$drag.fadeOut(10,function(){});
						countdrop++;
					myDrop.html(innerhtml).fadeIn(10,function(){
						if(countdrop==8 && countNext<3)
						{
							$(".conclusion").fadeIn(100);
							$nextBtn.delay(100).fadeIn(10);	

						}
						if(countdrop==8 && countNext==3)
						{
								$(".conclusion2").fadeIn(100);
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					});
					
					

					
				}
			});
		});			
	}

	diyLandingPage();
	//sampleSecondSlide();




	$nextBtn.on('click',function () {
		countNext++;	
		$(this).fadeOut(10);	
		sampleSecondSlide();

		loadTimelineProgress($total_page,countNext+1);
	});

	

});

function reArrangeArray(myarray)
{

	
	var newarray=shuffleArray(myarray);
	   
	var anotherArray=[];

	for (var k =0;k< newarray.length; k++) {

		var myKey=jQuery.inArray( newarray[k], plantesArray);

		anotherArray[k]={ "imgid":newarray[k],"src":$ref+"/images/page7/"+(myKey+1)+".png",labelObj:data.string['p7_1_'+(myKey+1)]};
	       
	}


	return anotherArray;
}


function getRandomQuestion(questionans,plantesArray)
{
	var allData=[];
	for(var i=0; i<questionans.length;i++)
	{
		var myind=questionans[i];
		allData[i]={"mytext":data.string["p7_2_"+myind], "bxId":plantesArray[myind-1]}
	}

	return allData;
}
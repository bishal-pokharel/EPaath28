




$(function(){



	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	/******************** Variables ********************************************/

	var counter=1; //count events
	var totalCounter=7; //count question
	var hintCounts=1; //count hints
	var countLife=6; //count life

	var arrayquestion=new Array(1,2,3,4,5,6,7);
	var arrayCounter=1;
	var newarray=shuffleArray(arrayquestion);
    loadTimelineProgress(2,counter);

	//countAnswer
	var rightcounter=0, wrongCounter=0;


	var findImageNumber=1; //findImagenumber for image background

	var lifeRocket = new Image();


	lifeRocket.src = $ref+"/exercise/images/exe2/life.png";

	var mySoundCorrect = new buzz.sound($ref+"/exercise/sound/clap.mp3");
	var mySoundIncorrect = new buzz.sound($ref+"/exercise/sound/buzer.mp3");
	var myRocketLunch = new buzz.sound($ref+"/exercise/sound/rocket1.mp3");



	/******************** functions ********************************************/

	goToAnotherPage(counter)

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(10);;
		counter++;
		goToAnotherPage(counter);

	});
	$(".allquestion").on('click','.lastArrow2',function(){
			$(this).fadeOut(10);;
		counter++;
		loadTimelineProgress(2,counter);
		goToAnotherPage(counter);
	});


	// start rocket launch

	$(".allquestion").on('click','.startIt',function(){
		$(this).fadeOut(10);
		blastOff();

	});

	//page first red arrow
	$(".allquestion").on('click','.lastArrow',function(){
		$(this).hide(0);
		$("#space_1, #space_2").fadeOut(500);
		$("#space_3").delay(500).fadeIn(500);
		$("#space_4").delay(1500).fadeIn(500,function(){
			$(".lastArrow2").fadeIn(10);
		});
	});

	//hints

	$(".allquestion").on('click','.moreSolarHints',function(){

		//$(".letterbx li:nth-child("+hintCounts+")").hide(0);
		hintCounts++;
		$(".letterbx li:nth-child("+hintCounts+")").show(0);
		$('.rocketLife').find('img').first().remove();
		countLife--;
		againCheckAstrosLife();
		if(hintCounts==3)
			$('.moreSolarHints').fadeOut(10);
	});

	//i have got an answer show planet options
	$(".allquestion").on('click','.iknowans',function(){
		$(".solarOptionBox").fadeIn();
	});

	// click on answer (planet)
	$(".allquestion").on('click','.solaroptions',function(){
		if($(this).hasClass('clickable'))
		{

			var whatThis=$(this).attr('dataval');
			var myClicked=$(this).attr('id');




			/**   if answer is correct then do this **/
			if(whatThis=="1")
			{
				$('.moreSolarHints').fadeOut(10);
				$(".iknowans").fadeOut();
				$('.solaroptions').removeClass('clickable');
				rightcounter++;
				countLife+=3;

				/**   all options fadeout and correct option enlarge **/
				$(".solaroptions:not([dataval='1']").fadeOut(1000,function(){
					$('.solaroptions').addClass('enlargeOption');
				});

				mySoundCorrect.play();

				/**   comment appers and rocket animates up to increase fuel **/
				$(".gameComment").html(data.string.space_18).delay(2000).fadeIn(20,function(){
					$(".fallRocket").fadeIn(10).animate({opacity:'0',top:'13%'},500,function(){

						$('.rocketLife').append('<img src='+lifeRocket.src+' /><img src='+lifeRocket.src+' /><img src='+lifeRocket.src+' />');
						checkAstrosLife(1); //check life/ fuel
						$(this).removeAttr('style');
				 	});


				});

			}
			else
			{
				/** if anwer is wrong then 2 rocket fuel decreases  **/
				wrongCounter--;
				countLife-=2;
				mySoundIncorrect.play();

				$("#"+myClicked).animate({opacity:'0',top:'100%'},100);

				/*$(".gameComment").html(data.string.space_17).delay(2000).fadeIn(20,function(){
				*/
				 	$('.rocketLife').find('img').first().remove();
					$('.rocketLife').find('img').first().remove();
					//$('.solaroptions').animate({opacity:'0'},100);
					checkAstrosLife(0);//check life/fuel


				/*}); 	*/
			}
		}


	});


	/*Click replay*/
	$(".allquestion").on('click','.replyMeBtn',function(){
		// ole.activityComplete.finishingcall();
		location.reload();
	});
	/*** filler between two question **/
	function goingToAnotherPlanet(whatval)
	{
		$dataval={
			text1:	data.string.space_22,
			rocketsrc:$ref+"/exercise/images/exe2/rocket.png",
			burningfire:$ref+"/exercise/images/exe2/rocket_flame.png"
		};
		var source;

		source   = $("#template-5").html();

		var template = Handlebars.compile(source);


		var html=template($dataval);

		$(".allquestion").fadeOut(10,function(){
			$(this).html(html);
		}).delay(100).fadeIn(10,function(){
			$("#goingrocketsrc").fadeIn(10,function(){

			}).animate({bottom:"20%",left:"25%"},1200).animate({bottom:"100%",left:"120%"},1200,function(){
				startQuestion();
			});
			$(".burningfire").fadeIn(10);
		});//fadein function ends
	}//goingToAnotherPlanet function ends

	 /**  if fuel is less than 2 then hint box hides **/
	function againCheckAstrosLife()
	{
		if(countLife<=1)
		{
			$('.moreSolarHints').fadeOut(10);
		}
	}//againCheckAstrosLife function ends

	/**  checks fuel of game**/
	function checkAstrosLife(mysval)
	{
		if(countLife==1)
		{
			$('.moreSolarHints').fadeOut(10);
		}
		if(countLife<1)
		{
			goToAnotherPage(10);

		}
		else
		{
			if(mysval==1)
			{
				arrayCounter++;

				$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
			}

		}

	}//checkAstrosLife function ends


	/**  checks the counter and loads function accordingly**/
	function goToAnotherPage(counters)
	{
		switch(counters)
		{
			case 1:
				getBackStory()
				break;
			case 2:
				startBlastOff();
				break;
			case 3:
				hintCounts=1
				startQuestion();
				break;
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			hintCounts=1;
			goingToAnotherPlanet();
				/*
				startQuestion();*/
				break;
			case 10:
				backToEarth();
				break;
		}
	}///goToAnotherPage function ends


	/** when games ends this function is loaded */
	function backToEarth()
	{

		$dataval={

			goToMyEarth:$ref+"/exercise/images/exe2/earth.png",
			thankyouNote:data.string.space_19,
			rocketsrc:$ref+"/exercise/images/exe2/rocket.png",
			landRocket:$ref+"/exercise/images/exe2/landing.png",
			landAstroGirl:$ref+"/exercise/images/exe2/deepa01.png",
			replayagain:data.string.space_23
		};
		var source;

		source   = $("#template-4").html();

		var template = Handlebars.compile(source);


		var html=template($dataval);

		$(".allquestion").fadeOut(10,function(){
			$(this).html(html);

		}).delay(100).fadeIn(10,function(){
			$(".myNewSolarBck").addClass('preImage'+findImageNumber);

			/****  gameover   ***/
			if(countLife<1)
			{
				$(".landRocket").fadeIn(10);
				$(".landAstroGirl").fadeIn(10);
				$(".thankyouNote p").html(data.string.space_16);
				$(".thankyouNote").fadeIn(1000,function()
				{
					$(".replyMeBtn").fadeIn(10);
					ole.footerNotificationHandler.showRestartButton();
				});
			}
			else
			{
				/****  Winner   ***/
				$(".thankyouNote").delay(3000).fadeOut(500,function(){
					$(".rocketsrc3").delay(100).fadeIn(100).animate({bottom:"50%"},2000).animate({bottom:"100%"},500,function(){

						$(".goToMyEarth").fadeIn(3000);
						$(".myNewSolarBck").removeClass('preImage'+findImageNumber).addClass('goingToEarthClass');
						$(".rocketsrc4").fadeIn(10).animate({bottom:"100%",left:'100%','width':'0%'},3000,function(){
							$(".thankyouNote p").html(data.string.space_20);
							$(".thankyouNote").fadeIn(10);
                            $("#activity-page-next-btn-enabled").show();
								$("#activity-page-next-btn-enabled").click (function(){
									ole.activityComplete.finishingcall();
								});
						});
					}).fadeIn(10);
				});
			}// else ends
		});//fadein  ends
	}//backToEarth ends

	/*********   This starts question                         ************/
	function startQuestion()
	{
		var whatVal=arrayCounter-1;
		var numberCount=newarray[whatVal];

		findImageNumber=numberCount;


		var imagesList=["mercury","venus","mars","jupiter","saturn","uranus","neptune"];
		var solaroptions=[];

		var theAnswer2=data.string["spaceques_ans_"+numberCount];

		var theAnswer=theAnswer2.toLowerCase();

		for(var i=0; i<imagesList.length;i++)
		{
			if(imagesList[i]==theAnswer)
				solaroptions[i]={idname:imagesList[i],optionImg:$ref+"/exercise/images/exe2/planets/"+imagesList[i]+".png",corr:1, solartxt:data.string["space_opt"+(i+1)]};
			else
				solaroptions[i]={idname:imagesList[i],optionImg:$ref+"/exercise/images/exe2/planets/"+imagesList[i]+".png",corr:0, solartxt:data.string["space_opt"+(i+1)]};
		}
		$dataval={
			rockethints:[data.string["spaceques_1_"+numberCount],data.string["spaceques_2_"+numberCount],data.string["spaceques_3_"+numberCount]],
			moreSolarHints:data.string.space_13,
			iknowans:data.string.space_14,
			landAstroGirl:$ref+"/exercise/images/exe2/deepa01.png",
			solaroptions:solaroptions,
			whereAmi:data.string.space_15,
			rocketLife:$ref+"/exercise/images/exe2/life.png",
			gameover:data.string.space_16,
			landRocket:$ref+"/exercise/images/exe2/landing.png"
		};

		var source;

		source   = $("#template-3").html();

		var template = Handlebars.compile(source);


		var html=template($dataval);

		$(".allquestion").fadeOut(10,function(){
			$(this).html(html);
		}).delay(100).fadeIn(10,function(){
			$("#secondsolarSpace").addClass('preImage'+numberCount);

			var imgsss="";
			for(var k=1;k<=countLife;k++)
			{
				imgsss+="<img src='"+lifeRocket.src+"' />";
			}
			if(countLife==1)
			{
				$('.moreSolarHints').fadeOut(10);
			}
			$(".rocketLife").html(imgsss).show(0);
			$(".landRocket").fadeIn(10);
		});//fadein  ends
	}// startQuestion  ends

	/*********        Rocket blask off and landing          ****************/
	function blastOff()
	{
		var img1 = new Image();


		img1.src = $ref+"/exercise/images/exe2/backgnd.png";


		$("#countdown3").fadeIn(100).delay(1600).fadeOut(100,function(){

			$(".space").addClass('launchShake');

			myRocketLunch.play();
			$(".burningfire").fadeIn(100);
			$("#cloud1").delay(100).fadeIn(10).animate({left:'1%',bottom:'-15%',width:"55%"},3000);
			$("#cloud2").delay(200).fadeIn(10).animate({left:'50%',width:"50%"},3000);
			$("#cloud3").delay(450).fadeIn(10).animate({left:'20%'},3000);

			$("#cloud5").delay(600).fadeIn(10).animate({bottom:"10%"},3000);
			$("#cloud6").delay(600).fadeIn(10).animate({bottom:"10%"},3000);


			$(".rocketsrc").delay(1000).animate({bottom:"20%"},1500).animate({bottom:"130%"},2000,function(){

				$("#mysolarSpace").delay(500).fadeOut(100);//fadeout ends
				$("#mysolarSpace2").delay(500).fadeIn(100,function(){

					$(".rocketText").html(data.string.space_7);

					$(".rocketsrc1").animate({bottom:"130%"},2000);
				}).delay(2000).fadeOut(100,function(){
					$(".rocketText").fadeOut(10,function(){
						$(this).html(data.string.space_8);
					}).delay(4000).fadeIn(10);

				});//fadeout ends

				$("#mysolarSpace2a").delay(2500).fadeIn(100,function(){


					$(".rocketsrc2").animate({'bottom':'20%','left':'20%'},4500,function(){

					}).animate({'bottom':'70%','left':'70%'},4000,function(){

					}).animate({'bottom':'110%','left':'110%'},500);

					$(this).addClass('moveBackground2');
				}).delay(9000).fadeOut(100,function(){
					$(this).removeClass('moveBackground2');
					myRocketLunch.stop();
				});//fadeout ends


				$("#mysolarSpace3").delay(11500).fadeIn(100,function(){

					var whatVal=arrayCounter-1;
					var randdomNumber =newarray[whatVal];
					$(this).addClass('preImage'+randdomNumber);
					$(".rocketText").html(data.string.space_9);
					$(".landAstroGirl").fadeIn(500);
					$(".landRocket").fadeIn(500);
					$(".astrospeech").delay(800).fadeIn(500);
					$(".rocketText").delay(4000).fadeOut(100,function(){

						$(".astrospeech").html(data.string.space_11);
						$(".rocketLife").show(0);

					}).delay(4000).fadeIn(100,function(){
						$(this).html(data.string.space_12);
						$("#activity-page-next-btn-enabled").fadeIn(10);
					});//fadein ends
				});//fadein ends
			});//rocketsrc ANIMATE ends
		});//fadeIn countdown ends


		/*** Countdown ***/
		$("#pr1").delay(300).fadeOut(100);
		$("#pr2").delay(500).fadeIn(100).delay(300).fadeOut(100);
		$("#pr3").delay(1000).fadeIn(100).delay(300).fadeOut(100);
		$("#pr4").delay(1500).fadeIn(100);
		/*** Countdown ***/
	} // blastOff function ends




	/***  loads second screen with lunchpad      ***/
	function startBlastOff()
	{
		$dataval={text1:data.string.space_6,
				rocketsrc:$ref+"/exercise/images/exe2/rocket.png",
				landAstroGirl:$ref+"/exercise/images/exe2/deepa01.png",
				astrospeech:data.string.space_10,
				startIt:data.string.space_21,
				rocketLife:$ref+"/exercise/images/exe2/life.png",
				rocketStand:$ref+"/exercise/images/exe2/stand.png",
				landRocket:$ref+"/exercise/images/exe2/landing.png",
				smoke:	$ref+"/exercise/images/exe2/smoke.png",
				burningfire:$ref+"/exercise/images/exe2/rocket_flame.png"
		};


		var source;
		source   = $("#template-2").html();

		var template = Handlebars.compile(source);
		var html=template($dataval);

		$(".allquestion").fadeOut(10,function(){
			$(this).html(html);
		}).delay(100).fadeIn(10,function(){
			$(".rocketsrc").fadeIn(10);
			$(".startIt").fadeIn(10);
		});//fadin ends
	} //startBlastOff function ends

	//first function that will give back story
	function getBackStory()
	{
		$dataval={
			spaceTravelTitle:data.string.space_1,
			spaceBox:[
				{spaceid:"space_1",spaceText:data.string.space_2},
				{spaceid:"space_2",spaceText:data.string.space_3},
				{spaceid:"space_3",spaceText:data.string.space_4},
				{spaceid:"space_4",spaceText:data.string.space_5},
				{spaceid:"space_5",spaceText:data.string.space_6}
			],
			imgspace:$ref+"/exercise/images/exe2/deepa02.png"

		};

		var source;

		source   = $("#template-1").html();
		var template = Handlebars.compile(source);
		var html=template($dataval);

		$(".allquestion").fadeOut(10,function(){
			$(this).html(html);
		}).delay(100).fadeIn(10,function(){

			$(".spaceTravelTitleBack").delay(2500).fadeOut(10);

			$(".spaceStory").delay(1500).fadeIn(10,function(){
					var $whatnextbtn=getSubpageMoveButton($lang,"next");
					$(".lastArrow").html($whatnextbtn);
					$(".lastArrow2").html($whatnextbtn);
			});	//fadein function
		});//fadein function
	}//getBackStory function ends
});//main function ends

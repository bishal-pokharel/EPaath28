
var arrayCorr=getCorrection();
var imgcorr=getImage();
var getRightVal=0;
$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);
	$("#repeatId").html(getReloadBtn());
	$(".instructText").html(data.string.exeByQues);




	var counter=0;
	var totalCounter=15;


	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);

	var newarray=shuffleArray(arrayquestion);


	var rightcounter=0, wrongCounter=0;

	myquestion(newarray[counter],counter);
	//myquestion2(2,2);




	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;


		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}
	});



	$(".allquestion").on("click",".ans",function(){

		var isCorrect=parseInt($(this).attr('corr'));
		var thisId=$(this).attr('id');
		$("#"+thisId).removeClass('ans');

		//$("#showAns div").removeClass('ans');

		//$("#showAns div[corr='1'] img").addClass('corrans');
		//$("#showAns div[corr='1'] .optname").show(0);

		if(isCorrect==1)
		{
			$("#showAns div").removeClass('ans');
			//$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
			getRightVal+=6;
			$("#commet").animate({"right":getRightVal+"%"},500);

			$("#"+thisId).append('<img src="'+$ref+'/exercise/images/correct.png" class="imgShowCorrme" />')


			rightcounter++;
			$("#activity-page-next-btn-enabled").fadeIn();
		}
		else
		{
			//$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
			$("#"+thisId).append('<img src="'+$ref+'/exercise/images/incorrect.png" class="imgShowCorrme" />');

			wrongCounter++;
		}


	});
});

var randOMNumber;

function myquestion(questionNo,counter)
{

	loadTimelineProgress(16,(counter+1));

	var myquestion,myanswer;


	var $dataval=getData(questionNo,counter+1);

	var source;

	source   = $("#label-template").html();

	var template = Handlebars.compile(source);


	var html=template($dataval);


	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#Imgshow").fadeOut(10);

	}).delay(100).fadeIn(10,function(){

		$('#commet').addClass('shake');
		if(counter>0)
		{
			$("#commet").css({"right":getRightVal+"%"});
		}



	});
}

function getCorrection()
{

	var arrayCorr=[
		[0,1,0],
		[0,0,1],
		[1,0,0],
		[0,1,0],
		[0,0,1],
		[1,0,0],
		[0,0,1],
		[0,0,1],
		[0,0,1],
		[0,0,1],
		[1,0,0],
		[0,0,1],
		[1,0,0],
		[0,0,1],
		[0,1,0],
	]

	return arrayCorr;
}

function getImage()
{

	var arrayCorr=[
		["earth","mercury","jupiter"],
		["moon","mars","venus"],
		["mars","mercury","jupiter"],
		["venus","earth","neptune"],
		["moon","mercury","sun"],
		["saturn","neptune","mercury"],
		["neptune","mercury","uranus"],
		["mars","moon","sun"],
		["earth","uranus","saturn"],
		["mars","mercury","neptune"],
		["moon","venus","sun"],
		["venus","earth","jupiter"],
		["moon","mars","saturn"],
		["mars","earth","uranus"],
		["saturn","jupiter","earth"]
	]

	return arrayCorr;
}
function getData(cnts,countNo)
{
	var dataval;
	var corr1=corr2=corr3=0;
	var imgsrc;

	var correctionArr=arrayCorr[cnts-1];
	var imgArr=imgcorr[cnts-1];

	var cntSpan="<span class='myspanno'>"+countNo+"</span>";

	if($lang=="np")
		cntSpan="<span class='myspanno'>"+ole.nepaliNumber(countNo)+"</span>";

	var optObjs=[
			{optObj1:"opt_"+cnts+"_1", yesno:correctionArr[0], optObjval:data.string['opt_'+cnts+'_1'],imgOptSrc:$ref+"/exercise/images/exe1/"+imgArr[0]+".png"},
			{optObj1:"opt_"+cnts+"_2", yesno:correctionArr[1], optObjval:data.string['opt_'+cnts+'_2'],imgOptSrc:$ref+"/exercise/images/exe1/"+imgArr[1]+".png"},
			{optObj1:"opt_"+cnts+"_3", yesno:correctionArr[2], optObjval:data.string['opt_'+cnts+'_3'],imgOptSrc:$ref+"/exercise/images/exe1/"+imgArr[2]+".png"},

		];

	var myArrAbs=new Array(0,1,2);
	var secArry=shuffleArray(myArrAbs);
	var imgFst1=secArry[0];
	var imgFst2=secArry[1];
	var imgFst3=secArry[2];


	dataval={
		bg2:$ref+"/exercise/images/exe1/bg2.png",
		commet:$ref+"/exercise/images/exe1/commet.png",
		myQuesion:cntSpan+data.string['exe_'+cnts],
		optObj:[optObjs[imgFst1],optObjs[imgFst2],optObjs[imgFst3]]
	}


	return dataval;
}



function randomNumberFromRange(min,max)
{
    var randomNumber = Math.floor(Math.random()*(max-min+1)+min);

    return randomNumber;
}
function myquestion2(right,wrong)
{
	loadTimelineProgress(16,16);

	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);


	var $dataval={
		rite:data.string.exe2,
		wrng:data.string.exe3,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:data.string["exe_1"],qans: data.string.opt_1_2,quesno:"ques_1"},
			{ques:data.string["exe_2"],qans: data.string.opt_2_3,quesno:"ques_2"},
			{ques:data.string["exe_3"],qans: data.string.opt_3_1,quesno:"ques_3"},
			{ques:data.string["exe_4"],qans: data.string.opt_4_2,quesno:"ques_4"},
			{ques:data.string["exe_5"],qans: data.string.opt_5_3,quesno:"ques_5"},
			{ques:data.string["exe_6"],qans: data.string.opt_6_1,quesno:"ques_6"},
			{ques:data.string["exe_7"],qans: data.string.opt_7_3,quesno:"ques_7"},
			{ques:data.string["exe_8"],qans: data.string.opt_8_3,quesno:"ques_8"},
			{ques:data.string["exe_9"],qans: data.string.opt_9_3,quesno:"ques_9"},
			{ques:data.string["exe_10"],qans: data.string.opt_10_3,quesno:"ques_10"},
			{ques:data.string["exe_11"],qans: data.string.opt_11_1,quesno:"ques_11"},
			{ques:data.string["exe_12"],qans: data.string.opt_12_3,quesno:"ques_12"},
			{ques:data.string["exe_13"],qans: data.string.opt_13_1,quesno:"ques_13"},
			{ques:data.string["exe_14"],qans: data.string.opt_14_3,quesno:"ques_14"},
			{ques:data.string["exe_15"],qans: data.string.opt_15_2,quesno:"ques_15"}

		]
	}


	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);

		$("#Imgshow").fadeOut(10);


	}).delay(100).fadeIn(10,function(){

		ole.footerNotificationHandler.pageEndSetNotification();

	});
}

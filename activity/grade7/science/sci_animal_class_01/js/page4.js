
var  $counter=0;
$.fn.dropMe=function ($accept,$arrlen,$newarr)
{
	var $this=$(this);

	$this.droppable({
        accept: $accept,
        drop: function( event, ui ) {

        	var img=ui.draggable.attr('src');
        	$("#showImg").remove('img');
        	$(this).append("<img src='"+img+"' />");

        	$counter++;

        	if($counter>=$arrlen)
        	{
        		$("#showImg").fadeOut();

        		$("#p4_2").html(data.string.p4_8);

        		$("#p4_2").addClass('textBlinking');

        		$(".dropbox").addClass('clickableDropBox');
        		 $(".dropbox .dropimg").droppable( "option", "disabled", true );
        	}
        	else
        	{
        		var newimg='<img src="activity/grade7/science/sci_animal_class_01/images/'+$newarr[$counter]+'.png" id="'+$newarr[$counter]+'" />';

				$("#showImg").html(newimg);
				$("#showImg img").draggable({ revert: "invalid"});
        	}
        }
    });

};



$(function(){



	var typeList,titleList,textList,exceptionlist,factslist, allimgLst,bloodList;
	var arrcount=0;
	var typeclickcounter=0;

	ole.footerNotificationHandler.pageEndSetNotification();

	$("#head_title").html(data.string.p4_1);
	$("#p4_2").html(data.string.p4_2);
	$("#p4_3").html(data.string.p4_3);
	$("#p4_4").html(data.string.p4_4);
	$("#p4_5").html(data.string.p4_5);
	$("#p4_6").html(data.string.p4_6);
	$("#p4_7").html(data.string.p4_7);

	var newArr=new Array('p3_1','p3_23','p3_3','p3_5','p3_15','p3_7','p3_11','p3_17','p3_9','p3_19','p3_21','p3_13');


	var newarray=shuffleArray(newArr);

	var arrLen=newarray.length;
	loadTimelineProgress(1,1);


	//$(".dropbox").addClass('clickableDropBox');


	$("#showImg").delay(500).fadeIn(function(){


		var img='<img src="activity/grade7/science/sci_animal_class_01/images/'+newarray[arrcount]+'.png" id="'+newarray[arrcount]+'" />';

		$(this).html(img);
		$("#showImg img").draggable({ revert: "invalid"});

	});



	$("#drop_1 .dropimg").dropMe('#p3_7, #p3_9',arrLen,newarray);
	$("#drop_2 .dropimg").dropMe('#p3_11',arrLen,newarray);
	$("#drop_3 .dropimg").dropMe(' #p3_13, #p3_15, #p3_17',arrLen,newarray);

	$("#drop_4 .dropimg").dropMe('#p3_19, #p3_21, #p3_23',arrLen,newarray);

	$("#drop_5 .dropimg").dropMe('#p3_1, #p3_3, #p3_5',arrLen,newarray);




	 typeList={
	 			'drop_1':['p4_1.gif','p4_2.gif','p4_4.gif','p4_5.gif','p4_11.gif','p4_37.png'],

	 			'drop_2':['p4_25.gif','p4_42.png','p4_24.png','p4_48.png','p4_27.png','p4_15.gif'],

	 			'drop_3':['p4_22.gif','p4_6.gif','p4_23.png','p4_47.png','p4_40.png','p4_49.png','p4_38.png'],

	 			'drop_4':['p4_7.gif','p4_8.gif','p4_26.png','p4_41.png','p4_36.png'],

	 			'drop_5':['p4_12.png','p3_3.png','p4_13.png','p4_9.png','p4_39.png','p4_10.png','p4_28.png']
		 	};

	 titleList={'drop_1':'p4_3','drop_2':'p4_4','drop_3':'p4_5','drop_4':'p4_6','drop_5':'p4_7'};


	textList={
	 			'drop_1':['p4_9','p4_10','p4_11','p4_12','p4_13','p4_14'],

	 			'drop_2':['p4_15','p4_16','p4_18','p4_19','p4_20','p4_21'],

	 			'drop_3':['p4_23','p4_24','p4_25','p4_26','p4_27','p4_28','p4_28_1'],

	 			'drop_4':['p4_29','p4_30','p4_31','p4_32','p4_33'],

	 			'drop_5':['p4_34','p4_35','p4_36','p4_37','p4_38','p4_39','p4_40']
	 		};




	 exceptionlist={ 'drop_4':{0:'p4_44'},
					'drop_5':{2:'p4_43'}
	 };

	factslist={ 'drop_5':['p4_41','p4_42']
	};


	bloodList={ 'drop_1':1,
				'drop_2':1,
				'drop_3':1,
				'drop_4':2,
				'drop_5':2
	}





	$("#totalPage").on('click','.clickableDropBox', function(){

		$("#p4_2").hide(0);
		var id=$(this).attr('id');

		var titlThis=titleList[id];
		var para=textList[id][typeclickcounter];

		var whtval=getSubpageMoveButton($lang,"next");

		var $dataval = {
			innerhead : data.string[titlThis],
			innerDesc:data.string[para],
			dropbxid:id,
			imgNextimgsrc:whtval

		}
		var $datalst=textList[id].length;
		loadTimelineProgress(($datalst+1),1);


		var source   = $("#my-template").html();

		var template = Handlebars.compile(source);

		var html = template($dataval);

		$("#animalContent2").html(html);

		var timestamp2 = new Date().getTime();

		$('#insideimgBox2').find('img').attr('src', 'activity/grade7/science/sci_animal_class_01/images/'+typeList[id][typeclickcounter]+'?'+timestamp2);





		if(factslist[id])
		{

			var splittxt=id.split("_");

			getExceptLink('fact_'+splittxt[1],'factid',data.string.p4_47_2);
		}
		if(bloodList[id])
		{


			var splittxt=id.split("_");
			if(bloodList[id]==1)
				getExceptLink('blood_'+splittxt[1],'bldTypeId',data.string.p4_47_3);
			else
				getExceptLink('blood_'+splittxt[1],'bldTypeId',data.string.p4_47_4);
		}

		if(exceptionlist[id]){


			if(exceptionlist[id][typeclickcounter])
			{
				var splittxt=id.split("_");
				getExceptLink('exp_'+splittxt[1]+'_'+typeclickcounter,'expid',data.string.p4_47_1);
			}

		}

		typeclickcounter++;
		$("#animalContent").hide(0);
		$("#animalContent2").show(0);


	});

	$("#animalContent2").on('click','#innerNextBtn', function(){

		$(".expid").remove();
		$(this).fadeOut();
		var id=$(this).attr('datadef');
		var para=textList[id][typeclickcounter];
		var $datalst=textList[id].length;



		loadTimelineProgress( ($datalst+1),typeclickcounter+1);

		var timestamp2 = new Date().getTime();


		$('#insideimgBox2').find('img').attr('src', 'activity/grade7/science/sci_animal_class_01/images/'+typeList[id][typeclickcounter]+'?'+timestamp2);

		$("#insidetxtBox2").html("<p>"+data.string[para]+"</p>");

		//put label in mamal lungs
		if(id=='drop_5')
		{
			if(typeclickcounter==4)
			{
				/*$("#insiderPage2").append("<div id='p4_47_7'>"+data.string.p4_47_7+"</div>");
				$("#insiderPage2").append("<div id='p4_47_5'>"+data.string.p4_47_5+"</div>");
				$("#insiderPage2").append("<div id='p4_47_6'>"+data.string.p4_47_6+"</div>");
				*/
			}
			else if(typeclickcounter==5)
			{
				$("#p4_47_7, #p4_47_5, #p4_47_6").remove();

				/*$("#insiderPage2").append("<div id='p4_47_8'>"+data.string.p4_47_8+"</div>");
				$("#insiderPage2").append("<div id='p4_47_9'>"+data.string.p4_47_9+"</div>");
				$("#insiderPage2").append("<div id='p4_47_10'>"+data.string.p4_47_10+"</div>");
				$("#insiderPage2").append("<div id='p4_47_11'>"+data.string.p4_47_11+"</div>");
				$("#insiderPage2").append("<div id='p4_47_12'>"+data.string.p4_47_12+"</div>");
				$("#insiderPage2").append("<div id='p4_47_13'>"+data.string.p4_47_13+"</div>");
				$("#insiderPage2").append("<div id='p4_47_18'>"+data.string.p4_47_18+"</div>");
				*/
				$("#insiderPage2").append("<div id='p4_47_14'>"+data.string.p4_47_14+"</div>");
				$("#insiderPage2").append("<div id='p4_47_15'>"+data.string.p4_47_15+"</div>");
				$("#insiderPage2").append("<div id='p4_47_16'>"+data.string.p4_47_16+"</div>");
				$("#insiderPage2").append("<div id='p4_47_17'>"+data.string.p4_47_17+"</div>");

			}
			else
			{
				$("#p4_47_8, #p4_47_9, #p4_47_10, #p4_47_11, #p4_47_12, #p4_47_13, #p4_47_14, #p4_47_15, #p4_47_16, #p4_47_17, #p4_47_18").remove();
			}


		}
		else if(id=='drop_4')
		{

			if(typeclickcounter==3)
			{
					$("#insiderPage2").append("<div id='p4_48'>"+data.string.p4_48+"</div>");
			}
			else
			{
				$("#p4_48").remove();
			}
		}
		else
		{


		}


		if(exceptionlist[id]){

			if(exceptionlist[id][typeclickcounter])
			{
				var splittxt=id.split("_");
				getExceptLink('exp_'+splittxt[1]+'_'+typeclickcounter,'expid',data.string.p4_47_1);
			}

		}


		typeclickcounter++;


		if(typeclickcounter>=($datalst+1))
		{
			//$(this).fadeOut();

			var allTxt=textList[id];
			var str="<ul></ul>";

			$("#insidetxtBox").append(str);

			$.each( allTxt, function( key, value ) {
				$("#insidetxtBox ul").append("<li><p>"+data.string[value]+"</p></li>");
			});


			var allImg=typeList[id];
			var nstr="<ul></ul>";


			$.each( allImg, function( key, value ) {
				$("#insideimgBox").append("<img src='activity/grade7/science/sci_animal_class_01/images/"+value+"' />");
			});

			$("#insiderPage").show(0);
			$("#insiderPage2").hide(0);
			$("#innerBtn").fadeIn();

		}
		else
		{

				$(this).fadeIn();

		}


	}); //next click

	$("#animalContent2").on('click','#innerBtn', function(){
		typeclickcounter=0;
		$("#animalContent2").hide(0);
		$("#animalContent2").html("");
		$("#animalContent").show(0);

		$("#myExcpetion").empty().hide(0);
		loadTimelineProgress(1,1);
	});



	$("#animalContent2").on('click',".factid",function(){


		var id=$(this).attr('id');

		var splittxt=id.split("_");
		var imgCloseSrc=getCloseBtn();

		var $datav = {

			popupTitle :data.string.p4_47_2,
			imgCloseSrc:imgCloseSrc
		}


		var source   = $("#my-exp-template").html();
		var template = Handlebars.compile(source);
		var html = template($datav);


		$(".expPopUp").find('.expPopUpContent').html(html);

		if(splittxt[1]=='5')
		{
			$(".expPopUpContent").find('.expBox').append('<div class="newInsiderFct"><p>'+data.string.p4_41+'</p><img src="activity/grade7/science/sci_animal_class_01/images/p4_14.png" title="bat" ></div>');

			$(".expPopUpContent").find('.expBox').append('<div class="newInsiderFct2"><img src="activity/grade7/science/sci_animal_class_01/images/p4_43.png" title="whale" ><p>'+data.string.p4_42+'</p><img src="activity/grade7/science/sci_animal_class_01/images/p4_44.png" title="Dolphin" ></div>');


		}

		$(".expPopUp").show(0);


	});


	$("#animalContent2").on('click',".expid",function(){


		var id=$(this).attr('id');

		var splittxt=id.split("_");
		var imgCloseSrc=getCloseBtn();

		var $datax = {

			popupTitle :data.string.p4_47_1,
			imgCloseSrc:imgCloseSrc
		}

		var source   = $("#my-exp-template").html();
		var template = Handlebars.compile(source);
		var html = template($datax);


		$(".expPopUp").find('.expPopUpContent').html(html);

		var valInsider='';

		if(splittxt[1]=='5')
		{
			valInsider='<div class="newInsider"><p>'+data.string.p4_43+'</p><img src="activity/grade7/science/sci_animal_class_01/images/p4_16.png" title="Platypus" ><img src="activity/grade7/science/sci_animal_class_01/images/p4_17.png" title="Echidna" ></div>';


		}
		else if(splittxt[1]=='4')
		{
			valInsider='<div class="newInsider"><p>'+data.string.p4_44+'</p><img src="activity/grade7/science/sci_animal_class_01/images/p4_46.png" title="Penguin" ><img src="activity/grade7/science/sci_animal_class_01/images/p4_45.png" title="Kiwi" ></div>';


		}


		$(".expPopUpContent").find('.expBox').append(valInsider);
		$(".expPopUp").show(0);


	});


	$("#animalContent2").on('click',".bldTypeId",function(){

		var id=$(this).attr('id');

		var splittxt=id.split("_");

		var titlebd, desbld, sumbld,bldimg1,bldimg2;

		console.log(id +""+splittxt);

		switch(splittxt[1])
		{
			case '1':
			case '2':
			case '3':
				titlebd=data.string.p4_46;
				desbld=data.string.p4_46_1;
				sumbld=data.string.p4_46_2;

				break;
			case '4':
			case '5':
				titlebd=data.string.p4_45;
				desbld=data.string.p4_45_1;
				sumbld=data.string.p4_45_2;
				break;
		}


		switch(splittxt[1])
		{
			case '1':
				bldimg1="activity/grade7/science/sci_animal_class_01/images/p4_31.png";
				bldimg2="activity/grade7/science/sci_animal_class_01/images/p4_32.png";
				break;
			case '2':
				bldimg1="activity/grade7/science/sci_animal_class_01/images/p4_18.png";
				bldimg2="activity/grade7/science/sci_animal_class_01/images/p4_19.png";
				break;
			case '3':
				bldimg1="activity/grade7/science/sci_animal_class_01/images/p4_33.png";
				bldimg2="activity/grade7/science/sci_animal_class_01/images/p4_34.png";
				break;

			case '4':
				bldimg1="activity/grade7/science/sci_animal_class_01/images/p4_29.png";
				bldimg2="activity/grade7/science/sci_animal_class_01/images/p4_30.png";
				break;

			case '5':

				bldimg1="activity/grade7/science/sci_animal_class_01/images/p4_20.png";
				bldimg2="activity/grade7/science/sci_animal_class_01/images/p4_21.png";
		}


		var imgCloseSrc=getCloseBtn();

		var $datavar = {

			bldTitle :titlebd,
			bldExp:desbld,
			bldSum:sumbld,
			bldimg1:bldimg1,
			bldimg2:bldimg2,
			imgCloseSrc:imgCloseSrc

		}

		var source   = $("#my-bldtype-template").html();
		var template = Handlebars.compile(source);
		var html = template($datavar);


		console.log($(".expPopUp").attr("class"));		
		$(".expPopUp").find('.expPopUpContent').html(html);



		$(".expPopUp").show(0);


	});


	$(".expPopUp").on('click',"#closeExp",function(){

		$(".expPopUp").find('.expPopUpContent').html('');
		$(".expPopUp").hide(0);
	});



});

function shuffleArray(array) {
	    for (var i = array.length - 1; i > 0; i--) {
	        var j = Math.floor(Math.random() * (i + 1));
	        var temp = array[i];
	        array[i] = array[j];
	        array[j] = temp;
	    }
	    return array;
}

function getExceptLink(id,cls,exception) {

	var html="<a id='"+id+"' class='"+cls+"'>"+exception+"</a>";



	$("#myExcpetion").append(html);

	$("#myExcpetion").show(0);
}

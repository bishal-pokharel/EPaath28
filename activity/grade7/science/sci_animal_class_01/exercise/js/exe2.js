
$(function(){

	var $i=0, arrcount=0;


	var  quesList;

	if($lang=="np")
		$("#head_title").html(data.string.exe_1+" १");
	else
		$("#head_title").html(data.string.exe_1+" 1");

	$("#exe_9").html(data.string.exe_9);



	var arrayquestion=new Array(1,2,3,4,5);
	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;




	var rightcounter=0, wrongCounter=0;


	quesList=getQuestion(newarray[arrcount],(arrcount+1));

	$("#totalBox").fadeIn(500,function(){
		myquestion(quesList);
	});

	$("#totalBox").on('click','.clickme',function(){

		var $this=$(this);
		var id=$this.attr('id');

		var questionNum=newarray[arrcount];

		$("#totalBox").find(".answers>div[id='incorrect']").removeClass('clickme').addClass('dontclick');

		$("#totalBox").find(".answers>div[id='correct']").removeClass('clickme').addClass('corrans');
		if(id=="correct")
		{
			rightOr=$(".rightExe");
			rightcounter++;

		}
		else
		{
			rightOr=$(".wrongExe");
			wrongCounter++;
		}


		rightOr.fadeIn(100).delay(1000).fadeOut(100,function()
		{

			arrcount++;
			if(arrcount<=4)
			{
				quesList=getQuestion(newarray[arrcount],(arrcount+1));
				myquestion(quesList);
			}
			else
			{
				loadTimelineProgress(6,6);
				$("#exe_9").html(data.string.exe_14);
				var $arry=[
					{"title":data.string.exe_9_1, "option":data.string.exe_9_1_a },
					{"title":data.string.exe_9_2, "option":data.string.exe_9_2_a },
					{"title":data.string.exe_9_3, "option":data.string.exe_9_3_a },
					{"title":data.string.exe_9_4, "option":data.string.exe_9_4_a },
					{"title":data.string.exe_9_5, "option":data.string.exe_9_5_a }
				];

				var totalAll=rightcounter+" / "+arrLen;
				var $datasoj={dobjects:$arry,

					ctAns:data.string.exe_10,
					ctAns1:rightcounter,
					wtAns:data.string.exe_11,
					wtAns1:wrongCounter,
					ttAns:data.string.exe_12,
					ttAns1:totalAll
				};

				var source2   = $("#solution-template").html();

				var template2= Handlebars.compile(source2);


				var html2=template2($datasoj);

				$("#totalBox").html(html2);



				ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);
				ole.footerNotificationHandler.pageEndSetNotification();
			


			}
		});

	});


	$("#totalBox").on('click','#replayid',function(){

		location.reload();
	});




});
function myquestion($arrays)
{
	var source   = $("#question-template").html();

	var template = Handlebars.compile(source);

	var $dataval=$arrays;
	var html=template($dataval);

	$("#totalBox").html(html);
	$("#totalBox").find(".answers > div").addClass('clickme').removeClass('dontclick');
}

function getQuestion($quesNo,arrcount)
{
	loadTimelineProgress(6,arrcount);
	var quesList;
	var arrayOption=new Array('a','b');
	var $correct1,$correct2;
	var newOption;

	if($quesNo%2==0)
	{
		newOption=['a','b'];
	}
	else
		newOption=['b','a'];

	var $answer1=data.string["exe_9_"+$quesNo+"_"+newOption[0]];
	var $answer2=data.string["exe_9_"+$quesNo+"_"+newOption[1]];
	if(newOption[0]=='a')
	{
		$correct1="correct";
		$correct2="incorrect";
	}
	else
	{
		$correct2="correct";
		$correct1="incorrect";
	}

	quesList={
		question:data.string["exe_9_"+$quesNo],
		correct1:$correct1,
		answer1:$answer1,
		correct2:$correct2,
		answer2:$answer2
	}

	return quesList;
}


$(function(){

	var $i=0, arrcount=0;
	
		
	var  quesList;


	if($lang=="np")
		$("#head_title").html(data.string.exe_1+" २");
	else 
		$("#head_title").html(data.string.exe_1+" 2");

	$("#exe_2").html(data.string.exe_2);
	$("#exe_3").html(data.string.exe_3);

	for($i=4;$i<=8;$i++)
	{
		$("#exe_"+$i).html(data.string["exe_"+$i]);
	}


	var arrayquetion=new Array(1,2,3,4,5);
	var newarray=shuffleArray(arrayquetion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;


	quesList=getQuestion(newarray[arrcount],(arrcount+1));

	myquestion(quesList);

	$("#totalBox").on('click','.clickMe',function(){

		var $this=$(this);
		var id=$this.attr('id');
		
		var counterarr=id.split("_");
		var counter=counterarr[1];

		var questionNum=newarray[arrcount];
		
		var rightOr=$(".rightExe");
		if(questionNum==counter)
		{
			rightOr=$(".rightExe");
			rightcounter++;


		}
		else
		{
			rightOr=$(".wrongExe");
			wrongCounter++;			
		}


		rightOr.fadeIn(100).delay(1000).fadeOut(100,function()
		{
			$("#box_"+questionNum).find('img').fadeIn(10);
			$(".divBoxes").removeClass('clickMe');
			arrcount++;
			if(arrcount<=4)
			{
				quesList=getQuestion(newarray[arrcount],(arrcount+1));
				myquestion(quesList);
			}
			else
			{
				
				loadTimelineProgress(6,6);
				var html2="<div class='scores'>"+
				"<div>"+data.string.exe_10+" : "+rightcounter+"</div>"+
				"<div>"+data.string.exe_11+" :"+wrongCounter+"</div>"+
				"<div>"+data.string.exe_12+" : "+rightcounter+" / "+arrLen+"</div>"+
				"</div>";

				$(".divBoxes2").html(html2);

				$("#activity-page-next-btn-enabled").show(0);
				
			}
		});
		
	});


	$("#totalBox").on('click','#replayid',function(){

		location.reload();
	});
	

	

});

$("#activity-page-next-btn-enabled").on('click',function () {
	ole.activityComplete.finishingcall();
})

function myquestion($arrays)
{
	
	$(".divBoxes2").html('').removeAttr('style').addClass('blur').fadeIn(500,function(){

		var source   = $("#question-template").html();
		
		var template = Handlebars.compile(source);
		
		var $dataval={dobjects:$arrays};
		var html=template($dataval);

		$(".divBoxes2").html(html);
		$(".divBoxes2").delay(2500).animate({'width': '46%','top': '0%','left': '28%','font-size': '1em','height':'45%'},1000,function(){
			$(this).removeClass('blur');
			$(".divBoxes").addClass('clickMe');
		});
		
		
	});
}

function getQuestion($quesNo,arrcount)
{
	
	loadTimelineProgress(6,arrcount);
	var quesList;

	switch($quesNo)
	{
		case  1:
			quesList=[data.string.exe_1_1,data.string.exe_1_2,data.string.exe_1_3];
			break;
		case  2:
			quesList=[data.string.exe_2_1,data.string.exe_2_2,data.string.exe_2_3];
			break;

		case  3:
			quesList=[data.string.exe_3_1,data.string.exe_3_2,data.string.exe_3_3,data.string.exe_3_4];
			break;
		case  4:
			quesList=[data.string.exe_4_1,data.string.exe_4_2,data.string.exe_4_3,data.string.exe_4_4];
			break;
		case  5:
			quesList=[data.string.exe_5_1,data.string.exe_5_2,data.string.exe_5_3,data.string.exe_5_4];
			break;

	}

	return quesList;
}
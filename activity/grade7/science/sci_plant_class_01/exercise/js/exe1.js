$(function(){

	// var $whatnextbtn=getSubpageMoveButton($lang,"next");

	// $("#activity-page-next-btn-enabled").html($whatnextbtn);

	if($lang=="np")
		$("#head_title").html(data.string.exe_1+" २");
	else
		$("#head_title").html(data.string.exe_1+" 2");


	$("#exe_2").html(data.string.exe_2);


	var valArray=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];

	var newImgarray=shuffleArray(valArray);
	//var newImgarray=valArray;
	var imgCount=0;

	var arrCount=newImgarray.length;
	var rightcounter=0, wrongCounter=0;
	var rightOr;

var imgArry={
		'1':'1.png',
		'2': '2.png',
		'3':'3.png',
		'4':'4.png',
		'5':'5.png',
		'6':'6.png',
		'7':'7.png',
		'8':'8.png',
		'9':'9.png',
		'10':'10.png',
		'11':'11.png',
		'12':'12.png',
		'13':'13.png',
		'14':'14.png',
		'15':'15.png',
		'16':'16.png'
	}

	var captionArry={
		'1':data.string.exe_c_1,
		'2':data.string.exe_c_2,
		'3':data.string.exe_c_3,
		'4':data.string.exe_c_4,
		'5':data.string.exe_c_5,
		'6':data.string.exe_c_6,
		'7':data.string.exe_c_7,
		'8':data.string.exe_c_8,
		'9':data.string.exe_c_9,
		'10':data.string.exe_c_10,
		'11':data.string.exe_c_11,
		'12':data.string.exe_c_12,
		'13':data.string.exe_c_13,
		'14':data.string.exe_c_14,
		'15':data.string.exe_c_15,
		'16':data.string.exe_c_16
	}

	quesList=getQuestion(newImgarray[imgCount],imgArry,captionArry,imgCount);


	myquestion(quesList);

	$("#totalBox").on('click','.clickme',function(){

		var $this=$(this);
		var id=$this.attr('id');

		$("#totalBox").find(".answers>div[id='correct']").removeClass('clickme').addClass('dontclick2');
		$("#totalBox").find(".answers>div[id='incorrect']").removeClass('clickme').addClass('dontclick');

		var questionNum=newImgarray[imgCount];
		if(id=="correct")
		{
			rightOr=$(".rightExe");
			rightcounter++;

		}
		else
		{
			rightOr=$(".wrongExe");
			wrongCounter++;
		}


		rightOr.fadeIn(100,function(){
			$("#activity-page-next-btn-enabled").fadeIn(100);
		});



	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(".rightExe, .wrongExe").fadeOut(0);

		$(this).fadeOut(10);

			imgCount++;
			console.log(imgCount);
			if(imgCount<=15)
			{
				quesList=getQuestion(newImgarray[imgCount],imgArry,captionArry,imgCount);
				myquestion(quesList);
			} else if (imgCount>16) {
				ole.activityComplete.finishingcall();
			} else
			{
				console.log("here");
				// alert("here");
				loadTimelineProgress(17,17);
					$("#exe_2").hide(0);
				var $fimage=['1.png','2.png','3.png','4.png','5.png','6.png','7.png','8.png', '12.png','16.png'];
				var $nimage=['9.png','10.png','11.png','13.png','14.png','15.png'];

				var totalAll=rightcounter+" / "+arrCount;

				var imgCloseSrc=getReloadBtn();
				var $dataval={

					exe_2_a:data.string.exe_2_a,
					exe_2_b:data.string.exe_2_b,
					fimage: $fimage,
					nimage:$nimage,
					ctAns:data.string.exe_10,
					ctAns1:rightcounter,
					wtAns:data.string.exe_11,
					wtAns1:wrongCounter,
					ttAns:data.string.exe_12,
					ttAns1:totalAll,
					imgCloseSrc:imgCloseSrc
				};

				var source   = $("#solution-template").html();

				var template = Handlebars.compile(source);


				var html=template($dataval);
				console.log(html);
				$("#exeContent #totalBox").html(html);
				$('#activity-page-next-btn-enabled').fadeIn();
			}

	});


	$("#totalBox").on('click','#replayid',function(){

		location.reload();
	});



});
function getQuestion($quesNo,$imgarr,$caparry,$cntter)
{
	loadTimelineProgress(17,($cntter+1));
	var quesList;
	var $correct1,$correct2;

	var $question=$ref+"/exercise/images/"+$imgarr[$quesNo];
	var $caption=$caparry[$quesNo];

	//var $whatVal=$quesNo.split(".");

	//var $whatVal1=parseInt($whatVal[0]);
	switch($quesNo)
	{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 12:
		case 16:
			$correct1="correct";
			$correct2="incorrect";
			break;

		case 9:
		case 10:
		case 11:
		case 13:
		case 14:
		case 15:
			$correct1="incorrect";
			$correct2="correct";
			break;
	}

	quesList={
		question:$question,
		optA:data.string.exe_2_a,
		optB:data.string.exe_2_b,
		correct1:$correct1,
		correct2:$correct2,
		caption:$caption
	}

	return quesList;
}

function myquestion($arrays)
{

	$("#totalBox").fadeOut(10,function(){

		var source   = $("#question-template").html();

		var template = Handlebars.compile(source);

		var $dataval=$arrays;
		var html=template($dataval);

		$(this).html(html);
		$(this).find(".answers > div").addClass('clickme').removeClass('dontclick');;
	}).delay(10).fadeIn(100,function(){

	});

}

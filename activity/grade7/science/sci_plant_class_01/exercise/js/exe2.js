
$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");

	$("#activity-page-prev-btn-enabled").html($whatnextbtn);

	var $i=0, arrcount=0;
	$("#replayid").html(getCloseBtn());


	var  quesList;


	if($lang=="np")
		$("#head_title").html(data.string.exe_1+" १");
	else
		$("#head_title").html(data.string.exe_1+" 1");


	$("#exe_3").html(data.string.exe_3);



	var arrayquestion=new Array(1,2,3,4,5);
	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;




	var rightcounter=0, wrongCounter=0;


	quesList=getQuestion(newarray[arrcount],arrcount);

	$("#totalBox").fadeIn(500,function(){
		myquestion(quesList);
	});

	$("#totalBox").on('click','.clickme',function(){

		var $this=$(this);
		var id=$this.attr('id');

		var questionNum=newarray[arrcount];

		$("#totalBox").find(".answers>div[id='correct']").removeClass('clickme').addClass('dontclick2');
		$("#totalBox").find(".answers>div[id='incorrect']").removeClass('clickme').addClass('dontclick');

		if(id=="correct")
		{
			rightOr=$(".rightExe");
			rightcounter++;

		}
		else
		{
			rightOr=$(".wrongExe");
			wrongCounter++;
		}

		rightOr.fadeIn(100,function(){
			$("#activity-page-next-btn-enabled").fadeIn(100);
		});



	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(".rightExe, .wrongExe").fadeOut(10,function(){
			$("#totalBox").html('');
		});

		$(this).fadeOut(10,function()
		{
			arrcount++;
			if(arrcount<=4)
			{
				quesList=getQuestion(newarray[arrcount],arrcount);
				myquestion(quesList);
			}
			else
			{

				loadTimelineProgress(6,6);
				var $arry=[
					{"title":data.string.exe_3_1, "option":data.string.exe_3_1_a },
					{"title":data.string.exe_3_2, "option":data.string.exe_3_2_a },
					{"title":data.string.exe_3_3, "option":data.string.exe_3_3_a },
					{"title":data.string.exe_3_4, "option":data.string.exe_3_4_a },
					{"title":data.string.exe_3_5, "option":data.string.exe_3_5_a }
				];

				$("#exe_3").hide(0);

				var totalAll=rightcounter+" / "+arrLen;
				var imgCloseSrc=getReloadBtn();

				var $datasoj={dobjects:$arry,

					ctAns:data.string.exe_10,
					ctAns1:rightcounter,
					wtAns:data.string.exe_11,
					wtAns1:wrongCounter,
					ttAns:data.string.exe_12,
					ttAns1:totalAll,
					imgCloseSrc:imgCloseSrc
				};

				var source2   = $("#solution-template").html();

				var template2= Handlebars.compile(source2);


				var html2=template2($datasoj);

				$("#totalBox").html(html2);

				ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);
				ole.footerNotificationHandler.pageEndSetNotification();



			}
		});

	});


	$("#totalBox").on('click','#replayid',function(){

		location.reload();
	});




});
function myquestion($arrays)
{
	var source   = $("#question-template").html();

	var template = Handlebars.compile(source);

	var $dataval=$arrays;
	var html=template($dataval);

	$("#totalBox").html(html);
	$("#totalBox").find(".answers > div").addClass('clickme');
}

function getQuestion($quesNo,arrcount)
{
	loadTimelineProgress(6,(arrcount+1));
	var quesList;
	var arrayOption=new Array('a','b');
	var $correct1,$correct2;
	var newOption;
	if($quesNo%2==0)
	{
		newOption=['a','b'];
	}
	else
		newOption=['b','a'];


	var $answer1=data.string["exe_3_"+$quesNo+"_"+newOption[0]];
	var $answer2=data.string["exe_3_"+$quesNo+"_"+newOption[1]];
	if(newOption[0]=='a')
	{
		$correct1="correct";
		$correct2="incorrect";
	}
	else
	{
		$correct2="correct";
		$correct1="incorrect";
	}

	quesList={
		question:data.string["exe_3_"+$quesNo],
		correct1:$correct1,
		answer1:$answer1,
		correct2:$correct2,
		answer2:$answer2
	}

	return quesList;
}

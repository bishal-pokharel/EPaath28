
$(function(){


	var $whatnext=getSubpageMoveButton($lang,"next");

	$("#activity-page-next-btn-enabled").append($whatnext);


	$whatnext=getSubpageMoveButton($lang,"prev");

	$("#activity-page-prev-btn-enabled").append($whatnext);


	$("#head_title").html(data.string.p1_1);

	// $("#p1_2").html(data.string.p1_2);


	var $contslide=0,$totalSlide=5;
	var $i;
	var countClicklife=0;



	// $("#p1_2").delay(500).fadeIn(500).delay(3500).animate({'margin-top': '-1%','font-size': '0.9em'},function(){
	// 	/*).delay(1500).fadeOut(100,function(){*/
		$(".topPage").fadeIn(500);
		showBtns($contslide,$totalSlide);
	// });


	loadTimelineProgress($totalSlide,$contslide+1);


	$("#activity-page-prev-btn-enabled").click(function(){

		$contslide--;
		loadTimelineProgress(5,$contslide);
		showBtns($contslide,$totalSlide);
	});


	$("#activity-page-next-btn-enabled").click(function(){

		$contslide++;
		loadTimelineProgress(5,$contslide+1);

		showBtns($contslide,$totalSlide);
	});


	$("#totalpage").on('click','.lifecycle',function(){

		countClicklife++;
		var $id= $(this).attr('id');
		$(this).css("pointer-events","none");
		// var $dataVal2={
		// 	p1_12:data.string.p1_12,
		// 	p1_13:data.string.p1_13
		// }
		// var source2;



		if($id=='rightOne')
		{
			$dataVal2={
				p1_13:data.string.p1_13
			}

			source2   = $("#chPopUp_5").html();
		}
		else
		{
			$dataVal2={
				p1_12:data.string.p1_12
			}
			source2   = $("#chPopUp_6").html();
		}
		console.log($id)
		var template2 = Handlebars.compile(source2);

		var html2 = template2($dataVal2);


		$(".chPopUp").html(html2);
		$(".chPopUp").fadeIn(200,function(){
			if($id=="rightOne")
			{
				cycleNonFlowering();
				$("#closePopBtn").html(getCloseBtn());
			}
			else
			{
				cycleFlowering();
				$("#closePopBtn").html(getCloseBtn());
			}
		});

		if(countClicklife==2)
		{
			ole.footerNotificationHandler.pageEndSetNotification();

		}

	});


	$(".chPopUp").on('click','#closePopBtn',function(){

		$(".chPopUp").html('');
		$(".chPopUp").fadeOut();
	});


});

function loadData($slidenum)
{
	var $dataVal;
	switch($slidenum)
	{
		case 0:
				break;
		case 1:
				$dataVal={
					p1_3:data.string.p1_3,
					p1_4:data.string.p1_4,
					p1_1_1:data.string.p1_1_1,
					p1_1_2:data.string.p1_1_2,
					p1_1_3:data.string.p1_1_3,
					p1_1_4:data.string.p1_1_4,
					p1_1_5:data.string.p1_1_5,
					p1_1_6:data.string.p1_1_6,
					p1_1_7:data.string.p1_1_7,
					p1_1_8:data.string.p1_1_8,
					p1_1_9:data.string.p1_1_9,
					p1_1_10:data.string.p1_1_10,
					p1_1_11:data.string.p1_1_11,
					p1_1_12:data.string.p1_1_12
				}
				break;
		case 2:
				$dataVal={
					p1_5:data.string.p1_5,
					p1_6:data.string.p1_6,
					p1_7:data.string.p1_7,
					p1_8:data.string.p1_8,
					p1_8_1:	data.string.p1_8_1,
					p1_8_3:	data.string.p1_8_3,
					p1_8_2:	data.string.p1_8_2,
					p1_8_4:	data.string.p1_8_4
				}
				break;
		case 3:
				$dataVal={
					p1_9:data.string.p1_9,
					p1_11:data.string.p1_11,
					p1_10:data.string.p1_10
				}
				break;
		case 4:
		$dataVal={
					p1_12:data.string.p1_12,
					p1_13:data.string.p1_13,
					p1_15:data.string.p1_15
				}
		break;

		case 5:
			$dataVal={
					p1_12:data.string.p1_12,
					p1_13:data.string.p1_13
				}
			break;
	}

	return $dataVal;
}

function showBtns($slidenum,$totalnum)
{

	if($slidenum==1)
	{
		$("#activity-page-prev-btn-enabled").hide(0);
		$("#activity-page-next-btn-enabled").show(0);
	}



	if(($slidenum!=1) && ($slidenum!=$totalnum))
	{
		$("#activity-page-prev-btn-enabled").show(0);
		$("#activity-page-next-btn-enabled").show(0);
	}

	if($slidenum==2)
	{
		$("#activity-page-next-btn-enabled").hide(0);
		$("#activity-page-prev-btn-enabled").hide(0);
	}
	console.log("$slidenum"+$slidenum);
	if($slidenum==4)
		$("#activity-page-next-btn-enabled").hide(0);
		// pageEndSetNotification();


	var $dataval=loadData($slidenum);

	var source   = $("#show-template_"+$slidenum).html();


	var template = Handlebars.compile(source);

	var html = template($dataval);
	$("#p1_2").html(data.string.p1_2);
	// $("#p1_2").delay(500).fadeIn(500).delay(3500).animate({'margin-top': '-1%','font-size': '0.9em'},function(){
	// 	/*).delay(1500).fadeOut(100,function(){*/
	// 	$(".topPage").fadeIn(500);
	// 	showBtns($contslide,$totalSlide);
	// });


	$("#p1_2").fadeIn(500).delay(500).animate({'margin-top': '-1%','font-size': '0.9em'});
		if($slidenum==0){
			$("#head_title").html(data.lesson.chapter) //chapter
			$("#p1_2").hide(); //chapter
		}
	$("#totalpage").fadeOut(10,function(){
		$(this).html(html);
	}).delay(10).fadeIn(100,function(){
		if($slidenum==2)
		{
			forTwo();

		}
	});





}
function reSetThis()
{
	$(".secondtxt,#quadrate,#qdrateBx,#qdrateBx2,#qdrateBx3").css({'display':'none'});
}
function forTwo()
{


	$("#p1_5").delay(500).fadeIn(500,function(){

		$("#quadrate").delay(500).fadeIn(500);

	}).delay(4500).fadeOut(500,function(){

		$("#p1_7").fadeIn(500,function(){
			$("#qdrateBx").delay(500).fadeIn(500);

		}).delay(4500).fadeOut(500,function(){

			$("#p1_6").fadeIn(500,function(){
				$("#qdrateBx").addClass('clickAble');
			});
		});
	});

	$("#totalpage").on('click','.clickAble',function(){

		$("#p1_6").fadeOut(10);

		$("#Extraid").fadeOut(100);
		$(this).fadeOut(100,function(){



			$("#qdrateBx2").fadeIn(500);

			$("#qdrateBx3").fadeIn(500);

			$("#conclusionTxt2").fadeIn(500);
			$("#conclusionTxt").fadeIn(500,function(){

				$("#activity-page-next-btn-enabled").show(0);
				$("#activity-page-prev-btn-enabled").show(0);
			});


		});
	});//clickable



}



function cycleNonFlowering()
{
	var countflowering=1;
	ole.vCenter(".mychPopUpContent",3);
	getnonFloweringVal(countflowering);


	$("#myNextBtn").click(function(){
		$(this).fadeOut(10);
		$("#myprevBtn").fadeOut(10);
		countflowering++;
		getnonFloweringVal(countflowering);
	});

	$("#myprevBtn").click(function(){
		$(this).fadeOut(10);
		$("#myNextBtn").fadeOut(10);
		countflowering--;
		getnonFloweringVal(countflowering);
	});




}//function

var $whatHeightchPopUp;

function getnonFloweringVal(countingme)
{


	var source2   = $("#chPopUp_5_"+countingme).html();


	var template2 = Handlebars.compile(source2);

	var html2, datavar;

	var $whatnext=getSubpageMoveButton($lang,"next");

	var $whatprev=getSubpageMoveButton($lang,"prev");


	switch(countingme)
	{
		case 1:
		{
			datavar={
				maintxt:data.string.p1_10_a_1
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2);
			}).delay(10).fadeIn(10,function(){

				$whatHeightchPopUp=parseInt($("#first_1").outerHeight());
				manageheight($whatHeightchPopUp);


				$("#mainText, #first_1").fadeIn(10);
				$("#mainText").removeClass("firstMain");



				$("#myNextBtn").html($whatnext).fadeIn(100);

			})
			break;
		}
		case 2:
		{
			datavar={
				maintxt:data.string.p1_10_a_2,
				text_2:data.string.p1_10_2
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText, #first_1").fadeIn(10);
				$("#mainText").addClass("firstMain");
				$("#first_2").fadeIn(10).animate({'width':'100%','left':'0%','top':''},1000,function(){
					$(".text_2").fadeIn(10);

					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);





				});
			})
			break;
		}
		case 3:
		{
			datavar={
				maintxt:data.string.p1_10_a_3,
				text_3:data.string.p1_10_3,
				text_4:data.string.p1_10_4
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2);
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").removeClass("firstMain").fadeIn(10);

				$("#first_3").fadeIn(200).delay(600).fadeOut(200);
				$("#first_4").delay(800).fadeIn(200,function(){
					$(".text_3").fadeIn(200);
					$(".text_4").fadeIn(200);
					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);

				});
			})
			break;
		}
		case 4:
		{
			datavar={
				maintxt:data.string.p1_10_a_4,
				text_5:data.string.p1_10_5
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2);
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10);
				$("#first_5").fadeIn(200).delay(800).fadeOut(200);
				$("#first_6").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_7").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_8").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_9").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_10").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_11").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_12").delay(7000).fadeIn(200,function(){

					$(".text_5").fadeIn(200);
					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);


				});
			})
			break;
		}
		case 5:
		{
			datavar={
				maintxt:data.string.p1_10_a_5,
				text_6:data.string.p1_10_6,
				text_7:data.string.p1_10_7,
				text_8:data.string.p1_10_8,
				text_9:data.string.p1_10_9
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2);
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10);
				$("#first_13").fadeIn(200).delay(800).fadeOut(200);
				$("#first_14").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_15").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_16").delay(3000).fadeIn(200,function(){
					 var outH=parseInt($(".myBox").outerHeight());

					$(".text_6").fadeIn(200);
					$(".text_7").fadeIn(200);
					$(".text_8").fadeIn(200);
					$(".text_9").fadeIn(200,function(){

						$("#myNextBtn").html($whatnext).fadeIn(100);
						$("#myprevBtn").html($whatprev).fadeIn(100);
					});

				});
			})
			break;
		}
		case 6:
		{
			datavar={
				maintxt:data.string.p1_10_a_6,
				text_10:data.string.p1_10_10
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2);
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10);

				$("#first_17").fadeIn(200).delay(800).fadeOut(200);
				$("#first_18").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_19").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_20").delay(3000).fadeIn(200).delay(800).fadeOut(200);

				$("#first_21").delay(4000).fadeIn(200,function(){
					$(".text_10").fadeIn(200);

					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);
				});
			})
			break;
		}
		case 7:
		{
			datavar={
				maintxt:data.string.p1_10_a_7,
				text_11:data.string.p1_10_11
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2);
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10);

				$("#first_21").fadeIn(10).animate({'width':'10%','left': '45%','top': '40%'},1200).fadeOut(100);
				$("#first_22").delay(200).fadeIn(200).delay(800).fadeOut(200);
				$("#first_23").delay(1200).fadeIn(200).delay(800).fadeOut(200);
				$("#first_24").delay(2200).fadeIn(200,function(){
					$(".text_11").fadeIn(10);
					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);

				});
			})
			break;
		}
		case 8:
		{
			datavar={
				maintxt:data.string.p1_10_a_8
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2);
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10);
				$("#first_24").fadeIn(10).animate({'width':'0%','left': '50%','top': '30%'},1200).fadeOut(100);
				$("#first_25").delay(600).fadeIn(100,function(){

						$("#myprevBtn").html($whatprev).fadeIn(100);
						$("#closePopBtn").show(0);
				});

			})
			break;
		}

	}//swicth




}

function manageheight($whatHeightchPopUp)
{

	var titleHgt=parseInt($("#pop_title").outerHeight());
				var $heightpop=$whatHeightchPopUp*1.05+titleHgt;
				$(".mychPopUpContent").height($heightpop);

				ole.vCenter(".mychPopUpContent",3);
}

/*
flowering plant l;ife cycle
**/
function cycleFlowering()
{

	var countnonflowering=1;
	ole.vCenter(".mychPopUpContent",3);
	getFloweringVal(countnonflowering);


	$("#myNextBtn").click(function(){
		$(this).fadeOut(10);
		$("#myprevBtn").fadeOut(10);
		countnonflowering++;
		getFloweringVal(countnonflowering);
	});

	$("#myprevBtn").click(function(){
		$(this).fadeOut(10);
		$("#myNextBtn").fadeOut(10);
		countnonflowering--;
		getFloweringVal(countnonflowering);
	});


}

function getFloweringVal(countingme)
{


	var source2   = $("#chPopUp_6_"+countingme).html();


	var template2 = Handlebars.compile(source2);

	var html2, datavar;

	var $whatnext=getSubpageMoveButton($lang,"next");

	var $whatprev=getSubpageMoveButton($lang,"prev");


	switch(countingme)
	{
		case 1:
		{
			datavar={
				maintxt:data.string.p1_14_1_a,
				stxt_1:data.string.p1_14_1
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){


				$whatHeightchPopUp=parseInt($("#second_1").outerHeight());
				manageheight($whatHeightchPopUp);
				$("#mainText").delay(50).fadeIn(400).removeClass("firstMain")


				$("#second_1").delay(100).fadeIn(500).delay(300).fadeOut(500);
				$("#second_2").delay(800).fadeIn(500).delay(300).fadeOut(500);
				$("#second_3").delay(1600).fadeIn(500).delay(300).fadeOut(500);

				$("#second_3a").delay(2200).fadeIn(500,function(){
					$(".stxt_1").fadeIn(100);

					$("#myNextBtn").html($whatnext).fadeIn(100);


				});
			})
			break;
		}
		case 2:
		{
			datavar={
				maintxt:data.string.p1_14_1_b,
				stxt_2:data.string.p1_14_2,
				stxt_3:data.string.p1_14_3,
				stxt_4:data.string.p1_14_4
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10).addClass("firstMain");
				$("#second_4").fadeIn(10,function(){
					$(".stxt_2").fadeIn(10);
					$(".stxt_3").fadeIn(10);
					$(".stxt_4").fadeIn(10);

					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);
				});
			})
			break;
		}
		case 3:
		{
			datavar={
				maintxt:data.string.p1_14_1_c,
				stxt_2:data.string.p1_14_2,
				stxt_3:data.string.p1_14_3,
				stxt_4:data.string.p1_14_4,
				stxt_5:data.string.p1_14_5,
				stxt_6:data.string.p1_14_6
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10).addClass("firstMain");

				$("#second_5").fadeIn(10,function(){
					$(".stxt_2").fadeIn(10);
					$(".stxt_3").fadeIn(10);
					$(".stxt_4").fadeIn(10);
				}).delay(200).fadeOut(100);

				$("#second_6").delay(200).fadeIn(200).delay(800).fadeOut(200);

				$("#second_7").delay(1200).fadeIn(200).delay(800).fadeOut(200);

				$("#second_8").delay(2200).fadeIn(200,function(){
					$(".stxt_5").fadeIn(10);
					$(".stxt_6").fadeIn(10);

					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);
				});
			})
			break;
		}
		case 4:
		{
			datavar={
				maintxt:data.string.p1_14_1_d,
				stxt_7:data.string.p1_14_7,
				stxt_8:data.string.p1_14_8
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10).addClass("firstMain");
				$("#second_9").fadeIn(200).delay(800).fadeOut(200);
				$("#second_10").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_11").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_12").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_13").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_14").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_15").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_16").delay(7000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_17").delay(8000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_18").delay(9000).fadeIn(200,function(){

					$(".stxt_7").fadeIn(10);
					$(".stxt_8").fadeIn(10);

					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);
				});

			})
			break;
		}
		case 5:
		{
			datavar={
				maintxt:data.string.p1_14_1_e,
				stxt_9:data.string.p1_14_9
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10).addClass("firstMain");

				$("#second_19").fadeIn(200).delay(800).fadeOut(200);
				$("#second_20").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_21").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_22").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_23").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_24").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_25").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_26").delay(7000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_27").delay(8000).fadeIn(200).delay(800).fadeOut(200);



				var timestamp33=new Date().getTime();

				$("#second_30").find('img').attr('src',$ref+'/images/page1/flower/30.gif?'+timestamp33);

				$("#second_30").delay(9000).fadeIn(200,function(){
					$(".stxt_9").fadeIn(10);

					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);
				});

			})
			break;
		}
		case 6:
		{
			datavar={
				maintxt:data.string.p1_14_1_f
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10);

				$("#second_31").fadeIn(200).delay(800).fadeOut(200);
				$("#second_32").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_33").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_34").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_35").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_36").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_37").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_38").delay(7000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_39").delay(8000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_40").delay(9000).fadeIn(200,function(){

					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);
				});

			})
			break;
		}
		case 7:
		{
			datavar={
				maintxt:data.string.p1_14_1_g,
				stxt_10:data.string.p1_14_10,
				stxt_11:data.string.p1_14_11,
				stxt_12:data.string.p1_14_12,
				stxt_13:data.string.p1_14_13
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10);

				$("#second_41").fadeIn(200).delay(800).fadeOut(200);
				$("#second_42").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_43").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_44").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_45").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_46").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_47").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_50").delay(7000).fadeIn(200,function(){
					$(".stxt_10").fadeIn(10);
					$(".stxt_11").fadeIn(10);
					$(".stxt_12").fadeIn(10);
					$(".stxt_13").fadeIn(10);

					$("#myNextBtn").html($whatnext).fadeIn(100);
					$("#myprevBtn").html($whatprev).fadeIn(100);
				});

			})
			break;
		}
		case 8:
		{
			datavar={
				maintxt:data.string.p1_14_1_h
			};

			html2 = template2(datavar);
			$(".mychPopUp").find(".myBox").fadeOut(10,function(){
				$(this).html(html2)
			}).delay(10).fadeIn(10,function(){

				manageheight($whatHeightchPopUp);

				$("#mainText").fadeIn(10).removeClass("firstMain");

				$("#second_53").fadeIn(200).delay(800).fadeOut(200);
				$("#second_54").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_55").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_56").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_57").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_58").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_59").delay(6000).fadeIn(200,function(){

					$("#myprevBtn").html($whatprev).fadeIn(100);
					$("#closePopBtn").show(0);

				});
			})
			break;
		}
	}//swicth
}

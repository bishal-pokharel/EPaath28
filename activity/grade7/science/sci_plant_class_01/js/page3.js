$.fn.ClickMe=function ($whatClick)
{
    var dropping=$("#position"+$whatDrop);
    var $accept="#first_"+$whatDrop;
    var dropped="#dropped_"+$whatDrop;
    dropping.show(0);

    dropping.droppable({
        accept: $accept,
        drop: function( event, ui ) {

            ui.draggable.hide(0);
            $(this).hide(0);
            $(dropped).show(0);

            $whatDrop++;
            $(".properPos").dropMe($whatDrop);
            
        }
    });



}



$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	var checkAllClicked = [null,null,null,null,null];

	loadTimelineProgress(1,1);
	
	$("#head_title").html(data.string.p1_1);
	$("#p3_1").html(data.string.p3_1);

	$("#part_5 span").html(data.string.p3_5_1);
	$("#part_4 span").html(data.string.p3_4_1);
	$("#part_3 span").html(data.string.p3_3_1);
	$("#part_2 span").html(data.string.p3_2_1);
	$("#part_1 span").html(data.string.p3_1_1);
	// ole.footerNotificationHandler.lessonEndSetNotification();

	
	var whatClicked, flowerclk;
	
	var ArrayVal={
		"part_1":
		{
			"title":data.string.p3_1_1,
			"dobjects":[	{"val":data.string.p3_1_2}	],
			"imges":$ref+"/images/page3/root.png",
			"labelObj":
			[
				{"labels":data.string.p3_1_5,"labelId":"p1Lb1"},
				{"labels":data.string.p3_1_6,"labelId":"p1Lb2"},
				
			],
			"wobjects":[{"wdesc":data.string.p3_1_3},{"wdesc":data.string.p3_1_4}],
			"largeimg":[
					{
						"imagesrc":$ref+"/images/page3/root/apple.png",
						"imgcaption":data.string.p3_1_9
					},
					{
						"imagesrc":$ref+"/images/page3/root/bamboo.png",
						"imgcaption":data.string.p3_1_10
					},
					{
						"imagesrc":$ref+"/images/page3/root/maize.png",
						"imgcaption":data.string.p3_1_11
					},
					{
						"imagesrc":$ref+"/images/page3/root/onion.png",
						"imgcaption":data.string.p3_1_12
					},
					{
						"imagesrc":$ref+"/images/page3/root/cactus.png",
						"imgcaption":data.string.p3_1_13
					},
					{
						"imagesrc":$ref+"/images/page3/root/potato.png",
						"imgcaption":data.string.p3_1_14
					}
				]
		},
		"part_2":
		{
			"title":data.string.p3_2_1,
			"dobjects":[{"val":data.string.p3_2_2}],
			"imges":$ref+"/images/page3/stem.png",
			"labelObj":
			[
				{"labels":data.string.p3_2_5,"labelId":"p2Lb1"},
				{"labels":data.string.p3_2_6,"labelId":"p2Lb2"},
				{"labels":data.string.p3_2_7,"labelId":"p2Lb3"}
			],
			"wobjects":[{"wdesc":data.string.p3_2_3},{"wdesc":data.string.p3_2_4}],
			"largeimg":[
					{
						"imagesrc":$ref+"/images/page3/stem/bamboo.png",
						"imgcaption":data.string.p3_2_8
					},
					{
						"imagesrc":$ref+"/images/page3/stem/cactus.png",
						"imgcaption":data.string.p3_2_9
					},
					{
						"imagesrc":$ref+"/images/page3/stem/maize.png",
						"imgcaption":data.string.p3_2_10
					},
					{
						"imagesrc":$ref+"/images/page3/stem/onion.png",
						"imgcaption":data.string.p3_2_11
					},
					{
						"imagesrc":$ref+"/images/page3/stem/paddy.png",
						"imgcaption":data.string.p3_2_12
					},
					{
						"imagesrc":$ref+"/images/page3/stem/apple.png",
						"imgcaption":data.string.p3_2_13
					}
				]
		},
		"part_3":
		{
			"title":data.string.p3_3_1,
			"dobjects":[
						{"val":data.string.p3_3_2}		],
			"imges":$ref+"/images/page3/leaf.png",
			"labelObj":
			[
				{"labels":data.string.p3_3_6,"labelId":"p3Lb1"},
				{"labels":data.string.p3_3_7,"labelId":"p3Lb2"},
				{"labels":data.string.p3_3_8,"labelId":"p3Lb3"}
			],
			"wobjects":[{"wdesc":data.string.p3_3_4},{"wdesc":data.string.p3_3_5}],
			
			"largeimg":[
					{
						"imagesrc":$ref+"/images/page3/leaf/apple.png",
						"imgcaption":data.string.p3_3_9
					},
					{
						"imagesrc":$ref+"/images/page3/leaf/bamboo.png",
						"imgcaption":data.string.p3_3_10
					},
					{
						"imagesrc":$ref+"/images/page3/leaf/cactus.png",
						"imgcaption":data.string.p3_3_11
					},
					{
						"imagesrc":$ref+"/images/page3/leaf/maize.png",
						"imgcaption":data.string.p3_3_12
					},
					{
						"imagesrc":$ref+"/images/page3/leaf/potato.png",
						"imgcaption":data.string.p3_3_13
					},
					{
						"imagesrc":$ref+"/images/page3/leaf/onion.png",
						"imgcaption":data.string.p3_3_14
					}
				]
		},
		"part_4":
		{
			"title":data.string.p3_4_1,
			"dobjects":[{"val":data.string.p3_4_2}],
			"imges":$ref+"/images/page3/seed.png",
			"labelObj":
			[
				{"labels":data.string.p3_4_6,"labelId":"p4Lb1"},
				{"labels":data.string.p3_4_7,"labelId":"p4Lb2"},
				{"labels":data.string.p3_4_8,"labelId":"p4Lb3"}
			],
			"wobjects":[
					{"wdesc":data.string.p3_4_3},
					{"wdesc":data.string.p3_4_4},
					{"wdesc":data.string.p3_4_5}],
			"largeimg":[
					{
						"imagesrc":$ref+"/images/page3/fruit/maize.png",
						"imgcaption":data.string.p3_4_9
					},
					{
						"imagesrc":$ref+"/images/page3/fruit/onion.png",
						"imgcaption":data.string.p3_4_10
					},
					{
						"imagesrc":$ref+"/images/page3/fruit/paddy.png",
						"imgcaption":data.string.p3_4_11
					},
					{
						"imagesrc":$ref+"/images/page3/fruit/potato.png",
						"imgcaption":data.string.p3_4_12
					},
					{
						"imagesrc":$ref+"/images/page3/fruit/apple.png",
						"imgcaption":data.string.p3_4_13
					},
					{
						"imagesrc":$ref+"/images/page3/fruit/mango.png",
						"imgcaption":data.string.p3_4_14
					}
				]
		}


	}


	var flowerVal={ 
		"flower_0":
		{
			"title":data.string.p3_5_1,
			"dobjects":[{"val":data.string.p3_5_2},{"val":data.string.p3_5_3}],
			"imgsrc":$ref+"/images/page3/floral/1.png",
			"labelObj":[
				{"labels":data.string.p3_5_4,"labelId":"f0Lb1"},
				{"labels":data.string.p3_5_5,"labelId":"f0Lb2"},
				{"labels":data.string.p3_5_6,"labelId":"f0Lb3"},
				{"labels":data.string.p3_5_7,"labelId":"f0Lb4"}
			]
		},
		"flower_1":
		{
			"title":data.string.p3_5_8,
			"dobjects":[{"val":data.string.p3_5_11}],
			"imgsrc":$ref+"/images/page3/floral/01.png",
			"labelObj":[
				{"labels":data.string.p3_5_5,"labelId":"f1Lb1"}
			]
		},
		"flower_2":
		{
			"title":data.string.p3_5_10,
			"dobjects":[{"val":data.string.p3_5_9}],
			"imgsrc":$ref+"/images/page3/floral/03.png",
			"labelObj":[
				{"labels":data.string.p3_5_4,"labelId":"f2Lb1"}
			]
		}
		,
		"flower_3":
		{
			"title":data.string.p3_5_12,
			"dobjects":[{"val":data.string.p3_5_13}],
			"imgsrc":$ref+"/images/page3/floral/05.png",
			"labelObj":[
				{"labels":data.string.p3_5_13_1,"labelId":"f3Lb1"},
				{"labels":data.string.p3_5_13_2,"labelId":"f3Lb2"},
				{"labels":data.string.p3_5_13_3,"labelId":"f3Lb3"},
				{"labels":data.string.p3_5_13_4,"labelId":"f3Lb4"}
			]
		},
		"flower_4":
		{
			"title":data.string.p3_5_14,
			"dobjects":[{"val":data.string.p3_5_15}],
			"imgsrc":$ref+"/images/page3/floral/06.png",
			"labelObj":[
				{"labels":data.string.p3_5_15_1,"labelId":"f4Lb1"},
				{"labels":data.string.p3_5_15_2,"labelId":"f4Lb2"},
				{"labels":data.string.p3_5_15_3,"labelId":"f4Lb3"},
				{"labels":data.string.p3_5_15_4,"labelId":"f4Lb4"},
				{"labels":data.string.p3_5_15_5,"labelId":"f4Lb5"},
				{"labels":data.string.p3_5_15_3,"labelId":"f4Lb6"},
				{"labels":data.string.p3_5_15_4,"labelId":"f4Lb7"}
			]
		},

		"flower_5":
		{
			"title":data.string.p3_5_16_1,
			"dobjects":[{"val":data.string.p3_5_16}],
			"imgsrc":$ref+"/images/page3/floral/1.png",
			"labelObj":[
				{"labels":data.string.p3_5_4,"labelId":"f0Lb1"},
				{"labels":data.string.p3_5_5,"labelId":"f0Lb2"},
				{"labels":data.string.p3_5_6,"labelId":"f0Lb3"},
				{"labels":data.string.p3_5_7,"labelId":"f0Lb4"}
			]
		},
		"flower_6":
		{
			"title":data.string.p3_5_16_1,
			"dobjects":[{"val":data.string.p3_5_16_1}],
			"imgsrc":$ref+"/images/page3/complete/1.png"
		}



	}

	/******* click everything except Flower*******************/
	$(".clickCls").click(function()
	{
		
		var $this=$(this);
		
		var id=$(this).attr('id');


		whatClicked=id;
		
		var myArray=ArrayVal[id];

		

	
		var $dataval = {
			innerhead:myArray.title,
			dobjects : myArray.dobjects,
			imges:myArray.imges,
			labelObj:myArray.labelObj,
			"p3_2":data.string.p3_2,
			wobjects:myArray.wobjects,
			whatnextbtn:$whatnextbtn,
			whatprevbtn:$whatprevbtn

		}
		
		ShowWhat( $("#part-template"),$dataval,whatClicked);

	});

	$("#plantBck2").on('click','div',function(){
		var index = $(this).index();
		checkAllClicked[index]=1;
	})


	$(".popUp").on('click','#nextPopBtn',function(){

		

		var myArray=ArrayVal[whatClicked];
		if(whatClicked=="part_3")
		{
			var $dataval = {
				innerhead:myArray.title,
				"p3_2":data.string.p3_2,
				wobjects:myArray.wobjects,
				"imgLeafbj":1,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn
			}
		}
		else
		{
			var $dataval = {
				innerhead:myArray.title,
				"p3_2":data.string.p3_2,
				wobjects:myArray.wobjects,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn
			}
		}
		
		
		
		ShowWhat($("#work-template"),$dataval,whatClicked);


	});



	$(".popUp").on('click','#nextPopBtnImg',function(){

		var myArray=ArrayVal[whatClicked];
		var imgCloseSrc=getCloseBtn();
		
		var $dataval = {
			innerhead:myArray.title,
			largeimg:myArray.largeimg,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn,
				imgCloseSrc:imgCloseSrc
		}
		
		ShowWhat( $("#image-template"),$dataval,whatClicked);


	});

	$(".popUp").on('click','#prevPopBtn',function(){

		var myArray=ArrayVal[whatClicked];
		
		var $dataval = {
			innerhead:myArray.title,
			dobjects : myArray.dobjects,
			imges:myArray.imges,
			labelObj:myArray.labelObj,
			wobjects:myArray.wobjects,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn
		}
		
		ShowWhat( $("#part-template"),$dataval,whatClicked);
	});

	$(".popUp").on('click','#prevPopBtnImg',function(){

		var myArray=ArrayVal[whatClicked];
			
		if(whatClicked=="part_3")
		{
			var $dataval = {
				innerhead:myArray.title,
				"p3_2":data.string.p3_2,
				wobjects:myArray.wobjects,
				"imgLeafbj":1,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn
			}
		}
		else
		{
			var $dataval = {
				innerhead:myArray.title,
				"p3_2":data.string.p3_2,
				wobjects:myArray.wobjects,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn
			}
		}
		
		ShowWhat($("#work-template"),$dataval,whatClicked);


	});


	$(".popUp").on('click','#closePopBtn',function(){

		$(".popUp").html('');
		$(".popUp").fadeOut();

		var checked = true;
		for (var i = 0; i < checkAllClicked.length; i++) {
			if (checkAllClicked[i]===null) {
				checked = false;
			}
		};
		if (checked) {
			ole.footerNotificationHandler.lessonEndSetNotification();
		};
	});

	/**************** Click ************************/
	$(".clickCls2").click(function()
	{
		
		var $this=$(this);
		
		var id=$(this).attr('id');

		flowerclk=0;
		whatClicked=id;
		
		ShowWhat2(flowerclk,flowerVal,true,false);

	});

	$(".popUp").on('click','#nextFlowerBtn',function(){

		flowerclk++;
		var islast=false;
		if(flowerclk<6)islast=false;
		else islast=true;

		ShowWhat2(flowerclk,flowerVal,false,islast);
	});

	$(".popUp").on('click','#prevFlowerBtn',function(){

		flowerclk--;
		var islast=false;
		var isFirst=false;
		
		if(flowerclk<6)islast=false;
		else islast=true;
		
		if(flowerclk==0) isFirst=true;
		else isFirst=false;

		ShowWhat2(flowerclk,flowerVal,isFirst,islast);
	});
	

	$(".popUp").on('click','#nextLastBtn',function(){

		flowerclk++;
		
		var myImgArray=[
					{
						"imagesrc":$ref+"/images/page3/flower/apple.png",
						"imgcaption":data.string.p3_3_17
					},
					{
						"imagesrc":$ref+"/images/page3/flower/cactus.png",
						"imgcaption":data.string.p3_3_18
					},
					{
						"imagesrc":$ref+"/images/page3/flower/maize.png",
						"imgcaption":data.string.p3_3_19
					},
					{
						"imagesrc":$ref+"/images/page3/flower/onion.png",
						"imgcaption":data.string.p3_3_20
					},
					{
						"imagesrc":$ref+"/images/page3/flower/paddy.png",
						"imgcaption":data.string.p3_3_21
					},
					{
						"imagesrc":$ref+"/images/page3/flower/potato.png",
						"imgcaption":data.string.p3_3_22
					}
				];


		var imgCloseSrc=getCloseBtn();
		var $dataval = 
		{
			innerhead:data.string.p3_5_1,
			largeimg:myImgArray,
			isflower:1,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn,
				imgCloseSrc:imgCloseSrc
		}

		ShowWhat( $("#image-template"),$dataval,whatClicked);
	});

	

});	



function ShowWhat2($whatFlower,flowerVal,$ifFirst,$iflast)
{
		
	var $whatnextbtn=getSubpageMoveButton($lang,"next");

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");

		var myArray=flowerVal['flower_'+$whatFlower];

		var $dataval;
		if($ifFirst)
		{
			$dataval = {
				innerhead:myArray.title,
				dobjects : myArray.dobjects,
				
				labelObj:myArray.labelObj,
				isFirstFlower:1,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn
			}
		}
		else if ($iflast)
		{
			$dataval = {
				innerhead:myArray.title,
				dobjects : myArray.dobjects,
				
				labelObj:myArray.labelObj,
				isLastFlower:1,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn
			}
		}
		else
		{
			$dataval = {
				innerhead:myArray.title,
				dobjects : myArray.dobjects,
				
				labelObj:myArray.labelObj,
				whatnextbtn:$whatnextbtn,
				whatprevbtn:$whatprevbtn
			}
		}
		
		
		var source   = $("#flower-template").html();

		
		
		var template = Handlebars.compile(source);
		
		var html = template($dataval);

		$(".popUp").html(html);
		var timestamp2 = new Date().getTime();
		var imgVal=myArray.imgsrc+'?'+timestamp2;
		
		if($whatFlower!=6)
			$("#imgObjects2").prepend('<img src="'+imgVal+'"  />');
		
		$(".popUp").fadeIn(500,function(){

			/*var outHeight=parseInt($(".myPopUp").outerHeight())/2;
			var innHeight=parseInt($(".myPopUpContent").outerHeight())/2;

			var inTop=outHeight-innHeight;

			$(".myPopUpContent").css({'top':inTop});*/

			$(".myPopUp").fadeIn(100,function(){

				$(".myPopUpContent").center(true);
				
				if($whatFlower==1)
				{
					
					$("#imgObjects2 img").delay(1000).fadeOut(100,function(){

						$("#imgObjects2").prepend('<img src="'+$ref+'/images/page3/floral/02.png"   />');
						$("#f1Lb1").fadeIn(100,function(){
							//$(".myPopUpContent").center(true);
						});
					});
				}
				else if($whatFlower==3)
				{
					
					
					$("#f3Lb1, #f3Lb2, #f3Lb3, #f3Lb4").fadeIn(100,function(){
						//$(".myPopUpContent").center(true);
					});
					
				}
				else if($whatFlower==4)
				{
					$("#imgObjects2 img").css({'width':'45%'});
					$("#imgObjects2").prepend('<img src="'+$ref+'/images/page3/floral/07.png" class="whatitis" />');
				
					$("#f4Lb1, #f4Lb2, #f4Lb3, #f4Lb4, #f4Lb5, #f4Lb6, #f4Lb7").fadeIn(100);
				}
				else if($whatFlower==6)
				{
					
					$("#imgObjects2").css({'text-align':'left'});
					$("#imgObjects2").prepend('<p>'+data.string.p3_5_16_2+'</p>');
						$("#imgObjects2").append('<div class="img23"><img src="'+$ref+'/images/page3/complete/1.png" class="whatitis2" />'+data.string.p3_5_16_6+'</div>');
					$("#imgObjects2").append('<div class="img23"><img src="'+$ref+'/images/page3/complete/2.png" class="whatitis2" />'+data.string.p3_5_16_7+'</div>');
					$("#imgObjects2").append('<div class="img23"><img src="'+$ref+'/images/page3/complete/3.png" class="whatitis2" />'+data.string.p3_5_16_8+'</div>');

					$("#descObjects3").append('<div class="img23"><img src="'+$ref+'/images/page3/complete/4.png" class="whatitis2" />'+data.string.p3_5_16_3+'</div>');
					$("#descObjects3").append('<div class="img23"><img src="'+$ref+'/images/page3/complete/5.png" class="whatitis2" />'+data.string.p3_5_16_4+'</div>');
					$("#descObjects3").append('<div class="img23"><img src="'+$ref+'/images/page3/complete/6.png" class="whatitis2" />'+data.string.p3_5_16_5+'</div>');
				}
				

			});

		});
}

function ShowWhat($whathtml,$dataval,whatClicked)
{
		
	
	var strwhat=$whathtml.attr('id');

	var source   = $whathtml.html();
	
	var template = Handlebars.compile(source);
	
	var html = template($dataval);

	$(".popUp").html(html).fadeIn(500,function()
	{
		$(".myPopUp").fadeIn(100,function(){
			$(".myPopUpContent").center(true);


			if(whatClicked=='part_3' && strwhat=='work-template')
			{
				 var timestamp2 = new Date().getTime();
				$(".myPopUpContent").find('#myleafimg').attr('src',$ref+'/images/page3/leaf/photosynthesis1.gif?'+timestamp2);
			
				
				 $('.myPopUpContent').find('img').load(function() {

				 	$("#leafImgcap").html(data.string.p3_3_15).fadeIn(100);
				 	setTimeout(function(){
				 		 var timestamp3 = new Date().getTime();
				 		$(".myPopUpContent").find('#myleafimg').attr('src',$ref+'/images/page3/leaf/photosynthesis2.gif?'+timestamp3);
				 		$('.myPopUpContent').find('img').load(function() {
				 		
				 			$("#leafImgcap2").html(data.string.p3_3_16).fadeIn(100);
				 		});

				 	},6000);
				 });
			}

		});
	});

}


$(function(){
	$("#head_title").html(data.string.p1_1);

	$("#p1_2").html(data.string.p1_2);
	

	var $contslide=1,$totalSlide=4;
	var $i;
	var countClicklife=0;

	

	

	$("#p1_2").delay(500).fadeIn(500).delay(3500).animate({'margin-top': '2%','font-size': '1em'}).delay(1500).fadeOut(100,function(){

		$(".topPage").fadeIn(500);
		showBtns($contslide,$totalSlide);

	});

	
	


	$("#pntPrev").click(function(){

		$contslide--;
		showBtns($contslide,$totalSlide);
	});


	$("#pntNext").click(function(){

		$contslide++;
		
		
		showBtns($contslide,$totalSlide);
	});


	$("#totalpage").on('click','.lifecycle',function(){

		countClicklife++;
		var $id= $(this).attr('id');
		var $dataVal2={
			p1_12:data.string.p1_12,
			p1_13:data.string.p1_13
		}
		var source2;  


		
		if($id=="rightOne")
		{	
			$dataVal2={
				p1_13:data.string.p1_13
			}

			source2   = $("#popup_5").html();
		}
		else
		{
			$dataVal2={
				p1_12:data.string.p1_12
			}
			source2   = $("#popup_6").html();
		}

		var template2 = Handlebars.compile(source2);

		var html2 = template2($dataVal2);

		
		$(".popUp").html(html2);
		$(".popUp").fadeIn(200,function(){
			if($id=="rightOne")
			{	
				cycleNonFlowering(); 
			}
			else 
			{ 
				cycleFlowering();
			}
		});

		if(countClicklife==2)
		{
			ole.nextBlinker();
		}
			
	});
	

	$(".popUp").on('click','#closePopBtn',function(){
	
		$(".popUp").html('');
		$(".popUp").fadeOut();
	});


});	

function loadData($slidenum)
{
	var $dataVal;
	switch($slidenum)
	{
		case 1:
				$dataVal={
					p1_3:data.string.p1_3,
					p1_4:data.string.p1_4,
					p1_1_1:data.string.p1_1_1,
					p1_1_2:data.string.p1_1_2,
					p1_1_3:data.string.p1_1_3,
					p1_1_4:data.string.p1_1_4,
					p1_1_5:data.string.p1_1_5,
					p1_1_6:data.string.p1_1_6,
					p1_1_7:data.string.p1_1_7,
					p1_1_8:data.string.p1_1_8,
					p1_1_9:data.string.p1_1_9,
					p1_1_10:data.string.p1_1_10,
					p1_1_11:data.string.p1_1_11,
					p1_1_12:data.string.p1_1_12
				}
				break;
		case 2:
				$dataVal={
					p1_5:data.string.p1_5,
					p1_6:data.string.p1_6,
					p1_7:data.string.p1_7,
					p1_8:data.string.p1_8,
					p1_8_1:	data.string.p1_8_1,
					p1_8_3:	data.string.p1_8_3,
					p1_8_2:	data.string.p1_8_2,
					p1_8_4:	data.string.p1_8_4						
				}
				break;
		case 3:
				$dataVal={
					p1_9:data.string.p1_9,
					p1_11:data.string.p1_11,
					p1_10:data.string.p1_10
				}
				break;
		case 4:
		$dataVal={
					p1_12:data.string.p1_12,
					p1_13:data.string.p1_13,
					p1_15:data.string.p1_15
				}
		break;

		case 5:
			$dataVal={
					p1_12:data.string.p1_12,
					p1_13:data.string.p1_13
				}
			break;
	}

	return $dataVal;
}

function showBtns($slidenum,$totalnum)
{
	
	if($slidenum==1)
	{	
		$("#pntPrev").hide(0);
		$("#pntNext").show(0);
	}


	if($slidenum==$totalnum)
		$("#pntNext").hide(0);

	if(($slidenum!=1) && ($slidenum!=$totalnum))
	{
		$("#pntPrev").show(0);
		$("#pntNext").show(0);
	}

	if($slidenum==2)
	{
		$("#pntNext").hide(0);
		$("#pntPrev").hide(0);
	}


	var $dataval=loadData($slidenum);

	var source   = $("#show-template_"+$slidenum).html();


	var template = Handlebars.compile(source);

	var html = template($dataval);

	$("#totalpage").fadeOut(10,function(){
		$(this).html(html)
	}).delay(10).fadeIn(100,function(){

		
		if($slidenum==2)
		{
			forTwo();

		}
		
		
	});

	
				


}
function reSetThis()
{
	$(".secondtxt,#quadrate,#qdrateBx,#qdrateBx2,#qdrateBx3").css({'display':'none'});
}
function forTwo()
{
	

	$("#p1_5").delay(500).fadeIn(500,function(){

		$("#quadrate").delay(500).fadeIn(500);

	}).delay(4500).fadeOut(500,function(){
		
		$("#p1_7").fadeIn(500,function(){
			$("#qdrateBx").delay(500).fadeIn(500);
	
		}).delay(4500).fadeOut(500,function(){

			$("#p1_6").fadeIn(500,function(){
				$("#qdrateBx").addClass('clickAble');
			});
		});
	});

	$("#totalpage").on('click','.clickAble',function(){

		$("#p1_6").fadeOut(10);
		
		$("#Extraid").fadeOut(100);
		$(this).fadeOut(100,function(){
			
			
					
			$("#qdrateBx2").fadeIn(500);

			$("#qdrateBx3").fadeIn(500);

			$("#conclusionTxt2").fadeIn(500);
			$("#conclusionTxt").fadeIn(500,function(){

				$("#pntNext").show(0);
				$("#pntPrev").show(0);
			});
			
			
		});
	});//clickable



}



function cycleNonFlowering()
{

	for(var $ii=2;$ii<=11;$ii++)
	{
		$(".text_"+$ii).html(data.string["p1_10_"+$ii]);
	}

	
	var $next=1;
	
	/******* Managing height of popup **********/
	var $whatHeightPopup=parseInt($("#first_1").outerHeight());
	var titleHgt=parseInt($("#pop_title").outerHeight());
	var $heightpop=$whatHeightPopup*1.05+titleHgt;
	$(".myPopUpContent").height($heightpop);

	ole.vCenter(".myPopUpContent",3);

	$("#mainText").html(data.string.p1_10_a_1);
	$("#mainText").delay(50).fadeIn(400);
	$(".text_1").delay(100).fadeIn(500);

	$("#first_1").delay(100).fadeIn(500,function(){

			$("#myNextBtn").delay(100).fadeIn(500);
	});

	$("#myNextBtn").click(function(){

		$(this).fadeOut(10);

		switch($next)
		{
			case 1:
			{
				$("#mainText").html(data.string.p1_10_a_2).addClass("firstMain");
				$(".text_1").fadeOut(10);
				$("#first_2").fadeIn(10).animate({'width':'100%','left':'0%','top':''},1000,function(){
					$(".text_2").fadeIn(10);
					$next++;
					$("#myNextBtn").fadeIn(100);
				});
				break;
			}
			case 2:
			{
				$("#mainText").html(data.string.p1_10_a_3).removeClass("firstMain");
				$(".text_2").fadeOut(200);
				$("#first_2").fadeOut(200);
				$("#first_1").fadeOut(200);

				$("#first_3").fadeIn(200).delay(600).fadeOut(200);
				$("#first_4").delay(800).fadeIn(200,function(){


					$(".text_3").fadeIn(200);
					$(".text_4").fadeIn(200);
					$("#myNextBtn").fadeIn(100);
					$next++;
				});
				
				break;
			}
			case 3:
			{
				$("#mainText").html(data.string.p1_10_a_4);
				$(".text_3").fadeOut(200);
				$(".text_4").fadeOut(200);
				$("#first_4").fadeOut(200);

				$("#first_5").fadeIn(200).delay(800).fadeOut(200);
				$("#first_6").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_7").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_8").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_9").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_10").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_11").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_12").delay(7000).fadeIn(200,function(){

					$(".text_5").fadeIn(200);
					$("#myNextBtn").fadeIn(100);
					$next++;

				});
				
				break;
			}
			case 4:
			{
				$("#mainText").html(data.string.p1_10_a_5);
				$("#first_12").fadeOut(200);
				$(".text_5").fadeOut(200);

				$("#first_13").fadeIn(200).delay(800).fadeOut(200);
				$("#first_14").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_15").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_16").delay(3000).fadeIn(200,function(){
					 var outH=parseInt($(".myBox").outerHeight());
					 
					
					$(".text_6").fadeIn(200);
					$(".text_7").fadeIn(200);
					$(".text_8").fadeIn(200);
					$(".text_9").fadeIn(200,function(){

						$("#myNextBtn").fadeIn(100);
						$next++;
					});
					
					
				});
				break;

			}
			case 5:
			{
				$("#mainText").html(data.string.p1_10_a_6);
				$("#first_16").fadeOut(200);
				$(".text_6").fadeOut(200);
				$(".text_7").fadeOut(200);
				$(".text_8").fadeOut(200);
				$(".text_9").fadeOut(200);

				$("#first_17").fadeIn(200).delay(800).fadeOut(200);
				$("#first_18").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_19").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#first_20").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				
				$("#first_21").delay(4000).fadeIn(200,function(){
					$(".text_10").fadeIn(200);
					
					$("#myNextBtn").fadeIn(100);
					$next++;
				});
				break;
			}
			case 6:
			{
				$("#mainText").html(data.string.p1_10_a_7);
				$("#first_21").animate({'width':'10%','left': '45%','top': '40%'},1200).fadeOut(100);
				$(".text_10").fadeOut(10);	
				

				$("#first_22").delay(200).fadeIn(200).delay(800).fadeOut(200);
				$("#first_23").delay(1200).fadeIn(200).delay(800).fadeOut(200);
				$("#first_24").delay(2200).fadeIn(200,function(){
						$(".text_11").fadeIn(10);
						$("#myNextBtn").fadeIn(100);
						$next++;	
				});
				break;	
			}
			case 7:
			{
				$("#mainText").html(data.string.p1_10_a_8);

				$("#first_24").animate({'width':'0%','left': '50%','top': '30%'},1200).fadeOut(100,function(){
					
					$("#closePopBtn").show(0);
				});
				$(".text_11").fadeOut(10);	
				$("#first_25").delay(600).fadeIn(100);
				break;
			}
			default:
			{
				
				
			}
		}
	});

	
	
	
}//function


/*
flowering plant l;ife cycle
**/
function cycleFlowering()
{
	for(var $ii=1;$ii<=15;$ii++)
	{
		$(".stxt_"+$ii).html(data.string["p1_14_"+$ii]);
	}
	var $next=1;
	
	var $whatHeightPopup=parseInt($("#second_1").outerHeight());
	var titleHgt=parseInt($("#pop_title").outerHeight());
	var $heightpop=$whatHeightPopup*1.05+titleHgt;
	$(".myPopUpContent").height($heightpop);

	ole.vCenter(".myPopUpContent",3);

	$("#mainText").html(data.string.p1_14_1_a);
	$("#mainText").delay(50).fadeIn(400);

	$("#second_1").delay(100).fadeIn(500).delay(300).fadeOut(500);
	$("#second_2").delay(800).fadeIn(500).delay(300).fadeOut(500);
	$("#second_3").delay(1600).fadeIn(500).delay(300).fadeOut(500);

	$("#second_3a").delay(2200).fadeIn(500,function(){
		$(".stxt_1").fadeIn(100);
			$("#myNextBtn").delay(100).fadeIn(500);


	});

	$("#myNextBtn").click(function(){

		$(this).fadeOut(10);

		switch($next)
		{
			case 1:
			{
				$("#mainText").html(data.string.p1_14_1_b).addClass("firstMain");
				$("#second_3a").fadeOut(10);
				$("#second_4").fadeIn(10,function(){
					$(".stxt_2").fadeIn(10);
					$(".stxt_3").fadeIn(10);
					$(".stxt_4").fadeIn(10);
					$next++;
					$("#myNextBtn").fadeIn(100);
				});
				break;
			}
			case 2:
			{
				$("#mainText").html(data.string.p1_14_1_c);
				$("#second_4").fadeOut(10);
				$("#second_5").fadeIn(10,function(){
					$(".stxt_2").fadeIn(10);
					$(".stxt_3").fadeIn(10);
					$(".stxt_4").fadeIn(10);
				}).delay(200).fadeOut(100);

				$("#second_6").delay(200).fadeIn(200).delay(800).fadeOut(200);

				$("#second_7").delay(1200).fadeIn(200).delay(800).fadeOut(200);

				$("#second_8").delay(2200).fadeIn(200,function(){
					$(".stxt_5").fadeIn(10);
					$(".stxt_6").fadeIn(10);
					$next++;
					$("#myNextBtn").fadeIn(100);
				});

				break;
			}
			case 3:
			{
				$("#mainText").html(data.string.p1_14_1_d);
				$("#second_8").fadeOut(200);
				$("#second_9").fadeIn(200).delay(800).fadeOut(200);
				$("#second_10").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_11").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_12").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_13").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_14").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_15").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_16").delay(7000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_17").delay(8000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_18").delay(9000).fadeIn(200,function(){
					
					$(".stxt_7").fadeIn(10);
					$(".stxt_8").fadeIn(10);
					$next++;
					$("#myNextBtn").fadeIn(100);
				});

				break;
			}
			case 4:
			{
				$("#mainText").html(data.string.p1_14_1_e);
				$("#second_18").fadeOut(200);
				$("#second_19").fadeIn(200).delay(800).fadeOut(200);
				$("#second_20").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_21").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_22").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_23").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_24").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_25").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_26").delay(7000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_27").delay(8000).fadeIn(200).delay(800).fadeOut(200);
			
				//$("#second_29").delay(9000).fadeIn(200).delay(800).fadeOut(200);
				
				var timestamp33=new Date().getTime();
					$("#second_30").find('img').attr('src','activity/grade7/science/sci_plant_class_01/images/page1/flower/30.gif?'+timestamp33);	
				 $("#second_30").delay(9000).fadeIn(200,function(){
					
					
														  
						$(".stxt_9").fadeIn(10);
						$next++;
						$("#myNextBtn").fadeIn(100);
					
				});
				break;
			}
			case 5:
			{
				$("#mainText").html(data.string.p1_14_1_f).removeClass("firstMain");
				$("#second_30").fadeOut(200);
				$("#second_31").fadeIn(200).delay(800).fadeOut(200);
				$("#second_32").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_33").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_34").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_35").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_36").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_37").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_38").delay(7000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_39").delay(8000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_40").delay(9000).fadeIn(200,function(){
					$next++;
					$("#myNextBtn").fadeIn(100);
				});
				break;
			}
			case 6:
			{
				$("#mainText").html(data.string.p1_14_1_g);
				$("#second_40").fadeOut(200);
				$("#second_41").fadeIn(200).delay(800).fadeOut(200);
				$("#second_42").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_43").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_44").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_45").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_46").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_47").delay(6000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_50").delay(7000).fadeIn(200,function(){
					$(".stxt_10").fadeIn(10);
					$(".stxt_11").fadeIn(10);
					$(".stxt_12").fadeIn(10);
					$(".stxt_13").fadeIn(10);

					$next++;
					$("#myNextBtn").fadeIn(100);
				});
				break;
			}
			case 7:
			{
				$("#mainText").html(data.string.p1_14_1_h);
				$("#second_50").fadeOut(200);
				$("#second_53").fadeIn(200).delay(800).fadeOut(200);
				$("#second_54").delay(1000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_55").delay(2000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_56").delay(3000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_57").delay(4000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_58").delay(5000).fadeIn(200).delay(800).fadeOut(200);
				$("#second_59").delay(6000).fadeIn(200,function(){

						$("#closePopBtn").show(0);

				});
			}

		}
	});

}
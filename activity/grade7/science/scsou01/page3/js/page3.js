// function for typewriter, for a id-passed as string, given some data, speed of typing-optional
// and a callback function -optional to call on complete
function typewriter(id, data, speed, oncomplete){
	var str = data,
    i = 0,
    text,
    typespeed,
    typevariable,
    strlen = str.length;

    // set the typespeed if provided set as it is or speed = 0 is default as 40, else give default speed 50
    if (typeof speed === "undefined" || speed === 0) {
			typespeed = 40;
		} else {
			typespeed = speed;
		}

	// function to type
	(function type() {
	    text = str.slice(0, ++i);

	    $("#"+id).html(text);

	    if (text === str) {
	    	/*if oncomplete function is provided call it and return
	    		else just return*/
	    	if (typeof oncomplete === "undefined") {
				clearInterval(typevariable);
			} else {
				oncomplete();
				clearInterval(typevariable);
			}
	    	return;
	    }

        typevariable = setTimeout(type, typespeed);
	}());
};

// show the table pop Up
function showMyTable()
{

	$("#popTable").delay(500).slideDown(500,function(){
		$("#popTable table,#tableRowAir").delay(500).fadeIn('1000', function() {
			$("#tableRowWater").delay(500).fadeIn('1000', function() {
				$("#tableRowIron").delay(500).fadeIn('1000', function() {
			$("#whySpeedDiffer").delay(500).fadeIn('500', function() {
					$("#closeTable").delay(500).show(0);
				$("#airReason").delay(500).fadeIn('1000', function() {
				$("#waterReason").delay(500).fadeIn('1000', function() {
				$("#ironReason").delay(500).fadeIn('1000', function() {

		});
		});
		});
		});
		});
		});
		});
	});
}

// show cat reaction
function catRxn(whichCat)
{
	$(whichCat).children('img:nth-of-type(1)').fadeOut(20,function(){
		$(whichCat).children('img:nth-of-type(2)').fadeIn(20,function(){
			$(whichCat).children('img:nth-of-type(2)').fadeOut(20, function() {
				$(whichCat).children('img:nth-of-type(3)').fadeIn(20, function() {
					$(whichCat).children('img:nth-of-type(3)').fadeOut(20, function() {
						$(whichCat).children('img:nth-of-type(4)').fadeIn(20, function() {
							if(whichCat == "#airCat"){
								showMyTable();
							}
						});
					});
				});
			});
		});
	});
}

function moveWhite()
{
	$("#airMove").show(0).animate({"left":"70%"},(whatDistance/velAir)*1000,"linear",
				function(){
					$("#airMove").hide(0).css({"left":"0%"});
					var numAir = whatDistance/velAir;
					var timeTakenAir = numAir.toFixed(2); //toFixed js method for showing to two decimal places
					$("#airTimeLapse").text(+timeTakenAir+"s");
					// update table for air row with the time taken
					$("#tableRowAir > td:nth-of-type(4)").text(timeTakenAir);

					catRxn("#airCat");
				});

	$("#waterMove").show(0).animate({"left":"70%"},(whatDistance/velWater)*1000,"linear",
				function(){
					$("#waterMove").hide(0).css({"left":"0%"});

					var numWater = whatDistance/velWater;
					var timeTakenWater = numWater.toFixed(2);
					$("#waterTimeLapse").text(+timeTakenWater+"s");
					// update table for water row with the time taken
					$("#tableRowWater > td:nth-of-type(4)").text(timeTakenWater);

					catRxn("#waterCat");
				});

	$("#ironMove").show(0).animate({"left":"70%"},(whatDistance/velIron)*1000,"linear",
				function(){
					$("#ironMove").hide(0).css({"left":"0%"});

					var numIron = whatDistance/velIron;
					var timeTakenIron = numIron.toFixed(2);
					$("#ironTimeLapse").text(+timeTakenIron+"s");
					// update table for iron row with the time taken
					$("#tableRowIron > td:nth-of-type(4)").text(timeTakenIron);
					catRxn("#ironCat");

				});
}



// global variable for the distance default be 1000
var whatDistance=1000;

//velocity of sound in different media
var velAir = 343;
var velWater = 1433;
var velIron = 5130;

//global variable to prevent simultaneous click on hammer use canIclick variable make it true at first
var canIclick = true;

//when to show the questionare
var showForm = 0;

$(document).ready(function() {
	loadTimelineProgress(1,1);

	// pull all data
	$("#topic").text(data.string.pg3s1);
	$("#airMedium > h1").text(data.string.pg3s2);
	$("#waterMedium > h1").text(data.string.pg3s3);
	$("#ironMedium > h1").text(data.string.pg3s4);
	$("#slideContent > h1").text(data.string.pg3s5);
	// $("#slideContent > em").text(data.string.pg3s26);
	$("#hammerBoard > h4").text(data.string.pg3s6);

	// for table
	$("#tableHead > th:nth-of-type(1)").text(data.string.pg3s18);
	$("#tableHead > th:nth-of-type(2)").text(data.string.pg3s19);
	$("#tableHead > th:nth-of-type(3)").text(data.string.pg3s20);
	$("#tableHead > th:nth-of-type(4)").text(data.string.pg3s21);

	$("#tableRowAir > td:nth-of-type(2)").text(data.string.pg3s2);
	$("#tableRowWater > td:nth-of-type(2)").text(data.string.pg3s3);
	$("#tableRowIron > td:nth-of-type(2)").text(data.string.pg3s4);

	// table reason
	$("#airReason > h2").text(data.string.pg3s2);
	$("#airReason > p").text(data.string.pg3s7);
	$("#airReason > h4").text(data.string.pg3s22);

	$("#waterReason > h2").text(data.string.pg3s3);
	$("#waterReason > p").text(data.string.pg3s8);
	$("#waterReason > h4").text(data.string.pg3s23);

	$("#ironReason > h2").text(data.string.pg3s4);
	$("#ironReason > p").text(data.string.pg3s9);
	$("#ironReason > h4").text(data.string.pg3s24);

	$("#whySpeedDiffer").text(data.string.pg3s25);

	typewriter("slideContent > em",data.string.pg3s26);

	//slider function - jquery ui
	 $("#slideMe").slider({value:1000,min: 1000,max: 3000,step: 1000,
			slide: function( event, ui )
			{
				whatDistance = ui.value;

				$( "#distance" ).text(whatDistance);
				$("#hammerBoard > h4").show(0);

			}
		});

	 // for sound
	 var myHammerWood = new buzz.sound($ref+"/page3/sound/hammerWood.ogg");

	 $("#hammer").on('click', function(event) {
	 	ole.footerNotificationHandler.hideNotification();
	 	// firstHit=true;
	 	if(canIclick)
	 	{
	 		buzz.all().stop();
	 		myHammerWood.play();

	 		canIclick = false;
	 		$("#hammerBoard > h4").hide(0);
	 		setTimeout(function  () {
	 			$("#slideMe").slider("disable");
	 		},500)


	 	// change the distance value in table
			$("#tableRowAir > td:nth-of-type(3)").text(whatDistance);
			$("#tableRowWater > td:nth-of-type(3)").text(whatDistance);
			$("#tableRowIron > td:nth-of-type(3)").text(whatDistance);


	 	$("#hammer").transition({left:"0%",rotate:"0deg"},10).delay(100).transition({left:"-30%",rotate:"310deg"},10);

	 	$("#airMove, #waterMove, #ironMove").stop();
	 	$("#airMove").hide(0).css({"left":"0%"});
	 	$("#waterMove").hide(0).css({"left":"0%"});
	 	$("#ironMove").hide(0).css({"left":"0%"});
	 	moveWhite();
	    }
	 });

		 // close the popTable div on clicking the closeTable button
	$("#closeTable").on('click', function() {
		$("#popTable table,#tableRowAir,#tableRowWater,#tableRowIron").hide(0);
		$("#airReason,#waterReason,#ironReason").hide(0);
		$("#whySpeedDiffer").hide(0);
		$("#closeTable").hide(0);
		$("#popTable").hide(0);

		// make cat sleep
		$("#airCat > img:nth-of-type(4), #waterCat > img:nth-of-type(4),#ironCat > img:nth-of-type(4)").hide(0);
	 	$("#airCat > img:nth-of-type(1), #waterCat > img:nth-of-type(1),#ironCat > img:nth-of-type(1)").show(0);

	 	canIclick = true;
		$("#slideMe").slider("enable");
		ole.footerNotificationHandler.pageEndSetNotification();
		// ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg3n1);
	});

});

// regularly used dom elements
var $molecule = $(".molecule"),
 $particleCol = $(".col"),
 $allparticles = $particleCol.children('div'),
 $cool = $("#cool"),
 $heat = $("#heat"),
 $degreetext = $("#degreetext");

// variables to store click count
var clickcount = 0;

// decrease the particle speed
function changeSoundSpeedslow(){
	var count;
	$($molecule).removeClass('pendulum pendulumfast');
	for(count=1;count<=10;count++){
		$($particleCol).children("div:nth-of-type("+count+")").removeClass("molCol"+count)
	}

	for(count=1;count<=10;count++){
		$($particleCol).children("div:nth-of-type("+count+")").addClass("molCol"+count+"slow")
	}

	// change color css of all particles for blue
	$allparticles.css({
		"background-color":"rgba(64, 192, 191,0.5)",
		"border":"0.2em solid rgba(64, 192, 191,0.1)"
	});
	var setspeed = setTimeout(function(){
		$($molecule).addClass('pendulumslow pendulum');
	},500);
};

// increase the particle speed
function changeSoundSpeedfast(){
	var count;
	$($molecule).removeClass('pendulumslow pendulum');

	for(count=1;count<=10;count++){
		$($particleCol).children("div:nth-of-type("+count+")").removeClass("molCol"+count+"slow");
	}

	for(count=1;count<=10;count++){
		$($particleCol).children("div:nth-of-type("+count+")").addClass("molCol"+count);
	}

	// change color css of all particles for red
	$allparticles.css({
		"background-color":"rgba(235, 55, 36,0.5)",
		"border":"0.2em solid rgba(235, 55, 36,0.1)"
	});

	var setspeed = setTimeout(function(){
		$($molecule).addClass('pendulumfast pendulum');
	},500);
};

// function for typewriter, for a id-passed as string, given some data, speed of typing-optional
// and a callback function -optional to call on complete
function typewriter(id, data, speed, oncomplete){
	var str = data,
    i = 0,
    text,
    typespeed,
    typevariable,
    strlen = str.length;

    // set the typespeed if provided set as it is or speed = 0 is default as 40, else give default speed 50
    if (typeof speed === "undefined" || speed === 0) {
			typespeed = 40;
		} else {
			typespeed = speed;
		}

	// function to type
	(function type() {
	    text = str.slice(0, ++i);

	    $("#"+id).html(text);

	    if (text === str) {
	    	/*if oncomplete function is provided call it and return
	    		else just return*/
	    	if (typeof oncomplete === "undefined") {
				clearInterval(typevariable);
			} else {
				oncomplete();
				clearInterval(typevariable);
			}
	    	return;
	    }

        typevariable = setTimeout(type, typespeed);
	}());
};

// display images
function displayimages(){
	$("#medium > figure:nth-of-type(1)").fadeIn(400, function() {
		$("#medium > figure:nth-of-type(2)").fadeIn(400, function() {

		});
	});
};

// display images
function nextslide(){
	$("#activity-page-next-btn-enabled").show(0);
};

function nexttempstatement(){
	typewriter("temptext2",data.string.pg5s8,0,thirdtempstatement);
}

function thirdtempstatement(){
	typewriter("temptext3",data.string.pg5s9,0,showairmolecule);
}

function showairmolecule(){
	$("#airmolecules").show(0);
}

function pageComplete(){
	ole.footerNotificationHandler.lessonEndSetNotification();
}

$(document).ready(function() {

	loadTimelineProgress(2,1);

	// var $whatnextbtn=getSubpageMoveButton($lang,"next");
	// $("#activity-page-next-btn-enabled").html($whatnextbtn);


	// pull all data
	$("#medium > figure:nth-of-type(1) > figcaption > span:nth-of-type(1)").text(data.string.pg5s2);
	$("#medium > figure:nth-of-type(2) > figcaption > span:nth-of-type(1)").text(data.string.pg5s3);
	$cool.text(data.string.pg5s13);
	$heat.text(data.string.pg5s14);
	$degreetext.text(data.string.pg5s11);

	// first function to be called on its own
	(function firstcall() {
	    typewriter("spacequestion",data.string.pg5s1,0,displayimages);
	}());

	// onclick handler for medium figure's
	$("#medium").on('click', 'figure', function() {
			 	var selected = $(this);
			 	var child = selected.children('figcaption');
			 	var grandchildren = child.children('span:nth-of-type(2)');
			 	grandchildren.show(0);
			 	switch(grandchildren.attr("class")){
			 		case "glyphicon glyphicon-ok":
			 			$("#result").text(data.string.pg5s4).css('color', 'darkgreen');
			 			break;
			 		case "glyphicon glyphicon-remove":
			 			$("#result").text(data.string.pg5s5).css('color', 'darkred');
			 			break;
			 		default:break;
			 	}
			 	$("#result").show(300,function(){
			 		typewriter("explanation",data.string.pg5s6,0,nextslide);
			 	});
			 	$("#medium").off("click");
			 	$("#medium").children('figure').css('cursor', 'default');
	});

	// next button click handler
	$("#activity-page-next-btn-enabled").on('click', function(e) {
    $("#activity-page-next-btn-enabled").hide(0);
		loadTimelineProgress(2,2);
		$("#space").css("left","-100%");
		$("#temperature").css("left","0%");
		$("#activity-page-next-btn-enabled").off("click");
		setTimeout(function(){
			typewriter("temptext",data.string.pg5s7,0,nexttempstatement);
		},1000);
	});

	$cool.on('click', function() {
		$(this).css("display","none");
		$heat.show(0);
		changeSoundSpeedslow();
		$degreetext.text(data.string.pg5s10);
		clickcount++;
	});

	$heat.on('click', function() {
		$(this).css("display","none");
		$cool.show(0);
		changeSoundSpeedfast();
		$degreetext.text(data.string.pg5s11);
		clickcount++;
		if(clickcount == 2){
			typewriter("tempexplain",data.string.pg5s12,100,pageComplete);
		}
	});
	// write first sentence in temperature div
	// typewriter("temptext",data.string.pg5s7,0,nexttempstatement);
});

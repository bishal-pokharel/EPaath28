// function for typewriter, for a id-passed as string, given some data, speed of typing-optional
// and a callback function -optional to call on complete
function typewriter(id, data, speed, oncomplete){
	var str = data,
    i = 0,
    text,
    typespeed,
    typevariable,
    strlen = str.length;

    // set the typespeed if provided set as it is or speed = 0 is default as 40, else give default speed 50
    if (typeof speed === "undefined" || speed === 0) {
			typespeed = 40;
		} else {
			typespeed = speed;
		}

	// function to type
	(function type() {
	    text = str.slice(0, ++i);

	    $("#"+id).html(text);

	    if (text === str) {
	    	/*if oncomplete function is provided call it and return
	    		else just return*/
	    	if (typeof oncomplete === "undefined") {
				clearInterval(typevariable);
			} else {
				oncomplete();
				clearInterval(typevariable);
			}
	    	return;
	    }

        typevariable = setTimeout(type, typespeed);
	}());
};

// function to show replay button
function showreplaybutton(){
	/*show page end notification also*/
	ole.footerNotificationHandler.pageEndSetNotification();
};
// function for cat animation
function animMyCat()
{
	setTimeout(function(){
		$("#myCat > img:nth-of-type(1)").fadeOut(500);
		$("#myCat > img:nth-of-type(2)").delay(150).fadeIn(500,function(){
			$("#instrument, #myCat").delay(2000).fadeOut(1800);
			$("#wave").delay(2000).animate({"top":"10%"},2000,function(){
				typewriter("lastInfo", data.string.pg1s4, 80, showreplaybutton);
			});
		});
	},1000);
}

// shows the circle sound effect pass parameters top and left
function circleEffect(l1,l2,id)
{
	var count = 0;
	$("#round1").css({"left":l1});
	$("#round2").css({"left":l2});
	$("#round1").transition({scale:5,opacity:1},500).transition({scale:1,opacity:0},0);
	$("#round2").delay(200).transition({scale:5,opacity:1},500).transition({scale:1,opacity:0},0);

	var myEffect = setInterval(function(){

	$("#round1").transition({scale:5,opacity:1},500).transition({scale:1,opacity:0},0);
	$("#round2").delay(200).transition({scale:5,opacity:1},500).transition({scale:1,opacity:0},0,
		function(){ /*on completion of animating second div do this function*/
			count++;
			if(count == 2)
			{
				clearInterval(myEffect);
				// for doing css animation again and again on every click removeClass
				$(id).removeClass('animated shake');
				$(id).removeClass('animated pulse');
				$(id).removeClass('animated wobble');
				$("#imageSelect").delay(1200).slideUp(800);
				$("#query").delay(1200).slideDown(800);
			}
		});

	},800);
}

//global variable for the images to be clickable or not
var canIclick = true;

//if the particle movement has started
var particleMoved = false;

// common variable to choose for which sound to relate with the image in the particle animation

var thisSound="";

$(document).ready(function() {

	 var $wahtar=getSubpageMoveButton($lang,"next");
	 var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	 $("#activity-page-next-btn-enabled").html($whatnxtbtn);

	 var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	 $("#activity-page-prev-btn-enabled").html($whatprevbtn);

	 var quest=1;

	 getFunction(quest);

	 $("#activity-page-next-btn-enabled").click(function(){
			quest++;
			$(this).fadeOut(10);
			$("#activity-page-prev-btn-enabled").fadeOut(10);
			getFunction(quest);
	 });

	 $("#activity-page-prev-btn-enabled").click(function(){
			quest--;
			$(this).fadeOut(10);
			$("#activity-page-next-btn-enabled").fadeOut(10);
			getFunction(quest);
 		});

		function getFunction(qno){
			 loadTimelineProgress(3,qno);
			 switch(qno)
			 {
					 case 1:
						 case1();
						 break;
					 case 2:
						 case2();
						 break;
					 case 3:
						 case3();
						 break;
			 }
		 }
	// pull required data at first
	function case1(){

		  var datavar={
	  		easyTitle: data.lesson.chapter,
	  		coverimg:$ref+"/page1/image/sound.png"
	  	}

	  	var source   = $("#front-template").html();

	  	var template = Handlebars.compile(source);

	  	var html=template(datavar);
	  	$("#firstpage").html(html);
	  	$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
	}
	
	function case2(){
		$("#firstpage").hide(0);
		$("#imageSelect > p:nth-of-type(1)").text(data.string.pg1s1);
		$("#query > span").text(data.string.pg1s2);
		/*$("#activity-page-next-btn-enabled").text(data.string.pg1s3);*/

		var mySoundDrum = new buzz.sound($ref+"/page1/sound/tabla.ogg");
		var mySoundShell = new buzz.sound($ref+"/page1/sound/conch.ogg");
		var mySoundGuitar = new buzz.sound($ref+"/page1/sound/guitar.ogg");
		var mySoundBell = new buzz.sound($ref+"/page1/sound/bell.ogg");


		$("#imageSelect > img:nth-of-type(1)").on('click', function() {
				if(canIclick)
				{
					thisSound = mySoundDrum;
					var sourceImg = $(this).attr("src");
					$("#wave > img").attr("src",sourceImg);
					$("#imageSelect > img:nth-of-type(1)").addClass('animated shake');
					canIclick = false;
					buzz.all().stop();
					mySoundDrum.play();
					$("#round1, #round2").show(0);
					circleEffect("12%","12.9%","#imageSelect > img:nth-of-type(1)");
					setTimeout(function(){
						$('#activity-page-next-btn-enabled').show(0);
					},4000);
				}
			});
			$("#imageSelect > img:nth-of-type(2)").on('click', function() {
			if(canIclick){
					thisSound = mySoundShell;
					var sourceImg = $(this).attr("src");
					$("#wave > img").attr("src",sourceImg);
				$("#imageSelect > img:nth-of-type(2)").addClass('animated pulse');
				canIclick = false;
				buzz.all().stop();
				mySoundShell.play();
				$("#round1, #round2").show(0);
				circleEffect("38%","39%","#imageSelect > img:nth-of-type(2)");
				setTimeout(function(){
					$('#activity-page-next-btn-enabled').show(0);
				},4000);
				}
			});

			$("#imageSelect > img:nth-of-type(3)").on('click', function() {
			if(canIclick){
				thisSound = mySoundGuitar;
				  var sourceImg = $(this).attr("src");
				  $("#wave > img").attr("src",sourceImg);
				$("#imageSelect > img:nth-of-type(3)").addClass('animated wobble');
				canIclick = false;
				buzz.all().stop();
				mySoundGuitar.play();
				$("#round1, #round2").show(0);
				circleEffect("64%","65%","#imageSelect > img:nth-of-type(3)");
				setTimeout(function(){
					$('#activity-page-next-btn-enabled').show(0);
				},4000);
				}
			});

			$("#imageSelect > img:nth-of-type(4)").on('click', function() {
			if(canIclick){
				thisSound = mySoundBell;
				var sourceImg = $(this).attr("src");
				$("#wave > img").attr("src",sourceImg);
				$("#imageSelect > img:nth-of-type(4)").addClass('animated shake');
				canIclick = false;
				buzz.all().stop();
				mySoundBell.play();
				$("#round1, #round2").show(0); //show them before animating
				circleEffect("86%","87%","#imageSelect > img:nth-of-type(4)");
				setTimeout(function(){
					$('#activity-page-next-btn-enabled').show(0);
				},4000);
				}
			});
	}

	function case3(){
		$("#activity-page-next-btn-enabled").hide(0);
		$("#query").hide(0);
		$("#wave").fadeIn(500, function(){
				buzz.all().stop();
				thisSound.play();
				animMyCat();
		});
	}




});

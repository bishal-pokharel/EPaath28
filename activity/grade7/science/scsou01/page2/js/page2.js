
function materialRequired()
{

	$("#activity-page-prev-btn-enabled,#activity-page-next-btn-enabled").css('display','none');
	$("#materialNeed > ul > li:nth-of-type(3) > img ").removeClass('animated tada'); // this is removed here for adding the class at last
	$("#materialNeed > h3").slideDown('5000', function() {
		$("#materialNeed > ul > li:nth-of-type(1) ").delay(1000).show('5000', function() {
			$("#materialNeed > ul > li:nth-of-type(1) > img ").addClass('animated tada');
			$("#materialNeed > ul > li:nth-of-type(2) ").delay(1000).show('5000', function() {
				 	$("#materialNeed > ul > li:nth-of-type(1) > img ").removeClass('animated tada');
				 	$("#materialNeed > ul > li:nth-of-type(2) > img ").addClass('animated tada');
				$("#materialNeed > ul > li:nth-of-type(3) ").delay(1000).show('5000', function() {
					$("#materialNeed > ul > li:nth-of-type(2) > img ").removeClass('animated tada');
					$("#materialNeed > ul > li:nth-of-type(3) > img ").addClass('animated tada');
					$("#activity-page-next-btn-enabled").show(0);
	});
	});
	});
	});
}

function howToPrepare()
{	$("#activity-page-prev-btn-enabled, #activity-page-next-btn-enabled").css('display', 'none');
	$("#prepMethod > h3").slideDown('5000', function() {
		$("#prepMethod > ul > li:nth-of-type(1) ").delay(1000).show('5000', function() {
			$("#prepMethod > ul > li:nth-of-type(1) > img ").addClass('animated tada');
			$("#prepMethod > ul > li:nth-of-type(2) ").delay(1000).show('5000', function() {
				 	$("#prepMethod > ul > li:nth-of-type(1) > img ").removeClass('animated tada');
				 	$("#prepMethod > ul > li:nth-of-type(2) > img ").addClass('animated tada');
				$("#prepMethod > ul > li:nth-of-type(3) ").delay(1000).show('5000', function() {
					$("#prepMethod > ul > li:nth-of-type(2) > img ").removeClass('animated tada');
					$("#prepMethod > ul > li:nth-of-type(3) > img ").addClass('animated tada');
						$("#prepMethod > ul > li:nth-of-type(3) ").delay(1000).show('5000', function() {
							$("#prepMethod > ul > li:nth-of-type(3) > img ").removeClass('animated tada');
								$("#prepMethod > ul > li:nth-of-type(4) ").delay(500).show('5000', function() {
										$("#prepMethod > ul > li:nth-of-type(5) ").delay(500).show('5000', function() {
							$("#activity-page-prev-btn-enabled, #activity-page-next-btn-enabled").show(0);
	});
	});
	});

	});
	});
	});
	});
}

function experiment()
{
	$("#kids").attr('src', $ref+"/page2/image/girltalk.png");
	$("#Italk").on('click', function() {
		$("#teleConclusion1, #teleConclusion2, #teleConclusion3").hide(0);
		$("#Italk").hide(0);
		buzz.all().stop();
	 	girltalk.play();

		function boytalkfunction(){
			checkboyTalkEnd = setInterval(function(){
		 		if ( boytalk.isEnded()) {
		 			buzz.all().stop();
	    		clearInterval(checkboyTalkEnd);
	    		$("#teleConclusion1").show(1000,function(){
	    			$("#teleConclusion2").delay(2000).show(1000,function(){
	    				$("#teleConclusion2 > button:nth-of-type(1)").delay(2000).show(100,function(){
	    					$("#teleConclusion2 > button:nth-of-type(1)").on('click', function() {
	    						$("#teleConclusion1, #teleConclusion2, #teleConclusion2 > button:nth-of-type(1)").hide(0);
	    						$("#teleConclusion3").slideDown(1000, function() {
	    							ole.footerNotificationHandler.pageEndSetNotification();
	    						});
	    					});
	    				});
	    			});
	    		});
	    		return;
				}
		 	},1);
		}

	 	checkgirlTalkEnd = setInterval(function(){
	 		if ( girltalk.isEnded()) {
	 			buzz.all().stop();
    			clearInterval(checkgirlTalkEnd);
    			$("#kids").attr('src', $ref+"/page2/image/boytalk.png");
    			boytalk.play();
    		    boytalkfunction();
			}
	 	},1);

	});
}

// global variable for setInterval in experiment function
var myTalkInterval;
var myListenInterval;
var checkgirlTalkEnd;
var checkboyTalkEnd;

// for sound global variable since it is called in different function
	 var girltalk = new buzz.sound($ref+"/page2/sound/girltalk.ogg");
	 var boytalk = new buzz.sound($ref+"/page2/sound/boytalk.ogg");
//for displaying the divs in pages
var pageCount = 1;

$(document).ready(function() {


	loadTimelineProgress(3,1);
	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	// pull all data
	$("#SoundLab2topic").text(data.string.pg2s1);
	$("#materialNeed > h3").text(data.string.pg2s2);
	$("#materialNeed > ul > li:nth-of-type(1) > p").text(data.string.pg2s3);
	$("#materialNeed > ul > li:nth-of-type(2) > p").text(data.string.pg2s4);
	$("#materialNeed > ul > li:nth-of-type(3) > p").text(data.string.pg2s5);
	$("#prepMethod > h3").text(data.string.pg2s6);
	$("#prepMethod > ul > li:nth-of-type(1) > p").text(data.string.pg2s7);
	$("#prepMethod > ul > li:nth-of-type(2) > p").text(data.string.pg2s8);
	$("#prepMethod > ul > li:nth-of-type(3) > p").text(data.string.pg2s9);
	$("#prepMethod > ul > li:nth-of-type(4) > p").text(data.string.pg2s10);
	$("#prepMethod > ul > li:nth-of-type(5) > p").text(data.string.pg2s11);
	$("#Italk").text(data.string.pg2s12);
	$("#teleConclusion1 > h3").text(data.string.pg2s13);
	$("#teleConclusion1 > p:nth-of-type(1)").text(data.string.pg2s14);
	$("#teleConclusion2 > h3").text(data.string.pg2s15);
	$("#teleConclusion2 > p:nth-of-type(1)").text(data.string.pg2s16);
	$("#teleConclusion2 > button:nth-of-type(1)").text(data.string.pg2s17);
	$("#teleConclusion3 > h5").text(data.string.pg2s17);
	$("#teleConclusion3 > p:nth-of-type(1)").text(data.string.pg2s18);



	//hide previous and next button for first time
	$("#activity-page-prev-btn-enabled, #activity-page-next-btn-enabled").css('display', 'none');
	 //play first page animation at first
	materialRequired();



	$("#activity-page-prev-btn-enabled").on('click', function() {
		 $("#materialNeed").clearQueue();
		 $("#prepMethod").clearQueue();
		 	clearInterval(checkboyTalkEnd);
    		clearInterval(checkgirlTalkEnd);
    		buzz.all().stop();
		pageCount--;

		loadTimelineProgress(3,pageCount);
			if(pageCount==1)
		 {
		 	/*console.log(pageCount);*/
		 	$("#activity-page-prev-btn-enabled").css('display', 'none');
		 	$("#activity-page-next-btn-enabled").show(0);
		 	$("#prepMethod").hide(0);
		 	$("#telephone").hide(0);
		 	$("#materialNeed").hide(0);
		 	$("#materialNeed > h3 , ul > li").hide(0);
			$("#materialNeed").show(0);
		 	materialRequired();

		 }

		else if(pageCount==2)
		{	/*console.log(pageCount);*/
			ole.footerNotificationHandler.hideNotification();
			$("#activity-page-prev-btn-enabled, #activity-page-next-btn-enabled").show(0);
			$("#materialNeed").hide(0);
		 	$("#telephone").hide(0);
		 	$("#prepMethod").hide(0);
		 	$("#prepMethod > h3 , ul > li").hide(0);
			$("#prepMethod").show(0);

		 	howToPrepare();

		}

		else
		{

			// console.log(pageCount);
			$("#activity-page-next-btn-enabled").css('display', 'none');
			$("#activity-page-prev-btn-enabled").show(0);
			$("#prepMethod").hide(0);
			$("#materialNeed").hide(0);
			$("#telephone").show(0);
			$("#Italk").show(0);

			experiment();

		}


	});



	$("#activity-page-next-btn-enabled").on('click', function() {
		 $("#materialNeed").clearQueue();
		 $("#prepMethod").clearQueue();
    		clearInterval(checkboyTalkEnd);
    		clearInterval(checkgirlTalkEnd);
    		buzz.all().stop();
			pageCount++;

		loadTimelineProgress(3,pageCount);
			if(pageCount==1)
		 {
		 	// console.log(pageCount);
		 	$("#activity-page-prev-btn-enabled").css('display', 'none');
		 	$("#activity-page-next-btn-enabled").show(0);
		 	$("#prepMethod").hide(0);
		 	$("#telephone").hide(0);
		 	$("#materialNeed").hide(0);
		 	$("#materialNeed > h3 , ul > li").hide(0);
			$("#materialNeed").show(0);
		 	materialRequired();

		 }

		else if(pageCount==2)
		{
			// console.log(pageCount);
			$("#activity-page-prev-btn-enabled, #activity-page-next-btn-enabled").show(0);
			$("#materialNeed").hide(0);
		 	$("#telephone").hide(0);
		 	$("#prepMethod").hide(0);
		 	$("#prepMethod > h3 , ul > li").hide(0);
			$("#prepMethod").show(0);


		 	howToPrepare();

		}

		else
		{
			console.log(pageCount);
			$("#activity-page-next-btn-enabled").css('display', 'none');
			$("#activity-page-prev-btn-enabled").show(0);
			$("#prepMethod").hide(0);
			$("#materialNeed").hide(0);
			$("#telephone").show(0);
			$("#Italk").show(0);
			$("#teleConclusion3").hide(0);
			experiment();

		}


	});
});

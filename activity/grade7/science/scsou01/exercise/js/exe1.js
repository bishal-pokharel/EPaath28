var counterMe=0;
$.fn.dropMe=function ($accept)
{
	var $this=$(this);

	$this.droppable({
        accept: $accept,
        drop: function( event, ui ) {
        	var id=ui.draggable.attr('id');
        	counterMe++;
        	var valme=$("#"+id).html();
        	$("#"+id).hide(0);

        	$this.append("<div class='secondSpan'>"+valme+"</div>")
        	if(counterMe==3 || counterMe==6)
        	{
				$("#img22").attr('src',$ref+'/exercise/images/correct.png');
				$("#img22").addClass('Ok');
				$("#explanation").addClass('expclass').show(0).addClass('animated bounceInDown');
			$("#activity-page-next-btn-enabled").fadeIn(10,function(){
				// $(this).addClass('animated shake');
			});


        	}
        }
        	
    });

};

$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);


	$("#toDo").html(data.string.e_1);


	var counter=0;
	var totalCounter=10;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);

	var newarray=shuffleArray(arrayquestion);
	//var newarray=arrayquestion;
	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;


	myquestion(newarray[counter],counter);

	$(".allquestion").on("click",".ans",function(){


		var isCorrect=parseInt($(this).attr('corr'));

		$("#showAns div").removeClass('ans');

		$("#showAns div[corr='1']").addClass('corrans');

		$("#explanation").show(0).addClass('animated bounceInDown');

		if(isCorrect==1)
		{
			$("#img22").attr('src',$ref+'/exercise/images/correct.png');
			$("#img22").addClass('Ok');

			rightcounter++;
		}
		else
		{
			$("#img22").attr('src',$ref+'/exercise/images/incorrect.png');
			$("#img22").addClass('notOk');

			wrongCounter++;
		}

		$("#activity-page-next-btn-enabled").fadeIn(10,function(){
			/*$(this).addClass('animated shake');*/
		});

	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}
		if(counter > totalCounter){
			ole.activityComplete.finishingcall();
		}
	});

});


function myquestion(questionNo,counter)
{
	loadTimelineProgress(11,(counter+1));
	var source   = $("#label-template").html();

	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter)
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){
		if(questionNo==6)
		{
			$(".draggableme").draggable({ revert: "invalid"});
			$("#l1").dropMe('#opt_6_2');
			$("#l2").dropMe('#opt_6_3');
			$("#l3").dropMe('#opt_6_1');

		}
		else if(questionNo==8)
		{
			$(".draggableme").draggable({ revert: "invalid"});
			$("#l4").dropMe('#opt_8_3');
			$("#l5").dropMe('#opt_8_1');
			$("#l6").dropMe('#opt_8_2');

		}
	});
}

function myquestion2(right,wrong)
{

	loadTimelineProgress(11,11);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var $dataval={rite:data.string.e_2,
		wrng:data.string.e_3,
		rnum:(right+2),
		wnum:wrong,
		allansw:[
			{ques:data.string.exe_1, qans: data.string.opt_1_3,quesno:"ques_1"},
			{ques:data.string.exe_2, qans: data.string.opt_2_2,quesno:"ques_2"},
			{ques:data.string.exe_3, qans: data.string.opt_3_2,quesno:"ques_3"},
			{ques:data.string.exe_4, qans: data.string.opt_4_1,quesno:"ques_4"},
			{ques:data.string.exe_5, qans: data.string.opt_5_2,quesno:"ques_5"},
			{ques:data.string.exe_6, qans: data.string.opt_6_ans,quesno:"ques_6"},
			{ques:data.string.exe_7, qans: data.string.opt_7_3,quesno:"ques_7"},
			{ques:data.string.exe_8, qans: data.string.opt_8_ans,quesno:"ques_8"},
			{ques:data.string.exe_9, qans: data.string.opt_9_3,quesno:"ques_9"},
			{ques:data.string.exe_10, qans: data.string.opt_10_1,quesno:"ques_10"}

		]


	}
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.e_4);

	}).delay(100).fadeIn(10,function(){

		// $("#replayBtn").addClass('animated bonce');
		$("#activity-page-next-btn-enabled").show(0);
	});
}

function getQuestion($quesNo,counter)
{
	var quesList;
	var nep=ole.nepaliNumber(counter+1);

	var whatri=whatCorr($quesNo);

	if($quesNo==2 || $quesNo==3|| $quesNo==10)
	{

		var imgss;

		if($quesNo==2)
			imgss=$ref+"/exercise/images/Pic-"+$quesNo+".gif"
		else
			imgss=$ref+"/exercise/images/Pic-"+$quesNo+".png"

		quesList={
			myQuesion:data.string["exe_"+$quesNo],
			img22:$ref+"/exercise/images/blank.png",
			imgques:imgss,
			optObj:[
					{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"],drags:"ans"},
					{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"],drags:"ans"}
					],
			drags:"ans",
			expla:data.string["exp_"+$quesNo]
		}
	}
	else if($quesNo==6)
	{
		quesList={
			myQuesion:data.string["exe_"+$quesNo],
			img22:$ref+"/exercise/images/blank.png",
			imgques:$ref+"/exercise/images/Pic-"+$quesNo+".png",
			optObj:[
					{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"],drags:"draggableme"},
					{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"],drags:"draggableme"},
					{optObj1:"opt_"+$quesNo+"_3", yesno:whatri[2],optObjval:data.string["opt_"+$quesNo+"_3"],drags:"draggableme"}
					],
			labObj:[
				{labid:"l1",labtxt:data.string.exe_lab_1},
				{labid:"l2",labtxt:data.string.exe_lab_2},
				{labid:"l3",labtxt:data.string.exe_lab_3}],
			expla:data.string["exp_"+$quesNo]

		}
	}
	else if($quesNo==8)
	{
		quesList={
			myQuesion:data.string["exe_"+$quesNo],
			img22:$ref+"/exercise/images/blank.png",
			imgques:$ref+"/exercise/images/Pic-"+$quesNo+".png",
			optObj:[
					{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"],drags:"draggableme"},
					{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"],drags:"draggableme"},
					{optObj1:"opt_"+$quesNo+"_3", yesno:whatri[2],optObjval:data.string["opt_"+$quesNo+"_3"],drags:"draggableme"}
					],
			labObj:[
				{labid:"l4",labtxt:data.string.exe_lab_4},
				{labid:"l5",labtxt:data.string.exe_lab_5},
				{labid:"l6",labtxt:data.string.exe_lab_6}],
			expla:data.string["exp_"+$quesNo]

		}
	}
	else
	{
		quesList={
			myQuesion:data.string["exe_"+$quesNo],
			img22:$ref+"/exercise/images/blank.png",
			imgques:$ref+"/exercise/images/Pic-"+$quesNo+".png",
			optObj:[
					{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"],drags:"ans"},
					{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"],drags:"ans"},
					{optObj1:"opt_"+$quesNo+"_3", yesno:whatri[2],optObjval:data.string["opt_"+$quesNo+"_3"],drags:"ans"}
					],
			expla:data.string["exp_"+$quesNo]
		}
	}

	return quesList;
}

function whatCorr($quesNo)
{
	var whatis=new Array();
	switch($quesNo)
	{
		case 1: 	whatis[0]=0;	whatis[1]=0; 	whatis[2]=1; break;
		case 2: 	whatis[0]=0;	whatis[1]=1; 	whatis[2]=0; break;
		case 3: 	whatis[0]=0;	whatis[1]=1; 	whatis[2]=0;  break;
		case 4: 	whatis[0]=1;	whatis[1]=0; 	whatis[2]=0; break;
		case 5: 	whatis[0]=0;	whatis[1]=1; 	whatis[2]=0;break;
		case 6: 	whatis[0]=0;	whatis[1]=1; 	whatis[2]=0; break;
		case 7: 	whatis[0]=0;	whatis[1]=0; 	whatis[2]=1; break;
		case 8: 	whatis[0]=1;	whatis[1]=0; 	whatis[2]=0; break;
		case 9: 	whatis[0]=0;	whatis[1]=0; 	whatis[2]=1; break;
		case 10: 	whatis[0]=1;	whatis[1]=0; 	whatis[2]=0; break;


	}
	return whatis;
}

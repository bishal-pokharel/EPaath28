//form action
function isCorrect()
{	
	
	// console.log($("input:checked").val() + "is checked")	;
	
	if($("input:checked").val() == "ansAir")
	{
		$("input:checked").next().css({"color":"red","text-decoration":"line-through"});
		$("#isCorrectInfo").css('color', 'red').text(data.string.pg3s16).show(0); //incorrect
	
	}

	else if($("input:checked").val() == "ansWater")
	{
		$("input:checked").next().css({"color":"red","text-decoration":"line-through"});
		$("#isCorrectInfo").css('color', 'red').text(data.string.pg3s16).show(0); //incorrect
		
	}	

	else if($("input:checked").val() == "ansIron")
	{
		$("input:checked").next().css({"color":"green"});
		$("#isCorrectInfo").css('color', 'green').text(data.string.pg3s15).show(0); //incorrect
	
		$("#concludingStatement").fadeIn(500).delay(510).hide(0).fadeIn(500).delay(1010).hide(0).fadeIn(500,function(){
			$("#question1").delay(1000).fadeIn(1000, function() {
				$("#infoHere").fadeOut(1000, function() {
					$("#question1").animate({"top": "0%"},
						3000, function() {
						firstThunder();
					});
				});
			});
		});


	}	
} 

function firstThunder()
{
	//thunder lightning animation
		$("#weather, #cloud").delay(3000).fadeIn(1000, function() {
			$("#lightning, #cloudColor").fadeIn(100, function() {
				$("#lightning, #cloudColor").fadeOut(100, function() {
				    $("#lightning, #cloudColor").fadeIn(100, function() {
				        $("#lightning, #cloudColor").fadeOut(100, function() {
				            $("#lightning, #cloudColor").fadeIn(100, function() {
				        		$("#lightning, #cloudColor").fadeOut(100, function() {
				           			$("#discuss").show(100);
				           					buzz.all().stop();
	 										myThunder.play();
	 									$("#reLight").delay(5000).fadeIn(0,function(){
	 										ole.footerNotificationHandler.pageEndSetNotification();
	 									});
			});	
			});
			});
			});
			});
			});
		});

}

function playAgain()
{
	
		$("#lightning, #cloudColor").fadeIn(100, function() {
				$("#lightning, #cloudColor").fadeOut(100, function() {
				    $("#lightning, #cloudColor").fadeIn(100, function() {
				        $("#lightning, #cloudColor").fadeOut(100, function() {
				            $("#lightning, #cloudColor").fadeIn(100, function() {
				        		$("#lightning, #cloudColor").fadeOut(100, function() {
				           	    			
				           			buzz.all().stop();
	 									myThunder.play();
	 									
			});	
			});
			});
			});
			});
			});
}

// sound of thunder
	var myThunder = new buzz.sound($ref+"/page4/sound/thunder.ogg");


$(document).ready(function() {	

	loadTimelineProgress(1,1);

	// pull all data
	$("#question1").text(data.string.pg4s1);
	$("#discuss").text(data.string.pg4s2);
	$("#reLight").text(data.string.pg4s3);

	$("#infoHere > p").text(data.string.pg4s10);
	$("#firstOption").text(data.string.pg4s11);
	$("#secondOption").text(data.string.pg4s12);
	$("#thirdOption").text(data.string.pg4s13);
	$("#checkAns").text(data.string.pg4s14);
	$("#concludingStatement").text(data.string.pg4s17);

	
	//firstThunder();
	
	// on pheri hera click
	$("#reLight").on('click', function() {
		$("#reLight").hide(0);
		playAgain();
	});


	 });
			
(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 3,
		$refImg = $ref+"/images/page7/",
		$refImg5 = $ref+"/images/page5/";
	loadTimelineProgress($total_page,countNext+1);

	var allDesc = [
		{
			title : data.string.p6_title,
			showDesc : data.string.p7_describe1
		},
		{
			title : data.string.p7_sphere1,
			showDesc : data.string.p7_describe2
		},
		{
			title : data.string.p7_sphere2,
			showDesc : data.string.p7_describe3
		},
		{
			title : data.string.p7_sphere3,
			showDesc : data.string.p7_describe4
		},
		{
			title : data.string.p7_sphere4,
			showDesc : data.string.p7_describe5
		},
		{
			title : data.string.p7_sphere5,
			showDesc : data.string.p7_describe6
		}
	]

/*
* template loading starts
*/
	function intro() {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			introText : data.string.p7_intro,
			img1 : $refImg+"circle01.png",
			introText2 : data.string.p7_intro2,

		};
		var html = template(content);
		$board.html(html);
	}
	intro();

	function changePic () {
		$board.find('.imgHolder img').attr('src',$refImg+"circle02.png");
		$board.find('.introText2').text(data.string.p7_introDesc);
	}

	function atmosphere() {
		var source = $("#atmosphere-template").html();
		var template = Handlebars.compile(source);
		var content = {
			// atmosphereImg : $refImg+"atmosphere.png",
			// rocket : $refImg+"rocket.png",
			// meteoroid : $refImg+"meteoroid.png",
			// aurora : $refImg+"aurora.png",
			// everest : $refImg+"everest.png",
			exosphere : data.string.p7_sphere1,
			thermosphere : data.string.p7_sphere2,
			mesosphere : data.string.p7_sphere3,
			stratosphere : data.string.p7_sphere4,
			troposphere : data.string.p7_sphere5,
		};
		var html = template(content);
		$board.html(html);

		descLoader(allDesc[0])
		$nextBtn.show(0);
	}

	function descLoader(desc) {
		var source = $("#descLoader-template").html();
		var template = Handlebars.compile(source);
		var content = {
			title : desc.title,
			showDesc : desc.showDesc
		};
		var html = template(content);
		$board.find(".desc").html(html);
	}

	$board.on('mouseenter','.layers',function() {
		var index = $(this).index();
		descLoader(allDesc[index+1]);
	});

	$nextBtn.on('click',function () {
		countNext++;
		thenext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		if (countNext%2===0) {
			countNext--;
		} else {
			countNext= countNext-2;
		}

		if (countNext<0) {
			countNext = 0;
		};

		newPlace = true;
		thenext();
	});

	function thenext () {
		// console.log(countNext);
		switch (countNext) {

			case 0:
				intro();
			break;

			case 1:
				changePic();
			break;

			case 2:
				atmosphere();
			break;

			default:
				// intro();
			break;
		};

		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
			// $prevBtn.show(0);
			$nextBtn.hide(0);
		} else if (countNext===0) {
			// $nextBtn.show(0);
			$prevBtn.hide(0);
		} else {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		}
	}

})(jQuery);
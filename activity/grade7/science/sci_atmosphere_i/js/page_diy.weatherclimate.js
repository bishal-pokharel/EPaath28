(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 6,
		countQ = 0,
		$refImg = $ref+"/images/diy/";
	loadTimelineProgress($total_page,countNext+1);
		$nextBtn.show(0);
	var canvas1,canvas2,width, height;
	var column = 1000,rows1,rows2;
	var cloudy,earth;

	var $earth = $ref+"/images/earth.png";

	var contents = [{
		title : data.string.p_diy_qDrag,
		info : data.string.p_diy_desc1,
		question : data.string.p_diy_q1,
		clueImg : $refImg+"circles/circle01.png",
		num : 1,
	},{
		title : data.string.p_diy_qDrag,
		info : data.string.p_diy_desc2,
		question : data.string.p_diy_q2,
		clueImg : $refImg+"circles/circle02.png",
		num : 1,
	},{
		title : data.string.p_diy_qDrag,
		info : data.string.p_diy_desc3,
		question : data.string.p_diy_q3,
		clueImg : $refImg+"circles/circle03.png",
		num : 1,
	},{
		title : data.string.p_diy_qDrag,
		info : data.string.p_diy_desc4,
		question : data.string.p_diy_q4,
		clueImg : $refImg+"circles/circle04.png",
		num : 1,
	},{
		title : data.string.p_diy_qDrag,
		info : data.string.p_diy_desc5,
		question : data.string.p_diy_q5,
		clueImg : $refImg+"circles/circle05.png",
		num : 1,
	}];

	var options1 = [];
	for (var i = 5; i > 0; i--) {
		options1.push(data.string["p7_sphere"+i])
	};

	var options = [{
		img : $refImg+"img01.png",
		name : "aurora",
		cls : "layer4"
	},{
		img : $refImg+"options/cloud.png",
		name : "cloud",
		cls : "layer1"
	},{
		img : $refImg+"options/meteoroid.png",
		name : "meteoroid",
		cls : "layer3"
	},{
		img : $refImg+"options/plane.png",
		name : "jet plane",
		cls : "layer2"
	},{
		img : $refImg+"options/satellite.png",
		name : "satellite",
		cls : "layer5"
	},
	// {
	// 	img : $refImg+"options/",
	// 	name : "",
	// 	cls : "layer"
	// }
];

	/*{
		img : $refImg+"options/space_station.png",
		name : "space station",
		cls : "layer4"
	},*/
/*
* template loading starts
*/
	function diyLandingPage () {
		var source = $("#diyLandingPage-template").html();
		var template = Handlebars.compile(source);
		var content={
			diyImageSource : "images/diy/diy1.png",
			diyTextData : data.string.diy,
			diyInstructionData : data.string.p_diy_intro
		}
		var html = template(content);
		$board.html(html);
		// $nextBtn.show(0);
	}
	// diyLandingPage();

	function canvasloader () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var content = {
			imgMM : $refImg+"globe.png",
			diyImg : "images/diy/diy1.png",
			diyTextData : data.string.diy,
		};
		var html = template(content);
		$board.html(html);
		canvas1 = mVas('stillCanvas',"canvasHolder");
		canvas2 = mVas('animationCanvas',"canvasHolder");
		canvas1Drawing();
		canvas2Drawing();
		// $nextBtn.show(0);
		loadQuestions();

		// playAll();
	};

	canvasloader();

	//functions to load questions
	function loadQuestions () {
		// alert("m");
		var source = $("#questionsAll-template").html();
		var template = Handlebars.compile(source);
		var content= contents[countQ];
		content.options = options;
		var html = template(content);
		$board.find(".others").html(html);
		$nextBtn.hide(0);

		$board.find('.options .option').draggable({ revert: "invalid" });
		$board.find('.options').hide(0);
		loadNewQuestion ();
		// playAll();
	}

	function loadNewQuestion () {

		var source = $("#questions-template").html();
		var template = Handlebars.compile(source);
		var content= {
			title : contents[countQ].question
		};
		content.options = options1;
		// alert("m here");
		var html = template(content);
		$board.find(".others .questions").html(html);
	}

	function layer1 () {
		$board.find(".questions").hide(0);
		$board.find('.imgHolder img').attr('src',contents[countQ].clueImg);
		$board.find('.titleQ').text(contents[0].title);
		var num = countQ+1;
		var len = 0;
		$board.find('.imgHolder').addClass("sphere"+num);
		$board.find(".sphere"+num).droppable({
			accept : ".option.layer"+num,
			// tolerance : "fit",
			drop : function (event, ui) {

				len++;
				// alert("len "+len+"count "+contents[countQ].num)
				if (len>=contents[countQ].num) {
					$board.find('.imgHolder').removeClass("sphere"+num);
					loadInfo ()

				};

			}
		})
	}

	function loadInfo () {
		// $board.find(".qa .options").hide(0);
		$board.find(".qa .info").show(0);
		$(".ok").hide();
		// alert(countQ);

		countQ++;
		$nextBtn.show(0);
	};

	function summary () {
		console.log("countNext from summary"+countNext);
		$board.find(".qa").hide(0);
		$nextBtn.hide(0);

		// ole.footerNotificationHandler.pageEndSetNotification();

	};

	function canvas1Drawing () {
		canvas1.gridColumn(column);
		rows1 = canvas1.getGridRows();
		earth = mVas.images($earth);
		// cloudy = mVas.images($refImg+"globe.png");
		canvas1.add(earth,column/2,rows1/2,170,170);
		canvas1.draw(true);
	};

	function showEarth () {
		var cloud1 = mVas.images($refImg+"options/cloud.png",{width:15, height:45});
		canvas2.addAnimation(cloud1,{ type : "circle",
			radius : 70,
			startAngle: 0,
			endAngle : 360,
			speed : 4,
			objRotate : true,
			referencePoint : {x: column/2, y:rows2/2}, //refrencepoint is centre for circle
		loop: true});

		cloud2 = mVas.images($refImg+"options/cloud.png",{width:45, height:45});
		var newPaths = mVas.getPath.bezierCurve({x:-50,y:-50},{x:50,y:30},{x:-50,y:-20},{x:20,y:30},5);
		console.log("here i am");
		canvas2.addAnimation(cloud2,{ type : "path",
			path : newPaths,
			speed : 1,
			rotate : 1,
			objRotate : true,
			referencePoint : {x: column/2, y:rows2/2}, //refrencepoint is centre for circle
			loop: true});
		canvas2.startAnimation();
	}

	function canvas2Drawing () {
		// var column = 1000;
		canvas2.gridColumn(column);
		rows2 = canvas1.getGridRows();

		cloud = mVas.images($refImg+"cloud.png",{width:15, height:20});
		cloud2 = mVas.images($refImg+"cloud.png",{width:100, height:100});
	}

	function showStratosphere () {
		console.log("nth");
		plane = mVas.images($refImg+"plane.png",{width:15, height:45});
		canvas2.addAnimation(plane,{ type : "circle",
			radius : 100,
			startAngle: 0,
			endAngle : 360,
			speed : 2,
			objRotate : true,
			referencePoint : {x: column/2, y:rows2/2}, //refrencepoint is centre for circle
		loop: true});
		// canvas2.startAnimation();
	};

	function showMesosphere () {
		var meteoroid=[];
		var circle1 = mVas.getPath.circle(120,0,360);
		var circle2 = mVas.getPath.circle(500,0,360);
		var getpaths = [];
		var angle = [];
		var len = 10000;
		for (var i = 0; i < len ; i++) {
			meteoroid[i]= mVas.images($refImg+"meteoroid.png",{width:45, height:15});
			var rdm = Math.floor((Math.random() * 360));
			getpaths[i] = mVas.getPath.line(circle2[rdm].x,circle2[rdm].y,circle1[rdm].x,circle1[rdm].y,25);
			angle[i] = circle2[rdm].angle;
		};
		var c =0;
		canvas2.addAnimation(meteoroid[c],{ type : "path",
			path : getpaths[c],
			speed : 1,
			rotate : angle[c],
			objRotate : true,
			referencePoint : {x: column/2, y:rows2/2}, //refrencepoint is centre for circle
			loop: false});
		c++;
		var setIntV = setInterval(function () {
			canvas2.addAnimation(meteoroid[c],{ type : "path",
				path : getpaths[c],
				speed : 1,
				rotate : angle[c],
				objRotate : true,
			referencePoint : {x: column/2, y:rows2/2}, //refrencepoint is centre for circle
			loop: false});
			c++;
			if (c>len) {
				clearInterval(setIntV);
			};
		},5000);
		// var getpaths = mVas.getPath.line(500,-500,50,-150,25);
	};

	function showThermosphere () {
		// space_station = mVas.images($refImg+"space_station.png");
		aurora = mVas.images($refImg+"img01.png");
		canvas1.add(aurora,column/2,rows1/2-150,100,50);
		// canvas1.add(space_station,column/2-180,rows1/2,100,100);
		canvas1.draw(true);
	}

	function showExosphere () {
		setellite = mVas.images($refImg+"satellite.png",{width:50, height:75});
		canvas2.addAnimation(setellite,{ type : "circle",
			radius : 250,
			startAngle: 0,
			endAngle : 360,
			speed : 1,
			objRotate : true,
			referencePoint : {x: column/2, y:rows2/2}, //refrencepoint is centre for circle
			loop: true});
		setTimeout(launchRocket,3000);
		setTimeout(launchRocket,50000);
	};

	function launchRocket () {
		rocket = mVas.images($refImg+"rocketship.png",{width:50, height:75});
		var newPaths = mVas.getPath.bezierCurve({x:10,y:10},{x:500,y:100},{x:200,y:200},{x:500,y:300},50);
		console.log("here i am");
		canvas2.addAnimation(rocket,{ type : "path",
			path : newPaths,
			speed : 1,
			rotate : 2,
			objRotate : true,
			referencePoint : {x: column/2, y:rows2/2}, //refrencepoint is centre for circle
			loop: false});
	}

	$board.on('click','.questions .qoptions span',function () {
		var $this = $(this);
		var index = $this.index();
		if (index===countQ) {
			// alert('yes'+countQ+" "+index);
			layer1 ();
			$board.find(".options").show(0);
		} else {
			$this.addClass("wrong");
		}
	})

	/*click check */
	$board.on('click','.options span',function () {
		var $this = $(this);
		var index = $(this).index();
		if (index===countQ) {
			if (countQ<5) {
				loadInfo();
			} else {
				$board.find(".others .qa").hide(0);
			}

		} else {
			$this.find('.wrong').show(0);
		}
	})

	$nextBtn.on('click',function () {
		countNext++;
		thenext();
	});

	$board.on('click','.others .qa .info .ok',function () {
		countNext++;
		thenext();
	})

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		if (countNext%2===0) {
			countNext--;
		} else {
			countNext= countNext-2;
		}

		if (countNext<0) {
			countNext = 0;
		};

		newPlace = true;
		thenext();
	});

	function playAll () {
		showEarth();
		showStratosphere();
		showMesosphere();
		showThermosphere();
		showExosphere();
	}

	$(".endBtn").on('click',function () {
		ole.activityComplete.finishingcall();
	})

	function thenext () {
		console.log("countNext "+countNext);

		if (countQ>0 && countQ<6) {
			var c = countQ-1;
			switch (c) {
				case 0 :
				showEarth();
				break;

				case 1 :
				showStratosphere();
				break;

				case 2 :
				showMesosphere();
				break;

				case 3 :
				showThermosphere();
				break;

				case 4 :
				showExosphere();
                ole.activityComplete.finishingcall();
				break;
			}
		};


		switch (countNext) {

			case 0:
				intro();
			break;

			/*case 1:
				canvasloader();
			break;*/

			// case 1:
				// $board.find('.others').hide(0);
				// playAll();
			// break;

			case 5:
				summary();
			break;

			default:
				loadQuestions();
			break;
		};

		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			// ole.footerNotificationHandler.pageEndSetNotification();
			$nextBtn.hide(0);
		} else if (countNext===0) {
			$prevBtn.hide(0);
		} else {
		}
	}

})(jQuery);

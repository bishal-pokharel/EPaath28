(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 6,
		$refImg = $ref+"/images/page7/",
		$refImg5 = $ref+"/images/page5/";
	loadTimelineProgress($total_page,countNext+1);
		$nextBtn.show(0);
	var allDesc = [
		{
			title : data.lesson.chapter
		},
		{
			title : data.string.p6_title,
			showDesc : data.string.p7_describe1
		},
		{
			title : data.string.p7_sphere1,
			showDesc : data.string.p7_describe2
		},
		{
			title : data.string.p7_sphere2,
			showDesc : data.string.p7_describe3
		},
		{
			title : data.string.p7_sphere3,
			showDesc : data.string.p7_describe4
		},
		{
			title : data.string.p7_sphere4,
			showDesc : data.string.p7_describe5
		},
		{
			title : data.string.p7_sphere5,
			showDesc : data.string.p7_describe6
		}
	]

/*
* template loading starts
*/
	function intro() {
		var source = $("#introFirst-template").html();
		var template = Handlebars.compile(source);
		var content = {
			introText : data.string.p5_2,
			img1 : $ref+"/images/earth.png",
			descText : data.string.p5_desc_1,
			newclass : "bg",
		};
		var html = template(content);
		$board.html(html);
	};
	function first_page() {
		var source = $("#introFirst-template").html();
		var template = Handlebars.compile(source);
		var content = {
			introText : data.lesson.chapter,
			img1 : $ref+"/images/atmosphere.png",
			newclass : "bg_full",
		};
		var html = template(content);
		$board.html(html);
	};

	first_page();

	function intro2() {
		var source = $("#introFirst-template").html();
		var template = Handlebars.compile(source);
		var content = {
			introText : data.string.p5_2,
			img1 : $refImg5+"atmospherer.png",
			descText : data.string.p5_desc_1,
			newclass : "atmosphereShow",
			descText2 : data.string.p6_defination,
		};
		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	};

// from Atmosphere2
	function introAtmosphere2() {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			introText : data.string.p7_intro,
			img1 : $refImg5+"atmosphere_layer.png",
			introText2 : "",
			clickToGo : data.string.clickToGo

		};
		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	}
	function changePic () {
		$board.find('.imgHolder img').attr('src',$refImg5+"atmosphere_layer_crossSection.png");
		$board.find('.imgHolder .clickPart').show(0);
		$board.find('.introText2').text(data.string.p7_introDesc).show(0);
		$nextBtn.hide(0);
	}

	$board.on('click','.imgHolder .clickPart',function () {
		countNext++;
		thenext();
	})

	function atmosphere() {
		var source = $("#atmosphere-template").html();
		var template = Handlebars.compile(source);
		var content = {
			exosphere : data.string.p7_sphere1,
			thermosphere : data.string.p7_sphere2,
			mesosphere : data.string.p7_sphere3,
			stratosphere : data.string.p7_sphere4,
			troposphere : data.string.p7_sphere5,
		};
		var html = template(content);
		$board.html(html);

		descLoader(allDesc[0])
		$nextBtn.show(0);
	}

	function descLoader(desc) {
		var source = $("#descLoader-template").html();
		var template = Handlebars.compile(source);
		var content = {
			title : desc.title,
			showDesc : desc.showDesc
		};
		var html = template(content);
		$board.find(".desc").html(html);
	}

	var checker = [null,null,null,null,null];

	$board.on('mouseenter','.layers',function() {
		var index = $(this).index();
		descLoader(allDesc[index+2]);
		var rambo = true; //just a name :D
		// var index = $this.index();
		checker[index]=1;
		for (var i = 0; i < checker.length; i++) {
			if (!checker[i]) {
				rambo=false;
				break;
			};
		};

		if (rambo) {
			// ole.footerNotificationHandler.pageEndSetNotification();
			ole.footerNotificationHandler.lessonEndSetNotification();
		};
	});

	$nextBtn.on('click',function () {
		countNext++;
		thenext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			countNext--;
		if (countNext<0) {
			countNext = 0;
		};

		newPlace = true;
		thenext();
	});

	function thenext () {
		console.log(countNext);
		switch (countNext) {

			case 1:
				intro();
			break;

			case 2:
				intro2();
				// $board.find("img").attr('src',$refImg5+"atmospherer.png");
				// $board.find('.atmosphereFirstWrapper .descText2').show(0);
			break;


			case 3:
				introAtmosphere2();
			break;

			case 4:
				changePic();
			break;

			case 5:
				atmosphere();
			break;

			default:
				// intro();
			break;
		};

		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			// ole.footerNotificationHandler.pageEndSetNotification();
			$prevBtn.show(0);
			$nextBtn.hide(0);
		} else if (countNext===0) {
			// $nextBtn.show(0);
			$prevBtn.hide(0);
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

})(jQuery);

// tissue to heart
function tissueToHeart(){
	$("#tissue").css({opacity:"0"});
	$("#heart > p:nth-of-type(1)").text(data.string.pg3s2).removeClass("flip").addClass('tada');
	$("#heart > p:nth-of-type(2)").text(data.string.pg3s6).removeClass("bounceInDown").addClass('tada');
	$("#hrtImg").show(0).addClass('heartBeat');
}

// heart to system
function heartToSystem(){
	$("#hrtImg").removeClass('heartBeat').hide(0);

	$("#human").show(0);
	$("#heart > p:nth-of-type(1)").text(data.string.pg3s3).removeClass("tada").addClass('rubberBand');
	$("#heart > p:nth-of-type(2)").text(data.string.pg3s7).removeClass("tada").addClass('rubberBand');
}

// heartSys to Breathing
function heartToLungs(){
	$("#human").hide(0);

	$("#lungs").show(0);
	$("#heart > p:nth-of-type(1)").text(data.string.pg3s4).removeClass("rubberBand").addClass('wobble');
	$("#heart > p:nth-of-type(2)").text(data.string.pg3s8).removeClass("rubberBand").addClass('wobble');
	$("#activity-page-next-btn-enabled").off("click").hide(0);
	$("#replayButton").show(0);
	ole.footerNotificationHandler.pageEndSetNotification();
}

// animation at the start
function startAnim(){
	$("#heart > p:nth-of-type(1)").show(0).addClass('animated flip').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		$("#tissue").velocity({opacity:"1"},1000,function(){
			$("#heart > p:nth-of-type(2)").show(0).addClass('animated bounceInDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$("#activity-page-next-btn-enabled").show(0);
			});
		});
	});
}

// clickCount for next button
var clickCount=0;
$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(4,1);

	/*the next example nxtEx button*/
	var $nxtEx = $("#activity-page-next-btn-enabled");

	if($lang === "en"){
		var $nsrc="images/arrows/next_"+$lang+".png";
		var $hsrc="images/arrows/next_hover_"+$lang+".png";
		$nxtEx.css({"background-image":"url("+$nsrc+")"});
		$nxtEx.hover(function() {
		 	/*Stuff to do when the mouse enters the element */
			$nxtEx.css('background-image', "url("+$hsrc+")");
		}, function() {
		 	/*Stuff to do when the mouse leaves the element */
		 	$nxtEx.css({"background-image":"url("+$nsrc+")"});
		});
	}

	// pull all data
	$("#heart > p:nth-of-type(1)").text(data.string.pg3s1);
	$("#heart > p:nth-of-type(2)").text(data.string.pg3s5);

	// animation at the start
	startAnim();

	// on clicking next button
	$("#activity-page-next-btn-enabled").on('click',function() {
		clickCount++;
		switch(clickCount){
			case 1: tissueToHeart();loadTimelineProgress(4,2);break;
			case 2: heartToSystem();loadTimelineProgress(4,3);break;
			case 3: heartToLungs();loadTimelineProgress(4,4);break;
			default:break;
		}
	});

	// on clicking replay button
	$("#replayButton").on('click',function() {
		document.location.reload();
	});

});

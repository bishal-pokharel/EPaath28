// do this on clicking plant
function clickPlant(){
	$("#envPlant > p:nth-of-type(1)").text(data.string.pg4s2).addClass('animated bounceInUp');
	$("#envP").fadeOut(1000);
	$("#plant").removeClass('blinkEffect').css({"cursor":"default"});
	$("#plant").velocity({top:"15%",left:"35%",width:"25%"},1000,"easeOutCirc");
	$("#plant").delay(1000).fadeOut(300);
	$("#flower").delay(2200).fadeIn(300,function(){
		$("#flower").hide(0);
		$("#flowerGrow").show(0);
		$("#flowerGrow").delay(3000).velocity({left:"0%"},500,"easeOutCirc",function(){
			$("#ful").show(0).velocity({left:"80%",top:"40%"},500,"easeOutElastic",function(){
				$("#root").show(0).velocity({left:"60%"},500,"easeOutElastic",function(){
					$("#leaf").show(0).velocity({left:"70%"},500,"easeOutElastic",function(){
						$("#trunk").show(0).velocity({left:"60%"},500,"easeOutElastic",function(){
							$("#envPlant > p:nth-of-type(2)").show(0).addClass('animated tada');
							$("#envPlant > b").show(0);
							/*$(".footer-next").addClass('blinkEffect');*/
							ole.footerNotificationHandler.pageEndSetNotification();
						});
					});
				});
			});
		});
	});
}

$(document).ready(function() {	
	/*load the progress bar on this page with one page*/
	loadTimelineProgress(2,1);
	// pull all data
	$("#envPlant > p:nth-of-type(1)").text(data.string.pg4s1);
	$("#envPlant > p:nth-of-type(2)").text(data.string.pg4s3);

	$("#envPlant > b:nth-of-type(1)").text(data.string.pg4s4);
	$("#envPlant > b:nth-of-type(2)").text(data.string.pg4s5);
	$("#envPlant > b:nth-of-type(3)").text(data.string.pg4s6);
	$("#envPlant > b:nth-of-type(4)").text(data.string.pg4s7);
	

	// on clicking plant
	$("#plant").on('click',function() {
		loadTimelineProgress(2,2);
		clickPlant();
	});
});				
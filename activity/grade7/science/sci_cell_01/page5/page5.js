// star animation
function startAnim(){
	$("#cell5 > p:nth-of-type(1)").show(0).addClass('animated wobble').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		$("#pTissue").show(0);
		$("#goToCell").delay(4000).fadeIn(500);
	});
}

/*varable to keep track of mouseover count*/
var cellPartsHoverCount = 0;

$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(3,1);

	/*the next example nxtEx button*/
	var $nxtEx = $("#goToCell");

	if($lang === "en"){
		var $nsrc="images/arrows/next_"+$lang+".png";
		var $hsrc="images/arrows/next_hover_"+$lang+".png";
		$nxtEx.css({"background-image":"url("+$nsrc+")"});
		$nxtEx.hover(function() {
		 	/*Stuff to do when the mouse enters the element */
			$nxtEx.css('background-image', "url("+$hsrc+")");
		}, function() {
		 	/*Stuff to do when the mouse leaves the element */
		 	$nxtEx.css({"background-image":"url("+$nsrc+")"});
		});
	}

	// pull all data
	$("#cell5 > p:nth-of-type(1)").text(data.string.pg5s1);
	$("#cell5 > p:nth-of-type(2)").text(data.string.pg5s4);
	$("#cell5 > p:nth-of-type(3)").text(data.string.pg5s5);
	$("#cell5 > p:nth-of-type(4)").text(data.string.pg5s6);

	startAnim();

	// on clicking go to cell button
	$("#goToCell").on('click', function() {
		loadTimelineProgress(3,2);
		$("#goToCell").css({"display":"none"});
		$("#pTissue").velocity({scale:"10",opacity:"0"},200,function(){
				$("#aCell").show(0);
				$("#pCell").fadeIn(200);
				$("#cell5 > p:nth-of-type(1)").text(data.string.pg5s2).removeClass("wobble").addClass('tada');
					// show insruction to click on cell
				$("#describe").text(data.string.pg5s3).show(0).addClass('blinkEffect');
			});
	});

	// on clicking plant cell
	$("#pCell").on('click',function() {
		loadTimelineProgress(3,3);
		// hide cell instruction and change the instruction to be showed later on
		$("#describe").hide(0).css({"width":"50%","left":"50%"}).text(data.string.pg5s10).removeClass('blinkEffect');
		// hide unlabelled cell
		$("#pCell").hide(0);
		$("#aCell").hide(0);
		$("#pCellLabel").show(0).velocity({left:"5%"},300,"easeOutElastic");
		$("#cell5 > p:nth-of-type(n+2)").show(0);
		$("#describe").css({"top":"40%"}).show(0);
		// added during rework
		$("#goToCell").hide(0);
		ole.footerNotificationHandler.pageEndSetNotification();
	});

	$("#cell5 > p:nth-of-type(n+2)").hover(function() {
		/* Stuff to do when the mouse enters the element */
		var overThat = $(this).attr("id");
		switch(overThat){
			case "cellWall":$("#"+overThat).css({"color":"purple"});$("#describe").text(data.string.pg5s7);cellPartsHoverCount++;break;
			case "vacuole":$("#"+overThat).css({"color":"purple"});$("#describe").text(data.string.pg5s8);cellPartsHoverCount++;break;
			case "chondria":$("#"+overThat).css({"color":"purple"});$("#describe").text(data.string.pg5s9);cellPartsHoverCount++;break;
			default:break;
		}

		if(cellPartsHoverCount > 2){
			$("#replayButton").show(0);

			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg5n1);
		}
	}, function() {
		/* Stuff to do when the mouse leaves the element */
		$(this).css({"color":"black"});
		$("#cell5 > strong:nth-of-type(1)").text(data.string.pg5s10);;
	});

	// on clicking replayButton
	$("#replayButton").on('click', function(event) {
		location.reload();
	});

// end curly braces
});

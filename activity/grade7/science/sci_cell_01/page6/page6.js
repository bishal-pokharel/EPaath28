// default data
$("#plantTopic").text(data.string.pg6s1);
$("#pName").text(data.string.pg6s1);
$("#describe").text(data.string.pg6s2);
$("#function > p:nth-of-type(1)").text(data.string.pg6s3);
$("#f1").text(data.string.pg6s4);
$("#f2").text(data.string.pg6s5);
// for plastid only just give data previously and show or hide when needed
$("#f3").text(data.string.pg6s115);
$("#plastidOnly > li:nth-of-type(1)").text(data.string.pg6s116);
$("#plastidOnly > li:nth-of-type(2)").text(data.string.pg6s117);
$("#plastidOnly > li:nth-of-type(3)").text(data.string.pg6s118);

/*variable to store first touch flag for mitochondria*/
chondriaFirstTouch = true;

/*call for notification footer*/
ole.footerNotificationHandler.setNotificationMsg(data.string.pg6n1);

/*svg manipulation started*/
svgDoc = null;  // reset to null

  // just to make sure svg is rendered before js loads
  	var object = document.getElementById('pcell');

  			if (object.contentDocument)  // works in Mozilla
   			 svgDoc = object.contentDocument;
			  else {
			    try {
			      svgDoc = object.getSVGDocument();
			    }
			    catch(exception) {
			      // ignore errors
			    }
			  }

plantCellFunctions();

function plantCellFunctions(){
	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);
	// cell wall
	var cellWall = svgDoc.getElementById("wall");
	// plasma membrane
	var cMem = svgDoc.getElementById("cellMembrane");
	// cytoplasm
	var cplasm = svgDoc.getElementById("cytoplasm");
	// vacuole
	var vac = svgDoc.getElementById("vacuole");
		// vacuole parts 1 2 and 3
		var vac1 = svgDoc.getElementById("vacuolePart1");
		var vac2 = svgDoc.getElementById("vacuolePart2");
		var vac3 = svgDoc.getElementById("vacuolePart3");
	// ribosomes
	// NOTE: get id for hovering all the group and class for making changes to only those elements
	var rsome = svgDoc.getElementById("ribosome");
	var rsomeClass = svgDoc.getElementsByClassName("ribosomes");

	// nucleus
	var nucleusId = svgDoc.getElementById("nucleus");
	var nucleusClass = svgDoc.getElementsByClassName("nucleusChange");

	// mitochondria
	var chondriaId = svgDoc.getElementById("mitochondria");
	var chondriaClass = svgDoc.getElementsByClassName("mitochondriaClass");

	// endoplasmic Reticulum
	var erId = svgDoc.getElementById("endoplasmicReticulum");
	var erClass = svgDoc.getElementsByClassName("endoplasmicReticulumClass");

	// golgi bodies
	var gbId = svgDoc.getElementById("golgiBody");
	var gbClass1 = svgDoc.getElementsByClassName("gbChange1");
	var gbClass2 = svgDoc.getElementsByClassName("gbChange2");

	// plastids
	var plastidId = svgDoc.getElementById("plastids");
	var plastidClass1 = svgDoc.getElementsByClassName("plastidChange1");
	var plastidClass2 = svgDoc.getElementsByClassName("plastidChange2");



	// on mouse out from cell image show default text
	svgDoc.addEventListener('mouseout', function(){
		$("#pName").text(data.string.pg6s1);
		$("#describe").text(data.string.pg6s2);
		$("#f1").text(data.string.pg6s4);
		$("#f2").text(data.string.pg6s5);
	});

	// on hovering cell wall show this data
	cellWall.addEventListener('mouseover', function(){
		$("#pName").text(data.string.pg6s11);
		$("#describe").text(data.string.pg6s12);
		$("#f1").text(data.string.pg6s13);
		$("#f2").text(data.string.pg6s14);
	});

	// on hovering plasma membrane show this data
	cMem.addEventListener('mouseover', function(){
		$("#pName").text(data.string.pg6s21);
		$("#describe").text(data.string.pg6s22);
		$("#f1").text(data.string.pg6s23);
		$("#f2").text(data.string.pg6s24);
	});

	// on hovering cytoplasm show this data
	cplasm.addEventListener('mouseover', function(){
		$("#pName").text(data.string.pg6s31);
		$("#describe").text(data.string.pg6s32);
		$("#f1").text(data.string.pg6s33);
		$("#f2").text(data.string.pg6s34);
	});

	// on hovering vacuole show this data
	vac.addEventListener('mouseover', function(){
		// data
		$("#pName").text(data.string.pg6s41);
		$("#describe").text(data.string.pg6s42);
		$("#f1").text(data.string.pg6s43);
		$("#f2").text(data.string.pg6s44);

		// changing in colors
		vac1.setAttribute("fill","#BA9C44");
		vac2.setAttribute("fill","#AE4F35");
		vac3.setAttribute("fill","#DE7472");
	});

	// on mouseOut vacuole show this data
	vac.addEventListener('mouseout', function(){
		// changing colors to default
		vac1.setAttribute("fill","#54B6CB");
		vac2.setAttribute("fill","#81C6D2");
		vac3.setAttribute("fill","#8DCBD4");
	});

	// on hovering ribosomes show this data and color change
	rsome.addEventListener('mouseover', function(){
		for(var i=0; i<rsomeClass.length;i++)
		{
			rsomeClass[i].setAttribute("fill","red");
		}

		$("#pName").text(data.string.pg6s51);
		$("#describe").text(data.string.pg6s52);
		$("#f1").text(data.string.pg6s53);
		// since ribosome has only one function we hide the second empty one
		$("#f2").text(data.string.pg6s54).css({"visibility":"hidden"});
	});

	// on mouse out of ribosomes change element color to default
	rsome.addEventListener('mouseout', function(){
		for(var i=0; i<rsomeClass.length;i++)
		{
			rsomeClass[i].setAttribute("fill","#3A5407");
		}

		// we need the second function to be shown on mouseOut of ribosome as it is reused
		// by other elements
		$("#f2").css({"visibility":"visible"});
	});

	// on hovering nucleus show this data and color change
	nucleusId.addEventListener('mouseover', function(){
		for(var i=0; i<nucleusClass.length;i++)
		{
			nucleusClass[i].setAttribute("fill","black");
		}

		$("#pName").text(data.string.pg6s61);
		$("#describe").text(data.string.pg6s62);
		$("#f1").text(data.string.pg6s63);
		$("#f2").text(data.string.pg6s64);
	});

	// on mouseOut nucleus show default colors
	nucleusId.addEventListener('mouseout', function(){
		// these are the default colors look in svg file associated element of this class
		nucleusClass[0].setAttribute("fill","#05A570");
		nucleusClass[1].setAttribute("fill","#33AED5");
		nucleusClass[2].setAttribute("fill","#00502F");
	});

	// on hovering mitochondria show this data and color change
	chondriaId.addEventListener('mouseover', function(){
		for(var i=0; i<chondriaClass.length;i++)
		{
			chondriaClass[i].setAttribute("fill","darkgreen");
		}

		$("#pName").text(data.string.pg6s71);
		$("#describe").text(data.string.pg6s72);
		$("#f1").text(data.string.pg6s73);
		$("#f2").text(data.string.pg6s74);

		/*show notification of go to next page on touching chondria*/
		if(chondriaFirstTouch){
			chondriaFirstTouch = false;
      ole.footerNotificationHandler.pageEndSetNotification();
			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg6n2);
		}
	});

	// on mouseOut mitochondria show default colors
	chondriaId.addEventListener('mouseout', function(){
		// these are the default colors look in svg file associated element of this class
		chondriaClass[0].setAttribute("fill","url(#SVGID_35_)");
		chondriaClass[1].setAttribute("fill","url(#SVGID_36_)");

	});


	// on hovering endoplasmic reticulum show this data and color change
	erId.addEventListener('mouseover', function(){
		for(var i=0; i<erClass.length;i++)
		{
			erClass[i].setAttribute("fill","url(#SVGID_erChangeColor_)");
		}

		$("#pName").text(data.string.pg6s81);
		$("#describe").text(data.string.pg6s82);
		$("#f1").text(data.string.pg6s83);
		$("#f2").text(data.string.pg6s84);
	});

	// on mouseOut endoplasmic reticulum show default colors
	erId.addEventListener('mouseout', function(){
		// these are the default colors look in svg file associated element of this class
		erClass[0].setAttribute("fill","url(#SVGID_1_)");
		erClass[1].setAttribute("fill","url(#SVGID_3_)");
		erClass[2].setAttribute("fill","url(#SVGID_5_)");

	});


	// on hovering golgi body show this data and color change
	gbId.addEventListener('mouseover', function(){
		// this only one element in gbClass1
		gbClass1[0].setAttribute("fill","url(#SVGID_gbChange1_)");
		for(var i=0; i<gbClass2.length;i++)
		{
			gbClass2[i].setAttribute("fill","url(#SVGID_gbChange2_)");
		}

		$("#pName").text(data.string.pg6s101);
		$("#describe").text(data.string.pg6s102);
		$("#f1").text(data.string.pg6s103);
		$("#f2").text(data.string.pg6s104);
	});

	// on mouseOut golgi body show default colors
	gbId.addEventListener('mouseout', function(){
		// these are the default colors look in svg file associated element of this classes
		// one gbClass1 element
		gbClass1[0].setAttribute("fill","#54B6CB");
		// four gbClass2 element
		gbClass2[0].setAttribute("fill","url(#SVGID_31_)");
		gbClass2[1].setAttribute("fill","url(#SVGID_32_)");
		gbClass2[2].setAttribute("fill","url(#SVGID_33_)");
		gbClass2[3].setAttribute("fill","url(#SVGID_34_)");

	});

	// on hovering plastid show this data and color change
	plastidId.addEventListener('mouseover', function(){
		for(var i=0; i<plastidClass1.length;i++)
		{
			plastidClass1[i].setAttribute("fill","#835929");
		}

		for(var i=0; i<plastidClass2.length;i++)
		{
			plastidClass2[i].setAttribute("fill","#9D447A");
		}

		$("#pName").text(data.string.pg6s111);
		$("#describe").text(data.string.pg6s112);
		$("#f1").text(data.string.pg6s113);
		$("#f2").text(data.string.pg6s114);

		// show 3rd function
		$("#f3").show(0);

		// show plastid types
		$("#plastidOnly").show(0);

		// since it is big make the top of the parts div less
		$("#parts").css({"top":"7%"});

		// make font size of functions less
		$("#function > li").css({"font-size":"1em"});

		$("#describe").css({"font-size":"1em"});
	});

	// on mouseOut plastid show default colors
	plastidId.addEventListener('mouseout', function(){
		// these are the default colors look in svg file associated element of this classes
		for(var i=0; i<plastidClass1.length;i++)
		{
			plastidClass1[i].setAttribute("fill","#3A5407");
		}

		for(var i=0; i<plastidClass2.length;i++)
		{
			plastidClass2[i].setAttribute("fill","#38529E");
		}

		// take every thing to defaut
		// hide third function and plastid types it is not needed for others
		$("#f3").hide(0);
		$("#plastidOnly").hide(0);

		// since it is big make the top of the parts div less
		$("#parts").css({"top":"20%"});

		// make font size of functions less
		$("#function > li").css({"font-size":"1.2em"});
		$("#describe").css({"font-size":"1.2em"});

	});
}

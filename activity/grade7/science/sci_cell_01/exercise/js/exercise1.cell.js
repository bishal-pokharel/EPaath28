(function ($) {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(10,1);
	var $board = $('.board'),
		$title = $('.title'),
		$popUp = $('.popUp'),
		$question = $('.questions'),
		countQuestion = 0,
		countCorrect = 0,
		countAnsClicked = 0;
		animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

	var randm = ole.getRandom (10,10,1);

	function first () {
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = {
			name1 : data.string.name1,
			name2 : data.string.name2,
			name3 : data.string.name3,
			name4 : data.string.name4,
			name5 : data.string.name5,
			name6 : data.string.name6,
			name7 : data.string.name7,
			name8 : data.string.name8,
			name9 : data.string.name9,
			name10 : data.string.name10,
			title1 : data.string.title1,
			title2 : data.string.title2,
			title3 : data.string.title3,
			title4 : data.string.title4,
			title5 : data.string.title5,
			title6 : data.string.title6,
			title7 : data.string.title7,
			title8 : data.string.title8,
			title9 : data.string.title9,
			title10 : data.string.title10,

			img1 : $ref+"/exercise/images/cells.png"
		}

		var html = template(content);
		$board.html(html);
		quest();

	}

	first();

	function quest () {
		var newQuest = data.string['statement'+randm[countQuestion]];
		$question.text(newQuest).show(function () {
			$question.addClass('animated bounceInDown');
		})
	}

	$board.on('click','.options',function () {
		var $this = $(this),
			$ans = $this.data('anum');
		if(randm[countQuestion]=== $ans){
			if ($ans<8) {
				$this.addClass('right').removeClass('options');
			} else {
				$this.addClass('rightSmall').removeClass('options');
			}

			console.log(countCorrect);
			if(countAnsClicked===0){
				countCorrect++;
				console.log(countCorrect);
			}
			countAnsClicked=0;
			countQuestion++;
			loadTimelineProgress(10,countQuestion);
			// console.log(countQuestion);
			if(countQuestion<10){
				$board.find('.wrong').removeClass('wrong');
				$question.removeClass('animated bounceInDown');
				quest ();
			} else {
				$question.hide(0);
				$board.animate({
					'margin-top': '-10%'
				},1000,function () {
					if (countCorrect<5) {
						var $text = ole.textSR(data.string.below5,"_xx_",countCorrect);
					} else if (countCorrect<7) {
						var $text = ole.textSR(data.string.below7,"_xx_",countCorrect);
					} else if (countCorrect<9) {
						var $text = ole.textSR(data.string.below9,"_xx_",countCorrect);
					} else if (countCorrect===10) {
						var $text = data.string.all10;
					};

					$('.finalText').text($text);
					$('.lastMsg').show(0).addClass('animated lightSpeedIn');
					ole.footerNotificationHandler.pageEndSetNotification();
				});
			}
		}

		else {
			$this.addClass('wrong');
			countAnsClicked++;
		}
	})

	
	function pop () {
		var source = $('#inst-template').html();
		var template = Handlebars.compile(source);
		var content = {
			inst : data.string.exerciseInst
		}

		var html = template(content);
		$popUp.html(html).show(0);
		ole.vCenter('.popUp .popInst',3)
	}

	pop()

	$popUp.on('click','.closeButton',function () {
		$popUp.hide(0)
	})

})(jQuery);


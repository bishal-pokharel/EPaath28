// default data
$("#animalTopic").text(data.string.pg7s1);
$("#aName").text(data.string.pg7s1);
$("#describe").text(data.string.pg7s2);
$("#function > p:nth-of-type(1)").text(data.string.pg7s3);
$("#f1").text(data.string.pg7s4);
$("#f2").text(data.string.pg7s5);

/*variable to store first touch flag for mitochondria*/
centrioleFirstTouch = true;

/*call for notification footer*/
ole.footerNotificationHandler.setNotificationMsg(data.string.pg7n1);

/*svg manipulation started*/
svgDoc = null;  // reset to null

 // just to make sure svg is rendered before js loads
  	var object = document.getElementById('acell');

  			if (object.contentDocument)  // works in Mozilla
   			 svgDoc = object.contentDocument;
			  else {
			    try {
			      svgDoc = object.getSVGDocument();
			    }
			    catch(exception) {
			      // ignore errors
			    }
			  }

animalCellFunctions();

function animalCellFunctions(){
	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);
// plasma membrane
var cMem = svgDoc.getElementById("cellMembrane");

// cytoplasm
var cplasm = svgDoc.getElementById("cytoplasm");

// vacuole
var vac = svgDoc.getElementById("vacuole");
	// vacuole parts 1 2 and 3
	var vac1 = svgDoc.getElementById("vacuole1");
	var vac2 = svgDoc.getElementById("vacuole2");
	var vac3 = svgDoc.getElementById("vacuole3");

// centrioless
// NOTE: get id for hovering all the group and class for making changes to only those elements
var centrioleId = svgDoc.getElementById("centriole");
var centrioleClass = svgDoc.getElementsByClassName("centrioleChange");

// ribosomes
// NOTE: get id for hovering all the group and class for making changes to only those elements
var rsome = svgDoc.getElementById("ribosome");
var rsomeClass = svgDoc.getElementsByClassName("ribosomes");


// nucleus
var nucleusId = svgDoc.getElementById("nucleus");
var nucleusClass = svgDoc.getElementsByClassName("nucleusChange");

// mitochondria
var chondriaId = svgDoc.getElementById("mitochondria");
var chondriaClass = svgDoc.getElementsByClassName("mitochondriaClass");

// endoplasmic Reticulum
var erId = svgDoc.getElementById("endoplasmicReticulum");
var erClass = svgDoc.getElementsByClassName("endoplasmicReticulumClass");

// golgi bodies
var gbId = svgDoc.getElementById("golgiBody");
var gbClass1 = svgDoc.getElementsByClassName("gbChange1");
var gbClass2 = svgDoc.getElementsByClassName("gbChange2");


// on mouse out from cell image show default text
svgDoc.addEventListener('mouseout', function(){
	$("#aName").text(data.string.pg7s1);
	$("#describe").text(data.string.pg7s2);
	$("#f1").text(data.string.pg7s4);
	$("#f2").text(data.string.pg7s5);
});


// on hovering plasma membrane show this data
cMem.addEventListener('mouseover', function(){
	$("#aName").text(data.string.pg6s21);
	$("#describe").text(data.string.pg6s22);
	$("#f1").text(data.string.pg6s23);
	$("#f2").text(data.string.pg6s24);
});

// on hovering cytoplasm show this data
cplasm.addEventListener('mouseover', function(){
	$("#aName").text(data.string.pg6s31);
	$("#describe").text(data.string.pg6s32);
	$("#f1").text(data.string.pg6s33);
	$("#f2").text(data.string.pg6s34);
});

// on hovering vacuole show this data
vac.addEventListener('mouseover', function(){
	// data
	$("#aName").text(data.string.pg6s41);
	$("#describe").text(data.string.pg6s42);
	$("#f1").text(data.string.pg6s43);
	$("#f2").text(data.string.pg6s44);

	// changing in colors
	vac1.setAttribute("fill","#F97838");
	vac2.setAttribute("fill","#F97838");
	vac3.setAttribute("fill","#F97838");
});

// on mouseOut vacuole show this data
vac.addEventListener('mouseout', function(){
	// changing colors to default
	vac1.setAttribute("fill","#007DBF");
	vac2.setAttribute("fill","#007DBF");
	vac3.setAttribute("fill","#007DBF");
});

// on hovering centriole show this data and color change
centrioleId.addEventListener('mouseover', function(){
	for(var i=0; i<centrioleClass.length;i++)
	{
		centrioleClass[i].setAttribute("fill","#D4FFAB");
	}

	$("#aName").text(data.string.pg6s121);
	$("#describe").text(data.string.pg6s122);
	$("#f1").text(data.string.pg6s123);
	// since centriole has only one function we hide the second empty one
	$("#f2").text(data.string.pg6s124).css({"visibility":"hidden"});

	/*show notification of go to next page on touching centriole*/
		if(centrioleFirstTouch){
			centrioleFirstTouch = false;
      ole.footerNotificationHandler.pageEndSetNotification();
			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg7n2);
		}
});

// on mouse out of centriole change element color to default
centrioleId.addEventListener('mouseout', function(){
	for(var i=0; i<centrioleClass.length;i++)
	{
		centrioleClass[i].setAttribute("fill","#F73C71");
	}

	// we need the second function to be shown on mouseOut of centriole as it is reused
	// by other elements
	$("#f2").css({"visibility":"visible"});
});

// on hovering ribosomes show this data and color change
rsome.addEventListener('mouseover', function(){
	for(var i=0; i<rsomeClass.length;i++)
	{
		rsomeClass[i].setAttribute("fill","#00FFFF");
	}

	$("#aName").text(data.string.pg6s51);
	$("#describe").text(data.string.pg6s52);
	$("#f1").text(data.string.pg6s53);
	// since ribosome has only one function we hide the second empty one
	$("#f2").text(data.string.pg6s54).css({"visibility":"hidden"});
});

// on mouse out of ribosomes change element color to default
rsome.addEventListener('mouseout', function(){
	for(var i=0; i<rsomeClass.length;i++)
	{
		rsomeClass[i].setAttribute("fill","#F24466");
	}

	// we need the second function to be shown on mouseOut of ribosome as it is reused
	// by other elements
	$("#f2").css({"visibility":"visible"});
});

// on hovering nucleus show this data and color change
nucleusId.addEventListener('mouseover', function(){
	// four different colors in nucleus
	nucleusClass[0].setAttribute("fill","#C11C4E");
	nucleusClass[1].setAttribute("fill","#EF533F");
	nucleusClass[2].setAttribute("fill","#99002E");
	nucleusClass[3].setAttribute("fill","#ED003C");

	$("#aName").text(data.string.pg6s61);
	$("#describe").text(data.string.pg6s62);
	$("#f1").text(data.string.pg6s63);
	$("#f2").text(data.string.pg6s64);
});

// on mouseOut nucleus show default colors
nucleusId.addEventListener('mouseout', function(){
	// these are the default colors look in svg file associated element of this class
	nucleusClass[0].setAttribute("fill","#991F6D");
	nucleusClass[1].setAttribute("fill","#9B1538");
	nucleusClass[2].setAttribute("fill","#630448");
	nucleusClass[3].setAttribute("fill","#2D0225");
});

// on hovering mitochondria show this data and color change
chondriaId.addEventListener('mouseover', function(){
	for(var i=0; i<chondriaClass.length;i++)
	{
		chondriaClass[i].setAttribute("fill","url(#SVGID_chondriaColorChange_)");
	}

	$("#aName").text(data.string.pg6s71);
	$("#describe").text(data.string.pg6s72);
	$("#f1").text(data.string.pg6s73);
	$("#f2").text(data.string.pg6s74);
});

// on mouseOut mitochondria show default colors
chondriaId.addEventListener('mouseout', function(){
	// these are the default colors look in svg file associated element of this class
	chondriaClass[0].setAttribute("fill","url(#SVGID_11_)");
	chondriaClass[1].setAttribute("fill","url(#SVGID_12_)");

});


// on hovering endoplasmic reticulum show this data and color change
erId.addEventListener('mouseover', function(){
	for(var i=0; i<erClass.length;i++)
	{
		erClass[i].setAttribute("fill","url(#SVGID_erChangeColor_)");
	}

	$("#aName").text(data.string.pg6s81);
	$("#describe").text(data.string.pg6s82);
	$("#f1").text(data.string.pg6s83);
	$("#f2").text(data.string.pg6s84);
});

// on mouseOut endoplasmic reticulum show default colors
erId.addEventListener('mouseout', function(){
	// these are the default colors look in svg file associated element of this class
	erClass[0].setAttribute("fill","url(#SVGID_1_)");
	erClass[1].setAttribute("fill","url(#SVGID_3_)");
	erClass[2].setAttribute("fill","url(#SVGID_5_)");

});


// on hovering golgi body show this data and color change
gbId.addEventListener('mouseover', function(){
	// this only one element in gbClass1
	gbClass1[0].setAttribute("fill","url(#SVGID_gbChange1_)");
	for(var i=0; i<gbClass2.length;i++)
	{
		gbClass2[i].setAttribute("fill","url(#SVGID_gbChange2_)");
	}

	$("#aName").text(data.string.pg6s101);
	$("#describe").text(data.string.pg6s102);
	$("#f1").text(data.string.pg6s103);
	$("#f2").text(data.string.pg6s104);
});

// on mouseOut golgi body show default colors
gbId.addEventListener('mouseout', function(){
	// these are the default colors look in svg file associated element of this classes
	// one gbClass1 element
	gbClass1[0].setAttribute("fill","#D95088");
	// four gbClass2 element
	gbClass2[0].setAttribute("fill","url(#SVGID_7_)");
	gbClass2[1].setAttribute("fill","url(#SVGID_8_)");
	gbClass2[2].setAttribute("fill","url(#SVGID_9_)");
	gbClass2[3].setAttribute("fill","url(#SVGID_10_)");

});

}

// at first animate the animateHeading
function animateHeading(){
	$("#cellGrp >p:nth-of-type(1)").addClass('animated rollIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
		$("#cellGrp >p:nth-of-type(2)").show(0).addClass('animated lightSpeedIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
			$("#cellGrp >p:nth-of-type(3)").show(0).addClass('animated rotateInDownRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					animationImg();
			});
		});
	});
}

// animation page load
function animationImg(){
	
		$("#blood1").css({"opacity":"1"}).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
			$("#brain1").css({"opacity":"1"}).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
				$("#ova1").css({"opacity":"1"}).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					$("#sperm1").css({"opacity":"1"}).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
						$("#brain2").css({"opacity":"1"}).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
							$("#blood2").css({"opacity":"1"}).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
								$("#ova2").css({"opacity":"1"}).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
									$("#sperm2").css({"opacity":"1"}).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
										// activate pointer events on images of cellGrp div
										//remove class important
										$("#cellGrp > img").css({"pointer-events":"auto"}).removeClass('animated bounceIn');
										$("#cell2instr").text(data.string.pg2s2).show(0);
									});
								});
							});
						});
					});
				});
			});
		
	});
}

$(document).ready(function() {	

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);
	
	// pull data
	$("#cellGrp > p:nth-of-type(1)").text(data.string.pg2s111);
	$("#cellGrp > p:nth-of-type(2)").text(data.string.pg2s112);
	$("#cellGrp > p:nth-of-type(3)").text(data.string.pg2s113);
	$("#cellGrp > p:nth-of-type(4)").text(data.string.pg2s19);
	$("#cellGrp > p:nth-of-type(5)").text(data.string.pg2s19);
	$("#cellGrp > p:nth-of-type(6)").text(data.string.pg2s20);
	$("#cellGrp > p:nth-of-type(7)").text(data.string.pg2s20);
	$("#cellGrp > p:nth-of-type(8)").text(data.string.pg2s21);
	$("#cellGrp > p:nth-of-type(9)").text(data.string.pg2s21);
	$("#cellGrp > p:nth-of-type(10)").text(data.string.pg2s22);
	$("#cellGrp > p:nth-of-type(11)").text(data.string.pg2s22);
	$("#bloodInfo > h3").text(data.string.pg2s3);
	$("#brainInfo > h3").text(data.string.pg2s4);
	$("#ovaInfo > h3").text(data.string.pg2s5);
	$("#spermInfo > h3").text(data.string.pg2s6);
	$("#bloodInfo > ul >li:nth-of-type(1)").text(data.string.pg2s7);
	$("#bloodInfo > ul >li:nth-of-type(2)").text(data.string.pg2s8);
	$("#brainInfo > ul >li:nth-of-type(1)").text(data.string.pg2s10);
	$("#ovaInfo > ul >li:nth-of-type(1)").text(data.string.pg2s13);
	$("#ovaInfo > ul >li:nth-of-type(2)").text(data.string.pg2s14);
	$("#spermInfo > ul >li:nth-of-type(1)").text(data.string.pg2s16);
	$("#spermInfo > ul >li:nth-of-type(2)").text(data.string.pg2s17);
	$("#spermInfo > ul >li:nth-of-type(3)").text(data.string.pg2s18);
	
	// animation at first call function below
	animateHeading();
	
	// on clicking the cells
	$("#cellGrp > img").on('click', function() {
		$("#cell2instr").hide(0);
		
		var whichCell = $(this).attr("class");
		// console.log(whichCell);
		switch(whichCell){
			case "blood": $("#bloodInfo").velocity({left:"0%"},500,"easeOutBounce");
							break;
			case "brain": $("#brainInfo").velocity({left:"0%"},500,"easeOutBounce");
							break;
			case "ova": $("#ovaInfo").velocity({left:"0%"},500,"easeOutBounce");
							break;
			case "sperm": $("#spermInfo").velocity({left:"0%"},500,"easeOutBounce");
							break;
			default:break;
		}

		ole.footerNotificationHandler.pageEndSetNotification();
	});

	//on hovering cell images give them its name
		$("#cellGrp > img").hover(function() {
		   var onTop = $(this).attr("id");
		   switch(onTop){
		   	case "blood1": $("#cellGrp >p:nth-of-type(4)").show(0);
		   							break;
		   	case "blood2": $("#cellGrp >p:nth-of-type(5)").show(0);
		   							break;						
		   	case "brain1": $("#cellGrp >p:nth-of-type(6)").show(0);
		   							break;
		   	case "brain2": $("#cellGrp >p:nth-of-type(7)").show(0);
		   							break;						
		   	case "ova1": $("#cellGrp >p:nth-of-type(8)").show(0);
		   							break;
		   	case "ova2": $("#cellGrp >p:nth-of-type(9)").show(0);
		   							break;						
		   	case "sperm1": $("#cellGrp >p:nth-of-type(10)").show(0);
		   							break;
			case "sperm2": $("#cellGrp >p:nth-of-type(11)").show(0);
									break;
		   	default:break;
		   }

		}, function() {
			/* Stuff to do when the mouse leaves the element */
			// hide all cell labels
			$("#cellGrp > p:nth-of-type(n+4)").hide(0);
		});

	// on clicking close button in every cell Info
	$(".cellInfo > span").on('click',function() {
		var closeThat = "#"+$(this).closest("div").attr("id");
		/*console.log(closeThat);*/
		$(closeThat).velocity({left:"100%"},350,"easeOutCirc");
		ole.footerNotificationHandler.hideNotification();
	});
})			


/*variable to store hover count on cell*/
var hoverCellCount = 0;
$(document).ready(function() {
	/*load the progress bar on this page with one page*/
	loadTimelineProgress(3,1);

	// pull all data
	$("#cell1instr").text(data.string.pg1s1);
	$("#animalCell > p:nth-of-type(1)").text(data.string.pg1s2);
	$("#animalCell > p:nth-of-type(2)").text(data.string.pg1s3);
	$("#animalCell > p:nth-of-type(3)").text(data.string.pg1s4);

	// on hovering the man hover it
	$("#man").click(function() {
		loadTimelineProgress(3,2);
		$("#cell1instr").hide(0);
		$("#man").removeClass('blinkEffect');
		$("#env").velocity({opacity:"0"},800,function(){
			// make sure the image is hidden
			$("#env").hide(0);
		});
		$("#man").velocity({top:"10%",left:"41.5%",width:"17%",height:"80%"},1000,function(){
			// show this sentence
			$("#cell1instr").text(data.string.pg1s9).css({"top":"3%"}).show(0);
			$("#manCell").fadeIn(2000);
				$("#manCell,#man").delay(2000).velocity({scale:"10",opacity:"0"},200,function(){
						// make sure the mancell div and man outline is hidden
					$("#manCell,#man").hide(0);
					$("#cellScatter").fadeIn(0);
					$("#animateCell").fadeIn(0,function(){
					$("#animateCell").addClass('rockMe');
					$("#cell1instr").show(0).text(data.string.pg1s5);
					});
				});
			});
		});

	// on clicking animateCell
	$("#animateCell").on('click',function() {
		loadTimelineProgress(3,3);
		// hide all scatter cellS
		$("#cellScatter").hide(0);
		// hide instruction
		$("#cell1instr").text(data.string.pg1s10);
		$("#animateCell").removeClass('rockMe').hide(0);
		$("#cellBody").show(0);
		$("#man").css({"display":"none"});
		$("#animalCell > p:nth-of-type(-n+3)").show(0);
		// make these area shown so that mouse event can take effect
		$("#wall,#nucleus,#cytoplasm").show(0);
		$(".footer-next").addClass('blinkEffect');
	});

	// on hovering on cell give description
	$("#wall,#nucleus,#cytoplasm").hover(function() {
		/* Stuff to do when the mouse enters the element */
		var part = $(this).attr("id");
		switch(part){
			case "wall" : $("#cellDesc").text(data.string.pg1s6).show(0);
							$("#animalCell > p:nth-of-type(2)").css({"color":"brown"});
							hoverCellCount++;
							break;
			case "cytoplasm" : $("#cellDesc").text(data.string.pg1s7).show(0);
								$("#animalCell > p:nth-of-type(1)").css({"color":"brown"});
								hoverCellCount++;
							break;
			case "nucleus" : $("#cellDesc").text(data.string.pg1s8).show(0);
							$("#animalCell > p:nth-of-type(3)").css({"color":"brown"});
							hoverCellCount++;
							break;
			default:break;
		}

		if(hoverCellCount > 3){
			ole.footerNotificationHandler.pageEndSetNotification();
			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg1n1);
		}

	}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#cellDesc").hide(0);
		$("#animalCell > p").css({"color":"black"});
	});
});

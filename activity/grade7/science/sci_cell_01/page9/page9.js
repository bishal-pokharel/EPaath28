// starting animation
function startAnim(){
	var animEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
	$("#heading").show(0).addClass('animated bounceInUp').one(animEnd, function() {
		$("#amoeba").show(500,function(){
			$("#explain").show(0).addClass('animated bounceInDown').one(animEnd, function() {
				$("#instr").show(0).addClass('animated flash').one(animEnd, function() {
					$("#features").css({"visibility":"visible"});
				});
			});
		});
	});
}

$(document).ready(function() {
	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);
	// pull all data
	$("#heading").text(data.string.pg9s1);
	$("#explain").text(data.string.pg9s2);
	$("#instr").text(data.string.pg9s21);

	$("#amoeba > em:nth-of-type(1)").text(data.string.pg9s22);
	$("#amoeba > em:nth-of-type(2)").text(data.string.pg9s23);
	$("#amoeba > em:nth-of-type(3)").text(data.string.pg9s24);
	$("#amoeba > em:nth-of-type(4)").text(data.string.pg9s25);
	$("#amoeba > em:nth-of-type(5)").text(data.string.pg9s26);
	$("#amoeba > em:nth-of-type(6)").text(data.string.pg9s27);

	$("#feat1, #habitat > b:nth-of-type(1)").text(data.string.pg8s3);
	$("#feat2, #movement > b:nth-of-type(1)").text(data.string.pg8s4);
	$("#feat3, #respiration > b:nth-of-type(1)").text(data.string.pg8s5);
	$("#feat4, #feeding > b:nth-of-type(1)").text(data.string.pg8s6);
	$("#feat5, #reproduction > b:nth-of-type(1)").text(data.string.pg8s7);
	$("#feat6, #excretion > b:nth-of-type(1)").text(data.string.pg8s8);

	$("#habitat > ul > li:nth-of-type(1)").text(data.string.pg9s3);
	$("#habitat > ul > li:nth-of-type(2)").text(data.string.pg9s4);
	$("#habitat > ul > li:nth-of-type(3)").text(data.string.pg9s5);

	$("#movement > ul > li:nth-of-type(1)").text(data.string.pg9s6);
	$("#movement > ul > li:nth-of-type(2)").text(data.string.pg9s7);
	$("#movement > ul > li:nth-of-type(3)").text(data.string.pg9s8);

	$("#respiration > ul > li:nth-of-type(1)").text(data.string.pg9s9);
	$("#respiration > ul > li:nth-of-type(2)").text(data.string.pg9s10);
	$("#respiration > ul > li:nth-of-type(3)").text(data.string.pg9s11);

	$("#feeding > ul > li:nth-of-type(1)").text(data.string.pg9s12);
	$("#feeding > ul > li:nth-of-type(2)").text(data.string.pg9s13);
	$("#feeding > ul > li:nth-of-type(3)").text(data.string.pg9s14);

	$("#reproduction > ul > li:nth-of-type(1)").text(data.string.pg9s15);
	$("#reproduction > ul > li:nth-of-type(2)").text(data.string.pg9s16);
	$("#reproduction > ul > li:nth-of-type(3)").text(data.string.pg9s17);

	$("#excretion > ul > li:nth-of-type(1)").text(data.string.pg9s18);
	$("#excretion > ul > li:nth-of-type(2)").text(data.string.pg9s19);
	$("#excretion > ul > li:nth-of-type(3)").text(data.string.pg9s20);

	// do this animation at first
	setTimeout(function(){
        startAnim();
    },1000);

	// added during rework
	var clickCount = 0;
	// on clicking the options
	$("#features > em").on('click', function() {
		var selected = $(this).attr("id");
		$("#mainPart").css({"opacity":"0.1"});
		$("#features").css({"pointer-events":"none"});

		// src attribute for respective features images
		var timestamp = new Date().getTime();
		// check for nepali of english page and select image accordingly
		if($lang == "np"){
			var movementImg = $ref+"/page9/image/movement.gif?"+timestamp;
			var respirationImg = $ref+"/page9/image/respiration.gif?"+timestamp;
			var feedingImg = $ref+"/page9/image/feeding.gif?"+timestamp;
			var reproductionImg = $ref+"/page9/image/reproduction.gif?"+timestamp;
			var habitatImg = $ref+"/page9/image/habitat.gif?"+timestamp;
			var excretionImg = $ref+"/page9/image/excretion.gif?"+timestamp;
		}

		else if($lang == "en"){
			var movementImg = $ref+"/page9/image/movement-en.gif?"+timestamp;
			var respirationImg = $ref+"/page9/image/respiration-en.gif?"+timestamp;
			var feedingImg = $ref+"/page9/image/feeding.gif?"+timestamp;
			var reproductionImg = $ref+"/page9/image/reproduction-en.gif?"+timestamp;
			var habitatImg = $ref+"/page9/image/habitat.gif?"+timestamp;
			var excretionImg = $ref+"/page9/image/excretion-en.gif?"+timestamp;
		}
		$(this).hasClass("clickedOn")?clickCount=clickCount:clickCount+=1;
		// console.log(!($(this).hasClass("clickedOn")));
		$(this).addClass("clickedOn");


			switch(selected){
				case "feat1": $("#habitat").show(0);
							  $("#habitat").children('img:nth-of-type(1)').attr('src',habitatImg);
							  break;
				case "feat2": $("#movement").show(0);
							  $("#movement").children('img:nth-of-type(1)').attr('src',movementImg);
							  break;
				case "feat3": $("#respiration").show(0);
							  $("#respiration").children('img:nth-of-type(1)').attr('src',respirationImg);
							  break;
				case "feat4": $("#feeding").show(0);
							  $("#feeding").children('img:nth-of-type(1)').attr('src',feedingImg);
							  break;
				case "feat5": $("#reproduction").show(0);
							  $("#reproduction").children('img:nth-of-type(1)').attr('src',reproductionImg);
							  break;
				case "feat6": $("#excretion").show(0);
							  $("#excretion").children('img:nth-of-type(1)').attr('src',excretionImg);
							  break;
				default:break;
			}

			// ole.footerNotificationHandler.lessonEndSetNotification();
			showFinishedButton(clickCount);
	});

	// added later during rework for showing lesson end
	function showFinishedButton(clickCount){
		console.log(clickCount);
		clickCount==6?ole.footerNotificationHandler.lessonEndSetNotification():'';
	}
	// added later during rework

	// close button functionality
	$(".explain > span:nth-of-type(2)").on('click', function() {
		$(this).parent("div").hide(0);
		$("#mainPart").css({"opacity":"1"});
		$("#features").css({"pointer-events":"auto"});
		// ole.footerNotificationHandler.hideNotification();
	});

	// reloading the gif images
	$(".explain > span:nth-of-type(1)").on('click', function() {
		var itsParent = $(this).parent("div");
		var whichFeature = itsParent.attr("id");

		// src attribute for respective features images
		var timestamp = new Date().getTime();
		// check for nepali of english page and select image accordingly
		if($lang == "np"){
			var movementImg = $ref+"/page9/image/movement.gif?"+timestamp;
			var respirationImg = $ref+"/page9/image/respiration.gif?"+timestamp;
			var feedingImg = $ref+"/page9/image/feeding.gif?"+timestamp;
			var reproductionImg = $ref+"/page9/image/reproduction.gif?"+timestamp;
			var habitatImg = $ref+"/page9/image/habitat.gif?"+timestamp;
			var excretionImg = $ref+"/page9/image/excretion.gif?"+timestamp;
		}

		else if($lang == "en"){
			var movementImg = $ref+"/page9/image/movement-en.gif?"+timestamp;
			var respirationImg = $ref+"/page9/image/respiration-en.gif?"+timestamp;
			var feedingImg = $ref+"/page9/image/feeding.gif?"+timestamp;
			var reproductionImg = $ref+"/page9/image/reproduction-en.gif?"+timestamp;
			var habitatImg = $ref+"/page9/image/habitat.gif?"+timestamp;
			var excretionImg = $ref+"/page9/image/excretion-en.gif?"+timestamp;
		}


		switch(whichFeature){
			case "movement":$("#movement").children('img:nth-of-type(1)').attr('src',movementImg);break;
			case "respiration":$("#respiration").children('img:nth-of-type(1)').attr('src',respirationImg);break;
			case "feeding":$("#feeding").children('img:nth-of-type(1)').attr('src',feedingImg);break;
			case "reproduction":$("#reproduction").children('img:nth-of-type(1)').attr('src',reproductionImg);break;
			case "habitat":$("#habitat").children('img:nth-of-type(1)').attr('src',habitatImg);break;
			case "excretion":$("#excretion").children('img:nth-of-type(1)').attr('src',excretionImg);break;
			default:break;
		}
	});

// end of document ready
});

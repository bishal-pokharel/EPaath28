$(function(){
	loadTimelineProgress(1,1);	
	var countAllclick=0;
	var isLampOn=false;
	var countLiving=0;
	var isTvOn=false;
	var isMobileOn=false;


	
	var countBed=0;
	var isCeilOn=false;
	var isTableOn=false;
	var isFanOn=false;
	var isClockOn=false;


	var countKth=0;
	var isHeatOn=false;
	var isFreezeOn=false;
	var isClock2On=false;

	
	$(".closeNewBtn").html(getCloseBtn());


	$(".header_title").html(data.string.txt_0);

	$(".txt_80_a").html(data.string.txt_80_a);
	//$(".txt_80_b").html(data.string.txt_80_b);

	$("#label1").html(data.string.txt_80);
	$("#label2").html(data.string.txt_84);
	$("#label3").html(data.string.txt_89);


	$(".closingText").html(data.string.txt_93);

	
	

	$(".windows").mouseover(function(){
		$(this).find('img').attr('src','activity/grade7/science/sci_electricity_01/images/p9_2.png');

		var winId=$(this).attr('id');
		/*if(winId=="window1")
			$("#label1").show(0);
		else if(winId=="window2")
			$("#label2").show(0);
		else if(winId=="window3")
			$("#label3").show(0);*/	

	}).mouseout(function(){
		$(this).find('img').attr('src','activity/grade7/science/sci_electricity_01/images/p9_3.png');

		/*$("#label1").hide(0);

		$("#label2").hide(0);

		$("#label3").hide(0);*/
	});

	$(".windows").click(function(){

		countAllclick++;

		$(".closingText").fadeOut(500);
		var id=$(this).attr('id');
		$(".innercontent").hide(0);

		if(id=='window1')
			$(".livingroom").show(0);
		else if(id=='window2')
			$(".bedroom").show(0);
		else if(id=='window3')
			$(".kitchen").show(0);
	});


	/******************* Living Room ***************************/
	
	$(".txt_80").html(data.string.txt_80_d);
	$(".txt_81").html(data.string.txt_81);
	$(".txt_82").html(data.string.txt_82);
	$(".txt_83").html(data.string.txt_83);

	var firsttv, firstlamp, firstmobile;
	firsttv=firstlamp=firstmobile=false;

	$("#tvon").click(function(){

		if(firsttv==false)
		{
			countLiving++;
			firsttv=true;
		}


		if(isTvOn==false)
		{
			isTvOn=true;
			var timestramp=new Date().getTime();
			$(this).find('img').attr('src','activity/grade7/science/sci_electricity_01/images/p9_7.gif?'+timestramp);
		}
		else
		{
			isTvOn=false;
			$(this).find('img').attr('src','activity/grade7/science/sci_electricity_01/images/p9_8.png');
		}
		$(".txt_83").show(0);

		showCloseBtn(countLiving,3,".livingroom");
	});

	$(".lamp").click(function(){

		
		if(firstlamp==false)
		{
			countLiving++;
			firstlamp=true;
		}
		if(isLampOn==false)
		{
			isLampOn=true;
			$("#lampon").show(0);
			$("#lampoff").hide(0);
		}
		else
		{
			isLampOn=false;
			$("#lampon").hide(0);
			$("#lampoff").show(0);
		}
		$(".txt_81").show(0);
		showCloseBtn(countLiving,3,".livingroom");
	});

	$("#mobile").click(function(){
		
		if(firstmobile==false)
		{
			countLiving++;
			firstmobile=true;
		}
		
		if(isMobileOn==false)
		{
			isMobileOn=true;
			$(this).find('img').show(0);
		}
		else
		{
				isMobileOn=false;
				$(this).find('img').hide(0);
		}
		$(".txt_82").show(0);
		showCloseBtn(countLiving,3,".livingroom");
	});

	$(".livingroom").on("click", ".closeNewBtn",function(){

		if(isLampOn==false && isTvOn==false && isMobileOn==false)
		{
			/*isLampOn=false;
			isTvOn=false;
			isMobileOn=false;*/

			countLiving=0;
			firsttv=firstlamp=firstmobile=false;


			$(".closingText").fadeOut(500);
			$("#mobile").find('img').hide(0);
			$("#lampon").hide(0);
			$("#lampoff").show(0);
			$("#tvon").find('img').attr('src','activity/grade7/science/sci_electricity_01/images/p9_8.png');
			$(".txt_81,.txt_82,.txt_83,.closeNewBtn").hide(0);

			if(countAllclick>=3){
				ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_1)
			}
			
			$(".livingroom").hide(0);
			$(".innercontent").show(0);
		}
		else
		{
			$(".closingText").fadeIn(500);
		}


	});

	
	/********** Bed ROOM ********** ********** ********** */

	$(".txt_84").html(data.string.txt_80_c);
	$(".txt_85").html(data.string.txt_85);
	$(".txt_86").html(data.string.txt_86);
	$(".txt_87").html(data.string.txt_87);
	$(".txt_88").html(data.string.txt_88);
	
	var firstfan, firstclock, firstceil, firsttable;
	firstfan=firstclock=firstceil=firsttable=false;

	$(".fan").click(function(){
	
		if(firstfan==false)
		{
			countBed++;
			firstfan=true;
		}
		isFanOn=showHide(isFanOn, "#fanoff", "#fanon",".txt_85");
		
				
	});


	$(".clock").click(function(){
		if(firstclock==false)
		{
			countBed++;
			firstclock=true;
		}
		isClockOn=showHide(isClockOn, "#clockoff", "#clockon",".txt_88");
	});

	$(".ceiling").click(function(){

		if(firstceil==false)
		{
			countBed++;
			firstceil=true;
		}

		isCeilOn=showHide(isCeilOn, "#ceiloff", "#ceilon",".txt_87");
	});

	$(".table").click(function(){
		
		if(firsttable==false)
		{
			countBed++;
			firsttable=true;
		}

		isTableOn=showHide(isTableOn, "#tableoff", "#tableon",".txt_86");
	});
	
	

	function showHide($whatTrue, $whatOff, $whatOn,$textShow)
	{
		
		
		if($whatTrue==false)
		{
			
			$whatTrue=true;
			$($whatOn).show(0);
			$($whatOff).hide(0);
		}
		else
		{
			$whatTrue=false;
			$($whatOn).hide(0);
			$($whatOff).show(0);
		}
		$($textShow).show(0);

		showCloseBtn(countBed,4,".bedroom");
		return $whatTrue;
	}

	$(".bedroom").on("click", ".closeNewBtn",function(){

		
		if(isCeilOn==false && isTableOn==false && isFanOn==false)
		{
			firstfan=firstclock=firstceil=firsttable=false;

			countBed=0;
			isCeilOn=isTableOn=isFanOn=isClockOn=false;

			$("#tableon, #ceilon, #clockon,#fanon").hide(0);
			$("#tableoff, #ceiloff, #clockoff,#fanoff").show(0);

			$(".txt_85,.txt_86,.txt_87,.txt_88,.closeNewBtn").hide(0);

			if(countAllclick>=3)ole.nextBlinker();

			$(".bedroom").hide(0);
			$(".innercontent").show(0);

			ole.footerNotificationHandler.pageEndSetNotification();
		}
		else
		{
			$(".closingText").fadeIn(500);
		}


	});


	/***************** KITCHEN ***************************/

	
	$(".txt_89").html(data.string.txt_80_e);
	$(".txt_90").html(data.string.txt_90);
	$(".txt_91").html(data.string.txt_91);
	$(".txt_92").html(data.string.txt_92);
	
	var firstfreeze, firstclock2, firstheat;
	firstfreeze=firstclock2=firstheat=false;
	/*var countKth=0;
	var isHeatOn=false;
	var isFreezeOn=false;
	var isClock2On=false;*/

	$(".freeze").click(function(){
		
		if(firstfreeze==false)
		{
			countKth++;
			firstfreeze=true;

		}

		isFreezeOn=showHide2(isFreezeOn, "#freezeoff", "#freezeon",".txt_91");
	});
	$(".heat").click(function(){
		
		if(firstheat==false)
		{
			countKth++;
			firstheat=true;
		}

		isHeatOn=showHide2(isHeatOn, "#heatoff", "#heaton",".txt_92");
	});

	$(".clock2").click(function(){
		
		if(firstclock2==false)
		{
			countKth++;
			firstclock2=true;
		}

		isClock2On=showHide2(isClock2On, "#clock2off", "#clock2on",".txt_90");
	});

	function showHide2($whatTrue, $whatOff, $whatOn,$textShow)
	{
		
		
		if($whatTrue==false)
		{
			
			$whatTrue=true;
			$($whatOn).show(0);
			$($whatOff).hide(0);
		}
		else
		{
			$whatTrue=false;
			$($whatOn).hide(0);
			$($whatOff).show(0);
		}
		$($textShow).show(0);

		showCloseBtn(countKth,3,".kitchen");
		return $whatTrue;
	}


	$(".kitchen").on("click", ".closeNewBtn",function(){

		if(isHeatOn==false && isFreezeOn==false)
		{
			firstfreeze=firstclock2=firstheat=false;

			countKth=0;
			isClock2On=isHeatOn=isFreezeOn=false;



			$("#clock2on, #heaton, #freezeon").hide(0);
			$("#clock2off, #heatoff, #freezeoff").show(0);

			$(".txt_90,.txt_91,.txt_92,.closeNewBtn").hide(0);

			$(".kitchen").hide(0);
			$(".innercontent").show(0);

			if(countAllclick>=3)
				{
					ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_1)
				}

		}
		else
		{
			$(".closingText").fadeIn(500);
		}



	});

	/*********************************************************/


	

	function showCloseBtn($counter,$whatval,$class)
	{
		if($counter==$whatval)
		{
				$($class).find(".closeNewBtn").show(0);
		}
	}


	
});
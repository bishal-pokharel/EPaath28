var counterMe=0;
$.fn.dropMe=function ($accept)
{
	var $this=$(this);

	$this.droppable({
        accept: $accept,
        drop: function( event, ui ) {
        	var id=ui.draggable.attr('id');
        	$("#"+id).draggable( "disable" );
        	counterMe++;
        	$("#"+id).removeClass('polaroid').addClass('dropped');
        	if(counterMe==8)
        	{
        		$("#e_1_3").delay(100).fadeIn(10);
        		/*$("#retryBtnId").delay(100).fadeIn(10,function(){
                    $(this).addClass('animated shake');
                });*/
				ole.footerNotificationHandler.pageEndSetNotification();
        	}
        }	 
        	
    });

};



$(function(){
    loadTimelineProgress(1,1);

	$("#toDo").html(data.string.e_1);
	$("#text1").html(data.string.e_1_1);
	$("#text2").html(data.string.e_1_2);
	$("#e_1_3").html(data.string.e_1_3);
	$("#retryBtnId").html(getReloadBtn());
	

	$(".draggable").draggable({ revert: "invalid"});

	$("#dropbox1 div").dropMe('#img2, #img3, #img5, #img6');
	$("#dropbox2 div").dropMe('#img1, #img4, #img7, #img8');

	for(var i=1;i<=8;i++)
	{
		$("#p"+i).html(data.string['e1_'+i])
	}

    $("#retryBtnId").click(function(){
        location.reload();
    });
});


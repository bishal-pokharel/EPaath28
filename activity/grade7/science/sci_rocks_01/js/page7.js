/*image paths*/
$refImg = $ref+"/images/page7/"

//array of image
	var funFactImg = [
	"images/funfact/funfact1.png",
	"images/funfact/funfact2.png",
	"images/funfact/funfact3.png",
	"images/funfact/funfact4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);

var content=[
	{	
		funFactImageSource : funFactImg[randomImageNumeral],
		funFactTextData : data.string.p7s0,
		funFactInstructionData : data.string.p7s1
	},
	{	
		smallFunFact: {	
				funFactsImageSource : funFactImg[randomImageNumeral],
				funFactsTextData : data.string.p7s0,
				funFactsInstructionData : data.string.p7s1
		},
		interestingHeadingData : data.string.p7s2,
		imageSrc : $refImg+"Diamond.png",
		interestingTextData : data.string.p7s3
	},
	{	
		smallFunFact: {	
				funFactsImageSource : funFactImg[randomImageNumeral],
				funFactsTextData : data.string.p7s0,
				funFactsInstructionData : data.string.p7s1
		},
		interestingHeadingData : data.string.p7s4,
		imageSrc : $refImg+"Oldestrock.png",
		interestingTextData : data.string.p7s5
	},
	{	
		smallFunFact: {	
				funFactsImageSource : funFactImg[randomImageNumeral],
				funFactsTextData : data.string.p7s0,
				funFactsInstructionData : data.string.p7s1
		},
		interestingHeadingData : data.string.p7s6,
		imageSrc : $refImg+"Pumice.png",
		interestingTextData : data.string.p7s7
	},
	{	
		smallFunFact: {	
				funFactsImageSource : funFactImg[randomImageNumeral],
				funFactsTextData : data.string.p7s0,
				funFactsInstructionData : data.string.p7s1
		},
		interestingHeadingData : data.string.p7s8,
		imageSrc : $refImg+"Moonrock.png",
		interestingTextData : data.string.p7s9
	}
	
]


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 5;
	loadTimelineProgress($total_page,countNext+1);


/*
* funFactLandingPage
*/
	function funFactLandingPage() {
		var source = $("#funFactLandingPage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show(0);
	}

/*
* interesting
*/
	function interesting() {
		var source = $("#interesting-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		if(countNext < 4){
			$nextBtn.show(0);
			$prevBtn.show(0);
			ole.footerNotificationHandler.hideNotification();
		}
		else if(countNext >= 4){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}	
	}

	funFactLandingPage();

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				interesting();
				break;
			case 2:
				$(this).css("display","none");
				interesting();
				break;
			case 3:
				$(this).css("display","none");
				interesting();
				break;
			case 4:
				$(this).css("display","none");
				interesting();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;		
		switch (countNext) {
			case 0:
				$(this).css("display","none");
				funFactLandingPage();
				break;
			case 1:
				$(this).css("display","none");
				interesting();
				break;
			case 2:
				$(this).css("display","none");
				interesting();
				break;
			case 3:
				$(this).css("display","none");
				interesting();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
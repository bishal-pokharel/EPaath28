(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 5,
		$refImg = $ref+"/images/page5/",
		$refImg7 = $ref+"/images/page7/",
		$refImg4 = $ref+"/images/page4/",
		countMetal=0;

		var listcontent; /*content storage for property list  */
		var listNo; /*storage for count of list to be shown*/

	loadTimelineProgress($total_page,countNext);
/*
* functions
*/
	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			textIntro : data.string.p5s1
		}
		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	}
	
	
	

	var properties = [{
		title : data.string.p5s2,
		propertyImage : [
							{propertiesImgSource: $refImg+"shape1.png"},
							{propertiesImgSource: $refImg+"shape2.png"},
							{propertiesImgSource: $refImg+"shape3.png"},
							{propertiesImgSource: $refImg+"shape4.png"}
				    	]
	},
	{
		title : data.string.p5s3,
		propertyImage : [
							{propertiesImgSource: $refImg+"color1.png"},
							{propertiesImgSource: $refImg+"color2.png"},
							{propertiesImgSource: $refImg+"color3.png"},
							{propertiesImgSource: $refImg+"color4.png"}
				    	]
	},
	{
		title : data.string.p5s4,
		propertyImage : [
							{propertiesImgSource: $refImg7+"Diamond.png"},
							{propertiesImgSource: $refImg7+"Pumice.png"},
							{propertiesImgSource: $refImg4+"Sandstone.png"},
							{propertiesImgSource: $refImg4+"Limestone.png"}
				    	]
	}
	];

	var $list = [
				{
					listcount : ole.nepaliNumber("1",$lang),
					listtext : data.string.p5s2
				},
				{
					listcount : ole.nepaliNumber("2",$lang),
					listtext : data.string.p5s3
				},
				{
					listcount : ole.nepaliNumber("3",$lang),
					listtext : data.string.p5s4
				}
	];

	function second (property) {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);		
		var content = {
			listTitle : data.string.p5s1,
			list : $list,
			propertyImage : property.propertyImage,
			titlecount : $list[countNext-1].listcount,
			title : property.title
		}
		var html = template(content);
		$board.html(html);
		$board.find(".propertyMain .propertiesImg");

		/*show the property list appropriately or hide the unwanted =) */
		var $propertylist = $board.children('div.wrapperProperty').children('div.propertyList').children('ul');
		$propertylist.children('li:nth-of-type(n+'+countNext+')').hide(0);			
	}

	function summary () {
		var source = $("#summary-template").html();
		var template = Handlebars.compile(source);
		var content = {
			listTitle : data.string.p5s1,
			list : $list
		}
		var html = template(content);
		$board.html(html);

	}

	first();

	$nextBtn.on('click',function () {
		$(this).hide(0);
		switch (countNext) {
			case 1:
			second(properties[0]);
			break;

			case 2:
			second(properties[1]);
			break;

			case 3:
			second(properties[2]);
			break;

			case 4:
			summary();
			break;
			
			break;
		}

		countNext++;
		loadTimelineProgress($total_page,countNext);

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}
	})

})(jQuery);
/*image paths*/
$refImg = $ref+"/images/page9/";
$refImg1 = $ref+"/images/page1/";
var content=[
	{	
		interestingTextData : data.string.p9s0
	},
	{	
		interestingTextData : data.string.p9s1
	},
	{	
		useHeadingData : data.string.p9s2,
		interestingTextData : data.string.p9s3,
		useExampleImageSource :
			[ 
				{
					imageClass : "singleImg",
					imageSrc : $refImg1+"earth2.png"
				}
			]
	},
	{	
		useHeadingData : data.string.p9s2,
		interestingTextData : data.string.p9s4,
		useExampleImageSource :
			[ 
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"building1.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"building2.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"building3.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"building4.png"
				}
			]
	},
	{	
		useHeadingData : data.string.p9s2,
		interestingTextData : data.string.p9s5,
		useExampleImageSource :
			[ 
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"weapon1.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"weapon2.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"weapon3.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"weapon4.png"
				}
			]
	},
	{	
		useHeadingData : data.string.p9s2,
		interestingTextData : data.string.p9s6,
		useExampleImageSource :
			[ 
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"shaligram1.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"shaligram2.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"shaligram3.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"shaligram4.png"
				}
			]
	},
	{	
		useHeadingData : data.string.p9s2,
		interestingTextData : data.string.p9s7,
		useExampleImageSource :
			[ 
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"fossil1.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"fossil2.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"fossil3.png"
				},
				{
					imageClass : "groupImg",
					imageSrc : $refImg+"fossil4.png"
				}
			]
	}
	
]


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 7;
	loadTimelineProgress($total_page,countNext+1);


/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show(0);
		if(countNext > 0){
			$prevBtn.show(0);
		}
	}

/*
* useofrocks
*/
	function useofrocks() {
		var source = $("#useofrocks-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		if(countNext < 6){
			$nextBtn.show(0);
			$prevBtn.show(0);
			ole.footerNotificationHandler.hideNotification();
		}
		else if(countNext >= 6){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}	
	}

	first();

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				first();
				break;
			case 2:
				$(this).css("display","none");
				useofrocks();
				break;
			case 3:
				$(this).css("display","none");
				useofrocks();
				break;
			case 4:
				$(this).css("display","none");
				useofrocks();
				break;
			case 5:
				$(this).css("display","none");
				useofrocks();
				break;
			case 6:
				$(this).css("display","none");
				useofrocks();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;		
		switch (countNext) {
			case 0:
				$(this).css("display","none");
				first();
				break;
			case 1:
				$(this).css("display","none");
				first();
				break;
			case 2:
				$(this).css("display","none");
				useofrocks();
				break;
			case 3:
				$(this).css("display","none");
				useofrocks();
				break;
			case 4:
				$(this).css("display","none");
				useofrocks();
				break;
			case 5:
				$(this).css("display","none");
				useofrocks();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
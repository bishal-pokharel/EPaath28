var $refImg = $ref+"/images/page4/";
var $refImg7 = $ref+"/images/page7/"; /*pumice, diamond location*/
var $refImg10 = $ref+"/images/page10/"; /*diorite location*/
var $refImg11 = $ref+"/images/page11/"; /*chalk location*/

var content=[
	{	
		textOnlyData : data.string.p4s1
	},
	{
		variety :[
			{
				imgSource : $refImg+"Coal.png",
				imageLabel : data.string.p4s2
			},
			{
				imgSource : $refImg+"Graphite.png",
				imageLabel : data.string.p4s3
			},
			{
				imgSource : $refImg7+"Pumice.png",
				imageLabel : data.string.p4s4
			},
			{
				imgSource : $refImg11+"Chalk.png",
				imageLabel : data.string.p4s5
			},
			{
				imgSource : $refImg7+"Diamond.png",
				imageLabel : data.string.p4s6
			}
		]
	},
	{
		variety :[
			{
				imgSource : $refImg10+"Diorite.png",
				imageLabel : data.string.p4s7
			},
			{
				imgSource : $refImg+"Marble.png",
				imageLabel : data.string.p4s8
			},
			{
				imgSource : $refImg+"Basalt.png",
				imageLabel : data.string.p4s9
			},
			{
				imgSource : $refImg+"Limestone.png",
				imageLabel : data.string.p4s10
			},
			{
				imgSource : $refImg+"Sandstone.png",
				imageLabel : data.string.p4s11
			}
		]
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);


/*
* textOnly
*/
	function textOnly () {
		var source = $("#textOnly-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$nextBtn.show(0);	
	}

	/*
* variety
*/
	function variety() {
		var source = $("#variety-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		if(countNext == 1){
			$nextBtn.show(0);
			ole.footerNotificationHandler.hideNotification();
		}
		else if(countNext >= 1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	textOnly();

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				variety();
				break;
			case 2:
				$(this).css("display","none");
				variety();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;		
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				variety();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
var $refImg4 = $ref+"/images/page4/";
var $refImg7 = $ref+"/images/page7/";
var $refImg6 = $ref+"/images/page6/";

//array of image
	var definitionImg = [
	"images/diy/diy1.png",
	"images/diy/diy2.png",
	"images/diy/diy3.png",
	"images/diy/diy4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);

var content=[
	{	
		diyImageSource : definitionImg[randomImageNumeral],
		diyTextData : data.string.p8s0,
		diyInstructionData : data.string.p8s1
	},
	{	
		smalldiy: {	
				diyImageSource : definitionImg[randomImageNumeral],
				diyTextData : data.string.p8s0,
				diyInstructionData : data.string.p8s2
			},
		stonesvariety :[
			{
				stoneClass : "coal",
				imgSource : $refImg4+"Coal.png"
			},
			{
				stoneClass : "dolomite",
				imgSource : $refImg6+"Dolomite.png"
			},
			{
				stoneClass : "gneiss",
				imgSource : $refImg4+"Gneiss.png"
			},
			{
				stoneClass : "shale",
				imgSource : $refImg4+"Shale.png"
			},
			{
				stoneClass : "granite",
				imgSource : $refImg4+"Granite.png"
			},
			{
				stoneClass : "slate",
				imgSource : $refImg4+"Slate.png"
			},
			{
				stoneClass : "marble",
				imgSource : $refImg4+"Marble.png"
			},
			{
				stoneClass : "graphite",
				imgSource : $refImg4+"Graphite.png"
			},
			{
				stoneClass : "limestone",
				imgSource : $refImg4+"Limestone.png"
			},
			{
				stoneClass : "sandstone",
				imgSource : $refImg4+"Sandstone.png"
			},
			{
				stoneClass : "diamond",
				imgSource : $refImg7+"Diamond.png"
			},
			{
				stoneClass : "conglomerate",
				imgSource : $refImg6+"Conglomerate.png"
			}
		],

		stonesname :[
			{
				stoneNameId : "dolomite",
				stoneNameText : data.string.p8s4
			},
			{
				stoneNameId : "graphite",
				stoneNameText : data.string.p8s10
			},
			{
				stoneNameId : "shale",
				stoneNameText : data.string.p8s6
			},
			{
				stoneNameId : "coal",
				stoneNameText : data.string.p8s3
			},
			{
				stoneNameId : "granite",
				stoneNameText : data.string.p8s7
			},
			{
				stoneNameId : "slate",
				stoneNameText : data.string.p8s8
			},			
			{
				stoneNameId : "diamond",
				stoneNameText : data.string.p8s13
			},
			{
				stoneNameId : "conglomerate",
				stoneNameText : data.string.p8s14
			},
			{
				stoneNameId : "marble",
				stoneNameText : data.string.p8s9
			},
			{
				stoneNameId : "gneiss",
				stoneNameText : data.string.p8s5
			},			
			{
				stoneNameId : "limestone",
				stoneNameText : data.string.p8s11
			},
			{
				stoneNameId : "sandstone",
				stoneNameText : data.string.p8s12
			}			
		],
		conclusionTextData : data.string.p8s15
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);


/*
* diyLandingPage
*/
	function diyLandingPage () {
		var source = $("#diyLandingPage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$nextBtn.show(0);	
	}

	/*
* variety
*/
	function variety() {
		var source = $("#variety-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);		
		
		var $nameCollector = $('.stonesName');
		var $stoneCollector = $(".varietyStones > figure > figcaption");
		var $stoneName = $nameCollector.children('p');

		var nameDropCount = 0;

	$stoneName.draggable({containment: ".variety", revert:"invalid"});

		/*eureka droppable this is nice use of droppable please see the use*/
	$stoneCollector.droppable({ tolerance: "pointer", 
		accept : function(dropElem){
			//dropElem was the dropped element, return true or false to accept/refuse
			if($(this).data('stoneclass') === dropElem.attr("id")){
				return true;
			}
		},
		drop : function (event, ui) {
			$(this).html($(ui.draggable).text());
			$(this).addClass('afterNameDrop');
			$(ui.draggable).css('display', 'none');
			if(++nameDropCount == 12){
				$(ui.draggable).siblings('h4').show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			}

		}
	});
}

	diyLandingPage();

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				variety();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
/*sequence number for journal table rows*/
var tableRowNumber = 10;
var seqNumber = [];
$(function(){
	for(var convertnumber = 1 ; convertnumber <= tableRowNumber ; convertnumber++){
		seqNumber[convertnumber-1] = ole.nepaliNumber(convertnumber, $lang);
	}
});


$commonImg = "images/";
/*image path change the path below inside double quotes accordingly*/
$refImg = $ref+"/images/exercise1/";

/*content array stones*/
var stones=[
		{
			stonenames : [
				{
					stonedata : "marble",
					stonename : data.string.e1marble
				},
				{
					stonedata : "slate",
					stonename : data.string.e1slate
				},
				{
					stonedata : "limestone",
					stonename : data.string.e1limestone
				},
				{
					stonedata : "shale",
					stonename : data.string.e1shale
				},
				{
					stonedata : "pumice",
					stonename : data.string.e1pumice
				},
				{
					stonedata : "granite",
					stonename : data.string.e1granite
				},
				{
					stonedata : "sandstone",
					stonename : data.string.e1sandstone
				},
				{
					stonedata : "basalt",
					stonename : data.string.e1basalt
				},
				{
					stonedata : "conglomerate",
					stonename : data.string.e1conglomerate
				}
			]
		}
	];

/*content object explaination*/
var explaination = {
						"marble" :{
							stoneHeading:data.string.e1marble,
							list:[
								data.string.e1list_marble1,
								data.string.e1list_marble2,
								data.string.e1list_marble3
							]
						},
						"slate" :{
							stoneHeading:data.string.e1slate,
							list:[
								data.string.e1list_slate1,
								data.string.e1list_slate2,
								data.string.e1list_slate3
							]
						},
						"limestone" :{
							stoneHeading:data.string.e1limestone,
							list:[
								data.string.e1list_limestone1,
								data.string.e1list_limestone2,
								data.string.e1list_limestone3
							]
						},
						"shale" :{
							stoneHeading:data.string.e1shale,
							list:[
								data.string.e1list_shale1,
								data.string.e1list_shale2,
								data.string.e1list_shale3
							]
						},
						"pumice" :{
							stoneHeading:data.string.e1pumice,
							list:[
								data.string.e1list_pumice1,
								data.string.e1list_pumice2,
								data.string.e1list_pumice3
							]
						},
						"granite" :{
							stoneHeading:data.string.e1granite,
							list:[
								data.string.e1list_granite1,
								data.string.e1list_granite2,
								data.string.e1list_granite3
							]
						},
						"sandstone" :{
							stoneHeading:data.string.e1sandstone,
							list:[
								data.string.e1list_sandstone1,
								data.string.e1list_sandstone2,
								data.string.e1list_sandstone3
							]
						},
						"basalt" :{
							stoneHeading:data.string.e1basalt,
							list:[
								data.string.e1list_basalt1,
								data.string.e1list_basalt2,
								data.string.e1list_basalt3
							]
						},
						"conglomerate" :{
							stoneHeading:data.string.e1conglomerate,
							list:[
								data.string.e1list_conglomerate1,
								data.string.e1list_conglomerate2,
								data.string.e1list_conglomerate3
							]
						}
	};

/*content array content*/
var content = [
				{
					geologistImgPath : $refImg+"geologistintro.png",
					introlist:[
						data.string.e1intro1list1,
						data.string.e1intro1list2,
						data.string.e1intro1list3
					]
				},
				{
					geologistImgPath : $refImg+"geologistintro.png",
					introlist:[
						data.string.e1intro2list1,
						data.string.e1intro2list2,
						data.string.e1intro2list3,
						data.string.e1intro2list4

					]
				},
				{
					visitLocationData : data.string.e1visit1text,
					locationImgPath : $refImg + "schoolvisit.png",
					geologistImgPath : $refImg + "geologistvisit.png"
				},
				{
					questionData : data.string.e1q1,
					optionData : [
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q1opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q1opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q1opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "stoneroof.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit2text,
					locationImgPath : $refImg + "rivervisit.png",
					geologistImgPath : $refImg + "geologistvisit.png"
				},
				{
					questionData : data.string.e1q2,
					optionData : [
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q2opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q2opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q2opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "conglomerate.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					questionData : data.string.e1q3,
					optionData : [

									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q3opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q3opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q3opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "shale.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					questionData : data.string.e1q4,
					optionData : [
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q4opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q4opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q4opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "limestone.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit3text,
					locationImgPath : $refImg + "pokharavisit.jpg",
					geologistImgPath : $refImg + "geologistvisit.png",
					hasanimationpokhara : true,
					vehiclename : "bus",
					wheelImgSrc : $refImg + "wheelbus.png"
				},
				{
					visitLocationData : data.string.e1visit4text,
					locationImgPath : $refImg + "pokharavisit2.png",
					geologistImgPath : $refImg + "geologistvisit.png"
				},
				{
					visitLocationData : data.string.e1visit5text,
					locationImgPath : $refImg + "pokharavisit3.png",
					geologistImgPath : $refImg + "geologistvisit.png"
				},
				{
					questionData : data.string.e1q5,
					optionData : [
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q5opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q5opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q5opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "granite.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					questionData : data.string.e1q6,
					optionData : [
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q6opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q6opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q6opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "landscape.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit6text,
					locationImgPath : $refImg + "cavevisit.png",
					geologistImgPath : $refImg + "geologistvisit.png"
				},
				{
					questionData : data.string.e1q7,
					optionData : [
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q7opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q7opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q7opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "cavevisit.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit7text,
					locationImgPath : $refImg + "indiavisit.jpg",
					geologistImgPath : $refImg + "geologistvisit.png",
					hasanimationindia : true,
					vehiclename : "train",
					wheelImgSrc : $refImg + "wheeltrain.png"
				},
				{
					questionData : data.string.e1q8,
					optionData : [
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q8opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q8opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q8opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "tajmahal.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					questionData : data.string.e1q9,
					optionData : [
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q9opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q9opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q9opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "qutubminar.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit8text,
					locationImgPath : $refImg + "usavisit.jpg",
					geologistImgPath : $refImg + "geologistvisit.png",
					hasanimationusa : true,
					vehiclename : "plane"
				},
				{
					questionData : data.string.e1q10,
					optionData : [
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q10opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q10opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q10opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "lincon.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit9text,
					locationImgPath : $refImg + "hawaiivisit.png",
					geologistImgPath : $refImg + "geologistvisit.png"
				},
				{
					questionData : data.string.e1q11,
					optionData : [
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q11opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q11opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q11opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "basalt.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit10text,
					locationImgPath : $refImg + "canyonvisit.png",
					geologistImgPath : $refImg + "geologistvisit.png"
				},
				{
					questionData : data.string.e1q12,
					optionData : [
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q12opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q12opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q12opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "canyonvisit.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit11text,
					locationImgPath : $refImg + "monolakevisit.png",
					geologistImgPath : $refImg + "geologistvisit.png"
				},
				{
					questionData : data.string.e1q13,
					optionData : [
									{
										optId : "opt2",
										inputvalue : "incorrect",
										optlabel : data.string.e1q13opwrong1,
										checkMarkImgSrc : $commonImg+"wrong.png"
									},
									{
										optId : "opt1",
										inputvalue : "correct",
										optlabel : data.string.e1q13opcorrect,
										checkMarkImgSrc : $commonImg+"correct.png"
									},
									{
										optId : "opt3",
										inputvalue : "incorrect",
										optlabel : data.string.e1q13opwrong2,
										checkMarkImgSrc : $commonImg+"wrong.png"
									}
								 ],
					triggerBookButtonData: data.string.e1triggerbookdata,
					questionImgPath : $refImg + "pumice.png",
					questiongeologistImgPath : $refImg + "geologistquestion.png"
				},
				{
					visitLocationData : data.string.e1visit12text,
					locationImgPath : $refImg + "backtonepal.jpg",
					geologistImgPath : $refImg + "geologistvisit.png",
					hasanimationnepal : true,
					vehiclename : "planeToNepal"
				},
				{
					journalDescription : data.string.e1journaldescription,
					jurnaltitle: data.string.e1journaltitle,
					heading:[
						data.string.e1jheading1,data.string.e1jheading2,data.string.e1jheading3
					],
					rowdata:[
						{
							sequenceNumber : seqNumber[0],
							rockName : data.string.e1marble,
							observation : data.string.e1jobservationmarble
						},
						{
							sequenceNumber : seqNumber[1],
							rockName : data.string.e1slate,
							observation : data.string.e1jobservationslate
						},
						{
							sequenceNumber : seqNumber[2],
							rockName : data.string.e1limestone,
							observation : data.string.e1jobservationlimestone
						},
						{
							sequenceNumber : seqNumber[3],
							rockName : data.string.e1shale,
							observation : data.string.e1jobservationshale
						},
						{
							sequenceNumber : seqNumber[4],
							rockName : data.string.e1pumice,
							observation : data.string.e1jobservationpumice
						},
						{
							sequenceNumber : seqNumber[5],
							rockName : data.string.e1granite,
							observation : data.string.e1jobservationgranite
						},
						{
							sequenceNumber : seqNumber[6],
							rockName : data.string.e1sandstone,
							observation : data.string.e1jobservationsandstone
						},
						{
							sequenceNumber : seqNumber[7],
							rockName : data.string.e1basalt,
							observation : data.string.e1jobservationbasalt
						},
						{
							sequenceNumber : seqNumber[8],
							rockName : data.string.e1conglomerate,
							observation : data.string.e1jobservationconglomerate
						}
					]
				}

	];


$(function () {
	var $board = $('.board');
	var $closedBook = $('div.book').children('div.closedBook');
	var $clickBookIcon = $closedBook.children('img.clickBook');
	var $openBook = $('div.book').children('div.openBook');
	var $openBookContent = $openBook.children('div.openBookContent');
	var $stonesExplained = $openBookContent.children('div.stonesExplained');
	var $stoneNames = $openBookContent.children('div.stoneNames');
	var $closeOpenBookBtn = $openBookContent.children('div.closeBtn');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	$nextBtn.show(0);
	/*boolean flag to store the if deleted state of the click me icon*/
	var clickBookIconDeleted = false;

	var countNext = 0;
	var $total_page = 28;
	loadTimelineProgress($total_page, countNext+1);
	var explainContent;

	$closedBook.children('p').html(data.string.e1bookTitle);

/*
* on clicking closed book
*/
	$closedBook.on('click', function(event){
		/* Act on the event */
		$(this).fadeOut(50);
		$openBook.fadeIn(50);
		if($nextBtn.is(":visible")){$nextBtn.css('opacity', 0);} /*does not affect the display property of the button but does not show =)*/
	});

/*
* on clicking close open book button
*/
	$closeOpenBookBtn.on('click', function(event) {
		/* Act on the event */
		$openBook.fadeOut(50);
		$closedBook.fadeIn(50);
		if(!clickBookIconDeleted){
			/*remove the click image from DOM -not required*/
			$clickBookIcon.remove();
			$nextBtn.show(0);
			clickBookIconDeleted = true;
		}
		if($nextBtn.is(":visible")){$nextBtn.css('opacity', 1);}
	});

/*
* stonesexplain
*/
	function stonesexplain(whichstone) {
		if(whichstone != null){
			switch(whichstone){
				case "marble": explainContent = explaination.marble; break;
				case "slate": explainContent = explaination.slate; break;
				case "limestone": explainContent = explaination.limestone; break;
				case "shale": explainContent = explaination.shale; break;
				case "pumice": explainContent = explaination.pumice; break;
				case "granite": explainContent = explaination.granite; break;
				case "sandstone": explainContent = explaination.sandstone; break;
				case "basalt": explainContent = explaination.basalt; break;
				case "conglomerate": explainContent = explaination.conglomerate; break;
				default:break;
			}
			var source = $("#stonesexplain-template").html();
			var template = Handlebars.compile(source);
			var html = template(explainContent);
			$stonesExplained.html(html);
		}
		else if(whichstone == null){
				/*show a instruction if no stone is selected*/
			$stonesExplained.html("<p>"+data.string.e1explainStonesInstruction+"</p>");
		}

	}

/*
* stonesnames
*/
	function stonesnames(){
		var source = $("#stonesnames-template").html();
		var template = Handlebars.compile(source);
		var html = template(stones[0]);
		$stoneNames.html(html);
		$theseStones = $stoneNames.children('input:radio');
		$theseStones.on('click', function() {
			/* Act on the event */
			stonesexplain($(this).data("stonenamedata"));
		});
	}

/*
* intro
*/
function intro(){
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		(countNext<1) ? $closedBook.css('display', 'none') : $closedBook.show(0);
	}

/*
* visitlocation
*/
function visitlocation(){
		var source = $("#visitlocation-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$closedBook.css('display', 'none');

		/*the animation div*/
		var $visitAnimationDiv = $board.children('div.visitLocation').children('div.visitAnimation');
		var $vehicle = $visitAnimationDiv.children('div.vehicle');

		/*show nextslide button instantly if the animation div is not present else hide it*/
		$visitAnimationDiv.length > 0 ? $nextBtn.css('display', 'none') : $nextBtn.show(0);

		/*
		* animate if there is animation and trigger $nextBtn to go to next slide
		*/
			/* Act on the event */
			if($vehicle.hasClass('bus')){
				var $wheels = $vehicle.children('img.wheel');
				$wheels.addClass('wheelrotate');
					$vehicle.addClass('moveright').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					$nextBtn.show(0);
				});
			}
			else if($vehicle.hasClass('train')){
				$vehicle.addClass('trainanimation').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					$nextBtn.show(0);
				});
			}
			else if($vehicle.hasClass('plane')){
				$vehicle.addClass('planeanimation').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					$nextBtn.show(0);
				});
			}
			else if($vehicle.hasClass('planeToNepal')){
				$vehicle.addClass('planenepalanimation').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
					$nextBtn.show(0);
				});
			}

	}

/*
* question
*/
function question(){
		var source = $("#question-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.css('display', 'none');

		var $triggerBookButton = $board.children('div.question').children('button.triggerBook');
		var $options = $board.children('div.question').children('div.questionoptions').children('b');
		var $optionInput;

		if($closedBook.is(":hidden")){
			$closedBook.show(0);
		}

		$triggerBookButton.on('click', function() {
			/* Act on the event */
			$closedBook.trigger('click');
		});

		/*on option selection*/
		$options.on('click', function() {
			$optionInput = $(this).children('input:radio');
			/* Act on the event */
			switch($optionInput.val()){
				case "correct": $options.children('input:radio').prop('disabled', true);
								$optionInput.prop("checked",true);
								$options.css('cursor', 'default');
								$options.off("click");
								$(this).css({'color': 'green','background-color':'rgba(0,100,0,0.2)'});
								$nextBtn.show(0);
								$triggerBookButton.remove();
								break;
				case "incorrect":$optionInput.prop({'disabled':true,"checked":true});
								$(this).css({'color': 'red','cursor':'default','background-color':'rgba(100,0,0,0.2)'});
								break;
				default:break;
			}
			$(this).children('img').css('opacity', 1);
		});
	}

/*
* mingmajournal
*/
function mingmajournal(){
		var source = $("#mingmajournal-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$closedBook.css('display', 'none');
		$nextBtn.show(0);
	}

	intro();
	//question(countNext+=21);
	// visitlocation(countNext+=26);
	// mingmajournal(countNext+=27);

	$nextBtn.on('click',function(){
		$(this).css("display","none");
		countNext++;
		switch (countNext) {
			case 1:
				intro();
				stonesnames();
				stonesexplain();
				break;

			case 2:
				// visitlocation();
				$nextBtn.show();
				break;

			case 3:
				question();
				break;

			case 4:
				visitlocation();
				break;

			case 5:
				question();
				break;

			case 6:
				question();
				break;

			case 7:
				question();
				break;

			case 8:
				visitlocation();
				break;

			case 9:
				visitlocation();
				break;

			case 10:
				visitlocation();
				break;

			case 11:
				question();
				break;

			case 12:
				question();
				break;

			case 13:
				visitlocation();
				break;

			case 14:
				question();
				break;

			case 15:
				visitlocation();
				break;

			case 16:
				question();
				break;

			case 17:
				question();
				break;

			case 18:
				visitlocation();
				break;

			case 19:
				question();
				break;

			case 20:
				visitlocation();
				break;

			case 21:
				question();
				break;

			case 22:
				visitlocation();
				break;

			case 23:
				question();
				break;

			case 24:
				visitlocation();
				break;

			case 25:
				question();
				break;

			case 26:
				visitlocation();
				break;

			case 27:
				mingmajournal();
				break;

			case 28:
				ole.activityComplete.finishingcall();
				break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});
});

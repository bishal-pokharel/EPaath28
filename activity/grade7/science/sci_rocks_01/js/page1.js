//array of image
	var timeToThinkImage = [
	"images/timetothink/timetothink1.png",
	"images/timetothink/timetothink2.png",
	"images/timetothink/timetothink3.png",
	"images/timetothink/timetothink4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);

var content=[
	{
		textClass : "title_text",
		imageClass : "bg_full",
		paraText : data.lesson.chapter,
		imageSrc : $ref+"/images/page1/rocks.png"
	},
	{
		imageNtextClassAdd : "",
		textClass : "",
		imageClass : "slide1earth",
		paraText : data.string.p1s1,
		imageSrc : $ref+"/images/page1/earth1.png"
	},
	{
		imageNtextClassAdd : "",
		textClass : "",
		imageClass : "",
		paraText : data.string.p1s2,
		imageSrc : $ref+"/images/page1/ground.png"
	},
	{
		imageNtextClassAdd : "",
		textClass : "",
		imageClass : "",
		paraText : data.string.p1s3,
		imageSrc : $ref+"/images/page1/earth.png"
	},
	{
		timeToThinkImageSource : timeToThinkImage[randomImageNumeral],
		timeToThinkTextData : data.string.p1s6,
		timeToThinkInstructionData : data.string.p1s4
	},
	{
		paraText : data.string.p1s7
	},
	{
		imageNtextClassAdd : "backgroundMeImage",
		textClass : "designText",
		imageClass : "noDisplayImage",
		paraText : data.string.p1s5,
		imageSrc : $ref+"/images/page1/earth2.png"
	}
]


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 7;
	loadTimelineProgress($total_page,countNext+1);

	$(".mixture_ex > p").text(data.string.p1s1);

/*
* imageNText
*/
	function imageNText() {
		var source = $("#imageNText-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		countNext > 0 ? $prevBtn.show(0) : $prevBtn.css('display', 'none');

		if(countNext < 6){
			$nextBtn.show(0);
		}
		else if(countNext == 6){
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

/*
* textOnly
*/
	function textOnly () {
		var source = $("#textOnly-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show(0);
		if(countNext > 0){
			$prevBtn.show(0);
		}
	}

	/*
* timeToThink
*/
	function timeToThink () {
		var source = $("#timeToThink-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.show(0);
		$prevBtn.show(0);
	}

	imageNText();

	$nextBtn.on('click',function () {
		countNext++;
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				imageNText();
				break;
			case 2:
				$(this).css("display","none");
				imageNText();
				break;
			case 3:
				$(this).css("display","none");
				imageNText();
				break;
			case 4:
				$(this).css("display","none");
				timeToThink();
				break;
			case 5:
				$(this).css("display","none");
				textOnly();
				break;
			case 6:
				$(this).css("display","none");
				imageNText();
				break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				imageNText();
				break;
			case 2:
				$(this).css("display","none");
				imageNText();
				break;
			case 3:
				$(this).css("display","none");
				imageNText();
				break;
			case 4:
				$(this).css("display","none");
				timeToThink();
				break;
			case 5:
				$(this).css("display","none");
				textOnly();
				ole.footerNotificationHandler.hideNotification();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});

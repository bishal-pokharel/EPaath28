/*image paths*/
$refImg = $ref+"/images/page10/";
$refImg4 = $ref+"/images/page4/"; /*coal, sandstone, limestone, shale, gneiss
									slate, marble, graphite, granite and basalt in this location*/
$refImg7 = $ref+"/images/page7/"; /*pumice in this location*/
var content=[
	{	
		information : data.string.p10s1,
		igneous : data.string.p10s4,
		sedimentary : data.string.p10s3,
		metamorphic : data.string.p10s5
	},
	{	
		rockTypeTitleClass : "igneousTitle",
		rockTypesTitleData : data.string.p10s4,
		rockWrapClass : "igneous",
		definitionRockData :data.string.p10s7,
		rockImages :
			[ 
				{
					imgLabelData : data.string.p10s9_igneousDiorite,
					imgSrc : $refImg+"Diorite.png"
				},
				{
					imgLabelData : data.string.p10s9_igneousGranite,
					imgSrc : $refImg4+"Granite.png"
				},
				{
					imgLabelData : data.string.p10s9_igneousBasalt,
					imgSrc : $refImg4+"Basalt.png"
				},
				{
					imgLabelData : data.string.p10s9_igneousPumice,
					imgSrc : $refImg7+"Pumice.png"
				}
			]
	},
	{	
		rockTypeTitleClass : "sedimentaryTitle",
		rockTypesTitleData : data.string.p10s3,
		rockWrapClass : "sedimentary",
		definitionRockData :data.string.p10s6,
		rockImages :
			[ 
				{
					imgLabelData : data.string.p10s10_sedimentaryCoal,
					imgSrc : $refImg4+"Coal.png"
				},
				{
					imgLabelData : data.string.p10s10_sedimentarySandstone,
					imgSrc : $refImg4+"Sandstone.png"
				},
				{
					imgLabelData : data.string.p10s10_sedimentaryLimestone,
					imgSrc : $refImg4+"Limestone.png"
				},
				{
					imgLabelData : data.string.p10s10_sedimentaryShale,
					imgSrc : $refImg4+"Shale.png"
				}
			]
	},
	{	
		rockTypeTitleClass : "metamorphicTitle",
		rockTypesTitleData : data.string.p10s5,
		rockWrapClass : "metamorphic",
		definitionRockData :data.string.p10s8,
		rockImages :
			[ 
				{
					imgLabelData : data.string.p10s11_metamorphicSlate,
					imgSrc : $refImg4+"Slate.png"
				},
				{
					imgLabelData : data.string.p10s11_metamorphicMarble,
					imgSrc : $refImg4+"Marble.png"
				},
				{
					imgLabelData : data.string.p10s11_metamorphicGraphite,
					imgSrc : $refImg4+"Graphite.png"
				},
				{
					imgLabelData : data.string.p10s11_metamorphicGneiss,
					imgSrc : $refImg4+"Gneiss.png"
				}
			]
	},
	{	
		rockTypesTitleData : data.string.p10s5,
		metamorphicExample :
			[ 
				{
					metamorphicImgSrc : $refImg+"limestoneMarble.png",
					metamorphosisCause : data.string.p10metamorphosiscause,
					originStone : data.string.p10s10_sedimentaryLimestone,
					productStone : data.string.p10s11_metamorphicMarble,
					metamorphosisExplain : data.string.p10s12
				},
				{
					metamorphicImgSrc : $refImg+"graniteGneiss.png",
					metamorphosisCause : data.string.p10metamorphosiscause,
					originStone : data.string.p10s9_igneousGranite,
					productStone : data.string.p10s11_metamorphicGneiss,
					metamorphosisExplain : data.string.p10s13
				},
				{
					metamorphicImgSrc : $refImg+"shaleSlate.png",
					metamorphosisCause : data.string.p10metamorphosiscause,
					originStone : data.string.p10s10_sedimentaryShale,
					productStone : data.string.p10s11_metamorphicSlate,
					metamorphosisExplain : data.string.p10s14
				}
			]
	}
	
]


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 5;
	loadTimelineProgress($total_page,countNext+1);


/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$board.children('div.textWrapper').children('p.information').fadeIn(1000, function() {
			$(this).next("p").delay(1000).fadeIn(500, function() {
				$(this).next("p").fadeIn(500, function() {
					$(this).next("p").fadeIn(500, function() {
						$nextBtn.show(0);
					});
				});
			});
		});
	}

/*
* typeofrocks
*/
	function typeofrocks() {
		var source = $("#typeofrocks-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show(0);
		$prevBtn.show(0);

		var $rockClass = $board.children('div.rockTypesWrapperClass').children('div:nth-of-type(1)');
		var $definition = $rockClass.children('p:nth-of-type(1)');
		
		var whichStoneType = $rockClass.attr('class');

		switch(whichStoneType){
			case "igneous" :  ole.parseToolTip($definition, data.string.p10s7_igneous_select);
						break;
			case "sedimentary" :  ole.parseToolTip($definition, data.string.p10s6_sedimentary_select);
						break;
			case "metamorphic" :  ole.parseToolTip($definition, data.string.p10s8_metamorphic_select);
						break;
			default:break;
		}
		ole.footerNotificationHandler.hideNotification();		
	}

/*
* metamorphic
*/
	function metamorphic() {
		var source = $("#metamorphic-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$prevBtn.show(0);
		ole.footerNotificationHandler.pageEndSetNotification();
		var $metamorphosisContainer = $board.children('div.metamorphicWrapper').children('div.metamorphosisContainer');
		var $metamorphosisExplain1 = $metamorphosisContainer.children('div:nth-of-type(1)').children('p');
		var $metamorphosisExplain2 = $metamorphosisContainer.children('div:nth-of-type(2)').children('p');
		var $metamorphosisExplain3 = $metamorphosisContainer.children('div:nth-of-type(3)').children('p');
		
		ole.parseToolTip($metamorphosisExplain1,data.string.p10s10_sedimentaryLimestone);
		ole.parseToolTip($metamorphosisExplain1,data.string.p10s11_metamorphicMarble);
		
		ole.parseToolTip($metamorphosisExplain2,data.string.p10s9_igneousGranite);
		ole.parseToolTip($metamorphosisExplain2,data.string.p10s11_metamorphicGneiss);

		ole.parseToolTip($metamorphosisExplain3,data.string.p10s10_sedimentaryShale);
		ole.parseToolTip($metamorphosisExplain3,data.string.p10s11_metamorphicSlate);
	}

	first();
	// typeofrocks(countNext+=3);
	// metamorphic(countNext+=4);

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		countNext++;		
		switch (countNext) {
			case 1:typeofrocks();
				break;
			case 2:typeofrocks();
				break;
			case 3:typeofrocks();
				break;
			case 4:metamorphic();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;		
		switch (countNext) {
			case 0:first();
				break;
			case 1:typeofrocks();
				break;
			case 2:typeofrocks();
				break;
			case 3:typeofrocks();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
var content=[
	{
		imageSrc : $ref+"/images/page3/himal.png",
		exampleText : data.string.p3s1
	},
	{
		imageSrc : $ref+"/images/page3/snowstone.png",
		exampleText : data.string.p3s2
	},
	{
		imageSrc : $ref+"/images/page3/riverstone.png",
		exampleText : data.string.p3s3
	}

]


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	$nextBtn.show(0);
	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);


/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		if(countNext > 0 && countNext < 2){
			$nextBtn.show(0);
			$prevBtn.show(0);
			ole.footerNotificationHandler.hideNotification();
		}
		else if(countNext > 1){
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	first();

	$nextBtn.on('click',function () {
		countNext++;
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				first();
				break;
			case 2:
				$(this).css("display","none");
				first();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		switch (countNext) {
			case 0:
				$(this).css("display","none");
				first();
				break;
			case 1:
				$(this).css("display","none");
				first();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});

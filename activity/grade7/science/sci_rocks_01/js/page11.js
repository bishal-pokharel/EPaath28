/*image path change the path below inside double quotes accordingly*/
$refImg4 = $ref+"/images/page4/";
$refImg6 = $ref+"/images/page6/";
$refImg7 = $ref+"/images/page7/";
$refImg11 = $ref+"/images/page11/";

/*igneous rocks same order as in data xml*/
var graniteImgSrc = $refImg4+"Granite.png";
var basaltImgSrc = $refImg4+"Basalt.png";
var pumiceImgSrc = $refImg7+"Pumice.png";

/*sedimentary rocks same order as in data xml*/
var chalkImgSrc = $refImg11+"Chalk.png";
var coalImgSrc = $refImg4+"Coal.png";
var sandstoneImgSrc = $refImg4+"Sandstone.png";
var limestoneImgSrc = $refImg4+"Limestone.png";
var shaleImgSrc = $refImg4+"Shale.png";
var chertImgSrc = $refImg6+"Chert.png";
var conglomerateImgSrc = $refImg6+"Conglomerate.png";
var dolomiteImgSrc = $refImg6+"Dolomite.png";

/*metamorphic rocks same order as in data xml*/
var slateImgSrc = $refImg4+"Slate.png";
var marbleImgSrc = $refImg4+"Marble.png";
var graphiteImgSrc = $refImg4+"Graphite.png";
var gneissImgSrc = $refImg4+"Gneiss.png";
var quartziteImgSrc = $refImg6+"Quartzite.png";

var $tooltipImgSrc;

/*content object*/

var content=[
	{
		informationData : data.string.p11s1
	},
	{
		informationData : data.string.p11s2,
		igneoustitle : data.string.p10s4,
		sedimentarytitle : data.string.p10s3,
		metamorphictitle : data.string.p10s5,

		igneousexamples : [
			{
				rockname :data.string.p11s_igneous1,
				rocknameClass : "granite",
				stoneImgSrc : graniteImgSrc
			},
			{
				rockname :data.string.p11s_igneous2,
				rocknameClass : "basalt",
				stoneImgSrc : basaltImgSrc
			},
			{
				rockname :data.string.p11s_igneous3,
				rocknameClass : "pumice",
				stoneImgSrc : pumiceImgSrc
			}
		],

		sedimentaryexamples : [
			{
				rockname :data.string.p11s_sedimentary1,
				rocknameClass : "chalk",
				stoneImgSrc : chalkImgSrc
			},
			{
				rockname :data.string.p11s_sedimentary2,
				rocknameClass : "coal",
				stoneImgSrc : coalImgSrc
			},
			{
				rockname :data.string.p11s_sedimentary3,
				rocknameClass : "sandstone",
				stoneImgSrc : sandstoneImgSrc
			},
			{
				rockname :data.string.p11s_sedimentary4,
				rocknameClass : "limestone",
				stoneImgSrc : limestoneImgSrc 
			},
			{
				rockname :data.string.p11s_sedimentary5,
				rocknameClass : "shale",
				stoneImgSrc : shaleImgSrc
			},
			{
				rockname :data.string.p11s_sedimentary6,
				rocknameClass : "chert",
				stoneImgSrc : chertImgSrc
			},
			{
				rockname :data.string.p11s_sedimentary7,
				rocknameClass : "conglomerate",
				stoneImgSrc : conglomerateImgSrc
			},
			{
				rockname :data.string.p11s_sedimentary8,
				rocknameClass : "dolomite",
				stoneImgSrc : dolomiteImgSrc
			}
		],

		metamorphicexamples : [
			{
				rockname :data.string.p11s_metamorphic1,
				rocknameClass : "slate",
				stoneImgSrc : slateImgSrc
			},
			{
				rockname :data.string.p11s_metamorphic2,
				rocknameClass : "marble",
				stoneImgSrc : marbleImgSrc
			},
			{
				rockname :data.string.p11s_metamorphic3,
				rocknameClass : "graphite",
				stoneImgSrc : graphiteImgSrc
			},
			{
				rockname :data.string.p11s_metamorphic4,
				rocknameClass : "gneiss",
				stoneImgSrc : gneissImgSrc
			},
			{
				rockname :data.string.p11s_metamorphic5,
				rocknameClass : "quartzite",
				stoneImgSrc : quartziteImgSrc
			}
		]
	}
	
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

	var imgHtmlPart1 = "<img src='";
	var imgHtmlPart2 = "'><figcaption>";
	var imgHtmlPart3 = "</figcaption>";
	var imgHtml;

/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show(0);
	}	

/*
* classificationExamples
*/
	function classificationExamples() {
		var source = $("#classificationExamples-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var stonesTouched = 0;
		var $stoneFigure = $board.children('div.classificationWrapper').children('div.rocks').children('div').children('ul.rockExamples').children('figure');
		var $stoneNames = $stoneFigure.children('li');
		var totalstones = $stoneNames.length;	
		var $stoneParent;
		
		/*if every stone name is touched at least once then show go to next page*/
		$stoneNames.one('mouseover', function(event) {
			/* Act on the event */
			/*the comparison is with 1 less then total stones because the increament to stones touched is 
			done after the comparison*/
			(stonesTouched >= totalstones-1) ? ole.footerNotificationHandler.pageEndSetNotification() : stonesTouched++;
			imgHtml = imgHtmlPart1+$(this).data("stoneimgsrc")+imgHtmlPart2+$(this).text()+imgHtmlPart3;
			$stoneParent = $(this).parent("figure");
			$(this).replaceWith(imgHtml);
			/* after the children of stone parent is changed to img and figcaption 
			  change their opacity to 1 */
			$stoneParent.children("*").show(500);
		});
	}

	first(); 
	// classificationExamples(countNext++);

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;
		classificationExamples();
		loadTimelineProgress($total_page,countNext+1);
	});

});

var $refImg4 = $ref+"/images/page4/";
var $refImg6 = $ref+"/images/page6/";

var content=[
	{	
		textOnlyData : data.string.p6s0
	},
	{	
		explainData : data.string.p6s1,
		variety :[
			{
				imgSource : $refImg4+"Coal.png",
				imageLabel : data.string.p6s2
			},
			{
				imgSource : $refImg6+"Dolomite.png",
				imageLabel : data.string.p6s3
			},
			{
				imgSource : $refImg4+"Gneiss.png",
				imageLabel : data.string.p6s4
			},
			{
				imgSource : $refImg4+"Shale.png",
				imageLabel : data.string.p6s5
			},
			{
				imgSource : $refImg4+"Granite.png",
				imageLabel : data.string.p6s6
			},
			{
				imgSource : $refImg4+"Slate.png",
				imageLabel : data.string.p6s7
			},
			{
				imgSource : $refImg4+"Marble.png",
				imageLabel : data.string.p6s8
			},
			{
				imgSource : $refImg6+"Quartzite.png",
				imageLabel : data.string.p6s9
			},
			{
				imgSource : $refImg4+"Limestone.png",
				imageLabel : data.string.p6s10
			},
			{
				imgSource : $refImg4+"Sandstone.png",
				imageLabel : data.string.p6s11
			},
			{
				imgSource : $refImg6+"Chert.png",
				imageLabel : data.string.p6s12
			},
			{
				imgSource : $refImg6+"Conglomerate.png",
				imageLabel : data.string.p6s13
			}
		]
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);


/*
* textOnly
*/
	function textOnly () {
		var source = $("#textOnly-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$nextBtn.show(0);	
	}

	/*
* variety
*/
	function variety() {
		var source = $("#variety-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	

		ole.footerNotificationHandler.pageEndSetNotification();
	}

	textOnly();

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				variety();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 1;

	loadTimelineProgress($total_page,countNext);
	$("#activity-page-next-btn-enabled").html(getSubpageMoveButton($lang,"next"));

/*
* first
*/
	// function first () {
	// 	var source = $("#first-template").html()
	// 	var template = Handlebars.compile(source);
	// 	var content = { fstText:data.string.p1_0
	// 	}
	// 	var html = template(content);
	// 	$board.html(html);
	// 	$("#activity-page-next-btn-enabled").show(0);
	// }
	$board.html('<img class="bg_full" src='+$ref+'/images/page1/some_useful_chemicals.png /><p class="front_page_title">'+data.lesson.chapter+'</p>');
	$("#activity-page-next-btn-enabled").show(0);

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);

		second ();

	});

	function second () {
		var source = $("#second-template").html()
		var template = Handlebars.compile(source);
		var content = { fstText:data.string.p1_1,
			imgObj:[
			{imgsrc:$ref+"/images/page1/img14.png",labels:data.string.eg_1},
			{imgsrc:$ref+"/images/page1/img07.png",labels:data.string.eg_2},
			{imgsrc:$ref+"/images/page1/img08.png",labels:data.string.eg_3},
			{imgsrc:$ref+"/images/page1/img04.png",labels:data.string.eg_4},
			{imgsrc:$ref+"/images/page1/img10.png",labels:data.string.eg_5},
			{imgsrc:$ref+"/images/page1/img11.png",labels:data.string.eg_6},
			{imgsrc:$ref+"/images/page1/img02.png",labels:data.string.eg_7},
			{imgsrc:$ref+"/images/page1/img01.png",labels:data.string.eg_8},
			{imgsrc:$ref+"/images/page1/img12.png",labels:data.string.eg_9},
			{imgsrc:$ref+"/images/page1/img05.png",labels:data.string.eg_10},
			{imgsrc:$ref+"/images/page1/img13.png",labels:data.string.eg_11},
			{imgsrc:$ref+"/images/page1/img03.png",labels:data.string.eg_12},
			{imgsrc:$ref+"/images/page1/img09.png",labels:data.string.eg_13},
			{imgsrc:$ref+"/images/page1/img06.png",labels:data.string.eg_14}]
		}
		var html = template(content);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){
				ole.footerNotificationHandler.pageEndSetNotification();

		});

	}





})(jQuery);

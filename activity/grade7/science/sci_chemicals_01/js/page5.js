(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 1;

	var arrayquestion=new Array(1,2,3,4,5);
	var newarray=shuffleArray(arrayquestion);

	var arrayCounter=0;


	var innerCount=1;

	$("#activity-page-next-btn-enabled").html(getSubpageMoveButton($lang,"next"));


	getHtml();

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		innerCount++;

		if(innerCount<=3)
			secondHtml();
		else
		{

			innerCount=1;
			arrayCounter++;
			getHtml();
		}

	});



	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		var source = $("#first-template").html()


		var template = Handlebars.compile(source);
		var content= { frstTxt1:data.string["p5_q_"+newarray[arrayCounter]],
						frstTxt:data.string.p5_q_0,
						imgObj:[{imgsrc:$ref+"/images/page5/img1.png" ,label:data.string.p4_6_1, imgid:"chem_1"},
						{imgsrc:$ref+"/images/page5/img2.png" ,label:data.string.p4_6_2, imgid:"chem_2"},
						{imgsrc:$ref+"/images/page5/img3.png" ,label:data.string.p4_6_3, imgid:"chem_3"},
						{imgsrc:$ref+"/images/page5/img4.png" ,label:data.string.p4_6_4, imgid:"chem_4"},
						{imgsrc:$ref+"/images/page5/img5.png" ,label:data.string.p4_6_5, imgid:"chem_5"}]
				};

		var html = template(content);
		$board.fadeOut(10,function(){

			$(this).html(html);
			hideChemical();
			//$(".firstText").html('');

		}).delay(10).fadeIn(10,function(){

			 if(arrayCounter<5)
			 {
			 	changeQuestion();
			 }
			 else
			 {
			 	$(".firstText2").hide(0);
			 	$(".firstText").html(data.string.complete).addClass('middleVal');
			 	$(".imgval").addClass('enlargeItems');

			 	ole.footerNotificationHandler.pageEndSetNotification();
			 }
		});

	}



	function changeQuestion(){


		setTimeout(function(){
			/*$(".firstText").html(data.string["p5_q_"+newarray[arrayCounter]]);*/
			$(".myImgObj").addClass('clickAsAnswer');
		},1000);
	}

	$(".board").on('click','.clickAsAnswer',function(){

		var whatcnt=newarray[arrayCounter];
		var clickId=$(this).attr('id');


		if(clickId==("chem_"+whatcnt))
		{
			$(".myImgObj").removeClass('clickAsAnswer');
			$("#correctMe").fadeIn(100,function(){

			}).delay(500).fadeOut(100,function(){
				secondHtml();
				// console.log(clickId)
			});


		}
		else
		{
			$("#incorrectMe").fadeIn(100).delay(500).fadeOut(100);
		}


	});

	function shuffleArray(array) {
		    for (var i = array.length - 1; i > 0; i--) {
		        var j = Math.floor(Math.random() * (i + 1));
		        var temp = array[i];
		        array[i] = array[j];
		        array[j] = temp;
		    }
		    return array;
	}

	function hideChemical()
	{
		var htmlImg="";
		if(arrayCounter>0)
		{

			for(var i=0;i<=(arrayCounter-1);i++)
			{

				$("#chem_"+newarray[i]).hide(0);

				htmlImg=htmlImg+"<img src='"+$ref+"/images/page5/img"+newarray[i]+".png' class='firstme'/>"

			}
			$(".imgval").html(htmlImg);
		}
	}


	function getDetailData()
	{

		switch(innerCount)
		{
			case 1:
			{

				// loadTimelineProgress(newarray.length,innerCount);
				var content={
					headtitle:data.string['p4_6_'+newarray[arrayCounter]],
					def:data.string['p6_'+newarray[arrayCounter]],
					imgsrc:$ref+"/images/page5/img"+newarray[arrayCounter]+".png",
					imgClass:"forCls1"
				}
				break;
			}
			case 2:
			{
				var cnt=advList(newarray[arrayCounter]);
				// loadTimelineProgress(newarray.length,innerCount);




				var content={
					headtitle:data.string['p4_6_'+newarray[arrayCounter]],
					ttl:data.string['why_'+newarray[arrayCounter]],
					imgsrc:$ref+"/images/page5/img"+newarray[arrayCounter]+"_2.png",
					imgClass:"forCls1",
					listme:cnt
				}
				break;
			}
			case 3:
			{


				var cnt=dangerList(newarray[arrayCounter]);
				// loadTimelineProgress(newarray.length,innerCount);

				if(newarray[arrayCounter]==2)
				{

					var content={
						headtitle:data.string['p4_6_'+newarray[arrayCounter]],
						ttl:data.string['danger'],
						imgsrc:$ref+"/images/page5/img"+newarray[arrayCounter]+"_3_"+$lang+".png",
						imgClass:"forCls3",
						listme:cnt
					}
				}
				else
				{
					var content={
						headtitle:data.string['p4_6_'+newarray[arrayCounter]],
						ttl:data.string['danger'],
						imgsrc:$ref+"/images/page5/img"+newarray[arrayCounter]+"_3.png",
						imgClass:"forCls3",
						listme:cnt
					}

				}

				break;
			}

		}
		return content;

	}

	function secondHtml()
	{
		var datacnt=getDetailData();
		var source = $("#second-template").html()


		var template = Handlebars.compile(source);


		var html = template(datacnt);
		loadTimelineProgress(3,innerCount);
		$board.fadeOut(10,function(){
			$(this).html(html);

		}).delay(10).fadeIn(10,function(){

			 	$("#activity-page-next-btn-enabled").show(0);

		});

	}

	function advList(acounter)
	{
		var cnt=[];
		switch(acounter)
		{
			case 1:
				cnt=[data.string.lst_1_1,data.string.lst_1_2,data.string.lst_1_3,data.string.lst_1_4,data.string.lst_1_5,data.string.lst_1_6];
				break

			case 2:
				cnt=[data.string.lst_2_1,data.string.lst_2_2,data.string.lst_2_3,data.string.lst_2_4,data.string.lst_2_5];
				break;

			case 3:
				cnt=[data.string.lst_3_1,data.string.lst_3_2,data.string.lst_3_3,data.string.lst_3_4];
				break;

			case 4:
				cnt=[data.string.lst_4_1,data.string.lst_4_2,data.string.lst_4_3];
				break;

			case 5:
				cnt=[data.string.lst_5_1,data.string.lst_5_2];
				break;
		}
		return cnt;
	}

	function dangerList(acounter)
	{

		var cnt=[];
			switch(acounter)
			{
				case 1:
					cnt=[data.string.samp_1_1];
					break

				case 2:
					cnt=[data.string.samp_2_2,data.string.samp_2_3];
					break;

				case 3:
					cnt=[data.string.samp_3_1,data.string.samp_3_2];
					break;

				case 4:
					cnt=[data.string.samp_4_1];
					break;

				case 5:
					cnt=[data.string.samp_5_1,data.string.samp_5_2,data.string.samp_5_3,data.string.samp_5_4,data.string.samp_5_5];

					break;
			}
			return cnt;
	}



})(jQuery);

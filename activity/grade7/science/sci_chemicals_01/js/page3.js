(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 4;

	
	$("#activity-page-next-btn-enabled").html(getSubpageMoveButton($lang,"next"));
	$(".prevButton").html(getSubpageMoveButton($lang,"prev"));


	getHtml(countNext)

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		countNext++;
		getHtml(countNext);

	});

	$(".prevButton").click(function(){
		$(this).hide(0);
		countNext--;
		getHtml(countNext);

	});

	

	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		 if(countNext==4)
			var source = $("#second-template").html();
		else var source = $("#first-template").html()
		
		var template = Handlebars.compile(source);
		var content= getdata();

		var html = template(content);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){
				if(countNext<$total_page)
				{
					$("#activity-page-next-btn-enabled").show(0);
				}
				if(countNext>1)
				{
					$(".prevButton").show(0);
				}
				else
				{
					ole.footerNotificationHandler.pageEndSetNotification();
				}

		});

	}
	function getdata()
	{
		var content;
		switch(countNext)
		{
			case 1:
				content = { 
						fstText:data.string.p4_1,
						imgObj:[{imgsrc:$ref+"/images/page3/water.png" ,label:data.string.p4_1_1},
						{imgsrc:$ref+"/images/page3/sugar.png" ,label:data.string.p4_1_2},
						{imgsrc:$ref+"/images/page3/salt.png" ,label:data.string.p4_1_3},
						{imgsrc:$ref+"/images/page3/oxygen.png" ,label:data.string.p4_1_4},
						{imgsrc:$ref+"/images/page3/vitaminc.png" ,label:data.string.p4_1_5},
						{imgsrc:$ref+"/images/page3/lead.png" ,label:data.string.p4_1_6}]
				}

				break;
			case 2:
			content = { 
						fstText:data.string.p4_2,
						imgObj:[{imgsrc:$ref+"/images/page3/honey.png" ,label:data.string.p4_2_1},
						{imgsrc:$ref+"/images/page3/blood.png" ,label:data.string.p4_2_2},
						{imgsrc:$ref+"/images/page3/milk.png" ,label:data.string.p4_2_3},
						{imgsrc:$ref+"/images/page1/img08.png" ,label:data.string.p4_2_4},
						{imgsrc:$ref+"/images/page1/img06.png" ,label:data.string.p4_2_5},
						{imgsrc:$ref+"/images/page1/img07.png" ,label:data.string.p4_2_6}]
				}
				break;
			case 3:
			content ={ 
						fstText:data.string.p4_3,
						fstText2:data.string.p4_4,
						imgObj:[{imgsrc:$ref+"/images/page3/sunrays.png" ,label:data.string.p4_3_1},
						{imgsrc:$ref+"/images/page3/pain.png" ,label:data.string.p4_3_2},
						{imgsrc:$ref+"/images/page3/sound.png" ,label:data.string.p4_3_3},
						{imgsrc:$ref+"/images/page3/maya.png" ,label:data.string.p4_3_4},
						{imgsrc:$ref+"/images/page3/study.png" ,label:data.string.p4_3_5},
						{imgsrc:$ref+"/images/page3/electricity.png" ,label:data.string.p4_3_6}]
				}
				break;
			case 4:
				content = { 
						fstText:data.string.p4_5,
						txt1:data.string.p4_5_1,
						txt2:data.string.p4_5_2,
						txt3:data.string.p4_5_3,
						imgObj:[{label:data.string.p4_1_1},
						{label:data.string.p4_1_2},
						{label:data.string.p4_1_3},
						{label:data.string.p4_1_4},
						{label:data.string.p4_1_5},
						{label:data.string.p4_1_6}],

						imgObj3:[{label:data.string.p4_3_1},
						{label:data.string.p4_3_2},
						{label:data.string.p4_3_3},
						{label:data.string.p4_3_4},
						{label:data.string.p4_3_5},
						{label:data.string.p4_3_6}],

						imgObj2:[{label:data.string.p4_2_1},
						{label:data.string.p4_2_2},
						{label:data.string.p4_2_3},
						{label:data.string.p4_2_4},
						{label:data.string.p4_2_5},
						{label:data.string.p4_2_6}]
				}
			break;
			

		}

		return content
	}


	
})(jQuery);




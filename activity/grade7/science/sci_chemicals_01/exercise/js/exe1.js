
(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 11,
		right=0,
		wrong=0,
		isclicked=false;

	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$nextBtn.html(whatnextBtn);
	
	$(".headtitle").html(data.string.exe1);


	getHtml(countNext);

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		isclicked=false;
		
		$("#Imgshow").fadeOut(0);
		countNext++;
		getHtml(countNext);

	});

	$(".board").on('click','.radioBx',function(){
		if(isclicked==false)
		{
			var ids=parseInt($(this).attr('corr'));
			var myids=$(this).attr('id');
			isclicked=true;

			if(ids==1)
			{
				$(this).addClass('correctColor');
				right++;		
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);	

			}
			else
			{
				wrong++;
				$(".radioBx").not(this).addClass('correctColor');
				$(this).addClass('colorChange');
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
			}
			if(countNext<11)
					$nextBtn.show(0);
			
		}
	});

	

	function getHtml(countNext)
	{
		loadTimelineProgress($total_page,countNext);
		

		var source = $("#first-template").html();
		if(countNext==11)
		 source =$("#template-2").html();
		
		var template = Handlebars.compile(source);
		var cor1,cor2;
		if(countNext==1)
		{
			cor1=0;
			cor2=1;
		}
		else if(countNext==2)
		{
			cor1=1;
			cor2=0;
		}
		else if(countNext==3)
		{
			cor1=1;
			cor2=0;
		}
		else if(countNext==4)
		{
			cor1=0;
			cor2=1;
		}
		else if(countNext==5)
		{
			cor1=1;
			cor2=0;
		}
		else if(countNext==6)
		{
			cor1=0;
			cor2=1;
		}
		else if(countNext==7)
		{
			cor1=1;
			cor2=0;
		}
		else if(countNext==8)
		{
			cor1=1;
			cor2=0;
		}
		else if(countNext==9)
		{
			cor1=0;
			cor2=1;
		}
		else if(countNext==10)
		{
			cor1=1;
			cor2=0;
		} else if(countNext==12) {
			ole.activityComplete.finishingcall();
		}

		if(countNext==11)
		{
			var content= { 
				allansw:[{ques:data.string.exe_1, qans:data.string.exe_1_optb},
				{ques:data.string.exe_2, qans:data.string.exe_2_opta},
				{ques:data.string.exe_3, qans:data.string.exe_3_opta},
				{ques:data.string.exe_4, qans:data.string.exe_4_optb},
				{ques:data.string.exe_5, qans:data.string.exe_5_opta},
				{ques:data.string.exe_6, qans:data.string.exe_6_optb},
				{ques:data.string.exe_7, qans:data.string.exe_7_opta},
				{ques:data.string.exe_8, qans:data.string.exe_8_opta},
				{ques:data.string.exe_9, qans:data.string.exe_9_optb},
				{ques:data.string.exe_10, qans:data.string.exe_10_opta}],
				rite:data.string.q1_11,
				wrng:data.string.q1_12,
				rnum:right,
				wnum:wrong
			};
			$nextBtn.show(0);
		}

		else
		{
			var content= { 
				fstText:data.string["exe_"+countNext],
				imgObj:[{imgsrc:$ref+"/exercise/images/Pic"+countNext+".png" ,label:data.string["p6_cap"+countNext]}],
				opt1:data.string["exe_"+countNext+"_opta"],
				opt2:data.string["exe_"+countNext+"_optb"],
				corr1:cor1,
				corr2:cor2
			};
		}
		

		var html = template(content);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){
				

			// if(countNext==11)
			// 	ole.footerNotificationHandler.showRestartButton();
				
		});

	}
	


	
})(jQuery);




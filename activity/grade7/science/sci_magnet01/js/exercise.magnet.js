(function ($) {

	loadTimelineProgress(1,1);

	var $board = $('.board'),
		animatEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
		questionClicked = '',
		randomQ = ole.getRandom(8,10,1),
		randomA = ole.getRandom(8,7),
		rightDone = 0,
		$popUp = $('.magnet .popUp'),
		lastTime = 0;


		var Imgs = [
			"",
			"makingmagnet02/attract.gif",
			"gif/mag_attr_all.gif",
			"gif/1.gif",
			"gif/2.gif",
			"gif/magnetic_field.gif",
			"naturepop.png",
			"electromagnet/wire_5.gif",
			"destroy/distroying1.gif",
			"ushapeprotect.png",
			"gif/non_megnatic.gif"
		]

	function first () {
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var contents = {
			questions : [],
			answer : []
		}
		var j=0;
		for (var i = 0; i < 8; i++) {
			var j=j+1;
			contents.questions.push({
				question : data.string["q"+randomQ[i]],
				qno : randomQ[i]
			});
			contents.answer.push({
				ans : data.string["a"+randomQ[randomA[i]]],
				ano : randomQ[randomA[i]]
			});
		};

		// console.log(contents.questions)

		var html = template(contents);
		$board.html(html);
		callInst();
	}

	$board.on('click','.qq',function () {
		var $this = $(this);
		questionClicked=$this.data('qno');
		$board.find('.qq').removeClass('qColor');
		$this.addClass('animated pulse qColor').one(animatEnd,function () {
			$this.removeClass('animated pulse');

		});
	})

	$board.on('click','.ans',function () {
		var $this = $(this);
		var ansClicked = $this.data('qno');
		if (questionClicked === ansClicked) {
			rightDone++;
			$this.addClass('animated rotateOut');
			$board.find('.qColor').addClass('animated rotateOut');
			loadCorrect();
		} else {
			questionClicked=0;
			$board.find('.qq').removeClass('qColor');
		}

	})

	function callInst () {
		var source = $('#popUp-instruction-template').html();
		var template = Handlebars.compile(source);
		var content = {
			inst1 : data.string.inst_1,
			qBox : data.string.inst_4,
			aBox : data.string.inst_5,
			inst2 : data.string.inst_2,
			start : data.string.inst_6
		}
		var html = template(content);

		$popUp.html(html).show(0);
		// ole.vCenter(".popUp .wrapPop",2);
	}

	$popUp.on('click','.start',function () {
		$popUp.hide(0);
		//set Initial time
		var d = new Date();
		lastTime = d.getTime();
		console.log(lastTime);
	})

	function finishLine () {
		var d = new Date();
		var newTime = d.getTime();
		var grossTime = (newTime-lastTime)/1000;

			minute = Math.floor(grossTime/60);
			second = Math.round((grossTime%60) * 100) / 100
		var t = ""
		if (minute!=0) {
			t = minute+" "+data.string.minute+" "
		}

		t = t+second+" "+data.string.second

		console.log(grossTime);


		var source = $('#last-template').html();
		var template = Handlebars.compile(source);
		var content = {
			inst : data.string.inst_3,
			timet : data.string.inst_7,
			timeS : t
		}
		var html = template(content);

		$popUp.html(html).show(0);
		// ole.vCenter(".popUp .wrapPop",2);
	}

	$popUp.on('click','.nextBtn',function () {
		ole.activityComplete.finishingcall();
	})

	$popUp.on('click','.glyphicon-repeat1',function () {
		location.reload();
	})

	function loadCorrect () {

		if (questionClicked===2 || questionClicked===6 || questionClicked===10) {
			newMsg = data.string["q"+questionClicked]+" --"+"<span class='ans1'>"+data.string["a"+questionClicked]+"</span>";
		} else {
			newMsg = ole.textSR(data.string["q"+questionClicked],'_____',"<span class='ans1'>"+data.string["a"+questionClicked]+"</span>");
		}

		var source = $('#afterCorrect-template').html();
		var template = Handlebars.compile(source);
		var content = {
			correct : data.string.correct,
			img1 : $ref+"/images/"+Imgs[questionClicked],
			msg : newMsg
		}
		var html = template(content);
		$popUp.html(html).show(function () {
			if (questionClicked===8 || questionClicked===1 || questionClicked===9) {
				$popUp.find('.imgHolder').animate({
					'width':'30%'},0,function () {
						// ole.vCenter(".popUp .showPic",2);
					});
			} else {
				// ole.vCenter(".popUp .showPic",2);
			}
		});
	}

	$popUp.on('click','.cls-btn',function () {
		$popUp.hide(0);
		if(rightDone>=8) {
			finishLine();
		}
	})

	first();

	// loadCorrect();

})(jQuery);
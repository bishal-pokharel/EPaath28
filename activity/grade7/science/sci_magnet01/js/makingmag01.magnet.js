(function ($) {
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext =0, countDrag=0;
	var $makerBoard = $('.makerBoard');
	var $spaceForPic = $makerBoard.find('.spaceForPic img');
	var $teller = $makerBoard.find('.teller');
	var $draggableObj  = $( ".draggableObj" );
	
	var $wahtar=getSubpageMoveButton($lang,"next");

	$("#activity-page-next-btn-enabled").html($wahtar);

	loadTimelineProgress(7,1); 


	$('.title').text(data.string.makingTitle01);
	$('.useDesc').text("");

	$('.title').css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {
		$teller.text(data.string.e01);
		$makerBoard.show(0);
		$nextBtn.delay(1500).fadeIn();
	});

	var animateWire1 = function () {
		$spaceForPic.attr('src',$ref+'/images/electromagnet/wire_4.gif');
		$teller.text(data.string.e04);
		$draggableObj.removeAttr('style');
		$draggableObj.hide(0);
		$nextBtn.fadeIn();
	}

	var animateWire2 = function () {
		$spaceForPic.attr('src',$ref+'/images/electromagnet/wire_6.gif');
		$teller.text(data.string.conclusionBattery);
		$draggableObj.removeAttr('style');
		$draggableObj.hide(0);
		$nextBtn.fadeIn();
	}

	$draggableObj.draggable();
	$( ".droppableObj" ).droppable({
		drop: function( event, ui ) {
			if (countDrag===0) {
				animateWire1();
			}
			else if (countDrag ===1) {
				animateWire2();
			}
			countDrag++;
		}
	});

	$nextBtn.on('click',function () {
		// if (countNext ===0) {
		// 	$makerBoard.find('.spaceForPic img').attr('src',$ref+'/images/electromagnet/wire_2.gif')
		// }

		switch (countNext) {
			case 0 :
			$spaceForPic.attr('src',$ref+'/images/electromagnet/wire_2.gif');
			$teller.text(data.string.e02);
			break;

			case 1 :
			$teller.text(data.string.e03);
			$draggableObj.find('img').attr('src',$ref+'/images/electromagnet/cell.png');
			$draggableObj.fadeIn();
			$nextBtn.fadeOut();
			break;

			case 2 :
			$spaceForPic.attr('src',$ref+'/images/electromagnet/wire_5.gif');
			$teller.text(data.string.e05);
			break;

			case 3 :
			$teller.text(data.string.conclusionCoil);
			break;

			case 4 :
			$teller.text(data.string.e06);
			$draggableObj.find('img').attr('src',$ref+'/images/electromagnet/cell_big.png');
			$draggableObj.fadeIn();
			$nextBtn.fadeOut();
			break;

			case 5 :
			$teller.text(data.string.e07);
			$nextBtn.fadeOut(ole.nextBlinker);
			break;

		}

		countNext++;

		loadTimelineProgress(7,countNext+1); 
		if (countNext+1>=7) {
			ole.footerNotificationHandler.pageEndSetNotification();
		};
	})

})(jQuery);
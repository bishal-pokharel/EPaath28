(function ($) {
	loadTimelineProgress(1,1); 
	var countNext = 0;
	var $destroyBoard=$('.destroyBoard');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countDragFirst = 0;
	var $popUp = $('.magnet .popUp');
	var click1 = 0, click2 = 0, click3 = 0;

	$('.title').text(data.string.destroyTitle01);
	$('.useDesc').text("");

	$('.title').css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {
		first();
	});

	/**
	* first function to call
	*/
	var first = function () {
		var source = $('#first-template').html();
		console.log(source);
		var template = Handlebars.compile(source);
		var content = {
			type1 : data.string.destroy_1,
			type2 : data.string.destroy_2,
			type3 : data.string.destroy_3,
			desc : data.string.destroy_desc
		}
		var html = template(content);
		$destroyBoard.html(html);
		$destroyBoard.find('.typesTitle').fadeIn();
	}

	var clickFire = function (){
		var timestamp = new Date().getTime();
		var source = $('#singlePop-template').html();
		var template = Handlebars.compile(source);
		var destroyFire = data.string.destroying;
		var changed = destroyFire.replace(" 0", "<sup>o</sup>");
		var content = {
			title : data.string.destroy_1,
			image : $ref+"/images/destroy/distroying.gif?"+timestamp,
			popDesc : changed
		}
		var html = template(content);
		$popUp.html(html);
		$popUp.show(0);
		
	}

	var clickForce = function (){
		var timestamp = new Date().getTime();
		var source = $('#doublePop-template').html();
		var template = Handlebars.compile(source);
		var content = {
			title : data.string.destroy_2,
			image1 : $ref+"/images/destroy/distroying1.gif?"+timestamp,
			image2 : $ref+"/images/destroy/distroying2.gif?"+timestamp,
			popDesc : data.string.destroying1
		}
		var html = template(content);
		$popUp.html(html);
		$popUp.show(0);
	}

	var clickElectroD = function (){
		var timestamp = new Date().getTime();
		var source = $('#singlePop2-template').html();
		var template = Handlebars.compile(source);
		var content = {
			title :  data.string.destroy_3,
			image : $ref+"/images/destroy/distroying3.png?"+timestamp,
			popDesc : data.string.destroying3
		}
		var html = template(content);
		$popUp.html(html);
		$popUp.show(0);
		
	}

	$destroyBoard.on('click','.typesTitle',function(){
		$this = $(this);
		var $val = $this.data('value');
		
		// $destroyBoard.find('.desc').fadeOut();

		switch ($val){
			case "type1":
			click1 = 1;
			clickFire();
			break;

			case "type2":
			clickForce();
			click2 = 1;
			break;

			case "type3":
			click3 = 1;
			clickElectroD();
			break;
		}

	})

	$popUp.on('click','.closeBtn',function () {
		
		$popUp.hide(0);

		if (click1===1 && click2===1 && click3===1) {
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	})


})(jQuery);
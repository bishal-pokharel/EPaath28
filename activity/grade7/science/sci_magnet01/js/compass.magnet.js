(function ($) {

	loadTimelineProgress(3,1); 
	var $wahtar=getSubpageMoveButton($lang,"next");

	$("#activity-page-next-btn-enabled").html($wahtar);
	
	var countNext = 0;
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $compassBox = $('.compassBox');
	var $makerImg ;
	$('.title').text(data.string.compassTitle);
	$('.title').css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {
		first();
	});

	function first () {
		var source = $("#compass-template").html();
		var template = Handlebars.compile(source);
		var content = {
			cork : $ref+"/images/direction/cork.png",
			needle : $ref+"/images/direction/needle.png",
			water : $ref+"/images/direction/bucket.png",
			bucket : $ref+"/images/direction/bucket_1.png",
			desc : data.string.compassInst1,
			maker : "mmm"
		}
		
		$compassBox.fadeOut(function () {
			var html = template(content);
			$compassBox.html(html).fadeIn();
			$makerImg = $compassBox.find('.maker img');
			$compassBox.find('#bucket').draggable({
				drag : function (event,ui) {
				$(this).css("border","none")
			}
			});
			droppables();
		});	

		
	}

	function droppables () {
		$compassBox.find('.maker').droppable({
	 	
			drop: function( event, ui ) {
				// alert('smth');
				switch (countNext) {
					case 0:
						$makerImg.attr('src',$ref+'/images/direction/buckets1.png').show(0);
						$compassBox.find('#bucket').hide(0);
						// $nextBtn.fadeIn();
						second();
						countNext++;
					break;

					case 1:
						$makerImg.attr('src',$ref+'/images/direction/buckets2.gif').show(0);
						$compassBox.find('#water').hide(0);
						$nextBtn.fadeIn();
					break;

					case 2:
						$makerImg.attr('src',$ref+'/images/direction/buckets3.png');
						$compassBox.find('#cork').hide(0);
						// $nextBtn.fadeIn();
						forth();
						countNext++;
					break;

					case 3:
						$makerImg.attr('src',$ref+'/images/direction/buckets3.gif');
						$compassBox.find('#needle').hide(0);
						// $nextBtn.fadeIn();
						fifth();
						countNext++;
					break;

				}
			}
		});

	}

	function second () {

		$compassBox.find(".instruction").text(data.string.putWater)
		$compassBox.find('#water').draggable({
			drag : function (event,ui) {
				$(this).css({
					"border" : "none",
					"transform" : "rotate(-20deg)"
				})
			}
		});
	}

	function third () {
		$compassBox.find(".instruction").text(data.string.putCork)
		$makerImg.attr('src',$ref+'/images/direction/buckets2.png');
		$compassBox.find('#cork').draggable({
			drag : function (event,ui) {
				$(this).css("border","none")
			}
		});
	}

	function forth () {
		$compassBox.find(".instruction").text(data.string.putNeedle)
		$compassBox.find('#needle').draggable({
			drag : function (event,ui) {
				$(this).css("border","none")
			}
		});
	}

	function fifth () {
		$compassBox.find(".instruction").text(data.string.needleInst);
		$nextBtn.fadeIn();
	}

	function sixth () {
		$compassBox.find(".lower").html(data.string.needleConclusion);
		ole.nextBlinker();
	}


	 
	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		switch (countNext) {
			case 0:
			second();
			break;

			case 1:
			third();
			break;

			case 2:
			forth();
			break;

			case 3:
			fifth();
			break;

			case 4:
			sixth();
			break;

		}

		countNext++;

		loadTimelineProgress(3,countNext); 
		if (countNext>=3) {
			ole.footerNotificationHandler.pageEndSetNotification();
		};
	})


})(jQuery);
(function ($) {

	var $wahtar=getSubpageMoveButton($lang,"next");
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		 quest++;
		 $(this).fadeOut(10);
		 $("#activity-page-prev-btn-enabled").fadeOut(10);
		 getFunction(quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		 quest--;
		 $(this).fadeOut(10);
		 $("#activity-page-next-btn-enabled").fadeOut(10);
		 getFunction(quest);
	 });

	 function getFunction(qno){
			loadTimelineProgress(2,qno);
			switch(qno)
			{
					case 1:
						case1();
						break;
					case 2:
						case2();
						break;
			}
		}

		function case1(){

			  var datavar={
		  		easyTitle: data.lesson.chapter,
		  		coverimg:$ref+"/images/magnet.png"
		  	}
		  	var source   = $("#front-template").html();

		  	var template = Handlebars.compile(source);

		  	var html=template(datavar);
		  	$("#firstpage").html(html);
		  	$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
		}

		function case2(){
			$("#firstpage").hide(0);
			$('.title').text(data.string.whyTitle);
			$('.useDesc').text("");

			$('.title').css({
				"top" : "30%"
			}).delay(400).fadeIn(400).delay(1000).animate({
				"top" : "0%"
			},200,function () {
				$('.useDesc').delay(800).fadeIn(100,function () {
					// ole.stringShow('.useDesc',data.string.descWhy,function () {
					// 	ole.nextBlinker();
					// });
					$('.useDesc').text(data.string.descWhy).fadeIn(600);
					$('#radio').delay(2000).fadeIn();
					$('#tv').delay(2500).fadeIn();
					$('#atm').delay(3000).fadeIn();
					$('#compass').delay(3500).fadeIn();
					$('#speaker').delay(4000).fadeIn(function () {
						ole.footerNotificationHandler.pageEndSetNotification();
					});
					// $('#radio').delay(5000).fadeIn();
				});
			});
		}

})(jQuery);

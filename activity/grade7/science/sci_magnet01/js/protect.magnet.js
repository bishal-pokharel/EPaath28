(function ($) {
	
	var countNext = 0;
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $storyBox = $('.storyBox');
	var $protectBox = $('.protectBox');

	var $wahtar=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($wahtar);
	
	loadTimelineProgress(10,1);


	$('.title').text(data.string.protectTitle);
	$('.title').css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {
		first();
	});

	function first () {
		var source = $('#slide1-template').html();
		var template = Handlebars.compile(source);
		var content = {
			wrapperClass : "first",
			text1 : data.string.protect1_1 ,
			image : $ref+"/images/protect/barmagnets.png",
			text2 : data.string.protect1_2
		}

		var html = template(content);
		$protectBox.html(html);
		
		$protectBox.delay(800).fadeIn();
		// $protectBox.find('.textDiv2').hide(0);
		$protectBox.find('.fig').css('margin-left',"200%").animate({
			"margin-left" : "10%"
		},2000,function () {
			$protectBox.find('.fig').delay(1000).animate({
				"margin-left" : "0%"
			},1000,function () {
				$protectBox.find('.textDiv1').animate({opacity: 1}, 1000);
				$nextBtn.fadeIn();
			});
			
		});
	}

	function second () {
		var source = $('#style2-template').html();
		var template = Handlebars.compile(source);
		var content = {
			wrapperID : "second",
			textAbsolute : data.string.protect2_2,
			image : $ref+"/images/protect/lines.png",
			text2 : data.string.protect2_1
		}
		var html = template(content);

		$protectBox.fadeOut(function () {
			$protectBox.html(html);
			$protectBox.fadeIn(function () {
				$nextBtn.delay(500).fadeIn();
			})	
		})
	}

	function second_next () {
		$protectBox.find('#second .textDiv').append("<br>"+data.string.protect2_3);
		
		$nextBtn.delay(500).fadeIn();
	}

	function second_next2 () {
		$protectBox.find('#second .textDiv').append("<br>"+data.string.protectWatch);
		
		$nextBtn.delay(500).fadeIn();
	}

	function third () {
		var source = $('#style2-template').html();
		var template = Handlebars.compile(source);
		var content = {
			wrapperID : "third",
			textAbsolute : data.string.protect3_1,
			image : $ref+"/images/protect/01.gif",
			text2 : data.string.protect3_2
		}
		var html = template(content);

		$protectBox.fadeOut(function () {
			$protectBox.html(html);
			$protectBox.fadeIn(function () {
				$nextBtn.delay(1000).fadeIn();
			})	
		})
	}

	function forth () {
		var source = $('#style2-template').html();
		var template = Handlebars.compile(source);
		var content = {
			wrapperID : "forth",
			textAbsolute : data.string.protect4_1,
			image : $ref+"/images/protect/wood.gif",
			text2 : data.string.protect4_2
		}
		var html = template(content);
		console.log(html);
		$protectBox.fadeOut(function () {
			$protectBox.html(html);
			$protectBox.fadeIn(function () {
				$nextBtn.delay(1000).fadeIn();
			})	
		})
	}

	function fifth () {
		var source = $('#style2-template').html();
		var template = Handlebars.compile(source);
		var content = {
			wrapperID : "fifth",
			textAbsolute : data.string.protect5_1,
			image : $ref+"/images/protect/02.gif",
			text2 : data.string.protect5_2
		}
		var html = template(content);
		console.log(html);
		$protectBox.fadeOut(function () {
			$protectBox.html(html);
			$protectBox.fadeIn(function () {
				$nextBtn.delay(1000).fadeIn();
			})
		})
	}

	function sixth () {
		var source = $('#style2-template').html();
		var template = Handlebars.compile(source);
		var content = {
			wrapperID : "sixth",
			textAbsolute : data.string.protect6_1,
			image : $ref+"/images/protect/05.gif",
			text2 : data.string.protect6_2
		}
		var html = template(content);
		console.log(html);
		$protectBox.fadeOut(function () {
			$protectBox.html(html);
			$protectBox.fadeIn(function () {
				$nextBtn.delay(1000).fadeIn();
			})	
		})
	}

	function seventh () {
		$protectBox.find('#sixth .textDiv').html("<br>"+data.string.protect6_3);
		$nextBtn.delay(1000).fadeIn();
	}

	function ushaped () {
		var source = $('#protect-template').html();
		var template = Handlebars.compile(source);
		var content = {
			desc : data.string.protectUshaped,
			newId : "ushaped",
			image : $ref+"/images/ushapeprotect.png"
		}

		var html = template(content);
		console.log(html);
		
		$protectBox.fadeOut(function () {

			$protectBox.html(html);
			$protectBox.fadeIn();
		})
	}

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		switch (countNext) {
			case 0:
			second();
			break;

			case 1:
			second_next();
			break;

			case 2:
			second_next2();
			break;

			case 3:
			third();
			break;

			case 4:
			forth();
			break;

			case 5:
			fifth();
			break;

			case 6:
			sixth();
			break;

			case 7:
			seventh();
			break;

			case 8:
			ushaped();
			break;
			
		}

		countNext++;

		loadTimelineProgress(10,countNext+1);
		if (countNext+1>=10) {
			ole.footerNotificationHandler.lessonEndSetNotification();
		};
	})
})(jQuery);
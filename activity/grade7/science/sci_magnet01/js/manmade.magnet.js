(function ($) {

	var $permanent = $('#permanent');
	var $temporary = $('#temporary');
	var $popUp = $('.magnet .popUp');
	var clickPermanent = 0;
	var clickTemporary = 0;

	loadTimelineProgress(1,1); 
	
	$('.title').text(data.string.manMadeTitle);
	$permanent.find('span').text(data.string.permanent);
	$temporary.find('span').text(data.string.temporary)

	function temporary () {
		setTimeout(function () {
			$temporary.find('img').attr('src',$ref+"/images/gif/paper_pin.gif").fadeIn();
			$temporary.find('span').fadeIn();
		},1600);
	}

	$('.title').css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {

		$permanent.find('img').attr('src',$ref+"/images/gif/magnetattract.gif").fadeIn(function () {
		$permanent.find('span').delay(800).fadeIn(function () {
				temporary();
			});
		});
			
	});

	$permanent.on('click',function () {
		var magnetData = {
			title : data.string.permanent,
			defination : data.string.permanentDesc
		};
		var source = $('#popup-template').html();
		var template = Handlebars.compile(source);
		var html    = template(magnetData);
		$popUp.html(html);
		$popUp.show(0);
		clickPermanent =1;
	})

	$temporary.on('click',function () {
		var magnetData = {
			title : data.string.temporary,
			defination : data.string.temporaryDesc
		};
		var source = $('#popup-template').html();
		var template = Handlebars.compile(source);
		var html    = template(magnetData);
		$popUp.html(html);
		$popUp.show(0);
		clickTemporary =1;
	})

	$popUp.on('click','.closeBtn',function () {

		if (clickPermanent ===1 && clickTemporary ===1 ) {
			ole.footerNotificationHandler.pageEndSetNotification();
		}
		$popUp.hide(0);
	})

})(jQuery);
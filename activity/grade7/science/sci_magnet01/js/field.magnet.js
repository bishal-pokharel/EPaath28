(function ($) {
	var $wahtar=getSubpageMoveButton($lang,"next");

	$("#activity-page-next-btn-enabled").html($wahtar);
	loadTimelineProgress(5,1); 
	var countNext = 0;
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $storyBox = $('.storyBox');
	var $fieldBox = $('.fieldBox');
	$('.title').text(data.string.fieldTitle);
	$('.title').css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {
		first();
	});

	function first () {
		$('.title').text(data.string.fieldTitle);
		var source = $("#field-template").html();
		var template = Handlebars.compile(source);
		var content = {
			getId : "intro",
			desc : data.string.fieldOwn,
			image : $ref+"/images/1.png"
		}
		var html = template(content);
		$fieldBox.html(html).show(0);
		$nextBtn.delay(1000).fadeIn();
	}

	function second () {
		$('.title').html(data.string.fieldTitle2);
		var source = $("#field-template").html();
		var template = Handlebars.compile(source);
		var content = {
			getId : "fieldDisplay",
			desc : data.string.fieldDirection,
			image : $ref+"/images/gif/magnetic_field.gif"
		}
		
		$fieldBox.fadeOut(function () {
			var html = template(content);
			$fieldBox.html(html).fadeIn(function () {
				$fieldBox.find('#fieldDisplay .desc').delay(1500).fadeIn();
			})
		});
	}

	function third () {
		var source = $("#field-template").html();
		var template = Handlebars.compile(source);
		var content = {
			getId : "fieldMore",
			desc : data.string.fieldMore,
			image : $ref+"/images/gif/magnetic_field.gif"
		}
			var html = template(content);
			$fieldBox.html(html).fadeIn(function () {
				$fieldBox.find('#fieldDisplay .desc').fadeIn();
			});
	}

	function forth () {
		var source = $("#field-template").html();
		var template = Handlebars.compile(source);
		var content = {
			getId : "fieldDust",
			desc : data.string.fieldDust,
			image : $ref+"/images/irondust.png"
		}
		
		$fieldBox.fadeOut(function () {
			var html = template(content);
			$fieldBox.html(html).fadeIn(function () {
				$fieldBox.find('#fieldDisplay .desc').fadeIn();
			})
		});
	}

	function fifth () {
		var source = $("#power-template").html();
		var template = Handlebars.compile(source);
		var content = {
			getId : "fieldPower",
			desc : data.string.fieldPower,
			image1 : $ref+"/images/gif/magnetic_field.gif",
			image2 : $ref+"/images/irondust.png"
		}
		
		$fieldBox.fadeOut(function () {
			var html = template(content);
			$fieldBox.html(html).fadeIn(function () {
				$fieldBox.find('#fieldDisplay .desc').fadeIn();
			})
		});	
	}

	function sixth () {
		$fieldBox.find('#fieldPower .desc').text(data.string.fieldPowerful);
		$nextBtn.fadeOut(ole.nextBlinker);
	}
	$nextBtn.on('click',function () {
		switch (countNext) {
			case 0:
			second();
			break;

			case 1:
			forth();
			break;

			case 2:
			fifth();
			break;

			case 3:
			sixth();
			break;
		}

		countNext++;

		loadTimelineProgress(5,countNext+1); 
		if (countNext+1>=5) {
			ole.footerNotificationHandler.pageEndSetNotification();
		};
	})
})(jQuery);
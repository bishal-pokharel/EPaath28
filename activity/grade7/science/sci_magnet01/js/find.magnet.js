(function ($) {

	loadTimelineProgress(1,1); 


	$('.title').text(data.string.find);
	$('#natural').find('span').text(data.string.natural);
	$('#manmade').find('span').text(data.string.manmade);

	var $natural =$('#natural');
	var $manMade=$('#manmade');
	var $popUp = $('.magnet .popUp');
	var clickNatural = 0, clickManMade = 0;

	$natural.on('click',function () {
		var magnetData = {
			title : data.string.natural,
			images : $ref+"/images/naturepop.png",
			defination : data.string.natureDefn
		};
		var source = $('#popup-template').html();
		var template = Handlebars.compile(source);
		var html    = template(magnetData);
		$popUp.html(html);
		$popUp.show(0);
		clickNatural = 1;
	})

	$manMade.on('click',function () {
		var magnetData = {
			title : data.string.manmade,
			images : $ref+"/images/magnets.png",
			defination : data.string.manMadeDefn
		};
		var source = $('#popup-template').html();
		var template = Handlebars.compile(source);
		var html    = template(magnetData);
		$popUp.html(html);
		$popUp.show(0);
		clickManMade =1;
	})

	$popUp.on('click','.closeBtn',function () {
		if (clickManMade ===1 && clickNatural ===1 ) {
			ole.footerNotificationHandler.pageEndSetNotification();
		}
		$popUp.hide(0);
	})

})(jQuery);
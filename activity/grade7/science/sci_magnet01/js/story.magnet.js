(function ($) {
	
	var countNext = 0;
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $storyBox = $('.storyBox');

	var $wahtar=getSubpageMoveButton($lang,"next");

	$("#activity-page-next-btn-enabled").html($wahtar);
	
	loadTimelineProgress(9,1); 

	$('.title').text(data.string.storyTitle);
	$('.title').css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {
		// alert(data.string.storyFirst)
		$storyBox.html("<div class='intro'>"+data.string.storyFirst+"</div>");
		$storyBox.delay(400).fadeIn();
		$nextBtn.delay(1500).fadeIn();

	});

	var first = function () {
		$('.title').text(data.string.storyTitle2);
		var source  = $('#pirate-talk').html();
		var template = Handlebars.compile(source);
		var content = {
			say : data.string.story1,
			image : $ref+"/images/pirate.png"
		}
		var html = template(content);
		$storyBox.html(html).delay(400).fadeIn();
		$nextBtn.delay(1500).fadeIn();
	}

	function second () {
		var source  = $('#pirate-fall').html();
			var template = Handlebars.compile(source);
			var content = {
				newClass : "pirateFall",
				say : data.string.storyFrighten,
				image : $ref+"/images/gif/boat1.gif",
				discription : data.string.storyFrightenBtn
			}
			var html = template(content);
			$storyBox.html(html).delay(400).fadeIn();
			setTimeout(function () {
				$storyBox.find('.say2').show(0);
			},10000)
	}

	function third () {
		var source  = $('#pirate-fall').html();
			var template = Handlebars.compile(source);
			var content = {
				newClass : "findBoat",
				say : data.string.storySurprise,
				image : $ref+"/images/gif/boat2.png"
				
			}
			var html = template(content);
			$storyBox.fadeOut(function () {
				$storyBox.html(html).delay(400).fadeIn();
				setTimeout(function () {
					$storyBox.find('.findBoat .say2').show(0);
				},1000)
			});
			
	}

	function forth () {
		var source  = $('#pirate-fall').html();
		var template = Handlebars.compile(source);
		var content = {
			newClass : "zoomBoat",
			say : data.string.storyDesc,
			image : $ref+"/images/gif/boat3.png"
		}
		var html = template(content);
		$storyBox.fadeOut(function () {
			$storyBox.html(html).delay(400).fadeIn();
			setTimeout(function () {
				$storyBox.find('.say2').show(0);
			},1000)
		});
	}

	function forth_0 () {
		$storyBox.find(".zoomBoat .say2").text(data.string.storyDesc1);
	}

	function forth_1 () {
		$storyBox.find(".zoomBoat .say2").text(data.string.storyDesc2);
	}

	function forth_2 () {
		$storyBox.find(".zoomBoat .say2").text(data.string.storyDesc3);
	}

	function forth_3 () {
		$storyBox.find(".zoomBoat .say2").text(data.string.storyDesc4);
		$nextBtn.fadeOut();
	}

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		switch (countNext) {
			case 0:
			first();
			$nextBtn.fadeIn();
			break;

			case 1:
			second()
			setTimeout(function () {
				$nextBtn.fadeIn()
			},10000);
			break;

			case 2:
			third();
			setTimeout(function () {
				$nextBtn.fadeIn()
			},5000);
			break;

			case 3:
			forth();
			setTimeout(function () {
				$nextBtn.fadeIn()
			},1000);
			break;

			case 4:
			forth_0();
				setTimeout(function () {
				$nextBtn.fadeIn()
			},1000);
			break;

			case 5:
			forth_1();
				setTimeout(function () {
				$nextBtn.fadeIn()
			},1000);
			break;
			
			case 6:
			forth_2();	setTimeout(function () {
				$nextBtn.fadeIn()
			},1000);
			break;

			case 7:
			forth_3();
			ole.nextBlinker();
			break;
		}

		countNext++;


		loadTimelineProgress(9,countNext+1); 
		if (countNext+1>=9) {
			ole.footerNotificationHandler.pageEndSetNotification();
		};
	});


})(jQuery);
(function ($) {
	
	var countNext = 0;
	var $makerBoard=$('.makerBoard');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countDragFirst = 0;

	var $wahtar=getSubpageMoveButton($lang,"next");

	$("#activity-page-next-btn-enabled").html($wahtar);

	loadTimelineProgress(5,1); 


	$('.title').text(data.string.makingTitle01);
	$('.useDesc').text("");

	$('.title').css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {
		first();
	});

	/**
	* first = fuction for first work or animation or whatever :P
	*/
	var first = function () {
		countDragFirst = 0;
		var source = $('#not-template').html();
		var  template = Handlebars.compile(source);
		var content = {
			rod : $ref+"/images/makingmagnet02/rod1.png",
			pin : $ref+"/images/makingmagnet02/nails.png",
			description : data.string.i01_01
		};
		var html = template(content);
		$makerBoard.html(html);
		$makerBoard.find("#notmagnetised .rod").draggable({
			drag:function () {
				setTimeout(function () {
					if (countDragFirst == 0) {
						$nextBtn.show(0);
						$makerBoard.find('.desc').text(data.string.i01_02);	
					};
					countDragFirst ++;
				},3000);
			}
		});
		countNext = 0;
	}

	var second = function () {
		var source = $('#process-template').html();
		var  template = Handlebars.compile(source);
		var content = {
			image : $ref+"/images/makingmagnet02/rod2.png",
			id : "rod",
			description : data.string.i02
		};
		var html = template(content);
		$makerBoard.html(html);
		// countNext = 1;
	}

	var third = function () {
		var source = $('#process-template').html();
		var  template = Handlebars.compile(source);
		var content = {
			image : $ref+"/images/makingmagnet02/process.gif",
			id : "process",
			description : data.string.i03
		};
		var html = template(content);
		$makerBoard.html(html);
		// countNext = 2;
	}

	var fourth = function (){
		var source = $('#finish-template').html();
		var  template = Handlebars.compile(source);
		var content = {
			rod : $ref+"/images/makingmagnet02/rod1.png",
			pin : $ref+"/images/makingmagnet02/nails.png",
			description : data.string.i04
		};
		var html = template(content);
		$makerBoard.html(html);
		$makerBoard.find("#magnetised .rod").draggable({
			drag: function( event, ui ) {
				if (collision($('.makerBoard').find('#magnetised .rod'),$('.makerBoard').find('#magnetised .pinHolder'))=== true) {
					attrackImg();
					ole.footerNotificationHandler.pageEndSetNotification();
				};
			}
		});
	}

	function attrackImg() {
		var source = $('#process-template').html();
		var  template = Handlebars.compile(source);
		var content = {
			image : $ref+"/images/makingmagnet02/attract.gif",
			id : "attract",
			description : data.string.i05
		};
		var html = template(content);
		$makerBoard.html(html);
	}

	function collision($div1, $div2) {
		var x1 = $div1.offset().left;
		var y1 = $div1.offset().top;
		var h1 = $div1.outerHeight(true);
		var w1 = $div1.outerWidth(true);
		var b1 = y1 + h1;
		var r1 = x1 + w1;
		var x2 = $div2.offset().left;
		var y2 = $div2.offset().top;
		var h2 = $div2.outerHeight(true);
		var w2 = $div2.outerWidth(true);
		var b2 = y2 + h2;
		var r2 = x2 + w2;

		if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
		return true;
	}

	$("#activity-page-next-btn-enabled").on('click',function () {
		// alert(countNext);
		$desc = $makerBoard.find('.desc');
		switch (countNext) {
			case 0:
			second();
			break;

			case 1:
			third();
			break;

			case 2:
			$makerBoard.find('.desc').text(data.string.i0dont);
			break;

			case 3:
			fourth();
			$nextBtn.fadeOut();
			// ole.nextBlinker();
			break;
		}

		countNext++;

		loadTimelineProgress(5,countNext+1); 
		if (countNext+1>=5) {
			// ole.footerNotificationHandler.pageEndSetNotification();
		};
	})

})(jQuery);
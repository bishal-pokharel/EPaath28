// (function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
		countNext = 0,
		$total_page = 31,
		countQ = 0,
		$refImg = $ref+"/images/wind/";
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.show(0);
	if(countNext==30){
		$nextBtn.hide(0);
		ole.footerNotificationHandler.pageEndSetNotification();
	}
	var tree1 = $refImg+"tree01.png",
		tree2 = $refImg+"tree02.png";

	var glowDown=false, stopStroke = false;

	var canvas2;
	var canvas1,width, height;
	var column = 1000,rows;
	var earthSize,quadrant1,quadrant2,mid,earthCol,earthSurfacePos,earthNMid; //for earth and sun
	var circles = [],
		numCol = 20,
		numRow = 15,
		goingUp = 80, //percentage going up
		goingUpArray =[],
		coldMove = 0,
		r= 3,
		disCol,disRow,warmColorStart;

	var circleArrays = function  () {
		disCol = column/numCol;
		disRow = rows/numRow;
		warmColorStart = Math.floor(80/100*numRow);
		var circlePath = mVas.getPath.circle(15,0,360);
		coldMove = numRow-2*(numRow-warmColorStart);
		// console.log("diff = "+diffx3);
		for (var i = 0; i < numRow; i++) {
			circles[i]=[];
			for (var j = 0; j < numCol; j++) {
				var rdm = Math.floor((Math.random() * 360));
				var point = circlePath[rdm];
				circles[i][j]={x:point.x,y:point.y};
			};
		};

		var totalAtomsHot = (numRow-warmColorStart)*disCol;
		var numgoingUp = goingUp/100*totalAtomsHot;
		goingUpArray = ole.getRandom(numgoingUp,totalAtomsHot,0);
		console.log(totalAtomsHot+" "+goingUpArray.length);
	}


/*
* template loading starts
*/

	function oneWord (word,selector) {
		var source = $("#oneWord-template").html();
		var template = Handlebars.compile(source);
		var content={
			textClass : selector,
			text : word,
			imgArrow : $refImg+"arrow.png",
		}
		var html = template(content);
		$board.find(".others2").html(html);
		$nextBtn.show(0);
	}

	function canvasloader () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var content = {
			imgMM : $refImg+"globe.png",
			diyImg : "images/diy/diy1.png",
			diyTextData : data.string.diy,
			tree : tree1,
			sunPic : $refImg+"sun.png"
		};
		var html = template(content);
		$board.html(html);
		canvas1 = mVas('canvas1',"canvasHolder");
		canvas2 = mVas('animationCanvas',"canvasHolder");
		canvas1Drawing();
		intro ();
		$nextBtn.show(0);
	};

	canvasloader();

	function canvas1Drawing () {
		canvas1.gridColumn(column);
		canvas2.gridColumn(column);
		rows = canvas1.getGridRows();
		circleArrays();
	};

	function drawCircles () {
		$board.find('.tree').attr('src',tree2);
		for (var i = 0; i < numRow; i++) {

			for (var j = 0; j < numCol; j++) {
				color = "#00C9DB";
				stroke = "#70E2EA";

				var thecircle = circles[i][j];
				var objCircle = mVas.circle({
					x:thecircle.x,y:thecircle.y,
					fillStyle:color,
					radius : r,
					strokeStyle : stroke});
				 canvas1.add(objCircle,j*disCol+50,i*disRow);
			};
		};
		canvas1.draw();
		$nextBtn.show(0);
	}

	function drawCircles2 () {
		$board.find('.sun').show(0);
		canvas1.clearStaticDraw();
		for (var i = 0; i < numRow; i++) {

			for (var j = 0; j < numCol; j++) {
				// if (i<()) {};
				if (i<warmColorStart) {
					color = "#00C9DB";
					stroke = "#70E2EA";
				}  else {
					stroke = "#ED8B2A";
					color = "#FAD160";
					break;
				}
				var thecircle = circles[i][j];
				var objCircle = mVas.circle({
					x:thecircle.x,y:thecircle.y,
					fillStyle:color,
					radius : r,
					strokeStyle : stroke});
				 canvas1.add(objCircle,j*disCol+50,i*disRow);
			};
		};
		canvas1.draw();
		glowStroke();
	};

	function glowStroke() {
		var counting = 1;
		var red = 84, green = 181, blue = 213;

		function glowCircles2 () {
			if (stopStroke) {

			} else {
				canvas2.clearStaticDraw();
				color = "rgb("+Math.floor(red)+","+Math.floor(green)+","+Math.floor(blue)+")";

				if (counting%2===0) {
					stroke = "rgb(249, 164, 31)";
				} else {
					stroke = "rgb(99, 189, 216)";
				}


				for (var i = 0; i < numRow; i++) {

					for (var j = 0; j < numCol; j++) {
							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : r,
								strokeStyle : stroke,
								lineWidth : 2
							});
							canvas2.add(objCircle,j*disCol+50,i*disRow);
						// }
					};
				};
				canvas2.draw();
				counting +=1;
				setTimeout(glowCircles2,1000);
			}
		};

		glowCircles2();
	}

	function glowCircles() {
		var hugeVal=r,hugeCircle=7,iterationChange=0.3;
		totalSteps = (hugeCircle-r)/iterationChange;
		var addFactors = {
			red : (252-82)/totalSteps,
			green : (237-181)/totalSteps,
			blue : (94-213)/totalSteps
		}
		var red = 84, green = 181, blue = 213;

		function glowCircles2 () {
			if (glowDown) {
			} else {
				canvas2.clearStaticDraw();
				color = "rgb("+Math.floor(red)+","+Math.floor(green)+","+Math.floor(blue)+")";
				red+=addFactors.red;
				green+=addFactors.green;
				blue+=addFactors.blue;

				if (hugeVal>(hugeCircle-(hugeCircle-r)/2)) {
					stroke = "rgb(249, 164, 31)";
				} else {
					stroke = "rgb(99, 189, 216)";
				}


				for (var i = 0; i < numRow; i++) {

					for (var j = 0; j < numCol; j++) {
						if (i<warmColorStart) {
						}  else {

							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : hugeVal,
								strokeStyle : stroke,
								lineWidth : 2
							});
							canvas2.add(objCircle,j*disCol+50,i*disRow);
						}
					};
				};
				canvas2.draw();
				hugeVal +=iterationChange;
				if (hugeVal>hugeCircle) {
					hugeVal = r;
					red = 84, green = 181, blue = 213;
					setTimeout(glowCircles2,1000);
				} else {
					setTimeout(glowCircles2,100);
				}
			}
		};

		glowCircles2();
	}

	var goUpCount = 0, upOffset =0;
	var counetMoveUp=0, upOffset=0;
	function moveUp () {
		if (counetMoveUp>3) {
			console.log("the count end "+counetMoveUp);
			$board.find(".others .warmerGoesUp .text1,.others .warmerGoesUp .text2").show(0);
			$nextBtn.show(0);
		} else {
			goUpCount=0;
			console.log("the count is "+counetMoveUp);
			canvas2.clearStaticDraw();
			for (var i = 0; i < numRow; i++) {

				for (var j = 0; j < numCol; j++) {
					if (i<warmColorStart) {
					} else {
						// alert("here i m");
						stroke = "#ED8B2A";
						color = "#FAD160";
						if (goingUpArray.indexOf(goUpCount) > -1) {
							upOffset = counetMoveUp;
							console.log("in goingUp "+upOffset);
							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : 5,
								strokeStyle : stroke});
							canvas2.add(objCircle,j*disCol+50,(i-upOffset)*disRow);
						} else {
							upOffset =0;
						}
						goUpCount++;
					}
				};
			};
			counetMoveUp+=0.1;
			canvas2.draw();
			setTimeout(moveUp,100);
		}
	}

	function drawCircles4 () {
		canvas1.clearStaticDraw();
		var sizeSet = 0;
		for (var i = 0; i < numRow; i++) {
			for (var j = 0; j < numCol; j++) {
				if (i<warmColorStart) {
					color = "#00C9DB";
					stroke = "#70E2EA";

					if (i>coldMove) {
						upOffset = -100; //goes downwards
						break;
					} else {
						upOffset =0;
					}

					sizeSet=0;

				} else {
					stroke = "#ED8B2A";
					color = "#FAD160";
					if (goingUpArray.indexOf(goUpCount) > -1) {
						upOffset = 100;
						sizeSet = 2;
						console.log("in goingUp");
					} else {
						upOffset =0;
						sizeSet=0;
					}
					goUpCount++;
				}
				// console.log("in goingUp try "+upOffset);
				var thecircle = circles[i][j];
				var objCircle = mVas.circle({
					x:thecircle.x,y:thecircle.y,
					fillStyle:color,
					radius : r+sizeSet,
					strokeStyle : stroke});
				 canvas1.add(objCircle,j*disCol+50,i*disRow-upOffset);
			};
		};
		$nextBtn.hide(0);
		canvas1.draw();
		moveDown();
	};

	var counetMoveDown = 0, upOffset=0;
	function moveDown () {
		if (counetMoveDown>3) {
			console.log("the count end "+counetMoveUp);
			// $board.find(".others .coldComesDown .text1").show(0);
			$nextBtn.show(0);
		} else {
			canvas2.clearStaticDraw();
			for (var i = 0; i < numRow; i++) {
				for (var j = 0; j < numCol; j++) {
					if (i<warmColorStart) {
						color = "#00C9DB";
						stroke = "#70E2EA";
						if (i>coldMove) {
							// alert("m here "+upOffset);
							upOffset = counetMoveDown;
							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : 5,
								strokeStyle : stroke});
							canvas2.add(objCircle,j*disCol+50,(i+upOffset)*disRow);
						} else {
							upOffset =0;
						}

					} else {
						upOffset =0;
					}

				};
			};
			counetMoveDown+=0.1;
			canvas2.draw();
			setTimeout(moveDown,100);
		}
	};

	var setFinish=false;
	function fullDemo1 () {
		$board.find('.tree').attr('src',tree1);
		$nextBtn.show(0);
		canvas1.clearStaticDraw();
		canvas2.clearStaticDraw();
		var colorStore = {
			type1 : ["#00C9DB","#70E2EA"],
			type2 : ["#F7E085","#70E2EA"],
			type3 : ["#FFF301","#70E2EA"],
			type4 : ["#FB9902","#FAD160"],
		};
		var countLooper=0;
		var toggle = true;
		var color,stroke;
		var shift1 = 0,shift2 = 1;
		function loopAll () {

			if (setFinish) {
				console.log("the count end "+counetMoveUp);
				$nextBtn.show(0);
			} else {
				console.log("m here");
				canvas1.clearStaticDraw();
				canvas2.clearStaticDraw();

				for (var i = 0; i < numRow; i++) {
					for (var j = 0; j < numCol; j++) {
						var thecircle = circles[i][j];
						upOffset= countLooper;
						if (i>4) {
							if (i<8) {
								color = colorStore.type2[0];
								stroke = colorStore.type2[1];
							} else if (i<12) {
								color = colorStore.type3[0];
								stroke = colorStore.type3[1];
							} else {
								color = colorStore.type4[0];
								stroke = colorStore.type4[1];
							}

							var objCircle2 = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : r,
								strokeStyle : stroke});

							canvas2.add(objCircle2,(j+shift2)*disCol+shift1,(i-upOffset)*disRow);
						};
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:colorStore.type1[0],
								radius : r,
								strokeStyle : colorStore.type1[1]});
							canvas1.add(objCircle,(j+shift2)*disCol,(i-3+upOffset)*disRow);
					};
				};
				countLooper+=0.1;
				var halt = 100;
				if (countLooper>3) {
					countLooper=0;
					shift1 = 0;
					toggle = false;
					halt = 800;
				};
				canvas2.draw();
				canvas1.draw();
				setTimeout(loopAll,halt);
			}
		};
		loopAll();
	}



	$nextBtn.on('click',function () {
		countNext++;
		// $prevBtn.show(0); //show prev btn
		thenext();

	});


	$prevBtn.on('click',function () {
	// 	if (countNext<=10) {
	// 		countNext=0;
	// 	} else if (countNext<=18) {
	// 		countNext=10;
	// 	} else if (countNext<=25) {
	// 		countNext=18;
	// 	} else {
	// 		countNext=25;
	// 	}

	// 	//hide previous btn
	// 	if (countNext===0) {
	// 		$prevBtn.hide(0);
	// 	};
	// 	thenext();
	// });
	countNext--;
	// $prevBtn.show(0); //show prev btn
	thenext();
});
	function thenext () {
		console.log("countNext "+countNext);
		switch (countNext) {

			case 0:
				intro();
			break;

			case 1:
				descWind1();
			break;

			case 2:
				drawCircles();
				molecules ();
			break;

			case 3:
				drawCircles2();
				sunRadiation ();
			break;

			case 4:
				stopStroke=true;
				glowCircles();
				warmerLand();
			break;

			case 5:
				glowDown=true;
				$nextBtn.hide(0);
				moveUp();
				warmerGoesUp ();
			break;

			case 6:
				coldComesDown ()
			break;

			case 7:
				$board.find(".others .coldComesDown .text1").show(0);
			break;

			case 8:
				$board.find(".others .coldComesDown .text0").hide(0);
				$board.find(".others .coldComesDown .text2").hide(0);
				$board.find(".others .coldComesDown .text1").hide(0);
				$board.find(".others .coldComesDown .text3").hide(0);
				$board.find(".others .coldComesDown .text4").hide(0);
				drawCircles4();
			break;

			case 9:
				fullDemo1();
				completeCycle();
			break;

			case 10:
				breezeIntro ();
				setFinish=true;
				$board.find(".imgs").hide(0);
				$board.find('#canvasHolder').removeClass("hillbg");
			break;

			case 11:
				seaBreezeText ();
				$board.find('#canvasHolder').addClass('breezebg');
				landBreeze ();
			break;

			case 12:
				$board.find(".seaBreezeText .text1").hide(0);
				$board.find(".seaBreezeText .text2").show(0);
				seaBreeze ();
			break;

			case 13:
				$board.find(".seaBreezeText .text2").hide(0);
				$board.find(".seaBreezeText .text3").show(0);
			break;

			case 14:
				seaBreezeUp();
			break;

			case 15:
				$board.find(".seaBreezeText .text3").hide(0);
				$board.find(".seaBreezeText .text4").show(0);
			break;

			case 16:
				$board.find(".others .seaBreezeText .text6 ").show(0);
				$board.find(".seaBreezeText .text4").hide(0);
				$board.find(".seaBreezeText .text5").show(0);
				countNext++;
				seaBreezeSide();
			break;

			case 17:

			break;

			case 18 :
				landBreezeText ();
				$board.find('#canvasHolder').removeClass("breezebg").addClass('nightbreezebg');
				landBreeze ();
			break;

			case 19:
				$board.find(".landBreezeText .text1").hide(0);
				$board.find(".landBreezeText .text2").show(0);
				landBreeze2 ();
			break;

			case 20:
				$board.find(".landBreezeText .text2").hide(0);
				$board.find(".landBreezeText .text3").show(0);
			break;

			case 21:
				landBreezeUp();
			break;

			case 22:
				$board.find(".landBreezeText .text3").hide(0);
				$board.find(".landBreezeText .text4").show(0);
			break;

			case 23:
				$board.find(".others .landBreezeText .text6 ").show(0);
				$board.find(".landBreezeText .text4").hide(0);
				$board.find(".landBreezeText .text5").show(0);
				countNext++;
				landBreezeSide();
			break;

			case 24:

			break;

			case 25:
				canvas1.clearStaticDraw();
				canvas2.clearStaticDraw();
				$board.find(".others2").hide(0);
				$board.find('#canvasHolder').removeClass('nightbreezebg');
				globeWindIntro1();
			break;

			case 26:
				// setFinish=true;
				// $board.find(".imgs").hide(0);
				// $board.find('#canvasHolder').removeClass('nightbreezebg');
				// $board.find(".others").hide(0);

				earthAndSun();
				globeWindIntro ();
			break;

			case 27:
				earthAndSun2();
				globeWindWarm ();
			break;

			case 28:
				earthAndSun3();
				globeWindWarmUp ();
			break;

			// case 29:
				// $board.find(".others .globeWindWarmUp .text1").show(0);
			// break;

			case 29:
				// earthAndSun4();
				globeFeelGap();
			break;

			case 30:
				$board.find(".others2 .lowPressureEarthnSun").hide(0);
				earthAndSun4();
				// globeFeelGap();
				$board.find(".others .globeFeelGap .text0").hide(0);
				$board.find(".others .globeFeelGap .text1").show(0);
			break;

			/*case 31:
				$board.find(".others .globeFeelGap .text0").hide(0);
				$board.find(".others .globeFeelGap .text1").show(0);
			break;*/

			default:
				// loadQuestions();
			break;
		};

		/*if (countNext<2) {
			$board.find(".imgs").hide(0);
			$board.find('#canvasHolder').removeClass("hillbg");
			countNext = 10;
		};*/

		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			// ole.footerNotificationHandler.pageEndSetNotification();
			// $prevBtn.show(0);
			$nextBtn.hide(0);
		} else if (countNext===0) {
			// $nextBtn.show(0);
			$prevBtn.hide(0);
		} else {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		}
	}

/*text and desc */
	function intro () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "intro",
			title : data.string.p_wind_1,
			text : [data.string.p_wind_2,data.string.p_wind_3]
		}
		var html = template(content);
		$board.find(".others").html(html);
		// $nextBtn.show(0);
	}

	function descWind1 () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "descWind1",
			text : [data.string.p_wind_3_1,
					data.string.p_wind_3_2,
					data.string.p_wind_3_3,
					data.string.p_wind_3_4,
					data.string.p_wind_3_5]
		}
		var html = template(content);
		$board.find(".others").html(html);
		// $nextBtn.show(0);
	}

	function molecules () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "molecules",
			text : [data.string.p_wind_4]
		}
		var html = template(content);
		$board.find(".others").html(html);
		// $nextBtn.show(0);
	}

	function sunRadiation () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "sunRadiation",
			text : [data.string.p_wind_5,data.string.p_wind_6]
		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function warmerLand () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "warmerLand",
			text : [data.string.p_wind_7]
		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function warmerGoesUp () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "warmerGoesUp",
			text : [data.string.p_wind_8,
				data.string.p_wind_9,
				data.string.lowPressure]
		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function coldComesDown () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "coldComesDown",
			text : [data.string.p_wind_10_1,
				data.string.p_wind_10_2,
				data.string.highPressure,
				data.string.p_wind_9,
				data.string.lowPressure]
		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function completeCycle () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "completeCycle",
			text : [data.string.p_wind_11,
				data.string.p_wind_11_1,
				data.string.p_wind_12]
		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function breezeIntro () {
		canvas1.clearStaticDraw();
		canvas2.clearStaticDraw();
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "breezeIntro",
			text : [data.string.p_wind_19_1,
				data.string.p_wind_19_2,
				data.string.p_wind_19_3]
		}
		var html = template(content);
		$board.find(".others").html(html);
		console.log("imgHouse");
		var imgHouse = new Image();
		imgHouse.onload = function () {
			$(this).addClass("treeHouse");

		}
		imgHouse.src = $refImg+"wind01.png";
		console.log(imgHouse);
		$board.find('.others2').html("imgHouse");
	}

	function seaBreezeText () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		$board.find('.others').html("");
		$board.find('.others2').html("");
		var content={
			newCls : "seaBreezeText",
			text : [
				data.string.p_wind_seabreeze_1,
				data.string.p_wind_seabreeze_2,
				data.string.p_wind_seabreeze_3,
				data.string.p_wind_seabreeze_4,
				data.string.p_wind_seabreeze_5,
				data.string.p_wind_seabreeze_6,
				data.string.lowPressure
				]
		}
		var html = template(content);
		$board.find(".others").html(html);
		console.log("imgHouse");
		var imgHouse = new Image();
		imgHouse.onload = function () {
			$(this).addClass("treeHouse");

		}
		imgHouse.src = $refImg+"wind01.png";
		console.log(imgHouse);
		$board.find('.others2').html(imgHouse);
	}

	function landBreezeText () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "landBreezeText",
			text : [
				data.string.p_wind_landbreeze_1,
				data.string.p_wind_landbreeze_2,
				data.string.p_wind_landbreeze_3,
				data.string.p_wind_landbreeze_4,
				data.string.p_wind_landbreeze_5,
				data.string.p_wind_landbreeze_6,
				data.string.lowPressure
				]
		}
		var html = template(content);
		$board.find(".others").html(html);
		$board.find('.others2 img.treeHouse').attr('src',$refImg+"wind-night01.png");
	}

	function globeWindIntro1 () {
		var source = $("#globe-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "globeWindIntro",
			title : data.string.p_wind_13,
			imgs : $refImg+"globe.png",
			desc : data.string.p_globeWind_1,
		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function globeWindIntro () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "globeWindIntro",
			title : data.string.p_wind_13,

		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function globeWindWarm () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "globeWindWarm",
			title : data.string.p_wind_13,
			text : [data.string.p_wind_14,
				data.string.p_wind_15]
		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function globeWindWarmUp () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "globeWindWarmUp",
			title : data.string.p_wind_13,
			text : [data.string.p_wind_16,
				data.string.p_wind_17]
		}
		var html = template(content);
		$board.find(".others").html(html);
	}

	function globeFeelGap () {
		var source = $("#text-template").html();
		var template = Handlebars.compile(source);
		var content={
			newCls : "globeFeelGap",
			title : data.string.p_wind_13,
			text : [data.string.p_wind_18,
					data.string.p_globeWind_2]
		}
		var html = template(content);
		$board.find(".others").html(html);
		$board.find(".others2 .showArrows").show(0);
	}

/*land breeze and sea breeze*/
	var breezePos;
	function landBreeze () {
		canvas1.clearStaticDraw();
		canvas2.clearStaticDraw();
		breezePos = {
			midRow : Math.floor(2*numRow/5)+1,
			midcol : Math.floor(2*numCol/5)
		}

		for (var i = 0; i < numRow-1; i++) {
			for (var j = 0; j < numCol; j++) {
					color = "#00C9DB";
					stroke = "#70E2EA";
				var thecircle = circles[i][j];
				var objCircle = mVas.circle({
					x:thecircle.x,y:thecircle.y,
					fillStyle:color,
					radius : 2,
					strokeStyle : stroke});
				 canvas1.add(objCircle,j*disCol+20,i*disRow);
			};
		};
		canvas1.draw();
	}

	function landBreeze2 () {
		canvas1.clearStaticDraw();
		// canvas2.clearStaticDraw();
		mid = Math.floor(2*numCol/5);
		for (var i = 0; i < numRow-1; i++) {
			for (var j = 0; j < numCol; j++) {
					color = "#53B5D5";
					stroke = "#53B5D5";
				if (i>breezePos.midRow && j>breezePos.midcol) {
					color = "#F9EF5C";
					stroke = "#EDC77D";
					break;
				};
				var thecircle = circles[i][j];
				var objCircle = mVas.circle({
					x:thecircle.x,y:thecircle.y,
					fillStyle:color,
					radius : 2,
					strokeStyle : stroke});
				 canvas1.add(objCircle,j*disCol+20,i*disRow);
			};
		};
		canvas1.draw();
		landBreezeheat();
	}

	function landBreezeheat () {
		// console.log("m here");
		canvas2.clearStaticDraw();
		for (var i = 0; i < numRow-1; i++) {
			for (var j = 0; j < numCol; j++) {
					color = "#53B5D5";
					stroke = "#53B5D5";
				console.log(breezePos.midRow+" "+breezePos.midcol);
				if (i>breezePos.midRow && j>breezePos.midcol) {
					color = "#F9EF5C";
					stroke = "#EDC77D";
					// console.log(i+" "+j);
					var thecircle = circles[i][j];
					var objCircle = mVas.circle({
						x:thecircle.x,y:thecircle.y,
						fillStyle:color,
						radius : 4,
						strokeStyle : stroke});
					 canvas2.add(objCircle,j*disCol+20,i*disRow);
				};
			};
		};
		canvas2.draw();
	}

	function landBreezeUp () {
		$nextBtn.hide(0);
		var upOffset = 0, countLoop = 0;
		function movement0 () {
			if (countLoop>5) {
				$board.find(".others .landBreezeText .text6 ").show(0);
				$nextBtn.show(0);
			} else {
				canvas2.clearStaticDraw();
				for (var i = 0; i < numRow-1; i++) {
					for (var j = 0; j < numCol; j++) {
						color = "#53B5D5";
						stroke = "#53B5D5";
						if (i>breezePos.midRow && j>breezePos.midcol) {
							color = "#F9EF5C";
							stroke = "#EDC77D";
							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : 4,
								strokeStyle : stroke});
							upOffset = countLoop;
							canvas2.add(objCircle,j*disCol+20,(i-upOffset)*disRow);
						};

					};
				};
				canvas2.draw();
				countLoop+=0.1;
				setTimeout(movement0,50);

			}
		}

		movement0 ();
	}

	function landBreezeSide () {
		canvas1.clearStaticDraw();
		// canvas2.clearStaticDraw();
		var goUp = 0;
		var r = 2;
		var show = true;
		mid = Math.floor(2*numCol/5);
		for (var i = 0; i < numRow-1; i++) {
			for (var j = 0; j < numCol; j++) {
					color = "#53B5D5";
					stroke = "#53B5D5";
				console.log(breezePos.midRow+" "+breezePos.midcol);
				if (i>breezePos.midRow && j<=breezePos.midcol) {
					console.log("smth happened");
					show = false;
				} else if (i>breezePos.midRow && j>breezePos.midcol) {
					color = "#F9EF5C";
					stroke = "#EDC77D";
					goUp = 5;
					r = 4;
					show = true;
				} else {
					goUp = 0;
					r=2;
					show = true;
				}

				if (show) {
					var thecircle = circles[i][j];
					var objCircle = mVas.circle({
						x:thecircle.x,y:thecircle.y,
						fillStyle:color,
						radius : r,
						strokeStyle : stroke});
					 canvas1.add(objCircle,j*disCol+20,(i-goUp)*disRow);
				 };
			};
		};
		canvas1.draw();
		movementSideBreeze();
	}

	function movementSideBreeze () {
		$nextBtn.hide(0);
		var sideOffset = 0,
			countLoop = 0,
			sidecut = 0;
			downOffset = 0,
			treeCounter = 0;
			var treeHouseArray = ["wind-night02","wind-night03","wind-night04"];
		function movement2 () {
			if (countLoop>10) {
				$nextBtn.show(0);
			} else {
				var index = treeCounter%3;
				var imgTreeHouse = $refImg+treeHouseArray[index]+".png";
				$board.find(".others2 img.treeHouse").attr('src',imgTreeHouse);
				treeCounter++;
				canvas2.clearStaticDraw();
				for (var i = 0; i < numRow-1; i++) {
					for (var j = 0; j < numCol; j++) {
						if (j<=breezePos.midcol+1) {
							if (i<=breezePos.midRow) {
								downOffset = breezePos.midRow;
								sidecut = breezePos.midcol+1;
							} else {
								downOffset = 0;
								sidecut = 0;
							}

							color = "#53B5D5";
							stroke = "#53B5D5";
							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : 3,
								strokeStyle : stroke});
							sideOffset = countLoop;
							canvas2.add(objCircle,(j+sideOffset-sidecut)*disCol+20,(i+downOffset)*disRow);
						};

					};
				};
				canvas2.draw();
				countLoop+=0.1;
				setTimeout(movement2,50);

			}
		}

		movement2 ();
	}

	/*sea breeze*/

	function seaBreeze () {
		canvas1.clearStaticDraw();
		// canvas2.clearStaticDraw();
		setFlag = true;
		mid = Math.floor(2*numCol/5);
		for (var i = 0; i < numRow-1; i++) {
			for (var j = 0; j < numCol; j++) {
					color = "#53B5D5";
					stroke = "#53B5D5";
				console.log(breezePos.midRow+" "+breezePos.midcol);
				if (i>breezePos.midRow && j<=breezePos.midcol) {
					color = "#F9EF5C";
					stroke = "#EDC77D";
					setFlag = false;
				} else {
					setFlag = true;
				}
				if (setFlag) {
					var thecircle = circles[i][j];
					var objCircle = mVas.circle({
						x:thecircle.x,y:thecircle.y,
						fillStyle:color,
						radius : 2,
						strokeStyle : stroke});
					canvas1.add(objCircle,j*disCol+20,i*disRow);
				};

			};
		};
		canvas1.draw();
		seaBreezeheat();
	}

	function seaBreezeheat () {
		// console.log("m here");
		canvas2.clearStaticDraw();
		for (var i = 0; i < numRow-1; i++) {
			for (var j = 0; j < numCol; j++) {
					color = "#53B5D5";
					stroke = "#53B5D5";
				console.log(breezePos.midRow+" "+breezePos.midcol);
				if (i>breezePos.midRow && j<=breezePos.midcol) {
					color = "#F9EF5C";
					stroke = "#EDC77D";
					// console.log(i+" "+j);
					var thecircle = circles[i][j];
					var objCircle = mVas.circle({
						x:thecircle.x,y:thecircle.y,
						fillStyle:color,
						radius : 4,
						strokeStyle : stroke});
					 canvas2.add(objCircle,j*disCol+20,i*disRow);
				};
				// console.log("in goingUp try "+upOffset);

			};
		};
		canvas2.draw();
	}

	function seaBreezeUp () {
		$nextBtn.hide(0);
		var upOffset = 0, countLoop = 0;
		function movement1 () {
			if (countLoop>5) {
				$board.find(".others .seaBreezeText .text6").show(0);
				$nextBtn.show(0);
			} else {
				canvas2.clearStaticDraw();
				for (var i = 0; i < numRow-1; i++) {
					for (var j = 0; j < numCol; j++) {
						color = "#53B5D5";
						stroke = "#53B5D5";
						// console.log(breezePos.midRow+" "+breezePos.midcol);
						if (i>breezePos.midRow && j<=breezePos.midcol) {
							color = "#F9EF5C";
							stroke = "#EDC77D";
							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : 4,
								strokeStyle : stroke});
							upOffset = countLoop;
							canvas2.add(objCircle,j*disCol+20,(i-upOffset)*disRow);
						};

					};
				};
				canvas2.draw();
				countLoop+=0.1;
				setTimeout(movement1,50);

			}
		}

		movement1 ();
	}

	function seaBreezeSide () {
		canvas1.clearStaticDraw();
		// canvas2.clearStaticDraw();
		var goUp = 0;
		var r = 2;
		var show = true;
		mid = Math.floor(2*numCol/5);
		for (var i = 0; i < numRow-1; i++) {
			for (var j = 0; j < numCol; j++) {
					color = "#53B5D5";
					stroke = "#53B5D5";
				console.log(breezePos.midRow+" "+breezePos.midcol);
				if (i>breezePos.midRow && j<=breezePos.midcol) {
					console.log("smth happened");
					color = "#F9EF5C";
					stroke = "#EDC77D";
					goUp = 5;
					r = 4;
					show = true;

				} else if (i>breezePos.midRow && j>breezePos.midcol) {
					show = false;
				} else {
					goUp = 0;
					r=2;
					show = true;
				}

				if (show) {
					var thecircle = circles[i][j];
					var objCircle = mVas.circle({
						x:thecircle.x,y:thecircle.y,
						fillStyle:color,
						radius : r,
						strokeStyle : stroke});
					 canvas1.add(objCircle,j*disCol+20,(i-goUp)*disRow);
				 };
			};
		};
		canvas1.draw();
		seaMovementSide();
	}

	function seaMovementSide () {
		$nextBtn.hide(0);
		var sideOffset = 0,
			countLoop = 0,
			sidecut = 0;
			downOffset = 0
			treeCounter = 0;
			var treeHouseArray = ["wind02","wind03","wind04"];
		function movement3 () {
			if (countLoop>8) {
				$nextBtn.show(0);
			} else {
				if (countLoop>4) {
					var index = treeCounter%3;
					var imgTreeHouse = $refImg+treeHouseArray[index]+".png";
					$board.find(".others2 img.treeHouse").attr('src',imgTreeHouse);
					treeCounter++;
				};
				canvas2.clearStaticDraw();
				for (var i = 0; i < numRow-1; i++) {
					for (var j = 0; j < numCol; j++) {

						// console.log(breezePos.midRow+" "+breezePos.midcol);
						if (i>breezePos.midRow) {
							sidecut = breezePos.midcol;
							color = "#53B5D5";
							stroke = "#53B5D5";
							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : 3,
								strokeStyle : stroke});
							sideOffset = countLoop;
							canvas2.add(objCircle,(j-sideOffset+sidecut)*disCol+20,(i)*disRow);
						};

					};
				};
				canvas2.draw();
				countLoop+=0.1;
				setTimeout(movement3,50);

			}
		}

		movement3 ();
	}
/*earth and global wind*/
	function earthAndSun () {
		$board.find(".hillbg").removeClass("hillbg");
		$board.find('.tree').hide(0);
		canvas1.clearStaticDraw();
		canvas2.clearStaticDraw();
		earthCol = 5;
		earthSize = disCol*earthCol;
		quadrant1 = Math.floor(numRow/4);
		quadrant2 = Math.floor(3*numRow/3.4);
		mid = Math.floor(2*numCol/5);
		for (var i = 0; i < numRow; i++) {
			for (var j = mid; j < numCol; j++) {
					color = "#00C9DB";
					stroke = "#70E2EA";
				// console.log("in goingUp try "+upOffset);
				console.log(i+" "+j);
				var thecircle = circles[i][j];
				var objCircle = mVas.circle({
					x:thecircle.x,y:thecircle.y,
					fillStyle:color,
					radius : r,
					strokeStyle : stroke});
				 canvas1.add(objCircle,j*disCol+50,i*disRow);
			};
		};
		var sun1 = mVas.images($refImg+"sun.png",{width:150, height:20});
		var earth = mVas.images($refImg+"earth.png",{width:150, height:200});
		canvas1.add(sun1,0*disCol,numRow/2*disRow,400,400);
		canvas1.add(earth,(numCol-2)*disCol,numRow/2*disRow,earthSize,earthSize);
		canvas1.draw();
	}

	function earthAndSun2 () {
		$board.find(".hillbg").removeClass("hillbg");
		canvas1.clearStaticDraw();
		earthSurfacePos = numCol-earthCol;
		earthNMid = (numCol/2+earthSurfacePos)/2-2;
		for (var i = 0; i < numRow; i++) {
			for (var j = mid; j < numCol; j++) {
					color = "#00C9DB";
					stroke = "#70E2EA";
				if (i>numRow/2-2 && i<numRow/2+2) {
					if (j>earthNMid && j<earthSurfacePos) {
						stroke = "#ED8B2A";
						color = "#FAD160";
					};
				};
				// console.log(i+" "+j);
				var thecircle = circles[i][j];
				var objCircle = mVas.circle({
					x:thecircle.x,y:thecircle.y,
					fillStyle:color,
					radius : r,
					strokeStyle : stroke});
				 canvas1.add(objCircle,j*disCol+50,i*disRow);
			};
		};
		var sun1 = mVas.images($refImg+"sun.png",{width:150, height:20});
		var earth = mVas.images($refImg+"earth.png",{width:150, height:200});
		canvas1.add(sun1,0*disCol,numRow/2*disRow,400,400);
		canvas1.add(earth,(numCol-2)*disCol,numRow/2*disRow,earthSize,earthSize);
		canvas1.draw();
	}

	function earthAndSun3 () {
		$board.find(".hillbg").removeClass("hillbg");
		canvas1.clearStaticDraw();
		earthSurfacePos = numCol-earthCol;
		earthNMid = (numCol/2+earthSurfacePos)/2-2;
		var leftOffset=0;
		for (var i = 0; i < numRow; i++) {
			for (var j = mid; j < numCol; j++) {
					color = "#00C9DB";
					stroke = "#70E2EA";
				if (i>numRow/2-2 && i<numRow/2+2) {
					if (j>earthNMid && j<earthSurfacePos) {
						stroke = "#ED8B2A";
						color = "#FAD160";
						leftOffset = 3;
						break;
					} else {
						leftOffset=0;
					}
				} else {
					leftOffset=0;

				}
				// console.log(i+" "+j);
				var thecircle = circles[i][j];
					var objCircle = mVas.circle({
						x:thecircle.x,y:thecircle.y,
						fillStyle:color,
						radius : r,
						strokeStyle : stroke});
					canvas1.add(objCircle,(j-leftOffset)*disCol+50,i*disRow);

			};
		};
		var sun1 = mVas.images($refImg+"sun.png",{width:150, height:20});
		var earth = mVas.images($refImg+"earth.png",{width:150, height:200});
		canvas1.add(sun1,0*disCol,numRow/2*disRow,400,400);
		canvas1.add(earth,(numCol-2)*disCol,numRow/2*disRow,earthSize,earthSize);
		canvas1.draw();
		movement();
		$nextBtn.hide(0);
	};

	var leftOffset = 0, countLoop = 0;
	function movement () {
		if (countLoop>3) {
			setTimeout(function () {
				oneWord(data.string.lowPressure,"lowPressureEarthnSun");
				$nextBtn.show(0);
			},800);
		} else {

			canvas2.clearStaticDraw();
			for (var i = 0; i < numRow; i++) {
				for (var j = mid; j < numCol; j++) {
					color = "#00C9DB";
					stroke = "#70E2EA";
					if (i>numRow/2-2 && i<numRow/2+2) {
						if (j>earthNMid && j<earthSurfacePos) {
							stroke = "#ED8B2A";
							color = "#FAD160";
							leftOffset=countLoop;

							var thecircle = circles[i][j];
							var objCircle = mVas.circle({
								x:thecircle.x,y:thecircle.y,
								fillStyle:color,
								radius : r,
								strokeStyle : stroke});
							canvas2.add(objCircle,(j-leftOffset)*disCol+50,i*disRow);
						} else {
							leftOffset=0;
						}
					} else {
						leftOffset=0;
					}
				};
			};
			countLoop+=0.1;
			canvas2.draw();
			setTimeout(movement,100);
		}
	}

	function earthAndSun4 () {
		canvas2.clearStaticDraw();
		// canvas1.clearStaticDraw();
		earthSurfacePos = numCol-earthCol;
		earthNMid = (numCol/2+earthSurfacePos)/2-2;
		var leftOffset=0;
		var setFlag = true;
		for (var i = 0; i < numRow; i++) {
			for (var j = mid; j < numCol; j++) {
					color = "#00C9DB";
					stroke = "#70E2EA";
				/*if (j>earthSurfacePos) {
					console.log(numRow+" earthNMid "+earthNMid+" j "+j+" earthSurfacePos "+earthSurfacePos);
					setFlag = false;
				} else*/ if (i>numRow/2-2 && i<numRow/2+2) {
					if (j>earthNMid && j<earthSurfacePos) {
						stroke = "#ED8B2A";
						color = "#FAD160";
						leftOffset = 3;
						// break;
					} else {
						leftOffset=0;
						// break;
					}

				} else {
					setFlag = true;
					leftOffset=0;

				}

				if (setFlag) {
					var thecircle = circles[i][j];
					var objCircle = mVas.circle({
						x:thecircle.x,y:thecircle.y,
						fillStyle:color,
						radius : r,
						strokeStyle : stroke});
					canvas2.add(objCircle,(j-leftOffset)*disCol+50,i*disRow);

				};
			};
		};
		var sun1 = mVas.images($refImg+"sun.png",{width:150, height:20});
		var earth = mVas.images($refImg+"earth.png",{width:150, height:200});
		canvas2.add(sun1,0*disCol,numRow/2*disRow,400,400);
		canvas2.add(earth,(numCol-2)*disCol,numRow/2*disRow,earthSize,earthSize);
		canvas2.draw();
		movementSide();
		$nextBtn.hide(0);
	};

	var sideOffset = 0, countSideLoop = 180,counterBoom =0;
	function movementSide () {
		if (countSideLoop>270) {
			setTimeout(function () {
			},800);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {

			canvas1.clearStaticDraw();
			// return;
			for (var i = 0; i < numRow; i++) {
				for (var j = mid; j < numCol; j++) {
					color = "#00C9DB";
					stroke = "#70E2EA";
					var radiusR = Math.sqrt(Math.pow((i-numRow/2),2)+Math.pow((j-earthNMid),2));
					radiusR = radiusR/3;
					// console.log("radiusR "+radiusR);
					var xC=0, yC =0;
					var angle = countSideLoop;
					var xC = radiusR*Math.cos(angle*Math.PI/180);
					var yC = radiusR*Math.sin(angle*Math.PI/180);
					if (i>numRow/2) {
						if (counterBoom===0) {
							console.log("radius = "+radiusR);
							color = "#FECB04";
							stroke = "#E9732E";
							counterBoom+=50;
						} else {
							color = "#00C9DB";
							stroke = "#70E2EA";
						}

						var changeX = (j+5-xC)*disCol;
						var changeY = (i+yC)*disRow;

					} else {

						var changeX = (j+5-xC)*disCol;
						var changeY = (i-yC)*disRow;
					}
					var thecircle = circles[i][j];
					var objCircle = mVas.circle({
						x:thecircle.x,y:thecircle.y,
						fillStyle:color,
						radius : r,
						strokeStyle : stroke});
					canvas1.add(objCircle,changeX,changeY);

				};
			};
			countSideLoop+=1;
			canvas1.draw();
			setTimeout(movementSide,100);
		}
	};
// })(jQuery);

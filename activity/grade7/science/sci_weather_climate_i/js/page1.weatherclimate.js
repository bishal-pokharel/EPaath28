(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 7,
		$refImg = $ref+"/images/page1/";
	loadTimelineProgress($total_page,countNext+1);

	var secondInfo = {};
	var secondImages = [];
	var months = [];

	var checker = [null,null,null,null,null,null];

	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var content = {
			cloud : $refImg+"02.png",
			tornado : $refImg+"05.png",
			weathercock : $refImg+"01.png",
			introTitle : data.string.p1_title
		};
		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	}

	/*first call to first*/
	first();

	function second () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		for (var i = 1; i < 7; i++) {
			secondImages.push({
				img : $refImg + "icon/img"+i+".png",
				caption : data.string["p1_2_"+i]
			});
		};
		var content = {
			introTitle : data.string.p1_2_title,
			img : secondImages
		};
		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	}

	function allcomponents() {
		var source = $("#area-template").html();
		var template = Handlebars.compile(source);
		var content1 = {
			componentHeading : data.string.p2_1,
			componentInstruction : data.string.p2_componentInstruction,
			slide2componentName : [
									{
										componenName:data.string.p2_componentSunshine,
										componentExplain : [data.string.p2_componentSunshineExplain],
										componentClass: "sunshine"
									},
									{
										componenName: data.string.p2_componentTemperature,
										componentExplain : [data.string.p2_componentTemperatureExplain_1,
												data.string.p2_componentTemperatureExplain_2,
												data.string.p2_componentTemperatureExplain_3,
												data.string.p2_componentTemperatureExplain_4],
										componentClass : "temperature"
									},
									{
										componenName: data.string.p2_componentAtmosphericPressure,
										componentExplain : [data.string.p2_componentAtm_Explain_1,
												data.string.p2_componentAtm_Explain_2,
												data.string.p2_componentAtm_Explain_3,
												data.string.p2_componentAtm_Explain_4],
										componentClass : "pressure"
									},
									{
										componenName: data.string.p2_componentWind,
										componentExplain : [data.string.p2_componentWindExplain_1,
												data.string.p2_componentWindExplain_2,
												data.string.p2_componentWindExplain_3,],
										componentClass : "wind"
									},
									{
										componenName: data.string.p2_componentHumidity,
										componentExplain : [data.string.p2_componentHumidityExplain_1,
												data.string.p2_componentHumidityExplain_2,
												data.string.p2_componentHumidityExplain_3,],
										componentClass : "humidity"
									},
									{
										componenName: data.string.p2_componentPrecipitation,
										componentExplain : [data.string.p2_componentPrecipitationExplain_1,
												data.string.p2_componentPrecipitationExplain_2,
												data.string.p2_componentPrecipitationExplain_3],
										componentClass : "precipitation"
									},
									{
										componenName: data.string.p2_componentCloudiness,
										componentExplain : [data.string.p2_componentCloudinessExplain_1,
												data.string.p2_componentCloudinessExplain_2,
												data.string.p2_componentCloudinessExplain_3,
												data.string.p2_componentCloudinessExplain_4,
												data.string.p2_componentCloudinessExplain_5],
										componentClass : "cloudiness"
									}
								]
		}
		var html = template(content1);
		$board.html(html);

		var $clickComponents = $board.children('div.wrapperArea').children('div.clickComponents').children('div');
		var $components = $clickComponents.children('p');
		var $componentParent;
		var $componentUncles;
		var $componentSibling;
		$components.hover(function() {
			$this = $(this);
			/* Stuff to do when the mouse enters the element */
			$this.siblings('div').show(100);
			$clickComponents.children('p').css("opacity","0.1");
			$this.css({'opacity' : '1',
				"text-decoration":"none",
				"color":"rgba(10, 33, 29,0.7)"});
			// $this.css("text-decoration","none");
			// $this.css(rgb(10, 33, 29))
			// to check if all been clicked or hovered
			var rambo = true; //just a name :D
			var index = $this.data("indexs");
			// console.log("smth"+typeof index);
			checker[index-1]=1;
			// console.log(checker);
			for (var i = 0; i < checker.length; i++) {
				if (!checker[i]) {
					rambo=false;
					// console.log("stop "+i);
					break;
				};
			};

			if (rambo) {
				$nextBtn.show(0);
				// ole.footerNotificationHandler.pageEndSetNotification();
			};

		}, function() {
			/* Stuff to do when the mouse leaves the element */
			$clickComponents.children('div').hide(100);
			$clickComponents.children('p').css("opacity","1");
		});
	}

	function third (thisdata) {
		var source = $("#third-template").html();
		var template = Handlebars.compile(source);
		var list = [];
		for (var i = 1; i <= 6; i++) {
			list.push(data.string["pComponents_"+i]);
		};

		var content = {
			weathertext : [data.string.p1slide4text_1,
					data.string.p1slide4text_2,
					data.string.p1slide4text_3],
			lists : list
		};
		var html = template(content);
		$board.html(html);

		$nextBtn.show(0);

		loadTimelineProgress($total_page,countNext+1);

		// ole.footerNotificationHandler.pageEndSetNotification();

		
	};

	function snowAndFrost () {
		var source = $("#snow-template").html();
		var template = Handlebars.compile(source);
		var content = {
			title : data.string.p1_snowFrost_title,
			snowForst : [
				{
					cls : "snow",
					title : data.string.p1_snow,
					img : $refImg + "snow.gif",
					imgTitle : data.string.p1_snowImgTitle,
					lists : [data.string.p1_snow_1,
						data.string.p1_snow_2,
						data.string.p1_snow_3,
						data.string.p1_snow_4]
				},
				{
					cls : "frost",
					title : data.string.p1_frost,
					img : $refImg + "frost.png",
					imgTitle : data.string.p1_frostImgTitle,
					lists : [data.string.p1_frost_1,
						data.string.p1_frost_2,
						data.string.p1_frost_3,
						data.string.p1_frost_4]
				}
			]
		};
		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	}

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		thenext();
	});

	function thenext () {
		countNext++;

		switch (countNext) {
			case 1:				
				second ();
				break;
				
			case 2:				
				third ();
				break;

			case 3:
				$board.find('.weathertext1').show(0);
				$nextBtn.show(0);
				break;

			case 4:
				$board.find('.weathertext2').show(0);
				$board.find('.wrapperThird ul').show(0);
				$nextBtn.show(0);
				break;

			case 5:
				allcomponents();
				break;

			case 6:
				snowAndFrost ();
				$nextBtn.hide(0);
				break;

			default:break;
		}

		// if (countNext<2) {
		// 	countNext=6;
		// };

		loadTimelineProgress($total_page,countNext+1);
		if (countNext>=$total_page-1) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// $(this).show(0);
		}
		
	}

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		thenext();
	});

	/*if (countNext>=$total_page-1) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}
*/
})(jQuery);
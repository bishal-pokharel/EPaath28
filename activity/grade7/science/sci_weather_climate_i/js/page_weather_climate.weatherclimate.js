(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 1,
		$refImg = $ref+"/images/page2/";
	loadTimelineProgress($total_page,countNext+1);

	var imgThinkYourself = "images/timetothink/timetothink1.png";

	$nextBtn.hide(0);
	/*text2 : data.string.p2_2,
			text3 : data.string.p2_3*/
/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var content = {
			imgThink : imgThinkYourself,
			textList : [data.string.p_climateText_1,
					data.string.p_climateText_2,
					data.string.p_climateText_3]
		};
		var html = template(content);
		$board.html(html);
	}

	/*first call to first*/
	first();
	loadTimelineProgress($total_page,countNext+1);
	ole.footerNotificationHandler.pageEndSetNotification();

})(jQuery);
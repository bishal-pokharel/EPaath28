(function ($) {

	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 4,
		$refImg = $ref+"/images/";
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.show(0);

	var newPlace = true, count = 0;

	var dataList = [
		{
			justClass : 'intro',
			text : data.string.p_climateNepal_1,
			img : $refImg+"map-of-nepal.png",
			desc : [data.string.p_climateNepal_2,
				data.string.p_climateNepal_3,
				data.string.p_climateNepal_4,
				data.string.p_climateNepal_5]
		}
	]

/*
* template loading starts
*/
	function intro(theData) {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		// var content = theData;
		console.log(theData);
		var html = template(theData);
		console.log(html);
		$board.html(html);
	}
	intro(dataList[0]);

	$board.on('click','.instruments',function () {
		$this = $(this);
		$this.find('.desc').show(0);
	})

	$nextBtn.on('click',function () {
		/*$(this).css("display","none");
		$prevBtn.css('display', 'none');*/
		countNext++;
		thenext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		/*$(this).css("display","none");
		$nextBtn.css('display', 'none');*/

		countNext--;

		if (countNext<0) {
			countNext = 0;
		};

		newPlace = true;
		thenext();
	});

	function thenext () {
		console.log(countNext);
		switch (countNext) {
			case 0:
				intro(dataList[0]);
			break;

			case 1 :
				$board.find('.wrapperIntro .desc .desc1').fadeIn();
			break;

			case 2 :
				$board.find('.wrapperIntro .desc .desc2').fadeIn();
			break;

			case 3 :
				$board.find('.wrapperIntro .desc .desc3').fadeIn();
			break;

			default:
				areaLoader();
			break;
		};

		/*$nextBtn.hide(0);
		$prevBtn.hide(0);*/

		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
			$prevBtn.show(0);
			$nextBtn.hide(0);
		} else if (countNext===0) {
			$nextBtn.show(0);
			$prevBtn.hide(0);
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

})(jQuery);

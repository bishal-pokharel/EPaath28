(function ($) {

	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 4,
		$refImg = $ref+"/images/how/";
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.show(0);

	var newPlace = true, count = 0;

	var checker = [null,null,null,null,null];

	var dataList = [
		{
			justClass : 'intro',
			text : data.string.p_how_forecast_1,
			img : $refImg+"weather-forecast.png",
			desc : [data.string.p_how_forecast_2]
		},
		/*{
			justClass : 'fun',
			text : "fun time : Is this forecasting?",
			img : $refImg+"forecasting_stone.png",
			desc : ""
		},*/
		{
			justClass : 'intro3',
			text : data.string.p_how_forecast_5,
			// img : $refImg+"how1.jpg",
			desc : [data.string.p_how_forecast_4,
					data.string.p_how_forecast_6,
					data.string.p_how_forecast_7]

		},
		{
			title : data.string.p_how_forecast_9,
			instruction : data.string.p_how_forecast_9_1,
			instruments : [{
				img : $refImg+"barometer.png",
				title : data.string.p_how_forecast_10,
				desc : data.string.p_how_forecast_11,
			},{
				img : $refImg+"maxmin_thermometer.png",
				title : data.string.p_how_forecast_12,
				desc : data.string.p_how_forecast_13,
			},{
				img : $refImg+"hygrometer.png",
				title : data.string.p_how_forecast_14,
				desc : data.string.p_how_forecast_15,
			},{
				img : $refImg+"anemometer.gif",
				title : data.string.p_how_forecast_16,
				desc : data.string.p_how_forecast_17,
			},{
				img : $refImg+"rain_gauge.png",
				title : data.string.p_how_forecast_18,
				desc : data.string.p_how_forecast_19,
			}]
		}
	]

/*
* template loading starts
*/
	function intro(theData) {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		// var content = theData;
		console.log(theData);
		var html = template(theData);
		console.log(html);
		$board.html(html);
	}
	intro(dataList[0]);

	function instruments (theData) {
		var source = $("#area-template").html();
		var template = Handlebars.compile(source);
		// var content = theData;
		console.log(theData);
		var html = template(theData);
		console.log(html);
		$board.html(html);
	};

	$board.on('click','.instruments',function () {
		$this = $(this);
		$this.find('.desc').show(0);
		var rambo = true; //just a name :D
			var index = $this.index();
			checker[index]=1;
			for (var i = 0; i < checker.length; i++) {
				if (!checker[i]) {
					rambo=false;
					// console.log("stop "+i);
					break;
				};
			};

			if (rambo) {
				ole.footerNotificationHandler.pageEndSetNotification();
			};
	})

	$nextBtn.on('click',function () {
		/*$(this).css("display","none");
		$prevBtn.css('display', 'none');*/
		countNext++;
		thenext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		/*$(this).css("display","none");
		$nextBtn.css('display', 'none');*/

		countNext--;



		if (countNext<0) {
			countNext = 0;
		};

		newPlace = true;
		thenext();
	});

	function thenext () {
		console.log(countNext);
		switch (countNext) {
			case 0:
				intro(dataList[countNext]);
			break;

			/*case 1 :
				// intro(dataList[countNext]);
			break;*/

			case 1 :
				intro(dataList[countNext]);
			break;

			case 2 :
				$board.find(".desc1, .desc2").show(0);
			break;

			case 3 :
				instruments(dataList[countNext-1]);
			break;

			case 9:

			break;

			default:
				areaLoader();
			break;
		};

		/*$nextBtn.hide(0);
		$prevBtn.hide(0);*/

		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			// ole.footerNotificationHandler.pageEndSetNotification();
			$prevBtn.show(0);
			$nextBtn.hide(0);
		} else if (countNext===0) {
			$nextBtn.show(0);
			$prevBtn.hide(0);
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

})(jQuery);

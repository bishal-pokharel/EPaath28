(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 3,
		$refImg = $ref+"/images/climatechange/";
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.show(0);

	var contents = [{
		cls : "change",
		title : data.string.p_climateChange_1,
		desc : [data.string.p_climateChange_2,
				data.string.p_climateChange_3,
				"["+data.string.p_climateChange_ref_1+"]"],
		img : $refImg+"polarbear.png"
	},{
		cls : "effects",
		title : data.string.p_climateChange_effects_title,
		effects : [
			{
				img : $refImg+"effect1.png",
				title : data.string.p_climateChange_4,
				desc : [data.string.p_climateChange_8,
						data.string.p_climateChange_9]
			},
			{
				img : $refImg+"effect2.png",
				title : data.string.p_climateChange_5,
				desc : [data.string.p_climateChange_10]
			},
			{
				img : $refImg+"effect3.png",
				title : data.string.p_climateChange_6,
				desc : [data.string.p_climateChange_12]
			},
			{
				img : $refImg+"effect4.png",
				title : data.string.p_climateChange_7,
				desc : [data.string.p_climateChange_12]
			}
		]
	},{
		cls : "reduce",
		title : data.string.p_climateChange_13,
		reduced : [
			{
				img : $refImg+"reduce1.png",
				title : data.string.p_climateChange_14,
				desc : [data.string.p_climateChange_15,
					"["+data.string.p_climateChange_ref_2+"]"]
			},
			{
				img : $refImg+"reduce2.png",
				title : data.string.p_climateChange_16,
				desc : [data.string.p_climateChange_17]
			}
		]
	},{
		cls : "reduce",
		title : data.string.p_climateChange_13,
		reduced : [
			{
				img : $refImg+"reduce3.png",
				title : data.string.p_climateChange_18,
				desc : [data.string.p_climateChange_19,
					"["+data.string.p_climateChange_ref_3+"]"]
			},
			{
				img : $refImg+"reduce4.png",
				title : data.string.p_climateChange_20,
				desc : [data.string.p_climateChange_21]
			}
		]
	}]

	var checker = [null,null,null,null];

/*
* first
*/
	function pageLoader(getContent) {
		// console.log(getContent);
		var source = $("#change-template").html();
		var template = Handlebars.compile(source);
		var html = template(getContent);
		// console.log(html)
		$board.html(html);
		// $nextBtn.show(0);
	}

	/*first call to first*/
	pageLoader(contents[0]);

	function pageLoader2() {
		// console.log(getContent);
		var source = $("#effects-template").html();
		var template = Handlebars.compile(source);
		var html = template(contents[1]);
		// console.log(html)
		$board.html(html);
		$nextBtn.hide(0);

		$board.find(".wrapperEffects .effects > div").mouseenter(function () {
			$this = $(this);
			var rambo = true; //just a name :D
			var index = $this.index();
			checker[index]=1;
			for (var i = 0; i < checker.length; i++) {
				if (!checker[i]) {
					rambo=false;
					// console.log("stop "+i);
					break;
				};
			};

			if (rambo) {
				$nextBtn.show(0);
				// ole.footerNotificationHandler.pageEndSetNotification();
			};

			$this.find(".desc").show(0);
		}).mouseleave(function () {
			$this.find(".desc").hide(0);
		})
	}

	function reduce(getContent) {
		console.log(getContent);
		var source = $("#reduce-template").html();
		var template = Handlebars.compile(source);
		var html = template(getContent);
		// console.log(html)
		$board.html(html);
		$nextBtn.show(0);
	}

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		thenext();
	});

	function thenext () {
		countNext++;
		switch (countNext) {
			case 1:
				pageLoader2();
				break;
			case 2:
				reduce(contents[2]);
				break;
			case 3:
				reduce(contents[3]);
			break;

			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
		if (countNext>=$total_page) {
			$nextBtn.hide(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// $(this).show(0);
		}
	}

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		thenext();
	});



})(jQuery);

var refImg = $ref+"/exercise/images/";
var refDeviceImg =  refImg+"devices/";
var content=[
	{	//1
		text1:	data.string.goose_1,
		gooseFull:refImg+"place5.png"
	},
	{//2
		cls : "hibernation",
		text1:	data.string.goose_1_1,
		gooseFull:refImg+"01.png",
		listMypage: [data.string.goose_1_2,data.string.goose_1_3,data.string.goose_1_4,data.string.goose_1_5,data.string.goose_1_6]

	},
	{//3
		text1:	data.string.goose_1,
		fullGoose2:refImg+"02.png",
		textmiddle: data.string.goose_3,
		text3: data.string.goose_4
	},
	{//4 done
		gooseYes:"yes",
		text1:	data.string.goose_4,
		gooseImg:refImg+"bird-travel.png",
		text4: data.string.goose_6,
		gooseFull:"gooseFull2",
		gooseImgBack : refImg+"03.png"
	},
	{	//5
		cls : "page5",
		text1: data.string.goose_7_a,
		gooseFull:refImg+"03.png",
		text2: data.string.goose_7,
		gooseleft:refImg+"girl.png"
	},
	{//6
		text1: data.string.goose_8,
		backpack:[	{imgsrc:refImg+"bookclosed.png",backText:data.string.goose_9,backpackimages:"backpackimages"},
					{imgsrc: refDeviceImg+"thermometer2.png",backText:data.string.goose_10 , backpackimages:"thermoClass"},
					{imgsrc: refDeviceImg+"barometer2.png",backText:data.string.goose_11,backpackimages:"backpackimages"},
					{imgsrc: refDeviceImg+"hygrometer2.png",backText:data.string.goose_12,backpackimages:"backpackimages"},
					{imgsrc: refDeviceImg+"anemometer2.png",backText:data.string.goose_13,backpackimages:"backpackimages"},
					{imgsrc: refDeviceImg+"rainguage2.png",backText:data.string.goose_14,backpackimages:"backpackimages"}
				]
	},
	{	//7
		text1: data.string.goose_15,
		fullGoose:refImg+"place1.png",
		fullmap:refImg+"map1.png",
		desText:data.string.goose_20_1,
		desTextclass:"desText",
		girl:refImg+"girl.png",
		standinggirl:"standinggirl"

	},
	{	//8

	},

	{	//9
		nextdestText: data.string.goose_46,
		firstDestination:refImg+"travel1.png",
		flyingGirl:refImg+"girl-with-duck.png"

	},
	{	//10
		text1: data.string.goose_16,
		fullGoose:refImg+"place2.png",
		fullmap:refImg+"map2.png",
		desText:data.string.goose_20_2,
		desTextclass:"desText1",
		girl:refImg+"girl.png",
		standinggirl:"standinggirl"
	},
	{	//11

	},
	{	//12
		nextdestText: data.string.goose_46,
		firstDestination:refImg+"travel2.png",
		flyingGirl:refImg+"girl-with-duck.png"

	},
	{	//13
		text1: data.string.goose_17,
		fullGoose:refImg+"place3.png",
		fullmap:refImg+"map3.png",
		desText:data.string.goose_20_3,
		desTextclass:"desText",
		girl:refImg+"girl.png",
		standinggirl:"standinggirl2"
	},
	{	//14

	},
	{	//15
		nextdestText: data.string.goose_46,
		firstDestination:refImg+"travel3.png",
		flyingGirl:refImg+"girl-with-duck.png"

	},
	{	//16
		text1: data.string.goose_18,
		fullGoose:refImg+"place4.png",
		fullmap:refImg+"map4.png",
		desText:data.string.goose_20_4,
		desTextclass:"desText2",
		girl:refImg+"girl.png",
		standinggirl:"standinggirl2"
	},
	{	//17

	},
	{	//18
		nextdestText: data.string.goose_46,
		firstDestination:refImg+"travel4.png",
		flyingGirl:refImg+"girl-with-duck.png"

	},
	{	//19
		text1: data.string.goose_19,
		fullGoose:refImg+"place5.png",
		fullmap:refImg+"map5.png",
		desText:data.string.goose_20_5,
		desTextclass:"desText3",
		girl:refImg+"girl.png",
		standinggirl:"standinggirl2"
	},
	{ //20
	},
	{	//21
		fullGoose:refImg+"1_goose3.png",
		desText:data.string.goose_38,
		desTextclass:"desText4"
	},
	{	//22
		gooseFull:refImg+"Deepa-Reporting.png",
		desText:data.string.goose_38,
		desTextclass:"desText4",
		thpresent:[data.string.goose_39,data.string.goose_place_0,
			data.string.goose_place_1,
			data.string.goose_place_2,
			data.string.goose_place_3,
			data.string.goose_place_4],
		listprest:[
			{insdevice:data.string.goose_10_1,deviceVal:[data.string.goose_30_6,data.string.goose_31_6,data.string.goose_32_6,data.string.goose_33_6,data.string.goose_34_6] },
			{insdevice:data.string.goose_11,deviceVal:[data.string.goose_30_1,data.string.goose_31_1,data.string.goose_32_1,data.string.goose_33_1,data.string.goose_34_1] },
			{insdevice:data.string.goose_12,deviceVal:[data.string.goose_30_3,data.string.goose_31_3,data.string.goose_32_3,data.string.goose_33_3,data.string.goose_34_3] },
			{insdevice:data.string.goose_13,deviceVal:[data.string.goose_30_4,data.string.goose_31_4,data.string.goose_32_4,data.string.goose_33_4,data.string.goose_34_4] },
			{insdevice:data.string.goose_14,deviceVal:[data.string.goose_30_2,data.string.goose_31_2,data.string.goose_32_2,data.string.goose_33_2,data.string.goose_34_2] }
			]
	}

];
var exerciseTime = [8,11,14,17,20];
for (var i = 0; i < exerciseTime.length; i++) {
	content[exerciseTime[i]-1] = {
		instruction: (function (i) {
					var text1 = ole.textSR(data.string.goose_21,"#place#",data.string["goose_place_"+i]);
					return text1;
				})(i),
		readingPlaceTitle:data.string["goose_place_"+i],
		devices: [
					{imgsrc: refDeviceImg+"thermometer"+i+".png",caption:data.string.goose_10,
						devicename:"thermometer",
						calculation : data.string.goose_23,
						unit : "<sup>o</sup>C",
						hintDataval:data.string["goose_3"+i+"_6_a"],
						desc : data.string.goose_40,
						values : data.string["goose_3"+i+"_6"]
					},
					{imgsrc: refDeviceImg+"barometer"+i+".png",
						caption:data.string.goose_11,
						devicename:"barometer",
						calculation : data.string.goose_24,
						unit : "hPa",
						hintDataval:data.string["goose_3"+i+"_1_a"],
						desc : data.string.goose_41,
						values : data.string["goose_3"+i+"_1"]
					},
					{imgsrc: refDeviceImg+"hygrometer"+i+".png",
						caption:data.string.goose_12,
						devicename:"hygrometer",
						calculation : data.string.goose_25,
						unit :"%",
						hintDataval:data.string["goose_3"+i+"_3_a"],
						desc : data.string.goose_42,
						values : data.string["goose_3"+i+"_3"]
					},
					{imgsrc: refDeviceImg+"anemometer"+i+".png",
						caption:data.string.goose_13,
						devicename:"anemometer",
						calculation : data.string.goose_27,
						unit : "MPH",
						hintDataval:data.string["goose_3"+i+"_4_a"],
						desc : data.string.goose_44,
						values : data.string["goose_3"+i+"_4"]
					},
					{imgsrc: refDeviceImg+"rainguage"+i+".png",
						caption:data.string.goose_14,
						devicename:"raingauge",
						calculation : data.string.goose_26,
						unit : "mm",
						hintDataval:data.string["goose_3"+i+"_2_a"],
						desc : data.string.goose_43,
						values : data.string["goose_3"+i+"_2"]
					}
				],

			//journal
			jttl:"journalTitle_"+$lang,
			journal:data.string.goose_22,
			journalTitle : data.string.goose_myJournal,
			closeBook:refImg+"bookclosed.png",
			ok:data.string.goose_28,
			//stickers
			stickercls:"stickerTitle_"+$lang,
			sticker:data.string.goose_35,
			stickerinst:data.string.goose_36,
			// pointMe:"./images/clickme_icon.png",
			stickerCollection:[
				{imgsrc:refImg+"sticker/windy.png",imgname:"windy"},
				{imgsrc:refImg+"sticker/sunny.png",imgname:"sunny"},
				{imgsrc:refImg+"sticker/rainy.png",imgname:"rainy"},
				{imgsrc:refImg+"sticker/cold.png",imgname:"cold"}
			],
			stickerComment:data.string.goose_30_7,
			commentLinkval:data.string.goose_37,
			commentLink:"commetBox_"+$lang,
			hint : data.string.goose_45,
			stickerMsg : data.string.goose_stickermsg,
			journalVals:[
				{	//temperature
					unit : "<sup>o</sup>C",
					thermo:data.string.goose_23,
					dataval:data.string["goose_3"+i+"_6"]},
				{	//pressure
					unit : "hPa",
					thermo:data.string.goose_24,
					dataval:data.string["goose_3"+i+"_1"]},
				{	//humidity
					unit :"%",
					thermo:data.string.goose_25,
					dataval:data.string["goose_3"+i+"_3"]},
				{	//wind speed
					unit : "MPH",
					thermo:data.string.goose_27,
					dataval:data.string["goose_3"+i+"_4"]},
				{	//rainfall
					unit : "mm",
					thermo:data.string.goose_26,
					dataval:data.string["goose_3"+i+"_2"]}
			],
	}
};

// console.log(content[8]);
var sourceval=[	"#template-1",//1
				"#template-1-a",//2
				"#template-2",//3
				"#template-2",//4
				"#template-6",//5
				"#template-3",//6
				"#template-4",//7
				"#template-5",//8
				"#template-8",//9 -new
				"#template-4",//10
				"#template-5",//11
				"#template-8",//12 -new
				"#template-4",//13
				"#template-5",//14
				"#template-8",//15 -new
				"#template-4",//16
				"#template-5",//17
				"#template-8",//18 -new
				"#template-4",//19
				"#template-5",//18
				"#template-4",//21
				"#template-7"];//22

var correctCheck = [null,null,null,null,null];
var counterDrop=0;
var totalCounTdrop=0;
$.fn.dropMe=function ($accept)
{
	var $this=$(this);

	$this.droppable({
        accept: $accept,
        drop: function( event, ui ) {
        	var id=ui.draggable.attr('id');

        	var imgsrc=ui.draggable.attr('src');


        	counterDrop++;
        	$("#"+id).hide(0);
        	$(this).append('<img src="'+imgsrc+'"/>');
        	if(counterDrop==totalCounTdrop)
        	{
        		$("#journalRead").removeClass('journalText3').addClass('journalText4');
        		$(".stickerComment").delay(500).fadeIn(500);
        		$("#commentLink").delay(500).fadeIn(500);
        		$("#activity-page-next-btn-enabled").show(0);
        		$(".stickerCollection").hide(0);
        	}
        }
    });

};

$(function(){
	$board = $(".board");
	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);


	var counter=1; //count events
	var totalCounter=22; //count question

	firstTemplate();

	$("#activity-page-next-btn-enabled").click(function(){
			counter++;
		if (counter>22) {
			ole.activityComplete.finishingcall();
			$("#activity-page-next-btn-enabled").hide(0);
		} else {
			$(this).hide(0);

			firstTemplate();
		}
	});

	/* clicking small notebook icon will open fullpage journal */
	$board.on("click",".templateBox .devices",function(){
		$this = $(this);
		var $idvars=$this.attr('id');

		var opacityCss = $this.css("opacity");

		$this.find(".pushmeimage").hide(0);
		$this.on('click','.hintOption',function () {
			$this.find(".hintText").css('opacity',1);
		})

		// checkAll();
		return;

	});


	$board.on('click','.clsbtn',function (event) {
		// alert("close");
		event.stopPropagation();
		$board.find(".putReadings").hide(0);
		$board.find(".templateBox .devices").animate({opacity:1},1000)
		checkAll ();
	})

	function checkAll () {

		var checkNull = true;
		for (var i = 0; i < correctCheck.length; i++) {
			if(correctCheck[i]=== null){
				checkNull = false;
				// console.log("checkNull "+i);
				break;
			}
		};

		console.log(correctCheck);
		// console.log(checkNull);
		if (checkNull) {
			$board.find('.readingsData').hide(0);
			correctCheck = [null,null,null,null,null];
			myoksFun()
		};
	}

	$board.on('click','.readingsData .okBtn',checkAll);

	$board.on("keyup",".exetext",function(e){

		if(e.which == 13) {
			checkAll ();
		}

		$this = $(this);
		var myvalstxt=$(this).val();
		var getData = $this.data("values");
		var getIndex = $this.data("index");

		var empty = 0;

		if(myvalstxt!=getData)
		{
			$(this).css({'background':"rgba(254,0,0,0.6)"});
			correctCheck[getIndex]=null;
		}
		else
		{
			correctCheck[getIndex]=1;
			$(this).css({'background':"rgba(27, 112, 24,0.6)","color":"#fff"});
		}

	});

// tooltips
	$board.tooltip({
			tooltipClass: 'mytooltip',
		});

	/*
		if ok is clicked in journal then devices will hide
		sticker will show

	*/
	var myoksFun = function(){
		$(".myOks").hide(0);
		$(".closeJournal").fadeOut(10);
		$(".exetext").hide(0);
		$(".readdataval").show(0);
		$("#toolBox").hide(0);
		$("#journalRead").removeClass('journalText').addClass('journalText3');
		$(".instruction").hide(0);
		$("#stickerPatch").show(0);
		$("#stickerBox").show(0);
		$(".stickerCollection").show(0);
		$("#noteDevice").hide(0);

		$(".draggable").draggable({ revert: "invalid"});
		var acceptclass="";
		if(counter==8)
		{
			acceptclass='#sunny, #rainy';
			totalCounTdrop=2;
			counterDrop=0;
		}
		else if(counter==11)
		{
			acceptclass='#sunny';
			totalCounTdrop=1;
			counterDrop=0;
		}
		else if(counter==14)
		{
			acceptclass=' #rainy, #sunny';
			totalCounTdrop=2;
			counterDrop=0;
		}
		else if(counter==17)
		{
			acceptclass='#cold, #windy';
			totalCounTdrop=2;
			counterDrop=0;
		}
		else if(counter==20)
		{
			acceptclass='#cold, #windy';
			totalCounTdrop=2;
			counterDrop=0;
		}

		$("#stickerBox").dropMe(acceptclass);
	};

	function firstTemplate()
	{
		$dataval=content[counter-1];
		var whatSourceval=sourceval[counter-1];
		var source= $(whatSourceval).html();

		var template = Handlebars.compile(source);


		var html=template($dataval);

		$(".board").fadeOut(10,function(){
			$(this).html(html);
		}).delay(100).fadeIn(10,function(){
			Callfunction();
		});//fadein function ends
	}//goingToAnotherPlanet function ends




	function Callfunction()
	{
		switch(counter)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6 :
			case 21:
			case 9:
			case 12:
			case 15:
			case 17:
			case 18:
				$("#activity-page-next-btn-enabled").show(0);
				loadTimelineProgress(content.length,counter);
				break;
			case 7 :
			case 10:
			case 13:
			case 16:
			case 19:
			$(".board").find('.fullmap').addClass("animated bounceInLeft");
			$("#activity-page-next-btn-enabled").show(0);
			loadTimelineProgress(content.length,counter);
				break;
			case 22:
			$("#activity-page-next-btn-enabled").show(0);
			loadTimelineProgress(content.length,counter);
			break;
			default:
			loadTimelineProgress(content.length,counter);

		}
	}
});

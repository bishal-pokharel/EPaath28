$(function(){
	var $wahtar=getSubpageMoveButton($lang,"next");
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		 quest++;
		 $(this).fadeOut(10);
		 $("#activity-page-prev-btn-enabled").fadeOut(10);
		 getFunction(quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		 quest--;
		 $(this).fadeOut(10);
		 $("#activity-page-next-btn-enabled").fadeOut(10);
		 getFunction(quest);
	 });
	 var cnt=0;

	 function getFunction(qno){
			loadTimelineProgress(3,qno);
			switch(qno)
			{
					case 1:
						case1();
						break;
					case 2:
						case2();
						break;
					case 3:
						case3();
						break;
					}
		}
		function case1(){

				var datavar={
					easyTitle: data.lesson.chapter,
					coverimg:$ref+"/images/page1/state_of_matter.png"
				}
				var source   = $("#front-template").html();

				var template = Handlebars.compile(source);

				var html=template(datavar);
				$("#firstpage").html(html);
				$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
		}
		function case2(){
			$("#firstpage").hide(0);
			$(".head_title").html(data.string.p1_1);
			$("#p1_1_a").html(data.string.p1_1_a);
			$("#p1_1_b").html(data.string.p1_1_b);
			$("#p1_1_c").html(data.string.p1_1_c);

			for(var $i=2; $i<=7;$i++)
			{
				$("#p1_"+$i+" div").html(data.string["p1_"+$i]);
			}


			$("#p1_1_a").delay(300).fadeIn(10,function(){


				$("#p1_1_a").addClass('animated bounceInDown');
				setTimeout(function(){

					$("#activity-page-next-btn-enabled").fadeIn(10,function(){ 	});

				},1500);

			});
		}

		function case3(){
			$("#p1_1_a").fadeOut(1);
			$("#p1_1_c").delay(300).fadeIn(1000,function(){
				var $a=10;
				var $k=2;
				for(var $i=2;$i<=7;$i++)
				{


					$("#p1_"+$i).fadeIn(10*$a,function(){

						var outHeight=parseInt($("#p1_"+$k).innerHeight())/2;
						var innHeight=parseInt($("#p1_"+$k+" div").innerHeight())/2;

						var inTop=outHeight-innHeight-40;
						$("#p1_"+$k+" div").css({'top':inTop});
						$k++;

						$(this).addClass('animated bounce');
					});
					$a=$a+20;
				}


			});

			$(".elements").click(function(){
				cnt++;
				var $id=$(this).attr('id');
				var $dataVar;
				var tempVar=$id.split("_"); ;
				var imgName=tempVar[1];

				var closeImgSrc=getCloseBtn();
				$dataVar={
					img:$ref+"/images/page1/"+imgName+".png",
					title:data.string[$id],
					textdesc:data.string[$id+"_a"],
					closeImgSrc:closeImgSrc
				}

				var source=$("#template-1").html();
				var template=Handlebars.compile(source);
				var html=template($dataVar);
				$(".popUp").html(html).fadeIn(10,function(){



					$("#elementPop").delay(100).fadeIn(10,function(){

						$("#eleInsider").center(true);
					});
				});

				if(cnt==6)
				{
					$("#p1_1_b").fadeIn(1000,function(){
						ole.footerNotificationHandler.pageEndSetNotification();
					});
				}

			});

			$(".popUp").on("click","#closeMebtn",function(){
				$(".popUp").fadeOut(100,function(){
					$(this).html('');
				});
			});
		}




});

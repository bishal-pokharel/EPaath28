$(function(){

	var counter=1;

	var $whatnext=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnext);

	var $whatprev=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprev);

	$(".head_title").html(data.string.p6_1);



	getFunction(counter)

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);
		counter++;
		getFunction(counter);

	});


	$("#activity-page-prev-btn-enabled").click(function(){
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		counter--;
		getFunction(counter);

	});


	$("#animationBox").on({mouseenter: function() {
			$(this).find('div').fadeOut(10).delay(10).fadeIn(10,function(){
				var outHeight=parseInt($(this).outerHeight())/2;
				var innHeight=parseInt($(this).find("span").outerHeight())/2;
				var inTop=outHeight-innHeight;
				$(this).find('span').css({'top':inTop});

			});
		},
		mouseleave: function() {
			$(this).find('div').fadeOut(10);
		}
	},'.hoverRoll');




});

function getdata(counter)
{
	var datavar;
	switch(counter)
	{
		case 1:
			datavar={
				text_1:data.string.p6_2,
				text_2:data.string.p6_3
			}
			break;
		case 2:
			datavar={
				text_2:data.string.p6_3,
				img1:$ref+"/images/page6/1.png",
				img2:$ref+"/images/page6/2.png"
			}
			break;
		case 3:
			datavar={
				txt1:data.string.p6_17,
				txt2:$ref+"/images/page6/1.png",
				txt3:$ref+"/images/page6/3.png",
				txt4:$ref+"/images/page6/4.png",
				txt5:$ref+"/images/page6/7.png",
				txt6:$ref+"/images/page6/8.png",
				txt7:data.string.p6_23,
				txt8:data.string.p6_24,
				txt9:data.string.p6_25,
				p6_25a:data.string.p6_25a,
				p6_23a:data.string.p6_23a
			}
			break;
		case 4:
			datavar={
				text_2:data.string.p6_7,
				img1:$ref+"/images/page6/3.png",
				img2:$ref+"/images/page6/4.png"
			}
			break;
		case 5:
			datavar={
				text_2:"",
				img1:$ref+"/images/page6/4.png",
				img2:$ref+"/images/page6/1.png"
			}
			break;
		case 6:
			datavar={
				text_5:data.string.p6_10,
				text_6:data.string.p6_11,
				s1:$ref+"/images/page6/2.png",
				s2:$ref+"/images/page6/4.png",
				s3:$ref+"/images/page6/3.png",
				s4:$ref+"/images/page6/6.png",
				s5:$ref+"/images/page6/10.png"
			}
			break;
	}

	return datavar;
}

function getHtml(counter)
{
	var source;
	var datsvr=getdata(counter);


	if(counter==1)
		source=$("#template-1").html();
	else if(counter==2 || counter==4 ||counter==5)
		source=$("#template-2").html();
	else if(counter==3)
		source=$("#template-3").html();
	else if(counter==6)
		source=$("#template-4").html();

	var template=Handlebars.compile(source);
	var html=template(datsvr);



	return html;
}

function animate_1(counter)
{

	var myHtml=getHtml(counter);

	$("#animationBox").fadeOut(10,function(){
		$(this).html(myHtml);
	}).delay(10).fadeIn(10,function(){

		$("#text_1").fadeIn(10,function(){
			$(this).addClass('animated bounceInRight');
			setTimeout(function(){

				$("#activity-page-next-btn-enabled").delay(1500).fadeIn(10,function(){	});

			},1500);
		});
	});
}


function animate_2(counter)
{

	var myHtml=getHtml(counter);

	$("#animationBox").fadeOut(10,function(){
		$(this).html(myHtml);
	}).delay(10).fadeIn(10,function(){

		if(counter==5)
		{
			var $i,$k;
			$k=26;

			for($i=1;$i<=2;$i++)
			{
				var $delayval=500*$i;
				$("#imge3"+$i).delay($delayval).fadeIn(10,function(){
					$(this).addClass('animated bounce');

					$(this).find('div').html(data.string["p6_"+$k]);
					$k++;
				});
			}

			$("#activity-page-next-btn-enabled").delay(3000).fadeIn(10,function(){

			});


			$("#activity-page-prev-btn-enabled").delay(3000).fadeIn(10,function(){

			});

		}
		else
		{
			$("#text_2").fadeIn(10,function(){
				$(this).addClass('animated bounceInRight');
				setTimeout(function(){

					var $i,$k;
					if(counter==2) $k=5;
					else $k=8;
					for($i=1;$i<=2;$i++)
					{
						var $delayval=1000*$i;
						$("#imge3"+$i).delay($delayval).fadeIn(10,function(){
							$(this).addClass('animated bounce');

							$(this).find('div').html(data.string["p6_"+$k]);
							$k++;
						});
					}

					$("#activity-page-next-btn-enabled").delay(3000).fadeIn(10,function(){

					});

					if(counter==4)
					{
						$("#activity-page-prev-btn-enabled").delay(3000).fadeIn(10,function(){

						});
					}
				},1500);
			});//fadein
		}
	});//fadein


}

function animate_4(counter)
{

	var myHtml=getHtml(counter);

	$("#animationBox").fadeOut(10,function(){
		$(this).html(myHtml);
	}).delay(10).fadeIn(10,function(){

		var $i,$k=11;
		for($i=1;$i<=5;$i++)
		{
			var $delayval=1000*$i;
			$("#s"+$i).delay($delayval).fadeIn(10,function(){
				$(this).addClass('animated bounce');

				$(this).find('span').html(data.string["p6_"+$k]);
				$k++;


			});
		}

		setTimeout(function()
		{
			$(".newImgs2").addClass('hoverRoll');

			$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10,function(){
				ole.footerNotificationHandler.lessonEndSetNotification();
			});

		},5500);
	});
}
function  animate_3(counter)
{
 	var myHtml=getHtml(counter);

	$("#animationBox").fadeOut(10,function(){
		$(this).html(myHtml);
	}).delay(10).fadeIn(10,function(){
		$("#bigCircle").delay(100).fadeIn(100,function(){
			for($i=2;$i<=6;$i++)
			{
				var $delayval=50*$i;
				$("#t"+$i).delay($delayval).fadeIn(10);
			}

			setTimeout(function(){
				$("#bigCircle").addClass('whatBig');
				$("#p6_23a").delay(5000).fadeIn(10,function(){

					$("#smallCircle").delay(2000).fadeIn(10,function(){

						$(this).addClass('whatsmall');
						$("#p6_25a").delay(2600).fadeIn(10,function(){

							$("#t7").delay(500).fadeIn(500);
							$("#t9").delay(500).fadeIn(500,function(){

								$(".imgTxt").delay(4500).animate({'opacity':'0'},200);

							});
						}).delay(6000).animate({'top':'65%','font-size':'2em'},500);
					});
				}).delay(12000).animate({'top':'20%','font-size':'2em'},500,function(){

						$("#t11").html("&nbsp;&nbsp;");
						$("#t8").delay(500).fadeIn(500,function(){
							$("#activity-page-next-btn-enabled").delay(1500).fadeIn(10,function(){

							});

							$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10,function(){

							});
						});
				});

			},1000);//set

		});


	});
}

function getFunction(counter)
{
	loadTimelineProgress(6,counter);

	switch(counter)
	{
		case 1:
			animate_1(counter);
			break;
		case 2:
			animate_2(counter);
			break;
		case 3:
			animate_3(counter);
			break;
		case 4:
			animate_2(counter);
			break;
		case 5:
			animate_2(counter);
			break;
		case 6:
			animate_4(counter);
			break;
	}

}

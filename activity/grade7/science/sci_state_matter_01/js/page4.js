$(function(){

	var counter=1;

	var $whatnext=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnext);

	var $whatprev=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprev);


	$(".head_title").html(data.string.p4_1);

	getFunction(counter);


	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(10);
		counter++;
		getFunction(counter);

	});

	$("#activity-page-prev-btn-enabled").click(function(){
		$(this).fadeOut(10);
		counter--;
		getFunction(counter);
		ole.footerNotificationHandler.hideNotification();
	});


});

function getdata(counter)
{
	loadTimelineProgress(2,counter);
	var datavar;
	switch(counter)
	{
		case 1:
			datavar={
				p4_2:data.string.p4_2,
				img1:$ref+"/images/page4/2.png",
				p4_3:data.string.p4_3,
				p4_4:data.string.p4_4,
				p4_5:data.string.p4_5,
				p4_6:data.string.p4_6,
				p4_7:data.string.p4_7,
				p4_8:data.string.p4_8,
				p4_9:data.string.p4_9,
				p4_10:data.string.p4_10
			}
			break;
		case 2:
			datavar={
				p4_11:data.string.p4_11,
				p4_12:data.string.p4_12,
				p4_13:data.string.p4_13,
				img1:$ref+"/images/page4/2.svg",
				img2:$ref+"/images/page4/3.svg"
			}
			break;

	}

	return datavar;
}

function getHtml(counter)
{
	var source;
	var datsvr=getdata(counter);


	if(counter==1)
		source=$("#template-1").html();
	else
		source=$("#template-2").html();

	var template=Handlebars.compile(source);
	var html=template(datsvr);

	var html=template(datsvr);

	return html;
}
function getFunction(counter)
{

	switch(counter)
	{
		case 1:
			animate_1(counter);
			break;
		case 2:
			animate_2(counter);
			break;
	}

}

function animate_1(counter)
{

	var myHtml=getHtml(counter);

	$("#animationBox").fadeOut(10,function(){
		$(this).html(myHtml);
	}).delay(10).fadeIn(10,function(){

		$("#p4_2").delay(200).fadeIn(500,function(){
			$("#animationBox").find('img').fadeIn(500,function(){
				$("#p4_5").fadeIn(500,function(){

					$("#p4_6").fadeIn(500,function(){

						$("#p4_7").fadeIn(500,function(){

							$("#p4_3").delay(500).fadeIn(500,function(){

								$("#p4_8").delay(500).fadeIn(500,function(){
									$("#p4_4").delay(500).fadeIn(500,function(){
										$("#p4_9").fadeIn(500);
										$("#p4_10").fadeIn(500);

										$("#activity-page-next-btn-enabled").delay(1500).fadeIn(10,function(){

										});

									});
								});
							});
						});

					});
				});
			});
		});
	});//fadein
}
function animate_2(counter)
{

	var myHtml=getHtml(counter);

	$("#animationBox").fadeOut(10,function(){
		$(this).html(myHtml);
	}).delay(10).fadeIn(10,function(){
		$("#p4_11").delay(200).fadeIn(500,function(){

			$("#all").delay(200).fadeIn(10,function(){
				$('#left').addClass('animated bounceInLeft');
				$('#right').addClass('animated bounceInRight');


				$("#activity-page-prev-btn-enabled").delay(1500).fadeIn(10,function(){


					ole.footerNotificationHandler.pageEndSetNotification();
				});
			});


		});
	});//fadin
}

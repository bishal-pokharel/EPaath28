$(function(){

	var $whatnext=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnext);

	$("#toDo").html(data.string.e_1);
	$("#retryBtnId").html(getReloadBtn());


	var counter=0;

	var arrayquestion=new Array(1,2,3,4,5,6);

	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;


	myquestion(newarray[counter],counter);
	var whatclick=0;
	var whatclickOpt;
	var yesSS=true;
	$(".allquestion").on("click",".ans",function(){
		yesSS=true;

		var isCorrect=parseInt($(this).attr('corr'));

		/***
		for question number 6 where two options need to be selected


		***/
		var whatMe=newarray[counter];


		if(whatMe==6)
		{
			yesSS=false;
			whatclick++; //to check number of click
			if(whatclick==1)
			{
				whatclickOpt=$(this).attr("id");

				$("#"+whatclickOpt).removeClass('ans');
			}
			else if(whatclick==2)
			{
				var click22=$(this).attr("id");
				yesSS=true;


				if((whatclickOpt=="opt_6_1") && (click22=="opt_6_2"))
				{
						isCorrect=1;

				}
				else if((whatclickOpt=="opt_6_2")&&(click22=="opt_6_1"))
				{
						isCorrect=1;

				}
				else
				{
					isCorrect=0;

				}
			}

		}
		/*************************************************/
		if(yesSS==true)
		{


			$("#showAns div").removeClass('ans');
			$("#showAns div[corr='1']").addClass('corrans');
			if(isCorrect==1)
			{
				$("#img22").attr('src',$ref+'/exercise/images/correct.png');
				$("#img22").addClass('Ok');

				rightcounter++;
			}
			else
			{
				$("#img22").attr('src',$ref+'/exercise/images/incorrect.png');
				$("#img22").addClass('notOk');

				wrongCounter++;
			}

			$("#activity-page-next-btn-enabled").fadeIn(10,function(){

			});
		}

	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;

		if(counter<6)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}
	});

	$("#retryBtnId").click(function(){
		location.reload();
	});
});


function myquestion(questionNo,counter)
{
	loadTimelineProgress(7,(counter+1));
	var source   = $("#label-template").html();

	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter)
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){
		if(questionNo==4)
		{

			setTimeout(function(){
				$(".allquestion").find("#opt_4_1").html("<img src='"+$ref+"/exercise/images/1.png' />");
				$(".allquestion").find("#opt_4_2").html("<img src='"+$ref+"/exercise/images/2.png' />");
				$(".allquestion").find("#opt_4_3").html("<img src='"+$ref+"/exercise/images/3.png' />");
			},100);
		}
	});
}

function myquestion2(right,wrong)
{
	loadTimelineProgress(7,7);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var $dataval={rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:data.string.exe_1, qans: data.string.opt_1_3,quesno:"ques_1"},
			{ques:data.string.exe_2, qans: data.string.opt_2_2,quesno:"ques_2"},
			{ques:data.string.exe_3, qans: data.string.opt_3_3,quesno:"ques_3"},
			{ques:data.string.exe_4, qans: '',quesno:"ques_4"},
			{ques:data.string.exe_5, qans: data.string.opt_5_3,quesno:"ques_5"},
			{ques:data.string.exe_6, qans: data.string.opt_6_2,quesno:"ques_6"}

		]


	}
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.exe3);

	}).delay(100).fadeIn(10,function(){

		setTimeout(function(){
			//quetion 4 has figure
			$(".allquestion").find("#ques_4").find('div').html("<img src='"+$ref+"/exercise/images/2.png' />");

			//multiple answer
			$(".allquestion").find("#ques_6").find('div').append("&nbsp; &nbsp;"+ data.string.opt_6_1);


			$("#retryBtnId").delay(500).fadeIn(10,function(){
				ole.footerNotificationHandler.pageEndSetNotification();
				ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);



			});

		},100);
	});
}

function getQuestion($quesNo,counter)
{
	var quesList;
	var nep=ole.nepaliNumber(counter+1);

	var whatri=whatCorr($quesNo);


	quesList={
		myQuesion:data.string["exe_"+$quesNo],
		img22:$ref+"/exercise/images/blank.png",
		optObj:[
				{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"]},
				{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"]},
				{optObj1:"opt_"+$quesNo+"_3", yesno:whatri[2],optObjval:data.string["opt_"+$quesNo+"_3"]}
				]
	}

	return quesList;
}

function whatCorr($quesNo)
{
		var whatis=new Array();
	switch($quesNo)
	{
		case 1: whatis[0]=0;whatis[1]=0; whatis[2]=1; break;
		case 2: whatis[0]=0;whatis[1]=1; whatis[2]=0; break;
		case 3: whatis[0]=0;whatis[1]=0; whatis[2]=1; break;
		case 4: whatis[0]=0;whatis[1]=1; whatis[2]=0; break;
		case 5: whatis[0]=0;whatis[1]=0; whatis[2]=1; break;
		case 6: whatis[0]=1;whatis[1]=1; whatis[2]=0; break;
	}
	return whatis;
}

var counterMe=0;
$.fn.dropMe=function ($accept)
{
	var $this=$(this);

	$this.droppable({
        accept: $accept,
        drop: function( event, ui ) {
        	var id=ui.draggable.attr('id');
        	counterMe++;
        	$("#"+id).draggable( "destroy" ).removeClass('polaroid').addClass('dropped');
        	if(counterMe==9)
        	{
        		$("#activity-page-next-btn-enabled").delay(100).fadeIn(10,function(){
                });
        	}
        }	 
        	
    });

};



$(function(){

    loadTimelineProgress(1,1);
	$("#toDo").html(data.string.e_2);
	$("#text1").html(data.string.e_2_1);
	$("#text2").html(data.string.e_2_2);
	$("#text3").html(data.string.e_2_3);

	$(".draggable").draggable({ revert: "invalid"});

	$("#dropbox1 div").dropMe('#img1, #img4, #img8');
	$("#dropbox2 div").dropMe('#img3, #img5, #img7');

	$("#dropbox3 div").dropMe('#img2, #img9, #img6');

    $("#activity-page-next-btn-enabled").click(function(){
        ole.activityComplete.finishingcall();
    });
});


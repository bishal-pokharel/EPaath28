var counterMe=1;
$(function(){

    loadTimelineProgress(1,1);
	$("#toDo").html(data.string.ex1);
	$("#text1").html(data.string.e_2_1);
	$("#text2").html(data.string.e_2_2);
	$("#text3").html(data.string.e_2_3);

    $("#conclusion").html(data.string.ex2);

    for(var i=1;i<=10;i++)
    {
        $("#app"+i).html(data.string["app"+i]);
         $("#app"+i).hide(0);

         $("#drag_"+i).html(data.string["app"+i]);
    }

	$(".draggable").draggable({ revert: true});

     dragdrop();


    $("#activity-page-next-btn-enabled").click(function(){
        // location.reload();
        ole.activityComplete.finishingcall();
    });
    function dragdrop(){
        $('.dropbox').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("id");
                var droppableans = $(this).attr("id");
                var dragans = draggableans.toString().split("drag_")[1];
                var dropans = droppableans.toString().split("img")[1];
                if(dragans==dropans||(dragans==1 && dropans ==4)||(dragans==4 && dropans ==1)) {
                    counterMe++;
                    $(this).find("p").show()
                    play_correct_incorrect_sound(1);
                    var id = ui.draggable.attr('id');
                    ui.draggable.hide();
                    if (counterMe == 10) {
                        $("#conclusion").delay(100).fadeIn(10,function()
                        {
                            $("#activity-page-next-btn-enabled").delay(200).fadeIn(10,function(){

                            });
                        });
                    }
                }
                else{
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }
});

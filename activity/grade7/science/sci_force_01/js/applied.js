
$(document).ready(function()
{

	var $whatNext=getSubpageMoveButton($lang,"next");
	$("#play").append($whatNext);

	var whatprev=getSubpageMoveButton($lang,"prev");
	$("#goback").append(whatprev);

	var pagenum=1;

	$("#outerbtn").html(getCloseBtn());

	$(".titleText").html(data.string.s5);
	$("#s11").html("<b>"+data.string['s11']+"</b>");
	$("#s11_6").html(data.string['s11_6']);
	$("#s11_7").html(data.string['s11_7']);
	$("#s11_8").html(data.string['s11_8']);
	
	$("#Go").html(data.string.go)
	$("#Reset").html(data.string.reset)
	// Regan added later
	$('.closebtn').click(function(){
		$('.PopUpView').hide(0);
	});



	$("#s11_1").html("<br/>"+data.string['s11_1']);

	$(".popup_paging").hide(0);




	$("#page_popup_1").show(0);
	page1();


	$("#play").click(function(){
		$(".popup_paging").hide(0);
		$("#page_popup_2").show(0);
		$(".closebtn").show(0);
		page2();

	});


	$("#goback").click(function(){
		$(".popup_paging").hide(0);
		$("#page_popup_1").show(0);
		$(".closebtn").hide(0);

	});

});
function page1()
{

	var animating = false;
	$(".StickFigure").click(function(){

		var id=$(this).attr('id');

		var timestramp=new Date().getTime();



			animating = true;
			if(id=="pullload")
			{

				$(this).find('img').attr('src','activity/grade7/science/sci_force_01/images/gif/applied-1.gif?'+timestramp);




			}
			else if(id=="kickball")
			{


				$(this).find('img').attr('src','activity/grade7/science/sci_force_01/images/gif/applied-2.gif?'+timestramp);


			}
			else if(id=="swinging")
			{

				$(this).find('img').attr('src','activity/grade7/science/sci_force_01/images/gif/applied-3.gif?'+timestramp);
			}

	});
}

function page2()
{



	$("#Go, .choiceselect").removeClass('opacitychange');
	$(".push-basket,.pull-basket").empty();
	$("#pushingperson").hide(0);
	$("#pullingperson").hide(0);


	$(".gorestbtn").hide(0);
	$(".item").hide(0);


	$("input[name='pulltype']").attr('checked',false);
	var pushitem=0;

	$("input[name='pulltype']").change(function(){


		if($(this).val()=="Push") pushitem=1;
		else pushitem=2;

		$(".item").show(0);
		$(".gorestbtn").show(0);


		$(".choiceselect").addClass('opacitychange');
		$("input[name='pulltype']").attr('disabled','disabled');
		if(pushitem==1)
		{
			$("#pushingperson").show(0);
			$("#pullingperson").hide(0);
		 }
		else if(pushitem==2)
		{
			$("#pushingperson").hide(0);
			$("#pullingperson").show(0);
		}

	});

	$(".item1,.item2,.item3").click(function(){

		var name=$(this).attr('valname');
		var itemimg=$(this).find('img').attr('src');
		if(pushitem==1)
		{
			$(".push-basket").append('<img src="'+itemimg+'" />');
		}
		else if(pushitem==2)
		{
			$(".pull-basket").append('<img src="'+itemimg+'" />');
		}
	});


	$("#Go").click(function(){

		if(!$(this).hasClass('opacitychange'))
		{
			$(this).addClass('opacitychange');
			if(pushitem==1)
			{

				$("#pushingperson").transition({ x: '100%' },3500,'linear');


			}
			else if(pushitem==2)
			{
				$("#pullingperson").transition({ x: '-75%' },3500,'linear');
			}
		}//if
	});//go

	$("#Reset").click(function(){
		$(".push-basket,.pull-basket").empty();
		$("#pushingperson").removeAttr("style");
		$("#pullingperson").removeAttr("style");
		$("#pushingperson").hide(0);
		$("#pullingperson").hide(0);

		$("#Go, .choiceselect").removeClass('opacitychange');
		$("input[name='pulltype']").removeAttr('disabled');
		$(".gorestbtn").hide(0);
		$(".item").hide(0);
		$("input[name='pulltype']").attr('checked',false);

	});

}

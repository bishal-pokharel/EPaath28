$(document).ready(function()
{

	var $whatNext=getSubpageMoveButton($lang,"next");
	$("#play").append($whatNext);

	var whatprev=getSubpageMoveButton($lang,"prev");
	$("#goback2").append(whatprev);

	$(".closebtn").html(getCloseBtn());


	var pagenum=1;
	$(".titleText").html(data.string.s6);
	
	$("#s12").html(data.string['s12']);
	$("#s12a").html(data.string['s12a']);
	
	$("#s14").html(data.string['s14']);
	
	$("#s13").html("<b>"+data.string['s13']+"</b>");

	$("#s13a").html(data.string['s13a']);
	
	$("#reset").html(data.string['s1100']);
	
	$(".popup_paging").hide(0);
	
	$("#page_popup_1").show(0);
	
	$("#play").click(function(){
		$(".popup_paging").hide(0);
		$("#page_popup_2").show(0);
		$(".closebtn").show(0);
		page2();
	});
	
	
	$("#goback2").click(function(){
		$(".popup_paging").hide(0);
		$("#page_popup_1").show(0);
		$(".closebtn").hide(0);	
	});
	
	
	
	

});


function page2()
{
		resetAnimation();
		var whatsurface="wood";
		var leftval="210px";
		$("#reset").hide(0);

		

		
		$(".holders").click(function(){
			 if(!$(this).hasClass('disabled'))
			 {
				whatsurface=$(this).attr('id');
				$("#surfacearea").attr({src: 'activity/grade7/science/sci_force_01/images/'+whatsurface+'.png'});
			}
			
		});
		
		
		$("#reset").click(function(){
		
			resetAnimation();
			
			
		});//reset
		
		
		$("#pusher" ).mouseup(function() {
			if(!$(this).hasClass('disabled-pusher'))
			{
				$(this).addClass('disabled-pusher');
				$(this).animate({'left' : '95%'},200,function() {
				
					
					$(".holders").addClass("disabled");
					
					
					var i=0;
					var addon='move_marbel-in-wood';
					var duration=1000;
					if(whatsurface=="wood")
					{		leftval="25%"; 
							duration=3000;
							addon='move_marbel-in-wood';
					}
					else if(whatsurface=="carpet")
					{		leftval="48%"; duration=5000;  
							addon='move_marbel-in-carpet';
					}
					else if(whatsurface=="ice")
					{		
						leftval="0%";	duration=1000;  
						addon='move_marbel-in-ice';

					}
					
					
					$("#marbel").animate({'left' : leftval}, {
						duration: duration,
						start:function(now, fx)
						{
							$(this).find('img').attr("src",$ref+"/images/marble-final.gif");

						},
						step: function(now, fx)
						{
						
							
							
						},//step,
						
						complete:function()
						{
							$("#reset").show(0);
							$(this).find('img').attr("src",$ref+"/images/marble-final.png");
						}
					});//animate
			
					
				})//function
				
			}//if
			
		})//up
		.mousedown(function() {
			
			if(!$(this).hasClass('disabled-pusher'))
			{
				$(this).animate({'left' : '98%'},500);
			}
		}); //mousedown
		


	
}


function rotateLogo(distance) {
	

	var diameter = $('#marbel').height();
	
	var perimeter = (22*diameter)/7;

	
	var degree = distance*360/perimeter;
	

	
	$('#marbel').css('-moz-transform','rotate('+degree+' deg)');
}

function resetAnimation()
{
	$(".holders").removeClass("disabled");
	$("#pusher").removeClass('disabled-pusher');
	$("#surfacearea").attr({src: 'activity/grade7/science/sci_force_01/images/wood.png'});
	


	$("#marbel").css({'left': '89%'});
}
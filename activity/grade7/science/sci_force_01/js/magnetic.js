$(document).ready(function()
{

	var $whatnext=getSubpageMoveButton($lang,"next");

	$("#play").append($whatnext);

	var $whatprev=getSubpageMoveButton($lang,"prev");

	$("#goback").append($whatprev);
	$(".closebtn").html(getCloseBtn());


	$(".titleText").html(data.string.s7);

	var pagenum=1;
	
	
	
	for(var i=15;i<=18;i++)
	{
		var eleid=$("#s"+i);
		var $data=data.string['s'+i];
		eleid.html($data);
		eleid.hide(0);
	}
	

		
	$(".popup_paging").hide(0);
	
	$("#page_popup_1").show(0);
	
	page1();
	
	$("#play, #goback2").click(function(){
		$(".popup_paging").hide(0);
		$("#page_popup_2").show(0);
		$(".closebtn").show(0);
			page2();
	});
	
	
	$("#goback").click(function(){

		resetAnimation();
		$(".popup_paging").hide(0);
		$("#page_popup_1").show(0);
		$(".closebtn").hide(0);	
			page1();
	});
	
	
	
	
	
	
	
});

function page1()
{
	$("#magnet_hand, .allmangnet2,.allmangnet3").hide(0);
	$("#play").hide(0);
	
	$("#s15").show(0);
	$("#magnet_hand").fadeIn(3000).animate({"left":"23%"},1000,function(){
			
			
			$("#only_magnet").animate({"left":"50%"},500);
	});
	
	
	$("#s16").delay(5000).fadeIn(1000);
	
	$(".allmangnet2").delay(6000).fadeIn(500);
	
	$("#magnet_hand2").delay(6500).animate({"left":"26%"},1000,function()
	{
			$("#only_magnet2").animate({"top":"0%"},100);
	});
	
	$("#s17").delay(7000).fadeIn(1000);
	
	
	
	$(".allmangnet3").delay(9000).fadeIn(500,'linear',function(){
		
		var timestramp=new Date().getTime();
		$(".magnetanimation").find('img').attr('src','activity/grade7/science/sci_force_01/images/gif/magnets-1.gif?'+timestramp);
		
			$("#play").show(0);
	});
	
	
}

function page2()
{


$("#s18").show(0);

$("#goback").show(0);

}

function resetAnimation()
{
	$("#magnet_hand, .allmangnet2,.allmangnet3").hide(0);
	$("#play").hide(0);
	
	for(var i=15;i<=19;i++)
	{
		var eleid=$("#s"+i);
		eleid.hide(0);
	}
	$("#magnet_hand").css('left','10%');
	
	$("#magnet_hand2").css('left','10%');
	$("#only_magnet").css('left','40%');
	$("#only_magnet2").css('top','25%');
	
	$("#magnetic1").css({"top":"0%"});
		$("#magnetic2").css({"top":"0%"});
		$("#magnetic3").css({"top":"0%"});
		$("#magnetic4").css({"top":"0%"});
		$("#magnetic5").css({"top":"20%"});
	
	
	
}
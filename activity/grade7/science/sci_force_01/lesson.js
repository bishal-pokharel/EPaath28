var dataArray=Array();
$(document).ready(function()
{
	var configfilename=langFileSelect();
	var langtype=configfilename['type'];
	
	var lessoncount=1;
	var lastlesson=2; //totalnumber of lessons
	
	
	var datafile;
	
	
	if(langtype=='en') datafile="data.xml";
	else datafile="data-np.xml";
	$.ajax({
			type: "GET",
			url: datafile,
			dataType: "xml",
			error: function(request, status, error){  alert(request.responseText);},
			beforeSend: function()
			{
						loading(); 
			},
			success: function(xml) 
			{
				closeloading();
				var i=0;
				$(xml).find("string").each(function () {
					
					dataArray[$(this).attr('id')]=$(this).text();
				});
			
				
				$(xml).find("chapter").each(function () {
					$(".lesson-intro").html($(this).text());
				});
				
				startlesson(lessoncount,lastlesson);
				
				$("#linkPrevLesson").click(function(){
					lessoncount--;						
					startlesson(lessoncount,lastlesson);
				});
				
				$("#linkNextLesson").click(function(){
					lessoncount++;						
					startlesson(lessoncount,lastlesson);
				});
				
			}//sucess
		});
		
		
			$("#closingID").click(function() {
	
			disablePopup();  // function close pop up
		});
			
			
			
		
		
		
});



function startlesson(lessoncount,lastlesson )
{
	if(lessoncount==1) $("#linkPrevLesson").hide(0);
	else  $("#linkPrevLesson").show(0);
	
	if(lessoncount==lastlesson) $("#linkNextLesson").hide(0);
	else  $("#linkNextLesson").show(0);
	
	$("#content").empty();
	
	switch(lessoncount)
	{
		case 1:
			$('#content').load("page1.html");
			break;
		case 2:
				$('#content').load("page2.html");
			break;
			
		/*case 3:
			intro3();
			break;
		case 4:
			siUnit();
			break;
		case 5:
			unitTable();
			break;
		*/
		default:
		{
			$('#content').append("<div>Sorry lesson could not be found</div>");
		}
	}
}



// page1 aniamtion
function animPage(){
	var animEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

	$("#lab1 > p:nth-of-type(1)").show(0).addClass('animated bounceInUp').one(animEnd, function() {
		$("#lab1 > p:nth-of-type(2)").show(0).addClass('animated bounceInUp').one(animEnd, function() {
			$("#uses > figure:nth-of-type(1)").show(0).addClass('animated bounceIn').one(animEnd, function() {
				$("#uses > figure:nth-of-type(2)").show(0).addClass('animated bounceIn').one(animEnd, function() {
					$("#uses > figure:nth-of-type(3)").show(0).addClass('animated bounceIn').one(animEnd, function() {
						$("#uses > figure:nth-of-type(4)").show(0).addClass('animated bounceIn').one(animEnd, function() {
							$("#uses > figure:nth-of-type(5)").show(0).addClass('animated bounceIn').one(animEnd, function() {
								$("#uses > figure:nth-of-type(6)").show(0).addClass('animated bounceIn').one(animEnd, function() {
									/*$(".footer-next > span").addClass('animated infinite shake');*/
									ole.footerNotificationHandler.pageEndSetNotification();
								});
							});
						});
					});
				});
			});
		});
	});
}

$(document).ready(function(){

	$('#lab1').prepend('<img class="bg_full" src='+$ref+'/page1/image/heat01.png /><span class="titletextyy">'+data.lesson.chapter+'</span>');
	loadTimelineProgress(2,1);
	$('#activity-page-next-btn-enabled').show(0);

	$('#activity-page-next-btn-enabled').click(function(){
		$('#activity-page-next-btn-enabled').hide(0);
		loadTimelineProgress(2,2);
		$('.bg_full,.titletextyy').hide(0);
		// pull data
		$("#lab1 > p:nth-of-type(1)").text(data.string.pg1s1);
		$("#lab1 > p:nth-of-type(2)").text(data.string.pg1s2);
		$("#uses > figure:nth-of-type(1) > figcaption").text(data.string.pg1s3);
		$("#uses > figure:nth-of-type(2) > figcaption").text(data.string.pg1s4);
		$("#uses > figure:nth-of-type(3) > figcaption").text(data.string.pg1s5);
		$("#uses > figure:nth-of-type(4) > figcaption").text(data.string.pg1s6);
		$("#uses > figure:nth-of-type(5) > figcaption").text(data.string.pg1s7);
		$("#uses > figure:nth-of-type(6) > figcaption").text(data.string.pg1s8);

		 // animation for the page
		 animPage();
	});

});

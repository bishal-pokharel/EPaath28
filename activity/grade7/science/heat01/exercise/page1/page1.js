// these are contents for answers
var ans=[
	{
		myMelt:false,
		myEvaporate:false,
		myCondense:false,
		myFreeze:false
	}
	
]

// flag keeps count of dropped elements
var droppedElements=0; var correctAns=0;

// start of document ready
$(document).ready(function() {	

	loadTimelineProgress(1,1);
	var $arrnextbtn=getArrowBtn();
  	$("#okay").html($arrnextbtn);	

	// pull these data
	$("#mainQn").text(data.string.e1s0);
	$("#dragMelt").text(data.string.e1s1);
	$("#dragEvaporate").text(data.string.e1s2);
	$("#dragCondense").text(data.string.e1s3);
	$("#dragFreeze").text(data.string.e1s4);
	// $("#userAns > div").text(data.string.e1s5);
	$("#result").text(data.string.e1s6);
	
// make the draggables
$("#draggable > p").draggable();

// make droppables
$("#userAns div").droppable({accept:"#draggable > p",
	over: function(overEvent,ui){
		$(this).css({"border-color":"#FFB100","box-shadow":"0em 0.1em 0.1em #FFB100"});
	},
	drop: function(dropEvent,ui){
		droppedElements++;
		// respective draggable
	var draggedData = ui.draggable.attr("data");
	var draggedContent =ui.draggable.html();
	ui.draggable.css({"display":"none"});
	   // active droppable
	var droppedData = $(this).attr('data');
	$(this).css({"border-color":"black","box-shadow":"0em 0.1em 0.1em #888888"});
	$(this).html(draggedContent);

	// for updating the ansCollect object variable to store the current droppable correctness- sorry
	// sometimes it is difficult to make it understand by code

		// check if dropped element is the correct one
		if(draggedData == droppedData){
				switch(droppedData){
					case "melt":ans.myMelt = true;break;
					case "evaporate":ans.myEvaporate = true;break;
					case "condense":ans.myCondense = true;break;
					case "freeze":ans.myFreeze = true;break;
					default:break;
				}
		}

		else if(draggedData != droppedData){
			switch(droppedData){
					case "melt":ans.myMelt = false;break;
					case "evaporate":ans.myEvaporate = false;break;
					case "condense":ans.myCondense = false;break;
					case "freeze":ans.myFreeze = false;break;
					default:break;
				}
		}

		// alert(currentDroppable+"="+ansCollect[mySeq[0]].currentDroppable);

		// check if all 3 elements are dropped
		if(droppedElements == 4){
			$("#okay").show(0);
			$("#draggable > p").draggable("disable");
			$("#userAns div").droppable("disable");
		}
	},
	out: function(outEvent,ui){
		$(this).css({"border-color":"black","box-shadow":"0em 0.1em 0.1em #888888"});
	}

});

// function to change color of the user answer after checking the answers 
function checkAnswer(){
	// css color codes for correct and incorrect answers
	var correctColorCode = {
			"color":"green",
			"border-color":"green",
			"box-shadow":"0em 0.1em 0.1em green"
		}
	var inCorrectColorCode = {
			"color":"#E4362E",
			"border-color":"#E4362E",
			"box-shadow":"0em 0.1em 0.1em #E4362E"
		}
	var isMeltCorrect = ans.myMelt;
	var isEvaporateCorrect = ans.myEvaporate;
	var isCondenseCorrect = ans.myCondense;
	var isFreezeCorrect = ans.myFreeze;

	// melt answer
	if(isMeltCorrect){
		correctAns++;
		$("#dropMelt").css(correctColorCode);
	}

	else if(!isMeltCorrect){
		$("#dropMelt").css(inCorrectColorCode);
	}

	// evaporate answer
	if(isEvaporateCorrect){
		correctAns++;
		$("#dropEvaporate").css(correctColorCode);
	}

	else if(!isEvaporateCorrect){
		$("#dropEvaporate").css(inCorrectColorCode);
	}

	// condense answer
	if(isCondenseCorrect){
		correctAns++;
		$("#dropCondense").css(correctColorCode);
	}

	else if(!isCondenseCorrect){
		$("#dropCondense").css(inCorrectColorCode);
	}

	// freeze answer
	if(isFreezeCorrect){
		correctAns++;
		$("#dropFreeze").css(correctColorCode);
	}

	else if(!isFreezeCorrect){
		$("#dropFreeze").css(inCorrectColorCode);
	}
}

// what to do on clicking next button
	$("#okay").on('click',function (){	
		$("#okay").css({"display":"none"});		
				checkAnswer();
		if(correctAns==4){
			$("#result").text(data.string.e1s7);
			$("#result").show(0);
			// $("#replay").show(0);
			/*$(".exerciseTab2").children('a:nth-of-type(2)').addClass('animated infinite flash');*/
			ole.footerNotificationHandler.pageEndSetNotification();
		}
		else if(correctAns < 4){
			// $("#result").text(data.string.e1s6);
			$("#result").show(0);
			$("#replay").show(0);
		}
	});
	
// next random question
	$("#replay").on('click',function (){
		location.reload();
	});
// end of document ready
})			
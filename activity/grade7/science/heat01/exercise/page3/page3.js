(function ($) {
/***
* most common variables
**/

var $whatnextbtn=getSubpageMoveButton($lang,"next");
 $("#activity-page-next-btn-enabled").html($whatnextbtn);


var $refImg = $ref+"/exercise/page3/images/",
$board = $('.board'),
$nextQuest = $("#activity-page-next-btn-enabled"),
$currentOptionBox, questionNumber=0,$explain,
animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

/*****************************
* data , content.num // and call it model ;)
******************************/
var content=[
	{
		Q:data.string.e3s1,
		choose:[
			{opImgPath:$refImg+"q1oc.png", opText:data.string.e3s2, correct:"correct"},
			{opImgPath:$refImg+"q1ow2.png", opText:data.string.e3s3},
			{opImgPath:$refImg+"q1ow1.png", opText:data.string.e3s4}
			],
		qImgPath:$refImg+"q1.gif",
		E:data.string.e3s33
	},
	{
		Q:data.string.e3s5,
		choose:[
			{opImgPath:$refImg+"q2ow1.png", opText:data.string.e3s6},
			{opImgPath:$refImg+"q2oc.png", opText:data.string.e3s7, correct:"correct"},
			{opImgPath:$refImg+"q2ow2.png", opText:data.string.e3s8}
			],
		qImgPath:$refImg+"q2.png",
		E:data.string.e3s34
	},
	{
		Q:data.string.e3s9,
		choose:[
			{opImgPath:$refImg+"q3ow1.png", opText:data.string.e3s10},
			{opImgPath:$refImg+"q3ow2.png", opText:data.string.e3s11},
			{opImgPath:$refImg+"q3oc.png", opText:data.string.e3s12, correct:"correct"}
			],
		qImgPath:$refImg+"q3.png",
		E:data.string.e3s35
	},
	{
		Q:data.string.e3s13 ,
		choose:[
			{opImgPath:$refImg+"q4oc.png", opText:data.string.e3s14, correct:"correct"},
			{opImgPath:$refImg+"q4ow2.png", opText:data.string.e3s15},
			{opImgPath:$refImg+"q4ow1.png", opText:data.string.e3s16}
			],
		qImgPath:$refImg+"q4.png",
		E:data.string.e3s36
	},
	{
		Q:data.string.e3s17,
		choose:[
			{opImgPath:$refImg+"q4oc.png", opText:data.string.e3s18},
			{opImgPath:$refImg+"q4ow1.png", opText:data.string.e3s19, correct:"correct"},
			{opImgPath:$refImg+"q4ow2.png", opText:data.string.e3s20}
			],
		qImgPath:$refImg+"q5.png",
		E:data.string.e3s37
	},
	{
		Q:data.string.e3s21 ,
		choose:[
			{opImgPath:$refImg+"q6ow1.png", opText:data.string.e3s22},
			{opImgPath:$refImg+"q6ow2.png", opText:data.string.e3s23},
			{opImgPath:$refImg+"q6oc.png", opText:data.string.e3s24, correct:"correct"}
			],
		qImgPath:$refImg+"q6.png",
		E:data.string.e3s38
	},
	{
		Q:data.string.e3s25 ,
		choose:[
			{opImgPath:$refImg+"q7oc.png", opText:data.string.e3s26, correct:"correct"},
			{opImgPath:$refImg+"q7ow2.png", opText:data.string.e3s27},
			{opImgPath:$refImg+"q7ow1.png", opText:data.string.e3s28}
			],
		qImgPath:$refImg+"q7.png",
		E:data.string.e3s39
	},
	{
		Q:data.string.e3s29 ,
		choose:[
			{opImgPath:$refImg+"q8ow1.png", opText:data.string.e3s30},
			{opImgPath:$refImg+"q8oc.png", opText:data.string.e3s31, correct:"correct"},
			{opImgPath:$refImg+"q8ow2.png", opText:data.string.e3s32}
			],
		qImgPath:$refImg+"q8.png",
		E:data.string.e3s40
	}
	]


/**
* after selection of correct option first question
 these functions are executed which are scene specific
*/
	function scene0afterOptionSelect(){
		var miscDiv = $board.children('.misc');
		var miscImage = miscDiv.children("img");
		miscImage.attr('src', $refImg+"a1.gif");
		setTimeout(function(){$explain.show(0);$nextQuest.show(0);}, 3000);
		
	}

/**
* on clicking options
*/
function optionSelectCheck () {
	var inCorrectCss={"color":"#FFC20E","cursor":"default","background":"#747371","pointer-events":"none"};
	var correctCss={"color":"black","cursor":"default","background":"#FFFFCC","pointer-events":"none","box-shadow":"none"};
	
	$currentOptionBox.on('click', 'figure', function() {
		var selected =$(this);
		var myOption = selected.attr("data-check");
		var caption = selected.children('figcaption');
		var siblingCaption = selected.siblings('figure').children('figcaption');
		if(myOption != "correct"){
			// all the color codes and information
			caption.children('.correctSign').addClass('glyphicon-remove').css({"color":"red"}).show(0);
			selected.css(inCorrectCss);
			selected.addClass('animated shake');
			selected.off("click");
		}

		else if(myOption == "correct"){
			// all the color codes and information
			caption.children('.correctSign').addClass('glyphicon-ok').css({"color":"green"}).show(0);
			selected.css(correctCss);
			selected.siblings('figure').css(inCorrectCss);
			siblingCaption.children('span').addClass('glyphicon-remove').css({"color":"red"}).show(0);
			selected.addClass('animated bounce').one(animationEnd,function(){
				if(questionNumber == 0){
					scene0afterOptionSelect();
				}
				else{
					if(questionNumber < 7){
						$explain.show(0);
						$nextQuest.show(0);}
					else if(questionNumber == 7){
						$explain.show(0);
						$nextQuest.show(0);
					}
					
				}
 		});
		}
	});
}

/**
* function to highlight question
*/

	function highlightQuestion(num){
		switch(num){
			case 0:case 2:case 4:case 6:
			$(".question").css({"color":"#88C100"});
			break;

			case 1:case 3:case 5:case 7:
			$(".question").css({"color":"#FF003C"});
			break;

			default:break;
		}
	}

/***************
* Lets call it View // which template to select
****************/
function myTemplate(num){

	loadTimelineProgress(8,(num+1));

		var source = $("#questions-template").html(); //why need one extra var :P
		var template = Handlebars.compile(source);
		var html = template(content[num]);
		$board.html(html);
		$currentOptionBox = $board.children('.optionBox');
		$explain = $board.children('.explain');
		// make sure this is called for each new handlebar template
		highlightQuestion(num);
		optionSelectCheck();
	}

/***************
* // events to occur on clicking nextQuestion button
****************/
$nextQuest.on('click',function() {
	$(this).css('display','none');
	questionNumber++;
	if(questionNumber > 7){
		ole.activityComplete.finishingcall();
	}
	else{
		myTemplate(questionNumber);
	}
	
});

// at start call my template zero scene and func0
myTemplate(0);

})(jQuery)
// these are contents for answers
var ans=[
	{
		myVacuum:false,
		myAlcohol:false,
		myIndicator:false,
		myMax:false,
		myMin:false,
		myMercury:false
	}
	
]

// flag keeps count of dropped elements
var droppedElements=0; var correctAns=0;

// start of document ready
$(document).ready(function() {	
	loadTimelineProgress(1,1);
	var $arrnextbtn=getArrowBtn();
  	$("#okay").html($arrnextbtn);
	// pull these data
	$("#mainQn").text(data.string.e2s0);
	$("#dragVacuum").text(data.string.e2s1);
	$("#dragAlcohol").text(data.string.e2s2);
	// $("#dragIndicator").text(data.string.e2s3);
	$("#dragMax").text(data.string.e2s4);
	$("#dragMin").text(data.string.e2s5);
	$("#dragMercury").text(data.string.e2s6);
	// $("#userAns > div").text(data.string.e1s5);
	$("#result").text(data.string.e2s7);
	
// make the draggables
$("#draggable > p").draggable();

// make droppables
$("#userAns div").droppable({accept:"#draggable > p",
	over: function(overEvent,ui){
		$(this).css({"border-color":"#FFB100","box-shadow":"0em 0.1em 0.1em #FFB100"});
	},
	drop: function(dropEvent,ui){
		droppedElements++;
		// respective draggable
	var draggedData = ui.draggable.attr("data");
	var draggedContent =ui.draggable.html();
	ui.draggable.css({"display":"none"});
	   // active droppable
	var droppedData = $(this).attr('data');
	$(this).css({"border-color":"black","box-shadow":"0em 0.1em 0.1em #888888"});
	$(this).html(draggedContent);

	// for updating the ansCollect object variable to store the current droppable correctness- sorry
	// sometimes it is difficult to make it understand by code

		// check if dropped element is the correct one
		if(draggedData == droppedData){
				switch(droppedData){
					case "vacuum":ans.myVacuum = true;break;
					case "alcohol":ans.myAlcohol = true;break;
					// case "indicator":ans.myIndicator = true;break;
					case "max":ans.myMax = true;break;
					case "min":ans.myMin = true;break;
					case "mercury":ans.myMercury = true;break;
					default:break;
				}
		}

		else if(draggedData != droppedData){
			switch(droppedData){
					case "vacuum":ans.myVacuum = false;break;
					case "alcohol":ans.myAlcohol = false;break;
					// case "indicator":ans.myIndicator = false;break;
					case "max":ans.myMax = false;break;
					case "min":ans.myMin = false;break;
					case "mercury":ans.myMercury = false;break;
					default:break;
				}
		}

		// alert(currentDroppable+"="+ansCollect[mySeq[0]].currentDroppable);

		// check if all 3 elements are dropped
		if(droppedElements == 5){
			$("#okay").show(0);
			$("#draggable > p").draggable("disable");
			$("#userAns div").droppable("disable");
		}
	},
	out: function(outEvent,ui){
		$(this).css({"border-color":"black","box-shadow":"0em 0.1em 0.1em #888888"});
	}

});

// function to change color of the user answer after checking the answers 
function checkAnswer(){
	// css color codes for correct and incorrect answers
	var correctColorCode = {
			"color":"green",
			"border-color":"green",
			"box-shadow":"0em 0.1em 0.1em green"
		}
	var inCorrectColorCode = {
			"color":"#E4362E",
			"border-color":"#E4362E",
			"box-shadow":"0em 0.1em 0.1em #E4362E"
		}
	var isVacuumCorrect = ans.myVacuum;
	var isAlcoholCorrect = ans.myAlcohol;
	// var isIndicatorCorrect = ans.myIndicator;
	var isMaxCorrect = ans.myMax;
	var isMinCorrect = ans.myMin;
	var isMercuryCorrect = ans.myMercury;

	// vacuum answer
	if(isVacuumCorrect){
		correctAns++;
		$("#dropVacuum").css(correctColorCode);
	}

	else if(!isVacuumCorrect){
		$("#dropVacuum").css(inCorrectColorCode);
	}

	// alcohol answer
	if(isAlcoholCorrect){
		correctAns++;
		$("#dropAlcohol").css(correctColorCode);
	}

	else if(!isAlcoholCorrect){
		$("#dropAlcohol").css(inCorrectColorCode);
	}

	// indicator answer
	/*if(isIndicatorCorrect){
		correctAns++;
		$("#dropIndicator").css(correctColorCode);
	}

	else if(!isIndicatorCorrect){
		$("#dropIndicator").css(inCorrectColorCode);
	}*/

	// max answer
	if(isMaxCorrect){
		correctAns++;
		$("#dropMax").css(correctColorCode);
	}

	else if(!isMaxCorrect){
		$("#dropMax").css(inCorrectColorCode);
	}

	// min answer
	if(isMinCorrect){
		correctAns++;
		$("#dropMin").css(correctColorCode);
	}

	else if(!isMinCorrect){
		$("#dropMin").css(inCorrectColorCode);
	}

	// mercury answer
	if(isMercuryCorrect){
		correctAns++;
		$("#dropMercury").css(correctColorCode);
	}

	else if(!isMercuryCorrect){
		$("#dropMercury").css(inCorrectColorCode);
	}
}

// what to do on clicking next button
	$("#okay").on('click',function (){	
		$("#okay").css({"display":"none"});		
				checkAnswer();
		if(correctAns==5){
			// alert("all correct");
			$("#result").text(data.string.e2s8);
			$("#result").show(0);
			// $("#replay").show(0);
			/*$(".exerciseTab2").children('a:nth-of-type(3)').addClass('animated infinite flash');*/
			ole.footerNotificationHandler.pageEndSetNotification();
		}
		else if(correctAns < 5){
			// $("#result").text(data.string.e1s6);
			$("#result").show(0);
			$("#replay").show(0);
		}
	});
	
// next random question
	$("#replay").on('click',function (){
		location.reload();
	});
// end of document ready
})			
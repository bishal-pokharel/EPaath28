// for slide 1 next button next counter
var next = 1;

// for slide's next button next counter
var nextSlide = 2;

// for slide's previous button previous counter
var previousSlide = 1;

// flag to see if these numbered slides are seen at least once
var slide4Saw = false;
var slide5Saw = false;

/*flag to store the state of if 6th slide is first visit*/
var slide6firstVisit = true;

$(document).ready(function() {

	var $arrnextbtn=getArrowBtn();
  	$("#nxtLine").html($arrnextbtn);


  	var $whatnextbtn=getSubpageMoveButton($lang,"next");
  	$("#activity-page-next-btn-enabled").html($whatnextbtn);

  	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
  	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

  	 loadTimelineProgress(6,1);

	$("#topic").text(data.string.pg4s1);
	$("#slide1 > p:nth-of-type(1)").text(data.string.pg4s7);
	$("#vaccum").text(data.string.pg4s2);
	$("#alcohol").text(data.string.pg4s3);
	$("#mercury").text(data.string.pg4s4);
	$("#slide1 > ul > li:nth-of-type(2)").text(data.string.pg4s8);
	$("#slide1 > ul > li:nth-of-type(3)").text(data.string.pg4s9);
	$("#slide1 > ul > li:nth-of-type(1)").text(data.string.pg4s10);
	$("#slide1 > ul > li:nth-of-type(4)").text(data.string.pg4s11);
	$("#slide2 > p:nth-of-type(1)").text(data.string.pg4s12);
	$("#min, #minLabel, #minLabel6").text(data.string.pg4s13);
	$("#slide3 > p:nth-of-type(1)").text(data.string.pg4s15);
	$("#max, #maxLabel, #maxLabel6").text(data.string.pg4s14);
	$("#slide4 > p:nth-of-type(1)").text(data.string.pg4s16);
	$("#maxPut4 > strong, #maxPut6 > strong").text(data.string.pg4s17);
	$("#minPut4 > strong, #minPut6 > strong").text(data.string.pg4s18);
	$("#help4, #help6").text(data.string.pg4s19);
	$("#slide5 > p:nth-of-type(1)").text(data.string.pg4s21);
	$("#instr5").text(data.string.pg4s22);
	$("#reset").text(data.string.pg4s23);
	$("#slide6 > p:nth-of-type(1)").text(data.string.pg4s24);
	$("#instr6").text(data.string.pg4s25);

	ole.parseToolTip("#slide1 > ul > li:nth-of-type(2)","अल्कोहल");
	ole.parseToolTip("#slide1 > ul > li:nth-of-type(2)","पारो");
	$("#slide1 > ul > li:nth-of-type(2) > span").addClass('animated infinite flash');
	ole.parseToolTip("#slide2 > p:nth-of-type(1)","न्यून तापक्रम");
	ole.parseToolTip("#slide3 > p:nth-of-type(1)","अधिकतम तापक्रम");

	// what to do on hovering over the span tags in slide one
	$("#slide1 > ul > li:nth-of-type(2) > span:nth-of-type(1)").hover(function(){
		/* Stuff to do when the mouse enters the element */
		$("#alcohol").addClass('animated wobble');
		$(this).removeClass("animated infinite flash");
		}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#alcohol").removeClass('animated wobble');
	});

	// what to do on hovering over the span tags in slide one
	$("#slide1 > ul > li:nth-of-type(2) > span:nth-of-type(2)").hover(function(){
		/* Stuff to do when the mouse enters the element */
		$("#mercury").addClass('animated wobble');
		$(this).removeClass("animated infinite flash");
		}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#mercury").removeClass('animated wobble');
	});

	// slide1 nxtLine button
	$("#nxtLine").on('click', function() {


		if(next==1){
			$("#slide1 > ul > li:nth-of-type(1)").show(0);
			next++;
		}
		else if(next==2){
			$("#slide1 > ul > li:nth-of-type(2)").show(0);
			next++;
		}
		else if(next==3){
			$("#slide1 > ul > li:nth-of-type(3)").show(0);
			next++;
		}
		else if(next==4){
			$("#slide1 > ul > li:nth-of-type(4)").show(0);
			$("#nxtLine").off("click");
			$("#nxtLine").css({"display":"none"});
			$("#activity-page-next-btn-enabled").show(0);
		}
	});

	// slide 4 min input
	$("#minPut4 > input").keyup(function() {
		// if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		// check for the input answer if ans is positive
			if(inp == "15"){
					$("#minPut4 > input").css({"color":"darkred"});
					// change the answer sign to correct
					$("#minPut4 > span").removeClass("glyphicon-remove").addClass('glyphicon-ok').css({"color":"green"}).show(0);
					$("#minPut4 > input").prop('disabled', true);
					$("#minLabel").removeClass("animated infinite flash");
					$("#minRead").css({"display":"none"});
					$("#maxPut4").show(100,function(){
						$("#maxLabel").addClass("animated infinite flash");
						$("#maxRead").show(0);
						$("#maxPut4 > input").focus();
						$("#help4").text(data.string.pg4s20);
					});
			}

			else{
				  $("#minPut4 > input").css({"color":"#D03030"});
				  // change the answer sign to wrong
				  $("#minPut4 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
				}
	});

	// slide 4 max input
	$("#maxPut4 > input").keyup(function() {
		// if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		// check for the input answer if ans is positive
			if(inp == "25"){
					$("#maxPut4 > input").css({"color":"darkred"});
					// change the answer sign to correct
					$("#maxPut4 > span").removeClass("glyphicon-remove").addClass('glyphicon-ok').css({"color":"green"}).show(0);
					$("#maxPut4 > input").prop('disabled', true);
					$("#maxLabel").removeClass("animated infinite flash");
					$("#maxRead").css({"display":"none"});
					$("#help4").css({"display":"none"});
					nextSlide=5;
					$("#activity-page-next-btn-enabled").show(0);
					$("#activity-page-prev-btn-enabled").show(0);
					// if slide 4 is not seen fully at least once
					slide4Saw = true;
			}

			else{
				  $("#maxPut4 > input").css({"color":"#D03030"});
				  // change the answer sign to wrong
				  $("#maxPut4 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
				}
	});

	// slide 6 min input
	$("#minPut6 > input").keyup(function() {
		// if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		// check for the input answer if ans is positive
			if(inp == "10"){
					$("#minPut6 > input").css({"color":"darkred"});
					// change the answer sign to correct
					$("#minPut6 > span").removeClass("glyphicon-remove").addClass('glyphicon-ok').css({"color":"green"}).show(0);
					$("#minPut6 > input").prop('disabled', true);
					$("#minLabel6").removeClass("animated infinite flash");
					$("#minRead6").css({"display":"none"});
					$("#maxPut6").show(100,function(){
						$("#maxLabel6").addClass("animated infinite flash");
						$("#maxRead6").show(0);
						$("#maxPut6 > input").focus();
						$("#help6").text(data.string.pg4s20);
					});
			}

			else{
				  $("#minPut6 > input").css({"color":"#D03030"});
				  // change the answer sign to wrong
				  $("#minPut6 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
				}
	});

	// slide 6 max input
	$("#maxPut6 > input").keyup(function() {
		// if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		// check for the input answer if ans is positive
			if(inp == "30"){
					$("#maxPut6 > input").css({"color":"darkred"});
					// change the answer sign to correct
					$("#maxPut6 > span").removeClass("glyphicon-remove").addClass('glyphicon-ok').css({"color":"green"}).show(0);
					$("#maxPut6 > input").prop('disabled', true);
					$("#maxLabel6").removeClass("animated infinite flash");
					$("#maxRead6").css({"display":"none"});
					$("#help6").css({"display":"none"});
					// if slide 6 is not seen fully at least once
					previousSlide = 5;
					$("#activity-page-next-btn-enabled").removeClass("animated infinite shake");
					$("#activity-page-prev-btn-enabled").show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
			}

			else{
				  $("#maxPut6 > input").css({"color":"#D03030"});
				  // change the answer sign to wrong
				  $("#maxPut6 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
				}
	});

	// what to do on clicking the reset button
	$("#reset").on('click',function() {
		$("#thermo5 > img").attr("src",$ref+"/page4/image/reset.gif");
			// hide the reset and statement
			$("#reset").css({"display":"none"});
			$("#instr5").css({"display":"none"});
		$('#thermo5 > img').load(function(){
			// when gif completes approx 1.5seconds do this
			setTimeout(function(){
				$("#reset").css({"display":"none"});
				$("#instr5").text(data.string.pg4s26).show(500,function(){
					nextSlide=6;
					$("#activity-page-next-btn-enabled").show(0);
					$("#activity-page-prev-btn-enabled").show(0);
					// slide 5 seen flag set to true
					slide5Saw = true;
				});
			}, 1500);
		});
	});

	// counter for time timeTravel
	var timeCount=0;
	// what to do on clicking the timeTravel button
	$("#timeTravel").on('click',function() {
		timeCount++;
		// hide the timeTravel Button and the instruction
		$("#info6").css({"display":"none"});
		switch(timeCount){
			case 1: $("#morning").animate({"opacity":"0"},3500,"linear");
					$("#afternoon").animate({"opacity":"1"},3500,"linear");
					$("#info6 > p:nth-of-type(1)").text(data.string.pg4s28);
					$("#lastThermo").attr("src",$ref+"/page4/image/sixs6-12.gif");
					$("#clock").attr("src",$ref+"/page4/image/clock6-12.gif");
					// when gif completes approx 3seconds do this
						setTimeout(function(){
							$("#info6").show(0);
						},4000);
					break;
			case 2: $("#afternoon").animate({"opacity":"0"},3500,"linear");
					$("#evening").animate({"opacity":"1"},3500,"linear");
					$("#info6 > p:nth-of-type(1)").text(data.string.pg4s29);
					$("#lastThermo").attr("src",$ref+"/page4/image/sixs12-18.gif");
					$("#clock").attr("src",$ref+"/page4/image/clock12-6.gif");
					// when gif completes approx 3seconds do this
						setTimeout(function(){
							$("#info6").show(0);
						},4000);
					break;
			case 3: $("#evening").animate({"opacity":"0"},3500,"linear");
					$("#midnight").animate({"opacity":"1"},3500,"linear");
					$("#info6 > p:nth-of-type(1)").text(data.string.pg4s30);
					$("#lastThermo").attr("src",$ref+"/page4/image/sixs18-00.gif");
					$("#clock").attr("src",$ref+"/page4/image/clock6-12.gif");
					// when gif completes approx 3seconds do this
						setTimeout(function(){
							$("#info6").show(0);
						},4000);
					break;
			case 4:	$("#midnight").animate({"opacity":"0"},3500,"linear");
					$("#morning").animate({"opacity":"1"},3500,"linear");
					$("#lastThermo").attr("src",$ref+"/page4/image/sixs00-6.gif");
					$("#clock").attr("src",$ref+"/page4/image/clock12-6.gif");
					// when gif completes approx 3seconds do this
						setTimeout(function(){
							$("#slide6 > p:nth-of-type(1)").text(data.string.pg4s16).css({"color":"darkblue"});
							$("#reading6").show(0);
							$("#minLabel6").addClass("animated infinite flash");
							$("#minRead6").show(0);
							$("#minPut6 > input").focus();
						},4000);
					break;
			default:break;
		}

		});

	// next slide button on click function
	$("#activity-page-next-btn-enabled").on('click', function() {

		loadTimelineProgress(6,nextSlide);
		if(nextSlide==2){
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(1), #lab4 > div:nth-of-type(n+3)").hide(0);
			// make sure slide2 is shown
			$("#lab4 > div:nth-of-type(2)").show(0);
			$("#activity-page-prev-btn-enabled").show(0);
			$("#activity-page-next-btn-enabled").show(0);
			nextSlide=3;
			previousSlide=1;
		}

		else if(nextSlide==3){
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(-n+2), #lab4 > div:nth-of-type(n+4)").hide(0);
			// make sure slide3 is shown
			$("#lab4 > div:nth-of-type(3)").show(0);
			$("#activity-page-prev-btn-enabled").show(0);
			$("#activity-page-next-btn-enabled").show(0);
			nextSlide=4;
			previousSlide=2;
		}

		else if(nextSlide==4){
			$("#activity-page-next-btn-enabled").hide(0);
			previousSlide=3;
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(-n+3), #lab4 > div:nth-of-type(n+5)").hide(0);
			// make sure slide4 is shown
			$("#lab4 > div:nth-of-type(4)").show(0);
			// if slide 4 is not seen fully at least once
			if(!slide4Saw){
				$("#activity-page-next-btn-enabled").css({"display":"none"});
				$("#minPut4 > input").focus();
			}

			else{
				$("#activity-page-prev-btn-enabled").show(0);
				$("#activity-page-next-btn-enabled").show(0);
				nextSlide=5;
				previousSlide=3;
			}

		}

		else if(nextSlide==5){
			$("#activity-page-next-btn-enabled").hide(0);
			previousSlide=4;
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(-n+4), #lab4 > div:nth-of-type(6)").hide(0);
			// make sure slide5 is shown
			$("#lab4 > div:nth-of-type(5)").show(0);
			// if slide 5 is not seen fully at least once
			if(!slide5Saw){
				$("#activity-page-next-btn-enabled").css({"display":"none"});
			}

			else{
				$("#activity-page-prev-btn-enabled").show(0);
				$("#activity-page-next-btn-enabled").show(0);
				nextSlide=6;
				previousSlide=4;
			}
		}

		else if(nextSlide==6){
			$("#activity-page-next-btn-enabled").hide(0);
			$("#activity-page-prev-btn-enabled").show(0);
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(-n+5)").hide(0);
			// make sure slide6 is shown
			$("#lab4 > div:nth-of-type(6)").show(0);
			previousSlide=5;
			if(!slide6firstVisit){
				ole.footerNotificationHandler.pageEndSetNotification();
			}
			slide6firstVisit = false;
		}

		/*console.log("NEXT-BUTTON -> nextSlide: "+nextSlide+"previousSlide: "+previousSlide);*/
	});

// next slide button on click function
	$("#activity-page-prev-btn-enabled").on('click', function() {
		loadTimelineProgress(6,previousSlide);

		if(previousSlide==1){
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(n+2)").hide(0);
			// make sure slide2 is shown
			$("#lab4 > div:nth-of-type(1)").show(0);
			$("#activity-page-prev-btn-enabled").hide(0);
			$("#activity-page-next-btn-enabled").show(0);
			nextSlide=2;
		}

		if(previousSlide==2){
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(1), #lab4 > div:nth-of-type(n+3)").hide(0);
			// make sure slide3 is shown
			$("#lab4 > div:nth-of-type(2)").show(0);
			$("#activity-page-prev-btn-enabled").show(0);
			$("#activity-page-next-btn-enabled").show(0);
			nextSlide=3;
			previousSlide=1;
		}

		else if(previousSlide==3){
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(-n+2), #lab4 > div:nth-of-type(n+4)").hide(0);
			// make sure slide4 is shown
			$("#lab4 > div:nth-of-type(3)").show(0);
			$("#activity-page-prev-btn-enabled").show(0);
			$("#activity-page-next-btn-enabled").show(0);
			nextSlide=4;
			previousSlide=2;

		}

		else if(previousSlide==4){
			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(-n+3), #lab4 > div:nth-of-type(n+5)").hide(0);
			// make sure slide5 is shown
			$("#lab4 > div:nth-of-type(4)").show(0);
			// if slide 5 is not seen fully at least once
			$("#activity-page-prev-btn-enabled").show(0);
			$("#activity-page-next-btn-enabled").show(0);
			nextSlide=5;
			previousSlide=3;
		}

		else if(previousSlide==5){

			// hide all slide divs except slide 2
			$("#lab4 > div:nth-of-type(-n+4), #lab4 > div:nth-of-type(6)").hide(0);
			// make sure slide6 is shown
			$("#lab4 > div:nth-of-type(5)").show(0);
			$("#activity-page-next-btn-enabled").show(0);
			$("#activity-page-prev-btn-enabled").show(0);
			nextSlide=6;
			previousSlide=4;
			ole.footerNotificationHandler.hideNotification();
		}
		/*console.log("PREV-BUTTON -> nextSlide: "+nextSlide+"  previousSlide: "+previousSlide);*/
	});

// end of document ready
});

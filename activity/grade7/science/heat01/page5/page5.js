/*what to do after drop, strdrag/drop->selects the respective drag and drop div , ht -> gives how much the mercury to raise */
function afterDrop(strdrop,strdrag,ht) 
{	 
$("#drag"+strdrag).draggable("disable");
$("#drag"+strdrag).hide(0);
$("#drop"+strdrop).droppable("disable"); 
$("#drop"+strdrop).children('img').show(0);
switch(strdrop)
		{
			case "a" : $("#info > p:nth-of-type(1)").html(data.string.pg5s9);break;
			case "b" : $("#info > p:nth-of-type(1)").html(data.string.pg5s10); break;
			case "c" : $("#info > p:nth-of-type(1)").html(data.string.pg5s11); break;
			case "d" : $("#info > p:nth-of-type(1)").html(data.string.pg5s12); break;
			case "e" : $("#info > p:nth-of-type(1)").html(data.string.pg5s13); break;
			case "f" : $("#info > p:nth-of-type(1)").html(data.string.pg5s14); break;
			default : break;
		}
$("#mercury").velocity({"height": ht},1000,"linear");	
}

$(document).ready(function() {

	loadTimelineProgress(1,1);	

		// pull data
		$("#draga > figure > figcaption").text(data.string.pg5s1);
  		$("#dragb > figure > figcaption").text(data.string.pg5s2);
  		$("#dragc > figure > figcaption").text(data.string.pg5s3);
  		$("#dragd > figure > figcaption").text(data.string.pg5s4);
  		$("#drage > figure > figcaption").text(data.string.pg5s5);
  		$("#dragf > figure > figcaption").text(data.string.pg5s6);
  		$("#info > p:nth-of-type(1)").html(data.string.pg5s7);
  		$("#info > p:nth-of-type(2)").html(data.string.pg5s8);
  		$("#complete").html(data.string.pg5s15);

		var i=0; // interger to calculate total number of dropped elements

		//draggable and droppable
		$( "#draggable" ).children("div").draggable({containment: "#mainPage",delay: 100,cursor: "move",
		revert: "invalid"});

		$("#dropa").droppable({ 
			accept: "#dragb", tolerance:"pointer",
			drop: function(){afterDrop("a", "b", "57%");
			i++; changeHelp(i); }}
			);

		$("#dropb").droppable({ 
			accept: "#dragf", tolerance:"pointer", 
			drop: function(){afterDrop("b", "f", "49%")
			;i++;changeHelp(i);}})
		;
		$("#dropc").droppable({ 
			accept: "#dragc", tolerance:"pointer", 
			drop: function(){afterDrop("c", "c", "37%");
			i++;changeHelp(i);}});

		$("#dropd").droppable({ 
			accept: "#drage", tolerance:"pointer", 
			drop: function(){afterDrop("d", "e", "28%")
			;i++;changeHelp(i);}});

		$("#drope").droppable({ 
			accept: "#draga", tolerance:"pointer", 
			drop: function(){afterDrop("e", "a", "14%");
			i++;changeHelp(i);}});

		$("#dropf").droppable({ 
			accept: "#dragd", tolerance:"pointer", 
			drop: function(){afterDrop("f", "d", "6%");
			++i;changeHelp(i);}});

		// close button functionality
		$("#info > span").on('click',function() {
			$("#info").hide(0);
		});

		$("#replay").on('click',function() {
			location.reload(true);
		});
// end of document ready
});

function changeHelp(i)
{
	if(i == 1)
		{	
			
			$("#info > p:nth-of-type(2)").hide(0);
		}

	else if(i==6){
		$("#complete, #replay").show(0);
		ole.footerNotificationHandler.lessonEndSetNotification();
	}
}
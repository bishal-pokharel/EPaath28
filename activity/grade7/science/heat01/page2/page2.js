// timestamp to start gif's anew
var timestamp = new Date().getTime();

$(document).ready(function(){
  loadTimelineProgress(1,1);

  // pull data
  $("#mainPart > p:nth-of-type(1)").text(data.string.pg2s1);
  $("#effectList1 > li:nth-of-type(1)").text(data.string.pg2s2);
  $("#effectList1 > li:nth-of-type(2)").text(data.string.pg2s3);
  $("#effectList2 > li:nth-of-type(1)").text(data.string.pg2s4);
  $("#effectList2 > li:nth-of-type(2)").text(data.string.pg2s5);
  $("#mainPart > p:nth-of-type(2)").text(data.string.pg2s6);
  $("#solid > h1, #s > figcaption").text(data.string.pg2s11);
  $("#liquid > h1, #l > figcaption").text(data.string.pg2s12);
  $("#gas > h1, #g > figcaption").text(data.string.pg2s13);
  $(".insideState > div:nth-of-type(2) > p:nth-of-type(1)").text(data.string.pg2s7);
  $(".insideState > div:nth-of-type(2) > p:nth-of-type(2)").text(data.string.pg2s8);
  $(".insideState > div:nth-of-type(2) > p:nth-of-type(3)").text(data.string.pg2s9);
  $(".insideState > div:nth-of-type(2) > p:nth-of-type(4)").text(data.string.pg2s10);

  $("#boxSt > p:nth-of-type(1)").text(data.string.pg2s20);
  $("#boxSt > p:nth-of-type(2)").text(data.string.pg2s21);

  $("#boxSs > p:nth-of-type(1)").text(data.string.pg2s14);
  $("#boxSs > p:nth-of-type(2)").text(data.string.pg2s15);

  $("#boxSv > p:nth-of-type(1)").text(data.string.pg2s16);
  $("#boxSv > p:nth-of-type(2)").text(data.string.pg2s17);

  $("#boxSc > p:nth-of-type(1)").text(data.string.pg2s18);
  $("#boxSc > p:nth-of-type(2)").text(data.string.pg2s19);

  $("#boxLt > p:nth-of-type(1)").text(data.string.pg2s27);
  $("#boxLt > p:nth-of-type(2)").text(data.string.pg2s28);

  $("#boxLs > p:nth-of-type(1)").text(data.string.pg2s22);
  $("#boxLs > p:nth-of-type(2)").text(data.string.pg2s23);

  $("#boxLv > p:nth-of-type(1)").text(data.string.pg2s24);
  $("#boxLv > p:nth-of-type(2)").text(data.string.pg2s25);

  $("#boxLc > p:nth-of-type(1)").text(data.string.pg2s18);
  $("#boxLc > p:nth-of-type(2)").text(data.string.pg2s26);

  $("#boxGt > p:nth-of-type(1)").text(data.string.pg2s34);
  $("#boxGt > p:nth-of-type(2)").text(data.string.pg2s35);

  $("#boxGv > p:nth-of-type(1)").text(data.string.pg2s31);
  $("#boxGv > p:nth-of-type(2)").text(data.string.pg2s32);

  $("#boxGs > p:nth-of-type(1)").text(data.string.pg2s29);
  $("#boxGs > p:nth-of-type(2)").text(data.string.pg2s30);

  $("#boxLt > em, #boxLv > em, #boxGv > em").text(data.string.pg2s36);

  // pause and play all pauseAnim classes animation on hover
  $("#state > figure").hover(function() {
  	/* Stuff to do when the mouse enters the element */
  	$(".pauseAnim").addClass('pauseMe');
  }, function() {
  	/* Stuff to do when the mouse leaves the element */
  	$(".pauseAnim").removeClass('pauseMe');
  });

  // on clicking the options
  $("#state > figure").on('click', function() {
    var selected = $(this).attr("id");

    // for fresh thermometer gif to be used everytime clicking
      var timestamp = new Date().getTime();
      var thermoAnim = $ref+"/page2/image/triseHigh.gif?"+timestamp;

    switch(selected){
        case "s": $("#solid").show(0);
                  $("#solid > div > div > img:nth-of-type(2)").attr("src",thermoAnim);
                  break;
        case "l": $("#liquid").show(0);
                  $("#liquid > div > div > img:nth-of-type(2)").attr("src",thermoAnim);
                  break;
        case "g": $("#gas").show(0);
                  $("#gas > div > div > img:nth-of-type(2)").attr("src",thermoAnim);
                  break;
        default:break;
      }

      ole.footerNotificationHandler.hideNotification();
  });

  // close button functionality for each inside state divs
  $(".closeState").on('click', function() {
    var me=$(this);
    me.parent("div").hide(0);

      var myLiquidTemperatureNephewImg = $("#boxLt").children('img');
      // myLiquidTemperatureNephewImg.attr("src",$ref+"/page2/image/lt.png");
      myLiquidTemperatureNephewImg.attr("src",$ref+"/page2/image/lv.png");

      var myLiquidVolumeNephewImg = $("#boxLv").children('img');
      myLiquidVolumeNephewImg.attr("src",$ref+"/page2/image/lv.png");

       var myGasVolumeNephewImg = $("#boxGv").children('img');
      myGasVolumeNephewImg.attr("src",$ref+"/page2/image/gv.png");

      ole.footerNotificationHandler.pageEndSetNotification();
  });

   // clicking rhe types of changes inside insideState divs
  $(".insideState > div:nth-of-type(2) > p").on('click',function() {
    var me = $(this);
    var myName = me.attr("id");
    var myParent = me.parent("div");
    var myGrampa = myParent.parent("div").attr("id");
    var myUncle = myParent.siblings("div:nth-of-type(1)");
    var myCousins = myUncle.children("div");
    var mySiblings = me.siblings('p');
        mySiblings.css({"background":"#ffc835"});
        me.css({"background":"#E04946"});

    // remove once and for all the blinking buttons
    myParent.children('p').removeClass('animated infinite flash');

    // for fresh thermometer gif to be used everytime clicking
      var thermoAnim = $ref+"/page2/image/triseHigh.gif?"+timestamp;

    // hide all box divs
       myCousins.hide(0);
      switch(myName){
        case "ss":$("#boxSs").show(0);
                break;
        case "sv":$("#boxSv").show(0);
                break;
        case "sc":$("#boxSc").show(0);
                break;
        case "st":$("#boxSt").show(0);
                  $("#solid > div > div > img:nth-of-type(2)").attr("src",thermoAnim);
                break;
        case "ls":$("#boxLs").show(0);
                break;
        case "lv":$("#boxLv").show(0);
                break;
        case "lc":$("#boxLc").show(0);
                break;
        case "lt":$("#boxLt").show(0);
                  $("#liquid > div > div > img:nth-of-type(2)").attr("src",$ref+"/page2/image/lv.png");
                  // $("#liquid > div > div > img:nth-of-type(2)").attr("src",thermoAnim);
                break;
        case "gs":$("#boxGs").show(0);
                break;
        case "gv":$("#boxGv").show(0);
                break;
        case "gc":$("#boxGc").show(0);
                break;
        case "gt":$("#boxGt").show(0);
                  $("#gas > div > div > img:nth-of-type(2)").attr("src",thermoAnim);
                break;
        default:break;
      }
  });

/****
***
  liquid temperature change
***
***/
 $("#boxLt").on('click', 'img', function() {
   $("#boxLt").children('img').attr('src', $ref+"/page2/image/lt.gif?"+timestamp);
 });

/****
***
  liquid volume change
***
***/
 $("#boxLv").on('click', 'img', function() {
   $("#boxLv").children('img').attr('src', $ref+"/page2/image/lv.gif?"+timestamp);
 });

 /****
***
  gas volume change
***
***/
 $("#boxGv").on('click', 'img', function() {
   $("#boxGv").children('img').attr('src', $ref+"/page2/image/gv.gif?"+timestamp);
 });

// end of document ready
});

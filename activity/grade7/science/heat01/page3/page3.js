// counter to increase the next listing
var nextLi=0;
$(document).ready(function(){

  loadTimelineProgress(1,1);

  var $arrnextbtn=getArrowBtn();
  $("#activity-page-next-btn-enabled").show(0);
  // $("#activity-page-next-btn-enabled").html($arrnextbtn);

  $("#heading").text(data.string.pg3s1);
  $("#listing > li:nth-of-type(1)").text(data.string.pg3s2);
  $("#listing > li:nth-of-type(2)").text(data.string.pg3s3);
  $("#listing > li:nth-of-type(3)").text(data.string.pg3s4);
  $("#listing > li:nth-of-type(4)").text(data.string.pg3s5);
  $("#listing > li:nth-of-type(5)").text(data.string.pg3s6);
  $("#listing > li:nth-of-type(6)").text(data.string.pg3s7);
  $("#listing > li:nth-of-type(7)").text(data.string.pg3s8);
  $("#listing > li:nth-of-type(8)").text(data.string.pg3s9);
  $("#capillary").text(data.string.pg3s10);
  $("#bulb").text(data.string.pg3s11);
  $("#mercury").text(data.string.pg3s12);

  ole.parseToolTip("#listing > li:nth-of-type(2)","थर्मोमिटर");
  ole.parseToolTip("#listing > li:nth-of-type(4)","केशनली");
  ole.parseToolTip("#listing > li:nth-of-type(4)","बल्ब");
  ole.parseToolTip("#listing > li:nth-of-type(5)","पारो");
	$("#listing > li:nth-of-type(4) > span").addClass('animated infinite flash');
	$("#listing > li:nth-of-type(5) > span").addClass('animated infinite flash');

	// what to do on hovering over the span tags
	$("#listing > li:nth-of-type(4) > span:nth-of-type(1)").hover(function(){
		/* Stuff to do when the mouse enters the element */
		$("#capillary").addClass('animated bounce');
		$(this).removeClass("animated infinite flash");
	}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#capillary").removeClass('animated bounce');
	});

	$("#listing > li:nth-of-type(4) > span:nth-of-type(2)").hover(function(){
		/* Stuff to do when the mouse enters the element */
		$("#bulb").addClass('animated bounce');
		$(this).removeClass("animated infinite flash");
	}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#bulb").removeClass('animated bounce');
	});

	$("#listing > li:nth-of-type(5) > span:nth-of-type(1)").hover(function(){
		/* Stuff to do when the mouse enters the element */
		$("#mercury").addClass('animated bounce');
		$(this).removeClass("animated infinite flash");
	}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#mercury").removeClass('animated bounce');
	});

  $("#activity-page-next-btn-enabled").on('click', function() {
  	nextLi++;
  	var listMe = $("#listing").children("li:nth-of-type("+nextLi+")");
  	listMe.show(100);
  	if(nextLi==8){
  		$("#activity-page-next-btn-enabled").css({"display":"none"});
  		/*$(".footer-next").addClass("animated infinite shake");*/
      ole.footerNotificationHandler.pageEndSetNotification();
  	}

  	else if(nextLi==2){
  		$("#thermometer").fadeIn(1000);
  	}
  });

// end of document ready
});

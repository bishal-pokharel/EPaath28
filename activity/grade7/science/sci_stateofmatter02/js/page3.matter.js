(function ($) {

   loadTimelineProgress(3,1);

   var $wharr=getSubpageMoveButton($lang,"next");
   $("#activity-page-next-btn-enabled").html($wharr);

	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $title = $('.title');
	var $popUp = $('.popUp');
	var countNext = 0;
	var countCandle = 1;
   var newCanvas = 0;
   var animatend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';


   function firstTypes () {
      $title.text(data.string.typesTitle).addClass('animated bounceInDown').one(animatend,function () {
         $title.removeClass('animated bounceInDown');
      });

      var $wharr2=getArrowBtn();


      var source = $('#types-template').html();
      var template = Handlebars.compile(source);
      var content = {
         title : data.string.typesDesc,
         physical : data.string.physicalChange,
         chemical : data.string.chemicalChange,
         arrsrc:$wharr2
      }
      var html = template(content);
      $board.html(html).find('*').hide(0);
      $board.find('.typesOfChange, .showTitle').show(0).addClass('animated bounceInUp').one(animatend,function () {
         $board.find('.arrowDown').show(0);//.addClass('animated infinite pulse').one(animatend,function () {
         
           
            $board.find('.arrowDown>#arrowNextBtn').show(0);
            $board.find('.typesOfChange, .showTitle').removeClass('animated bounceInUp')
        //});
      })
   }
   firstTypes();

   $board.on('click','.arrowDown',function () {

    

      $(this).hide(0);
      $board.find('.physicalCircle').show(0).addClass('animated zoomInLeft');
      $board.find('.chemicalCircle').show(0).addClass('animated zoomInRight').one(animatend,function () {
         setTimeout(function () {
            $nextBtn.show(0);
         },1000)

      })
   })

   function first () {
      $title.text(data.string.physicalChange);
      var source = $('#first-template').html();
      var template = Handlebars.compile(source);
      var content = {
         defination : data.string.page3_1,
         img1 : $ref+"/images/paper/paper.png",
         img2 : $ref+"/images/ice/scissors.png",
         text1 : data.string.page3_2,
         defineEg : data.string.page3_3
      }
      var html = template(content);
      $board.html(html);
   }

   // first();

$board.on('click','.first #img2',function () {

   $(this).addClass('animated bounceOutLeft').one(animatend,function () {
      $board.find('.first #img1').attr('src',$ref+'/images/paper/paper03.png');
      // $(this).removeClass('animated bounceOutLeft');
         $board.find('.first .paperDesc').addClass('animated hinge').one(animatend,function () {
            $board.find('.first .defination').addClass('animated bounceOutUp').one(animatend,function () {
               $board.find('.first .defination').hide(0);
               $board.find('.first .defineEg').show(0).addClass('animated bounceInDown').one(animatend,function () {
                  $board.find('.first .defineEg').removeClass('animated bounceInDown');
                  $nextBtn.show(0).addClass('animated bounceInDown');
                  $nextBtn.removeClass('animated bounceInDown');
               });
            });

         });

   });
})

function iceWaterGas_transition() {

   $board.find('.first .paperCut,.first .below').addClass('animated rotateOut');
   $board.find('.first .defineEg').addClass('animated bounceOutUp').one(animatend,function () {
      iceWaterGas();
      $nextBtn.hide(0);
   })
}
/*
* molecule animation
*/
function canvasApp(getMinX,getMinY,getMaxTop,getMaxLeft,getSpeed,getNo) {
   newCanvas++;
   var oldCanvas = newCanvas;

  function  drawScreen () {

      context.fillStyle = '#EEEEEE';
      context.fillRect(0, 0, theCanvas.width, theCanvas.height);

      //Box
      context.strokeStyle = '#EEEEEE';
      context.strokeRect(1,  1, theCanvas.width-2, theCanvas.height-2);

      //Place balls


      var ball;

      for (var i = 0; i <balls.length; i++) {
         ball = balls[i];
         ball.x += ball.xunits;
         ball.y += ball.yunits;

         context.beginPath();
         context.arc(ball.x,ball.y,ball.radius,0,Math.PI*2,true);
         context.stroke();
         context.fillStyle = "#9ADBF8";
         context.strokeStyle = '#000000';
         context.closePath();
         context.fill();

         if (ball.x > maxRight || ball.x < maxLeft ) {
            ball.angle = 180 - ball.angle;
            if (ball.x > maxRight) {
            	ball.x = maxRight;
            } else if (ball.x < maxLeft) {
            	ball.x = maxLeft;
            }

            updateBall(ball);
         } else if (ball.y > maxBelow || ball.y < maxTop) {
            ball.angle = 360 - ball.angle;
            updateBall(ball);
            if(ball.y < maxTop) {
            	ball.y = maxTop;
            } else if(ball.y > maxBelow) {
            	ball.y = maxBelow;
            }
         }
      }

   }

   function updateBall(ball) {

      ball.radians = ball.angle * Math.PI/ 180;
      ball.xunits = Math.cos(ball.radians) * ball.speed;
      ball.yunits = Math.sin(ball.radians) * ball.speed;

   }

   var numBalls = getNo ;
   var size = 10;
   var maxSpeed = getSpeed;
   var balls = new Array();
   var tempBall;
   var tempX;
   var tempY;
   var tempSpeed;
   var tempAngle;
   var tempRadius;
   var tempRadians;
   var tempXunits;
   var tempYunits;

   theCanvas = document.getElementById("canvasOne");
   context = theCanvas.getContext("2d");

//my added portion
	minX = getMinX;
	minY= getMinY;
	maxX = theCanvas.width;
	maxY = theCanvas.height;
	maxTop = getMaxTop+5;
	maxLeft = getMaxLeft+5;
	maxBelow = theCanvas.height-5;
	maxRight = theCanvas.width-maxLeft;


	for (var i = 0; i < numBalls; i++) {
      tempRadius = size;
      tempX = tempRadius*2 + (Math.floor(Math.random()*(maxX-minX))-tempRadius*2+minX);
      tempY = tempRadius*2 + (Math.floor(Math.random()*(maxY-minY))-tempRadius*4+minY);

      tempSpeed = maxSpeed;
      tempAngle = Math.floor(Math.random()*360);
      tempRadians = tempAngle * Math.PI/ 180;
      tempXunits = Math.cos(tempRadians) * tempSpeed;
      tempYunits = Math.sin(tempRadians) * tempSpeed;

      tempBall = {x:tempX,y:tempY,radius:tempRadius, speed:tempSpeed, angle:tempAngle,
          xunits:tempXunits, yunits:tempYunits}
      balls.push(tempBall);
   }

   function gameLoop() {
      if (oldCanvas=== newCanvas) {
         window.setTimeout(gameLoop, 20);
        drawScreen()
      }

   }
   gameLoop();

   }
// canvasApp();
/***
* cavas for solid state
*/

function solidStateCanvas (getRadius,getSpace,getSpeed) {

   newCanvas++;
   var oldCanvas = newCanvas;

	var numBalls = 100 ;
	var size = 10;
   var maxSpeed = getSpeed;
	var balls = new Array();
   var plus = false;
   var radius = getRadius;
   var space = getSpace;


   function  drawScreen () {

      context.fillStyle = '#EEEEEE';
      context.fillRect(0, 0, theCanvas.width, theCanvas.height);

      //Box
      context.strokeStyle = '#EEEEEE';
      context.strokeRect(1,  1, theCanvas.width-2, theCanvas.height-2);

      //Place balls


      var ball;

      for (var i = 0; i <balls.length; i++) {
         ball = balls[i];
         ball.x += ball.xunits;
         // ball.y += ball.yunits;

         context.beginPath();
         context.arc(ball.x,ball.y,ball.radius,0,Math.PI*2,true);
         context.stroke();
         context.fillStyle = "#9ADBF8";
         context.strokeStyle = '#000000';
         context.closePath();
         context.fill();

         updateBall(ball);
      }

      if (plus===false) {
         plus = true;
      } else {
         plus = false;
      }
      // console.log(balls[1].x + balls[2].x);
      // console.log(balls[1].xunits);

   }

   function updateBall (ball) {
      if (!plus) {
         ball.xunits =+maxSpeed;

      } else {
         ball.xunits =-maxSpeed;
      }

      // ball.yunits +=5;
   }

	theCanvas = document.getElementById("canvasOne");
	context = theCanvas.getContext("2d");

	totalHeight = theCanvas.height;
	totalWeight = theCanvas.width;


	atomPerRow = totalWeight/(2*radius+2*space);

   var rows = numBalls/atomPerRow;
   var totalRowHeight = rows*(2*radius+2*space);
   var topLevel = totalHeight-totalRowHeight;
   // console.log(atomPerRow+" "+rows);

   for (var i = 0; i < rows; i++) {

      for (var j = 0; j < atomPerRow; j++) {
        // var x = j*(2*space+2*radius)+;
         balls.push({
            y : i*(2*space+2*radius)+topLevel,
            x : j*(2*space+2*radius)+2*space,
            xunits : 0,
            yunits : 0,
            radius : radius
         })
      };
      // console.log(balls);
	}
   var countMM = 0;
	function gameLoop() {
      if (oldCanvas=== newCanvas) {
         window.setTimeout(gameLoop, 50);
         drawScreen()
      };

	}
	gameLoop();

}

/**
* ice water template loading
*/

	function iceWaterGas () {
      $title.text(data.string.ice_title);
		var source = $('#iceWater-template').html();
		var template = Handlebars.compile(source);
		var content = {
			thermometer : $ref+"/images/ice/thermometer.png",
			slider : $ref+"/images/ice/slider.png",
         img1 : $ref+"/images/ice/liquid.png",
         desc : data.string.startingIce,
		}

		var html = template(content);
		$board.html(html);
       getMinX = 0;
       getMinY = 0;
       getMaxTop = 0;
       getMaxLeft = 0;
       getSpeed = 1.5;
       getNo = 150;
       canvasApp(getMinX,getMinY,getMaxTop,getMaxLeft,getSpeed,getNo);
       dragActive ()
	}

   // iceWaterGas();
   function dragActive () {
      $board.find("#slideMe").draggable(
         { containment: "parent",
         axis:"y",
         drag:function(){
            var pos = $(this).position().top;
            // console.log(pos);
            var ht = $(this).parent().height();

            var newHt = Math.floor((ht-pos-10)/ht*100);
            console.log(newHt);
            $board.find("#mercury").css({"height":newHt+"%"});

            if (newHt<=10) {
               getRadius = 10;
               getSpace = newHt/2+3;
               getSpeed = getSpace;
               solidStateCanvas(getRadius,getSpace,getSpeed)

            } else if (newHt>10 && newHt<77) {
               var ht = newHt-10;
               getMinX = 0;
               getMinY = 0;
               getMaxTop = 0;
               getMaxLeft = 0;
               getNo = -150*ht/67+200;
               getSpeed = ht/67+4;
               canvasApp(getMinX,getMinY,getMaxTop,getMaxLeft,getSpeed,getNo);
            } else if (newHt>77) {
               ht = newHt-77;
               getMinX = 0;
               getMinY = 0;
               getMaxTop = 0;
               getMaxLeft = 0;
               getSpeed = 20/23*ht+10;
               getNo = 45;
               canvasApp(getMinX,getMinY,getMaxTop,getMaxLeft,getSpeed,getNo);
            }

            var $iceWaterGas = $board.find('.iceWater');
            if(newHt<=10) {
               var degreeText = ole.degree(data.string.zeroBelow);
               $iceWaterGas.find('.desc').html(degreeText);
               $iceWaterGas.find('.picture img').attr('src',$ref+'/images/ice/ice.png')
            }
            else if (newHt>10 && newHt<90){
               var degreeText = ole.degree(data.string.zeroAbove);
               $iceWaterGas.find('.desc').html(degreeText);
               if(newHt<50){
                  $iceWaterGas.find('.picture img').attr('src',$ref+'/images/ice/liquid.png');
               } else {
                  $iceWaterGas.find('.picture img').attr('src',$ref+'/images/ice/above50.gif');
               }

            }
            else if(newHt>=90){
               var degreeText = ole.degree(data.string.above100);
               $iceWaterGas.find('.desc').html(degreeText);
               $iceWaterGas.find('.picture img').attr('src',$ref+'/images/ice/above100.gif')
            }

            setTimeout(function () {
               // ole.nextBlinker();
               ole.footerNotificationHandler.pageEndSetNotification();
            },5000);
         }

      });
   }

   $nextBtn.on('click',function () {
        loadTimelineProgress(3,countNext+2);
      $(this).hide(0);
      if (countNext===0) {
         first();
      } else {
         iceWaterGas_transition();
      }
      countNext++;

   })

})(jQuery);
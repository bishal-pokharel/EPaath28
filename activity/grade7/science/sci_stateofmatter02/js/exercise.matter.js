(function () {

	var $wharr=getSubpageMoveButton($lang,"next");
   	$("#activity-page-next-btn-enabled").html($wharr);
   	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $title = $('.title');
	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

	var tortoise = {
		pos : 0,
		initalPos : 5,
		finalPos : 90,
	}

	var question = {
		count : 0,
		range : 10,
		total : 10,
		correct : []
	}
	var randomVals = ole.getRandom(question.range,question.total-1);
	$board.html(randomVals)
/*
 _              _   _
| |_ _   _ _ __| |_| | ___
| __| | | | '__| __| |/ _ \
| |_| |_| | |  | |_| |  __/
 \__|\__,_|_|   \__|_|\___|

*/
	function goTurtle (len,nextFunc) {
		// console.log('tortoise.pos= '+tortoise.pos);
		var lastpos = tortoise.pos, i=lastpos;
		// tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(question.count-1)+tortoise.initalPos);
		tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(len-1)+tortoise.initalPos);
		var newGo = setInterval(function () {
			// console.log('lastpos = '+lastpos+" newPos="+tortoise.pos+" i="+i);
			if(i%2===0) {
				$('#tortoise2').show(0);
				$('#tortoise1').hide(0);
			} else {
				$('#tortoise1').show(0);
				$('#tortoise2').hide(0);
			}
			$('.tortoise').animate({'left' : i+"%"},100);
			i++;
			if (i>tortoise.pos) {
				clearInterval(newGo);
			};
		},100);
	}

/*
* turtle out
*/



	var content = [
	{
		type:'',
		question : data.string.q1,
		answers: [
		{ans : data.string.q1name1,
		correct : "correct"},
		{ans : data.string.q1name2},
		{ans : data.string.q1name3},
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q2,
		answers: [
		{ans : data.string.q2name1,
		correct : "correct"},
		{ans : data.string.q2name2},
		{ans : data.string.q2name3},
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q3,
		answers: [
		{ans : data.string.q3name1},
		{ans : data.string.q3name2},
		{ans : data.string.q3name3,
		correct : "correct"}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q4,
		answers: [
		{ans : data.string.q4name1},
		{ans : data.string.q4name2,
		correct : "correct"},
		{ans : data.string.q4name3}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q5,
		answers: [
		{ans : data.string.q5name1},
		{ans : data.string.q5name2,
		correct : "correct"},
		{ans : data.string.q5name3}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q6,
		answers: [
		{ans : data.string.q6name1,
		correct : "correct"},
		{ans : data.string.q6name2},
		{ans : data.string.q6name3},
		],
		img : ''
	},
	{
		type : 'multipleImg',
		question : data.string.q7,
		answers: [
		{imgs : $ref+"/images/exercise/01.png",
		ans : data.string.q7name1},
		{imgs : $ref+"/images/exercise/02.png",
		ans : data.string.q7name2},
		{imgs : $ref+"/images/exercise/03.png",
		ans : data.string.q7name3,
		correct : "correct"}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q8,
		answers: [
		{ans : data.string.q8name1,
		correct : "correct"},
		{ans : data.string.q8name2},
		{ans : data.string.q8name3}
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q9,
		answers: [
		{ans : data.string.q9name1},
		{ans : data.string.q9name2},
		{ans : data.string.q9name3,
		correct : "correct"},
		],
		img : ''
	},
	{
		type:'',
		question : data.string.q10,
		answers: [
		{ans : data.string.q10name1},
		{ans : data.string.q10name2,
		correct : "correct"},
		{ans : data.string.q10name3},
		],
		img : ''
	}

	]

	function firstStoryTeller () {
		var source = $('#first-story-template').html();
		var template = Handlebars.compile(source);
		var storyContent = {
			sun : $ref+"/images/exercise/sun.png",
			story : data.string.story,
			enter : data.string.press,
			storyImg1 : $ref+"/images/exercise/first.png",
			storyImg2 : $ref+"/images/exercise/flag.png",
		}
		var html = template(storyContent);
		$board.html(html);
	}

	firstStoryTeller();

	function  qA () {

		loadTimelineProgress(11,question.count+1);

		$title.text(data.string.exerciseTitle).show(0);
		$('.advancement').show(0);
		var counter = randomVals[question.count];
		content[counter].correctImg = $ref+"/images/exercise/correct.png";
		content[counter].wrongImg = $ref+"/images/exercise/wrong.png";
			if (content[counter].type==='multipleImg') {
				// alert();
				var source = $('#qAImage-templete').html();
			} else {
				var source = $('#qA-templete').html();
			}

			var template = Handlebars.compile(source);
			var html = template(content[counter]);
			$board.html(html);
	}

	// qA();

/*
* results
*/
	function results () {
		$title.hide(0);
		loadTimelineProgress(11,11);

		
		var len = question.correct.length;
		if(len < question.total) {
			$('.tortoise').animate({
				'bottom' : '+=200%'
			},600,function () {
				$(this).html("<img src='"+$ref+"/images/exercise/laydown.png' >")
			}).animate({
				'bottom' : '-=200%'
			},600,function () {
				$(this).css({
					'width' : '15%',
				})
			})

			$('#flag').css({
				'width' : '15%',
			},1000);
			if(len === 9){
				var endContent = {
					story : data.string.missedOne
				}
			} else if(len === 0){
				var endContent = {
					story : data.string.noTry
				}
			} else if(len <5){
				var endContent = {
					story : data.string.needMore
				}
			} else if(len <9){
				var endContent = {
					story : data.string.someMore
				}
			}
		} else {
			$('.tortoise').html("<img src='"+$ref+"/images/exercise/dance.gif' >").animate({
				'left' : '80%',
				'width' : '15%',
			},1000);
			$('#flag').animate({
				'right' : '35%',
				'width' : '15%',
			},1000);
			var endContent = {
				story : data.string.successStory
			}
		}

		var source = $('#results-template').html();
		var template = Handlebars.compile(source);
		var html = template(endContent);
		$board.html(html);
		$board.find('.endBoard').addClass('animated bounceInDown').one(animationEnd,function () {
			$('.repeat').show(0).addClass('animated bounceInDown');
		});

	}

	$board.on('click','.neutral',function () {
		// console.log("what");
		var $this = $(this);
		var isCorrect = $this.data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right');
			$('.neutral').removeClass('neutral');
			$nextBtn.fadeIn();
			$('.imgCrot .correctImg').show(0);
			question.correct.push(question.count);
			console.log(question.correct);
			var len = question.correct.length;
			goTurtle (len);
		}
		else {
			$this.addClass('wrong')
			$('.neutral').not('.correct').addClass('wrong').removeClass('neutral');
			$('.correct').addClass('right animated  tada');
			$('.neutral').removeClass('neutral');
			$('.imgCrot .wrongImg').show(0);
			$nextBtn.fadeIn();

		}
	})

	$nextBtn.on('click',function () {

		$nextBtn.fadeOut();
		question.count++;

		if(question.count<question.total){
			// goTurtle (qA);
			qA();
		}
		else if(question.count===question.total) {
			// goTurtle(results);
			// alert('finish');

			results();
			$nextBtn.fadeIn();
		} else {
			ole.activityComplete.finishingcall();
		}
	})

	$board.on('click','.storyWrapper .enter',qA);

	$('.repeat').on('click',function () {
		location.reload();
	})

})(jQuery)
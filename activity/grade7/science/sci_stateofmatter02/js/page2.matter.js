(function ($) {

	loadTimelineProgress(1,1);

	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $title = $('.title');
	var $popUp = $('.matter .popUp');
	var countNext = 0,countCls =0;
	var countCandle = 1;
	var animatEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	var $heating;
	var typeClicked;

	var $wharr=getArrowBtn();
	var $wharr1=getArrowBtn("prev");
	



	function first () {
		$title.text(data.string.m_2_1);
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = {
			newClass : 'heating',
			candle : $ref+"/images/candle.gif",
			img1 : $ref+"/images/liquid.png",
			img2 : $ref+"/images/gas.png",
			img3 : $ref+"/images/solid.png",

			liquid : data.string.liquid,
			solid : data.string.solid,
			gas : data.string.gas,
			talking: data.string.m_2_2

		}
		var html = template(content);
		$board.html(html);
		$board.find('.candle').draggable({ axis: "x" });
		$heating = $board.find('.heating');
		$heating.find('.gas, .liquid, .solid').hide(0);
		$title.addClass('animated bounceInDown').one(animatEnd, function () {
			$heating.find('.solid').show(0).addClass('animated bounceInUp').one(animatEnd,function () {
				$heating.find('.liquid').show(0).addClass('animated bounceInUp').one(animatEnd,function () {
					$heating.find('.gas').show(0).addClass('animated bounceInUp').one(animatEnd,function () {
						$board.find('.talking').show(0).addClass('animated bounceInDown').one(animatEnd,function () {
							$board.find('.talking').removeClass('animated bounceInDown');
							$heating.find('.candle').addClass('animated wobble').one(animatEnd,function () {
								$heating.find('.candle').removeClass('animated wobble');
							});

						});
					});
				})
			});
		});
	}

	first();

	$board.find('.burnerHolder').droppable({
		drop: function( event, ui ) {
			$this = $( this );
			$this.addClass( "ui-state-highlight" )
			.find('.candleIn').show(0);
			$board.find('#candle'+countCandle).hide(0);
			setTimeout(function () {
				$heating.find('.candle').addClass('animated wobble').one(animatEnd,function () {
					$heating.find('.candle').removeClass('animated wobble');
				});
			},3000);

			countCandle++;

			var $type = $this.data('type');
			$board.find('.'+$type+' .imageHolder img').attr('src',$ref+'/images/'+$type+'.gif');

			if (countCandle>3) {
				setTimeout(seeEffects,3000);
			};
		}
	})

	function seeEffects () {

		$board.find('.talking').text(data.string.m_2_3).addClass('animated wobble');
		$board.find('.imageHolder').addClass('clickImg');
	}

var b4A4 = {
	solid : [
	{
		checkClass : 'solidB4A4',
		things : $ref+'/images/solid.gif',
		title : data.string.solidVolume_title,
		b4Text : data.string.b4Heating,
		img1 : $ref+"/images/solid.png",
		a4Text : data.string.a4Heating,
		img2 : $ref+"/images/solid.gif",
		desc : data.string.solidVolume_desc,
		prevImg:$wharr1,
		nextImg:$wharr
	},
	{
		things : $ref+'/images/solid.gif',
		title : data.string.solidMolecule_title,
		b4Text : data.string.b4Heating,
		img1 : $ref+"/images/solid_atom.gif",
		a4Text : data.string.a4Heating,
		img2 : $ref+"/images/solid_atom_heated.gif",
		desc : data.string.solidMolecule_desc,
		prevImg:$wharr1,
		nextImg:$wharr
	},
	{
		checkId : "solid",
		things : $ref+'/images/solid.gif',
		title : data.string.example,
		exampleImg : $ref+"/images/bottle.png",
		desc : data.string.solidEg,
		prevImg:$wharr1,
		nextImg:$wharr
	}

	],
	liquid : [
	{
		things : $ref+'/images/liquid.gif',
		title : data.string.liquidVolume_title,
		b4Text : data.string.b4Heating,
		img1 : $ref+"/images/liquid.png",
		a4Text : data.string.a4Heating,
		img2 : $ref+"/images/liquid.gif",
		desc : data.string.liquidVolume_desc,
		prevImg:$wharr1,
		nextImg:$wharr
	},
	{
		things : $ref+'/images/liquid.gif',
		title : data.string.liquidMolecule_title,
		b4Text : data.string.b4Heating,
		img1 : $ref+"/images/liquid_atom.gif",
		a4Text : data.string.a4Heating,
		img2 : $ref+"/images/liquid_atom_heated.gif",
		desc : data.string.liquidMolecule_desc,
		prevImg:$wharr1,
		nextImg:$wharr
	},
	{	checkId : "liquid",
		things : $ref+'/images/liquid.gif',
		title : data.string.example,
		exampleImg : $ref+"/images/liquid_eg.gif",
		desc : data.string.liquidEg,
		prevImg:$wharr1,
		nextImg:$wharr
	}

	],
	gas : [
	{
		things : $ref+'/images/gas.gif',
		title : data.string.gasVolume_title,
		b4Text : data.string.b4Heating,
		img1 : $ref+"/images/gas.png",
		a4Text : data.string.a4Heating,
		img2 : $ref+"/images/gas.gif",
		desc : data.string.gasVolume_desc,
		prevImg:$wharr1,
		nextImg:$wharr
	},
	{
		things : $ref+'/images/gas.gif',
		title : data.string.gasMolecule_title,
		b4Text : data.string.b4Heating,
		img1 : $ref+"/images/gas_atom.gif",
		a4Text : data.string.a4Heating,
		img2 : $ref+"/images/gas_atom_heated.gif",
		desc : data.string.gasMolecule_desc,
		prevImg:$wharr1,
		nextImg:$wharr
	},
	{
		checkId : 'gas',
		things : $ref+'/images/gas.gif',
		title : data.string.example,
		exampleImg : $ref+"/images/gas_eg.png",
		desc : data.string.gasEg,
		prevImg:$wharr1,
		nextImg:$wharr
	}

	]
}

var countB4A4 = 0, $mainType;

	$board.on('click','.clickImg',function () {
		$mainType = $(this).data('type');
		countB4A4 = 0;
		loadb4A4();
	})

	function loadb4A4 () {
		var source = $('#b4-after-template').html();
		var template = Handlebars.compile(source);
		var content = b4A4[$mainType][countB4A4]
		var html = template(content);
		$popUp.html(html).show(0);
		setTimeout(function () {
			// ole.vCenter('.popUp .popHolder');
		},50)
	}

	function loadExample () {
		var source = $('#example-template').html();
		var template = Handlebars.compile(source);
		var content = b4A4[$mainType][countB4A4]
		var html = template(content);
		$popUp.html(html).show(0);
		setTimeout(function () {
			// ole.vCenter('.popUp .popHolder');
		},50)
	}

	$popUp.on('click','.popHolder .prevNext',function () {
		var $type = $(this).data('type');
		var whenSingle = {
			liquid : 2,
			gas : 2,
			solid : 2,
		}
		console.log(whenSingle[$mainType]);
		if ($type === 'prev') {
			countB4A4--;
			$popUp.find('.next').show(0);

		} else {
			countB4A4++;
			$popUp.find('.prev').show(0);
		}

		if (whenSingle[$mainType]=== countB4A4) {
			loadExample();
		} else {
			loadb4A4 ();
		}

		$popUp.find('.next').show(0);
		$popUp.find('.prev').show(0);
		if (countB4A4 <=0) {
			$popUp.find('.prev').hide(0);

		} else if (countB4A4>= b4A4[$mainType].length-1) {
			$popUp.find('.next').hide(0);
		}

	})

	$popUp.on('click','.clsBtn',function () {
		$board.find('.talking').removeClass('animated wobble')
		$popUp.hide(0);
		if (countCls >=2) {
			$board.find('.talking').text(data.string.m_2_4).addClass('animated wobble');
			clickCandle();
		};
		countCls++;
	})

	function clickCandle () {
		var countFireOff = 0;
		$board.on('click','.burnerHolder',function () {
			var $this = $(this), $type = $this.data('type');
			$this.find('img').attr('src',$ref+'/images/fireout.png');
			$this.parent().find('.imageHolder img').attr('src',$ref+'/images/'+$type+"_reverse.gif");
			countFireOff++;
			if(countFireOff>2){
				// $nextBtn.show(0).addClass('animated bounceInDown');
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		})
	}

	/*$nextBtn.on('click',function () {
		location.reload();
	})*/

})(jQuery);
(function ($) {

	var $wahtar=getSubpageMoveButton($lang,"next");
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		 quest++;
		 $(this).fadeOut(10);
		 $("#activity-page-prev-btn-enabled").fadeOut(10);
		 getFunction(quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		 quest--;
		 $(this).fadeOut(10);
		 $("#activity-page-next-btn-enabled").fadeOut(10);
		 getFunction(quest);
	 });


	 	var $wharr2=getArrowBtn();

	 	var $wharr3=getArrowBtn("prev");

	 	var $board = $('.board');
	 	var $nextBtn = $("#activity-page-next-btn-enabled");
	 	var $title = $('.title');
	 	var $popUp = $('.popUp');

	 function getFunction(qno){
			loadTimelineProgress(3,qno);
			switch(qno)
			{
					case 1:
						case1();
						break;
					case 2:
						$("#firstpage").hide(0);
						first();
						break;
					case 3:
						second_transition();
						break;
					}
		}
		function case1(){

				var datavar={
					easyTitle: data.lesson.chapter,
					coverimg:$ref+"/images/properties_of_matter.png"
				}
				var source   = $("#front-template").html();

				var template = Handlebars.compile(source);

				var html=template(datavar);
				$("#firstpage").html(html);
				$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
		}




	function first () {
		$title.text(data.string.matter_1_title);
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = {
			desc1 : data.string.matter_1_1,
			desc2 : data.string.matter_1_2,
			img1 : $ref+"/images/solid1.png",
			img2 : $ref+"/images/liquid1.png",
			img3 : $ref+"/images/gas1.png",
			name1 : data.string.solid,
			name2 : data.string.liquid,
			name3 : data.string.gas,
			clickThemText : data.string.clickThemText
		}
		var html = template(content);
		$board.html(html);
		$('.title').css('display','block');
		$('.first').css({'opacity':'0'}).animate({"opacity":"1"},1000);
		$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
	}


/**
* first hover conditions
*/
	$board.find('.img1').hover( function () {
		$(this).find('img').attr('src',$ref+'/images/solid2.png')
	}, function () {
		$(this).find('img').attr('src',$ref+'/images/solid1.png')
	} );

	$board.find('.img2').hover( function () {
		$(this).find('img').attr('src',$ref+'/images/liquid2.png')
	}, function () {
		$(this).find('img').attr('src',$ref+'/images/liquid1.png')
	} );

	$board.find('.img3').hover( function () {
		$(this).find('img').attr('src',$ref+'/images/gas2.png')
	}, function () {
		$(this).find('img').attr('src',$ref+'/images/gas1.png')
	} );


/**
* second function
*/
	function second_transition () {
		$title.addClass('animated bounceOut');
		$board.find('.first .desc').addClass('animated bounceOutDown');
		$board.find('.first .image img').addClass('animated rotateOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
			$board.find('.first .img1').animate({
				'top' : '-110%',
				'left' : '75%'
			},1000);
			$board.find('.first .img2').animate({
				'top' : '-70%',
				'left' : '45%'
			},1000);
			$board.find('.first .img3').animate({
				'top' : '-20%',
				'left' : '10%'
			},1000, second);
		});

	}

	function second () {
		$title.text(data.string.matter_1_title);
		var source = $('#second-template').html();
		var template = Handlebars.compile(source);
		var content = {
			desc : data.string.matter_1_3,
			name1 : data.string.solid,
			name2 : data.string.liquid,
			name3 : data.string.gas,
			imgLine : $ref+"/images/line.png"

		}
		var html = template(content);
		$board.html(html);
		$title.removeClass('animated bounceOut').show(0).addClass('animated bounceIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
		});


		/**
		* li click type
		*/
			var clickCount = 0;
			var liClickType = 0;
			var arrowClickCount = 0;
			$board.on('click','.names li',function () {
				$board.find('.hoverHolder').hide(0);
				$board.find('.names li').removeClass('selected');
				$(this).addClass('selected');
				var $type = $(this).data('type');
				clickCount++;
				if(clickCount>2){
					// ole.nextBlinker();
					ole.footerNotificationHandler.pageEndSetNotification()
				}
				// console.log($type);
				switch ($type) {

					case 1:
					liClickType = 'solid';
					var content = {
						newClass : 'solid',
						desc : content_slg.solid[0].statement,
						img1 : content_slg.solid[0].img,
						arrow : $ref+"/images/line.png",
						nextprevntn: $wharr3,
						nextprevntn1:$wharr2
					}

					break;


					case 2:
					liClickType = 'liquid';
					var content = {
						newClass : 'liquid',
						desc : content_slg.liquid[0].statement,
						img1 : content_slg.liquid[0].img,
						arrow : $ref+"/images/line.png",
						nextprevntn: $wharr3,
						nextprevntn1:$wharr2
					}

					break;

					case 3:
					liClickType = 'gas';
					var content = {
						newClass : 'gas',
						desc : content_slg.gas[0].statement,
						img1 : content_slg.gas[0].img,
						arrow : $ref+"/images/line.png",
						nextprevntn: $wharr3,
						nextprevntn1:$wharr2

					}
					break;
				}

				var source = $('#secondHover-holder').html();
				var template = Handlebars.compile(source);
				var html = template(content);
				$board.find('.hoverHolder').html(html).show(0);
				arrowClickCount = 0;
			});

			/**
			* arrow clicked
			*/

			$board.on('click','.nextPrevBtn',function () {

				var $click = $(this).data('click');
				if($click === 'prev') {
					arrowClickCount --;
					$board.find('.next').css('opacity','1');
				}
				else {
					console.log("clicked");
					if(arrowClickCount <=0) {
						$board.find('.pre').css('opacity','1')
					}
					arrowClickCount ++;
				}

				if(arrowClickCount <=0) {
					$board.find('.pre').css('opacity','0')
				} else if (arrowClickCount >= content_slg[liClickType].length-1){
					$board.find('.next').css('opacity','0');
				}

				var desc = content_slg[liClickType][arrowClickCount].statement;
				var img1 = content_slg[liClickType][arrowClickCount].img;
				$board.find('.hoverProduct .description').text(desc);
				$board.find('.hoverProduct .img img').attr('src',img1);
			});




		/*
		* data
		*/
		var content_slg = {
		 	solid : [{
		 		statement : data.string.m_solid_1,
		 		img : $ref+"/images/solid_atom.gif"
		 	},{
		 		statement : data.string.m_solid_2,
		 		img : $ref+"/images/solid01.png"
		 	},{
		 		statement : data.string.m_solid_3,
		 		img : $ref+"/images/solid02.png"
		 	}],
		 	liquid : [{
		 		statement : data.string.m_liquid_1,
		 		img : $ref+"/images/liquid_molecule.gif"
		 	},{
		 		statement : data.string.m_liquid_2,
		 		img : $ref+"/images/pots.png"
		 	},{
		 		statement : data.string.m_liquid_3,
		 		img : $ref+"/images/river.png"
		 	},{
		 		statement : data.string.m_liquid_4,
		 		img : $ref+"/images/liquid_eg.png"
		 	}],
		 	gas : [{
		 		statement : data.string.m_gas_1,
		 		img : $ref+"/images/gas_molecule.gif"
		 	},{
		 		statement : data.string.m_gas_2,
		 		img : $ref+"/images/air01.png"
		 	},{
		 		statement : data.string.m_gas_3,
		 		img : $ref+"/images/gas01.png"
		 	},{
		 		statement : data.string.m_gas_3,
		 		img : $ref+"/images/gas02.png"
		 	},{
		 		statement : data.string.m_gas_4,
		 		img : $ref+"/images/air02.png"
		 	}]
		}
	}




})(jQuery);

(function ($) {

	loadTimelineProgress(4,1);
	var $wharr=getSubpageMoveButton($lang,"next");


   	var $wharr2=getSubpageMoveButton($lang,"prev");


	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var $nextPrevBtn = $('.nextPrevBtn');
	var $title = $('.title');
	// var $popUp = $('.popUp');
	var countNext = 0;
	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	var $first;

	// $prevBtn.html($wharr2);
	// $nextBtn.html($wharr);

	var content = [{
		text1 : data.string.p4_1,
		like : data.string.p4_3,
		img1 : $ref+"/images/egg.png",
		text2 : data.string.p4_2,
	},{
		text1 : data.string.p4_1,
		like : data.string.p4_4,
		img1 : $ref+"/images/fire.png",
		text2 : data.string.p4_5,
	},{
		text1 : data.string.p4_1,
		like : data.string.p4_4,
		img1 : $ref+"/images/milk.png",
		text2 : data.string.p4_6,
	},
	]

	function first () {
		console.log("countNext value"+countNext);
		$title.text(data.string.chemicalChange);
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);

		var html = template(content[countNext]);
		$board.html(html);
		$first = $board.find('.first');
		$first.find('.like, .img1, .eg1').hide(0);
		$first.find('.like').show(0).addClass('animated bounceInLeft');
		$first.find('.img1').show(0).addClass('animated bounceInLeft').one(animationEnd, function () {
			$first.find('.eg1').show(0).addClass('animated rotateInUpRight').one(animationEnd,function () {
				$nextBtn.show(0);
				$first.find('.like').removeClass('animated bounceInLeft');
				$first.find('.img1').removeClass('animated bounceInLeft');
				$first.find('.eg1').removeClass('animated rotateInUpRight');
			});
		});
	}
	first();

	function next_transition(getFunc) {
		if(typeof $first === 'undefined' || $first===null){
			first();
		} else {
			$first.find('.eg1').addClass('animated rotateOutDownRight').one(animationEnd, function () {
				$first.find('.img1').addClass('animated bounceOutLeft');
				$first.find('.like').addClass('animated bounceOutLeft').one(animationEnd, getFunc);
			});
		}

	}

	function second_transition() {

		$first.find('.eg1').addClass('animated rotateOutDownRight').one(animationEnd, function () {
			$first.find('.img1').addClass('animated bounceOutLeft');
			$first.find('.like').addClass('animated bounceOutLeft').one(animationEnd, function () {
				$title.text(data.string.chemicalVsPhysical).addClass('animated bounceInDown');
				$first.find('.chemicalDesc').addClass('animated hinge').one(animationEnd,function () {
					$first = null;
					second();
					$nextBtn.hide(0);
				});

			});
		});
	}

	function second () {
		var source = $('#second-template').html();
		var template = Handlebars.compile(source);
		var content = {
			title1 : data.string.physicalChange,
			title2 : data.string.chemicalChange,
			text1 : data.string.physical_1,
			text2 : data.string.chemical_1,
			text3 : data.string.physical_2,
			text4 : data.string.chemical_2,
			text5 : data.string.physical_3,
			text6 : data.string.chemical_3,
		}
		var html = template(content);
		$board.html(html);
	}
	$nextBtn.hide(0);
	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
        countNext++;
		loadTimelineProgress(4,countNext+1);
		if (countNext+1>=4) {
			ole.footerNotificationHandler.lessonEndSetNotification();
		};

        loadContent();
	})
function loadContent(){
    switch (countNext) {
        case 0:
        case 1:
        case 2:
            next_transition(first);
            break;
        case 3:
            second_transition();
            break;

    }
}
    $prevBtn.on('click',function () {
       countNext--;
       loadContent();
    });

})(jQuery)

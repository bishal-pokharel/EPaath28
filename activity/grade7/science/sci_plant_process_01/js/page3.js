$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);
	
	$(".head_title").html(data.string.p3_1_a);
	$("#p3_1").html(data.string.p3_1);
	$("#p3_2").html(data.string.p3_2);

	
	

	var counter=1;
	loadTimelineProgress(5,counter);
	

	$("#p3_1_a").addClass('animated bounceInDown');
	setTimeout(function(){
		
		$("#p3_1_a").removeClass('animated bounceInDown');
		
		animate_1(counter);	
		 		
	},1500);//animate

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(10);
		$("#activity-page-prev-btn-enabled").fadeOut(10);
		counter++;

		loadTimelineProgress(5,counter);
		switch(counter)
		{
			case 1: animate_1(counter);	break;
			case 2: animate_2(counter);	break;
			case 3: animate_4(counter);	break;
			case 4: animate_3(counter);	break;
			case 5: animate_5(counter);	break;
			case 6: animate_6(counter);	break;

		}
		
	});


	$("#activity-page-prev-btn-enabled").click(function(){
		$(this).fadeOut(10);
		$("#activity-page-next-btn-enabled").fadeOut(10);
		counter--;
		loadTimelineProgress(5,counter);
		switch(counter)
		{
			case 1: animate_1(counter);	break;
			case 2: animate_2(counter);	break;
			case 3: animate_4(counter);	break;
			case 4: animate_3(counter);	break;
			case 5: animate_5(counter);	break;
			case 6: animate_6(counter);	break;

		}
		
	});


});

function animate_1()
{
	var datas;
	var whatId;
	
	 datas={
			textto:data.string.p3_1,
			whatBx:"animationBox",
			imgob:[
					{	imgn:$ref+"/images/page3/01.png", cap:data.string.p3_1_1 },
					{	imgn:$ref+"/images/page3/06.png", cap:data.string.p3_1_2 },
					{	imgn:$ref+"/images/page3/02.png", cap:data.string.p3_1_3 },
					{	imgn:$ref+"/images/page3/04.png", cap:data.string.p3_1_4 }
					
				]
	};
	whatId="#animationBox";
			

	

	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
	$(".Whatbx").html(html);


	$("#p3_1").fadeIn(10).addClass('animated bounceInLeft');
	setTimeout(function(){
		$("#p3_1").removeClass('animated bounceInLeft');
		$(whatId).fadeOut(1).delay(100).fadeIn(10,function(){
			$(".classTo").fadeIn(10).addClass('animated bounceInLeft');
				
			$("#activity-page-next-btn-enabled").delay(2000).fadeIn(10,function(){
				
			});
		});
	},1500);
	
}


function animate_2()
{
	var datas;
	var whatId="#animationBox1";
	
	 datas={
			textto:data.string.p3_2,
			text1:data.string.p3_3,
			whatBx:"animationBox1",
			imgn:$ref+"/images/page3/1.gif"
	};

	var source=$("#template-2").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
	$(".Whatbx").html(html);
	$("#p3_3").html(data.string.p3_3);

	$("#p3_2").fadeIn(10).addClass('animated tada');
	setTimeout(function(){
		$("#p3_2").removeClass('animated tada');

		$("#p3_3").fadeIn(10).addClass('animated bounceInLeft');
		setTimeout(function(){
			$("#p3_3").removeClass('animated bounceInLeft');
			$(whatId).fadeOut(1).delay(100).fadeIn(10,function(){
							
				$("#activity-page-next-btn-enabled").delay(2000).fadeIn(10,function(){
					
				});
				$("#activity-page-prev-btn-enabled").delay(2000).fadeIn(10,function(){
					
				});
			});
		},1500);
	},1500);
}


function animate_3()
{
	var datas;
	var whatId="#animationBox1";
	
	 datas={
			textto:data.string.p3_5,
			whatBx:"animationBox1",
			imgn:$ref+"/images/page3/5.gif"
	};

	var source=$("#template-3").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
	$(".Whatbx").html(html);

	$("#p3_5").html(data.string.p3_5);

	$("#p3_5").fadeIn(10).addClass('animated bounceInLeft');
	setTimeout(function(){
		$(this).removeClass('animated bounceInLeft');
		$(whatId).fadeOut(1).delay(100).fadeIn(10,function(){
						
			$("#activity-page-next-btn-enabled").delay(2000).fadeIn(10,function(){
				
			});

			$("#activity-page-prev-btn-enabled").delay(2000).fadeIn(10,function(){
					
			});
		});
	},1500);
}

function animate_4()
{
	var datas;
	var whatId="#animationBox2";
	
	 datas={
			textto:data.string.p3_6,
			text1:data.string.p3_7,
			whatBx:"animationBox2",
			imgn:$ref+"/images/page3/6.gif"
	};

	var source=$("#template-3").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
	$(".Whatbx").html(html);

	$("#p3_7").html(data.string.p3_7);
	$("#p3_5").fadeIn(10).addClass('animated bounceInLeft');
	setTimeout(function(){
		$("#p3_5").removeClass('animated bounceInLeft');
		$(whatId).fadeOut(1).delay(500).fadeIn(10,function(){
						
			setTimeout(function(){
				var timestamp3 = new Date().getTime();
				$(whatId).find('img').attr('src',$ref+'/images/page3/7.gif?'+timestamp3);
				$("#p3_7").fadeIn(10,function(){

					$("#activity-page-next-btn-enabled").delay(2000).fadeIn(10,function(){
						
					});
					$("#activity-page-prev-btn-enabled").delay(2000).fadeIn(10,function(){
						
					});

				});

			},6000);
		});
	},1500);
}

function animate_5()
{


	var datas;
	
	
	 datas={
			textto:data.string.p3_8,
			whatBx:"animationBox4",
			imgn:$ref+"/images/page3/4.gif",
			text1:data.string.p3_9,
			txt1:data.string.p3_4_a,
			txt2:data.string.p3_4_b

	};

	var source=$("#template-4").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
	$(".Whatbx").html(html);

	$("#p3_8").html(data.string.p3_8);
	$("#p3_9").html(data.string.p3_9);


	$("#p3_8").fadeIn(10).addClass('animated bounceInLeft');
	setTimeout(function(){
		$("#p3_8").removeClass('animated bounceInLeft');
		$("#animationBox4").fadeIn(100,function(){
			$("#p3_9").fadeIn(10).addClass('animated bounceInLeft');
			setTimeout(function(){
				$("#p3_9").removeClass('animated bounceInLeft');

				/*$("#reloadMe").delay(1000).fadeIn(10,function(){
					
				
				});*/

				$("#activity-page-prev-btn-enabled").delay(1000).fadeIn(10,function(){
					ole.footerNotificationHandler.pageEndSetNotification();
				});
		
			},1500);
		});
		
	},1500);
}

function animate_6()
{
	var datas;
	
	
	 datas={
			textto:data.string.p3_9,
			whatclass:"newMargin1",
			
	};

	var source=$("#template-3").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
	$(".Whatbx").html(html);
	$("#p3_5").fadeIn(10).addClass('animated bounceInLeft')
	setTimeout(function(){
		$("#p3_5").removeClass('animated bounceInLeft');
		
			
	},1500);
}
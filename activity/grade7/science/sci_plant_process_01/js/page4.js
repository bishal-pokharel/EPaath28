$(function(){


	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);
	$("#p4_1").html(data.string.p4_1);
	
	

	var counter=0;
	loadTimelineProgress(5,1);

	$("#p4_1").addClass('animated bounceInLeft');
	setTimeout(function(){
		
		$(this).removeClass('animated bounceInDown');
		
		$("#activity-page-next-btn-enabled").delay(1000).fadeIn(10,function(){
				
			});
		
	},1500);//animate

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(10);
		$("#p4_1").fadeOut(10);
		counter++;
		loadTimelineProgress(5,counter+1);
		if(counter<4)
		 animate_1(counter);
		else
			animate_2(counter);
		
	});

});

function animate_1(counter)
{
	var datas;
	var whatId;
	
	switch(counter){

		case 1:
		 datas={
			title:data.string.p4_2,
			whatImg:$ref+"/images/page4/2.gif",
			txt1:data.string.p4_3,
			txt1id:"txt1",
			txt2:data.string.p4_4,
			txt2id:"txt2",
			txt3:data.string.p4_5,
			txt3id:"txt3",
			txt4id:"txt4",
			txt4:data.string.p4_6
		};
		break;

		case 2:
		 datas={
			title:data.string.p4_7,
			whatImg:$ref+"/images/page4/3.gif",
			txt1:data.string.p4_8,
			txt1id:"txt5",
			txt3:data.string.p4_9,
			txt3id:"txt6",
			
		};
		break;

		case 3:
		 datas={
			title:data.string.p4_10,
			whatImg:$ref+"/images/page4/1.gif",
			txt1:data.string.p4_11,
			txt1id:"txt5",
			txt3:data.string.p4_12,
			txt3id:"txt7",
			txt4id:"txt8",
			txt4:data.string.p4_13
			
		};
		break;
	}
	
			

	

	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
	

	$("#animationBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(10,function(){

		$("#myImg").addClass('animated fadeIn');
		setTimeout(function(){
			$("#activity-page-next-btn-enabled").delay(3000).fadeIn(10,function(){
				
			});
		},1500);

	});
	
		
	
}

function animate_2(counter)
{
	var datas;
	var whatId;
	
		 datas={
			title:data.string.p4_15,
			h1:data.string.p4_16,
			h2:data.string.p4_17,
			h3:data.string.p4_18,
			t1:data.string.p4_2,
			t2:data.string.p4_3,
			t2a:data.string.p4_4,
			t3:data.string.p4_5,
			t3a:data.string.p4_6,
			t4:data.string.p4_7,
			t5:data.string.p4_8,
			t6:data.string.p4_9,
			t7:data.string.p4_10,
			t8:data.string.p4_11,
			t9:data.string.p4_12,
			t10:data.string.p4_13,
		};
		
	
			

	var source=$("#template-2").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
	

	$("#animationBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(10,function(){

		$(".tabme").addClass('animated bounceInLeft');
		setTimeout(function(){
				ole.footerNotificationHandler.pageEndSetNotification();
		},1500);

	});
	
		
	
}
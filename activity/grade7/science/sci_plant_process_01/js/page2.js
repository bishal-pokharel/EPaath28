$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	$(".head_title").html(data.string.p2_1);
	$("#p2_2").html(data.string.p2_2);
	

	var counter=1;
	loadTimelineProgress(2,counter);
	$("#p2_1").addClass('animated bounceInDown');
	setTimeout(function(){
		
		$("#p2_1").removeClass('animated bounceInDown');
		$("#p2_2").fadeIn(10).addClass('animated bounceInLeft');
		setTimeout(function(){

			$("#animationBox").fadeIn(10,function(){
				$("#activity-page-next-btn-enabled").delay(2000).fadeIn(10,function(){	});
			});			
		},1500);		
	},1500);//animate


	$("#activity-page-next-btn-enabled").click(function(){
		counter++;
		loadTimelineProgress(2,counter);

		$(this).fadeOut(10).removeClass('animated shake');
		
		$("#p2_2").fadeOut(10,function(){
			$(this).html(data.string.p2_3);
		}).delay(100).fadeIn(10);

		$("#animationBox").fadeOut(10,function(){
			$(this).find('img').attr('src',$ref+'/images/page2/1.gif').addClass('class1');
		}).delay(100).fadeIn(10,function(){
			$("#activity-page-prev-btn-enabled").delay(2000).fadeIn(10,function(){
					ole.footerNotificationHandler.pageEndSetNotification();
			});
		});
	});

	$("#activity-page-prev-btn-enabled").click(function(){

		counter--;
		loadTimelineProgress(2,counter);

		$(this).fadeOut(10).removeClass('animated shake');
		
		$("#p2_2").fadeOut(10,function(){
			$(this).html(data.string.p2_2);
		}).delay(100).fadeIn(10);

		$("#animationBox").fadeOut(10,function(){
			$(this).find('img').attr('src',$ref+'/images/page2/repiration.gif').removeClass('class1');
		}).delay(100).fadeIn(10,function(){
			$("#activity-page-next-btn-enabled").delay(2000).fadeIn(10,function(){
					

			});
		});
	});




	$("#reloadMe").click(function(){
		location.reload();
	});
});

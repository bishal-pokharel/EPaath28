$(function(){
	var $wahtar=getSubpageMoveButton($lang,"next");
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var quest=1;

	getFunction(quest);

	$("#activity-page-next-btn-enabled").click(function(){
		 quest++;
		 $(this).fadeOut(10);
		 $("#activity-page-prev-btn-enabled").fadeOut(10);
		 getFunction(quest);
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		 quest--;
		 $(this).fadeOut(10);
		 $("#activity-page-next-btn-enabled").fadeOut(10);
		 getFunction(quest);
	 });
	 function getFunction(qno){
			loadTimelineProgress(4,qno);
			switch(qno)
			{
					case 1:
					$("#activity-page-next-btn-enabled").fadeIn(10);
					$('#processContent').prepend('<img class="bg_full" src='+$ref+'/images/page1/plant_life_process.png /><p class="front_page_title">'+data.lesson.chapter+'</p>');
					break;
					case 2:
					$('.bg_full,.front_page_title').hide(0);
					case2();
					break;
					case 3:
					$("#p1_2_a").fadeOut(10);
					$("#p1_3").fadeOut(10);
					animate_1();
					break;
					case 4:
					$("#p1_2_a").fadeOut(10);
					$("#p1_3").fadeOut(10);
					animate_2();
					break;
			}
		}


	// var $whatnextbtn=getSubpageMoveButton($lang,"next");
	// $("#activity-page-next-btn-enabled").html($whatnextbtn);
	$(".head_title").html(data.string.p1_1);
	$("#p1_2").html(data.string.p1_2);
	$("#p1_3").html(data.string.p1_3);
	$("#p1_2_a").html(data.string.p1_2_a);
	$("#p1_2_b").html(data.string.p1_2_b);
	var clickCount=0;
	function case2(){



		$("#p1_1").addClass('animated bounceInDown');

		setTimeout(function(){

			$(this).removeClass('animated bounceInDown');
			$("#p1_2_a").fadeIn(10).addClass('animated bounceInLeft');
			setTimeout(function(){
				$("#activity-page-next-btn-enabled").fadeIn(10);
			});


		},1500);//animate
	}


		function case3(){


			// switch(counter)
			// {
			// 	case 1:animate_1(); break;
			// 	case 2:animate_2(); break;
			// 	case 3:animate_3(); break;
			//
			// }
		}




	$("#animationBox").on('change','.radio',function() {

		clickCount++;

		$id=$(this).attr('id');

		$whatImg=$id.split("_");

		var imgName, clasname;

		if($whatImg[1]==1)
		{

			imgName="1.gif";
			clasname="class1";
			$("#animationBox").find("#img_left div").html(data.string.p1_9).show(0);
		}
		else if($whatImg[1]==2)
		{

			imgName="2.gif";
			clasname="class1";
			$("#animationBox").find("#img_left div").hide(0);

		}
		else if($whatImg[1]==3)
		{

			imgName="3.gif";
			clasname="class1";
			$("#animationBox").find("#img_left div").hide(0);
		}
		else if($whatImg[1]==4)
		{

			imgName="4.gif";
			clasname="class1";
			$("#animationBox").find("#img_left div").hide(0);
		}

		$("#animationBox").find("img").attr('src',$ref+'/images/page1/s2/'+imgName).addClass(clasname);


		if(clickCount==4)
		{
			$("#p1_3").delay(400).fadeIn(10).addClass('animated tada');
			setTimeout(function(){

				$(this).removeClass('animated tada');

				ole.footerNotificationHandler.pageEndSetNotification();

			},1500);//fadein
		}
	});
});
function animate_1()
{

	$("#animationBox").fadeOut(10,function(){
		var $imgs=$ref+"/images/page1/";
		var datas={
			img1:$imgs+"1.png",
			img2:$imgs+"2.png",
			img3:$imgs+"3.png",
			img4:$imgs+"4.png",
			img5:$imgs+"5.png",
			img6:$imgs+"6.png",
			img7:$imgs+"7.png",
			img8:$imgs+"8.png",
			img9:$imgs+"9.png",
			img10:$imgs+"10.png",
			img11:$imgs+"11.png"

		};
		var source=$("#template-1").html();
		var template=Handlebars.compile(source);
		var html=template(datas);
		$(this).html(html);

	}).delay(10).fadeIn(10,function(){


		$("#p1_2").fadeIn(10).addClass('animated tada');
		setTimeout(function(){
			$("#p1_2").removeClass('animated tada');

			$("#imgbx1").fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx2").delay(800).fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx3").delay(1600).fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx4").delay(2400).fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx5").delay(3200).fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx6").delay(4000).fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx7").delay(4800).fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx9").delay(5600).fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx10").delay(6400).fadeIn(200).delay(600).fadeOut(200);
			$("#imgbx11").delay(7200).fadeIn(200,function(){

				$("#activity-page-next-btn-enabled").fadeIn(10);


			});//fadein

		},1500);//animate
	});
}


function animate_2()
{


	$("#animationBox").fadeOut(10,function(){
		$("#p1_2").html(data.string.p1_2_b);
	var $imgs=$ref+"/images/page1/";
		var datas={
			images1:$imgs+"9.png",
			text11:"p1_4",
			text12:"p1_5",
			text13:"p1_6",
			text14:"p1_7"
		};
		var source=$("#template-2").html();
		var template=Handlebars.compile(source);
		var html=template(datas);
		$(this).html(html);


	}).delay(10).fadeIn(10,function(){

		for(var i=4;i<=7;i++)
			$("#p1_"+i).html(data.string["p1_"+i]);
	});




}

$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	$("#toDo").html(data.string.e_1);


	var counter=0;
	var totalCounter=10;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);

	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;




	myquestion(newarray[counter],counter);

	$(".allquestion").on("click",".ans",function(){


		var isCorrect=parseInt($(this).attr('corr'));

		$("#showAns div").removeClass('ans');

		$("#showAns div[corr='1']").addClass('corrans');

		if(isCorrect==1)
		{
			$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);


			rightcounter++;
		}
		else
		{
			$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);


			wrongCounter++;
		}

		$("#activity-page-next-btn-enabled").fadeIn(10,function(){

		});

	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}

		if(counter > totalCounter)
		{
			ole.activityComplete.finishingcall();
		}
	});

});


function myquestion(questionNo,counter)
{

	loadTimelineProgress(11,(counter+1));
	var source   = $("#label-template").html();

	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter)
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#Imgshow").fadeOut(10);

	}).delay(100).fadeIn(10,function(){

		if(questionNo==2)
		{
			$("#img2_1").append("<div id='whatMe1'>"+data.string.p1_9+"</div>");
		}
	});
}

function myquestion2(right,wrong)
{
	loadTimelineProgress(11,11);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var $dataval={
		rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:data.string.exe_1, qans: data.string.opt_1_2,quesno:"ques_1"},
			{ques:data.string.exe_2, qans: data.string.opt_2_1,quesno:"ques_2"},
			{ques:data.string.exe_3, qans: data.string.opt_3_3,quesno:"ques_3"},
			{ques:data.string.exe_4, qans: data.string.opt_4_2,quesno:"ques_4"},
			{ques:data.string.exe_5, qans: data.string.opt_5_2,quesno:"ques_5"},
			{ques:data.string.exe_6, qans: data.string.opt_6_2,quesno:"ques_6"},
			{ques:data.string.exe_7, qans: data.string.opt_7_1,quesno:"ques_7"},
			{ques:data.string.exe_8, qans: data.string.opt_8_1,quesno:"ques_8"},
			{ques:data.string.exe_9, qans: data.string.opt_9_1,quesno:"ques_9"},
			{ques:data.string.exe_10, qans: data.string.opt_10_2,quesno:"ques_10"}

		]


	}
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.exe3);
		$("#Imgshow").fadeOut(10);


	}).delay(100).fadeIn(10,function(){

		$("#activity-page-next-btn-enabled").delay(100).fadeIn(10);

	});
}

function getQuestion($quesNo,counter)
{
	var quesList;
	var nep=ole.nepaliNumber(counter+1);

	var whatri=whatCorr($quesNo);

	if($quesNo==1)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page2/1.gif"}];
	}
	else if($quesNo==2)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page1/s2/1.gif"}];
	}
	else if($quesNo==3)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page3/02.png"}];
	}
	else if($quesNo==4)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page3/6.gif"}];
	}
	else if($quesNo==5)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page3/1.gif"}];
	}
	else if($quesNo==6)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page3/5.gif"}];
	}
	else if($quesNo==7)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page4/3.gif"}];
	}
	else if($quesNo==8)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page4/1.gif"}];
	}
	else if($quesNo==9)
	{
		var imgObjA=[
			{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page3/02.png"},
			{mul:'c1',imgObj1:"img"+$quesNo+"_2", imgObjval:$ref+"/images/page3/03.png"},
			{mul:'c1',imgObj1:"img"+$quesNo+"_3", imgObjval:$ref+"/images/page3/04.png"}
		];
	}
	else if($quesNo==10)
	{
		var imgObjA=[
			{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/images/page1/s2/3.gif"},
			{mul:'c1',imgObj1:"img"+$quesNo+"_2", imgObjval:$ref+"/exercise/images/4.png"},
			{mul:'c1',imgObj1:"img"+$quesNo+"_3", imgObjval:$ref+"/images/page3/1.gif"},
		];
	}
	quesList={
		myQuesion:data.string["exe_"+$quesNo],
		img22:$ref+"/exercise/images/blank.png",
		imgobj:imgObjA,
		optObj:[
				{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"]},
				{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"]},
				{optObj1:"opt_"+$quesNo+"_3", yesno:whatri[2],optObjval:data.string["opt_"+$quesNo+"_3"]}
				]
	}

	return quesList;
}

function whatCorr($quesNo)
{
	var whatis=new Array();
	switch($quesNo)
	{
		case 1: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=2;  	break;
		case 2: 	whatis[0]=1;	whatis[1]=0;	whatis[2]=0; 	break;
		case 3: 	whatis[0]=0;	whatis[1]=0;	whatis[2]=1;  	break;
		case 4: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0; 	break;
		case 5: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0;	break;
		case 6: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0; 	break;
		case 7: 	whatis[0]=1;	whatis[1]=0;	whatis[2]=0; 	break;
		case 8: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0; 	break;
		case 9: 	whatis[0]=1;	whatis[1]=0;	whatis[2]=0; 	break;
		case 10: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0; 	break;
	}
	return whatis;
}

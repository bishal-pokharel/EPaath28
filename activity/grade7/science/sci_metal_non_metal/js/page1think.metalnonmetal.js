(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 3,
		$refImg = $ref+"/images/page2/";
	loadTimelineProgress($total_page,countNext+1);

	var imgThinkYourself = "images/timetothink/timetothink1.png";

	var flow = [
		{
			text : data.string.p1think_1,
			// img : $refImg+"cloudy.png",
		},
		{
			text : data.string.p1think_2,
			// img : $refImg+"earth.png",
		},
		{
			text : data.string.p1think_3,
			// img : $refImg+"",
		}
	];

	/*text2 : data.string.p2_2,
			text3 : data.string.p2_3*/
/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var content = {
			imgThink : imgThinkYourself,
			thinkText : data.string.thinkOnce,
			thinkOnClass : data.string.p1think_title
		};
		var html = template(content);
		$board.html(html);
		textLoader();
	}

	/*first call to first*/
	first();

	function textLoader () {
		var source = $("#textLoader-template").html();
		var template = Handlebars.compile(source);
		var html = template(flow[countNext]);
		$board.find(".textLoader").html(html);
	}

	$nextBtn.on('click',function () {
		/*$(this).css("display","none");
		$prevBtn.css('display', 'none');*/
		countNext++;
		thenext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		/*$(this).css("display","none");
		$nextBtn.css('display', 'none');*/
		countNext--;
		thenext();
	});

	function thenext () {
		console.log(countNext);
		switch (countNext) {

			default:
				textLoader ();
			break;
		};

		/*$nextBtn.hide(0);
		$prevBtn.hide(0);*/

		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
			$prevBtn.show(0);
			$nextBtn.hide(0);
		} else if (countNext===0) {
			$nextBtn.show(0);
			$prevBtn.hide(0);
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

})(jQuery);
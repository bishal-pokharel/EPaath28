(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 4,
		$refImg = $ref+"/images/page8/",
		countLoad=0;

	loadTimelineProgress($total_page,countNext);
	$("#activity-page-next-btn-enabled").show(0);

	var details = [
		{
			type : "gif",
			title : data.string.p10_3,
			metal : $refImg+"changeshape/"+"12.png",
			nonMetal : $refImg+"brokenglass/"+"13.png",
			replaceMetal : $refImg+"shape01.gif",
			replaceNonMetal : $refImg+"shape02.gif",
			desc1 : data.string.p10_desc1_1,
			desc2 : data.string.p10_desc1_2,
			metalImg : [
			"01.png",
			"02.png",
			"03.png",
			"04.png",
			"05.png",
			"06.png",
			"07.png",
			"08.png",
			"09.png",
			"10.png",
			"11.png"
			],
			metalLast : $refImg+"changeshape/"+"12.png",
			nonmetalImg : [
			"01.png",
			/*"02.png",
			"03.png",*/
			"04.png",
			"05.png",
			/*"06.png",
			"07.png",*/
			"08.png",
			"09.png",
			/*"10.png",
			"11.png",*/
			"12.png"
			],
			nonmetalLast : $refImg+"brokenglass/"+"13.png",
		},
		{

			title : data.string.p10_4,
			metal : $refImg+"shine01.png",
			nonMetal : $refImg+"shine02.png",
			desc1 : data.string.p10_desc2_1,
			desc2 : data.string.p10_desc2_2
		},
		{
			type : "sound",
			title : data.string.p10_5,
			metal : $refImg+"soundmetal0.png",
			nonMetal : $refImg+"soundnonmetal0.png",
			hitMetal : $refImg+"soundmetal1.png",
			hitNonMetal : $refImg+"soundnonmetal1.png",
			hitMetal : $refImg+"soundmetal2.png",
			hitNonMetal : $refImg+"soundnonmetal2.png",
			desc1 : data.string.p10_desc3_1,
			desc2 : data.string.p10_desc3_2,
			soundMetal : new buzz.sound($ref+"/sound/metal.ogg"),
			soundNonMetal : new buzz.sound($ref+"/sound/non-metal.ogg")
		}
	];

	for (var i = 0; i <details[0].metalImg.length; i++) {
		details[0].metalImg[i] = $refImg+"changeshape/"+details[0].metalImg[i];
	};

	for (var i = 0; i <details[0].nonmetalImg.length; i++) {
		details[0].nonmetalImg[i] = $refImg+"brokenglass/"+details[0].nonmetalImg[i];
	};

	// details[2].soundMetal.play();


/*
* functions
*/
	function intro () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			introTitle : data.string.p10_intro
		}
		var html = template(content);
		$board.html(html);
	}

	intro();

	function first (diff) {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			topTitle : data.string.p10_title,
			repeatType : diff.type,
			metal : data.string.p10_1,
			nonMetal : data.string.p10_2,
			typeTitle : diff.title,
			metalImg : diff.metal,
			nonMetalImg : diff.nonMetal,
			desc1 : diff.desc1,
			desc2 : diff.desc2
		};
		var html = template(content);
		$board.html(html);
	}
	// first(details[countLoad]);

	$nextBtn.on('click',function () {
		$(this).hide(0);


		switch (countNext) {
			case 1:
			first(details[countLoad]);
			break;

			case 2:
			first(details[countLoad]);
			break;

			case 3:
			first(details[countLoad]);
			break;

			case 8:
			summary();
			break;
		}
		countLoad++;
		countNext++;
		loadTimelineProgress($total_page,countNext);

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}
	});

	$board.on("click",'.wrapMetalVsNonMetal .reload', function () {
		var $getSide = $(this).data("side");
		var countForClick = countLoad-1;
		if (details[countForClick].type === "gif") {
			if ($getSide==="metal") {
				console.log("metal");
				imgMotion ($board.find(".imgClass .metal"),details[0].metalImg,details[0].metalLast,"metal",300);
				//gifReload($board.find(".metal img"),details[countForClick].replaceMetal);
			} else {
				imgMotion ($board.find(".imgClass .nonmetal"),details[0].nonmetalImg,details[0].nonmetalLast,"nonmetal",100);
				// gifReload($board.find(".nonMetal img"),details[countForClick].replaceNonMetal);
			}

		} else if (details[countForClick].type === "sound") {
			if ($getSide==="metal") {
				var collector = {
					img0 : $refImg+"soundmetal0.png",
					img1 : $refImg+"soundmetal1.png",
					img2 : $refImg+"soundmetal2.png",
					sound : new buzz.sound($ref+"/sound/metal.ogg"),
				}
				loadImgNSound($board.find(".metal img"),collector);
			} else {
				var collector = {
					img0 : $refImg+"soundnonmetal0.png",
					img1 : $refImg+"soundnonmetal1.png",
					img2 : $refImg+"soundnonmetal2.png",
					sound : new buzz.sound($ref+"/sound/non-metal.ogg")
				}
				loadImgNSound($board.find(".nonMetal img"),collector);
			}
		}
	});

	function loadImgNSound (selector,collector) {
		selector.attr('src',collector.img2);
		collector.sound.play();
		setTimeout(function (){
			selector.attr('src',collector.img1);
			setTimeout(function (){
				selector.attr('src',collector.img0);
			}, 600);
		}, 120);

	}

	function gifReload (selector,imgSrc) {
		if (typeof imgSrc === 'undefined') {
			imgSrc = selector.attr('src');
		}
		var timestamp = new Date().getTime();
		var newImg = imgSrc+"?"+timestamp+"mjtDoneIt";
		selector.attr('src',newImg);
	}

	function imgMotion (selector,imgAll,last,type,framerate) {
		var len = imgAll.length;
		var count= 0;
		var img = [];
		for (var i = 0; i < len; i++) {
			img[i] = new Image();
			img[i].src = imgAll[i];
		};
		var imgLst = new Image();
		imgLst.src = last;
		var clear = setInterval(function () {
			console.log(img[count]+" "+count+" length = "+len);
			selector.html(img[count]);
			count++;
			if (count>len) {
				clearInterval(clear);
				setTimeout(function () {
					selector.html(imgLst).find('img').addClass("reload").data('side',type);
				},800);

			};
		}, framerate)
	}

	// ole.footerNotificationHandler.pgEndNotification();
	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
})(jQuery);

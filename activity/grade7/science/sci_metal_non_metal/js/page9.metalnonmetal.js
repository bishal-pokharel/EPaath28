$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		counter = 0,
		$refImg = $ref+"/images/page9/",
		$total_page = 7;
	loadTimelineProgress($total_page,countNext+1);
	$("#activity-page-next-btn-enabled").show(0);

	var alloy = [
		{
			alloyName : data.string.p9_1,
			img1 : $refImg+"iron.png",
			img3 : $refImg+"carbon.png",
			img5 : $refImg+"steel.png",
			smallDesc : data.string.p9_2,
			elementTitle1 : data.string.p9_title_iron,
			elementTitle2 : data.string.p9_title_carbon
		},
		{
			alloyName : data.string.p9_3,
			img1 : $refImg+"copper.png",
			img3 : $refImg+"tin.png",
			img5 : $refImg+"bronze.png",
			smallDesc : data.string.p9_4,
			elementTitle1 : data.string.p9_title_copper,
			elementTitle2 : data.string.p9_title_tin
		},
		{
			alloyName : data.string.p9_5,
			img1 : $refImg+"copper.png",
			img3 : $refImg+"zinc.png",
			img5 : $refImg+"brass.png",
			smallDesc : data.string.p9_6,
			elementTitle1 : data.string.p9_title_copper,
			elementTitle2 : data.string.p9_title_zinc
		}
	]

	var example = [{
		title : data.string.pg_example_title1,
		imgs : [{img : $refImg+"eg/steel01.png",
				name : data.string.p9_name_steel_1
				},{img : $refImg+"eg/steel02.png",
				name : data.string.p9_name_steel_2
				},{img : $refImg+"eg/steel03.png",
				name : data.string.p9_name_steel_3
				},{img : $refImg+"eg/steel04.png",
				name : data.string.p9_name_steel_4
				}]
	},{
		title : data.string.pg_example_title2,
		imgs : [{img : $refImg+"eg/bronze01.png",
				name : data.string.p9_name_bronze_1
				},{img : $refImg+"eg/bronze02.png",
				name : data.string.p9_name_bronze_2
				},{img : $refImg+"eg/bronze03.png",
				name : data.string.p9_name_bronze_3
				},{img : $refImg+"eg/bronze04.png",
				name : data.string.p9_name_bronze_4
				}]
	},{
		title : data.string.pg_example_title3,
		imgs : [{img : $refImg+"eg/brass01.png",
				name : data.string.p9_name_brass_1
				},{img : $refImg+"eg/brass02.png",
				name : data.string.p9_name_brass_2
				},{img : $refImg+"eg/brass03.png",
				name : data.string.p9_name_brass_3
				},{img : $refImg+"eg/brass04.png",
				name : data.string.p9_name_brass_4
				}]
	}]
/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var content = alloy[counter];
		content.imageSqr = "images/recap/recap1.png";
		content.img2 = $refImg+"plus.png";
		content.img4 = $refImg+"equal.png";
		var html = template(content);
		$board.html(html);
		// $nextBtn.show(0);
	}

	function loadExample () {
		var source = $("#eg-template").html();
		var template = Handlebars.compile(source);
		var content = example[counter];
			content.alloyName =  alloy[counter].alloyName;
			content.imageSqr  = "images/recap/recap1.png";
		var html = template(content);
		$board.html(html);
	}

	/*first call to first*/
	first();

	function whyAlloy () {
		var source = $("#why-template").html();
		var template = Handlebars.compile(source);
		var content = {
			title : data.string.p9_why_title,
			lists : [data.string.p9_why_1,data.string.p9_why_2,data.string.p9_why_3,data.string.p9_why_4]
		};
		var html = template(content);
		$board.html(html);
	}

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		// $prevBtn.css('display', 'none');
		countNext++;
		if (countNext>5) {
			whyAlloy();
		} else if (countNext%2===0) {
			counter++;
			first();
		} else {
			loadExample();
		}
		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}
	});
});

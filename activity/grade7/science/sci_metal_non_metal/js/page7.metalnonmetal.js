(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 1,
		$refImg = $ref+"/images/page7/",
		countMetal=0,
		whichClicked={};
		$("#activity-page-next-btn-enabled").show(0);

	loadTimelineProgress($total_page,countNext);

	var repText1 = ole.textSR(data.string.p7_1,data.string.p7_hightlight1,"<span class='highlight'>"+data.string.p7_hightlight1+"</span>");
	var repText2 = ole.textSR(data.string.p7_2,data.string.p7_hightlight2,"<span class='highlight'>"+data.string.p7_hightlight2+"</span>");
	var repText3 = ole.textSR(data.string.p7_3,data.string.p7_hightlight3,"<span class='highlight'>"+data.string.p7_hightlight3+"</span>");
	var descText = [
		repText1,
		repText2,
		repText3,
	]
/*
* functions
*/
	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			exception : data.string.exception,
			textIntro : data.string.p7_title,
			subtitle : data.string.p7_subtitle,
			img1 : $refImg+"img1.png",
			name1 : data.string.name1_1,
			text1 : repText1,
			img2 : $refImg+"img2.png",
			name2 : data.string.name2_1,
			text2 : repText2,
			img3 : $refImg+"img3.png",
			name3 : data.string.name3_1,
			text3 : repText3,
			descText : ""
		};
		var html = template(content);
		$board.html(html);
		$board.find('.wrapImg div span').hide(0);
	}
	first();

	$board.on('click','.wrapImg div',function () {
		var $this = $(this);
		var index =$this.index();
		whichClicked[index] = true;
		$board.find(".wrapImg div.active").removeClass("active");
		$this.addClass("active");
		// console.log(index);
		$this.find('span').show(0);
		$(this).children().addClass("hideHand");
		// $board.find('.descText').text(descText[index]);
		if (whichClicked[0] && whichClicked[1] && whichClicked[2]) {
			ole.footerNotificationHandler.pageEndSetNotification();
		};
	})



	// ole.footerNotificationHandler.pgEndNotification();
	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
})(jQuery);

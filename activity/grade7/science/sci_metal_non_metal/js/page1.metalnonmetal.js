(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 12,
		$refImg = $ref+"/images/page1/",
		countImgs=0;
		$("#activity-page-next-btn-enabled").show(0);
		loadTimelineProgress($total_page,countNext);
	/*$nextBtn.html(getArrowBtn());*/

/*
* first
*/
	var imgAll = [
				// $refImg+"metal_and_non_metal.png",
				$refImg+"aero.png",
				$refImg+"bhada.png",
				$refImg+"car.png",
				$refImg+"coins.png",
				$refImg+"heavy-machine.png",
				$refImg+"iron-pool.png",
				$refImg+"jewerry.png",
				$refImg+"metro.png",
				$refImg+"nails.png"
				];

	var infotextAll = [];
	// infotextAll.push(data.lesson.chapter);
	for (var i = 1; i < 10; i++) {
		infotextAll.push(data.string["p1_info"+i]);
	};

	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			introText : data.string.p1_introText,
		}
		var html = template(content);
		$board.html(html);
	}

	function second () {
		var source = $("#second-template").html()
		var template = Handlebars.compile(source);
		var content = {
			imgEg : imgAll[countImgs],
			infotext : infotextAll[countImgs]
		}
		var html = template(content);
		$board.html(html);
		countImgs++;
	}

	// second();

	function last () {
		var source = $("#last-template").html()
		var template = Handlebars.compile(source);
		var content = {
			imgs : imgAll,
			lastText : data.string.p1_lastText,
			someDesc : data.string.p1_lastDesc
		}
		var html = template(content);
		$board.html(html);
	}

	if(countNext==1){
		$board.html('<img class="bg_full" src='+$refImg+'metal_and_non_metal.png /><p class="mid_text">'+data.lesson.chapter+'</p>');
	}

	$nextBtn.on('click',function () {
		$(this).hide(0);
		/*switch (countNext) {
			case 1:
			second();
			break;
		}*/
		switch (countNext) {
			case 1:
				first();
				break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			second();
			break;
			case 11:
			last();
			break;
			default:
				break;

		}


		countNext++;
		loadTimelineProgress($total_page,countNext);

		// regular slide condition to end the page
		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}
	});

})(jQuery);

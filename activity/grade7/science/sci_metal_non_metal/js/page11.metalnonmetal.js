(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 2,
		$refImg = $ref+"/images/",
		countLoad=0,
		countMetal=0,
		lenMetal = 0,
		countNonMetal=0,
		lenNonMetal=0;
	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	loadTimelineProgress($total_page,countNext);
	$("#activity-page-next-btn-enabled").show(0);

	var allText = [
		{
			text : data.string.p11_1,
			type : "nonmetal",
		},
		{
			text : data.string.p11_2,
			type : "metal",
		},
		{
			text : data.string.p11_3,
			type : "metal",
		},
		{
			text : data.string.p11_4,
			type : "metal",
		},
		{
			text : data.string.p11_5,
			type : "metal",
		},
		{
			text : data.string.p11_6,
			type : "metal",
		},
		{
			text : data.string.p11_7,
			type : "metal",
		},
		{
			text : data.string.p11_8,
			type : "nonmetal",
		},
		{
			text : data.string.p11_9,
			type : "metal",
		},
		{
			text : data.string.p11_10,
			type : "nonmetal",
		},
		{
			text : data.string.p11_11,
			type : "nonmetal",
		},
		{
			text : data.string.p11_12,
			type : "metal",
		},
		{
			text : data.string.p11_13,
			type : "nonmetal",
		},
		{
			text : data.string.p11_14,
			type : "metal",
		},
		{
			text : data.string.p11_15,
			type : "nonmetal",
		}
	]

/*
* functions
*/
	function intro () {
		var source = $("#intro-template").html()
		var template = Handlebars.compile(source);
		var content = {
			introText : data.string.p11_intro
		};
		var html = template(content);
		$board.html(html);
	}
	intro();

	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var metalList=[], nonmetalList=[];
		for (var i = allText.length-1; i >=0 ; i--) {
			var newtext = allText[i];
			if(newtext.type === "metal") {
				metalList.push(allText[i]);
			} else {
				nonmetalList.push(allText[i]);
			}
		};
		lenMetal = metalList.length;
		lenNonMetal = nonmetalList.length;
		var content = {
			metal : data.string.metal,
			nonMetal : data.string.nonmetal,
			listMetal : metalList,
			listNonmetal : nonmetalList,
			bora : $refImg+"bora.png",
			boraInfo : data.string.p11_info
		};
		var html = template(content);
		$board.html(html);


		var $textHolder = $board.find('.textHolder span');
		var newP = allText[countLoad];
		$textHolder.text(newP.text)
		$textHolder.addClass('animated zoomInDown').one(animationEnd,function () {
			$(this).removeClass();
		});
	}

	function displayP (clicked) {
		var $textHolder = $board.find('.textHolder span');
		var $textHolder2 = $board.find('.textHolder');
		countLoad++;
		var newCls = "flyIntoHomogeneousSack";
		if (clicked === "bora2") {
			newCls = "flyIntoHeterogeneousSack";
		}
		if (countLoad<allText.length) {
			var newP = allText[countLoad];
			$textHolder2.removeClass(newCls).addClass(newCls).one(animationEnd,function () {
				$(this).removeClass(newCls);
				$textHolder.text(newP.text)
				$textHolder.addClass('animated zoomInDown').one(animationEnd,function () {
					$(this).removeClass();
				});
			});

		} else {
			$textHolder.addClass('animated zoomInDown').hide(0);
			summary();
		}
	}

	function summary () {
		$board.find('.boraInfo').hide(0).text(data.string.p11_finishMsg);
		setTimeout(function () {
			$board.find('.boraInfo').show(0).addClass("animated slideInDown");
			ole.footerNotificationHandler.pageEndSetNotification();
		},1000);
	}

	// second();
	$nextBtn.on('click',function () {
		$(this).hide(0);

		switch (countNext) {
			case 1:
			first();
			break;

			case 2:

			break;
		}

		countNext++;
		loadTimelineProgress($total_page,countNext);

		/*if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// $(this).show(0);
		}*/
	});

	$board.on('click','.bora1',function () {
		$this = $(this);
		console.log(countLoad);
		if (allText[countLoad].type ==="metal") {
			var metalId = lenMetal-countMetal;
			console.log(metalId);
			$this.find(".list span:nth-child("+metalId+")").fadeTo("slow" , 1);
			countMetal++;
			displayP("bora1");
		} else {
			$board.find(".textHolder").addClass('animated shake').one(animationEnd,function () {
				$board.find(".textHolder").removeClass('animated shake');
			})
		}
	});

	$board.on('click','.bora2',function () {
		$this = $(this);
		if (allText[countLoad].type ==="nonmetal") {
			var nonMetalId = lenNonMetal-countNonMetal;
			console.log(nonMetalId);
			$this.find(".list span:nth-child("+nonMetalId+")").fadeTo("slow" , 1);
			countNonMetal++;
			displayP("bora2");
		} else {
			$board.find(".textHolder").addClass('animated shake').one(animationEnd,function () {
				$board.find(".textHolder").removeClass('animated shake');
			})
		}
	});


	// ole.footerNotificationHandler.pgEndNotification();
	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
})(jQuery);

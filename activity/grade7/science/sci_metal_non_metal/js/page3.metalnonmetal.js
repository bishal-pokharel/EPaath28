(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 2,
		$refImg = $ref+"/images/page3/",
		countMetal=0;

	loadTimelineProgress($total_page,countNext);
	/*$nextBtn.html(getArrowBtn());*/

	/*var draggableImg = [
		{ img : $refImg+"img01.png",
			cls : "nonmetal"
		},$refImg+"img02.png",$refImg+"img03.png",$refImg+"img04.png",$refImg+"img05.png",$refImg+"img06.png",$refImg+"img07.png",$refImg+"img08.png",$refImg+"img09.png",$refImg+"img10.png",$refImg+"img11.png",$refImg+"img12.png",$refImg+"img13.png",$refImg+"img14.png",$refImg+"img15.png",$refImg+"img16.png",$refImg+"img17.png",$refImg+"img18.png",$refImg+"img19.png"];*/

	var draggableImg = [
		{ img : $refImg+"img01.png",
			cls : "nonmetal",
			name : data.string.p3_dragName_1
		},

		{ img : $refImg+"img03.png",
			cls : "nonmetal",
			name : data.string.p3_dragName_2
		},
		{ img : $refImg+"img18.png",
			cls : "metal",
			name : data.string.p3_dragName_3
		},
		{ img : $refImg+"img04.png",
			cls : "nonmetal",
			name : data.string.p3_dragName_4
		},
		{ img : $refImg+"img05.png",
			cls : "metal",
			name : data.string.p3_dragName_5
		},
		{ img : $refImg+"img06.png",
			cls : "nonmetal",
			name : data.string.p3_dragName_6
		},
		{ img : $refImg+"img07.png",
			cls : "metal",
			name : data.string.p3_dragName_7
		},
		{ img : $refImg+"img08.png",
			cls : "metal",
			name : data.string.p3_dragName_8
		},
		{ img : $refImg+"img09.png",
			cls : "metal",
			name : data.string.p3_dragName_9
		},
		{ img : $refImg+"img10.png",
			cls : "metal",
			name : data.string.p3_dragName_10
		},
		{ img : $refImg+"img11.png",
			cls : "nonmetal",
			name : data.string.p3_dragName_11
		},
		{ img : $refImg+"img12.png",
			cls : "nonmetal",
			name : data.string.p3_dragName_12
		},
		{ img : $refImg+"img13.png",
			cls : "metal",
			name : data.string.p3_dragName_13
		},
		{ img : $refImg+"img14.png",
			cls : "metal",
			name : data.string.p3_dragName_14
		},
		{ img : $refImg+"img15.png",
			cls : "metal",
			name : data.string.p3_dragName_15
		},
		{ img : $refImg+"img02.png",
			cls : "nonmetal",
			name : data.string.p3_dragName_16
		},
		{ img : $refImg+"img16.png",
			cls : "metal",
			name : data.string.p3_dragName_17
		},
		{ img : $refImg+"img17.png",
			cls : "metal",
			name : data.string.p3_dragName_18
		},
		{ img : $refImg+"img19.png",
			cls : "metal",
			name : data.string.p3_dragName_19
		}];
/*
* first
*/
	function diyLandingPage () {
		var source = $("#diyLandingPage-template").html();
		var template = Handlebars.compile(source);
		var content={
			diyImageSource : "images/diy/diy1.png",
			diyTextData : data.string.p4s0,
			diyInstructionData : data.string.p3_introText
		}
		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	}
	// diyLandingPage();

	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			diyImg : "images/diy/diy1.png",
			diyTextData : data.string.p4s0,
			lastText : data.string.p3_lastText,
			obj : draggableImg,
			bucketText : data.string.p3bucketText
		}
		
		var html = template(content);
		$board.html(html);
		$board.find(".bClass").tooltip({
			tooltipClass: 'mytooltip',
		});
		$board.find(".objCollections img.nonmetal").draggable({ revert: "invalid" });
		$board.find(".objCollections img.metal").draggable({ revert: "invalid" });
		$board.find(".bucket").droppable({
			accept : ".metal",
			tolerance : "fit",
			drop : function (event, ui) {
				$(this).text("");
				var getVal = $(ui.draggable).data("type");

				if (getVal==="metal") {
					$(ui.draggable).removeClass("bClass");
					countMetal++;
				};

				console.log(countMetal);
				if (countMetal>=12) {
					$board.find(".objCollections img.nonmetal").addClass("animated fadeOut").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
						$board.find(".lastText").show(0);
					});


					ole.footerNotificationHandler.pageEndSetNotification();
				};
			}
		})
	}
	first();

	$nextBtn.on('click',function () {
		$(this).hide(0);
		switch (countNext) {
			case 1:
			first();
			break;
		}

		countNext++;
		loadTimelineProgress($total_page,countNext);
	})


	// ole.footerNotificationHandler.pgEndNotification();
	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
})(jQuery);
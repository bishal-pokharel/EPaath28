$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		correctPng = 'images/correct.png',
		countNext = 0,
		$total_page = 11,
		firstClick = true,
		countcorrect = 0,
		clickedLog = [];
	loadTimelineProgress($total_page,countNext+1);

	var exercise = [];

	for (var i = 1; i <= 10; i++) {
		exercise.push({
			q : data.string["e"+i],
			a : [
				data.string["e"+i+"_ans1"],
				data.string["e"+i+"_ans2"],
				data.string["e"+i+"_ans3"]
			],
			reason : data.string["e"+i+"_reason"]
		});
	}

	var correctAnsLog = [3,2,1,1,3,2,2,2,2,2,3];

	var getRandom = ole.getRandom(10,9);
	console.log(getRandom);

/*
* first
*/
	function first() {

		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var content = exercise[getRandom[countNext]];
		content.title = data.string.e_title;
		var html = template(content);
		$board.html(html);
		// $nextBtn.show(0);
		firstClick = true;
		clickedLog[countNext] = [];
		console.log("checked");
	};

	/*first call to first*/
	first();

	function summary () {
		console.log("summary");
		console.log("correct "+countcorrect);
		// clickedLog[0]=[1,2];
		console.log(clickedLog);
		var log = [];
		for (var i = 0; i < exercise.length; i++) {
			var value = getRandom[i];
			log[i] = {
				question : exercise[value].q,
				rightAns : exercise[value].a[correctAnsLog[value]-1],
				yourAns : exercise[value].a[clickedLog[i][0]]
			}
			if (clickedLog[i][0]===correctAnsLog[value]-1) {
				log[i].type = "success";
			} else {
				log[i].type = "danger";
			};
		};

		// summarySet = exercise;
		var source = $("#summary-template").html();
		var template = Handlebars.compile(source);
		var content = {
			title : data.string.e_summary,
			summarySet : log,
			questionTitle : data.string.e_head1,
			rightAnsTitle : data.string.e_head2,
			yourAnsTitle : data.string.e_head3,
			totalcorrect : data.string.e_score+" : "+countcorrect,
		};
		// content.title = data.string.e_title;
		var html = template(content);
		$board.html(html);
		$("#activity-page-next-btn-enabled").delay(2000).fadeIn(200);
			$("#activity-page-next-btn-enabled").click (function () {
				ole.activityComplete.finishingcall();
			});
	}

	// summary();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		// $prevBtn.css('display', 'none');
		countNext++;
		/*switch (countNext) {
			case 1:
				first();
				break;
			case 2:
				second();
				break;
			case 3:
				third();
				break;
			case 4:
				fourth();
				break;
			case 5:
				fifth();
				break;
			default:break;
		}*/
		if (countNext<10) {
			first();
		} else {
			summary();
		};
		loadTimelineProgress($total_page,countNext+1);
	});


	$board.on('click','.ansHolder .ans',function () {
		$this = $(this);
		var index = $this.index();
		var remarks = data.string.wrong;
		var cls = "danger";
		$this.find('input').attr('checked',true);
		// console.log(index+" "+countNext+" "+correctAnsLog[getRandom[countNext]]);
		clickedLog[countNext].push(index);
		if (index === correctAnsLog[getRandom[countNext]]-1) {
			console.log("correct"+countcorrect);
			if (firstClick) {
				countcorrect++;
				remarks = data.string.correct;
				cls = "success";
			};
			$this.find('.checkMark img').attr('src',correctPng);
		} else {
			firstClick = false;
		}

		console.log(getRandom[countNext]+" "+countNext);
		var r = getRandom[countNext]+1;
		$board.find(".infoText").addClass(cls);
		remarks += "! "+data.string["e"+r+"_reason"];
		$board.find(".infoText").text(remarks).show(0);
		$this.find('.checkMark').show(0);
		$nextBtn.show(0);
	});

	/*$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		countNext--;
		switch (countNext) {
			case 0:
				first();
				break;
			case 1:
				second();
				break;
			case 2:
				third();
				break;
			case 3:
				fourth();
				break;
			case 4:
				fourth();
				break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});*/

});

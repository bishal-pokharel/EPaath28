(function ($) {

	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 2,
		$refImg = $ref+"/images/page5/",
		countMetal=0,
		whichClicked={};
		$("#activity-page-next-btn-enabled").show(0);

	loadTimelineProgress($total_page,countNext);


	var repText1 = ole.textSR(data.string.p5_1,data.string.p5_replaceText_1,"<span class='highlight'>"+data.string.p5_replaceText_1+"</span>");
	var repText2 = ole.textSR(data.string.p5_2,data.string.p5_replaceText_2,"<span class='highlight'>"+data.string.p5_replaceText_2+"</span>");
	var repText3 = ole.textSR(data.string.p5_3,data.string.p5_replaceText_3,"<span class='highlight'>"+data.string.p5_replaceText_3+"</span>");
	var descText = [
		repText1,
		repText2,
		repText3,
	]

	// data.string.p5_magnetic
	var repMagnetic = ole.textSR(data.string.p5_magnetic,data.string.p5_highlight2_1,"<span class='highlight'>"+data.string.p5_highlight2_1+"</span>");
		repMagnetic = ole.textSR(repMagnetic,data.string.p5_highlight2_2,"<span class='highlight'>"+data.string.p5_highlight2_2+"</span>");
		repMagnetic = ole.textSR(repMagnetic,data.string.p5_highlight2_3,"<span class='highlight'>"+data.string.p5_highlight2_3+"</span>");
/*
* functions
*/
	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			exception : data.string.exception,
			textIntro : data.string.p5_title,
			subtitle : data.string.p5_subtitle,
			img1 : $refImg+"mercury.png",
			name1 : data.string.name1,
			text1 : repText1,
			img2 : $refImg+"potassium.png",
			name2 : data.string.name2,
			text2 : repText2,
			img3 : $refImg+"sodium.png",
			name3 : data.string.name3,
			text3 : repText3,
		}
		var html = template(content);
		$board.html(html);
		$board.find('.wrapImg div span').hide(0);
	}
	first();

	$board.on('click','.wrapImg div',function () {
		var $this = $(this);
		var index =$this.index();
		whichClicked[index] = true;
		console.log(whichClicked);
		$board.find(".wrapImg div.active").removeClass("active");
		$this.addClass("active");
		// console.log(index);
		$this.find('span').show(0);
		console.log($(this).children().attr("class"));
		$(this).children().addClass("hideHand");
		// console.log(index);
		$board.find('.descText').text(descText[index]);
		if (whichClicked[0] && whichClicked[1] && whichClicked[2]) {
			$nextBtn.show(0);
		};

	})

	function second () {
		var source = $("#second-template").html()
		var template = Handlebars.compile(source);
		var content = {
			magnetic : repMagnetic,
			imgMagnet : $refImg+"magnetattract.gif",
		}
		var html = template(content);
		$board.html(html);

		ole.footerNotificationHandler.pageEndSetNotification();
	}

	$nextBtn.on('click',function () {
		$(this).hide(0);
		switch (countNext) {
			case 1:
			second();
			break;
		}

		countNext++;
		loadTimelineProgress($total_page,countNext);

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}
	})


	// ole.footerNotificationHandler.pgEndNotification();
	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
})(jQuery);

(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 6,
		$refImg = $ref+"/images/page6/",
		countMetal=0;
		$("#activity-page-next-btn-enabled").show(0);

	var setNewLoad = false;

	loadTimelineProgress($total_page,countNext);

/*
* functions
*/
	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			textIntro : data.string.p6s1
		}
		var html = template(content);
		$board.html(html);
	}
	first();



	var $list = [];

	var properties = [{
		title : data.string.p6_property1,
		longtitle : data.string.p6_long_property1,
		img : $refImg+"img1.png"
	},
	{
		title : data.string.p6_property2,
		longtitle : data.string.p6_long_property2,
		img : $refImg+"img2.png"
	},
	{
		title : data.string.p6_property3,
		longtitle : data.string.p6_long_property3,
		img : $refImg+"img3.png"
	},
	{
		title : data.string.p6_property4,
		longtitle : data.string.p6_long_property4,
		img : $refImg+"img4.png"
	}
	];

	function second (property) {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var counter = countNext-2;
		if (counter>-1) {
			$list.push(properties[counter].title);
		};
		var content = {
			num : countNext,
			listTitle : data.string.p6_propertyTitle,
			list : $list,
			propertiesImg : property.img,
			longtitle : property.longtitle,
			title : property.title
		}
		var html = template(content);
		$board.html(html);
		$board.find(".propertyMain .propertiesImg").addClass("animated fadeIn");
	}

	function summary () {
		var source = $("#summary-template").html();
		var template = Handlebars.compile(source);
		var endList = [];
		for (var i = 0; i < properties.length; i++) {
			endList.push(properties[i].title);
		};
		var content = {
			lastTitle : data.string.p6_lastTitle,
			listTitle : data.string.p6_propertyTitle,
			list : endList,
			periodicText : data.string.p6_periodicText
		}
		var html = template(content);
		$board.html(html);

	}

	$nextBtn.on('click',function () {
		$(this).hide(0);
		$(this).hide(0);
		if (setNewLoad) {
			$board.find(".propertyMain .propertiesDesc").show(0);
			setNewLoad = false;
		} else {
		switch (countNext) {
			case 1:
			second(properties[0]);
			break;

			case 2:
			second(properties[1]);
			break;

			case 3:
			second(properties[2]);
			break;

			case 4:
			second(properties[3]);
			break;

			case 5:
			summary();
			break;

		}
		setNewLoad = true;
		countNext++;
		loadTimelineProgress($total_page,countNext);
	}

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}
	})


	// ole.footerNotificationHandler.pgEndNotification();
	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/

	/*handlebars helper to add +1*/
	Handlebars.registerHelper("currentIndex", function(value, options)
	{
		return parseInt(value);
	});
	Handlebars.registerHelper("inc", function(value, options)
	{
		return parseInt(value) + 1;
	});

})(jQuery);

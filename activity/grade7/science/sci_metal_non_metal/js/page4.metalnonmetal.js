(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 9,
		$refImg = $ref+"/images/page4/",
		countMetal=0;
		$("#activity-page-next-btn-enabled").show(0);
	var setNewLoad = false;

	loadTimelineProgress($total_page,countNext);

/*
* functions
*/
	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			textIntro : data.string.p4s1
		}
		var html = template(content);
		$board.html(html);
	}
	first();



	var $list = [];

	var properties = [{
			title : data.string.p4_property1,
			longtitle : data.string.p4_long_property1,
			img : $refImg+"img1.png",
			extraImg : [$refImg+'strong1.png',$refImg+'strong2.png']
		},
		{
			title : data.string.p4_property2,
			longtitle : data.string.p4_long_property2,
			img : $refImg+"img2.png",
			extraImg : [$refImg+'shine1.png',$refImg+'shine2.png']
		},
		{
			title : data.string.p4_property3,
			longtitle : data.string.p4_long_property3,
			img : $refImg+"img3.png",
			extraImg : [$refImg+'ductile1.png',$refImg+'ductile2.png']
		},
		{
			title : data.string.p4_property4,
			longtitle : data.string.p4_long_property4,
			img : $refImg+"img4.png"
		},
		{
			title : data.string.p4_property5,
			longtitle : data.string.p4_long_property5,
			img : $refImg+"img5.png"
		},
		{
			title : data.string.p4_property6,
			longtitle : data.string.p4_long_property6,
			img : $refImg+"img6.png"
		},
		{
			title : data.string.p4_property7,
			longtitle : data.string.p4_long_property7,
			img : $refImg+"shape01.gif",
			extraImg : [$refImg+'malleable1.png',$refImg+'shine2.png']
		}
	];

	function second (property) {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var counter = countNext-1;
		if (counter>-1) {
			$list.push(properties[counter].title);
		};

		var content = {
			listTitle : data.string.p4_propertyTitle,
			list : $list,
			num : countNext+1,
			propertiesImg : property.img,
			longtitle : property.longtitle,
			title : property.title,
			extraImg : property.extraImg
		};
		var html = template(content);
		$board.html(html);
		$board.find(".propertyMain .propertiesImg").addClass("animated fadeIn");
	}

	function summary () {
		var source = $("#summary-template").html();
		var template = Handlebars.compile(source);
		var endList = [];
		for (var i = 0; i < properties.length; i++) {
			endList.push(properties[i].title);
		};
		var content = {
			lastTitle : data.string.p4_lastTitle,
			listTitle : data.string.p4_propertyTitle,
			list : endList,
			periodicText : data.string.p4_periodicText
		}
		var html = template(content);
		$board.html(html);

	}

	$nextBtn.on('click',function () {
		// countNext++;
		thenext();
	});

	/*$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		console.log(countNext);
			countNext=countNext-2;
		if (countNext<0) {
			countNext = 0;
		};
		console.log(countNext);
		setNewLoad = false;
		thenext();
	});*/

	function thenext (){
		$nextBtn.hide(0);
		if (setNewLoad) {

			// console.log(countNext);
			// console.log(properties[countNext-1].extraImg);

			if (properties[countNext-1].extraImg) {
				$board.find(".propertyMain .propertiesImg").hide(0);
				$board.find(".propertyMain .moreImg").show(0);
			};

			setTimeout(function () {
				$board.find(".propertyMain .propertiesDesc").show(0).addClass('animated fadeIn');
				setTimeout(function () {
					$nextBtn.show(0);
				},1000);
			},600);
			setNewLoad = false;
		} else {
			switch (countNext) {
				case 7:
				summary();
				break;

				default :
				second(properties[countNext]);
				break;
			}

			setNewLoad = true;
			$nextBtn.show(0);
			countNext++;
			loadTimelineProgress($total_page,countNext+1);
		}

		if (countNext+1>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
			// $prevBtn.show(0);
			$nextBtn.hide(0);
		} else if (countNext===0) {
			// $nextBtn.show(0);
			// $prevBtn.hide(0);
		} else {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		}

	};
	$board.on('click','img',function () {
		var $this = $(this);
		var imgSrc = $this.attr('src');
		var timestamp = new Date().getTime();
		var newImg = imgSrc+"?"+timestamp;
		$this.attr('src',newImg);
		console.log('img'+newImg);
	});

loadTimelineProgress($total_page,countNext+1);
	// ole.footerNotificationHandler.pgEndNotification();
	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
	/*handlebars helper to add +1*/
	Handlebars.registerHelper("currentIndex", function(value, options)
	{
		return parseInt(value);
	});
	Handlebars.registerHelper("inc", function(value, options)
	{
		return parseInt(value) + 1;
	});
})(jQuery);

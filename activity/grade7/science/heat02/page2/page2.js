var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var globalInterval;
$.fn.vibrateMe = function ($speed,$spread) {

    var speed;
    var spread;

    if($speed)
    {
        speed=parseInt($speed);
    }
    else speed=30;


    if($spread)
    {
        spread=parseInt($spread);
    }
    else spread=3;



    return this.each(function () {
        var selector = $(this);

        continuousViber();

        function vibrateThis (){
            var topPos    = Math.floor(Math.random() * spread) - ((spread - 1) / 2);
            var leftPos    = Math.floor(Math.random() * spread) - ((spread - 1) / 2);


            selector.css({
                position:            'relative',
                left:                leftPos + 'px',
                top:                topPos + 'px'
            });
        };

        function continuousViber() {
            globalInterval=setInterval(vibrateThis,speed);

        };
    });
};

// function for vibration of the molecules
function heatMolecules(num){
	var moleculeGroup = $(".molCol"+num);
	moleculeGroup.css({"background":"#E53434"});
	moleculeGroup.vibrateMe(0,5);
}

// function to raise the Mercury
function raiseMercury(that, ht, duration){
	$(that).velocity({"height":ht+"%"},duration,"linear");
}

// function for heating animation of rod
function heatMyRod(){
	$("#heatPass").velocity({"width":"100%"},
					{
						duration: 8000,
    					progress: function(elements, percentComplete) {
        				// console.log("elements: "+elements+"\n percentComplete: "+Math.round(percentComplete * 100)+"% \n timeRemaining: "+timeRemaining+"ms \n timeStart: "+timeStart+"\n *********************************");
    					var completed = Math.round(percentComplete * 100);
    					switch(completed){
    						case 30: raiseMercury("#m1","60",3000);break;
    						case 50: raiseMercury("#m2","50",3500);break;
    						case 51: heatMolecules(1);break;
    						case 52: heatMolecules(2);break;
    						case 53: heatMolecules(3);break;
    						case 54: heatMolecules(4);break;
    						case 55: heatMolecules(5);break;
    						case 56: heatMolecules(6);break;
    						case 57: heatMolecules(7);break;
    						case 58: heatMolecules(8);break;
    						case 59: heatMolecules(9);break;
    						case 60: heatMolecules(10);break;
    						case 70: raiseMercury("#m3","30",4000);break;
                case 98: $("#result").show(0);break;
    						default:break;
    					}
    				}
    				}
		);
}

// function to animate show instruction at start of page
function showInstr(){
   $("#instr > p:nth-of-type(1)").css("opacity","1");
   $("#instr > p:nth-of-type(1)").one(transitionend, function() {
      $("#instr > p:nth-of-type(2)").css("opacity","1")
   });
   $("#instr > p:nth-of-type(2)").one(transitionend, function() {
        $( "#match" ).show(0).addClass('animated infinite flash');
    });
}

// next counter for the list
var nxtCount = 0;

$(function () {

  loadTimelineProgress(1,1);

  var $wahtar=getArrowBtn();
  $("#activity-page-next-btn-enabled").html($wahtar);


  // pull data
  $("#instr > p:nth-of-type(1)").text(data.string.pg2s1);
  $("#instr > p:nth-of-type(2)").text(data.string.pg2s2);
  $("#instr > p:nth-of-type(3)").text(data.string.pg1s10);
  $("#result").text(data.string.pg2s3);
  $("#summary > ul > li:nth-of-type(1)").text(data.string.pg2s4);
  $("#summary > ul > li:nth-of-type(2)").text(data.string.pg2s5);
  $("#summary > ul > li:nth-of-type(3)").text(data.string.pg2s6);
  $("#summary > ul > li:nth-of-type(4)").text(data.string.pg2s7);
  $("#summary > em").text(data.string.pg2s8);
  $("#summary > blockquote").text(data.string.pg2s9);

  // text show at first
  showInstr();

  // normal vibration of molecules
  $(".molecule").vibrateMe(0,0);

// make the match draggable
$( "#match" ).draggable({containment: "parent",cursorAt: {right:40, bottom:5 }, scope:"lamp",
        start: function(){
            $( "#match" ).removeClass('animated infinite flash');
        }
});

// make flame div droppable
$("#flame").droppable({accept: "#match", scope:"lamp",
 		over: function(){
 			$( "#match" ).draggable("disable");
 			$( "#match" ).css({"display":"none"});
 			$( "#candle" ).attr("src",$ref+"/page2/image/candle.gif");
      $("#instr").children('p:nth-of-type(-n+2)').css('display', 'none');
 			heatMyRod();
 		}
});

// on clicking result button following events occur
$("#result").on('click', function() {
  $("#result").removeClass('animated infinite tada zoomInRight');
  $("#summary").removeClass('animated zoomOutRight');
  $("#summary").css('visibility', 'visible');
    $("#result").addClass('animated zoomOutRight');
  $("#summary").addClass('animated zoomInRight').one(animationend, function() {
      $("#instr > p:nth-of-type(3)").show(0);
      $("#activity-page-next-btn-enabled").show(0);
    });
});

// on clicking the closeSummary button following events occur
$("#closeSummary").on('click', function() {
  $("#summary").removeClass('animated zoomInRight');
  $("#result").removeClass('animated zoomOutRight');
  $("#summary").addClass('animated zoomOutRight');
  $("#result").show(0).addClass('animated zoomInRight');
});

// on clicking the activity-page-next-btn-enabled button following events occur
$("#activity-page-next-btn-enabled").on('click', function() {
  nxtCount++;
  switch(nxtCount){
    case 1:$("#summary > ul > li:nth-of-type(2)").show(0);break;
    case 2:$("#summary > ul > li:nth-of-type(3)").show(0);break;
    case 3:$("#summary > ul > li:nth-of-type(4)").show(0);break;
    case 4:$("#summary").children('em').show(0);break;
    case 5:$("#summary").children('blockquote').show(0);
            $("#activity-page-next-btn-enabled").css('display', 'none');
            $("#closeSummary").show(0);
            ole.footerNotificationHandler.pageEndSetNotification();
            break;
    default:break;
  }
});

});

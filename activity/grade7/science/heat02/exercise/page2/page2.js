// function for listing summary table with animation
	function finishAnim () {
		$("#summary").show(0);
		var animEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		$("#summary tr:nth-of-type(1)").addClass('animated zoomInDown').one(animEnd,function(){
			$("#summary tr:nth-of-type(2)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
				$("#summary tr:nth-of-type(3)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
					$("#summary tr:nth-of-type(4)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
						$("#summary tr:nth-of-type(5)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
							$("#summary tr:nth-of-type(6)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
								$("#summary tr:nth-of-type(7)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
									$("#summary tr:nth-of-type(8)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
										$("#summary tr:nth-of-type(9)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
											$("#summary tr:nth-of-type(10)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
												$("#summary tr:nth-of-type(11)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
													$('#activity-page-next-btn-enabled').show(0);
													questionNumber++;
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	}

// these are contents for answers
var ansCollect=[
	{
		mainQ:data.string.e2s7,
		correctMethod:"conduction",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s8,
		correctMethod:"radiation",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s9,
		correctMethod:"conduction",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s10,
		correctMethod:"convection",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s11,
		correctMethod:"radiation",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s12,
		correctMethod:"radiation",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s13,
		correctMethod:"convection",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s14,
		correctMethod:"conduction",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s15,
		correctMethod:"radiation",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	},
	{
		mainQ:data.string.e2s16,
		correctMethod:"radiation",
		correctAns:"",
		yourAns:"",
		isCorrect:false
	}
	]

// function generates random number between max and min and returns an array as collection of those random numbers
//isZero is boolean to say if a zero is required
function myRandom(count,max,min,isZero) {
 var randomGen = [];

 if (typeof isZero === "undefined") {
 	// zero required
		isZero=true;
	}

	// if minimum of range not specified - take minumum zero
	if (typeof min === "undefined") {
			min = 0;
	 			// if max is a negative number and min is not defined
			if (max < 0){
				min=max;
				max=0;
			}
		}

	// if maximum is less then the minimum then swap the values
	if(max < min){
		var temp = max;
		max=min;
		min=temp;
	}

	(function randomGet () {
				if (randomGen.length<count) {
					var rnd = Math.floor(Math.random()*(max-min+1)+min);
					// if zero is not required
					if(!isZero){
						while(rnd == 0) {
				        	rnd = Math.floor(Math.random()*(max-min+1)+min);
				    	}
					}

				    var i = randomGen.length,checkIf=false;

				    while (i--) {
				        if (randomGen[i] == rnd) {
				            randomGet();
				            checkIf=true;
				            break;
				        	}
				    	}

				    if (checkIf != true) {
				    	randomGen.push(rnd);
				    	randomGet();
				    };

					}
				else {
					// this is to just break the condition
				}

		})();

	return randomGen;
}

// next question button counter
var questionNumber = 0;

// start of document ready
$(document).ready(function() {


	var $wahtar=getSubpageMoveButton($lang,"next");
  	$("#activity-page-next-btn-enabled").html($wahtar);

	// pull these data
	$("#summary tr:nth-of-type(1) th:nth-of-type(1)").text(data.string.e2s1);
	$("#summary tr:nth-of-type(1) th:nth-of-type(2)").text(data.string.e2s2);
	$("#summary tr:nth-of-type(1) th:nth-of-type(3)").text(data.string.e2s3);


	// generate random sequence from 0 to 7 whereas 8 and 9 should always come at last
	var mySeq = myRandom(10,9,0);

	var board = $('#board');
	var nextBtn = $('#activity-page-next-btn-enabled');

	// function to highlight question
	function highlightQuestion(num){
		loadTimelineProgress(11,num+1);
		switch(num){
			case 0:case 2:case 4:case 6:case 8:
			$("#mainQn").css({"background-color":"#0A7E07","color":"white"});
			break;

			case 1:case 3:case 5:case 7:case 9:
			$("#mainQn").css({"background-color":"#72d572","color":"black"});
			break;

			default:break;
		}
	}

	// my template
	function myTemplate(num){



		var source = $('#option-template').html();
		var template = Handlebars.compile(source);
		var content = {
			conduct:data.string.e2s4,
			convect:data.string.e2s5,
			radiate:data.string.e2s6
		};
		var html = template(content);
		board.html(html);
		// select the correct method and set its data to correct
		$("#mainQn").text(ansCollect[num].mainQ);
		var method = board.children('div.transferMethods');
		var trueMethod = method.children("p."+ansCollect[num].correctMethod);
		var trueAns = trueMethod.children('span').html();
		// set the correct answer
		ansCollect[num].correctAns = trueAns;
		trueMethod.attr('data', "correct");
		// highlightQuestion color
		highlightQuestion(questionNumber);
		// console.log(mySeq[num]+trueMethod.attr('class')+trueMethod.attr('data')+ansCollect[mySeq[num]].correctAns);
	}

	// what to do on clicking next button
	nextBtn.on('click',function (){
			nextBtn.css({"display":"none"});
			// increase count until counter reaches 8 then
			if(questionNumber < 9){
				questionNumber++;
				myTemplate(mySeq[questionNumber]);
			}

			else if(questionNumber == 9){
				loadTimelineProgress(11,11);
				for(var i=0; i<10 ; i++){
					var j=i+2;
					// for main question
					$("#summary tr:nth-of-type("+j+") td:nth-of-type(1)").text(ansCollect[mySeq[i]].mainQ);
					// for correct answers
					$("#summary tr:nth-of-type("+j+") td:nth-of-type(2)").text(ansCollect[mySeq[i]].correctAns);
					// for user answers
					$("#summary tr:nth-of-type("+j+") td:nth-of-type(3)").text(ansCollect[mySeq[i]].yourAns);

					// highlight correct and wrong user answers
					if(ansCollect[mySeq[i]].isCorrect){
								$("#summary tr:nth-of-type("+j+") td:nth-of-type(3)").css({"color":"darkgreen"});
							}
							else{
								$("#summary tr:nth-of-type("+j+") td:nth-of-type(3)").css({"color":"#D1026C"});
							}

				}
				$("#board, #mainQn").css({"display":"none"});
				finishAnim();
			}

			else if(questionNumber > 9){
				ole.activityComplete.finishingcall();
			}
	});



	// get user answers and update the respective to yourAnswer in answer collect
	function userAnswer(selectedClass){
		var userAns= $(selectedClass).children('span').html();
		ansCollect[mySeq[questionNumber]].yourAns = userAns;
		var rightAns= ansCollect[mySeq[questionNumber]].correctAns;
			if(userAns == rightAns){
				ansCollect[mySeq[questionNumber]].isCorrect = true;
			}
			else if(userAns != rightAns){
				ansCollect[mySeq[questionNumber]].isCorrect = false;
			}
		// console.log(userAns);
	}

	// functionality for the radio buttons to select if even the label beside is clicked
	$("#board").on('click', ".transferMethods > p",function() {
		var selected = $(this).attr("class");
		switch(selected){
			case "conduction": $(".conduction > input").prop("checked",true);
								userAnswer(".conduction");
								break;
			case "convection": $(".convection > input").prop("checked",true);
								userAnswer(".convection");
								break;
			case "radiation": $(".radiation > input").prop("checked",true);
								userAnswer(".radiation");
								break;
			default:break;
		}
		nextBtn.show(0);
	});

	// call the template for first random answer
	myTemplate(mySeq[questionNumber]);
// end of document ready
})

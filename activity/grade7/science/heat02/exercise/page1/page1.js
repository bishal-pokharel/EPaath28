// these are contents for answers
var ansCollect=[
	{
		pic:$ref+"/exercise/page1/image/1.png",
		conduction:{topPos:20,
					leftPos:35,
					correct:false
					},
		convection:{topPos:45,
					leftPos:70,
					correct:false
					},
		radiation:{topPos:63,
					leftPos:70,
					correct:false
					}
	},
	{
		pic:$ref+"/exercise/page1/image/2.png",
		conduction:{topPos:41,
					leftPos:50,
					correct:false
					},
		convection:{topPos:35,
					leftPos:10,
					correct:false
					},
		radiation:{topPos:75,
					leftPos:45,
					correct:false
					}
	},
	{
		pic:$ref+"/exercise/page1/image/3.png",
		conduction:{topPos:53,
					leftPos:75,
					correct:false
					},
		convection:{topPos:25,
					leftPos:50,
					correct:false
					},
		radiation:{topPos:43,
					leftPos:15,
					correct:false
					}
	}
]

// function to position the droppables
function setQuestion(questionNumber){
	var conductionPosition = {
		"top": ansCollect[questionNumber].conduction.topPos+"%",
		"left": ansCollect[questionNumber].conduction.leftPos+"%"
	}

	var convectionPosition = {
		"top": ansCollect[questionNumber].convection.topPos+"%",
		"left": ansCollect[questionNumber].convection.leftPos+"%"
	}

	var radiationPosition = {
		"top": ansCollect[questionNumber].radiation.topPos+"%",
		"left": ansCollect[questionNumber].radiation.leftPos+"%"
	}

	$("#correctImage, #userImage").attr('src', ansCollect[questionNumber].pic);
	$("#dropConduction, #correctConduction").css(conductionPosition);
	$("#dropConvection, #correctConvection").css(convectionPosition);
	$("#dropRadiation, #correctRadiation").css(radiationPosition);
}

// function generates random number between max and min and returns an array as collection of those random numbers
//isZero is boolean to say if a zero is required
function myRandom(count,max,min,isZero) {
 var randomGen = [];

 if (typeof isZero === "undefined") {
 	// zero required
		isZero=true;
	}

	// if minimum of range not specified - take minumum zero
	if (typeof min === "undefined") {
			min = 0;
	 			// if max is a negative number and min is not defined
			if (max < 0){
				min=max;
				max=0;
			}
		}

	// if maximum is less then the minimum then swap the values
	if(max < min){
		var temp = max;
		max=min;
		min=temp;
	}

	(function randomGet () {
				if (randomGen.length<count) {
					var rnd = Math.floor(Math.random()*(max-min+1)+min);
					// if zero is not required
					if(!isZero){
						while(rnd == 0) {
				        	rnd = Math.floor(Math.random()*(max-min+1)+min);
				    	}
					}

				    var i = randomGen.length,checkIf=false;

				    while (i--) {
				        if (randomGen[i] == rnd) {
				            randomGet();
				            checkIf=true;
				            break;
				        	}
				    	}

				    if (checkIf != true) {
				    	randomGen.push(rnd);
				    	randomGet();
				    };

					}
				else {
					// this is to just break the condition
				}

		})();

	return randomGen;
}

// next question button counter
var nextCount = 0;

// flag keeps count of dropped elements
var droppedElements=0;
// start of document ready
$(document).ready(function() {

	loadTimelineProgress(1,1);

	// pull these data
	$("#mainQn").text(data.string.e1s1);
	$("#dragConduction, #correctConduction").text(data.string.e1s2);
	$("#dragConvection, #correctConvection").text(data.string.e1s3);
	$("#dragRadiation, #correctRadiation").text(data.string.e1s4);
	// $("#userAns > div").text(data.string.e1s5);
	$("#heading > p:nth-of-type(1)").text(data.string.e1s6);
	$("#heading > p:nth-of-type(2)").text(data.string.e1s7);

// generate random sequence from 0 to 2
	var mySeq = myRandom(1,2,0);

// set the question according to generated random number
    setQuestion(mySeq[0]);

// make the draggables
$("#draggable > p").draggable();

// make droppables
$("#userAns div").droppable({accept:"#draggable > p",
	over: function(overEvent,ui){
		$(this).css({"border-color":"#FFB100","box-shadow":"0em 0.1em 0.1em #FFB100"});
	},
	drop: function(dropEvent,ui){
		droppedElements++;
		// respective draggable
	var draggedData = ui.draggable.attr("data");
	var draggedContent =ui.draggable.html();
	ui.draggable.css({"display":"none"});
	   // active droppable
	var droppedData = $(this).attr('data');
	$(this).css({"border-color":"black","box-shadow":"0em 0.1em 0.1em #888888"});
	$(this).html(draggedContent);

	// for updating the ansCollect object variable to store the current droppable correctness- sorry
	// sometimes it is difficult to make it understand by code

		// check if dropped element is the correct one
		if(draggedData == droppedData){
				switch(droppedData){
					case "conduction":ansCollect[mySeq[0]].conduction.correct = true;break;
					case "convection":ansCollect[mySeq[0]].convection.correct = true;break;
					case "radiation":ansCollect[mySeq[0]].radiation.correct = true;break;
					default:break;
				}
		}

		else if(draggedData != droppedData){
			switch(droppedData){
					case "conduction":ansCollect[mySeq[0]].conduction.correct = false;break;
					case "convection":ansCollect[mySeq[0]].convection.correct = false;break;
					case "radiation":ansCollect[mySeq[0]].radiation.correct = false;break;
					default:break;
				}
		}

		// alert(currentDroppable+"="+ansCollect[mySeq[0]].currentDroppable);

		// check if all 3 elements are dropped
		if(droppedElements == 3){
			$("#okay").show(0);
			$("#draggable > p").draggable("disable");
			$("#userAns div").droppable("disable");
		}
	},
	out: function(outEvent,ui){
		$(this).css({"border-color":"black","box-shadow":"0em 0.1em 0.1em #888888"});
	}

});

// function to change color of the user answer after checking the answers
function checkAnswer(){
	// css color codes for correct and incorrect answers
	var correctColorCode = {
			"color":"green",
			"border-color":"green",
			"box-shadow":"0em 0.1em 0.1em green"
		}
	var inCorrectColorCode = {
			"color":"#E4362E",
			"border-color":"#E4362E",
			"box-shadow":"0em 0.1em 0.1em #E4362E"
		}
	var isConductionCorrect = ansCollect[mySeq[0]].conduction.correct;
	var isConvectionCorrect = ansCollect[mySeq[0]].convection.correct;
	var isRadiationCorrect = ansCollect[mySeq[0]].radiation.correct;

	// conduction answer
	if(isConductionCorrect){
		$("#dropConduction").css(correctColorCode);
	}

	else if(!isConductionCorrect){
		$("#dropConduction").css(inCorrectColorCode);
	}

	// convection answer
	if(isConvectionCorrect){
		$("#dropConvection").css(correctColorCode);
	}

	else if(!isConvectionCorrect){
		$("#dropConvection").css(inCorrectColorCode);
	}

	// radiation answer
	if(isRadiationCorrect){
		$("#dropRadiation").css(correctColorCode);
	}

	else if(!isRadiationCorrect){
		$("#dropRadiation").css(inCorrectColorCode);
	}
}

// what to do on clicking next button
	$("#okay").on('click',function (){
		$("#okay").css({"display":"none"});
		$("#userAns").animate({"left":"0%"},500,"linear",function(){
			$("#correctAns").show(0,function(){
				// check answer for color codes of correct and wrong answers
				checkAnswer();
				$("#userAns, #correctAns").animate({"top":"20%"},500,"linear",function(){
					$("#heading").css('visibility', 'visible');
					ole.footerNotificationHandler.pageEndSetNotification();
					ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.e1n1);
				});
			});
		});
	});

// end of document ready
})

// page1 aniamtion
function animPage(){
	$("#dailyEx > p:nth-of-type(1)").show(0).addClass('animated bounceInUp').one(animEnd, function() {
		$("#dailyEx > p:nth-of-type(2)").show(0).addClass('animated bounceInUp').one(animEnd, function() {
			$("#uses > figure:nth-of-type(1)").show(0).addClass('animated bounceIn').one(animEnd, function() {
				$("#uses > figure:nth-of-type(2)").show(0).addClass('animated bounceIn').one(animEnd, function() {
					$("#uses > figure:nth-of-type(3)").show(0).addClass('animated bounceIn').one(animEnd, function() {
						$("#uses > figure:nth-of-type(4)").show(0).addClass('animated bounceIn').one(animEnd, function() {
							$("#uses > figure:nth-of-type(5)").show(0).addClass('animated bounceIn').one(animEnd, function() {
								$("#uses > figure:nth-of-type(6)").show(0).addClass('animated bounceIn').one(animEnd, function() {
									$("#activity-page-next-btn-enabled").show(0);
								});
							});
						});
					});
				});
			});
		});
	});
}

// animation end event for css animation
var animEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

$(document).ready(function(){

  var $wahtar=getSubpageMoveButton($lang,"next");
	var $whatnxtbtn=getSubpageMoveButton($lang,"next");
 $("#activity-page-next-btn-enabled").html($whatnxtbtn);

 var $whatprevbtn=getSubpageMoveButton($lang,"prev");
 $("#activity-page-prev-btn-enabled").html($whatprevbtn);

 var quest=1;

 getFunction(quest);

 $("#activity-page-next-btn-enabled").click(function(){
 	quest++;
 	$(this).fadeOut(10);
 	$("#activity-page-prev-btn-enabled").fadeOut(10);
 	getFunction(quest);
 });

 $("#activity-page-prev-btn-enabled").click(function(){
 	quest--;
 	$(this).fadeOut(10);
 	$("#activity-page-next-btn-enabled").fadeOut(10);
 	getFunction(quest);
 });

 function getFunction(qno){
	 	loadTimelineProgress(3,qno);
	 	switch(qno)
	 	{
	 			case 1:
		 			case1();
		 			break;
	 			case 2:
	 				case2();
	 				break;
				case 3:
	 				case3();
	 				break;
	 	}
  }
	function case1(){

		  var datavar={
	  		easyTitle: data.lesson.chapter,
	  		coverimg:$ref+"/page1/image/heat_transmission.png"
	  	}

	  	var source   = $("#front-template").html();

	  	var template = Handlebars.compile(source);

	  	var html=template(datavar);
	  	$("#firstpage").html(html);
	  	$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
	}

function case2(){
	$("#firstpage").hide(0);

	// pull data
	$("#dailyEx > p:nth-of-type(1)").text(data.string.pg1s1);
	$("#dailyEx > p:nth-of-type(2)").text(data.string.pg1s2);
	$("#uses > figure:nth-of-type(1) > figcaption").text(data.string.pg1s3);
	$("#uses > figure:nth-of-type(2) > figcaption").text(data.string.pg1s4);
	$("#uses > figure:nth-of-type(3) > figcaption").text(data.string.pg1s5);
	$("#uses > figure:nth-of-type(4) > figcaption").text(data.string.pg1s6);
	$("#uses > figure:nth-of-type(5) > figcaption").text(data.string.pg1s7);
	$("#uses > figure:nth-of-type(6) > figcaption").text(data.string.pg1s8);
	$("#transerMethod > p:nth-of-type(1)").text(data.string.pg1s9);
	$("#conduction").text(data.string.pg1s10);
	$("#convection").text(data.string.pg1s11);
	$("#radiation").text(data.string.pg1s12);
	$("#transerMethod > p:nth-of-type(2)").text(data.string.pg1s13);
	$("#uses > figure").css("display","block");
	 // animation for the page
	 animPage();
}


function case3(){
	// on clicking the next button

	 $("#dailyEx").css("display","none");
	 $("#transerMethod > p:nth-of-type(1)").show(0).addClass('animated lightSpeedIn').one(animEnd, function() {
		 $("#transerMethod > em:nth-of-type(1)").show(0).addClass('animated zoomInDown').one(animEnd, function() {
			 $("#transerMethod > em:nth-of-type(2)").show(0).addClass('animated zoomIn').one(animEnd, function() {
				 $("#transerMethod > em:nth-of-type(3)").show(0).addClass('animated zoomInUp').one(animEnd, function() {
					 $("#transerMethod > p:nth-of-type(2)").show(0).addClass('animated flash').one(animEnd, function() {
						 /*$(".footer-next").addClass('animated shake');*/
						 ole.footerNotificationHandler.pageEndSetNotification();
					 });
				 });
			 });
		 });
	 });
}


});


// function to animate show instruction at start of page
function showInstr(){
   $("#instr > p:nth-of-type(1)").show(0).addClass('animated zoomInUp').one(animEnd, function() {
      $("#instr > p:nth-of-type(2)").show(0).addClass('animated flash').one(animEnd, function() {
        $( "#match" ).show(0).addClass('animated infinite flash');
    });
   });
}

// animation end event for css animation
var animEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
// next counter for the list
var nxtCount = 0;

$(document).ready(function(){

  loadTimelineProgress(1,1);

  var $wahtar=getArrowBtn();
  $("#activity-page-next-btn-enabled").html($wahtar);

  // pull data
  $("#instr > p:nth-of-type(1)").text(data.string.pg3s1);
  $("#instr > p:nth-of-type(2)").text(data.string.pg3s2);
  $("#instr > p:nth-of-type(3)").text(data.string.pg1s11);
  $("#result").text(data.string.pg3s3);
  $("#summary > ul > li:nth-of-type(1)").text(data.string.pg3s4);
  $("#summary > ul > li:nth-of-type(2)").text(data.string.pg3s5);
  $("#summary > ul > li:nth-of-type(3)").text(data.string.pg3s6);
  $("#summary > ul > li:nth-of-type(4)").text(data.string.pg3s7);
  $("#summary > em").text(data.string.pg3s8);
  $("#summary > blockquote").text(data.string.pg3s9);

  // text show at first
  showInstr();

// for the first time on mouse over the bubble div
$("#bubbleContain").one('mouseenter',function() {
  setTimeout(function(){
    $("#result").show(0);
  },3000);
});

// on clicking result button following events occur
$("#result").on('click', function() {
  $("#result").removeClass('animated infinite tada zoomInRight');
  $("#summary").removeClass('animated zoomOutRight');
  $("#summary").css('visibility', 'visible');
    $("#result").addClass('animated zoomOutRight');
  $("#summary").addClass('animated zoomInRight').one(animEnd, function() {
    $("#instr > p:nth-of-type(-n+2)").css('display', 'none');
      $("#instr > p:nth-of-type(3)").show(0);
      $("#activity-page-next-btn-enabled").show(0);
    });
});

// on clicking the closeSummary button following events occur
$("#closeSummary").on('click', function() {
  $("#summary").removeClass('animated zoomInRight');
  $("#result").removeClass('animated zoomOutRight');
  $("#summary").addClass('animated zoomOutRight');
  $("#result").show(0).addClass('animated zoomInRight');
});

// on clicking the activity-page-next-btn-enabled button following events occur
$("#activity-page-next-btn-enabled").on('click', function() {
  nxtCount++;
  switch(nxtCount){
    case 1:$("#summary > ul > li:nth-of-type(2)").show(0);break;
    case 2:$("#summary > ul > li:nth-of-type(3)").show(0);break;
    case 3:$("#summary > ul > li:nth-of-type(4)").show(0);break;
    case 4:$("#summary").children('em').show(0);break;
    case 5:$("#summary").children('blockquote').show(0);
            $("#activity-page-next-btn-enabled").css('display', 'none');
            $("#closeSummary").show(0);
            ole.footerNotificationHandler.pageEndSetNotification();
            break;
    default:break;
  }
});
// end of document ready
});

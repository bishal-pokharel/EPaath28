(function () {
	var questionCount = 0,
		randomVals = ole.getRandom(10,10,0),
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		content2 = [],
		countFirstClick = 0,
		countCorrect = 0;


	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	console.log(randomVals)

	$('.title').text(data.string.exerciseTitle);
	var content = [
	{
		question : data.string.q1,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2,
		correct : "correct"},
		{ans : data.string.name3},
		{ans : data.string.name4},
		{ans : data.string.name5},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/01.png"
	},
	{
		question : data.string.q2,
		answers: [
		{ans : data.string.name1,
		correct : "correct"},
		{ans : data.string.name2},
		{ans : data.string.name3},
		{ans : data.string.name4},
		{ans : data.string.name5},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/02.png"
	},
	{
		question : data.string.q3,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3},
		{ans : data.string.name4},
		{ans : data.string.name5},
		{ans : data.string.name6,
		correct : "correct"}
		],
		img : $ref+"/images/exercise/03.png"
	},
	{
		question : data.string.q4,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3},
		{ans : data.string.name4,
		correct : "correct"},
		{ans : data.string.name5},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/04.png"
	},
	{
		question : data.string.q5,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3,},
		{ans : data.string.name4},
		{ans : data.string.name5,
		correct : "correct"},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/05.png"
	},
	{
		question : data.string.q6,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3,
		correct : "correct"},
		{ans : data.string.name4},
		{ans : data.string.name5},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/06.png"
	},
	{
		question : data.string.q7,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3},
		{ans : data.string.name4},
		{ans : data.string.name5},
		{ans : data.string.name6,
		correct : "correct"}
		],
		img : $ref+"/images/exercise/07.png"
	},
	{
		question : data.string.q8,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3},
		{ans : data.string.name4},
		{ans : data.string.name5,
		correct : "correct"},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/08.png"
	},
	{
		question : data.string.q9,
		answers: [
		{ans : data.string.name1,
		correct : "correct"},
		{ans : data.string.name2},
		{ans : data.string.name3},
		{ans : data.string.name4},
		{ans : data.string.name5},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/09.png"
	},
	{
		question : data.string.q10,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2,
		correct : "correct"},
		{ans : data.string.name3},
		{ans : data.string.name4},
		{ans : data.string.name5},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/10.png"
	},
	{
		question : data.string.q11,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3},
		{ans : data.string.name4,
		correct : "correct"},
		{ans : data.string.name5},
		{ans : data.string.name6}
		],
		img : $ref+"/images/exercise/11.png"
	}

	]




	console.log(content);
	function  qA () {

		loadTimelineProgress(11,questionCount+1);
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[randomVals[questionCount]]);
			$board.html(html);
			// console.log(html);
			console.log(questionCount);
			content2.push(content[randomVals[questionCount]]);
	}

	qA();

	$board.on('click','.neutral',function () {
		console.log("what");
		var $this = $(this);
		var isCorrect = $this.data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			if (countFirstClick ===0) {
				countCorrect++;
				content2[questionCount].correct = "yes";
			}
		}
		else {
			countFirstClick++;
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	function summary () {
		loadTimelineProgress(11,11);
		var summaryContent =  {
			title : data.string.correctAns,
			total: data.string.totalQ+" : " + content2.length,
			totalCorrect : data.string.totalCorrect+" : "+countCorrect,
			correctImg : $ref+"/images/correct.png",
			summary : content2
		}
		var source = $("#summery-template").html();
		var template = Handlebars.compile(source);
		var html = template(summaryContent);
		$(".sm1").html(html);
		$("#activity-page-next-btn-enabled").html($whatnextbtn);
	}

	$(".sm1").on('click','.summery .nextBtn',function () {
		ole.activityComplete.finishingcall();
	})

	$nextBtn.on('click',function () {
		countFirstClick =0;
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<10){
			qA();
		} else if (questionCount===10) {

			summary();
			ole.activityComplete.finishingcall();
			// $nextBtn.fadeIn(10);

		}
	})

	$('.sm1').on("click",'.repeat',function () {
		location.reload();
	})

})(jQuery);

/*
     _                 _                             _     _            
 ___(_)_ __ ___  _ __ | | ___   _ __ ___   __ _  ___| |__ (_)_ __   ___ 
/ __| | '_ ` _ \| '_ \| |/ _ \ | '_ ` _ \ / _` |/ __| '_ \| | '_ \ / _ \
\__ \ | | | | | | |_) | |  __/ | | | | | | (_| | (__| | | | | | | |  __/
|___/_|_| |_| |_| .__/|_|\___| |_| |_| |_|\__,_|\___|_| |_|_|_| |_|\___|
                |_|                                                     
                        _          
  _____  _____ _ __ ___(_)___  ___ 
 / _ \ \/ / _ \ '__/ __| / __|/ _ \
|  __/>  <  __/ | | (__| \__ \  __/
 \___/_/\_\___|_|  \___|_|___/\___|

 */

var arrayOfImages = ["pic21.png","pic22.png","pic23.png","pic24.png","pic25.png","pic26.png","pic27.png","pic28.png","wedgeeasy.png"]; // images to load;
var clueImages = ["pic025.png","pic026.png","pic027.png"]; //pic for below background

var imagesLoad = [];
var clueImage = [];


loadTimelineProgress(4,1);

for (var j = 0; j<3; j++) {
	
	imagesLoad[j] = "";
	// clueImage[j]= "<div class='bgBlankpg'><img src='"+$ref+"/images/"+clueImages[j]+"'></div>"
	for (var i = j*3; i < (j+1)*3; i++) {
		console.log(arrayOfImages[i]);
		
		imagesLoad[j] += "<div class='img3'><img src='"+$ref+"/images/"+arrayOfImages[i]+"' width='50%' alt=''></div>";
	}

}

//generating random set of ans
var rightAnsSet = [];
var wrongAnsSet =[];
var correctAnsSet = [0,1,2];
var questions = [];
var dialogueAnswer = [];

for (var i = 0; i < 3; i++) {
	var count = i+1;
	questions[i] = data.string["question"+count];
	rightAnsSet[i] = data.string["correct"+count];
	dialogueAnswer[i] = data.string["answer"+count];
	console.log(questions[i]);
};

for (var i = 0; i <=6 ; i++) {
	count = i+1;
	wrongAnsSet[i] = data.string["wrong"+count];
};

var qAset = [],ansSet=[];
for (var i = 0; i < 3; i++) {
	var oldrand;
	ansSet[i]=[];
	
	for (var j = 0; j < 3; j++) {
		if (correctAnsSet[i] === j) {
			ansSet[i][j] = "<div class='ans correct' value='correct'>"+rightAnsSet[j]+"</div>";rightAnsSet[i];
		}
		else {
			var randValue = (Math.floor(Math.random()*10))%6;
			
			if(oldrand === randValue){
				randValue = (Math.floor(Math.random()*10))%6;
			}
			//answers[i][j]=wrongAnsSet[randValue];
			ansSet[i][j] = "<div class='ans' value='wrong'>"+wrongAnsSet[randValue]+"</div>";
			oldrand = randValue;

		}
		
	};

};


count = 0;

function divChange () {
	var $images = $ref + "/images/"+clueImages[count];
	var data1 = {
		question : questions[count],
		answers : ansSet[count],
		
		image : $images
	}


	$("#showPic").html(imagesLoad[count]);
	var source   = $("#QAnsBox-template").html();
	var template = Handlebars.compile(source);
	var html    = template(data1);
	console.log(html);
	$("#QAnsBox").html(html);
}

divChange();
$popUp = $(".popUpInside");
$("#QAnsBox").on("click", ".ans", function  () {
	if($(this).attr("value")==="correct") {

		var $data = {
			check : data.string.check,
			description : dialogueAnswer[count]
		}
		var source   = $("#pop-template").html();
		var template = Handlebars.compile(source);
		var html    = template($data);
		
		$popUp.html(html);
		$popUp.show(0);
		$popUp.find(".fullView").show(0);
		count++;


		
	}
	else {
		$wrong = $(".wrongPop");
		$wrong.html(data.string.checkWrong)
		$wrong.show(100,function () {
		setTimeout(
			function () {
				$(".wrongPop").fadeOut(2000);
			},1000);
		
	});
	}
});

$popUp.on('click',".closebtn",function  () {
	loadTimelineProgress(4,count+1);
	if (count >= 3) {
		newVal = "<span>"+data.string.secondConclusionTitle+"</span>";
		heading = "<p class='heading'>"+"</p>";
		quote = "<ul><li>"+rightAnsSet[0]+"</li><li>"+rightAnsSet[1]+"</li><li>"+rightAnsSet[2]+"</li></ul>";
		imagesOfall = "<ul><li><img src='"+$ref+"/images/"+clueImages[0]+"'></li><li><img src='"+$ref+"/images/"+clueImages[1]+"'></li><li><img src='"+$ref+"/images/"+clueImages[2]+"'></li></ul>";
		if ($lang == 'np') {
			parrotpic = "pic029.png";
		} else {
			parrotpic = "pic028.png";
		}
		
		var totalDiv = "<div class='summaryExercise'><div class='ulcls'><p>"+newVal+"</p>"+quote+"</div><div class='congrats'><img src='"+$ref+"/images/"+parrotpic+"' width='100%'></div><div class='downImages'>"+imagesOfall+"</div></div>";

		// $(".fullView").find(".up1").html(heading);
		// $(".fullView").find(".down2").html(quote);
		$(".fullView").hide(0);
		$popUp.hide(0);
		console.log(totalDiv);
		$("#content-body-wrapper").html(totalDiv);
		// console.log("count = "+count);
		// alert("enter the dragan");
		ole.footerNotificationHandler.lessonEndSetNotification();
	}
	else {
		$popUp.hide(0);
		divChange();

		// console.log("count = "+count);
	}
	
})
/*
  ___  _     _____ _   _ _____ ____   _    _
 / _ \| |   | ____| \ | | ____|  _ \ / \  | |
| | | | |   |  _| |  \| |  _| | |_) / _ \ | |
| |_| | |___| |___| |\  | |___|  __/ ___ \| |___
 \___/|_____|_____|_| \_|_____|_| /_/   \_\_____|

 */
 $(document).ready(function () {
   var $whatnxtbtn=getSubpageMoveButton($lang,"next");
 	$("#activity-page-next-btn-enabled").html($whatnxtbtn);

 	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
 	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

 	$(".titleMe").html(data.string.title1);
 	$("#p1_2").html(data.string.p1_2);

 	var quest=1;

 	getFunction(quest);

 	$("#activity-page-next-btn-enabled").click(function(){
 		quest++;
 		$(this).fadeOut(10);
 		$("#activity-page-prev-btn-enabled").fadeOut(10);

 		getFunction(quest);
 	});

 	$("#activity-page-prev-btn-enabled").click(function(){
 		quest--;
 		$(this).fadeOut(10);
 		$("#activity-page-next-btn-enabled").fadeOut(10);
 		getFunction(quest);
 	});
  function getFunction(qno){
    loadTimelineProgress(2,qno);
    switch(qno)
    {
      case 1:
        case0();
        break;
        case 2:
          case1();
          break;
    }

  }


	// body...
  function case0(){
    var datavar={
  		easyTitle: data.lesson.chapter,
  		coverimg:$ref+"/images/simple_machine.png"
  	}

  	var source   = $("#front-template").html();

  	var template = Handlebars.compile(source);

  	var html=template(datavar);
  	$(".mainpage").html(html);
    $(".Qwrapper").hide(0);
  	$("#activity-page-next-btn-enabled").delay(500).fadeIn(10);
  }

  function case1(){
    $(".mainpage").hide(0);
    $(".Qwrapper").show(0);
    var $title, $examples={},$typeClicked;
    $examples = {
    	lever : [data.string.leverExample1,data.string.leverExample2,data.string.leverExample3,data.string.leverExample4,data.string.leverExample5],
    	pulley : [data.string.pulleyExample1,data.string.pulleyExample2,data.string.pulleyExample3,data.string.pulleyExample4],
    	wheel : [
    			data.string.wheelExample1,
    			data.string.wheelExample2,
    			data.string.wheelExample3,
    			data.string.wheelExample4,
    			],
    	plane : [
    			data.string.planeExample1,
    			data.string.planeExample2,
    			data.string.planeExample3,
    			],
    	screw : [
    			data.string.screwExample1,
    			data.string.screwExample2,
    			data.string.screwExample3,
    			],
    	wedge : [
    			data.string.wedgeExample1,
    			data.string.wedgeExample2,
    			data.string.wedgeExample3
    			],
    };
    var lastImg = {
    	lever : "seesaw.png",
    	pulley : "pulley.png",
    	wheel : "wheel.png",
    	plane : "plane.png",
    	screw : "screw.png",
    	wedge : "wedge.png"
    }
    console.log($examples);
    	// var changeHeight = function (className,ratio) {
    	// 	var wrap = $("."+className);
    	// 	var newWidth = parseFloat(wrap.css("width"));
    	// 	var newHeight = ratio*newWidth;
    	// 	wrap.css({"height": newHeight});
    	// }


    	// changeHeight('envelop',220/252);
    	// changeHeight('wrapCircle',1);

    	// $(window).on("resize", function (e) {
      //
    	// 	// changeHeight('envelop',225/252);
    	// 	// changeHeight('wrapCircle',1);
      //
    	// });

    	var countQuest=0, questionClicked=0;

    	var backQuest = data.string.l1;
    	// alert(data.string.l1);
    	// alert(backQuest);
    	// var sent = "1. उत्तोलक प्रयोग हुँदा के फरक देख्छौ? उत्तोलकमा क्लिक गरेर हेर।";

    	var boxCome = $('.answer');
    	var $full = $('.fullView');

    /*
    * when wrong image inside circular is clicked
    */
    var wrongClicked = function  () {
    	if (questionClicked == 0) {
    		var dataShow = data.string.l3;
    	}
    	else {mainpage
    		dataShow = data.string.l2;
    	}
    	var $data = {
    		dialogue : dataShow,
    		ref : $ref
    	}

    	var source   = $("#wrong-template").html();
    	var template = Handlebars.compile(source);
    	var html = template($data);
    	$(".wrongPop").html(html);

    	$(".wrongPop").show(100,function () {
    		setTimeout(
    			function () {
    				$(".wrongPop").fadeOut(2000);
    			},1000);

    	});
    };

    var $popUp = $(".popUpInside");
    function showAnsNmore (saidAns,typeClicked) {
    	var imgSrc = "";
    	if (questionClicked!=0) {
    		if (saidAns == 1) {
    			imgSrc = "<img src='"+$ref+"/images/correct.png' width='50px'>";
    		}
    		else {
    			imgSrc = "<img src='"+$ref+"/images/wrong.png' width='50px'>";
    		}
    	};

          	// boxCome.find('.ansChecked').html(objSm.name+imgSrc);
          	var hardImg =$ref+"/images/easyhard/"+typeClicked+"hard.gif";
          	var easyImg = $ref+"/images/easyhard/"+typeClicked+"easy.gif";
          	// var defnDiff = "<div class='easyHardBox clear'><div class='hardBox eHbox'><p>"+objSm.hard+"</p><div class='imgHolder'><img src="+hardImg+" ></div></div><div class='easyBox eHbox'><p>"+objSm.easy+"</p><div class='imgHolder'><img src="+easyImg+" ></div></div></div><div class='defination'></div>";
          	// boxCome.find('.down2').html(defnDiff)

          	//data getting
          	var $title = data.string["name"+countQuest1];
          	var $defination = data.string["definition"+countQuest1];
          	var $hard = data.string["hard"+countQuest1];
          	var $easy = data.string["easy"+countQuest1];

          	var $data = {
          		continue : data.string.continue,
          		title : $title,
          		ref : $ref,
          		image : imgSrc,
          		hardTitle : $hard,
          		hardImg : hardImg,
          		easyTitle : $easy,
          		easyImg : easyImg,
          	}
          	var source   = $("#right-template").html();
          	var template = Handlebars.compile(source);
          	var html = template($data);

          	$title = $data.title;
          	$typeClicked = typeClicked;
          	// $examples = []
          	// for (var i = 0; i < 5; i++) {
          	// 	$examples.push(data.string[typeClicked+examples+i]);
          	// };
          	$popUp.html(html);
          	$popUp.show(0);
          	$fullView = $popUp.find(".fullView")
          	$fullView.show(0);

    		// making height of P element equal********* setTimeOut to wait loading html
    		setTimeout(function () {

    			$hardP = $(".down2").find(".hardBox").find('p');
    			$easyP = $(".down2").find(".easyBox").find('p');


    			if($hardP.css('height')< $easyP.css('height')){
    				$hardP.css('height',$easyP.css('height')) ;
    			}
    			else {
    				$easyP.css('height',$hardP.css('height'));
    			}

    		},500); // making height of P element equal ends



    		setTimeout(function  () {


    		//putting delay to show defination
    		$(".fullView").find(".defination").text($defination)
    	} ,2500);



    		$fullView.on('click',".continue",function  () {
    			$(this).hide(0);
    			// $fullView.find(".preClosebtn").text("X").addClass("closebtn").removeClass("preClosebtn");
    			$fullView.find(".preClosebtn").text("").addClass("closebtn myCloseStyle").removeClass("preClosebtn");
    			$fullView.find(".down2").html("<p>"+$defination+"</p>")

    			var source = $('#bullets-template').html();
    			var template = Handlebars.compile(source);
    			var cntnt = {
    				text1 : $defination,
    				text2 : data.string[$typeClicked+"Like"],
    				examples : $examples[$typeClicked],
    				img : $ref + "/images/last/"+lastImg[$typeClicked]
    			}
    			var html = template(cntnt);
    			console.log(cntnt.examples);
    			console.log('typeClicked = '+ $examples[$typeClicked]);

    			$fullView.find(".down2").html(html);

    		})
    	}


    	$('.circular').on('click',function (e) {
    		var $this = $(this);
    		var typeClicked = $this.attr('id');
    		var saidAns=0;
    		if (countQuest<=6) {
    			switch (typeClicked) {
    				case "pulley":
    				if(countQuest==2){
    					saidAns = 1;
    					countQuest1 = 2;
    					showAnsNmore(saidAns,typeClicked)
    				}
    				else {
    					wrongClicked();
    				}

    				break;
    				case "lever":
    				if(countQuest==1){
    					saidAns = 1;
    					countQuest1 = 1;
    					showAnsNmore(saidAns,typeClicked)
    				}
    				else {
    					wrongClicked();
    				}

    				break;
    				case "wheel":
    				if(countQuest==3){
    					saidAns = 1;
    					countQuest1 = 3;
    					showAnsNmore(saidAns,typeClicked)
    				}
    				else {
    					wrongClicked();
    				}

    				break;
    				case "plane":
    				if(countQuest==4){
    					saidAns = 1;
    					countQuest1 = 4;
    					showAnsNmore(saidAns,typeClicked)
    				}
    				else {
    					wrongClicked();
    				}

    				break;
    				case "screw":
    				if(countQuest==5){
    					saidAns = 1;
    					countQuest1 = 5;
    					showAnsNmore(saidAns,typeClicked)
    				}
    				else {
    					wrongClicked();
    				}

    				break;
    				case "wedge":
    				if(countQuest==6){
    					saidAns = 1;
    					countQuest1 = 6;
    					showAnsNmore(saidAns,typeClicked)
    				}
    				else {
    					wrongClicked();
    				}

    				break;
    			}

    		} else {
    			switch (typeClicked) {
    				case "pulley":
    				saidAns = 1;
    				countQuest1 = 2;
    				showAnsNmore(saidAns,typeClicked)
    				break;

    				case "lever":
    				saidAns = 1;
    				countQuest1 = 1;
    				showAnsNmore(saidAns,typeClicked)
    				break;

    				case "wheel":
    				saidAns = 1;
    				countQuest1 = 3;
    				showAnsNmore(saidAns,typeClicked)
    				break;

    				case "plane":
    				saidAns = 1;
    				countQuest1 = 4;
    				showAnsNmore(saidAns,typeClicked)
    				break;

    				case "screw":
    				saidAns = 1;
    				countQuest1 = 5;
    				showAnsNmore(saidAns,typeClicked)
    				break;

    				case "wedge":
    				saidAns = 1;
    				countQuest1 = 6;
    				showAnsNmore(saidAns,typeClicked)
    				break;
    			}

    		}

    	});

    var questDiv = $('.qDiv');
    questDiv.text(backQuest);

    var slowmo = function () {
    	var qWords = ole.textSR(data.string.envelop_quest,"9asd9","<span style='color:red'>"+data.string["name"+countQuest]
    		+" </span> ");
    	questDiv.animate({bottom: "60%",zIndex: "17"},1000, "linear")
    	.animate({width:"100%",left:'-1.5%',},1000)
    	.attr("id",'hello')
    	.html(qWords);
    	/*.html("<span style='color:red'>"+data.string["name"+countQuest]
    		+" </span> "+data.string.envelop_quest1+" <span style='color:red'>"
    		+data.string["name"+countQuest]+ "</span> "+data.string.envelop_quest2);*/
    	questDiv.unbind("click");

    };

    $popUp.on('click',".closebtn",function  () {
    	if (countQuest >=6) {
    		$popUp.hide(0);
    		questDiv.text(data.string.lastMsg);
    		countQuest++;
    		ole.footerNotificationHandler.pageEndSetNotification();
    	}

    	else {
    		if (questionClicked>0) {
    			questionClicked = 0;
    			questDiv.text(backQuest);
    			questDiv.removeAttr('style').removeAttr('id').bind('click',function () {
    				questionClicked++;
    				countQuest++;
    				slowmo();
    			});
    		};
    		$popUp.hide(0);
    		$(".closebtn").html("<img src='"+$ref+"/images/298.GIF' width='10px'>").addClass("preClosebtn").removeClass("closebtn");
    	}


    });


    $('span').on('click',function() {
    	var cls = $(this).attr('class');

    	if (cls==="closebtn") {
    		if (questionClicked>0) {
    			questionClicked = 0;

    			questDiv.text(backQuest);
    			questDiv.removeAttr('style').removeAttr('id').bind('click',function () {
    				questionClicked++;
    				countQuest++;
    				slowmo();
    			});

    		};
    		$full.hide(0);
    		$(".closebtn").html("<img src='"+$ref+"/images/298.GIF' width='10px'>").addClass("preClosebtn").removeClass("closebtn");
    	};
    });

    questDiv.on('click',function () {
    	questionClicked++;
    	countQuest++;
    	slowmo();
    });

    $('.clickD').on('click',function () {
    	questionClicked++;
    	countQuest++;
    	slowmo();
    });


    $popUp.on('click','.btn1st',function () {
    	var imgSrc = $popUp.find('.hardImg').attr('src');
    	var timestamp = new Date().getTime();
    	var newImg = imgSrc+"?"+timestamp;
    	$popUp.find('.hardImg').attr('src',newImg);
    	console.log('img'+newImg);
    })

    $popUp.on('click','.btn2st',function () {
    	var imgSrc = $popUp.find('.easyImg').attr('src');
    	var timestamp = new Date().getTime();
    	var newImg = imgSrc+"?"+timestamp;
    	$popUp.find('.easyImg').attr('src',newImg);
    	console.log('img'+newImg);
    })
  }




});

var rightcounter=0, wrongCounter=0;

$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var counter=0;
	var totalCounter=10;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);

	var newarray=arrayquestion;

	var arrLen=newarray.length;


	myquestion(newarray[counter],counter);


	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		$(".hint").hide(0);
		$("#Imgshow").fadeOut(0);
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2();
		}

		if(counter > totalCounter){
			ole.activityComplete.finishingcall();
		}
	});

	$('.allquestion').on("click","#fConfirmBx",function(){

			var thisQuestionNum=newarray[counter];
			var inputBoxName=$('.allquestion').find('#firstInput');
			var inputText=$('.allquestion').find('#firstInput').val();

			if( inputText.length === 0 ) {

				inputBoxName.css({'border':'1px solid #ff0000'});
			}
			else
			{
				$(this).hide(0);
				$(".hint").show(0);
				inputBoxName.attr('disabled','disabled');

				var isInArray=data.string["ans_"+thisQuestionNum];


				if(inputText==isInArray)
				{
					rightcounter++;
					$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
         			$("#activity-page-next-btn-enabled").fadeIn(10);
         			inputBoxName.css({'background':'#30D60F'});
         		}
				else
				{
					wrongCounter++;
					$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
         			$("#activity-page-next-btn-enabled").fadeIn(10);

         			inputBoxName.css({'background':'#ff0000'});
				}

			}

	});//click

});


function myquestion(questionNo,counter)
{

	loadTimelineProgress(11,(counter+1));

	var source;




	source   = $("#label-template").html();
	var template = Handlebars.compile(source);

	if(questionNo==3)
	{
		var $dataval={
				whatif:data.string.exe_3_1,
				inputBx:"firstInput",
				myfirstText:data.string["exe_"+questionNo],
				cbtnId:"fConfirmBx",
				cnftxt:data.string.confrim,
				lastText:data.string["re_"+questionNo],
				hint:data.string["ans_"+questionNo]
			};
	}
	else
	{
		var $dataval={
				imgsrc:$ref+"/exercise/images/img_"+questionNo+".png",
				inputBx:"firstInput",
				myfirstText:data.string["exe_"+questionNo],
				cbtnId:"fConfirmBx",
				cnftxt:data.string.confrim,
				lastText:data.string["re_"+questionNo],
				hint:data.string["ans_"+questionNo]
			};
	}



	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){


	});
}


function myquestion2()
{

	loadTimelineProgress(11,11);

	var source;

	source   = $("#label-template-2").html();
	var template = Handlebars.compile(source);


	var $dataval={
			allansw:[
				{quesno:"ques_1",ques:data.string.exe_1,imgques:$ref+"/exercise/images/img_1.png",qans:data.string.ans_1},
				{quesno:"ques_2",ques:data.string.exe_2,imgques:$ref+"/exercise/images/img_2.png",qans:data.string.ans_2},
				{quesno:"ques_3",ques:data.string.exe_3,whatif:data.string.exe_3_1,qans:data.string.ans_3,res:data.string.re_3},
				{quesno:"ques_4",ques:data.string.exe_4,imgques:$ref+"/exercise/images/img_4.png",qans:data.string.ans_4},
				{quesno:"ques_5",ques:data.string.exe_5,imgques:$ref+"/exercise/images/img_5.png",qans:data.string.ans_5},
				{quesno:"ques_6",ques:data.string.exe_6,imgques:$ref+"/exercise/images/img_6.png",qans:data.string.ans_6},
				{quesno:"ques_7",ques:data.string.exe_7,imgques:$ref+"/exercise/images/img_7.png",qans:data.string.ans_7},
				{quesno:"ques_8",ques:data.string.exe_8,imgques:$ref+"/exercise/images/img_8.png",qans:data.string.ans_8},
				{quesno:"ques_9",ques:data.string.exe_9,imgques:$ref+"/exercise/images/img_9.png",qans:data.string.ans_9},
				{quesno:"ques_10",ques:data.string.exe_10,imgques:$ref+"/exercise/images/img_10.png",qans:data.string.ans_10}
			],
			rite:data.string.q1_11,
			wrng:data.string.q1_12,
			rnum:rightcounter,
			wnum:wrongCounter
		};




	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){
		$("#activity-page-next-btn-enabled").show(0);
	});
}

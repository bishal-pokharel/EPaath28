var multiplysign = data.string.multiplySign;
var newWidth;
var whatIL, whatIB, whatIH;
$(function() {
  var $board = $("#myboard"),
    $canvas = $("canvas#myCanvas");

  $(".headTitle").html(data.string.p1_1);
  var whatnextBtn = getSubpageMoveButton($lang, "next");
  $("#activity-page-next-btn-enabled").html(whatnextBtn);

  $("#repeatBtn").html(getReloadBtn());
  var counter = 1;

  function resizeCanvas() {
    var mainWidth = $board.width();
    var mainheight = $board.height();

    if (mainWidth > mainheight) {
      newWidth = mainheight;
    } else newWidth = mainWidth;

    var canvasWidth = (450 / 896.6666) * mainWidth;

    $canvas.width(newWidth);
    $canvas.height(newWidth);
  }

  /*call at first load of page*/
  resizeCanvas();

  /*on window resize please check the size of the canvas*/
  $(window).resize(function() {
    resizeCanvas();
  });

  getHtml(counter);

  $("#activity-page-next-btn-enabled").click(function() {
    $(this).hide(0);

    counter++;

    getHtml(counter);
  });

  $("#repeatBtn").click(function() {
    location.reload();
  });

  $("#figBox").on("keyup", ".inBox", function() {
    if (/[^1-9]/g.test(this.value)) {
      this.value = this.value.replace(/[^1-9]/g, "");
    }
  });

  $("#figBox").on("click", "#cBtn", function() {
    var whatL = $("#length").val();
    var whatB = $("#breadth").val();
    var whatH = $("#height").val();

    if (whatL.length == 0 || whatB.length == 0 || whatH.length == 0) {
    } else if (whatL < whatB) {
      $(this)
        .parent("div.halftext")
        .siblings("p.slide1instruction")
        .show(0);
      $("#length").val("");
      $("#breadth").val("");
    } else {
      $(this)
        .parent("div.halftext")
        .siblings("p.slide1instruction")
        .hide(0);
      $(this).hide(0);
      $("#length, #breadth, #height").attr({ disabled: "disabled" });
      whatIL = parseInt($("#length").val());
      whatIB = parseInt($("#breadth").val());
      whatIH = parseInt($("#height").val());

      drawCube(whatIL, whatIB, whatIH, newWidth);
      $("#activity-page-next-btn-enabled").show(0);
    }
  });
});

function getdata(cnt) {
  var datas;

  switch (cnt) {
    case 1: {
      datas = {
        coverimagesrc: $ref + "/images/page1/angles_bg.png",
        coverimage: "coverpage_img",
        titleobj: data.lesson.chapter
      };
      $("#activity-page-next-btn-enabled").show(0);
      $("#covertext").css("opacity", 0);
      break;
    }
    case 2: {
      datas = {
        instructionText: data.string.p1_instruction,
        text1: data.string.p1_2,
        text2: data.string.p1_3,
        text3: data.string.p1_4,
        text4: data.string.p1_5,
        confirm: data.string.clickme,
        lengthBreadthInstruction: data.string.p1slide1instruction
      };
      break;
    }
    case 3: {
      datas = {
        img1: $ref + "/images/page1/img01.png",
        imgs1: "firstImg",
        textObj: data.string.p1_71,
        text1: data.string.p1_6
      };
      break;
    }

    case 4: {
      datas = {
        img1: $ref + "/images/page1/cube-animation.gif",
        imgs1: "firstImg",
        textObj: data.string.p1_7
      };
      break;
    }

    case 5: {
      datas = {
        img1: $ref + "/images/page1/cube-animation.png",
        imgs1: "firstImg",

        text2: data.string.p1_8,
        labelobjs: [
          { labelid: "label1", labelobj: "b " + multiplysign + " h" },
          { labelid: "label2", labelobj: "b " + multiplysign + " h" },
          { labelid: "label3", labelobj: "l " + multiplysign + " h" },
          { labelid: "label4", labelobj: "l " + multiplysign + " h" },
          { labelid: "label5", labelobj: "l " + multiplysign + " b" },
          { labelid: "label6", labelobj: "l " + multiplysign + " b" }
        ],
        text3: data.string.p1_9,
        text4: data.string.p1_10
      };
      break;
    }
    case 6: {
      datas = {
        text1: data.string.p1_12,
        text2: data.string.p1_13
      };
      break;
    }
  }

  return datas;
}

function getHtml(cnt) {
  var datas = getdata(cnt);

  var source;
  loadTimelineProgress(6, cnt);

  if (cnt == 2) source = $("#template-1").html();
  else if (cnt == 1) source = $("#template-2").html();
  else if (cnt > 2 && cnt < 6) source = $("#template-2").html();
  else source = $("#template-3").html();

  var template = Handlebars.compile(source);
  var html = template(datas);
    
  $(".totaltext")
    .fadeOut(10, function() {
      $(this).html(html);
    })
    .delay(10)
    .fadeIn(10, function() {
      if (cnt > 2 && cnt < 6) {
        $(".headTitle").hide(0);
        $("#myboard").hide(0);
        $(this).addClass("fullBx");
        getAnime(cnt);
      } else if (cnt == 6) {
        getAnime(cnt);
      }
    });
    if(cnt==3||cnt==4){
      $("#covertext").hide(0);
      console.log('hide covertext');
    }
}
function getAnime(cnt) {
  switch (cnt) {
    case 3:
    case 4: {
      $("#activity-page-next-btn-enabled").show(0);
      break;
    }
    case 5: {
      animate4();
      break;
    }
    case 6: {
      animate5();
      break;
    }
  }
}
function animate5() {
  $(".headTitle")
    .html(data.string.p1_11)
    .fadeIn(10);
  $("#myboard").show(0);
  $(".totaltext")
    .removeClass("fullBx")
    .addClass("fullBx2");
  $("#tempTxt3").html(
    "= 2 (" +
      whatIB +
      multiplysign +
      whatIH +
      " + " +
      whatIH +
      multiplysign +
      whatIL +
      " + " +
      whatIB +
      multiplysign +
      whatIL +
      ")"
  );

  var totalVal = 2 * (whatIB * whatIH + whatIH * whatIL + whatIB * whatIL);
  $("#tempTxt4").html("= " + totalVal);
  ole.footerNotificationHandler.pageEndSetNotification();
}

function animate4() {
  $("#label1")
    .delay(100)
    .fadeIn(500)
    .delay(100)
    .animate({ top: "90%" }, 1000)
    .delay(100)
    .fadeOut(10, function() {
      $(".whatif")
        .html(" b " + multiplysign + " h")
        .fadeIn(10);

      $("#label2")
        .delay(100)
        .fadeIn(500)
        .delay(100)
        .animate({ top: "90%" }, 1000)
        .delay(100)
        .fadeOut(10, function() {
          var htmlVal = $(".whatif").html();
          $(".whatif").html(htmlVal + " + b " + multiplysign + " h");

          $("#label3")
            .delay(100)
            .fadeIn(500)
            .delay(100)
            .animate({ top: "90%" }, 1000)
            .delay(100)
            .fadeOut(10, function() {
              var htmlVal = $(".whatif").html();
              $(".whatif").html(htmlVal + " + l " + multiplysign + " h");

              $("#label4")
                .delay(100)
                .fadeIn(500)
                .delay(100)
                .animate({ top: "90%" }, 1000)
                .delay(100)
                .fadeOut(10, function() {
                  var htmlVal = $(".whatif").html();
                  $(".whatif").html(htmlVal + " + l " + multiplysign + " h");
                  $("#label5")
                    .delay(100)
                    .fadeIn(500)
                    .delay(100)
                    .animate({ top: "90%" }, 1000)
                    .delay(100)
                    .fadeOut(10, function() {
                      var htmlVal = $(".whatif").html();
                      $(".whatif").html(
                        htmlVal + " + l " + multiplysign + " b"
                      );

                      $("#label6")
                        .delay(100)
                        .fadeIn(500)
                        .delay(100)
                        .animate({ top: "90%" }, 1000)
                        .delay(100)
                        .fadeOut(10, function() {
                          var htmlVal = $(".whatif").html();
                          $(".whatif").html(
                            htmlVal + " + l " + multiplysign + " b"
                          );
                          $("#txt4")
                            .delay(200)
                            .fadeIn(100, function() {
                              $("#txt5")
                                .delay(200)
                                .fadeIn(100, function() {
                                  $("#activity-page-next-btn-enabled").show(0);
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

function drawCube(l1, b1, h1, mainWidth) {
  var c = document.getElementById("myCanvas");
  var ctx = c.getContext("2d");

  var l, b, h;
  l = l1 * 25;
  b = b1 * 25;
  h = h1 * 25;

  b = b / 2;

  /*var r1X = ((mainWidth-100) / 2) - (l/2);
	var r1Y=((mainWidth-100) / 2) - (h/2 );*/
  var r1X = 30;
  var r1Y = 30;

  var r2X = r1X + b;
  var r2Y = r1Y + b;

  ctx.beginPath();

  ctx.strokeStyle = "#000000";
  ctx.lineWidth = 1;
  ctx.strokeRect(r1X, r1Y, l, h);

  ctx.strokeRect(r2X, r2Y, l, h);

  ctx.beginPath();

  ctx.moveTo(r1X, r1Y);
  ctx.lineTo(r2X, r2Y);

  ctx.moveTo(r1X + l, r1Y);
  ctx.lineTo(r2X + l, r2Y);

  ctx.moveTo(r1X, r1Y + h);
  ctx.lineTo(r2X, r2Y + h);

  ctx.moveTo(r1X + l, r1Y + h);
  ctx.lineTo(r2X + l, r2Y + h);
  ctx.stroke();
}

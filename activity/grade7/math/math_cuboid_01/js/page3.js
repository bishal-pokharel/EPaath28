

$(function(){


	
	
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	

	var counter=1;


	


	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){

		
		$(this).hide(0);
		
		counter++;
		
		if(counter<=3)
			getHtml(counter);
		else
		{
			animateLast();
		}
		
	});

	$("#figBox").on('click',".clickme",function(){
		var ids=$(this).attr('id');
		
		if(ids=="img1")
		{
			$("#whatClass").removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-ok');

		}
		else $("#whatClass").removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
		$("#activity-page-next-btn-enabled").show(0);

	});

	$("#figBox").on('click',".radioBx",function(){
		var ids=$(this).attr('id');
		if(ids=="rd3")
		{
			$(this).addClass('correctColor');
			$("#activity-page-next-btn-enabled").show(0);
		}
		else
		{
			$(this).addClass('colorChange');
		}

	});
	
	$("#figBox").on('click',"#cBtn1",function(){

		var nexttxt=$("#newVal1").val();
		if(nexttxt=="6l")
		{
			$("#whatClass").removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-ok');
			$(this).hide(0);
			$(".imgbx2").show(0);
			$("#activity-page-next-btn-enabled").show(0);
		}
		else
		{
			$("#whatClass").removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
		}
	});

	$("#figBox").on('click',"#cBtn2",function(){

		var nexttxt=$("#newVal2").val();

		if(nexttxt=="l")
		{
			$("#whatClass3").removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-ok');
			$(this).hide(0);
			$(".imgbx3").show(0);
			ole.footerNotificationHandler.lessonEndSetNotification();
		}
		else
		{
			$("#whatClass3").removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
		}
	});

	

})

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={img1:$ref+"/images/page3/img04.png",
	 			img2:$ref+"/images/page3/img02.png",
	 			img3:$ref+"/images/page3/img03.png",
	 			imgs1:"img1",
	 			imgs2:"img2",
	 			imgs3:"img3",
	 			text2:data.string.p3_1,	 			
	 			text3:data.string.p2_3
	 		};
	 		break;
	 	}	
	 	case 2:
	 	{
	 		datas={text1:data.string.p3_2,
	 			opttxt1:data.string.p3_3,
	 			opttxt2:data.string.p3_4,
	 			opttxt3:data.string.p3_5,
	 			opttxt4:data.string.p3_6,
	 			text6:data.string.p2_6,
	 			img1:$ref+"/images/page1/img01.png",
	 			text2:data.string.p3_7
	 		};
	 		break;
	 	}	
	 	case 3:
	 	{
	 		datas={text1:data.string.p3_8,
	 			text2:data.string.p3_12,
	 			img1:$ref+"/images/page3/cube-animation.gif",
	 			imgs1:"imglab1",
	 			imgs2:"imglab2",
	 			img2:$ref+"/images/page3/cube-animation.gif",
	 			txt1:data.string.p3_10,
	 			txt2:data.string.p3_14,
	 			confirm:data.string.confrim,
	 			txt4:data.string.p3_11,
	 			txt5:data.string.p3_15
	 		};
	 		break;
	 	}	
	}


	return datas;
}

function getHtml(cnt)
{
	
	var datas=getdata(cnt);


	var source;
	loadTimelineProgress(4,cnt);

	if(cnt==1)
	 source=$("#template-1").html();
	else if(cnt==2)
	 source=$("#template-2").html();
	else if(cnt==3)
	 source=$("#template-3").html();



	var template=Handlebars.compile(source);
	var html=template(datas);



	$("#figBox").fadeOut(10,function(){ 
		$(this).html(html); 
	}).delay(10).fadeIn(10,function(){
		
		
		
	});
	
	


}
function animateLast()
{
	loadTimelineProgress(4,4);
	$("#temptext2").show(0);
	$(".divCls:last-child").show(0);
	
}

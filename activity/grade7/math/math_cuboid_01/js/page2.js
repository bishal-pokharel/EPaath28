var multiplysign = data.string.multiplySign;
var newWidth;
var whatIL, whatIB, whatIH;
$(function(){


	var $board = $('#myboard'),
		$canvas=$("canvas");

	$(".headTitle").html(data.string.p2_1);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	$("#repeatBtn").html(getReloadBtn());

	var counter=1;

	function resizeCanvas(){
		var mainWidth = $board.width();
		var mainheight= $board.height();

		if(mainWidth>mainheight)
		{
			newWidth=mainheight;

		}
		else newWidth=mainWidth;

		
		var canvasWidth = 450/896.6666*mainWidth;
		
		$canvas.width(newWidth);
		$canvas.height(newWidth);
	}

	/*call at first load of page*/
	resizeCanvas();

	/*on window resize please check the size of the canvas*/
	$( window ).resize(function() {
		resizeCanvas();
	});


	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		
		$(this).hide(0);
		
		counter++;
		
		getHtml(counter);
		
	});

	$("#repeatBtn").click(function(){
		location.reload();
	});

	$("#figBox").on("keyup",".inBox",function(){
		
		if(/[^1-9]/g.test(this.value))
		{
		
			this.value = this.value.replace(/[^1-9]/g, '');
		}
	});

	$("#figBox").on('click','#cBtn',function(){

		
		var  whatL=$("#length").val();
		var  whatB=$("#breadth").val();
		var whatH=$("#height").val();

		if(whatL.length==0 || whatB.length==0 || whatH.length==0 )
		{
			
		}
		else if(whatL < whatB)
		{	
			$(this).parent("div.halftext").siblings('p.slide2instruction').show(0);
			$("#length").val('');
			$("#breadth").val('');
		}
		else
		{
			$(this).parent("div.halftext").siblings('p.slide2instruction').hide(0);
			$(this).hide(0);
			$("#length, #breadth, #height").attr({'disabled':'disabled'});
			 whatIL=parseInt($("#length").val());
			 whatIB=parseInt($("#breadth").val());
			 whatIH=parseInt($("#height").val());
			drawCube(whatIL,whatIB,whatIH,newWidth);
		}


	});

})

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={img1:$ref+"/images/page2/img_1.png",
	 			text2:data.string.p2_2,
	 			
	 			text3:data.string.p2_3
	 		};
	 		break;
	 	}	
	 	case 2:
	 	{
	 		datas={
	 			instructionText : data.string.p2_instruction,
	 			lengthBreadthInstruction : data.string.p1slide1instruction,
	 			text1:data.string.p1_2,
	 			text2:data.string.p1_3,
	 			text3:data.string.p1_4,
	 			text4:data.string.p1_5,
	 			text5:data.string.p2_5,
	 			text6:data.string.p2_6,
	 			confirm:data.string.clickme
	 		};
	 		break;
	 	}	
	}


	return datas;
}

function getHtml(cnt)
{
	
	var datas=getdata(cnt);


	var source;
	loadTimelineProgress(2,cnt);

	
	if(cnt==1)source=$("#template-2").html();
	
	else source=$("#template-1").html();


	var template=Handlebars.compile(source);
	var html=template(datas);



	$(".totaltext").fadeOut(10,function(){ $(this).html(html); 
	}).delay(10).fadeIn(10,function(){
		if(cnt==1)
		{
			
			$("#myboard").hide(0);
			$(this).addClass('fullBx');
			getAnime(cnt);
			
		}
		else
		{
			$("#myboard").show(0);
			$(this).removeClass('fullBx');
		}
		
		
	});
	
	


}
function getAnime(cnt)
{
	

	switch(cnt)
	{
		case 1:
		{
			$("#mytext1").delay(500).fadeIn(500,function(){
				$(".drawme").delay(500).fadeIn(500,function(){
					$("#activity-page-next-btn-enabled").show(0);
				});

			});
			break;
		}
		case 2:
		{

	 		
	 		break;
	 	}
	 	
	}
}



function drawCube(l1,b1,h1,mainWidth)
{

	
	var c = document.getElementById("myCanvas");
	var ctx = c.getContext("2d");


	var l , b, h;
	l= l1 * 25;
	b = b1 * 25;
	h = h1 * 25;

	b = b / 2;

	
	/*var r1X = ((mainWidth-100) / 2) - (l/2);
	var r1Y=((mainWidth-100) / 2) - (h/2 );*/

	var r1X = 20;
	var r1Y=20;

	var r2X= r1X + b;
	var r2Y= r1Y + b;

	

	ctx.beginPath();


	ctx.strokeStyle = "#000000";
	ctx.lineWidth   = 1;
	ctx.strokeRect(r1X, r1Y, l, h);

	ctx.strokeRect(r2X, r2Y, l, h);


	ctx.beginPath();

	ctx.moveTo(r1X, r1Y);
	ctx.lineTo(r2X, r2Y);
				
	ctx.moveTo(r1X + l, r1Y);
	ctx.lineTo(r2X + l, r2Y);
		
	ctx.moveTo(r1X, r1Y + h);
	ctx.lineTo(r2X, r2Y + h);
					
	ctx.moveTo(r1X + l, r1Y + h);
	ctx.lineTo(r2X + l, r2Y + h);
	ctx.stroke();

	$("#my2txt1").delay(500).fadeIn(300,function(){
		$("#my2txt2").delay(500).fadeIn(300,function(){

			$("#my2txt3").fadeOut(10,function(){
				$(this).html("= "+l1+multiplysign+b1+multiplysign+h1);
			}).delay(100).fadeIn(300,function(){

				$("#my2txt4").fadeOut(10,function(){
					var vals=l1*b1*h1;
					$(this).html("= "+vals);
				}).delay(100).fadeIn(100,function(){
					$("#my2txt5").fadeOut(10,function(){
						var vals=l1*b1*h1;
						var newvals=data.string.p2_7;
						var res = newvals.replace("%(volume)s", vals);
						$(this).html(res);
					}).delay(100).fadeIn(100,function(){
						ole.footerNotificationHandler.pageEndSetNotification();
					});
				});
			});
		});		
	});	//fadein
}
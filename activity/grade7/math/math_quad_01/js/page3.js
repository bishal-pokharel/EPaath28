$(function(){


	$(".headTitle").html(data.string.p3_1);
	$("#activity-page-next-btn-enabled").html(getSubpageMoveButton($lang,"next"));
	$("#activity-page-prev-btn-enabled").html(getSubpageMoveButton($lang,"prev"));

	var counter=1;

	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		
		$(this).hide(0);
		$('#prevbtn').hide(0);
		counter++;
		getHtml(counter);

	});

	$('#prevbtn').click(function(){
		
		$(this).hide(0);
		$("#activity-page-next-btn-enabled").hide(0);
		counter--;
		getHtml(counter);

	});

});

function getHtml(cnt)
{
	var datas=getdata(cnt);
	var source;
	loadTimelineProgress(3,cnt);

	
	source=$("#template-1").html();
	
	var template=Handlebars.compile(source);
	var html=template(datas);

	$("#animateBox").fadeOut(10,function(){

		$(this).html(html);


	}).delay(10).fadeIn(10,function(){

		getAnime(cnt);
		
	});


}

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={intitle:data.string.p3_2,
	 			img1:$ref+"/images/page3/01.png",
	 			fstline:data.string.p3_6,
	 			sndline:data.string.p3_2_1,
	 			trdline:data.string.p3_5,
	 			forline:data.string.p3_2_2,
	 			fifline:data.string.p3_2_3
	 		};
	 		break;
	 	}	
	 	case 2:
	 	{
	 		datas={intitle:data.string.p3_3,
	 			img1:$ref+"/images/page3/02.png",
	 			fstline:data.string.p3_6,
	 			sndline:data.string.p3_3_1,
	 			trdline:data.string.p3_5,
	 			forline:data.string.p3_3_2,
	 			fifline:data.string.p3_3_3
	 		};
	 		break;
	 	}	
	 	case 3:
	 	{
	 		datas={intitle:data.string.p3_4,
	 			img1:$ref+"/images/page3/03.png",
	 			fstline:data.string.p3_6,
	 			sndline:data.string.p3_4_1,
	 			trdline:data.string.p3_5,
	 			forline:data.string.p3_4_2,
	 			fifline:data.string.p3_4_3
	 		};
	 		break;
	 	}	
	 	case 4:
	 	{
	 		datas={fstline:data.string.p3_7
	 		};
	 		break;
	 	}	
	 	
	}

	return datas;
}



function getAnime(cnt)
{
	$("#fstline").fadeIn(100,function(){
		$("#sndline").delay(1000).fadeIn(100,function(){
			$("#trdline").delay(1000).fadeIn(100,function(){
				$("#forline").delay(1000).fadeIn(100,function(){
					$("#fifline").delay(1000).fadeIn(100,function(){
						if(cnt>1) $('#prevbtn').show(0);
						if(cnt<3) $("#activity-page-next-btn-enabled").show(0);
						if(cnt==3)
						{
							ole.footerNotificationHandler.lessonEndSetNotification();
						}

					});
				});
			});
		});

	});
}


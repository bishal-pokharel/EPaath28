$(function(){


	$(".headTitle").html(data.string.p2_1);

	$("#activity-page-next-btn-enabled").html(getSubpageMoveButton($lang,"next"));
	$("#activity-page-prev-btn-enabled").html(getSubpageMoveButton($lang,"prev"));

	var counter=1;

	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){

		$(this).hide(0);
		$('#prevbtn').hide(0);
		counter++;
		getHtml(counter);

	});

	$('#prevbtn').click(function(){

		$(this).hide(0);
		$("#activity-page-next-btn-enabled").hide(0);
		counter--;
		getHtml(counter);

	});

});

function getHtml(cnt)
{
	var datas=getdata(cnt);
	var source;

	loadTimelineProgress(8,cnt);

	source=$("#template-1").html();

	var template=Handlebars.compile(source);
	var html=template(datas);

	$("#animateBox").fadeOut(10,function(){

		$(this).html(html);


	}).delay(10).fadeIn(10,function(){

		getAnime(cnt);

	});


}

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={intitle:data.string.p2_1_1,
	 			idme:"int1",
	 			img1:$ref+"/images/page2/1.png",
	 			concline:data.string.p2_1_3,
	 			rname:data.string.p2_1a,
	 			rside:data.string.p2_1b,
	 			rangle:data.string.p2_1c,
	 			rtotal:data.string.p2_1d,
	 			names:data.string.p2_1_1,
	 			sides:data.string.p2_1_5,
	 			angles:data.string.p2_1_6,
	 			totals:data.string.p2_1_7
	 		};
	 		break;
	 	}
	 	case 2:
	 	{
	 		datas={intitle:data.string.p2_2_1,
	 			idme:"int1",
	 			img1:$ref+"/images/page2/2.png",
	 			concline:data.string.p2_1_3,
	 			rname:data.string.p2_1a,
	 			rside:data.string.p2_1b,
	 			rangle:data.string.p2_1c,
	 			rtotal:data.string.p2_1d,
	 			names:data.string.p2_2_1,
	 			sides:data.string.p2_2_5,
	 			angles:data.string.p2_2_6,
	 			totals:data.string.p2_2_7,
	 			sndor:data.string.p2_1_O,
	 			scnd:data.string.p2_2_3
	 		};
	 		break;
	 	}
	 	case 3:
	 	{
	 		datas={intitle:data.string.p2_3_1,
	 			idme:"int1",
	 			img1:$ref+"/images/page2/03.png",
	 			concline:data.string.p2_1_3,
	 			rname:data.string.p2_1a,
	 			rside:data.string.p2_1b,
	 			rangle:data.string.p2_1c,
	 			rtotal:data.string.p2_1d,
	 			names:data.string.p2_3_1,
	 			sides:data.string.p2_3_5,
	 			angles:data.string.p2_3_6,
	 			totals:data.string.p2_3_7,
	 			sndor:data.string.p2_1_O,
	 			scnd:data.string.p2_2_3
	 		};
	 		break;
	 	}
	 	case 4:
	 	{
	 		datas={intitle:data.string.p2_4_1,
	 			idme:"int1",
	 			img1:$ref+"/images/page2/04.png",
	 			concline:data.string.p2_1_3,
	 			rname:data.string.p2_1a,
	 			rside:data.string.p2_1b,
	 			rangle:data.string.p2_1c,
	 			rtotal:data.string.p2_1d,
	 			names:data.string.p2_4_1,
	 			sides:data.string.p2_4_5,
	 			angles:data.string.p2_4_6,
	 			totals:data.string.p2_4_7,
	 			sndor:data.string.p2_1_O,
	 			scnd:data.string.p2_2_3
	 		};
	 		break;
	 	}
	 	case 5:
	 	{
	 		datas={
	 			intitle:data.string.p2_5_1,
	 			idme:"int1",
	 			img1:$ref+"/images/page2/05.png",
	 			concline:data.string.p2_1_3,
	 			rname:data.string.p2_1a,
	 			rside:data.string.p2_1b,
	 			rangle:data.string.p2_1c,
	 			rtotal:data.string.p2_1d,
	 			names:data.string.p2_5_1,
	 			sides:data.string.p2_5_5,
	 			angles:data.string.p2_5_6,
	 			totals:data.string.p2_5_7,
	 			sndor:data.string.p2_1_O,
	 			scnd:data.string.p2_2_3
	 		};
	 		break;
	 	}
	 	case 6:
	 	{
	 		datas={
	 			intitle:data.string.p2_6_1,
	 			idme:"int1",
	 			img1:$ref+"/images/page2/06.png",
	 			concline:data.string.p2_1_3,
	 			rname:data.string.p2_1a,
	 			rside:data.string.p2_1b,
	 			rangle:data.string.p2_1c,
	 			rtotal:data.string.p2_1d,
	 			names:data.string.p2_6_1,
	 			sides:data.string.p2_6_5,
	 			angles:data.string.p2_6_6,
	 			totals:data.string.p2_6_7,
	 			sndor:data.string.p2_1_O,
	 			scnd:data.string.p2_2_3
	 		};
	 		break;
	 	}
	 	case 7:
	 	{
	 		datas={
	 			intitle:data.string.p2_7_1,
	 			idme:"int1",
	 			img1:$ref+"/images/page2/07.png",
	 			concline:data.string.p2_1_3,
	 			rname:data.string.p2_1a,
	 			rside:data.string.p2_1b,
	 			rangle:data.string.p2_1c,
	 			rtotal:data.string.p2_1d,
	 			names:data.string.p2_7_1,
	 			sides:data.string.p2_7_5,
	 			angles:data.string.p2_7_6,
	 			totals:data.string.p2_7_7,
	 			sndor:data.string.p2_1_O,
	 			scnd:data.string.p2_2_3
	 		};
	 		break;
	 	}
	 	case 8:
	 	{
	 		datas={
	 			intitle:data.string.p2_8_1,
	 			idme:"int1",
	 			img1:$ref+"/images/page2/08.png",
	 			concline:data.string.p2_1_3,
	 			rname:data.string.p2_1a,
	 			rside:data.string.p2_1b,
	 			rangle:data.string.p2_1c,
	 			rtotal:data.string.p2_1d,
	 			names:data.string.p2_8_1,
	 			sides:data.string.p2_8_5,
	 			angles:data.string.p2_8_6,
	 			totals:data.string.p2_8_7,
	 			sndor:data.string.p2_1_O,
	 			scnd:data.string.p2_2_3
	 		};
	 		break;
	 	}
	}

	return datas;
}



function getAnime(cnt)
{


	switch(cnt)
	{
		case 1:
	 	{
	 		anime1();
	 		break;
	 	}
	 	case 2:
	 	{
	 		anime2(data.string.p2_2_2, data.string.p2_2_2_ans, "2a", data.string.p2_2_4, data.string.p2_2_4_ans, data.string.p2_2_3, data.string.p2_2_3_ans, cnt);
	 		break;
	 	}
	 	case 3:
	 	{
	 		anime2(data.string.p2_3_2, data.string.p2_3_2_ans, "03a", data.string.p2_3_4, data.string.p2_3_4_ans, data.string.p2_3_3, data.string.p2_3_3_ans,cnt);
	 		break;
	 	}
	 	case 4:
	 	{
	 		anime2(data.string.p2_4_2, data.string.p2_4_2_ans, "04a", data.string.p2_4_4, data.string.p2_4_4_ans, data.string.p2_4_3, data.string.p2_4_3_ans,cnt);
	 		break;
	 	}
	 	case 5:
	 	{
	 		anime2(data.string.p2_5_2, data.string.p2_5_2_ans, "05a", data.string.p2_5_4, data.string.p2_5_4_ans, data.string.p2_5_3, data.string.p2_5_3_ans,cnt);
	 		break;
	 	}
	 	case 6:
	 	{
	 		anime2(data.string.p2_6_2, data.string.p2_6_2_ans, "06a", data.string.p2_6_4, data.string.p2_6_4_ans, data.string.p2_6_3, data.string.p2_6_3_ans,cnt);
	 		break;
	 	}
	 	case 7:
	 	{
	 		anime2(data.string.p2_7_2, data.string.p2_7_2_ans, "07a", data.string.p2_7_4, data.string.p2_7_4_ans, data.string.p2_7_3, data.string.p2_7_3_ans,cnt);
	 		break;
	 	}
	 	case 8:
	 	{
	 		anime2(data.string.p2_8_2, data.string.p2_8_2_ans, "08a", data.string.p2_8_4, data.string.p2_8_4_ans, data.string.p2_8_3, data.string.p2_8_3_ans,cnt);
	 		break;
	 	}
	 }
}

function anime1()
{
	$(".intitle").fadeIn(100,function(){
		$("#figBox").fadeIn(10,function(){

			var whatnext=data.string.p2_1_2+"<input id='finput' type='text' class='textinput' />"+
				"<div class='goTo' id='fArrow'>"+getArrowBtn("next")+"</div>";

			$(".frst").html(whatnext).fadeIn(10,function(){

				$("#fArrow").show(0);

				$("#fArrow").click(function(){


					var valit=$("#finput").val();

					var pattern=/^\d*$/;

					if(!valit.match(pattern))
					{
						$("#finput").val('').focus();
					}
					else if(valit=="" || valit=='0')
					{

						$("#finput").val('').focus();

					}
					else
					{
						if(valit=="3")
						{
							$(this).hide(0);
							$("#finput").attr({'disabled':'diasbled'});

							$(".extraline").fadeIn(400,function(){



								whatnext=data.string.p2_1_4+"<input id='finput2' type='text' class='textinput' /> °"+
									"<div class='goTo' id='fArrow2'>"+getArrowBtn("next")+"</div>";

								$(".concline").html(whatnext).fadeIn(10,function(){
									$("#fArrow2").show(0);
									$("#fArrow2").click(function(){
										var valit=$("#finput2").val();

										var pattern=/^\d*$/;

										if(!valit.match(pattern))
										{
											$("#finput2").val('').focus();
										}
										else if(valit=="" || valit=='0')
										{
											$("#finput2").val('').focus();
										}
										else
										{
											if(valit=="180")
											{
												$(this).hide(0);
												$("#finput2").attr({'disabled':'diasbled'});

												$(".tabular").delay(500).fadeIn(100,function(){
													$("#activity-page-next-btn-enabled").show(0);
												});

											}
											else
											{
												$("#finput2").val('').focus();

											}
										}
									});//click
								});//html//fadein
							});//fadein
						}//if
						else
						{
							$("#finput").val('').focus();
						}//else
					}//else
				});//click
			});//fadein
		});//fadein
	});//fadein
}


function anime2(data1, check1, imag1, data2, check2, data3, check3, mycounter)
{
	$(".intitle").fadeIn(100,function(){
		$("#figBox").fadeIn(10,function(){
			var whatnext;
			whatnext=normalString(data1, "finput","fArrow");

			$(".frst").html(whatnext).fadeIn(10,function(){
				$("#fArrow").show(0);
				$("#fArrow").click(function(){
					if(checkVal("#finput",check1))
					{
						$(this).hide(0);



						$(".extraline").fadeIn(400,function(){

							if(mycounter==2 || mycounter==5)
							{
								whatnext=betweenString(data3, "finput3","fArrow3" );
							}
							else if(mycounter==3 || mycounter==6){
								whatnext=betweenStringNoDegree(data3, "finput3","fArrow3" );
							}
							else if(mycounter==4 || mycounter==7 || mycounter==8){
								whatnext=normalStringWithDegree(data3, "finput3","fArrow3" );
							}
							else
							{
								whatnext=normalString(data3, "finput3","fArrow3" );
							}

							$(".scnd").html(whatnext).fadeIn(10,function(){

								$("#fArrow3").show(0);

								$("#fArrow3").click(function(){
									if(checkVal("#finput3",check3))
									{
										$(this).hide(0);
										$(".snd").fadeIn(10,function(){

											$(".inbx").find('img').attr({'src':$ref+'/images/page2/'+imag1+'.png'});

											if(mycounter==4 || mycounter==7)
											{

												whatnext=betweenString(data2, "finput2","fArrow2" )

											}
											else if(mycounter==5 || mycounter==8)
											{

												whatnext=beginString(data2, "finput2","fArrow2" )

											}
											else
											{
												whatnext=normalStringWithDegree(data2, "finput2","fArrow2" );

											}

											$(".concline").html(whatnext).delay(500).fadeIn(10,function(){
												$("#fArrow2").show(0);
												$("#fArrow2").click(function(){
													if(checkVal("#finput2",check2))
													{
														$(this).hide(0);


														$(".tabular").delay(500).fadeIn(100,function(){

															if(mycounter<8)
																$("#activity-page-next-btn-enabled").show(0);
															$("#activity-page-prev-btn-enabled").show(0);
															if(mycounter==8)
															{
																ole.footerNotificationHandler.pageEndSetNotification();

															}
														});
													}
												});//click
											});//html fadin

										});//fadein

									}//check if
								});//click
							});//html fadein

							/**/
						});//fadein
					}//if

				});//click
			});//html fadein
		});//fadein
	});//fadein
}



function checkVal(testval,checkval)
{


	var valit=$(testval).val();

	var pattern=/^\d*$/;

	if(!valit.match(pattern))
	{
		$(testval).val('').focus();
	}
	else if(valit=="" || valit=='0')
	{

		$(testval).val('').focus();

	}
	else
	{
		if(valit==checkval)
		{
			$(testval).attr({'disabled':'diasbled'});
			return true;
		}
		else
		{
			$(testval).val('').focus();
		}
	}
	return false;

}

function normalString(data1, id,arrow )
{

	var whatnext=data1+"<input id='"+id+"' type='text' class='textinput' />"+
				"<div class='goTo' id='"+arrow+"'>"+getArrowBtn("next")+"</div>";
	return whatnext;
}

function normalStringWithDegree(data1, id,arrow )

{
	var whatnext=data1+"<input id='"+id+"' type='text' class='textinput' /> °"+
				"<div class='goTo' id='"+arrow+"'>"+getArrowBtn("next")+"</div>";
	return whatnext;
}

function betweenString(data1, id, arrow )
{

	var newval=data1.split("YYYYYYYY");

	var whatnext=newval[0]+
	"<input id='"+id+"' type='text' class='textinput' /> °"+
	newval[1]+
	"<div class='goTo' id='"+arrow+"'>"+getArrowBtn("next")+"</div>";

	return whatnext;
}

function betweenStringNoDegree(data1, id, arrow )
{

	var newval=data1.split("YYYYYYYY");

	var whatnext=newval[0]+
	"<input id='"+id+"' type='text' class='textinput' />"+
	newval[1]+
	"<div class='goTo' id='"+arrow+"'>"+getArrowBtn("next")+"</div>";

	return whatnext;
}

function beginString(data1, id, arrow )
{

	var newval=data1.split("YYYYYYYY");

	whatnext="<input id='"+id+"' type='text' class='textinput' /> "+
		newval[1]+
		"<div class='goTo' id='"+arrow+"'>"+getArrowBtn("next")+"</div>";
	return whatnext;
}

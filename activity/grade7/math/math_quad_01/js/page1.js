$(function(){

	var $next_btn = $("#activity-page-next-btn-enabled");
	var $prev_btn = $("#activity-page-prev-btn-enabled");

	loadTimelineProgress(2, 1);
	// $(".headTitle").html(data.string.p1_1);
	var sld_no = 1;
	$next_btn.show();
	$next_btn.click(function(){
		sld_no++;
		$next_btn.hide(0);
		getCases(sld_no);
	});
	$prev_btn.click(function(){
		sld_no--;
		$prev_btn.hide(0);
		getCases(sld_no);
	});

	// first slide call
	slide_1();

	function getCases(slide_num){
		loadTimelineProgress(2, slide_num);
		switch(slide_num){
			case 1:
				slide_1();
			break;
			case 2:
				slide_2();
			break;
		}
	}


	function slide_1(){
		var content={
			p_class:"cover_title",
			p_data:data.lesson.chapter,
			img_class:"coverimg",
			img_src:$ref+"/images/page1/coverpage.png"
		}
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);

		var html = template(content);
		$("#cover_page").html(html);
		$next_btn.show(0);
	}

	function slide_2(){
		$("#cover_page, .coverimg, .cover_title").css("display", 'none');
		$(".headTitle").html(data.string.p1_1);
		var counter=1;
		var clicked_counter = 0;
		for(var i=2; i<=6;i++)
		{
			$("#p1_"+i).html(data.string["p1_"+i]);
		}

		// clickme
		$(".optionbx, .whatvals, .figBox").css("z-index", 1000);
	 $('.options').click(function(){

		 if($(this).hasClass('clickme'))
		 {
			 ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_pg1);
			 $('.options').removeClass('clickme');
			 var id=$(this).attr('id');

			 var timestramp=new Date().getTime();

				 switch(id)
				 {
					 case "p1_2":
					 {
						 clicked_counter++;
	if(clicked_counter==5){
		ole.footerNotificationHandler.pageEndSetNotification();
	}
						 if($(".inbx> img") > 0){
							 $(".inbx>img").remove();
						 }
						 $(".inbx").html("<img src= "+ $ref+'/images/page1/rectangle.gif'+"></img>");
						 //$(".inbx> img").attr('src',$ref+'/images/page1/rectangle.gif');
						 $("#whatvals").addClass('whatval').html(data.string.p1_7);
						 break;
					 }
					 case "p1_3":
					 {
						 clicked_counter++;
	if(clicked_counter==5){
		ole.footerNotificationHandler.pageEndSetNotification();
	}
						 if($(".inbx> img") > 0){
							 $(".inbx>img").remove();
						 }
						 $(".inbx").html("<img src= "+ $ref+'/images/page1/square.gif?'+"></img>");



						 //$(".inbx").find('img').attr('src',$ref+'/images/page1/square.gif?'+timestramp);
						 $("#whatvals").addClass('whatval').html(data.string.p1_8);
						 break;
					 }
					 case "p1_4":
					 {
						 clicked_counter++;
	if(clicked_counter==5){
		ole.footerNotificationHandler.pageEndSetNotification();
	}
						 if($(".inbx> img") > 0){
							 $(".inbx>img").remove();
						 }
						 $(".inbx").html("<img src= "+ $ref+'/images/page1/rhombus.gif?'+"></img>");
						 //$(".inbx").find('img').attr('src',$ref+'/images/page1/rhombus.gif?'+timestramp);
						 $("#whatvals").addClass('whatval').html(data.string.p1_9);
						 break;
					 }
					 case "p1_5":
					 {
						 clicked_counter++;
	if(clicked_counter==5){
		ole.footerNotificationHandler.pageEndSetNotification();
	}
						 if($(".inbx> img") > 0){
							 $(".inbx>img").remove();
						 }
						 $(".inbx").html("<img src= "+ $ref+'/images/page1/parallelogram.gif'+"></img>");
						 //$(".inbx").find('img').attr('src',$ref+'/images/page1/parallelogram.gif?'+timestramp);
						 $("#whatvals").addClass('whatval').html(data.string.p1_10);
						 break;
					 }
					 case "p1_6":
					 {
						 clicked_counter++;
	if(clicked_counter==5){
		ole.footerNotificationHandler.pageEndSetNotification();
	}

						 if($(".inbx> img") > 0){
							 $(".inbx>img").remove();
						 }
						 $(".inbx").html("<img src= "+ $ref+'/images/page1/kite.gif'+"></img>");
						 //$(".inbx").find('img').attr('src',$ref+'/images/page1/kite.gif?'+timestramp);
						 $("#whatvals").addClass('whatval').html(data.string.p1_11);
						 break;
					 }
				 }
			 setTimeout(function(){
				 $('.options').addClass('clickme');
			 },7000);

		 }
	 });
	}

	// $(".headTitle").html(data.string.p1_1);
	// var counter=1;
	// var clicked_counter = 0;
	// for(var i=2; i<=6;i++)
	// {
	// 	$("#p1_"+i).html(data.string["p1_"+i]);
	// }

	// $(".covertext").html(eval("data.lesson.chapter"));
	// $(".cover_cross").on("click", function(){
	// 	$(".covertext, .cover_cross, .cover_page").hide(0);
	// });
// 	$('.options').click(function(){
//
// 		if($(this).hasClass('clickme'))
// 		{
// 			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_pg1);
// 			$('.options').removeClass('clickme');
// 			var id=$(this).attr('id');
//
// 			var timestramp=new Date().getTime();
//
// 				switch(id)
// 				{
// 					case "p1_2":
// 					{
// 						clicked_counter++;
// if(clicked_counter==5){
//   ole.footerNotificationHandler.pageEndSetNotification();
// }
// 						if($(".inbx> img") > 0){
// 							$(".inbx>img").remove();
// 						}
// 						$(".inbx").html("<img src= "+ $ref+'/images/page1/rectangle.gif'+"></img>");
// 						//$(".inbx> img").attr('src',$ref+'/images/page1/rectangle.gif');
// 						$("#whatvals").addClass('whatval').html(data.string.p1_7);
// 						break;
// 					}
// 					case "p1_3":
// 					{
// 						clicked_counter++;
// if(clicked_counter==5){
//   ole.footerNotificationHandler.pageEndSetNotification();
// }
// 						if($(".inbx> img") > 0){
// 							$(".inbx>img").remove();
// 						}
// 						$(".inbx").html("<img src= "+ $ref+'/images/page1/square.gif?'+"></img>");
//
//
//
// 						//$(".inbx").find('img').attr('src',$ref+'/images/page1/square.gif?'+timestramp);
// 						$("#whatvals").addClass('whatval').html(data.string.p1_8);
// 						break;
// 					}
// 					case "p1_4":
// 					{
// 						clicked_counter++;
// if(clicked_counter==5){
//   ole.footerNotificationHandler.pageEndSetNotification();
// }
// 						if($(".inbx> img") > 0){
// 							$(".inbx>img").remove();
// 						}
// 						$(".inbx").html("<img src= "+ $ref+'/images/page1/rhombus.gif?'+"></img>");
// 						//$(".inbx").find('img').attr('src',$ref+'/images/page1/rhombus.gif?'+timestramp);
// 						$("#whatvals").addClass('whatval').html(data.string.p1_9);
// 						break;
// 					}
// 					case "p1_5":
// 					{
// 						clicked_counter++;
// if(clicked_counter==5){
//   ole.footerNotificationHandler.pageEndSetNotification();
// }
// 						if($(".inbx> img") > 0){
// 							$(".inbx>img").remove();
// 						}
// 						$(".inbx").html("<img src= "+ $ref+'/images/page1/parallelogram.gif'+"></img>");
// 						//$(".inbx").find('img').attr('src',$ref+'/images/page1/parallelogram.gif?'+timestramp);
// 						$("#whatvals").addClass('whatval').html(data.string.p1_10);
// 						break;
// 					}
// 					case "p1_6":
// 					{
// 						clicked_counter++;
// if(clicked_counter==5){
//   ole.footerNotificationHandler.pageEndSetNotification();
// }
//
// 						if($(".inbx> img") > 0){
// 							$(".inbx>img").remove();
// 						}
// 						$(".inbx").html("<img src= "+ $ref+'/images/page1/kite.gif'+"></img>");
// 						//$(".inbx").find('img').attr('src',$ref+'/images/page1/kite.gif?'+timestramp);
// 						$("#whatvals").addClass('whatval').html(data.string.p1_11);
// 						break;
// 					}
// 				}
// 			setTimeout(function(){
// 				$('.options').addClass('clickme');
// 			},7000);
//
// 		}
// 	});

});

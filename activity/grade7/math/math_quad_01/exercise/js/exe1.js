$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	$("#toDo").html(data.string.e_1);


	var counter=0;
	var totalCounter=9;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9);

	var newarray=arrayquestion;

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;

	myquestion(newarray[counter],counter);



	$(".allquestion").on("click",".ans",function(){

		var isCorrect=parseInt($(this).attr('corr'));


		if(newarray[counter]>5)
		{
			var isCorrect2=$(this).attr('corr');
			var whatInput=$("#showAnsBx").find('input').val();
			if(whatInput)
			{
				if(whatInput==isCorrect2)
				{

					$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);


					rightcounter++;
					$("#activity-page-next-btn-enabled").fadeIn(10,function(){

				});
				}
				else
				{
					$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);


					wrongCounter++;
				}

			}
			else
			{
				$("#showAnsBx").find('input').css({"border":"#ff0000 1px solid"});
			}
		}
		else
		{


			if(newarray[counter]==1)
			{
				$("#showImg2 div").removeClass('ans');

				$("#showImg2 div[corr='1']").addClass('corrans');

			}

			$("#showAns div").removeClass('ans');

			/*added by dilak*/
				$("#showAns div[corr='0']").addClass('wroans');

				$("#showAns div[corr='1']").addClass('corrans');

			if(isCorrect==1)
			{
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);


				rightcounter++;
			}
			else
			{
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);


				wrongCounter++;
			}

		}

		$("#activity-page-next-btn-enabled").fadeIn(10,function(){

		});
	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}

		if(counter > totalCounter){
			loadTimelineProgress(11,11)
			ole.activityComplete.finishingcall();
		}
	});

});

var randOMNumber;

function myquestion(questionNo,counter)
{
	var whatRandNum=Math.floor(Math.random() * 6) + 1;
	randOMNumber=whatRandNum;
	loadTimelineProgress(11,(counter+1));

	var source   = $("#label-template").html();

	if(questionNo==1)
		source   = $("#label-template-2").html();
	else if(questionNo>=2  && questionNo<=5)
			source   = $("#label-template-3").html();


	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter,whatRandNum)
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#Imgshow").fadeOut(10);

	}).delay(100).fadeIn(10,function(){

		if(questionNo==2)
		{
			$("#img2_1").append("<div id='whatMe1'>"+data.string.p1_9+"</div>");
		}
	});
}

function myquestion2(right,wrong)
{
	loadTimelineProgress(11,10);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var $dataval={
		rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:data.string["exe_1_"+randOMNumber], imgobj: $ref+"/exercise/images/img_"+randOMNumber+".png"},
			{ques:data.string.exe_2, qans: data.string.opt_2_2,quesno:"ques_2"},
			{ques:data.string.exe_3, qans: data.string.opt_3_1,quesno:"ques_3"},
			{ques:data.string.exe_4, qans: data.string.opt_4_4,quesno:"ques_4"},
			{ques:data.string.exe_5, qans: data.string.opt_5_4,quesno:"ques_5"},
			{ques:data.string.opt_6_1, imgques: $ref+"/exercise/images/img_7.png",qans:data.string.opt_6_ans},
			{ques:data.string.opt_7_1,  imgques: $ref+"/exercise/images/img_7.png",qans:data.string.opt_7_ans1},
			{ques:data.string.opt_8_1,imgques: $ref+"/exercise/images/img_7.png", qans: data.string.opt_8_ans+" "+data.string.opt_8_2},
			{ques:data.string.exe_9,imgques: $ref+"/exercise/images/img_9.png", qans: data.string.opt_9_ans1}

		]


	}
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.exe3);
		$("#Imgshow").fadeOut(10);


	}).delay(100).fadeIn(10,function(){

		$("#activity-page-next-btn-enabled").delay(100).fadeIn(10);

	});
}

function getQuestion($quesNo,counter,whatRandNum)
{
	var quesList;
	var nep=ole.nepaliNumber(counter+1);

	var whatri=whatCorr($quesNo);



	 if($quesNo==6)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/img_7.png"}];
	}
	else if($quesNo==7)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/img_8.png"}];
	}
	else if($quesNo==8)
	{
		var imgObjA=[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/img_7.png"}];
	}
	else if($quesNo==9)
	{
		var imgObjA=[{mul:'c55',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/img_9.png"}];
	}


	if($quesNo==1)
	{

		 var whatquest=data.string["exe_"+$quesNo+"_"+whatRandNum];


		 var whatCorr5=whatCorrForOne(whatRandNum);

		 var imgObjA=[
				{mul:'optionImg',imgObj1:"img"+$quesNo+"_1_1", imgObjval:$ref+"/exercise/images/img_1.png", corr:whatCorr5[0]},
				{mul:'optionImg',imgObj1:"img"+$quesNo+"_1_2", imgObjval:$ref+"/exercise/images/img_2.png", corr:whatCorr5[1]},
				{mul:'optionImg',imgObj1:"img"+$quesNo+"_1_3", imgObjval:$ref+"/exercise/images/img_3.png", corr:whatCorr5[2]},
				{mul:'optionImg',imgObj1:"img"+$quesNo+"_1_4", imgObjval:$ref+"/exercise/images/img_4.png", corr:whatCorr5[3]},
				{mul:'optionImg',imgObj1:"img"+$quesNo+"_1_5", imgObjval:$ref+"/exercise/images/img_5.png", corr:whatCorr5[4]},
				{mul:'optionImg',imgObj1:"img"+$quesNo+"_1_6", imgObjval:$ref+"/exercise/images/img_6.png", corr:whatCorr5[5]}


			];

		 quesList={
			myQuesion:whatquest,
			img22:$ref+"/exercise/images/blank.png",
			imgobj:imgObjA,

		}


	}
	else if($quesNo>=2  && $quesNo<=5)
	{


		quesList={
			myQuesion:data.string["exe_"+$quesNo],
			img22:$ref+"/exercise/images/blank.png",
			imgobj:imgObjA,
			optObj:[
					{optObj1:"opt_"+$quesNo+"_1", yesno:whatri[0],optObjval:data.string["opt_"+$quesNo+"_1"]},
					{optObj1:"opt_"+$quesNo+"_2", yesno:whatri[1],optObjval:data.string["opt_"+$quesNo+"_2"]},
					{optObj1:"opt_"+$quesNo+"_3", yesno:whatri[2],optObjval:data.string["opt_"+$quesNo+"_3"]},
					{optObj1:"opt_"+$quesNo+"_4", yesno:whatri[3],optObjval:data.string["opt_"+$quesNo+"_4"]}
					]
		}
	}
	else
	{
		quesList={
			myQuesion:data.string["exe_"+$quesNo],
			imgobj:imgObjA,
			txt1:data.string["opt_"+$quesNo+"_1"],
			txt2:data.string["opt_"+$quesNo+"_2"],
			confrimval:data.string.exe4,
			corr:data.string["opt_"+$quesNo+"_ans"]
		}
	}
	return quesList;
}

function whatCorr($quesNo)
{
	var whatis=new Array();
	switch($quesNo)
	{

		case 2: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0; whatis[3]=0;	break;
		case 3: 	whatis[0]=1;	whatis[1]=0;	whatis[2]=0; whatis[3]=0;	break;
		case 4: 	whatis[0]=0;	whatis[1]=0;	whatis[2]=0; whatis[3]=1;	break;
		case 5: 	whatis[0]=0;	whatis[1]=0;	whatis[2]=0; whatis[3]=1;	break;

	}
	return whatis;
}


function whatCorrForOne($randno)
{
	var whatis=new Array();

	switch($randno)
	{
		case 1: 	whatis[0]=1;	whatis[1]=0;	whatis[2]=0; whatis[3]=0;	whatis[4]=0;	whatis[5]=0; 	break;
		case 2: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0; whatis[3]=0;	whatis[4]=0;	whatis[5]=0; 		break;
		case 3: 	whatis[0]=0;	whatis[1]=0;	whatis[2]=1; whatis[3]=0;	whatis[4]=0;	whatis[5]=0; 	  	break;
		case 4: 	whatis[0]=0;	whatis[1]=0;    whatis[2]=0; whatis[3]=1;	whatis[4]=0;	whatis[5]=0; 	 	break;
		case 5: 	whatis[0]=0;	whatis[1]=0;    whatis[2]=0; whatis[3]=0;	whatis[4]=1;	whatis[5]=0; 		break;
		case 6: 	whatis[0]=0;	whatis[1]=0;    whatis[2]=0; whatis[3]=0;	whatis[4]=0;	whatis[5]=1; 		break;

	}
	return whatis;


}

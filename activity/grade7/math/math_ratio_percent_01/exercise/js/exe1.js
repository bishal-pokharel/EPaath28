var initialval=0;
var finalval=0;
$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var counter=0;
	var totalCounter=10;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);

	var newarray=arrayquestion;

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;

	myquestion(newarray[counter],counter);

	$(".allquestion").on("click",".ans",function(){

		var isCorrect=$(this).attr('corr');

		$("#showAns div").removeClass('ans');
		if(isCorrect=="yes")
		{
			$(this).css({'background':'#30D60F'});
			rightcounter++;
			$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);

		}
		else
		{
			$("#showAns div:not([corr='no'])").css({'background':'#30D60F'});
			wrongCounter++;
			$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
		}
		$("#activity-page-next-btn-enabled").show(0);


	});

	$(".allquestion").on("keyup",".myIntinput",function(){


		if(/\D/g.test(this.value))
		{

			this.value = this.value.replace(/\D/g, '');
		}
	});

	$(".allquestion").on("click","#confirm1",function(){

		var whatIntss=$(".allquestion").find("#inputid1").val();
		if(whatIntss.length==0)
		{
			$("#inputid1").css({'border':'1px solid #ff0000'});
		}
		else
		{
			$(this).hide(0);
			initialval=parseInt($(".allquestion").find("#inputid1").val());

			$("#inputid1").attr({'disabled':'disbaled'});

			finalval=parseInt($('.allquestion').find('#inputid1').attr('corrans'));
			if(initialval==finalval)
			{
				rightcounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);

				$("#inputid1").css({'background':'#30D60F'});
				$('#activity-page-next-btn-enabled').show(0);

			}
			else
			{
				wrongCounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);


				$("#inputid1").css({'background':'#ff0000'});

				$('#activity-page-next-btn-enabled').show(0);

			}
		}
	});

	$(".allquestion").on("click","#inConfirmAll1",function(){

		var in1=$("#input1");
		var in2=$("#input2");
		var in3=$("#input3");
		var in4=$("#input4");

		var val1=in1.val();
		var val2=in2.val();
		var val3=in3.val();
		var val4=in4.val();


		if(val1.length==0)
		{
			in1.css({'border':'1px solid #ff0000'});
		}
		else if(val2.length==0)
		{
			in2.css({'border':'1px solid #ff0000'});
		}
		else if(val3.length==0)
		{
			in3.css({'border':'1px solid #ff0000'});
		}
		else if(val4.length==0)
		{
			in4.css({'border':'1px solid #ff0000'});
		}
		else
		{
			$(this).hide(0);
			$(".myIntinput").attr({'disabled':'disbaled'});
			var vals1=parseInt(in1.val());
			var vals2=parseInt(in2.val());
			var vals3=parseInt(in3.val());
			var vals4=parseInt(in4.val());

			if(newarray[counter]==7)
				var corrvalues=[parseInt(data.string.opt_7_1),parseInt(data.string.opt_7_2),parseInt(data.string.opt_7_3),parseInt(data.string.opt_7_4)];
			else
				var corrvalues=[parseInt(data.string.opt_8_1),parseInt(data.string.opt_8_2),parseInt(data.string.opt_8_3),parseInt(data.string.opt_8_4)];



			var isInArray1=jQuery.inArray(vals1, corrvalues);
			var isInArray2=jQuery.inArray(vals2, corrvalues);
			var isInArray3=jQuery.inArray(vals3, corrvalues);
			var isInArray4=jQuery.inArray(vals4, corrvalues);

			if(isInArray1==-1 || isInArray2==-1 || isInArray3==-1 || isInArray4==-1)
			{
				wrongCounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);

				$('#activity-page-next-btn-enabled').show(0);
			}
			else
			{
				var whattxt=vals1/vals2;
				var whattxt2=vals3/vals4;


				if(whattxt===whattxt2)
				{
					rightcounter++;
					$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
     				$("#activity-page-next-btn-enabled").show(0);
				}
				else
				{
					wrongCounter++;
					$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
					$('#activity-page-next-btn-enabled').show(0);
				}
			}
		}
	});

	$(".allquestion").on("click","#inConfirmAll2",function(){
		var in4=$("#input4");

		var val4=in4.val();
		if(val4.length==0)
		{
			in4.css({'border':'1px solid #ff0000'});
		}
		else
		{
			$(this).hide(0);
			$("#input4").attr({'disabled':'disbaled'});
			var vals4=parseInt(in4.val());

			var ansval=parseInt(data.string["ans_"+newarray[counter]]);

			if(vals4==ansval)
			{
				$("#input4").css({'background':'#30D60F'});
				rightcounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
     			$("#activity-page-next-btn-enabled").show(0);
			}
			else
			{
				wrongCounter++;
				$("#input4").css({'background':'#ff0000'});
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
				$('#activity-page-next-btn-enabled').show(0);
			}
		}
	});


	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}

		if(counter > totalCounter){
			ole.activityComplete.finishingcall();
		}
	});

});

var randOMNumber;

function myquestion(questionNo,counter)
{

	loadTimelineProgress(11,(counter+1));

	var $dataval=getQuestion(questionNo,counter)


	var source   = $("#label-template").html();


	if(questionNo>4 && questionNo<=6)
		source   = $("#label-template-2").html();
	else if(questionNo>6  && questionNo<=8)
		source   = $("#label-template-3").html();
	else if(questionNo>8 && questionNo<=9)
		source   = $("#label-template-4").html();
	else if(questionNo>9 )
		source   = $("#label-template-5").html();

	var template = Handlebars.compile(source);


	var html=template($dataval);


	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#Imgshow").fadeOut(10);

	}).delay(100).fadeIn(10,function(){

	});
}

function myquestion2(right,wrong)
{
	loadTimelineProgress(11,11);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var $dataval={
		rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:data.string["exe_1"], qans:data.string.ans_1},
			{ques:data.string.exe_2, qans: data.string.ans_2},
			{ques:data.string.exe_3, qans: data.string.ans_3},
			{ques:data.string.exe_4, qans: data.string.ans_4},
			{ques:data.string.exe_5, qans: data.string.opt_5_1},
			{ques:data.string.exe_6, qans:data.string.opt_6_3},
			{ques:data.string.exe_7,  qans:data.string.ans_7},
			{ques:data.string.exe_8, qans: data.string.ans_8},
			{ques:data.string.exe_9,qans: data.string.ans_9_a},
			{ques:data.string.exe_9,qans: data.string.ans_10_a}

		]


	}
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.exe3);
		$("#Imgshow").fadeOut(10);


	}).delay(100).fadeIn(10,function(){

		$("#activity-page-next-btn-enabled").delay(100).fadeIn(10);

	});
}

function getQuestion($quesNo,counter)
{
	var quesList;


	var whatri=whatCorr($quesNo);



	switch($quesNo)
	{
		case 1:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				imgobj:[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/q03.png"}],
				confrimval:data.string.exe4,
				ans:data.string.ans_1,
				txt2: " % ",
				types:"inputid1"
			}
			break;
		}
		case 2:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				imgobj:[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/q02.png"}],
				confrimval:data.string.exe4,
				ans:data.string.ans_2,
				txt1: " Rs. ",
				types:"inputid1"
			}
			break;
		}
		case 3:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				imgobj:[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/q01.png"}],
				confrimval:data.string.exe4,
				ans:data.string.ans_3,
				txt2: " % ",
				types:"inputid1"
			}
			break;
		}
		case 4:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				imgobj:[{mul:'c1',imgObj1:"img"+$quesNo+"_1", imgObjval:$ref+"/exercise/images/q04.png"}],
				confrimval:data.string.exe4,
				ans:data.string.ans_4,
				txt2: " km ",
				types:"inputid1"
			}
			break;
		}

		case 5:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				optObj:[{optObj1:"Opt_5_1",optObjval:data.string.opt_5_1, yesno:"yes"},
				{optObj1:"Opt_5_2",optObjval:data.string.opt_5_2, yesno:"no"}]

			}
			break;
		}
		case 6:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				optObj:[{optObj1:"Opt_6_1",optObjval:data.string.opt_6_3,yesno:"yes"},
				{optObj1:"Opt_6_2",optObjval:data.string.opt_6_2,yesno:"no"},
				{optObj1:"Opt_6_3",optObjval:data.string.opt_6_1,yesno:"no"}]

			}
			break;
		}
		case 7:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				confrimval:data.string.exe4
			}
			break;
		}
		case 8:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				confrimval:data.string.exe4
			}
			break;
		}
		case 9:
		{
			quesList={
				myQuesion:data.string["exe_"+$quesNo],
				confrimval:data.string.exe4,
				txt1:data.string.opt_9_1,
				txt2:data.string.opt_9_3,
				txt3:data.string.opt_9_2
			}
			break;
		}
		case 10:
		{
			quesList={
				myQuesion:data.string["exe_"+9],
				confrimval:data.string.exe4,
				txt1:data.string.opt_10_1,
				txt2:data.string.opt_10_2,
				txt3:data.string.opt_10_3
			}
			break;
		}
	}

	return quesList;
}

function whatCorr($quesNo)
{
	var whatis=new Array();
	switch($quesNo)
	{

		case 2: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0; whatis[3]=0;	break;
		case 3: 	whatis[0]=1;	whatis[1]=0;	whatis[2]=0; whatis[3]=0;	break;
		case 4: 	whatis[0]=0;	whatis[1]=0;	whatis[2]=0; whatis[3]=1;	break;
		case 5: 	whatis[0]=0;	whatis[1]=0;	whatis[2]=0; whatis[3]=1;	break;

	}
	return whatis;
}

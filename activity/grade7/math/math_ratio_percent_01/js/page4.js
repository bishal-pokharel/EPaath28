var initialval=0;
var finalval=0;

$(function(){


	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);



	$(".headTitle").html(data.string.p4_1);
	$("#textId1").html(data.string.p4_2);
	$("#textId2").html(data.string.p4_3);
	$("#textId3").html(data.string.p4_4);
	$("#textId4").html(data.string.p4_5);
	$("#textId5").html(data.string.p4_6);
	$("#textId6").html(data.string.p4_7);
	$("#textId7").html(data.string.p4_8);




	var counter=1;

	getAnimate(counter);

	$("#activity-page-next-btn-enabled").click(function(){

		$(this).hide(0);

		counter++;


		getAnimate(counter);

	});

	$("#repeatBtn").click(function(){
		location.reload();
	});





})

function getAnimate(cnt)
{
	loadTimelineProgress(7,cnt);
	switch(cnt)
	{
		case 1:
		{

			$("#activity-page-next-btn-enabled").show(0);
			break;
		}
		/*following lines removed because they seem redundant*/
		// case 2:
		// {
		// 	$("#textId1").show(0);
		// 	$("#activity-page-next-btn-enabled").show(0);

		// 	break;
		// }
		case 2:
		{
			$("#textId1").hide(0);
			$("#textId2").show(0);
			$('.whatImage').css({'opacity':1});
			$("#activity-page-next-btn-enabled").show(0);
			break;
		}
		case 3:
		{
			$("#textId3").show(0);

			$("#activity-page-next-btn-enabled").show(0);
			break;
		}
		case 4:
		{
			mathParseReplace(data.string.p4_5, data.string.p4_5_1, data.string.p4_5_2, "#textId4" );


			$("#activity-page-next-btn-enabled").show(0);break;
		}
		case 5:
		{
			mathParseReplace(data.string.p4_6, data.string.p4_6_1, data.string.p4_6_2, "#textId5" );
			$("#textId5").show(0);
			$("#activity-page-next-btn-enabled").show(0);break;
		}
		case 6:
		{
			mathParseReplace(data.string.p4_7, data.string.p4_7_1, data.string.p4_7_2, "#textId6" );

			$("#activity-page-next-btn-enabled").show(0);break;
		}
		case 7:
		{
			mathParseReplace(data.string.p4_8, data.string.p4_8_1, data.string.p4_8_1, "#textId7" );





			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_1);
			ole.footerNotificationHandler.pageEndSetNotification();
			break;
		}
	}

}




function mathParseReplace(totalString, stringtoBRepalce, stringtoBRepalce2, selector )
{


	var str=totalString;
	var string=stringtoBRepalce;

	var string2=stringtoBRepalce2;

	var changeText="$${"+string+"}$$";


	var changeText2="$${"+string2+"}$$";

	var firstOne=false;



	if(string==string2)
	{

		firstOne=false;
		var what=encodeURIComponent(str)
		var res = str.replace(/4\/5/g, changeText);




	}
	else
	{
		firstOne=true;
		var res = str.replace(string, changeText);
		var res2 = res.replace(string2, changeText2);
	}



	$(selector).fadeOut(10,function(){
		if(firstOne==false)
		{	$(this).html(res);}
		else $(this).html(res2);
		 M.parseMath(document.body);
	}).delay(10).fadeIn(10);
}

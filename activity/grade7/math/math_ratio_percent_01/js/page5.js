var initialval=0;
var finalval=0;

$(function(){


	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	


	

	var counter=1;
	getHtml(counter);
	// getHtml(counter+=2);

	$("#activity-page-next-btn-enabled").click(function(){
		
		$(this).hide(0);
		
		counter++;
		
		getHtml(counter);
		
	});

	$("#repeatBtn").click(function(){
		location.reload();
	});

	
	$("#figBox").on("keyup",".txtInputBx",function(){
	
		
		if(/\D/g.test(this.value))
		{
		
			this.value = this.value.replace(/\D/g, '');
		}
	});
	$("#figBox").on("click",".confirmbtn",function(){

		var id=$(this).attr('id');
		var inputId,nextshow;
		var ansVal;

		if(id=="firstConfirm")
		{
			inputId="#firstInput";
			txtId="#textFirstId2";	
			ansVal=parseInt(data.string.p5_30_ans_1);
		}
		else if(id=="secConfirm")
		{
			inputId="#secInput";
			txtId="#textFirstId3";	
			ansVal=parseInt(data.string.p5_30_ans_2);
		}
		else if(id=="trdConfirm")
		{
			inputId="#trdInput";
			txtId="#textFirstId4, #textFirstId5, #textFirstId6";	
			ansVal=parseInt(data.string.p5_30_ans_2);
		}
		else if(id=="frthConfirm")
		{
			inputId="#frthInput";
			
			ansVal=parseInt(data.string.p5_30_ans_3);
		}

		var whatIntss=$("#figBox").find(inputId).val();
		if(whatIntss.length==0)
		{
		}
		else
		{
			initialval=parseInt($("#figBox").find(inputId).val());
			
			if(initialval.length==0)
			{

			}
			else if(initialval==ansVal)
			{
				$(this).hide(0);
				$(inputId).attr({'disabled':'disbaled'}).css({"background":"green"});
				
				if(id=="frthConfirm")
				{
					
					ole.footerNotificationHandler.lessonEndSetNotification();
			
				}
				else	$(txtId).show(0);
			}
			else
			{
				$(inputId).css({"background":"red"});
			}
		}
	});//click

	
});



function getHtml(counter)
{
	var datas;
	
	switch(counter)
	{
		case 1:
		{
			datas={headtitle:data.string.p5_1,
	 			myclass:"firstInnerBx",
	 			txtOnj:[{txtId:"txtVal1",textVal:data.string.p5_2},
	 			{txtId:"txtVal2",textVal:data.string.p5_3},
	 			{txtId:"txtVal3",textVal:data.string.p5_4},
	 			{txtId:"txtVal4",textVal:data.string.p5_5},
	 			{txtId:"txtVal5",textVal:data.string.p5_6},
	 			{txtId:"txtVal6",textVal:data.string.p5_7},
	 			{txtId:"txtVal7",textVal:data.string.p5_8},
	 			{txtId:"txtVal8",textVal:data.string.p5_9},
	 			{txtId:"txtVal9",textVal:data.string.p5_10}] 			
	 			
	 		};
	 		break;
		}
		case 2:
		{
			datas={headtitle:data.string.p5_11,
	 			myclass:"secInnerBx",
	 			txtOnj:[{txtId:"txtVal1",textVal:data.string.p5_12},
	 			{txtId:"txtVal2",textVal:data.string.p5_13},
	 			{txtId:"txtVal3",textVal:data.string.p5_14},
	 			{txtId:"txtVal4",textVal:data.string.p5_15},
	 			{txtId:"txtVal5",textVal:data.string.p5_16},
	 			{txtId:"txtVal6",textVal:data.string.p5_17}],
	 			myImg:$ref+"/images/page5/sound.gif",
	 			imgcls:"soundImage"		
	 			
	 		};
	 		break;

		}
		case 3:
		{
			datas={headtitle:data.string.p5_18,
	 			myclass:"trdInnerBx",
	 			confirmText:data.string.p2_1_a,
	 			textVal:data.string.p5_19,
	 			txtId:"txtVal1",

	 			textfirst:data.string.p5_20	,
	 			inText1:data.string.p5_21	,
	 			textsec:data.string.p5_22,
	 			inText2:data.string.p5_23,
	 			texttrd:data.string.p5_24,
	 			inText3:data.string.p5_25,
	 			inText4:data.string.p5_26,
	 			inText5:data.string.p5_27,
	 			textfft1: data.string.p5_28,
	 			//textfft2: data.string.p5_29,
	 			textfft:data.string.p5_30,
	 			feet:data.string.p5_29,//data.string.p5_31,
	 			myImg:$ref+"/images/page5/house.png",
	 			imgcls:"imgcls" 			
	 		};

		}
	}
	
	loadTimelineProgress(3,counter);

	var source;
	

	
	if (counter > 2) {
		source = $("#template-2").html();
	} else {
		source = $("#template-1").html();
	}

	
	var template=Handlebars.compile(source);
	var html=template(datas);

	

	$("#figBox").fadeOut(10,function(){

		$(this).html(html);

		


	}).delay(10).fadeIn(10,function(){

			getAnimate(counter);
		
	});


}
function getAnimate(cnt)
{
	
	switch(cnt)
	{
		
		case 1:
		{
			animate1();
			break;
		}
		case 2:
		{
			animate2();
			break;
		}		
		case 3:
		{
			$("#textFirstId1").delay(500).fadeIn(10);
			break;
		}
		

	}

}


function animate1()
{
	var mytext,whatstring,changeText, newval,changeText2;
	$("#txtVal2").delay(500).fadeIn(10,function(){

		mytext=$("#txtVal3").html();
		whatstring=data.string.p5_4_1;
		changeText="$${"+whatstring+"}$$";
		newval = mytext.replace(whatstring, changeText);
		$("#txtVal3").delay(500).fadeIn(10,function(){
			$(this).html(newval);
		 	M.parseMath(document.body);


		 	mytext=$("#txtVal4").html();
			whatstring=data.string.p5_5_1;
			changeText="$${"+whatstring+"}$$";
			newval = mytext.replace(whatstring, changeText);

		 	$("#txtVal4").delay(500).fadeIn(10,function(){
				$(this).html(newval);
		 		M.parseMath(document.body);

		 		mytext=$("#txtVal5").html();
				whatstring=data.string.p5_5_1;
				changeText="$${"+whatstring+"}$$";
				changeText2="$${"+data.string.p5_4_1+"}$$";
				newval = mytext.replace(whatstring, changeText).replace(data.string.p5_4_1,changeText2);
				
				$("#txtVal5").delay(500).fadeIn(10,function(){
					$(this).html(newval);
			 		M.parseMath(document.body);

					mytext=$("#txtVal6").html();
					
					newval = mytext.replace(whatstring, changeText).replace(data.string.p5_4_1,changeText2);
					
					$("#txtVal6").delay(500).fadeIn(10,function(){
						$(this).html(newval);
						M.parseMath(document.body);

						$("#txtVal7").delay(500).fadeIn(10,function(){


							mytext=$("#txtVal8").html();
							whatstring=data.string.p5_9_1;
							changeText="$${"+whatstring+"}$$";
							newval = mytext.replace(whatstring, changeText);
							
							$("#txtVal8").delay(500).fadeIn(10,function(){
								$(this).html(newval);
								M.parseMath(document.body);

								$("#txtVal9").delay(500).fadeIn(10,function(){
									$("#activity-page-next-btn-enabled").show(0);
								});//9
							});//8
							
						});//7
					});//6

			 	});//5
			});//4
		
		});//3

	});//2
}


function animate2()
{
	var mytext,whatstring,changeText, newval,changeText2;
	$("#txtVal2").delay(500).fadeIn(10,function(){

		
		$("#txtVal3").delay(500).fadeIn(10,function(){
		

			mytext=$("#txtVal4").html();
			whatstring=data.string.p5_15_1;
			changeText="$${"+whatstring+"}$$";
			changeText2="$${"+data.string.p5_15_2+"}$$";
			newval = mytext.replace(whatstring, changeText).replace(data.string.p5_15_2,changeText2);


		 	$("#txtVal4").delay(500).fadeIn(10,function(){
				$(this).html(newval);
		 		M.parseMath(document.body);

		 		$("#txtVal5").delay(500).fadeIn(10,function(){

		 			$("#txtVal6").delay(500).fadeIn(10,function(){
		 					$("#activity-page-next-btn-enabled").show(0);
		 			});//6

		 		});//5
		 			
			});//4
		
		});//3

	});//2
}


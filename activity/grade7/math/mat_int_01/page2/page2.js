$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);

	// pull all data
	$("#int2topic").text(data.string.pg2s1);
	$("#ex1 > em").text(data.string.pg2s10);
	$("#ex1 > p").text(data.string.pg2s26);

	$("#ex2 > em").text(data.string.pg2s2);
	$("#ex2 > p").text(data.string.pg2s7);
	$("#ex2 > strong").text(data.string.pg2s9);

	$("#ex3 > em").text(data.string.pg2s27);
	$("#range > em").text(data.string.pg2s35);
	$("#labelHill").text(data.string.pg2s29);
	$("#labelGorge").text(data.string.pg2s30);
	$("#labelGround").text(data.string.pg2s31);
	$("#ex3detail").text(data.string.pg2s28);

	$("#fig1 > figcaption").text(data.string.pg2s3);
	$("#fig2 > figcaption").text(data.string.pg2s4);
	$("#fig3 > figcaption").text(data.string.pg2s5);
	$("#options > b").text(data.string.pg2s6);

	// ole.footerNotificationHandler.setNotificationMsg(data.string.pg2n1);

	// on clicking any options at first show the respective div
	$("#options > figure").on('click',function() {
		// ole.footerNotificationHandler.pageEndSetNotification() :
		var selected= $(this).attr("id");
		$("#options").hide(0);
		switch(selected){
			case "fig1":$("#ex1").show(0);
						$("#ex1 > p").show(0).addClass('animated infinite fadeIn');
						ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg2n2);
						break;
			case "fig2":$("#ex2").show(0);
						$("#soluHot > img:nth-of-type(2)").show(0);
						$("#ex2 > strong").show(0).addClass('animated infinite flash');
						ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg2n2);
						break;
			case "fig3":$("#ex3").show(0);
						$("#ex3detail").addClass('animated infinite flash');
						ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg2n2);
						ole.footerNotificationHandler.pageEndSetNotification();
						break;
			default:break;
		}
	});

	// on clicking close button on each example
	$(".myCloseStyle").on('click',function() {
		location.reload();
	});

	// for example2 soluHot thermometer
	$("#soluHot > img:nth-of-type(2)").on('click', function() {
		$("#soluHot > img:nth-of-type(2)").hide(0);
		$("#soluHot").css({"display":"none"});
		var timestamp = new Date().getTime();
		image = $ref+"/page2/image/thermoFall.gif?"+timestamp;
		// console.log(image);
		$("#soluCold").show(0);
		$("#soluCold > img:nth-of-type(2)").attr("src",image).show(0);
		$("#ex2 > p").text(data.string.pg2s8).css({"color":"brown"});
		$("#ex2 > strong").css({"display":"none"});
		$("#ex2 > strong").removeAttr("class");
	});

	// for example2 soluHot thermometer
	$("#soluCold > img:nth-of-type(2)").on('click', function() {
		$("#soluCold > img:nth-of-type(2)").hide(0);
		$("#soluCold").css({"display":"none"});
		var timestamp = new Date().getTime();
		image = $ref+"/page2/image/thermoRise.gif?"+timestamp;
		$("#soluHot").show(0);
		$("#soluHot > img:nth-of-type(2)").attr("src",image).show(0);
		$("#ex2 > p").text(data.string.pg2s7).css({"color":"blue"});
	});

	// on hovering the images on example 2
	$("#ex1Collect > img").on('mouseover', function() {
		var overMe = $(this).attr("id");
		$("#ex1Detail").show(0);
		$("#ex1 > p").removeClass('animated infinite fadeIn').css({"display":"none"});
		switch(overMe){
			case "op1": $("#ex1Detail > strong").text(data.string.pg2s11);
			 			$("#names > p:nth-of-type(1)").text(data.string.pg2s12);
			 			$("#names > p:nth-of-type(2)").text(data.string.pg2s13);
			 			$("#ex1Detail > ul > li:nth-of-type(1)").text(data.string.pg2s14);
			 			$("#ex1Detail > ul > li:nth-of-type(2)").text(data.string.pg2s15);
			 			$("#ex1Detail > img").attr("src",$ref+"/page2/image/bank1.png");
						break;
			case "op2": $("#ex1Detail > strong").text(data.string.pg2s16);
			 			$("#names > p:nth-of-type(1)").text(data.string.pg2s17);
			 			$("#names > p:nth-of-type(2)").text(data.string.pg2s18);
			 			$("#ex1Detail > ul > li:nth-of-type(1)").text(data.string.pg2s19);
			 			$("#ex1Detail > ul > li:nth-of-type(2)").text(data.string.pg2s20);
			 			$("#ex1Detail > img").attr("src",$ref+"/page2/image/bank2.png");
						break;
			case "op3": $("#ex1Detail > strong").text(data.string.pg2s21);
			 			$("#names > p:nth-of-type(1)").text(data.string.pg2s22);
			 			$("#names > p:nth-of-type(2)").text(data.string.pg2s23);
			 			$("#ex1Detail > ul > li:nth-of-type(1)").text(data.string.pg2s24);
			 			$("#ex1Detail > ul > li:nth-of-type(2)").text(data.string.pg2s25);
			 			$("#ex1Detail > img").attr("src",$ref+"/page2/image/bank3.png");
						break;
			default:break;
		}
	});

	// on hovering height example image
	$("#overHill, #overGorge, #overGround").hover(function() {
		$("#ex3detail").removeClass('animated infinite flash');
		/* Stuff to do when the mouse enters the element */
		var overMe = $(this).attr("id");
		switch(overMe){
			case "overHill":$("#ex3detail").text(data.string.pg2s32);
							$("#labelHill,#range >b:nth-of-type(1),#ex3detail").css({"color":"blue"});

							break;
			case "overGorge":$("#ex3detail").text(data.string.pg2s33);
							$("#labelGorge,#range >b:nth-of-type(3),#ex3detail").css({"color":"blue"});

							break;
			case "overGround":$("#ex3detail").text(data.string.pg2s34);
							$("#labelGround,#range >b:nth-of-type(2),#ex3detail").css({"color":"blue"});

							break;
			default:break;
		}
	}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#ex3detail").text(data.string.pg2s28);
		$("#ex3detail").css({"color":"#F33343"});

		$("#ex3image > em").css({"color":"#EA44A3"});
		$("#range > b").css({"color":"black"});
	});


});

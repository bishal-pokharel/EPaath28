//animation for fifth step
function fifthStep(){
	$("#integer > p:nth-of-type(1)").fadeIn(1000,function(){
				// next page attention
				$(".footer-next").addClass('animated wobble infinite');
				$("#whole > strong:nth-of-type(1)").show(0).addClass('animated bounce infinite');
				// just add animated here
				$("#zero").addClass('animated');
				$("#activity-page-next-btn-enabled").off("click");
				/*$("#activity-page-next-btn-enabled").removeClass("animated infinite shake");*/
				$("#activity-page-next-btn-enabled").css({"display":"none"});
				ole.footerNotificationHandler.setNotificationMsg(data.string.pg1n1);
			});
}

// animation for fourth step
function fourthStep(){
	$("#seq, #animDetail").hide(0,function(){
		$("#integer").show(0).addClass('animated slideInRight').one(vendorAnimationEnd,function(){
			$("#activity-page-next-btn-enabled").show(0);
		});
	});
}

//animation for third step
function thirdStep(){
	// hide detail and change text to be reused later
	$("#animDetail").hide(0).text(data.string.pg1s4).removeClass('flash');
	$("#seq").hide(0).css({"top":"70%"});
	$("#animDetail").css({"top":"80%"});
	$("#whole").show(0,function(){
		$("#seq").show(0);
		$("#seq").children('span:nth-of-type(4)').delay(1000).fadeIn(1000,function(){
			// hide detail and change text to be reused later
			$("#seq").children('span:nth-of-type(3)').show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
				$("#seq").children('span:nth-of-type(2)').show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
					$("#seq").children('span:nth-of-type(1)').show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
						$("#animDetail").show(0).addClass('flash').one(vendorAnimationEnd,function(){
							$("#activity-page-next-btn-enabled").show(0);//.addClass('shake');
						});
					});
				});
			});
		});
	});
}

// animation for second step
function secondStep(){
	$("#animDetail").hide(0).text(data.string.pg1s3);
	$("#natural").show(0).addClass('animated slideInRight').one(vendorAnimationEnd,function(){
		$("#seq").children('span:nth-of-type(5)').show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
			$("#animDetail").show(0,function(){
				$("#activity-page-next-btn-enabled").show(0);//.addClass('shake');
			});
		});
	});

}

// animation at the start
function startshow(startvar){
$(".cover_image, .cover_text").hide(0)
	$("#int1topic").show(0).addClass('animated rotateInUpRight').one(vendorAnimationEnd,function(){
			// first show the b tag and then its contents
		$("#seq").show(0);
			// show natural numbers
		$("#seq").children('span:nth-of-type(6)').show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
			$("#seq").children('span:nth-of-type(7)').show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
				$("#seq").children('span:nth-of-type(8)').show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
					$("#seq").children('span:nth-of-type(9)').show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
						// change text and show details
						$("#animDetail").text(data.string.pg1s2).show(0).addClass('animated flash').one(vendorAnimationEnd,function(){
							$("#activity-page-next-btn-enabled").show(100);//.addClass('animated infinite shake');

						});
					});
				});
			});
		});
	});
}

// variable for using with animate css animation - keep track of animation end with one()
var  vendorAnimationEnd= "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

// variable to keep track of click of next button starts from 2
var nxtCount=2;

$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);
		console.log(nxtCount);
	// if(3>nxtCount>0){
	// 	$('#activity-page-prev-btn-disabled').show(0);
	// }
	// pull all data
	$(".cover_text").html(eval("data.lesson.chapter"));
	$("#int1topic").text(data.string.pg1s1);
	$("#natural > em").text(data.string.pg1s2);
	$("#whole > em").text(data.string.pg1s3);
	$("#integer > em").text(data.string.pg1s4);
	$("#integer > p").text(data.string.pg1s5);
	$("#whole > strong:nth-of-type(1)").text(data.string.pg1s6);
	$("#zero > p:nth-of-type(1)").text(data.string.pg1s7);
	$("#zero > p:nth-of-type(2)").text(data.string.pg1s8);
	$("#zero > p:nth-of-type(3)").text(data.string.pg1s9);

	ole.parseToolTip("#natural","प्राकृतिक (Natural) सङ्ख्या");
	ole.parseToolTip("#natural","गन्&#8205;ने सङ्ख्या");
	ole.parseToolTip("#natural","घनात्मक सङ्ख्या");
	ole.parseToolTip("#whole","पूर्ण (Whole) सङ्ख्या");
	ole.parseToolTip("#integer","पूर्णाङ्क सङ्ख्या (Integer)");
	$("#natural .parsedString").css({'color':"brown","border-bottom":"none"});
	$("#whole .parsedString").css({'color':"darkgreen","border-bottom":"none"});
	$("#integer .parsedString").css({'color':"darkblue","border-bottom":"none"});
	// animation starts here
	// startshow(0);

	// on clicking zeros story
	$("#whole > strong:nth-of-type(1)").on('click', function() {
		$("#zero").removeClass("hinge");
		$("#zero").show(0).addClass('lightSpeedIn');
		ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg1n2);
	});

	// on clicking close on zero
	$("#zero > div:nth-of-type(1)").on('click', function() {
		$("#zero").removeClass("lightSpeedIn");
		$("#zero").addClass('hinge').one(vendorAnimationEnd,function(){
			$("#zero").hide(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		});
	});
	$("#activity-page-next-btn-enabled").show(0);
	// on clicking the next button
	$("#activity-page-next-btn-enabled").on('click', function() {
		console.log(nxtCount);
		switch(nxtCount){
			case 2:
				startshow(0);
			break;
			case 3:
			$("#activity-page-next-btn-enabled").hide(0).removeClass("shake");
					secondStep();
					break;
			case 4:
			$("#activity-page-next-btn-enabled").hide(0).removeClass("shake");
					thirdStep();
					break;
			case 5:
			$("#activity-page-next-btn-enabled").hide(0).removeClass("shake");
					fourthStep();
					break;
			case 6:
			$("#activity-page-next-btn-enabled").hide(0).removeClass("shake");
					fifthStep();
					break;
			default:break;
		}
		nxtCount++;
	});
});

// next count for steps in example
var nextStep = 2;

// next count for example
var nextExample = 2;

// previous count for example
var prevExample = 1;

// flag for once all the examples are completed
// this is set to true once all the examples are seen
var completed = false;

$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(4,1);

	/*the next example nxtEx button*/
	var $nxtEx = $("#activity-page-next-btn-enabled");
	var $prevEx = $("#activity-page-prev-btn-enabled");

	// if($lang === "en"){
	// 	var $nsrc="images/arrows/next_"+$lang+".png";
	// 	var $nhsrc="images/arrows/next_hover_"+$lang+".png";
	// 	var $psrc="images/arrows/prev_"+$lang+".png";
	// 	var $phsrc="images/arrows/prev_hover_"+$lang+".png";
	//
	// 	$nxtEx.css({"background-image":"url("+$nsrc+")"});
	// 	$prevEx.css({"background-image":"url("+$psrc+")"});
	//
	// 	$nxtEx.hover(function() {
	// 	 	/*Stuff to do when the mouse enters the element */
	// 		$nxtEx.css('background-image', "url("+$nhsrc+")");
	// 	}, function() {
	// 	 	/*Stuff to do when the mouse leaves the element */
	// 	 	$nxtEx.css({"background-image":"url("+$nsrc+")"});
	// 	});
	//
	// 	$prevEx.hover(function() {
	// 	 	/*Stuff to do when the mouse enters the element */
	// 		$prevEx.css('background-image', "url("+$phsrc+")");
	// 	}, function() {
	// 	 	/*Stuff to do when the mouse leaves the element */
	// 	 	$prevEx.css({"background-image":"url("+$psrc+")"});
	// 	});
	// }



	// pull all data
	$("#int8Topic").text(data.string.pg8s1);
	$("#int8 > div > p").text(data.string.pg8s2);

	$("#ex1List > li:nth-of-type(1)").text(data.string.pg8s3);
	$("#ex1List > li:nth-of-type(2)").text(data.string.pg8s4);

	$("#ex2List > li:nth-of-type(1)").text(data.string.pg8s5);
	$("#ex2List > li:nth-of-type(2)").text(data.string.pg8s6);

	$("#ex3List > li:nth-of-type(1)").text(data.string.pg8s7);
	$("#ex3List > li:nth-of-type(2)").text(data.string.pg8s8);

	$("#ex4List > li:nth-of-type(1)").text(data.string.pg8s9);
	$("#ex4List > li:nth-of-type(2)").text(data.string.pg8s10);

	// on Clicking nxtEx button
	$("#activity-page-next-btn-enabled").on('click', function() {
		/*console.log("nextExample="+nextExample+"& prevExample="+prevExample);*/
		if(nextExample == 1){
			loadTimelineProgress(4,1);

			$("#mulEx2").hide(0);
			$("#mulEx3").hide(0);
			$("#mulEx4").hide(0);
			$("#mulEx1").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
				if(completed){
					nextExample=2;
					$("#activity-page-next-btn-enabled").show(0);
					$("#activity-page-prev-btn-enabled").hide(0);
				}
			});
		}

		else if(nextExample == 2){
			loadTimelineProgress(4,2);
			$("#mulEx1").hide(0);
			$("#mulEx3").hide(0);
			$("#mulEx4").hide(0);
			$("#mulEx2").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
				if(completed){
					nextExample=3;
					prevExample=1;
					$("#activity-page-next-btn-enabled").show(0);
					$("#activity-page-prev-btn-enabled").show(0);
				}
			});
		}

		else if(nextExample == 3){
			loadTimelineProgress(4,3);
			$("#mulEx1").hide(0);
			$("#mulEx2").hide(0);
			$("#mulEx4").hide(0);
			$("#mulEx3").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
				if(completed){
					nextExample=4;
					prevExample=2;
					$("#activity-page-next-btn-enabled").show(0);
					$("#activity-page-prev-btn-enabled").show(0);
				}
			});
		}

		else if(nextExample == 4){
			loadTimelineProgress(4,4);
			$("#mulEx1").hide(0);
			$("#mulEx2").hide(0);
			$("#mulEx3").hide(0);
			$("#mulEx4").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
				if(completed){
					prevExample=3;
					$("#activity-page-prev-btn-enabled").show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				}
			});
		}
	});

	// on Clicking prevEx button
	$("#activity-page-prev-btn-enabled").on('click', function() {
		/*console.log("nextExample="+nextExample+"& prevExample="+prevExample);*/
		if(prevExample == 1){
			loadTimelineProgress(4,1);
			$("#mulEx2").hide(0);
			$("#mulEx3").hide(0);
			$("#mulEx4").hide(0);
			$("#mulEx1").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
				if(completed){
					nextExample=2;
					$("#activity-page-next-btn-enabled").show(0);
					$("#activity-page-prev-btn-enabled").hide(0);
				}
			});
		}

		else if(prevExample == 2){
			loadTimelineProgress(4,2);
			$("#mulEx1").hide(0);
			$("#mulEx3").hide(0);
			$("#mulEx4").hide(0);
			$("#mulEx2").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
				if(completed){
					nextExample=3;
					prevExample=1;
					$("#activity-page-next-btn-enabled").show(0);
					$("#activity-page-prev-btn-enabled").show(0);
				}
			});
		}

		else if(prevExample == 3){
			loadTimelineProgress(4,3);
			ole.footerNotificationHandler.hideNotification();
			$("#mulEx1").hide(0);
			$("#mulEx2").hide(0);
			$("#mulEx4").hide(0);
			$("#mulEx3").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
				if(completed){
					nextExample=4;
					prevExample=2;
					$("#activity-page-next-btn-enabled").show(0);
					$("#activity-page-prev-btn-enabled").show(0);
				}
			});
		}

		else if(prevExample == 4){
			loadTimelineProgress(4,4);
			$("#mulEx1").hide(0);
			$("#mulEx2").hide(0);
			$("#mulEx3").hide(0);
			$("#mulEx4").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
				if(completed){
					prevExample=3;
					$("#activity-page-next-btn-enabled").hide(0);
					$("#activity-page-prev-btn-enabled").show(0);
				}
			});
		}
	});

	// on clicking first example nextstep button
	$("#ex1nxt").on('click',function() {
		if(nextStep == 2){
			$("#ex1nxt").hide(0);
			$("#ex1Rule").removeClass("animated infinite pulse");
			// show second step
			$("#ex1List > li:nth-of-type(2)").show(500,function(){
				$("#ex1nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 3){
			$("#ex1nxt").hide(0);
			// show third step
			$("#ex1St").show(500,function(){
				$("#ex1numLine").show(0, function() {
					// at last step of each example make the nextStep equal to 2
					nextStep=2;
					// show the nextEx button and increase nextExample count to 2
					nextExample = 2;
					$("#activity-page-next-btn-enabled").show(0);
				});

			});
		}
	});

	// on clicking first example nextstep button
	$("#ex2nxt").on('click',function() {
		if(nextStep == 2){
			$("#ex2nxt").hide(0);
			$("#ex2Rule").removeClass("animated infinite pulse");
			// show second step
			$("#ex2List > li:nth-of-type(2)").show(500,function(){
				$("#ex2nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 3){
			$("#ex2nxt").hide(0);
			// show third step
			$("#ex2St").show(500,function(){
				$("#ex2numLine").show(0, function() {
					// at last step of each example make the nextStep equal to 2
					nextStep=2;
					// show the nextEx button and increase nextExample count to 2
					nextExample = 3;
					$("#activity-page-next-btn-enabled").show(0);
				});

			});
		}
	});

	// on clicking first example nextstep button
	$("#ex3nxt").on('click',function() {
		if(nextStep == 2){
			$("#ex3nxt").hide(0);
			$("#ex3Rule").removeClass("animated infinite pulse");
			// show second step
			$("#ex3List > li:nth-of-type(2)").show(500,function(){
				$("#ex3nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 3){
			$("#ex3nxt").hide(0);
			// show third step
			$("#ex3St").show(500,function(){
				$("#ex3numLine").show(0, function() {
					// at last step of each example make the nextStep equal to 2
					nextStep=2;
					// show the nextEx button and increase nextExample count to 2
					nextExample = 4;
					$("#activity-page-next-btn-enabled").show(0);
				});

			});
		}
	});

	// on clicking first example nextstep button
	$("#ex4nxt").on('click',function() {
		if(nextStep == 2){
			$("#ex4nxt").hide(0);
			$("#ex4Rule").removeClass("animated infinite pulse");
			// show second step
			$("#ex4List > li:nth-of-type(2)").show(500,function(){
				$("#ex4nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 3){
			$("#ex4nxt").hide(0);
			// show third step
			$("#ex4St").show(500,function(){
				$("#ex4numLine").show(0, function() {
					// at last step of each example make the nextStep equal to 2
					nextStep=2;
					nextExample=4;
					prevExample=3;
					// this is last example so no need to show nextExample button
					// show the previous button
					$("#activity-page-prev-btn-enabled").show(0);
					$("#activity-page-next-btn-enabled").removeClass("animated infinite shake");
					completed=true;
					$("#ex1nxt, #ex2nxt, #ex3nxt, #ex4nxt").off("click");
					$("#ex1nxt, #ex2nxt, #ex3nxt, #ex4nxt").css({"display":"none"});
					ole.footerNotificationHandler.pageEndSetNotification();
				});

			});
		}
	});
// end of document ready
});

// next count for steps in example
var nextStep = 2;

// next count for example
var nextExample = 2;

$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(3,1);

	/*the next example nxtEx button*/
	var $nxtEx = $("#activity-page-next-btn-enabled");

	// if($lang === "en"){
	// 	var $nsrc="images/arrows/next_"+$lang+".png";
	// 	var $hsrc="images/arrows/next_hover_"+$lang+".png";
	// 	$nxtEx.css({"background-image":"url("+$nsrc+")"});
	// 	$nxtEx.hover(function() {
	// 	 	/*Stuff to do when the mouse enters the element */
	// 		$nxtEx.css('background-image', "url("+$hsrc+")");
	// 	}, function() {
	// 	 	/*Stuff to do when the mouse leaves the element */
	// 	 	$nxtEx.css({"background-image":"url("+$nsrc+")"});
	// 	});
	// }

	// pull all data
	$("#int7Topic").text(data.string.pg7s1);
	$("#int7 > div > ol > li:nth-of-type(1)").text(data.string.pg7s2);

	$("#subEx1 > ol > li:nth-of-type(2)").text(data.string.pg7s3);
	$("#subEx1 > ol > li:nth-of-type(3)").text(data.string.pg7s4);
	$("#subEx1 > ol > li:nth-of-type(4)").text(data.string.pg7s5);
	$("#subEx1 > ol > li:nth-of-type(5)").text(data.string.pg7s6);

	$("#subEx2 > ol > li:nth-of-type(2)").text(data.string.pg7s7);
	$("#subEx2 > ol > li:nth-of-type(3)").text(data.string.pg7s8);
	$("#subEx2 > ol > li:nth-of-type(4)").text(data.string.pg7s9);
	$("#subEx2 > ol > li:nth-of-type(5)").text(data.string.pg7s10);

	$("#subEx3 > ol > li:nth-of-type(2)").text(data.string.pg7s11);
	$("#subEx3 > ol > li:nth-of-type(3)").text(data.string.pg7s12);
	$("#subEx3 > ol > li:nth-of-type(4)").text(data.string.pg7s13);
	$("#subEx3 > ol > li:nth-of-type(5)").text(data.string.pg7s14);

	// on Clicking nxtEx button
	$("#activity-page-next-btn-enabled").on('click', function() {
		if(nextExample == 1){
			loadTimelineProgress(3,1);
			$("#subEx2").hide(0);
			$("#subEx3").hide(0);
			$("#subEx1").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
			});
		}

		else if(nextExample == 2){
			loadTimelineProgress(3,2);
			$("#subEx1").hide(0);
			$("#subEx3").hide(0);
			$("#subEx2").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
			});
		}

		else if(nextExample == 3){
			loadTimelineProgress(3,3);
			$("#subEx1").hide(0);
			$("#subEx2").hide(0);
			$("#subEx3").show(0,function(){
				$("#activity-page-next-btn-enabled").hide(0);
			});
		}
	});

	// on clicking first example nextstep button
	$("#ex1nxt").on('click',function() {

		var showStep = $("#subEx1 > ol").children("li:nth-of-type("+nextStep+")");

		if(nextStep == 2){
			$("#ex1nxt").hide(0);
			// show second step
			showStep.show(500);
			$("#ex1locate").velocity({"left":"75%"},2000,"linear",function(){
				$("#ex1nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 3){
			$("#ex1nxt").hide(0);
			$("#ex1locate").removeClass("animated infinite flash");
			// show third step
			showStep.show(500,function(){
				$("#subEx1 > em > span:nth-of-type(n+2)").addClass("animated flash");
				$("#subEx1 > em > span:nth-of-type(2)").html("-");
				$("#subEx1 > em > span:nth-of-type(3)").html("2");
				$("#ex1rule").show(0);
				$("#ex1nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 4){
			$("#ex1nxt").hide(0);
			// show third step
			showStep.show(500,function(){
				$("#subEx1 > em > span:nth-of-type(n+2)").removeClass("animated flash");
				$("#ex1imageHolder > img").attr('src', $ref+"/page7/image/ex1.png");
				$("#ex1rule").removeClass('animated infinite pulse');
				$("#ex1locate").velocity({"left":"64%"},700,"linear",function(){
					$("#ex1nxt").show(0);
					nextStep++;
				});
			});
		}

		else if(nextStep == 5){
			$("#ex1nxt").hide(0);
			// show third step
			showStep.show(500,function(){
				// at last step of each example make the nextStep equal to 2
					nextStep=2;
				// show the nextEx button and increase nextExample count to 2
					nextExample = 2;
					$("#activity-page-next-btn-enabled").show(0);
				});
		}
	});

// on clicking second example nextstep button
	$("#ex2nxt").on('click',function() {

		var showStep = $("#subEx2 > ol").children("li:nth-of-type("+nextStep+")");

		if(nextStep == 2){
			$("#ex2nxt").hide(0);
			// show second step
			showStep.show(500);
			$("#ex2locate").velocity({"left":"41%"},500,"linear",function(){
				$("#ex2nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 3){
			$("#ex2nxt").hide(0);

			$("#ex2locate").removeClass("animated infinite flash");
			// show third step
			showStep.show(500,function(){
				$("#subEx2 > em > span:nth-of-type(n+2)").addClass("animated flash");
				$("#subEx2 > em > span:nth-of-type(3)").html("2");
				$("#ex2rule").show(0);
				$("#ex2nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 4){
			$("#ex2nxt").hide(0);
			// show third step
			showStep.show(500,function(){
				$("#subEx2 > em > span:nth-of-type(n+2)").removeClass("animated flash");
				$("#ex2rule").removeClass('animated infinite pulse');
				$("#ex2imageHolder > img").attr('src', $ref+"/page7/image/ex2.png");
				$("#ex2locate").velocity({"left":"30%"},700,"linear",function(){
					$("#ex2nxt").show(0);
					nextStep++;
				});
			});
		}

		else if(nextStep == 5){
			$("#ex2nxt").hide(0);
			// show third step
			showStep.show(500,function(){
				// at last step of each example make the nextStep equal to 2
					nextStep=2;
				// show the nextEx button and increase nextExample count to 2
					nextExample = 3;
					$("#activity-page-next-btn-enabled").show(0);
				});
		}
	});

// on clicking third example nextstep button
	$("#ex3nxt").on('click',function() {
		var showStep = $("#subEx3 > ol").children("li:nth-of-type("+nextStep+")");

		if(nextStep == 2){
			$("#ex3nxt").hide(0);
			// show second step
			showStep.show(500);
			$("#ex3locate").velocity({"left":"41%"},500,"linear",function(){
				$("#ex3nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 3){
			$("#ex3nxt").hide(0);

			$("#ex3locate").removeClass("animated infinite flash");
			// show third step
			showStep.show(500,function(){
				$("#subEx3 > em > span:nth-of-type(n+2)").addClass("animated flash");
				$("#subEx3 > em > span:nth-of-type(2)").html("+");
				$("#subEx3 > em > span:nth-of-type(3)").html("2");
				$("#ex3rule").show(0);
				$("#ex3nxt").show(0);
				nextStep++;
			});
		}

		else if(nextStep == 4){
			$("#ex3nxt").hide(0);
			// show third step
			showStep.show(500,function(){
				$("#subEx3 > em > span:nth-of-type(n+2)").removeClass("animated flash");
				$("#ex3rule").removeClass('animated infinite pulse');
				$("#ex3imageHolder > img").attr('src',$ref+"/page7/image/ex3.png");
				$("#ex3locate").velocity({"left":"52%"},700,"linear",function(){
					$("#ex3nxt").show(0);
					nextStep++;
				});
			});
		}

		else if(nextStep == 5){
			$("#ex3nxt").hide(0);
			// show third step
			showStep.show(500,function(){
				// at last step of each example make the nextStep equal to 2
					nextStep=2;
				// show the nextEx button and increase nextExample count to 2
					nextExample = 1;
					ole.footerNotificationHandler.pageEndSetNotification();
				});
		}
	});

});

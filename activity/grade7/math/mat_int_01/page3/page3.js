// variable for nxt button
	var nxtCount=2;

$(document).ready(function() {	

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);
	
	// pull all data
	$("#int3topic").text(data.string.pg3s1);
	$("#labelNl > strong:nth-of-type(1)").text(data.string.pg3s2);
	$("#labelNl > strong:nth-of-type(2)").text(data.string.pg3s3);
	$("#explain > li:nth-of-type(1)").text(data.string.pg3s4);
	$("#explain > li:nth-of-type(2)").text(data.string.pg3s5);
	$("#explain > li:nth-of-type(3)").text(data.string.pg3s6);
	$("#explain > li:nth-of-type(4)").text(data.string.pg3s7);
	
	// show next button after 3 seconds
	$("#activity-page-next-btn-enabled").delay(2000).fadeIn(500,function(){
		//$("#activity-page-next-btn-enabled").addClass('animated wobble infinite')
	});

	// on clicking nxt button
	$("#activity-page-next-btn-enabled").on('click',function() {

		var prevCount = nxtCount-1;
		var nLine = $("#line");
		var list = $("#explain");
		if(nxtCount > 4)
				{	
					$("#activity-page-next-btn-enabled").hide(0);
					$("#activity-page-next-btn-enabled").removeClass("animated wobble infinite");
					nLine.children("img:nth-of-type("+prevCount+")").fadeOut(1000);
					$("#labelNl").fadeIn(1000,function(){
						/*$(".footer-next").addClass('animated infinite wobble');*/
						ole.footerNotificationHandler.pageEndSetNotification();
					});
					$("#activity-page-next-btn-enabled").off("click");

				}

			else{
					$("#activity-page-next-btn-enabled").hide(0);
					//$("#activity-page-next-btn-enabled").removeClass("wobble");
					nLine.children("img:nth-of-type("+prevCount+")").fadeOut(1000);
					nLine.children("img:nth-of-type("+nxtCount+")").fadeIn(1000);
					list.children("li:nth-of-type("+nxtCount+")").fadeIn(1000,function(){
						// show next button after 2.5 seconds
						$("#activity-page-next-btn-enabled").delay(1000).fadeIn(500,function(){
							//$("#activity-page-next-btn-enabled").addClass('wobble');
							nxtCount++;
						});
					});
				}
	});

// end of document ready
});				
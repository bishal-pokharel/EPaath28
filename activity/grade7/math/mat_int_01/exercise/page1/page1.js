// these variables are changed to push to the ansCollect.secondQ
var second1=second2=second3=second4=second5=second6=second7=second8=second9=second10=data.string.pg1s1;
// these are contents for answers
var ansCollect=[
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	},
	{
		mainQ:0,
		secondQ:0,
		correctAns:0,
		yourAns:0
	}
	]

// function generates random number between max and min and returns an array as collection of those random numbers
//isZero is boolean to say if a zero is required
function myRandom(count,max,min,isZero) {
 var randomGen = [];

 if (typeof isZero === "undefined") {
 	// zero required
		isZero=true;
	}

	// if minimum of range not specified - take minumum zero
	if (typeof min === "undefined") {
			min = 0;
	 			// if max is a negative number and min is not defined
			if (max < 0){
				min=max;
				max=0;
			}
		}

	// if maximum is less then the minimum then swap the values
	if(max < min){
		var temp = max;
		max=min;
		min=temp;
	}

	(function randomGet () {
				if (randomGen.length<count) {
					var rnd = Math.floor(Math.random()*(max-min+1)+min);
					// if zero is not required
					if(!isZero){
						while(rnd == 0) {
				        	rnd = Math.floor(Math.random()*(max-min+1)+min);
				    	}
					}

				    var i = randomGen.length,checkIf=false;

				    while (i--) {
				        if (randomGen[i] == rnd) {
				            randomGet();
				            checkIf=true;
				            break;
				        	}
				    	}

				    if (checkIf != true) {
				    	randomGen.push(rnd);
				    	randomGet();
				    };

					}
				else {
					// this is to just break the condition
				}

		})();

	return randomGen;
}

// next question button counter
var nextCount = 0;
// start of document ready
$(document).ready(function() {
	loadTimelineProgress(10, nextCount + 1);

	// pull these data
	$("#summary tr:nth-of-type(1) th:nth-of-type(1)").text(data.string.e1s12);
	$("#summary tr:nth-of-type(1) th:nth-of-type(2)").text(data.string.e1s13);
	$("#summary tr:nth-of-type(1) th:nth-of-type(3)").text(data.string.e1s14);
	$("#hint").text(data.string.e1s15);
	// start creating question bank
	ansCollect[0].mainQ = data.string.e1s1;
	ansCollect[1].mainQ = data.string.e1s1;
	ansCollect[2].mainQ = data.string.e1s1;
	ansCollect[3].mainQ = data.string.e1s1;
	ansCollect[4].mainQ = data.string.e1s1;
	ansCollect[5].mainQ = data.string.e1s1;
	ansCollect[6].mainQ = data.string.e1s3;
	ansCollect[7].mainQ = data.string.e1s3;
	ansCollect[8].mainQ = data.string.e1s2;
	ansCollect[9].mainQ = data.string.e1s2;

	// for second questions which are varied and their correct answers

	// for first two second question
	var s12rnd = myRandom(2,5000,100);
	second1 = ole.textSR(data.string.e1s4,"500",s12rnd[0]);
	second2 = ole.textSR(data.string.e1s5,"200",s12rnd[1]);

	ansCollect[0].secondQ = second1;
	ansCollect[0].correctAns ="-"+s12rnd[0];
	ansCollect[1].secondQ = second2;
	ansCollect[1].correctAns =+s12rnd[1];

	// for 3rd and 4th second question
	var s34rnd = myRandom(2,30,1,false);
	second3 = ole.textSR(data.string.e1s6,"10",s34rnd[0]);
	second4 = ole.textSR(data.string.e1s7,"15",s34rnd[1]);

	ansCollect[2].secondQ = second3;
	ansCollect[2].correctAns = "-"+s34rnd[0];
	ansCollect[3].secondQ = second4;
	ansCollect[3].correctAns =+s34rnd[1];

	// for 5rd and 6th second question
	var s56rnd = myRandom(2,7,1,false);
	second5 = ole.textSR(data.string.e1s8,"5",s56rnd[0]);
	second6 = ole.textSR(data.string.e1s9,"2",s56rnd[1]);

	ansCollect[4].secondQ = second5;
	ansCollect[4].correctAns = +s56rnd[0];
	ansCollect[5].secondQ = second6;
	ansCollect[5].correctAns = "-"+s56rnd[1];

	// for 7th second question
	var s7rnd = myRandom(1,5000,1,false);
	second7 = "-"+s7rnd[0];
	ansCollect[6].secondQ = second7;
	ansCollect[6].correctAns =+s7rnd[0];

	// for 8th second question
	var s8rnd = myRandom(1,5000,1,false);
	second8 = "+"+s8rnd[0];
	ansCollect[7].secondQ = second8;
	ansCollect[7].correctAns = "-"+s8rnd[0];

	// for 9th second question
	var s9rnd = myRandom(1,-100,-3000,false);
	second9 = s9rnd[0];
	ansCollect[8].secondQ = second9;
	ansCollect[8].correctAns = data.string.e1s11;

	// for 10th second question
	var s10rnd = myRandom(1,3000,100,false);
	second10 = "+"+s10rnd[0];
	ansCollect[9].secondQ = second10;
	ansCollect[9].correctAns = data.string.e1s10;

	// generate random sequence from 0 to 7 whereas 8 and 9 should always come at last
	var mySeq1 = myRandom(8,7,0);
	var mySeq2 = myRandom(2,9,8);
	var mySeq =  mySeq1.concat(mySeq2);

	$("#mainQn").text(ansCollect[mySeq[0]].mainQ);
	$("#secondQn").text(ansCollect[mySeq[0]].secondQ);

	var countQuestion = 0, countCorrect = "0",countDisplayWrong = 0;
	var board = $('#board');
	var nextBtn = $('#activity-page-next-btn-enabled');


	// which template to select
	function whichTemplate(num){
		switch(num){
			case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:
			// console.log("input Type");
				inputType();
			break;

			case 8:case 9:
			// console.log("option Type");
			    optionType();
			break;

			default:break;
		}
	}

	//input type template
	function inputType() {
		var source = $('#input-template').html();
		var template = Handlebars.compile(source);
		var content = {};
		var html = template(content);
		board.html(html);
		$(".get").focus();
		$("#hint").show(0).addClass('animated flash');
	}

	//selection type template
	function optionType() {
		var source = $('#option-template').html();
		var template = Handlebars.compile(source);
		var content = {
			ptive:data.string.e1s10,
			ntive:data.string.e1s11
		};
		var html = template(content);
		board.html(html);
		$("#hint").css({"display":"none"});
		$("#hint").removeClass('animated flash');
	}

	// get user answers and update the respective to yourAnswer in answer collect
	function userAnswer(num){
		var userAns;
		switch(num){
			case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:
			// get input answer
			userAns= $("#board").find(".get").val();
			ansCollect[num].yourAns = userAns;
			break;

			case 8:case 9:
			// get option answer
			userAns= $("#board > p > input[name=integer]:checked").val();
			if(userAns == "p"){
				userAns = data.string.e1s10;
			}
			else if(userAns == "n"){
				userAns = data.string.e1s11;
			}
			ansCollect[num].yourAns = userAns;
			break;
			default:break;
		}
		// console.log(userAns);
	}

	// function to highlight question
	function highlightQuestion(num){
		switch(num){
			case 0:case 2:case 4:case 6:case 8:
			$("#mainQn").css({"background-color":"#005751","color":"white"});
			$("#secondQn").css({"background-color":"#492D61","color":"white"});
			break;

			case 1:case 3:case 5:case 7:case 9:
			$("#mainQn").css({"background-color":"#53C3D0","color":"black"});
			$("#secondQn").css({"background-color":"#FBD22A","color":"black"});
			break;

			default:break;
		}
	}

	// function for listing summary table with animation
	function finishAnim () {
		$("#summary").show(0);
		var animEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		$("#summary tr:nth-of-type(1)").addClass('animated zoomInDown').one(animEnd,function(){
			$("#summary tr:nth-of-type(2)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
				$("#summary tr:nth-of-type(3)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
					$("#summary tr:nth-of-type(4)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
						$("#summary tr:nth-of-type(5)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
							$("#summary tr:nth-of-type(6)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
								$("#summary tr:nth-of-type(7)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
									$("#summary tr:nth-of-type(8)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
										$("#summary tr:nth-of-type(9)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
											$("#summary tr:nth-of-type(10)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
												$("#summary tr:nth-of-type(11)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
													ole.footerNotificationHandler.pageEndSetNotification();
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	}


	// call the template for first random answer
	whichTemplate(mySeq[0]);

	// what to do on clicking next button
	nextBtn.on('click',function (){
			// get the user answers before increasing next counter
			userAnswer(mySeq[nextCount]);
			// console.log(ansCollect[mySeq[nextCount]].mainQ+"\n"+ansCollect[mySeq[nextCount]].secondQ+"\n"+ansCollect[mySeq[nextCount]].correctAns+"\n"+ansCollect[mySeq[nextCount]].yourAns+"\n"+"*************************************************************");
			nextBtn.css({"display":"none"});
			// increase count until counter reaches 8 then
			if(nextCount < 9){
				nextCount++;
				loadTimelineProgress(10, nextCount + 1);
				// change the background color for questions to make sure they are visible
				highlightQuestion(nextCount);
				$("#mainQn").text(ansCollect[mySeq[nextCount]].mainQ);
				$("#secondQn").text(ansCollect[mySeq[nextCount]].secondQ);
				whichTemplate(mySeq[nextCount]);
			}

			else if(nextCount == 9){
				/*for(var i=0; i<10 ; i++)
				{
					console.log(ansCollect[i].mainQ+"\n"+ansCollect[i].secondQ+"\n"+ansCollect[i].correctAns+"\n"+ansCollect[i].yourAns+"\n"+"*************************************************************");
				}*/

				userAnswer(mySeq[9]);
				console.log(mySeq[9]);

				for(var i=0; i<10 ; i++){
					var j=i+2;
					// for main question
					switch(mySeq[i]){
						case 0:case 1:case 2:case 3:case 4:case 5:
							$("#summary tr:nth-of-type("+j+") td:nth-of-type(1)").text(ansCollect[mySeq[i]].secondQ);

							break;
						case 6:case 7:case 8:case 9:
							$("#summary tr:nth-of-type("+j+") td:nth-of-type(1)").text(ansCollect[mySeq[i]].mainQ+": "+ansCollect[mySeq[i]].secondQ);
							break;
						default:break;
					}

					// for correct answers
					switch(mySeq[i]){
						case 1:case 3:case 4:case 6:
							$("#summary tr:nth-of-type("+j+") td:nth-of-type(2)").text(ansCollect[mySeq[i]].correctAns+"/ +"+ansCollect[mySeq[i]].correctAns);
							break;

						case 0:case 2:case 5:case 7:case 8:case 9:
							$("#summary tr:nth-of-type("+j+") td:nth-of-type(2)").text(ansCollect[mySeq[i]].correctAns);
							break;
						default:break;
					}

					// for user answers
					$("#summary tr:nth-of-type("+j+") td:nth-of-type(3)").text(ansCollect[mySeq[i]].yourAns);

					// highlight correct and wrong user answers
					if((ansCollect[mySeq[i]].yourAns == ansCollect[mySeq[i]].correctAns) || (ansCollect[mySeq[i]].yourAns == "+"+ansCollect[mySeq[i]].correctAns)){
								$("#summary tr:nth-of-type("+j+") td:nth-of-type(3)").css({"color":"darkgreen"});
							}
							else{
								$("#summary tr:nth-of-type("+j+") td:nth-of-type(3)").css({"color":"#D1026C"});
							}


				}
				nextBtn.off("click");
				$("#board, #mainQn, #secondQn").css({"display":"none"});
				finishAnim();
			}
	});

	// functionality for the input to select if even the label beside is clicked
	// used this way of selecting the input because of handlebar
	$("#board").on("keyup",".get",function() {
		var ans = $(this).val();
		// if other than number and plus or minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^-+\d]/g,''));

		// this pattern is to match if integers are entered
		var ansPat = /[-+]?\d+/;
		// if integer are input show next button else hide it
		if(ansPat.test(ans)){
			nextBtn.show(0);
		}

		else if(!ansPat.test(ans)){
			nextBtn.css({"display":"none"});
		}
	});

	// functionality for the radio buttons to select if even the label beside is clicked
	$("#board").on('click', "p",function() {
		var selected = $(this).attr("class");
		switch(selected){
			case "positive": $(".positive > input").prop("checked",true);break;
			case "negative": $(".negative > input").prop("checked",true);break;
			default:break;
		}
		nextBtn.show(0);
	});

// end of document ready
})

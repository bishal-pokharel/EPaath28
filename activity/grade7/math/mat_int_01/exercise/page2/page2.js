// function to check the answer
function checkAns(inputAns, correctAns, orAns,thisInput){
	if(orAns == "0"){
		if(inputAns != correctAns){
			thisInput.css({"color":"#F02639","border-color":"#F02639"});
		}

		else{
			thisInput.css({"color":"#00B400","border-color":"#00B400"});
		}
	}

	else if(orAns != "0"){
		if((inputAns != correctAns) && (inputAns != orAns)){
			thisInput.css({"color":"#F02639","border-color":"#F02639"});
		}

		else{
			thisInput.css({"color":"#00B400","border-color":"#00B400"});
		}
	}

}

// these are contents for answers
var ansCollect=[
	{
		mainImg:$ref+"/exercise/page2/image/1.png",
		correctImg:$ref+"/exercise/page2/image/c1.png",
		leftInp1:{
			   		ans: "2",
			   		// or answer is for positive numbers only
			   		orAns:"+2",
			   		pos: "17%"

			   	},
		leftInp2:{
			   		ans: "5",
			   		orAns:"+5",
			   		pos: "39.5%"

			  	},
		leftInp3:{
			   		ans: "8",
			   		orAns:"+8",
			   		pos: "62%"
			   	},
		leftInp4:{
			   		ans: "9",
			   		orAns:"+9",
			   		pos: "69.5%"
			   	},
		leftInp5:{
					ans: "10",
					orAns:"+10",
					pos: "76.5%"
				}
	},
	{
		mainImg:$ref+"/exercise/page2/image/2.png",
		correctImg:$ref+"/exercise/page2/image/c2.png",
		leftInp1:{
			   		ans: "-12",
			   		// or ans is kept 0 for negative answers since there must be negative
			   		orAns:"0",
			   		pos: "2.5%"
			   	},
		leftInp2:{
			   		ans: "-11",
			   		orAns:"0",
			   		pos: "10%"
			  	},
		leftInp3:{
			   		ans: "-7",
			   		orAns:"0",
			   		pos: "39.5%"
			   	},
		leftInp4:{
			   		ans: "-4",
			   		orAns:"0",
			   		pos: "62%"
			   	},
		leftInp5:{
					ans: "-3",
					orAns:"0",
					pos: "70%"
				}
	},
	{
		mainImg:$ref+"/exercise/page2/image/3.png",
		correctImg:$ref+"/exercise/page2/image/c3.png",
		leftInp1:{
			   		ans: "-9",
			   		orAns:"0",
			   		pos: "6%"
			   	},
		leftInp2:{
			   		ans: "-7",
			   		orAns:"0",
			   		pos: "14.5%"
			  	},
		leftInp3:{
			   		ans: "-3",
			   		orAns:"0",
			   		pos: "33%"
			   	},
		leftInp4:{
			   		ans: "6",
			   		orAns:"+6",
			   		pos: "74%"
			   	},
		leftInp5:{
					ans: "9",
					orAns:"+9",
					pos: "87.5%"
				}
	}

	]

$(document).ready(function() {
	loadTimelineProgress(1, 1);
	// pull all data
	$("#q").text(data.string.e2s1);
	$("#qNumberLine > p:nth-of-type(1)").text(data.string.e2s2);
	$("#aNumberLine > p:nth-of-type(1)").text(data.string.e2s3);
	// $("#int2ex > em:nth-of-type(1)").text(data.string.e2s4);

	// get a random value between 0 to 2 we require to give 3 below
	var randomEx = Math.floor((Math.random() * 3) + 0);
	// this variable as alias for the random example number
	var myEx = ansCollect[randomEx];

	var myImage = myEx.mainImg;
	var myCorrectImage = myEx.correctImg;
	var l1= myEx.leftInp1.pos;
	var l2= myEx.leftInp2.pos;
	var l3= myEx.leftInp3.pos;
	var l4= myEx.leftInp4.pos;
	var l5= myEx.leftInp5.pos;

	// all the answers
	var l1ans= myEx.leftInp1.ans;
	var l2ans= myEx.leftInp2.ans;
	var l3ans= myEx.leftInp3.ans;
	var l4ans= myEx.leftInp4.ans;
	var l5ans= myEx.leftInp5.ans;

	// all the or answers
	var orL1ans= myEx.leftInp1.orAns;
	var orL2ans= myEx.leftInp2.orAns;
	var orL3ans= myEx.leftInp3.orAns;
	var orL4ans= myEx.leftInp4.orAns;
	var orL5ans= myEx.leftInp5.orAns;

	//change the image randomly and set the position of all the inputs according to the image choosen randomly
	$("#qLine").attr("src",myImage);
	$("#aLine").attr("src",myCorrectImage);
	$("#left1").css({"left":l1});
	$("#left2").css({"left":l2});
	$("#left3").css({"left":l3});
	$("#left4").css({"left":l4});
	$("#left5").css({"left":l5});

	// on each input
	$("#qNumberLine > input").keyup(function() {
		// if other than number and plus or minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^-+\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		var which = $(this).attr("id");

		// go for check answer only when all inputs have at least one value
		var leftVal1 = $("#left1").val();
		var leftVal2 = $("#left2").val();
		var leftVal3 = $("#left3").val();
		var leftVal4 = $("#left4").val();
		var leftVal5 = $("#left5").val();
		var allFilled = (leftVal1.length > 0 && !(isNaN(leftVal1))) && (leftVal2.length > 0 && !(isNaN(leftVal2))) && (leftVal3.length > 0 && !(isNaN(leftVal3))) && (leftVal4.length > 0 && !(isNaN(leftVal4))) && (leftVal5.length > 0 && !(isNaN(leftVal5)));
		console.log(allFilled);
		if(allFilled){
			$("#activity-page-next-btn-enabled").show(0);
		}

		else{
			$("#activity-page-next-btn-enabled").css({"display":"none"});
		}

	});

	var countOk = 0;
	// what to do on clicking ok
	$("#activity-page-next-btn-enabled").on('click', function() {
		$("#activity-page-next-btn-enabled").css({"display":"none"});
		$("#qNumberLine > input").prop('disabled', 'true');

		// go for check answer only when all inputs have at least one value
		var leftVal1 = $("#left1").val();
		var leftVal2 = $("#left2").val();
		var leftVal3 = $("#left3").val();
		var leftVal4 = $("#left4").val();
		var leftVal5 = $("#left5").val();

		// check ans
		checkAns(leftVal1, l1ans, orL1ans, $("#left1"));
		checkAns(leftVal2, l2ans, orL2ans, $("#left2"));
		checkAns(leftVal3, l3ans, orL3ans, $("#left3"));
		checkAns(leftVal4, l4ans, orL4ans, $("#left4"));
		checkAns(leftVal5, l5ans, orL5ans, $("#left5"));

		// position the question number line
		$("#qNumberLine").css({"top":"30%"});
		$("#qNumberLine > p:nth-of-type(1)").show(0);
		$("#aNumberLine").show(0);
		$("#replay").show(0);
		$("#activity-page-next-btn-enabled").show(0);

		if(countOk++ == 1){
			ole.activityComplete.finishingcall();
		}
	});

	$("#replay").on('click',function() {
		location.reload(true);
	});
});

$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(2,1);

	/*the next example nxtEx button*/
	var $nxtEx = $("#activity-page-next-btn-enabled");
	var $prevEx = $("#activity-page-prev-btn-enabled");

	// if($lang === "en"){
	// 	var $nsrc="images/arrows/next_"+$lang+".png";
	// 	var $nhsrc="images/arrows/next_hover_"+$lang+".png";
	// 	var $psrc="images/arrows/prev_"+$lang+".png";
	// 	var $phsrc="images/arrows/prev_hover_"+$lang+".png";
	//
	// 	$nxtEx.css({"background-image":"url("+$nsrc+")"});
	// 	$prevEx.css({"background-image":"url("+$psrc+")"});
	//
	// 	$nxtEx.hover(function() {
	// 	 	/*Stuff to do when the mouse enters the element */
	// 		$nxtEx.css('background-image', "url("+$nhsrc+")");
	// 	}, function() {
	// 	 	/*Stuff to do when the mouse leaves the element */
	// 	 	$nxtEx.css({"background-image":"url("+$nsrc+")"});
	// 	});
	//
	// 	$prevEx.hover(function() {
	// 	 	/*Stuff to do when the mouse enters the element */
	// 		$prevEx.css('background-image', "url("+$phsrc+")");
	// 	}, function() {
	// 	 	/*Stuff to do when the mouse leaves the element */
	// 	 	$prevEx.css({"background-image":"url("+$psrc+")"});
	// 	});
	// }

	// pull all data
	$("#int9Topic").text(data.string.pg9s1);
	$("#int9 > div > em > span:nth-of-type(6)").text(data.string.pg9s3);
	$("#int9 > div > ol > li:nth-of-type(2)").text(data.string.pg9s2);
	$("#ex1List > li:nth-of-type(1)").text(data.string.pg9s4);
	$("#ex2List > li:nth-of-type(1)").text(data.string.pg9s5);

	// on Clicking nxtEx button
	$("#activity-page-next-btn-enabled").on('click', function() {
		loadTimelineProgress(2,2);
			$("#activity-page-next-btn-enabled").hide(0);
			$("#divEx1").hide(0);
			$("#divEx2").show(0);
	});

	// on Clicking prevEx button
	$("#activity-page-prev-btn-enabled").on('click', function() {
		loadTimelineProgress(2,1);
		location.reload();
	});

	// on clicking first example nextstep button
	$("#ex1nxt").on('click',function() {
			$("#ex1nxt").hide(0);
			// show second step
			$("#ex1List > li:nth-of-type(2)").show(500,function(){
				$("#ex1numLine").show(0, function(){
					$("#activity-page-next-btn-enabled").show(0);
				});
			});
	});

	// on clicking first example nextstep button
	$("#ex2nxt").on('click',function() {
		$("#ex2nxt").hide(0);
			// show second step
			$("#ex2List > li:nth-of-type(2)").show(500,function(){
				$("#ex2numLine").show(0, function(){
					$("#activity-page-prev-btn-enabled").show(0);
					ole.footerNotificationHandler.lessonEndSetNotification();
				});
			});
	});


// end of document ready
});

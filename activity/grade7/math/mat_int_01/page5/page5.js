// to execute a function only once
var firstTime= true;

$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);

	// pull all data
	$("#int5Topic").text(data.string.pg5s1);
	$("#detail > ul > em").text(data.string.pg5s5);
	$("#detail > ul > li:nth-of-type(1)").text(data.string.pg5s3);
	$("#detail > ul > li:nth-of-type(2)").text(data.string.pg5s4);
	$("#degreeImage > figCaption").text(data.string.pg5s6);

	$("#detail > ul").addClass("animated infinite bounce");
	// what to do on sliding the image- slideme
	$("#slideMe").draggable({containment: "parent", axis:"y",drag:function(){
					// if this is the first drag change the text
					    if(firstTime){
					    	$("#detail > ul").removeClass("animated infinite bounce");
					    	$("#detail > ul > em").text(data.string.pg5s2).css({"text-decoration":"underline"});
					    	$("#detail > ul > li").show(500);
					    	ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg5n1);
					    	firstTime = false;
						}

					$("#slideMe").removeClass('flash');
					var pos = $(this).position();
					var depth = $("#dragArea").innerHeight();
					var posT = (pos.top);
					// calculation below is very important 10000/96 makes the div height calculate from 0 to 100
					// also solves the error of the image height which does not give the total 0 tp 100 range at bottomMost
					// i u did not understand this comment i dont care- genius do it on your own or call meh!
					var posTP = Math.floor((posT/depth)*(10000/95.5));
					// makes the range from 50 tp -50
					var temp=50-posTP;

					/*these lines below make the range go from 100 to -20
					just for sake of example
					var posTP = Math.floor((posT/depth)*(13000/95.5));
					var temp=100-posTP;	*/


					$("#degree").html(temp+"&#176;C");
					var mercuryHt;
					// this condition for mercury Ht because for range for less then 50 it
					// shows problem for height
					if(posTP<50){
						mercuryHt = 98-posTP;
					}
					else{
						mercuryHt = 100-posTP;
					}

					$("#mercury").css({"height":mercuryHt+"%"});
					$("#slideNline").css({"right":posTP+"%"});

					// change the image and text for image on certain temperatures
					switch(temp){
						case -20:$("#degreeImage > img").attr("src",$ref+"/page5/image/-20.png");
								 $("#degreeImage > figCaption").text(data.string.pg5s7);
								 break;

						case 18:$("#degreeImage > img").attr("src",$ref+"/page5/image/18.png");
								 $("#degreeImage > figCaption").text(data.string.pg5s8);
								 break;

						case 37:$("#degreeImage > img").attr("src",$ref+"/page5/image/37.png");
								 $("#degreeImage > figCaption").text(data.string.pg5s9);
								 break;

						case 49:$("#degreeImage > img").attr("src",$ref+"/page5/image/49.png");
								 $("#degreeImage > figCaption").text(data.string.pg5s10);
								 ole.footerNotificationHandler.pageEndSetNotification();
								 break;
						case 0: $("#degreeImage > img").attr("src",$ref+"/page5/image/0.png");
								 $("#degreeImage > figCaption").text(data.string.pg5s6);
								 break;
						default:break;
					}
			}
	});

	$("#slideMe").hover(function() {
		/* Stuff to do when the mouse enters the element */
		$("#slideMe").removeClass('flash');
	}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#slideMe").addClass('flash');
	});
});

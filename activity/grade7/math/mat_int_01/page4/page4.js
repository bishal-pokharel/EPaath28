// these are contents for answers
var ansCollect=[
	{
		mainImg:$ref+"/page4/image/ex1.gif",
		answer1:{
			   		ans: "7",
			   		ansImg: $ref+"/page4/image/hint11.png",
			   		isPositive:true
			   	},
		answer2:{
			   		ans: "-6",
			   		ansImg: $ref+"/page4/image/hint12.png",
			   		isPositive:false
			  	},
		answer3:{
			   		ans: "7",
			   		ansImg: $ref+"/page4/image/hint13.png"
			   	},
		answer4:{
			   		ans: "6",
			   		ansImg: $ref+"/page4/image/hint14.png"
			   	}
		},
	{
		mainImg:$ref+"/page4/image/ex2.gif",
		answer1:{
			   		ans: "-7",
			   		ansImg: $ref+"/page4/image/hint21.png",
			   		isPositive:false
			   	},
		answer2:{
			   		ans: "-3",
			   		ansImg: $ref+"/page4/image/hint22.png",
			   		isPositive:false
			  	},
		answer3:{
			   		ans: "7",
			   		ansImg: $ref+"/page4/image/hint23.png"
			   	},
		answer4:{
			   		ans: "3",
			   		ansImg: $ref+"/page4/image/hint24.png"
			   	}
		},
	{
		mainImg:$ref+"/page4/image/ex3.gif",
		answer1:{
			   		ans: "5",
			   		ansImg: $ref+"/page4/image/hint31.png",
			   		isPositive:true
			   	},
		answer2:{
			   		ans: "2",
			   		ansImg: $ref+"/page4/image/hint32.png",
			   		isPositive:true
			  	},
		answer3:{
			   		ans: "5",
			   		ansImg: $ref+"/page4/image/hint33.png"
			   	},
		answer4:{
			   		ans: "2",
			   		ansImg: $ref+"/page4/image/hint34.png"
			   	}
		},
	{
		mainImg:$ref+"/page4/image/ex4.gif",
		answer1:{
			   		ans: "-1",
			   		ansImg: $ref+"/page4/image/hint41.png",
			   		isPositive:false
			   	},
		answer2:{
			   		ans: "3",
			   		ansImg: $ref+"/page4/image/hint42.png",
			   		isPositive:true
			  	},
		answer3:{
			   		ans: "1",
			   		ansImg: $ref+"/page4/image/hint43.png"
			   	},
		answer4:{
			   		ans: "3",
			   		ansImg: $ref+"/page4/image/hint44.png"
			   	}
		}
	]

// character count for answer 1
var charCountAns1=0;

// character count for answer 2
var charCountAns2=0;

// for vendor prefixes of animation end
var animEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);

	// pull all data
	$("#instr").text(data.string.pg4s1);
	$("#q1 > em:nth-of-type(1)").text(data.string.pg4s3);
	$("#q2 > em:nth-of-type(1)").text(data.string.pg4s4);
	$("#q3 > em:nth-of-type(1)").text(data.string.pg4s5);
	$("#q4 > em:nth-of-type(1)").text(data.string.pg4s6);
	$("#qBank > div > p:nth-of-type(1) > strong:nth-of-type(1)").text(data.string.pg4s7);
	$("#helpSt").text(data.string.pg4s8);
	$("#qBank > div > p > input").attr("title",data.string.pg4s12);
	// get a random value between 0 to 3 we require to give 4 below
	var randomEx = Math.floor((Math.random() * 4) + 0);
	// this variable as alias for the random example number
	var myEx = ansCollect[randomEx];

	// set the gif for the random value above
	$("#nLineAnim").attr("src",myEx.mainImg);

	// on clicking number line div
	$("#numLine").on('click',function() {
		$("#numLine").off("click");
		$("#numLine").css({"cursor":"default"});
		$("#nLine").hide(0);
		$("#nLineAnim").show(0);
		$("#instr").removeAttr("class");
		$("#instr").fadeOut(0);
		$("#instr").text(data.string.pg4s2);
		// gif is 5s long
		$("#instr").delay(5000).fadeIn(1000,function(){
			$("#q1").show(0,function(){
				$("#q1").addClass('animated bounceInDown');
				$("#a1 > input").focus();
				$("#helpSt").show(0);
				// change the main image with hintImage for question 1
				$("#nLineAnim").attr('src', myEx.answer1.ansImg);
			});
		});
	});

	// on input of first answer
	$("#a1 > input").keyup(function() {
		// if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^-\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		// alias for answer1 object members
		var correctAns = myEx.answer1.ans;
		var positive = myEx.answer1.isPositive;
		// increase character count
		charCountAns1++;
		// check for the input answer if ans is positive
			if(inp == correctAns){
			$("#a1 > input").css({"color":"darkgreen"});
			// change the answer sign to correct
			$("#a1 > span").removeClass("glyphicon-remove").addClass('glyphicon-ok').css({"color":"green"}).show(0);
			$("#a1 > input").prop('disabled', true);
			$("#q2").show(0,function(){
				$("#q2").addClass('animated bounceInDown').one(animEnd, function() {
					$("#a2 > input").focus();
					// change the help statement for second question
					$("#helpSt").text(data.string.pg4s9);
					$("#helpSt").css({"color":"#D03030","top":"47%"});
					// change the main image with hintImage for question 1
					$("#nLineAnim").attr('src', myEx.answer2.ansImg);
				});
			});
			}

		else{
			if(positive){
				$("#a1 > input").css({"color":"#D03030"});
				// change the answer sign to wrong
				$("#a1 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
			}

			else{
				if(charCountAns1 > 1){
				$("#a1 > input").css({"color":"#D03030"});
				// change the answer sign to wrong
				$("#a1 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
				}
			}
		}

	});

	// on input of second answer
	$("#a2 > input").keyup(function() {
		// if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^-\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		// alias for answer1 object members
		var correctAns = myEx.answer2.ans;
		var positive = myEx.answer2.isPositive;
		// increase character count
		charCountAns2++;
		// check for the input answer if ans is positive
		if(inp == correctAns){
			$("#a2 > input").css({"color":"darkgreen"});
			// change the answer sign to correct
			$("#a2 > span").removeClass("glyphicon-remove").addClass('glyphicon-ok').css({"color":"green"}).show(0);
			$("#a2 > input").prop('disabled', true);
			$("#q3").show(0,function(){
				$("#q3").addClass('animated bounceInDown').one(animEnd, function() {
					$("#a3 > input").focus();
				// change the help statement for second question
				$("#helpSt").text(data.string.pg4s10);
				$("#helpSt").css({"color":"darkblue","top":"64%"});
				// change the main image with hintImage for question 1
				$("#nLineAnim").attr('src', myEx.answer3.ansImg);
				});
			});
		}

		else{
			if(positive){
				$("#a2 > input").css({"color":"#D03030"});
				// change the answer sign to wrong
				$("#a2 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
			}

			else{
				if(charCountAns2 > 1){
				$("#a2 > input").css({"color":"#D03030"});
				// change the answer sign to wrong
				$("#a2 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
				}
			}
		}
	});

// on input of third answer
	$("#a3 > input").keyup(function() {
		// if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^-\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		// alias for answer1 object members
		var correctAns = myEx.answer3.ans;
		// check for the input answer
		if(inp == correctAns){
			$("#a3 > input").css({"color":"darkgreen"});
			// change the answer sign to correct
			$("#a3 > span").removeClass("glyphicon-remove").addClass('glyphicon-ok').css({"color":"green"}).show(0);
			$("#a3 > input").prop('disabled', true);
			$("#q4").show(0,function(){
				$("#q4").addClass('animated bounceInLeft').one(animEnd, function(){
					$("#a4 > input").focus();
				// change the help statement for second question
				$("#helpSt").text(data.string.pg4s11);
				$("#helpSt").css({"color":"#D03030","top":"80%"});
				// change the main image with hintImage for question 1
				$("#nLineAnim").attr('src', myEx.answer4.ansImg);
				});
			});
		}

		else{
			$("#a3 > input").css({"color":"#D03030"});
			// change the answer sign to wrong
			$("#a3 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
		}
	});

// on input of third answer
	$("#a4 > input").keyup(function() {
		// if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^-\d]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val();
		// console.log($.type(inp)+"->"+inp);
		// alias for answer1 object members
		var correctAns = myEx.answer4.ans;
		// check for the input answer
		if(inp == correctAns){
			$("#a4 > input").css({"color":"darkgreen"});
			// change the answer sign to correct
			$("#a4 > span").removeClass("glyphicon-remove").addClass('glyphicon-ok').css({"color":"green"}).show(0);
			$("#a4 > input").prop('disabled', true);
			$("#helpSt").hide(0);
			$("#rePlay").show(0);
			/*$(".footer-next").addClass('animated infinite shake');*/
			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.pg4n1);
			ole.footerNotificationHandler.pageEndSetNotification();
		}

		else{
			$("#a4 > input").css({"color":"#D03030"});
			// change the answer sign to wrong
			$("#a4 > span").removeClass("glyphicon-ok").addClass('glyphicon-remove').css({"color":"darkred"}).show(0);
		}
	});

// replay function
	$("#rePlay").on('click',function() {
		location.reload();
	});
});

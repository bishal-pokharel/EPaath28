var pathToImage = $ref + "/images/page3/";

var content = [
  {
    housescaleClass: "housescaleWrap",
    firstslideHeadingData: data.string.firstslideheading,
    housescaleImgSrc: pathToImage + "housescale.png",
    housescaleLabelData: data.string.housescalelabel,
    houseImgSrc: pathToImage + "house.png",
    houseLabelData: data.string.houselabel,
    explainData: data.string.scaletext
  },
  {
    housescaleClass: "housebuildWrap",
    firstslideHeadingData: data.string.firstslideheading,
    housescaleImgSrc: pathToImage + "housemap.png",
    housescaleLabelData: data.string.housemaplabel,
    houseImgSrc: pathToImage + "housebrown.png",
    houseLabelData: data.string.houselabel,
    explainData: data.string.whyscaledrawing1,
    showscaledrawbutton: true
  },
  {
    housescaleClass: "bridgebuildWrap",
    firstslideHeadingData: data.string.bridgeslideheading,
    housescaleImgSrc: pathToImage + "bridgemap.png",
    housescaleLabelData: data.string.bridgescalelabel,
    houseImgSrc: pathToImage + "bridge.png",
    houseLabelData: data.string.bridgelabel,
    explainData: data.string.bridgeexplain
  },
  {
    bridgetextHeadingData: data.string.bridgetextHeading,
    bridgetextfirstparaData: data.string.bridgetextfirstpara,
    bridgetextsecondparaData: data.string.bridgetextsecondpara,
    bridgetextthirdparaData: data.string.bridgetextthirdpara,
    bridgetextfifthparaData: data.string.bridgetextfifthpara
  },
  {
    diyHeadingData: data.string.diyHeading,
    diyfirstparaData: data.string.diyfirstpara,
    diythirdparaData: data.string.diythirdpara,
    diycongratulationData: data.string.diycongratulation,
    diyhintData: data.string.diyhint,
    diyhintfirstparaData: data.string.diyhintfirstpara,
    diyhintsecondparaData: data.string.diyhintsecondpara,
    diyhintthirdparaData: data.string.diyhintthirdpara,
    diyhintfourthparaData: data.string.diyhintfourthpara,
    diyhintfifthparaData: data.string.diyhintfifthpara
  },
  {
    makegroundmapHeadingData: data.string.makegroundmapHeading,
    makegroundmapQuestionData: data.string.makegroundmapQuestion,
    makegroundmapLengthInputData: data.string.makegroundmapLengthInput,
    makegroundmapWidthInputData: data.string.makegroundmapWidthInput,
    makegroundmapcongratulationData: data.string.diycongratulation,
    makegroundmapImgSrc: pathToImage + "footballground.png"
  },
  {
    schoolworkHeadingData: data.string.diyHeading,
    schoolworkfirstparaData: data.string.schoolworkfirstpara,
    hintData: data.string.diyhint,
    hintfirstparaData: data.string.hintfirstpara,
    hintsecondparaData: data.string.hintsecondpara,
    hintthirdparaData: data.string.hintthirdpara,
    hintfourthparaData: data.string.hintfourthpara,
    hintfifthparaData: data.string.hintfifthpara,
    hintsixthparaData: data.string.hintsixthpara,
    hintseventhparaData: data.string.hintseventhpara,
    eighthparaRightNumeratorData: data.string.eighthparaRightNumerator,
    eighthparaRightDinominatorData: data.string.eighthparaRightDinominator,
    eighthparaLeftExpressionData: data.string.eighthparaLeftExpression,
    ninthparaRightNumeratorData: data.string.ninthparaRightNumerator,
    hinttenthparaData: data.string.hinttenthpara
  }
];
(function($) {
  var $board = $(".board"),
    $nextBtn = $("#activity-page-next-btn-enabled"),
    $prevBtn = $(".prevButton"),
    countNext = 0,
    $total_page = 7;

  loadTimelineProgress($total_page, countNext + 1);

  /*
   * housescale
   */
  function housescale() {
    var source = $("#housescale-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    if ($board.children("div:nth-of-type(1)").hasClass("housescaleWrap")) {
      $nextBtn.show(0);
      countNext == 0
        ? $prevBtn.css("display", "none")
        : $prevBtn.css("display", "block");
    } else if (
      $board.children("div:nth-of-type(1)").hasClass("housebuildWrap")
    ) {
      $nextBtn.show();
      var sentenceCount = 0;
      var $housebuildWrap = $board.children("div.housebuildWrap");
      var $scaledrawBtn = $housebuildWrap.children("div.scaledrawBtn");
      var $changetextPara = $housebuildWrap.children("p:nth-of-type(2)");

      var makechanges = function() {
        (this.textchange = function() {
          $changetextPara.html(
            data.string["whyscaledrawing" + (sentenceCount + 1)]
          );
          if (sentenceCount == 1) {
            ole.parseToolTip(
              $changetextPara,
              data.string.whyscaledrawing2_highlight
            );
          }
        }),
          (this.showNextPageNotification = function() {
            $changetextPara.html(
              data.string["whyscaledrawing" + (sentenceCount + 1)]
            );
            $scaledrawBtn.css("display", "none");
            $nextBtn.show(0);
            $prevBtn.show(0);
          });
      };

      var makechange = new makechanges();

      $scaledrawBtn.on("click", function() {
        ++sentenceCount <= 1
          ? makechange.textchange()
          : makechange.showNextPageNotification();
      });
    } else if (
      $board.children("div:nth-of-type(1)").hasClass("bridgebuildWrap")
    ) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    }
  }

  /*
   * bridgetextonly
   */
  function bridgetextonly() {
    var source = $("#bridgetextonly-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "block");
    $prevBtn.css("display", "block");

    var $bridgetextWrap = $board.children("div.bridgetextWrap");
    var $scaledrawBtn = $bridgetextWrap.children("div.scaledrawBtn");
    var $paragraphContainer = $bridgetextWrap.children(
      "div.paragraphContainer"
    );
    var $paragraphContainerChildren;
    var sentenceCount = 0;

    var makechanges = function() {
      (this.textchange = function() {
        switch (sentenceCount) {
          case 1:
            $paragraphContainerChildren = $paragraphContainer.children(
              "p.bridgetextsecondpara"
            );
            break;
          case 2:
            $paragraphContainerChildren = $paragraphContainer.children(
              "p.bridgetextthirdpara"
            );
            break;
          case 3:
            $paragraphContainerChildren = $paragraphContainer.children(
              "div.bridgetextfourthpara"
            );
            break;
          default:
            break;
        }
        $paragraphContainerChildren.show(0);
      }),
        (this.showNextPageNotification = function() {
          $paragraphContainer.children("p.bridgetextfifthpara").show(0);
          $scaledrawBtn.css("display", "none");
          $nextBtn.show(0);
          $prevBtn.show(0);
        });
    };

    var makechange = new makechanges();
    $scaledrawBtn.on("click", function() {
      sentenceCount++ < 3
        ? makechange.textchange()
        : makechange.showNextPageNotification();
    });
  }

  /*
   * doityourself
   */
  function doityourself() {
    var source = $("#doityourself-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "none");
    $prevBtn.css("display", "none");

    var $diyWrap = $board.children("div.diyWrap");
    var $diyparagraphContainer = $diyWrap.children("div.diyparagraphContainer");
    var $diyInput = $diyparagraphContainer
      .children("p.diythirdpara")
      .children("input");
    var $diyhint = $diyparagraphContainer
      .children("p.diythirdpara")
      .children("em.showHint");
    var $answermark = $diyparagraphContainer
      .children("p.diythirdpara")
      .children("b.answermark");
    var $diycongratulationspara = $diyWrap.children("p.diycongratulationspara");
    var $hintparagraphContainer = $diyWrap.children(
      "div.hintparagraphContainer"
    );

    /*what does hint button do on hover*/
    $diyhint.hover(
      function() {
        /* Stuff to do when the mouse enters the element */
        $hintparagraphContainer.show(50);
      },
      function() {
        /* Stuff to do when the mouse leaves the element */
        $hintparagraphContainer.hide(50);
      }
    );

    /*on answer input*/
    $diyInput.keypress(function(event) {
      if (event.keyCode == 13) {
        if ($(this).val() == "60") {
          $(this).css("border", "2px solid rgba(0,255,0,0.8)");
          $(this).prop("disabled", true);
          $diycongratulationspara.show(0);
          $answermark.html("&#10003;");
          $answermark.css("color", "green").show(0);
          $nextBtn.show(0);
          $prevBtn.show(0);
        }

        if ($(this).val() != "60") {
          $(this).css("border", "2px solid rgba(255,0,0,0.8)");
          $answermark.html("&#10799;");
          $answermark.css("color", "darkred").show(0);
        }
        return;
      }
      // if other than number and minus sign remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
      $(this).val(
        $(this)
          .val()
          .replace(/[^-\d]/g, "")
      );
      // get the input value and change it to toLowerCase
      var inp = $(this).val();
    });
  }

  /*
   * makegroundmap
   */
  function makegroundmap() {
    var source = $("#makegroundmap-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "none");
    $prevBtn.css("display", "none");

    var $makegroundmapWrap = $board.children("div.makegroundmapWrap");
    var $makegroundmapinputContainer = $makegroundmapWrap.children(
      "div.makegroundmapinputContainer"
    );
    var $makegroundmapInputs = $makegroundmapinputContainer
      .children("p")
      .children("input");
    var $makegroundmapLengthInput = $makegroundmapinputContainer
      .children("p.makegroundmapLengthInput")
      .children("input");
    var $makegroundmapWidthInput = $makegroundmapinputContainer
      .children("p.makegroundmapWidthInput")
      .children("input");
    var $answermarklength = $makegroundmapinputContainer
      .children("p.makegroundmapLengthInput")
      .children("b.answermark");
    var $answermarkwidth = $makegroundmapinputContainer
      .children("p.makegroundmapWidthInput")
      .children("b.answermark");
    var $makegroundmapcongratulationspara = $makegroundmapWrap.children(
      "p.makegroundmapcongratulationspara"
    );
    var $scaledrawBtn = $makegroundmapWrap.children("div.scaledrawBtn");

    $scaledrawBtn.css("display", "none");
    $makegroundmapLengthInput.focus();

    /*on answer input*/
    $makegroundmapInputs.keyup(function() {
      /*if other than number and minus sign remove it => The g means Global,
			 	and causes the replace call to replace all matches, not just the first one*/
      $(this).val(
        $(this)
          .val()
          .replace(/[^-\d]/g, "")
      );

      if (
        $makegroundmapLengthInput.val().length > 0 &&
        $makegroundmapWidthInput.val().length > 0
      ) {
        $scaledrawBtn.show(0);
      } else {
        $scaledrawBtn.css("display", "none");
      }
    });

    var lengthanswercorrect = (widthanswercorrect = false);

    $scaledrawBtn.on("click", function() {
      if ($makegroundmapLengthInput.val() == "10") {
        $answermarklength.html("&#10003;");
        $answermarklength.css("color", "green").show(0);
        $makegroundmapLengthInput.prop("disabled", true);
        lengthanswercorrect = true;
        if (widthanswercorrect) {
          $scaledrawBtn.css("display", "none");
          $makegroundmapcongratulationspara.show(0);
          $nextBtn.show(0);
          $prevBtn.show(0);
        }
      } else {
        $answermarklength.html("&#10799;");
        $answermarklength.css("color", "darkred").show(0);
        $scaledrawBtn.css("display", "none");
      }

      if ($makegroundmapWidthInput.val() == "8") {
        $answermarkwidth.html("&#10003;");
        $answermarkwidth.css("color", "green").show(0);
        $makegroundmapWidthInput.prop("disabled", true);
        widthanswercorrect = true;
        if (lengthanswercorrect) {
          $scaledrawBtn.css("display", "none");
          $makegroundmapcongratulationspara.show(0);
          $nextBtn.show(0);
          $prevBtn.show(0);
        }
      } else {
        $answermarkwidth.html("&#10799;");
        $answermarkwidth.css("color", "darkred").show(0);
        $scaledrawBtn.css("display", "none");
      }
    });
  }

  /*
   * schoolwork
   */
  function schoolwork() {
    var source = $("#schoolwork-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "none");

    var $schoolworkWrap = $board.children("div.schoolworkWrap");
    var $visiblediv = $schoolworkWrap.children("div.visiblediv");
    var $showHint = $visiblediv.children("em.showHint");
    var $totalHint = $schoolworkWrap.children("div.totalHint");

    $showHint.on("click", function() {
      $visiblediv.css("margin-top", "2%");
      $totalHint.show(0);
    });
    $prevBtn.show(0);
    ole.footerNotificationHandler.lessonEndSetNotification();
  }

  housescale();

  $nextBtn.on("click", function() {
    countNext++;
    $(this).hide(0);
    switch (countNext) {
      case 1:
        housescale();
        break;
      case 2:
        housescale();
        break;
      case 3:
        bridgetextonly();
        break;
      case 4:
        doityourself();
        break;
      case 5:
        makegroundmap();
        break;
      case 6:
        schoolwork();
        break;
      default:
        break;
    }
    // console.log("countNext = "+countNext);
    loadTimelineProgress($total_page, countNext + 1);
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    countNext--;
    $(this).hide(0);
    switch (countNext) {
      case 0:
        housescale();
        break;
      case 1:
        housescale();
        break;
      case 2:
        housescale();
        break;
      case 3:
        bridgetextonly();
        break;
      case 4:
        doityourself();
        break;
      case 5:
        makegroundmap();
        ole.footerNotificationHandler.hideNotification();
        break;

      default:
        break;
    }
    // console.log("countNext = "+countNext);
    loadTimelineProgress($total_page, countNext + 1);
  });
})(jQuery);

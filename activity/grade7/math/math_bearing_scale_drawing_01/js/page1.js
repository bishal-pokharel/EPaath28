var pathToImage = $ref+"/images/page1/";
var content = [
		{
			coverimg : pathToImage+"coverpage.png",
			directionImgSrc : pathToImage+"north.png",
			img_extra_class:"coverpage",
			directionHeadingData: data.lesson.chapter,
			heading_extra_class:"coverHeading"
		},
		{
			directionHeadingData : data.string.directionheading,
			directionImgSrc : pathToImage+"north.png",
			northdirectionData : data.string.northdirection,
			eastdirectionData : data.string.eastdirection,
			southdirectionData : data.string.southdirection,
			westdirectionData : data.string.westdirection,
			directionNoteData : data.string.directionNote,
			directionHeadingData: data.lesson.chapter,
			heading_extra_class:"coverHeading",
			coverimg : pathToImage+"coverpage.png",
			img_extra_class:"coverpage",
		},
		{
			mapHeadingData : data.string.mapheading,
			mapImgSrc : pathToImage+"nepal.png",
			mapNoteData : data.string.mapnote,
		},
		{
			dragmapHeadingData : data.string.dragmapheading,
			dragmapImgSrc : pathToImage+"nepal.png",
			northdirectionData : data.string.northdirection,
			eastdirectionData : data.string.eastdirection,
			southdirectionData : data.string.southdirection,
			westdirectionData : data.string.westdirection,
			congratulationTextData: data.string.congratulationText,
		},
		{
			clickmapHeadingData : data.string.clickmapheading,
			clickmapImgSrc : pathToImage+"ilam.png",
			directionImgSrc : pathToImage+"west.png",
			ilamData : data.string.ilamText,
			sagarmathaData : data.string.sagarmathaText,
			mahakaliData : data.string.mahakaliText,
			janakpurData : data.string.janakpurText,
			northdirectionData : data.string.northdirection,
			eastdirectionData : data.string.eastdirection,
			southdirectionData : data.string.southdirection,
			westdirectionData : data.string.westdirection,
			congratulationTextData : data.string.congratulationText,
		}
];
(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		// $prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 5;

	loadTimelineProgress($total_page,countNext+1);

/*
* fourdirection
*/
	function fourdirection() {
		var source = $("#fourdirection-template").html()
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'block');

		var $directionWrap = $board.children('div.directionWrap');
		var $directionBtn = $directionWrap.children('div.directionBtn');
		var $directionNote = $directionWrap.children('p.directionNote');
		var $directionsDiv = $directionWrap.children('div.directions');
		var $directionsImg = $directionsDiv.children('img:nth-of-type(1)');
		var directionSrc, countpara=1;

		$directionBtn.on('click', function() {
			countpara++;
			if(countpara < 6){
				switch(countpara){
					case 3: directionSrc = pathToImage+"east.png"; break;
					case 4: directionSrc = pathToImage+"south.png"; break;
					case 5: directionSrc = pathToImage+"west.png"; break;
					default: break;
				}
					$directionsImg.attr('src', directionSrc);
					$directionsDiv.children("p:nth-of-type("+countpara+")").show(0);
			}

			else if(countpara == 6){
				$directionNote.show(0);
				$directionBtn.css('display', 'none');/*button not needed hide it*/
				$nextBtn.show(0);
			}

		});
	}

/*
* nepalmap
*/
	function nepalmap() {
		var source = $("#nepalmap-template").html()
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.show(0);
		var $mapWrap = $board.children('div.mapWrap');
		var $mapNote = $mapWrap.children('p.mapNote');
	}

/*
* nepalmapdrag
*/
	function nepalmapdrag() {
		var source = $("#nepalmapdrag-template").html()
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.css('display', 'none');

		var $directiondroppables = $board.children('div.dragmapWrap').children('div.mapDirectionDroppable').children('div');
		var $directiondraggables = $board.children('div.dragmapWrap').children('div.mapDirectionDraggable').children('p');
		var directionDropCount = 0;
		$directiondraggables.draggable({containment: "div.dragmapWrap", revert:"invalid"});

		/*eureka droppable this is nice use of droppable please see the use*/
		$directiondroppables.droppable({ tolerance: "pointer",
		accept : function(dropElem){
			//dropElem was the dropped element, return true or false to accept/refuse
			if($(this).attr("id") === dropElem.data('dragdirection')){
				return true;
			}
		},
		drop : function (event, ui) {
			$(this).children('p').css("visibility","visible");
			$(ui.draggable).css('display', 'none');
			if(++directionDropCount == 4){
				// ui.draggable.siblings('b').show(0);
				$nextBtn.show(0);
			}
		}
		});
	}

/*
* nepalmapclick
*/
	function nepalmapclick() {
		var source = $("#nepalmapclick-template").html()
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);
		var $clickmapWrap = $board.children('div.clickmapWrap');
		var $directionClickables = $clickmapWrap.children('div.clickdirections').children('p');
		var $clickMapContainer = $clickmapWrap.children('div.clickMapContainer');
		var $clickmapImage = $clickMapContainer.children('img.clickmapImg');
		var $clickdirectionBtn = $clickmapWrap.children('div.clickdirectionBtn');

		var correctAnsOrder = ["ilam","sagarmatha","mahakali","janakpur"];
		var directionClickCount = 0;
		var clickedItem;

		$directionClickables.on('click', function() {
			clickedItem = $(this).data("matchplace");
			if(clickedItem == correctAnsOrder[directionClickCount]){
				$(this).css({'border':'none', 'pointer-events':'none'});
				$clickdirectionBtn.show(0);
			}
		});

		$clickdirectionBtn.on('click', function() {
			directionClickCount++;
				switch(directionClickCount){
					case 1: directionSrc = pathToImage+correctAnsOrder[directionClickCount]+".png";
							whichpara = correctAnsOrder[directionClickCount];
							break;
					case 2: directionSrc = pathToImage+correctAnsOrder[directionClickCount]+".png";
							whichpara = correctAnsOrder[directionClickCount];
							break;
					case 3: directionSrc = pathToImage+correctAnsOrder[directionClickCount]+".png";
							whichpara = correctAnsOrder[directionClickCount];
							break;
					case 4: $clickdirectionBtn.css('display', 'none');
							$clickmapWrap.children('b').show(0);
							ole.footerNotificationHandler.pageEndSetNotification();
							break;
					default: break;
				}

				$clickmapImage.attr('src', directionSrc);
				$clickMapContainer.children('p').css('display', 'none');
				$clickMapContainer.children("p."+whichpara).show(0);
				$clickdirectionBtn.css('display', 'none');
		});
	}

fourdirection();
// nepalmapclick(countNext+=3);


	$nextBtn.on('click',function () {
		countNext++;
		// $(this).hide(0);
		switch (countNext) {
			case 1:
				fourdirection();
				$(".coverpage, .coverHeading").hide(0);
			break;
			case 2: nepalmap(); break;
			case 3: nepalmapdrag(); break;
			case 4: nepalmapclick();
			ole.footerNotificationHandler.pageEndSetNotification();
							break;
			default:break;
		}
		// console.log("countNext = "+countNext);
		loadTimelineProgress($total_page,countNext+1);
	})

})(jQuery);

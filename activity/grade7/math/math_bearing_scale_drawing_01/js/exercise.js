var pathToImage = $ref+"/images/exercise/";
var q2optionIddata = ["q2op1","q2op2"];
var q2labels = {
					centimeter: "cm",
					meter: "m",
					southeast : "॰SE",
					north : "॰N",
					northwest : "॰NW",
				};

var q1labels = {
					north : data.string.northdirectionOption,
					east : data.string.eastdirectionOption,
					south : data.string.southdirectionOption,
					west : data.string.westdirectionOption,
					northeast : data.string.northeastdirectionOption,
					southeast : data.string.southeastdirectionOption,
					southwest : data.string.southwestdirectionOption,
					northwest : data.string.northwestdirectionOption,
				};

var q2nametags = {
					lengthtag : data.string.makegroundmapLengthInput,
					widthtag : data.string.makegroundmapWidthInput,
					bearingtag : data.string.bearingtagText,
				};

var content = [
		{
			submit:data.string.submit,
			directionscaleHeadingData : data.string.directionscaleHeading ,
			questionImageIndividualClass : "slide1questionImage",
			housemap : true,
			questionimagesrc :  pathToImage+"q1.png",
			storeroomData : data.string.storeroomText ,
			bedroomleftData : data.string.bedroomText ,
			bedroomrightData : data.string.bedroomText ,
			bathroomData : data.string.bathroomText ,
			sittingroomData : data.string.sittingroomText ,
			kitchenData : data.string.kitchenText ,
			questionconclusionData : data.string.slide1questionconclusion ,
			question1Data : data.string.slide1question1Text ,
			o1isCorrect : "incorrect" ,
			o1labelData : q1labels.north ,
			o2isCorrect : "incorrect" ,
			o2labelData : q1labels.southeast ,
			o3isCorrect : "incorrect" ,
			o3labelData : q1labels.northeast ,
			o4isCorrect : "correct" ,
			o4labelData : q1labels.west ,
			question2Data : data.string.slide1question2Text,
			q2options : [
							{
								q2nametag : q2nametags.lengthtag,
								q2optionid : q2optionIddata[0],
								correctansData : "10",
								q2labelData : q2labels.meter,
							},
							{
								q2nametag : q2nametags.widthtag,
								q2optionid : q2optionIddata[2],
								correctansData : "4",
								q2labelData : q2labels.meter,
							}
						],

		},
		{
            submit:data.string.submit,
            directionscaleHeadingData : data.string.directionscaleHeading ,
			questionImageIndividualClass : "slide2questionImage",
			housemap : true,
			questionimagesrc :  pathToImage+"q2.png",
			storeroomData : data.string.storeroomText ,
			bedroomleftData : data.string.bedroomText ,
			bedroomrightData : data.string.bedroomText ,
			bathroomData : data.string.bathroomText ,
			sittingroomData : data.string.sittingroomText ,
			kitchenData : data.string.kitchenText ,
			questionconclusionData : data.string.slide2questionconclusion ,
			question1Data : data.string.slide2question1Text ,
			o1isCorrect : "incorrect" ,
			o1labelData : q1labels.northwest ,
			o2isCorrect : "incorrect" ,
			o2labelData : q1labels.southeast ,
			o3isCorrect : "correct" ,
			o3labelData : q1labels.east ,
			o4isCorrect : "incorrect" ,
			o4labelData : q1labels.north ,
			question2Data : data.string.slide2question2Text,
			q2options : [
							{
								q2nametag : q2nametags.lengthtag,
								q2optionid : q2optionIddata[0],
								correctansData : "9",
								q2labelData : q2labels.meter,
							},
							{
								q2nametag : q2nametags.widthtag,
								q2optionid : q2optionIddata[2],
								correctansData : "7.5",
								q2labelData : q2labels.meter,
							}
						],

		},
		{
            submit:data.string.submit,
            directionscaleHeadingData : data.string.directionscaleHeading ,
			questionImageIndividualClass : "slide3questionImage",
			housemapWithWell : true,
			questionimagesrc :  pathToImage+"q3.png",
			storeroomData : data.string.storeroomText ,
			bedroomleftData : data.string.bedroomText ,
			bedroomrightData : data.string.bedroomText ,
			bathroomData : data.string.bathroomText ,
			sittingroomData : data.string.sittingroomText ,
			kitchenData : data.string.kitchenText ,
			waterWellData : data.string.waterWellText,
			questionconclusionData : data.string.slide3questionconclusion ,
			question1Data : data.string.slide3question1Text ,
			o1isCorrect : "correct" ,
			o1labelData : q1labels.southeast ,
			o2isCorrect : "incorrect" ,
			o2labelData : q1labels.northeast ,
			o3isCorrect : "incorrect" ,
			o3labelData : q1labels.southwest ,
			o4isCorrect : "incorrect" ,
			o4labelData : q1labels.south ,
			question2Data : data.string.slide3question2Text,
			q2options : [
							{
								q2nametag : q2nametags.bearingtag,
								q2optionid : q2optionIddata[0],
								correctansData : "135",
								q2labelData : q2labels.southeast,
							}
						],

		},
		{
            submit:data.string.submit,
            directionscaleHeadingData : data.string.directionscaleHeading ,
			questionImageIndividualClass : "slide4questionImage",
			housemap : false,
			questionimagesrc :  pathToImage+"q4.png",
			cityOnTopData : data.string.mustangcity,
			cityOnBottomData : data.string.butwalcity,
			questionconclusionData : data.string.slide4questionconclusion ,
			question1Data : data.string.slide4question1Text ,
			o1isCorrect : "incorrect" ,
			o1labelData : q1labels.southeast ,
			o2isCorrect : "correct" ,
			o2labelData : q1labels.north ,
			o3isCorrect : "incorrect" ,
			o3labelData : q1labels.south ,
			o4isCorrect : "incorrect" ,
			o4labelData : q1labels.southwest ,
			question2Data : data.string.slide4question2Text,
			q2options : [
							{
								q2nametag : q2nametags.bearingtag,
								q2optionid : q2optionIddata[0],
								correctansData : "000",
								q2labelData : q2labels.north,
							}
						],

		},
		{
            submit:data.string.submit,
            directionscaleHeadingData : data.string.directionscaleHeading ,
			questionImageIndividualClass : "slide5questionImage",
			housemap : false,
			questionimagesrc :  pathToImage+"q5.png",
			cityOnTopData : data.string.kathmanducity,
			cityOnBottomData : data.string.biratnagarcity,
			questionconclusionData : data.string.slide5questionconclusion ,
			question1Data : data.string.slide5question1Text ,
			o1isCorrect : "incorrect" ,
			o1labelData : q1labels.southeast ,
			o2isCorrect : "incorrect" ,
			o2labelData : q1labels.west ,
			o3isCorrect : "correct" ,
			o3labelData : q1labels.northwest ,
			o4isCorrect : "incorrect" ,
			o4labelData : q1labels.southwest ,
			question2Data : data.string.slide5question2Text,
			q2options : [
							{
								q2nametag : q2nametags.bearingtag,
								q2optionid : q2optionIddata[0],
								correctansData : "315",
								q2labelData : q2labels.northwest,
							}
						],

		},
		{
            submit:data.string.submit,
            bearingquestionHeadingData : data.string.bearingquestionHeading ,
			bearingimageSrc : pathToImage+"q6.png",
			northdirectionData : data.string.bearingNtext,
			northeastdirectionData : data.string.bearingNEtext,
			eastdirectionData : data.string.bearingEtext,
			southeastdirectionData : data.string.bearingSEtext,
			southdirectionData : data.string.bearingStext,
			southwestdirectionData : data.string.bearingSWtext,
			westdirectionData : data.string.bearingWtext,
			northwestdirectionData : data.string.bearingNWtext,
			congratulationTextData : data.string.congratulationText,
		}

];

(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		// $prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 6;

	loadTimelineProgress($total_page,countNext+1);

/*
* directionscale
*/
	function directionscale() {
		var source = $("#directionscale-template").html()
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		$directionscale = $board.children('div.directionscale');
		$scaledrawBtn = $directionscale.children('div.scaledrawBtn');
		$questionImageAndHint = $directionscale.children('div.questionImageAndHint');
		$questionConclusion = $questionImageAndHint.children('p.questionConclusion');
		$questions = $directionscale.children('div.questions');
		$question1 = $questions.children('div.question1');
		$question2 = $questions.children('div.question2');
		$q1options = $question1.children("div.question1options").children('div');
		$q2options = $question2.children("div.question2options").children('p').children('input');
		$q2option1 = $question2.children("div.question2options").children('p:nth-of-type(1)').children('input');
		$q2option1mark = $question2.children('div.question2options').children('p:nth-of-type(1)').children("b.answermark");
		$q2option2 = $question2.children("div.question2options").children('p:nth-of-type(2)').children('input');
		$q2option2mark = $question2.children('div.question2options').children('p:nth-of-type(2)').children("b.answermark");

		/*on clicking question1 options*/
		$q1options.on('click', function() {
			$(this).children('b.answermark').show(0);
			if($(this).children('input:radio').val() == "incorrect"){
				$(this).children('input[value="incorrect"]').siblings('label').css("background","rgba(255,0,0,0.2)");
			}
			else if($(this).children('input:radio').val() == "correct"){
				$(this).children('input[value="correct"]').siblings('label').css("background","rgba(0,255,0,0.2)");
				$q1options.css('pointer-events', 'none');
				$question2.show(0);
			}
		});

		/*on answer input textbox*/
		$q2options.keyup(function() {
			 /*if other than number and minus sign remove it => The g means Global,
			 	and causes the replace call to replace all matches, not just the first one*/
				$(this).val( $(this).val().replace(/[^-\d\.]/g,''));
				if(content[countNext].q2options.length == 2){
					if($q2option1.val().length > 0 && $q2option2.val().length > 0){
						$scaledrawBtn.show(0);
					}
					else{
						$scaledrawBtn.css('display', 'none');
					}
				}

				else if(content[countNext].q2options.length == 1){
					if($q2option1.val().length > 0){
						$scaledrawBtn.show(0);
					}
					else{
						$scaledrawBtn.css('display', 'none');
					}
				}
		});

		/*flag to keep status of correct answer*/
		var q2op1correct = q2op2correct = false;

		$scaledrawBtn.on('click', function() {
			if(content[countNext].q2options.length == 2){
				if($q2option1.val() == $q2option1.data("correctans")){
					$q2option1mark.html("&#10003;");
					$q2option1mark.css('color', 'green').show(0);
					$q2option1.prop('disabled', true);
					q2op1correct = true;
					if(q2op2correct){
						$scaledrawBtn.css('display', 'none');
						$nextBtn.show(0);
					}
				}
				else{
					$q2option1mark.html("&#10799;");
					$q2option1mark.css('color', 'darkred').show(0);
					$scaledrawBtn.css('display', 'none');
				}

				if($q2option2.val() == $q2option2.data("correctans")){
					$q2option2mark.html("&#10003;");
					$q2option2mark.css('color', 'green').show(0);
					$q2option2.prop('disabled', true);
					q2op2correct = true;
					if(q2op1correct){
						$scaledrawBtn.css('display', 'none');
						$nextBtn.show(0);
					}
				}
				else{
					$q2option2mark.html("&#10799;");
					$q2option2mark.css('color', 'darkred').show(0);
					$scaledrawBtn.css('display', 'none');
				}
				$questionConclusion.show(0);

		}

		else if(content[countNext].q2options.length == 1){
			if($q2option1.val() == $q2option1.data("correctans")){
					$q2option1mark.html("&#10003;");
					$q2option1mark.css('color', 'green').show(0);
					$q2option1.prop('disabled', true);
					$scaledrawBtn.css('display', 'none');
					$nextBtn.show(0);
				}
			else{
					$q2option1mark.html("&#10799;");
					$q2option1mark.css('color', 'darkred').show(0);
					$scaledrawBtn.css('display', 'none');
				}
			$questionConclusion.show(0);
		}

		});
	}


/*
* bearingquestion
*/
	function bearingquestion() {
		var source = $("#bearingquestion-template").html()
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		$bearingquestion = $board.children('div.bearingquestion');
		var $bearingdroppables = $bearingquestion.children('div.bearingDroppables').children('div');
		var $bearingdraggables = $bearingquestion.children('div.bearingDraggables').children('p');
		var directionDropCount = 0;

		$bearingdraggables.draggable({containment: "div.dragmapWrap", revert:"invalid"});

		/*eureka droppable this is nice use of droppable please see the use*/
		$bearingdroppables.droppable({ tolerance: "pointer",
		accept : function(dropElem){
			//dropElem was the dropped element, return true or false to accept/refuse
			if($(this).attr("id") === dropElem.data('dragdirection')){
				return true;
			}
		},
		drop : function (event, ui) {
			$(this).children('p').css("visibility","visible");
			$(ui.draggable).css('display', 'none');
			if(++directionDropCount == 7){
				ui.draggable.siblings('b').show(0);
				$nextBtn.show(0);
			}
		}
		});
	}

directionscale(countNext+=0);

	$nextBtn.on('click',function () {
		countNext++;
		$(this).hide(0);
		switch (countNext) {
			case 1: directionscale(); break;
			case 2: directionscale(); break;
			case 3: directionscale(); break;
			case 4: directionscale(); break;
			case 5: bearingquestion(); break;
			case 6: ole.activityComplete.finishingcall(); break;
			default:break;
		}
		// console.log("countNext = "+countNext);
		loadTimelineProgress($total_page,countNext+1);
	})

})(jQuery);

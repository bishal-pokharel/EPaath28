var pathToImage = $ref + "/images/page2/";
var pathToAnimAngleImage = $ref + "/images/page2/animangle/";
var pathToAngles = $ref + "/images/page2/angles/";
var pathToVideoPlayImage = "images/playbtn.png";

var playbuttonImageHtml =
  "<img class='playimageInstr' src='" + pathToVideoPlayImage + "'>";

var slide2instruction = ole.textSR(
  data.string.anglesinstruction,
  "#imageplaceholder#",
  playbuttonImageHtml
);

var content = [
  {
    compassHeadingData: data.string.compassheading,
    compassImgSrc: pathToImage + "compass.png",
    compassInfoData: data.string.compassinfo
  },
  {
    anglesHeadingData: data.string.compassheading,
    bearingDefData: data.string.bearingdef,
    anglesImgSrc: pathToAnimAngleImage + "animangle1.png",
    playAnimationImgSrc: pathToVideoPlayImage,
    anglesInfo1: slide2instruction,
    anglesInfo2: data.string.anglesinfo2
  },
  {
    anglesexpalinHeadingData: data.string.anglesexpalinheading,
    anglesexpalinImgSrc: pathToAngles + "0.png",
    anglesexplainData: data.string.anglesexpalin1
  }
];
(function($) {
  var $board = $(".board"),
    $nextBtn = $("#activity-page-next-btn-enabled"),
    // $prevBtn = $(".prevButton"),
    countNext = 0,
    $total_page = 11;

  loadTimelineProgress($total_page, countNext + 1);

  /*
   * bearingcompass
   */
  function bearingcompass() {
    var source = $("#bearingcompass-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "block");
    var $compasswrap = $board.children("div.compasswrap");
  }

  /*
   * bearingangles
   */
  function bearingangles() {
    var source = $("#bearingangles-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    // $nextBtn.css('display', 'none');

    var $angleswrap = $board.children("div.angleswrap");
    var $animangleDiv = $angleswrap.children("div.animangle");
    var $anglesimage = $animangleDiv.children("img.animangleimage");
    var $playAnglesanim = $animangleDiv.children("img.playAnimation");
    var $bearingdef = $angleswrap.children("p:nth-of-type(2)");
    var $anglesinfo1 = $angleswrap.children("p:nth-of-type(3)");
    var $anglesinfo2 = $angleswrap.children("p:nth-of-type(4)");
    var $bearingBtn = $("#activity-page-next-btn-enabled");

    var stepcount = 0;
    var animImageOrder = [
      "animangle2.png",
      "animangle3.png",
      "animangle4.png",
      "animangle5.png",
      "animangle6.png",
      "animangle7.png",
      "animangle8.png",
      "animangle9.png"
    ];
    var animImagecount = 0;
    var animationTimeInterval = 500;
    var setintervalvar;
    var changeToImage;
    var firstplay = true;

    ole.parseToolTip($bearingdef, data.string.bearingdef_highlight);
    $bearingBtn.css("display", "none");

    function angleAnimFunc() {
      setintervalvar = setInterval(function() {
        changeToImage = pathToAnimAngleImage + animImageOrder[animImagecount++];
        $anglesimage.attr("src", changeToImage);
        if (animImagecount == 8) {
          clearInterval(setintervalvar); /*stop animation here*/
          $playAnglesanim.show(0);
          if (firstplay) {
            firstplay = false;
            $anglesinfo1.html(data.string.anglesinfo1);
            $bearingBtn.show(0);
          }
        }
      }, animationTimeInterval);
    }

    $playAnglesanim.on("click", function() {
      $(this).css("display", "none");
      $anglesimage.attr("src", pathToAnimAngleImage + "animangle1.png");
      animImagecount = 0;
      angleAnimFunc();
    });

    $bearingBtn.on("click", function() {
      $(this).css("display", "none");
      $anglesinfo2.show(0);
      $nextBtn.show(0);
    });
  }

  /*
   * bearinganglesexplain
   */
  function bearinganglesexplain() {
    var source = $("#bearinganglesexplain-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    // $nextBtn.css('display', 'none');

    var $anglesexpalinwrap = $board.children("div.anglesexpalinwrap");
    var $bearingBtn = $("#activity-page-next-btn-enabled");
    var $angleimage = $anglesexpalinwrap.children("img.angleimage");
    var $angleExplain = $anglesexpalinwrap.children("p:nth-of-type(2)");
    var anglescount = 0;
    var makechanges = function() {
      (this.textandimage = function() {
        $angleimage.attr("src", pathToAngles + anglescount + ".png");
        $angleExplain.html(data.string["anglesexpalin" + (anglescount + 1)]);
      }),
        (this.image = function() {
          $angleimage.attr("src", pathToAngles + anglescount + ".png");
        }),
        (this.showNextPageNotification = function() {
          $bearingBtn.css("display", "none");
          ole.footerNotificationHandler.pageEndSetNotification();
        });
    };

    var makechange = new makechanges();

    $bearingBtn.on("click", function() {
      ++anglescount < 8
        ? anglescount < 5
          ? makechange.textandimage()
          : makechange.image()
        : makechange.showNextPageNotification();
    });
  }

  bearingcompass();

  $nextBtn.on("click", function() {
    countNext++;
    $(this).hide(0);
    switch (countNext) {
      case 1:
        bearingangles();
        break;
      case 2:
        bearinganglesexplain();
        break;
      default:
        break;
    }
    // console.log("countNext = "+countNext);
    loadTimelineProgress($total_page, countNext + 1);
  });
})(jQuery);

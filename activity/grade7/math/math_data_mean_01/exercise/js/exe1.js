
var newWidth;
var wrongCounter=rightCounter=0;
$(function(){


	var isclicked=false;


	$(".headTitle").html(data.string.exe1);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	
	var counter=1;


	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		
		$(this).hide(0);
		$("#Imgshow").fadeOut(10);
		isclicked=false;
		
		counter++;		
		getHtml(counter);	
	});

	

	$("#figBox").on("keyup",".noInbx",function(){
	
		
		if(/\D/g.test(this.value))
		{
		
			this.value = this.value.replace(/\D/g, '');
		}
	});

	$("#figBox").on('click','#checkInputVal',function(){
		var emptyyes=true;
		var allrite=true;

		$( ".noInbx" ).each(function( index ) {
				var myval=$( ".noInbx" ).val();
				if(myval.length==0)
				{
					emptyyes=false;
					
				}
				
		});
		if(emptyyes==true)
  		{
  			$(this).hide(0);
  			var myval=parseInt($("#myInput1").val());
  			var realval=parseInt($("#myInput1").attr('ansval'));
  			if(myval==realval)
  			{
  				$("#myInput1").css({'background':'green'});
  			}
  			else
  			{
  				$("#myInput1").css({'background':'red'});
  				allrite=false;
  			}

  			myval=parseInt($("#myInput2").val());
  			realval=parseInt($("#myInput2").attr('ansval'));
  			if(myval==realval)
  			{
  				$("#myInput2").css({'background':'green'});
  			}
  			else
  			{
  				$("#myInput2").css({'background':'red'});
  				allrite=false;
  			
  			}

  			myval=parseInt($("#myInput3").val());
  			realval=parseInt($("#myInput3").attr('ansval'));
  			if(myval==realval)
  			{
  				$("#myInput3").css({'background':'green'});
  			}
  			else
  			{
  				$("#myInput3").css({'background':'red'});
  				allrite=false;
  			
  			}

  			if(allrite==true)
  			{
  				rightCounter++;	
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);	
  			}
  			else
  			{		wrongCounter++;
  				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
  			}

			$("#activity-page-next-btn-enabled").show(0);
  		}
  	});

	$("#figBox").on('click','.clickMeBox',function(){
	
		if(isclicked==false)
		{
			if($('.clickMeBox').hasClass('radioBx'))
			{
				$('.clickMeBox').removeClass('radioBx').addClass('radioBxA');

			}
			else
				$('.clickMeBox').removeClass('radioBx1').addClass('radioBx1A');

			var ids=parseInt($(this).attr('corr'));
			var myids=$(this).attr('id');
			isclicked=true;

			if(ids==1)
			{
				$(this).addClass('correctColor');
				rightCounter++;		
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);	

			}
			else
			{
				wrongCounter++;
				$(".clickMeBox[corr='1']").addClass('correctColor');
				$(this).addClass('colorChange');
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
			}
			$("#activity-page-next-btn-enabled").show(0);
		}
	});


	$("#figBox").on('click','#checkInputVal2',function(){
		var emptyyes=true;
		var allrite=true;

		$( ".noInbx" ).each(function( index ) {
				var myval=$( ".noInbx" ).val();
				if(myval.length==0)
				{
					emptyyes=false;
					
				}
				
		});
		if(emptyyes==true)
  		{
  			$(this).hide(0);
  			

  			if(counter==7)
  			{
				var myval=parseInt($("#myInput5").val());
				var realval=parseInt($("#myInput5").attr('ansval'));
				if(myval==realval)
				{
					$("#myInput5").css({'background':'green'});
				}
				else
				{
					$("#myInput5").css({'background':'red'});
					allrite=false;
				}
				var myval=parseInt($("#myInput6").val());
				var realval=parseInt($("#myInput6").attr('ansval'));
				if(myval==realval)
				{
					$("#myInput6").css({'background':'green'});
				}else
				{
					$("#myInput6").css({'background':'red'});
					allrite=false;
				}

  			}
  			else
  			{
				var myval=parseInt($("#myInput4").val());
				var realval=parseInt($("#myInput4").attr('ansval'));
				if(myval==realval)
				{
					$("#myInput4").css({'background':'green'});
				}else
				{
					$("#myInput4").css({'background':'red'});
					allrite=false;
				}

  			}

  			if(allrite==true)
  			{
  				rightCounter++;	
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);	
  			}
  			else
  			{		wrongCounter++;
  				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
  			}

  			$("#activity-page-next-btn-enabled").show(0);

  		}
	});

})

function getHtml(cnt)
{
	loadTimelineProgress(8,cnt);
	
	var datas=getdata(cnt);


	var source;


	

	source=$("#template-1").html();
	if(cnt==8)
		source=$("#template-2").html();
	
	

	var template=Handlebars.compile(source);
	var html=template(datas);



	$("#figBox").fadeOut(10,function(){ $(this).html(html); 
	}).delay(10).fadeIn(10,function(){
		
		
		if(cnt<=7){
			animateGraph();
		}	
		else
		{ 
				ole.footerNotificationHandler.pageEndSetNotification();
			
		}	

	});
}


function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		
	 	
	 	case 1:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e1p1title,
	 			firstText_1:"firstText_1",
	 			mytable:"yes",
	 			tabtxt1:data.string.e1p1t1,
	 			tabtxt2:data.string.e1p1t2,
	 			tabtxt3:data.string.e1p1t3,
	 			tabtxt4:data.string.e1p1t4,
	 			tabtxt5:data.string.e1p1t5,
	 			tabtxt6:data.string.s_1_term_1,
	 			tabtxt7:data.string.s_1_term_2,
	 			tabtxt8:data.string.e1p1t6,
	 			tabtxt9:data.string.s_2_term_1,
	 			tabtxt10:data.string.s_2_term_2,
	 			tabtxt11:data.string.e1p1t7,
	 			tabtxt12:data.string.s_3_term_1,
	 			tabtxt13:data.string.s_3_term_2,
	 			tabtxt14:data.string.e1p1t8,
	 			tabtxt15:data.string.s_4_term_1,
	 			tabtxt16:data.string.s_4_term_2,
	 			tabtxt17:data.string.e1p1t9,
	 			tabtxt18:data.string.s_5_term_1,
	 			tabtxt19:data.string.s_5_term_2,
	 			myGraph:"yes",
	 			trdText:data.string.p1_3,
	 			trdText_1:"trdText_1",
	 			frthText_1:"frthText_1",
	 			frthText:data.string.p1_4,	 			
	 			lab1Text1:data.string.e1p1t5,
	 			lab1Text2:data.string.e1p1t6,
	 			lab1Text3:data.string.e1p1t7,
	 			lab1Text4:data.string.e1p1t8,
	 			lab1Text5:data.string.e1p1t9,
	 			lab2Text1:data.string.e1p1t3,
	 			lab2Text2:data.string.e1p1t4,
	 			confirm:data.string.confrim
	 		};
	 		break;
	 		
	 	}	
	 	case 2:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e1p2title,
	 			firstText_1:"firstText_1",
	 			myGraph:"yes",
	 			trdText:data.string.p1_3,
	 			trdText_1:"trdText_1",
	 			frthText_1:"frthText_1",
	 			frthText:data.string.p1_4,	 			
	 			lab1Text1:data.string.e1p1t5,
	 			lab1Text2:data.string.e1p1t6,
	 			lab1Text3:data.string.e1p1t7,
	 			lab1Text4:data.string.e1p1t8,
	 			lab1Text5:data.string.e1p1t9,
	 			lab2Text1:data.string.e1p1t3,
	 			lab2Text2:data.string.e1p1t4,
	 			confirm:data.string.confrim,
	 			chooseMe:"yes",
	 			quesTitle:data.string.e1p2q,
	 			opt1:data.string.f2option1,
	 			opt2:data.string.f2option2,
	 			corr1:1,
	 			corr2:0,
	 			radioBx:"radioBx"
	 		}
	 		break;
	 		
	 	}
	 	case 3:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e1p2title,
	 			firstText_1:"firstText_1",
	 			myGraph:"yes",
	 			trdText:data.string.p1_3,
	 			trdText_1:"trdText_1",
	 			frthText_1:"frthText_1",
	 			frthText:data.string.p1_4,	 			
	 			lab1Text1:data.string.e1p1t5,
	 			lab1Text2:data.string.e1p1t6,
	 			lab1Text3:data.string.e1p1t7,
	 			lab1Text4:data.string.e1p1t8,
	 			lab1Text5:data.string.e1p1t9,
	 			lab2Text1:data.string.e1p1t3,
	 			lab2Text2:data.string.e1p1t4,
	 			confirm:data.string.confrim,
	 			chooseMe:"yes",
	 			quesTitle:data.string.e1p3q,
	 			opt1:data.string.e1p1t5,
	 			opt2:data.string.e1p1t6,
	 			opt3:data.string.e1p1t7,
	 			opt4:data.string.e1p1t8,
	 			opt5:data.string.e1p1t9,	 			
	 			corr1:0,
	 			corr2:0,
	 			corr3:1,
	 			corr4:0,
	 			corr5:0,
	 			radioBx:"radioBx1"
	 		}
	 		break;
	 		
	 	}
	 	case 4:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e1p2title,
	 			firstText_1:"firstText_1",
	 			myGraph:"yes",
	 			trdText:data.string.p1_3,
	 			trdText_1:"trdText_1",
	 			frthText_1:"frthText_1",
	 			frthText:data.string.p1_4,	 			
	 			lab1Text1:data.string.e1p1t5,
	 			lab1Text2:data.string.e1p1t6,
	 			lab1Text3:data.string.e1p1t7,
	 			lab1Text4:data.string.e1p1t8,
	 			lab1Text5:data.string.e1p1t9,
	 			lab2Text1:data.string.e1p1t3,
	 			lab2Text2:data.string.e1p1t4,
	 			confirm:data.string.confrim,
	 			chooseMe:"yes",
	 			quesTitle:data.string.e1p4q,
	 			opt1:data.string.e1p1t5,
	 			opt2:data.string.e1p1t6,
	 			opt3:data.string.e1p1t7,
	 			opt4:data.string.e1p1t8,
	 			opt5:data.string.e1p1t9,	 			
	 			corr1:0,
	 			corr2:0,
	 			corr3:0,
	 			corr4:1,
	 			corr5:0,
	 			radioBx:"radioBx1"
	 		}
	 		break;
	 		
	 	}
	 	case 5:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e1p2title,
	 			firstText_1:"firstText_1",
	 			myGraph:"yes",
	 			trdText:data.string.p1_3,
	 			trdText_1:"trdText_1",
	 			frthText_1:"frthText_1",
	 			frthText:data.string.p1_4,	 			
	 			lab1Text1:data.string.e1p1t5,
	 			lab1Text2:data.string.e1p1t6,
	 			lab1Text3:data.string.e1p1t7,
	 			lab1Text4:data.string.e1p1t8,
	 			lab1Text5:data.string.e1p1t9,
	 			lab2Text1:data.string.e1p1t3,
	 			lab2Text2:data.string.e1p1t4,
	 			confirm:data.string.confrim,
	 			fillinblanck:"yes",
	 			quesTitle:data.string.e1p5q,
	 			ansval:data.string.ans5,
	 			myInputs:"myInput4"
	 			
	 		}
	 		break;
	 		
	 	}
	 	case 6:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e1p2title,
	 			firstText_1:"firstText_1",
	 			myGraph:"yes",
	 			trdText:data.string.p1_3,
	 			trdText_1:"trdText_1",
	 			frthText_1:"frthText_1",
	 			frthText:data.string.p1_4,	 			
	 			lab1Text1:data.string.e1p1t5,
	 			lab1Text2:data.string.e1p1t6,
	 			lab1Text3:data.string.e1p1t7,
	 			lab1Text4:data.string.e1p1t8,
	 			lab1Text5:data.string.e1p1t9,
	 			lab2Text1:data.string.e1p1t3,
	 			lab2Text2:data.string.e1p1t4,
	 			confirm:data.string.confrim,
	 			fillinblanck:"yes",
	 			quesTitle:data.string.e1p6q,
	 			ansval:data.string.ans6,
	 			myInputs:"myInput4"
	 			
	 		}
	 		break;
	 		
	 	}
	 	case 7:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e1p2title,
	 			firstText_1:"firstText_1",
	 			myGraph:"yes",
	 			trdText:data.string.p1_3,
	 			trdText_1:"trdText_1",
	 			frthText_1:"frthText_1",
	 			frthText:data.string.p1_4,	 			
	 			lab1Text1:data.string.e1p1t5,
	 			lab1Text2:data.string.e1p1t6,
	 			lab1Text3:data.string.e1p1t7,
	 			lab1Text4:data.string.e1p1t8,
	 			lab1Text5:data.string.e1p1t9,
	 			lab2Text1:data.string.e1p1t3,
	 			lab2Text2:data.string.e1p1t4,
	 			confirm:data.string.confrim,
	 			fillinblanck:"yes",
	 			quesTitle:data.string.e1p7q,
	 			ansval:data.string.ans71,
	 			maxval:data.string.e1p7t1,
	 			minval:data.string.e1p7t2,
	 			ansval2:data.string.ans72,
	 			myInputs:"myInput5",
	 			myInputs2:"myInput6"

	 			
	 		}
	 		break;
	 		
	 	}

	 	case 8:
	 	{
	 		datas={
	 			allansw:[
	 				{ques:data.string.e1p1title , qans: data.string.s_1_term_1 +","+ data.string.s_3_term_1+ ","+data.string.s_5_term_2 },
	 				{ques:data.string.e1p2q , qans: data.string.f2option1  },
	 				{ques:data.string.e1p3q , qans: data.string.e1p1t7  },
	 				{ques:data.string.e1p4q , qans: data.string.e1p1t8  },
	 				{ques:data.string.e1p5q , qans: data.string.ans5  },
	 				{ques:data.string.e1p6q , qans: data.string.ans6  },
	 				{ques:data.string.e1p7q , qans: data.string.e1p7t1+data.string.ans71+" "+data.string.e1p7t2+data.string.ans72}	 				
	 			],
	 			rite:data.string.q1_11,
	 			wrng:data.string.q1_12,
	 			rnum:rightCounter,
	 			wnum:wrongCounter	 			
	 		}
	 		break;
	 		
	 	}
	 	
	 	


	}


	return datas;
}





function animateGraph()
{

	
	$("#frthText_1").html(data.string.p1_10);
	$('.labelGraph2').show(0);
	
	var myArray=[32,36,36, 40, 33,37, 56,65,67];

	$('.column').css('height','0');
	
	
	
	
		var flt=parseInt(data.string['s_1_term_1']);
		
		$('#col1').css({height: flt+"%"});

		var flt2=parseInt(data.string['s_1_term_2']);
		
		$('#col2').css({height: flt2+"%"});


		 flt=parseInt(data.string['s_2_term_1']);
		
		$('#col3').css({height: flt+"%"});

		 flt2=parseInt(data.string['s_2_term_2']);
		
		$('#col4').css({height: flt2+"%"});


		 flt=parseInt(data.string['s_3_term_1']);
		
		$('#col5').css({height: flt+"%"});

		 flt2=parseInt(data.string['s_3_term_2']);
		
		$('#col6').css({height: flt2+"%"});


		 flt=parseInt(data.string['s_4_term_1']);
		
		$('#col7').css({height: flt+"%"});

		 flt2=parseInt(data.string['s_4_term_2']);
		
		$('#col8').css({height: flt2+"%"});

		 flt=parseInt(data.string['s_5_term_1']);
		
		$('#col9').css({height: flt+"%"});

		 flt2=parseInt(data.string['s_5_term_2']);
		
		$('#col10').css({height: flt2+"%"});
	

}

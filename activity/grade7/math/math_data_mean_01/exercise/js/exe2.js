
var newWidth;
var wrongCounter=rightCounter=0;
var counterMe=0;
$(function(){


	var isclicked=false;


	$(".headTitle").html(data.string.exe2);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	
	var counter=1;


	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		
		$(this).hide(0);
		$("#Imgshow").fadeOut(10);
		isclicked=false;
		
		counter++;		
		getHtml(counter);	
	});

	

	$("#figBox").on("keyup",".noInbx",function(){
			$("#activity-page-next-btn-enabled").show(0);
	
		
		if(/\D/g.test(this.value))
		{
		
			this.value = this.value.replace(/\D/g, '');
		}
	});

	$("#figBox").on('click','#checkInputVal',function(){
		var emptyyes=true;
		var allrite=true;

		$( ".noInbx" ).each(function( index ) {
				var myval=$( ".noInbx" ).val();
				if(myval.length==0)
				{
					emptyyes=false;
					$( ".noInbx" ).css({'background':'red'});
					
				}
				
		});
		if(emptyyes==true)
  		{
  			var myval=parseInt($("#myInput1").val());
  			var realval=parseInt($("#myInput1").attr('ansval'));
  			if(myval==realval)
  			{
  				$("#myInput1").css({'background':'green'});
  			}
  			else
  			{
  				$("#myInput1").css({'background':'red'});
  				allrite=false;
  			}

  			myval=parseInt($("#myInput2").val());
  			realval=parseInt($("#myInput2").attr('ansval'));
  			if(myval==realval)
  			{
  				$("#myInput2").css({'background':'green'});
  			}
  			else
  			{
  				$("#myInput2").css({'background':'red'});
  				allrite=false;
  			
  			}
  			if(counter==1)
  			{
  				myval=parseInt($("#myInput3").val());
	  			realval=parseInt($("#myInput3").attr('ansval'));
	  			if(myval==realval)
	  			{
	  				$("#myInput3").css({'background':'green'});
	  			}
	  			else
	  			{
	  				$("#myInput3").css({'background':'red'});
	  				allrite=false;
	  			
	  			}
  			}
	  			

  			if(allrite==true)
  			{
  				rightCounter++;	
  				$(this).hide(0);
				$("#activity-page-next-btn-enabled").show(0);
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);	
  			}
  			else
  			{		wrongCounter++;
  				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
  			}
  		}
  	});

	$("#figBox").on('click','.clickMeBox',function(){
	
		if(isclicked==false)
		{
			if($('.clickMeBox').hasClass('radioBx'))
			{
				$('.clickMeBox').removeClass('radioBx').addClass('radioBxA');

			}
			else
				$('.clickMeBox').removeClass('radioBx1').addClass('radioBx1A');

			var ids=parseInt($(this).attr('corr'));
			var myids=$(this).attr('id');
			isclicked=true;

			if(ids==1)
			{
				$(this).addClass('correctColor');
				rightCounter++;		
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);	

			}
			else
			{
				wrongCounter++;
				$(".clickMeBox[corr='1']").addClass('correctColor');
				$(this).addClass('colorChange');
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
			}
			$("#activity-page-next-btn-enabled").show(0);
		}
	});


	$("#figBox").on('click','#checkInputVal1',function(){
		
		var allrite=true;		
		$(this).hide(0);
			

		var myval=parseFloat($("#myInput4").val());
		var realval=parseInt(data.string.e2p1tt1e)/parseInt(data.string.e2p1tt1d);
		
		if(myval==realval)
		{
			$("#myInput4").css({'background':'green'});
		}
		else
		{
			$("#myInput4").css({'background':'red'});
			allrite=false;
		}

		if(allrite==true)
		{
			rightCounter++;	
			$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);	
		}
		else
		{	wrongCounter++;
			$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
		}

		$("#activity-page-next-btn-enabled").show(0);
  		
	});

})

function getHtml(cnt)
{
	loadTimelineProgress(7,cnt);
	
	var datas=getdata(cnt);


	var source;


	

	source=$("#template-1").html();
	if(cnt==7)
		source=$("#template-2").html();
	
	

	var template=Handlebars.compile(source);
	var html=template(datas);



	$("#figBox").fadeOut(10,function(){ $(this).html(html); 
	}).delay(10).fadeIn(10,function(){
		if(cnt==3)
		{
			$(".dragmeto" ).draggable({ revert: "invalid"});
			$(".whatdrop").dropMe();
		}	
		
		if(cnt==7){
			$("#activity-page-next-btn-enabled").show(0);
			
		}	

		if(cnt > 7){
			ole.activityComplete.finishingcall();
		}
		

	});
}


function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		
	 	
	 	case 1:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e2p1title,
	 			firstText_1:"firstText_1",
	 			mytable:"yes",
	 			tabtxt1:data.string.e2p1tt1,
	 			tabtxt2:data.string.e2p1tt2,	 			
	 			trdText:data.string.p1_3,
	 			tabtxt1a:data.string.e2p1t1,
	 			tabtxt2a:data.string.e2p1t2,
	 			tabtxt3a:data.string.e2p1t3,
	 			confirm:data.string.confrim,
	 			ans1:data.string.e2p1tt1a,
	 			ans2:data.string.e2p1tt1b,
	 			ans3:data.string.e2p1tt1c
	 		};
	 		break;
	 		
	 	}	
	 	case 2:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e2p2title,
	 			firstText_1:"firstText_1",
	 			mytable:"yes",
	 			tabtxt1:data.string.e2p1tt1,
	 			tabtxt2:data.string.e2p1tt2,	 			
	 			trdText:data.string.p1_3,
	 			tabtxt1a:data.string.e2p1t1,
	 			tabtxt2a:data.string.e2p1t2,
	 			tabtxt3a:data.string.e2p1t3,
	 			confirm:data.string.confrim,
	 			txt1:data.string.e2p1tt1a,
	 			txt2:data.string.e2p1tt1b,
	 			txt3:data.string.e2p1tt1c,
	 			secondMe:"yes",
	 			txt4:data.string.e2p1t4,
	 			ans4:data.string.e2p1tt1d,
	 			ans5:data.string.e2p1tt1e
	 		}
	 		break;
	 		
	 	}
	 	case 3:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e2p3title,
	 			firstText_1:"firstText_1",
	 			tabtxt1:data.string.e2p1tt1,
	 			tabtxt2:data.string.e2p1tt2,
	 			dragMe:"yes",
	 			drag_txt_1:data.string.e2p1t5,
	 			drag_txt_2:data.string.drag1text,
	 			drag_txt_3:data.string.drag2text
	 		}
	 		break;
	 		
	 	}
	 	case 4:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e2p3title,
	 			firstText_1:"firstText_1",
	 			tabtxt1:data.string.e2p1tt1,
	 			tabtxt2:data.string.e2p1tt2,
	 			confirm:data.string.confrim,
	 			drag_txt_1:data.string.e2p1t5,
	 			drag_txt_2:data.string.drag1text,
	 			drag_txt_3:data.string.drag2text,
	 			notdragMe:"yes"
	 		}
	 		break;
	 		
	 	}
	 	case 5:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e2p5title,
	 			firstText_1:"firstText_1",
	 			chooseMe:"yes",
	 			tabtxt1:data.string.e2p1tt1,
	 			tabtxt2:data.string.e2p1tt2,
	 			quesTitle:data.string.e2p6title2,
	 			opt1:data.string.e2p6o2,
	 			opt2:data.string.e2p6o1,
	 			radioBx:"radioBx",
	 			corr1:0,
	 			corr2:1
	 			
	 		}
	 		break;
	 		
	 	}
	 	case 6:
	 	{
	 		datas={mycounter:cnt,
	 			firstText:data.string.e2p5title,
	 			firstText_1:"firstText_1",
	 			chooseMe:"yes",
	 			tabtxt1:data.string.e2p1tt1,
	 			tabtxt2:data.string.e2p1tt2,
	 			quesTitle:data.string.e2p5title2,
	 			opt1:data.string.e2p5o1,
	 			opt2:data.string.e2p5o2,
	 			opt3:data.string.e2p5o3,
	 			radioBx:"radioBx1",
	 			corr1:1,
	 			corr2:0,
	 			corr3:0
	 			
	 		}
	 		break;
	 		
	 	}
	 	case 7:
	 	{
	 		var realval=parseInt(data.string.e2p1tt1e)/parseInt(data.string.e2p1tt1d);
	 		datas={
	 			allansw:[
	 					{ques:data.string.e2p1title, qans:data.string.e2p1tt1a+", "+data.string.e2p1tt1b+", "+data.string.e2p1tt1c},
	 					{ques:data.string.e2p2title, qans:data.string.e2p1tt1d+","+data.string.e2p1tt1e},
	 					{ques:data.string.e2p3title, qans:data.string.drag2text+"/"+data.string.drag1text},
	 					{ques:data.string.e2p1t5, qans:realval},
	 					{ques:data.string.e2p6title2, qans:data.string.e2p6o1},
	 					{ques:data.string.e2p5title2, qans:data.string.e2p5o1}
	 					],
	 			rite:data.string.q1_11,
	 			wrng:data.string.q1_12,
	 			rnum:rightCounter,
	 			wnum:wrongCounter
	 			
	 		}
	 		
	 	}

	 	
	 	
	 	


	}


	return datas;
}





$.fn.dropMe=function ($accept)
{
	$(".tg").hide(0);
	var $this=$(this);

	$this.droppable({
          drop: function( event, ui ) {
        	var id=ui.draggable.attr('id');
        	var val=$("#"+id).html();
        	$("#"+id).hide(0);
        	
        	counterMe++;
        	$(this).html(val);

        	$(this).droppable( 'disable' );

        	if(counterMe==2)
        	{
        		var drp1=$(".drop1").html();
        		var drp2=$(".drop2").html();
        		var whatval=data.string.drag2text
        		if(drp1==whatval)
        		{
        			rightCounter++;	
					$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
        		}
        		else 
        		{
        			wrongCounter++;
  					$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
        		}
        		$("#activity-page-next-btn-enabled").show(0);
        	}
        	
        }	 
        	
    });

};


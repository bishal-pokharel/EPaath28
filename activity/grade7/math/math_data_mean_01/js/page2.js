
var newWidth;
var whatVal=[];
var toggleClick=true;
$(function(){


	var $board = $('#myboard'),
		$canvas=$("canvas");


	$(".headTitle").html(data.string.p2_2);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	$("#repeatBtn").html(getReloadBtn());

	var counter=1;

	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		
		$(this).hide(0);
		
		counter++;
		

		
			getHtml(counter);
		
		
	});

	$("#repeatBtn").click(function(){
		location.reload();
	});

	$("#figBox").on("keyup",".inBx",function(){
	
		
		if(/\D/g.test(this.value))
		{
		
			this.value = this.value.replace(/\D/g, '');
		}
	});

	$("#figBox").on('click','#confirmBtnVal',function(){
		var emptyyes=true;

		$( ".inBx" ).each(function( index ) {
				var myval=$( ".inBx" ).val();
				if(myval.length==0)
				{
					emptyyes=false;
					
				}
				
		});
		if(emptyyes==true)
  		{
  			$(".lastBx span").hide(0);
  			$(this).hide(0);
  			$(".lastBx span").hide(0);

			$( ".inBx" ).each(function(index) {
				whatVal[index]=parseInt($(this).val());
			});

			
			$("#fstRow").html(whatVal[0]+whatVal[1]);
			$("#secRow").html(whatVal[2]+whatVal[3]);
			$("#trdRow").html(whatVal[4]+whatVal[5]);
			$("#frtRow").html(whatVal[6]+whatVal[7]);
			$("#activity-page-next-btn-enabled").show(0);
  		}
  		else
  		{
  			$(".lastBx span").show(0);
  		}

		

	});

	$("#figBox").on('click',".graphController1>div",function(){

		var idVal=$(this).attr('id');
		

		switch(idVal)
		{
			case "control_1a_up":
			{

				var heightVal=parseInt($('#col0 div').html());
				var newVal=heightVal+1;
				$('#col0').animate({height: newVal+"%"},10,function(){
					$('#col0').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_1b_up":
			{

				var heightVal=parseInt($('#col1 div').html());
				var newVal=heightVal+1;
				$('#col1').animate({height: newVal+"%"},10,function(){
					$('#col1').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_2a_up":
			{

				var heightVal=parseInt($('#col2 div').html());
				var newVal=heightVal+1;
				$('#col2').animate({height: newVal+"%"},10,function(){
					$('#col2').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_2b_up":
			{

				var heightVal=parseInt($('#col3 div').html());
				var newVal=heightVal+1;
				$('#col3').animate({height: newVal+"%"},10,function(){
					$('#col3').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_3a_up":
			{

				var heightVal=parseInt($('#col4 div').html());
				var newVal=heightVal+1;
				$('#col4').animate({height: newVal+"%"},10,function(){
					$('#col4').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_3b_up":
			{

				var heightVal=parseInt($('#col5 div').html());
				var newVal=heightVal+1;
				$('#col5').animate({height: newVal+"%"},10,function(){
					$('#col5').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_4a_up":
			{

				var heightVal=parseInt($('#col6 div').html());
				var newVal=heightVal+1;
				$('#col6').animate({height: newVal+"%"},10,function(){
					$('#col6').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_4b_up":
			{

				var heightVal=parseInt($('#col7 div').html());
				var newVal=heightVal+1;
				$('#col7').animate({height: newVal+"%"},10,function(){
					$('#col7').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
		}
	});//click
	$("#figBox").on('click',".graphController2>div",function(){

		var idVal=$(this).attr('id');
		

		switch(idVal)
		{
			case "control_1a_down":
			{

				var heightVal=parseInt($('#col0 div').html());
				var newVal=heightVal-1;
				$('#col0').animate({height: newVal+"%"},10,function(){
					$('#col0').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_1b_down":
			{

				var heightVal=parseInt($('#col1 div').html());
				var newVal=heightVal-1;
				$('#col1').animate({height: newVal+"%"},10,function(){
					$('#col1').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_2a_down":
			{

				var heightVal=parseInt($('#col2 div').html());
				var newVal=heightVal-1;
				$('#col2').animate({height: newVal+"%"},10,function(){
					$('#col2').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_2b_down":
			{

				var heightVal=parseInt($('#col3 div').html());
				var newVal=heightVal-1;
				$('#col3').animate({height: newVal+"%"},10,function(){
					$('#col3').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_3a_down":
			{

				var heightVal=parseInt($('#col4 div').html());
				var newVal=heightVal-1;
				$('#col4').animate({height: newVal+"%"},10,function(){
					$('#col4').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_3b_down":
			{

				var heightVal=parseInt($('#col5 div').html());
				var newVal=heightVal-1;
				$('#col5').animate({height: newVal+"%"},10,function(){
					$('#col5').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_4a_down":
			{

				var heightVal=parseInt($('#col6 div').html());
				var newVal=heightVal-1;
				$('#col6').animate({height: newVal+"%"},10,function(){
					$('#col6').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
			case "control_4b_down":
			{

				var heightVal=parseInt($('#col7 div').html());
				var newVal=heightVal-1;
				$('#col7').animate({height: newVal+"%"},10,function(){
					$('#col7').html("<div>"+newVal+"</div>");		
				});


				break;	
			}
		}
	});//click

	$("#figBox").on('click',"#graphConfirm",function(){

		var currentValues=[];
		var myBoolean=false;
		for(var i=0;i<=7;i++)
		{
			currentValues[i]=parseInt($("#col"+i+" div").html());
		}

		for(var j=0;j<=7;j++)
		{
			if(currentValues[j]==whatVal[j])
			{
				myBoolean=true;
			}
			else {
				myBoolean=false;
				break;
			}
		}

		if(myBoolean==true)
		{
			$(".graphController2, .graphController1").hide(0);
			$("#Imgshow").attr('src',$ref+'/images/page2/correct.png').fadeIn(10);
			$("#activity-page-next-btn-enabled").show(0);
		}
		else
		{
			$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
		}
	});

	$("#figBox").on('click','#showTablebtn',function(){
		if(toggleClick==true)
		{
			$(this).val(data.string.p2_59_hide);
			$(".allTableFor4").show(0);
			$(".allTableFor5").hide(0);
			toggleClick=false;
		}
		else
		{
			$(this).val(data.string.p2_59_show);
			$(".allTableFor4").hide(0);
			$(".allTableFor5").show(0);
			toggleClick=true;
		}
			

	});


});

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={firstText:data.string.p2_1,
	 			img1:$ref+"/images/page1/graph.png",
	 			firstText_1:"firstText_1"
	 		};
	 		break;
	 	}	
	 	case 2:
	 	{
	 		datas={firstText:data.string.p2_3,
	 			firstText_1:"firstText_2"	 			
	 			
	 		};
	 		break;
	 	}	
	 	case 3:
	 	{
	 		datas={firstText:data.string.p2_4,
	 			firstText_1:"firstText_3",
	 			firstText2:data.string.p2_4_1,
	 			firstText_12:"firstText_12",
	 			mytable:"yes",	 			
	 			tabtxt1:data.string.p2_4_2,
	 			tabtxt2:data.string.p2_4_3,
	 			tabtxt3:data.string.p2_4_4,
	 			tabtxt4:data.string.p2_4_5,
	 			tabtxt5:data.string.p2_4_6,
	 			tabtxt6:data.string.p2_4_7,
	 			tabtxt7:data.string.p2_4_8,
	 			tabtxt8:data.string.p2_4_9,
	 			confirm:data.string.confrim,
	 			fillIn:data.string.fillIn
	 		};
	 		break;
	 		
	 	}	
	 	case 4:
	 	{

	 		datas={firstText:data.string.p2_5,
	 			firstText_1:"firstText_2"	 			
	 			
	 		};
	 		break;
	 	}
	 	case 5:
	 	{
	 		datas={
	 			mytable:"yes",
	 			tabtxt1:data.string.p2_4_2,
	 			tabtxt2:data.string.p2_4_3,
	 			tabtxt3:data.string.p2_4_4,
	 			tabtxt4:data.string.p2_4_5,
	 			tabtxt1a:data.string.p2_4_6,
	 			tabtxt2a:data.string.p2_4_7,
	 			tabtxt3a:data.string.p2_4_8,
	 			tabtxt4a:data.string.p2_4_9,
	 			tabtxt5:whatVal[0],
	 			tabtxt6:whatVal[1],
	 			tabtxt7:whatVal[2],
	 			tabtxt8:whatVal[3],
	 			tabtxt9:whatVal[4],
	 			tabtxt10:whatVal[5],
	 			tabtxt11:whatVal[6],
	 			tabtxt12:whatVal[7],
	 			tabtxt1b:whatVal[0]+whatVal[1],
	 			tabtxt2b:whatVal[2]+whatVal[3],
	 			tabtxt3b:whatVal[4]+whatVal[5],
	 			tabtxt4b:whatVal[6]+whatVal[7],
	 			myGraph:"yes",
	 			secText_1:"sectext_1",
	 			secText:data.string.p2_6,
	 			lab1Text1:data.string.p2_4_6,
	 			lab1Text2:data.string.p2_4_7,
	 			lab1Text3:data.string.p2_4_8,
	 			lab1Text4:data.string.p2_4_9,
	 			lab2Text1:data.string.p2_4_3,
	 			lab2Text2:data.string.p2_4_4,
	 			confirm:data.string.confrim,
	 			imgShows:$ref+"/images/page2/correct.png"

	 		};
	 		break;
	 		
	 	}
	 	case 6:
	 	{
	 		datas={
	 			trdfirstText_1:"trdfirstText_1",
	 			trdfirstText:data.string.p2_8,
	 			mytable:"yes",
	 			tabtxt1:data.string.p2_10,
	 			tabtxt2:data.string.p2_11,
	 			tabtxt3:data.string.p2_12,
	 			tabtxt4:data.string.p2_13,
	 			tabtxt5:data.string.p2_14,
	 			tabtxt6:data.string.p2_15,
	 			tabtxt7:data.string.p2_16,
	 			tabtxt8:data.string.p2_17,
	 			tabtxt9:data.string.p2_18,
	 			tabtxt10:data.string.p2_19,
	 			trdfirstText_2:data.string.p2_9,
	 			halftabme:"halftabme"
	 		};
	 		break;
	 	}
	 	case 7:
	 	{
	 		datas={
	 			trdfirstText_1:"trdfirstText_1",
	 			trdfirstText:data.string.p2_8,
	 			mytable:"yes",
	 			tabtxt1:data.string.p2_10,
	 			tabtxt2:data.string.p2_11,
	 			tabtxt3:data.string.p2_12,
	 			tabtxt4:data.string.p2_13,
	 			tabtxt5:data.string.p2_14,
	 			tabtxt6:data.string.p2_15,
	 			tabtxt7:data.string.p2_16,
	 			tabtxt8:data.string.p2_17,
	 			tabtxt9:data.string.p2_18,
	 			tabtxt10:data.string.p2_19,
	 			trdfirstText_2:data.string.p2_9,
	 			forthfirstText_2:data.string.p2_20,
	 			mytable2:"yes",
	 			tabtxt11:data.string.p2_21,
	 			tabtxt12:data.string.p2_22,
	 			tabtxt13:data.string.p2_23,
	 			tabtxt14:data.string.p2_24,
	 			tabtxt15:data.string.p2_25,
	 			tabtxt16:data.string.p2_26,
	 			tabtxt17:data.string.p2_27,
	 			tabtxt18:data.string.p2_28,
	 			tabtxt19:data.string.p2_29,
	 			tabtxt20:data.string.p2_30,
	 			tabtxt21:data.string.p2_31,
	 			tabtxt22:data.string.p2_32,
	 			tabtxt23:data.string.p2_33,
	 			tabtxt24:data.string.p2_34,
	 			halftabme:"halftabme"
	 		};
	 		break;
	 	}
	 	case 8:
	 	{
	 		datas={firstText:data.string.p2_35,
	 			img1:$ref+"/images/page1/graph.png",
	 			firstText_1:"firstText_1"
	 		};
	 		break;
	 	}	

	 	case 9:
	 	{
	 		datas={forthfirstText:data.string.p2_36,
	 			forthfirstText_1:"forthfirstText_1",
	 			tabtxt1:data.string.p2_37,
	 			tabtxt2:data.string.p2_38,
	 			tabtxt3:data.string.p2_39,
	 			tabtxt4:data.string.p2_40,
	 			tabtxt5:data.string.p2_41,
	 			tabtxt6:data.string.p2_42,
	 			tabtxt7:data.string.p2_43,
	 			tabtxt8:data.string.p2_44,
	 			tabtxt9:data.string.p2_45,
	 			tabtxt10:data.string.p2_46,
	 			tabtxt11:data.string.p2_47,
	 			tabtxt12:data.string.p2_48,
	 			tabtxt13:data.string.p2_49,
	 			tabtxt14:data.string.p2_50,
	 			tabtxt15:data.string.p2_51,
	 			tabtxt16:data.string.p2_52,
	 			tabtxt17:data.string.p2_53,
	 			tabtxt18:data.string.p2_54,
	 			tabtxt19:data.string.p2_55,
	 			tabtxt20:data.string.p2_56,
	 			tabtxt21:data.string.p2_57,
	 			tabtxt22:data.string.p2_58
	 		};
	 		break;
	 	}	
	 	case 10:
	 	{
	 		datas={forthfirstText:data.string.p2_36,
	 			forthfirstText_1:"forthfirstText_1",
	 			tabtxt1:data.string.p2_37,
	 			tabtxt2:data.string.p2_38,
	 			tabtxt3:data.string.p2_39,
	 			tabtxt4:data.string.p2_40,
	 			tabtxt5:data.string.p2_41,
	 			tabtxt6:data.string.p2_42,
	 			tabtxt7:data.string.p2_43,
	 			tabtxt8:data.string.p2_44,
	 			tabtxt9:data.string.p2_45,
	 			tabtxt10:data.string.p2_46,
	 			tabtxt11:data.string.p2_47,
	 			tabtxt12:data.string.p2_48,
	 			tabtxt13:data.string.p2_49,
	 			tabtxt14:data.string.p2_50,
	 			tabtxt15:data.string.p2_51,
	 			tabtxt16:data.string.p2_52,
	 			tabtxt17:data.string.p2_53,
	 			tabtxt18:data.string.p2_54,
	 			tabtxt19:data.string.p2_55,
	 			tabtxt20:data.string.p2_56,
	 			tabtxt21:data.string.p2_57,
	 			tabtxt22:data.string.p2_58,
	 			showtable:data.string.p2_59_show,
	 			forthfirstText_2:"forthfirstText_2",
	 			forthfirstText2:data.string.p2_60,
	 			linetxt:[data.string.p2_61,data.string.p2_62,data.string.p2_63,data.string.p2_64,data.string.p2_65],
	 			mylist:"yese"
	 		};
	 		break;
	 	}	
	 	case 11:
	 	{
	 		datas={forthfirstText:data.string.p2_36,
	 			forthfirstText_1:"forthfirstText_1",
	 			tabtxt1:data.string.p2_37,
	 			tabtxt2:data.string.p2_38,
	 			tabtxt3:data.string.p2_39,
	 			tabtxt4:data.string.p2_40,
	 			tabtxt5:data.string.p2_41,
	 			tabtxt6:data.string.p2_42,
	 			tabtxt7:data.string.p2_43,
	 			tabtxt8:data.string.p2_44,
	 			tabtxt9:data.string.p2_45,
	 			tabtxt10:data.string.p2_46,
	 			tabtxt11:data.string.p2_47,
	 			tabtxt12:data.string.p2_48,
	 			tabtxt13:data.string.p2_49,
	 			tabtxt14:data.string.p2_50,
	 			tabtxt15:data.string.p2_51,
	 			tabtxt16:data.string.p2_52,
	 			tabtxt17:data.string.p2_53,
	 			tabtxt18:data.string.p2_54,
	 			tabtxt19:data.string.p2_55,
	 			tabtxt20:data.string.p2_56,
	 			tabtxt21:data.string.p2_57,
	 			tabtxt22:data.string.p2_58,
	 			showtable:data.string.p2_59_show,
	 			forthfirstText_2:"forthfirstText_2",
	 			forthfirstText2:data.string.p2_66,	
	 			mylist2:"yese",
	 			tablst1:data.string.p2_67,
	 			tablst2:data.string.p2_68,
	 			tablst3:data.string.p2_69,
	 			tablst4:data.string.p2_70,
	 			tablst5:data.string.p2_71,
	 			tablst6:data.string.p2_72,
	 			tablst7:data.string.p2_73,
	 			tablst8:data.string.p2_74,
	 			tablst9:data.string.p2_75,
	 			tablst10:data.string.p2_76,
	 			tablst11:data.string.p2_77,
	 			tablst12:data.string.p2_78

	 		};
	 		break;
	 	}	
	 	/*case 12:
	 	{
	 		datas={
	 			tabtxt1:data.string.p2_37,
	 			tabtxt2:data.string.p2_38,
	 			tabtxt3:data.string.p2_39,
	 			tabtxt4:data.string.p2_40,
	 			tabtxt5:data.string.p2_41,
	 			tabtxt6:data.string.p2_42,
	 			tabtxt7:data.string.p2_43,
	 			tabtxt8:data.string.p2_44,
	 			tabtxt9:data.string.p2_45,
	 			tabtxt10:data.string.p2_46,
	 			tabtxt11:data.string.p2_47,
	 			tabtxt12:data.string.p2_48,
	 			tabtxt13:data.string.p2_49,
	 			tabtxt14:data.string.p2_50,
	 			tabtxt15:data.string.p2_51,
	 			tabtxt16:data.string.p2_52,
	 			tabtxt17:data.string.p2_53,
	 			tabtxt18:data.string.p2_54,
	 			tabtxt19:data.string.p2_55,
	 			tabtxt20:data.string.p2_56,
	 			tabtxt21:data.string.p2_57,
	 			tabtxt22:data.string.p2_58,
	 			finaltext:data.string.p2_79
	 		};
	 		break;*/
	 	//}	



	}


	return datas;
}

function getHtml(cnt)
{
	
	var datas=getdata(cnt);


	var source;
	loadTimelineProgress(11,cnt);

	source=$("#template-1").html();
	if(cnt==5)
		source=$("#template-2").html();
	else if(cnt==6 || cnt==7)
		source=$("#template-3").html();
	else if(cnt>=9)
		source=$("#template-4").html();
	

	var template=Handlebars.compile(source);
	var html=template(datas);


	if(cnt==8)$(".headTitle").html(data.string.p2_35);
	$("#figBox").fadeOut(10,function(){ $(this).html(html); 
	}).delay(10).fadeIn(10,function(){
		if(cnt==1 || cnt==8) $(".headTitle").hide(0);
		else 
		{
			$(".headTitle").show(0);
			
			
		}
		
		getAnime(cnt);
	});
	


}
function getAnime(cnt)
{
	

	switch(cnt)
	{
		case 1:
		case 2:
		case 4:
		case 6:
		case 7:
		case 8:
		case 9:
		{

	 		$("#activity-page-next-btn-enabled").show(0);
	 		break;
	 	}
	 	case 5:
	 	{
	 		animategraph();
	 		break;
	 	}
	 	case 10:
	 	case 11:
	 	{
	 		animateShowHide(cnt);
	 		break;
	 	}
	 	//case 12:
	 	//{
	 		//ole.footerNotificationHandler.pageEndSetNotification();
	 		//break;
	 	//}

	 	
	}
}

function animategraph()
{
	$('.column').css('height','0');
	
	
	$('#col0').css({height: "40%"});
	$('#col0').html("<div>40</div>");		
	$('#col1').css({height: "40%"});
	$('#col1').html("<div>40</div>");	
	$('#col2').css({height: "40%"});
	$('#col2').html("<div>40</div>");	
	$('#col3').css({height: "40%"});
	$('#col3').html("<div>40</div>");	
	$('#col4').css({height: "40%"});
	$('#col4').html("<div>40</div>");	
	$('#col5').css({height: "40%"});
	$('#col5').html("<div>40</div>");	
	$('#col6').css({height: "40%"});
	$('#col6').html("<div>40</div>");	
	$('#col7').css({height: "40%"});
	$('#col7').html("<div>40</div>");	
	$('#col8').css({height: "40%"});
	$('#col8').html("<div>40</div>");	
}

function animateShowHide(cnt)
{
toggleClick=true;
	$(".allTableFor4").hide(0);
	if (cnt == 10){
		$("#activity-page-next-btn-enabled").show(0);
	}else{
		ole.footerNotificationHandler.pageEndSetNotification();
	}
}


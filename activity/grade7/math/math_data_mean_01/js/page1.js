
var newWidth;
var whatIL, whatIB, whatIH;
$(function(){


	var $board = $('#myboard'),
		$canvas=$("canvas");

	$(".headTitle").html(data.string.p1_1);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	$("#repeatBtn").html(getReloadBtn());

	var counter=1;





	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){

		$(this).hide(0);

		counter++;

		counter==1?$("#activity-page-next-btn-enabled").show():'';
		if(counter<=5)
			getHtml(counter);
		else if(counter>5 && counter<=12)
		{
			changeGraphAnimation(counter);
		}
		else if(counter>12 && counter<=14) getHtml(counter);
		else changeGraphAnimation(counter);


	});

	$("#repeatBtn").click(function(){
		location.reload();
	});



});

function getdata(cnt)
{
	var datas;
	cnt==1?$("#activity-page-next-btn-enabled").show():'';
	cnt>1?$(".cover_image").hide(0):'';

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={
				firstText:data.lesson.chapter,
				firstText_1:"cover_text"
	 		};
			$(".headTitle").hide(0);
	 		break;
	 	}
		case 2:
	 	{
	 		datas={firstText:data.string.p1_1,
	 			img1:$ref+"/images/page1/graph.png",
	 			firstText_1:"firstText_1"
	 		};
	 		break;
	 	}
	 	case 3:
	 	{
	 		datas={firstText:data.string.p1_2,
	 			firstText_1:"firstText_2",
	 			mytable:"yes",
	 			tabtxt1:data.string.p1m1t2,
	 			tabtxt2:data.string.p1m1t1,
	 			tabtxt3:data.string.p1m1t3,
	 			tabtxt4:data.string.p1m1t4,
	 			tabtxt5:data.string.p1m1t5,
	 			tabtxt6:data.string.p1m1t6,
	 			tabtxt7:data.string.p1m1t7,
	 			tabtxt8:data.string.p1m1t8,
	 			tabtxt9:data.string.p1m1t9,
	 			tabtxt10:data.string.p1m1t10,
	 			tabtxt11:data.string.p1m1t11,
	 			tabtxt12:data.string.p1m1t12,
	 			tabtxt13:data.string.p1m1t13,
	 			tabtxt14:data.string.p1m1t14,
	 			tabtxt15:data.string.p1m1t15,
	 			tabtxt16:data.string.p1m1t16,
	 			tabtxt17:data.string.p1m1t17
	 		};
	 		break;
	 	}

	 	case 4:
	 	{
	 		datas={secText:data.string.p1_3,
	 			secText_1:"secText_1",
	 			mytable:"yes",
	 			tabtxt1:data.string.p1m1t2,
	 			tabtxt2:data.string.p1m1t1,
	 			tabtxt3:data.string.p1m1t3,
	 			tabtxt4:data.string.p1m1t4,
	 			tabtxt5:data.string.p1m1t5,
	 			tabtxt6:data.string.p1m1t6,
	 			tabtxt7:data.string.p1m1t7,
	 			tabtxt8:data.string.p1m1t8,
	 			tabtxt9:data.string.p1m1t9,
	 			tabtxt10:data.string.p1m1t10,
	 			tabtxt11:data.string.p1m1t11,
	 			tabtxt12:data.string.p1m1t12,
	 			tabtxt13:data.string.p1m1t13,
	 			tabtxt14:data.string.p1m1t14,
	 			tabtxt15:data.string.p1m1t15,
	 			tabtxt16:data.string.p1m1t16,
	 			tabtxt17:data.string.p1m1t17
	 		};
	 		break;

	 	}

	 	case 5:
	 	{
	 		datas={
	 			mytable:"yes",
	 			tabtxt1:data.string.p1m1t2,
	 			tabtxt2:data.string.p1m1t1,
	 			tabtxt3:data.string.p1m1t3,
	 			tabtxt4:data.string.p1m1t4,
	 			tabtxt5:data.string.p1m1t5,
	 			tabtxt6:data.string.p1m1t6,
	 			tabtxt7:data.string.p1m1t7,
	 			tabtxt8:data.string.p1m1t8,
	 			tabtxt9:data.string.p1m1t9,
	 			tabtxt10:data.string.p1m1t10,
	 			tabtxt11:data.string.p1m1t11,
	 			tabtxt12:data.string.p1m1t12,
	 			tabtxt13:data.string.p1m1t13,
	 			tabtxt14:data.string.p1m1t14,
	 			tabtxt15:data.string.p1m1t15,
	 			tabtxt16:data.string.p1m1t16,
	 			tabtxt17:data.string.p1m1t17,
	 			myGraph:"yes",
	 			trdText:data.string.p1_3,
	 			trdText_1:"trdText_1",
	 			frthText_1:"frthText_1",
	 			frthText:data.string.p1_4,
	 			sixthText_1:"sixthText_1",
	 			sixthText:data.string.p1_5,
	 			lab1Text1:data.string.p1m1t6,
	 			lab1Text2:data.string.p1m1t10,
	 			lab1Text3:data.string.p1m1t14,
	 			lab2Text1:data.string.p1m1t3,
	 			lab2Text2:data.string.p1m1t4,
	 			lab2Text3:data.string.p1m1t5,


	 		};
	 		break;

	 	}
	 	case 13:
	 	{
	 		datas={secText:data.string.p1_14,
	 			secText_1:"secText_12"
	 		};
	 		break;

	 	}
	 	case 14:
	 	{
	 		datas={
	 			tabVal:[data.string.p1_16,
		 			data.string.planet1,
		 			data.string.planet2,
		 			data.string.planet3,
		 			data.string.planet4,
		 			data.string.planet5,
		 			data.string.planet6,
		 			data.string.planet7,

		 			data.string.planet9],

	 			tabVal2:[data.string.p1_15,
		 			data.string.planetval1,
		 			data.string.planetval2,
		 			data.string.planetval3,
		 			data.string.planetval4,
		 			data.string.planetval5,
		 			data.string.planetval6,
		 			data.string.planetval7,

		 			data.string.planetval9],
	 			lab1Text1:data.string.planet1,
	 			lab1Text2:data.string.planet2,
	 			lab1Text3:data.string.planet3,
	 			lab1Text4:data.string.planet4,
	 			lab1Text5:data.string.planet5,
	 			lab1Text6:data.string.planet6,
	 			lab1Text7:data.string.planet7,

	 			lab1Text9:data.string.planet9,
	 			lab2Text1:data.string.p1_20,
	 			lab2Text2:data.string.p1_21,
	 			firstText_2:"firstText_2",
	 			firstText:data.string.p1_22


	 		};
	 		break;
	 	}



	}


	return datas;
}

function getHtml(cnt)
{

	var datas=getdata(cnt);


	var source;
	loadTimelineProgress(15,cnt);

	source=$("#template-1").html();
	if(cnt>13)
		source=$("#template-2").html();


	var template=Handlebars.compile(source);
	var html=template(datas);



	$("#figBox").fadeOut(10,function(){ $(this).html(html);
	}).delay(10).fadeIn(10,function(){
		if(cnt==2){
			$(".headTitle").hide(0);
		}
		else
		{
			$(".headTitle").show(0);


		}

		getAnime(cnt);
	});



}
function getAnime(cnt)
{


	switch(cnt)
	{
		case 2:
		case 3:
		case 4:
		case 13:
	 	{

	 		$("#activity-page-next-btn-enabled").show(0);
	 		break;
	 	}
	 	case 5:
	 	{
			animateGraph();

	 		break;
	 	}
	 	case 14:
	 	{

	 		$(".headTitle").html(data.string.p1_19);
	 		animate13();
	 		break;
	 	}
	}
}
function animateGraph()
{

	$(".tabme_1").addClass('graphTable');
	$('.column').css('height','0');
	$('.riga, .labelGraph, .labelGraph2').hide(0);
	$("#activity-page-next-btn-enabled").show(0);
	/**/
}

function changeGraphAnimation(counter)
{
	loadTimelineProgress(15,counter);
	switch(counter)
	{
		case 6:
		{
			animate5();
			break;
		}
		case 7:
		{
			animate6();
			break;
		}
		case 8:
		{
			animate7();
			break;
		}
		case 9:
		{
			animate8();
			break;
		}
		case 10:
		{
			animate9();
			break;
		}
		case 11:
		{
			animate10();
			break;
		}
		case 12:
		{
			animate11();
			break;
		}
		case 15:
		{
			animateLast();
			break;
		}
	}
}

function animate5()
{

	$(".sixthText_1").css({'opacity':1});
	$("#frthText_1").html(data.string.p1_6);
	$("#activity-page-next-btn-enabled").show(0);
}

function animate6()
{


	$("#frthText_1").html(data.string.p1_7);
	$('.labelGraph').show(0);
	$("#activity-page-next-btn-enabled").show(0);
}

function animate7()
{


	$("#frthText_1").html(data.string.p1_8);
	$('.riga').show(0);
	$("#activity-page-next-btn-enabled").show(0);
}


function animate8()
{


	$("#frthText_1").html(data.string.p1_9);

	$("#activity-page-next-btn-enabled").show(0);
}

function animate9()
{


	$("#frthText_1").html(data.string.p1_10);
	$('.labelGraph2').show(0);

	var myArray=[32,36,36, 40, 33,37, 56,65,67];

	$(".firstrow").addClass('borderRed');
	$(".firstrow .tg-031e:nth-child(2)").addClass('backPulsate');
	$('#col'+0).animate({height: myArray[0]+"%"}, 1500,function(){

		$(this).html("<div>"+myArray[0]+"</div>");
		$(".firstrow .tg-031e:nth-child(2)").removeClass('backPulsate');
		$(".firstrow .tg-031e:nth-child(3)").addClass('backPulsate');


		$('#col1').delay(100).animate({height: myArray[1]+"%"}, 1500,function(){

			$(this).html("<div>"+myArray[1]+"</div>");
			$(".firstrow .tg-031e:nth-child(3)").removeClass('backPulsate');
			$(".firstrow .tg-031e:nth-child(4)").addClass('backPulsate');


			$('#col2').delay(100).animate({height: myArray[2]+"%"}, 1500,function(){

				$(this).html("<div>"+myArray[2]+"</div>");

				$(".firstrow").removeClass('borderRed');
				$(".firstrow .tg-031e:nth-child(4)").removeClass('backPulsate');
				$(".secondrow").addClass('borderRed');
				$(".secondrow .tg-031e:nth-child(2)").addClass('backPulsate');



				$('#col3').delay(100).animate({height: myArray[3]+"%"}, 1500,function(){

					$(this).html("<div>"+myArray[3]+"</div>");
					$(".secondrow .tg-031e:nth-child(2)").removeClass('backPulsate');
					$(".secondrow .tg-031e:nth-child(3)").addClass('backPulsate');


					$('#col4').delay(100).animate({height: myArray[4]+"%"}, 1500,function(){

						$(this).html("<div>"+myArray[4]+"</div>");
						$(".secondrow .tg-031e:nth-child(3)").removeClass('backPulsate');
						$(".secondrow .tg-031e:nth-child(4)").addClass('backPulsate');

						$('#col5').delay(100).animate({height: myArray[5]+"%"}, 1500,function(){

							$(this).html("<div>"+myArray[5]+"</div>");
							$(".secondrow").removeClass('borderRed');
							$(".secondrow .tg-031e:nth-child(4)").removeClass('backPulsate');
							$(".thirdrow").addClass('borderRed');
							$(".thirdrow .tg-031e:nth-child(2)").addClass('backPulsate');



							$('#col6').delay(100).animate({height: myArray[6]+"%"}, 1500,function(){

								$(this).html("<div>"+myArray[6]+"</div>");
								$(".thirdrow .tg-031e:nth-child(2)").removeClass('backPulsate');
								$(".thirdrow .tg-031e:nth-child(3)").addClass('backPulsate');

								$('#col7').delay(100).animate({height: myArray[7]+"%"}, 1500,function(){

									$(this).html("<div>"+myArray[7]+"</div>");
									$(".thirdrow .tg-031e:nth-child(3)").removeClass('backPulsate');
									$(".thirdrow .tg-031e:nth-child(4)").addClass('backPulsate');

									$('#col8').delay(100).animate({height: myArray[8]+"%"}, 1500,function(){

										$(this).html("<div>"+myArray[8]+"</div>");
										$(".thirdrow").removeClass('borderRed');
										$(".thirdrow .tg-031e:nth-child(4)").removeClass('backPulsate');

										$("#activity-page-next-btn-enabled").show(0);

									});

								});

							});



						});

					});

				});

			});

		});

	});
}

function animate10()
{


	$("#frthText_1").html(data.string.p1_12);
	$("#activity-page-next-btn-enabled").show(0);
}

function animate11()
{


	$("#frthText_1").html(data.string.p1_13);
	$("#activity-page-next-btn-enabled").show(0);
}


function animate13()
{


	$("#frthText_1").html(data.string.p1_10);
	$('.labelGraph2').show(0);
	$('.column2').css('height','0');


	$('#pla1').css({height: "28%"});
	$('#pla3').css({height: "28%"});
	$('#pla5').css({height: "28%"});
	$('#pla7').css({height: "28%"});
	$('#pla9').css({height: "28%"});
	$('#pla11').css({height: "28%"});
	$('#pla13').css({height: "28%"});
	$('#pla15').css({height: "28%"});
	$('#pla17').css({height: "28%"});
	for(var j=1, k=2; k<=18; k=k+2, j++)
	{
		var flt=parseFloat(data.string['planetval'+j]);
		var percent=28*flt;
		$('#pla'+k).css({height: percent+"%"});
	}


	$(".allGraph2").delay(1000).fadeIn(600,function(){
		$("#activity-page-next-btn-enabled").show(0);
	});

}


function animateLast()
{
	$(".firstText_2").fadeIn(600,function(){
		ole.footerNotificationHandler.pageEndSetNotification();


	});
}

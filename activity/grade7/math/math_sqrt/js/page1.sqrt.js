(function ($) {
	var $value = 1;
	var $length = 0;
	var $area = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countclickNext = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countclickNext+1);
	$nextBtn.removeClass('glyphicon glyphicon-circle-arrow-right').html(getArrowBtn());

	$('.title').text(data.string.p1_title).addClass('animated bounceIn');

	// coverpage
	function cover_page_func(){
		var source = $("#cover-template").html()
		var template = Handlebars.compile(source);
		var content = {
			cover_src: $ref+"/images/coverpage.png",
			text_to_show: data.lesson.chapter,
		}
		var html = template(content);
		$board.html(html);
	}
	cover_page_func();
	countclickNext==0?$nextBtn.show(0):'';
/*
* first
*/
	function first () {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		var content = {
			img1 : $ref+"/images/01.png",
			text1 : 1,
			text2 : 2,
			text3 : 3,
			text4 : 4,
			text5 : 5,
			text6 : 6,
			text7 : 7,
			text8 : 8,
			desc : data.string.p1_01
		}
		var html = template(content);
		$board.html(html);
	}
	// first();

/*
* second work
*/
	$board.on('click','.first li',function () {
		$value =  $(this).data('value');
		// console.log($value);
		$img = $ref + "/images/0"+$value+".gif";
		$length = $value;

		$area = $length*$length;
		// console.log($length+" area"+$area);

		$board.find('ul, .desc').fadeOut(function () {
			$board.find('.bird').width('25%');
			$board.find('.bird img').attr('src',$img);
			setTimeout(function () {
				$nextBtn.fadeIn();
			},parseInt($value)*2000);
		});
	})

/*
* third function
*/
	function third () {
		var source = $('#third-template').html();
		console.log($value);
		var template = Handlebars.compile(source);
		var content = {
			img1 : $ref+"/images/gif_"+$value+".gif",
			text1 : data.string.p1_3_1 + " "+$length +"cm "+data.string.p1_3_2,
			length : data.string.length,
			text3 : data.string.p1_3_3,
			text2 : data.string.p1_3_hint,
			text4 : data.string.p1_3_4,
			text5 : data.string.p1_3_5,
			text6 : data.string.p1_3_6,
			text7 : $area,
			text8 : $area,
			text9 : $length,
			text10 : $area,
			text11 : data.string.p1_3_7,
			text12 : $length,
			text13 : data.string.p1_3_8,
			feri : data.string.again,
			wrong : data.string.wrong
		}

		var html = template(content);
		console.log(html);

		$board.find('.bird').animate({
			'width' : '40%',
			'margin-left' : '5%',
			'margin-top' : '10%',
		},1000,function () {
			$board.html(html);
			$board.find('.span1').fadeIn(function () {
				$board.find('.span2').fadeIn();

			});
		})
	}

	$board.on('click','.inBtn2',function () {
		$input = parseInt($board.find('.span2 input').val());
		console.log($input);
		if($input === $area){
			$board.find('.span3').fadeIn();
			$board.find('.inBtn2').fadeOut();
			$board.find('.feri').delay(1000).fadeIn();
					ole.footerNotificationHandler.pageEndSetNotification();

		}
		else {
			$board.find('.wrong').fadeIn(function () {
				$(this).delay(4000).fadeOut();
			})
		}
	})

	$nextBtn.on('click',function () {
		$(this).hide(0);
		switch (countclickNext) {
			case 0:
				first ();
			break;
			case 1:
			third();
			break;
		}

		countclickNext++;
		loadTimelineProgress($total_page,countclickNext+1);
	})

	$board.on('click','.feri',function () {
		location.reload();
	})
})(jQuery);

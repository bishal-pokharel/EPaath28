(function  ($) {

	var $title = $('.title');
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext =0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.removeClass('glyphicon glyphicon-circle-arrow-right').html(getArrowBtn());

	$title.text(data.string.p3_title).addClass('animated bounceInLeft');
	$title.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
		first();
		// second();
	});

	function first () {
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = {
			text1 : data.string.p3_1,
			text2 : data.string.p3_2,
			img1 : $ref+'/images/arrow.png',

		}

		var html = template(content);
		
		$board.html(html);

		$board.find('.div1').show(0).addClass('animated bounceInLeft');

	}

	$board.on('click','.next1',function () {
		$board.find('.div2').show(0).addClass('animated bounceInRight');
		$(this).hide(0);
	})

	$board.on('click','.next2',function () {
		$board.find('.pic').show(0).addClass('animated bounceInDown');
		$(this).hide(0);
		$nextBtn.delay(1000).fadeIn();
	})

	function first_transition () {
		$board.find('.text1').addClass('animated bounceOutLeft');
		$board.find('.text2').addClass('animated bounceOutRight');
		$board.find('.pic').addClass('animated bounceOutUp');
		// $board.find('.second').addClass('animated bounceOutDown');
		second();
	}

	function second () {
		console.log('second called');
		var source = $('#second-template').html();
		var template = Handlebars.compile(source);
		var a = [];
		for (var i = 0; i < 10; i++) {
			j=1+i;
			a[i] = {
				number : j,
				square : j*j
			}
		};
		var content = {
			text1 : data.string.p3_5,
			img1 : $ref+"/images/img01.png",
			img2 : $ref+"/images/img02.png",
			small1 : data.string.p3_3,
			small2 : data.string.p3_4,
			fillTd : a,
			sqrNoun1 : data.string.p3_3,
			sqrNoun2 : data.string.p3_4,
		}
		var html = template(content);
		console.log(html)
		setTimeout(function () {
			$board.html(html);
			third ();
			$board.find('.second').hide(0);
		},1000);
	}

	function third () {
		var source = $('#third-template').html();
		var template = Handlebars.compile(source);
		var no1 = Math.floor((Math.random()*9)+1);

		var content = {
			textClick : data.string.redBtn,
			text1 : no1,
			text2 : data.string.square,
			text3 : no1*no1,
			text22 : data.string.square_root
		}
		var html = template(content);
		console.log(html);
		$board.find('.secondT').html(html);
		$board.find('.num, .repeat').hide(0);
		$board.find('.step1 .num1, .step1 .num2').fadeIn();
	}

	$board.on('click','.step1 .num2',function () {
		$board.find('.step1 .num3').fadeIn(function () {
			$board.find('.step2 .num1, .step2 .num2').delay(800).fadeIn();
		});
	})

	$board.on('click','.step2 .num2',function () {
		$board.find('.clickInfo').addClass('animated hinge');
		$board.find('.step2 .num3').fadeIn(function () {
			$board.find('.second').show(0).addClass('animated tada').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
				$board.find('.repeat').show(0).addClass('animated bounceInDown');
				ole.footerNotificationHandler.pageEndSetNotification();
			})
		});
	})

	$board.on('click','.repeat',function () {
		third();
		$board.find('.second').removeClass('animated tada')

	})

	$nextBtn.on('click',function () {
		$(this).fadeOut();
		switch (countNext) {
			case 0:
				first_transition();
			break;
		}
		countNext++;
		loadTimelineProgress($total_page,countNext+1);
	})

})(jQuery);
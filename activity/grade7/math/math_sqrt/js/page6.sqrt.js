(function ($) {

	var $board = $('.board');
	var $title = $('.title');
	var $popUp = $('.popUp');
	var showsteps = "";

	var clicked = {
		add : 0,
		subtract : 0,
		multiply : 0,
		divide : 0
	};

	var countNext =0;

	var $total_page = 1;
	loadTimelineProgress($total_page,countNext+1);

	function first () {
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = {
			textZ : data.string.p6_1_1 ,
			text1 : data.string.p6_add ,
			text2 : data.string.p6_subtract ,
			text3 : data.string.p6_multiply ,
			text4 : data.string.p6_divide
		}
		var html = template(content);
		$board.html(html);
	}
	first();

	$board.on('click','.obj',function () {
		var $val1 = $(this).data('value');

		switch ($val1) {
			case 1:
			clicked.add=1;
			 var content = {
			 	text1 : data.string.p6_add,
			 	math1 : '9+16',
			 	math2 : '25',
			 	math3 : '5',
			 	emath1 : "√<span class='mathUpperLine'>9</span>+√<span class='mathUpperLine'>16</span>",
			 	emath2 : "√<span class='mathUpperLine'>3<sup>2<sup></span>+√<span class='mathUpperLine'>4<sup>2<sup></span>",
			 	emath3 : "3+4",
			 	emath4 : "7",
			 	text2 : "√<span class='mathUpperLine'>9+16</span>"+" <span class='notEq2'>=</span> "+"√<span class='mathUpperLine'>9</span>+√<span class='mathUpperLine'>16</span>",
			 	text3 : data.string.p6_add_guf
			 }
			break;

			case 2:
			clicked.subtract = 1;
			var content = {
			 	text1 : data.string.p6_subtract,
			 	math1 : '25-16',
			 	math2 : '9',
			 	math3 : '3',
			 	emath1 : "√<span class='mathUpperLine'>25</span>-√<span class='mathUpperLine'>16</span>",
			 	emath2 : "√<span class='mathUpperLine'>5<sup>2<sup></span>-√<span class='mathUpperLine'>4<sup>2<sup></span>",
			 	emath3 : "5-4",
			 	emath4 : "1",
			 	text2 : "√<span class='mathUpperLine'>25-16</span>"+" <span class='notEq2'>=</span> "+"√<span class='mathUpperLine'>25</span>-√<span class='mathUpperLine'>16</span>",
			 	text3 : data.string.p6_add_guf
			}
			break;


			case 3:
			clicked.multiply = 1;
			var content = {
			 	text1 : data.string.p6_multiply,
			 	math1 : '25x16',
			 	math2 : '400',
			 	math3 : '20',
			 	emath1 : "√<span class='mathUpperLine'>25</span>x√<span class='mathUpperLine'>16</span>",
			 	emath2 : "√<span class='mathUpperLine'>5<sup>2<sup></span>x√<span class='mathUpperLine'>4<sup>2<sup></span>",
			 	emath3 : "5x4",
			 	emath4 : "20",
			 	text2 : "√<span class='mathUpperLine'>25x16</span>"+" <span class='Eq2'>=</span> "+"√<span class='mathUpperLine'>25</span>x√<span class='mathUpperLine'>16</span>",
			 	text3 : data.string.p6_divide_guf
			}
			break;

			case 4:
			clicked.divide = 1;
			var content = {
			 	text1 : data.string.p6_divide,
			 	math1 : '625',
			 	math2 : '25',
			 	math3 : '5',
			 	emath1 : "√<span class='mathUpperLine'>625</span>/√<span class='mathUpperLine'>25</span>",
			 	emath2 : "√<span class='mathUpperLine'>25<sup>2<sup></span>/√<span class='mathUpperLine'>5<sup>2<sup></span>",
			 	emath3 : "25/5",
			 	emath4 : "5",
			 	text2 : "√<span class='mathUpperLine'>625/25</span>"+" <span class='Eq2'>=</span> "+"√<span class='mathUpperLine'>625</span>/√<span class='mathUpperLine'>25</span>",
			 	text3 : data.string.p6_divide_guf
			}
			break;
		}

	if($val1===4){
		// console.log($popUp);
		var source = $('#firstPopup2-template').html();
		var template = Handlebars.compile(source);
		var html = template(content);
		console.log(html);
		$popUp.html(html).show(0);

		ole.vCenter('.popUp .popUpDivide');
	} else {
		var source = $('#firstPopup-template').html();
		var template = Handlebars.compile(source);
		var html = template(content);
		$popUp.html(html).show(0);

		ole.vCenter('.popUp .popUpFirst');
		$popUp.find('.calcs, .conclusionMath, .conclusion').hide(0);

		var countFlow = 0;
		var typo = countFlow+1;
		$popUp.find('.cal1 .type'+typo).show(0).addClass('animated bounceInLeft');
		countFlow ++;
		showsteps = setInterval(function () {

			if(countFlow < 4){
				var typo = countFlow+1;
				$popUp.find('.cal1 .type'+typo).show(0).addClass('animated bounceInLeft');
			}
			else if (countFlow < 8) {
				var typo = countFlow-3;
				$popUp.find('.cal2 .type'+typo).show(0).addClass('animated bounceInRight');

			}
			else if (countFlow <9) {

				$popUp.find('.conclusionMath').show(0).addClass('animated bounceInUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
					$popUp.find('.conclusion').show(0).addClass('animated bounceInDown');
				});
			}
			else {
				clearInterval(showsteps);
			}
			countFlow++;
		},1000);

	}


});

	 $popUp.on('click','.clsBtn',function () {
 		$popUp.hide(0);
 		clearInterval(showsteps);

 		if (clicked.subtract===1 && clicked.add===1 && clicked.multiply===1 && clicked.divide===1) {
 			ole.footerNotificationHandler.pageEndSetNotification();
 		};
 	})

})(jQuery);

(function () {

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var countQuestion = 0,
		countCorrect = 0,
		countDisplayWrong = 0,
		$title = $('.title'),
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countDivide = 0,
		countQuestion= 0;

	$title.text(data.string.exercise2Title);

	var content = {
		divide : {
			divide : true,
			firstNum : 1,
			secondNum : 1,
			ans : 1,
		},
		multiply : {

		}

	}


	function genAns (num) {
		loadTimelineProgress(11,countQuestion+1);
		if(num > 2) {
			var newRand = ole.getRandom(2,num-1,1);
			var firstCond = ole.getRandom(1,1)[0];
			var secondCond = ole.getRandom(1,1)[0];
			var randomAns = ole.getRandom(3,2);
			var allAns = [];
			for (var i = 0; i < randomAns.length; i++) {
				if(randomAns[i]===0) {
					if(firstCond===0){
						allAns.push({
							ans : num-newRand[0]
						});
					} else {
						allAns.push({
							ans : num+newRand[0]
						});
					}
				} else if (randomAns[i]===1) {
					allAns.push({
						ans : num,
						correct : "correct"
					});
				} else {
					if(secondCond===0){
						allAns.push({
							ans : num-newRand[1]
						});
					} else {
						allAns.push({
							ans : num+newRand[1]
						});
					}
				}
			};

		} else {
			var randomAns = ole.getRandom(3,2);
			var allAns = [];
			for (var i = 0; i < randomAns.length; i++) {
				if(randomAns[i]===0) {
					if(firstCond===0){
						allAns.push({
							ans : 10
						});
					} else {
						allAns.push({
							ans : 3
						});
					}
				} else if (randomAns[i]===1) {
					allAns.push({
						ans : num,
						correct : "correct"
					});
				} else {
					if(secondCond===0){
						allAns.push({
							ans : 5
						});
					} else {
						allAns.push({
							ans : 4
						});
					}
				}
			};
		}

		return allAns;
	}

	function first () {
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = simplifyQ();
		content.answer = genAns(content.ans)
		var html = template(content);
		console.log(html);
		$board.html(html);
	}
	// first();



// sqrt generation
	var sqrTable = [];
	for (var i = 2; i < 101; i++) {
		if (Math.sqrt(i)===Math.ceil(Math.sqrt(i))) {
			// console.log(Math.sqrt(i)+" = "+i);
			sqrTable.push({
				wholeNo : i,
				sqrt : Math.sqrt(i)
			})
		};

	};
	console.log(sqrTable);

	function getExpression () {
		var randGet = ole.getRandom(1,1)[0];
		var exp2;
		if (randGet === 1) {
			var exp1 = {
				val : ole.getRandom(1,20,1)[0]
			}
		} else {
			var randGetNew = ole.getRandom(1,sqrTable.length-1);
			var exp1 = {
				val : sqrTable[randGetNew].sqrt,
				wholeNo : sqrTable[randGetNew].wholeNo,
			}
		}

		var randGetNew = ole.getRandom(1,sqrTable.length-1);
		var temp = {
			val : sqrTable[randGetNew].sqrt,
			wholeNo : sqrTable[randGetNew].wholeNo,
		}

		if (temp.val > exp1.val) {
			exp2 = exp1;
			exp1 = temp;
		} else {
			exp2 = temp;
		}

		var expression = {
			exp1 : exp1,
			exp2 : exp2
		}

		return expression;
	}

	function addition () {
		var randGet = ole.getRandom(1,1)[0];
		var getExp = getExpression();
		var content={} ;
		content.addition = true;
		if (randGet===0) {
			content.exp1 = getExp.exp2;
			content.exp2 = getExp.exp1;
		} else {
			content.exp1 = getExp.exp1;
			content.exp2 = getExp.exp2;
		}

		content.ans = content.exp1.val + content.exp2.val;
		console.log(content);
		return content;
	}

	function subtraction () {
		var getExp = getExpression();
		var content={} ;
		content.subtract = true;
		content.exp1 = getExp.exp1;
		content.exp2 = getExp.exp2;

		content.ans = content.exp1.val - content.exp2.val;
		console.log(content);
		return content;
	}

	function multiply () {
		var getExp = getExpression();
		var content={} ;
		content.multiply = true;
		if (typeof getExp.exp1.wholeNo !="undefined") {
			content.exp1 = getExp.exp2;
			content.exp2 = getExp.exp1;
		} else {
			content.exp1 = getExp.exp1;
			content.exp2 = getExp.exp2;
		}

		content.ans = content.exp1.val * content.exp2.val;
		console.log(content);
		return content;
	}

	var divideArray=[];

	(function divide () {

			var getExp = getExpression();
			var content={} ;
			content.divide = true;
			content.exp1 = getExp.exp1;
			content.exp2 = getExp.exp2;
			content.ans = content.exp1.val / content.exp2.val;


			// console.log("ans = "+Math.floor(content.ans));
			// console.log(content);
			if (Math.floor(content.ans)===content.ans) {
				divideArray.push(content);
				// console.log("hi from divide "+countDivide)
				countDivide++;
				if (countDivide<10) {
					divide()
				};
			} else {
				divide();
			}
		})();

	console.log(divideArray);

	function getDivide () {
		var randGet2 = Math.floor((Math.random()*9));
		console.log(divideArray[randGet2]);
		return divideArray[randGet2];
	}

	function simplifyQ () {
		var randGet = ole.getRandom(1,3)[0];
		console.log(randGet)
		// randGet = 3;
		if(randGet === 0){
			var qVar = addition();
		} else if (randGet===1) {
			var qVar = subtraction();
		} else if (randGet===2) {
			var qVar = multiply();
		} else if (randGet===3) {
			var qVar = getDivide();
		}

		return qVar;
	}

	// setTimeout(first,1000);
	first();


	$board.on('click','.opts',function () {
		var $check = $(this).data('check');

		if($check==="correct"){
			$board.find('.opts').addClass('wrongFound').removeClass('opts');
			$board.find('.correct').addClass('right')
			countCorrect++;
		} else {
			$board.find('.opts').addClass('wrong').removeClass('opts');
			$board.find('.correct').addClass('animated tada right');
		}

		$nextBtn.show(0);
	})

	$nextBtn.on('click',function () {
		countQuestion++;
		$(this).hide(0);
		if(countQuestion===10){
			result();
		} else if(countQuestion<10) {
			first();
		} else {
			ole.activityComplete.finishingcall();
		}

	});


	function result () {

		loadTimelineProgress(11,11);

		if(countCorrect<7){
			var content = {
				idd : 'low',
				msg : data.string.lowMsg
			}
		}
		else if(countCorrect<10){
			var content = {
				idd : 'medium',
				msg : data.string.medium
			}
		} else {
			var content = {
				idd : 'good',
				msg : data.string.good
			}
		}

		content.score = data.string.correct+" = "+countCorrect;
		content.fullMark = data.string.total+" = "+10;
		var source = $('#finish-template').html();
		var template = Handlebars.compile(source);
		var html = template(content);
		$board.html(html);
		$title.hide(0);
		$nextBtn.fadeIn();
		// $("#activity-page-next-btn-enabled").html($whatnextbtn);
	}

$('.sqrt').on('click','.replay',function () {
		location.reload();
	})

})(jQuery);
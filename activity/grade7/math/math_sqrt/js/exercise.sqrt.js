(function () {

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var countQuestion = 0,
		countCorrect = 0,
		countDisplayWrong = 0,
		$title = $('.title'),
		$board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled");

	$title.text(data.string.exerciseTitle);


	function genFacMultiple (num) {
		var factors = ole.getFactors(num);
		var factorsX =factors[0];
		for (var i = 1; i < factors.length; i++) {
			factorsX+="x"+factors[i];
		};
		return factorsX;
	}

	var randomGen = [],sqrTable = [];
	for (var i = 10; i < 1000; i++) {
		if (Math.sqrt(i)===Math.ceil(Math.sqrt(i))) {
			// console.log(Math.sqrt(i)+" = "+i);
			sqrTable.push({
				wholeNo : i,
				sqrt : Math.sqrt(i)
			})
		};

	};

	var getRandom = ole.getRandom(10,sqrTable.length-1);

	function genAns (num) {

		loadTimelineProgress(11,countQuestion+1);

		var newRand = ole.getRandom(2,num-1,1);
		var firstCond = ole.getRandom(1,1)[0];
		var secondCond = ole.getRandom(1,1)[0];
		var randomAns = ole.getRandom(3,2);
		var allAns = [];
		for (var i = 0; i < randomAns.length; i++) {
			if(randomAns[i]===0) {
				if(firstCond===0){
					allAns.push({
						ans : num-newRand[0]
					});
				} else {
					allAns.push({
						ans : num+newRand[0]
					});
				}
			} else if (randomAns[i]===1) {
					allAns.push({
						ans : num,
						correct : "correct"
					});
			} else {
				if(secondCond===0){
					allAns.push({
						ans : num-newRand[1]
					});
				} else {
					allAns.push({
						ans : num+newRand[1]
					});
				}
			}
		};

		return allAns;
	}



	function first () {
		/*console.log("new");
		console.log(sqrTable);
		console.log("countQuestion "+countQuestion);
		console.log(getRandom[countQuestion]);
		console.log(sqrTable[getRandom[countQuestion]]);
		console.log(countQuestion);
		console.log("old");*/

		var newQA = sqrTable[getRandom[countQuestion]];
		// var ans = genAns(newQA.sqrt);

		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = {
			valQ : newQA.wholeNo,
			name : data.string.factors,
			click : data.string.clickFactors,
			factors : genFacMultiple(newQA.wholeNo),
			ans : genAns(newQA.sqrt),
			img : $ref+"/images/correct.png",
			text1 : "jdfsd",
		}
		var html = template(content);
		// console.log(html);
		$board.html(html);

		countQuestion++;
	}

	first();

	$('.sqrt').on('click','.replay',function () {
		location.reload();
	})

	$board.on('click','.clickToShow',function () {
		$(this).hide(0);
	})

	$board.on('click','.opts',function () {
		var $check = $(this).data('check');

		if($check==="correct"){
			$board.find('.opts').addClass('wrongFound').removeClass('opts');
			$board.find('.correct').addClass('right')
			countCorrect++;
		} else {
			$board.find('.opts').addClass('wrong').removeClass('opts');
			$board.find('.correct').addClass('animated tada right');
		}

		$nextBtn.show(0);
	})

	$nextBtn.on('click',function () {
		$(this).hide(0);
		if(countQuestion>=10){
			result();
		} else {
			first()
		}

	});

	function result () {
		loadTimelineProgress(11,11);
		if(countCorrect<7){
			var content = {
				idd : 'low',
				msg : data.string.lowMsg
			}
		}
		else if(countCorrect<10){
			var content = {
				idd : 'medium',
				msg : data.string.medium
			}
		} else {
			var content = {
				idd : 'good',
				msg : data.string.good
			}
		}

		content.score = data.string.correct+" = "+countCorrect;
		content.fullMark = data.string.total+" = "+10;
		var source = $('#finish-template').html();
		var template = Handlebars.compile(source);
		var html = template(content);
		$('.sqrt').html(html);
		ole.footerNotificationHandler.pageEndSetNotification();
	}

})(jQuery);
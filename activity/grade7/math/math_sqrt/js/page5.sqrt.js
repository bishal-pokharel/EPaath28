(function ($) {

	var a;
	var $board = $('.board');
	var $input =0;

	var countNext =0;

	var $total_page = 1;
	loadTimelineProgress($total_page,countNext+1);

	$('.title').text(data.string.p5_title);
	function first () {
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = {
			text1 : data.string.p5_1,
			text2 : data.string.p5_2,
			text3 : data.string.p4_rough,
		}
		var html = template(content);
		$board.html(html);
		$board.find('.factor').hide(0);
		$board.find('.answer').hide(0);
	}

	first();

/*
* put factors in the factor div
*/
	function factorSide (num) {
		var getArray = [];
		getArray = ole.getFactors(parseInt(num),true);
		var len = getArray.length;
		// console.log(getArray[length].residue);
		var source = $('#factorSection-template').html();
		var template = Handlebars.compile(source);
		var content = {
			
			fullArray : getArray,
			placeholder1 : getArray[len-1].residue,
			one : 1
		}
		var html = template(content);
		$board.find('.factorBelow').html(html).show(0).addClass('animated shake');
		$board.find('.factor').show(0).addClass('animated shake');
		setTimeout(function () {
			callProcess(getArray);
		},500);
	}

/*
* event listener : arrow
*/
	$board.on('click','.arrowHead',function () {
		var regVal = /^[1-9][0-9]*$/;
		$input = parseInt($board.find('.sqtitle input').val());
		if(regVal.test($input)){
			$board.find('.answer').hide(0).removeClass('animated bounceInDown');
			factorSide($input);
		}
		else {
			console.log('cant factor');
		}
		
	})

/*
* display process fn
*/
	function callProcess (getArray) {
		console.log('factorsArray');
		getArray.sort();
		var factorsArray = [];
		var factors = "";
		var factorPower = "";
		var singlePower = "";
		var current = null;
		var cnt = 0;
		var lastAns;
		var yesSqrt;

		for (var i = 0; i < getArray.length; i++) {
			if (getArray[i].factor !=current) {
				if (cnt>0) {

				}
				current = getArray[i].factor;
				factorsArray[cnt] = {
					digit : getArray[i].factor,
					count : 1
				}
				cnt++;

			}
			else {
				console.log('cnt = '+cnt);
				console.log(factorsArray);
				factorsArray[cnt-1].count++;
			}
		};
			console.log(factorsArray);
		factors+=getArray[0].factor;
		for (var i = 1; i < getArray.length; i++) {
			factors+="x"+getArray[i].factor;
		};
			console.log(factors);

		factorPower = factorsArray[0].digit+"<sup>"+factorsArray[0].count+"</sup>"
		singlePower = factorsArray[0].digit+"<sup>"+factorsArray[0].count+"x<sup>1</sup>&frasl;<sub>2</sub></sup>"
		for (var i = 1; i < factorsArray.length; i++) {
			factorPower += "x"+factorsArray[i].digit+"<sup>"+factorsArray[i].count+"</sup>"
			singlePower += "."+factorsArray[i].digit+"<sup>"+factorsArray[i].count+"x<sup>1</sup>&frasl;<sub>2</sub></sup>"
		};
		console.log(factorPower);

		if (Math.sqrt($input)===Math.ceil(Math.sqrt($input))) {
			var pSqr = 1;
			var pSqrShow = factorsArray[0].digit+"<sup>"+factorsArray[0].count/2+"</sup>";
			for (var i = 0; i < factorsArray.length; i++) {
				pSqr = pSqr*Math.pow(factorsArray[i].digit,factorsArray[i].count/2);
			};
			for (var j = 1; j < factorsArray.length; j++) {
				pSqrShow += "x"+factorsArray[j].digit+"<sup>"+factorsArray[j].count/2+"</sup>";
			};
			console.log(pSqr);
			processTemplate($input,factors,factorPower,factorPower,singlePower,pSqrShow,pSqr)
			lastAns = pSqr;
			yesSqrt = 'yes';
		} else {
			console.log('not perfet sq');
			var divisibleBy2 = [];
			var singlers = [];
			var highPowersButNotDivisibleBy2 = [];
			var outNInStage = [];
			var wholeSqr;

			for (var i = 0; i < factorsArray.length; i++) {
				if(factorsArray[i].count%2===0){
					divisibleBy2.push({
						digit : factorsArray[i].digit,
						power : factorsArray[i].count/2
					})
				} else if (factorsArray[i].count===1) {
					singlers.push(factorsArray[i].digit)
				} else {
					singlers.push(factorsArray[i].digit);
					divisibleBy2.push({
						digit : factorsArray[i].digit,
						power : (factorsArray[i].count-1)/2
					})
				}
			};
			console.log(divisibleBy2);
			if (typeof divisibleBy2[0] != 'undefined') {
				pSqrShow = divisibleBy2[0].digit+"<sup>"+divisibleBy2[0].power+"</sup>";
				wholeSqr = Math.pow(divisibleBy2[0].digit,divisibleBy2[0].power)
				for (var j = 1; j < divisibleBy2.length; j++) {
					pSqrShow += "x"+divisibleBy2[j].digit+"<sup>"+divisibleBy2[j].power+"</sup>";
					wholeSqr = wholeSqr*Math.pow(divisibleBy2[j].digit,divisibleBy2[j].power)
				};
			}
			
			if (typeof singlers[0] != 'undefined') {
				if (typeof divisibleBy2[0] != 'undefined') {
					pSqrShow += "√<span>"+singlers[0]+"</span>";

				}
				else {
					wholeSqr = '';
					pSqrShow = "√<span>"+singlers[0]+"</span>";
				}
				var singleRooters = singlers[0];
				for (var k = 1; k < singlers.length; k++) {
					pSqrShow += "√<span>"+singlers[k]+"</span>";
					singleRooters = singleRooters*singlers[k];
				};

				wholeSqr += "√<span class='underRoot'>"+singleRooters+"</span>";
			};

			lastAns = wholeSqr;
			yesSqrt = 'no';
			processTemplate($input,factors,factorPower,factorPower,singlePower,pSqrShow,wholeSqr)
		}
			ansSection(lastAns,yesSqrt);
	}

/*
* process template
*/

	function processTemplate (math1,math2,math3,math4,math5,math6,math7) {
		var source = $('#sqrtProcess-template').html();
		var template = Handlebars.compile(source);
		var content = {
			process1 : math1,
			process2 : math2,
			process3 : math3,
			process4 : math4,
			process5 : math5,
			process6 : math6,
			process7 : math7
		}
		var html = template(content);
		$board.find('.process').html(html);
		var k = 1;
		var processSteps = setInterval(function () {
			console.log('mathe rules');
			$board.find('.process'+k).show(0).addClass('animated bounceIn');
			k++;

			if(k === 8){
				clearInterval(processSteps);
			}
		},1000);
		$board.find('.factor').removeClass('animated shake');
	}

/*
* answer template
*/
	function ansSection (lastAns, yesSqrt) {

		if (yesSqrt==='yes') {
			ansText = data.string.perfectSqrt;
		}
		else {
			ansText = data.string.notPerfectSqrt;
		}

		var source = $('#ansSection-template').html();
		var template = Handlebars.compile(source);
		var content = {
			text1 : data.string.text1 ,
			ans : lastAns,
			finalMsg : ansText
		}
		var html = template(content);
		$board.find('.answer').html(html);
		setTimeout(function () {
			$board.find('.answer').show(0).addClass('animated bounceInDown');
			ole.footerNotificationHandler.pageEndSetNotification();
		},9000);
	}

})(jQuery)
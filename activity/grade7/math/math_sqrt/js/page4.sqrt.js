(function ($) {
	var smth=0;
	var $title = $('.title');
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page =3;
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.removeClass('glyphicon glyphicon-circle-arrow-right').html(getArrowBtn());

	$title.text(data.string.p4_title);
	$title.addClass('animated bounceInLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
		first();
	});

	function first () {
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);

		var textSr = ole.textSR(data.string.p4_1_1,'#space#',"<span class='upperLine'>&nbsp;</span>");
		var content = {
			text1 : textSr,
			a1 : 'a',
			a2 : 'X',
			a3 : '100',
			sqrtWhat : data.string.p4_1_2
		}
		var html = template(content);
		$board.html(html);
		$board.find('.text1Holder, .repH1, .repH2, .repH3').hide(0);
		$board.find('.text1Holder').show(0).addClass('animated bounceInRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
			$board.find('.repH1').show(0).addClass('animated bounceInDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
				$board.find('.repH2').show(0).addClass('animated bounceInDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
					$board.find('.repH3').show(0).addClass('animated bounceInDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
						$nextBtn.fadeIn();
					})
				})
			})
		})
	}

	function first_transition() {
		$board.find('.text1Holder, .repH2').addClass('animated hinge').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
			$board.find('.repH3').addClass('animated rotateOutUpRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
				second();
			})
		});
		$board.find('.repH1').addClass('animated rotateOutUpRight');
	}



	function second () {
		var source = $('#second-template').html();
		var template = Handlebars.compile(source);
		var content = {
			text1 : data.string.p4_click,
			math1 : 81,
			math2 : 9,
			math3 : 36,
			math4 : 64
		}
		var html = template(content);
		$board.html(html);
		procesShow(100);
		$board.find('.secondClick').hide(0);
		setTimeout(function () {
			$board.find('.secondClick').show(0);
			$board.find('.secondClick .text1').addClass('animated bounceInUp');
			$board.find('.secondClick .text1').addClass('animated bounceInDown');
		},12000)
	}

	function second_transition() {
		$board.find('.second .descQ').addClass('animated bounceOutUp');
		$board.find('.second .process').addClass('animated bounceOutUp');
		$board.find('.secondClick').addClass('animated bounceOutUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
			third();
		});
	}

	function procesShow (num) {

		var source = $('#procesShow-template').html();
		var template = Handlebars.compile(source);
		var content = {
			math1 : num,
			text1 : data.string.p4_process,
			math2 :  Math.sqrt(num)
		}
		var html = template(content);
		$board.find('.second').html(html);
		$board.find('.second .descQ').addClass('animated bounceInUp');
		for (var i = 1; i <= 7; i++) {
			$board.find('.second .math'+i).hide(0);

		};
		var k = 1;
		var handle = setInterval(function () {
			$board.find('.second .math'+k).show(0).addClass('animated bounceInDown');
			k++;

			if(k === 9){
				clearInterval(handle);
				$nextBtn.fadeIn();
			}
		},1500);
	}

	$board.on('click','.tabss span',function () {
		var $val1 = $(this).data('value');
		procesShow($val1);

	})	

	function third () {
		var source = $('#third-template').html();
		var template = Handlebars.compile(source);
		var content = {
			text1 : data.string.perfect_1,
			text2 : data.string.perfect_2,
			text3 : data.string.perfect_3,
			text4 : data.string.perfect_4
		}
		var html = template(content);
		console.log(html);
		$board.html(html);
	}

	$nextBtn.on('click',function () {
		$(this).fadeOut();
		switch (countNext) {
			case 0:
			first_transition();
			break;

			case 1:
			second_transition();
			break;
		}
		countNext++;
		loadTimelineProgress($total_page,countNext+1);
		if (countNext+1>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification()
		};

	})

})(jQuery);

// पूर्ण वर्ग सङ्ख्याहरूको वर्गमूल पूर्ण सङ्ख्या हुन्छ। तर सबै सङ्ख्या पूर्ण वर्ग सङ्ख्या हुँदैनन् । जस्तै; 5,7,13,17,19 ..................जस्ता सङ्ख्याहरू पूर्णवर्ग सङ्ख्या होइनन् ।
// यस्ता सङ्ख्याहरूको वर्गमुल दशमलवमा आउँछ। विस्तृत जानकारीको लागी अगाडीको पृष्ठमा जाऔँ।
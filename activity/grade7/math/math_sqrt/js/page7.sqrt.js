(function () {

	var $board = $('.board');
	var $setofQ;

	var countNext =0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

	$('.title').text(data.string.p7_title);

	$("#activity-page-next-btn-enabled").removeClass('glyphicon glyphicon-circle-arrow-right').html(getSubpageMoveButton($lang,"next"));

	function first () {
		var source = $('#first-template').html();
		var template = Handlebars.compile(source);
		var content = {
			text1 : data.string.firstTitle,
			img1 : $ref+'/images/wrong.png',
			tryAgain : data.string.tryAgain,
			text2 : data.string.p7_desc,
			text3 : data.string.doThis,
			hint : data.string.hint,
			finalText : data.string.p7_last_msg,
			textHint1 : data.string.hintx_1+"AB "+data.string.hintx,
			textHint2 : data.string.hintx_1+"BC "+data.string.hintx,
			textHint3 : data.string.hintx_1+"AC"+data.string.hintx,

		}
		var html = template(content);
		$board.html(html);
		$("#activity-page-next-btn-enabled").show(0);
		$setofQ = $board.find('.setofQ');
		// $setofQ.find('.text1').show(0).addClass('animated bounceInLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
			$setofQ.find('.abc1, .xhint1').show(0).addClass('animated bounceInLeft');
		// });
	}
	first();

	$setofQ.on('click','.arrowHead1',function () {
		var $input = parseInt($setofQ.find('.abc1 input').val());
		if ($input === 3) {
			$(this).fadeOut();
			$board.find('.xhint1').fadeOut();
			$board.find('.xhint2').fadeIn();
			$setofQ.find('.showMistake, .hint1').fadeOut();
			$('.a1, .a2').removeClass('animateABC');
			$setofQ.find('.showMistake').removeClass('animated tada');
			$setofQ.find('.abc2').show(0).addClass('animated bounceInLeft');
		} else {
			$setofQ.find('.showMistake').show(0).addClass('animated tada').delay(2000).fadeOut();
		}
	})

	$setofQ.on('click','.arrowHead2',function () {
		var $input = parseInt($setofQ.find('.abc2 input').val());
		if ($input === 4) {
			$(this).fadeOut();
			$board.find('.xhint2').fadeOut();
			$board.find('.xhint3').fadeIn();
			$setofQ.find('.showMistake, .hint2').fadeOut();
			$('.a3, .a2').removeClass('animateABC');
			$setofQ.find('.showMistake').removeClass('animated tada');
			$setofQ.find('.abc3').show(0).addClass('animated bounceInLeft');
		} else {
			$setofQ.find('.showMistake').show(0).addClass('animated tada').delay(2000).fadeOut();
		}
	})
	$setofQ.on('click','.arrowHead3',function () {
		var $input = parseInt($setofQ.find('.abc3 input').val());
		if ($input === 5) {
			$(this).fadeOut();
			$board.find('.xhint3').fadeOut();
			$setofQ.find('.showMistake, .hint3, .text3').fadeOut();
			$('.a3, .a1').removeClass('animateABC');
			$setofQ.find('.showMistake').removeClass('animated tada');
			showMath();
		} else {
			$setofQ.find('.showMistake').show(0).addClass('animated tada').delay(2000).fadeOut();
		}

		function showMath () {
			$('.nums').show(0);
			$setofQ.find('.text2').show(0).addClass('animated bounceInLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function () {
				$setofQ.find('.sqrtShow1').show(0).addClass('animated bounceInUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function (){
					$setofQ.find('.sqrtShow2').show(0).addClass('animated bounceInLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function (){
						$setofQ.find('.sqrtShow3').show(0).addClass('animated bounceInDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function (){
							$setofQ.find('.sqrtShow4').show(0).addClass('animated lightSpeedIn');
							$("#activity-page-next-btn-enabled").fadeIn();
						})
					})
				})

			})
		}
	})

/**
* A B C animation for hint
*/
	$setofQ.on('click','.hint1',function () {
		$('.a1, .a2').removeClass('animateABC');
		$('.a1, .a2').addClass('animated wobble').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
			$('.a1, .a2').removeClass('animated wobble').addClass('animateABC');
		});
	})

	$setofQ.on('click','.hint2',function () {
		$('.a3, .a2').removeClass('animateABC');
		$('.a3, .a2').addClass('animated wobble').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
			$('.a3, .a2').removeClass('animated wobble').addClass('animateABC');
		});
	})

	$setofQ.on('click','.hint3',function () {
		$('.a3, .a1').removeClass('animateABC');
		$('.a3, .a1').addClass('animated wobble').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
			$('.a3, .a1').removeClass('animated wobble').addClass('animateABC');
		});
		// $setofQ.find('.text3').fadeIn();
	})

/**
* hover
*/
	$('.hoverLadder').on('mouseenter',function () {
		$('#scale').show(0);
	}).on('mouseout',function () {
		$('#scale').hide(0);
	})

	$('.hoverFooter').on('mouseenter',function () {
		$('#scale3').show(0);
	}).on('mouseout',function () {
		$('#scale3').hide(0);
	})

	$('.hoverTree').on('mouseenter',function () {
		$('#scale1').show(0);
	}).on('mouseout',function () {
		$('#scale1').hide(0);
	})

/**
* next button click
*/
	$("#activity-page-next-btn-enabled").on('click',function () {
		$setofQ.addClass('animated hinge').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
			$setofQ.hide(0);
			$('.finalMsg').show(0).addClass('animated flipInX');
			$("#activity-page-next-btn-enabled").fadeOut();

			countNext++;
			loadTimelineProgress($total_page,countNext+1);
			ole.footerNotificationHandler.lessonEndSetNotification();
		});

	})
})(jQuery)

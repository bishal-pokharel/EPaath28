(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 10,
		$refImg = $ref+"/images/";

		$nextBtn.show(0);

	loadTimelineProgress($total_page,countNext);
	$nextBtn.html(getSubpageMoveButton($lang,"next"));

/*
* first
*/
	function func (className, title, footTitle, imageLink) {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		console.log($refImg+imageLink);
		var content = {
			className : className,
			title : title,
			footTitle : footTitle,
			img : $refImg+imageLink
		}
		var html = template(content);
		$board.html(html);
		$board.find(".footTitle, .imgHolder").hide(0);
	}
	// func("first", data.string.p1_1,data.string.p1_2,"mirror_girl.gif");

	function func2 () {
		var source = $("#second-template").html()
		var template = Handlebars.compile(source);
		// console.log($refImg+imageLink);
		var content = {
			className : "second",
			title : data.string.p1_3,
			footTitle : data.string.p1_4,
			img1 : $refImg+"daaee_final.gif",
			img2 : $refImg+"door_rotare.gif",
			img3 : $refImg+"water_tap.gif"
		}
		var html = template(content);
		$board.html(html);
		$board.find(".footTitle, .imgHolder").hide(0);
	}

	function func3 () {
		var source = $("#third-template").html()
		var template = Handlebars.compile(source);
		// console.log($refImg+imageLink);
		var content = {
			className : "third",
			title : data.lesson.chapter,
			img_src : $refImg+"angles_bg.png",
		}
		var html = template(content);
		$board.html(html);
		// $board.find(".footTitle, .imgHolder").hide(0);
	}
	func3 ();

	$nextBtn.on('click',function () {
		$(this).hide(0);
		switch (countNext) {
			case 1:
			func("first", data.string.p1_1,data.string.p1_2,"mirror_girl.gif");
			break;

			case 2:
			$board.find(".imgHolder").show(0);
			break;

			case 3:
			$board.find(".footTitle").show(0);
			break;

			case 4:
			func2();
			break;

			case 5:
			$board.find(".imgHolder").show(0);
			break;

			case 6:
			$board.find(".footTitle").show(0);
			break;

			case 7:
				func("third", data.string.p1_5, data.string.p1_6,"book_translation.gif");
			break;

			case 8:
			$board.find(".imgHolder").show(0);
			break;

			case 9:
			$board.find(".footTitle").show(0);
			break;
		}

		countNext++;
		console.log("countNext = "+countNext);
		loadTimelineProgress($total_page,countNext);
		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}
	})



	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
})(jQuery);

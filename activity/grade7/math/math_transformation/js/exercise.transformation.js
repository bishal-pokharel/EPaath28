(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 11,
		countCorrect = 0;
		firstClick = true;
		$refImg = $ref+"/images/exercise/";
	$nextBtn.hide(0);
	loadTimelineProgress($total_page,countNext);
	$nextBtn.html(getSubpageMoveButton($lang,"next"));


	var arrayQuestions = [
		{
			type : 2,
			question : data.string.e1t1,
			option : [$refImg+"q02a.png", $refImg+"q02b.png", $refImg+"q02c.png"],
			correct : 3
		},
		{
			type : 1,
			question : data.string.e1t2,
			primaryImage : $refImg+"q06a.png",
			option : [$refImg+"q06b.png", $refImg+"q06c.png", $refImg+"q06d.png"],
			correct : 1
		},
		{
			type : 1,
			question : data.string.e1t4,
			primaryImage : $refImg+"q04a.png",
			option : [$refImg+"q04b.png", $refImg+"q04c.png"],
			correct : 2
		},
		{
			type : 1,
			question : data.string.e1t7,
			primaryImage : $refImg+"q01a.png",
			option : [$refImg+"q01b.png", $refImg+"q01c.png", $refImg+"q01d.png"],
			correct : 2
		},
		{
			type : 2,
			question : data.string.e1t5,
			option : [$refImg+"q05a.png", $refImg+"q05b.png", $refImg+"q05c.png"],
			correct : 1
		},
		{
			type : 1,
			question : data.string.e1t6,
			primaryImage : $refImg+"q03a.png",
			option : [$refImg+"q03b.png", $refImg+"q03c.png", $refImg+"q03d.png"],
			correct : 3
		},
		{
			type : 1,
			question : data.string.e1t7,
			primaryImage : $refImg+"q07a.png",
			option : [$refImg+"q07b.png", $refImg+"q07c.png", $refImg+"q07d.png"],
			correct : 3
		},
		{
			type : 3,
			question : data.string.e1t8,
			options : [data.string.e1t8a, data.string.e1t9a, data.string.e1t10a],
			correct : 1
		},
		{
			type : 3,
			question : data.string.e1t9,
			options : [data.string.e1t8a, data.string.e1t9a, data.string.e1t10a],
			correct : 2
		},
		{
			type : 3,
			question : data.string.e1t10,
			options : [data.string.e1t8a, data.string.e1t9a, data.string.e1t10a],
			correct : 3
		}		
	];

	var random = ole.getRandom(10,9);
	// alert(random);

/*
* first
*/
	function type1 (pass) {
		var source = $("#type1-template").html();
		var template = Handlebars.compile(source);
		console.log(pass.option);
		var content = {
			wrong : $ref+"/images/wrong.png",
			right : $ref+"/images/correct.png",
			question : pass.question,
			primary : pass.primaryImage,
			images : pass.option
		};
		var html = template(content);
		$board.html(html);
	}

	function type2 (pass) {
		var source = $("#type2-template").html();
		var template = Handlebars.compile(source);
		// console.log(pass.option);
		var content = {
			wrong : $ref+"/images/wrong.png",
			right : $ref+"/images/correct.png",
			question : pass.question,
			images : pass.option
		};
		var html = template(content);
		$board.html(html);
	}

	function type3 (pass) {
		var source = $("#type3-template").html();
		var template = Handlebars.compile(source);
		// console.log(pass.option);
		var content = {
			wrong : $ref+"/images/wrong.png",
			right : $ref+"/images/correct.png",
			smily : $refImg+"smily.png",
			question : pass.question,
			images : pass.options
		};
		var html = template(content);
		$board.html(html);
	}

	// type3(arrayQuestions[8]);

	function functionLoader () {
		// if (arrayQuestions[random[countNext]].type === 1) {

		// } else
		switch (arrayQuestions[random[countNext-1]].type) {
			case 1:
			type1(arrayQuestions[random[countNext-1]]);
			break;

			case 2:
			type2(arrayQuestions[random[countNext-1]]);
			break;

			case 3:
			type3(arrayQuestions[random[countNext-1]]);
			break;
		}

	}
	functionLoader();
	
	function remarks () {
		var source = $("#remark-template").html();
		var template = Handlebars.compile(source);
		// console.log(pass.option);
		var content = {
			remarkTextData : data.string.e1remarkText,
			up : countCorrect,
			down : $total_page
		};
		var html = template(content);
		$board.html(html);
	}

	$nextBtn.on('click',function () {
		$(this).hide(0);

		countNext++;
		if (countNext < $total_page) {
			functionLoader();
			
		} else if(countNext == $total_page) {
			remarks();
			$nextBtn.show(0);
		}
		else if(countNext > $total_page){
			ole.activityComplete.finishingcall();
		}

		firstClick = true;
		// console.log("countNext = "+countNext);
		loadTimelineProgress($total_page,countNext);

	});

	$board.on('click','.options span',function () {
		$this = $(this);
		var index = $this.index();
		// console.log(index);
		if (index==arrayQuestions[random[countNext-1]].correct-1) {
			$this.css({"border":"3px solid green","border-radius":"5px"});
			$this.siblings('span').css({"pointer-events":"none"});
			$board.find(".remarkImg #wrong").hide(0);
			$board.find(".remarkImg #right").show(0);
			if (firstClick) {
				countCorrect++;
			};
			$nextBtn.show(0);
		} else {
			$this.css({"border":"3px solid #FF7E7E","border-radius":"5px"});
			$board.find(".remarkImg #right").hide(0);
			$board.find(".remarkImg #wrong").show(0);
		}

		firstClick = false;
		
	});



	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
})(jQuery);
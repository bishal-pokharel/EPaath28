(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 3,
		$refImg = $ref+"/images/";
	$nextBtn.hide(0);
	loadTimelineProgress($total_page,countNext);
	$nextBtn.html(getSubpageMoveButton($lang,"next"));

	var images = {
		first : ["arrow_rotate.gif","arrow_reflection.gif","arrow_translation.gif"],
		second : ["box_reflection.gif","box_rotation.gif","box_translation.gif"],
		third : ["moon_reflection.gif","moon_rotation.gif","moon_translation.gif"]
	}

/*
* first
*/
	function func (className, title, imageLink, check1, check2, check3) {
		var source = $("#first-template").html()
		var template = Handlebars.compile(source);
		console.log($refImg+imageLink);
		var content = {
			className : className,
			title : title,
			img1 : $refImg+imageLink[0],
			img2 : $refImg+imageLink[1],
			img3 : $refImg+imageLink[2],
			correct1 : check1,
			correct2 : check2,
			correct3 : check3,
			right : $refImg+"correct.png",
			wrong : $refImg+"wrong.png",
		}
		var html = template(content);
		$board.html(html);
	}
	func("first", data.string.p2_1,images.first,0,0,1);

	function func2 () {
		var source = $("#second-template").html()
		var template = Handlebars.compile(source);
		// console.log($refImg+imageLink);
		var content = {
		}
		var html = template(content);
		$board.html(html);
		$board.find(".footTitle, .imgHolder").hide(0);
	}

	$nextBtn.on('click',function () {
		$(this).hide(0);
		switch (countNext) {
			case 1:
			func("first", data.string.p2_2,images.second,0,1,0);
			break;

			case 2:
			func("first", data.string.p2_3,images.third,1,0,0);
			break;

			/*case 3:
			func2();
			break;
*/
		}

		countNext++;
		console.log("countNext = "+countNext);
		loadTimelineProgress($total_page,countNext);
		/*if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			$(this).show(0);
		}*/
	})

	$board.on('click','.click',function () {
		$this = $(this);
		$check = $this.data("check");
		if ($check===1) {
			$this.find('.right').show(0);
			if (countNext===3) {
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				$nextBtn.show(0);
			}
		} else {
			$this.find('.wrong').show(0);
		}
	})



	/*other caller functions for footerNotificationHandler-
		pageEndSetNotification
		lessonEndSetNotification
		setNotificationMsg
		showNextPageButton
		hideNextPageButton
		setNotificationMsgHideNextPagebutton
		setNotificationMsgShowNextPagebutton
		hideNotification
	*/
})(jQuery);
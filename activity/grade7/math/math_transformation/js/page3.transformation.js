(function ($) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		countNext = 1,
		$total_page = 1,
		$refImg = $ref+"/images/",
		$canvas=$("canvas");

	$nextBtn.hide(0);
	loadTimelineProgress($total_page,countNext);
	$nextBtn.html(getSubpageMoveButton($lang,"next"));

	/*text putting*/
	$board.find(".title span").text(data.string.p3_1);
	$board.find(".translation").text(data.string.p3_2);
	$board.find(".btnTranslation, .btnRotation").text(data.string.btnClick);
	$board.find(".rotation").text(data.string.p3_3);
	$board.find(".angleGive").text(data.string.p3_4);
	$board.find(".reflection").text(data.string.p3_5);
	$board.find(".btnRefresh").text(data.string.repeat);
	$board.find(".error").text(data.string.p3_errorMsg);

	/*
	* canvas width, height
	*
	*/
	// width = .width();
	// height = $("canvas").height();
	// console.log(width+"***"+height);

	mainWidth = $board.width();
	console.log(mainWidth);
	canvasWidth = 460/896.6666*mainWidth;
	$canvas.width(canvasWidth);
	$canvas.height(canvasWidth);
	var canvas = function () {
		this.width = canvasWidth;
		this.height = canvasWidth;
		this.d = this.width;

		this.c = document.getElementById("graph");
		this.c.width = this.width;
		this.c.height = this.height;
		this.ctx=this.c.getContext("2d");

		this.dExtra = 528; //dimension of graph paper image with extra space
		this.dAbsolute = 480; //dimension of the graph paper image without extra space
		this.oneSideGap = (this.dExtra-this.dAbsolute)/2;
		this.percentage1Side = this.oneSideGap/this.dExtra*100;
		this.percentage2Side = this.percentage1Side*2;
		// this.check = this.dExtra-this.percentage2Side*this.dExtra/100;
		this.newDAbsolute = this.d-this.percentage2Side*this.d/100;
		this.newSingleGap = this.percentage1Side*this.d/100;
		this.center = {
			x : this.newDAbsolute/2+this.newSingleGap,
			y : this.newDAbsolute/2+this.newSingleGap
		}

		this.numOfSmallBox = 6;
		this.tinyBoxSize = 16; //16x16 size
		this.tinyBoxPercentage = this.tinyBoxSize/this.dExtra*100;

		this.newTinyBoxSize = this.tinyBoxPercentage*this.d/100;

		this.sqLength = this.newTinyBoxSize*this.numOfSmallBox;
		this.square = {
			length : this.newTinyBoxSize*this.numOfSmallBox,
			x : this.center.x,
			y : this.center.y,
			angle : 0
		}

		this.ghostSquare = {
			on : false,
			length : this.square.length,
			x : this.center.x,
			y : this.center.y,
			angle : 0
		}

		this.updateGhost = function () {
			this.ghostSquare = {
				on : true,
				length : this.square.length,
				x : this.square.x,
				y : this.square.y,
				angle : this.square.angle
			}
		}



		this.canvas = function () {
			// this.ctx.save();
			// this.ctx.setTransform(1, 0, 0, 1, 0, 0);
			this.ctx.clearRect ( 0 , 0 , this.c.width, this.c.height );
			this.c.width = this.c.width;
			// this.ctx.restore();
			if (this.ghostSquare.on=== true) {
				this.createGhostSq();
			};

			this.createSq();


		}

		this.rotate = function (angle) {
			this.updateGhost();
			this.square.angle = angle;
			this.canvas();
		}

		this.translate = function (x,y) {
			if (x>12 || y>12) {
				errorshow(0);
			} else {
				this.updateGhost();

				this.square.x = this.center.x+x*this.newTinyBoxSize;
				this.square.y = this.center.y-y*this.newTinyBoxSize;
				this.canvas();
			}
		}

		this.reflectX = function () {
			this.updateGhost();
			var y = this.center.y-this.square.y;
			var angle = 180-this.square.angle;
			this.square.y = this.center.y+y;
			this.square.angle = angle;
			this.canvas();
		}

		this.reflectY = function () {
			this.updateGhost();
			var x = this.center.x-this.square.x;
			var angle = 180-this.square.angle;
			this.square.x = this.center.x+x;
			this.square.angle = angle;
			this.canvas();
		}

		this.refresh = function () {
			this.ghostSquare.on = false;
			this.square.x = this.center.x;
			this.square.y = this.center.y;
			this.square.angle = 0;
			this.canvas();
		}

		this.createGhostSq = function () {
			this.ctx.save();
			this.ctx.beginPath();
			this.ctx.lineWidth="3";
			this.ctx.strokeStyle="#A2B354";
			// this.ctx.translate(-this.square.x, -this.square.y);
			this.ctx.translate(this.ghostSquare.x, this.ghostSquare.y);
			// this.ctx.translate(0, 20);
			console.log(this.ghostSquare.angle+"** sq = "+this.square.angle);
			this.ctx.rotate(this.ghostSquare.angle*Math.PI/180);
			// this.ctx.rect(-this.sqLength/2,-this.sqLength/2,this.sqLength,this.sqLength);
			this.ctx.rect(-this.square.length/2,-this.square.length/2,this.square.length,this.square.length);
			this.ctx.stroke();
			this.ctx.restore();

		}

		this.createSq = function () {

			this.ctx.beginPath();
			this.ctx.lineWidth="2";
			this.ctx.strokeStyle="red";
			this.ctx.translate(this.square.x, this.square.y);
			this.ctx.save();
				// this.ctx.beginPath();
				// this.ctx.strokeStyle="#006400";
				this.ctx.arc(0, 0, 2, 0, 2 * Math.PI, false);
				/*this.ctx.fillStyle = 'green';
				this.ctx.fill();*/
				this.ctx.lineWidth = 0;
				// this.ctx.strokeStyle = '#003300';
				this.ctx.stroke();
			this.ctx.restore();
			this.ctx.rotate(this.square.angle*Math.PI/180);
			this.ctx.rect(-this.square.length/2,-this.square.length/2,this.square.length,this.square.length);

			this.ctx.stroke();

		}


	}

	/*object to keep track of if we should show go to next page*/
	var goNextPage = function(){
		this.translationClicked = false;
		this.rotationClicked = false;
		this.reflectXClicked = false;
		this.reflectYClicked = false;
		this.shallIgotoNextPage = function(){
			if(this.translationClicked && this.rotationClicked
				&& this.reflectXClicked && this.reflectYClicked){
				ole.footerNotificationHandler.lessonEndSetNotification() ;
			}
		};
	}


	function errorShow () {
		$board.find(".error").fadeIn().delay(2000).fadeOut();
	}

	var showNextPageButtonNow = new goNextPage(); /*instance of goNextPage*/
	var a = new canvas();
	a.createSq();

	// console.log(a.width+"***"+a.height+"***"+a.center.x+"***"+a.tinyBoxSize);

	/*on clicks*/
	//translation
	$board.on('click','.btnTranslation',function () {
		$val1 = $board.find(".translation1").val() || 0;
		$val2 = $board.find(".translation2").val() || 0;
		a.translate($val1,$val2);
		if(!showNextPageButtonNow.translationClicked){
			showNextPageButtonNow.translationClicked = true;
			showNextPageButtonNow.shallIgotoNextPage();
		}
	})

	$board.on('click','.btnRotation',function () {
		$val1 = $board.find(".rotationInput").val() || 0;
		a.rotate($val1);
		if(!showNextPageButtonNow.rotationClicked){
			showNextPageButtonNow.rotationClicked = true;
			showNextPageButtonNow.shallIgotoNextPage();
		}
	})

	$board.on('click','.reflectX',function () {
		a.reflectX();
		if(!showNextPageButtonNow.reflectXClicked){
			showNextPageButtonNow.reflectXClicked = true;
			showNextPageButtonNow.shallIgotoNextPage();
		}
	})

	$board.on('click','.reflectY',function () {
		a.reflectY();
		if(!showNextPageButtonNow.reflectYClicked){
			showNextPageButtonNow.reflectYClicked = true;
			showNextPageButtonNow.shallIgotoNextPage();
		}
	})

	$board.on('click','.btnRefresh',function () {
		a.refresh();
	});


})(jQuery);

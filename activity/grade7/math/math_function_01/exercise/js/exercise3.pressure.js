Handlebars.registerHelper('equalsto', function(index) {
		console.log(index);
		if (index>0) {
			return "=";
		};
		return "";
	});

(function () {

	var $wahtar=getSubpageMoveButton($lang,"next");

	var $board = $('.board');
	$nextBtn = $("#activity-page-next-btn-enabled"),
	$title = $('.title'),
	animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
	$refImg = $ref+"/exercise/images/";

	$nextBtn.html($wahtar);

	var tortoise = {
		pos : 0,
		initalPos : 5,
		finalPos : 90,
	}

	var question = {
		count : 0,
		range : 2,
		total : 2,
		correct : []
	}
	// var randomVals = ole.getRandom(question.range,question.total-1);
	// console.log(randomVals);
	// $board.html(randomVals)
/*
 _              _   _
| |_ _   _ _ __| |_| | ___
| __| | | | '__| __| |/ _ \
| |_| |_| | |  | |_| |  __/
 \__|\__,_|_|   \__|_|\___|

*/
	function goTurtle (len,nextFunc) {
		// console.log('tortoise.pos= '+tortoise.pos);
		var lastpos = tortoise.pos, i=lastpos;
		// tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(question.count-1)+tortoise.initalPos);
		tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(len-1)+tortoise.initalPos);
		var newGo = setInterval(function () {
			// console.log('lastpos = '+lastpos+" newPos="+tortoise.pos+" i="+i);
			if(i%2===0) {
				$('#tortoise2').show(0);
				$('#tortoise1').hide(0);
			} else {
				$('#tortoise1').show(0);
				$('#tortoise2').hide(0);
			}
			$('.tortoise').animate({'left' : i+"%"},100);
			i++;
			if (i>tortoise.pos) {
				clearInterval(newGo);
				// nextFunc();
			};
		},100);
	}

/*
* turtle out
*/

	var content = [
	{
		type:'',
		question : 
				'<div class="ray-holder">'+
				'<div class="input-holder2 oval">'+
				'<div id="entry1">1</div>'+
				'<div id="entry2">2</div>'+
				'<div id="entry3">3</div>'+
				'<div id="entry3">4</div>'+
				'</div>'+
				'<div class="lineholders">'+
				'<div class="line1holder">'+
				'<div class="arrowhead1">'+
				'</div>'+
				'</div>'+
				
				'<div class="line2holder">'+
				'<div class="arrowhead2">'+
				'</div>'+
				'</div>'+
				
				'<div class="line3holder">'+
				'<div class="arrowhead3">'+
				'</div>'+
				'</div>'+
				
				'<div class="line4holder">'+
				'<div class="arrowhead4">'+
				'</div>'+
				
				'</div>'+
				'</div>'+
				'<div class="resulto oval">'+
				'<div id="result1">4</div>'+
				'<div id="result2">5</div>'+
				'<div id="result3">6</div>'+
				'<div id="result3">7</div>'+
				'</div>'+
				'</div>'+
				'</div>',
		// hint : "hint",
		answers: [
		{ans : math.parseEquation("Y = X * 4")},
		{ans : math.parseEquation("Y = X * 2")},
		{ans : math.parseEquation("Y = X + 3"),
		correct : "correct"},
		{ans : math.parseEquation("Y = X / 4")}
		],
		hintSolution : [
			math.parseEquation("m^7 / m^7"),
			math.parseEquation("m^(7-7)"),
			math.parseEquation("m^0"),
			math.parseEquation("1")
		]
	},{
		type:'',
		question : '<div id="graph"></div>',
					
		// hint : "hint",
		answers: [
		{ans : math.parseEquation("Y = X * 2"),
			correct : "correct"},
		{ans : math.parseEquation("Y = X + 2")},
		{ans : math.parseEquation("Y = X - 2")},
		{ans : math.parseEquation("Y = X / 2")}
		],
		hintSolution : [
			math.parseEquation("3b^4 / b^4"),
			math.parseEquation("3*b^(4-4)"),
			math.parseEquation("3*b^0"),
			math.parseEquation("3*1"),
			"1"
		]
	},{
		type:'',
		question : math.parseEquation("4z^0 =? "),
		// hint : "hint",
		answers: [
		{ans : "1"},
		{ans : "4",
		correct : "correct"},
		{ans : math.parseEquation("4z")},
		{ans : "0"}
		],
		hintSolution : [
			math.parseEquation("3a^0"),
			math.parseEquation("3*1"),
			math.parseEquation("3")
		]
	},{
		type:'',
		question : "If y = 3, find the value of : "+ math.parseEquation("3y^2+2y^0=?"),
		// hint : "hint",
		answers: [
		{ans : math.parseEquation("27")},
		{ans : math.parseEquation("29"),
		correct : "correct"},
		{ans : math.parseEquation("30")},
		{ans : math.parseEquation("25")}
		],
		hintSolution : [
			"If a = 3, find the value of : "+ math.parseEquation("2a^3=?"),
			math.parseEquation("2*(a)^3"),
			math.parseEquation("2*(3)^3"),
			math.parseEquation("2*27"),
			math.parseEquation("54")
		]
	},
	]

	function  qA () {
		$title.text(data.string.exerciseTitle_3).show(0);
		$('.advancement').show(0);
		var counter = question.count;

		loadTimelineProgress(question.total,question.count+1);
		content[counter].correctImg = $refImg+"correct.png";
		content[counter].wrongImg = $refImg+"wrong.png";
		content[counter].question =ole.textSR(content[counter].question,'sup2','<sup>2</sup>');
			if (content[counter].type==='multipleImg') {
				// alert();
				var source = $('#qAImage-templete').html();
			} else {
				var source = $('#qA-templete').html();
			}

			var template = Handlebars.compile(source);
			var html = template(content[counter]);
			$board.html(html);
			loadSolutionHint(counter);
	}

	qA ()

	function loadSolutionHint (num) {
		var source = $('#hintSolution-template').html();
		var template = Handlebars.compile(source);
		var html = template(content[num]);
		$(".hintBox").html(html);
	}

/*
* results
*/
	function results () {
		$title.hide(0);
		var len = question.correct.length;
		if(len < question.total) {
			$('.tortoise').animate({

			},600,function () {
				$(this).html("<img src='"+$refImg+"laydown.png' >")
				$(this).css({
					'width' : '20%',
					'bottom' : "-10%",
				})
			})

			$('#flag').css({
				'width' : '15%',
			},1000);
			if(len === 9){
				var endContent = {
					story : data.string.missedOne
				}
			} else if(len === 0){
				var endContent = {
					story : data.string.noTry
				}
			} else if(len <5){
				var endContent = {
					story : data.string.needMore
				}
			} else if(len <9){
				var endContent = {
					story : data.string.someMore
				}
			}
		} else {
			$('.tortoise').html("<img src='"+$refImg+"dance.gif' >").animate({
				'left' : '80%',
				'width' : '15%',
			},1000);
			$('#flag').animate({
				'right' : '35%',
				'width' : '15%',
			},1000);
			var endContent = {
				story : data.string.successStory
			}
		}

		var source = $('#results-template').html();
		var template = Handlebars.compile(source);
		var html = template(endContent);
		$board.html(html);
		$board.find('.endBoard').addClass('animated bounceInDown').one(animationEnd,function () {
			$('.repeat').show(0).addClass('animated bounceInDown');
		});

	}

	$board.on('click','.neutral',function () {
		// console.log("what");
		var $this = $(this);
		var isCorrect = $this.data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right');
			$('.neutral').removeClass('neutral');
			$nextBtn.fadeIn();
			$('.imgCrot .correctImg').show(0);
			question.correct.push(question.count);
			// console.log(question.correct);
			var len = question.correct.length;
			goTurtle (len);
		}
		else {
			$this.addClass('wrong')
			$('.neutral').not('.correct').addClass('wrong').removeClass('neutral');
			$('.correct').addClass('right animated  tada');
			$('.neutral').removeClass('neutral');
			$('.imgCrot .wrongImg').show(0);
			$nextBtn.fadeIn();

		}
	})

	$(".rhino").on('click',function () {
		$(".hintBox").show(0);
	})

	$nextBtn.on('click',function () {
		$(".hintBox").hide(0);
		$nextBtn.fadeOut();
		question.count++;

		if(question.count<question.total){
			// goTurtle (qA);
			qA();
		}
		else if (question.count===question.total) {
			// goTurtle(results);
			// alert('finish');
			// $nextBtn.fadeOut();
			results();
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// ole.activityComplete.finishingcall();
		}
	})

	$board.on('click','.storyWrapper .enter',qA);

})(jQuery)


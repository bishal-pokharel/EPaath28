$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 4;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{term1: "9", term2: "(\u00D75)+6", sym: "?", answer : "51"},
		{term1: "16", term2: "(\u00D75)+6", sym: "?", answer : "86"},
		{term1: "36", term2: "(÷3)-5", sym: "?", answer : "7"},
		{term1: "60", term2: "(÷3)-5", sym: "?", answer : "15"}
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p6_1,
			qTitle : data.string.p6_2
		}
		var html = template(content);
		
		$board.html(html);

		first(qDatas[0]);
	}


	function first(sub) {
		$('.q'+countNext).hide(0);
		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : data.string.p1_0,
			clickToSee : data.string.p6_3,
			answer : data.string.p6_4,
			baseString : data.string.p6_5,
			wrongnote: data.string.p6_6,
			//powerString : data.string.p2_6,
			term1 : sub.term1,
			term2 : sub.term2,
			sym : sub.sym,
			ans : sub.answer,
			couter : countNext+1
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){

		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var answer = $board.find(cls+" input.base").val().toLowerCase();
		//var power = $board.find(cls+" input.power").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if (answer ===upDatas.answer) {
			check[0]=1;
			$board.find(cls+" .check1").html(right);
			if (countNext>=3) {
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				$nextBtn.show(0);
			}
		} else {
			$board.find(cls+" .check1").html(wrong);
			$board.find(cls+" .wrong-note").show(0);
			$('.base').val("");
			$board.find(cls+" .clicks .click").show(0);
			
		}

		/*if (power===upDatas.power) {
			check[1]=1;
			$board.find(cls+" .check2").html(right);
		} else {
			$board.find(cls+" .check2").html(wrong);
		}*/


	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});
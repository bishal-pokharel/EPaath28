/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	$('#activity-page-next-btn-enabled').show(0);
	var characterDialouges1 = [ {
		diaouges : data.string.p7_7
	},//input01

	];


	var characterDialouges2 = [ {
		diaouges : data.string.p7_8
	},//output01

	];



	var characterDialouges3 = [ {
		diaouges : data.string.p7_9
	},//des1

	];



	var characterDialouges4 = [ {
		diaouges : data.string.p7_10
	},//des2

	];


	var characterDialouges5 = [ {
		diaouges : data.string.p7_11
	},//des3

	];

	var characterDialouges6 = [ {
		diaouges : data.string.p7_12
	},//des4

	];

	var characterDialouges7 = [ {
		diaouges : data.string.p7_13
	},//des5

	];

	var characterDialouges8 = [ {
		diaouges : data.string.p7_14
	},//des6

	];

	var characterDialouges9 = [ {
		diaouges : function () {
		var d = ole.textSR(data.string.p7_15,data.string.p7_91,"<span class='ind2'>"+data.string.p7_91+"</span>");
			d = ole.textSR(d,data.string.p7_92,"<span class='ind5'>"+data.string.p7_92+"</span>")
			d = ole.textSR(d,data.string.p7_93,"<span class='ind2'>"+data.string.p7_93+"</span>")
			d = ole.textSR(d,data.string.p7_94,"<span class='ind5'>"+data.string.p7_94+"</span>")

			return d;
		}


	},//des7

	];


	var characterDialouges10 = [ {
		diaouges : data.string.p7_16
	},//des8

	];

	var characterDialouges11 = [ {
		diaouges : data.string.p7_17
	},//des9

	];

	var characterDialouges12 = [ {
		diaouges : data.string.p7_18
	},//des10

	];

	var characterDialouges13 = [ {
		diaouges : data.string.p7_19
	},//des11

	];



/*page1 end*************************************************/




/***************page2******************/


var characterDialouges14 = [ {
		diaouges : data.string.p7_22
	},//input02

	];


	var characterDialouges15 = [ {
		diaouges : data.string.p7_23
	},//output02

	];



	var characterDialouges16 = [ {
		diaouges : data.string.p7_24
	},//des12

	];



	var characterDialouges17 = [ {
		diaouges : data.string.p7_25
	},//des13

	];


	var characterDialouges18 = [ {
		diaouges : data.string.p7_26
	},//des14

	];

	var characterDialouges19 = [ {
		diaouges : data.string.p7_27
	},//des15

	];

	var characterDialouges20 = [ {
		diaouges : data.string.p7_28
	},//des16

	];

	var characterDialouges21 = [ {
		diaouges : data.string.p7_29
	},//des17

	];


		var characterDialouges22 = [ {
		diaouges : function () {
		var d = ole.textSR(data.string.p7_30,data.string.p7_91,"<span class='ind2'>"+data.string.p7_91+"</span>");
			d = ole.textSR(d,data.string.p7_92,"<span class='ind5'>"+data.string.p7_92+"</span>")
			d = ole.textSR(d,data.string.p7_93,"<span class='ind2'>"+data.string.p7_93+"</span>")
			d = ole.textSR(d,data.string.p7_94,"<span class='ind5'>"+data.string.p7_94+"</span>")

			return d;
		}


	},//des7

	];



	var characterDialouges23 = [ {
		diaouges : data.string.p7_31
	},//des19

	];

	var characterDialouges24 = [ {
		diaouges : data.string.p7_32
	},//des20

	];

	var characterDialouges25 = [ {
		diaouges : data.string.p7_33
	},//des21

	];

	var characterDialouges26 = [ {
		diaouges : data.string.p7_34
	},//des22

	];



/*****end of page2*********************/




/*****page3*********************/



var characterDialouges27 = [ {
		diaouges : data.string.p7_37
	},//input03

	];


	var characterDialouges28 = [ {
		diaouges : data.string.p7_38
	},//output03

	];



	var characterDialouges29 = [ {
		diaouges : data.string.p7_39
	},//des23

	];



	var characterDialouges30 = [ {
		diaouges : data.string.p7_40
	},//des24

	];


	var characterDialouges31 = [ {
		diaouges : data.string.p7_41
	},//des25

	];

	var characterDialouges32 = [ {
		diaouges : data.string.p7_42
	},//des26

	];

	var characterDialouges33 = [ {
		diaouges : data.string.p7_43
	},//des27

	];

	var characterDialouges34 = [ {
		diaouges : data.string.p7_44
	},//des28

	];

	var characterDialouges35 = [ {
		diaouges : data.string.p7_45
	},//des29

	];

	var characterDialouges36 = [ {
		diaouges : function () {
		var d = ole.textSR(data.string.p7_46,data.string.p7_91,"<span class='ind2'>"+data.string.p7_91+"</span>");
			d = ole.textSR(d,data.string.p7_92,"<span class='ind5'>"+data.string.p7_92+"</span>")
			d = ole.textSR(d,data.string.p7_93,"<span class='ind2'>"+data.string.p7_93+"</span>")
			d = ole.textSR(d,data.string.p7_94,"<span class='ind5'>"+data.string.p7_94+"</span>")

			return d;
		}


	},


	];

	var characterDialouges37 = [ {
		diaouges : data.string.p7_47
	},//des31

	];

	var characterDialouges38 = [ {
		diaouges : data.string.p7_48
	},//des32

	];

	var characterDialouges39 = [ {
		diaouges : data.string.p7_49
	},//des33

	];

	var characterDialouges40 = [ {
		diaouges : data.string.p7_50
	},//des34

	];


/*****end of page3*********************/

/******page4*************************/



var characterDialouges401 = [ {
		diaouges : data.string.p7_67
	},//example1

	];

var characterDialouges41 = [ {
		diaouges : data.string.p7_68
	},//input04

	];


	var characterDialouges42 = [ {
		diaouges : data.string.p7_69
	},//output04

	];

	var characterDialouges43 = [ {
		diaouges : data.string.p7_70
	},//des35

	];


	var characterDialouges44 = [ {
		diaouges : data.string.p7_71
	},//des36

	];

	var characterDialouges45 = [ {
		diaouges : data.string.p7_72
	},//des37

	];

	var characterDialouges46 = [ {
		diaouges : data.string.p7_73
	},//des38

	];



	var characterDialouges47 = [ {
		diaouges : data.string.p7_74
	},//input05

	];


	var characterDialouges48 = [ {
		diaouges : data.string.p7_75
	},//output05

	];



	var characterDialouges49 = [ {
		diaouges : data.string.p7_76
	},//des39

	];

	var characterDialouges50 = [ {
		diaouges : data.string.p7_77
	},//des40

	];

	var characterDialouges51 = [ {
		diaouges : data.string.p7_78
	},//des41

	];



	var characterDialouges52 = [ {
		diaouges : data.string.p7_79
	},//des42

	];

	var characterDialouges53 = [ {
		diaouges : data.string.p7_80
	},//input06
	];



	var characterDialouges54 = [ {
		diaouges : data.string.p7_81
	},//output06

	];



	var characterDialouges55 = [ {
		diaouges : data.string.p7_82
	},//des43

	];

	var characterDialouges56 = [ {
		diaouges : data.string.p7_83
	},//des44

	];

	var characterDialouges57 = [ {
		diaouges : data.string.p7_84
	},//des45

	];

	var characterDialouges58 = [ {
		diaouges : data.string.p7_85
	},//des46

	];

	var characterDialouges59 = [ {
		diaouges : data.string.p7_86
	},//des47

	];

	var characterDialouges60 = [ {
		diaouges : function () {
		var d = ole.textSR(data.string.p7_87,data.string.p7_88,"<span class='ind2'>"+data.string.p7_88+"</span>");


			return d;
		}


	},


	];


/*******end of page4*****************/


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=37;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,10,19,28]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [function () {
			var d = ole.textSR(ole.textSR(data.string.p7_1,data.string.p7_2,"<span class='ind2'>"+data.string.p7_2+"</span>"), data.string.p7_3, "<span class='ind2'>"+data.string.p7_3+"</span>");

			return d;
		},

		function () {
		var d = ole.textSR(data.string.p7_2,data.string.p7_3,"<span class='ind2'>"+data.string.p7_3+"</span>");
			d = ole.textSR(d,data.string.p7_89,"<span class='ind2'>"+data.string.p7_89+"</span>")
			d = ole.textSR(d,data.string.p7_90,"<span class='ind2'>"+data.string.p7_90+"</span>")

			return d;
		},


		data.string.p7_6],

	},






	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

		data.string.p7_20,
		data.string.p7_21],

	},

/*end of second page */

	/*------------------------------------------------------------------------------------------------*/

/*start of third page*/
	{
		justClass : "third",

			text : [

		data.string.p7_35,
		data.string.p7_36],

	},

/*start of fourth page*/
	{
		justClass : "fourth",

			text : [


		data.string.p7_607],

	}

]















/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};

		/*first call to first*/
		// first();

		function slide_70 () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}


		function slide_71 () {

			$board.find(".wrapperIntro.firstPage .text2").show(0);

		}

		function slide_72 () {
			$board.find(".wrapperIntro.firstPage #output01").hide(0);
			$board.find(".wrapperIntro.firstPage .imageholder01").show(0);
			$("#activity-page-next-btn-enabled").hide(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input01").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges2,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output01").css('top', '64%');
			$("#output01").append(appendDialouge);


			$( ".wrapperIntro.firstPage .imageholder01" ).find(function() {
				$( "#input01" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.firstPage .main-fun-a").delay(2000).hide(0);$(".wrapperIntro.firstPage .main-fun-b").delay(2000).show(0); characterDialouge3();},
					complete: function() {

					$(this).hide(0);
					$('#output01').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge4,
						duration: 2000,
						complete: function() {$("#activity-page-next-btn-enabled").show(0);characterDialouge5();characterDialouge6();characterDialouge7();}


						} )
					}
				} );


			});

		}



		function characterDialouge3(){
			appendDialouge = '<div class="p7_9">';
			var dialouges =$.each(characterDialouges3,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").append(appendDialouge);
		}


		function characterDialouge4(){
			appendDialouge = '<div class="p7_10">';
			var dialouges =$.each(characterDialouges4,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des2").append(appendDialouge);
		}


		function characterDialouge5(){
			appendDialouge = '<div class="p7_11">';
			var dialouges =$.each(characterDialouges5,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des3").append(appendDialouge);
		}


		function characterDialouge6(){
			appendDialouge = '<div class="p7_12">';
			var dialouges =$.each(characterDialouges6,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des4").append(appendDialouge);
		}


		function characterDialouge7(){
			appendDialouge = '<div class="p7_13">';
			var dialouges =$.each(characterDialouges7,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des5").append(appendDialouge);
		$board.find(".wrapperIntro .image1des").css('background','#ddddff none repeat scroll 0 0');
		}







		function characterDialouge8(){
			appendDialouge = '<div class="p7_14">';
			var dialouges =$.each(characterDialouges8,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des6").append(appendDialouge);
		}



		function characterDialouge9(){


			appendDialouge = '<div class="p7_15">';
			var dialouges =$.each(characterDialouges9,function(key,values){

			chDialouges = values.diaouges();

			appendDialouge += chDialouges;
		});
		appendDialouge += '</div>';
		$(".des7").append(appendDialouge);
		}


		function characterDialouge10(){
			appendDialouge = '<div class="p7_16">';
			var dialouges =$.each(characterDialouges10,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des8").append(appendDialouge);
		}


		function characterDialouge11(){
			appendDialouge = '<div class="p7_17">';
			var dialouges =$.each(characterDialouges11,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des9").append(appendDialouge);
		}


		function characterDialouge12(){
			appendDialouge = '<div class="p7_18">';
			var dialouges =$.each(characterDialouges12,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des10").append(appendDialouge);
		}


		function characterDialouge13(){
			appendDialouge = '<div class="p7_19">';
			var dialouges =$.each(characterDialouges13,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des11").append(appendDialouge);
		}



	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


	function second() {

			var source = $("#intro-template01").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);

		};


	function slide_73 () {
			//$board.find(".wrapperIntro.second ,.wrapperIntro.second ").show(0);
			//$board.find(".wrapperIntro.second .text1").css("opacity",1);
			$board.find(".wrapperIntro.second .text1").show(0);



		}




	function slide_74 () {
			$board.find(".wrapperIntro.second #output02").hide(0);
			$board.find(".wrapperIntro.second .imageholder02").show(0);
			$("#activity-page-next-btn-enabled").hide(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges14,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input02").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges15,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output02").css('top', '64%');
			$("#output02").append(appendDialouge);


			$( ".wrapperIntro.second .imageholder02" ).find(function() {
				$( "#input02" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.second .main-fun-c").delay(2000).hide(0);$(".wrapperIntro.second .main-fun-d").delay(2000).show(0); characterDialouge16();},
					complete: function() {

					$(this).hide(0);
					$('#output02').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge17,
						duration: 2000,
						complete: function() {$("#activity-page-next-btn-enabled").show(0); characterDialouge18(); characterDialouge19(); characterDialouge20();}
						} )
					}
				} );


			});

		}


		function characterDialouge16(){
			appendDialouge = '<div class="p7_24">';
			var dialouges =$.each(characterDialouges16,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des12").append(appendDialouge);
		}


		function characterDialouge17(){
			appendDialouge = '<div class="p7_25">';
			var dialouges =$.each(characterDialouges17,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des13").append(appendDialouge);
		}


		function characterDialouge18(){
			appendDialouge = '<div class="p7_26">';
			var dialouges =$.each(characterDialouges18,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des14").append(appendDialouge);
		}


		function characterDialouge19(){
			appendDialouge = '<div class="p7_27">';
			var dialouges =$.each(characterDialouges19,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des15").append(appendDialouge);
		}



		function characterDialouge20(){
			appendDialouge = '<div class="p7_28">';
			var dialouges =$.each(characterDialouges20,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des16").append(appendDialouge);
		$board.find(".wrapperIntro .image2des").css('background','#ddddff none repeat scroll 0 0');
		}


		function characterDialouge21(){
			appendDialouge = '<div class="p7_29">';
			var dialouges =$.each(characterDialouges21,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des17").append(appendDialouge);
		}


		function characterDialouge22(){
			appendDialouge = '<div class="p7_30">';
			var dialouges =$.each(characterDialouges22,function(key,values){
			chDialouges = values.diaouges();

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des18").append(appendDialouge);
		}


		function characterDialouge23(){
			appendDialouge = '<div class="p7_31">';
			var dialouges =$.each(characterDialouges23,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des19").append(appendDialouge);
		}



		function characterDialouge24(){
			appendDialouge = '<div class="p7_32">';
			var dialouges =$.each(characterDialouges24,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des20").append(appendDialouge);
		}


		function characterDialouge25(){
			appendDialouge = '<div class="p7_33">';
			var dialouges =$.each(characterDialouges25,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des21").append(appendDialouge);
		}


		function characterDialouge26(){
			appendDialouge = '<div class="p7_34">';
			var dialouges =$.each(characterDialouges26,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des22").append(appendDialouge);
		}



/***end of page2*******/





/***page 3***************/

	function third() {

			var source = $("#intro-template02").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);

		};


	function slide_75 () {
			//$board.find(".wrapperIntro.second ,.wrapperIntro.second ").show(0);
			//$board.find(".wrapperIntro.second .text1").css("opacity",1);
			$board.find(".wrapperIntro.third .text1").show(0);


		}




	function slide_76 () {
			$board.find(".wrapperIntro.third #output03").hide(0);
			$board.find(".wrapperIntro.third .imageholder03").show(0);
			$("#activity-page-next-btn-enabled").hide(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges27,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input03").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges28,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output03").css('top', '64%');
			$("#output03").append(appendDialouge);


			$( ".wrapperIntro.third .imageholder03" ).find(function() {
				$( "#input03" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.third .main-fun2-e").delay(2000).hide(0);$(".wrapperIntro.third .main-fun2-f").delay(2000).show(0); characterDialouge29();},
					complete: function() {

					$(this).hide(0);
					$('#output03').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge30,
						duration: 2000,
						complete: function() {$("#activity-page-next-btn-enabled").show(0); characterDialouge31(); characterDialouge32(); characterDialouge33(); characterDialouge34(); }
						} )
					}
				} );


			});

		}


		function characterDialouge29(){
			appendDialouge = '<div class="p7_39">';
			var dialouges =$.each(characterDialouges29,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des23").append(appendDialouge);
		}


		function characterDialouge30(){
			appendDialouge = '<div class="p7_40">';
			var dialouges =$.each(characterDialouges30,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des24").append(appendDialouge);
		}


		function characterDialouge31(){
			appendDialouge = '<div class="p7_41">';
			var dialouges =$.each(characterDialouges31,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des25").append(appendDialouge);
		}


		function characterDialouge32(){
			appendDialouge = '<div class="p7_42">';
			var dialouges =$.each(characterDialouges32,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des26").append(appendDialouge);
		}



		function characterDialouge33(){
			appendDialouge = '<div class="p7_43">';
			var dialouges =$.each(characterDialouges33,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des27").append(appendDialouge);
		}


		function characterDialouge34(){
			appendDialouge = '<div class="p7_44">';
			var dialouges =$.each(characterDialouges34,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des28").append(appendDialouge);
		$board.find(".wrapperIntro .image3des").css('background','#ddddff none repeat scroll 0 0');
		}


		function characterDialouge35(){
			appendDialouge = '<div class="p7_45">';
			var dialouges =$.each(characterDialouges35,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des29").append(appendDialouge);
		}


		function characterDialouge36(){
			appendDialouge = '<div class="p7_46">';
			var dialouges =$.each(characterDialouges36,function(key,values){
			chDialouges = values.diaouges();

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des30").append(appendDialouge);
		}



		function characterDialouge37(){
			appendDialouge = '<div class="p7_47">';
			var dialouges =$.each(characterDialouges37,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des31").append(appendDialouge);
		}


		function characterDialouge38(){
			appendDialouge = '<div class="p7_48">';
			var dialouges =$.each(characterDialouges38,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des32").append(appendDialouge);
		}


		function characterDialouge39(){
			appendDialouge = '<div class="p7_49">';
			var dialouges =$.each(characterDialouges39,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des33").append(appendDialouge);
		}


		function characterDialouge40(){
			appendDialouge = '<div class="p7_50">';
			var dialouges =$.each(characterDialouges40,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des34").append(appendDialouge);
		}




/****end of page3********/



/******page 4***********************************************************************************/



function fourth() {

			var source = $("#intro-template03").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);

		};


	function characterDialouge401(){
			appendDialouge = '<div class="p7_67">';
			var dialouges =$.each(characterDialouges401,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".example1").append(appendDialouge);
		}




	function slide_78 () {
			$board.find(".wrapperIntro.fourth #output04").hide(0);
			$board.find(".wrapperIntro.fourth .main-imgholder .imageholder04").show(0);
			$("#activity-page-next-btn-enabled").hide(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges41,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input04").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges42,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output04").css('top', '68%');
			$("#output04").append(appendDialouge);


			$( ".wrapperIntro.fourth .imageholder04" ).find(function() {
				$( "#input04" ).animate({ "top": "+=68%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.fourth .main-fun-a1").delay(2000).hide(0);$(".wrapperIntro.fourth .main-fun-a2").delay(2000).show(0);characterDialouge43();},
					complete: function() {

					$(this).hide(0);
					$('#output04').show(0).animate({ "left": "+=10%" }, {
						start:characterDialouge44,
						duration: 2000,
						complete: function() {$("#activity-page-next-btn-enabled").show(0); characterDialouge45();}
						} )
					}
				} );


			});

		}




		function characterDialouge43(){
			appendDialouge = '<div class="p7_70">';
			var dialouges =$.each(characterDialouges43,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des35").append(appendDialouge);
		}



		function characterDialouge44(){
			appendDialouge = '<div class="p7_71">';
			var dialouges =$.each(characterDialouges44,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des36").append(appendDialouge);
		}


		function characterDialouge45(){
			appendDialouge = '<div class="p7_72">';
			var dialouges =$.each(characterDialouges45,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des37").append(appendDialouge);
		}


		function characterDialouge46(){
			appendDialouge = '<div class="p7_73">';
			var dialouges =$.each(characterDialouges46,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des38").append(appendDialouge);
		}



		function slide_79 () {
			$board.find(".wrapperIntro.fourth #output05").hide(0);
			$board.find(".wrapperIntro.fourth .imageholder05").show(0);
			$("#activity-page-next-btn-enabled").hide(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges47,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input05").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges48,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output05").css('top', '68%');
			$("#output05").append(appendDialouge);


			$( ".wrapperIntro.fourth .imageholder05" ).find(function() {
				$( "#input05" ).animate({ "top": "+=68%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.fourth .main-fun-b1").delay(2000).hide(0);$(".wrapperIntro.fourth .main-fun-b2").delay(2000).show(0); characterDialouge49();},
					complete: function() {

					$(this).hide(0);
					$('#output05').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge50,
						duration: 2000,
						complete: function() {$("#activity-page-next-btn-enabled").show(0); characterDialouge51();}
						} )
					}
				} );


			});

		}




		function characterDialouge49(){
			appendDialouge = '<div class="p7_76">';
			var dialouges =$.each(characterDialouges49,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des39").append(appendDialouge);
		}


		function characterDialouge50(){
			appendDialouge = '<div class="p7_77">';
			var dialouges =$.each(characterDialouges50,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des40").append(appendDialouge);
		}


		function characterDialouge51(){
			appendDialouge = '<div class="p7_78">';
			var dialouges =$.each(characterDialouges51,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des41").append(appendDialouge);
		}


		function characterDialouge52(){
			appendDialouge = '<div class="p7_79">';
			var dialouges =$.each(characterDialouges52,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des42").append(appendDialouge);
		}




	function slide_80 () {
			$board.find(".wrapperIntro.fourth #output06").hide(0);
			$board.find(".wrapperIntro.fourth .imageholder06").show(0);
			$("#activity-page-next-btn-enabled").hide(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges53,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input06").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges54,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output06").css('top', '68%');
			$("#output06").append(appendDialouge);


			$( ".wrapperIntro.fourth .imageholder06" ).find(function() {
				$( "#input06" ).animate({ "top": "+=68%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.fourth .main-fun-c1").delay(2000).hide(0);$(".wrapperIntro.fourth .main-fun-c2").delay(2000).show(0); characterDialouge55();},
					complete: function() {

					$(this).hide(0);
					$('#output06').show(0).animate({ "left": "+=15%" }, {
						start:characterDialouge56,
						duration: 2000,
						complete: function() {$("#activity-page-next-btn-enabled").show(0); characterDialouge57();}
						} )
					}
				} );


			});

		}




		function characterDialouge55(){
			appendDialouge = '<div class="p7_82">';
			var dialouges =$.each(characterDialouges55,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des43").append(appendDialouge);
		}


		function characterDialouge56(){
			appendDialouge = '<div class="p7_83">';
			var dialouges =$.each(characterDialouges56,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des44").append(appendDialouge);
		}


		function characterDialouge57(){
			appendDialouge = '<div class="p7_84">';
			var dialouges =$.each(characterDialouges57,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des45").append(appendDialouge);
		}


		function characterDialouge58(){
			appendDialouge = '<div class="p7_85">';
			var dialouges =$.each(characterDialouges58,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des46").append(appendDialouge);
		}

		function characterDialouge59(){
			appendDialouge = '<div class="p7_86">';
			var dialouges =$.each(characterDialouges59,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des47").append(appendDialouge);
		}

		function characterDialouge60(){
			appendDialouge = '<div class="p7_87">';
			var dialouges =$.each(characterDialouges60,function(key,values){
			chDialouges = values.diaouges();

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des48").append(appendDialouge);
		}

/*****end of page4*****************************************************************************/
		/*********/


		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

	// 	$refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				slide_70,
				slide_71,
				slide_72,
				/*characterDialouge3,
				characterDialouge4,
				characterDialouge5,
				characterDialouge6,
				characterDialouge7,*/
				characterDialouge8,
				characterDialouge9,
				characterDialouge10,
				characterDialouge11,
				characterDialouge12,
				characterDialouge13,
				second,//second
				slide_73,
				slide_74,
				/*characterDialouge16,
				characterDialouge17,
				characterDialouge18,
				characterDialouge19,
				characterDialouge20,*/
				characterDialouge21,
				characterDialouge22,
				characterDialouge23,
				characterDialouge24,
				characterDialouge25,
				characterDialouge26,
				third,//third page
				slide_75,
				slide_76,
				/*characterDialouge29,
				characterDialouge30,
				characterDialouge31,
				characterDialouge32,
				characterDialouge33,
				characterDialouge34,*/
				characterDialouge35,
				characterDialouge36,
				characterDialouge37,
				characterDialouge38,
				characterDialouge39,
				characterDialouge40,

				fourth,

				characterDialouge401,
				slide_78,

				characterDialouge46,
				slide_79,

				characterDialouge52,
				slide_80,

				characterDialouge58,
				characterDialouge59,
				characterDialouge60

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

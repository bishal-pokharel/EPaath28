/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/






$(function () {
	

	var characterDialouges1 = [ {
		diaouges : data.string.p11_18
	},//x1
	
	];
	
	
	var characterDialouges2 = [ {
		diaouges : data.string.p11_19
	},//x2
	
	];
	
	
	var characterDialouges3 = [ {
		diaouges : data.string.p11_20
	},//x3
	
	];
	
	
	var characterDialouges4 = [ {
		diaouges : data.string.p11_21
	},//x4
	
	];
	
	
	var characterDialouges5 = [ {
		diaouges : data.string.p11_22
	},//y1
	
	];
	
	
	var characterDialouges6 = [ {
		diaouges : data.string.p11_23
	},//y2
	
	];
	
	
	
	var characterDialouges7 = [ {
		diaouges : data.string.p11_24
	},//y3
	
	];
	
	
	
	var characterDialouges8 = [ {
		diaouges : data.string.p11_25
	},//y4
	
	];
	
	
	var characterDialouges9 = [ {
		diaouges : data.string.p11_26
	},//ans1
	
	];
	
	var characterDialouges10 = [ {
		diaouges : data.string.p11_27
	},//ans2
	
	];
	
	var characterDialouges11 = [ {
		diaouges : data.string.p11_28
	},//ans3
	
	];
	
	var characterDialouges12 = [ {
		diaouges : data.string.p11_29
	},//ans4
	
	];
	
	
	
	
	
	

	
	
	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=24;
		
	

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,7]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		data.string.p11_1,
		
		
		function () {
			var d = ole.textSR(data.string.p11_2,data.string.p11_3,"<span class='ind2'>"+data.string.p11_3+"</span>");
			d = ole.textSR(d,data.string.p11_4,"<span class='ind2'>"+data.string.p11_4+"</span>")
			return d;
		},
		
		data.string.p11_5,
		function () {
			var d = ole.textSR(data.string.p11_6,data.string.p11_7,"<span class='ind2'>"+data.string.p11_7+"</span>");
			
			return d;
		},
		data.string.p11_8,
		
		function() {
			var d = ole.textSR(data.string.p11_9,data.string.p11_10,"<span class='ind2'>"+data.string.p11_10+"</span>");
			return d;
		},
		
		
		
		function() {
			var d = ole.textSR(data.string.p11_11,data.string.p11_12,"<span class='ind2'>"+data.string.p11_12+"</span>");
			return d;
		},
		
		
		
		
		
		],
		
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
			
			data.string.p11_13,
			data.string.p11_14,
			
			function () {
			var d = ole.textSR(data.string.p11_15,data.string.p11_16,"<span class='ind2'>"+data.string.p11_16+"</span>");
			return d;
			},
			data.string.p11_17
			],
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "third",
		
			text : [function () {
				var d = ole.textSR(data.string.p1_25,data.string.p1_26,"<span class='ind2'>"+data.string.p1_26+"</span>");
				return d;
			}, 
			function () {
				var d = ole.textSR(data.string.p1_27,data.string.p1_28,"<span class='ind2'>"+data.string.p1_28+"</span>");
				d = ole.textSR(d, data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>");
				return d;
			},
			
			data.string.p1_29
				],
	}
	
	
	
	
	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			
		};

		/*first call to first*/
		// first();

		function second () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);
			
		}

		function third () {
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			
		}

		function fourth () {
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			
		}

		function fifth () {
			$board.find(".wrapperIntro.firstPage .text4").show(0);
		}

		function sixth () {
			$board.find(".wrapperIntro.firstPage .text5").show(0);
		}
		
		function seventh () {
			$board.find(".wrapperIntro.firstPage .text6").show(0);
		}
		
		function eighth () {
			$board.find(".wrapperIntro.firstPage .text7").show(0);
		}
		
		
		function ninth () {
			$board.find(".wrapperIntro.firstPage .text8").show(0);
		}
		
		function tenth () {
			$board.find(".wrapperIntro.firstPage .text9").show(0);
		}
	/*-------------------------------------------------------------------------------------------*/
	
	/*second page*/
		
		
		/*text 0 firstline*/
		function beforeClicks () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}
		/*end of text 0 firstline*/
		
		/*first click*/
		function beforeClicks_1 () {
			$board.find(".wrapperIntro.second .text1").show(0);
		}
		
		function beforeClicks_2 () {
			$board.find(".wrapperIntro.second .text2").show(0);
		}
		
		function beforeClicks_3 () {
			$board.find(".wrapperIntro.second .text3").show(0);
		}
		
		function beforeClicks_4 () {
			$board.find(".wrapperIntro.second .tg.tableholder").show(0);
		}
		
		
		function characterDialouge1(){
			appendDialouge = '<div class="p11_18">';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".x1").append(appendDialouge);
		}
		
		
		
		function characterDialouge5(){
			appendDialouge = '<div class="p11_22">';
			var dialouges =$.each(characterDialouges5,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".y1").append(appendDialouge);
		$board.find(".wrapperIntro.second .ans1").show(0);
		
		}
		
		
		
		
		
		function characterDialouge2(){
			$board.find(".wrapperIntro.second .ans1").hide(0);
			appendDialouge = '<div class="p11_19">';
			var dialouges =$.each(characterDialouges2,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".x2").append(appendDialouge);
		}
		
		
		
		
		
		
		
		function characterDialouge6(){
			$board.find(".wrapperIntro.second .ans2").show(0);
			appendDialouge = '<div class="p11_23">';
			var dialouges =$.each(characterDialouges6,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".y2").append(appendDialouge);
		}
		
		
		function characterDialouge3(){
			$board.find(".wrapperIntro.second .ans2").hide(0);
			appendDialouge = '<div class="p11_20">';
			var dialouges =$.each(characterDialouges3,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".x3").append(appendDialouge);
		}
		
		
		function characterDialouge7(){
			$board.find(".wrapperIntro.second .ans3").show(0);
			appendDialouge = '<div class="p11_24">';
			var dialouges =$.each(characterDialouges7,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".y3").append(appendDialouge);
		}
		
		
		function characterDialouge4(){
			$board.find(".wrapperIntro.second .ans3").hide(0);
			appendDialouge = '<div class="p11_21">';
			var dialouges =$.each(characterDialouges4,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".x4").append(appendDialouge);
		}
		
		
		
		function characterDialouge8(){
			$board.find(".wrapperIntro.second .ans4").show(0);
			appendDialouge = '<div class="p11_25">';
			var dialouges =$.each(characterDialouges8,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".y4").append(appendDialouge);
		}
		
		
		
		/**ans1***/
		function characterDialouge9(){
			appendDialouge = '<div class="p11_26">';
			var dialouges =$.each(characterDialouges9,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".ans1").append(appendDialouge);
		}
		
		function characterDialouge10(){
			appendDialouge = '<div class="p11_27">';
			var dialouges =$.each(characterDialouges10,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".ans2").append(appendDialouge);
		}
		
		
		
		function characterDialouge11(){
			appendDialouge = '<div class="p11_28">';
			var dialouges =$.each(characterDialouges11,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".ans3").append(appendDialouge);
		}
		
		
		function characterDialouge12(){
			appendDialouge = '<div class="p11_29">';
			var dialouges =$.each(characterDialouges12,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".ans4").append(appendDialouge);
		}
		
		
		function test(){
			t();
			$("#scatterplot").show(0);
			
		}
	
		
		
		
		/* end of first click*/
		
		
	
		/* end of second click*/
		
		
		
		
	
		
		
		
		

		
		
		
		
		
	
		
		
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function thirdPage () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}
		
		
		function thirdPage1 () {
			$board.find(".wrapperIntro.third .text0").show(0);
			
		}
		
		
		
		
		
		
		
		
	
/*----------------------------------------------------------------------------------------------------*/
		
		
		
		
		
		


		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				second,
				third,
				fourth,
				fifth,
				sixth,
				seventh,
				
				
				beforeClicks, //second one
				
				
				beforeClicks_1,
				beforeClicks_2,
				beforeClicks_3,
				beforeClicks_4,
				characterDialouge1,
				characterDialouge5,
				characterDialouge9,/*ans1*/
				
				characterDialouge2,
				characterDialouge6,
				characterDialouge10,/*ans2*/
				characterDialouge3,
				characterDialouge7,
				characterDialouge11,/*ans3*/
				characterDialouge4,
				characterDialouge8,
				characterDialouge12,/*ans4*/
				test
				
				
				
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				
			}
		}
	/****************/
	
	


});





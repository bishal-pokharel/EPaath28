/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	
	var $board = $('.board'),
	$nextBtn = $("#activity-page-next-btn-enabled"),
	$prevBtn = $(".prevButton"),
	countNext = 0,
	all_page = 5;
	$total_page = 4;
	loadTimelineProgress($total_page,countNext+1);
	
	var qDatas = [
		{term1: "+", term2: "7", term3: "Y", term4 : "/",term5 : "-",term6 : "X", answer : "Y = X + 7 OR Y = 7 + X"},
		{term1: "-", term2: "5", term3: "Y", term4 : "/",term5 : "-5",term6 : "X",term7 : "*", answer : "Y = X - 5 OR Y = -5 + X"},
		{term1: "()", term2: "/", term3: "Y", term4 : "/",term5 : "5",term6 : "X",term7 : "*", answer : "Y = X / 5" },
		{term1: "()*4", term2: "-3",  term3: "Y", term4 : "4",term5 : "*4",term6 : "X",term7 : "*", answer : "Y = X*4 - 3 OR" }
	]
	var qAnsFrst = [
	      		{term1: "Y", term2: "=", term3: "X", term4 : "+",term5 : "7"},
	      		{term1: "Y", term2: "=", term3: "X", term4 : "-",term5 : "5"},
	      		{term1: "Y", term2: "=", term3: "X", term4 : "/",term5 : "5"},
	      		{term1: "Y", term2: "=", term3: "X*4", term4 : "-",term5 : "3"}
	]
	var qAnsSec = [
	      		{term1: "Y", term2: "=", term3: "7", term4 : "+",term5 : "X"},
	      		{term1: "Y", term2: "=", term3: "-5", term4 : "+",term5 : "X"},
	      		{term1: "Y", term2: "=", term3: "X", term4 : "/",term5 : "5"},
	      		{term1: "Y", term2: "=", term3: "X*4", term4 : "-",term5 : "3"}
	]
	
	
	
	
	
	var correctanswer = ["Y","eq","X","+","7"];
	var fruits = ["labour", "doctor", "police", "civilengineer", "farmer", "journalist"];
	var UserAnswer = [];
	var UserAnswerOrder= [];
	var correctanswer = ["Y","eq","X","+","7"];

	var characterDialouges0 = [ {
		diaouges : data.string.p9_13
	},//des0
	
	];
	
	
	
	
	var characterDialouges1 = [ {
		diaouges : data.string.p9_14
	},//des1
	
	];
	
	
	var characterDialouges2 = [ {
		diaouges : data.string.p9_15
	},//des2
	
	];
	
	
	
	var characterDialouges3 = [ {
		diaouges : data.string.p9_16
	},//des3
	
	];
	
	
	
//------------------------------------------------------------------------------------------------//	
	
	

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,10]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		submit:data.string.submit,
		text : [
		
		
		
		
		function () {
			var d = ole.textSR(data.string.p9_1,data.string.p9_2,"<span class='ind2'>"+data.string.p9_2+"</span>");
			return d;
		},
		
		
		data.string.p9_3,
		
		data.string.p9_4,
		
	
		],
		
		
		
		
		
	
		
		
		
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
			
			data.string.p9_13,
			
			
			function () {
			var d = ole.textSR(data.string.p9_15,data.string.p9_16,"<span class='ind2'>"+data.string.p9_16+"</span>");
			return d;
			},
			data.string.p9_17
			],
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "third",
		
			text : [function () {
				var d = ole.textSR(data.string.p1_25,data.string.p1_26,"<span class='ind2'>"+data.string.p1_26+"</span>");
				return d;
			}, 
			function () {
				var d = ole.textSR(data.string.p1_27,data.string.p1_28,"<span class='ind2'>"+data.string.p1_28+"</span>");
				d = ole.textSR(d, data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>");
				return d;
			},
			
			data.string.p1_29
				],
	}
	
	
	
	
	];
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			
			var content = dataObj[0];
			
			
			var html = template(content);
			$board.html(html);
			
			var orgFruits = [qDatas[0].term1,qDatas[0].term2, qDatas[0].term3, qDatas[0].term4, qDatas[0].term5, qDatas[0].term6,qDatas[0].term7];
			var fruits = ["labour", "doctor", "police", "civilengineer", "farmer", "journalist"];
			// Create the pile of shuffled cards
			orgFruits.sort(function() {
				return Math.random() - .5
			});
			//console.log(fruits);
			
			for (var i = 1; i <= 6; i++) { //This is an option one
			
				$('<div id="drag'+orgFruits[i - 1]+'">' + orgFruits[i - 1] + '</div>').data('orgFruits', $.inArray(orgFruits[i - 1], fruits)).appendTo('#cardPile').draggable({
					helper: 'original',
					cursor : 'move',
					revert : 'invalid',
					
				});
			}
			for (var i = 1; i < 6; i++) {
				if(i == 2){
					var appendText="<div id='holder"+i+"' class='col-xs-2 col-sm-2 col-md-2 col-lg-2 slot'><span class='textgen'>=</span></div>";
				}else{
					var appendText="<div id='holder"+i+"' class='col-xs-2 col-sm-2 col-md-2 col-lg-2 slot'></div>";
				}
				
				if(i == 2){
					$(appendText).data('holder', i).appendTo('#cardSlots');
				}else{
					$(appendText).data('holder', i).appendTo('#cardSlots').droppable({
						tolerance: 'touch', 
						drop: handleCardDrop
					});
				}
				
			}
		};
		
		function handleCardDrop(event, ui) {
			$(this).append(ui.draggable);
			$( this ).droppable( "disable" );
			ui.draggable.css('position','absolute');
			ui.draggable.css('left','-1px');
			ui.draggable.css('color','#4a4a4a');
			ui.draggable.css('top','-1px');
			ui.draggable.css('font-size','20px');
			ui.draggable.css('border','1px solid #cccccc');
			ui.draggable.css('background-color','#f7f5f6');
			ui.draggable.css('background-image','linear-gradient(to bottom, #f7f5f6, #dddddd)');
			ui.draggable.css('height','45px');
			ui.draggable.css('width','45px');
			
			ui.draggable.css('text-align','center');
			ui.draggable.css('border-radius','10px');
			ui.draggable.css('padding','7px');
			
			
			
			
			
			 ui.helper.css({width:'45px',height:'45px'});

			var slotNumber = $(this).data('holder');
			var cardNumber = ui.draggable.text();
			
			UserAnswerOrder.push(slotNumber);
			UserAnswer.push(cardNumber);
			$.each(UserAnswerOrder,function(key,val){
				if(UserAnswer[val -1] == correctanswer[val -1]){
					correctAns++;
				}
				
			});
			//console.log("CC_:"+correctAns);
			/*if(correctAns >= 15){
				//alert("Correct");
			}*/
			
			}
		/*first call to first*/
		// first();
		
		$board.on('click','.clicks .check',function () {
			var frst_pos = $('#holder1').find('.ui-draggable').attr('id');
			var scnd_pos = $('#holder3').find('.ui-draggable').attr('id');
			var third_pos = $('#holder4').find('.ui-draggable').attr('id');
			var fourth_pos = $('#holder5').find('.ui-draggable').attr('id');
			
			if(typeof(frst_pos) == 'undefined' || typeof(scnd_pos) == 'undefined' || typeof(third_pos) == 'undefined' || typeof(fourth_pos) == 'undefined'){
				swal("Please fill up all the fields");
			}else{
				
				var eq_result = new Array();
				var frst_pos_str = frst_pos.replace('drag','');
				var scnd_pos_str = scnd_pos.replace('drag','');
				var third_pos_str = third_pos.replace('drag','');
				var fourth_pos_str = fourth_pos.replace('drag','');
				eq_result.push(frst_pos_str);
				eq_result.push('=');
				eq_result.push(scnd_pos_str);
				eq_result.push(third_pos_str);
				eq_result.push(fourth_pos_str);
				
				var result ='not matched';
				var wrong = "<img src='images/wrong.png'>";
				// wrong.src = "images/wrong.png";
				var right = "<img src='images/correct.png'>";
				
				var Frstans = [qAnsFrst[0].term1,qAnsFrst[0].term2, qAnsFrst[0].term3, qAnsFrst[0].term4, qAnsFrst[0].term5];
				var Secndans = [qAnsSec[0].term1,qAnsSec[0].term2, qAnsSec[0].term3, qAnsSec[0].term4, qAnsSec[0].term5];
				var eq_string = eq_result.toString().replace(/\,/g,'');
				var Frst_string = Frstans.toString().replace(/\,/g,'');
				var Secnd_string = Secndans.toString().replace(/\,/g,'');
				
				if(eq_string == Frst_string || eq_string == Secnd_string){
					$('.board').find('.ansArea .check1').html(right);
					$('.board').find('.clicks .click').show(0);
				}else{
					$('.board').find('#cardPile').html('');
					$('.board').find('#cardSlots').html('');
					
					var orgFruits = [qDatas[0].term1,qDatas[0].term2, qDatas[0].term3, qDatas[0].term4, qDatas[0].term5, qDatas[0].term6,qDatas[0].term7];
					
					// Create the pile of shuffled cards
					orgFruits.sort(function() {
						return Math.random() - .5
					});
					//console.log(fruits);
					
					for (var i = 1; i <= 6; i++) { //This is an option one
					
						$('<div id="drag'+orgFruits[i - 1]+'">' + orgFruits[i - 1] + '</div>').data('orgFruits', $.inArray(orgFruits[i - 1], fruits)).appendTo('#cardPile').draggable({
							helper: 'original',
							cursor : 'move',
							revert : 'invalid',
							
						});
					}
					// Create the pile of shuffled cards
					// Create the card slots
					
					for (var i = 1; i < 6; i++) {
						if(i == 2){
							var appendText="<div id='holder"+i+"' class='col-xs-2 col-sm-2 col-md-2 col-lg-2 slot'><span class='textgen'>=</span></div>";
						}else{
							var appendText="<div id='holder"+i+"' class='col-xs-2 col-sm-2 col-md-2 col-lg-2 slot'></div>";
						}
						
						if(i == 2){
							$(appendText).data('holder', i).appendTo('#cardSlots');
						}else{
							$(appendText).data('holder', i).appendTo('#cardSlots').droppable({
								tolerance: 'touch', 
								drop: handleCardDrop
							});
						}
						
					}
					
					$('.board').find('.wrapperIntro .ansArea .check1').html(wrong);
					$('.board').find('.wrapperIntro .ansArea .wrong-note').show(0);
					$('.board').find('.wrapperIntro .ansArea .wrong-note').css('color','#000');
					$('.board').find('.clicks .click').show(0);
				}
			}
		});
		
		$board.on('click','.clicks .click',function (){
			$('.board').find('.showAns').show(0);
			$('.board').find('.showAns').css('color','#000');
			//$("#activity-page-next-btn-enabled").show(0);
		});
		
		function second () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);
			
		}

		function third () {
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			
		}

		function fourth () {
			$board.find(".wrapperIntro.firstPage .input-holder").show(0); /*for input boxex*/
			$(".nextBtn.myNextStyle").hide(0);
			
			
			appendDialouge = '<div class="p9_13">';
			var dialouges =$.each(characterDialouges0,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
			});
			appendDialouge += '</div>';
			$(".des0").append(appendDialouge);
			
			
		
		}
		
		
		function characterDialouge1(){
			$(".nextBtn.myNextStyle").show(0);
			appendDialouge = '<div class="p9_14">';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").append(appendDialouge);
		$(".test").show(0);
		$board.find(".wrapperIntro.firstPage .text1").hide(0);
		$board.find(".wrapperIntro.firstPage .text2").hide(0);
		
		}
		

		function fifth () {
			$board.find(".wrapperIntro.firstPage .text4").show(0);
		}

		function sixth () {
			$board.find(".wrapperIntro.firstPage .text5").show(0);
		}
		
		function seventh () {
			$board.find(".wrapperIntro.firstPage .text6").show(0);
		}
		
		function eighth () {
			$board.find(".wrapperIntro.firstPage .text7").show(0);
		}
		
		
		function ninth () {
			$board.find(".wrapperIntro.firstPage .text8").show(0);
		}
		
		function tenth () {
			$board.find(".wrapperIntro.firstPage .text9").show(0);
		}
		
		function numeric(){
			$board.find(".wrapperIntro.firstPage .des2").show(0);
			$board.find(".wrapperIntro.firstPage .des3").show(0);
		}
	/*-------------------------------------------------------------------------------------------*/
	
	/*second page*/
		
		
		/*text 0 firstline*/
		function beforeClicks () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}
		/*end of text 0 firstline*/
		
		/*first click*/
		function beforeClicks_1 () {
			$board.find(".wrapperIntro.second .text1").show(0);
		}
		
		function beforeClicks_2 () {
			$board.find(".wrapperIntro.second .text2").show(0);
		}
		
		function beforeClicks_3 () {
			$board.find(".wrapperIntro.second .text3").show(0);
		}
		
		function beforeClicks_4 () {
			$board.find(".wrapperIntro.second .tg.tableholder").show(0);
		}
		
		
		
		
		
		
		
		/* end of first click*/
		
		
	
		/* end of second click*/
		
		
		
		
	
		
		
		
		

		
		
		
		
		
	
		
		
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function thirdPage () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}
		
		
		function thirdPage1 () {
			$board.find(".wrapperIntro.third .text0").show(0);
			
		}
		
		
		
		
		
		
		
		
	
/*----------------------------------------------------------------------------------------------------*/
		
		
		
		
		
		


		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				second,
				third,
				fourth,
				characterDialouge1,
				numeric,
				/*fifth,
				sixth,
				seventh,
				
				
				beforeClicks, //second one
				
				
				beforeClicks_1,
				beforeClicks_2,
				beforeClicks_3,
				beforeClicks_4*/
				
			];
			
			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};
			
			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

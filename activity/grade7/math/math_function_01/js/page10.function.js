$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		$total_page = 2;
	loadTimelineProgress($total_page,countNext+1);
	
	var qDatas = [
		{term1: "+", term2: "5", term3: "Y", term4 : "15",term5 : "-",term6 : "X",term7 : "\u00D7", answer : "Y = X - 5 OR Y = -5 + X"},
		{term1: "-", term2: "5", term3: "Y", term4 : "3",term5 : "-5",term6 : "X",term7 : "\u00D7", answer : "Y = X \u00D7 5 OR Y = 5 \u00D7 X"},
	]
	var qAnsFrst = [
	      		{term1: "Y", term2: "=", term3: "X", term4 : "-",term5 : "5"},
	      		{term1: "Y", term2: "=", term3: "X", term4 : "\u00D7",term5 : "5"},
	]
	var qAnsSec = [
	      		{term1: "Y", term2: "=", term3: "-5", term4 : "+",term5 : "X"},
	      		{term1: "Y", term2: "=", term3: "5", term4 : "\u00D7",term5 : "X"},
	]
	var ovalDatasFrst = [
	      		{term1: "10", term2: "15", term3: "20"},
	      		{term1: "1", term2: "2", term3: "3"},
	]
	var ovalDatasScnd = [
	     	      		{term1: "5", term2: "10", term3: "15"},
	     	      		{term1: "5", term2: "10", term3: "15"},
	]
	
	var correctAns = 0;
	var orgFruits = ["eq","X", "Y", "+", "-", "9","*"];
	var fruits = ["labour", "doctor", "police", "civilengineer", "farmer", "journalist"];
	var correctanswer = ["Y","eq","X","+","9"];
	var UserAnswer = [];
	var UserAnswerOrder= [];

/*
* first
*/
	function init() {
		// Hide the success message
		
		var source = $("#intro-template-title").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p10_1,
			qTitle : data.string.p10_2
		}
		var html = template(content);
		$board.html(html);
		// Reset the game
		//correctCards = 0;
		first(qDatas[0],ovalDatasFrst[0],ovalDatasScnd[0]);
		
		//UserAnswer = [];
		
	}
	
	function first(sub,oFrst,oScnd) {
		$('.q'+countNext).remove();
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : data.string.p1_0,
			clickToSee : data.string.p4_3,
			answer : data.string.p4_4,
			baseString : data.string.p4_5,
			wrongnote: data.string.p4_6,
			//powerString : data.string.p2_6,
			term1 : sub.term1,
			term2 : sub.term2,
			term3 : sub.term3,
			term4 : sub.term4,
			term5 : sub.term5,
			term6 : sub.term6,
			term7 : sub.term7,
			sym : sub.sym,
			ans : sub.answer,
			ovalFrstVal1 : oFrst.term1,
			ovalFrstVal2 : oFrst.term2,
			ovalFrstVal3 : oFrst.term3,
			ovalScndVal1 : oScnd.term1,
			ovalScndVal2 : oScnd.term2,
			ovalScndVal3 : oScnd.term3,
			couter : countNext+1
		}
		
		var html = template(content);
		$board.append(html);
		var orgFruits = [sub.term1,sub.term2, sub.term3, sub.term4, sub.term5, sub.term6,sub.term7];
		
		// Create the pile of shuffled cards
		orgFruits.sort(function() {
			return Math.random() - .5
		});
		//console.log(fruits);
		
		for (var i = 1; i <= 7; i++) { //This is an option one
		
			$('<div id="drag'+orgFruits[i - 1]+'">' + orgFruits[i - 1] + '</div>').data('orgFruits', $.inArray(orgFruits[i - 1], fruits)).appendTo('#cardPile').draggable({
				helper: 'original',
				cursor : 'move',
				revert : 'invalid',
				
			});
		}
		// Create the pile of shuffled cards
		// Create the card slots
		
		for (var i = 1; i < 6; i++) {
			if(i == 2){
				var appendText="<div id='holder"+i+"' class='col-xs-2 col-sm-2 col-md-2 col-lg-2 slot'><span class='textgen'>=</span></div>";
			}else{
				var appendText="<div id='holder"+i+"' class='col-xs-2 col-sm-2 col-md-2 col-lg-2 slot'></div>";
			}
			
			if(i == 2){
				$(appendText).data('holder', i).appendTo('#cardSlots');
			}else{
				$(appendText).data('holder', i).appendTo('#cardSlots').droppable({
					tolerance: 'touch', 
					drop: handleCardDrop
				});
			}
			
		}
		
		
		
		// $board.find(".qaArea input.base").focus();
	};

	function handleCardDrop(event, ui) {
					$(this).append(ui.draggable);
					$( this ).droppable( "disable" );
					ui.draggable.css('position','absolute');
					ui.draggable.css('background-image','linear-gradient(to bottom, #f7f5f6, #dddddd)');
					ui.draggable.css('border-radius','4px');
					ui.draggable.css('left','-2%');
					ui.draggable.css('color','#4a4a4a');
					ui.draggable.css('top','-24%');
					ui.draggable.css('text-align','center');
					ui.draggable.css('font-size','30px');
					ui.draggable.css('padding','27%');
					ui.draggable.css('border','solid 1px #ccc');
					ui.helper.css({width:'104%',height:'auto'});

		var slotNumber = $(this).data('holder');
		var cardNumber = ui.draggable.text();
		
		UserAnswerOrder.push(slotNumber);
		UserAnswer.push(cardNumber);
		$.each(UserAnswerOrder,function(key,val){
			if(UserAnswer[val -1] == correctanswer[val -1]){
				correctAns++;
			}
			
		});
		//console.log("CC_:"+correctAns);
		if(correctAns >= 15){
			//alert("Correct");
		}
		
	}
	

	$board.on('click','.clicks .click',function (){
		$('.board').find('.showAns').show(0);
		$('.board').find('.showAns').css('color','#000');
		$("#activity-page-next-btn-enabled").show(0);
	});
	
	$board.on('click','.clicks .check',function () {
		var frst_pos = $('#holder1').find('.ui-draggable').attr('id');
		var scnd_pos = $('#holder3').find('.ui-draggable').attr('id');
		var third_pos = $('#holder4').find('.ui-draggable').attr('id');
		var fourth_pos = $('#holder5').find('.ui-draggable').attr('id');
		
		if(typeof(frst_pos) == 'undefined' || typeof(scnd_pos) == 'undefined' || typeof(third_pos) == 'undefined' || typeof(fourth_pos) == 'undefined'){
			swal("Please fill up all the fields")
		}else{
			
			var eq_result = new Array();
			var frst_pos_str = frst_pos.replace('drag','');
			var scnd_pos_str = scnd_pos.replace('drag','');
			var third_pos_str = third_pos.replace('drag','');
			var fourth_pos_str = fourth_pos.replace('drag','');
			eq_result.push(frst_pos_str);
			eq_result.push('=');
			eq_result.push(scnd_pos_str);
			eq_result.push(third_pos_str);
			eq_result.push(fourth_pos_str);
			
			var result ='not matched';
			var wrong = "<img src='images/wrong.png'>";
			// wrong.src = "images/wrong.png";
			var right = "<img src='images/correct.png'>";
			
			var Frstans = [qAnsFrst[countNext].term1,qAnsFrst[countNext].term2, qAnsFrst[countNext].term3, qAnsFrst[countNext].term4, qAnsFrst[countNext].term5];
			var Secndans = [qAnsSec[countNext].term1,qAnsSec[countNext].term2, qAnsSec[countNext].term3, qAnsSec[countNext].term4, qAnsSec[countNext].term5];
			var eq_string = eq_result.toString().replace(/\,/g,'');
			var Frst_string = Frstans.toString().replace(/\,/g,'');
			var Secnd_string = Secndans.toString().replace(/\,/g,'');
			
			if(eq_string == Frst_string || eq_string == Secnd_string){
				$('.board').find('.ansArea .check1').html(right);
				$('.board').find('.clicks .click').show(0);
			}else{
				$('.board').find('#cardPile').html('');
				$('.board').find('#cardSlots').html('');
				var orgFruits = [qDatas[countNext].term1,qDatas[countNext].term2, qDatas[countNext].term3, qDatas[countNext].term4, qDatas[countNext].term5, qDatas[countNext].term6,qDatas[countNext].term7];
				
				// Create the pile of shuffled cards
				orgFruits.sort(function() {
					return Math.random() - .5
				});
				//console.log(fruits);
				
				for (var i = 1; i <= 7; i++) { //This is an option one
				
					$('<div id="drag'+orgFruits[i - 1]+'">' + orgFruits[i - 1] + '</div>').data('orgFruits', $.inArray(orgFruits[i - 1], fruits)).appendTo('#cardPile').draggable({
						helper: 'original',
						cursor : 'move',
						revert : 'invalid',
						
					});
				}
				// Create the pile of shuffled cards
				// Create the card slots
				
				for (var i = 1; i < 6; i++) {
					if(i == 2){
						var appendText="<div id='holder"+i+"' class='col-xs-2 col-sm-2 col-md-2 col-lg-2 slot'><span class='textgen'>=</span></div>";
					}else{
						var appendText="<div id='holder"+i+"' class='col-xs-2 col-sm-2 col-md-2 col-lg-2 slot'></div>";
					}
					
					if(i == 2){
						$(appendText).data('holder', i).appendTo('#cardSlots');
					}else{
						$(appendText).data('holder', i).appendTo('#cardSlots').droppable({
							tolerance: 'touch', 
							drop: handleCardDrop
						});
					}
					
				}
				$('.board').find('.ansArea .check1').html(wrong);
				$('.board').find('.ansArea .wrong-note').show(0);
				$('.board').find('.ansArea .wrong-note').css('color','#000');
				$('.board').find('.clicks .click').show(0);
			}
		}
	});

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.click(function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext],ovalDatasFrst[countNext],ovalDatasScnd[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});
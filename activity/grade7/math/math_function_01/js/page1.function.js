/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var characterDialouges1 = [ {
		diaouges : data.string.p1_16
	},//

	];

	var characterDialouges2 = [ {
		diaouges : data.string.p1_17
	},//

	];

	var characterDialouges3 = [ {
		diaouges : data.string.p1_18
	},//

	];


	var characterDialouges4 = [ {
		diaouges : ole.textSR(data.string.p1_19,data.string.p1_20,"<span class='green'>"+data.string.p1_20+"</span>")

	},//

	];




	var characterDialouges5 = [ {
		diaouges : data.string.p1_21
	},//

	];

	var characterDialouges6 = [ {
		diaouges : data.string.p1_22
	},//

	];

	var characterDialouges7 = [ {
		diaouges : data.string.p1_23
	},//

	];

	var characterDialouges8 = [ {
		diaouges : data.string.p1_24
	},//

	];

	//end of 2nd page//


	//3rd page//

	var characterDialouges9 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p1_27,data.string.p1_28,"<span class='ind2'>"+data.string.p1_28+"</span>"), data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>")

	},//

	];

	var characterDialouges10 = [ {
		diaouges : data.string.p1_30
	},//
	];

	var characterDialouges11 = [ {
		diaouges : data.string.p1_31
	},//
	];

	var characterDialouges12 = [ {
		diaouges : data.string.p1_32
	},//
	];

	var characterDialouges13 = [ {
		diaouges : data.string.p1_33
	},//
	];

	var characterDialouges14 = [ {
		diaouges : data.string.p1_34
	},//
	];

	var characterDialouges15 = [ {
		diaouges : data.string.p1_35
	},//
	];

	var characterDialouges16 = [ {
		diaouges : data.string.p1_36
	},//fullname
	];

	var characterDialouges17 = [ {
		diaouges : data.string.p1_37
	},//firstname
	];


	var characterDialouges18 = [ {
		diaouges : data.string.p1_38
	},
	];

	var characterDialouges19 = [ {
		diaouges : data.string.p1_39
	},
	];

	var characterDialouges20 = [ {
		diaouges : data.string.p1_40
	},
	];


	var characterDialouges21 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p1_41,data.string.p1_42,"<span class='ind2'>"+data.string.p1_42+"</span>"), data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>")

	},//

	];


	//end of 3rd page//


	//page4//

	var characterDialouges22 = [ {
		diaouges : data.string.p1_49
	},//input
	];

	var characterDialouges23 = [ {
		diaouges : data.string.p1_50
	},//output
	];

	var characterDialouges24 = [ {
		diaouges : data.string.p1_51
	},
	];

	var characterDialouges25 = [ {
		diaouges : data.string.p1_52
	},
	];

	var characterDialouges26 = [ {
		diaouges : data.string.p1_53

	},
	];


	var characterDialouges27 = [ {
		diaouges : data.string.p1_54
	},//input02
	];

	var characterDialouges28 = [ {
		diaouges : data.string.p1_55
	},//output02
	];


	var characterDialouges29 = [ {
		diaouges : data.string.p1_56

	},
	];

	var characterDialouges30 = [ {
		diaouges : data.string.p1_57

	},
	];

	var characterDialouges31 = [ {
		diaouges : data.string.p1_58

	},
	];
	//end of page4//


	//page5//


	var characterDialouges32 = [ {
		diaouges : data.string.p1_63
	},//input
	];

	var characterDialouges33 = [ {
		diaouges : data.string.p1_64
	},//output
	];

	var characterDialouges34 = [ {
		diaouges : data.string.p1_65
	},
	];

	var characterDialouges35 = [ {
		diaouges : data.string.p1_66
	},
	];

	var characterDialouges36 = [ {
		diaouges : data.string.p1_67
	},
	];


	var characterDialouges37 = [ {
		diaouges : data.string.p1_68
	},//input
	];

	var characterDialouges38 = [ {
		diaouges : data.string.p1_69
	},//output
	];


	var characterDialouges39 = [ {
		diaouges : data.string.p1_70
	},
	];

	var characterDialouges40 = [ {
		diaouges : data.string.p1_71
	},
	];

	var characterDialouges41 = [ {
		diaouges : data.string.p1_72
	},//output
	];




	//end of page5//

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=30;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,9,15,22,27]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		coverpage:true,
		justClass : "firstPage_sec",
		cover_src: $ref+"/image/coverpage.png",
		covertext: data.lesson.chapter

	},{
		justClass : "firstPage",
		animate : "true",
		text : [function () {
			var d = ole.textSR(data.string.p1_1,data.string.p1_2,"<span class='ind2'>"+data.string.p1_2+"</span>");

			return d;
		},function () {
			var d = ole.textSR(data.string.p1_3,data.string.p1_2,"<span class='ind2'>"+data.string.p1_2+"</span>");
			d = ole.textSR(d,data.string.p1_4,"<span class='ind5'>"+data.string.p1_4+"</span>")
			d = ole.textSR(d,data.string.p1_5,"<span class='ind5'>"+data.string.p1_5+"</span>")
			d = ole.textSR(d,data.string.p1_6,"<span class='ind5'>"+data.string.p1_6+"</span>")
			return d;
		},function() {
			var d = ole.textSR(data.string.p1_7,"<span class='ind2'>"+data.string.p1_7+"</span>");
			return d;
		},
		data.string.p1_8,
		data.string.p1_9,
		data.string.p1_10,
		data.string.p1_11,
		data.string.p1_12],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [function () {
			var d = ole.textSR(data.string.p1_13,data.string.p1_2,"<span class='ind2'>"+data.string.p1_2+"</span>");
			return d;
			},function () {
				var d = ole.textSR(data.string.p1_14,data.string.p1_15,"<span class='yellow'>"+data.string.p1_15+"</span>");
				return d;
			}],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [function () {
				var d = ole.textSR(data.string.p1_25,data.string.p1_26,"<span class='ind2'>"+data.string.p1_26+"</span>");
				return d;
			},
			function () {
				var d = ole.textSR(data.string.p1_27,data.string.p1_28,"<span class='ind2'>"+data.string.p1_28+"</span>");
				d = ole.textSR(d, data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>");
				return d;
			},

			data.string.p1_29
				],
	},



	{
		justClass : "fourth",

			text : [function () {
			var d = ole.textSR(ole.textSR(data.string.p1_43,data.string.p1_44,"<span class='ind2'>"+data.string.p1_44+"</span>"), data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>");
			return d;
			},

			function () {
			var d = ole.textSR(ole.textSR(data.string.p1_45,data.string.p1_46,"<span class='ind2'>"+data.string.p1_46+"</span>"), data.string.p1_47, "<span class='ind2'>"+data.string.p1_47+"</span>");
			return d;
			},

			data.string.p1_48

			],
	},


	{
		justClass : "fifth",

			text : [function () {
			var d = ole.textSR(ole.textSR(data.string.p1_59,data.string.p1_60,"<span class='ind2'>"+data.string.p1_60+"</span>"), data.string.p1_61, "<span class='ind2'>"+data.string.p1_61+"</span>");
			return d;
			},


			data.string.p1_62

			],
	}


	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};

		// added to add coverpage
			function first_sec() {

				var source = $("#intro-template").html();
				var template = Handlebars.compile(source);
				var content = dataObj[1];
				var html = template(content);
				$board.html(html);

			};

		/*first call to first*/
		// first();

		function second () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}

		function third () {
			$board.find(".wrapperIntro.firstPage .text2").show(0);

		}

		function fourth () {
			$board.find(".wrapperIntro.firstPage .text3").show(0);

		}

		function fifth () {
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .blackboard.img-responsive").show(0);
		}

		function sixth () {
			$board.find(".wrapperIntro.firstPage .text5").show(0);
		}

		function seventh () {
			$board.find(".wrapperIntro.firstPage .text6").show(0);
		}

		function eighth () {
			$board.find(".wrapperIntro.firstPage .text7").show(0);
		}
	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function beforeClicks () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}
		/*end of text 0 firstline*/

		/*first click*/
		function beforeClicks_1 () {
			$board.find(".wrapperIntro.second .text1").show(0);
		}
		/* end of first click*/


		/* second click*/
		function beforeClicks_2 () {
			$board.find(".wrapperIntro.second #ball_2").hide(0);
			$board.find(".wrapperIntro.second .imageholder01").show(0);
			$(".nextBtn.myNextStyle").hide(0);

			$( ".wrapperIntro.second .imageholder01" ).find(function() {
				$( "#ball_1" ).animate({ "top": "+=65%" }, {
					duration: 2000,
					complete: function() {$(this).css('background', '#ff0000'); characterDialouge2();},
					start: characterDialouge1
				} ).animate({ "left": "+=43%" }, {
					duration: 2000,
					complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge3();}
					//complete: characterDialouge3; $(".nextBtn.myNextStyle").hide(0);
				} );


			});

		}
		/* end of second click*/

		/* third click*/
		function characterDialouge1(){
			appendDialouge = '<div class="p1_16">';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").append(appendDialouge);
		}
		/* end of third click*/


		/* fourth click*/
		function characterDialouge2(){
			appendDialouge = '<div class="p1_17">';
			var dialouges =$.each(characterDialouges2,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des2").append(appendDialouge);
		}
		/* end of fourth click*/





		/* fifth click*/
		function characterDialouge3(){
			appendDialouge = '<div class="p1_18">';
			var dialouges =$.each(characterDialouges3,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des3").append(appendDialouge);
		$board.find(".wrapperIntro .image1des").css('background','#ddddff none repeat scroll 0 0');
		}
		/* end of fifth click*/





		/* sixth click*/


		function characterDialouge4(){
			appendDialouge = '<div class="p1_19">';
			var dialouges =$.each(characterDialouges4,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des4").append(appendDialouge);
		}



		/* end of sixth click*/


		/* seventh click*/

		function beforeClicks_3 () {
			$board.find(".wrapperIntro.second #ball_2").hide(0);
			$board.find(".wrapperIntro.second .imageholder02").show(0);
			$(".nextBtn.myNextStyle").hide(0);


			$( ".wrapperIntro.second .imageholder02" ).find(function() {
				$( "#ball_3" ).animate({ "top": "+=65%" }, {
					duration: 2000,
					start:characterDialouge5,
					complete: function() {$(this).css('background', '#ff0000');characterDialouge6();}

				} ).animate({ "left": "+=43%" }, {
					duration: 2000,
					complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge7();}
					//complete: characterDialouge7

				} );


			});

		}

		/* end of seventh click*/



		/* 8th click*/

		function characterDialouge5(){
			appendDialouge = '<div class="p1_21">';
			var dialouges =$.each(characterDialouges5,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des5").append(appendDialouge);
		}

		/* end of 8th click*/




		/* 9th click*/

		function characterDialouge6(){
			appendDialouge = '<div class="p1_22">';
			var dialouges =$.each(characterDialouges6,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des6").append(appendDialouge);
		}

		/* end of 9th click*/



		/* 10th click*/

		function characterDialouge7(){
			appendDialouge = '<div class="p1_23">';
			var dialouges =$.each(characterDialouges7,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des7").append(appendDialouge);
		$board.find(".wrapperIntro .image2des").css('background','#ddddff none repeat scroll 0 0');
		}





		function characterDialouge8(){
			appendDialouge = '<div class="p1_24">';
			var dialouges =$.each(characterDialouges8,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des8").append(appendDialouge);
		$(".des8").css('border', '2px solid #538cdb');
		}



		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function thirdPage () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}


		function thirdPage1 () {
			$board.find(".wrapperIntro.third .text0").show(0);

		}


		function characterDialouge9(){
			$('.third .text1').show(0);
		}


		function characterDialouge10(){
			$('.third .text2').show(0);
		}


		function beforeClicks_4 () {
			$board.find(".wrapperIntro.third #firstname").hide(0);
			$board.find(".wrapperIntro.third .imageholder03").show(0);
			$(".nextBtn.myNextStyle").hide(0);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges10,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#fullname").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges11,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#firstname").css('top', '65%');
			$("#firstname").append(appendDialouge);


			$( ".wrapperIntro.third .imageholder03" ).find(function() {
				$( "#fullname" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start:characterDialouge12,
					complete: function() {

					$(this).hide(0);
					characterDialouge13();
					$('#firstname').show(0).animate({ "left": "+=50%" }, {
						duration: 2000,
						complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge14();}

						} )
					}


				} );


			});

		}

		function characterDialouge12(){
			appendDialouge = '<div class="p1_32">';
			var dialouges =$.each(characterDialouges12,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des9").append(appendDialouge);

		}


		function characterDialouge13(){
			appendDialouge = '<div class="p1_33">';
			var dialouges =$.each(characterDialouges13,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des10").append(appendDialouge);
		}

		function characterDialouge14(){
			appendDialouge = '<div class="p1_34">';
			var dialouges =$.each(characterDialouges14,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des11").append(appendDialouge);
		$board.find(".wrapperIntro .image3des").css('background','#ddddff none repeat scroll 0 0');
		}

		function characterDialouge15(){
			appendDialouge = '<div class="p1_35">';
			var dialouges =$.each(characterDialouges15,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des12").append(appendDialouge);
		}




		function beforeClicks_5 () {
			$board.find(".wrapperIntro.third #firstname01").hide(0);
			$board.find(".wrapperIntro.third .imageholder04").show(0);
			$(".nextBtn.myNextStyle").hide(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges16,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#fullname01").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges17,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#firstname01").css('top', '65%');
			$("#firstname01").append(appendDialouge);


			$( ".wrapperIntro.third .imageholder04" ).find(function() {
				$( "#fullname01" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start:characterDialouge18,
					complete: function() {

					$(this).hide(0);
					characterDialouge19();
					$('#firstname01').show(0).animate({ "left": "+=50%" }, {

						duration: 2000,
						complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge20();}
						//complete:characterDialouge20
						} )
					}
				} );


			});

		}



		function characterDialouge18(){
			appendDialouge = '<div class="p1_38">';
			var dialouges =$.each(characterDialouges18,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des13").append(appendDialouge);
		}


		function characterDialouge19(){
			appendDialouge = '<div class="p1_39">';
			var dialouges =$.each(characterDialouges19,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des14").append(appendDialouge);
		}



		function characterDialouge20(){
			appendDialouge = '<div class="p1_40">';
			var dialouges =$.each(characterDialouges20,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des15").append(appendDialouge);
		$board.find(".wrapperIntro .image4des").css('background','#ddddff none repeat scroll 0 0');
		}


		function characterDialouge21(){
			appendDialouge = '<div class="p1_41">';
			var dialouges =$.each(characterDialouges21,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des16").append(appendDialouge);
		}

/*----------------------------------------------------------------------------------------------------*/


		function fourthPage () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}


		function fourthPage1 () {
			$board.find(".wrapperIntro.fourth .text0").show(0);

		}


		function fourthPage2 () {
			$board.find(".wrapperIntro.fourth .text1").show(0);

		}


		function fourthPage3 () {
			$board.find(".wrapperIntro.fourth .text2").show(0);

		}


		function beforeClicks_6 () {
			$board.find(".wrapperIntro.fourth #output01").hide(0);
			$board.find(".wrapperIntro.fourth .imageholder05").show(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges22,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input01").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges23,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output01").css('top', '64%');
			$("#output01").append(appendDialouge);
			$(".nextBtn.myNextStyle").hide(0);


			$( ".wrapperIntro.fourth .imageholder05" ).find(function() {
				$( "#input01" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.fourth .main-fun").delay(2000).hide(0);$(".wrapperIntro.fourth .main-fun01").delay(2000).show(0); characterDialouge24();},
					complete: function() {

					$(this).hide(0);
					$('#output01').show(0).animate({ "left": "+=40%" }, {

						start:characterDialouge25,
						duration: 2000,
						complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge26();}

						} )
					}
				} );


			});

		}


		function characterDialouge24(){
			appendDialouge = '<div class="p1_51">';
			var dialouges =$.each(characterDialouges24,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des17").append(appendDialouge);
		}



		function characterDialouge25(){
			appendDialouge = '<div class="p1_52">';
			var dialouges =$.each(characterDialouges25,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des18").append(appendDialouge);
		}


		function characterDialouge26(){
			appendDialouge = '<div class="p1_53">';
			var dialouges =$.each(characterDialouges26,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des19").append(appendDialouge);
		$board.find(".wrapperIntro .image5des").css('background','#ddddff none repeat scroll 0 0');
		}




		function beforeClicks_7 () {
			$board.find(".wrapperIntro.fourth #output02").hide(0);
			$board.find(".wrapperIntro.fourth .imageholder06").show(0);
			$(".nextBtn.myNextStyle").hide(0);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges27,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input02").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges28,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output02").css('top', '64%');
			$("#output02").append(appendDialouge);


			$( ".wrapperIntro.fourth .imageholder06" ).find(function() {
				$( "#input02" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.fourth .main-fun-a").delay(2000).hide(0);$(".wrapperIntro.fourth .main-fun-b").delay(2000).show(0); characterDialouge29();},
					complete: function() {

					$(this).hide(0);
					$('#output02').show(0).animate({ "left": "+=40%" }, {

						start:characterDialouge30,
						duration: 2000,
						complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge31();}
						} )
					}
				} );


			});

		}



		function characterDialouge29(){
			appendDialouge = '<div class="p1_56">';
			var dialouges =$.each(characterDialouges29,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des20").append(appendDialouge);
		}

		function characterDialouge30(){
			appendDialouge = '<div class="p1_57">';
			var dialouges =$.each(characterDialouges30,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des21").append(appendDialouge);
		}


		function characterDialouge31(){
			appendDialouge = '<div class="p1_58">';
			var dialouges =$.each(characterDialouges31,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des22").append(appendDialouge);
		$board.find(".wrapperIntro .image6des").css('background','#ddddff none repeat scroll 0 0');
		}
/*******page4 end*************************************************************************************/

/*****page5******************************************************************************************/



	function fifthPage () {
			var source = $("#intro5-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[5]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}


		function fifthPage1 () {
			$board.find(".wrapperIntro.fifth .text0").show(0);

		}

		function fifthPage2 () {
			$board.find(".wrapperIntro.fifth .text1").show(0);

		}


		function beforeClicks_8 () {
			$board.find(".wrapperIntro.fifth #output03").hide(0);
			$board.find(".wrapperIntro.fifth .imageholder07").show(0);
			$(".nextBtn.myNextStyle").hide(0);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges32,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input03").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges33,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output03").css('top', '64%');
			$("#output03").append(appendDialouge);


			$( ".wrapperIntro.fifth .imageholder07" ).find(function() {
				$( "#input03" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.fifth .main-fun-c").delay(2000).hide(0);$(".wrapperIntro.fifth .main-fun-d").delay(2000).show(0); characterDialouge34();},
					complete: function() {

					$(this).hide(0);
					$('#output03').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge35,
						duration: 2000,
						complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge36();}
						} )
					}
				} );


			});

		}


		function characterDialouge34(){
			appendDialouge = '<div class="p1_65">';
			var dialouges =$.each(characterDialouges34,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des23").append(appendDialouge);
		}


			function characterDialouge35(){
			appendDialouge = '<div class="p1_66">';
			var dialouges =$.each(characterDialouges35,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des24").append(appendDialouge);
		}


		function characterDialouge36(){
			appendDialouge = '<div class="p1_67">';
			var dialouges =$.each(characterDialouges36,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des25").append(appendDialouge);
		$board.find(".wrapperIntro .image7des").css('background','#ddddff none repeat scroll 0 0');
		}





		function beforeClicks_9 () {
			$board.find(".wrapperIntro.fifth #output04").hide(0);
			$board.find(".wrapperIntro.fifth .imageholder08").show(0);
			$(".nextBtn.myNextStyle").hide(0);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges37,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input04").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges38,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output04").css('top', '64%');
			$("#output04").append(appendDialouge);


			$( ".wrapperIntro.fifth .imageholder08" ).find(function() {
				$( "#input04" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.fifth .main-fun-e").delay(2000).hide(0);$(".wrapperIntro.fifth .main-fun-f").delay(2000).show(0); characterDialouge39();},
					complete: function() {

					$(this).hide(0);
					$('#output04').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge40,
						duration: 2000,
						complete: function() {characterDialouge41();}
						} )
					}
				} );


			});

		}




		function characterDialouge39(){
			appendDialouge = '<div class="p1_70">';
			var dialouges =$.each(characterDialouges39,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des26").append(appendDialouge);
		}


		function characterDialouge40(){
			appendDialouge = '<div class="p1_71">';
			var dialouges =$.each(characterDialouges40,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des27").append(appendDialouge);
		}


		function characterDialouge41(){
			appendDialouge = '<div class="p1_72">';
			var dialouges =$.each(characterDialouges41,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des28").append(appendDialouge);
		$board.find(".wrapperIntro .image8des").css('background','#ddddff none repeat scroll 0 0');
		}




/****end of page5***********************************************************************************/

		/*base and indecis*/
		function baseAndIndices () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			baseAndIndicesClick = [null,null];
			$nextBtn.hide(0);
		}

		$board.on('click','.wrapperIntro.baseAndIndices .text1',function () {
			// $(this).css('background',"#FF9900");
			$board.find('.maths .base').css('color',"#FF9900").addClass("animated infinite flash");
			$board.find('.maths .power').css('color',"#434343").removeClass("animated infinite flash");
			baseAndIndicesClick[0]=1;
			callNextChecker();
		});


		$board.on('click','.wrapperIntro.baseAndIndices .text2',function () {
			// $(this).css('color',"#6600CC");
			//remove class animate
			$board.find('.maths .base').css('color',"#434343").removeClass("animated infinite flash");
			//add class animate
			$board.find('.maths .power').css('color',"#FF9900").addClass("animated infinite flash");
			baseAndIndicesClick[1]=1;
			callNextChecker();
		});

		function callNextChecker () {
			var nextChecker = true;
			for (var i = 0; i < baseAndIndicesClick.length; i++) {
				if (baseAndIndicesClick[i] ===null){
					nextChecker = false;
					break;
				}
			};
			if (nextChecker) {
				$nextBtn.show(0);
			};
		}
		/*end of base and indecis*/

		function baseAndIndices2 () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}

		function baseAndIndices2_1 () {
			$board.find(".wrapperIntro.baseAndIndices2 .text2").show(0);
			$board.find(".wrapperIntro.baseAndIndices2 .maths0 .base,.wrapperIntro.baseAndIndices2 .maths1 .base").addClass("animated flash");
		}

		function baseAndIndices2_2 () {
			$board.find(".wrapperIntro.baseAndIndices2 .text2,"
				+".wrapperIntro.baseAndIndices2 .text0,"
				+".wrapperIntro.baseAndIndices2 .maths0,"
				+".wrapperIntro.baseAndIndices2 .maths1").hide(0);

			$board.find(".wrapperIntro.baseAndIndices2 .text1,"
				+".wrapperIntro.baseAndIndices2 .maths2,"
				+".wrapperIntro.baseAndIndices2 .maths3").show(0);

		}

		function baseAndIndices2_3 () {
			$board.find(".wrapperIntro.baseAndIndices2 .text3").show(0);
			$board.find(".wrapperIntro.baseAndIndices2 .maths .power").addClass("animated flash");
		}
		/*********/

		function lastBaseIndices () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}


		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;
		switch (countNext) {
			case 0:
				$(".blackboard").hide(0);
			break;
			case 1:
				$(".coverpage").hide(0);
			break;
			default:

		}

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			countNext==1?first_sec():"";
			fnSwitcher();
		});
	//
	// 	$refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first_sec,
				second,
				third,
				fourth,
				fifth,
				sixth,
				seventh,
				eighth,
				beforeClicks, //second one


				beforeClicks_1,
				beforeClicks_2,
				characterDialouge4,
			   	beforeClicks_3,
				characterDialouge8,



				thirdPage,//third one
				characterDialouge9,
				characterDialouge10,
				beforeClicks_4,


				characterDialouge15,
				beforeClicks_5,

				characterDialouge21,


				fourthPage,//fourthPage
				fourthPage2,
				fourthPage3,
				beforeClicks_6,
				beforeClicks_7,



				fifthPage,//fifthPage
				fifthPage2,
				beforeClicks_8,
				/*characterDialouge34,
				characterDialouge35,
				characterDialouge36,*/
				beforeClicks_9,
				/*characterDialouge39,
				characterDialouge40,
				characterDialouge41*/



			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

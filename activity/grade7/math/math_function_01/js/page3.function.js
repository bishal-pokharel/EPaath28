/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

	var characterDialouges1 = [ {
		diaouges : data.string.p3_4
	},//input01

	];


	var characterDialouges2 = [ {
		diaouges : data.string.p3_5
	},//output01

	];



	var characterDialouges3 = [ {
		diaouges : data.string.p3_6
	},//des1

	];



	var characterDialouges4 = [ {
		diaouges : data.string.p3_7
	},//des2

	];


	var characterDialouges5 = [ {
		diaouges : data.string.p3_8
	},//des3

	];

	var characterDialouges6 = [ {
		diaouges : data.string.p3_9
	},//input02

	];


	var characterDialouges7 = [ {
		diaouges : data.string.p3_10
	},//output02

	];

	var characterDialouges8 = [ {
		diaouges : data.string.p3_11
	},//des4

	];

	var characterDialouges9 = [ {
		diaouges : data.string.p3_12
	},//des5
	];

	var characterDialouges10 = [ {
		diaouges : data.string.p3_13
	},//des6

	];

/*page1 end*************************************************/




/***************page2******************/

	var characterDialouges11 = [ {
		diaouges : data.string.p3_17
	},//input03

	];


	var characterDialouges12 = [ {
		diaouges : data.string.p3_18
	},//output03

	];


	var characterDialouges13 = [ {
		diaouges : data.string.p3_19
	},//des7

	];

	var characterDialouges14 = [ {
		diaouges : data.string.p3_20
	},//des8
	];

	var characterDialouges15 = [ {
		diaouges : data.string.p3_21
	},//des9
	];

	var characterDialouges16 = [ {
		diaouges : data.string.p3_22
	},//input04

	];


	var characterDialouges17 = [ {
		diaouges : data.string.p3_23
	},//output04

	];

	var characterDialouges18 = [ {
		diaouges : data.string.p3_24
	},//des10

	];

	var characterDialouges19 = [ {
		diaouges : data.string.p3_25
	},//des11
	];

	var characterDialouges20 = [ {
		diaouges : data.string.p3_26
	},//des12
	];



/*****end of page2*********************/




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=7;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,4]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [function () {
			var d = ole.textSR(ole.textSR(data.string.p3_1,data.string.p3_2,"<span class='ind2'>"+data.string.p3_2+"</span>"), data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>");

			return d;
		},

		data.string.p3_3],

	},






	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [function () {
			var d = ole.textSR(ole.textSR(data.string.p3_14,data.string.p3_15,"<span class='ind2'>"+data.string.p3_15+"</span>"), data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>");

			return d;
		},

		data.string.p3_16],

	}

]






	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/








/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};

		/*first call to first*/
		// first();

		function slide_30 () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}


		function slide_31 () {
			$board.find(".wrapperIntro.firstPage #output01").hide(0);
			$board.find(".wrapperIntro.firstPage .imageholder01").show(0);
			$(".nextBtn.myNextStyle").hide(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input01").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges2,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output01").css('top', '64%');
			$("#output01").append(appendDialouge);


			$( ".wrapperIntro.firstPage .imageholder01" ).find(function() {
				$( "#input01" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.firstPage .main-fun-a").delay(2000).hide(0);$(".wrapperIntro.firstPage .main-fun-b").delay(2000).show(0); characterDialouge3();},
					complete: function() {

					$(this).hide(0);
					$('#output01').show(0).animate({ "left": "+=40%" }, {

						start:characterDialouge4,
						duration: 2000,
						complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge5();}
						} )
					}
				} );


			});

		}



		function characterDialouge3(){
			appendDialouge = '<div class="p3_6">';
			var dialouges =$.each(characterDialouges3,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").append(appendDialouge);
		}


		function characterDialouge4(){
			appendDialouge = '<div class="p3_7">';
			var dialouges =$.each(characterDialouges4,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des2").append(appendDialouge);
		}


		function characterDialouge5(){
			appendDialouge = '<div class="p3_8">';
			var dialouges =$.each(characterDialouges5,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des3").append(appendDialouge);
		$board.find(".wrapperIntro .image1des").css('background','#ddddff none repeat scroll 0 0');
		}




		function slide_32 () {
			$board.find(".wrapperIntro.firstPage #output02").hide(0);
			$board.find(".wrapperIntro.firstPage .imageholder02").show(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges6,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input02").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges7,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output02").css('top', '64%');
			$("#output02").append(appendDialouge);


			$( ".wrapperIntro.firstPage .imageholder02" ).find(function() {
				$( "#input02" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.firstPage .main-fun-c").delay(2000).hide(0);$(".wrapperIntro.firstPage .main-fun-d").delay(2000).show(0); characterDialouge8();},
					complete: function() {

					$(this).hide(0);
					$('#output02').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge9,
						duration: 2000,
						complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge10();}
						} )
					}
				} );


			});

		}



		function characterDialouge8(){
			appendDialouge = '<div class="p3_11">';
			var dialouges =$.each(characterDialouges8,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des4").append(appendDialouge);
		}



		function characterDialouge9(){
			appendDialouge = '<div class="p3_12">';
			var dialouges =$.each(characterDialouges9,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des5").append(appendDialouge);
		}


		function characterDialouge10(){
			appendDialouge = '<div class="p3_13">';
			var dialouges =$.each(characterDialouges10,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des6").append(appendDialouge);
		$board.find(".wrapperIntro .image2des").css('background','#ddddff none repeat scroll 0 0');
		}



	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


	function second() {

			var source = $("#intro-template01").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);

		};



	function slide_33 () {
			$board.find(".wrapperIntro.second .text1").show(0);


		}



	function slide_34 () {
			$board.find(".wrapperIntro.second #output03").hide(0);
			$board.find(".wrapperIntro.second .imageholder03").show(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges11,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input03").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges12,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output03").css('top', '64%');
			$("#output03").append(appendDialouge);


			$( ".wrapperIntro.second .imageholder03" ).find(function() {
				$( "#input03" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.second .main-fun-e").delay(2000).hide(0);$(".wrapperIntro.second .main-fun-f").delay(2000).show(0); characterDialouge13();},
					complete: function() {

					$(this).hide(0);
					$('#output03').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge14,
						duration: 2000,
						complete: function() {$(".nextBtn.myNextStyle").show(0); characterDialouge15();}
						} )
					}
				} );


			});

		}


		function characterDialouge13(){
			appendDialouge = '<div class="p3_19">';
			var dialouges =$.each(characterDialouges13,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des7").append(appendDialouge);

		}


		function characterDialouge14(){
			appendDialouge = '<div class="p3_20">';
			var dialouges =$.each(characterDialouges14,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des8").append(appendDialouge);
		}


		function characterDialouge15(){
			appendDialouge = '<div class="p3_21">';
			var dialouges =$.each(characterDialouges15,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des9").append(appendDialouge);
		$board.find(".wrapperIntro .image3des").css('background','#ddddff none repeat scroll 0 0');
		}



		function slide_35 () {
			$board.find(".wrapperIntro.second #output04").hide(0);
			$board.find(".wrapperIntro.second .imageholder04").show(0);



			appendDialouge = '';
			var dialouges =$.each(characterDialouges16,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#input04").append(appendDialouge);


			appendDialouge = '';
			var dialouges =$.each(characterDialouges17,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '';
			$("#output04").css('top', '64%');
			$("#output04").append(appendDialouge);


			$( ".wrapperIntro.second .imageholder04" ).find(function() {
				$( "#input04" ).animate({ "top": "+=64%" }, {
					duration: 2000,
					start: function() {$(".wrapperIntro.second .main-fun-g").delay(2000).hide(0);$(".wrapperIntro.second .main-fun-h").delay(2000).show(0); characterDialouge18();},
					complete: function() {

					$(this).hide(0);
					$('#output04').show(0).animate({ "left": "+=40%" }, {
						start:characterDialouge19,
						duration: 2000,
						complete: function() {characterDialouge20();}
						} )
					}
				} );


			});

		}

			function characterDialouge18(){
			appendDialouge = '<div class="p3_24">';
			var dialouges =$.each(characterDialouges18,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des10").append(appendDialouge);
		}


		function characterDialouge19(){
			appendDialouge = '<div class="p3_25">';
			var dialouges =$.each(characterDialouges19,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des11").append(appendDialouge);
		}


		function characterDialouge20(){
			appendDialouge = '<div class="p3_26">';
			var dialouges =$.each(characterDialouges20,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des12").append(appendDialouge);
		$board.find(".wrapperIntro .image4des").css('background','#ddddff none repeat scroll 0 0');
		}



		/*********/


		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});


	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				slide_30,
				slide_31,
				/*characterDialouge3,
				characterDialouge4,
				characterDialouge5,	*/
				slide_32,
				/*
				characterDialouge8,
				characterDialouge9,
				characterDialouge10,*/


				second,//page2
				slide_33,
				slide_34,
				/*characterDialouge13,
				characterDialouge14,
				characterDialouge15,*/
				slide_35

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

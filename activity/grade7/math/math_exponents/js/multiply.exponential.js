$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=14,
		showCounter=0;
	var timer = 200;
	var fnArray = [
		first,
		first_1,
		first_2,
		first_3,
		first_4,
		first_5,
		first_6,
		first_7,
		first_8,
		first_9,
		first_10,
		first_11,
		first_12,
		first_13,
		lokharkeShow
	]
/*var st = math.parseEquation("(4^5)^2= 4^(5*2)= 4^10");
$board.html(st)
return 0;*/
	var base = 3, power = 4; //fourth function
	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);

	var dataObj = [{
		justClass : "commons firstPage",
		text : [
			data.string.pMultiply_1,
			substitue(data.string.pMultiply_2,[
				data.string.pMultiply_3,
				data.string.pMultiply_4,
				data.string.pMultiply_6,
				data.string.pMultiply_7,]),
			data.string.pMultiply_8
		],
		boxText : [
			substitue(data.string.pMultiply_9,[
				data.string.pMultiply_10,
				data.string.pMultiply_11]),
			"",
			data.string.pMultiply_12,
			ole.textSR(data.string.pMultiply_13,"#4#5#","4<sup>5</sup>"),
			substitue(data.string.pMultiply_14,[
				data.string.pMultiply_10,
				data.string.pMultiply_15]),
			"",
			data.string.pMultiply_16,
			ole.textSR(data.string.pMultiply_17,"#a#7#","a<sup>7</sup>"),
			substitue(data.string.pMultiply_19,[
				data.string.pMultiply_10,
				data.string.pMultiply_20]),
			"",
			data.string.pMultiply_21,
			data.string.pMultiply_22,
		],
		lokharkeText : data.string.pMultiply_24+": "+math.parseEquation("(a^m)^n = a^(m*n)")
	}]

	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}

	/*
	* first
	*/
		function first() {
			$nextBtn.hide(0);
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);
			$board.html(html);
			$board.find('.top.text1').delay(1000).fadeIn(1000,nextBtn_show);
		};

		function first_1 () {
			$board.find(".top.text2").fadeIn();
			$board.find(".box").fadeIn();

		}

		function first_2 () {
			$board.find(".box .text0").css("opacity",1);
			$board.find(".top.text1, .top.text2").fadeOut();
		}

		function first_3 () {
			$nextBtn.hide(0);
			$board.find(".box .text1").css("opacity",1);
			var arrays = math.parseEquation("(4^5)^2 = 4^(5*2)= 4^10","array");
			$board.find(".box .text1").loopArray(arrays,timer,nextBtn_show)
		}

		function first_4 () {
			$board.find(".box .text2").css("opacity",1);
			$board.find(".box .text1 span.coefficient").addClass('animated flash');
		}

		function first_5 () {
			$board.find(".box .text3").css("opacity",1);
			$board.find(".box .text1 sup").addClass('animated flash');
		}

		function first_6 () {
			$board.find(".box .text4").css("opacity",1);
		}

		function first_7 () {
			$nextBtn.hide(0);
			$board.find(".box .text5").css("opacity",1);
			var arrays = math.parseEquation("(a^7)^3 =a^(7*3)=a^21 ","array");
			$board.find(".box .text5").loopArray(arrays,timer,nextBtn_show)
		}

	first();

		function first_8 () {
			$board.find(".box .text6").css("opacity",1);
			$board.find(".box .text5 span.coefficient").addClass('animated flash');
		}

		function first_9 () {
			$board.find(".box .text7").css("opacity",1);
			$board.find(".box .text5 sup").addClass('animated flash');
		}

		function first_10 () {
			$board.find(".box .text8").css("opacity",1);
		}

		function first_11 () {
			$nextBtn.hide(0);
			$board.find(".box .text9").css("opacity",1);
			var arrays = math.parseEquation("(3y^2)^3 = 3^(1*3) * (y^2)^3","array");
			$board.find(".box .text9").loopArray(arrays,timer,first_11_1)
		}

		function first_11_1 () {
			$board.find(".box .text9").append("<div class='secondLine'></div>");
			var arrays = math.parseEquation("= 3^3 * y^(2*3)","array");
			$board.find(".box .text9 .secondLine").loopArray(arrays,timer,first_11_2)
		}

		function first_11_2 () {
			var arrays = math.parseEquation("= 27 y^6","array");
			$board.find(".box .text9").append("<div class='thirdLine'></div>");
			$board.find(".box .text9 .thirdLine").loopArray(arrays,timer,nextBtn_show)
		}

		function first_12 () {
			$board.find(".box .text10").css("opacity",1);
			$board.find(".box .text9 span.coefficient").addClass('animated flash');
		}

		function first_13 () {
			$board.find(".box .text11").css("opacity",1);
			$board.find(".box .text9 .secondLine sup").addClass('animated flash');
		}

		function lokharkeShow () {
			$board.find('.lokharke').show(0);
		}


	function nextBtn_show () {
		$nextBtn.show(0);
	}

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			// $prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}




		function fnSwitcher () {
			console.log(countNext);
			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

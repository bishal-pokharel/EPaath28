$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=14,
		showCounter=0;
		var fnArray = [
			first,
			first_1,
			first_2,
			first_3,
			first_4,
			first_5,
			first_6,
			first_7,
			first_8,
			first_9,
			first_10,
			first_11,
			first_12,
			first_13,
			lokharkeShow
		]
	var base = 3, power = 4; //fourth function
	var clickArray = [0,14]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);

	var dataObj = [{
		justClass : "commons firstPage",
		text : [
			data.string.pRadd_1,
			substitue(data.string.pRadd_2,[
				data.string.pRadd_3,
				data.string.pRadd_4,
				data.string.pRadd_5,
				data.string.pRadd_6]),
			data.string.pRadd_7
		],
		boxText : [
			substitue(data.string.pRadd_8,[
				data.string.pRadd_9,
				data.string.pRadd_10]),
			// </span>
			"",
			data.string.pRadd_11,
			data.string.pRadd_12,
			substitue(data.string.pRadd_13,[
				data.string.pRadd_9,
				data.string.pRadd_14]),
			"",
			data.string.pRadd_15,
			data.string.pRadd_16,
			substitue(data.string.pRadd_17,[
				data.string.pRadd_9,
				data.string.pRadd_18]),
			"",
			data.string.pRadd_19,
			data.string.pRadd_20,
		],
		lokharkeText : data.string.pRadd_21+": <span class='mathsFont'>x</span><sup>m</sup> x <span class='mathsFont'>x</span><sup>n</sup> = <span class='mathsFont'>x</span> <sup>(m+n)</sup>"
	}];

	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}

	/*
	* first
	*/
		function first() {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);
			$board.html(html);
			$board.find('.top.text1').delay(1000).fadeIn(1000,nextBtn_show);
		};

		function first_1 () {
			$board.find(".top.text2").fadeIn();
			$board.find(".box").fadeIn();
		}

		function first_2 () {
			$board.find(".box .text0").css("opacity",1);
		}

		function first_3 () {
			$board.find(".box .text1").css("opacity",1);
			var arrays = ["<span class='LHS'>4</span>",
			" x ","<span class='LHS'>4</span>",
			"<sup>2</sup>",
			" x ",
			"<span class='LHS'>4</span>",
			"<sup>3</sup>","= ",
			"<span>4</span>",
			"<span><sup>(</sup>","<sup class='goTop'>1</sup>","<sup>+</sup>","<sup class='goTop'>2</sup>","<sup>+</sup>","<sup class='goTop'>3</sup>","<sup>)</sup></span>",
			"= ",
			"<span>4</span>",
			"<sup class='RHS'>6</sup>"]
			$board.find(".box .text1").loopArray(arrays,400,nextBtn_show)
		}

		function first_4 () {
			$board.find(".box .text2").css("opacity",1);
			$board.find(".box .text1 .LHS").addClass('animated flash');
		}

		function first_5 () {
			$board.find(".box .text3").css("opacity",1);
			$board.find(".box .text1 .goTop, .box .text1 .RHS").addClass('animated flash');
		}

		function first_6 () {
			$board.find(".box .text4").css("opacity",1);
		}

		function first_7 () {
			$board.find(".box .text5").css("opacity",1);
			var arrays = [
				"<span class='LHS'>a</span>",
				"<span> x </span>",
				"<span class='LHS'>a</span>",
				"<sup>3</sup>",
				"<span> x </span>",
				"<span class='LHS'>a</span>",
				"<sup>5</sup>",
				"<span> = </span>",
				"<span>a</span>",
				"<sup>1</sup>",
				"<span> x </span>",
				"<span>a</span>",
				"<sup>3</sup>",
				"<span> x </span>",
				"<span>a</span>",
				"<sup>5</sup>",
				"<span> =</span>",
				"<span>a</span>",
				"<sup>(</sup>",
				"<sup class='goTop'>1</sup>",
				"<sup>+</sup>",
				"<sup class='goTop'>3</sup>",
				"<sup>+</sup>",
				"<sup class='goTop'>5</sup>",
				"<sup>)</sup>",
				"<span> = </span>",
				"<span>a</span>",
				"<sup class='RHS'>9</sup>"
			]
			$board.find(".box .text5").loopArray(arrays,400,nextBtn_show)
		}

	first();

		function first_8 () {
			$board.find(".box .text6").css("opacity",1);
			$board.find(".box .text5 .LHS").addClass('animated flash');
		}

		function first_9 () {
			$board.find(".box .text7").css("opacity",1);
			$board.find(".box .text5 .goTop, .box .text5 .RHS").addClass('animated flash');
		}

		function first_10 () {
			$board.find(".box .text8").css("opacity",1);
		}

		function first_11 () {
			$board.find(".box .text9").css("opacity",1);
			var arrays = [
				"<span class='LHS coefficient'>3</span>",
				"<span class='LHS base'>y</span>",
				"<sup>2</sup>",
				" x ",
				"<span class='LHS coefficient'>2</span>",
				"<span class='LHS base'>y</span>",
				"<sup>6</sup>",
				" x ",
				"<span class='LHS coefficient'>5</span>",
				"<span class='LHS base'>y</span>",
				"<sup>5</sup>",
				" = ",
				"<span class='mids coefficient'>3</span>",
				" x ",
				"<span class='mids coefficient'>2</span>",
				" x ",
				"<span class='mids coefficient'>5</span>",
				" x ",
				"<span class=' base'>y</span>",
				"<sup>(<sup>",
				"<sup class='goTop'>2</sup>",
				"<sup>+</sup>",
				"<sup class='goTop'>6</sup>",
				"<sup>+</sup>",
				"<sup class='goTop'>5</sup>",
				"<sup>)</sup> ",
				"= ",
				"<span class='RHS coefficient'>30</span>",
				"<span class='RHS base'>y</span>",
				"<sup class='RHS power'>13</sup>",
			]
			$board.find(".box .text9").loopArray(arrays,400,nextBtn_show)
		}

		function first_12 () {
			$board.find(".box .text10").css("opacity",1);
			$board.find(".box .text9 .coefficient").addClass('animated flash');
		}

		function first_13 () {
			$board.find(".box .text11").css("opacity",1);
			$board.find(".box .text9 .base").addClass('animated flash');
		}

		function lokharkeShow () {
			$board.find('.lokharke').show(0);
		}


	function nextBtn_show () {
		$nextBtn.show(0);
	}

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			// $prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}



		function fnSwitcher () {
			console.log(countNext);
			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

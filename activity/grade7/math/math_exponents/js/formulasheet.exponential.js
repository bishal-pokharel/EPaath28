$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=14,
		showCounter=0;

	var base = 3, power = 4; //fourth function
	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);

	var dataObj = {
		recap : data.string.formulaSheet,
		formulaText : [
			"1) "+data.string.formula_1,
			data.string.example+": "+math.parseEquation("2^3"),
			data.string.here+", "+data.string.base+" = "+2+", "+data.string.power+" = "+3,
			"2) "+data.string.formula_2,
			data.string.example+": "+math.parseEquation("2 * 3= 2 + 2 + 2  = 6"),
			math.parseEquation("2^3 = 2 * 2 * 2 = 8"),
			"3) "+data.string.formula_3,
			substitue(data.string.formula_4,[
					data.string.formula_5,
					data.string.formula_6,
					data.string.formula_7,
					data.string.formula_8]),
			math.parseEquation("a^m * a^n = a ^(m+n)"),
			substitue(data.string.formula_9,[
					data.string.formula_5,
					data.string.formula_10,
					data.string.formula_7,
					data.string.formula_11]),
			math.parseEquation(" a^m / a^n = a ^(m - n)"),
			data.string.formula_12,
			math.parseEquation(" a^0 = 1"),
			data.string.formula_13,
			math.parseEquation("(a^m)^n = a^(m*n)"),
			data.string.formula_14,
			math.parseEquation("a^(-m) = 1/a^m"),
			math.parseEquation("1/a^m = a^(-m)"),
		]

	}

	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}

	/*
	* first
	*/
		function first() {
			var source = $("#formulaSheet-templete").html();
			var template = Handlebars.compile(source);
			var html = template(dataObj);
			$board.html(html);
		};

		first();

		ole.footerNotificationHandler.lessonEndSetNotification();

});
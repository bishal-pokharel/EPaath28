$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		timer = 400,
		$total_page = 4;
	loadTimelineProgress($total_page+1,countNext+1);

	var qDatas = [
		{base : "4", power : "5",
			expand1 : math.expandExponent(4,5),
			expand2 : "4*4*4*4*4",
			value : math.valueExponent(4,5),
		},
		{base : "5", power : "5",
			expand1 : math.expandExponent(5,5),
			expand2 : "5*5*5*5*5",
			value : math.valueExponent(5,5),
		},
		{base : "10", power : "4",
			expand1 : math.expandExponent(10,4),
			expand2 : "10*10*10*10",
			value : math.valueExponent(10,4),
		},
		{base : "a", power : "4",
			expand1 : math.expandExponent("a",4),
			expand2 : "a*a*a*a",
			value : math.valueExponent("a",4),
		},
	];

	console.log(qDatas);

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p2_1,
			qTitle : data.string.p5_1
		};
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
	}

	$.fn.extend({
		showAns : function (base,power) {
			var base = parseInt(base);
			var power = parseInt(power);
			console.log(base+" "+power);
			var numArray = math.expandExponent(base,power,'array');
			var inBase;
			var inPower;
			var counter=0;
			var value = Math.pow(base,power);
			if (countNext===0) {
				inBase = $(".ansArea").find('.baseIn');
				inValue = $(".ansArea").find('span.valueIn');
			}
			console.dir("This is numArray"+numArray+"..."+inBase.attr("class"))

				$(inBase).addClass(' highlight');
				looper();
			function looper () {
				inBase.append(numArray[counter]);
				counter++;
				if (counter>=numArray.length) {
					setTimeout(function(){
						inBase.removeClass('highlight');
						inValue.html(value);
						inValue.addClass(' highlight')
						setTimeout(function(){
							$nextBtn.show(0);
						},1000);
					},1000);
				} else {
					setTimeout(looper,timer);
				}
			}
		
		}
	});


	function first(sub) {

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : data.string.check,
			clickToSee : data.string.p2_3,
			answer : data.string.p2_4,
			expandString : data.string.p5_3,
			valueString : data.string.p5_4,
			base : sub.base,
			power : sub.power,
			expand : sub.expand,
			value : sub.value ,
			couter : countNext,
		};
		if (countNext===0) {
			content.example=true;
			content.couter = data.string.example;
		};
		var html = template(content);
		$board.find(".qaArea").append(html);
		if (countNext===0) {
			$board.find(".clicks").hide(0);
			$board.find('.qExample').showAns(4,5);
		}
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').css("opacity","1");
		$nextBtn.show(0);
	});

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var expandelem = $board.find(cls+" span.expand");
		var expand = $board.find(cls+" input.expand").val().toLowerCase().trim();
		var valueOfIndices = $board.find(cls+" input.value").val().toLowerCase().trim();

		var upDatas = qDatas[num];
		console.log(expand+"  "+valueOfIndices);
		console.log(upDatas);
		console.log(expand ===upDatas.expand);
		expandelem.html(upDatas.expand1);

		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		var right = "<img src='images/correct.png'>";
		if (expand ===upDatas.expand1 || expand ===upDatas.expand2) {
			check[0]=1;
			$board.find(cls+" .check1").html(right);
		} else {
			$board.find(cls+" .check1").html(wrong);
		}

		if (valueOfIndices===""+upDatas.value) {
			check[1]=1;
			$board.find(cls+" .check2").html(right);
		} else {
			$board.find(cls+" .check2").html(wrong);
		};

		if (check[0]===1 && check[1]===1) {
			if (countNext>=$total_page) {
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				$nextBtn.show(0);
			}
		} else {
			$board.find(cls+" .clicks .click").show(0);
		}

	});

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;
		if (countNext<2) {
			$board.find(".qExample span.valueIn").removeClass("highlight");
		};
		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page+1,countNext+1);
	});

});
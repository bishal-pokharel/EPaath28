$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=15,
		showCounter=0;

	var base = 3, power = 4; //fourth function
	var clickArray = [0,11,13,14,15]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);

	var dataObj = [{
		justClass : "firstPage",
		text : []
	}]


	/*
	* first
	*/
		function first() {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro>div").hide(0);
			$board.find(".wrapperIntro .text0").show(0);
			setTimeout(function () {
				$board.find(".wrapperIntro .text1").show(0);
				$nextBtn.show(0);
			},500);
			showCounter=1;
		};

		// first func call
	first();

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			// $prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			console.log(countNext);
			if (countNext>=11) {
				$prevBtn.show(0);
				switch (countNext) {
					case 11:
						second();
						break;
					case 12:
						second_1();
						break;
					case 13 :
						third();
						break;
					case 14:
						fifth();
						break;
					case 15:
						fourth();
						break;

					default:break;
				}
			}else {
				justshow(0);
			}

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});
$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
		countNext = 0,
		all_page=15,
		showCounter=0;
		$nextBtn.show(0);
	var base = 3, power = 4; //fourth function
	var clickArray = [0,11,13,14,15]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress(1,1);

	function divideCreator1 (a,b) {
		var c = -(-b);
		var text = "<div class='LHS'><span class=''>"+a+"</span><sup class=''>-"+b+"</sup> = </div>";
		text+="<span class='nemoDemo'><div class='numerator'>1</div><div class='denominator'>"
		+"<span class=''>"+a+"</span><sup class=''>"+c+"</sup></div></span>";
		return text;
	}

	function divideCreator2 (a,b) {
		var c = -(-b);
		var text ="<span class='nemoDemo'><div class='numerator'>1</div><div class='denominator'>"
		+"<span class=''>"+a+"</span><sup class=''>"+b+"</sup></div></span>";
		text += "<div class='RHS'> = <span class=''>"+a+"</span><sup class=''>-"+c+"</sup></div>";
		return text;
	}

	console.log(divideCreator1(1,2));

	var dataObj = [{
		justClass : "firstPage",
		text : [
			data.string.pNegative_1,
			substitue(data.string.pNegative_2,[
				data.string.pNegative_3,
				data.string.pNegative_4,
				data.string.pNegative_5,
				data.string.pNegative_6,
				data.string.pNegative_7]),
			data.string.pNegative_8
		],
		boxText : [
			substitue(data.string.pNegative_9,[
				data.string.pNegative_3]),
			substitue(data.string.pNegative_10,[
				data.string.pNegative_4]),
		],
		firstPart : [
			data.string.pNegative_11,
			data.string.example+":",
			divideCreator1(4,1),
			divideCreator1(3,2),
			divideCreator1(2,5),
			divideCreator1("a",7),
			divideCreator1("b",1),
		],
		secondPart : [
			data.string.pNegative_12,
			data.string.example+":",
			divideCreator2(4,1),
			divideCreator2(3,2),
			divideCreator2(2,5),
			divideCreator2("a",7),
			divideCreator2("b",1),
		],
		lokharkeText : data.string.pNegative_13+": "+math.parseEquation("a^(-m) = 1/a^m")
						+"<br>"
                       +math.parseEquation("1/a^m = a^(-m)")

	}];



	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}

	/*
	* first
	*/
		function first() {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
		};

		// first func call
	first();

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			// $prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			console.log(countNext);
			ole.footerNotificationHandler.pageEndSetNotification();
			
			// if (countNext>=11) {
			// 	$prevBtn.show(0);
			// 	switch (countNext) {
			// 		case 11:
			// 			second();
			// 			break;
			// 		case 12:
			// 			second_1();
			// 			break;
			// 		case 13 :
			// 			third();
			// 			break;
			// 		case 14:
			// 			fifth();
			// 			break;
			// 		case 15:
			// 			fourth();
			// 			break;
			//
			// 		default:break;
			// 	}
			// }else {
			// 	// justshow(0);
			// }

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

	//$nextBtn.show(0);
	ole.footerNotificationHandler.pageEndSetNotification();
});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=16;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,7,11,12,16]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		coverpage:true,
		justClass : "firstPage_sec",
		cover_src: $ref+"/image/coverpage.png",
		covertext: data.lesson.chapter
	},{
		justClass : "firstPage",
		animate : "true",
		text : [function () {
			var d = ole.textSR(data.string.p1_1,data.string.p1_2,"<span class='ind1'>"+data.string.p1_2+"</span>");
			return d;
		},function () {
			var d = ole.textSR(data.string.p1_4,data.string.p1_8,"<b>"+data.string.p1_8+"</b>");
			d = ole.textSR(d,data.string.p1_9,"<b>"+data.string.p1_9+"</b>")
			return d;
		},
		data.string.p1_5,
		data.string.p1_6,
		data.string.p1_7,
		data.string.p1_10,
		data.string.p1_11],
		maths : [{base: 5, exponent : 4},
			{base: 4, exponent : 5},
			{base: 2, exponent : 4}]
	},{
		justClass : "second",
		maths : [{base: 5, exponent : 4}
			],
		text : [data.string.p1_10,
			data.string.p1_11,
			function () {
				var d = data.string.p1_12;
				d = ole.textSR(d,data.string.p1_8,"<b>"+data.string.p1_8+"</b>");
				d = ole.textSR(d,data.string.p1_16,"<b>"+data.string.p1_16+"</b>")
				return d;
			},function () {
				var d = data.string.p1_13;
				d = ole.textSR(d,"#54#"," 5<sup>4</sup> ");
				// d = ole.textSR(d,data.string.p1_9,"<b>"+data.string.p1_9+"</b>")
				return d;
			},
				data.string.p1_14+" ",
			function () {
				var d = data.string.p1_15;
				d = ole.textSR(d,"#54#"," 5<sup>4</sup> ");
				// d = ole.textSR(d,data.string.p1_9,"<b>"+data.string.p1_9+"</b>")
				return d;
			}]
	},{
		justClass : "baseAndIndices",
		maths : [{base: 5, exponent : 4},
			{base: 4, exponent : 5},
			{base: 2, exponent : 4},
			{base: 12, exponent : 4},
			{base: 21, exponent : 4},
			{base: 5, exponent : 13}
			],
		text : [data.string.p1_17,
			data.string.p1_18,
			data.string.p1_19]
	},{
		justClass : "baseAndIndices2",
		text : [data.string.p1_20,
			data.string.p1_21,
			data.string.p1_18,
			data.string.p1_19,
			data.string.constant,
			data.string.variable,
			data.string.constant,
			data.string.variable],
		maths : [{base: 2, exponent : 5},
			{base: "a", exponent : 5},
			{base: 2, exponent : 5},
			{base: 2, exponent : "b"},
			],
	},{
		justClass : "lastBaseIndices",
		text : [data.string.p1_22,
			function () {
				var d = data.string.p1_23;
				d = ole.textSR(d,data.string.p1_18,"<span>"+data.string.p1_18+"</span>");
				return d;
			},
			function () {
				var d = data.string.p1_24;
				d = ole.textSR(d,data.string.p1_19,"<span>"+data.string.p1_19+"</span>");
				return d;
			}],
		maths : [{base: 2, exponent : 4},
			{base: "a", exponent : 5},
			{base: 4, exponent : 5},
			{base: 12, exponent : "b"},
			{base: 21, exponent : 5},
			{base: "b", exponent : 5},
			{base: "x", exponent : "y"},
			{base: "x", exponent : "10"},
			{base: 2, exponent : "b"},
			{base: "a", exponent : "b"},
			{base: "x", exponent : "z"},
			],
	}]

	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
		};
		// added while keepimg cover page
			function first_sec() {

				var source = $("#intro-template").html();
				var template = Handlebars.compile(source);
				var content = dataObj[1];
				var html = template(content);
				$board.html(html);
			};

		/*first call to first*/
		// first();

		function firstShowOther () {
			$board.find(".wrapperIntro.firstPage .maths1,.wrapperIntro.firstPage .maths2").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",0);
		}

		function firstOpacity () {
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);
			// $nextBtn.show(0);
		}

		function firstHide () {
			$board.find(".wrapperIntro.firstPage .text0,.wrapperIntro.firstPage .text1,.wrapperIntro.firstPage .maths1,.wrapperIntro.firstPage .maths2 ").hide(0);
			$board.find(".wrapperIntro.firstPage .text2,.wrapperIntro.firstPage .text3,.wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .text5,.wrapperIntro.firstPage .text6").hide(0);
		}

		function indexShow () {
			$board.find(".wrapperIntro.firstPage .text5").show(0);
		}

		function baseShow () {
			$board.find(".wrapperIntro.firstPage .text6").show(0);
		}

		function beforeClicks () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}

		function beforeClicks_1 () {
			$board.find(".wrapperIntro.second .text3").show(0);
		}

		function beforeClicks_2 () {
			$board.find(".wrapperIntro.second .text4").show(0);
			$nextBtn.hide(0);
			var tArray = [];
			var base = 5, power = 4;
			tArray.push(5+"<sup>"+power+"</sup>");
			tArray.push(" = ");
			for (var i = 0; i < power; i++) {
				tArray.push(base);
				if (i<power-1) {
					tArray.push("x");
				};
			};
			tArray.push(" = ");
			tArray.push(Math.pow(base,power));
			// console.log(tArray);
			var c = 0;
			var len = tArray.length;
			function looper () {
				$board.find(".wrapperIntro.second .text4").append(tArray[c]);
				c++;
				if (c<len) {
					setTimeout(looper,500);
				} else {
					$nextBtn.show(0);
				}
			}
			setTimeout(looper,500);
		}

		function beforeClicks_3 () {
			$board.find(".wrapperIntro.second .text5").show(0);
		}


		/*base and indecis*/
		function baseAndIndices () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			baseAndIndicesClick = [null,null];
			$nextBtn.hide(0);
		}

		$board.on('click','.wrapperIntro.baseAndIndices .text1',function () {
			// $(this).css('background',"#FF9900");
			$board.find('.maths .base').css('color',"#FF9900").addClass("animated infinite flash");
			$board.find('.maths .power').css('color',"#434343").removeClass("animated infinite flash");
			baseAndIndicesClick[0]=1;
			callNextChecker();
		});


		$board.on('click','.wrapperIntro.baseAndIndices .text2',function () {
			// $(this).css('color',"#6600CC");
			//remove class animate
			$board.find('.maths .base').css('color',"#434343").removeClass("animated infinite flash");
			//add class animate
			$board.find('.maths .power').css('color',"#FF9900").addClass("animated infinite flash");
			baseAndIndicesClick[1]=1;
			callNextChecker();
		});

		function callNextChecker () {
			var nextChecker = true;
			for (var i = 0; i < baseAndIndicesClick.length; i++) {
				if (baseAndIndicesClick[i] ===null){
					nextChecker = false;
					break;
				}
			};
			if (nextChecker) {
				$nextBtn.show(0);
			};
		}
		/*end of base and indecis*/

		function baseAndIndices2 () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}

		function baseAndIndices2_1 () {
			$board.find(".wrapperIntro.baseAndIndices2 .text4,.wrapperIntro.baseAndIndices2 .text5").show(0);
			$board.find(".wrapperIntro.baseAndIndices2 .maths0 .base,.wrapperIntro.baseAndIndices2 .maths1 .base").addClass("animated flash");
		}

		function baseAndIndices2_2 () {
			$board.find(".wrapperIntro.baseAndIndices2 .text4,"
				+".wrapperIntro.baseAndIndices2 .text5,"
				+".wrapperIntro.baseAndIndices2 .text0,"
				+".wrapperIntro.baseAndIndices2 .maths0,"
				+".wrapperIntro.baseAndIndices2 .maths1").hide(0);

			$board.find(".wrapperIntro.baseAndIndices2 .text1,"
				+".wrapperIntro.baseAndIndices2 .maths2,"
				+".wrapperIntro.baseAndIndices2 .maths3").show(0);

		}

		function baseAndIndices2_3 () {
			$board.find(".wrapperIntro.baseAndIndices2 .text6,.wrapperIntro.baseAndIndices2 .text7").show(0);
			$board.find(".wrapperIntro.baseAndIndices2 .maths .power").addClass("animated flash");
		}
		/*********/

		function lastBaseIndices () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[5];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}


		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			countNext==1?first_sec():'';
			fnSwitcher();
		});

	// 	$refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first_sec,  //first one
				firstShowOther,
				firstOpacity,
				firstHide,
				indexShow,
				baseShow,
				beforeClicks, //second one
				beforeClicks_1,
				beforeClicks_2,
				beforeClicks_3,
				baseAndIndices, //third one
				baseAndIndices2,
				baseAndIndices2_1,
				baseAndIndices2_2,
				baseAndIndices2_3,
				lastBaseIndices
			];

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

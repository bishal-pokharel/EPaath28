$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=6,
		showCounter=0;
		var fnArray = [
			first,
			first_1,
			first_2,
			first_3,
			first_4,
			first_5,
			first_6
		]
	var base = 3, power = 4; //fourth function
	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);

	var numDem = function (numerator,denominator,className) {
			var creator = "<span class='denoMino "+className+"'>"
				+"<div class='numerator'>"+numerator+"</div>"
				+"<div class='denominator'>"+denominator+"</div>"
			+"</span>";
			return creator;
		}

	var dataObj = [{
		justClass : "firstPage",
		text : [
			data.string.pZero_1,
			substitue(data.string.pZero_2,[
				data.string.pZero_7,
				data.string.pZero_8,
				]),
			substitue(data.string.pZero_3,[
				data.string.pZero_7,
				data.string.pZero_8,
				])+" <span class='mathsFont'>x</span><sup>0</sup> = 1",
		],
		boxText : [
			data.string.example+":",
			"4<sup>0</sup> = 1",
			"3<sup>0</sup> = 1",
			"1<sup>0</sup> = 1 ",
			"a<sup>0</sup> = 1 ",
			"b<sup>0</sup> = 1",
			data.string.pZero_9,
		],
		methods1 : [
					data.string.pZero_4+" 1: "+data.string.pZero_5,
			data.string.example+" 1:",
			numDem("5<sup>3</sup>","5<sup>3</sup>","eg1")+
				"<span class='equalsTo'> = </span>" + numDem(math.expandExponent(5,3,"tag"," ","span"),math.expandExponent(5,3,"tag"," ","span"),"eg1 RHS")
				+"<span class='equalsTo one'> = 1</span>",

			data.string.example+" 2:",
			numDem("a<sup>10</sup>","a<sup>10</sup>","eg2")+
				"<span class='equalsTo'> = </span>" + numDem(math.expandExponent("a",10,"tag"," ","span"),math.expandExponent("a",10,"tag"," ","span"),"eg2 RHS")
				+"<span class='equalsTo one'> = 1</span>"
		],
		methods2 : [
			data.string.pZero_4+" 2: ",
			"<span class='tree'>a<sup>0</sup> = a <sup>(4-4)</sup> = </span>"+numDem("a<sup>4</sup>","a<sup>4</sup>"),
			data.string.pZero_6,
			"<span class='equalsTo'> = </span>"+numDem(math.expandExponent("a",4,"tag"," ","span"),math.expandExponent("a",4,"tag"," ","span")," eg1 RHS"),
			"= 1",
		],
		lokharkeText : data.string.pZero_10+" 0 = 1, a<sup>0</sup> = 1"

	}]



	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}

	function show_next () {
		$nextBtn.show(0);
	}

	/*
	* first
	*/
		function first() {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro .top.text1,"
				+" .wrapperIntro .top.text2").hide(0).delay(500).fadeIn(show_next);
			$board.find(".box > div").css('opacity',0);
		};

		function first_1 () {
			$nextBtn.hide(0);
			$board.find('.box').fadeIn();
			var i = 0;
			function looper () {
				$board.find('.box .text'+i).css('opacity',1);
				i++;
				if (i<7) {
					setTimeout(looper,500);
				} else {
					$board.find('.box .text6').addClass("animated flash");
					$nextBtn.show(0);
				}

			}
			looper();
		}

		function first_2 () {
			$board.find('.box .methods1').css('opacity',1);
			$board.find('.box .methods1 .methodText0').css('opacity',1);
			$nextBtn.hide(0);
			setTimeout(function () {
				$board.find('.box .methods1 .methodText1,'
					+'.box .methods1 .methodText2').css('opacity',1);
				setTimeout(function () {
					$board.find(".methods1 .eg1.RHS").canceller(3,showOne);
				},800);
			},800);

		}

		function showOne () {
			$board.find(".one").show(0);
		}

		var timer = 800;
		$.fn.extend({
			canceller : function (len,fn) {
				var $this = $(this);
				var counter=1;
				console.log($this);
				function looper () {
					$this.find('.numerator span:nth-child('+counter+')').append("<div class='cutter'></div>")
					$this.find('.denominator span:nth-child('+counter+')').append("<div class='cutter'></div>")
					counter=counter+1;
					if (counter>len) {
						$nextBtn.show(0);
						fn($this);
						// setTimeout(fn,timer);
					} else {
						setTimeout(looper,timer);
					}
				}
				looper();
			}
		});

		function first_3 () {
			$board.find('.box .methods1 .methodText3').css('opacity',1);
			$nextBtn.hide(0);
			$board.find(".methodText4 .one").hide(0);
			setTimeout(function () {
				$board.find('.box .methods1 .methodText4,'
					+'.box .methods1 .methodText5').css('opacity',1);
				setTimeout(function () {
					$board.find(".methods1 .eg2.RHS").canceller(10,showOne);
				},800);
			},800);
		}

		function first_4 () {
			$nextBtn.hide(0);
			$board.find('.box .methods2').css('opacity',1);
			$board.find('.box .methods2 .methodText0').css('opacity',1);
			setTimeout(function () {
				$board.find('.box .methods2 .methodText1').css('opacity',1);
			},500);
			setTimeout(function () {
				$board.find('.box .methods2 .methodText2').css('opacity',1);
				$nextBtn.show(0);
			},1500);
		}

		function first_5 () {
			$nextBtn.hide(0);
			$board.find('.box .methods2 .methodText3').css('opacity',1);
			setTimeout(function () {
				$board.find(".methods2 .eg1.RHS").canceller(5,show_one2);
			},600);

		}

		function show_one2 () {
			$board.find(".methods2 .methodText4").css('opacity',1);
		}

		function first_6 () {
			$board.find(".lokharke").show(0);
		}

		// first func call
	first();

	/*click functions*/
		$nextBtn.on('click',function () {
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}



		function fnSwitcher () {
			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

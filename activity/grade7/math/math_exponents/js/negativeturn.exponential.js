var dataObj = [{
		// title : data.string.pRaddTurn_1,
		solve : data.string.pRnegative_1,
		examples : [
			data.string.example,
			math.parseEquation("(a^2)"),
			"= "+math.parseEquation(" 1/ a^(-2)"),
			"",
			data.string.answer+" = "+math.parseEquation("1/ a^(-2)")
		],
		yourTurn : data.string.yourTurn
	},{
		count : 1,
		q : math.parseEquation("8^3"),
		solution : ["= "+math.parseEquation("1/8^(-3)"),
		"",
		data.string.answer+" = "+math.parseEquation("1/8^(-3)")],
		ans : ["1","8","-3"]
	},{
		count : 2,
		q : math.parseEquation("b^(-7)"),
		solution : ["= "+math.parseEquation("1/b^7"),
		"",
		data.string.answer+" = "+math.parseEquation("1/b^7")],
		ans : ["1","b","7"]
	},{
		count : 3,
		q : math.parseEquation("1/a^(-5)"),
		solution : ["= "+math.parseEquation("a^5"),
		data.string.answer+" = "+math.parseEquation("a^5")],
		ans : ["5","a"]
	}];

	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}
// startYourTurn (dataObj);
$(function () {
	startYourTurn (dataObj);
});

function startYourTurn(dataObj) {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=4,
		showCounter=0,
		correctImg = "images/correct.png",
		wrongImg = "images/wrong.png";

	var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

	// var base = 3, power = 4; //fourth function
	// var clickArray = [0,14]; //manipulate the clicks like back and from top
	$total_page = dataObj.length;
	loadTimelineProgress($total_page,1);

	var mainTemplate = "<div class='wrapperIntro {{justClass}}'>"
        +"<div class='title'>{{{title}}}</div>"
        +"<div class='solve'>{{{solve}}}</div>"
        +"<div class='example'>"
        +"{{#each examples}}"
            +"<div class='eg{{@index}}'>{{{this}}}</div>"
        +"{{/each}}"
        +"</div>"
        +"<div class='lokharke first'><img src='images/lokharke/2.png' alt=''/><span>{{{yourTurn}}}</span></div>"
        +"<div class='qaBox'></div>"
        +"<div class='solutionBox'></div>"
    +"</div>";

    var qaTemplate = "<div class='qa qadivide qa{{count}}'>"
        +"<span class='qCount'>{{{count}}}) </span>"
        +"<div class='question'>{{{q}}} = </div>"
        +"<div class='inputs'>"
            +"{{#each ans}}"
                +"<input type='text'/>"
            +"{{/each}}"
        +"</div>"
        +"<div class='checkBox'></div>"
        +"<div>"
            +"<div class='check' data-count='{{count}}'>{{{check}}}</div>"
            +"<div class='reset checkShow' data-count='{{count}}'>{{{reset}}}</div>"
            +"<div class='clickCheck checkShow' data-count='{{count}}'>{{{clickCheck}}}</div>"
        +"</div>"
    +"</div>";

    var solutionTemplate = "<div class='question'>{{{q}}}</div>"
    +"{{#each solution}}"
        +"<div class='solutionEg solution{{@index}}'>{{{this}}}</div>"
    +"{{/each}}";



	/*
	* first
	*/
		function first() {
			var source = mainTemplate;
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find('.example > *').fadeOut(0);

			$board.find('.example .eg0').fadeIn();
			$board.find('.example .eg1').delay(800).fadeIn();
			$board.find('.example .eg2').delay(1500).fadeIn();
			$board.find('.example .eg3').delay(2400).fadeIn();
			$board.find('.example .eg4').delay(2800).fadeIn(function () {
				$board.find('.lokharke').show(0);
				$board.find('.lokharke').addClass('animated zoomInDown').one(animationEnd,function () {
					$nextBtn.show(0);
				});
			});

		};

		first();

		function justLoader (num) {
			loadQuestion(num);
			loadSolution(num);
		}

		function loadQuestion (num) {
			// var source = qaTemplate;
			console.log(qaTemplate);
			var template = Handlebars.compile(qaTemplate);
			var content = dataObj[num];
			content.check = data.string.check;
			content.reset = data.string.reset;
			content.clickCheck = data.string.clickToCheckAns;
			var html = template(content);
			$board.find('.qaBox').append(html);
			$board.find(".checkShow").hide(0);
			$board.find('.qaBox .qa'+num+" input:nth-child(1)").focus();

		}

		function loadSolution (num) {
			$board.find('.solutionBox').hide(0);
			var source = solutionTemplate;
			var template = Handlebars.compile(solutionTemplate);
			var content = dataObj[num];
			var html = template(content);
			$board.find('.solutionBox').html(html);
		}

		function firstNext () {
			/*$board.find('.lokharke').removeClass('animated zoomInDown first').addClass('animated zoomInLeft').one(animationEnd,function () {})*/
			$board.find('.lokharke').removeClass('animated zoomInDown first');
			$board.find(".qaBox").show(0);
			setTimeout(function () {
				justLoader(1);
			},800);
		}

	$board.on('click','.check',function () {
		var $this = $(this);
		$this.hide(0);
		var $count = parseInt($this.data("count"));
		var imgShow;
		var parent = ".qa"+$count;
		var ans = [];
		$board.find(parent+" .inputs input").each(function (index) {
			// console.log("count"+index+$(this).val());
			var $this = $(this);
			if (dataObj[$count].ans[index]===$this.val()) {
				ans[index] = 1;
			} else {
				ans[index] = 0;
				$this.addClass("wrongInput");
			}


		});
		console.log(ans);
		var condition = true;
		for(key in ans){
			console.log(ans);
			if (ans[key] === 0 ) {
				condition = false;
				break;
			};
			console.log(key);
		}
		if (condition) {
			imgShow = correctImg;
			$nextBtn.show(0);
			$board.find(parent+" .checkBox").html("<img class='correct' src="+imgShow+"/>");
			$(".correct").attr("src",imgShow);
		} else {
			$board.find(parent+" .checkShow").show(0);
			imgShow = wrongImg;
			$board.find(parent+" .checkBox").html("<img class='wrong' src="+imgShow+"/>");
			$(".wrong").attr("src",imgShow);
		}
	});

	$board.on('click','.reset',function () {
		var $this = $(this);
		var $count = $this.data("count");
		var parent = ".qa"+$count;
		$board.find(parent+" input.wrongInput").focus();
		$board.find(parent+" input.wrongInput").val("");
		$board.find(parent+" input.wrongInput").removeClass("wrongInput");
		$board.find(parent+" .checkShow").hide(0);
		$board.find(parent+" .checkBox").html("");
		$board.find(parent+" .check").show(0);

	});

	$board.on('click','.clickCheck',function () {
		$board.find('.solutionBox').show(0);
		$board.find(".checkShow").hide(0);
		$nextBtn.show(0);
	});

	function nextBtn_show () {
		$nextBtn.show(0);
	}

	/*click functions*/
		$nextBtn.on('click',function () {
			$(this).css("display","none");
			// $prevBtn.show(0);
			countNext++;
			if (countNext<dataObj.length) {
				if (countNext==1) {
					firstNext();
				} else {
					justLoader (countNext);
				}
			};

			loadTimelineProgress($total_page,countNext+1);

			if (countNext>=all_page-1) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {

			}
		});
	/****************/

};
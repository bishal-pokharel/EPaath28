$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		timer = 400,
		$total_page = 4;
	loadTimelineProgress($total_page,countNext+1);
	// alert("hello");
	$nextBtn.show(0);

	function adder (num) {
		var addTop = 1;
		for (var i = 0; i < num-1; i++) {
			addTop+="+"+1;
		};
		// console.log(addTop);
		return addTop;
	}

	var qDatas = [
		{base : "a", power : "4",
			expand : math.expandExponent("a",4,"string"," ")
			,
			expandWithOne : math.expandPower1("a",4," "),
			addTop : adder(4),
		},
		{base : "5", power : "6",
			expand : math.expandExponent(5,6,"string"," "),
			expandWithOne : math.expandPower1(5,6," "),
			addTop : adder(6),
		},
		{base : "10", power : "7",
			expand : math.expandExponent(10,7,"string"," "),
			expandWithOne : math.expandPower1(10,7," "),
			addTop : adder(7)
		},
		{base : "b", power : "6",
			expand : math.expandExponent("b",6,"string"," "),
			expandWithOne : math.expandPower1("b",6," "),
			addTop : adder(6)
		},
	]

	console.log(qDatas);

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p2_1,
			qTitle : data.string.p6_1
		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
	}

	$.fn.extend({
		showAns : function () {
			if (countNext>0) {
				$board.find(".wrapperIntro > .showAns").show(0);
			} else {
				$board.find(".qExample .showAns").show(0);
			}
			$board.find(".showAns .expandForm").delay(1000).fadeIn();
			$board.find(".showAns .expandForm sup").delay(2000).fadeIn();
			$board.find(".showAns .addTop").delay(3000).fadeIn();
			$board.find(".showAns .addTop span.power").delay(4000).fadeIn();
			$board.find(".showAns .add").delay(5000).fadeIn();
			$board.find(".showAns .add sup").delay(6000).fadeIn(function () {
				$nextBtn.delay(1500).fadeIn();
				if (countNext===0) {
					$board.find(".qExample .qTitle .exBP").delay(500).fadeIn();
				};
			});


		}
	});


	function first(sub) {

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : data.string.check,
			clickToSee : data.string.p2_3,
			answer : data.string.p2_4,
			expandString : data.string.p5_3,
			valueString : data.string.p5_4,
			base : sub.base,
			power : sub.power,
			expand : sub.expand,
			value : sub.value ,
			couter : countNext,
			topAdder : sub.addTop,
			expandWithOne : sub.expandWithOne
		}
		if (countNext===0) {
			content.example=true;
			content.couter = data.string.example;
		};
		var html = template(content);
		$board.find(".qaArea").append(html);

		$board.find(".wrapperIntro > .showAns").hide(0);

		if (countNext===0) {
			$board.find(".clicks").hide(0);
			$board.find('.qExample').showAns("a",4);
			$board.find(".qExample .expandForm, .qExample .addTop, .qExample .add").hide(0);
		} else {
			$board.find(".qaArea .base input").focus();
			var source2 = $("#showAns-template").html();
			var template2 = Handlebars.compile(source2);
			var html2 = template2(content);
			console.log(html2);
			$board.find(".wrapperIntro > .showAns").html(html2);
			$board.find(".wrapperIntro > .showAns .expandForm, .wrapperIntro > .showAns .addTop, .wrapperIntro > .showAns .add").hide(0);
		}
		$nextBtn.stop();
		$nextBtn.hide(0);
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls ).showAns();
		$board.find(cls+" .clicks").hide(0);
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this),
			num = parseInt($this.data("cls")),
			cls = ".q"+num,
			base = $board.find(cls+" .qTitle .base input").val().toLowerCase().trim(),
			power = $board.find(cls+" .qTitle .power input").val().toLowerCase().trim(),
			upDatas = qDatas[num],
			check = [null,null],
			wrong = "<img src='images/wrong.png'>",
			right = "<img src='images/correct.png'>";
		if (base ===upDatas.base) {
			check[0]=1;
		}

		if (power===""+upDatas.power) {
			check[1]=1;
		}

		if (check[0]===1 && check[1]===1) {
			$this.hide(0);
			if (countNext>=$total_page) {
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				$nextBtn.show(0);
			}
			$board.find(cls+" .checkShow").html(right);
		} else {
			$board.find(cls+" .checkShow").html(wrong);
			$board.find(cls+" .clicks .click").show(0);
		}

	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;
		if (countNext<2) {
			$board.find(".qExample span.valueIn").removeClass("highlight");
		};
		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

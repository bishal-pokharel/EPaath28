$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=14,
		showCounter=0;
		var fnArray = [
			first,
			first_1,
			first_2,
			first_3,
			first_4,
			first_5,
			first_6,
			first_7,
			first_8,
			first_9,
			first_10,
			first_11,
			first_12,
			first_13,
			lokharkeShow
		]
	var base = 3, power = 4; //fourth function
	var clickArray = [0,11,13,14,15]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);

	var dataObj = [{
		justClass : "commons firstPage",
		text : [
			data.string.pRulesSub_1,
			substitue(data.string.pRulesSub_2,[
				data.string.pRulesSub_3,
				data.string.pRulesSub_4,
				data.string.pRulesSub_5,
				data.string.pRulesSub_6]),
			data.string.pRulesSub_7
		],
		boxText : [
			substitue(data.string.pRulesSub_8,[
				data.string.pRulesSub_9,
				data.string.pRulesSub_10]),
			"",
			data.string.pRulesSub_11,
			data.string.pRulesSub_12,
			substitue(data.string.pRulesSub_13,[
				data.string.pRulesSub_9,
				data.string.pRulesSub_14]),
			"",
			data.string.pRulesSub_15,
			data.string.pRulesSub_16,
			substitue(data.string.pRulesSub_17,[
				data.string.pRulesSub_9,
				data.string.pRulesSub_18]),
			"",
			data.string.pRulesSub_19,
			data.string.pRulesSub_20,
		],
		lokharkeText : data.string.pRulesSub_21+": x<sup>m</sup> / x<sup>n</sup> = x<sup>(m - n)</sup>"
	}]

	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}

	/*
	* first
	*/
		function first() {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);
			$board.html(html);
			$board.find('.top.text1').delay(1000).fadeIn(1000,nextBtn_show);
		};

		first();

		function first_1 () {
			$board.find(".top.text2").fadeIn();
			$board.find(".box").fadeIn();
		}

		function first_2 () {
			$board.find(".box .text0").css("opacity",1);
		}

		function first_3 () {
			$nextBtn.show(0);
			var numGen1 = numDem("dem1");
			$board.find(".box .text1").css("opacity",1).html(numGen1);
			var arrays = [
				"<span class='LHS'>4</span>",
				"<sup>5</sup>",
			];
			$board.find(".box .text1 .numerator").loopArray(arrays,400,first_3_1)
		}

		function first_3_1 () {
			var array2 = ["<span class='LHS'>4</span>",
				"<sup>3</sup>"];
			$board.find(".box .text1 .denominator").loopArray(array2,400,first_3_2);
		}

		var spanAdd ="<span class='denoMinoPlus'></span>";
		function first_3_2 () {
			$board.find(".box .text1").append(spanAdd);
			var arrays = ["= ",
				"<span>4</span>",
				"<span><sup>(</sup>",
				"<sup class='goTop'>5</sup>",
				"<sup>-</sup>",
				"<sup class='goTop'>3</sup>",
				"<sup>)</sup></span>",
				" = ",
				"<span>4</span>",
				"<sup class='RHS'>2</sup>"
			];
			$board.find(".box .text1 .denoMinoPlus").loopArray(arrays,400,nextBtn_show);
		}

		function first_4 () {
			$board.find(".box .text2").css("opacity",1);
			$board.find(".box .text1 .LHS").addClass('animated flash');
		}

		function first_5 () {
			$board.find(".box .text3").css("opacity",1);
			$board.find(".box .text1 .goTop, .box .text1 .RHS").addClass('animated flash');
		}

		function first_6 () {
			$board.find(".box .text4").css("opacity",1);
		}

		var numDem = function (className) {
			var creator = "<span class='denoMino "+className+"'>"
				+"<div class='numerator'></div>"
				+"<div class='denominator'></div>"
			+"</span>";
			return creator;
		}
		function first_7 () {
			var numGen1 = numDem("dem1");
			$board.find(".box .text5").css("opacity",1).html(numGen1);
			var array1 = ["<span class='LHS'>a</span>",
				"<sup>7</sup>",]
			$board.find(".box .text5 .numerator").loopArray(array1,400,first_7_1);
		}

		function first_7_1 () {
			var array2 = ["<span class='LHS'>a</span>",
				"<sup>5</sup>"]
			$board.find(".box .text5 .denominator").loopArray(array2,400,first_7_2);
		}

		function first_7_2 () {
			$board.find(".box .text5").append(spanAdd);
			var arrays = ["<span> = </span>",
			"<span>a</span>",
			"<sup>(</sup>",
			"<sup class='goTop'>7</sup>",
			"<sup>-</sup>",
			"<sup class='goTop'>5</sup>",
			"<sup>)</sup>",
			"<span> = </span>",
			"<span>a</span>",
			"<sup class='RHS'>2</sup>"];
			$board.find(".box .text5 .denoMinoPlus").loopArray(arrays,400,nextBtn_show);
		}

		function first_8 () {
			$board.find(".box .text6").css("opacity",1);
			$board.find(".box .text5 .LHS").addClass('animated flash');
		}

		function first_9 () {
			$board.find(".box .text7").css("opacity",1);
			$board.find(".box .text5 .goTop, .box .text5 .RHS").addClass('animated flash');
		}

		function first_10 () {
			$board.find(".box .text8").css("opacity",1);
		}

		function first_11 () {
			var numGen1 = numDem("dem1");
			$board.find(".box .text9").css("opacity",1).html(numGen1);
			var array1 = ["<span class='LHS coefficient'>9</span>",
				"<span class='LHS base'>y</span>",
				"<sup>10</sup>",]
			$board.find(".box .text9 .numerator").loopArray(array1,400,first_11_1);
		}

		function first_11_1 () {
			var array2 = ["<span class='LHS coefficient'>3</span>",
				"<span class='LHS base'>y</span>",
				"<sup>5</sup>"]
			$board.find(".box .text9 .denominator").loopArray(array2,400,first_11_2);
		}
		function first_11_2 () {
			var numGen1 = numDem("dem2");
			$board.find(".box .text9").css("opacity",1).append("<span class='equalsTo'> = </span>",numGen1);
			var array1 = ["<span class='LHS coefficient'>9</span>",
				"<span class='LHS base'>y</span>",
				"<sup>10</sup>",]
			$board.find(".box .text9 .dem2 .numerator").loopArray(array1,400,first_11_3);
		}

		function first_11_3 () {
			var array2 = ["<span class='LHS coefficient'>3</span>",
				"<span class='LHS base'>y</span>",
				"<sup>5</sup>"]
			$board.find(".box .text9 .dem2 .denominator").loopArray(array2,400,first_11_4);
		}

		function first_11_4 () {
			$board.find(".box .text9").append(spanAdd);
			var arrays = [" = ",
				"<span class=' base'>3y</span>",
				"<sup>(<sup>",
				"<sup class='goTop'>10</sup>",
				"<sup>-</sup>",
				"<sup class='goTop'>5</sup>",
				"<sup>)</sup> ",
				"= ",
				"<span class='RHS coefficient'>3</span>",
				"<span class='RHS base'>y</span>",
				"<sup class='RHS power'>5</sup>"];
			$board.find(".box .text9 .denoMinoPlus").loopArray(arrays,400,nextBtn_show);
		}

		function first_12 () {
			$board.find(".box .text10").css("opacity",1);
			$board.find(".box .text9 .coefficient").addClass('animated flash');
		}

		function first_13 () {
			$board.find(".box .text11").css("opacity",1);
			$board.find(".box .text9 .base").addClass('animated flash');
		}

		function lokharkeShow () {
			$board.find('.lokharke').show(0);
		}


	function nextBtn_show () {
		$nextBtn.show(0);
	}

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			// $prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}



		function fnSwitcher () {
			console.log(countNext);
			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

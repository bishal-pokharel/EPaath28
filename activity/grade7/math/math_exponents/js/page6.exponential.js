/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=7,
		$refImg = $ref+"/image/",
		paraTime = 1000;
$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "baseAndIndices",
		maths : [{base: 2, exponent : "?"},
			{base: "a", exponent : "?"},
			{base: 23, exponent : "?"},
			{base: 5, exponent : "?"},
			{base: 6, exponent : "?"},
			{base: 18, exponent : "?"}
			],
		text : [data.string.p6_1,
			data.string.p6_2]
	},{
		justClass : "how",
		text : [
			"2x2x2x2",
			"= 2<sup>4</sup>",
			data.string.p6_4,
			data.string.p6_9,
			"",
			data.string.p6_6+" 1+1+1+1 = 4",
			data.string.p6_7+ " 2x2x2x2 = 2<sup>4</sup>",
		],
		lokharkeText : data.string.p6_3,
		plane : $refImg+"plane.gif",
		parachute : $refImg+"paradrop.png",
		lokharkeText2 : data.string.p6_8
	}]

	/*base and indices*/
	function baseAndIndices () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[0];
		var html = template(content);
		$board.html(html);
	}

	baseAndIndices();

	function baseAndIndices_1 () {
		$board.find(".baseAndIndices .text0").hide(0);
		$board.find(".baseAndIndices .text1,.baseAndIndices .maths span.power").show(0);

	}

	function how () {
		var source = $("#how-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[1];
		var html = template(content);
		$board.html(html);
		$nextBtn.hide(0);
		$board.find('.text1').fadeOut(0);
		$board.find('.text1').delay(1000).fadeIn();
		$board.find('.text2').delay(2000).fadeIn();
		$board.find('.lokharke').delay(4000).fadeIn(function () {
			$nextBtn.show(0);
		});
	}

	// how();

	function how_1 () {
		$board.find('.text3').fadeIn();
		$board.find('.powerDrop').delay(1000).fadeIn();
	}

	function planeCome () {
		$nextBtn.hide(0);
		$board.find(".plane").animate({
			left : "55%",
			top : "20%"
		},2000,paraDrops)
	}

	function paraDrops () {
		$board.find(".parachute").show(0);
		$board.find(".parachute1").animate({
			top: "50%",
			left: "53%"
		},paraTime,function () {
			$board.find(".powerDrop .rightSide .power1").show(0);
			$board.find(".parachute1").fadeOut();
		})

		$board.find(".parachute2").animate({
			top: "50%",
			left: "60%"
		},paraTime,function () {
			$board.find(".powerDrop .rightSide .power2").show(0);
			$board.find(".parachute2").fadeOut();
		})

		$board.find(".parachute3").animate({
			top: "50%",
			left: "65%"
		},paraTime,function () {
			$board.find(".powerDrop .rightSide .power3").show(0);
			$board.find(".parachute3").fadeOut();
		})

		$board.find(".parachute4").animate({
			top: "50%",
			left: "71%"
		},paraTime,function () {
			$board.find(".powerDrop .rightSide .power4").show(0);
			$board.find(".parachute4").fadeOut();
			planeGo();
		})

	}

	function planeGo () {
		$board.find(".plane").animate({
			left : "100%",
			top : "0%"
		},2000);
		$nextBtn.show(0);
	}

	function showNext () {
		$nextBtn.show(0);
	}

	function how_2 () {
		$board.find(".wrapperIntro.how .text4").show(0);
		$nextBtn.hide(0);
		ole.stringShow(".board .wrapperIntro.how .text4",data.string.p6_5," ",400,null,showNext);
		$board.find(".powerDrop .power").addClass("animated infinite flash");
	}

	function how_3 () {
		$board.find(".wrapperIntro.how .text5").show(0);
		$board.find(".wrapperIntro.how .text6").delay(1000).fadeIn();
		$nextBtn.show(0);
	}

	function how_4 () {
		$board.find(".lokharke .bubble2").show(0);
	}

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

	// 	$refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		var fnArray = [
				baseAndIndices,
				baseAndIndices_1,
				how,
				how_1,
				planeCome,
				how_2,
				how_3,
				how_4

			];
			/*var fnArray = [
				how,
				how_1,
				planeCome,
				how_2,
				how_3,
				how_4
			]*/

		function fnSwitcher () {

			// console.log(countNext);


			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
			/*load functions*/
			fnArray[countNext]();
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=14,
		timer = 100;
	var fnArray = [
			first,  //first one
			second, //second
			second_3,
			second_4,
			second_5,
			thirds, //third -->5
			third_all,
			third_all,
			third_all,
			third_all,
			third_all,
			forth, //forth -->10
			forth_2,
			forth_3,
			forth_4
		];
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,5,10]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "first",
		text : [data.string.p4_1,
			"2<sup>3</sup> =",
			" 2 x 2 x 2"]
	},{
		justClass : "second",
		text : [data.string.p4_2,
			data.string.example+" 1:",
			"",
			data.string.example+" 2:",
			"",
			function () {
				var d = data.string.p4_3;
				d = ole.textSR(d,data.string.p4_4,"<span>"+data.string.p4_4+"</span>");
				d = ole.textSR(d,data.string.p4_5,"<span>"+data.string.p4_5+"</span>")
				return d;
			},
			"",
			data.string.p4_6,
			],
		lokharke : "images/timetothink/timetothink1.png",
		lokharkeText : data.string.p4_7,
	},{
		justClass : "third",
		text : [
			data.string.p4_8,
			"","","",
			data.string.p4_9,
			data.string.p4_10,
			data.string.p4_11,
			data.string.p4_12,
			data.string.p4_13,
		]
	},{
		justClass : "forth",
		text : [
			data.string.p4_1,
			function () {
				var d = data.string.p4_14;
				d = ole.textSR(d,data.string.p4_15,"<span>"+data.string.p4_15+"</span>");
				d = ole.textSR(d,data.string.p4_16,"<span>"+data.string.p4_16+"</span>")
				return d;
			},
			"9 × 6 =  9 + 9 + 9 + 9 + 9 + 9",
			function () {
				var d = data.string.p4_17;
				d = ole.textSR(d,data.string.p4_18,"<span>"+data.string.p4_18+"</span>");
				d = ole.textSR(d,data.string.p4_19,"<span>"+data.string.p4_19+"</span>")
				return d;
			},
			"9<sup>6</sup> = 9 × 9 × 9 × 9 × 9 × 9",
			function () {
				var d = data.string.p4_20;
				d = ole.textSR(d,data.string.p4_16,"<span>"+data.string.p4_16+"</span>");
				d+= " ⇒ "+data.string.p4_19
				return d;
			},
			"9 + 9 + 9 + 9 + 9 + 9 ⇒ 9 × 6",
			function () {
				var d = data.string.p4_21;
				d = ole.textSR(d,data.string.p4_19,"<span>"+data.string.p4_19+"</span>");
				d+= " ⇒ "+data.string.p4_18
				return d;
			},
			"9 × 9 × 9 × 9 × 9 × 9 ⇒ 9<sup>6</sup>",
		]
	}]

	var dToShow = ["1 x 2 = 1 + 1",
			"2 x 3 = 2 + 2 +2"];


	function supMaker (base,power) {
		var arrays = [base,"<sup>"+power+"</sup>", " = "];
		arrays.push(base);
		for (var i = 0; i < power-1; i++) {
			arrays.push(" x ");
			arrays.push(base);
		};
		return arrays;
	}

	var thirdsVals = [
		supMaker(1,2),
		supMaker(2,3),
		supMaker(4,4),
	]

	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		};

		function second () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$nextBtn.hide(0);
			$board.find(".wrapperIntro.second .lokharke").hide(0);
			setTimeout(second_1,400);
		}

		function second_1 () {
			$board.find(".wrapperIntro.second .text1,.wrapperIntro.second .text2").show(0);
			ole.stringShow(".wrapperIntro.second .text2", dToShow[0], "", 500,null,second_2);
		}
		function second_2 () {
			$board.find(".wrapperIntro.second .text3,.wrapperIntro.second .text4").show(0);
			ole.stringShow(".wrapperIntro.second .text4", dToShow[1], "", 500,null,second_next);
		}
		function second_next () {
			$nextBtn.show(0);
		}
		function second_3 () {
			$board.find(".wrapperIntro.second .text5,.wrapperIntro.second .text6").show(0);
			$board.find(".wrapperIntro.second .text2").addClass('animated flash');
			$nextBtn.hide(0);
			ole.stringShow(".wrapperIntro.second .text6", dToShow[0], "", 500,null,second_next);
		}
		function second_4 () {
			$board.find(".wrapperIntro.second .text7").show(0);
		}

		function second_5 () {
			$board.find(".wrapperIntro.second .lokharke").addClass('animated slideInLeft').show(0);
		}

		var countHowmany = 0;
		function thirds () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$nextBtn.hide(0);
			third_1 ();
		}

		// thirds();

		function third_1 () {
			var counter = 0;
			var newArray = thirdsVals[countHowmany];
			var num = countHowmany+1;
			function looper () {
				$board.find(".wrapperIntro.third .text"+num).append(newArray[counter]);
				if (counter<newArray.length) {
					 counter++;
					setTimeout(looper,100);
				} else if (countHowmany<2) {
					countHowmany++;
					third_1();
				} else {
					$nextBtn.show(0);
				}
			}

			looper();
		}

		var thirdAllVal = 4
		function third_all () {
			$board.find(".wrapperIntro.third .text"+thirdAllVal).show(0);
			thirdAllVal++;
		}


		function forth () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.forth .text1, "
				+".wrapperIntro.forth .text2").show(0);
			forth_1();
			$nextBtn.hide(0);
		}

		// forth();

		function forth_1 () {
			$board.find(".wrapperIntro.forth .text2").html("");
			ole.stringShow(".wrapperIntro.forth .text2", "9 × 6 =  9 + 9 + 9 + 9 + 9 + 9", "", timer,null,second_next);
		}

		function forth_2 () {
			$nextBtn.hide(0);
			$board.find(".wrapperIntro.forth .text1, "
				+".wrapperIntro.forth .text2").hide(0);
			$board.find(".wrapperIntro.forth .text3, "
				+".wrapperIntro.forth .text4").show(0);
			$board.find(".wrapperIntro.forth .text4").html("9<sup>6</sup>");
			ole.stringShow(".wrapperIntro.forth .text4", " = 9 × 9 × 9 × 9 × 9 × 9", "", timer,null,second_next);
		}

		function forth_3 () {
			$board.find(".wrapperIntro.forth .text1, "
				+".wrapperIntro.forth .text2").show(0);
		}

		function forth_4 () {
			$board.find(".wrapperIntro.forth .text1, "
				+".wrapperIntro.forth .text2,"
				+".wrapperIntro.forth .text3,"
				+".wrapperIntro.forth .text4").hide(0);
			$board.find(".wrapperIntro.forth .text5, "
				+".wrapperIntro.forth .text6,"
				+".wrapperIntro.forth .text7,"
				+".wrapperIntro.forth .text8").show(0);
		}

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 11;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}



		function fnSwitcher () {
			// console.log(countNext+" "+fnArray[countNext]);
			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $(".prevButton"),
		countNext = 0,
		all_page=15,
		showCounter=0;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var base = 3, power = 4; //fourth function

	var clickArray = [0,11,13,14,15]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		text : [multiplications(9,10," "),
		" = 9<sup>10</sup>",
		data.string.p3_1,
		multiplications(9,25),
		" = 9<sup>25</sup>",
		data.string.p3_2,
		multiplications(9,75," "),
		data.string.p3_3+" <span>9<sup>75</sup></span>",
		data.string.p3_4,
		data.string.p3_5,
		data.string.p3_6,
		data.string.p3_7]
	},{
		justClass : "second",
		text1 : data.string.p3_8,
		example1 : data.string.example+": "+"a<sup>7</sup>",
		text2 : data.string.p3_9,
		text3 : function () {
			var m = "z";
			var power = 6;
			var d = data.string.example+": "+m+"<sup>"+power+"</sup>";
			for (var i = 0; i < power-1; i++) {
				m +="z"
			};
			d+=" = "+m;
			return d;
		},
	},{
		justClass : "second",
		text1 : data.string.p3_10,
		text4 : data.string.p3_11,
		example1 : data.string.p3_12+"<sup>4</sup>",
		example2 : data.string.p3_13,
		text2 : data.string.p3_14
	},{
		justClass : "activity1",
		text1 : function () {
				var d =  data.string.p3_15;
				d = ole.textSR(d,data.string.p3_19,"<b>"+data.string.p3_19+"</b>");
				d = ole.textSR(d,data.string.p3_18,"<b>"+data.string.p3_18+"</b>");
				return d;
			},
		text2 : data.string.p3_16,
		text3 : data.string.p3_17,
		base : base,
		power : power,
	},{
		justClass : "fifth",
		text : [
			data.string.p3_20,
			1,
			function () {
				var d = data.string.p3_21;
				d+=" ("+data.string.example+" 9<sup>1</sup> = 9)";
				return d;
			},
			0,
			function () {
				var d = data.string.p3_22;
				d+=" ("+data.string.example+" 9<sup>0</sup> = 1)";
				return d;
			},
			function () {
				var d =  data.string.p3_23;
				d = ole.textSR(d,"#00#","<span class='bold'>0<sup>0</sup></span>");
				d = ole.textSR(d,data.string.indeterminate,"<span class='italics'>"+data.string.indeterminate+"</span>");
				return d;
			},
		]
	}]

	function multiplications (a,b,c) {
		var m = a;
		// if (typeof c === 'undefined') {};
		c = typeof c === 'undefined' ? "":" "
		for (var i = 0; i < b-1; i++) {
			m+=c+"x"+c+a;
		};
		// console.log(m);
		return m;
	};

	function fnTimer () {
			$board.find(".wrapperIntro.second .box .text2").show(0);
			$nextBtn.hide(0);
			var tArray = [],rBase;
			var base = "a", power = 7;
			rBase=base;
			tArray.push(base+"<sup>"+power+"</sup>");
			tArray.push(" = ");
			for (var i = 0; i < power; i++) {
				tArray.push(base);
				if (i<power-1) {
					tArray.push(" x ");
				};
				rBase+=base;
			};
			tArray.push(" = ");
			tArray.push(rBase);
			// console.log(tArray);
			var c = 0;
			var len = tArray.length;
			function looper () {
				$board.find(".wrapperIntro.second .box .text2").append(tArray[c]);
				c++;
				if (c<len) {
					setTimeout(looper,500);
				} else {
					$nextBtn.show(0);
				}
			}
			looper();
		}

	/*
	* first
	*/
		function first() {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro>div").hide(0);
			$board.find(".wrapperIntro .text0").show(0);
			setTimeout(function () {
				$board.find(".wrapperIntro .text1").show(0);
				$nextBtn.show(0);
			},500);
			showCounter=1;
		};

		function justShow()
		{
			showCounter++;
			$board.find(".wrapperIntro .text"+showCounter).show(0);
			if (showCounter===8) {
				$board.find(".wrapperIntro .text6").addClass("animated infinite flash");
			} else if (showCounter>8) {
				$board.find(".wrapperIntro .text6").removeClass("animated infinite flash");
			}
		}


		// first func call
		first();

		function second() {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			fnTimer();
		};

		function second_1 () {
			$board.find(".wrapperIntro.second>.text2,.wrapperIntro.second>.text3").show(0);
		}

		function third() {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			// fnTimer();
		};

		function fourth () {
			var source = $("#exponents-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			ansManage();
		}

		// fourth();

		$board.on('click','.wrapperIntro.activity1 .base .arrow',function () {
			$this = $(this);
			var arrow = $this.data('arrow');
			if (arrow==='up') {
				base++;
			} else {
				base--;
			}
			$board.find('.wrapperIntro.activity1 .base .value').html(base);
			ansManage();
		});

		$board.on('click','.wrapperIntro.activity1 .power .arrow',function () {
			$this = $(this);
			var arrow = $this.data('arrow');
			if (arrow==='up') {
				power++;
			} else {
				power--;
			}
			$board.find('.wrapperIntro.activity1 .power .value').html(power);
			ansManage();
		});

		$board.on('click','.wrapperIntro.activity1 .reset',function () {
			base = dataObj[3].base;
			power = dataObj[3].power;
			$board.find('.wrapperIntro.activity1 .base .value').html(base);
			$board.find('.wrapperIntro.activity1 .power .value').html(power);
			ansManage();
		})

		function ansManage () {
			var showString;
			console.log(base+" "+power);
			if (base===0 && power ===0) {
				showString = "0<sup>0</sup> = "+data.string.indeterminate
			} else if (power>0) {
				showString = base;
				for (var i = 0; i < power-1; i++) {
					showString += " x "+base;
				};
				showString+=" = "+Math.pow(base,power);
			} else if (power<0) {
				var newPower = -(power);
				showString = "<span class='singular'>"
						+"<div class='one'>"+1+"</div>"
						+"<div class='base'>"+base+"</div>"
						+"</span>";
				for (var i = 0; i < newPower; i++) {
					showString +=" x "+"<span class='singular'>"
						+"<div class='one'>"+1+"</div>"
						+"<div class='base'>"+base+"</div>"
						+"</span>";
				};
				showString+=" = "+Math.pow(base,power);
			} else if (power===0) {
				showString = "1 = 1";
			};
			console.log(showString);
			$board.find(".wrapperIntro.activity1 .ans").html(showString);
		}

		function fifth () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4];
			var html = template(content);
			$board.html(html);
		}

		// fifth();

	/*click functions*/
		$nextBtn.click(function () {
			// $(this).css("display","none");
			// $prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

	// 	$refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function justShow() {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			console.log(countNext);
			if (countNext>=11) {
				$prevBtn.show(0);
				switch (countNext) {
					case 11:
						second();
						break;
					case 12:
						second_1();
						break;
					case 13 :
						third();
						break;
					case 14:
						fifth();
						break;
					case 15:
						fourth();
						break;

					default:break;
				}
			}

			else {
				// justshow();
				showCounter++;
				$board.find(".wrapperIntro .text"+showCounter).show(0);
				if (showCounter===8) {
					$board.find(".wrapperIntro .text6").addClass("animated infinite flash");
				} else if (showCounter>8) {
					$board.find(".wrapperIntro .text6").removeClass("animated infinite flash");
				}
			}

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

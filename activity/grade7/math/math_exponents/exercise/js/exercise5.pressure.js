Handlebars.registerHelper('equalsto', function(index) {
		console.log(index);
		if (index>0) {
			return "=";
		};
		return "";
	});

(function () {
	$(".title").show().css("display","block");

	var $wahtar=getSubpageMoveButton($lang,"next");

	var $board = $('.board');
	$nextBtn = $("#activity-page-next-btn-enabled"),
	$title = $('.title'),
	animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
	$refImg = $ref+"/exercise/images/";

	$nextBtn.html($wahtar);

	var tortoise = {
		pos : 0,
		initalPos : 5,
		finalPos : 90,
	};

	var question = {
		count : 0,
		range : 4,
		total : 4,
		correct : []
	};
	// var randomVals = ole.getRandom(question.range,question.total-1);
	// console.log(randomVals);
	// $board.html(randomVals)
/*
 _              _   _
| |_ _   _ _ __| |_| | ___
| __| | | | '__| __| |/ _ \
| |_| |_| | |  | |_| |  __/
 \__|\__,_|_|   \__|_|\___|

*/
	function goTurtle (len,nextFunc) {
		// console.log('tortoise.pos= '+tortoise.pos);
		var lastpos = tortoise.pos, i=lastpos;
		// tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(question.count-1)+tortoise.initalPos);
		tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(len-1)+tortoise.initalPos);
		var newGo = setInterval(function () {
			// console.log('lastpos = '+lastpos+" newPos="+tortoise.pos+" i="+i);
			if(i%2===0) {
				$('#tortoise2').show(0);
				$('#tortoise1').hide(0);
			} else {
				$('#tortoise1').show(0);
				$('#tortoise2').hide(0);
			}
			$('.tortoise').animate({'left' : i+"%"},100);
			i++;
			if (i>tortoise.pos) {
				clearInterval(newGo);
				// nextFunc();
			};
		},100);
	}

/*
* turtle out
*/

	var content = [
		{
			type:'',
			question : math.parseEquation("(5^3 * 10^6)/10^3"),
			// hint : "hint",
			answers: [
			{ans : math.parseEquation("5^6 * 2^3"),
				correct : "correct"},
			{ans : math.parseEquation("5^12 * 2^9")},
			{ans : math.parseEquation("5^6 * 2^9")},
			{ans : math.parseEquation("5^15 * 2^3")}
			],
			hintSolution : [
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation("")
			]
		},
		{
			type:'',
			question : math.parseEquation("6x^3/2x^3+2=?"),
			// hint : "hint",
			answers: [
			{ans : math.parseEquation("5"),
				correct : "correct"},
			{ans : math.parseEquation("4")},
			{ans : math.parseEquation("6")},
			{ans : math.parseEquation("3")}
			],
			hintSolution : [
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation("")
			]
		},{
			type:'',
			question : math.parseEquation("5x^4+ (3x^2)^2 +4x^10/2x^6=?"),
			// hint : "hint",
			answers: [
			{ans : math.parseEquation("10x^4")},
			{ans : math.parseEquation("16x^4"),
				correct : "correct"},
			{ans : math.parseEquation("22x^4")},
			{ans : math.parseEquation("18x^4")}
			],
			hintSolution : [
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation("")
			]
		},{
			type:'',
			question : math.parseEquation("(y^2 * y^5 * y^0 * y^6)/(y^2  y^5 * y^0) =?"),
			// hint : "hint",
			answers: [
			{ans : math.parseEquation("y^20")},
			{ans : math.parseEquation("y^6"),
				correct : "correct"},
			{ans : math.parseEquation("y^50")},
			{ans : math.parseEquation("y^20")}
			],
			hintSolution : [
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation("")
			]
		},{
			type:'',
			question : math.parseEquation("(x^6*(x^3)^2 * x^5)/(x^3)4=? "),
			// hint : "hint",
			answers: [
			{ans : math.parseEquation("x^29")},
			{ans : math.parseEquation("x^4")},
			{ans : math.parseEquation("x^9")},
			{ans : math.parseEquation("x^5"),
				correct : "correct"}
			],
			hintSolution : [
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation(""),
				math.parseEquation("")
			]
		},
	];

	function  qA () {
		$title.text(data.string.exerciseTitle_5).show(0);
		$('.advancement').show(0);
		var counter = question.count;

		loadTimelineProgress(question.total,question.count+1);
		content[counter].correctImg = $refImg+"correct.png";
		content[counter].wrongImg = $refImg+"wrong.png";
		content[counter].question =ole.textSR(content[counter].question,'sup2','<sup>2</sup>');
			if (content[counter].type==='multipleImg') {
				// alert();
				var source = $('#qAImage-templete').html();
			} else {
				var source = $('#qA-templete').html();
			}

			var template = Handlebars.compile(source);
			var html = template(content[counter]);
			$board.html(html);
			loadSolutionHint(counter);
	}

	qA ();

	function loadSolutionHint (num) {
		var source = $('#hintSolution-template').html();
		var template = Handlebars.compile(source);
		var html = template(content[num]);
		$(".hintBox").html(html);
	}

/*
* results
*/
	function results () {
		$title.hide(0);
		var len = question.correct.length;
		if(len < question.total) {
			$('.tortoise').animate({

			},600,function () {
				$(this).html("<img src='"+$refImg+"laydown.png' >");
				$(this).css({
					'width' : '20%',
					'bottom' : "-10%",
				});
			});

			$('#flag').css({
				'width' : '15%',
			},1000);
			if(len === 9){
				var endContent = {
					story : data.string.missedOne
				};
			} else if(len === 0){
				var endContent = {
					story : data.string.noTry
				};
			} else if(len <5){
				var endContent = {
					story : data.string.needMore
				};
			} else if(len <9){
				var endContent = {
					story : data.string.someMore
				};
			}
		} else {
			$('.tortoise').html("<img src='"+$refImg+"dance.gif' >").animate({
				'left' : '80%',
				'width' : '15%',
			},1000);
			$('#flag').animate({
				'right' : '35%',
				'width' : '15%',
			},1000);
			var endContent = {
				story : data.string.successStory
			};
		}

		var source = $('#results-template').html();
		var template = Handlebars.compile(source);
		var html = template(endContent);
		$board.html(html);
		$board.find('.endBoard').addClass('animated bounceInDown').one(animationEnd,function () {
			$('.repeat').show(0).addClass('animated bounceInDown');
		});

	}

	$board.on('click','.neutral',function () {
		// console.log("what");
		var $this = $(this);
		var isCorrect = $this.data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right');
			$('.neutral').removeClass('neutral');
			$nextBtn.show(0);
			$('.imgCrot .correctImg').show(0);
			question.correct.push(question.count);
			// console.log(question.correct);
			var len = question.correct.length;
			goTurtle (len);
		}
		else {
			$this.addClass('wrong');
			$('.neutral').not('.correct').addClass('wrong').removeClass('neutral');
			$('.correct').addClass('right animated  tada');
			$('.neutral').removeClass('neutral');
			$('.imgCrot .wrongImg').show(0);
			$nextBtn.show(0);

		}
	});

	$(".rhino").on('click',function () {
		$(".hintBox").show(0);
	});

	$nextBtn.on('click',function () {
		$(".hintBox").hide(0);
		$nextBtn.hide(0);
		question.count++;

		if(question.count<question.total){
			// goTurtle (qA);
			qA();
		}
		else if (question.count===question.total) {
			// goTurtle(results);
			// alert('finish');
			// $nextBtn.hide(0);
			results();
			ole.activityComplete.finishingcall();
			// ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// ole.activityComplete.finishingcall();
		}
	});

	$board.on('click','.storyWrapper .enter',qA);

})(jQuery);

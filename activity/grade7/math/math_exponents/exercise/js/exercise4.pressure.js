Handlebars.registerHelper('equalsto', function(index) {
		console.log(index);
		if (index>0) {
			return "=";
		};
		return "";
	});

(function () {
	$(".title").show().css("display","block");

	var $wahtar=getSubpageMoveButton($lang,"next");

	var $board = $('.board');
	$nextBtn = $("#activity-page-next-btn-enabled"),
	$title = $('.title'),
	animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
	$refImg = $ref+"/exercise/images/";

	$nextBtn.html($wahtar);

	var tortoise = {
		pos : 0,
		initalPos : 5,
		finalPos : 90,
	};

	var question = {
		count : 0,
		range : 5,
		total : 5,
		correct : []
	};
	// var randomVals = ole.getRandom(question.range,question.total-1);
	// console.log(randomVals);
	// $board.html(randomVals)
/*
 _              _   _
| |_ _   _ _ __| |_| | ___
| __| | | | '__| __| |/ _ \
| |_| |_| | |  | |_| |  __/
 \__|\__,_|_|   \__|_|\___|

*/
	function goTurtle (len,nextFunc) {
		// console.log('tortoise.pos= '+tortoise.pos);
		var lastpos = tortoise.pos, i=lastpos;
		// tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(question.count-1)+tortoise.initalPos);
		tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(len-1)+tortoise.initalPos);
		var newGo = setInterval(function () {
			// console.log('lastpos = '+lastpos+" newPos="+tortoise.pos+" i="+i);
			if(i%2===0) {
				$('#tortoise2').show(0);
				$('#tortoise1').hide(0);
			} else {
				$('#tortoise1').show(0);
				$('#tortoise2').hide(0);
			}
			$('.tortoise').animate({'left' : i+"%"},100);
			i++;
			if (i>tortoise.pos) {
				clearInterval(newGo);
				// nextFunc();
			};
		},100);
	}

/*
* turtle out
*/

	var content = [
	{
		type:'',
		question : math.parseEquation("( x^7 )^3 =?"),
		// question : math.parseEquation("(2^(22+3+4)) =?"),
		// hint : "hint",
		answers: [
		{ans : math.parseEquation("x^10")},
		{ans : math.parseEquation("x^4")},
		{ans : math.parseEquation("x^21"),
		correct : "correct"},
		{ans : math.parseEquation("x^2")}
		],
		hintSolution : [
			math.parseEquation("( a^2 )^5"),
			math.parseEquation("a^(2*5)"),
			math.parseEquation("a^10")
		]
	},{
		type:'',
		question : math.parseEquation("( 2x^3 )^3 = ?"),
		// hint : "hint",
		answers: [
		{ans : math.parseEquation("2x^9")},
		{ans : math.parseEquation("4x^9")},
		{ans : math.parseEquation("8x^9"),
		correct : "correct"},
		{ans : math.parseEquation("8x^6")}
		],
		hintSolution : [
			math.parseEquation("( a^2 )^5"),
			math.parseEquation("a^(2*5)"),
			math.parseEquation("a^10")
		]
	},{
		type:'',
		question : math.parseEquation("( 2x^3 / x^2 )^3 = ?"),
		// hint : "hint",
		answers: [
		{ans : math.parseEquation("8x^3"),
		correct : "correct"},
		{ans : math.parseEquation("8x^18")},
		{ans : math.parseEquation("8x^15")},
		{ans : math.parseEquation("8x^8")}
		],
		hintSolution : [
			math.parseEquation("(a^2/a^1)^5"),
			math.parseEquation("(a^(2-1))^5"),
			math.parseEquation("(a^1)^5"),
			math.parseEquation("a^(1*5)"),
			math.parseEquation("a^5")
		]
	},{
		type:'',
		question : math.parseEquation("2( x^2 )^3-( x^3 )^2 = ?"),
		// hint : "hint",
		answers: [
		{ans : math.parseEquation("x^5")},
		{ans : math.parseEquation("x^6"),
		correct : "correct"},
		{ans : math.parseEquation("x")},
		{ans : math.parseEquation("7x^6")}
		],
		hintSolution : [
			math.parseEquation("3a-2a"),
			math.parseEquation("a")
		]
	},{
		type:'',
		question : data.string.ques1 +" "+math.parseEquation("8x^4 -3( x^2 )^2 +6( x^2 ) ^3 = ? ")+" "+data.string.ques_1,
		// hint : "hint",
		answers: [
		{ans : math.parseEquation("464"),
		correct : "correct"},
		{ans : math.parseEquation("128")},
		{ans : math.parseEquation("134")},
		{ans : math.parseEquation("272")}
		],
		hintSolution : [
			"If a = 3, Find the value of "+math.parseEquation("5a^2 = ? "),
			math.parseEquation("5*(a)^2"),
			math.parseEquation("5*(3)^2"),
			math.parseEquation("5*9"),
			math.parseEquation("45")
		]
	},
	];

	function  qA () {
		$title.text(data.string.exerciseTitle_4).show(0);
		$('.advancement').show(0);
		var counter = question.count;

		loadTimelineProgress(question.total,question.count+1);
		content[counter].correctImg = $refImg+"correct.png";
		content[counter].wrongImg = $refImg+"wrong.png";
		content[counter].question =ole.textSR(content[counter].question,'sup2','<sup>2</sup>');
			if (content[counter].type==='multipleImg') {
				// alert();
				var source = $('#qAImage-templete').html();
			} else {
				var source = $('#qA-templete').html();
			}

			var template = Handlebars.compile(source);
			var html = template(content[counter]);
			$board.html(html);
			loadSolutionHint(counter);
	}

	qA ();

	function loadSolutionHint (num) {
		var source = $('#hintSolution-template').html();
		var template = Handlebars.compile(source);
		var html = template(content[num]);
		$(".hintBox").html(html);
	}

/*
* results
*/
	function results () {
		$title.hide(0);
		var len = question.correct.length;
		if(len < question.total) {
			$('.tortoise').animate({

			},600,function () {
				$(this).html("<img src='"+$refImg+"laydown.png' >");
				$(this).css({
					'width' : '20%',
					'bottom' : "-10%",
				});
			});

			$('#flag').css({
				'width' : '15%',
			},1000);
			if(len === 9){
				var endContent = {
					story : data.string.missedOne
				};
			} else if(len === 0){
				var endContent = {
					story : data.string.noTry
				};
			} else if(len <5){
				var endContent = {
					story : data.string.needMore
				};
			} else if(len <9){
				var endContent = {
					story : data.string.someMore
				};
			}
		} else {
			$('.tortoise').html("<img src='"+$refImg+"dance.gif' >").animate({
				'left' : '80%',
				'width' : '15%',
			},1000);
			$('#flag').animate({
				'right' : '35%',
				'width' : '15%',
			},1000);
			var endContent = {
				story : data.string.successStory
			};
		}

		var source = $('#results-template').html();
		var template = Handlebars.compile(source);
		var html = template(endContent);
		$board.html(html);
		$board.find('.endBoard').addClass('animated bounceInDown').one(animationEnd,function () {
			$('.repeat').show(0).addClass('animated bounceInDown');
		});

	}

	$board.on('click','.neutral',function () {
		// console.log("what");
		var $this = $(this);
		var isCorrect = $this.data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right');
			$('.neutral').removeClass('neutral');
			$nextBtn.show(0);
			$('.imgCrot .correctImg').show(0);
			question.correct.push(question.count);
			// console.log(question.correct);
			var len = question.correct.length;
			goTurtle (len);
		}
		else {
			$this.addClass('wrong');
			$('.neutral').not('.correct').addClass('wrong').removeClass('neutral');
			$('.correct').addClass('right animated  tada');
			$('.neutral').removeClass('neutral');
			$('.imgCrot .wrongImg').show(0);
			$nextBtn.show(0);

		}
	});

	$(".rhino").on('click',function () {
		$(".hintBox").show(0);
	});

	$nextBtn.on('click',function () {
		$(".hintBox").hide(0);
		$nextBtn.hide(0);
		question.count++;

		if(question.count<question.total){
			// goTurtle (qA);
			qA();
		}
		else if (question.count===question.total) {
			// goTurtle(results);
			// alert('finish');
			// $nextBtn.hide(0);
			results();
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// ole.activityComplete.finishingcall();
		}
	});

	$board.on('click','.storyWrapper .enter',qA);

})(jQuery);


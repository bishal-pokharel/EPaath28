$(function(){

	var $next_btn = $("#activity-page-next-btn-enabled");
	var $prev_btn = $("#activity-page-prev-btn-enabled");

	loadTimelineProgress(2, 1);
	var sld_no = 1;
	$next_btn.show();
	$next_btn.click(function(){
		sld_no++;
		$next_btn.hide(0);
		getCases(sld_no);
	});
	$prev_btn.click(function(){
		sld_no--;
		$prev_btn.hide(0);
		getCases(sld_no);
	});

	// first slide call
	slide_1();

	function getCases(slide_num){
		loadTimelineProgress(2, slide_num);
		switch(slide_num){
			case 1:
				slide_1();
			break;
			case 2:
				slide_2();
			break;
		}
	}


	function slide_1(){
		var content={
			p_class:"cover_title",
			p_data:data.lesson.chapter,
			img_class:"coverimg",
			img_src:$ref+"/images/page1/coverpage.png"
		}
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);

		var html = template(content);
		$("#cover_page").html(html);
		$next_btn.show(0);
	}

	function slide_2(){

			$("#cover_page, .coverimg, .cover_title").css("display", 'none');
			$(".questitle").html(data.string.p1_2);
			$(".myTitle").html(data.string.p1_3);
			$(".exp").html(data.string.p1_4);
			$("#closeImgbtn").html(getCloseBtn());

			var countClick=0;
			var totalCorrectCount = 0;
			var showNextPageNotificationAfterThisNumberOfCorrectChoice = 4;
			var firstVal,secondVal;

			var imgFirst, imgSec;
			$("#closeImgbtn").click(function(){
				countClick=0;
				totalCorrectCount++;
				$(".textBox").fadeOut(10);

				if(totalCorrectCount >= showNextPageNotificationAfterThisNumberOfCorrectChoice){
					ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);
				}
			});

			$('.imgSrc').bind('click', function(e){

				if(!$(this).find('img').hasClass("imgCircle")) {
				countClick++;
				if(countClick==1)
				{
					firstVal=$(this).attr('dataval');
					$(this).find('img').addClass('imgCircle');

					imgFirst=$(this).find('img').attr('src');
				}
				else if(countClick==2)
				{
					secondVal=$(this).attr('dataval');
					$('.imgSrc').find('img').removeClass('imgCircle');

					if(firstVal==secondVal)
					{
						imgSec=$(this).find('img').attr('src');

						var datas={src1:imgFirst, src2:imgSec};

						var source=$("#template-1").html();
						var template=Handlebars.compile(source);
						var html=template(datas);
						$(".textBox").fadeOut(10,function(){
							$(".myImg").html(html);
						}).fadeIn(10,function(){
							if(totalCorrectCount >= showNextPageNotificationAfterThisNumberOfCorrectChoice){
								ole.footerNotificationHandler.pageEndSetNotification();
									// ole.footerNotificationHandler.hideNotification();
							}
						});

					}
					else
					{
						$("#incorrect").fadeIn(10).delay(500).fadeOut(10,function(){
							countClick=0;
							$('.imgSrc').find('img').removeClass('imgCircle');
						});
					}
				}
				}

			});
	}


	// $(".questitle").html(data.string.p1_2);
	// $(".myTitle").html(data.string.p1_3);
	// $(".exp").html(data.string.p1_4);
	// $("#closeImgbtn").html(getCloseBtn());
	//
	// var countClick=0;
	// var totalCorrectCount = 0;
	// var showNextPageNotificationAfterThisNumberOfCorrectChoice = 4;
	// var firstVal,secondVal;
	//
	// var imgFirst, imgSec;
	// $("#closeImgbtn").click(function(){
	// 	countClick=0;
	// 	totalCorrectCount++;
	// 	$(".textBox").fadeOut(10);
	//
	// 	if(totalCorrectCount >= showNextPageNotificationAfterThisNumberOfCorrectChoice){
	// 		ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);
	// 	}
	// });
	//
	// $('.imgSrc').bind('click', function(e){
	//
	// 	if(!$(this).find('img').hasClass("imgCircle")) {
	// 	countClick++;
	// 	if(countClick==1)
	// 	{
	// 		firstVal=$(this).attr('dataval');
	// 		$(this).find('img').addClass('imgCircle');
	//
	// 		imgFirst=$(this).find('img').attr('src');
	// 	}
	// 	else if(countClick==2)
	// 	{
	// 		secondVal=$(this).attr('dataval');
	// 		$('.imgSrc').find('img').removeClass('imgCircle');
	//
	// 		if(firstVal==secondVal)
	// 		{
	// 			imgSec=$(this).find('img').attr('src');
	//
	// 			var datas={src1:imgFirst, src2:imgSec};
	//
	// 			var source=$("#template-1").html();
	// 			var template=Handlebars.compile(source);
	// 			var html=template(datas);
	// 			$(".textBox").fadeOut(10,function(){
	// 				$(".myImg").html(html);
	// 			}).fadeIn(10,function(){
	// 				if(totalCorrectCount >= showNextPageNotificationAfterThisNumberOfCorrectChoice){
	// 					ole.footerNotificationHandler.pageEndSetNotification();
	// 						// ole.footerNotificationHandler.hideNotification();
	// 				}
	// 			});
	//
	// 		}
	// 		else
	// 		{
	// 			$("#incorrect").fadeIn(10).delay(500).fadeOut(10,function(){
	// 				countClick=0;
	// 				$('.imgSrc').find('img').removeClass('imgCircle');
	// 			});
	// 		}
	// 	}
	// 	}
	//
	// });

});

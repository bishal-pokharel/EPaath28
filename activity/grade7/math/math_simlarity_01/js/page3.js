var counterMe=0;

$(function(){

  loadTimelineProgress(1,1);
	$(".headTitle").html(data.string.p3_1);
	$("#text1").html(data.string.p3_2);
	$("#text2").html(data.string.p3_3);

  $("#activity-page-next-btn-enabled").html(getSubpageMoveButton($lang,"next"));




    $("#retryBtnId").click(function(){
        location.reload();
    });


    var counterCnt=1;

    getHtml(counterCnt);

    $("#activity-page-next-btn-enabled").click(function(){

        $(this).hide(0);

        counterCnt++;

        getHtml();

    });

    $('#repeatBtn').click(function(){
        location.reload();
    });

    function getHtml()
    {
        loadTimelineProgress(14, counterCnt);

        var datas=getdata();

        var source=$("#template-1").html();

        var template=Handlebars.compile(source);
        var html=template(datas);



        $(".alloption").fadeOut(10,function(){

            $(this).html(html);

        }).delay(10).fadeIn(10,function(){

            $(".alloption").find(".draggable").draggable({ revert: true});
            dragdrop();
        });
    }

    function getdata()
    {
        var datas={img1:"img_"+counterCnt, imgid:"dropbox1", imgsrc:$ref+"/images/page3/"+counterCnt+".png"};
        if(counterCnt==3||counterCnt==6||counterCnt==8||counterCnt==12||counterCnt==13){
            datas={img1:"img_"+counterCnt, imgid:"dropbox2", imgsrc:$ref+"/images/page3/"+counterCnt+".png"};
            return datas;
        }
        else{
            return datas;
        }

    }
    function dragdrop(){
        $('.dropbox').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("id");
                var droppableans = $(this).attr("data-answer");
                if($(this).hasClass(draggableans)) {
                    console.log("if")
                    play_correct_incorrect_sound(1);
                    counterMe++;
                    var id = ui.draggable.attr('id');
                    ui.draggable.detach().appendTo($(this).find('div'));
                    if (counterMe < 14) {
                        $("#activity-page-next-btn-enabled").fadeIn();
                    }
                    else {

                        ole.footerNotificationHandler.lessonEndSetNotification();
                    }
                }
                else{
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }

});

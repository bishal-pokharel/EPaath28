$(function(){


	$(".headTitle").html(data.string.p2_1);
	$(".questitle").html(data.string.p2_2);
	$(".myTitle").html(data.string.p2_3);
	$(".exp").html(data.string.p2_4);
	$("#closeImgbtn").html(getCloseBtn());

	var countClick=0;
	var clickCnt=0;
	var showNextPageNotificationAfterThisNumberOfCorrectChoice = 4;
	var firstVal,secondVal;

	var imgFirst, imgSec;
	$("#closeImgbtn").click(function(){
		countClick=0;
		clickCnt++;
		$(".textBox").fadeOut(10);
		if(clickCnt >= showNextPageNotificationAfterThisNumberOfCorrectChoice ){
			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);
		}

	});

	$('.imgSrc').bind('click', function(e){

		countClick++;
		if(countClick==1)
		{
			firstVal=$(this).attr('dataval');
			$(this).find('img').addClass('imgCircle');


		}
		else if(countClick==2)
		{
			secondVal=$(this).attr('dataval');
			$('.imgSrc').find('img').removeClass('imgCircle');

			if(firstVal==secondVal)
			{
				imgSec=getGif(secondVal);

				var datas={src1:imgSec};

				var source=$("#template-1").html();
				var template=Handlebars.compile(source);
				var html=template(datas);
				$(".textBox").fadeOut(10,function(){
					$(".myImg").html(html);
				}).fadeIn(10,function(){
					if(clickCnt >= showNextPageNotificationAfterThisNumberOfCorrectChoice){
						ole.footerNotificationHandler.pageEndSetNotification();
							// ole.footerNotificationHandler.hideNotification();
					}
				});

			}
			else
			{
				$("#incorrect").fadeIn(10).delay(500).fadeOut(10,function(){
					countClick=0;
					$('.imgSrc').find('img').removeClass('imgCircle');
				});
			}
		}

	});



	function getGif(cls)
	{
		var imgsrc;
		switch(cls)
		{
			case "cls1":
				imgsrc=$ref+"/images/page2/img1.gif";
				break;

			case "cls2":
				imgsrc=$ref+"/images/page2/img5.gif";
				break;

			case "cls3":
				imgsrc=$ref+"/images/page2/img9.gif";
				break;

			case "cls4":
				imgsrc=$ref+"/images/page2/img7.gif";
				break;

			case "cls5":
				imgsrc=$ref+"/images/page2/img3.gif";
				break;

			case "cls6":
				imgsrc=$ref+"/images/page2/img11.gif";
				break;
		}
		return imgsrc;
	}

})


$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var counter=0;
	var totalCounter=10;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);

	var newarray=arrayquestion;


	var rightcounter=0, wrongCounter=0;

	myquestion(newarray[counter],counter);




	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2((rightcounter+1),wrongCounter);

		}

		if(counter > totalCounter){
			ole.activityComplete.finishingcall();
		}
	});

	$(".allquestion").on("click",".ans",function(){

		var isCorrect=parseInt($(this).attr('corr'));
		$("#showAns div").removeClass('ans');

		$("#showAns div[corr='1']").addClass('corrans');

		if(isCorrect==1)
		{
			$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);


			rightcounter++;
		}
		else
		{
			$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);


			wrongCounter++;
		}

		$("#activity-page-next-btn-enabled").fadeIn();
	});
});

var randOMNumber;

function myquestion(questionNo,counter)
{

	loadTimelineProgress(11,(counter+1));

	var myquestion,myanswer;


	var $dataval=getData(questionNo);

	var source;
	if(questionNo<10)
	{
		source   = $("#label-template").html();
	}
	else  {

		source   = $("#label-template-2").html();

	}




	var template = Handlebars.compile(source);


	var html=template($dataval);


	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#Imgshow").fadeOut(10);

	}).delay(100).fadeIn(10,function(){
		if(questionNo==10)
		{
			$(".allquestion").find(".draggable").draggable({ revert: "invalid"});

			$("#droppedBox1").dropMe('#img_1, #img_3');
			$("#droppedBox2").dropMe('#img_2, #img_4');
		}

	});
}

function getData(cnts)
{
	var dataval;


	switch(cnts)
	{
		case 1:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgObjval:$ref+"/exercise/images/1.png",
				optObj:[
						{optObj1:"obj_"+cnts+"_1", yesno:1, optObjval:data.string['e1q'+cnts+'_1']},
						{optObj1:"obj_"+cnts+"_2", yesno:0, optObjval:data.string['e1q'+cnts+'_2']},
						{optObj1:"obj_"+cnts+"_3", yesno:0, optObjval:data.string['e1q'+cnts+'_3']},
						{optObj1:"obj_"+cnts+"_4", yesno:0, optObjval:data.string['e1q'+cnts+'_4']}
				]
			}
		break;
		case 2:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgObjval:$ref+"/exercise/images/2.png",
				optObj:[
						{optObj1:"obj_"+cnts+"_1", yesno:0, optObjval:data.string['e1q'+cnts+'_1']},
						{optObj1:"obj_"+cnts+"_2", yesno:1, optObjval:data.string['e1q'+cnts+'_2']},
						{optObj1:"obj_"+cnts+"_3", yesno:0, optObjval:data.string['e1q'+cnts+'_3']}
				]
			}
		break;
		case 3:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgObjval:$ref+"/exercise/images/3.png",
				optObj:[
						{optObj1:"obj_"+cnts+"_1", yesno:1, optObjval:data.string['e1q'+cnts+'_1']},
						{optObj1:"obj_"+cnts+"_2", yesno:0, optObjval:data.string['e1q'+cnts+'_2']}
				]
			}
		break;
		case 4:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgObjval:$ref+"/exercise/images/10_1.png",
				optObj:[
						{optObj1:"obj_"+cnts+"_1", yesno:1, optObjval:data.string['e1q'+cnts+'_1']},
						{optObj1:"obj_"+cnts+"_2", yesno:0, optObjval:data.string['e1q'+cnts+'_2']}
				]
			}
		break;
		case 5:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgObjval:$ref+"/exercise/images/4.png",
				optObj:[
						{optObj1:"obj_"+cnts+"_1", yesno:0, optObjval:data.string['e1q'+cnts+'_1']},
						{optObj1:"obj_"+cnts+"_2", yesno:0, optObjval:data.string['e1q'+cnts+'_2']},
						{optObj1:"obj_"+cnts+"_3", yesno:1, optObjval:data.string['e1q'+cnts+'_3']}
				]
			}
		break;

		case 6:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgObjval:$ref+"/exercise/images/5.png",
				optObj:[
						{optObj1:"obj_"+cnts+"_1", yesno:1, optObjval:data.string['e1q'+cnts+'_1']},
						{optObj1:"obj_"+cnts+"_2", yesno:0, optObjval:data.string['e1q'+cnts+'_2']}
				]
			}
		break;
		case 7:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgObjval:$ref+"/exercise/images/6.png",
				optObj:[
						{optObj1:"obj_"+cnts+"_1", yesno:1, optObjval:data.string['e1q'+cnts+'_1']},
						{optObj1:"obj_"+cnts+"_2", yesno:0, optObjval:data.string['e1q'+cnts+'_2']}
				]
			}
		break;
		case 8:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgOpt:[
						{optObj1:"obj_"+cnts+"_1", yesno:0, imgOptSrc:$ref+"/exercise/images/6_1.png"},
						{optObj1:"obj_"+cnts+"_1", yesno:1, imgOptSrc:$ref+"/exercise/images/7.png"},
						{optObj1:"obj_"+cnts+"_1", yesno:0, imgOptSrc:$ref+"/exercise/images/8.png"},
						{optObj1:"obj_"+cnts+"_1", yesno:0, imgOptSrc:$ref+"/exercise/images/9.png"}
				]
			}
		break;
		case 9:
			dataval={
				myQuesion:data.string['e1q'+cnts],
				imgOpt:[
						{optObj1:"obj_"+cnts+"_1", yesno:0, imgOptSrc:$ref+"/exercise/images/7.png"},
						{optObj1:"obj_"+cnts+"_1", yesno:0, imgOptSrc:$ref+"/exercise/images/9.png"},
						{optObj1:"obj_"+cnts+"_1", yesno:0, imgOptSrc:$ref+"/exercise/images/10.png"},
						{optObj1:"obj_"+cnts+"_1", yesno:1, imgOptSrc:$ref+"/exercise/images/6_1.png"}
				]
			}
		break;

		case 10:
            $(".allquestion").css("font-size","0.6em");

			dataval={
				myQuesion:data.string['e1q'+cnts],
				titleBox1:data.string.e1q10_1,
				titleBox2:data.string.e1q10_2,
				imgOpt:[{imgOptSrc:$ref+"/exercise/images/6_1.png", imgname:"img_1"},
				{imgOptSrc:$ref+"/exercise/images/7.png", imgname:"img_2"},
				{imgOptSrc:$ref+"/exercise/images/8.png", imgname:"img_3"},
				{imgOptSrc:$ref+"/exercise/images/10.png", imgname:"img_4"}]
			}


		break;
	}

	return dataval;
}



function randomNumberFromRange(min,max)
{
    var randomNumber = Math.floor(Math.random()*(max-min+1)+min);

    return randomNumber;
}
function myquestion2(right,wrong)
{
	loadTimelineProgress(11,11);

	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);






	var $dataval={
		rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		allansw:[
			{ques:data.string["e1q1"],qans: data.string.e1q1_1,quesno:"ques_1"},
			{ques:data.string["e1q2"],qans: data.string.e1q2_2,quesno:"ques_2"},
			{ques:data.string["e1q3"],qans: data.string.e1q3_1,quesno:"ques_3"},
			{ques:data.string["e1q4"],qans: data.string.e1q4_1,quesno:"ques_4"},
			{ques:data.string["e1q5"],qans: data.string.e1q5_3,quesno:"ques_5"},
			{ques:data.string["e1q6"],qans: data.string.e1q6_1,quesno:"ques_6"},
			{ques:data.string["e1q7"],qans: data.string.e1q7_1,quesno:"ques_7"},
			{ques:data.string["e1q8"],imgques: $ref+"/exercise/images/7.png"} ,
			{ques:data.string["e1q9"],imgques: $ref+"/exercise/images/6_1.png"}

		]
	}


	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.exe3);
		$("#Imgshow").fadeOut(10);


	}).delay(100).fadeIn(10,function(){

		$("#activity-page-next-btn-enabled").delay(100).fadeIn(10);

	});
}


var countDrop=0;


$.fn.dropMe=function ($accept)
{
	var $this=$(this);

	$this.droppable({
        accept: $accept,
        drop: function( event, ui ) {
        	var id=ui.draggable.attr('id');

        	countDrop++;

        	var imgsrc=$("#"+id).attr('src');
        	$("#"+id).hide(0);

           	$(this).append('<img src="'+imgsrc+'"/>');
            if(countDrop>=4)
        	{
        		$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);


				$("#activity-page-next-btn-enabled").fadeIn();
        	}

        }

    });

};

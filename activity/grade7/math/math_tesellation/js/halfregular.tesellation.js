(function ($) {

	var $title = $('.title');
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var countHexagonClicked = 0;
	var countTriangleClicked = 0;
	var checkRepeat = 0;
	var $droppingImg;
	var dropHexaSquareCount = 0;
	var dropTriSquareCount = 0;
	var dropSquareTriCount = 0;
	var $tesellation = $('.tesellation');

	var $total_page = 6;
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.removeClass('glyphicon glyphicon-circle-arrow-right').html(getSubpageMoveButton($lang,"next"));

	$title.text(data.string.halfRegularTitle);

	$title.css({
		"top" : "30%"
	}).delay(400).fadeIn(400).delay(1000).animate({
		"top" : "0%"
	},200,function () {
		first();
	});


	function first () {
		var source = $('#firstPage-template').html();
		var template = Handlebars.compile(source);
		var content = {
			title : data.string.firstDesc ,
			img1 : $ref+"/images/page3/101.png",
			img2 : $ref+"/images/page3/102.png",
			img3 : $ref+"/images/page3/103.png",
			img4 : $ref+"/images/page3/104.png"
		}
		var html = template(content);
		$board.hide(0);
		$board.html(html).delay(400).fadeIn(function () {
			$nextBtn.delay(400).fadeIn();
		});
	}

	function second () {
		var source= $('#second-template').html();
		var template = Handlebars.compile(source);
		var content = {
			mainImg : $ref+"/images/page3/101.png",
			img1 : $ref+"/images/page3/hexagon.png",
			img2 : $ref+"/images/page3/traingle.png",
			desc : data.string.semiregularDoubleDesc
		}
		var html = template(content);
		$board.fadeOut(function () {
			$board.html(html).fadeIn();
		});

	}

	$board.on('click','.btnObj',function () {
		console.log("dfgdfgdfgdfgd");
		var btnVal = $(this).data('btnimg');
		if (btnVal === 1 ) {
			var $img = $ref+"/images/page3/tessellations_2.gif";
			countHexagonClicked = 1;
		}
		else if (btnVal ===2) {
			var $img = $ref+"/images/page3/tessellations_2a.gif";
			countTriangleClicked =1;
		}
console.log($board.find('.mainImg'));
console.log($img);
		$board.find('.mainImg').attr('src',$img);

		if(countTriangleClicked ===1 && countHexagonClicked===1){
			$nextBtn.fadeIn();
		}
	})

	function show360Sum () {
		$board.find('.mainImg').attr('src', $ref+"/images/page3/semiregularangle.gif");
		$board.find('.desc').fadeOut();
		$board.find('.btnImg').fadeOut(function () {
			$(this).html('120+60+120+60 = 360').delay(400).fadeIn(function () {
				var degree = ole.degree(data.string.semiregular320Angle);
				$board.find('.desc').html(degree).delay(800).fadeIn(function () {
					$nextBtn.fadeIn();
				});
			});
		});
	}

/**
* hexagon Square starts from here
*/

	function hexagonSquareStart () {

		var source = $('#drag-template').html();
		var template = Handlebars.compile(source);
		var content = {
			newClass : 'hexaSquareDrag',
			drag1 : $ref+'/images/hexasquare/sq1.png',
			arrow : $ref+'/images/arrow.png',
			text1 : data.string.hexasquare_1,
			image3 : $ref + "/images/hexasquare/01.png"
		}
		console.log(data.string.hexasquare_1);

		var html = template(content);
		$board.fadeOut(function () {
			$board.html(html).fadeIn();
			dropHexaSquare ();
			$droppingImg = $board.find('.dropholder img');
			$board.find('.hexaSquareDrag .image1').draggable({
				appendTo: ".mainBox",
				helper: "clone"
			});
		})

		dropHexaSquareCount = 0;
	}

	function dropHexaSquare () {
		$board.find('.dropholder').droppable({
			drop: function( event, ui ) {

				if (dropHexaSquareCount ===0) {
					$droppingImg.show(0);
					$board.find('.arrow').fadeOut();
					$board.find('.image1 img').attr('src',$ref+'/images/hexasquare/hexa01.png');
					dropHexaSquareCount++;
				}
				else if (dropHexaSquareCount>0 && dropHexaSquareCount<10) {
						$droppingImg.attr('src',$ref+"/images/hexasquare/0"+dropHexaSquareCount+".png");

					if (dropHexaSquareCount===16) {
						$board.find('.image1').fadeOut();
						$board.find('.text1').animate({"opacity": 0},400).text(data.string.hexagon_2).delay(1000).animate({"opacity": 1},400,function () {
							$nextBtn.fadeIn();	
						});
					}
				}

				if(dropHexaSquareCount===3){
					$board.find('.image1 img').attr('src',$ref+'/images/hexasquare/sq1.png');
				} else if (dropHexaSquareCount===7){
					$board.find('.image1 img').attr('src',$ref+'/images/hexasquare/hexa01.png');					
				}
				else if (dropHexaSquareCount===9){
					$board.find('.image1 img').delay(200).fadeOut(function () {
						$nextBtn.fadeIn()
					})
				}
				dropHexaSquareCount++;
			}
		})
	}

	function zoom1 () {

		// $board.find('.hexaSquareDrag .text1').text(data.string.sumIs360);
		$board.find('.hexaSquareDrag .dropholder img').attr('src',$ref+"/images/hexasquare/zoom.gif");
		setTimeout(repeatHolder,1000);
	}
/*
* tri square starts
*/

	function triSquareStart () {
		var source = $('#drag-template').html();
		var template = Handlebars.compile(source);
		var content = {
			newClass : 'triSquareDrag',
			drag1 : $ref+'/images/trisquare/sq3.png',
			arrow : $ref+'/images/arrow.png',
			text1 : data.string.hexasquare_1,
			image3 : $ref + "/images/trisquare/01.png"
		}

		var html = template(content);
		$board.fadeOut(function () {
			$board.html(html).fadeIn();
			dropTriSquare ();
			$droppingImg = $board.find('.dropholder img');
			$board.find('.TriSquareDrag .image1').draggable({
				appendTo: ".mainBox",
				helper: "clone"
			});
		})

		dropTriSquareCount = 0;
	}

	function dropTriSquare () {
		$board.find('.dropholder').droppable({
			drop: function( event, ui ) {

				if (dropTriSquareCount ===0) {
					$droppingImg.show(0);
					$board.find('.arrow').fadeOut();
					dropTriSquareCount++;
				}
				else if (dropTriSquareCount<10) {
					$droppingImg.attr('src',$ref+"/images/trisquare/0"+dropTriSquareCount+".png");
				}
				else if (dropTriSquareCount>10) {
					$droppingImg.attr('src',$ref+"/images/trisquare/"+dropTriSquareCount+".png");
				
					if (dropTriSquareCount===38) {
						$board.find('.image1').fadeOut();
						$board.find('.text1').animate({"opacity": 0},400).text(data.string.hexagon_2).delay(1000).animate({"opacity": 1},400,function () {
							// $tesellation.find('.repeatHolder').fadeIn();
							$nextBtn.fadeIn();
						});
					}
				}

				if(dropTriSquareCount===17){
					$board.find('.image1 img').attr('src',$ref+'/images/trisquare/tr02.png');
				} 

				dropTriSquareCount++;
			}
		})
	}

	function zoom2 () {
		// $board.find('.hexaSquareDrag .text1').text(data.string.sumIs360);
		$board.find('.triSquareDrag .dropholder img').attr('src',$ref+"/images/trisquare/zoom.gif");
		setTimeout(repeatHolder,1000);
	}

/*
*
*/
	function squareTriStart () {
		var source = $('#drag-template').html();
		var template = Handlebars.compile(source);
		var content = {
			newClass : 'SquareTriDrag',
			drag1 : $ref+'/images/squatri/tr01.png',
			arrow : $ref+'/images/arrow.png',
			text1 : data.string.hexasquare_1,
			image3 : $ref + "/images/squatri/01.png"
		}

		var html = template(content);
		$board.fadeOut(function () {
			$board.html(html).fadeIn();
			dropSquareTri ();
			$droppingImg = $board.find('.dropholder img');
			$board.find('.SquareTriDrag .image1').draggable({
				appendTo: ".mainBox",
				helper: "clone"
			});
		})

		dropSquareTriCount = 0;
	}

	function dropSquareTri () {
		$board.find('.dropholder').droppable({
			drop: function( event, ui ) {

				if (dropSquareTriCount ===0) {
					$droppingImg.show(0);
					$board.find('.arrow').fadeOut();
					dropSquareTriCount++;
				}
				else if (dropSquareTriCount<10) {
					$droppingImg.attr('src',$ref+"/images/squatri/0"+dropSquareTriCount+".png");
				}
				else if (dropSquareTriCount>10) {
					$droppingImg.attr('src',$ref+"/images/squatri/"+dropSquareTriCount+".png");
				
					if (dropSquareTriCount===23) {
						$board.find('.image1').fadeOut();
						$board.find('.text1').animate({"opacity": 0},400).text(data.string.hexagon_2).delay(1000).animate({"opacity": 1},400,function () {
							$nextBtn.fadeIn();
						});
					}
				}

				if(dropSquareTriCount===15){
					$board.find('.image1 img').attr('src',$ref+'/images/squatri/sq2.png');
				} 

				dropSquareTriCount++;
			}
		})
	}

	function zoom3 () {
		$board.find('.SquareTriDrag .dropholder img').attr('src',$ref+"/images/squatri/zoom.gif");
	}

/*
* angle zoom switch
*/
	function zoom () {
		$nextBtn.delay(1000).fadeIn();
		if (checkRepeat===0) {
			zoom1();
		} else if (checkRepeat===1) {
			zoom2();
		} else {
			zoom3();
		}
	}

/*
*
*/

	$tesellation.on('click','.repeatHolder',function () {
		$(this).hide(0);
		$nextBtn.hide(0);
		console.log('clicked repeat');
		if (checkRepeat === 0) {
			triSquareStart();
			console.log('clicked repeat 2');
		}
		else {
			squareTriStart();
		}
		checkRepeat++;
		countNext = 3;
	})

/*
*
*/

	function repeatHolder () {
		var source = $('#repeat-template').html();
		var template = Handlebars.compile(source);
		var content = {
			text1 : data.string.repeatOk
		}
		var html = template(content);
		$board.find('.text1').css('opacity',0);
		$tesellation.prepend(html);
		$nextBtn.fadeIn();
		
	}

/*
*
*/
	function football () {
		var source = $('#football-template').html();
		var template = Handlebars.compile(source);
		var content = {
			question : data.string.football,
			img1 : $ref+'/images/page3/football3.gif',
			img2 : $ref+'/images/page3/football2.gif',
			img3 : $ref+'/images/page3/football1.png',
			text1 : data.string.footballDesc
		}
		$tesellation.find('.repeatHolder').fadeOut();
		var html = template(content);
		$board.fadeOut(function () {
			$board.html(html).fadeIn();
		})
	}

/*
*
*/
	$nextBtn.on('click',function () {
		$title.fadeOut();
		$(this).fadeOut();
		switch (countNext) {
			case 0:
			second();
			break;

			case 1:
			show360Sum();
			break;

			case 2:
			hexagonSquareStart();
			break;

			case 3:
			zoom();
			break;


			case 4:
			football();
			break;
		}

		countNext++;
		loadTimelineProgress($total_page,countNext+1);

		if (countNext+1>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		};
	})
})(jQuery);
(function($) {
  var $title = $(".title");
  var $board = $(".tesellation");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $popUp = $(".popUp");
  var countNext = 0;
  var $total_page = 1;
  loadTimelineProgress($total_page, countNext + 1);
  //   $nextBtn
  //     .removeClass("glyphicon glyphicon-circle-arrow-right")
  //     .html(getSubpageMoveButton($lang, "next"));

  $title.text(data.string.irregularTitle);
  $title
    .css({
      "margin-left": "150%"
    })
    .animate(
      {
        "margin-left": "-30%"
      },
      400
    )
    .animate(
      {
        "margin-left": "30%"
      },
      200
    )
    .animate(
      {
        "margin-left": "-10%"
      },
      200
    )
    .animate(
      {
        "margin-left": "0%"
      },
      200,
      function() {
        first();
      }
    );

  function first() {
    var source = $("#first-template").html();
    var template = Handlebars.compile(source);
    var content = {
      desc: data.string.irregularDesc,
      //img1 : $ref+"/images/page4/01.png",
      img2: $ref + "/images/page4/04.jpg",
      img3: $ref + "/images/page4/02.png",
      img4: $ref + "/images/page4/06.jpg"
    };
    var html = template(content);
    $board.hide(0);

    $title.delay(400).fadeOut(function() {
      $board.html(html);
      $board.find("#img0, #img1,#img2, #img4").hide(0);
      $board.fadeIn();
      $board
        .find("#img0")
        .delay(200)
        .fadeIn();
      $board
        .find("#img1")
        .delay(600)
        .fadeIn();
      $board
        .find("#img2")
        .delay(1000)
        .fadeIn();
      $board
        .find("#img4")
        .delay(1200)
        .fadeIn();
      ole.footerNotificationHandler.lessonEndSetNotification();
    });
  }

  function second() {
    $nextBtn.fadeOut();
    $board.fadeOut(function() {
      $title.text(data.string.dailyTitle);
      $title
        .css({
          "margin-left": "150%"
        })
        .show(0)
        .animate(
          {
            "margin-left": "-30%"
          },
          400
        )
        .animate(
          {
            "margin-left": "30%"
          },
          200
        )
        .animate(
          {
            "margin-left": "-10%"
          },
          200
        )
        .animate(
          {
            "margin-left": "0%"
          },
          200,
          function() {
            locaters();
          }
        );
    });
    // $board.css({'top': '-28%'});
    ole.footerNotificationHandler.lessonEndSetNotification();
  }

  function locaters() {
    console.log("locaters");
    $board.html(" ");
    var source = $("#dailyBasis-template").html();
    var template = Handlebars.compile(source);
    var content = {
      desc: data.string.clickThem,
      img1: $ref + "/images/icon1.png",
      img2: $ref + "/images/icon2.png",
      img3: $ref + "/images/icon3.png",
      img4: $ref + "/images/icon4.png",
      img5: $ref + "/images/icon5.png",
      img6: $ref + "/images/icon6.png"
    };

    var html = template(content);
    console.log(html);
    $board.html(html).show(0);
    for (var i = 1; i <= 6; i++) {
      $board
        .find("#ico" + i)
        .delay(i * 400)
        .fadeIn();
    }

    $board
      .find(".desc2")
      .delay(2800)
      .animate({ opacity: 1 }, 700);
  }

  var clicked = {
    type1: 0,
    type2: 0,
    type3: 0,
    type4: 0,
    type5: 0,
    type6: 0
  };

  $board.on("click", ".icons", function() {
    console.log("clicked");
    var $data = $(this).data("iconval");
    console.log($data);
    var desc, $img;
    switch ($data) {
      case 1:
        clicked.type1 = 1;
        desc = data.string.dommy1;
        $img = $ref + "/images/irregular/art.png";
        break;

      case 2:
        clicked.type2 = 1;
        desc = data.string.dommy2;
        $img = $ref + "/images/irregular/house.png";
        break;

      case 3:
        clicked.type3 = 1;
        desc = data.string.dommy3;
        $img = $ref + "/images/irregular/road.png";
        break;

      case 4:
        clicked.type4 = 1;
        desc = data.string.dommy4;
        $img = $ref + "/images/irregular/chess.png";
        break;

      case 5:
        clicked.type5 = 1;
        desc = data.string.dommy5;
        $img = $ref + "/images/irregular/dhaka.png";
        break;

      case 6:
        clicked.type6 = 1;
        desc = data.string.dommy6;
        $img = $ref + "/images/irregular/modernbuil.png";
        break;
    }

    var source = $("#popUp-template").html();
    var template = Handlebars.compile(source);
    var content = {
      text1: desc,
      img1: $img
    };
    var html = template(content);
    $popUp.html(html).show(function() {
      ole.vCenter($popUp.find(".popUpIcon"));
    });
  });

  $popUp.on("click", ".clsBtn", function() {
    $popUp.hide(0);

    if (
      clicked.type1 === 1 &&
      clicked.type2 === 1 &&
      clicked.type3 === 1 &&
      clicked.type4 === 1 &&
      clicked.type5 === 1 &&
      clicked.type6 === 1
    ) {
      ole.footerNotificationHandler.lessonEndSetNotification();
    }
  });

  $nextBtn.on("click", function() {
    // second();
    // countNext++;
    loadTimelineProgress($total_page, countNext + 1);
  });
})(jQuery);

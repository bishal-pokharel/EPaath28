(function($) {
  var countNext = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $title = $(".title");
  var dropHexagonCount = 0;
  var $droppingImg = "";
  var dropSquareCount = 0;
  var dropTriangleCount = 0;
  var dropPentagonCount = 0;
  var $popUp = $(".popUp");

  var $total_page = 11;
  loadTimelineProgress($total_page, countNext + 1);
  $nextBtn
    .removeClass("glyphicon glyphicon-circle-arrow-right")
    .html(getSubpageMoveButton($lang, "next"));

  $title.text(data.string.regularTitle);

  /**
   * powering
   */
  function powerUp(string) {
    var changeText = "<sup>o</sup>";
    return string.replace("#^o^#", changeText);
  }

  /**
   * hexagons starts
   */
  (function hexagon() {
    var source = $("#watch1-template").html();
    var template = Handlebars.compile(source);

    var $text =
      data.string.regularText1 +
      "<span class='clicktext'>" +
      data.string.regularTextClick1 +
      "</span>";
    var content = {
      newId: "hexagon",
      text1: $text,
      image1: $ref + "/images/beehibe2.png"
    };
    var html = template(content);
    $board.html(html);
  })();

  $board.on("click", "#hexagon .image1", function() {
    // console.log("sjfsdjf");
    $board.find("#hexagon .text1").fadeOut();
    $board
      .find("#hexagon .image1")
      .find("img")
      .attr("src", $ref + "/images/bee_hibe.gif");
    $nextBtn.delay(5000).fadeIn();
  });

  function hexagonDrag() {
    var source = $("#drag-template").html();
    var template = Handlebars.compile(source);
    var content = {
      newClass: "hexagonDrag",
      drag1: $ref + "/images/hexagon/single.png",
      arrow: $ref + "/images/arrow.png",
      text1: data.string.hexagon_1,
      image3: $ref + "/images/hexagon/01.png"
    };
    var html = template(content);
    $title.fadeOut();
    $board.fadeOut(function() {
      $board.html(html).fadeIn();
      dropHexagon();
      $droppingImg = $board.find(".dropholder img");
      $board.find(".hexagonDrag .image1").draggable({
        appendTo: ".mainBox",
        helper: "clone"
      });
    });
    dropHexagonCount = 0;
  }

  function dropHexagon() {
    $board.find(".dropholder").droppable({
      drop: function(event, ui) {
        if (dropHexagonCount === 0) {
          $droppingImg.show(0);
          $board.find(".arrow").fadeOut();
          dropHexagonCount++;
        } else if (dropHexagonCount > 0 && dropHexagonCount < 8) {
          $droppingImg.attr(
            "src",
            $ref + "/images/hexagon/0" + dropHexagonCount + ".png"
          );

          if (dropHexagonCount === 7) {
            $board.find(".image1").fadeOut();
            $board
              .find(".text1")
              .animate({ opacity: 0 }, 400)
              .text(data.string.hexagon_2)
              .delay(1000)
              .animate({ opacity: 1 }, 400, function() {
                $nextBtn.fadeIn();
              });
          }
        }
        dropHexagonCount++;
      }
    });
  }

  function hexagonReplace() {
    $droppingImg.attr("src", $ref + "/images/hexagon/all.gif");
    var angleText = powerUp(data.string.hexagon_angle);
    $board
      .find(".text1")
      .animate({ opacity: 0 }, 400)
      .html(angleText)
      .delay(3000)
      .animate({ opacity: 1 }, 400, function() {
        $nextBtn.delay(1000).fadeIn();
      });
  }

  /**
   * square starts from here
   */

  function squareStart() {
    var source = $("#drag-template").html();
    var template = Handlebars.compile(source);
    var content = {
      newClass: "squareDrag",
      drag1: $ref + "/images/square/single.png",
      arrow: $ref + "/images/arrow.png",
      text1: data.string.square_1,
      image3: $ref + "/images/square/1.png"
    };
    var html = template(content);
    $board.fadeOut(function() {
      $board.html(html).fadeIn();
      dropSquare();
      $droppingImg = $board.find(".dropholder img");
      $board.find(".squareDrag .image1").draggable({
        appendTo: ".mainBox",
        helper: "clone"
      });
    });

    dropSquareCount = 0;
  }

  function dropSquare() {
    $board.find(".dropholder").droppable({
      drop: function(event, ui) {
        if (dropSquareCount === 0) {
          $droppingImg.show(0);
          $board.find(".arrow").fadeOut();
          dropSquareCount++;
        } else if (dropSquareCount > 0 && dropSquareCount < 17) {
          $droppingImg.attr(
            "src",
            $ref + "/images/square/" + dropSquareCount + ".png"
          );

          if (dropSquareCount === 16) {
            $board.find(".image1").fadeOut();
            $board
              .find(".text1")
              .animate({ opacity: 0 }, 400)
              .text(data.string.hexagon_2)
              .delay(1000)
              .animate({ opacity: 1 }, 400, function() {
                $nextBtn.fadeIn();
              });
          }
        }
        dropSquareCount++;
      }
    });
  }

  function squareReplace() {
    $droppingImg.attr("src", $ref + "/images/square/all.gif");
    var angleText = powerUp(data.string.square_angle);
    $board
      .find(".text1")
      .animate({ opacity: 0 }, 400)
      .html(angleText)
      .delay(3000)
      .animate({ opacity: 1 }, 400, function() {
        $nextBtn.delay(1000).fadeIn();
      });
  }

  /**
   * triangle starts
   */
  function triangleStart() {
    var source = $("#drag-template").html();
    var template = Handlebars.compile(source);
    var content = {
      newClass: "triangleDrag",
      drag1: $ref + "/images/triangle/single.png",
      arrow: $ref + "/images/arrow.png",
      text1: data.string.triangle_1,
      image3: $ref + "/images/triangle/01.png"
    };
    var html = template(content);
    $title.fadeOut();
    $board.fadeOut(function() {
      $board.html(html).fadeIn();
      dropTriangle();
      $droppingImg = $board.find(".dropholder img");
      $board.find(".triangleDrag .image1").draggable({
        appendTo: ".mainBox",
        helper: "clone"
      });
    });
    dropTriangleCount = 0;
  }

  function dropTriangle() {
    $board.find(".dropholder").droppable({
      drop: function(event, ui) {
        if (dropTriangleCount === 0) {
          $droppingImg.show(0);
          $board.find(".arrow").fadeOut();
          dropTriangleCount++;
        } else if (dropTriangleCount > 0 && dropTriangleCount < 10) {
          $droppingImg.attr(
            "src",
            $ref + "/images/triangle/0" + dropTriangleCount + ".png"
          );
        } else if (dropTriangleCount >= 10 && dropTriangleCount < 15) {
          $droppingImg.attr(
            "src",
            $ref + "/images/triangle/" + dropTriangleCount + ".png"
          );

          if (dropTriangleCount === 14) {
            $board.find(".image1").fadeOut();
            $board
              .find(".text1")
              .animate({ opacity: 0 }, 400)
              .text(data.string.hexagon_2)
              .delay(1000)
              .animate({ opacity: 1 }, 400, function() {
                $nextBtn.fadeIn();
              });
          }
        }
        dropTriangleCount++;
      }
    });
  }

  function triangleReplace() {
    $droppingImg.attr("src", $ref + "/images/triangle/all.gif");
    var angleText = powerUp(data.string.triangle_angle);
    $board
      .find(".text1")
      .animate({ opacity: 0 }, 400)
      .html(angleText)
      .delay(3000)
      .animate({ opacity: 1 }, 400, function() {
        $nextBtn.delay(1000).fadeIn();
      });
  }

  /**
   * pentagon
   */
  function pentagonStart() {
    var source = $("#drag-template").html();
    var template = Handlebars.compile(source);
    var content = {
      newClass: "pentagonDrag",
      drag1: $ref + "/images/pentagon/single.png",
      arrow: $ref + "/images/arrow.png",
      text1: data.string.pentagon_1,
      image3: $ref + "/images/pentagon/01.png"
    };
    var html = template(content);
    $title.fadeOut();
    $board.fadeOut(function() {
      $board.html(html).fadeIn();
      dropPentagon();
      $droppingImg = $board.find(".dropholder img");
      $board.find(".pentagonDrag .image1").draggable({
        appendTo: ".mainBox",
        helper: "clone"
      });
    });
    dropPentagonCount = 0;
  }

  function dropPentagon() {
    $board.find(".dropholder").droppable({
      drop: function(event, ui) {
        if (dropPentagonCount === 0) {
          $droppingImg.show(0);
          $board.find(".arrow").fadeOut();
          dropPentagonCount++;
        } else if (dropPentagonCount > 0 && dropPentagonCount < 9) {
          $droppingImg.attr(
            "src",
            $ref + "/images/pentagon/0" + dropPentagonCount + ".png"
          );

          if (dropPentagonCount === 8) {
            $board.find(".image1").fadeOut();
            $droppingImg.attr("src", $ref + "/images/pentagon/space.gif");
            $board
              .find(".text1")
              .animate({ opacity: 0 }, 400)
              .text(data.string.pentagon_no)
              .delay(1000)
              .animate({ opacity: 1 }, 400, function() {
                $nextBtn.fadeIn();
              });
          }
        }
        dropPentagonCount++;
      }
    });
  }

  function pentagonReplace() {
    $droppingImg.attr("src", $ref + "/images/pentagon/all.gif");
    var angleText = powerUp(data.string.pentagon_angle);
    $board
      .find(".text1")
      .animate({ opacity: 0 }, 400)
      .html(angleText)
      .delay(3000)
      .animate({ opacity: 1 }, 400, function() {
        $nextBtn.delay(1000).fadeIn();
      });
  }

  /**
   * summary
   */
  function summaryTable() {
    var source = $("#summaryTable-template").html();
    var template = Handlebars.compile(source);
    var content = {
      heading1: data.string.regularHeading1,
      heading2: data.string.regularHeading2,
      heading3: data.string.regularHeading3,
      heading4: data.string.regularHeading4,
      img1: $ref + "/images/triangle/14.png",
      img2: $ref + "/images/hexagon/07.png",
      img3: $ref + "/images/square/16.png",
      img4: $ref + "/images/pentagon/space.gif",
      text1: data.string.regularSummary1
    };

    var html = template(content);
    $board.fadeOut(function() {
      $board.html(html).fadeIn(function() {
        $board
          .find(".summay1")
          .delay(1000)
          .fadeIn(function() {
            $nextBtn.delay(800).fadeIn();
          });
      });
    });
  }

  function lastButNotLeast() {
    $title.hide(0);
    var source = $("#last-template").html();
    var template = Handlebars.compile(source);
    var angleText = powerUp(data.string.regularLastAns);

    var content = {
      text1: data.string.regularLast,
      text2: angleText,
      img1: $ref + "/images/triangle/14.png",
      img2: $ref + "/images/hexagon/07.png",
      img3: $ref + "/images/square/16.png",
      img4: $ref + "/images/pentagon/space.gif"
    };

    var html = template(content);
    $board.html(html);
  }

  var clicked = {
    id1: 0,
    id2: 0,
    id3: 0,
    id4: 0
  };

  $board.on("click", ".imgObj", function() {
    console.log("whay no print??");
    var getDataVal = $(this).data("varid");
    console.log($(this));
    var $image, $c, $ans;
    var $a = 360;
    var yesOrNo = data.string.wholeNo;
    switch (getDataVal) {
      case "id1":
        clicked.id1 = 1;
        $image = "01.png";
        $c = 60;
        $ans = $a / $c;
        break;

      case "id3":
        clicked.id3 = 1;
        $image = "02.png";
        $c = 90;
        $ans = $a / $c;
        break;

      case "id2":
        clicked.id2 = 1;
        $image = "03.png";
        $c = 120;
        $ans = $a / $c;
        break;

      case "id4":
        clicked.id4 = 1;
        $image = "04.png";
        $c = 108;
        $ans = $a / $c;
        yesOrNo = data.string.notWholeNo;
        break;
    }

    var source = $("#popup-template").html();
    var template = Handlebars.compile(source);
    var content = {
      images: $ref + "/images/last/" + $image,
      varA: $a,
      varC: $c,
      ans: $ans,
      text: yesOrNo
    };
    var html = template(content);
    $popUp.html(html).show(0);
    console.log(html);
  });

  $popUp.on("click", ".clsBtn", function() {
    $popUp.hide(0);
    if (
      clicked.id1 === 1 &&
      clicked.id3 === 1 &&
      clicked.id3 === 1 &&
      clicked.id4 === 1
    ) {
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
  /**
   * next button commands
   */

  $nextBtn.on("click", function() {
    $nextBtn.fadeOut();
    switch (countNext) {
      case 0:
        hexagonDrag();
        break;

      case 1:
        hexagonReplace();
        break;

      case 2:
        squareStart();
        break;

      case 3:
        squareReplace();
        break;

      case 4:
        triangleStart();
        break;

      case 5:
        triangleReplace();
        break;

      case 6:
        pentagonStart();
        break;

      case 7:
        pentagonReplace();
        break;

      case 8:
        summaryTable();
        break;

      case 9:
        lastButNotLeast();
        ole.footerNotificationHandler.pageEndSetNotification();
        break;

      // case 10:
      // break;
    }

    countNext++;
    loadTimelineProgress($total_page, countNext + 1);
  });
})(jQuery);

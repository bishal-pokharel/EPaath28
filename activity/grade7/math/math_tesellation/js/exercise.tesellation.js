(function($) {
  var $board = $(".board"),
    loadSvg = "",
    loadBg = "",
    $popUp = $(".tesellation .popUp"),
    $clickedName = "",
    $nextBtn = $("#activity-page-next-btn-enabled"),
    clickedCount = 0;

  var $whatnextbtn = getSubpageMoveButton($lang, "next");
  $nextBtn.html($whatnextbtn);
  loadTimelineProgress(1, 1);

  $(".clickedAlready").text(data.string.clickedAlready);
  // alert("smth");
  function loadTesellation(givenId) {
    console.log("i reached here");
    var tesellation = Snap(".board .svgHolder");
    Snap.load($ref + "/images/exercise/click.svg", function(f) {
      // console.log("m i here");
      var roof = f.select("#roof"),
        window1 = f.select("#window1"),
        window2 = f.select("#window2"),
        wall = f.select("#wall"),
        goalPost = f.select("#goalPost"),
        football = f.select("#football");
      footpath = f.select("#footpath");
      door = f.select("#door");
      backWindow = f.select("#backWindow");
      backWall = f.select("#backWall");

      roof.click(function() {
        clickData.roof.isT = 1;
        clickFunc("roof");
      });
      window1.click(function() {
        clickData.windows.isT = 1;
        clickFunc("windows");
      });
      window2.click(function() {
        clickData.windows.isT = 1;
        clickFunc("windows");
      });
      wall.click(function() {
        clickData.wall.isT = 1;
        clickFunc("wall");
      });
      goalPost.click(function() {
        clickData.goalPost.isT = 1;
        clickFunc("goalPost");
      });
      football.click(function() {
        clickData.football.isT = 1;
        clickFunc("football");
      });
      footpath.click(function() {
        clickData.footpath.isT = 1;
        clickFunc("footpath");
      });
      door.click(function() {
        clickFunc("door");
      });
      backWindow.click(function() {
        clickFunc("backWindow");
      });
      backWall.click(function() {
        clickFunc("backWall");
      });

      tesellation.append(f);
      loadSvg = "ok";
    });
  }

  function first() {
    var source = $("#first-template").html();
    var template = Handlebars.compile(source);
    var content = {
      text1: data.string.intro_ex,
      pic1: $ref + "/images/exercise/paint.png",
      bgImg: $ref + "/images/exercise/background.png"
    };
    var html = template(content);
    $board.html(html);
    loadBg = "ok";
    loadTesellation();
  }

  first();

  var setInt = setInterval(function() {
    console.log(loadSvg + " === " + loadBg);
    if (loadSvg === "ok" && loadBg === "ok") {
      $("#preload").hide(0);
      console.log("cleared");
      clearInterval(setInt);
      suggestToDo();
    }
  }, 10);

  $board.on("click", ".pictureOnWall", function() {
    clickFunc("picture");
  });

  function clickFunc(clickedName) {
    if (typeof clickData[clickedName].checkClick === "undefined") {
      console.log(clickedName);
      var source = $("#popFirst-template").html();
      var template = Handlebars.compile(source);
      clickData[clickedName].showAns = data.string.correct;
      var html = template(clickData[clickedName]);
      $popUp.html(html).show(0);
      $clickedName = clickedName;
      // ole.vCenter(".popUp .popHolder",3);
    } else {
      $(".clickedAlready")
        .show(0)
        .addClass("animated bounceInDown")
        .one(
          "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
          function() {
            setTimeout(function() {
              $(".clickedAlready")
                .removeClass("animated bounceInDown")
                .fadeOut();
            }, 3000);
          }
        );
    }
  }

  $nextBtn.on("click", function() {
    ole.activityComplete.finishingcall();
  });

  /*
   * question and answers datas
   */

  var clickData = {
    wall: {
      img: $ref + "/images/exercise/show04.png",
      question: data.string.exQ2,
      options: [
        { ans: data.string.shape1 },
        { ans: data.string.shape2, correct: "correct" },
        { ans: data.string.shape3 }
      ],
      say: data.string.saywall
    },
    roof: {
      img: $ref + "/images/exercise/roof.png",
      question: data.string.exQ10,
      options: [
        { ans: data.string.ansYes, correct: "correct" },
        { ans: data.string.ansNo }
      ],
      say: data.string.sayroof
    },
    windows: {
      img: $ref + "/images/exercise/show06.png",
      question: data.string.exQ1,
      options: [
        { ans: data.string.type1 },
        { ans: data.string.type2, correct: "correct" },
        { ans: data.string.type3 }
      ],
      say: data.string.saywindows
    },
    goalPost: {
      img: $ref + "/images/exercise/show03.png",
      question: data.string.exQ10,
      options: [
        { ans: data.string.ansYes, correct: "correct" },
        { ans: data.string.ansNo }
      ],
      say: data.string.saygoalpost
    },
    football: {
      img: $ref + "/images/exercise/show02.png",
      question: data.string.exQ2,
      options: [
        { ans: data.string.shape4 },
        { ans: data.string.shape5 },
        { ans: data.string.shape6, correct: "correct" }
      ],
      say: data.string.sayfootball
    },
    footpath: {
      img: $ref + "/images/exercise/show01.png",
      question: data.string.exQ2,
      options: [
        { ans: data.string.type1, correct: "correct" },
        { ans: data.string.type2 },
        { ans: data.string.type3 }
      ],
      say: data.string.sayfootpath
    },
    picture: {
      img: $ref + "/images/exercise/show05.png",
      question: data.string.exQ1,
      options: [
        { ans: data.string.type1 },
        { ans: data.string.type2 },
        { ans: data.string.type3, correct: "correct" }
      ],
      say: data.string.saypicture,
      isT: 1
    },
    backWall: {
      img: $ref + "/images/exercise/show07.png",
      question: data.string.exQ10,
      options: [
        { ans: data.string.ansYes },
        { ans: data.string.ansNo, correct: "correct" }
      ],
      say: data.string.saybackWall
    },
    door: {
      img: $ref + "/images/exercise/door.png",
      question: data.string.exQ10,
      options: [
        { ans: data.string.ansYes },
        { ans: data.string.ansNo, correct: "correct" }
      ],
      say: data.string.saydoor
    },
    backWindow: {
      img: $ref + "/images/exercise/show08.png",
      question: data.string.exQ10,
      options: [
        { ans: data.string.ansYes },
        { ans: data.string.ansNo, correct: "correct" }
      ],
      say: data.string.saybackWindow
    }
  };

  /*
   * shows suggest what to d'
   */
  function suggestToDo() {
    $board
      .find(".suggestToDo")
      .show(0)
      .addClass("animated bounce");
  }

  $board.on("click", ".clsBtn-ToDo", function() {
    $(this)
      .parent()
      .addClass("animated bounceOut");
  });

  /**
   * after pop up
   */
  $popUp.on("click", ".clsBtn-pop", function() {
    $popUp.hide(0);
    if (clickedCount >= 7) {
      // alert("done it");
      lastMsg();
    }
  });

  // $popUp.on('click','.questHolder li',function () {
  // 	var $clicked = $(this).data('check');
  // 	if($clicked==="correct"){
  // 		$popUp.find('.showAns').show(0).addClass('animated zoomInDown')
  // 		$popUp.find('.clsBtn-pop').addClass('animated bounce');
  // 		clickData[$clickedName].checkClick = 1;
  // 		$(this).addClass('right');
  // 		if(typeof clickData[$clickedName].isT!='undefined'){
  // 			clickedCount ++;
  // 			// alert("clicked");
  // 		}

  // 	} else {
  // 		$(this).addClass('animated tada wrong');
  // 	}
  // })

  $popUp.on("click", ".questHolder .opts", function() {
    var $check = $(this).data("check");

    if ($check === "correct") {
      $popUp
        .find(".opts")
        .addClass("wrongFound")
        .removeClass("opts");
      $popUp.find(".correct").addClass("right");
      var $correct0oO = data.string.correct + "!";
    } else {
      $popUp
        .find(".opts")
        .addClass("wrong")
        .removeClass("opts");
      $popUp.find(".correct").addClass("animated tada right");
      var $correct0oO = data.string.wrong + "!";
    }

    console.log($check + " = " + $clickedName);

    clickData[$clickedName].checkClick = 1;
    if (typeof clickData[$clickedName].isT != "undefined") {
      clickedCount++;
    }

    var newText = $correct0oO + " " + clickData[$clickedName].say;
    $popUp
      .find(".showAns")
      .text(newText)
      .show(0)
      .addClass("animated zoomInDown");
  });

  function lastMsg() {
    var source = $("#lastMsg-template").html();
    var template = Handlebars.compile(source);
    var content = {
      msg: data.string.lastMsg
    };
    var html = template(content);
    $(".lastPop")
      .html(html)
      .show(0);

    $nextBtn.show(0);
    // ole.vCenter('.lastPop .wrapPop',3);
  }

  $(".lastPop").on("click", ".glyphicon-repeat", function() {
    location.reload();
  });

  $board.on("click", ".goalPostTop", function() {
    clickData.goalPost.isT = 1;
    clickFunc("goalPost");
  });
})(jQuery);

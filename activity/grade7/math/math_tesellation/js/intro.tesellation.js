(function($) {
  var count = 0;
  var countBullets = 0;
  var $question = $(".question");
  var $obj = $(".objects");
  var $markAns = $(".markAns");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var countNext = 0;
  var $tesellation = $(".tesellation");

  var $total_page = 6;
  loadTimelineProgress($total_page, countNext + 1);
  $nextBtn
    .removeClass("glyphicon glyphicon-circle-arrow-right")
    .html(getSubpageMoveButton($lang, "next"));

  $question.text(data.string.intro_1_q);

  // $obj.find('#img1 img').delay(400).fadeIn(function () {
  // 	$obj.find('#img2 img').delay(800).fadeIn(function () {
  // 		$obj.find('#img3 img').delay(800).fadeIn(function  () {
  // 			$question.delay(800).fadeIn(800);
  // 			$follow.delay(1200).fadeIn();
  // 		});
  // 	})
  // })

  $(".objt").on("click", function() {
    $id = $(this).data("ids");
    if ($id === 2) {
      if (count === 0) {
        console.log(count);
        $markAns
          .hide(0)
          .css("background-color", "green")
          .text(data.string.introRightGuess)
          .delay(400)
          .fadeIn(function() {
            $nextBtn.fadeIn();
          });
      }
    } else {
      $markAns
        .hide(0)
        .css("background-color", "red")
        .text(data.string.introWrongGuess)
        .delay(400)
        .fadeIn();
    }
  });

  function showNextPart() {
    console.log(count);
    count = 1;
    $obj.fadeOut(function() {
      // $('#img3').width("50%");
      $(".lastImg")
        .delay(600)
        .fadeIn(function() {
          $question.text(data.string.introShow).fadeIn();
          $nextBtn.fadeOut();
          setTimeout(function() {
            $nextBtn.fadeIn();
          }, 1500);
        });
    });
  }

  /*
   * adding
   */

  function nextpageCollide() {
    $obj.show(0);
    $(".lastImg").fadeOut();
    $(".objt")
      .find("img")
      .removeAttr("style")
      .fadeIn();
    $question.text(data.string.viewNicely);
    $question.delay(1000).fadeIn(600, function() {
      $nextBtn.delay(400).fadeIn();
    });
  }

  function bulletsLoaded() {
    var source = $("#budha-template").html();
    var template = Handlebars.compile(source);
    var content = {
      question: data.string.intro2Question,
      img1: $ref + "/images/01.png",
      img2: $ref + "/images/02.png",
      img3: $ref + "/images/03.png",
      imgcorrect: $ref + "/images/correct.png",
      allQ1: data.string.allQ1,
      q1: data.string.q1,
      q2: data.string.q2,
      q3: data.string.q3,
      ans1: data.string.yes,
      ans2: data.string.no
    };
    var html = template(content);
    $tesellation.html(html);
    $(".allQ")
      .delay(400)
      .fadeIn(function() {
        loadBullet();
        yesNoClick();
      });
  }

  function loadBullet() {
    countBullets++;
    $this = $tesellation.find(".quests>ul li:nth-child(" + countBullets + ")");
    $this.fadeIn();
    // $this.find('.yesNo').fadeIn();
  }

  function yesNoClick() {
    var $ans = 0;
    $tesellation.find(".yesNo").on("click", "li", function() {
      $ans = $(this).data("ans");
      $question = $tesellation.find(
        ".quests>ul li:nth-child(" + countBullets + ")"
      );
      $question.find(".yesNo").hide(0);
      if ($ans === 1) {
        $question.find("img").fadeIn();
      }
      console.log(countBullets);

      loadBullet();
      if (countBullets > 3) {
        $tesellation.find(".nextBtn").show(0);
        $("#activity-page-next-btn-enabled")
          .removeClass("glyphicon glyphicon-circle-arrow-right")
          .html(getSubpageMoveButton($lang, "next"));
        newClick();
      }
    });
  }

  function newClick() {
    $tesellation.on("click", ".nextBtn", function() {
      var source = $("#defination-template").html();
      var template = Handlebars.compile(source);
      var content = {
        title: data.string.tesellationDefnTitle1,
        image: $ref + "/images/tessellations.gif",
        defination: data.string.tesellationDefn1
      };
      countNext++;
      loadTimelineProgress($total_page, countNext + 1);
      var html = template(content);
      $tesellation.html(html);
      $tesellation
        .find(".defination .text2")
        .delay(6000)
        .fadeIn(function() {
          ole.footerNotificationHandler.pageEndSetNotification();
        });
    });
  }

  switch (countNext) {
    case 0:
      $(".chapter_name").html(eval("data.lesson.chapter"));
      $nextBtn.fadeIn(0);
      break;
    default:
  }
  if (countNext == 4 && countNext == 5) {
    $nextBtn.hide(0);
  }

  $nextBtn.on("click", function() {
    switch (countNext) {
      case 0:
        $obj
          .find("#img1 img")
          .delay(400)
          .fadeIn(function() {
            $obj
              .find("#img2 img")
              .delay(800)
              .fadeIn(function() {
                $obj
                  .find("#img3 img")
                  .delay(800)
                  .fadeIn(function() {
                    $question.delay(800).fadeIn(800);
                  });
              });
          });

        $(".coverimg").hide(0);
        $nextBtn.hide(0);
        break;
      case 1:
        $("#img1 img").fadeOut(600, function() {
          $("#img3 img").fadeOut(600, function() {
            $(".question, .markAns").fadeOut(function() {
              showNextPart();
            });
          });
        });
        break;

      case 2:
        nextpageCollide();
        break;

      case 3:
        bulletsLoaded();
        break;
    }

    countNext++;
    loadTimelineProgress($total_page, countNext + 1);
  });
})(jQuery);

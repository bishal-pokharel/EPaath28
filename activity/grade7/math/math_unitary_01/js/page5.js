$(function(){

	var $myid;

	$("#head_title").html(data.string.p5_1);


	var $myid;

	var questionArray=new Array(1,2,3,4,5);

	var suffleArr=questionArray;

	var whatCount=1;
	var arrIndex=0;

		loadTimelineProgress(2,1);

	$("#exampDef").html(data.string["p3_0"]+" "+whatCount);



	$('#head_title').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

		$("#exampDef").fadeIn(10,function(){

			$(this).addClass('animated flash');
			$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(this).removeClass('animated flash');
				getTempFunc(whatCount);
			});//animate complete

		});



	});//animate complete



	$("#activity-page-next-btn-enabled").click(function(){
		loadTimelineProgress(2,2);
		arrIndex++;
		whatCount++;
		$("#exampleList").html('').fadeOut(10);

		$("#exampDef").html(data.string["p3_0"]+" "+whatCount).addClass('animated flash');
		$("#exampDef").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

			$(this).removeClass('animated flash');
				getTempFunc(whatCount);
		});//animate complete
		$(this).hide(0);



	});


	$("#activity-page-prev-btn-enabled").click(function(){
		loadTimelineProgress(2,1);
		arrIndex--;
		whatCount--;
		$("#exampleList").html('').fadeOut(10);

		$("#exampDef").html(data.string["p3_0"]+" "+whatCount).addClass('animated flash');
		$("#exampDef").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

			$(this).removeClass('animated flash');
				getTempFunc(whatCount);
		});//animate complete



	});




});

function getDataVar($i)
{
	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	var $datavar;

	switch($i)
	{
		case 1:
			$datavar={
				questionTxt:data.string.p5_38,
				isnotlast:1,
				imgnext:$whatnextbtn
			}
			break;
		case 2:
			$datavar={
				questionTxt:data.string.p5_2,
				isnotfirst:1,
				imgprev:$whatprevbtn
			}
			break;
	}




	return $datavar;
}


function getTemplateHtml($i)
{

	var dataval=getDataVar($i)


	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(dataval);

	return html;
}

function parseQuestion($i)
{


	switch($i)
	{
		case 1:

			ole.parseToolTip("#questionTxt",data.string.p5_38_r_1);
			ole.parseToolTip("#questionTxt",data.string.p5_38_r_2);


			break;
		case 2:
			ole.parseToolTip("#questionTxt",data.string.p5_2_r_1);
			ole.parseToolTip("#questionTxt",data.string.p5_2_r_2);
			ole.parseToolTip("#questionTxt",data.string.p5_2_r_3);
			ole.parseToolTip("#questionTxt",data.string.p5_2_r_4);

			break;

	}

	$("#questionTxt").children('span:nth-of-type(n+1)').css({"color":"black"});
}



function getTempFunc($i)
{

	var htmlTemp=getTemplateHtml($i);

	$("#exampleList").html(htmlTemp).fadeIn(100,function(){


		parseQuestion($i);


		getAnimation($i);



	});


}


function getAnimation($i)
{
	switch($i)
		{
			case 1:
				animation_1($i);
				break;
			case 2:
				animation_2($i);
				break;

		}

}
function animation_1($i)
{
	var parser;
	/*
	F50CCA 	intentDing 	**/

	parser=[
		{"datavalue":data.string.p5_40_r_1,"replaceValue":''},
		{"datavalue":data.string.p5_40_r_2,"replaceValue":''},
		{"datavalue":data.string.p5_40_r_3,"replaceValue":'$${427/7}$$'}
	];
	addDivtoAns(".ansDiv","p5_40",'hideThis','',data.string.p5_40,parser,"#F73802");
	M.parseMath(document.body);
	$("#p5_40").delay(600).fadeIn(300,function(){

		parser=[
		{"datavalue":data.string.p5_44_r_1,"replaceValue":'$${3/7}$$'},
		{"datavalue":data.string.p5_44_r_2,"replaceValue":''},
		{"datavalue":data.string.p5_44_r_3,"replaceValue":''}
		];
		addDivtoAns(".ansDiv","p5_44",'hideThis','',data.string.p5_44,parser,"#F73802");
		M.parseMath(document.body);
		$("#p5_44").delay(600).fadeIn(300,function(){

		});

		$(".ansDiv").append('<div id="p5_46" class="conCluTxt hideThis ">'+data.string['p5_46']+'</div>');

			$("#p5_46").delay(600).fadeIn(300,function(){
			$("#activity-page-next-btn-enabled").fadeIn(300);

		});

	});//fadein





}

function animation_2($i)
{
	var parser;


	parser=[
		{"datavalue":data.string.p5_2_r_1,"replaceValue":''},
		{"datavalue":data.string.p5_2_r_2,"replaceValue":''},
		{"datavalue":data.string.p5_2_r_3,"replaceValue":''},
		{"datavalue":data.string.p5_2_r_4,"replaceValue":''}
	];
	addDivtoAns(".ansDiv","p5_2_a",'hideThis','',data.string.p5_2_a,parser,"#F73802");

	$("#p5_2_a").delay(600).fadeIn(300,function(){

		parser=[
		{"datavalue":data.string.p5_3_r_1,"replaceValue":''},
		{"datavalue":data.string.p5_3_r_2,"replaceValue":''}
		];
		addDivtoAns(".ansDiv","p5_3",'hideThis','',data.string.p5_3,parser,"#F73802");

		$("#p5_3").delay(600).fadeIn(300,function(){


			parser=[
			{"datavalue":data.string.p5_4_r_1,"replaceValue":''},
			{"datavalue":data.string.p5_4_r_2,"replaceValue":''}
			];
			addDivtoAns(".ansDiv","p5_4",'hideThis','',data.string.p5_4,parser,"#F73802");

			$("#p5_4").delay(600).fadeIn(300,function(){

				parser=[
				{"datavalue":data.string.p5_6_r_1,"replaceValue":''},
				{"datavalue":data.string.p5_6_r_2,"replaceValue":'$${720/12}$$'},
				{"datavalue":data.string.p5_6_r_3,"replaceValue":''}
				];
				addDivtoAns(".ansDiv","p5_6",'hideThis','',data.string.p5_6,parser,"#F73802");

				M.parseMath(document.body);

				$("#p5_6").delay(600).fadeIn(300,function(){

					parser=[
					{"datavalue":data.string.p5_7_r_1,"replaceValue":''},
					{"datavalue":data.string.p5_7_r_2,"replaceValue":data.string.p5_7_r_2_a},
					{"datavalue":data.string.p5_7_r_3,"replaceValue":''}
					];
					addDivtoAns(".ansDiv","p5_7",'hideThis','',data.string.p5_7,parser,"#F50CCA");

					$("#p5_7").delay(600).fadeIn(300,function(){

						parser=[
						{"datavalue":data.string.p5_8_r_1,"replaceValue":''},
						{"datavalue":data.string.p5_8_r_2,"replaceValue":data.string.p5_8_r_2_a},
						{"datavalue":data.string.p5_8_r_3,"replaceValue":''}
						];
						addDivtoAns(".ansDiv","p5_8",'hideThis','',data.string.p5_8,parser,"#F50CCA");

						$("#p5_8").delay(600).fadeIn(300,function(){

								parser=[
							{"datavalue":data.string.p5_9_r_1,"replaceValue":''},
							{"datavalue":data.string.p5_9_r_2,"replaceValue":data.string.p5_9_r_2_a},
							{"datavalue":data.string.p5_9_r_3,"replaceValue":''}
							];
							addDivtoAns(".ansDiv","p5_9",'hideThis','',data.string.p5_9,parser,"#F50CCA");

							$("#p5_9").delay(600).fadeIn(300,function(){



							});	//fadein

							$(".ansDiv").append('<div id="p5_10" class="conCluTxt hideThis intentDing5">'+data.string['p5_10']+'</div>');

							$("#p5_10").delay(1200).fadeIn(300,function(){

								$("#activity-page-prev-btn-enabled").fadeIn(300,function(){
										ole.footerNotificationHandler.lessonEndSetNotification();
								});
							});




						});	//fadein


					});	//fadein


				});	//fadein


			});	//fadein


		});	//fadein


	});	//fadein

}



function addnextBtn($where, $what, $class)
{

	var $what="<div id='"+$what+"' class='"+$class+"'><span class='glyphicon glyphicon-arrow-right'></span></div>";
	$($where).append($what);
}

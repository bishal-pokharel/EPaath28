$(function(){

	var $myid;
	var counterMe=0;
	$("#animateBox").addClass('cursorWait').fadeOut(0);
	$("#head_title").html(data.string.p1_1);
	var $kk;
	for($kk=4;$kk<=8;$kk++)
	{
		$("#p1_"+$kk).html(data.string["p1_"+$kk]);
	}

	for($kk=20;$kk<=26;$kk++)
	{
		$("#p1_"+$kk).html(data.string["p1_"+$kk]);
	}

	$("#p1_2").html(data.string.p1_2_a);
	$("#p1_3").html(data.string.p1_3_a);
	$("#p1_1_1").html(data.string.p1_1_1);
	$("#p1_1_2").html(data.string.p1_1_2);
	
	
	

	$('#head_title').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

		$("#p1_1_1").fadeIn(0).addClass('animated tada');

		$("#animateBox").delay(1000).fadeIn(0,function(){

			$("#p1_2").delay(100).fadeIn(0,function()
			{
				$(this).addClass('animated bounceInleft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){

					$(this).removeClass('animated bounceInleft');
				});
			});
			$("#p1_3").delay(100).fadeIn(0,function()
			{
				$(this).addClass('animated bounceInRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){

					$(this).removeClass('animated bounceInRight');
				
				});
			});

			$(".defThis").delay(1200).fadeIn(500,function(){
			
				$(".examplehead").fadeIn(500,function(){
				
					$(".exampleObj").fadeIn(500,function(){

						$("#p1_2, #p1_3").addClass('type_txt clickMeTxt');
						$("#animateBox").removeClass('cursorWait');
					});
							
				});//examplehead fadein

			});//defthis


		});
		


		

				
		
	});

	
	$("#animateBox").on('click','.clickMeTxt',function(){

		$("#mathContent").fadeOut(10);
		$myid=$(this).attr('id');
		
		counterMe++;
		show_2($myid);
		


	});//click

	
	$("#mathContent2").on('click','#nextidBtn2',function(){

		


		$("#mathContent2").fadeOut(10);
		if($(this).hasClass('whatNext_p1_2'))
		{
			 $myid="p1_2";
		}
		else
		{
			$myid="p1_3";
		}
		show_3($myid);
	});//click

	$("#mathContent2").on('click','#closeBtnid',function(){

			$("#mathContent").fadeIn(10);
			$("#mathContent2").fadeOut(10);
			$("#mathContent2").html('');

	});


	$("#mathContent2").on('click','#previdBtn',function(){

		if($(this).hasClass('whatPrev_p1_2'))
		{
			 $myid="p1_2";
		}
		else
		{
			$myid="p1_3";
		}

		$("#mathContent2").fadeOut(10).html('');

		
		show_2($myid);
	});

});

function show_2($clickid)
{

	var datavar;

	if($clickid=="p1_2")
	{
		datavar={
			innertitle:data.string.p1_9,
			definition:data.string.p1_10,
			direct:1,
			id:$clickid,
			weight:data.string.p1_11,
			cost:data.string.p1_12,
			p1_10_1:data.string.p1_10_1,
			p1_10_2:data.string.p1_10_2,
			conTxt:data.string.p1_17
		}
		var source=$("#table-template").html();
	}
	else
	{

		datavar={
			innertitle:data.string.p1_25,
			definition:data.string.p1_26,
			id:$clickid,
			weight:data.string.p1_27,
			cost:data.string.p1_28,
			questionTxt:data.string.p1_29,
			ansObj:[data.string.p1_30,data.string.p1_31,data.string.p1_32],
			conTxt:data.string.p1_33,
			labelwhat:data.string.p1_42,
			conCluCar:data.string.p1_44_6
		}

		var source=$("#table-template2").html();
	}//else

	
	var template=Handlebars.compile(source);
	var html=template(datavar);
	$("#mathContent2").html(html).delay(100).fadeIn(300,function(){


		$("#inner_title").fadeIn(10,function(){
			$(this).addClass('animated pulse');

			$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

				$(".def-me").delay(200).fadeIn(500,function()
				{
					if($clickid=="p1_2")
					{	
						$("#p1_10_1").delay(200).fadeIn(500,function()
						{
							$("#p1_10_2").delay(200).fadeIn(500,function(){
								$(".tabMe").delay(200).fadeIn(500);
							});
						});
						
					}
					else
					{
						$(".carAnimation").delay(200).fadeIn(500);	
					}
				});//def show

			});//animate done

		});//inner show

	});	

	if($clickid=="p1_2")
	{	directClick();
	}
	else
	{
		inDirectClick();
	}

}
function inDirectClick()
{
	
	$("#timerCar").html(data.string.p1_28+":");
	var whatBlink=1
	var whatTime=3000;
	$("#switch_car").delay(3500).fadeIn(500,function(){

		$("#what_1").addClass('blinkLabelCar');
	});

	$("#show_2").on('click',".blinkLabelCar",function(){

		$('#whatTxtCar').fadeOut(0);

		$("#timerCar").html(data.string.p1_28+":");
		$("#carimg").removeAttr('style');
		$("#what_"+whatBlink).removeClass('blinkLabelCar');

		var whatTimestamp=new Date().getTime();

		$("#carimg").find('img').attr('src',$ref+'/images/car-moving.gif?'+whatTimestamp);
		$("#carimg").animate({'left':'80%'},whatTime,function(){
			$(this).find('img').attr('src',$ref+'/images/car.png');
			var timertime;
			switch(whatBlink)
			{
				case 1: timertime=60; $('#whatTxtCar').html(data.string.p1_44_2);  whatTime=1500; break;

				case 2: timertime=30; $('#whatTxtCar').html(data.string.p1_44_3); whatTime=1000;break;

				case 3: timertime=20; $('#whatTxtCar').html(data.string.p1_44_4); whatTime=750; break;
				case 4: timertime=15; $('#whatTxtCar').html(data.string.p1_44_5); break;
			}

			$("#timerCar").html(data.string.p1_28+":"+timertime+"s");

			$('#whatTxtCar').fadeIn(300,function(){

				whatBlink++;
			
				if(whatBlink==5)
				{
					

					$(".tabMe").delay(100).fadeIn(0).addClass('animated lightSpeedIn');

					
					$("#conCluCar").delay(1800).fadeIn(400,function()
						{
							$("#nextidBtn2").fadeIn(10);
						});
				}
				else
					$("#what_"+whatBlink).addClass('blinkLabelCar');
				
			});//fadein
			

		});

	});
	
}


function directClick()
{

	

	$("#show_2").on('keyup',"#inFirst",function(){

		var pattern=/^\d*$/;

        var val=$("#inFirst").val();


        if(!val.match(pattern))
       	{
	       	$("#inFirst").val('').addClass('borderMe');
        }
	});

	$("#show_2").on('change',"#inFirst",function(){
	
		var thisVal=parseInt($(this).val());
		var checkVal;
		
		
			checkVal=300;
		

		
		if(thisVal==checkVal)
		{

			alert(thisVal+" is right answer.");
			$(".inputTd").html(checkVal);

		}
		else
		{

			alert(thisVal+" is wrong answer. Right answer is "+checkVal);
			$(".inputTd").html(checkVal);
		}

		
		$(".conTxt").fadeIn(500,function(){
			$("#nextidBtn2").fadeIn(10);

		});
	});
	

	$("#show_2").on('click',".spanLi",function(){

		var thisval;

		var checkVal;

		
			checkVal=data.string.p1_14;
		

		//check correct answer
		

		$(".ansTxt").find('li').each(function(j, li){
           
           thisval=$(this).find('span').html();
          if(thisval==checkVal)
          {
          	$(this).append('<img src="'+$ref+'/images/correct.png" />');
          	$(this).removeClass('clickLi');
          }
          else
          {
          	$(this).append('<img src="'+$ref+'/images/incorrect.png" />');
          	$('.clickLi').not(this).addClass('noclickLi').removeClass('clickLi');
          }
        });

		

		
		
	});
}

function show_3($clickid)
{

	var datavar;
	var clickCount=1;
	var leftVal=18; //initial value of left of blinking circle

	if($clickid=="p1_2")
	{
		datavar={
			innertitle:data.string.p1_9,
			direct:1,
			id:$clickid,
			weight:data.string.p1_11,
			cost:data.string.p1_12,
			tabTitle:data.string.p1_34,
			graphtittle:data.string.p1_35,
			imggr:$ref+"/images/graph1/1.png",
			ycor:data.string.p1_37,
			xcor:data.string.p1_36,
			showall:data.string.p1_38,
			showlastCon:data.string.p1_35_1
		}
	}
	else
	{

		datavar={
			innertitle:data.string.p1_25,
			id:$clickid,
			weight:data.string.p1_42,
			cost:data.string.p1_43,
			tabTitle:data.string.p1_40,
			graphtittle:data.string.p1_41,
			imggr:$ref+"/images/graph2/1.png",
			ycor:data.string.p1_43,
			xcor:data.string.p1_42,
			showall:data.string.p1_38
		}
	}//else

	var source=$("#graph-template").html();
	var template=Handlebars.compile(source);
	var html=template(datavar);
	$("#mathContent2").html(html).delay(100).fadeIn(300,function(){

		$("#inner_title").fadeIn(10,function(){
			$(this).addClass('animated pulse');

			$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

				$(".TabTitle").delay(200).fadeIn(500,function(){

					$(".graph").fadeIn(10,function(){
						$(this).addClass('animated lightSpeedIn');

						$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

							
								$("#circleX").addClass('clickthis');
								if($clickid=="p1_3")
								{
									$("#circleX").css({'top':'85.5%','left':'29%'});

								}

							});

						});//complete animate
					

				});//fadein
				

			});//animate done

		});//inner show
	});

	$("#show_3").on('click','.clickthis',function(){

		
		clickCount++;
		
		
		
		var compare;

		if($clickid=="p1_2")
		{
			leftVal+=13;
			$(".graphImg").find('img').attr('src',$ref+'/images/graph1/'+clickCount+'.png');
			$("#graphconclusion").html(data.string["p1_39_"+clickCount]);
			compare=8;
		}
		else
		{

			if(clickCount==2)
			{
				leftVal=48;
			} 
			else if(clickCount==3)
			{
				leftVal=69;
			} 
			else if(clickCount==4)
			{
				leftVal=90;
			} 
			
			
			

			$(".graphImg").find('img').attr('src',$ref+'/images/graph2/'+clickCount+'.png');
			$("#graphconclusion").html(data.string["p1_44_"+clickCount]);
			compare=5;
		}

		if(clickCount<compare)
		{
			$(this).animate({'left':leftVal+'%'},1000);
		}
		else
		{
			$(this).fadeOut(30);

			$("#showall").fadeIn(10);
		}
		
	});


	$("#show_3").on('click','#showall',function(){
		$(this).hide(0);
		if($clickid=="p1_2")
		{
			$(".graphImg").find('img').attr('src',$ref+'/images/graph1/9.png');
			
			$("#graphconclusion").html(data.string["p1_39_9"]);
			$("#graphTitle").delay(100).fadeIn(200);
			$("#showlastCon").delay(100).fadeIn(200);

		}
		else
		{
			$(".graphImg").find('img').attr('src',$ref+'/images/graph2/6.png');
			
			$("#graphconclusion").html(data.string["p1_44_6"]);
			$("#graphTitle").delay(100).fadeIn(200);
			$("#showlastCon").delay(100).fadeIn(200);
			

		}
		
		$("#previdBtn, #closeBtnid").delay(200).fadeIn(200);
	});





}
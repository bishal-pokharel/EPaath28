$(function(){

	var $myid;

	var $next_btn = $("#activity-page-next-btn-enabled");
	var $prev_btn = $("#activity-page-prev-btn-enabled");

	loadTimelineProgress(2, 1);
	var sld_no = 1;
	$next_btn.show();
	$next_btn.click(function(){
		sld_no++;
		$next_btn.hide(0);
		getCases(sld_no);
	});
	$prev_btn.click(function(){
		sld_no--;
		$prev_btn.hide(0);
		getCases(sld_no);
	});

	// first slide call
	slide_1();

	$(".covertext").html(eval("data.lesson.chapter"));
	function getCases(slide_num){
		loadTimelineProgress(2, slide_num);
		switch(slide_num){
			case 1:
				slide_1();
			break;
			case 2:
				slide_2();
			break;
		}
	}


	function slide_1(){
		var content={
			p_class:"cover_title",
			p_data:data.lesson.chapter,
			img_class:"coverimg",
			img_src:$ref+"/images/page1/coverpage.png"
		}
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);

		var html = template(content);
		$("#cover_page").html(html);
		$next_btn.show(0);
	}

	function slide_2(){
		$("#cover_page, .coverimg, .cover_title").css("display", 'none');
		for(var $i=1; $i<=7;$i++)
		{
			$("#p0_"+$i).html(data.string["p0_"+$i]);
		}

		$("#head_title").html(data.string.p1_1);
			$("#p0_1").delay(100).fadeIn(100,function(){


				$("#p0_2").delay(100).fadeIn(0).addClass('animated bounceInLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

					ole.parseToolTip("#p0_3",data.string.p0_3_r_1);
					ole.parseToolTip("#p0_3",data.string.p0_3_r_2);
					ole.parseToolTip("#p0_3",data.string.p0_3_r_3);
					$("#p0_3").children('span:nth-of-type(n+1)').css({"color":"#F50CCA"});

					$("#p0_3").delay(100).fadeIn(0).addClass('animated bounceInRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){


						var countPen=0;

						var myVar=setInterval(function(){

								countPen++;
								$("#pnc_"+countPen).fadeIn(0).addClass('animated bounceInleft');
								if(countPen==5)
									clearInterval(myVar);

							},300);//setinterval

						$("#txtval_1").fadeIn(0).addClass('animated bounceIndown');

						$("#p0_4").delay(2200).fadeIn(0).addClass('animated bounceInright').delay(2500).fadeOut(20,function(){

							var countCoin=0;
							var myVar2=setInterval(function(){

								countCoin++;
								$("#coins_"+countCoin).fadeIn(0).addClass('animated bounceInright');
								if(countCoin==5)
									clearInterval(myVar2);

							},300); //setinterval


							setTimeout(function(){

								var counteql=1;
								var myVar3=setInterval(function(){

									counteql++;
									$("#txtval_"+counteql).fadeIn(0).addClass('animated bounceIn');
									if(counteql==5)
									clearInterval(myVar3);

								},300); //setinterval

									setTimeout(function(){
										$("#id_5").addClass('animated bounceOutRight').fadeOut(0);
										$("#id_4").addClass('animated bounceOutLeft').fadeOut(0);

										$("#id_3").addClass('animated bounceOutRight').fadeOut(0);
										$("#id_2").addClass('animated bounceOutLeft').fadeOut(0, function(){


											$("#p0_5").fadeIn(0).addClass('animated bounceInright');
											$("#p0_6").delay(1500).fadeIn(0).addClass('animated bounceInLeft');

											ole.parseToolTip("#p0_7",data.string.p0_7_r_1);
											ole.parseToolTip("#p0_7",data.string.p0_7_r_2);
											$("#p0_7").children('span:nth-of-type(n+1)').css({"color":"#F50CCA"});
											$("#p0_7").delay(2500).fadeIn(0).addClass('animated bounceInright');
											$("#id_6").delay(3000).fadeIn(0,function(){

												ole.footerNotificationHandler.pageEndSetNotification();
											});



										});//fadeout
									},4000);


							},4000);



						});//fadeout

					});//complete animation

				});//complete animation

			});//fadein

	}

});

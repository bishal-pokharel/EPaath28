$(function(){

	var $myid;



	$("#head_title").html(data.string.p4_1);


	var $myid;

	var questionArray=new Array(1,2,3,4,5);

	var suffleArr=questionArray;

	var whatCount=1;
	var arrIndex=0;

	loadTimelineProgress(2,1);

	$("#exampDef").html(data.string["p3_0"]+" "+whatCount);



		$("#exampDef").fadeIn(10,function(){

			$(this).addClass('animated flash');
			$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(this).removeClass('animated flash');
				getTempFunc(whatCount);
			});//animate complete

		});


	$("#activity-page-next-btn-enabled").click(function(){
		loadTimelineProgress(2,2);
		arrIndex++;
		whatCount++;
		$("#exampleList").html('').fadeOut(10);

		$("#exampDef").html(data.string["p3_0"]+" "+whatCount).addClass('animated flash');
		$("#exampDef").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

			$(this).removeClass('animated flash');
				getTempFunc(whatCount);
		});//animate complete
		$(this).hide(0);


	});


	$("#activity-page-prev-btn-enabled").click(function(){
			loadTimelineProgress(2,1);
		arrIndex--;
		whatCount--;
		$("#exampleList").html('').fadeOut(10);

		$("#exampDef").html(data.string["p3_0"]+" "+whatCount).addClass('animated flash');
		$("#exampDef").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(this).removeClass('animated flash');
				getTempFunc(whatCount);
		});//animate complete



	});




});

function getDataVar($i)
{
	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	var $whatprevbtn=getSubpageMoveButton($lang,"prev");

	var $datavar;

	switch($i)
	{
		case 1:
			$datavar={
				questionTxt:data.string.p4_10,
				isnotlast:1,
				imgnext:$whatnextbtn
			}
			break;
		case 2:
			$datavar={
				questionTxt:data.string.p4_2,
				isnotfirst:1,
				imgprev:$whatprevbtn
			}
			break;
	}




	return $datavar;
}


function getTemplateHtml($i)
{

	var dataval=getDataVar($i)


	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(dataval);

	return html;
}

function parseQuestion($i)
{


	switch($i)
	{
		case 1:

			ole.parseToolTip("#questionTxt",data.string.p4_10_r_1);
			ole.parseToolTip("#questionTxt",data.string.p4_10_r_2);
			ole.parseToolTip("#questionTxt",data.string.p4_10_r_3);

			break;
		case 2:
			ole.parseToolTip("#questionTxt",data.string.p4_2_r_1);
			ole.parseToolTip("#questionTxt",data.string.p4_2_r_2);
			ole.parseToolTip("#questionTxt",data.string.p4_2_r_3);
			break;

	}

	$("#questionTxt").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
}



function getTempFunc($i)
{

	var htmlTemp=getTemplateHtml($i);

	$("#exampleList").html(htmlTemp).fadeIn(100,function(){


		parseQuestion($i);


		getAnimation($i);



	});


}


function getAnimation($i)
{
	switch($i)
		{
			case 1:
				animation_1($i);
				break;
			case 2:
				animation_2($i);
				break;

		}

}
function animation_1($i)
{

	var parser;


	parser=[
		{"datavalue":data.string.p4_11_r_1,"replaceValue":''},
		{"datavalue":data.string.p4_11_r_2,"replaceValue":''}
	];
	addDivtoAns(".ansDiv","p4_11",'hideThis','',data.string.p4_11,parser,"#F73802");

	$("#p4_11").delay(600).fadeIn(300,function(){

		parser=[
		{"datavalue":data.string.p4_14_r_1,"replaceValue":''},
		{"datavalue":data.string.p4_14_r_2,"replaceValue":'$${10/100}$$'}
		];
		addDivtoAns(".ansDiv","p4_14",'hideThis','',data.string.p4_14,parser,"#F73802");

		M.parseMath(document.body);

		$("#p4_14").delay(600).fadeIn(300,function(){

				parser=[
				{"datavalue":data.string.p4_12_r_1,"replaceValue":''},
				{"datavalue":data.string.p4_12_r_2,"replaceValue":'$${(10/100)X1000}$$'},
				{"datavalue":data.string.p4_12_r_3,"replaceValue":''}
				];
				addDivtoAns(".ansDiv","p4_12",'hideThis','',data.string.p4_12,parser,"#F73802");

				M.parseMath(document.body);

				$("#p4_12").delay(600).fadeIn(300,function(){

				});
				$(".ansDiv").append('<div id="p4_13" class="conCluTxt hideThis">'+data.string['p4_13']+'</div>');
				ole.parseToolTip("#p4_13",data.string.p4_13_r_1);
				$("#p4_13").children('span:nth-of-type(n+1)').css({"color":"#F73802"});

				$("#p4_13").delay(600).fadeIn(300);
				$("#activity-page-next-btn-enabled").delay(600).fadeIn(300);

		});



	});


}

function animation_2($i)
{
	var parser;


	parser=[
		{"datavalue":data.string.p4_3_r_1,"replaceValue":''},
		{"datavalue":data.string.p4_3_r_2,"replaceValue":''},
		{"datavalue":data.string.p4_3_r_3,"replaceValue":''},
		{"datavalue":data.string.p4_3_r_4,"replaceValue":''}
	];
	addDivtoAns(".ansDiv","p4_3",'hideThis','',data.string.p4_3,parser,"#F73802");

	$("#p4_3").delay(600).fadeIn(300,function(){

		parser=[
			{"datavalue":data.string.p4_4_r_1,"replaceValue":''},
			{"datavalue":data.string.p4_4_r_2,"replaceValue":'$${1500/5}$$'},
			{"datavalue":data.string.p4_4_r_3,"replaceValue":''},
			{"datavalue":data.string.p4_4_r_4,"replaceValue":''}
		];
		addDivtoAns(".ansDiv","p4_4",'hideThis','',data.string.p4_4,parser,"#F73802");

		M.parseMath(document.body);

		$("#p4_4").delay(600).fadeIn(300,function(){

			parser=[
			{"datavalue":data.string.p4_5_r_1,"replaceValue":''},
			{"datavalue":data.string.p4_5_r_3,"replaceValue":'$${1/20}$$'},
			{"datavalue":data.string.p4_5_r_4,"replaceValue":'$${300/6000}$$'}
			];
			addDivtoAns(".ansDiv","p4_5",'hideThis','',data.string.p4_5,parser,"#F73802");

			M.parseMath(document.body);

			$("#p4_5").delay(600).fadeIn(300,function(){

				parser=[
				{"datavalue":data.string.p4_6_r_1,"replaceValue":''},
				{"datavalue":data.string.p4_6_r_2,"replaceValue":''},
				{"datavalue":data.string.p4_6_r_3,"replaceValue":'$${(1/20)X100}$$'},
				{"datavalue":data.string.p4_6_r_4,"replaceValue":''}
				];
				addDivtoAns(".ansDiv","p4_6",'hideThis','',data.string.p4_6,parser,"#F73802");

				M.parseMath(document.body);

				$("#p4_6").delay(600).fadeIn(300,function(){


					$(".ansDiv").append('<div id="p4_9" class="conCluTxt hideThis intentDing3">'+data.string['p4_9']+'</div>');
					$("#p4_9").delay(600).fadeIn(300);

					$("#activity-page-prev-btn-enabled").delay(600).fadeIn(300,function(){
							ole.footerNotificationHandler.pageEndSetNotification();
					});



				});//fadein

			});//fadein

		});//fadein


	});	//fadein

}



function addnextBtn($where, $what, $class)
{

	var $what="<div id='"+$what+"' class='"+$class+"'><span class='glyphicon glyphicon-arrow-right'></span></div>";
	$($where).append($what);
}

$(function(){


	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var $myid;
	var counterMe=1;

	var clickCount=1;
	var leftVal=18; //initial value of left of blinking circle


	loadTimelineProgress(3,counterMe);

	$("#animateBox").addClass('cursorWait').fadeOut(0);
	$("#head_title").html(data.string.p1_3_a);
	var $kk;
	for($kk=4;$kk<=8;$kk++)
	{
		$("#p1_"+$kk).html(data.string["p1_"+$kk]);
	}

	for($kk=20;$kk<=26;$kk++)
	{
		$("#p1_"+$kk).html(data.string["p1_"+$kk]);
	}






	$('#head_title').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

		pageGen(counterMe);

	});


	$("#animateBox").on('click','.clickthis',function(){
		clickCount++;

		var compare;


		leftVal+=13;

		compare=8;

		if(clickCount==2)
		{
			leftVal=47.5;
		}
		else if(clickCount==3)
		{
			leftVal=68.5;
		}
		else if(clickCount==4)
		{
			leftVal=89.5;
		}

		$(".graphImg").find('img').attr('src',$ref+'/images/graph2/'+clickCount+'.png');
		$("#graphconclusion").html(data.string["p1_44_"+clickCount]);
		compare=5;

		if(clickCount<compare)
		{
			$(this).animate({'left':leftVal+'%'},1000);
		}
		else
		{
			$(this).fadeOut(30);

			$("#showall").fadeIn(10);
		}
	});

	$("#animateBox").on('click','#showall',function(){
		$(this).hide(0);

			$(".graphImg").find('img').attr('src',$ref+'/images/graph2/6.png');

			$("#graphconclusion").html(data.string["p1_39_10"]);
			$("#graphTitle").delay(100).fadeIn(200);
			$("#showlastCon").delay(100).fadeIn(200);


			clickCount=1;
			leftVal=18;

			$("#activity-page-prev-btn-enabled").delay(200).fadeIn(200);
				ole.footerNotificationHandler.pageEndSetNotification();
	});



	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(100);
		$("#activity-page-prev-btn-enabled").fadeOut(100);
		counterMe++;
		loadTimelineProgress(3,counterMe);
		pageGen(counterMe);

	});

	$("#activity-page-prev-btn-enabled").click(function(){
		$(this).fadeOut(100);
		$("#activity-page-next-btn-enabled").fadeOut(100);
		counterMe--;
		loadTimelineProgress(3,counterMe);
		pageGen(counterMe);

	});


});

function getTemplateHtml(counter)
{

	var dataval=dataGen(counter);

	var source=$("#template-"+counter).html();
	var template=Handlebars.compile(source);
	var html=template(dataval);

	return html;
}

function dataGen(counter)
{
	var datavar;
	switch(counter)
	{
		case 1:
			datavar={
				image1:$ref+"/images/page1/speed.png",
				image2:$ref+"/images/heat2.png",
				image3:$ref+"/images/page1/work.png",
				p1_4:data.string.p1_20,
				p1_5:data.string.p1_21,
				p1_6:data.string.p1_22,
				p1_7:data.string.p1_23,
				p1_8:data.string.p1_24

			}
			break;
		case 2:
			datavar={

			definition:data.string.p1_26,

			weight:data.string.p1_27,
			cost:data.string.p1_28,
			questionTxt:data.string.p1_29,
			ansObj:[data.string.p1_30,data.string.p1_31,data.string.p1_32],
			conTxt:data.string.p1_33,
			labelwhat:data.string.p1_42,
			conCluCar:data.string.p1_44_6
		}

			break;
		case 3:
			datavar={
				innertitle:data.string.p1_25,
				weight:data.string.p1_42,
				cost:data.string.p1_43,
				tabTitle:data.string.p1_40,
				graphtittle:data.string.p1_41,
				imggr:$ref+"/images/graph2/1.png",
				ycor:data.string.p1_43,
				xcor:data.string.p1_42,
				showall:data.string.p1_38
			}
		break;
	}

	return datavar;
}

function pageGen(counter)
{
	var html=getTemplateHtml(counter);
	switch(counter)
	{
		case 1:
			animate_1(html);
			break;
		case 2:
			animate_2(html);
			break;
		case 3:
			animate_3(html);
			break;
	}
}

function animate_1(html)
{

	$("#animateBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(100,function(){

		$(".defThis").delay(100).fadeIn(500,function(){

				$(".examplehead").fadeIn(500,function(){

					$(".exampleObj").fadeIn(500,function(){

						$("#activity-page-next-btn-enabled").fadeIn(100);

					});

				});//examplehead fadein

			});//defthis
	});



}

function animate_2(html)
{

	$("#animateBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(100,function(){
		$("#show_2").fadeIn(100,function(){

			$(".def-me").delay(200).fadeIn(500,function()
			{

				$(".carAnimation").delay(200).fadeIn(500,function(){
					inDirectClick();
				});

			});//def show

		});

	});
}
function inDirectClick()
{

	$("#timerCar").html(data.string.p1_28+":");
	var whatBlink=1
	var whatTime=3000;
	$("#switch_car").delay(500).fadeIn(500,function(){

		$("#what_1").addClass('blinkLabelCar');
	});

	$("#show_2").on('click',".blinkLabelCar",function(){

		$('#whatTxtCar').fadeOut(0);

		$("#timerCar").html(data.string.p1_28+":");
		$("#carimg").removeAttr('style');
		$("#what_"+whatBlink).removeClass('blinkLabelCar');

		var whatTimestamp=new Date().getTime();

		$("#carimg").find('img').attr('src',$ref+'/images/car-moving.gif?'+whatTimestamp);
		$("#carimg").animate({'left':'80%'},whatTime,function(){
			$(this).find('img').attr('src',$ref+'/images/car.png');
			var timertime;
			switch(whatBlink)
			{
				case 1: timertime=60; $('#whatTxtCar').html(data.string.p1_44_2);  whatTime=1500; break;

				case 2: timertime=30; $('#whatTxtCar').html(data.string.p1_44_3); whatTime=1000;break;

				case 3: timertime=20; $('#whatTxtCar').html(data.string.p1_44_4); whatTime=750; break;
				case 4: timertime=15; $('#whatTxtCar').html(data.string.p1_44_5); break;
			}

			$("#timerCar").html(data.string.p1_28+":"+timertime+"s");

			$('#whatTxtCar').fadeIn(300,function(){

				whatBlink++;

				if(whatBlink==5)
				{


					$(".tabMe").delay(100).fadeIn(0).addClass('animated lightSpeedIn');


					$("#conCluCar").delay(1800).fadeIn(400,function()
						{
							$("#activity-page-next-btn-enabled").fadeIn(10);
							$("#activity-page-prev-btn-enabled").fadeIn(10);
						});
				}
				else
					$("#what_"+whatBlink).addClass('blinkLabelCar');

			});//fadein


		});

	});

}
function animate_3(html)
{


	$("#animateBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(100,function(){
		$(".TabTitle").delay(200).fadeIn(500,function(){

			$(".graph").fadeIn(10,function(){
					$(this).addClass('animated lightSpeedIn');

				$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$("#circleX").addClass('clickthis');
				});

			});//complete animate
		});//fadein
	});






}

function shuffleArray(array) {
	    for (var i = array.length - 1; i > 0; i--) {
	        var j = Math.floor(Math.random() * (i + 1));
	        var temp = array[i];
	        array[i] = array[j];
	        array[j] = temp;
	    }
	    return array;
}


function addDivtoAns($wherediv,$id,class1,class2,$dataval,$paersedata,$color)
{

	
	
	var $id2="#"+$id;
	

	$($wherediv).append("<div id='"+$id+"' class='"+class1+" "+class2+"' ></div>");
	
	$($id2).html($dataval);
	

	// $.each( $paersedata, function( key, value ) {
	
		
	// 	if(value['replaceValue']=='')
	// 		ole.parseToolTip($id2,value['datavalue']);
	// 	else
	// 		ole.parseToolTip($id2,value['datavalue'],'',false,value['replaceValue']);
	// });
	
	$($id2).children('span:nth-of-type(n+1)').css({"color":$color});

}


function parseQuestion($i)
{
	//change in color of numbers in queston
	switch($i)
	{
		case 1:

			ole.parseToolTip("#questionTxt",data.string.p01_2_r_1);
			ole.parseToolTip("#questionTxt",data.string.p01_2_r_2);
			ole.parseToolTip("#questionTxt",data.string.p01_2_r_3);
			break;
		case 2:
			ole.parseToolTip("#questionTxt",data.string.p01_12_r_1);
			ole.parseToolTip("#questionTxt",data.string.p01_12_r_2);
			ole.parseToolTip("#questionTxt",data.string.p01_12_r_3);
			break;
		
	}

	$("#questionTxt").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
}


function parseFirstDive($i)
{
	/* change in color in first div**/
	switch($i)
	{
		case 1:

			ole.parseToolTip("#firstDive",data.string.p01_2_r_1);
			ole.parseToolTip("#firstDive",data.string.p01_2_r_2);
			

			break;
		case 2:

			ole.parseToolTip("#firstDive",data.string.p01_13_r_1);
			ole.parseToolTip("#firstDive",data.string.p01_13_r_2);
			break;
		
	}

	$("#firstDive").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
}

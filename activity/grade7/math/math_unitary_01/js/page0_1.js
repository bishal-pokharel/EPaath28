$(function(){

	var $myid;

	var questionArray=new Array(1,2);

	var suffleArr=questionArray;

	var whatCount=1;
	var arrIndex=0;

	$("#head_title").html(data.string.p01_0);

	$("#exampDef").html(data.string.p01_01);


	loadTimelineProgress(2,1);

	//$('#head_title').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

		$("#exampDef").fadeIn(10,function(){

			$(this).addClass('animated flash');
			$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(this).removeClass('animated flash');
				getTempFunc(whatCount);
			});//animate complete

		});


	//});//animate complete



	$("#activity-page-next-btn-enabled").click(function(){
		$("#exampDef").html(data.string.p01_12_0);
		arrIndex++;
		whatCount++;
		loadTimelineProgress(2,2);
		$("#exampleList").html('').fadeOut(10);
		getTempFunc(whatCount);
		$(this).hide(0);
	});


	$("#activity-page-prev-btn-enabled").click(function(){

		arrIndex--;
		whatCount--;
		loadTimelineProgress(2,1);
		$("#exampleList").html('').fadeOut(10);

		$("#exampDef").html(data.string.p01_01);

				getTempFunc(whatCount);

	});




});

function getDataVar($i)
{
	var $whatnextbtn=getSubpageMoveButton($lang,"next");


	var $whatprevbtn=getSubpageMoveButton($lang,"prev");



	var $datavar;
	switch($i)
	{
		case 1:
			$datavar={
				questionTxt:data.string.p01_2,
				questiondesc:data.string.p01_1,
				imgobj:$ref+"/images/page3_1.png",
				firstLine:data.string.p01_3,
				isnotlast:1,
				imgnext:$whatnextbtn
			}
			break;
		case 2:
			$datavar={
				questionTxt:data.string.p01_12,
				questiondesc:data.string.p01_12_1,
				imgobj:$ref+"/images/page1/math.jpg",
				firstLine:data.string.p01_13,
				isnotfirst:1,
				imgprev:$whatprevbtn
			}
			break;

	}


	return $datavar;
}


function getTemplateHtml($i)
{



	var dataval=getDataVar($i);

	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(dataval);

	return html;
}

function parseQuestion($i)
{
	//change in color of numbers in queston
	switch($i)
	{
		case 1:

			ole.parseToolTip("#questionTxt",data.string.p01_2_r_1);
			ole.parseToolTip("#questionTxt",data.string.p01_2_r_2);
			ole.parseToolTip("#questionTxt",data.string.p01_2_r_3);

			break;
		case 2:
			ole.parseToolTip("#questionTxt",data.string.p01_12_r_1);
			ole.parseToolTip("#questionTxt",data.string.p01_12_r_2);
			ole.parseToolTip("#questionTxt",data.string.p01_12_r_3);
			break;

	}

	$("#questionTxt").children('span:nth-of-type(n+1)').css({"color":"#000"});
}


function parseFirstDive($i)
{
	/* change in color in first div**/
	switch($i)
	{
		case 1:

			ole.parseToolTip("#firstDive",data.string.p01_2_r_1);
			ole.parseToolTip("#firstDive",data.string.p01_2_r_2);


			break;
		case 2:

			ole.parseToolTip("#firstDive",data.string.p01_13_r_1);
			ole.parseToolTip("#firstDive",data.string.p01_13_r_2);
			break;

	}

	$("#firstDive").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
}

function getTempFunc($i)
{

	var htmlTemp=getTemplateHtml($i);

	$("#exampleList").html(htmlTemp).delay(500).fadeIn(100,function(){


		parseQuestion($i);

		parseFirstDive($i);

		$("#firstDive").delay(600).fadeIn(300,function(){



			getAnimation($i);
		});


	});


}


function getAnimation($i)
{
		switch($i)
		{
			case 1:
				animation_1($i);
				break;
			case 2:
				animation_2($i);
				break;

		}
}
function animation_1($i)
{

	var parser;


	parser=[
		{"datavalue":data.string.p01_4_r_1,"replaceValue":''},
		{"datavalue":data.string.p01_4_r_3,"replaceValue":'__'}
	];
	addDivtoAns(".ansDiv","p01_4",'hideThis','',data.string.p01_4,parser,"#000");

	$("#p01_4").delay(600).fadeIn(300,function(){



		parser=[
			{"datavalue":"4","replaceValue":''},
			{"datavalue":"12","replaceValue":''}
		];
		addDivtoAns(".oval-thought","extra",'hideThis','',data.string.p01_3,parser,"#F50CCA");

		$("#extra").delay(600).fadeIn(300,function(){
			$(".oval-thought").addClass('oval-class');

			parser=[
				{"datavalue":'4/4',"replaceValue":'$${4/4}$$'},
				{"datavalue":'12/4',"replaceValue":'$${12/4}$$'}
			];

			addDivtoAns(".oval-thought","p01_5",'hideThis','',data.string.p01_5,parser,"#F50CCA");

			M.parseMath(document.body);

			$("#p01_5").delay(600).fadeIn(300,function(){

				parser=[
					{"datavalue":'1 ',"replaceValue":''},
					{"datavalue":'12/4',"replaceValue":'$${12/4}$$'},
					{"datavalue":'3',"replaceValue":''}
					];

				addDivtoAns(".oval-thought","p01_6",'hideThis','',data.string.p01_6,parser,"#F50CCA");

				M.parseMath(document.body);


				$("#p01_6").delay(600).fadeIn(300,function(){
					addnextBtn(".oval-thought", 'nextBtnTo1','intendBlink');
				});//fadein
			});//fadein
		});//fadein

	});//fadein

	$("#exampleList").on('click','#nextBtnTo1',function(){

		$(this).remove();


		$(".oval-thought").fadeOut(0,function(){

			$(this).removeClass('oval-class');
			$("#p01_4").html(data.string.p01_7);
			ole.parseToolTip("#p01_4",'1');
			ole.parseToolTip("#p01_4",'3');
			$("#p01_4").children('span:nth-of-type(n+1)').css({"color":"#F50CCA"});
		}).html('').delay(100).fadeIn(10);





		parser=[
			{"datavalue":'9',"replaceValue":''},
			{"datavalue":'27',"replaceValue":'__'}
		];

		addDivtoAns(".ansDiv","p01_8",'hideThis','',data.string.p01_8,parser,"#F73802");

		$("#p01_8").delay(1000).fadeIn(300,function(){

			parser=[
				{"datavalue":'1',"replaceValue":''},
				{"datavalue":'3',"replaceValue":''}
			];

			addDivtoAns(".oval-thought","p01_9",'hideThis','',data.string.p01_9,parser,"#F50CCA");

			$("#p01_9").delay(600).fadeIn(300,function(){
				$(".oval-thought").addClass('oval-class');
				parser=[
					{"datavalue":'3X9',"replaceValue":'3 X 9'},
					{"datavalue":' 9 ',"replaceValue":''},
					{"datavalue":'27',"replaceValue":''}
				];
				addDivtoAns(".oval-thought","p01_10",'hideThis','',data.string.p01_10,parser,"#F50CCA");

				$("#p01_10").delay(600).fadeIn(300,function(){
						addnextBtn(".oval-thought", 'nextBtnTo2','intendBlink hideThis');
					$("#nextBtnTo2").delay(600).fadeIn(100);
				});//fadein
			});//fadein
		});//fadein




	});//click1

	$("#exampleList").on('click','#nextBtnTo2',function(){


		$(this).remove();

		$(".oval-thought").fadeOut(0,function(){
			$(this).removeClass('oval-class');
			$("#p01_8").html(data.string.p01_8);
			ole.parseToolTip("#p01_8",'9');
			ole.parseToolTip("#p01_8",'27');
			$("#p01_8").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
		}).html('').delay(100).fadeIn(10);



		parser=[
				{"datavalue":'',"replaceValue":''}
			];
			addDivtoAns(".ansDiv","p01_11_conc",'hideThis','conCluTxt',data.string.p01_11_conc,parser,"#F50CCA");


		$("#p01_11_conc").delay(1000).fadeIn(300,function(){

			$("#activity-page-next-btn-enabled").fadeIn(300);
		});//fadein


	});//click2


}

function animation_2($i)
{

	var parser;


	parser=[
		{"datavalue":data.string.p01_14_r_1,"replaceValue":'__'},
		{"datavalue":data.string.p01_14_r_2,"replaceValue":''}
	];
	addDivtoAns(".ansDiv","p01_14",'hideThis','',data.string.p01_14,parser,"#F73802");

	$("#p01_14").delay(600).fadeIn(300,function(){

		parser=[
			{"datavalue":data.string.p01_15_r_1,"replaceValue":''},
			{"datavalue":data.string.p01_15_r_2,"replaceValue":''}
		];
		addDivtoAns(".oval-thought","p01_15",'hideThis','intentDing',data.string.p01_15,parser,"#F50CCA");

		$("#p01_15").delay(600).fadeIn(300,function(){
			$(".oval-thought").addClass('oval-class');

			parser=[
				{"datavalue":data.string.p01_16_r_1,"replaceValue":''},
				{"datavalue":data.string.p01_16_r_2,"replaceValue":''},
				{"datavalue":data.string.p01_16_r_3,"replaceValue":''}
			];
			addDivtoAns(".oval-thought","p01_16",'hideThis','intentDing',data.string.p01_16,parser,"#F50CCA");

			$("#p01_16").delay(600).fadeIn(300,function(){

				parser=[
					{"datavalue":data.string.p01_17_r_2,"replaceValue":''},
					{"datavalue":data.string.p01_17_r_1,"replaceValue":''},
					{"datavalue":data.string.p01_17_r_3,"replaceValue":''}
				];
				addDivtoAns(".oval-thought","p01_17",'hideThis','intentDing',data.string.p01_17,parser,"#F50CCA");

				$("#p01_17").delay(600).fadeIn(300,function()
				{
					addnextBtn(".oval-thought", 'nextBtnTo3','intendBlink');


				});//fadein


			});//fadein

		});//fadein


	});//fadein

	$("#exampleList").on('click','#nextBtnTo3',function(){
		$(this).fadeOut(0);
		$(".oval-thought").fadeOut(0,function(){

			$(this).removeClass('oval-class');

			$("#p01_14").html(data.string.p01_14);
			ole.parseToolTip("#p01_14",data.string.p01_14_r_1);
			ole.parseToolTip("#p01_14",data.string.p01_14_r_2);
			$("#p01_14").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
		}).html('').delay(100).fadeIn(10);//fadein

		$(".ansDiv").append('<div id="p01_18" class="conCluTxt hideThis">'+data.string['p01_18']+'</div>');
		$("#p01_18").delay(1000).fadeIn(300,function(){

			$("#activity-page-prev-btn-enabled").fadeIn(300);

			ole.footerNotificationHandler.pageEndSetNotification();
		});


	});

}

function addnextBtn($where, $what, $class)
{

	var imgarrow=getArrowBtn();
	var $what="<div id='"+$what+"' class='"+$class+"'>"+imgarrow+"</div>";
	$($where).append($what);
}

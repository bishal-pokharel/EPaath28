$(function(){

	var $myid;



	$("#head_title").html(data.string.p3_1);


	var $myid;

	var questionArray=new Array(1,2,3,4,5);

	var suffleArr=questionArray;

	var whatCount=1;
	var arrIndex=0;



	loadTimelineProgress(2,1);

	$("#head_title").html(data.string.p3_1);

	$("#exampDef").html(data.string["p3_0"]+" "+whatCount);




		$("#exampDef").fadeIn(10,function(){

			$(this).addClass('animated flash');
			$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(this).removeClass('animated flash');
				getTempFunc(whatCount);
			});//animate complete

		});



	$("#activity-page-next-btn-enabled").click(function(){
		loadTimelineProgress(2,2);
		arrIndex++;
		whatCount++;
		$("#exampleList").html('').fadeOut(10);

		$("#exampDef").html(data.string["p3_0"]+" "+whatCount).addClass('animated flash');
		$("#exampDef").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

			$(this).removeClass('animated flash');
				getTempFunc(whatCount);
		});//animate complete
		$(this).hide(0);



	});


	$("#activity-page-prev-btn-enabled").click(function(){
		loadTimelineProgress(2,1);
		arrIndex--;
		whatCount--;
		$("#exampleList").html('').fadeOut(10);

		$("#exampDef").html(data.string["p3_0"]+" "+whatCount).addClass('animated flash');
		$("#exampDef").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

			$(this).removeClass('animated flash');
				getTempFunc(whatCount);
		});//animate complete



	});




});

function getDataVar($i)
{
	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	var $whatprevbtn=getSubpageMoveButton($lang,"prev");

	var $datavar;

	switch($i)
	{
		case 1:
			$datavar={
				questionTxt:data.string.p3_2,
				isnotlast:1,
				imgnext:$whatnextbtn
			}
			break;
		case 2:
			$datavar={
				questionTxt:data.string.p3_13,
				isnotfirst:1,
				imgprev:$whatprevbtn
			}
			break;
	}




	return $datavar;
}


function getTemplateHtml($i)
{

	var dataval=getDataVar($i)


	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(dataval);

	return html;
}

function parseQuestion($i)
{


	switch($i)
	{
		case 1:

			ole.parseToolTip("#questionTxt",data.string.p3_2_r_1);
			ole.parseToolTip("#questionTxt",data.string.p3_2_r_2);


			break;
		case 2:
			ole.parseToolTip("#questionTxt",data.string.p3_13_r_1);
			ole.parseToolTip("#questionTxt",data.string.p3_13_r_2);

			break;

	}

	$("#questionTxt").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
}



function getTempFunc($i)
{

	var htmlTemp=getTemplateHtml($i);

	$("#exampleList").html(htmlTemp).fadeIn(100,function(){


		parseQuestion($i);


		getAnimation($i);



	});


}


function getAnimation($i)
{
	switch($i)
		{
			case 1:
				animation_1($i);
				break;
			case 2:
				animation_2($i);
				break;

		}

}
function animation_1($i)
{
	var parser;


	parser=[
		{"datavalue":data.string.p3_3_r_1,"replaceValue":''},
		{"datavalue":data.string.p3_3_r_2,"replaceValue":''}
	];
	addDivtoAns(".ansDiv","p3_3",'hideThis','',data.string.p3_3,parser,"#F73802");

	$("#p3_3").delay(600).fadeIn(300,function(){

		parser=[
		{"datavalue":data.string.p3_4_r_1,"replaceValue":''},
		{"datavalue":data.string.p3_4_r_2,"replaceValue":''},
		{"datavalue":data.string.p3_4_r_3,"replaceValue":''}
		];
		addDivtoAns(".ansDiv","p3_4",'hideThis','',data.string.p3_4,parser,"#F73802");

		$("#p3_4").delay(600).fadeIn(300,function(){

			parser=[
			{"datavalue":data.string.p3_10_r_1,"replaceValue":''},
			{"datavalue":data.string.p3_10_r_2,"replaceValue":''},
			{"datavalue":data.string.p3_10_r_3,"replaceValue":''}
			];
			addDivtoAns(".ansDiv","p3_10",'hideThis','',data.string.p3_10,parser,"#F73802");

			$("#p3_10").delay(600).fadeIn(300,function(){

				$(".ansDiv").append('<div id="p3_12" class="conCluTxt hideThis">'+data.string['p3_12']+'</div>');
				ole.parseToolTip("#p3_12",data.string.p3_12_r_1);
				ole.parseToolTip("#p3_12",data.string.p3_12_r_2);

				$("#p3_12").children('span:nth-of-type(n+1)').css({"color":"#F73802"});

				$("#p3_12").delay(700).fadeIn(300);

				$("#activity-page-next-btn-enabled").delay(800).fadeIn(300);
			});


		});//fadein
	});	//fadein




	$("#exampleList").on('click','#nextBtnToL',function(){
		$(this).fadeOut(0);

		$(".intentDing").fadeOut(300,function(){
			$("#p3_8").html(data.string.p3_8);
			ole.parseToolTip("#p3_8",data.string.p3_8_r_1);
			ole.parseToolTip("#p3_8",data.string.p3_8_r_2);
			$("#p3_8").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
		});

		$(".ansDiv").append('<div id="p3_12" class="conCluTxt hideThis">'+data.string['p3_12']+'</div>');
		ole.parseToolTip("#p3_12",data.string.p3_12_r_1);
		ole.parseToolTip("#p3_12",data.string.p3_12_r_2);

		$("#p3_12").children('span:nth-of-type(n+1)').css({"color":"#F73802"});

		$("#p3_12").delay(700).fadeIn(300);

		$("#activity-page-next-btn-enabled").delay(800).fadeIn(300);
	});

}

function animation_2($i)
{
	var parser;


	parser=[
		{"datavalue":data.string.p3_14_r_1,"replaceValue":''},
		{"datavalue":data.string.p3_14_r_2,"replaceValue":''}
	];
	addDivtoAns(".ansDiv","p3_14",'hideThis','',data.string.p3_14,parser,"#F73802");

	$("#p3_14").delay(600).fadeIn(300,function(){

		parser=[
		{"datavalue":data.string.p3_15_r_1,"replaceValue":''}
		];
		addDivtoAns(".ansDiv","p3_15",'hideThis','',data.string.p3_15,parser,"#F73802");

		$("#p3_15").delay(600).fadeIn(300,function(){

			parser=[
			{"datavalue":data.string.p3_16_r_1,"replaceValue":''},
			{"datavalue":data.string.p3_16_r_2,"replaceValue":''}
			];
			addDivtoAns(".ansDiv","p3_16",'hideThis','',data.string.p3_16,parser,"#F73802");

			$("#p3_16").delay(600).fadeIn(300,function(){

				parser=[
				{"datavalue":data.string.p3_17_r_1,"replaceValue":''},
				{"datavalue":data.string.p3_17_r_2,"replaceValue":'__'}
				];
				addDivtoAns(".ansDiv","p3_17",'hideThis','',data.string.p3_17,parser,"#F73802");

				$("#p3_17").delay(600).fadeIn(300,function(){

					parser=[
					{"datavalue":data.string.p3_18_r_1,"replaceValue":''},
					{"datavalue":data.string.p3_18_r_2,"replaceValue":''}
					];
					addDivtoAns(".oval-thought","p3_18",'hideThis','',data.string.p3_18,parser,"#F50CCA");

					$("#p3_18").delay(600).fadeIn(300,function(){
						$(".oval-thought").addClass('oval-class');
						parser=[
						{"datavalue":data.string.p3_19_r_1,"replaceValue":''},
						{"datavalue":data.string.p3_19_r_2,"replaceValue":''},
						{"datavalue":data.string.p3_19_r_3,"replaceValue":''}
						];
						addDivtoAns(".oval-thought","p3_19",'hideThis','',data.string.p3_19,parser,"#F50CCA");

						$("#p3_19").delay(600).fadeIn(300,function(){

							parser=[
							{"datavalue":data.string.p3_20_r_1,"replaceValue":''},
							{"datavalue":data.string.p3_20_r_2,"replaceValue":''},
							{"datavalue":data.string.p3_20_r_3,"replaceValue":''}
							];
							addDivtoAns(".oval-thought","p3_20",'hideThis','',data.string.p3_20,parser,"#F50CCA");

							$("#p3_20").delay(600).fadeIn(300,function(){
								addnextBtn(".oval-thought", 'nextBtnToO','intendBlink');

							});//fadein


						});//fadein


					});//fadein

				});//fadein


			});//fadein


		});//fadein


	});//fadein

	$("#exampleList").on('click','#nextBtnToO',function(){

		$(this).fadeOut(0);
		$(".oval-thought").fadeOut(10,function(){

			$(this).removeClass('oval-class');

			$("#p3_17").html(data.string.p3_17);
			ole.parseToolTip("#p3_17",data.string.p3_17_r_1);
			ole.parseToolTip("#p3_17",data.string.p3_17_r_2);
			$("#p3_17").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
		}).html('').delay(100).fadeIn(10);

		$(".ansDiv").append('<div id="p3_21" class="conCluTxt hideThis">'+data.string['p3_21']+'</div>');
		ole.parseToolTip("#p3_21",data.string.p3_21_r_1);
		ole.parseToolTip("#p3_21",data.string.p3_21_r_2);

		$("#p3_21").delay(600).fadeIn(300,function(){
			$("#activity-page-prev-btn-enabled").fadeIn(100);
				ole.footerNotificationHandler.pageEndSetNotification();

		});//fadein

	});

}



function addnextBtn($where, $what, $class)
{
	var imgarrow=getArrowBtn();
	var $what="<div id='"+$what+"' class='"+$class+"'>"+imgarrow+"</div>";
	$($where).append($what);
}

$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var $whatprevbtn=getSubpageMoveButton($lang,"prev");
	$("#activity-page-prev-btn-enabled").html($whatprevbtn);

	var $myid;
	var counterMe=1;
	var clickCount=1;
	var leftVal=18; //initial value of left of blinking circle
	loadTimelineProgress(3,counterMe);
	$("#animateBox").addClass('cursorWait').fadeOut(0);
	$("#head_title").html(data.string.p1_2_a);

	var $kk;
	for($kk=4;$kk<=8;$kk++)
	{
		$("#p1_"+$kk).html(data.string["p1_"+$kk]);
	}

	for($kk=20;$kk<=26;$kk++)
	{
		$("#p1_"+$kk).html(data.string["p1_"+$kk]);
	}

	$("#p1_2").html(data.string.p1_2_a);

	$("#p1_1_1").html(data.string.p1_1_1);
	$("#p1_1_2").html(data.string.p1_1_2);




	$('#head_title').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

		pageGen(counterMe);

	});




	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(100);
		$("#activity-page-prev-btn-enabled").fadeOut(100);
		counterMe++;
		loadTimelineProgress(3,counterMe);
		pageGen(counterMe);

	});

	$("#activity-page-prev-btn-enabled").click(function(){
		$(this).fadeOut(100);
		$("#activity-page-next-btn-enabled").fadeOut(100);
		counterMe--;
		loadTimelineProgress(3,counterMe);
		pageGen(counterMe);

	});

	$("#animateBox").on('click','.clickthis',function(){

		clickCount++;

		var compare;


		if(clickCount==2)
		{
			leftVal=31;
		}
		else if(clickCount==3)
		{
			leftVal=44;
		}
		else if(clickCount==4)
		{
			leftVal=55.5;
		}
		if(clickCount==5)
		{
			leftVal=68;
		}
		else if(clickCount==6)
		{
			leftVal=81;
		}
		else if(clickCount==7)
		{
			leftVal=93;
		}



			compare=8;
		$(".graphImg").find('img').attr('src',$ref+'/images/graph1/'+clickCount+'.png');
		$("#graphconclusion").html(data.string["p1_39_"+clickCount]);



		if(clickCount<compare)
		{
			$(this).animate({'left':leftVal+'%'},1000);
		}
		else
		{
			$(this).fadeOut(30);

			$("#showall").fadeIn(10);
		}

	});


	$("#animateBox").on('click','#showall',function(){
		$(this).hide(0);

		$(".graphImg").find('img').attr('src',$ref+'/images/graph1/9.png');

		$("#graphconclusion").html(data.string["p1_39_9"]);
		$("#graphTitle").delay(100).fadeIn(200);
		$("#showlastCon").delay(100).fadeIn(200);
		clickCount=1;
		leftVal=18;

		$("#activity-page-prev-btn-enabled").delay(200).fadeIn(200);
			ole.footerNotificationHandler.pageEndSetNotification();
	});


});

function getTemplateHtml(counter)
{

	var dataval=dataGen(counter);

	var source=$("#template-"+counter).html();
	var template=Handlebars.compile(source);
	var html=template(dataval);

	return html;
}

function dataGen(counter)
{
	var datavar;
	switch(counter)
	{
		case 1:
			datavar={
				image1:$ref+"/images/page1/heat.gif",
				image2:$ref+"/images/page1/pressure.png",
				image3:$ref+"/images/page1/taxi.png",
				p1_4:data.string.p1_4,
				p1_5:data.string.p1_5,
				p1_6:data.string.p1_6,
				p1_7:data.string.p1_7,
				p1_8:data.string.p1_8

			}
			break;
		case 2:
			datavar={
				definition:data.string.p1_10,
				direct:1,
				weight:data.string.p1_11,
				cost:data.string.p1_12,
				p1_10_1:data.string.p1_10_1,
				p1_10_2:data.string.p1_10_2,
				conTxt:data.string.p1_17
			}
			break;
		case 3:
			datavar={
				innertitle:data.string.p1_9,
				direct:1,
				weight:data.string.p1_11,
				cost:data.string.p1_12,
				tabTitle:data.string.p1_34,
				graphtittle:data.string.p1_35,
				imggr:$ref+"/images/graph1/1.png",
				ycor:data.string.p1_37,
				xcor:data.string.p1_36,
				showall:data.string.p1_38,
				showlastCon:data.string.p1_35_1
			}
		break;
	}

	return datavar;
}

function pageGen(counter)
{

	var html=getTemplateHtml(counter);
	switch(counter)
	{
		case 1:
			animate_1(html);
			break;
		case 2:
			animate_2(html);
			break;
		case 3:
			animate_3(html);
			break;
	}
}

function animate_1(html)
{

	$("#animateBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(100,function(){

		$(".defThis").delay(100).fadeIn(500,function(){

				$(".examplehead").fadeIn(500,function(){

					$(".exampleObj").fadeIn(500,function(){

						$("#activity-page-next-btn-enabled").fadeIn(100);

					});

				});//examplehead fadein

			});//defthis
	});



}

function animate_2(html)
{

	$("#animateBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(100,function(){
		$("#show_2").fadeIn(100,function(){

			$(".def-me").delay(200).fadeIn(500,function()
			{

				$("#p1_10_1").delay(200).fadeIn(500,function()
				{
					$("#p1_10_2").delay(200).fadeIn(500,function(){
						$(".tabMe").delay(200).fadeIn(500,function(){
							directClick();
						});

					});
				});

			});//def show

		});

	});
}
function directClick()
{



	$("#show_2").on('keyup',"#inFirst",function(){

		var pattern=/^\d*$/;

        var val=$("#inFirst").val();


        if(!val.match(pattern))
       	{
	       	$("#inFirst").val('').addClass('borderMe');
        }
	});

	$("#show_2").on('change',"#inFirst",function(){

		var thisVal=parseInt($(this).val());
		var checkVal;


			checkVal=300;



		if(thisVal==checkVal)
		{

			/*alert(thisVal+" is right answer.");*/
			$(".inputTd").html(checkVal);

		}
		else
		{

			/*alert(thisVal+" is wrong answer. Right answer is "+checkVal);*/
			$(".inputTd").html(checkVal);
		}


		$(".conTxt").fadeIn(500,function(){
			$("#activity-page-next-btn-enabled").fadeIn(10);
			$("#activity-page-prev-btn-enabled").fadeIn(10);

		});
	});



}
function animate_3(html)
{


	$("#animateBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(100).fadeIn(100,function(){
		$(".TabTitle").delay(200).fadeIn(500,function(){

			$(".graph").fadeIn(10,function(){
					$(this).addClass('animated lightSpeedIn');

				$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$("#circleX").addClass('clickthis');
				});

			});//complete animate
		});//fadein
	});




}

$(function(){

	var $myid;

	var questionArray=new Array(1,2);

	var suffleArr=questionArray;

	var whatCount=1;
	var arrIndex=0;

	loadTimelineProgress(2,1);

	$("#head_title").addClass('boxshd').html(data.string.p02_0);
	$("#exampDef").html(data.string.p02_1);




	//$('#head_title').addClass('animated lightSpeedIn');


	$("#exampDef").fadeIn(10,function(){


		setTimeout(function(){

			getTempFunc(whatCount);
		},500);

	});






	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		loadTimelineProgress(2,2);
		$("#exampDef").html(data.string.p02_11);
		arrIndex++;
		whatCount++;
		$("#exampleList").html('').fadeOut(10);

		getTempFunc(whatCount);
	});


	$("#activity-page-prev-btn-enabled").click(function(){
		loadTimelineProgress(2,1);
		$("#exampDef").html(data.string.p02_1);
		arrIndex--;
		whatCount--;
		$("#exampleList").html('').fadeOut(10);


				getTempFunc(whatCount);

	});




});

function getDataVar($i)
{
	var $whatnextbtn=getSubpageMoveButton($lang,"next");


	var $whatprevbtn=getSubpageMoveButton($lang,"prev");

	var $datavar;
	switch($i)
	{
		case 1:
			$datavar={
				questionTxt:data.string.p02_2,
				firstLine:data.string.p02_3,
				isnotlast:1,
				imgnext:$whatnextbtn
			}
			break;
		case 2:
			$datavar={
				questionTxt:data.string.p02_12,
				imgobj:$ref+"/images/page1/math.jpg",
				firstLine:data.string.p02_13,
				isnotfirst:1,
				imgprev:$whatprevbtn
			}
			break;

	}


	return $datavar;
}


function getTemplateHtml($i)
{

	var dataval=getDataVar($i);

	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(dataval);

	return html;
}

function parseQuestion($i)
{
	//change in color of numbers in queston
	switch($i)
	{
		case 1:

			ole.parseToolTip("#questionTxt",data.string.p02_2_r_1);
			ole.parseToolTip("#questionTxt",data.string.p02_2_r_3);
			ole.parseToolTip("#questionTxt",data.string.p02_2_r_2);


			break;
		case 2:
			ole.parseToolTip("#questionTxt",data.string.p02_12_r_1);
			ole.parseToolTip("#questionTxt",data.string.p02_12_r_2);
			ole.parseToolTip("#questionTxt",data.string.p02_12_r_3);
			break;

	}

	$("#questionTxt").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
}


function parseFirstDive($i)
{
	/* change in color in first div**/
	switch($i)
	{
		case 1:

			ole.parseToolTip("#firstDive",data.string.p02_3_r_1);
			ole.parseToolTip("#firstDive",data.string.p02_3_r_2);


			break;
		case 2:

			ole.parseToolTip("#firstDive",data.string.p02_13_r_1);
			ole.parseToolTip("#firstDive",data.string.p02_13_r_2);
			break;

	}

	$("#firstDive").children('span:nth-of-type(n+1)').css({"color":"#F73802"});
}

function getTempFunc($i)
{

	var htmlTemp=getTemplateHtml($i);

	$("#exampleList").html(htmlTemp).delay(500).fadeIn(100,function(){


		parseQuestion($i);

		parseFirstDive($i);

		$("#firstDive").delay(600).fadeIn(300,function(){



			getAnimation($i);
		});


	});


}


function getAnimation($i)
{
		switch($i)
		{
			case 1:
				animation_1($i);
				break;
			case 2:
				animation_2($i);
				break;

		}
}
function animation_1($i)
{

	var parser;


	parser=[
		{"datavalue":data.string.p02_4_r_1,"replaceValue":''},
		{"datavalue":data.string.p02_4_r_2,"replaceValue":''},
		{"datavalue":data.string.p02_4_r_3,"replaceValue":''}
	];
	addDivtoAns(".ansDiv","p02_4",'hideThis','',data.string.p02_4,parser,"#F73802");

	$("#p02_4").delay(600).fadeIn(300,function(){


		parser=[
		{"datavalue":data.string.p02_5_r_1,"replaceValue":''},
		{"datavalue":data.string.p02_5_r_2_2,"replaceValue":''}
		];
		addDivtoAns(".ansDiv","p02_5",'hideThis','',data.string.p02_5,parser,"#F73802");
		$("#p02_5").delay(600).fadeIn(300,function(){



			parser=[
			{"datavalue":data.string.p02_6_r_1,"replaceValue":''},
			{"datavalue":data.string.p02_6_r_2,"replaceValue":''}
			];

			addDivtoAns(".oval-thought","p02_6",'hideThis','',data.string.p02_6,parser,"#F50CCA");
			$("#p02_6").delay(600).fadeIn(300,function(){
				$(".oval-thought").addClass('oval-class');

				parser=[{"datavalue":data.string.p02_7_r_2,"replaceValue":'$${216/4}$$'},
					{"datavalue":data.string.p02_7_r_3,"replaceValue":''},
				{"datavalue":data.string.p02_7_r_1,"replaceValue":''}
				];

				addDivtoAns(".oval-thought","p02_7",'hideThis','',data.string.p02_7,parser,"#F50CCA");
				M.parseMath(document.body);

				$("#p02_7").delay(600).fadeIn(300,function(){
					addnextBtn(".oval-thought", 'nextBtnTo1a','intendBlink');
				});


			});

		});

	});//fadein

	$("#exampleList").on('click','#nextBtnTo1a',function(){

		$(this).fadeOut(10);

		$(".oval-thought").fadeOut(0,function(){

			$(this).removeClass('oval-class');

			$("#p02_5").html(data.string.p02_10);
			ole.parseToolTip("#p02_5",data.string.p02_10_r_1);
			ole.parseToolTip("#p02_5",data.string.p02_10_r_3);
			//ole.parseToolTip("#p02_5",data.string.p02_10_r_1);


			$("#p02_5").children('span:nth-of-type(n+1)').css({"color":"#F73802"});


		}).html('').fadeIn(10);//fadeout


		$("#p02_9").html(data.string.p02_9);

		$("#p02_9").delay(600).fadeIn(300,function(){ });

		$("#activity-page-next-btn-enabled").delay(600).fadeIn(300);

	});


}

function animation_2($i)
{
	var parser;


	parser=[
		{"datavalue":data.string.p02_14_r_1,"replaceValue":''},
		{"datavalue":data.string.p02_14_r_2,"replaceValue":''}
	];
	addDivtoAns(".ansDiv","p02_14",'hideThis','',data.string.p02_14,parser,"#F73802");

	$("#p02_14").delay(600).fadeIn(300,function(){

		parser=[{"datavalue":data.string.p02_15_r_1,"replaceValue":'__'},

		{"datavalue":data.string.p02_15_r_2,"replaceValue":''}];

		addDivtoAns(".ansDiv","p02_15",'hideThis','',data.string.p02_15,parser,"#F73802");

		$("#p02_15").delay(600).fadeIn(300,function(){

			parser=[{"datavalue":data.string.p02_16_r_1,"replaceValue":''},
			{"datavalue":data.string.p02_16_r_2,"replaceValue":''}];

			addDivtoAns(".oval-thought","p02_16",'hideThis','',data.string.p02_16,parser,"#F50CCA");

			$("#p02_16").delay(600).fadeIn(300,function(){
					$(".oval-thought").addClass('oval-class');
				parser=[{"datavalue":data.string.p02_17_r_1,"replaceValue":''},
				{"datavalue":data.string.p02_17_r_2,"replaceValue":'$${(40 X 72)/144}$$'},
				{"datavalue":data.string.p02_17_r_3,"replaceValue":''}

				];

				addDivtoAns(".oval-thought","p02_17",'hideThis','',data.string.p02_17,parser,"#F50CCA");

				M.parseMath(document.body);

				$("#p02_17").delay(600).fadeIn(300,function(){

					addnextBtn(".oval-thought", 'nextBtnTo2a','intendBlink');
				});
			});

		});

	});

	$("#exampleList").on('click','#nextBtnTo2a',function(){


		$(this).fadeOut(10);

		$(".oval-thought").fadeOut(10,function(){

			$(this).removeClass('oval-class');
			$("#p02_15").html(data.string.p02_18);
			ole.parseToolTip("#p02_15",data.string.p02_18_r_1);
			ole.parseToolTip("#p02_15",data.string.p02_18_r_2);

			$("#p02_15").children('span:nth-of-type(n+1)').css({"color":"#F73802"});


		}).html('').delay(100).fadeIn(10);//fadeout


		$("#p02_9").html(data.string.p02_19);

		$("#p02_9").delay(600).fadeIn(300,function(){ });


		$("#activity-page-prev-btn-enabled").delay(600).fadeIn(300,function(){

				ole.footerNotificationHandler.pageEndSetNotification();
		 });

	});


}

function addnextBtn($where, $what, $class)
{
	var imgarrow=getArrowBtn();
	var $what="<div id='"+$what+"' class='"+$class+"'>"+imgarrow+"</div>";
	$($where).append($what);
}

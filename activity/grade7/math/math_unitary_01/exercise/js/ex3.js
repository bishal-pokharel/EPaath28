$(function(){

	var $myid;

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var whatpage=1;

	var innerpage=1;

	var valArray=[1,2,3,4,5,6,7,8,9,10];

	var newImgarray=valArray;
	var imgCount=0;

	var arrCount=newImgarray.length;

	var quesList=getQuestion(newImgarray[imgCount]);

	$("#head_title").html(data.string.ex1_1);


	$('#head_title').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){


			myquestion(quesList);




	});//headin

	$("#mathContent").on('click','.checknext',function(){


		var anstrue=true;
		$('.inpTxt').each(function(index, element) {


			var corr=$(this).attr('valans');
			var answer=$(this).val();


			if(answer!=corr)
			{
					$(this).addClass('borderMe');
					$(this).val('');

					anstrue=false;
			}
		});

		if(anstrue==true)
		{
			$(".checknext").fadeOut(0);

			$("#mathContent").find('.imgCor').fadeIn(0).addClass('animated bounce').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {

					$("#activity-page-next-btn-enabled").fadeIn(10).addClass('animated wobble');
			})

		}
	});//click

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).removeClass('animated wobble').fadeOut(0);
		$(".mulQues").addClass('animated hinge').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {

			$(this).fadeOut(10).removeClass('animated hinge');


			$("#mathContent").find('.imgCor').fadeOut(0).removeClass('animated bounce');
			imgCount++;

			if(imgCount<arrCount)
			{
				$("#myquest").html('');
				quesList=getQuestion(newImgarray[imgCount]);
				myquestion(quesList);
			}
			else
			{
				loadTimelineProgress(11,11);
				$("#ex2_t2").html(data.string.ex2_corr);
				$("#myquest").html('');

				var source   = $("#last-template").html();

				var template = Handlebars.compile(source);


				var html=template({});

				$("#myquest").html(html);

				var newhtml="<table class='newTab'>";

				newhtml=newhtml+
				"<tr  class='mytar'><td>"+data.string["ex1_qt5"]+"</td><td>"+data.string["ex1_qt6"]+"</td><td>"+data.string["ex1_qt7"]+"</td></tr>";

				var q1=parseInt(data.string.ex1_qt2),
				q2=parseInt(data.string.ex1_qt3),
				q3=parseInt(data.string.ex1_qt4);



				for(var $kk=1;$kk<=10;$kk++)
				{
					var ques=parseInt(data.string["ex1_q1"+$kk]);
					var ans1= whatans(ques);

					newhtml=newhtml+"<tr  class='mytartd'><td>"+data.string["ex1_q1"+$kk]+"</td>"+
						"<td>"+ans1[0]+"</td>"+
						"<td>"+ans1[1]+"</td>"+
						"</tr>";

				}
				newhtml+="</table>";

				$("#myquest").find('#tabme').html(newhtml);

				$("#myquest").find('#tabme').delay(300).fadeIn(0,function(){
					ole.footerNotificationHandler.pageEndSetNotification();
				});//tabme
			}
		})//hinge

	});//click

});

function getQuestion($quesNo)
{
	var $whatnextbtn=getArrowBtn();

	loadTimelineProgress(11,$quesNo);
	var quesList;

	var q1=parseInt(data.string.ex1_qt2),
			q2=parseInt(data.string.ex1_qt3),
			q3=parseInt(data.string.ex1_qt4);


	var ques=parseInt(data.string["ex1_q1"+$quesNo]);
	var ans1= whatans(ques);

	quesList={
		questionPart1 : data.string["ex1_qt5part1"],
		questionPart2 : data.string["ex1_q1"+$quesNo],
		questionPart3 : data.string["ex1_qt5part2"],
		question:data.string["ex1_qt5"],
		opt1:data.string["ex1_qt6"],
		opt2:data.string["ex1_qt7"],
		ans1:ans1[0],
		ans2:ans1[1],
		srcImg:$ref+"/exercise/images/correct.png",
		imgnext:$whatnextbtn
		}
	return quesList;
}

function whatans(ques)
{
	var ans=new Array();
	ans[0]= (ques)/2;
	ans[1]= (ques)/4;

	return ans;
}

function myquestion($arrays)
{
	$("#myquest").fadeOut(10).delay(10).fadeIn(500,function(){
		var source   = $("#question-template").html();

		var template = Handlebars.compile(source);

		var $dataval=$arrays;
		var html=template($dataval);

		$("#myquest").html(html);

	});

}

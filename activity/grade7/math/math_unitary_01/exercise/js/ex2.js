$(function(){

	var $myid;

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	var whatpage=1;

	var innerpage=1;

	var valArray=[1,2,3,4,5,6,7];

	var newImgarray=valArray;
	var imgCount=0;

	var arrCount=newImgarray.length;

	var quesList=getQuestion(newImgarray[imgCount]);

	$("#head_title").html(data.string.ex2_t1);
	$("#ex2_t2").html(data.string.ex2_t2);



	$("#ex2_t2").fadeIn(100,function(){

		myquestion(quesList);


	});//fadin



	$("#mathContent").on('click','.checknext',function(){


		var anstrue=true;
		$('.inpTxt').each(function(index, element) {


			var corr=$(this).attr('valans');
			var answer=$(this).val();


			if(answer!=corr)
			{
					$(this).addClass('borderMe');
					$(this).val('');

					anstrue=false;
			}
		});

		if(anstrue==true)
		{
			$(".checknext").fadeOut(0);

			$("#mathContent").find('.imgCor').fadeIn(0).addClass('animated bounce').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {

					$("#activity-page-next-btn-enabled").fadeIn(10);
			});

		}
	});//click

	$("#activity-page-next-btn-enabled").click(function(){
		console.log('hey');
		$(this).removeClass('animated wobble').fadeOut(0);

		$(".mulQues").addClass('animated hinge').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {

			$(this).fadeOut(10).removeClass('animated hinge');

			$("#mathContent").find('.imgCor').fadeOut(0).removeClass('animated bounce');
			imgCount++;

			if(imgCount<arrCount)
			{
				$("#myquest").html('');
				quesList=getQuestion(newImgarray[imgCount]);
				myquestion(quesList);
			}
			else
			{
				loadTimelineProgress(8,8);
				$("#ex2_t2").html(data.string.ex2_corr);
				$("#myquest").html('');

				var source   = $("#last-template").html();

				var template = Handlebars.compile(source);


				var html=template({});

				$("#myquest").html(html);

				var newhtml="<table class='newTab'>";

				for(var $kk=1;$kk<=7;$kk++)
				{
					newhtml=newhtml+"<tr  class='mytar'><td>"+data.string["ex2_q"+$kk]+"</td><td colspan='3'></td></tr>"
					+"<tr  class='mytartd'><td></td><td>"+data.string["ex2_q"+$kk+"_1"]+" : "+data.string["ex2_q"+$kk+"_1_ans"]+"</td>"
					+"<td>"+data.string["ex2_q"+$kk+"_2"]+" : "+data.string["ex2_q"+$kk+"_2_ans"]+"</td>"
					+"<td>"+data.string["ex2_q"+$kk+"_3"]+" : "+data.string["ex2_q"+$kk+"_3_ans"]+"</td></tr>";

				}
				newhtml+="</table>";

				$("#myquest").find('#tabme').html(newhtml);

				$("#myquest").find('#tabme').delay(300).fadeIn(0,function(){
					$("#myquest").find('.reloadThis').delay(800).fadeIn(0);
				});//tabme
			}
		});//hinge

	});

	$("#mathContent").on('click','.reloadThis',function(){
		ole.activityComplete.finishingcall();
	});

});

function getQuestion($quesNo)
{
	var $whatnextbtn=getArrowBtn();
	loadTimelineProgress(8,$quesNo);
	var quesList;

	quesList={
		question:data.string["ex2_q"+$quesNo],
		opt1:data.string["ex2_q"+$quesNo+"_1"],
		opt2:data.string["ex2_q"+$quesNo+"_2"],
		opt3:data.string["ex2_q"+$quesNo+"_3"],
		ans1:data.string["ex2_q"+$quesNo+"_1_ans"],
		ans2:data.string["ex2_q"+$quesNo+"_2_ans"],
		ans3:data.string["ex2_q"+$quesNo+"_3_ans"],
		srcImg:$ref+"/exercise/images/correct.png",
		imgnext:$whatnextbtn
		}
	return quesList;
}

function myquestion($arrays)
{
	$("#myquest").fadeOut(10,function(){
		$(".mulQues").fadeIn(10);
	}).delay(10).fadeIn(500,function(){
		var source   = $("#question-template").html();

		var template = Handlebars.compile(source);

		var $dataval=$arrays;
		var html=template($dataval);

		$("#myquest").html(html);

	});

}

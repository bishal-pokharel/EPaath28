var content=[
	{
		// intropage
		introText : data.lesson.chapter,
		text_additional_class:"covertext",
		coverpage : $ref+"/images/coverpage.png",

	},
	{
		// intropage
		introText : data.string.p1s1,
		defination : data.string.def1,

	},
	{
		// second-slide
		example : data.string.eg1,
		animals : $ref+"/images/animals.png",
		cap1 : data.string.p1_1,

		info6 : data.string.p1s7,
		info7 : data.string.p1s8,
		info8 : data.string.p1s9,
		info9 : data.string.p1s10,
		cap2 : data.string.p1_2,

		cap3 : data.string.p1_3,
		shape : $ref+"/images/shapes.png",

		cloth : $ref+"/images/01.png",
		cap4 : data.string.p1_4,

		fruit : $ref+"/images/fruits.png",
		cap6 : data.string.p1_6,

		info10 : data.string.p1s11,
		info11 : data.string.p1s12,
		info12 : data.string.p1s13,
		info13 : data.string.p1s14,
		info14 : data.string.p1s15,
		info15 : data.string.p1s16,
		info16 : data.string.p1s17,
		info17 : data.string.p1s18,
		info18 : data.string.p1s19,
		info19 : data.string.p1s20,
		info20 : data.string.p1s21,
		cap5 : data.string.p1_5
	},
	{
		// third-slide
		example : data.string.topic2,

		animals : $ref+"/images/animals.png",
		cap1 : data.string.p1_1,
		para1 : data.string.p1s22,

		animal1 : $ref+"/images/02.png",
		animal2 : $ref+"/images/03.png",
		animal3 : $ref+"/images/04.png",
		animal4 : $ref+"/images/05.png",
		animal5 : $ref+"/images/06.png",
		animal6 : $ref+"/images/07.png",
		animal7 : $ref+"/images/08.png",

		cap6 : data.string.p1_6,
		cloth : $ref+"/images/01.png",
		para06 : data.string.p1s28,
		picture2 : $ref+"/images/2.png",
		picture3 : $ref+"/images/3.png",
		picture4 : $ref+"/images/4.png",
		picture5 : $ref+"/images/5.png",
		picture6 : $ref+"/images/6.png",
		picture7 : $ref+"/images/7.png",
		picture8 : $ref+"/images/8.png",
	},

	{
		// fourth-slide
		example : data.string.topic2,

		cap3 : data.string.p1_3,
		shape : $ref+"/images/shapes.png",

		cap4 : data.string.p1_4,
		fruit : $ref+"/images/fruits.png",

		para03 : data.string.p1s26,
		img1 : $ref+"/images/fruit01.png",
		img2 : $ref+"/images/fruit02.png",
		img3 : $ref+"/images/fruit03.png",
		img4 : $ref+"/images/fruit04.png",
		img5 : $ref+"/images/fruit05.png",

		para04 :data.string.p1s27,
		img6 : $ref+"/images/shape1.png",
		img7 : $ref+"/images/shape2.png",
		img8 : $ref+"/images/shape3.png",
		img9 : $ref+"/images/shape4.png",
		img10 : $ref+"/images/shape5.png",
		img11 : $ref+"/images/shape6.png",
		img12 : $ref+"/images/shape7.png"
	},

	{
		// fifth-slide
		example : data.string.topic2,

		info6 : data.string.p1s7,
		info7 : data.string.p1s8,
		info8 : data.string.p1s9,
		info9 : data.string.p1s10,
		cap2 : data.string.p1_2,

		info10 : data.string.p1s11,
		info11 : data.string.p1s12,
		info12 : data.string.p1s13,
		info13 : data.string.p1s14,
		info14 : data.string.p1s15,
		info15 : data.string.p1s16,
		info16 : data.string.p1s17,
		info17 : data.string.p1s18,
		info18 : data.string.p1s19,
		info19 : data.string.p1s20,
		info20 : data.string.p1s21,
		cap5 : data.string.p1_5,
		para3 : data.string.p1s24,
		para2 : data.string.p1s23,
	},

	{
		// last-slide
		paral : data.string.p1s25
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 7;
	loadTimelineProgress($total_page,countNext+1);

/*
* first
*/
	function first () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');
		$(".board > p:nth-of-type(2)").delay(100).fadeIn(1000, function(){
			$("#activity-page-next-btn-enabled").finish().show(0);
		});

	}

/*
* second
*/
	function second () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');
		$(document).ready(function(){
			$(".set1").delay(500).fadeIn();
			$(".set4").delay(1000).fadeIn();
			$(".set3").delay(1500).fadeIn();
			$(".clothes").delay(2000).fadeIn();
			$(".set2").delay(2500).fadeIn();
			$(".set5").delay(3000).fadeIn();
			$("#activity-page-next-btn-enabled").delay(3500).fadeIn();
		});

	}
/*
* third
*/
	function third () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		$(document).ready(function(){
			$(".set1").delay(500).fadeIn();
			$("#detail1").delay(1000).fadeIn();
			$(".clothes").delay(1500).fadeIn();
			$("#cloth_detail").delay(2000).fadeIn();
			$("#activity-page-next-btn-enabled").delay(2100).fadeIn();
		});
	}
/*
* fourth
*/
	function fourth () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		$(document).ready(function(){
			$(".set3").css("top", "10%").css("left", "10%").css("padding", "0%").delay(800).fadeIn();
			$("#detail3").delay(1200).fadeIn();
			$(".set4").delay(1800).fadeIn();
			$(".set4").css("top", "60%");
			$(".set4").css("left", "9%");
			$("#detail4").delay(2000).fadeIn();
			$("#activity-page-next-btn-enabled").delay(2000).fadeIn();
		});
	}

/*
* fifth
*/
	function fifth () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		$(document).ready(function(){
			$(".set5").css("left", "10%").css("margin-bottom", "28%").delay(500).fadeIn();
			$("#detail5").delay(1000).fadeIn();
			$(".set2").css("left", "10%").delay(1500).fadeIn();
			$("#detail2").delay(2000).fadeIn();
			$("#activity-page-next-btn-enabled").delay(2100).fadeIn();
		});
	}

/*
* sixth
*/
	function sixth () {
	var source = $("#sixth-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', '');
		ole.footerNotificationHandler.pageEndSetNotification();
	}


	first(countNext);

	$nextBtn.on('click',function () {
		countNext++;
		switch (countNext) {
			case 1:
				first ();
			break;
			case 2:
				second();
				break;
			case 3:
				third();
				break;
			case 4:
				fourth();
				break;
			case 5:
				fifth();
				break;
			case 6:
				sixth();
				$("#activity-page-next-btn-enabled").hide(0);
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});

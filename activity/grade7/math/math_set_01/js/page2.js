var content=[
	{	
		introText : data.string.p2s1,
		definition : data.string.def2,
		definition2 : data.string.def3,

	},

	{
		example : data.string.eg2,
		para1 : data.string.p2_s_1,
		para2 : data.string.p2s2,
		para3 : data.string.p2s3
		
	},	

	{
		example : data.string.eg2,
		para4 : data.string.p2s4,
		para5 : data.string.p2s5,
		para6 : data.string.p2s6
	},	
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

/*
* first
*/
	function first () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');
		$(".Btn1").hide(0);
		$(".board > p:nth-of-type(2)").delay(100).fadeIn(1000);
		$(".board > p:nth-of-type(3)").delay(200).fadeIn(1000, function(){
			$("#activity-page-next-btn-enabled").finish().show(0);
		});
	}

/*
* second
*/
	function second () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

			$("#main > p:nth-of-type(2)").delay(150).fadeIn();
			$("#main > p:nth-of-type(3)").delay(150).fadeIn();
			$(".Btn1").delay(1000).fadeIn();

			$(".Btn1").click (function(){

				$("#caption").show(0);
				$(".Btn1").hide(0);
				$("#activity-page-next-btn-enabled").show(0);
			});
			
	}
/*
* third
*/
	function third () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');
		$(".Btn1").hide(0);

		$("#main2 > p:nth-of-type(2)").delay(150).fadeIn();
		$(".Btn1").delay(1000).fadeIn(0);
		$(".Btn1").click (function(){
			$(".Btn1").hide(0);
			$("#activity-page-next-btn-enabled").hide(0);
			$("#caption").hide(0);
			$("#caption2").fadeIn(100,function(){
			ole.footerNotificationHandler.pageEndSetNotification();
			});		
		});		
	}
	

	first(countNext);

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				second();
				break;
			case 2:
				third();
				$("#activity-page-next-btn-enabled").hide(0);
				break;
			
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
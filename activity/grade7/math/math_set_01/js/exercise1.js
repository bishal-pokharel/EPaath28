/*content object*/
var content=[
	{
		para1 : data.string. e1s7,
		para2 : data.string. e1s8,

		para5 : data.string.e1s5,
		para6 : data.string.e1s6,	
		draggableitemdata : "M",
	},
	{
		para1 : data.string. e1s9,
		para2 : data.string. e1s10,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "BnF",		
	},
	{
		para1 : data.string. e1s11,
		para2 : data.string. e1s12,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "BnMnF",			
	},
	{
		para1 : data.string. e1s13,
		para2 : data.string. e1s14,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "BnM",
	},
	{
		para1 : data.string. e1s15,
		para2 : data.string. e1s16,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "F",
					
	},
	{
		para1 : data.string. e1s17,
		para2 : data.string. e1s18,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "B",
					
	},
	{
		para1 : data.string. e1s19,
		para2 : data.string. e1s20,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "BnF",
					
	},
	{
		para1 : data.string. e1s21,
		para2 : data.string. e1s22,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "M",
					
	},
	{
		para1 : data.string. e1s23,
		para2 : data.string. e1s24,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "MnF",
					
	},
	{
		para1 : data.string. e1s25,
		para2 : data.string. e1s26,
		para5 : data.string.e1s5,
		para6 : data.string.e1s6,
		draggableitemdata : "BnMnF",
					
	},
];

var smth = ole.getRandom(10,9,0);
console.log(smth);


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var count = 0;
	var correctdrops = 0;

	var $total_page = 10;
	loadTimelineProgress($total_page,countNext+1);

/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[smth[countNext]]);
		$board.html(html);
		$nextBtn.hide(0);
		$("#correct").hide(0);
		$("#wrong").hide(0);
		$("#topic").text(data.string.e1s1);

		$("#title1").html(data.string.e1s2);
		$("#title2").text(data.string.e1s3);
		$("#title3").text(data.string.e1s4);

		var draggables = $board.children("table#condition_table").children("tbody").children("tr:nth-of-type(2)").children("td:nth-of-type(1)").children("p");

		draggables.draggable({
			revert : function(event, ui) {
            $("#wrong").show(0);
            return !event;
        }});
		
		var droppables = $("div.center_box").children("div.droppableArea");

		droppables.droppable({
			accept:function(e){
				if (e.data('draggableitem') == $(this).attr('id')) {
					return true;					
				}				
			},

			drop:function(e,ui){	
				var oldhtml = $("#"+ui.draggable.data('draggableitem')).html();
				$("#"+ui.draggable.data('draggableitem')).html(oldhtml+"<p>"+ui.draggable.text()+"</p>");
				draggables.hide(0);
				$("#correct").show(0);				
				$("#wrong").hide(0);
					correctdrops++;
					// alert(correctdrops);
					if(correctdrops < 10){
						$("#activity-page-next-btn-enabled").show(0);
					}
					else if(correctdrops == 10){
						ole.footerNotificationHandler.pageEndSetNotification();
					}
					/*lines below comes in combination with return !event in draggables
					to send the draggables to original position if it reverts*/
					$(this).data("uiDraggable").originalPosition = {
		                top : 0,
		                left : 0
            		}
				}
									
			});	
	}	

	/*first call to first*/
	first();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		countNext++;
		switch (countNext) {
			case 1:				
				first();
				break;
			case 2:
				first();
				break;
			case 3:
				first();
				break;
			case 4:
				first();
				break;
			case 5:
				first();
				break;
			case 6:
				first();
				break;
			case 7:
				first();
				break;
			case 8:
				first();
				break;
			case 9:
				first();
				break;
			case 10:
				first();
				break;
			default:break;
		}		
		loadTimelineProgress($total_page,countNext+1);
	});
});
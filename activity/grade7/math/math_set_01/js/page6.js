/*content object*/
var content=[
	{
		introText : data.string.p5s1,

	},
	{
		introText : data.string.p5s1,
		para1 : data.string.def8,
	},
	{
		introText : data.string.eg8,
		para4 : data.string.p5s9,
		paraA : data.string.start_line,
		paraB : data.string.end_line,
		spantag1 : "<span class = 'setelement1'> </span>",
		spantag2 : "<span class = 'setelement2'> </span>",
		frog : $ref+"/images/frog001.png",
		correct : $ref+"/images/cartoon.png"

	},
	{
		introText : data.string.eg8,
		paraA : data.string.start_line,
		paraB : data.string.end_line,
		paraC : data.string.start_line2,
		paraD : data.string.end_line2,
		spantag1 : "<span class = 'setelement1'> </span>",
		spantag2 : "<span class = 'setelement2'> </span>",
		frog : $ref+"/images/frog001.png",
		correct : $ref+"/images/cartoon.png"
	},
	{
		introText : data.string.eg8,
		paraA : data.string.start_line,
		paraB : data.string.end_line1_1,
		paraC : data.string.start_line3_1,
		paraD : data.string.end_line3,
		spantag1 : "<span class = 'setelement1'> </span>",
		spantag2 : "<span class = 'setelement2'> </span>",

		frog : $ref+"/images/frog001.png",
		correct : $ref+"/images/cartoon.png"
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var img_count = 0;
	var img_count1 = 0;
	var img_count2 = 0;
	var img_count3 = 0;

	var countclicks = 0;

	var $total_page = 5;
	loadTimelineProgress($total_page,countNext+1);
/*
* first
*/
	function first () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', '');
	}
/*
* second
*/
	function second () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', "");

		$("#info").delay(200).fadeIn(300);
	}
/*
* third
*/
	function third () {
		var source = $("#third-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		$(".setelement1").css("color", "#0099CC");

		var counter = Math.floor((Math.random() * 2) + 1);

		switch (counter){
			case 1:
			$("span.setelement1").text(data.string.p5s4);
				$("#numbers").children("div").children("p").draggable({
				revert:"invalid"
				});

				$("#single_set").droppable({
					accept:function(e){
						if(e.hasClass("prime")){
							return true;
						}
					},
						drop:function(e){
							img_count++;

							if (img_count == 8) {
							$("#numbers").children("div").children("p").draggable("disable");
							$("#cartoon").show(0);
							$nextBtn.show(0);
							countclicks ++;
							}
						}
				});

				countclicks ++;
			break;

			case 2:
			$("span.setelement1").text(data.string.p5s5);
				$("#numbers").children("div").children("p").draggable({
				revert:"invalid"
				});

				$("#single_set").droppable({
					accept:function(e){
						if(e.hasClass("whole")){
							return true;
						}
					},
					drop:function(e){
						img_count++;

						if (img_count == 11) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						$nextBtn.show(0);
						countclicks ++;
						}
					}
				});
				countclicks ++;
			break;
			default:break;
		}
}
/*
* fourth
*/
	function fourth () {
		var source = $("#fourth-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		// $nextBtn.css('display', 'none');
		$(".setelement1").css("color", "#0099CC");
		$(".setelement2").css("color", "#0099CC");
		$("#double_set1").show(0);
		$("#double_set2").show(0);


		var count = Math.floor((Math.random() * 2) + 1);

		switch(count){
			case 1:
			$("span.setelement1").text(data.string.p5s4);
			$("span.setelement2").text(data.string.p5s5);
			$("#activity_space > p:nth-of-type(2)").show(0);

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			$("#double_set1").droppable({
				accept:function(e){
					if(e.hasClass("prime")){
						return true;
					}
				},
				drop:function(e){
					img_count1++;
					if (img_count1 == 8 && img_count2 == 9) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						$nextBtn.show(0);
						countclicks ++;
					}
				}
			});

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			$("#double_set2").droppable({
				accept:function(e){
					if(e.hasClass("whole")){
						return true;
					}
				},
				drop:function(e){
					img_count2++;

					if (img_count1 == 8 && img_count2 == 11) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						$nextBtn.show(0);
						countclicks ++;
					}
				}
			});

			break;

			case 2:
			$("span.setelement1").text(data.string.p5s7);
			$("span.setelement2").text(data.string.p5s6);

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			$("#double_set1").droppable({
				accept:function(e){
					if(e.hasClass("odd")){
						return true;
					}
				},
				drop:function(e){
					img_count1++;
					if (img_count1 == 10 && img_count2 == 10) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						$nextBtn.show(0);
						countclicks ++;
					}
				}
			});

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			$("#double_set2").droppable({
				accept:function(e){
					if(e.hasClass("even")){
						return true;
					}
				},
				drop:function(e){
					img_count2++;

					if (img_count1 == 10 && img_count2 == 10) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						$nextBtn.show(0);
						countclicks ++;
					}
				}
			});
			break;
		default:break;
	}

	}
/*
* fifth
*/
	function fifth () {
		var source = $("#fifth-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$(".setelement1").css("color", "#0099CC");
		$(".setelement2").css("color", "#0099CC");
		$(".info1").css("left", "0%");

		$(".info2").css("left","1%");

		$("#activity_space > p:nth-of-type(2)").show(0);

	var different = Math.floor((Math.random() * 2) + 1);
		switch(different){

			case 1:
			$("span.setelement1").text(data.string.p5s11);
			$("span.setelement2").text(data.string.p5s12);

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});
			var imgcount1 = 0;
			$("#circledata1").droppable({
				accept:function(e){
					if(e.hasClass("two")){
						return true;
					}
				},
				drop:function(e){
					imgcount1++;
					console.log(imgcount1);
					if (imgcount1 == 8 && imgcount2 == 2 && imgcount3 == 2) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						ole.footerNotificationHandler.lessonEndSetNotification();
					}
				}
			});

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			var imgcount2 =0;
			$("#intersection").droppable({
				accept:function(e){
					if(e.hasClass("inter1")){
						return true;
					}
				},
				drop:function(e){
					imgcount2++;
					console.log(imgcount2);
					if (imgcount1 == 8 && imgcount2 == 2 && imgcount3 == 2) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						ole.footerNotificationHandler.lessonEndSetNotification();
					}
				}
			});

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			var imgcount3 =0;
			$("#circledata2").droppable({
				tolerance: "fit",
				accept:function(e){
					if(e.hasClass("five")){
						return true;
					}
				},
				drop:function(e){
					imgcount3++;
					console.log(imgcount3);

					if (imgcount1 == 8 && imgcount2 == 2 && imgcount3 == 2) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						ole.footerNotificationHandler.lessonEndSetNotification();
					}

				}
			});


			break;

			case 2:
			$("span.setelement1").text(data.string.p5s7);
			$("span.setelement2").text(data.string.p5s10);

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			var imgcount1 =0;
			$("#circledata1").droppable({
				accept:function(e){
					if(e.hasClass("odd")){
						return true;
					}
				},
				drop:function(e){
					imgcount1++;
					console.log(imgcount1);
					if (imgcount1 == 7 && imgcount2 == 3 && imgcount3 == 3) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						ole.footerNotificationHandler.lessonEndSetNotification();
					}
				}
			});

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			var imgcount2 = 0;
			$("#intersection").droppable({
				accept:function(e){
					if(e.hasClass("inter2")){
						return true;
					}
				},
				drop:function(e){
					imgcount2++;
					console.log(imgcount2);
					if (imgcount1 == 7 && imgcount2 == 3 && imgcount3 == 3) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						ole.footerNotificationHandler.lessonEndSetNotification();
					}
				}
			});

			$("#numbers").children("div").children("p").draggable({
			revert:"invalid"
			});

			var imgcount3 = 0;
			$("#circledata2").droppable({
				tolerance: "fit",
				accept:function(e){
					if(e.hasClass("three")){
						return true;
					}
				},
				drop:function(e){
					imgcount3++;
					console.log(imgcount3);
					if (imgcount1 == 7 && imgcount2 == 3 && imgcount3 == 3) {
						$("#numbers").children("div").children("p").draggable("disable");
						$("#cartoon").show(0);
						ole.footerNotificationHandler.lessonEndSetNotification();

					}
				}
			});

			break;
			default:break;
		}
	}

	/*first call to first*/
	first();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		countNext++;
		switch (countNext) {
			case 1:
				second();
				break;
			case 2:
				third();
				break;
			case 3:
				fourth();
				break;
			case 4:
				fifth();
				break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});
});

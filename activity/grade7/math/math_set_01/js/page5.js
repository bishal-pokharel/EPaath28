var content = [
  {
    introText: data.string.p6s1,
    definition: data.string.def7,
    example: data.string.eg7,
    fruit: $ref + "/images/fruits.png",
    fruit2: $ref + "/images/fruits01.png",
    fruit3: $ref + "/images/appleandmango.png",
    fruit4: $ref + "/images/fruits-02.png",
    fruit5: $ref + "/images/fruits-03.png",
    cap4: data.string.caption1,
    cap5: data.string.caption2,
    cap6: data.string.caption3,
    setinfo1: data.string.sinfo1,
    setinfo2: data.string.sinfo2
  },

  {
    introText: data.string.p6s1,
    para1: data.string.p6s2,
    shape: $ref + "/images/shapes2.png"
  },

  {
    introText: data.string.p6s1,
    para1: data.string.p6s3,
    para2: data.string.p6s4,
    para3: data.string.p6s5,

    img6: $ref + "/images/shape01.png",
    shape1: "shape1",

    img7: $ref + "/images/shape02.png",
    shape2: "shape2",

    img8: $ref + "/images/shape03.png",
    shape3: "shape3",

    img9: $ref + "/images/shape04.png",
    shape4: "shape4",

    img10: $ref + "/images/shape05.png",
    shape5: "shape5",

    img11: $ref + "/images/shape06.png",
    shape6: "shape6",

    img12: $ref + "/images/shape07.png",
    shape7: "shape7"
  },

  {
    introText: data.string.p6s1,
    para4: data.string.p6s4_1,
    para5: data.string.p6s5_1,
    para6: data.string.p6s10,
    para7: data.string.p6s6,
    para8: data.string.p6s7
  },

  {
    introText: data.string.p6s1,
    para4: data.string.p6s4_1,
    para5: data.string.p6s5_1,
    para12: data.string.p6s8,
    para10: data.string.p6s11,
    para11: data.string.p6s9
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var countNext = 0;

  var imgarray = [];
  var secondarray = [];
  var classarray1 = [];
  var classarray2 = [];

  var common_usr = [];
  var difference_usr = [];
  var difference_cmp = [];

  var clickedimg;

  var $total_page = 5;
  loadTimelineProgress($total_page, countNext + 1);
  /*
   * first
   */
  function first() {
    var source = $("#first-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "none");
    $(".board > p:nth-of-type(2)")
      .delay(1000)
      .fadeIn(1000);
    $(".board > p:nth-of-type(3)")
      .delay(2000)
      .fadeIn(2000);
    $(".set4")
      .delay(2000)
      .fadeIn(2000);
    $(".set4 > p")
      .delay(2500)
      .fadeIn(2000);
    $("#caption4")
      .delay(2000)
      .fadeIn(2000);
    $(".set5")
      .delay(3000)
      .fadeIn(2000);
    $(".set5 > p")
      .delay(3500)
      .fadeIn(2000);
    $("#caption5")
      .delay(3000)
      .fadeIn(2000);
    $(".set6")
      .delay(4000)
      .fadeIn(2000);
    $(".set7")
      .delay(4000)
      .fadeIn(2000);
    $(".set8")
      .delay(4000)
      .fadeIn(2000);
    $("#c8")
      .delay(4000)
      .fadeIn(2000, function() {
        $("#activity-page-next-btn-enabled")
          .delay(100)
          .fadeIn();
      });
  }

  /*
   * second
   */
  function second() {
    var source = $("#second-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    $nextBtn.css("display", "");
  }
  /*
   * third
   */
  function third() {
    var source = $("#third-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "none");
    var global1 = 0;
    $("#shapes > img").css("width", "70%");

    var select = 0;
    var toclick = $board
      .children(".center")
      .children("div")
      .css("border", "none");
    var clickedimgnumber = 0;

    toclick.on("click", function() {
      var selected = $(this).attr("id");
      var imgpath = $(this)
        .children("img")
        .attr("src");
      var classpath = $(this).attr("class");
      var toshow;

      classarray1.push(classpath);
      // console.log(classarray1);

      imgarray.push(imgpath);
      // console.log(imgarray);

      switch (selected) {
        case "choose1":
          toshow = "#im1";
          tohide = "#choose1";
          break;

        case "choose2":
          toshow = "#im2";
          tohide = "#choose2";
          break;

        case "choose3":
          toshow = "#im3";
          tohide = "#choose3";
          break;

        case "choose4":
          toshow = "#im4";
          tohide = "#choose4";
          break;

        case "choose5":
          toshow = "#im5";
          tohide = "#choose5";
          break;

        case "choose6":
          toshow = "#im6";
          tohide = "#choose6";
          break;

        case "choose7":
          toshow = "#im7";
          tohide = "#choose7";
          break;

        default:
          break;
      }

      if (clickedimgnumber >= 5) {
        toshow.off("click");
      } else {
        $board
          .children("#user")
          .children(toshow)
          .show(0);
        $board
          .children(".center")
          .children(tohide)
          .hide(0);
        clickedimgnumber++;
      }
      clickedimg = clickedimgnumber;
      console.log(clickedimg);

      if (clickedimg == 5) {
        $(".image").hide(0);

        var elements = $(".image");
        var elementCount = elements.size();
        var elementsToShow = 5;
        var alreadyChoosen = " ";
        var i = 0;
        $nextBtn.show(0);
        while (i < elementsToShow) {
          var rand = Math.floor(Math.random() * elementCount);
          console.log("rand value >>>>>>>>"+rand)
          if (alreadyChoosen.indexOf(" " + rand + " ") < 0) {
            alreadyChoosen += rand + " ";

            elements
              .eq(rand)
              .finish("toclick")
              .fadeIn()
              .css("border", "none");
            var imgpath2 = elements.eq(rand).attr("src");
            var classpath2 = elements.eq(rand).attr("id");
            classarray2.push(classpath2);

            secondarray.push(imgpath2);
            ++i;
          }
        }
      }
    });
  }
  /*
   * fourth
   */
  function fourth() {
    var source = $("#fourth-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "none");
    $(".popup_btn").hide(0);
    $(".Btn1").show(0);
    $(".comma").hide(0);

    for (var i = 1; i < 6; i++) {
      $("#body > p:nth-of-type(1)")
        .children("img:nth-of-type(" + i + ")")
        .attr("src", imgarray[i - 1]);
      $("#body > p:nth-of-type(1)")
        .children("img:nth-of-type(" + i + ")")
        .attr("class", classarray1[i - 1]);

      $("#body > p:nth-of-type(2)")
        .children("img:nth-of-type(" + i + ")")
        .attr("src", secondarray[i - 1]);
      $("#body > p:nth-of-type(2)")
        .children("img:nth-of-type(" + i + ")")
        .attr("class", classarray2[i - 1]);
    }

    jQuery.grep(classarray1, function(el) {
      if (jQuery.inArray(el, classarray2) >= 0) {
        common_usr.push(el);
      } else if (jQuery.inArray(el, classarray2) == -1) {
        difference_usr.push(el);
      }
    });

    jQuery.grep(classarray2, function(el) {
      if (jQuery.inArray(el, classarray1) == -1) {
        difference_cmp.push(el);
      }
    });

    for (var i = 0; i < common_usr.length; i++) {
      switch (common_usr[i]) {
        case "shape1":
          $("." + common_usr[i]).css("border", "2px solid red");
          break;

        case "shape2":
          $("." + common_usr[i]).css("border", "2px solid yellow");
          break;

        case "shape3":
          $("." + common_usr[i]).css("border", "2px solid #FF0066");
          break;

        case "shape4":
          $("." + common_usr[i]).css("border", "2px solid #00FF00");
          break;

        case "shape5":
          $("." + common_usr[i]).css("border", "2px solid #CC00CC");
          break;

        case "shape6":
          $("." + common_usr[i]).css("border", "2px solid #800000");
          break;

        case "shape7":
          $("." + common_usr[i]).css("border", "2px solid #00FFFF");
          break;

        default:
          break;
      }
    }

    var union = [];
    union = common_usr.concat(difference_usr, difference_cmp);

    for (var i = 0; i < union.length; i++) {
      var j = i + 1;
      $("#answer > p")
        .children("img:nth-of-type(" + j + ")")
        .attr("src", $("." + union[i]).attr("src"))
        .show(0);
        $(".f"+i).show();
    }

    $(".Btn1").click(function() {
      $("#extra_info").show(0);
      $("#answer").show(0);
      $(".Btn1").hide(0);
      $(".popup_btn").show(0);
    });

    $(".popup_btn").click(function() {
      $("#popup").show(0);
      $(".popup_btn").hide(0);
      $("#body").css("border", "none");
      $("#body").css("background", "none");
      $("#activity-page-next-btn-enabled").show(0);
    });
  }

  /*
   * fifth
   */
  function fifth() {
    var source = $("#fifth-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "");
    $(".popup_btn").hide(0);
    $(".Btn1").show(0);

    for (var i = 1; i < 6; i++) {
      $("#body > p:nth-of-type(1)")
        .children("img:nth-of-type(" + i + ")")
        .attr("src", imgarray[i - 1]);
      $("#body > p:nth-of-type(1)")
        .children("img:nth-of-type(" + i + ")")
        .attr("class", classarray1[i - 1]);

      $("#body > p:nth-of-type(2)")
        .children("img:nth-of-type(" + i + ")")
        .attr("src", secondarray[i - 1]);
      $("#body > p:nth-of-type(2)")
        .children("img:nth-of-type(" + i + ")")
        .attr("class", classarray2[i - 1]);
    }

    for (var i = 0; i < common_usr.length; i++) {
      switch (common_usr[i]) {
        case "shape1":
          $("." + common_usr[i]).css("border", "2px solid red");
          break;

        case "shape2":
          $("." + common_usr[i]).css("border", "2px solid yellow");
          break;

        case "shape3":
          $("." + common_usr[i]).css("border", "2px solid #FF0066");
          break;

        case "shape4":
          $("." + common_usr[i]).css("border", "2px solid #00FF00");
          break;

        case "shape5":
          $("." + common_usr[i]).css("border", "2px solid #CC00CC");
          break;

        case "shape6":
          $("." + common_usr[i]).css("border", "2px solid #800000");
          break;

        case "shape7":
          $("." + common_usr[i]).css("border", "2px solid #00FFFF");
          break;

        default:
          break;
      }
    }

    // for (var i = 0; i < union.length; i++) {
    //   var j = i + 1;
    //   $("#answer > p")
    //     .children("img:nth-of-type(" + j + ")")
    //     .attr("src", $("." + union[i]).attr("src"))
    //     .show(0);

    //   $("#answer > p")
    //     .children("img:nth-of-type(" + j + ")")
    //     .append("<strong>,</strong>");
    // }

    $(".Btn1").click(function() {
      $("#extra_info").show(0);
      $("#answer").show(0);
      $(".Btn1").hide(0);
      $(".popup_btn").show(0);
    });

    $("#activity-page-next-btn-enabled").hide(0);
    $(".popup_btn").click(function() {
      $("#popup").show(0);
      $(".popup_btn").hide(0);
      $("#body").css("border", "none");
      $("#body").css("background", "none");
      ole.footerNotificationHandler.pageEndSetNotification();
    });
  }

  first(countNext);

  $nextBtn.on("click", function() {
    countNext++;
    switch (countNext) {
      case 1:
        second();
        break;
      case 2:
        third();
        break;
      case 3:
        fourth();
        break;
      case 4:
        fifth();
        break;
      default:
        break;
    }
    loadTimelineProgress($total_page, countNext + 1);
  });
});

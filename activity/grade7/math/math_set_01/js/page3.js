/*content object*/
var content=[
	{
		// intropage
		introText : data.string.p4s1,
		definition : data.string.def,
	},

	{
		example : data.string.eg3,
		para1 : data.string.p4s1,
		para2 : data.string.p4s2,
		para3 : data.string.p4s3,
		cartoon : $ref+"/images/cartoon.png",
	},
	{
		introText : data.string.p4s1,
		para4 : data.string.p4s4,
		para5 : data.string.p4s21,
		para6 : data.string.p4s5,
		para7 : data.string.p4s6,
		para8 : data.string.p4s7,
		para9 : data.string.p4s8,

		eno : data.string.p4s14,
		oddno : data.string.p4s18,
		fno : data.string.p4s16,
		tno :data.string.p4s20,
		frog : $ref+"/images/frog.png",
		arrow1 : $ref+"/images/arrow-e.png",
		arrow2 : $ref+"/images/red-arrow.png",
		arrow3 : $ref+"/images/arrow-o.png",
		arrow4 : $ref+"/images/arrow-t.png",
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var check1 = false;
	var check2 = false;
	var check3 = false;
	var check4 = false; 
	
	var $total_page = 4;
	loadTimelineProgress($total_page,countNext+1);
		
/*
* first
*/
	function first () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
        $nextBtn.css('display', 'block');
		$(".board > p:nth-of-type(2)").delay(100).fadeIn(1000);
	}

/*
* second
*/
	function second () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		if (check1 == true) {
				$("#c1").css("background", "#B6B6B6");
			}
		if (check2 == true){
			$("#c2").css("background", "#B6B6B6");
		}	
		if ( check3 == true){
			$("#c3").css("background", "#B6B6B6");
		}
		if ( check4 == true){
			$("#c4").css("background", "#B6B6B6");
		}

		$("#c1").text(data.string.p4s5);
		$("#c2").text(data.string.p4s6);
		$("#c3").text(data.string.p4s7);
		$("#c4").text(data.string.p4s8);

		if (check1 == true && check2 == true && check3 == true && check4 ==true) {
			$("#activity-page-next-btn-enabled").show(0);
			$("#c1").die();
			$("#c2").die();
			$("#c3").die();
			$("#c4").die();
		};
		
		var typeofnumber = "";
		var circleclicked = $board.find("#circles").find("div");
		var $totalnumber ;
		var totalnumberofclicks;
		var totalcircleclicked = 0;
		circleclicked.on('click', function(){
			var circleclass = $(this).attr("class");
			totalnumberofclicks = 0;
			// alert(circleclass);
		 	$totalnumber = $board.find("#selection_box").find("#numbers").find("."+circleclass);
		 	// alert($totalnumber.length);
			$board.find('#selection_box').find('#numbers').find('div').find('p').show(0);
			totalcircleclicked ++;
			// alert(totalcircleclicked);
			if(totalcircleclicked == 4){
						$nextBtn.show(0);
					}
			$(this).css("background", "#B6B6B6");
			$(this).css('box-shadow','0px 0px 5px red');
			switch (circleclass){
				case "even": 
				$("span.setelement1").text(data.string.p4s9);
				$("span.setelement2").text("");
				typeofnumber = "even";
				break;

				case "odd": 
				$("span.setelement1").text(data.string.p4s17);
				$("span.setelement2").text("");
				typeofnumber = "odd";
				break;

				case "five":
				$("span.setelement1").text(data.string.p4s15);
				$("span.setelement2").text("");
				typeofnumber = "five";
				break;

				case "ten":
				$("span.setelement2").text("");
				$("span.setelement1").text(data.string.p4s19);
				typeofnumber = "ten";
				break;

				default:break;
			}
		});

		var totalclicksrequired;
		var clicked_number = $board.find('#selection_box').find('#numbers').find('div').find('p');
		clicked_number.on("click", function(){
			totalclicksrequired = $totalnumber.length;
			if ($(this).hasClass(typeofnumber)) {
				$(this).hide(0);

				var value = $(this).html();
				totalnumberofclicks++;
            	console.log(totalnumberofclicks);
				if (totalnumberofclicks < totalclicksrequired) {
            		$("span.setelement2").html($("span.setelement2").html()+value+",");

				}
            	else if(totalnumberofclicks == totalclicksrequired ) {
            	$("span.setelement2").html($("span.setelement2").html()+value+"}");

				}
			};
		});
	
	}
/*
* third
*/
	function third () {
		var source = $("#third-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		$("#even_info").delay(800).fadeIn(0);
			$("#a1").delay(800).fadeIn();
			$("#pic1").delay(1600).fadeIn(0);
			$("#e"). delay(1600).fadeIn(0);

			$("#five_info").delay(2400).fadeIn(0);
			$("#a2").delay(2400).fadeIn(0);
			$("#pic2").delay(3200).fadeIn(0);
			$("#f"). delay(3200).fadeIn(0);

			$("#ten_info").delay(4000).fadeIn(0);
			$("#a4").delay(4000).fadeIn(0);
			$("#pic4").delay(4800).fadeIn(0);
			$("#t"). delay(4800).fadeIn(0);

			$("#odd_info").delay(5600).fadeIn();
			$("#a3").delay(5600).fadeIn(0);
			$("#pic3").delay(6400).fadeIn(0);
			$("#o"). delay(6400).fadeIn(0, function(){
				$("#activity-page-next-btn-enabled").show(0);
			});

		$("#activity-page-next-btn-enabled").click (function(){
			
			$("#info").fadeIn(300,function(){
				ole.footerNotificationHandler.pageEndSetNotification();
			});
			$("#activity-page-next-btn-enabled").hide(0);
				
		});	

	}


	/*first call */
	first(countNext);

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				second();
				break;
			case 2:
				third();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
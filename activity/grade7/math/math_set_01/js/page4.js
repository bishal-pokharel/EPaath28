/*content object*/
var content=[
	{
		// intropage
		introText : data.string.p2s7,
		definition : data.string.def4,
		example : data.string.eg4,
		animals : $ref+"/images/animals.png",
		arrow1 : $ref+"/images/red-arrow.png",
		arrow2 : $ref+"/images/arrow-t.png",
		para1 : data.string.allanimal,
		para2 : data.string.wild,
		animal1 : $ref+"/images/02.png",
		animal2 : $ref+"/images/03.png",
		animal3 : $ref+"/images/04.png",
		animal4 : $ref+"/images/05.png",
		animal5 : $ref+"/images/06.png",
		animal6 : $ref+"/images/07.png",
		animal7 : $ref+"/images/08.png",
		ani1 : $ref+"/images/02.png",
		ani2 : $ref+"/images/03.png",
		ani3 : $ref+"/images/04.png",
		last1 : data.string.conclusion,
	},

	{
		// next slide
		intro : data.string.eg5,
		intro2 : data.string.def5,
		para1 : data.string.p2s8,
		para2 : data.string.p2_1,
		para3 : data.string.p2_2,

		// table data
		title1 : data.string.p2s9,
		title2 : data.string.p2s10,
		title3 : data.string.p2s11,
		title4 : data.string.p2s12,

		para4 : data.string.p2s13,
		para5 : data.string.p2s14,
		para6 : data.string.p2s15,
		para7 : data.string.p2s16,
		para8 : data.string.p2s17,

		para9 : data.string.p2s18,
		para10 : data.string.p2s19,
		para11 : data.string.p2s20,
		para12 : data.string.p2s21,
		para13 : data.string.p2s22,
		para15 : data.string.p4s23,

		maintext1 : data.string.p2s23,
		maintext2 : data.string.p2s24,
		maintext3 : data.string.p2s25,
		maintext4 : data.string.p2s26,
		maintext5 : data.string.p2s27,
		maintext6 : data.string.p2s28,
		para14 : data.string.p4s22
	},
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

	var global1 = 0;
	var global2 = 0;
	var global3 = 0;
	var global4 = 0;
	var global5 = 0;
	var global6 = 0;

	var names = [];
	var games = [];
	var subject = [];
	var age = [];

/*
* first
*/
	function first () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		$(".board > p:nth-of-type(2)").delay(1000).fadeIn(1000);
		$(".board > p:nth-of-type(3)").delay(1000).fadeIn(1000);
		$(".set1").delay(2000).fadeIn(500);
		$("#a1").finish().delay(2000).fadeIn(2000);
		$("#detail1").delay(3000).fadeIn(1000);
		$("#a2").delay(4000).fadeIn(2000);		
		$("#detail2").delay(5000).fadeIn(1000);
		$(".Btn1").delay(5000).fadeIn(1000);
		$(".Btn1").click (function(){
			$("#final").show(0);
			$(".Btn1").hide(0);
			$("#activity-page-next-btn-enabled").show(0);
		});
	}
/*
* second
*/
	function second () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');
		
		var $ageCell = $board.children('table#info_table').children('tbody').children('tr:nth-of-type(n+2)').children('td.age').children('input');
		var $nameCell = $board.children('table#info_table').children('tbody').children('tr:nth-of-type(n+2)').children('td.names').children('input');
			
			// make it certain that age input is only numbers
		$ageCell.keyup(function() {
			$(this).val( $(this).val().replace(/[^-+\d]/g,''));
		});

			// make it certain that age input is only letters
		$nameCell.keyup(function() {
			$(this).val( $(this).val().replace(/[^a-zA-Z]/g,''));
		});

		$("#btn").click (function(){
			var counterNew = 0;

			$board.find('#info_table').each(function () {
				$this=$(this);
				console.log($this);				
				
				var sm=$this.find('tr#row1').html();

				$this.find('tr').each(function () {
					if (counterNew > 0) {
						var namecheck = $(this).find("td.names input").val();
						var gamecheck = $(this).find("td select.dropdwn").val();
						var subjectcheck = $(this).find("td select.dropdwn2").val();
						var agecheck = $(this).find("td.age input").val();
						$("#dontleave").hide(0);
						
						if (namecheck==="" || gamecheck==="" || subjectcheck==="" || agecheck==="") {
							$("#dontleave").show(0);
							$("#activity-page-next-btn-enabled").hide(0);
							return false;
						};
							$("#activity-page-next-btn-enabled").show(0);
							names.push(namecheck);
							games.push(gamecheck);
							subject.push(subjectcheck);
							age.push(agecheck);
						
					};
					counterNew++;								
				});
			});
				

			var allname=names[0];
			for (var i = 1; i < names.length; i++) {
				allname +=", "+names[i]
			};

			$(".last > p:first").text(data.string.p2s23+" "+allname+"}");

			var footballChooser = [];
			console.log(games);

			for (var i = 0; i < 6; i++) {
				if (games[i] === "football") {
					footballChooser.push(i);
				};
			};

			var footballChooserNames = names[footballChooser[0]];
			for (var i = 1; i < footballChooser.length; i++) {
				footballChooserNames+=", "+names[footballChooser[i]];
			};

			if(footballChooser.length > 0){
				$(".last > p:nth-of-type(3)").text(data.string.p2s25+" "+footballChooserNames+"}");
			}
			else{
				$(".last > p:nth-of-type(3)").text(data.string.p2s25+" }");
			}
			
			var mathsChooser = [];
			console.log(subject);

			for (var i = 0; i < 6; i++) {
				if (subject[i] === "maths") {
					mathsChooser.push(i);
				};
			};

			var mathsChooserNames = names[mathsChooser[0]];
			for (var i = 1; i < mathsChooser.length; i++) {
				mathsChooserNames+=", "+names[mathsChooser[i]];

			};
			
			if(mathsChooser.length > 0){
				$(".last > p:nth-of-type(4)").text(data.string.p2s26+" "+mathsChooserNames+"}");
			}
			else{
				$(".last > p:nth-of-type(4)").text(data.string.p2s26+" }");
			}
			

			var thirteenChoser = [];
			console.log(age);

			for (var i = 1; i < 6; i++) {
				if (age[i] === "13") {
					thirteenChoser.push(i);
				};
			};

			var thirteenChoserNames = names[thirteenChoser[0]];
			for (var i = 1; i < thirteenChoser.length; i++) {
				thirteenChoserNames+=", "+names[thirteenChoser[i]];

			};

			if(thirteenChoser.length > 0){
				$(".last > p:nth-of-type(5)").text(data.string.p2s27+" "+thirteenChoserNames+"}");
			}
			else{
				$(".last > p:nth-of-type(5)").text(data.string.p2s27+" }");
			}
			
			// $("#activity-page-next-btn-enabled").show(0);
		});				

			$("#activity-page-next-btn-enabled").click (function(){
				$("#topic").hide(0);
				$("#topic2").show(0);
				$("#inst").hide(0);
				$("#info_table").hide(0);
				$("#tablebtn2").show(0);
				$(".last").show(0);
				$("#btn").hide(0);
				$("#activity-page-next-btn-enabled").hide(0);
				$("#dontleave").hide(0);

				$(".last > p:nth-of-type(1)").show(0);
				$(".last > p:nth-of-type(2)").show(0);
				$(".last > p:nth-of-type(3)").show(0);
				$(".last > p:nth-of-type(4)").show(0);
				$(".last > p:nth-of-type(5)").show(0);
				$(".last > p:nth-of-type(6)").show(0);
				ole.footerNotificationHandler.pageEndSetNotification();				
			});

			$("#tablebtn2").click (function(){
				$("#info_table").show(0);
				$("#tablebtn1").show(0);
				$("#tablebtn2").hide(0);
				$(".last").hide(0);
			});

			$("#tablebtn1").click (function(){
				$("#info_table").hide(0);
				$("#tablebtn1").hide(0);
				$("#tablebtn2").show(0);
				$(".last").show(0);
			});						
	}
	
	/*first call */
	first(countNext);

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				second();
				break;		
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
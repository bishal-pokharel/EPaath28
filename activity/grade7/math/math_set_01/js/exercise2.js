/*content object*/
var content=[
	{
		para5 : data.string.e2s1,

		para7 : data.string.e1s7,
		para8 : data.string.e1s9,
		para9 : data.string.e1s11,
		para10 : data.string.e1s13,
		para11 : data.string.e1s15,
		para12 : data.string.e1s17,
		para13 : data.string.e1s19,
		para14 : data.string.e1s21,
		para15 : data.string.e1s23,
		para16 : data.string.e1s25,
		answerdata1 : "sita",
		clickcount : 1,
	},
	{
		para5 : data.string.e2s2,

		para7 : data.string.e1s7,
		para8 : data.string.e1s9,
		para9 : data.string.e1s11,
		para10 : data.string.e1s13,
		para11 : data.string.e1s15,
		para12 : data.string.e1s17,
		para13 : data.string.e1s19,
		para14 : data.string.e1s21,
		para15 : data.string.e1s23,
		para16 : data.string.e1s25,
		answerdata1 : "roshan",
		clickcount : 1,
	},
	{
		para5 : data.string.e2s3,

		para7 : data.string.e1s7,
		para8 : data.string.e1s9,
		para9 : data.string.e1s11,
		para10 : data.string.e1s13,
		para11 : data.string.e1s15,
		para12 : data.string.e1s17,
		para13 : data.string.e1s19,
		para14 : data.string.e1s21,
		para15 : data.string.e1s23,
		para16 : data.string.e1s25,
		answerdata1 : "santu",
		clickcount : 1,
	},

	{
		para5 : data.string.e2s4,

		para7 : data.string.e1s7,
		para8 : data.string.e1s9,
		para9 : data.string.e1s11,
		para10 : data.string.e1s13,
		para11 : data.string.e1s15,
		para12 : data.string.e1s17,
		para13 : data.string.e1s19,
		para14 : data.string.e1s21,
		para15 : data.string.e1s23,
		para16 : data.string.e1s25,
		answerdata1 : "sarina",
		clickcount : 1,
	},
	{
		para5 : data.string.e2s5,

		para7 : data.string.e1s7,
		para8 : data.string.e1s9,
		para9 : data.string.e1s11,
		para10 : data.string.e1s13,
		para11 : data.string.e1s15,
		para12 : data.string.e1s17,
		para13 : data.string.e1s19,
		para14 : data.string.e1s21,
		para15 : data.string.e1s23,
		para16 : data.string.e1s25,
		answerdata1 : "rama",
		answerdata2 : "ganesh",
		clickcount : 2,
	},
	{
		para5 : data.string.e2s6,

		para7 : data.string.e1s7,
		para8 : data.string.e1s9,
		para9 : data.string.e1s11,
		para10 : data.string.e1s13,
		para11 : data.string.e1s15,
		para12 : data.string.e1s17,
		para13 : data.string.e1s19,
		para14 : data.string.e1s21,
		para15 : data.string.e1s23,
		para16 : data.string.e1s25,
		answerdata1 : "tika",
		answerdata2 : "rabi",
		clickcount : 2,
	},
	{
		para5 : data.string.e2s7,

		para7 : data.string.e1s7,
		para8 : data.string.e1s9,
		para9 : data.string.e1s11,
		para10 : data.string.e1s13,
		para11 : data.string.e1s15,
		para12 : data.string.e1s17,
		para13 : data.string.e1s19,
		para14 : data.string.e1s21,
		para15 : data.string.e1s23,
		para16 : data.string.e1s25,
		answerdata1 : "narendra",
		answerdata2 : "kayo",
		clickcount : 2,
	},
	{
		para2 : data.string.e1s2,
		para3 : data.string.e1s3,
		para4 : data.string.e1s4,

		para5 : data.string.e2s8,
		
		answerdata1 : "corn",
		
		clickcount : 3,
	},

	{
		para2 : data.string.e1s2,
		para3 : data.string.e1s3,
		para4 : data.string.e1s4,

		para5 : data.string.e2s9,

		answerdata1 : "rice",
		
		clickcount : 3,
		
	},

	{
		para2 : data.string.e1s2,
		para3 : data.string.e1s3,
		para4 : data.string.e1s4,

		para5 : data.string.e2s10,

		answerdata1 : "fafad",
		
		clickcount : 3,
		
	},
];

var smth = ole.getRandom(10,9,0);
console.log(smth);

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	
	var countNext = 0;

	var countclicks = 0;

	var $total_page = 10;
	loadTimelineProgress($total_page,countNext+1);
/*
* first
*/
	function first() {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[smth[countNext]]);
		$board.html(html);
		$nextBtn.hide(0);
		$("#correct").hide(0);
		$("#wrong").hide(0);

		$("#topic").text(data.string.e1s1);

		$("#title1").text(data.string.e1s2);
		$("#title2").text(data.string.e1s3);
		$("#title3").text(data.string.e1s4);

		$("#M > p:nth-of-type(1)").text(data.string.e1s7+",");
		$("#M > p:nth-of-type(2)").text(data.string.e1s21);
		$("#F > p:nth-of-type(1)").text(data.string.e1s15+",");
		$("#B > p:nth-of-type(1)").text(data.string.e1s17);
		$("#BnF > p:nth-of-type(1)").text(data.string.e1s9);
		$("#MnF > p:nth-of-type(1)").text(data.string.e1s19);
		$("#F > p:nth-of-type(2)").text(data.string.e1s23);
		$("#BnMnF > p:nth-of-type(1)").text(data.string.e1s11+",");
		$("#BnMnF > p:nth-of-type(2)").text(data.string.e1s25);		
		$("#BnM > p:nth-of-type(1)").text(data.string.e1s13);

		
		var answer = $board.children(".question_box").children("#options").children("p");
			// answer.css("background","green");		
		var id = answer.attr("id");
			// alert(id);	
		
		var question = $board.children(".question_box").children("p");
		// question.css("background","green");

		var attr1 = question.data('requiredanswer1');
		// alert(attr1);

		var attr2 = question.data('requiredanswer2');
		// alert(attr2);

		var condition = question.data('requiredcount');
		// alert(condition);
	
	
	if (condition == 1) {
		answer.click (function(){
			// alert("first click funtion");
			var clickeditem1 = $(this).attr("id");
			// alert(clickeditem1);
				if (clickeditem1 == attr1) {
					$(this).css('box-shadow','0px 0px 15px green');
					$("#correct").show(0);
					$("#wrong").hide(0);
					$("#activity-page-next-btn-enabled").show(0);
				}
				else {
					$("#wrong").show(0);
					$(this).css('box-shadow','0px 0px 15px red')
				}
		});	
	}

	var count = [null,null];

	function countCheckFor2 (index) {
		count[index]=1;
		var check = true;

		for (var i = 0; i < count.length; i++) {
			if(count[i]==null){
				check = false;
				break;
			}
		};
		if (check) {
			$("#correct").show(0);
			$("#activity-page-next-btn-enabled").show(0);
		};
	}

	if(condition == 2) {
		
		answer.click (function(){			
			var clickeditem1 = $(this).attr("id");
			if(clickeditem1 == attr1){
				$(this).css('box-shadow','0px 0px 15px green');
				$("#wrong").hide(0);
				countCheckFor2 (0);
			}
			else{
				$("#wrong").show(0);
				$(this).css('box-shadow','0px 0px 15px red');
			}
		});	

		answer.click (function(){
			var clickeditem2 = $(this).attr("id");
			if (clickeditem2 == attr2) {
				$(this).css('box-shadow','0px 0px 15px green');
				$("#wrong").hide(0);
				countCheckFor2 (1);
			}
		});			
	}

	// if (condition == 3) {
	// 	for (var i = 1; i <= 10; i++) {
	// 		$("#options > p:nth-of-type[i]".hide(0);
	// 	}
	// }

	if(condition == 3) {
		$("#options > p:nth-of-type(1)").hide(0);
		$("#options > p:nth-of-type(2)").hide(0);
		$("#options > p:nth-of-type(3)").hide(0);
		$("#options > p:nth-of-type(4)").hide(0);
		$("#options > p:nth-of-type(5)").hide(0);
		$("#options > p:nth-of-type(6)").hide(0);
		$("#options > p:nth-of-type(7)").hide(0);
		$("#options > p:nth-of-type(8)").hide(0);
		$("#options > p:nth-of-type(9)").hide(0);
		$("#options > p:nth-of-type(10)").hide(0);

		$("#options > p:nth-of-type(11)").show(0);
		$("#options > p:nth-of-type(12)").show(0);
		$("#options > p:nth-of-type(13)").show(0);

		answer.click (function(){			
			var clickeditem1 = $(this).attr("id");
			if (clickeditem1 == attr1) {
					$(this).css('box-shadow','0px 0px 15px green');
					$("#correct").show(0);
					$("#wrong").hide(0);
					$("#activity-page-next-btn-enabled").show(0);
				}
				else {
					$("#wrong").show(0);
					$(this).css('box-shadow','0px 0px 15px red')
				}
		});
	}
}	



	/*first call to first*/
	first();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		
		countNext++;
		switch (countNext) {
			case 1:				
				first();
				break;
			case 2:
				first();
				break;
			case 3:
				first();
				break;
			case 4:
				first();
				break;
			case 5:
				first();
				break;
			case 6:
				first();
				break;
			case 7:
				first();
				break;
			case 8:
				first();
				break;
			case 9:
				first();

				break;
			case 10:
			ole.activityComplete.finishingcall();
			break;

			default:break;
		}		
		loadTimelineProgress($total_page,countNext+1);
	});

	// $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
	// 	$(this).css("display","none");
	// 	$nextBtn.css('display', 'none');
	// 	countNext--;
	// 	switch (countNext) {
	// 		case 0:			
	// 			first();
	// 			break;
	// 		case 1:			
	// 			second();
	// 			break;
	// 		default:break;
	// 	}
	// 	loadTimelineProgress($total_page,countNext+1);
	// });

});
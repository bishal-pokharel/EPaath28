var whatPopId=0;
var whatdegree=0;
var innerCount=1;
var countPopBtn = 0;
$(function(){

	$(".headTitle").html(data.string.p1_1);
	loadTimelineProgress(1,1);

	$(".p2_01").html(data.string.p2_01);


	$("#angle_60 div").html(data.string.p2_1);
	$("#angle_30 div").html(data.string.p2_2);
	$("#angle_15 div").html(data.string.p2_3);
	$("#angle_90 div").html(data.string.p2_4);
	$("#angle_45 div").html(data.string.p2_5);
	$("#angle_135 div").html(data.string.p2_6);


	$(".popBtn").on('click',function(){
        countPopBtn++;
		var id=$(this).attr('id');
		getHtml(id);

	});


	$(".popUp").on('click','#closeImgbtn',function(){

		$(".popUp").fadeOut(100,function(){
			$(this).html('');
			innerCount=1;
		});
	});

	$(".popUp").on('click','#nextbtn',function(){

		$(this).hide(0);
		$('#nextbtn').hide(0);

		innerCount++;

		afterNext(innerCount);



	});

	$(".popUp").on('click','#prevbtn',function(){

		$(this).hide(0);
		$("#activity-page-next-btn-enabled").hide(0);

		innerCount--;

		afterNext(innerCount);

	});

});

function afterNext(innerCount)
{
	var ids="angle_"+whatdegree;

	$(".innerDesc").html(data.string["p2_"+whatPopId+"_"+innerCount]);

	if(innerCount!=6)
	{
		$(".lastBx").fadeOut(10);
		$('#video1').show(0);
		$( "#protractor" ).removeAttr('style');
		var tv_main_channel=$('#vdo1');
		var tv_main_channel2=$('#vdo2');

		var vdo_source_webm=$ref+"/video/"+ids+"/"+ids+"_"+innerCount+".webm";
		var vdo_source_mp4=$ref+"/video/"+ids+"/"+ids+"_"+innerCount+".mp4";

		tv_main_channel.attr('src', vdo_source_webm);
		tv_main_channel2.attr('src', vdo_source_mp4);

		var video_block = $('#video1');
		video_block.load();


	}
	else
	{
		$('#video1').hide(0);




		$(".lastBx").fadeIn(10,function(){

			var height=parseInt($("#elementinpop").innerHeight());
			var height2=parseInt($("#mylastimg").height());
			var newtop=((height)-height2)/2;
			$(".lastBx").css('top',newtop+"px");
		});
		$( "#protractor" ).draggable({ containment: "#elementinpop", scroll: false });
	}


	setTimeout(function(){
		if(innerCount==5)
		{

			$("#closeImgbtn").show(0);
			$("#prevbtn").show(0);
            countPopBtn==6?ole.footerNotificationHandler.lessonEndSetNotification():"";

		}
		else
		{
			$("#nextbtn").show(0);
			if(innerCount!=1)$("#nextbtn").show(0);
		}


	},2000);
}

function getdata(ids)
{
	var datas;
	var nextbtn=getSubpageMoveButton($lang,"next");
	var prevbtn=getSubpageMoveButton($lang,"prev");
	var clsbtn=getCloseBtn();

	switch(ids)
	{
		case "angle_60":
		{
			whatPopId=1;
			whatdegree=60;
			datas={
				title:data.string.p2_1,
				textdesc:data.string["p2_"+whatPopId+"_1"],
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				lastimg:$ref+"/images/page2/"+ids+"a_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}
			break;
		}
		case "angle_30":
		{
			whatPopId=2;
			whatdegree=30;
			datas={
				title:data.string.p2_2,
				textdesc: data.string["p2_"+whatPopId+"_1"],
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				lastimg:$ref+"/images/page2/"+ids+"a_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}
			break;
		}
		case "angle_15":
		{
			whatPopId=3;
			whatdegree=15;
			datas={
				title:data.string.p2_3,
				textdesc: data.string["p2_"+whatPopId+"_1"],
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				lastimg:$ref+"/images/page2/"+ids+"a_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}
			break;
		}
		case "angle_90":
		{
			whatPopId=4;
			whatdegree=90;
			datas={
				title:data.string.p2_4,
				textdesc:data.string["p2_"+whatPopId+"_1"],
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				lastimg:$ref+"/images/page2/"+ids+"a_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}
			break;
		}
		case "angle_45":
		{
			whatPopId=5;
			whatdegree=45;
			datas={
				title:data.string.p2_5,
				textdesc: data.string["p2_"+whatPopId+"_1"],
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				lastimg:$ref+"/images/page2/"+ids+"a_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}
			break;
		}
		case "angle_135":
		{
			whatPopId=6;
			whatdegree=135;
			datas={
				title:data.string.p2_6,
				textdesc: data.string["p2_"+whatPopId+"_1"],
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				lastimg:$ref+"/images/page2/"+ids+"a_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}
			break;
		}
	}

	return datas;

}
function getHtml(ids)
{
	var datas=getdata(ids);
	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(datas);
    $(".popUp").html(html).css("display","block");
    setTimeout(function(){
        		$("#nextbtn").show(0);
        	},2000)
}

$(function(){

	var $next_btn = $("#activity-page-next-btn-enabled");
	var $prev_btn = $("#activity-page-prev-btn-enabled");

	loadTimelineProgress(2, 1);
	$(".headTitle").html(data.string.p1_1);
	var sld_no = 1;
	$next_btn.show();
	$next_btn.click(function(){
		sld_no++;
		$next_btn.hide(0);
		getCases(sld_no);
	});
	$prev_btn.click(function(){
		sld_no--;
		$prev_btn.hide(0);
		getCases(sld_no);
	});

	// first slide call
	slide_1();

	function getCases(slide_num){
		loadTimelineProgress(2, slide_num);
		switch(slide_num){
			case 1:
				slide_1();
			break;
			case 2:
				slide_2();
			break;
		}
	}

	function slide_1(){
		var content={
			p_class:"cover_title",
			p_data:data.lesson.chapter,
			img_class:"coverimg",
			img_src:$ref+"/images/page1/coverpage.png"
		}
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);

		var html = template(content);
		$("#cover_page").html(html);
		$next_btn.show(0);
	}

	function slide_2(){
		$(".coverimg, .cover_title").hide(0);
		$(".p1_2").html(data.string.p1_2);


 	 	$(".p1_3").html(data.string.p1_3);

 	 	$(".p1_4").html(data.string.p1_4);

 	 	$(".p1_5").html(data.string.p1_5);

 	 	$(".p1_6").html(data.string.p1_6);

 	 	$(".p1_7").html(data.string.p1_7);
 	 $(".p1_2").delay(200).fadeIn(600,function(){

  		$(".p1_3").delay(600).fadeIn(600,function(){

  			$(".listbx").delay(600).fadeIn(600,function(){
  				ole.footerNotificationHandler.pageEndSetNotification();

  			});

  		});
  	});
	}
	$(".covertext").html(eval("data.lesson.chapter"));
})


var counter=0;
var totalCounter=10;
$(function(){


	$(".headTitle").html(data.string.e2_1);
	var nextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(nextbtn);
	$("#repeatBtn").html(getReloadBtn());

	var arrayquestion=new Array(50,5,80,160,60,10,65,35,170,180);


	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;
	getQuestion(counter, newarray);

			
	$(".wholequesbox").on('click',"#confirm",function(){

		var pattern=/^\d*$/;

        var val=$("#inputval").val();
        

        if(val=="")
        {
        	$("#inputval").val('').addClass('borderMe');
        }       
		else if(!val.match(pattern))
       	{
	       	$("#inputval").val('').addClass('borderMe');
        }
        else
        {
        	var intval=parseInt(val);
        	var dataval=parseInt($("#inputval").attr('data'));
        	if(intval==dataval)
        	{
        		$("#right").show(0);
        		rightcounter++;
				$("#activity-page-next-btn-enabled").show(0);
				$("#inputval").attr('disabled','disabled');
				$(this).hide(0);
        		$("#wrong").hide(0);
			}
        	else
        	{
        		$("#wrong").show(0);
        		wrongCounter++;
        	}

        	
        }
	});

	$("#activity-page-next-btn-enabled").click(function(){
	
		$(this).hide(0);
		counter++;

		
		if(counter<totalCounter)
		{
			getQuestion(counter, newarray);
		}
		else
		{
			getSummary(newarray,rightcounter,wrongCounter);
		}

	});

	$("#repeatBtn").click(function(){

		location.reload();
	});
	
});

function getThisData(counter, newarray)
{

	var cntme=newarray[counter];

	var imgsrc1=$ref+"/exercise/images/ang_"+cntme+".png";

	var newarr=Array(cntme,imgsrc1);

	return newarr;
}

function getQuestion(counter, newarray)
{
	loadTimelineProgress(11,counter+1);
	var dataMe=getThisData(counter, newarray);
	
	var datas={
				quesimg:data.string.p2_1,
				imgpro:$ref+"/exercise/images/p1.png",
				e2:data.string.e2_2,
				confirm:data.string.e2_3,
				dataval:dataMe[0],
				quesimg:dataMe[1]
			}	

	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(datas);

	$(".wholequesbox").fadeOut(10,function(){
		$(this).html(html);
	
	}).delay(100).fadeIn(10,function(){

		$( "#protractor" ).draggable({ containment: ".questionBox", scroll: false });
	});

}

function getSummary(newarray,rightcounter,wrongCounter)
{
	loadTimelineProgress(11,11);

	var imgArr=[];
	for(var i=0;i<10;i++)
	{

		var cntme=newarray[i];

		var imgsrc1=$ref+"/exercise/images/ang_"+cntme+".png";
		imgArr[i]={imgsrc:imgsrc1,imgdata:cntme};

	}

	var datame={imgval:imgArr,
				e2_5:data.string.e2_5,
				rt:rightcounter,
				wr:wrongCounter,
				e2_6:data.string.e2_6,
				allans:totalCounter
			};
	
	var source=$("#template-2").html();
	var template=Handlebars.compile(source);
	var html=template(datame);

	$(".headTitle").html(data.string.e2_4);
	$(".wholequesbox").fadeOut(10,function(){
		
		$(this).html(html);
	
	}).delay(100).fadeIn(10,function(){

		$("#repeatBtn").show(0);
		ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);
		
	});


}



$(function(){


	$(".headTitle").html(data.string.e1_1);
	var nextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(nextbtn);
	$("#repeatBtn").html(getReloadBtn());

	var counter=0;
	var totalCounter=6;

	var arrayquestion=new Array(1,2,3,4,5,6);

	var newarray=shuffleArray(arrayquestion);
	
	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;


	myquestion(newarray[counter],counter);

	$(".wholequesbox").on("click",".ans",function(){


		var isCorrect=parseInt($(this).attr('corr'));

		$("#optionslist div").removeClass('ans'); 

		$("#optionslist div[corr='1']").addClass('corrans'); 
		$("#optionslist div[corr='0']").addClass('wrongans'); 
		
		if(isCorrect==1)
		{
			$("#right").show(0);
			
			
			rightcounter++;
		}
		else
		{
			
			$("#wrong").show(0);
			
			wrongCounter++;
		}

		setTimeout(function(){

			var questCnt=newarray[counter];
			var imgSrcm=$ref+"/exercise/images/exe2/Q0"+questCnt+".gif";
			$(".wholequesbox").find('img').attr('src',imgSrcm);

			$(".wholequesbox").find('img').load(function(){

				setTimeout(function(){

					$("#activity-page-next-btn-enabled").show(0);
				},2500);
			});

		},500);
		

	});

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		counter++;
		
		
		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}
		if(counter > totalCounter)
		{
			ole.activityComplete.finishingcall();
		}
	});
});


function myquestion(questionNo,counter)
{
	
	var source   = $("#template-1").html();

	
	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter);

	var html=template($dataval);

	loadTimelineProgress(7,counter+1);


	$(".wholequesbox").fadeOut(10,function(){
		$(this).html(html);
		
		
	}).delay(100).fadeIn(10,function(){
		
		
	});
}



function getQuestion($quesNo,counter)
{
	var quesList;
		
	var whatri=whatCorr($quesNo);
	
	quesList={
		imgsrc:$ref+"/exercise/images/exe2/Q0"+$quesNo+".png",
		imgsrc2:$ref+"/exercise/images/p1.png",
		optObj:[
				{optObj1:"e1q"+$quesNo+"a1", yesno:whatri[0],optObjval:data.string["e1q"+$quesNo+"a1"]},
				{optObj1:"e1q"+$quesNo+"a2", yesno:whatri[1],optObjval:data.string["e1q"+$quesNo+"a2"]},
				{optObj1:"e1q"+$quesNo+"a3", yesno:whatri[2],optObjval:data.string["e1q"+$quesNo+"a3"]},
				{optObj1:"e1q"+$quesNo+"a4", yesno:whatri[3],optObjval:data.string["e1q"+$quesNo+"a4"]}
				]
	}
	

	return quesList;
}


function whatCorr($quesNo)
{
	var whatis=new Array();
	switch($quesNo)
	{
		case 1: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0;	whatis[3]=0; 	break;
		case 2: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0;	whatis[3]=0;  	break;
		case 3: 	whatis[0]=0;	whatis[1]=0;	whatis[2]=0;	whatis[3]=1;   	break;
		case 4: 	whatis[0]=0;	whatis[1]=0;	whatis[2]=1;	whatis[3]=0;  	break;
		case 5: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0;	whatis[3]=0; 	break;
		case 6: 	whatis[0]=0;	whatis[1]=1;	whatis[2]=0;	whatis[3]=0;  	break;
		
	}
	return whatis;
}

function myquestion2(right,wrong)
{
	loadTimelineProgress(7,7);
	var source   = $("#template-2").html();
	
	var template = Handlebars.compile(source);
	
	var $dataval={
		e2_5:data.string.e2_5,
		e2_6:data.string.e2_6,
		rt:right,
		wr:wrong,
		allans:6,
		imgval:[
			{imgsrc:$ref+"/exercise/images/exe2/r1.png", imgdata: data.string.e1q1a2,imgid:"ques_1"},
			{imgsrc:$ref+"/exercise/images/exe2/r2.png", imgdata: data.string.e1q2a2,imgid:"ques_2"},
			{imgsrc:$ref+"/exercise/images/exe2/r3.png", imgdata: data.string.e1q3a4,imgid:"ques_3"},
			{imgsrc:$ref+"/exercise/images/exe2/r4.png", imgdata: data.string.e1q4a3,imgid:"ques_4"},
			{imgsrc:$ref+"/exercise/images/exe2/r5.png", imgdata: data.string.e1q5a2,imgid:"ques_5"},
			{imgsrc:$ref+"/exercise/images/exe2/r6.png", imgdata: data.string.e1q6a2,imgid:"ques_6"}
			
		]


	}
	var html=template($dataval);

	$(".wholequesbox").fadeOut(10,function(){
		$(this).html(html);
		$(".headTitle").html(data.string.e2_4);
		
	}).delay(100).fadeIn(10,function(){

		$("#activity-page-next-btn-enabled").delay(100).fadeIn(10);
		
	});
}


var counter=0;
var totalCounter=10;
var ver=0;
var ancver=0;

var cnt1=0,cnt2=0;
var cid1,cid2;
var rightcounter=0, wrongCounter=0;

$(function(){


	$(".headTitle").html(data.string.exeTitle);

	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);	

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);

	var newarray=arrayquestion;//shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	
	getQuestion(counter, newarray);
	

	$("#activity-page-next-btn-enabled").click(function(){
	
		$(this).hide(0);
		counter++;

		
		if(counter<totalCounter)
		{
			getQuestion(counter, newarray);
		}
		else
		{	
			getSummary(rightcounter,wrongCounter);
		}

		if(counter > totalCounter){
			ole.activityComplete.finishingcall();
		}

	});
	
});

function getSummary(rightcounter,wrongCounter)
{
	loadTimelineProgress(11,11);
	$(".headTitle").html(data.string.q1_10).addClass("sumcls");
	

	var html="<div class='summarybx'>"+
			"<div class='rtbx'>"+data.string.q1_11+" : "+rightcounter+"</div>"+
			"<div class='rtbx'>"+data.string.q1_12+" : "+wrongCounter+"</div>"+
			"</div>";
	
	$(".questionBx").fadeOut(10,function(){
		$(this).html(html);
	
	}).delay(100).fadeIn(10,function(){

		$("#activity-page-next-btn-enabled").show(0);
	});

}

function getQuestion(counter, newarray)
{

	loadTimelineProgress(11,counter+1);
	var datas={
				question:data.string['l1q'+newarray[counter]],
				imgpro:$ref+"/exercise/images/p1.png",
				hintbx:data.string['q1a'+newarray[counter]]
		};

	var source;
	cnt1=0;
	cnt2=0;
	
	ver=0;
	cid1=0,cid2=0;

	if(newarray[counter]<=8)
	{
		ancver=2;
		
	} 
	else if(newarray[counter]>=9)   
	{
		ancver=1;
		
	}
	
	source=$("#template-1").html();
	
	var template=Handlebars.compile(source);
	var html=template(datas);

	$(".questionBx").fadeOut(10,function(){
		$(this).html(html);
	
	}).delay(100).fadeIn(10,function(){

		if(newarray[counter]<=7)
		{
			animatefunc(counter, newarray);
		}
		else if(newarray[counter]==8)
		{
			animatefunc2(counter, newarray);
		}
		else if(newarray[counter]==9)
		{
			animatefunc2(counter, newarray);
		}
		else if(newarray[counter]==10)
		{
			animatefunc3(counter, newarray);
		}
			
	});

}

function animatefunc(counter, newarray)
{
	var snaps = Snap("#questionimg");
	var nowVal=newarray[counter];
	
		Snap.load($ref+"/exercise/images/1.svg", function(f)
		{
			onSVGLoaded(f);
		});
	
	

			
	function onSVGLoaded(f)
	{
		
		var cls_1 = f.select("#cls_1"),
			cls_2 = f.select("#cls_2"),
			cls_3 = f.select("#cls_3"),
			g = f.select("g");

		
			var	cls_4 = f.select("#cls_4"),
				cls_5 = f.select("#cls_5"),
				cls_6 = f.select("#cls_6"),
				cls_7 = f.select("#cls_7"),
				cls_8 = f.select("#cls_8");
		
		
		
		
		cls_1.click(function() {

			if (!cls_1.hasClass('clicked')) {
				ver++;

				if (ver < ancver) {
					cls_1.removeClass('class1').addClass('clicked');
					clickFunc("cls_1", counter, newarray);

				} else if (ver == ancver) {
					cls_1.removeClass('class1').addClass('clicked');
					cls_2.removeClass('class1');
					cls_3.removeClass('class1');
					cls_4.removeClass('class1');
					cls_5.removeClass('class1');
					cls_6.removeClass('class1');
					cls_7.removeClass('class1');
					cls_8.removeClass('class1');

					clickFunc("cls_1", counter, newarray);
				}
			}

		}); 


		
		cls_2.click(function() {
			if (!cls_2.hasClass('clicked')) {
				ver++;

				if (ver < ancver) {
					cls_2.removeClass('class1').addClass('clicked');
					clickFunc("cls_2", counter, newarray);

				} else if (ver == ancver) {
					cls_2.removeClass('class1').addClass('clicked');
					cls_1.removeClass('class1');
					cls_3.removeClass('class1');

					cls_4.removeClass('class1');
					cls_5.removeClass('class1');
					cls_6.removeClass('class1');
					cls_7.removeClass('class1');
					cls_8.removeClass('class1');

					clickFunc("cls_2", counter, newarray);

				}
			}

		}); 


		
		cls_3.click(function() {
			if (!cls_3.hasClass('clicked')) {
				ver++;

				if (ver < ancver) {
					cls_3.removeClass('class1').addClass('clicked');
					clickFunc("cls_3", counter, newarray);

				} else if (ver == ancver) {
					cls_3.removeClass('class1').addClass('clicked');
					cls_1.removeClass('class1');
					cls_2.removeClass('class1');

					cls_4.removeClass('class1');
					cls_5.removeClass('class1');
					cls_6.removeClass('class1');
					cls_7.removeClass('class1');
					cls_8.removeClass('class1');

					clickFunc("cls_3", counter, newarray);
				}
			}

		}); 


		if(nowVal<=7)
		{
		
			cls_4.click(function() {
				if (!cls_4.hasClass('clicked')) {
					ver++;
					if (ver < ancver) {
						cls_4.removeClass('class1').addClass('clicked');
						clickFunc("cls_4", counter, newarray);

					} else if (ver == ancver) {
						cls_4.removeClass('class1').addClass('clicked');
						cls_1.removeClass('class1');
						cls_2.removeClass('class1');
						cls_3.removeClass('class1');
						cls_5.removeClass('class1');
						cls_6.removeClass('class1');
						cls_7.removeClass('class1');
						cls_8.removeClass('class1');
						clickFunc("cls_4", counter, newarray);
					}

				}
			}); 



			
			cls_5.click(function() {
				if (!cls_5.hasClass('clicked')) {
					ver++;
					if (ver < ancver) {
						cls_5.removeClass('class1').addClass('clicked');
						clickFunc("cls_5", counter, newarray);

					} else if (ver == ancver) {
						cls_5.removeClass('class1').addClass('clicked');
						cls_1.removeClass('class1');
						cls_2.removeClass('class1');
						cls_3.removeClass('class1');
						cls_4.removeClass('class1');
						cls_6.removeClass('class1');
						cls_7.removeClass('class1');
						cls_8.removeClass('class1');
						clickFunc("cls_5", counter, newarray);
					}
				}

			}); 


			
			cls_6.click(function() {
				if (!cls_6.hasClass('clicked')) {
					ver++;
					if (ver < ancver) {
						cls_6.removeClass('class1').addClass('clicked');
						clickFunc("cls_6", counter, newarray);

					} else if (ver == ancver) {
						cls_6.removeClass('class1').addClass('clicked');
						cls_1.removeClass('class1');
						cls_2.removeClass('class1');
						cls_3.removeClass('class1');
						cls_5.removeClass('class1');
						cls_4.removeClass('class1');
						cls_7.removeClass('class1');
						cls_8.removeClass('class1');
						clickFunc("cls_6", counter, newarray);

					}
				}
			}); 


			
			cls_7.click(function() {
				if (!cls_7.hasClass('clicked')) {
					ver++;
					if (ver < ancver) {
						cls_7.removeClass('class1').addClass('clicked');
						clickFunc("cls_7", counter, newarray);

					} else if (ver == ancver) {
						cls_7.removeClass('class1').addClass('clicked');
						cls_1.removeClass('class1');
						cls_2.removeClass('class1');
						cls_3.removeClass('class1');
						cls_5.removeClass('class1');
						cls_6.removeClass('class1');
						cls_4.removeClass('class1');
						cls_8.removeClass('class1');

						clickFunc("cls_7", counter, newarray);
					}
				}
			}); 


			
			cls_8.click(function() {
				if (!cls_8.hasClass('clicked')) {
					ver++;
					if (ver < ancver) {
						cls_8.removeClass('class1').addClass('clicked');
						clickFunc("cls_8", counter, newarray);

					} else if (ver == ancver) {
						cls_8.removeClass('class1').addClass('clicked');
						cls_1.removeClass('class1');
						cls_2.removeClass('class1');
						cls_3.removeClass('class1');
						cls_5.removeClass('class1');
						cls_6.removeClass('class1');
						cls_7.removeClass('class1');
						cls_4.removeClass('class1');
						clickFunc("cls_8", counter, newarray);
					}
				}
			}); 

		}
		snaps.append(f);
	}


		
}

function animatefunc2(counter, newarray)
{
	var snaps = Snap("#questionimg");
	var nowVal=newarray[counter];
	
	Snap.load($ref+"/exercise/images/2.svg", function(f)
	{
		onSVGLoaded(f);
	});
	
	function onSVGLoaded(f)
	{
		
		var cls_1 = f.select("#cls_1"),
			cls_2 = f.select("#cls_2"),
			cls_3 = f.select("#cls_3"),
			g = f.select("g");

		
		
		cls_1.click(function () {
			ver++;
			
			if(ver<ancver)
			{	
				cls_1.removeClass('class1').addClass('clicked');
				clickFunc("cls_1",counter, newarray);
				
			}
			else if(ver==ancver)
			{
				cls_1.removeClass('class1').addClass('clicked');
				cls_2.removeClass('class1');
				cls_3.removeClass('class1');
				clickFunc("cls_1",counter, newarray);
			}
		});

		cls_2.click(function () {
			ver++;

			if(ver<ancver)
			{	
				cls_2.removeClass('class1').addClass('clicked');
				clickFunc("cls_2",counter, newarray);
				
			}
			else if(ver==ancver)
			{
				cls_2.removeClass('class1').addClass('clicked');
				cls_1.removeClass('class1');
				cls_3.removeClass('class1');
				
				clickFunc("cls_2",counter, newarray);
			}				
		});

		cls_3.click(function () {
			ver++;
			
			if(ver<ancver)
			{	cls_3.removeClass('class1').addClass('clicked');
				clickFunc("cls_3",counter, newarray);
				
			}
			else if(ver==ancver)
			{
				cls_3.removeClass('class1').addClass('clicked');
				cls_1.removeClass('class1');
				cls_2.removeClass('class1');
				

				clickFunc("cls_3",counter, newarray);
			}
		});

		
		snaps.append(f);
	}


		
}
function animatefunc3(counter, newarray)
{
	var snaps = Snap("#questionimg");
	var nowVal=newarray[counter];
	
	Snap.load($ref+"/exercise/images/3.svg", function(f)
	{
		onSVGLoaded(f);
	});
	
	function onSVGLoaded(f)
	{
		
		var cls_1 = f.select("#cls_1"),
			cls_2 = f.select("#cls_2"),
			g = f.select("g");

		
		
		cls_1.click(function () {
			ver++;
			
			if(ver<ancver)
			{	
				cls_1.removeClass('class1').addClass('clicked');
				clickFunc("cls_1",counter, newarray);
				
			}
			else if(ver==ancver)
			{
				cls_1.removeClass('class1').addClass('clicked');
				cls_2.removeClass('class1');
				
				clickFunc("cls_1",counter, newarray);
			}
		});

		cls_2.click(function () {
			ver++;

			if(ver<ancver)
			{	
				cls_2.removeClass('class1').addClass('clicked');
				clickFunc("cls_2",counter, newarray);
				
			}
			else if(ver==ancver)
			{
				cls_2.removeClass('class1').addClass('clicked');
				cls_1.removeClass('class1');
				
				
				clickFunc("cls_2",counter, newarray);
			}				
		});

		

		
		snaps.append(f);
	}


		
}

function clickFunc(clickId, counter, newarray)
{

	var cntme=newarray[counter];
		
	if(cntme<=7)
	{
		
		if(ver<ancver)
		{
			var ss=clickId.split("_");
			cid1=ss[1]; 
			cnt1=1;
		} 
		else if(ver==ancver)
		{
			var ss=clickId.split("_");
			cid2=ss[1]; 
			cnt2=1;
		}

		
		if(cnt1==1 && cnt2==1)
		{
			
			switch(cntme)
			{
				case 1:
				{
					
					if(
					   	   (cid1==1 && cid2==4)
						|| (cid1==4 && cid2==1)
						|| (cid1==2 && cid2==3)
						|| (cid1==3 && cid2==2)
						|| (cid1==5 && cid2==8)
						|| (cid1==8 && cid2==5)
						|| (cid1==6 && cid2==7)
						|| (cid1==7 && cid2==6)
					   ){

						$("#correct").show(0);
						rightcounter++;
					}
					else
					{
						$("#incorrect").show(0);
						wrongCounter++;
					}
					
					break;
				}
				case 2:
				case 3:
				{
					if(	   (cid1==1 && cid2==2)
						|| (cid1==2 && cid2==1)
						|| (cid1==3 && cid2==4)
						|| (cid1==4 && cid2==3)
						|| (cid1==1 && cid2==3) 
						|| (cid1==3 && cid2==1) 
						|| (cid1==2 && cid2==4)
						|| (cid1==4 && cid2==2)
					  ){

						$("#correct").show(0);
						rightcounter++;
					}
					else if(  
							   (cid1==5 && cid2==6)
							|| (cid1==6 && cid2==5)
							|| (cid1==7 && cid2==8)
							|| (cid1==8 && cid2==7)
							|| (cid1==5 && cid2==7) 
							|| (cid1==7 && cid2==5) 
							|| (cid1==6 && cid2==8)
							|| (cid1==8 && cid2==6)
						   )
					{

						$("#correct").show(0);
						rightcounter++;
					}
					else
					{
						$("#incorrect").show(0);
						wrongCounter++;
					}
					
					break;
				}
				case 4:
				{
					if(
						   (cid1==3 && cid2==6)
						|| (cid1==6 && cid2==3)
						|| (cid1==4 && cid2==5)
						|| (cid1==5 && cid2==4)
					  ){

						$("#correct").show(0);
						rightcounter++;
					}
					else
					{
						$("#incorrect").show(0);
						wrongCounter++;
					}
					
					break;
				}
				case 5:
				{
					if(
						   (cid1==1 && cid2==5)
						|| (cid1==5 && cid2==1)
						|| (cid1==3 && cid2==7)
						|| (cid1==7 && cid2==3)
						|| (cid1==2 && cid2==6)
						|| (cid1==6 && cid2==2)
						|| (cid1==4 && cid2==8)
						|| (cid1==8 && cid2==4)
					){

						$("#correct").show(0);
						rightcounter++;
					}
					else
					{
						$("#incorrect").show(0);
						wrongCounter++;
					}
					
					break;
				}
				case 6:
				{
					if(
						   (cid1==3 && cid2==5)
						|| (cid1==5 && cid2==3)
						|| (cid1==4 && cid2==6)
						|| (cid1==6 && cid2==4)
					){
						rightcounter++;
						$("#correct").show(0);
					}
					else
					{
						$("#incorrect").show(0);
						wrongCounter++;
					}
					
					break;
				}
				case 7:
				{
					if(
						   (cid1==3 && cid2==4)
						|| (cid1==3 && cid2==5)
						|| (cid1==3 && cid2==6)
						|| (cid1==5 && cid2==3)
						|| (cid1==5 && cid2==4)
						|| (cid1==5 && cid2==6)
						|| (cid1==4 && cid2==3)
						|| (cid1==4 && cid2==5)
						|| (cid1==4 && cid2==6)
						|| (cid1==6 && cid2==3)
						|| (cid1==6 && cid2==4)
						|| (cid1==6 && cid2==5)
					){
						rightcounter++;
						$("#correct").show(0);
					}
					else
					{
						$("#incorrect").show(0);
						wrongCounter++;
					}
					
					break;
				}
			}
			$(".hintBx").show(0);
			$("#activity-page-next-btn-enabled").show(0);
		}
	}
	else if(cntme==8)
	{
		
		
		if(ver<ancver)
		{
			var ss=clickId.split("_");
			cid1=ss[1]; 
			cnt1=1;
		} 
		else if(ver==ancver)
		{
			var ss=clickId.split("_");
			cid2=ss[1]; 
			cnt2=1;
		}
		
		if(cnt1==1 && cnt2==1)
		{
			if(cid1==2 && cid2==3 || cid1==3 && cid2==2)
			{
				rightcounter++;
				$("#correct").show(0);
			}
			else
			{
				$("#incorrect").show(0);
				wrongCounter++;
			}
			$(".hintBx").show(0);
			$("#activity-page-next-btn-enabled").show(0);	
		}
	}
	else if(cntme==9)
	{
		if(ver==ancver)
		{
			var ss=clickId.split("_");
			cid1=ss[1]; 
			if(cid1==1)
			{
				rightcounter++;
				$("#correct").show(0);
			}
			else
			{
				$("#incorrect").show(0);
				wrongCounter++;
			}
			$(".hintBx").show(0);
			$("#activity-page-next-btn-enabled").show(0);	
		}
	}
	else if(cntme==10)
	{
		if(ver==ancver)
		{
			var ss=clickId.split("_");
			cid1=ss[1]; 
			if(cid1==2)
			{
				rightcounter++;
				$("#correct").show(0);
			}
			else
			{
				$("#incorrect").show(0);
				wrongCounter++;
			}
			$(".hintBx").show(0);
			$("#activity-page-next-btn-enabled").show(0);	
		}
	}

	
}

$(function(){

	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	var whatnextBtn2=getSubpageMoveButton($lang,"prev");
	$('#activity-page-prev-btn-enabled').html(whatnextBtn2);

	var counter=1;
	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		$('#activity-page-prev-btn-enabled').hide(0);
		counter++;
		getHtml(counter);
	});

	$('#activity-page-prev-btn-enabled').click(function(){
		$(this).hide(0);
		$("#activity-page-next-btn-enabled").hide(0);
		counter--;
		getHtml(counter);
		ole.footerNotificationHandler.hideNotification();
	});

})


function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={
	 				headtitleText : data.lesson.chapter,
					head_additional_class:"cover_title",
		 			idme:"classme",
					image_additinoal_class:"coverpage",
		 			img1:$ref+"/images/page1/angles_bg.png",
		 			imgs1:"img8",
				};
				$(".imgdesc").css("display", "none");
	 		break;
	 	}
		case 2:
	 	{
	 		datas={
	 				headtitleText : data.string.p1angle,
	 				inttxt:data.string.s1angledefinition,
		 			idme:"classme",
		 			img1:$ref+"/images/page1/angle.png",
		 			imgs1:"img8",
		 			imgdesc:data.string.s1anglexample,
				};
	 		break;
	 	}
	 	case 3:
	 	{
	 		datas={
	 				headtitleText : data.string.p1option2,
	 				inttxt:data.string.s1adjecent,
		 			idme:"classme",
		 			img1:$ref+"/images/page1/adjecent.png",
		 			imgs1:"img8",
		 			imgdesc:data.string.s1adjecentexample,
				};
	 		break;

	 	}
	 	case 4:
	 	{
	 		datas={
	 				headtitleText : data.string.p1option9,
	 				inttxt:data.string.s1complementry,
		 			idme:"classme",
		 			img1:$ref+"/images/page1/complementary.png",
		 			imgs1:"img8",
		 			imgdesc:data.string.s1complementryexample,
				};
	 		break;
	 	}
	 	case 5:
	 	{
	 		datas={
	 				headtitleText : data.string.p1option4,
	 				inttxt:data.string.s1supplementary,
		 			idme:"classme",
		 			img1:$ref+"/images/page1/supplementary.png",
		 			imgs1:"img8",
		 			imgdesc:data.string.s1supplementaryexample
				};
	 		break;
	 	}
	}

	return datas;
}

function getHtml(cnt)
{
	var datas=getdata(cnt);
	var source;
	loadTimelineProgress(5,cnt);

	source=$("#template-2").html();

	var template=Handlebars.compile(source);
	var html=template(datas);


	$("#figBox").fadeOut(10,function(){
		$(this).html(html);
	}).delay(10).fadeIn(10,function(){
		/*switch below is to highlight words in definition*/
		switch(cnt){
			case 1 : ole.parseToolTip("div#figBox > div.classme",data.string.s1angledefinition_highlight);break;
			case 2 : ole.parseToolTip("div#figBox > div.classme",data.string.s1adjecent_highlight);break;
			case 3 : ole.parseToolTip("div#figBox > div.classme",data.string.s1complementry_highlight);break;
			case 4 : ole.parseToolTip("div#figBox > div.classme",data.string.s1supplementary_highlight);break;
			case 5 : ole.parseToolTip("div#figBox > div.classme",data.string.s1supplementary_highlight);break;
			default : break;
		}
		if(cnt>1)
		{
			$('#activity-page-prev-btn-enabled').show(0);
		}

		if(cnt<5)
		{
			$("#activity-page-next-btn-enabled").show(0);
		}
		if(cnt==5)
		{
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});
}

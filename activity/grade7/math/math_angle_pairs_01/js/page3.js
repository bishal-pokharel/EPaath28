/*variable to abbreviate for the css animation and transition end test */
var animationEndTestVar = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
var tranistionEndTestVar = 'webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend';
// path to image
var pathToImage = $ref+"/images/page3/";
var pathToThreeLinesCrossImage = pathToImage+"/threeLinesCross/";
var pathToCommonBeginAnglePairImage = pathToImage+"angle-pairs-common-begin-image.png";
var pathToExteriorAnglesImage = pathToImage+"/exterior/";
var pathToInteriorAnglesImage = pathToImage+"/interior/";
var pathToCointeriorAnglesImage = pathToImage+"/coInterior/";
var pathToAlternateAnglesImage = pathToImage+"/alternate/";
var pathToCorrespondingAnglesImage = pathToImage+"/corresponding/";
var pathToSupplementaryAnglesImage = pathToImage+"/adjecent/";
var pathToAdjecentAnglesImage = pathToImage+"/adjecent/";
var pathToVerticallyOppAnglesImage = pathToImage+"/verticallyOpposite/";

// contents for each slide
var slideContent=[
	{
		threelinescrossQuestionData : data.string.s3threelinescrossQuestion,
		threelinescrossImgSrc : pathToThreeLinesCrossImage+"01.png",
		threelinescrossExplainData1 : data.string.s3threelinescrossExplain1,
		threelinescrossExplainData2 : data.string.s3threelinescrossExplain2,
	},
	{
		angleTitleData : data.string.p1option8,
		angleDefinitionData : data.string.s3exterior,
		definitionhighlightData : data.string.s3exterior_highlight,
		angleImgSrc : pathToCommonBeginAnglePairImage,
		angleExampleData : data.string.s3exterior_example,
		angleExampleInstruction : data.string.anglehoverinstruction1,
		anglepairs : [
				{
					anglepairid : "exterior1",
					anglepairData : data.string.s3exterior1,
					pairImgSrc : pathToExteriorAnglesImage+"02.png",
				},
				{
					anglepairid : "exterior2",
					anglepairData : data.string.s3exterior2,
					pairImgSrc : pathToExteriorAnglesImage+"03.png",
				},
				{
					anglepairid : "exterior3",
					anglepairData : data.string.s3exterior3,
					pairImgSrc : pathToExteriorAnglesImage+"04.png",
				},
				{
					anglepairid : "exterior4",
					anglepairData : data.string.s3exterior4,
					pairImgSrc : pathToExteriorAnglesImage+"05.png",
				}
		],
	},
	{
		angleTitleData : data.string.p1option7,
		angleDefinitionData : data.string.s3interior,
		definitionhighlightData : data.string.s3interior_highlight,
		angleImgSrc : pathToCommonBeginAnglePairImage,
		angleExampleData : data.string.s3interior_example,
		angleExampleInstruction : data.string.anglehoverinstruction1,
		anglepairs : [
				{
					anglepairid : "interior1",
					anglepairData : data.string.s3interior1,
					pairImgSrc : pathToInteriorAnglesImage+"02.png",
				},
				{
					anglepairid : "interior2",
					anglepairData : data.string.s3interior2,
					pairImgSrc : pathToInteriorAnglesImage+"03.png",
				},
				{
					anglepairid : "interior3",
					anglepairData : data.string.s3interior3,
					pairImgSrc : pathToInteriorAnglesImage+"04.png",
				},
				{
					anglepairid : "interior4",
					anglepairData : data.string.s3interior4,
					pairImgSrc : pathToInteriorAnglesImage+"05.png",
				}
		],
	},
	{
		angleTitleData : data.string.p1option6,
		angleDefinitionData : data.string.s3coInterior,
		definitionhighlightData : data.string.s3coInterior_highlight,
		angleImgSrc : pathToCommonBeginAnglePairImage,
		angleExampleData : data.string.s3coInterior_example,
		angleExampleInstruction : data.string.anglehoverinstruction2,
		anglepairs : [
				{
					anglepairid : "cointerior25",
					anglepairData : data.string.s3coInterior_pair25,
					pairImgSrc : pathToCointeriorAnglesImage+"pair25.png",
				},
				{
					anglepairid : "cointerior38",
					anglepairData : data.string.s3coInterior_pair38,
					pairImgSrc : pathToCointeriorAnglesImage+"pair38.png",
				},
		],
	},
	{
		angleTitleData : data.string.p1option3,
		angleDefinitionData : data.string.s3alternate,
		definitionhighlightData : data.string.s3alternate_highlight,
		angleImgSrc : pathToCommonBeginAnglePairImage,
		angleExampleData : data.string.s3alternate_example,
		angleExampleInstruction : data.string.anglehoverinstruction2,
		anglepairs : [
				{
					anglepairid : "alternate28",
					anglepairData : data.string.s3alternate_pair28,
					pairImgSrc : pathToAlternateAnglesImage+"pair28.png",
				},
				{
					anglepairid : "alternate35",
					anglepairData : data.string.s3alternate_pair35,
					pairImgSrc : pathToAlternateAnglesImage+"pair35.png",
				},
		],
	},
	{
		angleTitleData : data.string.p1option5,
		angleDefinitionData : data.string.s3corresponding,
		definitionhighlightData : data.string.s3corresponding_highlight,
		angleImgSrc : pathToCommonBeginAnglePairImage,
		angleExampleData : data.string.s3corresponding_example,
		angleExampleInstruction : data.string.anglehoverinstruction2,
		anglepairs : [
				{
					anglepairid : "corresponding15",
					anglepairData : data.string.s3corresponding_pair15,
					pairImgSrc : pathToCorrespondingAnglesImage+"pair15.png",
				},
				{
					anglepairid : "corresponding26",
					anglepairData : data.string.s3corresponding_pair26,
					pairImgSrc : pathToCorrespondingAnglesImage+"pair26.png",
				},
				{
					anglepairid : "corresponding37",
					anglepairData : data.string.s3corresponding_pair37,
					pairImgSrc : pathToCorrespondingAnglesImage+"pair37.png",
				},
				{
					anglepairid : "corresponding48",
					anglepairData : data.string.s3corresponding_pair48,
					pairImgSrc : pathToCorrespondingAnglesImage+"pair48.png",
				},
		],
	},
	{
		transitionData : data.string.s3transitionText,
	},
	{
		angleTitleData : data.string.p1option2,
		angleDefinitionData : data.string.s3adjacent,
		definitionhighlightData : data.string.s3adjecent_highlight,
		angleImgSrc : pathToCommonBeginAnglePairImage,
		angleExampleData : data.string.s3adjecent_example,
		angleExampleInstruction : data.string.anglehoverinstruction2,
		anglepairs : [
				{
					anglepairid : "adjecent12",
					anglepairData : data.string.s3adjecent_pair12,
					pairImgSrc : pathToAdjecentAnglesImage+"pair12.png",
				},
				{
					anglepairid : "adjecent23",
					anglepairData : data.string.s3adjecent_pair23,
					pairImgSrc : pathToAdjecentAnglesImage+"pair23.png",
				},
				{
					anglepairid : "adjecent34",
					anglepairData : data.string.s3adjecent_pair34,
					pairImgSrc : pathToAdjecentAnglesImage+"pair34.png",
				},
				{
					anglepairid : "adjecent14",
					anglepairData : data.string.s3adjecent_pair14,
					pairImgSrc : pathToAdjecentAnglesImage+"pair14.png",
				},
				{
					anglepairid : "adjecent56",
					anglepairData : data.string.s3adjecent_pair56,
					pairImgSrc : pathToAdjecentAnglesImage+"pair56.png",
				},
				{
					anglepairid : "adjecent67",
					anglepairData : data.string.s3adjecent_pair67,
					pairImgSrc : pathToAdjecentAnglesImage+"pair67.png",
				},
				{
					anglepairid : "adjecent78",
					anglepairData : data.string.s3adjecent_pair78,
					pairImgSrc : pathToAdjecentAnglesImage+"pair78.png",
				},
				{
					anglepairid : "adjecent58",
					anglepairData : data.string.s3adjecent_pair58,
					pairImgSrc : pathToAdjecentAnglesImage+"pair58.png",
				},
		],
	},
	{
		angleTitleData : data.string.p1option4,
		angleDefinitionData : data.string.s3supplementary,
		definitionhighlightData : data.string.s3supplementary_highlight,
		angleImgSrc : pathToCommonBeginAnglePairImage,
		angleExampleData : data.string.s3supplementary_example,
		angleExampleInstruction : data.string.anglehoverinstruction2,
		anglepairs : [
				{
					anglepairid : "supplementary12",
					anglepairData : data.string.s3supplementary_pair12,
					pairImgSrc : pathToSupplementaryAnglesImage+"pair12.png",
				},
				{
					anglepairid : "supplementary23",
					anglepairData : data.string.s3supplementary_pair23,
					pairImgSrc : pathToSupplementaryAnglesImage+"pair23.png",
				},
				{
					anglepairid : "supplementary34",
					anglepairData : data.string.s3supplementary_pair34,
					pairImgSrc : pathToSupplementaryAnglesImage+"pair34.png",
				},
				{
					anglepairid : "supplementary14",
					anglepairData : data.string.s3supplementary_pair14,
					pairImgSrc : pathToSupplementaryAnglesImage+"pair14.png",
				},
				{
					anglepairid : "supplementary56",
					anglepairData : data.string.s3supplementary_pair56,
					pairImgSrc : pathToSupplementaryAnglesImage+"pair56.png",
				},
				{
					anglepairid : "supplementary67",
					anglepairData : data.string.s3supplementary_pair67,
					pairImgSrc : pathToSupplementaryAnglesImage+"pair67.png",
				},
				{
					anglepairid : "supplementary78",
					anglepairData : data.string.s3supplementary_pair78,
					pairImgSrc : pathToSupplementaryAnglesImage+"pair78.png",
				},
				{
					anglepairid : "supplementary58",
					anglepairData : data.string.s3supplementary_pair58,
					pairImgSrc : pathToSupplementaryAnglesImage+"pair58.png",
				},
		],
	},
	{
		angleTitleData : data.string.p1option1,
		angleDefinitionData : data.string.s3verticallyOpp,
		definitionhighlightData : data.string.s3verticallyOpp_highlight,
		angleImgSrc : pathToCommonBeginAnglePairImage,
		angleExampleData : data.string.s3verticallyOpp_example,
		angleExampleInstruction : data.string.anglehoverinstruction2,
		anglepairs : [
				{
					anglepairid : "s3verticallyOpp12",
					anglepairData : data.string.s3verticallyOpp_pair13,
					pairImgSrc : pathToVerticallyOppAnglesImage+"pair13.png",
				},
				{
					anglepairid : "s3verticallyOpp23",
					anglepairData : data.string.s3verticallyOpp_pair24,
					pairImgSrc : pathToVerticallyOppAnglesImage+"pair24.png",
				},
				{
					anglepairid : "s3verticallyOpp34",
					anglepairData : data.string.s3verticallyOpp_pair57,
					pairImgSrc : pathToVerticallyOppAnglesImage+"pair57.png",
				},
				{
					anglepairid : "s3verticallyOpp14",
					anglepairData : data.string.s3verticallyOpp_pair68,
					pairImgSrc : pathToVerticallyOppAnglesImage+"pair68.png",
				},
		],
	},
];

$(function () {
/*******************************************************
* reusable code below, the variables below till loadtimeline can be common
**************************************************************************/
	var $board = $('.board');
	var $nextSlideBtn = $('#activity-page-next-btn-enabled');
	var $prevSlideBtn = $('#activity-page-prev-btn-enabled');
	$nextSlideBtn.show(0);
	/*number of slides are from 0 to total_slide*/
	var slideNumber = 0; /*count for slideNumber initially 0*/
	var total_slide = 10;
	loadTimelineProgress(total_slide,slideNumber+1);

/*************************
* page specific code starts here
***********************************/

/*******
* threelinescross
*************/
	function threelinescross() {
		var source = $("#threelinescross-template").html();
		var template = Handlebars.compile(source);
		var html = template(slideContent[slideNumber]);
		$board.html(html);

		var $threelinescross = $board.children('div.threelinescross');
		var $threelinescrossQuestion = $threelinescross.children('p.threelinescrossQuestion');
		var $threelinescrossImage = $threelinescross.children('img.threelinescrossImage');
		var $threelinescrossExpalin = $threelinescross.children('div.threelinescrossExpalin');
		var $threelinescrossExpalinList1 = $threelinescrossExpalin.children('li:nth-of-type(1)');
		var $threelinescrossExpalinList2 = $threelinescrossExpalin.children('li:nth-of-type(2)');
		var $nextStepBtn = $("#activity-page-next-btn-enabled");
		$nextStepBtn.show(0);
		var stepcount = 0;
		var animImageOrder = ["02.png","03.png","04.png",
								"05.png","06.png","07.png",
								"08.png","09.png"];
		var animImagecount = 0;
		var animTotalImages = animImageOrder.length;
		var animationTimeInterval = 500;
		var setintervalvar;
		var changeToImage;

		/*animation angle function*/
		function angleAnimFunc(){
			setintervalvar = setInterval(function()
			{
			 	changeToImage = pathToThreeLinesCrossImage+animImageOrder[animImagecount++];
			 	$threelinescrossImage.attr("src",changeToImage);
			 	if(animImagecount == animTotalImages){
			 		clearInterval(setintervalvar);/*stop animation here*/
			 		$nextStepBtn.show(0);
			 	}

			}, animationTimeInterval);
		}

		$nextStepBtn.on('click', function() {
			$(this).css('display', 'none');
			switch(++stepcount){
				case 1: angleAnimFunc();
						break;
				case 2: $threelinescrossExpalinList1.css('opacity',"1");
						$threelinescrossImage.css('left', '0%').one(tranistionEndTestVar,function(){
							$nextStepBtn.show(0); /*show next step button on transition end*/
						});
						break;
				case 3: $threelinescrossExpalinList2.css('opacity', '1');
						$nextSlideBtn.show(0);
						break;
				default:break;
			}

		});

	}

/*******
* angleintro
*************/
	function angleintro() {
		var source = $("#angleintro-template").html();
		var template = Handlebars.compile(source);
		var html = template(slideContent[slideNumber]);
		$board.html(html);

		var $angleIntroCommon = $board.children('div.angleIntroCommon');
		var $angleDefinition = $angleIntroCommon.children('p.angleDefinition');
		var $imageNexampleDiv = $angleIntroCommon.children('div.imageNexample');
		var $angleImage = $imageNexampleDiv.children('img.angleImage');
		var $angleExample = $imageNexampleDiv.children('div.angleExample');
		var $anglePairSet = $angleExample.children('div').children('span');
		var highlightImage; /*variable to store the image source which is shown on angle pair hover*/
		var definitionhighlightString = $angleDefinition.data("definitionhighlight");
		var originalAngleImage = $angleImage.attr('src');
		var anglePairsTouched = 0;

			/*highlight the angle name string in definition*/
			$angleDefinition.html(ole.highlightTextTool.definitionhighlight($angleDefinition.text(),definitionhighlightString));
			/* what to do on hovering the pair */
			$anglePairSet.hover(function() {
				/* Stuff to do when the mouse enters the element */
				highlightImage = $(this).data('correspondingimage');
				$angleImage.attr('src',highlightImage);
			}, function(){
				/* Stuff to do when the mouse leaves the element */
				$angleImage.attr('src',originalAngleImage);
			});

			$anglePairSet.one('mouseover', function() {
				anglePairsTouched++;
				/*if anglePairsTouched is equal to the number of angle pairs to be touched*/
				if(anglePairsTouched == $anglePairSet.length){
					if(slideNumber+1 == total_slide){
						ole.footerNotificationHandler.lessonEndSetNotification();
					}
					else if(slideNumber+1 < total_slide){
						$nextSlideBtn.show(0);
					}

				}
			});
	}

/*******
* transition
*************/
	function transition() {
		var source = $("#transition-template").html();
		var template = Handlebars.compile(source);
		var html = template(slideContent[slideNumber]);
		$board.html(html);
		$nextSlideBtn.show(0);
	}

threelinescross();
// angleintro(slideNumber+=9);
// transition(slideNumber+=6);

/******************************************************************
* reusable code below, controls the behaviour of next and previous slide buttons
********************************************************************************/
	/*next button behaviour*/
	$nextSlideBtn.on('click',function () {
		if (slideNumber < total_slide){
			slideNumber++;
		}else{
			slideNumber = total_slide;
			ole.footerNotificationHandler.lessonEndSetNotification();
		}
		$(this).css('display', 'none');
		slideNumber > 0 ? $prevSlideBtn.show(0) : $prevSlideBtn.css('display', 'none');
		showThisSlide();
	});

	/*prev button behaviour*/
	$prevSlideBtn.on('click',function () {
		slideNumber > 0 ? slideNumber-- : slideNumber = 0;
		if(slideNumber < 1) $(this).css('display', 'none');
		$nextSlideBtn.css('display', 'none');
		/* hide the page end set notification bar */
		if(slideNumber < total_slide) ole.footerNotificationHandler.hideNotification();
		showThisSlide();
	});

	/*this function extends the behaviour of prevSlideBtn and nextSlideBtn according to slideNumber*/
	function showThisSlide(){
		switch(slideNumber) {
			case 0:
				threelinescross();
				break;
			case 1:
				angleintro();
				break;
			case 2:
				angleintro();
				break;
			case 3:
				angleintro();
				break;
			case 4:
				angleintro();
				break;
			case 5:
				angleintro();
				break;
			case 6:
				transition();
				break;
			case 7:
				angleintro();
				break;
			case 8:
				angleintro();
				break;
			case 9:
				angleintro();
				break;
			default:break;
		}

		loadTimelineProgress(total_slide,slideNumber+1);
	}
});

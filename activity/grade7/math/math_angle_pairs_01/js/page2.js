// path to image
var pathToImage = $ref+"/images/page2/";
var pathToTwoLinesCrossImage = pathToImage+"/twoLinesCross/";
var pathToVerticallyOppositeAnglesImage = pathToImage+"/verticallyOpposite/";

// contents for each slide
var slideContent=[
	{
		twolinescrossQuestionData : data.string.s2twolinescrossQuestion,
		twolinecrossImgSrc : pathToTwoLinesCrossImage+"01.png",
		twolinescrossExplainData : data.string.s2twolinescrossExplain,
	},
	{
		angleTitleData : data.string.p1option1,
		angleDefinitionData : data.string.s2verticallyopposite,
		definitionhighlightData : data.string.s2verticallyopposite_highlight,
		angleImgSrc : pathToVerticallyOppositeAnglesImage+"verticallyOppRaw.png",
		angleExampleData : data.string.s2verticallyopposite_example,
		angleExampleInstruction : data.string.anglehoverinstruction3,
		anglepairs : [
				{
					anglepairid : "verticaloppPair13",
					anglepairData : data.string.s2verticallyopposite_pair13,
					pairImgSrc : pathToVerticallyOppositeAnglesImage+"pair1-3.png",
				},
				{
					anglepairid : "verticaloppPair24",
					anglepairData : data.string.s2verticallyopposite_pair24,
					pairImgSrc : pathToVerticallyOppositeAnglesImage+"pair2-4.png",
				}
		],
	}
];

$(function () {
/*******************************************************
* reusable code below, the variables below till loadtimeline can be common
**************************************************************************/
	var $board = $('.board');
	var $nextSlideBtn = $('#activity-page-next-btn-enabled');
	var $prevSlideBtn = $('#activity-page-prev-btn-enabled');
	/*number of slides are from 0 to total_slide*/
	var slideNumber = 0; /*count for slideNumber initially 0*/
	var total_slide = 2;
	loadTimelineProgress(total_slide,slideNumber+1);

/*************************
* page specific code starts here
***********************************/

/*******
* twolinescross
*************/
	function twolinescross() {
		var source = $("#twolinescross-template").html();
		var template = Handlebars.compile(source);
		var html = template(slideContent[slideNumber]);
		$board.html(html);
		// $nextSlideBtn.css('display', 'none');
		$nextSlideBtn.show(0);

		var $twolinescross = $board.children('div.twolinescross');
		var $twolinescrossQuestion = $twolinescross.children('p.twolinescrossQuestion');
		var $twolinecrossImage = $twolinescross.children('img.twolinecrossImage');
		var $twolinescrossExpalin = $twolinescross.children('p.twolinescrossExpalin');
		var $nextStepBtn = $("#activity-page-next-btn-enabled");

		var stepcount = 0;
		var animImageOrder = ["02.png","03.png","04.png",
								"05.png","06.png","07.png",
								"08.png","09.png"];
		var animImagecount = 0;
		var animTotalImages = animImageOrder.length;
		var animationTimeInterval = 500;
		var setintervalvar;
		var changeToImage;

		/*animation angle function*/
		function angleAnimFunc(){
			setintervalvar = setInterval(function()
			{
			 	changeToImage = pathToTwoLinesCrossImage+animImageOrder[animImagecount++];
			 	$twolinecrossImage.attr("src",changeToImage);
			 	if(animImagecount == animTotalImages){
			 		clearInterval(setintervalvar);/*stop animation here*/
			 		$nextStepBtn.show(0);
			 	}

			}, animationTimeInterval);
		}

		$nextStepBtn.on('click', function() {
			$(this).css('display', 'none');
			switch(++stepcount){
				case 1: angleAnimFunc();
						break;
				case 2: $twolinescrossExpalin.show(0);
						$nextSlideBtn.show(0);
						break;
				default:break;
			}

		});

	}

/*******
* angleintro
*************/
	function angleintro() {
		var source = $("#angleintro-template").html();
		var template = Handlebars.compile(source);
		var html = template(slideContent[slideNumber]);
		$board.html(html);

		var $angleIntroCommon = $board.children('div.angleIntroCommon');
		var $angleDefinition = $angleIntroCommon.children('p.angleDefinition');
		var $imageNexampleDiv = $angleIntroCommon.children('div.imageNexample');
		var $angleImage = $imageNexampleDiv.children('img.angleImage');
		var $angleExample = $imageNexampleDiv.children('div.angleExample');
		var $anglePairSet = $angleExample.children('div').children('span');
		var highlightImage; /*variable to store the image source which is shown on angle pair hover*/
		var definitionhighlightString = $angleDefinition.data("definitionhighlight");
		var originalAngleImage = $angleImage.attr('src');
		var anglePairsTouched = 0;

			/*highlight the angle name string in definition*/
			$angleDefinition.html(ole.highlightTextTool.definitionhighlight($angleDefinition.text(),definitionhighlightString));
			/* what to do on hovering the pair */
			$anglePairSet.hover(function() {
				/* Stuff to do when the mouse enters the element */
				highlightImage = $(this).data('correspondingimage');
				$angleImage.attr('src',highlightImage);
			}, function(){
				/* Stuff to do when the mouse leaves the element */
				$angleImage.attr('src',originalAngleImage);
			});

			$anglePairSet.one('mouseover', function() {
				/* Act on the event */
				anglePairsTouched++;
				if(anglePairsTouched == 2){
					ole.footerNotificationHandler.pageEndSetNotification();
				}
			});
	}

twolinescross();
// angleintro(slideNumber+=2);

/******************************************************************
* reusable code below, controls the behaviour of next and previous slide buttons
********************************************************************************/
	/*next button behaviour*/
	$nextSlideBtn.on('click',function () {
		slideNumber < total_slide ? slideNumber++ : slideNumber = total_slide;
		$(this).css('display', 'none');
		slideNumber > 0 ? $prevSlideBtn.show(0) : $prevSlideBtn.css('display', 'none');
		showThisSlide();
	});

	/*prev button behaviour*/
	$prevSlideBtn.on('click',function () {
		slideNumber > 0 ? slideNumber-- : slideNumber = 0;
		$(this).css('display', 'none');
		/* hide the page end set notification bar */
		if(slideNumber < total_slide) ole.footerNotificationHandler.hideNotification();
		showThisSlide();
	});

	/*this function extends the behaviour of prevSlideBtn and nextSlideBtn according to slideNumber*/
	function showThisSlide(){
		switch(slideNumber) {
			case 0:
				twolinescross();
				break;
			case 1:
				angleintro();
				break;
			default:break;
		}

		loadTimelineProgress(total_slide,slideNumber+1);
	}
});

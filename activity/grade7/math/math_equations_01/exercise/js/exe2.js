
var counter=0;
var totalCounter=10;
$(function(){


	$(".headTitle").html(data.string.e1_1);
	var nextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(nextbtn);
	

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);


	var newarray=arrayquestion;//shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;
	getQuestion(counter, newarray);
	
			
	$(".wholequesbox").on('click',"#confirm",function(){

        var val=$("#inputval").val();
        var checkedval=$("input:checked" ).val();
             // alert(checkedval);

        if(val.length==0)
        {
        	$("#inputval").addClass('borderMe');
        } 
        else if(checkedval==undefined)
        {
        	$(".option").addClass('borderMe');
        } 		
        else
        {
        	var intval=val;
        	var dataval=$("#inputval").attr('data');
        	var dataval2=$("input:checked" ).attr('dataval');
        	
        	if(intval==dataval && checkedval==dataval2)
        	{
        		// alert("intval="+intval+"| dataval ="+dataval+"\n checkdval="+checkedval+"| dataval2="+dataval2);
        		$("#right").show(0);
        		rightcounter++;
        	}
        	else
        	{	
        		// alert("intval="+intval+"| dataval ="+dataval+"\n checkdval="+checkedval+"| dataval2="+dataval2);
        		$("#wrong").show(0);
        		wrongCounter++;
        	}
        	$(".hintMe").show(0);
        	$("#inputval").attr('disabled','disabled');
        	$(this).hide(0);

        	
        	$("#activity-page-next-btn-enabled").show(0);
        }
	});

	$("#activity-page-next-btn-enabled").click(function(){
	
		$(this).hide(0);
		counter++;

		
		if(counter<totalCounter)
		{
			getQuestion(counter, newarray);
		}
		else
		{
			getSummary(newarray,rightcounter,wrongCounter);
		}

		if(counter > totalCounter)
		{
			ole.activityComplete.finishingcall();
		}
	});
	
});



function getQuestion(counter, newarray)
{
	var cntme=newarray[counter];
	loadTimelineProgress(11,counter+1);
	
	
	var datas={	quesTitle:data.string.e1t2,	
				imgsrc:$ref+"/exercise/images/"+cntme+".png",
				optionval:[
					{dataval:data.string["opt_"+cntme+"_1"],whatval2:data.string.greatequal,whatval:data.string.opta_1},
					{dataval:data.string["opt_"+cntme+"_1"],whatval2:data.string.lessequal,whatval:data.string.opta_2},
					{dataval:data.string["opt_"+cntme+"_1"],whatval2:data.string.great,whatval:data.string.opta_3},
					{dataval:data.string["opt_"+cntme+"_1"],whatval2:data.string.less,whatval:data.string.opta_4}
				],		
				e2:data.string["what_"+cntme],
				confirm:data.string.submit,
				dataval2:data.string["opt_"+cntme+"_2"]				
			}	

	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(datas);

	$(".wholequesbox").fadeOut(10,function(){
		$(this).html(html);
	
	}).delay(100).fadeIn(10,function(){

		
	});

}

function getSummary(newarray,rightcounter,wrongCounter)
{
	loadTimelineProgress(11,11);
	
	

	var datame={
				e2_5:data.string.e2_5,
				rt:rightcounter,
				wr:wrongCounter,
				e2_6:data.string.e2_6,
				allans:[{quesMe:data.string["q_1"], e2:data.string["what_1"], dataval:data.string["ans_1"]},
				{quesMe:data.string["q_2"], e2:data.string["what_2"], dataval:data.string["ans_2"]},
				{quesMe:data.string["q_3"], e2:data.string["what_3"], dataval:data.string["ans_3"]},
				{quesMe:data.string["q_4"], e2:data.string["what_4"], dataval:data.string["ans_4"]},
				{quesMe:data.string["q_5"], e2:data.string["what_5"], dataval:data.string["ans_5"]},
				{quesMe:data.string["q_6"], e2:data.string["what_6"], dataval:data.string["ans_6"]},
				{quesMe:data.string["q_7"], e2:data.string["what_7"], dataval:data.string["ans_7"]},
				{quesMe:data.string["q_8"], e2:data.string["what_8"], dataval:data.string["ans_8"]},
				{quesMe:data.string["q_9"], e2:data.string["what_9"], dataval:data.string["ans_9"]},
				{quesMe:data.string["q_10"], e2:data.string["what_10"], dataval:data.string["ans_10"]}]
			};
	
	var source=$("#template-2").html();
	var template=Handlebars.compile(source);
	var html=template(datame);

	$(".headTitle").html(data.string.e2_4);
	$(".wholequesbox").fadeOut(10,function(){
		
		$(this).html(html);
	
	}).delay(100).fadeIn(10,function(){

		$("#activity-page-next-btn-enabled").show(0);
		
		
	});


}



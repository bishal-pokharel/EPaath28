
var counter=0;
var totalCounter=10;
$(function(){


	$(".headTitle").html(data.string.e1_1);
	var nextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(nextbtn);
	

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);


	var newarray=shuffleArray(arrayquestion);

	var arrLen=newarray.length;
	var rightcounter=0, wrongCounter=0;
	getQuestion(counter, newarray);
	
			
	$(".wholequesbox").on('click',"#confirm",function(){

		

        var val=$("#inputval").val();
       

        if(val.length==0)
        {
        	$("#inputval").addClass('borderMe');
        }  		
        else
        {
        	var intval=(val);
        	var dataval=($("#inputval").attr('data'));
        	if(intval==dataval)
        	{
        		$("#right").show(0);
        		rightcounter++;
        	}
        	else
        	{
        		$("#wrong").show(0);
        		wrongCounter++;
        	}
        	$(".hintMe").show(0);
        	$("#inputval").attr('disabled','disabled');
        	$(this).hide(0);

        	
        	$("#activity-page-next-btn-enabled").show(0);
        }
	});

	$("#activity-page-next-btn-enabled").click(function(){
	
		$(this).hide(0);
		counter++;

		
		if(counter<totalCounter)
		{
			getQuestion(counter, newarray);
		}
		else
		{
			getSummary(newarray,rightcounter,wrongCounter);
		}

	});

	$("#repeatBtn").click(function(){

		location.reload();
	});
	
});



function getQuestion(counter, newarray)
{
	var cntme=newarray[counter];
	loadTimelineProgress(11,counter+1);
	
	
	var datas={	quesTitle:data.string.e1t1,
				questSuggestion : data.string.e1t1_1,
				e2:data.string["what_"+cntme],
				confirm:data.string.submit,
				quesMe:data.string["q_"+cntme],
				hintMe:data.string["hint_"+cntme],
				dataval:data.string["ans_"+cntme]
				
			}	

	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(datas);

	$(".wholequesbox").fadeOut(10,function(){
		$(this).html(html);
	
	}).delay(100).fadeIn(10,function(){

		
	});

}

function getSummary(newarray,rightcounter,wrongCounter)
{
	loadTimelineProgress(11,11);

	
	

	var datame={
				e2_5:data.string.e2_5,
				rt:rightcounter,
				wr:wrongCounter,
				e2_6:data.string.e2_6,
				allans:[{quesMe:data.string["q_1"], e2:data.string["what_1"], dataval:data.string["ans_1"]},
				{quesMe:data.string["q_2"], e2:data.string["what_2"], dataval:data.string["ans_2"]},
				{quesMe:data.string["q_3"], e2:data.string["what_3"], dataval:data.string["ans_3"]},
				{quesMe:data.string["q_4"], e2:data.string["what_4"], dataval:data.string["ans_4"]},
				{quesMe:data.string["q_5"], e2:data.string["what_5"], dataval:data.string["ans_5"]},
				{quesMe:data.string["q_6"], e2:data.string["what_6"], dataval:data.string["ans_6"]},
				{quesMe:data.string["q_7"], e2:data.string["what_7"], dataval:data.string["ans_7"]},
				{quesMe:data.string["q_8"], e2:data.string["what_8"], dataval:data.string["ans_8"]},
				{quesMe:data.string["q_9"], e2:data.string["what_9"], dataval:data.string["ans_9"]},
				{quesMe:data.string["q_10"], e2:data.string["what_10"], dataval:data.string["ans_10"]}]
			};
	
	var source=$("#template-2").html();
	var template=Handlebars.compile(source);
	var html=template(datame);

	$(".headTitle").html(data.string.e2_4);
	$(".wholequesbox").fadeOut(10,function(){
		
		$(this).html(html);
	
	}).delay(100).fadeIn(10,function(){

		
		ole.footerNotificationHandler.pageEndSetNotification();
		
	});

	


}



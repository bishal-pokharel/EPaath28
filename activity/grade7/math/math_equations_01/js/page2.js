
var content=[
	{
		firstText:data.string.p2_1
	},
	{
		subText:data.string.p2_2,
		eq1:data.string.p2_3,
		eq2:data.string.p2_4,
		eq3:data.string.p2_5,
		eq4:data.string.p2_6,
		eq5:data.string.p2_7,
		secondText:data.string.p2_8,
		thirdText:data.string.p2_9,
		fourText:data.string.p2_10,
		fifthText:data.string.p2_11
	},
	{
		subText:data.string.p2_12,
		eq1:data.string.p2_18,
		eq2:data.string.p2_19,
		eq3:data.string.p2_20,
		eq4:data.string.p2_21,
		eq5:data.string.p2_22,
		eq6:data.string.p2_23,
		secondText:data.string.p2_13,
		thirdText:data.string.p2_14,
		fourText:data.string.p2_16,
		fifthText:data.string.p2_17,
		sixText:data.string.p2_15
	},
	{
		subText:data.string.p2_24,
		eq1:data.string.p2_29,
		eq2:data.string.p2_30,
		eq3:data.string.p2_31,
		eq4:data.string.p2_32,
		eq5:data.string.p2_33,
		secondText:data.string.p2_25,
		thirdText:data.string.p2_26,
		fourText:data.string.p2_27,
		fifthText:data.string.p2_28		
	},
	{
		subText:data.string.p2_34,
		threeText1:data.string.p2_35,
		
		
	},
	{
		subText:data.string.p2_34,
		threeText1:data.string.p2_35,
		threeText2:data.string.p2_36,
		
	},
	{
		subText:data.string.p2_34,
		threeText1:data.string.p2_35,
		threeText2:data.string.p2_36,
		threeText3:data.string.p2_37,
		
	},
	{
		subText:data.string.p2_34,
		threeText1:data.string.p2_35,
		threeText2:data.string.p2_36,
		threeText3:data.string.p2_37,
		threeText4:data.string.p2_38
		
	},
	{
		subText:data.string.p2_34,	
		threeText1:data.string.p2_35,	
		threeText3:data.string.p2_39,
		eq1:data.string.p2_41,
		eq2:data.string.p2_42,
		eq3:data.string.p2_43,
		eq4:data.string.p2_44,
		eq5:data.string.p2_44,
		threeText5:data.string.p2_40
		
	},
	{
		subText:data.string.p2_45,	
		threeText1:data.string.p2_46	
	},
	{
		subText:data.string.p2_45,	
		threeText1:data.string.p2_46,
		threeText2:data.string.p2_47,
		threeText3:data.string.p2_48,
		imgsrcs:$ref+"/images/box.png"
	},
	{
		subText:data.string.p2_45,	
		threeText1:data.string.p2_46,
		threeText2:data.string.p2_47,
		threeText3:data.string.p2_48,
		imgsrcs:$ref+"/images/box.png",
		threeText5:data.string.p2_49
	},
	{
		subText:data.string.p2_45,	
		threeText1:data.string.p2_46,
		threeText2:data.string.p2_47,
		threeText3:data.string.p2_48,
		imgsrcs:$ref+"/images/box.png",
		threeText5:data.string.p2_49,
		threeText6:data.string.p2_50
	},
	{
		subText:data.string.p2_45,	
		threeText1:data.string.p2_46,
		threeText2:data.string.p2_47,
		threeText3:data.string.p2_48,
		imgsrcs:$ref+"/images/box.png",
		threeText7:data.string.p2_51,
		threeText8:data.string.p2_52,
		threeText9:data.string.p2_53,
		threeText10:data.string.p2_54,
		threeText11:data.string.p2_55,
		threeText12:data.string.p2_56,
		threeText13:data.string.p2_57,
		
	}
	
];

$(function(){


	var $board = $('#myboard');

	$(".headTitle").html(data.string.p2_1);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	$("#repeatBtn").html(getReloadBtn());

	var counter=1;
	var innernextClick=0;
	var totalCount=14;	


	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		counter++;					
		getHtml(counter);	
		
		
		
	});

	$("#repeatBtn").click(function(){
		location.reload();
	});

	





	function getHtml(cnt)
	{
		
		var datas=content[cnt-1]


		var source;
		loadTimelineProgress(totalCount,cnt);

		source=$("#template-2").html();
		if(cnt==1)
			source=$("#template-1").html();
		else if(cnt>=5 && cnt<10 )
			source=$("#template-3").html();
		else if(cnt>=10)
			source=$("#template-4").html();

		
		

		var template=Handlebars.compile(source);
		var html=template(datas);



		$("#figBox").fadeOut(10,function(){ $(this).html(html); 
		}).delay(10).fadeIn(10,function(){
			if(cnt==1) $(".headTitle").hide(0);
			else 
			{
				$(".headTitle").show(0);			
			}
					getAnime(cnt);
		});
		


	}
	function getAnime(cnt)
	{
		

		switch(cnt)
		{
			case 1:
			$("#activity-page-next-btn-enabled").show(0);

		 		break;
			case 2:
			case 4:
				animateText();
				break;
			case 3:
				animateSecText();
				break;
			
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				animateThirdText();
				
				break;
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
				animateThirdText();
				
				break;
				}
	}
	function animateText()
	{
		$(".figBoxex").delay(500).fadeIn(500,function(){
			$("#eq1").fadeIn(100,function(){
				$("#eq2").delay(500).fadeIn(100);
				$("#secondText").delay(500).fadeIn(100,function(){
					$("#eq3").delay(500).fadeIn(100,function(){
						$("#eq4").delay(500).fadeIn(100);
						$("#thirdText").delay(500).fadeIn(100,function(){
							$("#eq5").delay(500).fadeIn(100,function(){
								$("#fourText").delay(500).fadeIn(100);
								$("#fifthText").delay(500).fadeIn(100,function(){
									$("#activity-page-next-btn-enabled").show(0);
								});
							});				
						});
					});					
				});
			});
		});
	}
	function animateSecText()
	{
		$(".figBoxex").delay(500).fadeIn(500,function(){
			$("#eq1").fadeIn(100,function(){
				$("#eq2").delay(500).fadeIn(100);
				$("#secondText").delay(500).fadeIn(100,function(){
					$("#eq3").delay(500).fadeIn(100,function(){
						$("#eq4").delay(500).fadeIn(100);
						$("#thirdText").delay(500).fadeIn(100,function(){
							$("#sixText").delay(500).fadeIn(100);
							$("#eq5").delay(500).fadeIn(100,function(){
								$("#eq6").delay(500).fadeIn(100,function(){
									$("#fourText").delay(500).fadeIn(100);
									$("#fifthText").delay(500).fadeIn(100,function(){
										$("#activity-page-next-btn-enabled").show(0);
									});
								});
							});				
						});
					});					
				});
			});
		});		
	}
	function animateThirdText()
	{
		$(".figBoxex").fadeIn(10,function(){
			
			if(counter==14)
				ole.footerNotificationHandler.pageEndSetNotification();
			else
				$("#activity-page-next-btn-enabled").show(0);
		});
		
	}

});

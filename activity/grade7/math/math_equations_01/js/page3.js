
var content=[
	{
		firstText:data.string.p3_1
	},
	{
		subText:data.string.p3_2,
		whatVal:[
				{geThan:"geThan",first: data.string.p3_3_1,second:data.string.p3_3_2, third:data.string.p3_3_3},
				{geThan:"leThan",first: data.string.p3_4_1,second:data.string.p3_4_2, third:data.string.p3_4_3},
				{geThan:"gThan",first: data.string.p3_5_1,second:data.string.p3_5_2, third:data.string.p3_5_3},
				{geThan:"lThan",first: data.string.p3_6_1,second:data.string.p3_6_2, third:data.string.p3_6_3}
		]
	},
	{
		threeText1:data.string.p3_7
	},
	{
		subText:data.string.p3_8,
		threeText2a:data.string.p3_9
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_9,
		threeText3:data.string.p3_10,
		imageBox:$ref+"/images/img02.png"
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_9,
		threeText3:data.string.p3_11,
		imageBox:$ref+"/images/img03.png"
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_9,
		threeText3:data.string.p3_12,
		imageBox:$ref+"/images/img04.png"
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_9,
		threeText3:data.string.p3_13,
		imageBox:$ref+"/images/img04.png"
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_9,
		threeText3:data.string.p3_14,
		imageBox:$ref+"/images/img04.png"
	},
	{
		subText:data.string.p3_8,
		threeText2a:data.string.p3_15
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_15,
		threeText3:data.string.p3_16,
		imageBox:$ref+"/images/img02.png"
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_15,
		threeText3:data.string.p3_17,
		imageBox:$ref+"/images/img05.png"
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_15,
		threeText3:data.string.p3_18,
		imageBox:$ref+"/images/img06.png"
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_15,
		threeText3:data.string.p3_19,
		imageBox:$ref+"/images/img06.png"
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_15,
		threeText3:data.string.p3_20,
		imageBox:$ref+"/images/img06.png"
	},
	{
		subText:data.string.p3_8,
		threeText2a:data.string.p3_21
		
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_21,
		imageBox:$ref+"/images/img02.png",
		threeText3:data.string.p3_22
		
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_21,
		imageBox:$ref+"/images/img05.png",
		threeText3:data.string.p3_23
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_21,
		imageBox:$ref+"/images/img07.png",
		threeText3:data.string.p3_24
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_21,
		imageBox:$ref+"/images/img07.png",
		threeText3:data.string.p3_25
	},
	{
		subText:data.string.p3_8,
		threeText2:data.string.p3_21,
		imageBox:$ref+"/images/img07.png",
		threeText3:data.string.p3_26
	},
	{
		
		threeText2a:data.string.p3_27
	},
	{
		
		subText:data.string.p3_27,
		eq1:data.string.p3_28,
		eq2:data.string.p3_29,
		eq3:data.string.p3_30,
		simageBox:$ref+"/images/img08.png"
	},
	{
		
		subText:data.string.p3_27,
		eq1:data.string.p3_31,
		eq2:data.string.p3_32,
		eq3:data.string.p3_33,
		simageBox:$ref+"/images/img09.png"
	},
	{
		subText:data.string.p3_27,
		eq1:data.string.p3_34,
		eq2:data.string.p3_35,
		eq3:data.string.p3_36,
		simageBox:$ref+"/images/img10.png"
	},
	{
		subText:data.string.p3_27,
		eq1:data.string.p3_37,
		eq2:data.string.p3_38,
		eq3:data.string.p3_39,
		simageBox:$ref+"/images/img11.png"
	}
];

$(function(){


	var $board = $('#myboard');

	$(".headTitle").html(data.string.p3_1);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	$("#repeatBtn").html(getReloadBtn());

	var counter=1;
	var innernextClick=0;
	var totalCount=26;	


	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		counter++;					
		getHtml(counter);	
		
		
		
	});

	$("#repeatBtn").click(function(){
		location.reload();
	});

	





	function getHtml(cnt)
	{
		
		var datas=content[cnt-1]


		var source;
		loadTimelineProgress(totalCount,cnt);

		source=$("#template-2").html();
		if(cnt==1)
			source=$("#template-1").html();
		else if(cnt>=3  )
			source=$("#template-3").html();
		

		
		

		var template=Handlebars.compile(source);
		var html=template(datas);



		$("#figBox").fadeOut(10,function(){ $(this).html(html); 
		}).delay(10).fadeIn(10,function(){
			if(cnt==1) $(".headTitle").hide(0);
			else 
			{
				$(".headTitle").show(0);			
			}
					getAnime(cnt);
		});
		


	}
	function getAnime(cnt)
	{
		

		switch(cnt)
		{
			case 1:
			$("#activity-page-next-btn-enabled").show(0);

		 		break;
			case 2:
				animateBox();
				break;	
			case 3:
			case 4:	
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21:
			case 22:
				$("#activity-page-next-btn-enabled").show(0);			
				break;
			case 23:
			case 24:
			case 25:
			case 26:
				animateBox2();
				break;	
			
				
		}
	}
	function animateBox()
	{
		
		$( ".whatBoxVal" ).hover(
			function() {
				var id=$( this ).attr('id');
				$("#"+id+"_1").show(0);
				$("#activity-page-next-btn-enabled").show(0);

			}, function() {
				var id=$( this ).attr('id');
				$("#"+id+"_1").hide(0);
			}
		);
	}

	function animateBox2()
	{
		$("#eq1").fadeIn(100,function(){
			$("#eq2").delay(500).fadeIn(100,function(){
				$("#eq3").delay(500).fadeIn(100,function(){
					$(".simageBox").delay(500).fadeIn(100,function(){
						
						if(counter==26)
							ole.footerNotificationHandler.lessonEndSetNotification();
						else 
							$("#activity-page-next-btn-enabled").show(0);	
					});
				});
			});
		});
	}
	

});

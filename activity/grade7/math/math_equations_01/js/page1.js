var content=[
	{
		firstText:data.lesson.chapter,
	},
	{
		firstText:data.string.p1_1
	},
	{
		firstText:data.string.p1_2,

	},
	{
		firstText:data.string.p1_2,
		eq1:"x + 6 = 12"
	},
	{
		firstText:data.string.p1_2,
		eq1:"x + 6 = 12",
		secondText:data.string.p1_3,

	},
	{
		firstText:data.string.p1_2,
		eq1:"x + 6 = 12",
		secondText:data.string.p1_3,
		thirdText:data.string.p1_4

	},
	{
		firstText:data.string.p1_2,
		eq1:"x + 6 = 12",
		secondText:data.string.p1_3,
		thirdText:data.string.p1_4,
		equTxt:data.string.submit
	},
	{

		eq1:"x + 6 = 12",
		secondText:data.string.p1_5,

	},
	{

		eq1:"x + 6 = 12",
		secondText:data.string.p1_5,
		eq2:"6 + 6 = 12",
		eq3:"12 = 12",
		thirdText:data.string.p1_6
	}
];

$(function(){


	var $board = $('#myboard');

	$(".headTitle").html(data.string.p1_1);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	$("#repeatBtn").html(getReloadBtn());

	var counter=1;
	var innernextClick=0;
	var totalCount=9;


	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).hide(0);
		counter++;
		getHtml(counter);



	});

	$("#repeatBtn").click(function(){
		location.reload();
	});







	function getHtml(cnt)
	{

		var datas=content[cnt-1]


		var source;
		loadTimelineProgress(totalCount,cnt);

		source=$("#template-2").html();
		if(cnt==1 && cnt==2)
			source=$("#template-1").html();


		var template=Handlebars.compile(source);
		var html=template(datas);



		$("#figBox").fadeOut(10,function(){ $(this).html(html);
		}).delay(10).fadeIn(10,function(){
			if(cnt==1 && cnt==2) $(".headTitle").hide(0);
			else
			{
				$(".headTitle").show(0);
			}
			if(cnt==8 || cnt==9)
					$("#secondText").css({'text-align':'center'});
			getAnime(cnt);
		});



	}
	// cnt>1?$(".cover_image").hide(0):"";
	function getAnime(cnt)
	{


		switch(cnt)
		{
			case 1:
				$(".figBoxex").hide(0);
					$("#activity-page-next-btn-enabled").show(0);
			break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 8:
				$("#activity-page-next-btn-enabled").show(0);
				$(".cover_image").hide(0);

		 		break;
			case 7:
				animateText();
				$(".cover_image").hide(0);
				break;
			case 9:
				ole.footerNotificationHandler.pageEndSetNotification();
				$(".cover_image").hide(0);
			break;
			default:
				$("#activity-page-next-btn-enabled").show(0);
				$(".cover_image").hide(0);
			break;
		}
	}
	function animateText()
	{
		$("#inputbtn").click(function(){
			var inputBxval=$("#inputfst").val();

			if(inputBxval.length==0)
			{

			}
			else if(/\D/g.test(inputBxval))
			{
				$("#inputfst").val('');
			}
			else
			{
				$(this).hide(0);
				if(inputBxval=="6")
				{
					$("#equationBx .glyphicon").addClass('glyphicon-ok');
				}
				else
				{
					$("#equationBx .glyphicon").addClass('glyphicon-remove');
					$("#correctans").html(6);
				}
				$("#activity-page-next-btn-enabled").show(0);

			}
		});
	}

});

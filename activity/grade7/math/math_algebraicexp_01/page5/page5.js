// parallelogram third line animation
function thirdLinePgram(){
	$("#pGram > p:nth-of-type(3) > span:nth-of-type(1)").velocity({opacity:"1"},500,function(){
				$("#pGram > p:nth-of-type(2) > ref:nth-of-type(-n+3)").addClass('blurryText');
				$("#pGram > p:nth-of-type(3) > span:nth-of-type(2)").delay(3100).velocity({opacity:"1"},500,function(){
					$("#pGram > p:nth-of-type(3) > span:nth-of-type(3)").velocity({opacity:"1"},500,function(){
						$("#pGram > p:nth-of-type(2) > ref:nth-of-type(5) > span:nth-of-type(-n+2)").addClass('blurryText');						
						$("#pGram > p:nth-of-type(3) > span:nth-of-type(4)").delay(3100).velocity({opacity:"1"},500,function(){
							$("#pGram > p:nth-of-type(2) > ref:nth-of-type(5) > span:nth-of-type(-n+2)").removeClass('blurryText');
							$("#pGram > p:nth-of-type(3) > span:nth-of-type(5)").velocity({opacity:"1"},500,function(){
								$("#pGram > p:nth-of-type(2) > ref:nth-of-type(5) > span:nth-of-type(1)").addClass('blurryText');
								$("#pGram > p:nth-of-type(2) > ref:nth-of-type(5) > span:nth-of-type(3)").addClass('blurryText');
								$("#pGram > p:nth-of-type(4)").delay(3100).velocity({opacity:"1"},500,function(){
									$("#pGram > p:nth-of-type(3) > span:nth-of-type(1)").addClass('blurryText');
									$("#pGram > p:nth-of-type(3) > span:nth-of-type(n+4)").addClass('blurryText');									
									$("#pGram > p:nth-of-type(4)").fadeIn(500,function(){
										$("#pGram > p:nth-of-type(5)").delay(3100).fadeIn(500,function(){
											$("#pGram > p:nth-of-type(6)").fadeIn(500,function(){
												$("#pGram > p:nth-of-type(6) > input:nth-of-type(1)").focus();
											});
										});
									});
								});
							});
						});
					});	
				});			
	});
}

// parallelogram second line animation
function secondLinePgram(){
	$("#pGram > p:nth-of-type(2) > ref:nth-of-type(1)").velocity({opacity:"1"},500,function(){
		$("#pGram > p:nth-of-type(1) > span:nth-of-type(2)").addClass('blurryText');
		// highlight left triangle in parallelogram
		$("#areaLight > img:nth-of-type(1)").css({"opacity":"1"});
		// blurryText effect is 3 second long so delay the below line for 3.1 seconds
		$("#pGram > p:nth-of-type(2) > ref:nth-of-type(2)").delay(3100).velocity({opacity:"1"},500,function(){
			// hide left triangle and highlight right triangle
			$("#areaLight > img:nth-of-type(1)").css({"opacity":"0"});
			$("#areaLight > img:nth-of-type(3)").css({"opacity":"1"});
			$("#pGram > p:nth-of-type(2) > ref:nth-of-type(3)").velocity({opacity:"1"},500,function(){
				$("#pGram > p:nth-of-type(1) > span:nth-of-type(3)").addClass('blurryText');
				$("#pGram > p:nth-of-type(2) > ref:nth-of-type(4)").delay(3100).velocity({opacity:"1"},500,function(){
					// hide right triangle and highlight rectangle
					$("#areaLight > img:nth-of-type(3)").css({"opacity":"0"});
					$("#areaLight > img:nth-of-type(2)").css({"opacity":"1"});
					$("#pGram > p:nth-of-type(2) > ref:nth-of-type(5),span").velocity({opacity:"1"},500,function(){
						$("#pGram > p:nth-of-type(1) > span:nth-of-type(4)").addClass('blurryText');
						// just making sure the third line span are with 0 pacity with line below
						$("#pGram > p:nth-of-type(3) > span").css({"opacity":"0"});
						$("#pGram > p:nth-of-type(3)").delay(3100).velocity({opacity:"1"},500,function(){
							// hide rectangle
							$("#areaLight > img:nth-of-type(2)").css({"opacity":"0"});
							thirdLinePgram();
						});
					});
				});
			});
		});
	});
}

// animation of the parallelogram equation
function animateEqnPgram(){
	// first line animation
	$("#pGram > p:nth-of-type(1) > span:nth-of-type(1)").delay(500).velocity({opacity:"1"},500,function(){
		$("#pGram > p:nth-of-type(1) > span:nth-of-type(2)").velocity({opacity:"1"},500,function(){
			$("#pGram > p:nth-of-type(1) > span:nth-of-type(3)").velocity({opacity:"1"},500,function(){
				$("#pGram > p:nth-of-type(1) > span:nth-of-type(4)").velocity({opacity:"1"},500,function(){
					$("#pGram > p:nth-of-type(2)").velocity({opacity:"1"},500,function(){
						secondLinePgram();
					});
				});
			});
		});
	});
					
}

// checking full answer for triangle formula input
function checkTriFormula(term1,term2){
	// check both blank spaces
			var firstIp = term1.length;
			var secondIp = term2.length;
			
			if((firstIp+secondIp) > 1){			
				var expression = term1+term2;
				if(expression == "bh"){
					// disable the input if true
					$("#triF1 > input").prop('disabled', true);
					$("#triangle > p:nth-child(n+4):nth-child(-n+5)").fadeIn(0);
					$("#triN1 > input:nth-of-type(1)").focus();
					}

					else{
						// $("#a1info").text(data.string.pg3s4).css({"color":"red","display":"block","top":"90%"});
					}
			}
					
}

// checking full answer for rectangle formula input
function checkRectFormula(term1,term2){
	// check both blank spaces
			var firstIp = term1.length;
			var secondIp = term2.length;
			
			if((firstIp+secondIp) > 1){			
				var expression = term1+term2;
				if(expression == "ab"){
					// disable the input if true
					$("#rectF1 > input").prop('disabled', true);
					$("#rectangle > p:nth-child(n+4):nth-child(-n+5)").fadeIn(0);
					$("#rectN1 > input:nth-of-type(1)").focus();
					}

					else{
						// $("#a1info").text(data.string.pg3s4).css({"color":"red","display":"block","top":"90%"});
					}
			}
					
}

// function to be called after the shape is selected
function switchShape(shape, data, selected){
	$(shape).fadeIn(0);
	// clones but does not clone event handler - with false
	$("#"+selected).clone(false).appendTo(shape);
	
	$("#aExp5 > p:nth-of-type(2)").text(data);

	$("#aExp5 > div:nth-of-type(n+2) input:nth-of-type(1)").focus();

	// if parallelogram call for animation
	if(selected == "shape2"){
		animateEqnPgram();
	}
}

$(document).ready(function() {	

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);

	$("#aExp5Replay").text(data.string.pg5n1);
	
	// pull all data
	$("#aExp5 > p:nth-of-type(1)").text(data.string.pg5s1);
	$("#aExp5 > p:nth-of-type(2)").text(data.string.pg5s2);
	$("#rectangle > p:nth-of-type(1)").text(data.string.pg5s6);
	$("#rectangle > p:nth-of-type(2)").text(data.string.pg5s7);
	$("#rectF1 > input:nth-of-type(1)").attr("placeholder",data.string.pg5s8);
	$("#rectF1 > input:nth-of-type(2)").attr("placeholder",data.string.pg5s9);
	$("#rectangle > p:nth-of-type(4)").text(data.string.pg5s10);
	$("#rectangle > p:nth-of-type(6)").text(data.string.pg5s11);


	$("#triangle > p:nth-of-type(1)").text(data.string.pg5s14);
	$("#triangle > p:nth-of-type(2)").text(data.string.pg5s15);
	$("#triF1 > input:nth-of-type(1)").attr("placeholder",data.string.pg5s16);
	$("#triF1 > input:nth-of-type(2)").attr("placeholder",data.string.pg5s17);
	$("#triangle > p:nth-of-type(4)").text(data.string.pg5s10);

	$("#pGram > p:nth-of-type(1) > span:nth-of-type(1)").text(data.string.pg5s18);
	// $("#pGramF1 > input:nth-of-type(1)").attr("placeholder",data.string.pg5s16);
	// $("#pGramF1 > input:nth-of-type(2)").attr("placeholder",data.string.pg5s17);
	$("#pGram > p:nth-of-type(5)").text(data.string.pg5s10);

	// on clicking any of the image shapes
	$("#aExp5 > div:nth-of-type(1) > img").on('click', function() {
		$(this).siblings('img').hide(0).off();	
		// keep selected element into that variable
		var selected = $(this).attr("id");
		
		$(this).closest('div').fadeOut(0);

		switch(selected){	
			case "shape1": switchShape("#triangle",data.string.pg5s3,selected);
							break;
			case "shape2": switchShape("#pGram",data.string.pg5s4,selected);
							break;
			case "shape3": switchShape("#rectangle",data.string.pg5s5,selected);
							break;
			default: break;
		}		
	});

	// check for the formula in rectangle
	$("#rectF1 > input").keyup(function() {
		// console.log("i am in");
		
		// if other than alphabets remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		 $(this).val( $(this).val().replace(/[^a-zA-Z]/g,''));
		 
		var term1 = $("#rectF1 > input:nth-of-type(1)").val().toLowerCase();
		var term2 = $("#rectF1 > input:nth-of-type(2)").val().toLowerCase();
		

		// for first term
		if(term1 == "a"){
			$("#rectF1 > input:nth-of-type(1)").css({"color":"green"});
			$("#rectF1 > input:nth-of-type(2)").focus();
			checkRectFormula(term1,term2);
		}
		else{
			$("#rectF1 > input:nth-of-type(1)").css({"color":"#E70000"});
			checkRectFormula(term1,term2);
		}

		// for second term
		if(term2 == "b"){
			$("#rectF1 > input:nth-of-type(2)").css({"color":"green"});
			checkRectFormula(term1,term2);
		}
		else{
			$("#rectF1 > input:nth-of-type(2)").css({"color":"#E70000"});
			checkRectFormula(term1,term2);
		}
	});

	// check for the numeric input for rectangle
	$("#rectN1 > input").keyup(function() {
		// if other than numbers remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		 $(this).val( $(this).val().replace(/[^1-9]/g,''));
		 
		 var term1big = false;
		var term1 = Number($("#rectN1 > input:nth-of-type(1)").val());
		var term2 = Number($("#rectN1 > input:nth-of-type(2)").val());
			
		if (term2 > term1){
			
			// show error msg
			$("#aExp5 > p:nth-of-type(3)").text(data.string.pg5s13).css({"color":"red"});

			// length should be greater than breadth else make the second input empty
			$("#rectN1 > input:nth-of-type(2)").val('');
			// make the seeAns button invisible if above case is true 
			$("#rectN1 > span").removeClass('blinkEffect').fadeOut(0);
		}

		else
		{	
			term1big = true;
			// console.log(term1+"isBig");
		}


		var firstIp = term1.length;
		var secondIp = term2.length;

		if(term1big && term1>0 && term2>0){	
			
			// hide error msg
			$("#aExp5 > p:nth-of-type(3)").text("");	
			// show the button to see answer	
				$("#rectN1 > span").fadeIn(0).addClass('blinkEffect');
				// on clicking the button span
				$("#rectN1 > span").on('click', function(){
					term1 = $("#rectN1 > input:nth-of-type(1)").val();
					term2 = $("#rectN1 > input:nth-of-type(2)").val();
					var expression = term1*term2;
					$("#rectN1 > span").removeClass('blinkEffect').fadeOut(0);
					$("#rectN1 > input").prop('disabled', true);
					// to pass the value of square-&#178; literally use html
					$("#rectangle > p:nth-of-type(6)").html(data.string.pg5s11+expression+" unit&#178;");
					$("#rectangle > p:nth-of-type(6)").fadeIn(0);
					$("#aExp5Replay").show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
						if(term1 == term2){
							$("#rectangle > p:nth-of-type(6)").html(data.string.pg5s11+expression+" unit&#178;"+data.string.pg5s12);
						}
					});
				}
			});
				

// check for the formula in triangle
	$("#triF1 > input").keyup(function() {
		// console.log("i am in");
		
		// if other than alphabets remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		 $(this).val( $(this).val().replace(/[^a-zA-Z]/g,''));
		 
		var term1 = $("#triF1 > input:nth-of-type(1)").val().toLowerCase();
		var term2 = $("#triF1 > input:nth-of-type(2)").val().toLowerCase();
		

		// for first term
		if(term1 == "b"){
			$("#triF1 > input:nth-of-type(1)").css({"color":"green"});
			$("#triF1 > input:nth-of-type(2)").focus();
			checkTriFormula(term1,term2);
		}
		else{
			$("#triF1 > input:nth-of-type(1)").css({"color":"#E70000"});
			checkTriFormula(term1,term2);
		}

		// for second term
		if(term2 == "h"){
			$("#triF1 > input:nth-of-type(2)").css({"color":"green"});
			checkTriFormula(term1,term2);
		}
		else{
			$("#triF1 > input:nth-of-type(2)").css({"color":"#E70000"});
			checkTriFormula(term1,term2);
		}
	});

	// check for the numeric input for rectangle
	$("#triN1 > input").keyup(function() {
		// if other than numbers remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		 $(this).val( $(this).val().replace(/[^1-9]/g,''));
		 
		var term1 = Number($("#triN1 > input:nth-of-type(1)").val());
		var term2 = Number($("#triN1 > input:nth-of-type(2)").val());
			
		var firstIp = term1.length;
		var secondIp = term2.length;

		if(term1>0 && term2>0){				
				// show the button to see answer	
				$("#triN1 > span").fadeIn(0).addClass('blinkEffect');
				// on clicking the button span
				$("#triN1 > span").on('click', function(){
					term1 = Number($("#triN1 > input:nth-of-type(1)").val());
					term2 = Number($("#triN1 > input:nth-of-type(2)").val());
					var expression = (1/2)*term1*term2;
					$("#triN1 > span").removeClass('blinkEffect').fadeOut(0);
					$("#triN1 > input").prop('disabled', true);
					// to pass the value of square-&#178; literally use html
					$("#triangle > p:nth-of-type(6)").html(data.string.pg5s11+expression+" unit&#178;");
					$("#triangle > p:nth-of-type(6)").fadeIn(0);
					$("#aExp5Replay").show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
					});
				}
			});



	// check for the numeric input for rectangle
	$("#pGramN1 > input").keyup(function() {
		// if other than numbers remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		 $(this).val( $(this).val().replace(/[^1-9]/g,''));
		 
		var term1 = Number($("#pGramN1 > input:nth-of-type(1)").val());
		var term2 = Number($("#pGramN1 > input:nth-of-type(2)").val());
			
		var firstIp = term1.length;
		var secondIp = term2.length;

		if(term1>0 && term2>0){				
				// show the button to see answer	
				$("#pGramN1 > span").fadeIn(0).addClass('blinkEffect');
				// on clicking the button span
				$("#pGramN1 > span").on('click', function(){
					term1 = Number($("#pGramN1 > input:nth-of-type(1)").val());
					term2 = Number($("#pGramN1 > input:nth-of-type(2)").val());
					var expression = term1*term2;
					$("#pGramN1 > span").removeClass('blinkEffect').fadeOut(0);
					$("#pGramN1 > input").prop('disabled', true);
					// to pass the value of square-&#178; literally use html
					$("#pGram > p:nth-of-type(7)").html(data.string.pg5s11+expression+" unit&#178;");
					$("#pGram > p:nth-of-type(7)").fadeIn(0);
					$("#aExp5Replay").show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
					});
				}
			});

// on clicking replay button
$("#aExp5Replay").on('click',function() {
	$(this).hide(0).removeClass('rotateReplay');
	location.reload();
});

// end curly braces
})			
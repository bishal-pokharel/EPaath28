// this is the speed to be used in animation of the equation
var duration=400;

// show the fourth row animation
function fourthRowAnim(whichCell){
	if(whichCell < 26){
		$("#eqn  table tr:nth-of-type(4) td:nth-of-type("+whichCell+")").velocity({opacity:"1"},duration,function(){
			whichCell++;
			fourthRowAnim(whichCell);
		});
	}
	else{
		$("#eqn  table tr:nth-of-type(5) td:nth-of-type(1)").velocity({opacity:"1"},duration,function(){
			$("#eqn  table tr:nth-of-type(5) td:nth-of-type(2)").velocity({opacity:"1"},duration,function(){
				$("#eqn  table tr:nth-of-type(5) td:nth-of-type(3)").velocity({opacity:"1"},duration,function(){
					$("#conclude").fadeIn(1000,function(){
						$("#aExp6Replay").show(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					});
				});
			});
		});
	}

}

// show the third row animation
function thirdRowAnim(){
	$("#eqn  table tr:nth-of-type(3) td:nth-of-type(2)").velocity({opacity:"1"},duration,function(){
		$("#eqn  table tr:nth-of-type(2) td:nth-of-type(4)").addClass('blurryText');
		$("#eqn  table tr:nth-of-type(2) td:nth-of-type(8)").addClass('blurryText');
		$("#eqn  table tr:nth-of-type(2) td:nth-of-type(10)").addClass('blurryText');
		//blurryText effect takes 3 seconds of time
		$("#eqn  table tr:nth-of-type(3) td:nth-of-type(3)").delay(3000).velocity({opacity:"1"},duration,function(){
			 // blurryText class should be removed in below two elements to be used later on
			$("#eqn  table tr:nth-of-type(2) td:nth-of-type(4)").removeClass('blurryText');
			$("#eqn  table tr:nth-of-type(2) td:nth-of-type(8)").removeClass('blurryText');
			$("#eqn  table tr:nth-of-type(2) td:nth-of-type(10)").removeClass('blurryText');
			$("#eqn  table tr:nth-of-type(2) td:nth-of-type(5)").addClass('blurryText');
			$("#eqn  table tr:nth-of-type(3) td:nth-of-type(4)").delay(3000).velocity({opacity:"1"},duration,function(){
				$("#eqn  table tr:nth-of-type(2) td:nth-of-type(5)").removeClass('blurryText');
				$("#eqn  table tr:nth-of-type(2) td:nth-of-type(6)").addClass('blurryText');
				$("#eqn  table tr:nth-of-type(2) td:nth-of-type(8)").addClass('blurryText');
				$("#eqn  table tr:nth-of-type(2) td:nth-of-type(10)").addClass('blurryText');
				$("#eqn  table tr:nth-of-type(3) td:nth-of-type(5)").delay(3000).velocity({opacity:"1"},duration,function(){
					$("#eqn  table tr:nth-of-type(2) td:nth-of-type(6)").removeClass('blurryText');
					$("#eqn  table tr:nth-of-type(2) td:nth-of-type(8)").removeClass('blurryText');
					$("#eqn  table tr:nth-of-type(2) td:nth-of-type(10)").removeClass('blurryText');
					$("#eqn  table tr:nth-of-type(2) td:nth-of-type(5)").addClass('blurryText');
					$("#eqn  table tr:nth-of-type(3) td:nth-of-type(6)").delay(3000).velocity({opacity:"1"},duration,function(){
						$("#eqn  table tr:nth-of-type(2) td:nth-of-type(4)").addClass('blurryText');
						$("#eqn  table tr:nth-of-type(2) td:nth-of-type(8)").addClass('blurryText');
						$("#eqn  table tr:nth-of-type(2) td:nth-of-type(12)").addClass('blurryText');
						$("#eqn  table tr:nth-of-type(3) td:nth-of-type(7)").delay(3000).velocity({opacity:"1"},duration,function(){
							$("#eqn  table tr:nth-of-type(2) td:nth-of-type(4)").removeClass('blurryText');
							$("#eqn  table tr:nth-of-type(2) td:nth-of-type(8)").removeClass('blurryText');
							$("#eqn  table tr:nth-of-type(2) td:nth-of-type(12)").removeClass('blurryText');
							$("#eqn  table tr:nth-of-type(2) td:nth-of-type(11)").addClass('blurryText');
							$("#eqn  table tr:nth-of-type(3) td:nth-of-type(8)").delay(3000).velocity({opacity:"1"},duration,function(){
								$("#eqn  table tr:nth-of-type(2) td:nth-of-type(6)").addClass('blurryText');
								$("#eqn  table tr:nth-of-type(2) td:nth-of-type(8)").addClass('blurryText');
								$("#eqn  table tr:nth-of-type(2) td:nth-of-type(12)").addClass('blurryText');
								$("#eqn  table tr:nth-of-type(3) td:nth-of-type(9)").delay(3000).velocity({opacity:"1"},duration,function(){
									$("#eqn  table tr:nth-of-type(4) td:nth-of-type(1)").velocity({opacity:"1"},duration,function(){
										fourthRowAnim(2);
									});
								});
							});
						});
					});
				});
			});
		});
	});
}

// show the second row animation
function secondRowAnim(){
	$("#eqn > table tr:nth-of-type(2) td:nth-of-type(2)").velocity({opacity:"1"},duration,function(){
		$("#area").addClass('blinkTop');
		$("#eqn > table tr:nth-of-type(2) td:nth-of-type(3)").velocity({opacity:"1"},duration,function(){
			$("#eqn > table tr:nth-of-type(2) td:nth-of-type(4)").velocity({opacity:"1"},duration,function(){
				$("#eqn > table tr:nth-of-type(2) td:nth-of-type(5)").velocity({opacity:"1"},duration,function(){
					$("#eqn > table tr:nth-of-type(2) td:nth-of-type(6)").velocity({opacity:"1"},duration,function(){
						$("#eqn > table tr:nth-of-type(2) td:nth-of-type(7)").velocity({opacity:"1"},duration,function(){
							$("#eqn > table tr:nth-of-type(2) td:nth-of-type(8)").velocity({opacity:"1"},duration,function(){
								$("#area").removeClass('blinkTop');
								$("#area").addClass('blinkRight');
								$("#eqn > table tr:nth-of-type(2) td:nth-of-type(9)").velocity({opacity:"1"},duration,function(){
									$("#eqn > table tr:nth-of-type(2) td:nth-of-type(10)").velocity({opacity:"1"},duration,function(){
										$("#eqn > table tr:nth-of-type(2) td:nth-of-type(11)").velocity({opacity:"1"},duration,function(){
											$("#eqn > table tr:nth-of-type(2) td:nth-of-type(12)").velocity({opacity:"1"},duration,function(){
												$("#area").removeClass('blinkRight');
												$("#eqn > table tr:nth-of-type(2) td:nth-of-type(13)").velocity({opacity:"1"},duration,function(){

													$("#eqn > table tr:nth-of-type(3) td:nth-of-type(1)").velocity({opacity:"1"},duration,function(){
														thirdRowAnim();
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
}

// show the equation animation
function eqnAnim(){
	$("#eqn > table tr:nth-of-type(1) td:nth-of-type(1)").velocity({opacity:"1"},duration,function(){
		$("#eqn > table tr:nth-of-type(1) td:nth-of-type(2)").velocity({opacity:"1"},duration,function(){
			$("#eqn > table tr:nth-of-type(1) td:nth-of-type(3)").velocity({opacity:"1"},duration,function(){
				$("#eqn > table tr:nth-of-type(1) td:nth-of-type(4)").velocity({opacity:"1"},duration,function(){
					$("#eqn > table tr:nth-of-type(1) td:nth-of-type(5)").velocity({opacity:"1"},duration,function(){
						$("#eqn > table tr:nth-of-type(2) td:nth-of-type(1)").velocity({opacity:"1"},duration,function(){
						secondRowAnim();
						});
					});
				});
			});
		});
	});
}

// function to update table data
function updateTableData(){
	$("#eqn > table tr:nth-of-type(4) td:nth-of-type(4)").text(a);
	$("#eqn > table tr:nth-of-type(4) td:nth-of-type(6)").text(x);
	$("#eqn > table tr:nth-of-type(4) td:nth-of-type(10)").text(a);
	$("#eqn > table tr:nth-of-type(4) td:nth-of-type(12)").text(y);
	$("#eqn > table tr:nth-of-type(4) td:nth-of-type(16)").text(b);
	$("#eqn > table tr:nth-of-type(4) td:nth-of-type(18)").text(x);
	$("#eqn > table tr:nth-of-type(4) td:nth-of-type(22)").text(b);
	$("#eqn > table tr:nth-of-type(4) td:nth-of-type(24)").text(y);
	$("#eqn > table tr:nth-of-type(5) td:nth-of-type(3)").html(area+" unit&#178;");
}

var x=50;
var y=50;
var a=47;
var b=48;
var area=9500;

$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(1,1);

	$("#aExp6Replay").text(data.string.pg6n1);

	// pull all data
	$("#aExp6 > p:nth-of-type(1)").text(data.string.pg6s1);
	$("#aExp6 > p:nth-of-type(2)").text(data.string.pg6s2);
	$("#conclude").text(data.string.pg6s7);
	// table
	$("#eqn > table tr td:nth-of-type(1)").text(data.string.pg6s4);
	$("#eqn > table tr:nth-of-type(1) td:nth-of-type(3)").text(data.string.pg6s5);
	$("#eqn > table tr:nth-of-type(1) td:nth-of-type(5)").text(data.string.pg6s6);
	$("#eqn > table tr:nth-of-type(1) td:nth-of-type(5)").text(data.string.pg6s6);

	//table 4th and 5th row
	updateTableData();

	// make the hLine and vLine div draggable inside dragrea div
	$("#vLine").draggable({containment: "#dragArea", axis:"x",drag: function(){
		var vPos = $(this).position();
		var rightMost = $("#area").innerWidth();
		var posL = (vPos.left)/2;
		var posLP = Math.floor((posL/rightMost)*100);
		var posR = (rightMost-(vPos.left))/2;
		var posRP = Math.floor((((vPos.left)+posR)/rightMost)*100);
		x=posLP*2; /*length is taken as 100 for full rectangle*/
		y=100-x;
		area=(a*x)+(a*y)+(b*x)+(b*y);
		$("#area > p:nth-of-type(1) dfn").text(x);
		$("#area > p:nth-of-type(2) dfn").text(y);

		// console.log("x:"+x+" y:"+y+" a:"+a+" b:"+b+" area:"+area);
		// set the position of top two variables
		$("#area > p:nth-of-type(1)").css("left",posLP+"%");
		$("#area > p:nth-of-type(2)").css("left",posRP+"%");
	},stop:function(){
		$("#activity-page-next-btn-enabled").show(0);
	}});

	$("#hLine").draggable({containment: "#dragArea", axis:"y",drag: function(){
		var hPos = $(this).position();
		var bottomMost = $("#area").innerHeight();
		var posT = (hPos.top)/2;
		var posTP = Math.floor((posT/bottomMost)*100);
		var posB = (bottomMost-(hPos.top))/2;
		var posBP = Math.floor((((hPos.top)+posB)/bottomMost)*100);
		a=(posTP*2)-5; /*breadth is taken as 95 for full rectangle*/
		b=95-a;
		area=(a*x)+(a*y)+(b*x)+(b*y);
		$("#area > p:nth-of-type(3) dfn").text(a);
		$("#area > p:nth-of-type(4) dfn").text(b);

		console.log("x:"+x+" y:"+y+" a:"+a+" b:"+b+" area:"+area);
		// set the position of bottom two variables
		$("#area > p:nth-of-type(3)").css("top",(posTP-5)+"%");
		$("#area > p:nth-of-type(4)").css("top",(posBP-5)+"%");
	},stop:function(){
		$("#activity-page-next-btn-enabled").show(0);
	}});


	// on clicking toSolution div
	$("#activity-page-next-btn-enabled").on('click',function() {
		updateTableData();
		$("#activity-page-next-btn-enabled").off('click');
		$("#activity-page-next-btn-enabled").fadeOut(0);
		$("#vLine").draggable("disable");
		$("#hLine").draggable("disable");
		$("#vLine").css({"cursor":"default"});
		$("#hLine").css({"cursor":"default"});
		$("#aExp6 > p:nth-of-type(2)").fadeOut(0);

		$("#area > p").fadeIn(0);

			$("#area").delay(1000).velocity({top:"15%"},1000,function(){
				// animate table equation
					eqnAnim();

			});


	});

// do this on clicking replay
		$("#aExp6Replay").on('click',function() {
			$(this).hide(0).removeClass('rotateReplay');
			location.reload();
		});

// end curly braces
})

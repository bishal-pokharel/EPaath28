// function to make all expression ready for animation
function expressionReady(){
	$("#calculation > h1:nth-of-type(2)").text("9+").css({"top":"0%","left":"30%"});
	$("#calculation > h1:nth-of-type(3)").text("9").css({"top":"20%", "opacity":"0%","left":"30%"});
	$("#calculation > h1:nth-of-type(4)").text("").css({"display":"none","top":"60%","left":"30%"});
	$("#calculation > p").css({"opacity":"0"});
	$("#calcExplain").css({"display":"none"});
}

// function to check and if  okay ,display the conclusion statement
function showConclusion(){
	if(egClicked == 3)
	{
		/*console.log("i am activated");*/
		$("#aExp2conclusion").fadeIn(1000,function(){
			ole.footerNotificationHandler.pageEndSetNotification();
		});		
	}
}

// variable to make conclusion statement come alive at least 3 examples should be clicked
var egClicked=0;
// 5 variables for 5 examples
var firstClick = [false,false,false,false,false];

$(document).ready(function() {	

	loadTimelineProgress(1,1);
	// pull all data
	$("#aExp2 > p:first-child").text(data.string.pg2s1);
	$("#aExp2 > p:nth-of-type(2)").text(data.string.pg2s2);
	$("#aExp2conclusion").text(data.string.pg2s3);
	$("#calcExplain").text(data.string.pg2s4);

	ole.parseToolTip("#aExp2 > p:first-child","9+x");
	ole.parseToolTip("#aExp2 > p:first-child","(x)");
	$("#aExp2 > p:first-child").children('span:nth-of-type(n+1)').css({'color':"darkgreen"});

	// for clicking clouds what to do
	$("#sky > div:nth-of-type(1)").on('click', function() {
		$("#sky > div").css({"pointer-events":"none"});
		expressionReady();
		$("#calculation > h1:nth-of-type(2)").animate({"opacity":"1","top":"20%"},1000,function(){
			$("#aExp2 > em:nth-of-type(1)").animate({"top":"53%","left":"50%","opacity":"1"},1000,function(){
				// make sure at next click it is at its previous position
				$("#aExp2 > em:nth-of-type(1)").css({"top":"28%","left":"10%","opacity":"0"});
				$("#calculation > h1:nth-of-type(2)").text("=9+(-2)").css({"left":"25%"});
				$("#calculation > h1:nth-of-type(3)").animate({"opacity":"1","top":"40%"},1000,function(){
					$("#calculation > p").animate({"opacity":"1"},1000,function(){
						$("#aExp2 > em:nth-of-type(6)").animate({"opacity":"1","top":"60%","left":"45%"},1000,function(){
							$("#aExp2 > em:nth-of-type(6)").css({"top":"55%","left":"68%","opacity":"0"});
							$("#calculation > h1:nth-of-type(3)").text("=9-2").css({"left":"25%"});
							$("#calculation > h1:nth-of-type(4)").text("=7").css({"left":"25%"});
							$("#calculation > h1:nth-of-type(4)").delay(1000).fadeIn(1000,function(){
							$("#sky > div").css({"pointer-events":"auto"});
								$("#calcExplain").text(data.string.pg2s4).fadeIn(1000,function(){
									if(!firstClick[0]){
										firstClick[0]=true;
										egClicked++;
										showConclusion();
										}
								});
							
					});
					});
					});
					
				});
			});
		});
	});

	$("#sky > div:nth-of-type(2)").on('click', function() {
		$("#sky > div").css({"pointer-events":"none"});
		expressionReady();
		$("#calculation > h1:nth-of-type(2)").animate({"opacity":"1","top":"20%"},1000,function(){
			$("#aExp2 > em:nth-of-type(2)").animate({"top":"53%","left":"50%","opacity":"1"},1000,function(){
				// make sure at next click it is at its previous position
				$("#aExp2 > em:nth-of-type(2)").css({"top":"28%","left":"30%","opacity":"0"});
				$("#calculation > h1:nth-of-type(2)").text("=9+(-1)").css({"left":"25%"});
				$("#calculation > h1:nth-of-type(3)").animate({"opacity":"1","top":"40%"},1000,function(){
					$("#calculation > p").animate({"opacity":"1"},1000,function(){
						$("#aExp2 > em:nth-of-type(6)").animate({"opacity":"1","top":"60%","left":"45%"},1000,function(){
							$("#aExp2 > em:nth-of-type(6)").css({"top":"55%","left":"68%","opacity":"0"});
							$("#calculation > h1:nth-of-type(3)").text("=9-1").css({"left":"25%"});
							$("#calculation > h1:nth-of-type(4)").text("=8").css({"left":"25%"});
							$("#calculation > h1:nth-of-type(4)").delay(1000).fadeIn(1000,function(){
							$("#sky > div").css({"pointer-events":"auto"});
								$("#calcExplain").text(data.string.pg2s5).fadeIn(1000,function(){
									if(!firstClick[1]){
										firstClick[1]=true;
										egClicked++;
										showConclusion();
										}
								});
					});
					});
					});
					
				});
			});
		});
	});

	$("#sky > div:nth-of-type(3)").on('click', function() {
		$("#sky > div").css({"pointer-events":"none"});
		expressionReady();
		$("#calculation > h1:nth-of-type(2)").animate({"opacity":"1","top":"20%"},1000,function(){
			$("#aExp2 > em:nth-of-type(3)").animate({"top":"53%","left":"50%","opacity":"1"},1000,function(){
				// make sure at next click it is at its previous position
				$("#aExp2 > em:nth-of-type(3)").css({"top":"28%","left":"50%","opacity":"0"});
				$("#calculation > h1:nth-of-type(2)").text("=9+0").css({"left":"25%"});					
					$("#calculation > h1:nth-of-type(4)").text("=9").css({"top":"40%","left":"25%"});
					$("#calculation > h1:nth-of-type(4)").delay(1000).fadeIn(1000,function(){
						$("#sky > div").css({"pointer-events":"auto"});	
						$("#calcExplain").text(data.string.pg2s6).fadeIn(1000,function(){
									if(!firstClick[2]){
										firstClick[2]=true;
										egClicked++;
										showConclusion();
										}
								});
					});				
			});
		});
	});

	$("#sky > div:nth-of-type(4)").on('click', function() {
		$("#sky > div").css({"pointer-events":"none"});
		expressionReady();
		$("#calculation > h1:nth-of-type(2)").animate({"opacity":"1","top":"20%"},1000,function(){
			$("#aExp2 > em:nth-of-type(4)").animate({"top":"53%","left":"50%","opacity":"1"},1000,function(){
				// make sure at next click it is at its previous position
				$("#aExp2 > em:nth-of-type(4)").css({"top":"28%","left":"70%","opacity":"0"});
				$("#calculation > h1:nth-of-type(2)").text("=9+1").css({"left":"25%"});					
					$("#calculation > h1:nth-of-type(4)").text("=10").css({"top":"40%","left":"25%"});
					$("#calculation > h1:nth-of-type(4)").delay(1000).fadeIn(1000,function(){
						$("#sky > div").css({"pointer-events":"auto"});
						$("#calcExplain").text(data.string.pg2s7).fadeIn(1000,function(){
									if(!firstClick[3]){
										firstClick[3]=true;
										egClicked++;
										showConclusion();
										}
								});
					});				
			});
		});
	});

	$("#sky > div:nth-of-type(5)").on('click', function() {
		$("#sky > div").css({"pointer-events":"none"});
		expressionReady();
		$("#calculation > h1:nth-of-type(2)").animate({"opacity":"1","top":"20%"},1000,function(){
			$("#aExp2 > em:nth-of-type(5)").animate({"top":"53%","left":"50%","opacity":"1"},1000,function(){
				// make sure at next click it is at its previous position
				$("#aExp2 > em:nth-of-type(5)").css({"top":"28%","left":"85%","opacity":"0"});
				$("#calculation > h1:nth-of-type(2)").text("=9+2").css({"left":"25%"});					
					$("#calculation > h1:nth-of-type(4)").text("=11").css({"top":"40%","left":"25%"});
					$("#calculation > h1:nth-of-type(4)").delay(1000).fadeIn(1000,function(){
						$("#sky > div").css({"pointer-events":"auto"});
						$("#calcExplain").text(data.string.pg2s8).fadeIn(1000,function(){
									if(!firstClick[4]){
										firstClick[4]=true;
										egClicked++;
										showConclusion();
										}
								});
					});		
			});
		});
	});


})			
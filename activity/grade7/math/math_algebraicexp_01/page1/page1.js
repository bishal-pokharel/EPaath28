// animate expression - exp is the expression string
function animateExp(exp){

	$(exp+" > h1:nth-of-type(1)").animate({"opacity":"1"},700,function(){
		$(exp+" > h1:nth-of-type(2)").animate({"opacity":"1"},700,function(){
		$(exp+" > h1:nth-of-type(3)").animate({"opacity":"1"},700);
		// console.log("here i am");
	});
	});
}

// used for on click with nextExp button
var increaseCount=0;
$(document).ready(function() {

	/*load the progress bar on this page with one page*/
	loadTimelineProgress(6,1);

	// pull all data
	$(".covertext").html(eval("data.lesson.chapter"));
	$("#intro").text(data.string.pg1s1);
	$("#constant").text(data.string.pg1s2);
	$("#operator").text(data.string.pg1s3);
	$("#variable").text(data.string.pg1s4);

	$("#expWd1").text(data.string.pg1s5);
	$("#expWd2").text(data.string.pg1s6);
	$("#expWd3").text(data.string.pg1s7);
	$("#expWd4").text(data.string.pg1s8);

	$("#cloudInstruction").text(data.string.pg1s9);

	// make the cursor wait signal till the clouds come
	/*$("#aExp1").css({"cursor":"wait"});*/


	$("#activity-page-next-btn-enabled").show(0);
	// to do on nextExp button click
	$("#activity-page-next-btn-enabled").on('click',function() {

		$(this).hide(0);

		$("#expEx"+increaseCount+", #expWd"+increaseCount).fadeOut(500);
		  increaseCount++;
			loadTimelineProgress(6,(increaseCount+1));
			if(increaseCount==1){
				$(".covertext, .cover_page, .cover_image").hide(0);
				$("#intro").effect("slide", 2000,function(){
					$("#expLabel, #expWd1").show(0).addClass('blurryText');
					$("#expEx1").show(0);
					animateExp("#expEx1");
					$("#expWd1").on("webkitAnimationEnd animationend",function(){
						$("#activity-page-next-btn-enabled").show(0);
						$("#cloud1").fadeIn(500);
					});
				});
			}else{
				$("#cloud"+increaseCount).fadeIn(500);
			$("#expWd"+increaseCount).show(0).addClass('blurryText');
			$("#expEx"+increaseCount).show(0);
			animateExp("#expEx"+increaseCount);
			// only if all examples are displayed do following or else it is not executed
			if(increaseCount == 4){
					$("#activity-page-next-btn-enabled").on('click',function(){
						$("#cloud"+increaseCount).fadeIn(500);
						$("#expLabel").fadeOut(500,function(){
							// console.log("fadeOut expLabel");
						});
						$("#expEx"+increaseCount+", #expWd"+increaseCount).fadeOut(500);
						// now make the clouds active with mouse events
						$(".cloud").css({"pointer-events":"auto"});
						$("#aExp1").css({"cursor":"defaut"});
						$("#cloudInstruction").show(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					});
				}

			$("#expWd"+increaseCount).on("webkitAnimationEnd animationend",function(){
				// only till the 3rd example is nextExp switch needed
				if(increaseCount < 5){
					console.log(increaseCount);
					$("#activity-page-next-btn-enabled").show(0);
				}

				else{
					$("#aExp1").css({"cursor":"default"});
				}

			});
			}
	});

	// what to do on cloud1 hover
	$("#cloud1").hover(function() {
		$(".cloud").hide(0);
		$("#cloud1").show(0);
		$("#expLabel, #expEx1, #expWd1").show(0);
		$("#cloudInstruction").hide(0);
	}, function() {
		$(".cloud").show(0);
		$("#expLabel, #expEx1, #expWd1").hide(0);
		$("#cloudInstruction").show(0);
	});

	// what to do on cloud2 hover
	$("#cloud2").hover(function() {
		$(".cloud").hide(0);
		$("#cloud2").show(0);
		$("#expLabel, #expEx2, #expWd2").show(0);
		$("#cloudInstruction").hide(0);
	}, function() {
		$(".cloud").show(0);
		$("#expLabel, #expEx2, #expWd2").hide(0);
		$("#cloudInstruction").show(0);
	});

	// what to do on cloud3 hover
	$("#cloud3").hover(function() {
		$(".cloud").hide(0);
		$("#cloud3").show(0);
		$("#expLabel, #expEx3, #expWd3").show(0);
		$("#cloudInstruction").hide(0);
	}, function() {
		$(".cloud").show(0);
		$("#expLabel, #expEx3, #expWd3").hide(0);
		$("#cloudInstruction").show(0);
	});

	// what to do on cloud4 hover
	$("#cloud4").hover(function() {
		$(".cloud").hide(0);
		$("#cloud4").show(0);
		$("#expLabel, #expEx4, #expWd4").show(0);
		$("#cloudInstruction").hide(0);
	}, function() {
		$(".cloud").show(0);
		$("#expLabel, #expEx4, #expWd4").hide(0);
		$("#cloudInstruction").show(0);
	});
});

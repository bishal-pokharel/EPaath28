// traingle bug animation
function traingleAnim(){
	$("#bugTriangle").animate({"top":"-10%"},1500,"linear",function(){
		$("#bugTriangle").transition({"rotate":"110deg"},1000,"linear",function(){
			$("#bugTriangle").animate({"top":"85%","left":"90%"},1500,"linear",function(){
				$("#bugTriangle").transition({"rotate":"270deg"},1000,"linear",function(){});
				$("#bugTriangle").animate({"left":"-10%"},1500,"linear",function(){
					$("#q2").fadeIn(1000,function(){
						$("#a2").fadeIn(1000,function(){
							$("#hintMan").show(0).addClass('blinkEffect');
						});
					});
				});
			});
		});
	});
}

// straight line animation
function stLineAnim(){
	$("#stLineEg > div:nth-of-type(1) > div:nth-of-type(1), #bugLeft").fadeIn(1000,function(){
		$("#bugLeft").animate({"left":"-3%"},2000,"linear",function(){
			$("#stLineEg > div:nth-of-type(1) > div:nth-of-type(1) > div > p").fadeIn(1000);
			$("#stLineEg > div:nth-of-type(1) > div:nth-of-type(2), #bugRight").fadeIn(1000,function(){
				$("#bugRight").animate({"left":"95%"},1000,"linear",function(){
					$("#stLineEg > div:nth-of-type(1) > div:nth-of-type(2)").css({"border-color":"#EE5199"});
					$("#stLineEg > div:nth-of-type(1) > div:nth-of-type(2) > p").fadeIn(1000,function(){
						$("#q1, #a1, #hintMan").fadeIn(500);
					});
				});
			});
		});
	});
}

// checking full answer for question one
function checkFullAnsStLine(){
	// check both blank spaces
			var firstIp = $("#a1 > input[type=text]:nth-of-type(1)").val().length;
			var secondIp = $("#a1 > input[type=text]:nth-of-type(2)").val().length;
			if((firstIp+secondIp) > 1){
				var term1 = $("#a1 > input[type=text]:nth-of-type(1)").val().toLowerCase();
				var term2 = $("#a1 > input[type=text]:nth-of-type(2)").val().toLowerCase();
				var expression = "5"+term1+"+"+term2;
				if(expression == "5a+b"){
					// disable the input if true
					$("#a1 > input").prop('disabled', true);
					$("#a1").css({"color":"darkgreen"});
					$("#a1info").text(data.string.pg3s3).css({"color":"green","display":"block"});
					// remove hint and show the next example button
					$("#hintMan").removeClass('blinkEffect').hide(0);
					$("#activity-page-next-btn-enabled").show(0);
					}

					else{
						$("#a1info").text(data.string.pg3s4).css({"color":"red","display":"block"});
					}
							}

}

// checking full answer for question one
function checkFullAnsTriangle(){
	// console.log("all three full and here");
	var term1 = $("#a2 > input[type=text]:nth-of-type(1)").val().toLowerCase();
	var term2 = $("#a2 > input[type=text]:nth-of-type(2)").val().toLowerCase();
	var term3 = $("#a2 > input[type=text]:nth-of-type(3)").val().toLowerCase();
	var exp = term1+term2+term3;
	if(exp == "abc" || exp == "acb" || exp == "bac" || exp == "bca" || exp == "cab" || exp == "cba"){
		// disable the input if true
		$("#a2 > input").prop('disabled', true);
		$("#a2").css({"color":"darkgreen"});
		$("#a1info").text(data.string.pg3s3).css({"color":"green","display":"block"});
		// remove hint and show the next example button
		$("#hintMan").removeClass('blinkEffect').hide(0);
		$("#activity-page-next-btn-enabled").show(0);
		}

		else{
			$("#a1info").text(data.string.pg3s4).css({"color":"red","display":"block"});
		}


}

// checking full answer for question one
function checkTerms(term1,term2){
	// check both blank spaces
			var firstIp = $("#aExp3Summary > table tr:nth-of-type(2) input").val().length;
			var secondIp = $("#aExp3Summary > table tr:nth-of-type(3) input").val().length;

			if((firstIp+secondIp) > 1){
				var expression = term1+"+"+term2;
				if(expression == "2+3"){
					// disable the input if true
					$("#aExp3Summary > table tr input[type=text]").prop('disabled', true);
					$("#a2").css({"color":"darkgreen"});
					$("#a1info").text(data.string.pg3s3).css({"color":"green","display":"block","top":"90%","left":"45%"});
					// remove hint and show the next example button
					$("#hintMan").removeClass('blinkEffect').fadeOut(0);
					ole.footerNotificationHandler.pageEndSetNotification();
					}

					else{
						$("#a1info").text(data.string.pg3s4).css({"color":"red","display":"block","top":"90%"});
					}
							}

}

$(document).ready(function() {

	loadTimelineProgress(1,1);
	// pull all data
	$("#intro").text(data.string.pg3s1);
	$("#q1").text(data.string.pg3s2);
	$("#a1info").text(data.string.pg3s3);
	$("#hintLine").text(data.string.pg3s5);
	$("#q2").text(data.string.pg3s6);
	$("#aExp3Summary > table caption").text(data.string.pg3s8);
	$("#aExp3Summary > table tr:nth-of-type(1) > th:nth-of-type(1)").text(data.string.pg3s9);
	$("#aExp3Summary > table tr:nth-of-type(1) > th:nth-of-type(2)").text(data.string.pg3s10);


// for the line example
	// at page start the stLineEg animation
	stLineAnim();



	$("#hintMan").addClass('blinkEffect');
	// autofocus first input on load
	$("#a1 > input[type=text]:nth-of-type(1)").focus();
	// event handler for the inputs in stLine example
	/*keyup is used instead of keypress - to get the value which is
	entered and check .note: change happens after keypress so keyup is
	sensible to be used*/
	$("#a1 > input[type=text]:nth-of-type(1)").keyup(function() {
		// if other than alphabets remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		$(this).val( $(this).val().replace(/[^a-zA-Z]/g,''));
		// get the input value and change it to toLowerCase
		var inp = $(this).val().toLowerCase();
		// check for the input answer
		if(inp == "a"){
			$("#a1 > input[type=text]:nth-of-type(1)").css({"color":"darkgreen"});
			// check full answer
			checkFullAnsStLine();
		}

		else{
			$("#a1 > input[type=text]:nth-of-type(1)").css({"color":"red"});
			checkFullAnsStLine();
		}

	});

	$("#a1 > input[type=text]:nth-of-type(2)").keyup(function() {
		// if other than alphabets remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		 $(this).val( $(this).val().replace(/[^a-zA-Z]/g,''));
		 // get the input value and change it to toLowerCase
		var inp = $(this).val().toLowerCase();
		// check for the input answer
		if(inp == "b" ){
			$("#a1 > input[type=text]:nth-of-type(2)").css({"color":"darkgreen"});
				checkFullAnsStLine();
		}

		else{
			$("#a1 > input[type=text]:nth-of-type(2)").css({"color":"red"});
			checkFullAnsStLine();
		}

	});


	// what to do on hovering for hint
	$("#hintMan").hover(function() {
		/* Stuff to do when the mouse enters the element */
		$("#hintMan").attr('src','activity/grade7/math/math_algebraicexp_01/page3/image/gotit.png');
		$("#hintLine").show(0);
		$("#hintMan").removeClass('blinkEffect');

	}, function() {
		/* Stuff to do when the mouse leaves the element */
		$("#hintMan").attr('src', 'activity/grade7/math/math_algebraicexp_01/page3/image/hint.png');
		$("#hintLine").hide(0);
		$("#hintMan").addClass('blinkEffect');
	});


// for traingle example
		$("#activity-page-next-btn-enabled").on('click', function() {
			$("#a1info").hide(0);
			$("#activity-page-next-btn-enabled").hide(0);
			$("#stLineEg").fadeOut(500);
			$("#hintLine").text(data.string.pg3s7);
			$("#triangleEg").fadeIn(500,function(){
				traingleAnim();
			});
		});

// check for the answer in traingle example
	$("#a2 > input").keyup(function() {
		$("#hintLine").text(data.string.pg3s7);
		// if other than alphabets remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		 $(this).val( $(this).val().replace(/[^a-zA-Z]/g,''));

		 // check all blank spaces
			var firstIp = $("#a2 > input[type=text]:nth-of-type(1)").val().length;
			var secondIp = $("#a2 > input[type=text]:nth-of-type(2)").val().length;
			var thirdIp = $("#a2 > input[type=text]:nth-of-type(3)").val().length;
			if((firstIp+secondIp+thirdIp) > 2){
				checkFullAnsTriangle();
			}
	});

// for summary
		$("#activity-page-next-btn-enabled").on('click', function() {
			$("#a1info").hide(0);
			$("#hintLine").text(data.string.pg3s11);
			$("#activity-page-next-btn-enabled").hide(0);
			$("#triangleEg").fadeOut(500);
			$("#hintMan").show(0).addClass('blinkEffect');
			$("#aExp3Summary").fadeIn(500);
		});

// check for the answer in summary
	$("#aExp3Summary > table tr input").keyup(function() {
		// console.log("i am in");

		// if other than alphabets remove it => The g means Global, and causes the replace call to replace all matches, not just the first one
		 $(this).val( $(this).val().replace(/[^0-9]/g,''));

		var term1 = $("#aExp3Summary > table tr:nth-of-type(2) input").val();
		var term2 = $("#aExp3Summary > table tr:nth-of-type(3) input").val();


		// for first term
		if(term1 == "2"){
			$("#aExp3Summary > table tr:nth-of-type(2) input").css({"color":"green"});
			checkTerms(term1,term2);
		}
		else{
			$("#aExp3Summary > table tr:nth-of-type(2) input").css({"color":"#E70000"});
			checkTerms(term1,term2);
		}

		// for second term
		if(term2 == "3"){
			$("#aExp3Summary > table tr:nth-of-type(3) input").css({"color":"green"});
			checkTerms(term1,term2);
		}
		else{
			$("#aExp3Summary > table tr:nth-of-type(3) input").css({"color":"#E70000"});
			checkTerms(term1,term2);
		}

	});

});

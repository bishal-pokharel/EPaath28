// duration for timing of animation
var speed=300;
// duration for delay given after blinking effect animation
// this is three times the duration of css animation highlightPink
var late=1500;

// fifth step animation
function fifthStepAnim(){
	var step4 = $("#solution > p:nth-of-type(4)");
	var step5 = $("#solution > p:nth-of-type(5)");

	step5.children('span:nth-of-type(1)').velocity({opacity:"1"},speed,function(){
		step5.children('span:nth-of-type(2)').velocity({opacity:"1"},speed,function(){
			step5.children('sup:nth-of-type(1)').velocity({opacity:"1"},speed,function(){
				step5.children('span:nth-of-type(3)').velocity({opacity:"1"},speed,function(){
					step5.children('span:nth-of-type(4)').velocity({opacity:"1"},speed,function(){
						// blink effect
						step5.children('span:nth-of-type(4)').addClass('highlightPink');
						step4.children('span:nth-of-type(7)').addClass('highlightPink');
						step4.children('span:nth-of-type(8)').addClass('highlightPink');
						step4.children('span:nth-of-type(9)').addClass('highlightPink');
						step4.children('sup:nth-of-type(n+2)').addClass('highlightPink');

						$("#solution > ref:nth-of-type(2)").fadeIn(0,function(){
							// last answer
							$("#solution > strong:nth-of-type(4)").delay(late).fadeIn(500,function(){
								ole.footerNotificationHandler.pageEndSetNotification();
							});
						});
					});
				});
			});
		});
	});
}

// fourth step animation
function fourthStepAnim(){
	var step3 = $("#solution > p:nth-of-type(3)");
	var step4 = $("#solution > p:nth-of-type(4)");

	step4.children('span:nth-of-type(1)').velocity({opacity:"1"},speed,function(){
		step4.children('span:nth-of-type(2)').velocity({opacity:"1"},speed,function(){
			step4.children('span:nth-of-type(3)').velocity({opacity:"1"},speed,function(){
				step4.children('sup:nth-of-type(1)').velocity({opacity:"1"},speed,function(){
					// add blink effect
					step3.children('sup:nth-of-type(-n+2)').addClass('highlightPink');
					step4.children('sup:nth-of-type(1)').addClass('highlightPink');
					$("#solution > ref:nth-of-type(1)").fadeIn(0,function(){
						step4.children('span:nth-of-type(4)').delay(late).velocity({opacity:"1"},speed,function(){
							step4.children('span:nth-of-type(5)').velocity({opacity:"1"},speed,function(){
								step4.children('span:nth-of-type(5)').velocity({opacity:"1"},speed,function(){
									step4.children('span:nth-of-type(6)').velocity({opacity:"1"},speed,function(){
										step4.children('span:nth-of-type(7)').velocity({opacity:"1"},speed,function(){
											step4.children('sup:nth-of-type(2)').velocity({opacity:"1"},speed,function(){
												step4.children('span:nth-of-type(8)').velocity({opacity:"1"},speed,function(){
													step4.children('span:nth-of-type(9)').velocity({opacity:"1"},speed,function(){
														step4.children('sup:nth-of-type(3)').velocity({opacity:"1"},speed,function(){
															step4.children('span:nth-of-type(10)').velocity({opacity:"1"},speed,function(){
																fifthStepAnim();
															});
														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
}


// third step animation
function thirdStepAnim(){
	var step3 = $("#solution > p:nth-of-type(3)");

	step3.children('span:nth-of-type(1)').velocity({opacity:"1"},speed,function(){
		step3.children('span:nth-of-type(2)').velocity({opacity:"1"},speed,function(){
			step3.children('span:nth-of-type(3)').velocity({opacity:"1"},speed,function(){
				step3.children('sup:nth-of-type(1)').velocity({opacity:"1"},speed,function(){
					// highlight power
					step3.children('sup:nth-of-type(1)').addClass('highlightPink');
					step3.children('span:nth-of-type(4)').delay(late).velocity({opacity:"1"},speed,function(){
						// remove blink effect to reuse later
						step3.children('sup:nth-of-type(1)').removeClass('highlightPink');
						step3.children('span:nth-of-type(5)').velocity({opacity:"1"},speed,function(){
							step3.children('sup:nth-of-type(2)').velocity({opacity:"1"},speed,function(){
								// blink effect
								step3.children('sup:nth-of-type(2)').addClass('highlightPink');
								step3.children('span:nth-of-type(6)').delay(late).velocity({opacity:"1"},speed,function(){
									// remove blink effect
									step3.children('sup:nth-of-type(2)').removeClass('highlightPink');
									step3.children('span:nth-of-type(7)').velocity({opacity:"1"},speed,function(){
										step3.children('span:nth-of-type(8)').velocity({opacity:"1"},speed,function(){
											step3.children('span:nth-of-type(9)').velocity({opacity:"1"},speed,function(){
												step3.children('sup:nth-of-type(3)').velocity({opacity:"1"},speed,function(){
													// add blink effect
													step3.children('sup:nth-of-type(3)').addClass('highlightPink');
													step3.children('span:nth-of-type(10)').delay(late).velocity({opacity:"1"},speed,function(){
														// remove blink effect
														step3.children('sup:nth-of-type(3)').removeClass('highlightPink');
														step3.children('span:nth-of-type(10)').velocity({opacity:"1"},speed,function(){
															step3.children('span:nth-of-type(11)').velocity({opacity:"1"},speed,function(){

																step3.children('sup:nth-of-type(4)').velocity({opacity:"1"},speed,function(){
																	// add blink effect
																	step3.children('sup:nth-of-type(4)').addClass('highlightPink');
																	step3.children('span:nth-of-type(12)').delay(late).velocity({opacity:"1"},speed,function(){
																		// remove blink effect
																		step3.children('sup:nth-of-type(4)').removeClass('highlightPink');
																		fourthStepAnim();
																	});
																});
															});
														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
}

// second step animation
function secondStepAnim(){
	var step1 = $("#solution > p:nth-of-type(1)");
	var step2 = $("#solution > p:nth-of-type(2)");

	step2.children('span:nth-of-type(1)').velocity({opacity:"1"},speed,function(){
		// blink step one required operand and operator
		step1.children('span:nth-of-type(3)').addClass('highlightPink');
		step1.children('span:nth-of-type(7)').addClass('highlightPink');
		step1.children('span:nth-of-type(8)').addClass('highlightPink');
		step2.children('span:nth-of-type(2)').delay(late).velocity({opacity:"1"},speed,function(){
			step1.children('span:nth-of-type(4)').addClass('highlightPink');
			 // to reuse highlightPink class again on same element make sure it is removed first
				step1.children('span:nth-of-type(7)').removeClass('highlightPink');
				step1.children('span:nth-of-type(8)').removeClass('highlightPink');
			step2.children('span:nth-of-type(3)').delay(late).velocity({opacity:"1"},speed,function(){
				step1.children('span:nth-of-type(5)').addClass('highlightPink');
				step1.children('span:nth-of-type(7)').addClass('highlightPink');
				step1.children('span:nth-of-type(8)').addClass('highlightPink');
				step2.children('span:nth-of-type(4)').delay(late).velocity({opacity:"1"},speed,function(){
				  // show the next third step instruction
				  	$("#solution > strong:nth-of-type(3)").fadeIn(1000,function(){
				  		thirdStepAnim();
				  	});
				});
			});
		});
	});
}


// first step animation
function firstStepAnim(){
	var step1 = $("#solution > p:nth-of-type(1)");
	step1.children('span:nth-of-type(1)').velocity({opacity:"1"},speed,function(){
		step1.children('span:nth-of-type(2)').velocity({opacity:"1"},speed,function(){
			step1.children('span:nth-of-type(3)').velocity({opacity:"1"},speed,function(){
				step1.children('span:nth-of-type(4)').velocity({opacity:"1"},speed,function(){
					step1.children('span:nth-of-type(5)').velocity({opacity:"1"},speed,function(){
						step1.children('span:nth-of-type(6)').velocity({opacity:"1"},speed,function(){
							step1.children('span:nth-of-type(7)').velocity({opacity:"1"},speed,function(){
								step1.children('span:nth-of-type(8)').velocity({opacity:"1"},speed,function(){
									secondStepAnim();
								});
							});
						});
					});
				});
			});
		});
	});
}

// animation for solution
function solutionAnim(){
	$("#solution > strong:nth-of-type(1)").fadeIn(500, function() {
		$("#solution > strong:nth-of-type(2)").fadeIn(500);
		$("#solution > strong:nth-of-type(2)").delay(1000).fadeOut(1000,function(){
			// first step animation
			firstStepAnim();
		});
	});
}


$(document).ready(function() {
	loadTimelineProgress(1,1);
	// pull all data
	$("#topic").text(data.string.pg4s1);
	$("#q1").text(data.string.pg4s2);
	$("#solution").children('strong:nth-of-type(1)').text(data.string.pg4s3);
	$("#solution").children('strong:nth-of-type(2)').text(data.string.pg4s4);
	$("#solution").children('strong:nth-of-type(3)').text(data.string.pg4s5);
	$("#solution").children('ref:nth-of-type(1)').text(data.string.pg4s6);
	$("#solution").children('ref:nth-of-type(2)').text(data.string.pg4s7);
	$("#solution").children('strong:nth-of-type(4)').text(data.string.pg4s8);

	ole.parseToolTip("#solution > strong:nth-of-type(2)","x+y");
	ole.parseToolTip("#solution > strong:nth-of-type(2)"," x");
	$("#solution > strong:nth-of-type(2)").children('span:nth-of-type(n+1)').css({'color':"#EC008C", "text-decoration":"none", "font-family":"comic", "cursor":"default", "border":"none"});


	// animation at first
		$("#q1").show(0).velocity({ top: "90%" },500, "easeOutBounce",function(){
			$("#activity-page-next-btn-enabled").show(0);
		});

	// what happens on clicking seeAns button
	$("#activity-page-next-btn-enabled").on('click', function() {
		$("#activity-page-next-btn-enabled").hide(0);
		$("#topic").css({"font-size":"1.5em"});
		$("#q1").css({top:"8%", width:"70%", left:"0%", color:"#EC008C"});
		$("#solution").show(0);
		$("#rectangle").velocity({width:"30%", top:"0%", left:"70%"},1000, "easeOutElastic",function(){
			solutionAnim();
		});

	});

});

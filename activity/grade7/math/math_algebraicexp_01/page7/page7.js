// fourth example show reason two
function fourExReasonTwo(){
    // hide powerRule first and second power rule for 4th example
   $("#powerRule4Ex1").hide(0);
   $("#powerRule4Ex2").fadeIn(1000);
    // blink the b2/b2 in second step
    $("#four > strong:nth-of-type(2) > b:nth-of-type(6)").addClass('blurryText');
    // after delay of 1500 show reason second
    $("#four > ref:nth-of-type(2) > b:nth-of-type(1)").delay(1500).velocity({opacity:"1"},speed,function(){
       $("#four > ref:nth-of-type(2) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
            $("#four > ref:nth-of-type(2) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                $("#four > ref:nth-of-type(2) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                    $("#four > ref:nth-of-type(2) > b:nth-of-type(5)").velocity({opacity:"1"},speed,function(){
                        $("#four > ref:nth-of-type(2) > b:nth-of-type(6)").velocity({opacity:"1"},speed,function(){
                            $("#four > ref:nth-of-type(2) > b:nth-of-type(7)").velocity({opacity:"1"},speed,function(){
                               // show last term of last line
                               $("#four > strong:nth-of-type(3) > b:nth-of-type(6)").velocity({opacity:"1"},speed,function(){
                                    $("#four > ref:nth-of-type(2) > b:nth-of-type(7)").addClass('blurryText');
                                    $("#four > strong:nth-of-type(3) > b:nth-of-type(6)").addClass('blurryText');
                                    $("#replay7").show(0);
                                    ole.footerNotificationHandler.lessonEndSetNotification(data.string.pg7n1);
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// fourth example show reason one
function fourExReasonOne(){
    // show powerRule first
   $("#powerRule4Ex1").fadeIn(1000);
    // start after delay of 1500
    $("#four > ref:nth-of-type(1) > b:nth-of-type(1)").delay(1500).velocity({opacity:"1"},speed,function(){
        $("#four > ref:nth-of-type(1) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
            $("#four > ref:nth-of-type(1) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                $("#four > ref:nth-of-type(1) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                    $("#four > ref:nth-of-type(1) > b:nth-of-type(5)").velocity({opacity:"1"},speed,function(){
                        $("#four > ref:nth-of-type(1) > b:nth-of-type(6)").velocity({opacity:"1"},speed,function(){
                            $("#four > ref:nth-of-type(1) > b:nth-of-type(7)").velocity({opacity:"1"},speed,function(){
                                // third step second term 5a/b
                                $("#four > strong:nth-of-type(3) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                    $("#four > strong:nth-of-type(3) > b:nth-of-type(4) > span:nth-of-type(2)").addClass('blurryText');
                                    $("#four > ref:nth-of-type(1) > b:nth-of-type(7)").addClass('blurryText');
                                        // show reason in sentence
                                    $("#four > p:nth-of-type(2)").css({"opacity":"1"});
                                    // after delay of 1500 show next term
                                    $("#four > strong:nth-of-type(3) > b:nth-of-type(5)").delay(1500).velocity({opacity:"1"},speed,function(){
                                        fourExReasonTwo();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// animation of third example
function fourExAnim(){
  $("#four > strong:nth-of-type(1) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
        $("#four > strong:nth-of-type(1) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
            $("#four > p:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                $("#four > strong:nth-of-type(2) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                    // second step
                   $("#four > strong:nth-of-type(2) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                        $("#four > strong:nth-of-type(2) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                            $("#four > strong:nth-of-type(2) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                $("#four > strong:nth-of-type(2) > b:nth-of-type(5)").velocity({opacity:"1"},speed,function(){
                                    $("#four > strong:nth-of-type(2) > b:nth-of-type(6)").velocity({opacity:"1"},speed,function(){
                                     // third step
                                        $("#four > strong:nth-of-type(3) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                                            $("#four > strong:nth-of-type(3) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                                $("#four > strong:nth-of-type(3) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                                                    // blink b of 5ab/b2
                                                    $("#four > strong:nth-of-type(2) > b:nth-of-type(4) > span:nth-of-type(n+3)").addClass('blurryText');
                                                    // after delay of above css animation show reason
                                                    fourExReasonOne();
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// animation of third example
function thirdExAnim(){
  $("#three > strong:nth-of-type(1) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
        $("#three > strong:nth-of-type(1) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
            $("#three > p:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                $("#three > strong:nth-of-type(2) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                    $("#three > strong:nth-of-type(2) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                        $("#three > strong:nth-of-type(2) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                            $("#three > strong:nth-of-type(2) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                $("#three > strong:nth-of-type(2) > b:nth-of-type(2)").addClass('blurryText');
                                       // show powerRule
                                     $("#powerRule3Ex").fadeIn(1000);
                                    // above css animation is 1.5 seconds long so the delay 1500 below
                                $("#three > ref:nth-of-type(1) > b:nth-of-type(1)").delay(1500).velocity({opacity:"1"},speed,function(){
                                    $("#three > ref:nth-of-type(1) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                        $("#three > ref:nth-of-type(1) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                                             $("#three > ref:nth-of-type(1) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                                $("#three > ref:nth-of-type(1) > b:nth-of-type(5)").velocity({opacity:"1"},speed,function(){
                                                    $("#three > ref:nth-of-type(1) > b:nth-of-type(6)").velocity({opacity:"1"},speed,function(){
                                                        $("#three > ref:nth-of-type(1) > b:nth-of-type(7)").velocity({opacity:"1"},speed,function(){
                                                            $("#three > p:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                                                $("#three > strong:nth-of-type(3) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                                                                    $("#three > strong:nth-of-type(3) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                                                        $("#three > ref:nth-of-type(1) > b:nth-of-type(7)").addClass('blurryText');
                                                                        // above css animation is 1.5 seconds long so the delay 1500 below
                                                                        $("#three > strong:nth-of-type(3) > b:nth-of-type(3)").delay(1500).velocity({opacity:"1"},speed,function(){
                                                                            $("#three > strong:nth-of-type(3) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                                                                        $("#replay7").show(0);
                                                                                        ole.footerNotificationHandler.lessonEndSetNotification(data.string.pg7n1);
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// animation of second example
function secondExAnim(){
  $("#two > strong:nth-of-type(1) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
        $("#two > strong:nth-of-type(1) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
            $("#two > p:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                $("#two > strong:nth-of-type(2) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                    $("#two > strong:nth-of-type(2) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                        $("#two > strong:nth-of-type(2) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                            $("#two > strong:nth-of-type(2) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                // cut the two numbers
                                $("#two > strong:nth-of-type(2) > b:nth-of-type(2) > span:nth-of-type(1)").css({"text-decoration":"line-through","color":"red"});
                                $("#two > strong:nth-of-type(2) > b:nth-of-type(2) > span:nth-of-type(3)").css({"text-decoration":"line-through","color":"red"});
                                // hide these numbers after cut
                                $("#two > strong:nth-of-type(2) > b:nth-of-type(2) > span:nth-of-type(1), #two > strong:nth-of-type(2) > b:nth-of-type(2) > span:nth-of-type(3)").delay(1000).fadeOut(200,function(){
                                $("#two > strong:nth-of-type(2) > b:nth-of-type(2)").addClass('blurryText');
                                $("#powerRule").fadeIn(1000);
                                // above css animation is 1.5 seconds long so the delay 1500 below
                                $("#two > ref:nth-of-type(1) > b:nth-of-type(1)").delay(1500).velocity({opacity:"1"},speed,function(){
                                    $("#two > ref:nth-of-type(1) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                        $("#two > ref:nth-of-type(1) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                                             $("#two > ref:nth-of-type(1) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                                $("#two > ref:nth-of-type(1) > b:nth-of-type(5)").velocity({opacity:"1"},speed,function(){
                                                    $("#two > ref:nth-of-type(1) > b:nth-of-type(6)").velocity({opacity:"1"},speed,function(){
                                                        $("#two > ref:nth-of-type(1) > b:nth-of-type(7)").velocity({opacity:"1"},speed,function(){
                                                            $("#two > p:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                                                $("#two > strong:nth-of-type(3) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                                                                    $("#two > strong:nth-of-type(3) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                                                        $("#two > ref:nth-of-type(1) > b:nth-of-type(7)").addClass('blurryText');
                                                                        // above css animation is 1.5 seconds long so the delay 1500 below
                                                                        $("#two > strong:nth-of-type(3) > b:nth-of-type(3)").delay(1500).velocity({opacity:"1"},speed,function(){
                                                                            $("#two > strong:nth-of-type(3) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                                                                            $("#replay7").show(0);
                                                                                            ole.footerNotificationHandler.lessonEndSetNotification(data.string.pg7n1);
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// animation of first example
function firstExAnim(){
  $("#one > strong:nth-of-type(1) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
        $("#one > strong:nth-of-type(1) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
            $("#one > p:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                $("#one > strong:nth-of-type(2) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                    $("#one > strong:nth-of-type(2) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                        $("#one > strong:nth-of-type(2) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                            $("#one > strong:nth-of-type(2) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                $("#one > strong:nth-of-type(2) > b:nth-of-type(2)").addClass('blurryText');
                                    $("#powerRule").fadeIn(1000);
                                // above css animation is 1.5 seconds long so the delay 1500 below
                                $("#one > ref:nth-of-type(1) > b:nth-of-type(1)").delay(1500).velocity({opacity:"1"},speed,function(){
                                    $("#one > ref:nth-of-type(1) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                        $("#one > ref:nth-of-type(1) > b:nth-of-type(3)").velocity({opacity:"1"},speed,function(){
                                             $("#one > ref:nth-of-type(1) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                                $("#one > ref:nth-of-type(1) > b:nth-of-type(5)").velocity({opacity:"1"},speed,function(){
                                                    $("#one > ref:nth-of-type(1) > b:nth-of-type(6)").velocity({opacity:"1"},speed,function(){
                                                        $("#one > ref:nth-of-type(1) > b:nth-of-type(7)").velocity({opacity:"1"},speed,function(){
                                                            $("#one > p:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                                                $("#one > strong:nth-of-type(3) > b:nth-of-type(1)").velocity({opacity:"1"},speed,function(){
                                                                    $("#one > strong:nth-of-type(3) > b:nth-of-type(2)").velocity({opacity:"1"},speed,function(){
                                                                        $("#one > ref:nth-of-type(1) > b:nth-of-type(7)").addClass('blurryText');
                                                                        // above css animation is 1.5 seconds long so the delay 1500 below
                                                                        $("#one > strong:nth-of-type(3) > b:nth-of-type(3)").delay(1500).velocity({opacity:"1"},speed,function(){
                                                                            $("#one > strong:nth-of-type(3) > b:nth-of-type(4)").velocity({opacity:"1"},speed,function(){
                                                                                            $("#replay7").show(0);
                                                                                            ole.footerNotificationHandler.lessonEndSetNotification(data.string.pg7n1);
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// this speed for animation duration
var speed=500;

$(document).ready(function() {

    /*load the progress bar on this page with one page*/
    loadTimelineProgress(1,1);

    $("#replay7").text(data.string.pg5n1);

	// pull all data
	$("#aExp7 > p:nth-of-type(1)").text(data.string.pg7s1);
	$("#aExp7 > strong:nth-of-type(1)").html("<span>a+b</span> "+data.string.pg7s2+" <span>a</span> "+data.string.pg7s3);
	$("#aExp7 > strong:nth-of-type(2)").html("<span>2a+b</span> "+data.string.pg7s2+" <span>2a</span> "+data.string.pg7s3);
	$("#aExp7 > strong:nth-of-type(3)").html("<span>a<sup>2</sup>+b<sup>2</sup></span> "+data.string.pg7s2+" <span>a<sup>2</sup></span> "+data.string.pg7s3);
    $("#aExp7 > strong:nth-of-type(4)").html("<span>a<sup>2</sup>+5ab</span>+<span>b<sup>2</sup></span> "+data.string.pg7s2+" <span>b<sup>2</sup></span> "+data.string.pg7s3);
    $(".example > p:nth-of-type(1)").text(data.string.pg7s4);
    $(".example > p:nth-of-type(2)").text(data.string.pg7s5);

    // on hovering the selections
	$("#aExp7 > strong:nth-of-type(-n+4)").hover(function() {
        /* Stuff to do when the mouse enters the element */
        $(this).addClass('animated swing infinite');

    }, function() {
        /* Stuff to do when the mouse leaves the element */
        $(this).removeClass('animated swing infinite');
    });

    // on clicking the selections
    $("#aExp7 > strong:nth-of-type(-n+4)").on('click', function() {
        // unbind the hover function for all the options
        $(this).removeClass('animated swing infinite');
        $("#aExp7 > p:nth-of-type(1)").hide(0);
        $("#aExp7 > strong:nth-of-type(-n+4)").off("mouseenter mouseleave");
        // get the id of the selected option
        var selected = "#"+$(this).attr("id");
        // make other then selected fall
        $("#aExp7 > strong:nth-of-type(-n+4):not("+selected+")").addClass('animated hinge');

        $(selected).velocity({top:"5%"},500,"easeOutBounce").css({"background-color":"rgba(236, 0, 162,0.8)","text-shadow":"0px 0.1em 0.2em #000000"});

        // now show the respective divs as selected options
        switch(selected){
            case "#ex1": $("#one").delay(500).fadeIn(500,function(){firstExAnim()});break;
            case "#ex2": $("#two").delay(500).fadeIn(500,function(){secondExAnim()});break;
            case "#ex3": $("#three").delay(500).fadeIn(500,function(){thirdExAnim()});break;
            case "#ex4": $("#four").delay(500).fadeIn(500,function(){fourExAnim()});break;
            default:break;
        }
    });

    // on clicking replay
    $("#replay7").on('click',function() {
        $("#replay7").hide(0);
        location.reload();
    });
// end curly braces
})

// these are contents for answers
var ansCollect=[
	{
		q:$ref+"/exercise/page1/image/1/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/1/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/1/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/1/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/1/s.png",
		correctAns:$ref+"/exercise/page1/image/1/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/2/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/2/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/2/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/2/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/2/s.png",
		correctAns:$ref+"/exercise/page1/image/2/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/3/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/3/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/3/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/3/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/3/s.png",
		correctAns:$ref+"/exercise/page1/image/3/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/4/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/4/o2.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/4/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/4/a.png", correct:"true", sign:"&#10003;"}
		],
		s:$ref+"/exercise/page1/image/4/s.png",
		correctAns:$ref+"/exercise/page1/image/4/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/5/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/5/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/5/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/5/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/5/s.png",
		correctAns:$ref+"/exercise/page1/image/5/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/6/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/6/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/6/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/6/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/6/s.png",
		correctAns:$ref+"/exercise/page1/image/6/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/7/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/7/o2.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/7/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/7/a.png", correct:"true", sign:"&#10003;"}
		],
		s:$ref+"/exercise/page1/image/7/s.png",
		correctAns:$ref+"/exercise/page1/image/7/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/8/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/8/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/8/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/8/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/8/s.png",
		correctAns:$ref+"/exercise/page1/image/8/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/9/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/9/o2.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/9/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/9/a.png", correct:"true", sign:"&#10003;"}
		],
		s:$ref+"/exercise/page1/image/9/s.png",
		correctAns:$ref+"/exercise/page1/image/9/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/10/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/10/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/10/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/10/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/10/s.png",
		correctAns:$ref+"/exercise/page1/image/10/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/11/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/11/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/11/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/11/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/11/s.png",
		correctAns:$ref+"/exercise/page1/image/11/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/12/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/12/o2.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/12/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/12/a.png", correct:"true", sign:"&#10003;"}
		],
		s:$ref+"/exercise/page1/image/12/s.png",
		correctAns:$ref+"/exercise/page1/image/12/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/13/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/13/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/13/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/13/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/13/s.png",
		correctAns:$ref+"/exercise/page1/image/13/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/14/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/14/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/14/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/14/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/14/s.png",
		correctAns:$ref+"/exercise/page1/image/14/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/15/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/15/o2.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/15/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/15/a.png", correct:"true", sign:"&#10003;"}
		],
		s:$ref+"/exercise/page1/image/15/s.png",
		correctAns:$ref+"/exercise/page1/image/15/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/16/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/16/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/16/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/16/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/16/s.png",
		correctAns:$ref+"/exercise/page1/image/16/a.png",
		isCorrect:false,
		userAns:""
	},
	{
		q:$ref+"/exercise/page1/image/17/q.png",
		a:[
		{ans:$ref+"/exercise/page1/image/17/o1.png", correct:"false", sign:"&#10007;"},
		{ans:$ref+"/exercise/page1/image/17/a.png", correct:"true", sign:"&#10003;"},
		{ans:$ref+"/exercise/page1/image/17/o2.png", correct:"false", sign:"&#10007;"}
		],
		s:$ref+"/exercise/page1/image/17/s.png",
		correctAns:$ref+"/exercise/page1/image/17/a.png",
		isCorrect:false,
		userAns:""
	}
]

// function generates random number between max and min and returns an array as collection of those random numbers
//isZero is boolean to say if a zero is required
function myRandom(count,max,min,isZero) {
 var randomGen = [];

 if (typeof isZero === "undefined") {
 	// zero required
		isZero=true;
	}

	// if minimum of range not specified - take minumum zero
	if (typeof min === "undefined") {
			min = 0;
	 			// if max is a negative number and min is not defined
			if (max < 0){
				min=max;
				max=0;
			}
		}

	// if maximum is less then the minimum then swap the values
	if(max < min){
		var temp = max;
		max=min;
		min=temp;
	}

	(function randomGet () {
				if (randomGen.length<count) {
					var rnd = Math.floor(Math.random()*(max-min+1)+min);
					// if zero is not required
					if(!isZero){
						while(rnd == 0) {
				        	rnd = Math.floor(Math.random()*(max-min+1)+min);
				    	}
					}

				    var i = randomGen.length,checkIf=false;

				    while (i--) {
				        if (randomGen[i] == rnd) {
				            randomGet();
				            checkIf=true;
				            break;
				        	}
				    	}

				    if (checkIf != true) {
				    	randomGen.push(rnd);
				    	randomGet();
				    };

					}
				else {
					// this is to just break the condition
				}

		})();

	return randomGen;
}

// next question button counter
var nextCount = 0;
// start of document ready
$(document).ready(function() {
	/*load the progress bar on this page with one page*/
	loadTimelineProgress(10,nextCount+1);
	// pull these data
	$("#mainQn").text(data.string.e1s1);
	$("#summary tr:nth-of-type(1) th:nth-of-type(1)").text(data.string.e1s2);
	$("#summary tr:nth-of-type(1) th:nth-of-type(2)").text(data.string.e1s3);
	$("#summary tr:nth-of-type(1) th:nth-of-type(3)").text(data.string.e1s4);
	$("#summary tr:nth-of-type(1) th:nth-of-type(4)").text(data.string.e1s5);

	// generate random sequence from 0 to 7 whereas 8 and 9 should always come at last
	var mySeq = myRandom(10,16,0);

	var board = $('#board');
	var nextBtn = $('#activity-page-next-btn-enabled');


	// which template to select
	function myTemplate(num){
		var source = $('#option-template').html();
		var template = Handlebars.compile(source);
		var content = {
			Q:       ansCollect[mySeq[num]].q,
			op1:     ansCollect[mySeq[num]].a[0].ans,
			op1data: ansCollect[mySeq[num]].a[0].correct,
			op1answerMark : ansCollect[mySeq[num]].a[0].sign,
			op2:     ansCollect[mySeq[num]].a[1].ans,
			op2data: ansCollect[mySeq[num]].a[1].correct,
			op2answerMark : ansCollect[mySeq[num]].a[1].sign,
			op3:     ansCollect[mySeq[num]].a[2].ans,
			op3data: ansCollect[mySeq[num]].a[2].correct,
			op3answerMark : ansCollect[mySeq[num]].a[2].sign,
		};
		var html = template(content);
		board.html(html);
	}

	myTemplate(nextCount);

	// function for listing summary table with animation
	function finishAnim () {
		$("#summary").show(0);
		var animEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		$("#summary tr:nth-of-type(1)").addClass('animated bounceInUp').one(animEnd,function(){
			$("#summary tr:nth-of-type(2)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
				$("#summary tr:nth-of-type(3)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
					$("#summary tr:nth-of-type(4)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
						$("#summary tr:nth-of-type(5)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
							$("#summary tr:nth-of-type(6)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
								$("#summary tr:nth-of-type(7)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
									$("#summary tr:nth-of-type(8)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
										$("#summary tr:nth-of-type(9)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
											$("#summary tr:nth-of-type(10)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
												$("#summary tr:nth-of-type(11)").show(0).addClass('animate zoomInDown').one(animEnd,function(){
													nextBtn.show(0);
													nextCount++;
													$("#summary tr td:nth-of-type(4) span").addClass('animated infinite flash');
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	};

	// what to do on clicking next button
	nextBtn.on('click',function (){
		nextBtn.css({"display":"none"});
		// alert("nextCount = "+nextCount);
		// increase count until counter reaches 8 then
		if(nextCount < 9){
				// console.log(ansCollect[mySeq[nextCount]].userAns);
				nextCount++;
				loadTimelineProgress(10,nextCount+1);
				myTemplate(nextCount);
			}

		else if(nextCount == 9){
			loadTimelineProgress(10,nextCount+1);
			nextBtn.css({"display":"none"});
			$("#mainQn, #board").css({"display":"none"});
			// fill the summary Table
				for(var i=0; i<10 ; i++){
					var trCount=i+2;
					$("#summary tr:nth-of-type("+trCount+") td:nth-of-type(1) img").attr("src",ansCollect[mySeq[i]].q);
					$("#summary tr:nth-of-type("+trCount+") td:nth-of-type(2) img").attr("src",ansCollect[mySeq[i]].correctAns);
					$("#summary tr:nth-of-type("+trCount+") td:nth-of-type(3) img").attr("src",ansCollect[mySeq[i]].userAns);
					// console.log("question: "+ansCollect[mySeq[i]].q+"\n"+"correctOption: "+ansCollect[mySeq[i]].a[0].ans+": "+ansCollect[mySeq[i]].a[0].correct+"\n"+"wrongOption1: "+ansCollect[mySeq[i]].a[1].ans+": "+ansCollect[mySeq[i]].a[1].correct+"\n"+"wrongOption2: "+ansCollect[mySeq[i]].a[1].ans+": "+ansCollect[mySeq[i]].a[1].correct+"\n"+"solution: "+ansCollect[mySeq[i]].s+"\n"+"correctAns: "+ansCollect[mySeq[i]].correctAns+"\n"+"isCorrect: "+ansCollect[mySeq[i]].isCorrect+"\n"+"userAns: "+ansCollect[mySeq[i]].userAns+"\n"+"******************************************************************"+"\n");
				}
			finishAnim();
		}

		else if(nextCount > 9){
			ole.activityComplete.finishingcall();
		}
	});

	// functionality for the radio buttons to select if even the label beside is clicked
	$("#board").on('click', "div.option > div",function() {
		var selected = $(this).attr("class");
		var selectedData = $("."+selected).attr("data");
		var selectedAns = $("."+selected).children('img').attr("src");
		$("."+selected).siblings('div').css({"background":"#F2E7C4"});
		$("."+selected).css({"background":"rgb(82, 198, 222)"});
		$(this).children('span').show(0);

		var summaryChildCount = nextCount +2;
		if(selectedData == "true"){
			ansCollect[mySeq[nextCount]].isCorrect = true;
			$("#summary tr:nth-of-type("+summaryChildCount+") td:nth-of-type(3)").css({"background-color":"rgba(152, 192, 0,0.5)"});
			$("#summary tr:nth-of-type("+summaryChildCount+") td:nth-of-type(4) span").css({"color":"rgba(152, 192, 0,1)"});
		}

		else if(selectedData == "false"){
			ansCollect[mySeq[nextCount]].isCorrect = false;
			$("#summary tr:nth-of-type("+summaryChildCount+") td:nth-of-type(3)").css({"background-color":"rgba(204, 42, 65,0.5)"});
			$("#summary tr:nth-of-type("+summaryChildCount+") td:nth-of-type(4) span").css({"color":"rgba(204, 42, 65,1)"});
		}
		ansCollect[mySeq[nextCount]].userAns = selectedAns;
		$(this).css('pointer-events', 'none');
		$(this).siblings('div').css('pointer-events', 'none');
		nextBtn.show(0);
	});

	// on clicking the solution button
	$('#summary tr td:nth-of-type(4) span').on('click',function () {
		$(this).removeClass("animated infinite flash");
		var solutionNum = parseInt($(this).attr("data"));
		$("#solution > img").attr("src",ansCollect[mySeq[solutionNum]].s);
		$("#solution").show(0);
		$("#summary").css({"opacity":"0.1"});
	});

	// on clicking close button on solution div
	$('#solution > span').on('click',function () {
		$("#solution").css({"display":"none"});
		$("#summary").css({"opacity":"1"});
	});

// end of document ready
})

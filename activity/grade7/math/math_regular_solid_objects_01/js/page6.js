$(function(){


	$(".headTitle").html(data.string.p6_1);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	var counter=1;
	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){

		$(this).hide(0);

		counter++;

		getHtml(counter);

	});
	$("#repeatBtn").click(function(){
		location.reload();
	});

})

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={img1:$ref+"/images/Page6/image01.png",
	 		imgs1:"firstImg"

	 		};
	 		break;
	 	}
	 	case 2:
	 	{
	 		datas={img1:$ref+"/images/Page6/animation01.gif",
	 		imgs1:"firstImg",
	 		textObj:data.string.p6_2

	 		};
	 		break;
	 	}

	 	case 3:
	 	{
	 		datas={img1:$ref+"/images/Page6/image02.png",
	 		imgs1:"firstImg",
	 		textObj:data.string.p6_3

	 		};
	 		break;

	 	}

	 	case 4:
	 	{
	 		datas={
	 			img1:$ref+"/images/Page6/image01.png",
		 		imgs1:"lastimg",
		 		textObj:[data.string.p6_2,data.string.p6_3]

	 		};
	 		break;
	 	}


	}


	return datas;
}

function getHtml(cnt)
{
	var datas=getdata(cnt);


	var source;
	loadTimelineProgress(4,cnt);
	if(cnt<4)
		source=$("#template-1").html();
	else
		source=$("#template-2").html();

	var template=Handlebars.compile(source);
	var html=template(datas);



	$("#figBox").fadeOut(10,function(){

		$(this).html(html);


	}).delay(10).fadeIn(10,function(){


		if(cnt<4)$
			('#activity-page-next-btn-enabled').show(0);
		else
		{

			ole.footerNotificationHandler.lessonEndSetNotification();
		}

	});


}

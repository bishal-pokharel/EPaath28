$(function(){


	$(".headTitle").html(data.string.p2_1);
	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);


	var counter=1;
	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){

		$(this).hide(0);

		counter++;

		getHtml(counter);

	});

	$("#repeatBtn").click(function(){
		location.reload();
	});

})

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={img1:$ref+"/images/Page2/img01.png",
	 		imgs1:"firstImg"

	 		};
	 		break;
	 	}
	 	case 2:
	 	{
	 		datas={img1:$ref+"/images/Page2/animation01.gif",
	 		imgs1:"firstImg",
	 		textObj:data.string.p2_2

	 		};
	 		break;
	 	}

	 	case 3:
	 	{
	 		datas={img1:$ref+"/images/Page2/animation02.gif",
	 		imgs1:"firstImg",
	 		textObj:data.string.p2_3

	 		};
	 		break;

	 	}

	 	case 4:
	 	{
	 		datas={img1:$ref+"/images/Page2/animation03.gif",
		 		imgs1:"firstImg",
		 		textObj:data.string.p2_4
	 		};
	 		break;
	 	}
	 	case 5:
	 	{
	 		datas={img1:$ref+"/images/Page2/animation04.gif",
		 		imgs1:"firstImg",
		 		textObj:data.string.p2_5
	 		};
	 		break;
	 	}
	 	case 6:
	 	{
	 		datas={
	 			img1:$ref+"/images/Page2/animation03.gif",
		 		imgs1:"lastimg",
		 		textObj:[data.string.p2_2,data.string.p2_3,data.string.p2_4,data.string.p2_5]

	 		};
	 		break;
	 	}


	}


	return datas;
}

function getHtml(cnt)
{
	var datas=getdata(cnt);


	var source;
	loadTimelineProgress(6,cnt);
	if(cnt<6)
		source=$("#template-1").html();
	else
		source=$("#template-2").html();

	var template=Handlebars.compile(source);
	var html=template(datas);



	$("#figBox").fadeOut(10,function(){

		$(this).html(html);


	}).delay(10).fadeIn(10,function(){
		if(cnt<6)$("#activity-page-next-btn-enabled").show(0);
		else
		{
			ole.footerNotificationHandler.pageEndSetNotification();
			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_1)
		}

	});


}

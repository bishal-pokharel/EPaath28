$(function(){


	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);



	var counter=1;
	$(".pageContainer").css("background", "#9c008e");
	getHtml(counter);
	$(".headTitle").html(data.lesson.chapter);
	$("#activity-page-next-btn-enabled").click(function(){
		$(".headTitle").html(data.string.p1_1);
  	$(".pageContainer").css("background", "#e6f3c8");
		$(this).hide(0);
		counter++;
		getHtml(counter);
	});

	$("#repeatBtn").click(function(){
		location.reload();
	});

})

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={img1:$ref+"/images/Page1/angles_bg.png",
			drawme_additional_class:"coverpage",
	 		imgs1:"coverpage",
	 		};
	 		break;
	 	}
		case 2:
	 	{
	 		datas={img1:$ref+"/images/Page1/wheel.gif",
	 		imgs1:"firstImg"

	 		};
	 		break;
	 	}
	 	case 3:
	 	{
	 		datas={img1:$ref+"/images/Page1/circle-final.gif",
	 		imgs1:"firstImg",
	 		textObj:data.string.p1_2

	 		};
	 		break;
	 	}

	 	case 4:
	 	{
	 		datas={img1:$ref+"/images/Page1/circle01.png",
		 		imgs1:"firstImg",
		 		labelobj:data.string.p1_3,
		 		labelid:"label1"
	 		};
	 		break;

	 	}

	 	case 5:
	 	{
	 		datas={img1:$ref+"/images/Page1/circle02.png",
		 		imgs1:"firstImg",
		 		labelobj:data.string.p1_4,
		 		labelid:"label2"
	 		};
	 		break;
	 	}
	 	case 6:
	 	{
	 		datas={img1:$ref+"/images/Page1/circle03.png",
		 		imgs1:"firstImg",
		 		labelobj:data.string.p1_5,
		 		labelid:"label3"
	 		};
	 		break;
	 	}
	 	case 7:
	 	{
	 		datas={img1:$ref+"/images/Page1/circle04.png",
		 		imgs1:"firstImg",
		 		labelobj:data.string.p1_6,
		 		labelid:"label4"
	 		};
	 		break;
	 	}


	}


	return datas;
}

function getHtml(cnt)
{
	var datas=getdata(cnt);


	var source;
	loadTimelineProgress(7,cnt);

	source=$("#template-1").html();

	var template=Handlebars.compile(source);
	var html=template(datas);
	// $(".coverpage").hide(0);


	$("#figBox").fadeOut(10,function(){

		$(this).html(html);


	}).delay(10).fadeIn(10,function(){

		if(cnt<7)$("#activity-page-next-btn-enabled").show(0);
		else
		{
			ole.footerNotificationHandler.pageEndSetNotification();
			ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_1)
		}

	});


}

var rightcounter=1, wrongCounter=0;

$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);

	$("#toDo").html(data.string.exe1);




	var counter=0;
	var totalCounter=10;

	var arrayquestion=new Array(1,2,3,4,5,6,7,8,9,10);


	var newarray=arrayquestion;

	var arrLen=newarray.length;

	myquestion(newarray[counter],counter);
	//myquestion(11,counter);

	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		$("#Imgshow").fadeOut(0);
		counter++;

		if(counter<totalCounter)
		{
			myquestion(newarray[counter],counter);
		}
		else
		{
			myquestion(11,counter);

		}

		if(counter > totalCounter)
		{
			ole.activityComplete.finishingcall();
		}
	});

	// change all input data to uppercase will work for numeric too
	// but does not matter =)
	$('.allquestion').on("keyup","#firstInput",function(){
		$(this).val(function(){
				return this.value.toUpperCase();
		});
	});


	$('.allquestion').on("click","#fConfirmBx",function(){

			var thisQuestionNum=newarray[counter];
			var inputBoxName=$('.allquestion').find('#firstInput');
			var inputText=$('.allquestion').find('#firstInput').val();

			if( inputText.length === 0 ) {

				inputBoxName.css({'border':'1px solid #ff0000'});
			}
			else if($.isNumeric(inputText))
			{
				inputBoxName.val('');
				inputBoxName.css({'border':'1px solid #ff0000'});
			}
			else
			{
				$(this).hide(0);
				inputBoxName.attr('disabled','disabled');

				var corrvalues=Array();

				if(thisQuestionNum==2)
				{

					 corrvalues=[data.string.ques_2_ans_1,data.string.ques_2_ans_2,data.string.ques_2_ans_3,data.string.ques_2_ans_4,data.string.ques_2_ans_5,data.string.ques_2_ans_6,];

				}
				else if(thisQuestionNum==3)
				{

					 corrvalues=[data.string.ques_3_ans_1,data.string.ques_3_ans_2];

				}
				else if(thisQuestionNum==4)
				{

					 corrvalues=[data.string.ques_4_ans_1,data.string.ques_4_ans_2,data.string.ques_4_ans_3,data.string.ques_4_ans_4,data.string.ques_4_ans_5,data.string.ques_4_ans_6];

				}

				var upperText=inputText.toUpperCase();

				var isInArray=jQuery.inArray(upperText, corrvalues);


				if(isInArray===-1)
				{
					wrongCounter++;
					$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
         			$("#activity-page-next-btn-enabled").fadeIn(10);

         			inputBoxName.css({'background':'#ff0000'});
         		}
				else
				{
					rightcounter++;
					$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
         			$("#activity-page-next-btn-enabled").fadeIn(10);
         			inputBoxName.css({'background':'#30D60F'});
				}

			}

	});//click



	$('.allquestion').on("click",".optionBtn",function(){

		var id=$(this).attr('id');

		if($(this).hasClass('confrimBtn'))
		{
			$('.allquestion').find(".optionBtn").removeClass('confrimBtn').addClass('confrimDoneBtn');


			if(id==="fConfirmBx3")
			{
				rightcounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
         		$("#activity-page-next-btn-enabled").fadeIn(10);
         		$(this).removeClass('confrimDoneBtn').addClass('confrimGreenBtn');
			}
			else
			{
				wrongCounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
         		$("#activity-page-next-btn-enabled").fadeIn(10);

         		$("#fConfirmBx3").removeClass('confrimDoneBtn').addClass('confrimGreenBtn');
			}
		}
	});//click


	$('.allquestion').on("click","#allConfirmBtn",function(){

		var anstrue=true;

		var input1=$('.allquestion').find('#myfirstInput');
		var input2=$('.allquestion').find('#mysecInput');
		var input3=$('.allquestion').find('#mythrdInput');

		var val1=input1.val();
		var val2=input2.val();
		var val3=input3.val();

		if( val1.length === 0  || val2.length === 0   || val3.length === 0 ) {

			if( val1.length === 0 )
			{
				input1.css({'border':'1px solid #ff0000'});
			}
			if( val2.length === 0 )
			{
				input2.css({'border':'1px solid #ff0000'});
			}
			if( val3.length === 0 )
			{
				input3.css({'border':'1px solid #ff0000'});
			}
		}
		else
		{
			$(this).hide(0);

			input1.attr('disabled','disabled');
			input2.attr('disabled','disabled');
			input3.attr('disabled','disabled');

			$('.allquestion').find('.innerInput').each(function(index, element) {

				var corr=$(this).attr('ansval');
				var answer=$(this).val();

				if(answer!=corr)
				{
					$(this).css({'background':'#ff0000'});

					anstrue=false;
				}
				else
				{
					$(this).css({'background':'#30D60F'});
				}
			});

			if(anstrue==false)
			{
				wrongCounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
         		$("#activity-page-next-btn-enabled").fadeIn(10);
			}
			else
			{
				rightcounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
         		$("#activity-page-next-btn-enabled").fadeIn(10);
			}
		}


	});//click

});


function getQuestion($quesNo,counter)
{
	var quesList;


	switch($quesNo)
	{
		case 1:
		{
			quesList={
				imgsrc:$ref+"/exercise/images/img_1.png",
				boysrc:$ref+"/exercise/images/img_2.png",
				myfirstText:data.string.ques_1
			}
			break;
		}
		case 2:
		{
			quesList={
				imgsrc:$ref+"/exercise/images/img_3.png",
				inputBx:"firstInput",
				myfirstText:data.string.ques_2,
				cbtnId:"fConfirmBx",
				cnftxt:data.string.exe2
			}
			break;
		}
		case 3:
		{
			quesList={
				imgsrc:$ref+"/exercise/images/img_3.png",
				inputBx:"firstInput",
				myfirstText:data.string.ques_3,
				cbtnId:"fConfirmBx",
				cnftxt:data.string.exe2
			}
			break;
		}
		case 4:
		{
			quesList={
				imgsrc:$ref+"/exercise/images/img_3.png",
				inputBx:"firstInput",
				myfirstText:data.string.ques_4,
				cbtnId:"fConfirmBx",
				cnftxt:data.string.exe2
			}
			break;
		}
		case 5:
		{
			quesList={
				imgsrc:$ref+"/exercise/images/img_3.png",
				myfirstText:data.string.ques_5,
				cbtnId:"fConfirmBx1",
				cbtnIdb:"fConfirmBx2",
				cbtnIdc:"fConfirmBx3",
				optionBtn:"optionBtn",
				cnftxt:data.string.ques_5_1,
				cnftxtb:data.string.ques_5_2,
				cnftxtc:data.string.ques_5_3
			}
			break;
		}
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		{
			var titleval;
			var img1, img2;
			if($quesNo==6){
				titleval=data.string.p2_1;
				img1=$ref+"/images/Page2/img01.png";
				img2=$ref+"/images/Page2/animation03.gif";

			}
			else if($quesNo==7)
			{
				titleval=data.string.p3_1;

				img1=$ref+"/images/Page3/img01.png";
				img2=$ref+"/images/Page3/animation04.gif";
			}
			else if($quesNo==8)
			{
				titleval=data.string.p5_1;

				img1=$ref+"/images/Page5/image01.png";
				img2=$ref+"/images/Page5/animation02.gif";
			}
			else if($quesNo==9){
				titleval=data.string.p4_1;

				img1=$ref+"/images/Page4/image01.png";
				img2=$ref+"/images/Page4/animation04.gif";
			}
			else if($quesNo==10) {
				titleval=data.string.p6_1;

				img1=$ref+"/images/Page6/image01.png";
				img2=$ref+"/images/Page6/animation01.gif";

			}



			quesList={
				innerTextArea:titleval,
				imgsrc:img1,
				imgsrc2:img2,
				cnftxt1:data.string.ques_6_1,
				cnftxt2:data.string.ques_6_2,
				cnftxt3:data.string.ques_6_3,
				input1:"myfirstInput",
				input2:"mysecInput",
				input3:"mythrdInput",
				cnftxt4:data.string.exe2,
				cbtnId:"allConfirmBtn",
				ansval1:data.string["ques_"+$quesNo+"_ans_1"],
				ansval2:data.string["ques_"+$quesNo+"_ans_2"],
				ansval3:data.string["ques_"+$quesNo+"_ans_3"]
			}
			break;
		}
		case 11:
		{
			quesList={
				rite:data.string.exe3,
				wrng:data.string.exe4,
				rnum:rightcounter,
				wnum:wrongCounter,
				allansw:[

							{imgques:$ref+"/exercise/images/img_3.png",ques:[{question:data.string.ques_2, answer:data.string.ques_2_ans_1},
							{question:data.string.ques_3, answer:data.string.ques_3_ans_1},
							{question:data.string.ques_4, answer:data.string.ques_4_ans_1},
							{question:data.string.ques_5, answer:data.string.ques_5_3}
							]},


							{imgques:$ref+"/images/Page2/animation03.gif",ques:[{question:data.string.ques_6_1, answer:data.string.ques_6_ans_1},
							{question:data.string.ques_6_2, answer:data.string.ques_6_ans_2},
							{question:data.string.ques_6_3, answer:data.string.ques_6_ans_3}
							]},

							{imgques:$ref+"/images/Page3/animation04.gif",ques:[{question:data.string.ques_6_1, answer:data.string.ques_7_ans_1},
							{question:data.string.ques_6_2, answer:data.string.ques_7_ans_2},
							{question:data.string.ques_6_3, answer:data.string.ques_7_ans_3}
							]},

							{imgques:$ref+"/images/Page5/image01.png",ques:[{question:data.string.ques_6_1, answer:data.string.ques_8_ans_1},
							{question:data.string.ques_6_2, answer:data.string.ques_8_ans_2},
							{question:data.string.ques_6_3, answer:data.string.ques_8_ans_3}
							]},

							{imgques:$ref+"/images/Page4/animation04.gif",ques:[{question:data.string.ques_6_1, answer:data.string.ques_9_ans_1},
							{question:data.string.ques_6_2, answer:data.string.ques_9_ans_2},
							{question:data.string.ques_6_3, answer:data.string.ques_9_ans_3}
							]},

							{imgques:$ref+"/images/Page6/image01.png",ques:[{question:data.string.ques_6_1, answer:data.string.ques_10_ans_1},
							{question:data.string.ques_6_2, answer:data.string.ques_10_ans_2},
							{question:data.string.ques_6_3, answer:data.string.ques_10_ans_3}
							]}

						]


			}
			break;
		}
	}

	return quesList;
}
function myquestion(questionNo,counter)
{
	var whatRandNum=Math.floor(Math.random() * 6) + 1;
	randOMNumber=whatRandNum;
	loadTimelineProgress(11,(counter+1));

	var source;

	switch(questionNo)
	{
		case 1:
			source   = $("#label-template").html();
			break;
		case 2:
		case 3:
		case 4:
			source   = $("#label-template-2").html();
			break;
		case 5:
			source   = $("#label-template-3").html();
			break;
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		{
			source   = $("#label-template-4").html();
			break;
		}
		case 11:
		{

			source   = $("#label-template-5").html();
			break;
		}

	}


	var template = Handlebars.compile(source);

	var $dataval=getQuestion(questionNo,counter)
	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);


	}).delay(100).fadeIn(10,function(){

		if(questionNo==1)
		{
			animate1();
		}
		else if(questionNo==11)
		{
			$("#activity-page-next-btn-enabled").show(0);
		}
	});
}

function animate1()
{
	$('.allquestion').find('.dragMeCenter').draggable({revert:"invalid"});

	$('.allquestion').find('.img_placeholder').droppable({
         drop: function( event, ui ) {
         	$(ui.draggable).draggable("destroy");
         	$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);
         	$("#activity-page-next-btn-enabled").fadeIn(10);
        }
    });


}

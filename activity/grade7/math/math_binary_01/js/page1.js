$(function(){

	var $next_btn = $("#activity-page-next-btn-enabled");
	var $prev_btn = $("#activity-page-prev-btn-enabled");

	loadTimelineProgress(2, 1);
	var sld_no = 1;
	$next_btn.show();
	$next_btn.click(function(){
		sld_no++;
		$next_btn.hide(0);
		getCases(sld_no);
	});
	$prev_btn.click(function(){
		sld_no--;
		$prev_btn.hide(0);
		getCases(sld_no);
	});

	// first slide call
	slide_1();

	$(".headTitle").html(data.string.p1_1);
	function getCases(slide_num){
		loadTimelineProgress(2, slide_num);
		switch(slide_num){
			case 1:
				slide_1();
			break;
			case 2:
				slide_2();
			break;
		}
	}


		function slide_1(){
			var content={
				p_class:"cover_title",
				p_data:data.lesson.chapter,
				img_class:"coverimg",
				img_src:$ref+"/images/coverpage.png"
			}
			var source = $("#general-template").html();
			var template = Handlebars.compile(source);

			var html = template(content);
			$("#cover_page").html(html);
			$next_btn.show(0);
		}


		function slide_2(){
			$("#cover_page, .coverimg, .cover_title").css("display", 'none');
			$("#decimal").html(data.string.p1_2);
			$("#binary").html(data.string.p1_3);
			$("#quinary").html(data.string.p1_4);
			for(var i=0; i<=11;i++)
			{
				$(".numberBox p:nth-child("+i+")").delay(600*i).fadeIn(10);

			}

			setTimeout(function(){
				ole.footerNotificationHandler.pageEndSetNotification();
			},7500);
		}

	// $(".covertext").html(eval("data.lesson.chapter"));
	// $(".cover_cross").on("click", function(){
	// 	$(".covertext, .cover_cross, .cover_page").hide(0);
	// 		$("#decimal").html(data.string.p1_2);
	// 		$("#binary").html(data.string.p1_3);
	// 		$("#quinary").html(data.string.p1_4);
	// 	for(var i=0; i<=11;i++)
	// 	{
	// 		$(".numberBox p:nth-child("+i+")").delay(600*i).fadeIn(10);
	//
	// 	}
	//
	// 	setTimeout(function(){
	// 		ole.footerNotificationHandler.pageEndSetNotification();
	// 	},7500);
	// });

})

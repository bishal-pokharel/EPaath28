
var inputThisValue=0;


var countDivision=0;

var changeDecimal=0;
$(function(){


	$(".headTitle").html(data.string.p4_1);

	var whatnextBtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html(whatnextBtn);

	
	$("#activity-page-next-btn-enabled").html(getArrowBtn('next'));


	var counter=1;
	
	getHtml(counter);

	$("#activity-page-next-btn-enabled").click(function(){
		
		$(this).hide(0);
		
		counter++;
		
		getHtml(counter);
		
	});

	$("#repeatBtn").click(function(){
		location.reload();
	});


	$("#figBox").on("keyup",".inbx",function(){
			
		if(/\D/g.test(this.value))
		{		
			this.value = this.value.replace(/\D/g, '');
		}
	});

	$("#figBox").on("keyup",".inbx2",function(){
					
		if(/^[01234]+$/.test(this.value))
		{		
			
		}
		else
		{
			this.value = '';
		}
	});
	

	$("#figBox").on('click','#decimalToBi',function(){

		var innerval=$("#decimalVal").val();
		if(innerval.length==0)
		{

		}
		else
		{
			$(this).hide(0);
			$("#figBox").find("#decimalVal").attr({'disabled':'disabled'});
			var whatInerval=parseInt($("#decimalVal").val());

			$("#binaryValues").html(divideBy2(whatInerval));

			$("#binaryValues2").html(divideBy2Val(whatInerval));

			$("#activity-page-next-btn-enabled").show(0);
			

		}

	});


	$("#figBox").on('click','#binaryToDeci',function(){

		var innerval=$("#binaryVal").val();
		if(innerval.length==0)
		{

		}
		else
		{
			$(this).hide(0);
			$("#figBox").find("#binaryVal").attr({'disabled':'disabled'});
			var whatInerval=parseInt($("#binaryVal").val());

			$("#decimalValues").html(multiplyBy2(whatInerval));

			$("#decimalValues2").html(whatInerval+" = ("+changeDecimal+")<sub>10</sub>");

			
			ole.footerNotificationHandler.lessonEndSetNotification();

		}

	});

	
	
	
})

function getdata(cnt)
{
	var datas;

	switch(cnt)
	{
		case 1:
	 	{
	 		datas={text1:data.string.p4_2
	 			
	 		};
	 		break;
	 	}	
	 	case 2:
	 	{
	 		datas={
	 			number:"11",
	 			remainder: data.string.p3_5,
	 			src:$ref+"/images/arrows.png",
	 			imgsrc:	$ref+"/images/number01.png"
	 		};
	 		break;
	 	}	

	 	case 3:
	 	{
	 		datas={
	 			number:"214",
	 			remainder: data.string.p3_5,
	 			src:$ref+"/images/arrows.png",
	 			imgsrc:	$ref+"/images/number02.png"	
	 		};
	 		break;
	 	}	
	 	case 4:
	 	{
	 		datas={
	 			number:"723",
	 			remainder: data.string.p3_5,
	 			src:$ref+"/images/arrows.png",
	 			imgsrc:	$ref+"/images/number03.png"	
	 		};
	 		break;
	 	}	

	 	case 5:
	 	{
	 		datas={
	 			
	 			confirmval:data.string.confrim
	 		};
	 		break;
	 	}	

	 	case 6:
	 	{
	 		datas={
	 			
	 			confirmval:data.string.confrim
	 		};
	 		break;
	 	}	
	}


	return datas;
}

function getHtml(cnt)
{
	var datas=getdata(cnt);
	

	var source;
	loadTimelineProgress(6,cnt);


	if(cnt==1)
	{
		source=$("#template-1").html();
	}	
	else if(cnt==2)
	{
		$(".headTitle").html(data.string.p4_5);
		source=$("#template-2").html();
	}
	else if(cnt==3)
	{
		$(".headTitle").html(data.string.p4_5);
		source=$("#template-3").html();
	}
	else if(cnt==4)
	{
		$(".headTitle").html(data.string.p4_5);
		source=$("#template-4").html();
	}
	else if(cnt==5)
	{
		$(".headTitle").html(data.string.p4_9);
		source=$("#template-5").html();
	}
	else if(cnt==6)
	{
		$(".headTitle").html(data.string.p4_4);
		source=$("#template-6").html();
	}
	
	var template=Handlebars.compile(source);
	var html=template(datas);

	

	$("#figBox").fadeOut(10,function(){

		$(this).html(html);

	}).delay(10).fadeIn(10,function(){

		

		getAnime(cnt)
		
		
	});


}

function getAnime(cnt)
{
	switch(cnt)
	{
		case 1:
			$("#activity-page-next-btn-enabled").show(0);
			break;
		case 2:
			
			animate2();
			break;

		case 3:
			
			animate3();
			break;
		case 4:
			
			animate4();
			break;
		case 5:
			
			
			break;
		case 6:
			
			
			break;
	}
}

function animate2()
{

	//var textHtml=divideBy2(9);

	$("#conversionVal").fadeOut(10,function(){
		
	}).delay(10).fadeIn(10,function(){

		

		$("#step_1").fadeIn(10,function(){
			
			setTimeout(function(){

				$("#step_1").find(".whatfrst span").delay(500).fadeIn(100,function(){
					$("#step_1").find(".whatfrst2").addClass('bottomBorder');

					$("#step_2").delay(300).fadeIn(100,function(){
						$("#step_1").find(".remainder span").delay(500).fadeIn(100,function(){
							$("#step_2").find(".remainder span").delay(1500).fadeIn(100,function(){	

								$(".whatArrow img").fadeIn(10,function(){
									$(".firstMe").delay(100).fadeIn(10,function(){

										$(".secMe").delay(700).fadeIn(10,function(){
											$(".whatme").fadeIn(10);
											$("#activity-page-next-btn-enabled").show(0);
										});
									});
								});//fadein
							});
						});//fade
					});//fade
				});//fade

			},500);//set
		});	//fadein
	});

}


function animate3()
{

	

	$("#conversionVal").fadeOut(10,function(){
		
	}).delay(10).fadeIn(10,function(){

		

		$("#step_1").fadeIn(10,function(){
			
			setTimeout(function(){

				$("#step_1").find(".whatfrst span").delay(500).fadeIn(100,function(){
					$("#step_1").find(".whatfrst2").addClass('bottomBorder');

					$("#step_2").delay(300).fadeIn(100,function(){
						$("#step_1").find(".remainder span").delay(500).fadeIn(100,function(){
									

							$("#step_2").find(".whatfrst span").delay(1500).fadeIn(10,function(){
								$("#step_2").find(".whatfrst2").addClass('bottomBorder');
								$("#step_3").delay(300).fadeIn(100,function(){
									$("#step_2").find(".remainder span").delay(500).fadeIn(100,function(){

										$("#step_3").find(".whatfrst span").delay(1500).fadeIn(10,function(){
											$("#step_3").find(".whatfrst2").addClass('bottomBorder');
											$("#step_4").delay(300).fadeIn(100,function(){
												$("#step_3").find(".remainder span").delay(500).fadeIn(100,function(){

													$("#step_4").find(".remainder span").delay(1500).fadeIn(100,function(){

														$(".whatArrow img").fadeIn(10,function(){
															$(".firstMe").delay(100).fadeIn(10,function(){

																$(".secMe").delay(700).fadeIn(10,function(){
																	$(".whatme").fadeIn(10);
																	$("#activity-page-next-btn-enabled").show(0);
																});
															});
														});//fadein
													});
																							
												});//fade
											});//fade
										});//fade

									});//fade
								});//fade
							});//fade

						});//fade

					});//fade

				});//fade

			},500);//set
		});	//fadein
	});

}


function animate4()
{

	

	$("#conversionVal").fadeOut(10,function(){
		
	}).delay(10).fadeIn(10,function(){

		

		$("#step_1").fadeIn(10,function(){
			
			setTimeout(function(){

				$("#step_1").find(".whatfrst span").delay(500).fadeIn(100,function(){
					$("#step_1").find(".whatfrst2").addClass('bottomBorder');

					$("#step_2").delay(300).fadeIn(100,function(){
						$("#step_1").find(".remainder span").delay(500).fadeIn(100,function(){
									

							$("#step_2").find(".whatfrst span").delay(1500).fadeIn(10,function(){
								$("#step_2").find(".whatfrst2").addClass('bottomBorder');
								$("#step_3").delay(300).fadeIn(100,function(){
									$("#step_2").find(".remainder span").delay(500).fadeIn(100,function(){

										$("#step_3").find(".whatfrst span").delay(1500).fadeIn(10,function(){
											$("#step_3").find(".whatfrst2").addClass('bottomBorder');
											$("#step_4").delay(300).fadeIn(100,function(){
												$("#step_3").find(".remainder span").delay(500).fadeIn(100,function(){

													$("#step_4").find(".whatfrst span").delay(1500).fadeIn(10,function(){
														$("#step_4").find(".whatfrst2").addClass('bottomBorder');
														$("#step_5").delay(300).fadeIn(100,function(){
															$("#step_4").find(".remainder span").delay(500).fadeIn(100,function(){

																$("#step_5").find(".remainder span").delay(1500).fadeIn(100,function(){
																	$(".whatArrow img").fadeIn(10,function(){
																		$(".firstMe").delay(100).fadeIn(10,function(){

																			$(".secMe").delay(700).fadeIn(10,function(){
																				$(".whatme").fadeIn(10);
																				$("#activity-page-next-btn-enabled").show(0);
																			});
																		});
																	});//fadein
																});
															});
														});
													});													
												});//fade
											});//fade
										});//fade

									});//fade
								});//fade
							});//fade

						});//fade

					});//fade

				});//fade

			},500);//set
		});	//fadein
	});

}
function divideBy2(num)
{

	var whatName=num;
	var remainder=[];
	var dividend=[];
	countDivision=0
	var html="<div class='divideAct2' ><div class='division2'><div class='whatfrst'></div><div  class='whatfrst3'></div></div><div class='remainder remaintxt'>"+data.string.p3_5+"</div></div>";
	while(whatName!=0)
	{
		dividend[countDivision]=whatName;
		remainder[countDivision]=whatName%5;
		
		if(whatName<5)
			html=html+"<div class='divideAct2' id='step_"+countDivision+"'><div class='division2'><div class='whatfrst'>&nbsp;</div><div  class='whatfrst2'>"+dividend[countDivision]+"</div></div><div class='remainder'>"+remainder[countDivision]+"</div></div>";
		else if(countDivision==0)
			html=html+"<div class='divideAct2' id='step_"+countDivision+"'><div class='division2 '><div class='whatfrst '>5</div><div  class='whatfrst2 bottomBorder'>"+dividend[countDivision]+"</div></div><div class='remainder'>"+remainder[countDivision]+"</div></div>";
		else
			html=html+"<div class='divideAct2' id='step_"+countDivision+"'><div class='division2 '><div class='whatfrst'>5</div><div  class='whatfrst2 bottomBorder'>"+dividend[countDivision]+"</div></div><div class='remainder'>"+remainder[countDivision]+"</div></div>";
		



		whatName=Math.floor(whatName/5);
		countDivision++;

	}


	return html;
	
	
}


function divideBy2Val(num)
{

	var whatName=num;
	var remainder=[];
	var dividend=[];
	countDivision=0
	var html="";
	while(whatName>0)
	{
		dividend[countDivision]=whatName;
		remainder[countDivision]=whatName%5;
		
		html=remainder[countDivision]+ " " +html;



		whatName=Math.floor(whatName/5);
		countDivision++;

	}
	html=num+" = ("+html+")<sub>5</sub>";

	return html;
	
	
}


function multiplyBy2(num)
{

	var whatName=num;
	var remainder;
	var dividend;
	countDivision=0
	var html="";
	while(whatName!=0)
	{
		remain=	whatName%10;

		whatName=Math.floor(whatName/10);

		html= " 5<sup>"+countDivision+"</sup> X "+remain+" + "+html;

		changeDecimal=Math.pow(5, countDivision)*remain+changeDecimal; 
		countDivision++;

	}
	digitme = html.replace(/\+([^\+]*)$/,' ');
	

	return digitme;
	
	
}
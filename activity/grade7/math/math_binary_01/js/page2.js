
var inputThisValue=0;
$(function(){


    $(".headTitle").html(data.string.p1_2);

    var whatnextBtn=getSubpageMoveButton($lang,"next");
    $('#nextbtn').html(whatnextBtn);

    /*$("#repeatBtn").html(getReloadBtn());*/
    $("#arrowBtn").html(data.string.gonext);




    var counter=1;
    var cntNext=0;
    var cntNext2=4;
    getHtml(counter);

    $('#activity-page-next-btn-enabled').click(function(){

        $(this).hide();

        counter++;

        getHtml(counter);

    });

    $("#figBox").on("keyup",".inbx",function(){


        if(/\D/g.test(this.value))
        {

            this.value = this.value.replace(/\D/g, '');
        }
    });

    $("#figBox").on("keyup","#textval",function(){


        if(/\D/g.test(this.value))
        {

            this.value = this.value.replace(/\D/g, '');
        }
    });


    $("#figBox").on('click','#conform1',function(){
        var in1=parseInt($("#in1").val());
        var in2=parseInt($("#in2").val());
        var in3=parseInt($("#in3").val());
        if(in1==1 && in2==5 && in3==3)
        {
            $(this).hide();
            $("#in1, #in2, #in3").css({'background':"#2EEB15"});
            $("#in1, #in2, #in3").attr({'disabled':'disbaled'});
            $("#arrowBtn").show();
        }
        else
        {
            $("#in1, #in2, #in3").css({'background':"#ff0000"});
        }
    });



    $("#arrowBtn").click(function(){

        cntNext++;
        cntNext2++;

        if(cntNext==1)
        {
            $(".text5").show();
        }
        else if(cntNext==2)
        {
            $(".text6").show();
        }
        else if(cntNext==3)
        {
            $(".text7").show();
        }
        else if(cntNext==4)
        {
            $("#activity-page-next-btn-enabled").show()
            $(".text8").show();
            $("#nextbtn").hide();
            $(this).hide();
        }
        else if(cntNext==5)
        {

            var what1=inputThisValue;

            var noArry=Array();
            var j=0;

            while(what1!=0)
            {

                var intNo=what1%10;
                what1=parseInt(what1/10);
                noArry[j]=intNo;
                j++;

            }
            if(j==2){
                noArry[j]=0;
            }
            else if(j==1){
                noArry[1]=0;
                noArry[2]=0;
            }
            $("#figBox").find("#inx1 div").html(noArry[2]);
            $("#figBox").find("#inx2 div").html(noArry[1]);
            $("#figBox").find("#inx3 div").html(noArry[0]);


            $("#figBox").find(".secbxx").show();

        }
        else if(cntNext==6)
        {
            $(".text10").show();
        }
        else if(cntNext==7)
        {

            var what1=inputThisValue;
            var digitme=" ";

            var noArry=Array();
            var j=0;


            while(what1!=0)
            {

                var intNo=what1%10;
                what1=parseInt(what1/10);
                noArry[j]=intNo;
                digitme=intNo+" X 10<sup>"+j+"</sup>"+ " + "+digitme;
                j++;

            }

            digitme = digitme.replace(/\+([^\+]*)$/,' ');

            $(".text11").html(inputThisValue+" = { "+digitme+" } ");

            $(this).hide();
            /*$("#repeatBtn").show();*/
            ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_1);
            ole.footerNotificationHandler.pageEndSetNotification();
        }


    });

    $("#figBox").on('click','#conform2',function(){

        var textval=$("#textval").val();
        if(textval.length==0)
        {

        }
        else
        {
            $(this).hide();
            inputThisValue=parseInt($("#textval").val());

            $("#arrowBtn").show();
        }


    });


})

function getdata(cnt)
{
    var datas;

    switch(cnt)
    {
        case 1:
        {
            datas={text1:data.string.p2_1,
                text2:data.string.p2_2,
                text3:data.string.p2_3,
                text4:data.string.p2_4,
                conform1:data.string.confrim,
                text5:data.string.p2_5,
                text6:data.string.p2_6,
                text7:data.string.p2_7,
                text8:data.string.p2_8

            };
            break;
        }
        case 2:
        {
            datas={text1:data.string.p2_10,
                conform1:data.string.confrim,
                text2:data.string.p2_2,
                text3:data.string.p2_3,
                text4:data.string.p2_4,
                text5:data.string.p2_11

            };
            break;
        }


    }


    return datas;
}

function getHtml(cnt)
{
    var datas=getdata(cnt);


    var source;
    loadTimelineProgress(2,cnt);


    if(cnt==1)
    {
        source=$("#template-1").html();
    }
    else
    {
        $(".headTitle").html(data.string.p2_9);
        source=$("#template-2").html();
    }

    var template=Handlebars.compile(source);
    var html=template(datas);



    $("#figBox").fadeOut(10,function(){

        $(this).html(html);

    }).delay(10).fadeIn(10,function(){


    });


}


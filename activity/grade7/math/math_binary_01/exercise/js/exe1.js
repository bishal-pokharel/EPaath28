var initialval=0;
var finalval=0;

var q1=[];
var q2=[];
var q3=[];
var q4=[];
var a1=[];
var a2=[];
var a3=[];
var a4=[];


$(function(){

	var $whatnextbtn=getSubpageMoveButton($lang,"next");
	$("#activity-page-next-btn-enabled").html($whatnextbtn);


	$("#repeatId").html(getReloadBtn());


	var counter=0;
	var totalCounter=10;


	var rightcounter=0, wrongCounter=0;

	myquestion(counter);

	$(".allquestion").on("click",".ans",function(){

		var isCorrect=$(this).attr('corr');

		$("#showAns div").removeClass('ans');
		if(isCorrect=="yes")
		{
			$(this).css({'background':'#30D60F'});
			rightcounter++;
			$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);

		}
		else
		{
			$("#showAns div:not([corr='no'])").css({'background':'#30D60F'});
			wrongCounter++;
			$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);
		}
		$("#activity-page-next-btn-enabled").show(0);




	});

	$(".allquestion").on("keyup",".myIntinput",function(){


		if(/\D/g.test(this.value))
		{

			this.value = this.value.replace(/\D/g, '');
		}
	});

	$(".allquestion").on("click","#confirm1",function(){

		var whatIntss=$(".allquestion").find("#inputid1").val();
		if(whatIntss.length==0)
		{
			$("#inputid1").css({'border':'1px solid #ff0000'});
		}
		else
		{
			$(this).hide(0);
			initialval=parseInt($(".allquestion").find("#inputid1").val());

			$("#inputid1").attr({'disabled':'disbaled'});

			finalval=parseInt($('.allquestion').find('#inputid1').attr('corrans'));
			if(initialval==finalval)
			{
				rightcounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/correct.png').fadeIn(10);

				$("#inputid1").css({'background':'#30D60F'});
				$('#activity-page-next-btn-enabled').show(0);

			}
			else
			{
				wrongCounter++;
				$("#Imgshow").attr('src',$ref+'/exercise/images/incorrect.png').fadeIn(10);


				$("#inputid1").css({'background':'#ff0000'});

				$('#activity-page-next-btn-enabled').show(0);

			}
		}
	});



	$("#activity-page-next-btn-enabled").click(function(){
		$(this).fadeOut(0);
		counter++;

		if(counter<totalCounter)
		{
			myquestion(counter);
		}
		else
		{
			myquestion2(rightcounter,wrongCounter);

		}

		if(counter > totalCounter){
			ole.activityComplete.finishingcall();
		}
	});

	/*$("#repeatId").click(function(){
		location.reload();
	});*/
});

var randOMNumber;

function myquestion(counter)
{

	loadTimelineProgress(11,(counter+1));

	var qType = randomNumberFromRange(1, 4);
    var question = randomNumberFromRange(1, 999);

    var myquestion,myanswer;


	if (qType == 1) {
		myquestion=question;
		myanswer=FromDecimal(question,2);
		q1[q1.length]=myquestion;
		a1[a1.length]=myanswer;


	} else if (qType == 2) {
		myquestion=FromDecimal(question,2);
		myanswer=question;
		q2[q2.length]=myquestion;
		a2[a2.length]=myanswer;


	} else if (qType == 3) {

		myquestion=question;
		myanswer=FromDecimal(question,5);
		q3[q3.length]=myquestion;
		a3[a3.length]=myanswer;


	} else if (qType == 4) {
		myquestion=FromDecimal(question,5);
		myanswer=question;
		q4[q4.length]=myquestion;
		a4[a4.length]=myanswer;

	}


	var $dataval={
				myQuesion:data.string["exe_"+qType],
				confrimval:data.string.confrim,
				txt1:myquestion,
				ans: myanswer,
				types:"inputid1"
			}


	var source   = $("#label-template").html();




	var template = Handlebars.compile(source);


	var html=template($dataval);


	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#Imgshow").fadeOut(10);

	}).delay(100).fadeIn(10,function(){


	});
}

function randomNumberFromRange(min,max)
{
    var randomNumber = Math.floor(Math.random()*(max-min+1)+min);

    return randomNumber;
}
function myquestion2(right,wrong)
{
	loadTimelineProgress(11,11);
	var source   = $("#template-2").html();

	var template = Handlebars.compile(source);

	var qAns1=findAnswer(q1,a1);
	var qAns2=findAnswer(q2,a2);
	var qAns3=findAnswer(q3,a3);
	var qAns4=findAnswer(q4,a4);
	var y1=y2=y3=y4=0;
	var a=0;
	var laongques=[];

	if(qAns1.length>=1)
	{
		y1=1;
		laongques[a++]={longques:data.string.exe_1,qtype1:qAns1};
	}
	if(qAns2.length>=1)
	{
		y2=1;
		laongques[a++]={longques:data.string.exe_2,qtype1:qAns2};
	}
	if(qAns3.length>=1)
	{
		y3=1;
		laongques[a++]={longques:data.string.exe_3,qtype1:qAns3};
	}
	if(qAns4.length>=1)
	{
		y4=1;
		laongques[a++]={longques:data.string.exe_4,qtype1:qAns4};
	}


	var $dataval={
		rite:data.string.exe1,
		wrng:data.string.exe2,
		rnum:right,
		wnum:wrong,
		longques2:laongques
	}


	var html=template($dataval);

	$(".allquestion").fadeOut(10,function(){
		$(this).html(html);
		$("#toDo").html(data.string.exe3);
		$("#Imgshow").fadeOut(10);


	}).delay(100).fadeIn(10,function(){

		// $("#repeatId").delay(100).fadeIn(10);
		$("#activity-page-next-btn-enabled").show(0);
	});
}



function FromDecimal(num,toWhat)
{

	var whatName=num;
	var remainder=[];
	var dividend=[];
	countDivision=0
	var html="";
	while(whatName>0)
	{
		dividend[countDivision]=whatName;
		remainder[countDivision]=whatName%toWhat;

		html=remainder[countDivision]+ "" +html;

		whatName=Math.floor(whatName/toWhat);
		countDivision++;

	}


	return html;


}


function ToDecimal(num, fromWhat)
{

	var whatName=num;
	var remainder;
	var dividend;
	var changeDecimal;

	while(whatName!=0)
	{
		remain=	whatName%10;

		whatName=Math.floor(whatName/10);



		changeDecimal=Math.pow(fromWhat, countDivision)*remain+changeDecimal;


	}



	return changeDecimal;


}

function findAnswer(q,a)
{
	var i=0;
	var qtype=[];
	var answwer;
	for(i=0;i<q.length;i++)
	{
		qtype[i]={ ques:q[i],qans:a[i] };
	}

	return qtype;
}

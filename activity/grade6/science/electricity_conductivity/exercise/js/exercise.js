var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
// slide 1
{
contentblockadditionalclass : "greenbg",
dragdivclass:"dragdiv_2",
droptxtclass_sec:"dropdiv_2",
extratextblock:[{
	textclass:'toptxt',
	textdata: data.string.top1
}],
dragdiv: [{
	imageblock: [{
		imagestoshow: [
				{
						imgdiv: "myimg1 dragable options_lhs_sec opt1",
						imgclass: "img_drag_sec img1",
						imgid: 'ex1typ1qn1',
						imgsrc: '',
						imgans:"high_conductive",
						textclass:'box_txt',
				},{
						imgdiv: "myimg2 dragable options_lhs_sec opt2",
						imgclass: "img_drag_sec img2",
						imgid: 'ex1typ2qn1',
						imgsrc: '',
						textclass:'box_txt',
						imgans:"low_conductive",
				},{
						imgdiv: "myimg3 dragable options_lhs_sec opt3",
						imgclass: "img_drag_sec img3",
						imgid: 'ex1typ3qn1',
						imgsrc: '',
						textclass:'box_txt',
						imgans:"low_conductive",
				},{
						imgdiv: "myimg4 dragable options_lhs_sec opt1",
						imgclass: "img_drag_sec img4",
						imgid: 'ex1typ1qn1',
						imgsrc: '',
						imgans:"high_conductive",
						textclass:'box_txt',
				}
		]
	}]
}],
droptxt: [{
						txtdiv: "options_rhs b1",
						textclass: "txt_prop_sec",
						textdata:data.string.q5op1,
						drop_place:"drop_sec",
						imgans:"high_conductive",
				},
				{
						txtdiv: "options_rhs b2",
						textclass: "txt_prop_sec",
						textdata:data.string.q5op2,
						drop_place:"drop_sec",
						imgans:"low_conductive",

				}]
},
// slide 2
	{
	contentblockadditionalclass : "greenbg",
	dragdivclass:"dragdiv_2",
	droptxtclass_sec:"dropdiv_2",
	extratextblock:[{
		textclass:'toptxt',
		textdata: data.string.top2
	}],
	dragdiv: [{
		imageblock: [{
		  imagestoshow: [
		      {
		          imgdiv: "myimg1 options_lhs opt1",
		          imgclass: "img_drag img1",
		          imgid: 'ex1typ1qn1',
		          imgsrc: '',
		          imgans:"conductor",
							textclass:'box_txt',
		      },{
		          imgdiv: "myimg2 options_lhs opt2",
		          imgclass: "img_drag img2",
		          imgid: 'ex1typ2qn1',
		          imgsrc: '',
							textclass:'box_txt',
		          imgans:"semiconductor",
		      },{
		          imgdiv: "myimg3 options_lhs opt3",
		          imgclass: "img_drag img3",
		          imgid: 'ex1typ3qn1',
		          imgsrc: '',
							textclass:'box_txt',
		          imgans:"insulator",
		      },{
		          imgdiv: "myimg4 options_lhs opt1",
		          imgclass: "img_drag img4",
		          imgid: 'ex1typ1qn1',
		          imgsrc: '',
		          imgans:"conductor",
							textclass:'box_txt',
		      },{
		          imgdiv: "myimg5 options_lhs opt2",
		          imgclass: "img_drag img5",
		          imgid: 'ex1typ2qn1',
		          imgsrc: '',
							textclass:'box_txt',
		          imgans:"semiconductor",
		      },{
		          imgdiv: "myimg6 options_lhs opt3",
		          imgclass: "img_drag img6",
		          imgid: 'ex1typ3qn1',
		          imgsrc: '',
							textclass:'box_txt',
		          imgans:"insulator",
		      }
		  ]
		}]
	}],
	droptxt: [{
		          txtdiv: "options_container_rhs b1",
		          textclass: "txt_prop_third",
							textdata:data.string.q4op1,
							drop_place:"dropplace_prop_sec",
							imgans:"conductor",
		      },
		      {
		          txtdiv: "options_container_rhs b2",
		          textclass: "txt_prop_third",
							textdata:data.string.q4op2,
							drop_place:"dropplace_prop_sec",
							imgans:"semiconductor",

		      },
		      {
		          txtdiv: "options_container_rhs b3",
		          textclass: "txt_prop_third",
							textdata:data.string.q4op3,
							drop_place:"dropplace_prop_sec",
							imgans:"insulator",
		      }]
	},
	// slide 3
	{
	contentblockadditionalclass : "greenbg",
	dragdivclass:"dragdiv_1",
	droptxtclass_sec:"dropdiv_1",
	extratextblock:[{
		textclass:'toptxt',
		textdata: data.string.top3
	}],
	dragdiv: [{
		imageblock: [{
		  imagestoshow: [
		      {
		          imgdiv: "myimg1 options_container_lhs opt1",
		          imgclass: "img_drag img1",
		          imgid: 'ex1typ1qn1',
		          imgsrc: '',
		          imgans:"conductor",
							textclass:'box_txt',
		      },
		      {
		          imgdiv: "myimg2 options_container_lhs opt2",
		          imgclass: "img_drag img2",
		          imgid: 'ex1typ2qn1',
		          imgsrc: '',
							textclass:'box_txt',
		          imgans:"insulator",
		      },
		      {
		          imgdiv: "myimg3 options_container_lhs opt3",
		          imgclass: "img_drag img3",
		          imgid: 'ex1typ3qn1',
		          imgsrc: '',
							textclass:'box_txt',
		          imgans:"semiconductor",
		      }
		  ]
		}]
	}],
	droptxt: [{
		          txtdiv: "options_container_rhs b1",
		          textclass: "txt_prop",
							textdata:data.string.q2op1,
							drop_place:"dropplace_prop_sec",
							imgans:"conductor",
		      },
		      {
		          txtdiv: "options_container_rhs b2",
		          textclass: "txt_prop",
							textdata:data.string.q2op2,
							drop_place:"dropplace_prop_sec",
							imgans:"semiconductor",

		      },
		      {
		          txtdiv: "options_container_rhs b3",
		          textclass: "txt_prop",
							textdata:data.string.q2op3,
							drop_place:"dropplace_prop_sec",
							imgans:"insulator",
		      }]
	},
				// slide 4
				{
				contentblockadditionalclass : "greenbg",
				dragdivclass:"dragdiv_1",
				droptxtclass_sec:"dropdiv_1",
				extratextblock:[{
					textclass:'toptxt',
					textdata: data.string.top4
				}],
				dragdiv: [{
					imageblock: [{
					  imagestoshow: [
					      {
					          imgdiv: "myimg1 options_container_lhs opt1",
					          imgclass: "img_drag img1",
					          imgid: 'ex1typ1qn1',
					          imgsrc: '',
					          imgans:"conductor",
										textclass:'box_txt',
					      },
					      {
					          imgdiv: "myimg2 options_container_lhs opt2",
					          imgclass: "img_drag img2",
					          imgid: 'ex1typ2qn1',
					          imgsrc: '',
										textclass:'box_txt',
					          imgans:"insulator",
					      },
					      {
					          imgdiv: "myimg3 options_container_lhs opt3",
					          imgclass: "img_drag img3",
					          imgid: 'ex1typ3qn1',
					          imgsrc: '',
										textclass:'box_txt',
					          imgans:"semiconductor",
					      }
					  ]
					}]
				}],
				droptxt: [{
					          txtdiv: "options_container_rhs b1",
					          textclass: "txt_prop",
										textdata:data.string.q3op1,
										drop_place:"dropplace_prop_sec",
										imgans:"conductor",
					      },
					      {
					          txtdiv: "options_container_rhs b2",
					          textclass: "txt_prop",
										textdata:data.string.q3op2,
										drop_place:"dropplace_prop_sec",
										imgans:"semiconductor",

					      },
					      {
					          txtdiv: "options_container_rhs b3",
					          textclass: "txt_prop",
										textdata:data.string.q3op3,
										drop_place:"dropplace_prop_sec",
										imgans:"insulator",
					      }]
			},

			// slide 5
			{
			contentblockadditionalclass : "greenbg",
			dragdivclass:"dragdiv",
			dropdivclass:"dropdiv",
			extratextblock:[{
				textclass:'toptxt',
				textdata: data.string.top5
			}],
			dragdiv: [{
				imageblock: [{
				  imagestoshow: [
				      {
				          imgdiv: "myimg1 options_container opt1",
				          imgclass: "img_prop img1",
				          imgid: 'ex1typ1qn1',
				          imgsrc: '',
				          imgans:"conductor",
									textclass:'box_txt',
				      },
				      {
				          imgdiv: "myimg2 options_container opt2",
				          imgclass: "img_prop img2",
				          imgid: 'ex1typ2qn1',
				          imgsrc: '',
									textclass:'box_txt',
				          imgans:"semiconductor",
				      },
				      {
				          imgdiv: "myimg3 options_container opt3",
				          imgclass: "img_prop img3",
				          imgid: 'ex1typ3qn1',
				          imgsrc: '',
									textclass:'box_txt',
				          imgans:"insulator",
				      }
				  ]
				}]
			}],
			dropdiv: [{
				imageblock: [{
				  imagestoshow: [
				      {
				          imgdiv: "options_container_sec box_1",
				          imgclass: "img_prop_sec c1",
				          imgid: 'ckt_cond',
				          imgsrc: '',
									drop_place:"dropplace_prop",
									imgans:"conductor",

				      },
				      {
				          imgdiv: "options_container_sec box_2",
				          imgclass: "img_prop_sec c2",
				          imgid: 'ckt_semi',
				          imgsrc: '',
									drop_place:"dropplace_prop",
									imgans:"semiconductor",

				      },
				      {
				          imgdiv: "options_container_sec box_3",
				          imgclass: "img_prop_sec c3",
				          imgid: 'ckt_insul',
				          imgsrc: '',
									drop_place:"dropplace_prop",
				          imgans:"insulator",
				      }
				  ]
				}]
			}]
			},
		// slide 6
		{
		contentblockadditionalclass : "greenbg",
		dragdivclass:"dragdiv_2",
		dropdivclass:"dropdiv_2",
		extratextblock:[{
			textclass:'toptxt',
			textdata: data.string.top6
		}],
		dragdiv: [{
			imageblock: [{
				imagestoshow: [
						{
								imgdiv: " full_img",
								imgclass: "inside_full_sec img1",
								imgid: 'ckt_insul',
								imgsrc: '',
								imgans:"high_conductive",
								textclass:'box_txt',
						},{
								imgdiv: "myimg3 full_img_dragable opt1",
								imgclass: "inside_drag",
								imgid: 'stone',
								imgsrc: '',
								imgans:"insulator",
								textclass:'box_txt',
						}
				]
			}]
		}],
		dropdiv: [{
			imageblock: [{
				imagestoshow: [
						{
								imgdiv: "myimg1 options_container_rhs",
								imgclass: "hidden inside_full img1",
								imgid: 'elec_ckt_wood',
								imgsrc: '',
			          textclass: "txt_prop_third",
								textdata:data.string.q4op1,
								imgans:"conductor",
								drop_place:"dropplace_prop_sec",
						},{
								imgdiv: "myimg2 options_container_rhs",
								imgclass: "hidden inside_full",
								imgid: 'stone',
								imgsrc: '',
			          textclass: "txt_prop_third",
								textdata:data.string.q4op2,
								imgans:"semiconductor",
								drop_place:"dropplace_prop_sec",
						},{
								imgdiv: "myimg3 options_container_rhs",
								imgclass: "insulator hidden inside_full",
								imgid: 'stone',
								imgsrc: '',
			          textclass: "txt_prop_third",
								textdata:data.string.q4op3,
								imgans:"insulator",
								drop_place:"dropplace_prop_sec",
						}
				]
			}]
		}],

		},
			// slide 7
		{
		contentblockadditionalclass : "greenbg",
		dragdivclass:"dragdiv_2",
		dropdivclass:"dropdiv_2",
		extratextblock:[{
			textclass:'toptxt',
			textdata: data.string.top7
		}],
		dragdiv: [{
			imageblock: [{
				imagestoshow: [
						{
								imgdiv: " full_img",
								imgclass: "inside_full_sec img1",
								imgid: 'ckt_semi',
								imgsrc: '',
								imgans:"high_conductive",
								textclass:'box_txt',
						},{
								imgdiv: "myimg2 full_img_dragable opt1",
								imgclass: "inside_drag",
								imgid: 'stone',
								imgsrc: '',
								imgans:"semiconductor",
								textclass:'box_txt',
						}
				]
			}]
		}],
		dropdiv: [{
			imageblock: [{
				imagestoshow: [
						{
								imgdiv: "myimg1 options_container_rhs",
								imgclass: "hidden inside_full img1",
								imgid: 'elec_ckt_wood',
								imgsrc: '',
								textclass: "txt_prop_third",
								textdata:data.string.q4op1,
								imgans:"conductor",
								drop_place:"dropplace_prop_sec",
						},{
								imgdiv: "myimg2 options_container_rhs",
								imgclass: "semiconductor hidden inside_full",
								imgid: 'stone',
								imgsrc: '',
								textclass: "txt_prop_third",
								textdata:data.string.q4op2,
								imgans:"semiconductor",
								drop_place:"dropplace_prop_sec",
						},{
								imgdiv: "myimg3 options_container_rhs",
								imgclass: "insulator inside_full",
								imgid: 'stone',
								imgsrc: '',
								textclass: "txt_prop_third",
								textdata:data.string.q4op3,
								imgans:"insulator",
								drop_place:"dropplace_prop_sec",
						}
				]
			}]
		}],
		},
			// slide 8
		{
		contentblockadditionalclass : "greenbg",
		dragdivclass:"dragdiv_2",
		dropdivclass:"dropdiv_2",
		extratextblock:[{
			textclass:'toptxt',
			textdata: data.string.top8
		}],
		dragdiv: [{
			imageblock: [{
				imagestoshow: [
						{
								imgdiv: " full_img",
								imgclass: "inside_full_sec img1",
								imgid: 'ckt_cond',
								imgsrc: '',
								imgans:"high_conductive",
								textclass:'box_txt',
						},{
								imgdiv: "myimg1 full_img_dragable opt1",
								imgclass: " inside_drag",
								imgid: 'stone',
								imgsrc: '',
								imgans:"conductor",
								textclass:'box_txt',
						}
				]
			}]
		}],
		dropdiv: [{
			imageblock: [{
				imagestoshow: [
						{
								imgdiv: "myimg1 options_container_rhs",
								imgclass: "conductor hidden inside_full img1",
								imgid: 'elec_ckt_wood',
								imgsrc: '',
								textclass: "txt_prop_third",
								textdata:data.string.q4op1,
								imgans:"conductor",
								drop_place:"dropplace_prop_sec",
						},{
								imgdiv: "myimg2 options_container_rhs",
								imgclass: "semiconductor hidden inside_full",
								imgid: 'stone',
								imgsrc: '',
								textclass: "txt_prop_third",
								textdata:data.string.q4op2,
								imgans:"semiconductor",
								drop_place:"dropplace_prop_sec",
						},{
								imgdiv: "myimg3 options_container_rhs",
								imgclass: "insulator inside_full",
								imgid: 'stone',
								imgsrc: '',
								textclass: "txt_prop_third",
								textdata:data.string.q4op3,
								imgans:"insulator",
								drop_place:"dropplace_prop_sec",
						}
				]
			}]
		}],
		},
		// slide 9
	{
		contentblockadditionalclass:'bg_crm',
		extratextblock:[{
			textclass:'toptxt',
			textdata: data.string.top9
		}],
		exetype1: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques1,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						optaddclass: "class1",
						optdata: data.string.q9op1,
					},{
						optaddclass: "class2",
						optdata: data.string.q9op2,
					},{
						optaddclass: "class3",
						optdata: data.string.q9op3,
					}]

			}
		],
	dragdivclass:'lhs_drag',
	dragdiv:[{
			imageblock : [{
				imagestoshow:[{
					imgdiv:"left_big",
					imgclass:'qnimg',
					imgid : 'ex1typ2qn2',
					imgsrc: "",
				}]
			}]
		}]
	},
	// slide 10
{
	contentblockadditionalclass:'bg_crm',
	extratextblock:[{
		textclass:'toptxt',
		textdata: data.string.top10
	}],
	exetype1: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.exques1,
			sentdata: data.string.exeins,

			exeoptions: [
				{
					optaddclass: "class1",
					optdata: data.string.q10op1,
				},{
					optaddclass: "class2",
					optdata: data.string.q10op2,
				},{
					optaddclass: "class3",
					optdata: data.string.q10op3,
				}]

		}
	],
dragdivclass:'lhs_drag',
dragdiv:[{
		imageblock : [{
			imagestoshow:[{
				imgdiv:"left_big",
				imgclass:'qnimg',
				imgid : 'ex1typ2qn2',
				imgsrc: "",
			}]
		}]
	}]
},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;
	var img_src_1;
	var img_src_2;
	var img_src_3;
	var score_count = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "car", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "iron", src: imgpath+"iron.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wood", src: imgpath+"wood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stone", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elec_ckt_wood", src: imgpath+"electricial_circuit_wood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: 'images/right.png', type: createjs.AbstractLoader.IMAGE},
			{id: "ckt_cond", src: imgpath+"circuit03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ckt_semi", src: imgpath+"circuit02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ckt_insul", src: imgpath+"circuit01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "broom", src: imgpath+"broom.png", type: createjs.AbstractLoader.IMAGE},

			// exercise type 1
			{id: "ex1typ1qn1", src: imgpath+"iron.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex1typ1qn2", src: imgpath+"silver.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex1typ1qn3", src: imgpath+"copper.png", type: createjs.AbstractLoader.IMAGE},

			{id: "ex1typ2qn1", src: imgpath+"lead.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex1typ2qn2", src: imgpath+"germanium.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex1typ2qn3", src: imgpath+"cilicon.png", type: createjs.AbstractLoader.IMAGE},

			{id: "ex1typ3qn1", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex1typ3qn2", src: imgpath+"plasticbottle01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex1typ3qn3", src: imgpath+"wood.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "exe_ins_-01", src: soundAsset+"exe_ins_-01.ogg"},
			{id: "exe_ins_-02", src: soundAsset+"exe_ins_-02.ogg"},
			{id: "exe_ins_-03", src: soundAsset+"exe_ins_-03.ogg"},
			{id: "exe_ins_-04", src: soundAsset+"exe_ins_-04.ogg"},
			{id: "exe_ins_-05", src: soundAsset+"exe_ins_-05.ogg"},
			{id: "exe_ins_-06", src: soundAsset+"exe_ins_-06.ogg"},
			{id: "exe_ins_-07", src: soundAsset+"exe_ins_-07.ogg"},
			{id: "exe_ins_-08", src: soundAsset+"exe_ins_-08.ogg"},
			{id: "exe_ins_-09", src: soundAsset+"exe_ins_-09.ogg"},
			{id: "exe_ins_-10", src: soundAsset+"exe_ins_-10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		switch(countNext){
			case 4:
				put_image_third(content, countNext);
			break;
		}
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
}

	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		var count = 0;
		var cor_count = 0;
		var updateScore = 0;
		$(".toptxt").prepend((countNext+1)+". ");
	 	switch(countNext){
		 case 0:
		 	sound_player("exe_ins_-0"+(countNext+1));
			 var quesNo = rand_generator(2);
			 var secqno = quesNo+1;
			 loadimage($(".myimg1").find("img"),$(".myimg1").find("p"),preload.getResult('ex1typ1qn'+quesNo).src,eval('data.string.ex1typ1op'+quesNo));
			 loadimage($(".myimg2").find("img"),$(".myimg2").find("p"),preload.getResult('ex1typ2qn'+quesNo).src,eval('data.string.ex1typ2op'+quesNo));
			 loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex1typ3op'+quesNo));
			 loadimage($(".myimg4").find("img"),$(".myimg4").find("p"),preload.getResult('ex1typ1qn'+secqno).src,eval('data.string.ex1typ1op'+secqno));

			randomize(".dragdiv_2");

			 $(".img_drag_sec").draggable({
				 revert : true,
				 start : function(event, ui){
					 $(this).parent().css({"z-index" : "2000"});
				 }
			 });
			 $(".drop_sec").droppable({
				 accept : ".img_drag_sec",
					hoverClass: "hovered",
				 drop : function(event, ui){
					 handleDrop(event,ui, $(".img_drag_sec"),$(this),4);
				 }
			 });
			break;
			case 1:
				sound_player("exe_ins_-0"+(countNext+1));
				var quesNo = rand_generator(2);
				var secqno = quesNo+1;
				loadimage($(".myimg1").find("img"),$(".myimg1").find("p"),preload.getResult('ex1typ1qn'+quesNo).src,eval('data.string.ex1typ1op'+quesNo));
				loadimage($(".myimg2").find("img"),$(".myimg2").find("p"),preload.getResult('ex1typ2qn'+quesNo).src,eval('data.string.ex1typ2op'+quesNo));
				loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex1typ3op'+quesNo));
				loadimage($(".myimg4").find("img"),$(".myimg4").find("p"),preload.getResult('ex1typ1qn'+secqno).src,eval('data.string.ex1typ1op'+secqno));
				loadimage($(".myimg5").find("img"),$(".myimg5").find("p"),preload.getResult('ex1typ2qn'+secqno).src,eval('data.string.ex1typ2op'+secqno));
				loadimage($(".myimg6").find("img"),$(".myimg6").find("p"),preload.getResult('ex1typ3qn'+secqno).src,eval('data.string.ex1typ3op'+secqno));
				randomize(".dragdiv_2");
				$(".img_drag").draggable({
					revert : true,
 				 start : function(event, ui){
 					 $(this).parent().css({"z-index" : "2000"});
 				 }
				});
				$(".dropplace_prop_sec").droppable({
					accept : ".img_drag",
					hoverClass: "hovered",
					drop : function(event, ui){
						handleDrop(event,ui, $(".img_drag"),$(this), 6);
					}
				});
			 break;
	 case 2:
	 	sound_player("exe_ins_-0"+(countNext+1));
		 var quesNo = rand_generator(3);
		 loadimage($(".myimg1").find("img"),$(".myimg1").find("p"),preload.getResult('ex1typ1qn'+quesNo).src,eval('data.string.ex1typ1op'+quesNo));
		 loadimage($(".myimg2").find("img"),$(".myimg2").find("p"),preload.getResult('ex1typ2qn'+quesNo).src,eval('data.string.ex1typ2op'+quesNo));
		 loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex1typ3op'+quesNo));

		randomize(".dragdiv_1");

		 $(".img_drag").draggable({
			 revert : true,
		 });
		 $(".dropplace_prop_sec").droppable({
			 accept : ".img_drag",
			 hoverClass: "hovered",
			 drop : function(event, ui){
				 handleDrop(event,ui, ".img_drag",$(this), 3);
			 }
		 });
		break;
		 case 3:
		 	sound_player("exe_ins_-0"+(countNext+1));
			 var quesNo = rand_generator(3);
	 		 loadimage($(".myimg1").find("img"),$(".myimg1").find("p"),preload.getResult('ex1typ1qn'+quesNo).src,eval('data.string.ex1typ1op'+quesNo));
	 		 loadimage($(".myimg2").find("img"),$(".myimg2").find("p"),preload.getResult('ex1typ2qn'+quesNo).src,eval('data.string.ex1typ2op'+quesNo));
	 		 loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex1typ3op'+quesNo));
			 randomize(".dragdiv_1");
			 $(".img_drag").draggable({
				 revert : true,
			 });
			 $(".dropplace_prop_sec").droppable({
				 accept : ".img_drag",
				 hoverClass: "hovered",
				 drop : function(event, ui){
					 handleDrop(event,ui, ".img_drag", $(this), 3);
				 }
			 });
			break;
	 		case 4:
				sound_player("exe_ins_-0"+(countNext+1));
		      var quesNo = rand_generator(3);
				  loadimage($(".myimg1").find("img"),$(".myimg1").find("p"),preload.getResult('ex1typ1qn'+quesNo).src,eval('data.string.ex1typ1op'+quesNo));
				  loadimage($(".myimg2").find("img"),$(".myimg2").find("p"),preload.getResult('ex1typ2qn'+quesNo).src,eval('data.string.ex1typ2op'+quesNo));
				  loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex1typ3op'+quesNo));
					randomize(".dragdiv");
					$(".img_prop").draggable({
						revert : true,
					});
					$(".dropplace_prop").droppable({
						accept : ".img_prop",
						hoverClass: "hovered",
						drop : function(event, ui){
							handleDrop(event,ui, ".img_prop",$(this), 3);
						}
					});
					break;
			case 5:
				sound_player("exe_ins_-0"+(countNext+1));
				var quesNo = rand_generator(3);
				var imgreplace = $(".dragdiv_2").find(".myimg3").children().first();
			 	imgreplace.attr("src",preload.getResult('ex1typ3qn'+quesNo).src);
			  imgreplace = $(".dropdiv_2").find(".myimg3").children().first();
				imgreplace.attr("src",preload.getResult('ex1typ3qn'+quesNo).src);

				img_src_1 = 'ex1typ3qn'+quesNo;
				$(".inside_drag").draggable({
					revert : true,
				});
				$(".dropplace_prop_sec").droppable({
					accept : ".inside_drag",
					hoverClass: "hovered",
					drop : function(event, ui){
						handleDrop_sec(event, ui,".inside_drag", $(this) );
				}
				});
			break;
			case 6:
				sound_player("exe_ins_-0"+(countNext+1));
				$(".myimg3").find("img").first().attr("src",preload.getResult(img_src_1).src);
				$(".insulator").removeClass("hidden");
					var quesNo = rand_generator(3);

					var imgreplace = $(".dragdiv_2").find(".myimg2").children().first();
				 	imgreplace.attr("src",preload.getResult('ex1typ2qn'+quesNo).src);
				  imgreplace = $(".dropdiv_2").find(".myimg2").children().first();
					imgreplace.attr("src",preload.getResult('ex1typ2qn'+quesNo).src);
					img_src_2 = 'ex1typ2qn'+quesNo;

					$(".inside_drag").draggable({
						revert : true,
					});

					$(".dropplace_prop_sec").droppable({
						accept : ".inside_drag",
						hoverClass: "hovered",
						drop : function(event, ui){
							handleDrop_sec(event, ui,".inside_drag", $(this) );
					}
					});
			break;
			case 7:
				sound_player("exe_ins_-0"+(countNext+1));
				$(".myimg3").find("img").first().attr("src",preload.getResult(img_src_1).src);
				$(".insulator").removeClass("hidden");
				$(".myimg2").find("img").attr("src",preload.getResult(img_src_2).src);
				$(".semiconductor").removeClass("hidden");
					var quesNo = rand_generator(3);

					var imgreplace = $(".dragdiv_2").find(".myimg1").children().first();
				 	imgreplace.attr("src",preload.getResult('ex1typ1qn'+quesNo).src);
				  imgreplace = $(".dropdiv_2").find(".myimg1").children().first();
					imgreplace.attr("src",preload.getResult('ex1typ1qn'+quesNo).src);
					img_src_3 = 'ex1typ1qn'+quesNo;

					$(".inside_drag").draggable({
						revert : true,
					});

					$(".dropplace_prop_sec").droppable({
						accept : ".inside_drag",
						hoverClass: "hovered",
						drop : function(event, ui){
							handleDrop_sec(event, ui,".inside_drag", $(this) );
					}
					});
				break;
				case 8:
					sound_player("exe_ins_-0"+(countNext+1));
					randomize(".optionsdiv");
				break;
				case 9:
					sound_player("exe_ins_-10");
					randomize(".optionsdiv");
				break;
	 	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
  //  function to handle correct dropped events in drag and drop
		function handleDrop_sec(event, ui, dragclass, dropclass){
				createjs.Sound.stop();
				var draggableans = ui.draggable.parent().find("img").attr("data-answer");
				if(draggableans.toString().trim() == $(dropclass).parent().attr("data-answer").toString().trim())
				{
					play_correct_incorrect_sound(1);
					$(dropclass).children(".corctopt").show(0);
					$(dropclass).css({
						"border" : "4px solid #00ff00"
					});
					$(dragclass).css({"border" : "none"});
					testin.update(true);
					ui.draggable.hide(0);
					$nextBtn.show(0);
					switch (countNext) {
						case 5:
							$(".insulator").removeClass("hidden");
						break;
						case 6:
							$(".semiconductor").removeClass("hidden");
						break;
						case 7:
							$(".conductor").removeClass("hidden");
						break;
					}
				}
				else{
					play_correct_incorrect_sound(0);
					testin.update(false);
					}
			}

		function handleDrop(event, ui,dragclass, dropclass, countVal){
			createjs.Sound.stop();
			var draggableans = ui.draggable.parent().find("img").attr("data-answer");
			switch(countNext){
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					if(draggableans.toString().trim() == $(dropclass).parent().attr("data-answer").toString().trim()) {
							count++;
							updateScore++;
							item_txt= ui.draggable.parent().find("p").text();
						$(dropclass).css({
								"color"			 : "#000",
								"font-size"	 : "2vw",
							});
							$(dropclass).append("<p>"+item_txt+"</p>");
							play_correct_incorrect_sound(1);
							$(dropclass).children(".corctopt").show(0);
							ui.draggable.parent().hide(0);
							$(dropclass).css({
								"border" : "4px solid#00ff00"
							});
						 if(updateScore == countVal){
								 testin.update(true);
						 }
							cor_count++;
							if(countNext == 1){
								if(cor_count == 6)
									$nextBtn.show(0);
							}
							else if(countNext == 0){
								if(cor_count == 4)
									$nextBtn.show(0);
							}
							else{
								if(cor_count == 3)
									$nextBtn.show(0);
							}
					}
					else {
						updateScore--;
						item_txt ='';
							play_correct_incorrect_sound(0);
								testin.update(false);
					}
				break;
			}
		}


  function rand_generator(limit){
    var randNum = Math.floor((Math.random() * limit) + 1);
    return randNum;
  }

		$(".buttonsel").click(function(){
			createjs.Sound.stop();
			$(this).removeClass('forhover');
				if(ansClicked == false){
					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css({
							"background": "#bed62fff",
							"border":"5px solid #deef3c",
							"color":"white",
							'pointer-events': 'none'
						});
						$(this).siblings(".corctopt").show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						$('.buttonsel').removeClass('clock-hover');
						ansClicked = true;
						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css({
							"background":"#FF0000",
							"border":"5px solid #980000",
							"color":"white",
							'pointer-events': 'none'
						});
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
		function put_image_sec(content, count){
			if(content[count].dragdiv[0].hasOwnProperty('imageblock')){
				var imageblock = content[count].dragdiv[0].imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						// console.log(selector);
						$(selector).attr('src',image_src);
					}
				}
			}
		}
			function put_image_third(content, count){
				if(content[count].dropdiv[0].hasOwnProperty('imageblock')){
					var imageblock = content[count].dropdiv[0].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of claloadimagesses
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							// console.log(selector);
							$(selector).attr('src',image_src);
						}
					}
				}
			}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function loadimage(imgrep,imgtext,imgsrc,quesNo){
		imgrep.attr("src",imgsrc);
		imgtext.text(quesNo);
	}

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "cream",
	midtxtblock:[{
		textclass: "middletxt",
		textdata: data.string.p2s0txt
	}]
	},
	// slide1
	{
		contentblockadditionalclass: "cream",
	midtxtblock:[{
		textclass: "uptxt",
		textdata: data.string.p2s1txt
	},{
		textclass: "button fst",
		textdata: data.string.cond
	},{
		textclass: "button sec",
		textdata: data.string.insul
	},{
		textclass: "button third",
		textdata: data.string.semicond
	}]
	},
	// slide2
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.cond
		}],
		extraimgblock:[{
			imagestoshow:[{
				boximgclass:"box start",
				imgclass:'box_img b1',
				imgid:"copper",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.copper
			},{
				boximgclass:"box mid",
				imgclass:'box_img b2',
				imgid:"iron",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.iron
			},{
				boximgclass:"box end",
				imgclass:'box_img b3',
				imgid:"silver_wire",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.silver
			}]
		}],
		extratextblock:[{
		textclass:"description",
		textdata:data.string.p2s2txt
		}]
	},
	// slide3
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.cond
		}],
		extratextblock:[{
		textclass:"description",
		textdata:data.string.p2s3txt
		},{
		textclass:"fig_txt swt",
		textdata:data.string.switch
		},{
		textclass:"fig_txt bat",
		textdata:data.string.battery
		},{
		textclass:"fig_txt wire",
		textdata:data.string.wire
		},{
		textclass:"fig_txt bulb",
		textdata:data.string.bulb
		},{
		textclass:"fig_txt cop_or_ir",
		textdata:data.string.cop_or_ir
		},{
		textclass:"fig_txt plastic",
		textdata:data.string.plastic
		}],
	extraimgblock:[{
		imagestoshow:[{
			boximgclass:"box_big_left_1",
			imgclass:'box_img_left b1' ,
			imgid:"elec_ckt",
			imgsrc:'',
			textclass:"box_txt_left_1",
			textdata:data.string.elec_circuit
		},{
			boximgclass:"box_big_right_1 midright",
			imgclass:'box_img_left b2',
			imgid:"cable",
			imgsrc:'',
			textclass:"box_txt_left_1",
			textdata:data.string.wires
		}]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass:'arrow1' ,
			imgid:"arrow",
			imgsrc:'',
		},{
			imgclass:'arrow2' ,
			imgid:"arrow",
			imgsrc:'',
	 }]
	}]
	},
	// slide4
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.cond
		}],
		extratextblock:[{
		textclass:"description",
		textdata:data.string.p2s4txt
	}],
	extraimgblock:[{
		imagestoshow:[{
			boximgclass:"box_big",
			imgclass:'box_img_left',
			imgid:"elec_ckt_iron_gif",
			imgsrc:'',
			textclass:"box_txt_big",
			textdata:data.string.iron
		}]
	}],
	// svgblock:[{
	// 	svgblock:"svgcontainer",
	// }]
	},
	// slide5
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.insul
		}],
		extraimgblock:[{
			imagestoshow:[{
				boximgclass:"box start transparent",
				imgclass:'box_img b1',
				imgid:"bottle",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.plastic
			},{
				boximgclass:"box mid transparent",
				imgclass:'box_img b2',
				imgid:"wood",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.wood
			},{
				boximgclass:"box end transparent",
				imgclass:'box_img b3',
				imgid:"stone",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.stone
			}]
		}],
		extratextblock:[{
		textclass:"description",
		textdata:data.string.p2s6txt
		}]
	},
	// slide6
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.insul
		}],
		extratextblock:[{
		textclass:"lhs_text",
		textdata:data.string.p2s7txt
		},{
		textclass:"fig_txt arr_txt_1",
		textdata:data.string.arr_txt_1
		},{
		textclass:"fig_txt arr_txt_2",
		textdata:data.string.arr_txt_2
		}],
		extraimgblock:[{
			imagestoshow:[{
				boximgclass:"rhs",
				imgclass:'rhs_boxImg',
				imgid:"cable",
				imgsrc:'',
			}]
		}],
	imageblock:[{
		imagestoshow:[{
			imgclass:'arrow1_sec' ,
			imgid:"arrow",
			imgsrc:'',
		},{
			imgclass:'arrow2_sec' ,
			imgid:"arrow",
			imgsrc:'',
	 }]
	}]
	},
	// slide8
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.insul
		}],
		extratextblock:[{
		textclass:"description",
		textdata:data.string.p2s10txt
	}],
	extraimgblock:[{
		imagestoshow:[{
			boximgclass:"box_big",
			imgclass:'box_img_left',
			imgid:"elec_ckt_wood_gif",
			imgsrc:'',
			textclass:"box_txt_big",
			textdata:data.string.wood
		}]
	}],
	},
	// slide9
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.semicond
		}],
		extraimgblock:[{
			imagestoshow:[{
				boximgclass:"box start transparent",
				imgclass:'box_img b1',
				imgid:"silicon",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.silicon
			},{
				boximgclass:"box mid transparent",
				imgclass:'box_img b2',
				imgid:"lead",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.lead
			},{
				boximgclass:"box end transparent",
				imgclass:'box_img b3',
				imgid:"germanium",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.germanium
			}]
		}],
		extratextblock:[{
		textclass:"description font_vh",
		textdata:data.string.p2s9txt
		}]
	},
	// slide10
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.semicond
		}],
		extraimgblock:[{
			imagestoshow:[{
				boximgclass:"box mid transparent",
				imgclass:'box_img_sec b1',
				imgid:"pencil",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.pencil
			},{
				boximgclass:"box end transparent",
				imgclass:'box_img b2',
				imgid:"stone",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.graphite
			}]
		}],
		extratextblock:[{
		textclass:"description font_vh",
		textdata:data.string.p2s10txt_sec
	},{
		textclass:"defn_txt",
		textdata:data.string.p2pencil_txt
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass:'arrow_10' ,
			imgid:"arrow",
			imgsrc:'',
		}]
	}]
	},
	// slide11
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.semicond
		}],
		extratextblock:[{
		textclass:"description",
		textdata:data.string.p2s11txt
	}],
	extraimgblock:[{
		imagestoshow:[{
			boximgclass:"box_big",
			imgclass:'box_img_left',
			imgid:"elec_ckt_graphite_gif",
			imgsrc:'',
			textclass:"box_txt_big",
			textdata:data.string.graphite
		}]
	}],
	},
	// slide12
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.semicond
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"3txts",
		textclass:"box_txt_left",
		textdata:data.string.p2s12txt
		},{
		textclass:"slider_txt sldtxt-1",
		textdata:data.string.p2s12txt_sec
		},{
		textclass:"slider_txt sldtxt-2",
		textdata:data.string.p2s12txt_third
		}],
	selectorbox:[{
				selectorboxclass:'sldbox',
				define_next_catagory: 'next_button',
				define_prev_catagory: 'prev_button',
				textdata:data.string.diytext2,
				imageinsideclass:'image',
	}]
	},
	// slide9
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
		textclass:"toptxt purple",
		textdata:data.string.comparison
		}],
		extraimgblock:[{
			imagestoshow:[{
				boximgclass:"box start",
				imgclass:'box_img_1 b1',
				imgid:"elec_ckt_wood_gif",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.wood
			},{
				boximgclass:"box mid",
				imgclass:'box_img_1 b2',
				imgid:"elec_ckt_graphite_gif",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.graphite
			},{
				boximgclass:"box end",
				imgclass:'box_img_1 b3',
				imgid:"elec_ckt_iron_gif",
				imgsrc:'',
				textclass:"box_txt",
				textdata:data.string.iron
			}]
		}],
		extratextblock:[{
		textclass:"description font_vh",
		textdata:data.string.p2s13txt
	},{
		textclass:"btm_txt txt1",
		textdata:data.string.wood_insul
	},{
		textclass:"btm_txt txt2",
		textdata:data.string.graph_semi
	},{
		textclass:"btm_txt txt3",
		textdata:data.string.iron_cond
	}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "copper", src: imgpath+"copper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "iron", src: imgpath+"iron.png", type: createjs.AbstractLoader.IMAGE},
			{id: "silver_wire", src: imgpath+"silver_wire.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circuit_svg", src: imgpath+"electricity_circut.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "img1", src: imgpath+"semiconductors.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img2", src: imgpath+"cilicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prev", src: imgpath+"btn01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nxt", src: imgpath+"btn02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cable", src: imgpath+"submersible_cable.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elec_ckt", src: imgpath+"electricial_circuit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elec_ckt_wood", src: imgpath+"electricial_circuit_wood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elec_ckt_iron_gif", src: imgpath+"conductivity_iron.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "elec_ckt_wood_gif", src: imgpath+"conductivity_wood.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "elec_ckt_graphite_gif", src: imgpath+"conductivity_graphite.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bottle", src: imgpath+"plastic.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wood", src: imgpath+"wood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stone", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "silicon", src: imgpath+"cilicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lead", src: imgpath+"lead.png", type: createjs.AbstractLoader.IMAGE},
			{id: "germanium", src: imgpath+"germanium.png", type: createjs.AbstractLoader.IMAGE},
			{id: "electricity_circut_new", src: imgpath+"electricity_circut_new.svg", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s2_p7_1", src: soundAsset+"s2_p7_1.ogg"},
			{id: "s2_p7_2", src: soundAsset+"s2_p7_2.ogg"},
			{id: "s2_p8", src: soundAsset+"s2_p8.ogg"},
			{id: "s2_p9", src: soundAsset+"s2_p9.ogg"},
			{id: "s2_p10_1", src: soundAsset+"s2_p10_1.ogg"},
			{id: "s2_p10_2", src: soundAsset+"s2_p10_2.ogg"},
			{id: "s2_p11", src: soundAsset+"s2_p11.ogg"},
			{id: "s2_p12_1", src: soundAsset+"s2_p12_1.ogg"},
			{id: "s2_p12_2", src: soundAsset+"s2_p12_2.ogg"},
			{id: "s2_p12_3", src: soundAsset+"s2_p12_3.ogg"},
			{id: "s2_p13", src: soundAsset+"s2_p13.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	// islastpageflag ?
 	// ole.footerNotificationHandler.lessonEndSetNotification() :
 	// ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		put_speechbox_image(content, countNext);
		var currentSound;
		var showImage;
		var index = 1;
		$(".sldtxt-1, .sldtxt-2").css({display:'none'});
		$('.prev_button').attr("src",preload.getResult('prev').src);
		$('.next_button').attr("src",preload.getResult('nxt').src);

		function showImage(img_name, index){
			$('.image_inside').attr("src",preload.getResult(img_name+index).src);
			 $(".sldtxt-"+index).css({display:'block'});
			if(index == 1){
				$('.prev_button').hide(0);
				$('.next_button').show(0);
			}
			else{
				// $('.lstSldTxt').addClass("show");
				$('.prev_button').show(0);
				$('.next_button').hide(0);
				// nav_button_controls(1000);
			}
		}
		$('.next_button').click(function(){
				index = index+1;
				showImage("img",index);
				// alert(index);
				 $(".sldtxt-"+(index-1)).css({display:'none'});
		});
		$('.prev_button').click(function(){
				index = index-1;
					// navigationcontroller();
				showImage("img",index);
				// alert(index);
				$(".sldtxt-"+(index+1)).css({display:'none'});
		});
		switch(countNext){
			case 0:
				sound_player("s2_p"+(countNext+1),1)
			break;
			case 1:
				sound_player("s2_p"+(countNext+1),1)
			break;
			case 2:
				sound_player("s2_p"+(countNext+1),1)
			break;
			case 3:
				sound_player("s2_p"+(countNext+1),1)
			break;
			case 4:
				var s = Snap("#svgcontainer");
				var svg = Snap.load(preload.getResult('electricity_circut_new').src, function ( loadedFragment ) {
						  s.append(loadedFragment);
					} );
					sound_player("s2_p"+(countNext+1),1)
			break;
			case 5:
				sound_player("s2_p"+(countNext+1),1);
			break;
			case 6:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p7_1");
				current_sound.play();
				current_sound.on('complete', function(){
					sound_player("s2_p7_2",1);
				});
			break;
			case 7:
				sound_player("s2_p"+(countNext+1),1);
			break;
			case 8:
				sound_player("s2_p"+(countNext+1),1);
			break;
			case 9:
				// sound_player("s2_p"+(countNext+1),1);
				$(".defn_txt, .arrow_10").hide(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p10_1");
					current_sound.play();
					current_sound.on('complete', function(){
					$(".defn_txt, .arrow_10").fadeIn(100);
						sound_player("s2_p10_2",1);
					});
			break;
			case 10:
				sound_player("s2_p"+(countNext+1),1)
			break;
			case 11:
			$(".3txts").css("opacity","0");
			// showImage("img", 1);
			showImage("img", 1);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p12_1");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".3txts:eq(0)").css("opacity","1");
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p12_2");
					current_sound.play();
					current_sound.on('complete', function(){
						$(".3txts:eq(1)").css("opacity","1");
						sound_player("s2_p12_3",1);
					});
				});
			break;
			default:
				sound_player("s2_p"+(countNext+1),1)
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
		function put_image_sec(content, count){
			if(content[count].hasOwnProperty('extraimgblock')){
				var imageblock = content[count].extraimgblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[
		{
			textclass: "covertext",
			textdata: data.string.diy
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'diybg',
				imgsrc: ""
			}
		],
		imagelabels:[{
			imagelabelclass:'diytxt',
			imagelabeldata:data.string.diy_txt
		}]
	}]
},
// slide 1
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img insulator",
				imgid : 'wood',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.wood
			}]
		}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				// textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img",
				imgid : 'wood',
				imgsrc: "",
			}]
		}],
		dropplace:[{}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_wood,
        exeoptions:[
          {
			optaddclass:"class3",
			optdata: data.string.op1
          },{
			optaddclass:"class2",
			optdata: data.string.op2
          },{
			optaddclass:"class1",
			optdata: data.string.op3
          }]
		}],
},
// slide 2
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable conductor box_img",
				imgid : 'iron',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.iron
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img",
				imgid : 'iron',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_iron,
        exeoptions:[
          {
			optaddclass:"class1",
			optdata: data.string.op1
          },{
			optaddclass:"class2",
			optdata: data.string.op2
          },{
			optaddclass:"class3",
			optdata: data.string.op3
          }]
		}],
},
// slide 3
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img insulator",
				imgid : 'plastic',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.plastic
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img",
				imgid : 'plastic',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_plastic,
        exeoptions:[
          {
			optaddclass:"class3",
			optdata: data.string.op1
          },{
			optaddclass:"class2",
			optdata: data.string.op2
          },{
			optaddclass:"class1",
			optdata: data.string.op3
          }]
		}],
},
// slide 4
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img_sec insulator",
				imgid : 'stone',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.stone
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img_sec",
				imgid : 'stone',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_stone,
        exeoptions:[
          {
			optaddclass:"class3",
			optdata: data.string.op1
          },{
			optaddclass:"class2",
			optdata: data.string.op2
          },{
			optaddclass:"class1",
			optdata: data.string.op3
          }]
		}],
},
// slide 5
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img_sec conductor",
				imgid : 'coin',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.coin
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img_sec",
				imgid : 'coin',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_coins,
        exeoptions:[
          {
			optaddclass:"class1",
			optdata: data.string.op1
          },{
			optaddclass:"class2",
			optdata: data.string.op2
          },{
			optaddclass:"class3",
			optdata: data.string.op3
          }]
		}],
},
// slide 6
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img insulator",
				imgid : 'eraser',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.eraser
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img",
				imgid : 'eraser',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_eraser,
        exeoptions:[
          {
			optaddclass:"class3",
			optdata: data.string.op1
          },{
			optaddclass:"class2",
			optdata: data.string.op2
          },{
			optaddclass:"class1",
			optdata: data.string.op3
          }]
		}],
},
// slide 7
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img conductor",
				imgid : 'nail',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.ironnail
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img",
				imgid : 'nail',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_ironnail,
        exeoptions:[
          {
			optaddclass:"class1",
			optdata: data.string.op1
          },{
			optaddclass:"class2",
			optdata: data.string.op2
          },{
			optaddclass:"class3",
			optdata: data.string.op3
          }]
		}],
},
// slide 8
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img semiconductor",
				imgid : 'pencil',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.pencil
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img",
				imgid : 'pencil',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_pencil,
        exeoptions:[
          {
			optaddclass:"class3",
			optdata: data.string.op1
          },{
			optaddclass:"class1",
			optdata: data.string.op2
          },{
			optaddclass:"class2",
			optdata: data.string.op3
          }]
		}],
},
// slide 9
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img_sec semiconductor",
				imgid : 'silicon',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.silicon
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img_sec",
				imgid : 'silicon',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_silicon,
        exeoptions:[
          {
			optaddclass:"class3",
			optdata: data.string.op1
          },{
			optaddclass:"class1",
			optdata: data.string.op2
          },{
			optaddclass:"class2",
			optdata: data.string.op3
          }]
		}],
},
// slide 10
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "instrn diyinstext",
				textdata: data.string.p3text1
			},{
				textclass: "instrn diyinstext_1",
				textdata: data.string.p3text2
			}],
		dragdiv:[{
			imagestoshow:[{
				dragclass: "dragbox",
				imgclass: "draggable box_img_sec semiconductor",
				imgid : 'lead',
				imgsrc: "",
				textclass:"box_txt",
				textdata: data.string.lead
			}]
		}],
		dropplace:[{}],
		dropdiv:[{
			imagestoshow:[{
				dropclass: "imgcontainer",
				imgclass: "full_img ",
				imgid : 'elc_ckt',
				imgsrc: "",
				textclass:"dropbox droppable ",
			},{
				dropclass: "drop_img_div",
				imgclass: "drop_img_sec",
				imgid : 'lead',
				imgsrc: "",
			}]
		}],
		exetype4:[{
		ques_class : "question",
		textdata : data.string.qn_lead,
        exeoptions:[
          {
			optaddclass:"class3",
			optdata: data.string.op1
          },{
			optaddclass:"class1",
			optdata: data.string.op2
          },{
			optaddclass:"class2",
			optdata: data.string.op3
          }]
		}],
	},
	//slide 11
	{
	contentblockadditionalclass:"diybg",
	boxtxt:[{
		box_container:"left_box",
		text:[{
			textclass:"box_txt_sec blue",
			textdata: data.string.semicond
		},{
			textclass:"names",
			textdata: data.string.semis
		}]
	},{
		box_container:"mid_box ",
		text:[{
			textclass:"box_txt_sec green",
			textdata: data.string.insul
		},{
			textclass:"names",
			textdata: data.string.insuls
		}]
	},{
		box_container:"right_box ",
		text:[{
			textclass:"box_txt_sec yellow",
			textdata: data.string.cond
		},{
			textclass:"names",
			textdata: data.string.conducs
		}]
	},{
		box_container:"congrat_box ",
		text:[{
			textclass:"congrat",
			textdata: data.string.congrat
		}]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "congrat_img",
			imgid : 'monkey',
			imgsrc: "",
		}]
	}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var finCount = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diybg", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-1", src: imgpath+"diy/candle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wood", src: imgpath+"woodlog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elc_ckt", src: imgpath+"wood01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bulb1", src: imgpath+"bulb01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bulb2", src: imgpath+"bulb02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "silicon", src: imgpath+"cilicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coin", src: imgpath+"coin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "iron", src: imgpath+"ironrod.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plastic", src: imgpath+"plasticbottle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stone", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eraser", src: imgpath+"eraser.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nail", src: imgpath+"iron_nail.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil", src: imgpath+"pencil01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lead", src: imgpath+"lead.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cond_bulb", src: imgpath+"circuit03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "semi_bulb", src: imgpath+"circuit02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "insul_bulb", src: imgpath+"circuit01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: "images/sundar/correct-1.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p2_1", src: soundAsset+"s3_p2_1.ogg"},
			{id: "s3_p2_2", src: soundAsset+"s3_p2_2.ogg"},
			{id: "s3_p12", src: soundAsset+"s3_p12.ogg"},
			{id: "correct", src: "sounds/common/grade1/correct.ogg"},
			{id: "incorrect", src: "sounds/common/grade1/incorrect.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		put_image_sec(content, countNext);
		put_image_third(content, countNext);

		$(".mcq_container").hide(0);
		$(".diyinstext_1").hide(0);

	$(".buttonsel").click(function(){
		/*class 1 is always for the right answer. updates scoreboard and disables other click if
		right answer is clicked*/
		if($(this).hasClass("class1")){
			$(".buttonsel").removeClass('forhover');
			play_correct_incorrect_sound(1);
			$(this).css("background","#98c02e");
			$(this).css("border","5px solid #deef3c");
      $(this).css("color","white");
			$(this).siblings(".corctopt").show(0);
			navigationcontroller(true);
		}
		else{
			if($(this).hasClass("forhover")){
			play_correct_incorrect_sound(0);
			$(this).css("background","#FF0000");
			$(this).css("border","5px solid #980000");
			$(this).css("color","white");
			$(this).siblings(".wrngopt").show(0);
			}
		}
	});
		switch(countNext){
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
			sound_player("s3_p2_1");
			break;
			case 11:
			sound_player("s3_p12",1);
			break;

		}

		$(".draggable").draggable({
	 		containment : ".generalTemplateblock",
      		revert : true,
			 		cursor : "move",
			 		zIndex: 100000,
					// stop : function(event, ui){}
	 	});

		$(".dropplace").droppable({
      		hoverClass: "hovered",
	 		drop: function (event, ui){
	 			$this = $(this);
	 			dropfunc(event, ui, $this);
				$(this).find("img").css({"top":"0%"});
	 		}
	 	});
		// $(".draggable").on("dragstop", function(e));
	 	function dropfunc(event, ui, $droppedOn){
			createjs.Sound.stop();
	 		var left = 20;
	 		var top = 10;
			ui.draggable.detach();
			$(".diyinstext").hide(0);
			if($(".drop_img_div").children().hasClass("drop_img")){
				$(".drop_img").css({"display":"block"});
			}else{
				$(".drop_img_sec").css({"display":"block"});
			}
			$(".dragbox").hide(0);
			$(".diyinstext_1").show(0);
			$(".mcq_container").show(0);
			if(ui.draggable.hasClass("conductor")){
				$(".full_img").attr("src",preload.getResult("cond_bulb").src);
			}
			else if (ui.draggable.hasClass("semiconductor")) {
				$(".full_img").attr("src",preload.getResult("semi_bulb").src);
			}
			else if (ui.draggable.hasClass("insulator")) {
				$(".full_img").attr("src",preload.getResult("insul_bulb").src);
			}
	 	}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			nav_button_controls();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('dragdiv')){
			var imageblock = content[count].dragdiv[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_third(content, count){
		if(content[count].hasOwnProperty('dropdiv')){
			var imageblock = content[count].dropdiv[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$refreshBtn.click(function(){
		templatecaller();
	});

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

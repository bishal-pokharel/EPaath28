var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 1
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "midTxt sldFrmLeft",
			textdata: data.string.p2s1
		},{
			textclass: "sldUpTxt sldUpAnim",
			textdata: data.string.p2s1_1
		}],
	},
	//slide 2
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "toptxt tptxtlft ttxt",
			textdata: data.string.p2s2
		},{
			textclass: "toptxt tptxtlft btm",
			textdata: data.string.p2s2_2
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainer",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fim1",
	      imgclass: "imgInBox imBx1",
	      imgid: 'cooking',
				imgsrc: "",
				textclass: "ImgName iN1",
				textdata: data.string.cooking
			},{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fim2",
	      imgclass: "imgInBox imBx2",
	      imgid: 'coalcompact',
				imgsrc: "",
				textclass: "ImgName iN2",
				textdata: data.string.elgen
			},{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fim3",
	      imgclass: "imgInBox imBx3",
	      imgid: 'drying',
				imgsrc: "",
				textclass: "ImgName iN3",
				textdata: data.string.drycloth
			}]
		}]
	},
	//slide 3
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p2s3
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainerRandom",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"randomImg img1",
	      imgclass: "imgInBox imBx1",
	      imgid: 'sun',
				imgsrc: "",
				textclass: "ImgName iN1",
				textdata: data.string.sun
			},{
				imgtoshowindiv:true,
				eachimgclass:"randomImg img2",
	      imgclass: "imgInBox imBx2",
	      imgid: 'dung_cake',
				imgsrc: "",
				textclass: "ImgName iN2",
				textdata: data.string.biofuel
			},{
				imgtoshowindiv:true,
				eachimgclass:"randomImg img3",
	      imgclass: "imgInBox imBx3",
	      imgid: 'coal_fire',
				imgsrc: "",
				textclass: "ImgName iN3",
				textdata: data.string.cook
			},{
				imgtoshowindiv:true,
				eachimgclass:"randomImg img4",
	      imgclass: "imgInBox imBx4",
	      imgid: 'heater1',
				imgsrc: "",
				textclass: "ImgName iN3",
				textdata: data.string.elec
			}]
		}]
	},
	//slide 4
	{
		imageblock:[{
			imagestoshow:[{
				imgclass: "girlSpkSec",
				imgid: 'girl_talking_gif',
				imgsrc: ""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'bubbles',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2s4,
		}]
	},
	//slide 5-->biofl_twn
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p2s5
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainer frImgCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim1",
	      imgclass: "imgInBox imBx1",
	      imgid: 'biofuel_town',
				imgsrc: "",
				textclass: "ImgNameBx iN1",
				textdata: data.string.biofl_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim2",
	      imgclass: "imgInBox imBx2",
	      imgid: 'solar_town',
				imgsrc: "",
				textclass: "ImgNameBx iN2",
				textdata: data.string.sun_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim3",
	      imgclass: "imgInBox imBx3",
	      imgid: 'fossil_town',
				imgsrc: "",
				textclass: "ImgNameBx iN3",
				textdata: data.string.fos_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim4",
	      imgclass: "imgInBox imBx4",
	      imgid: 'electric_town',
				imgsrc: "",
				textclass: "ImgNameBx iN3",
				textdata: data.string.elec_twn
			}]
		}]
	},
	//slide 6
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.biofl_twn
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "full_bg",
	      imgid: 'bg01',
				imgsrc: ""
			}]
		}]
	},
	//slide 7
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.biofl_twn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"showlater",
			textclass: "midWhite",
			textdata: data.string.p2s7
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "full_bg",
	      imgid: 'bg01',
				imgsrc: ""
			}]
		}]
	},
	//slide 8
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.biofl_twn
		}],
		imageload:true,
		popup:[
			{
				popupdivclass:"tempoPop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "background fbTempo",
			      imgid: 'tempo',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.tempo_txt
				}]
			},
			{
				popupdivclass:"stovePop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
						imgtoshowindiv:true,
						eachimgclass:"gasImg gimLeft",
						imgclass: "full_bg fb1",
						imgid: 'smoke02',
						imgsrc: "",
						textclass: "btmBxTxt",
						textdata: data.string.imp_stv
					},{
						imgtoshowindiv:true,
						eachimgclass:"gasImg gimright",
						imgclass: "full_bg fb1 trad",
						imgid: 'smoke01',
						imgsrc: "",
						textclass: "btmBxTxt bt1",
						textdata: data.string.trad_stv
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt uper",
					textclass: "boxTxt",
					textdata: data.string.stove_txt
				}]
			},
			{
				popupdivclass:"biogasPop",
				imageblock:[{
					popimgclass:"sideImg",
					imagestoshow:[{
						imgclass: "full_bg bgas",
						imgid: 'bio_mass',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"sideTxtDiv",
					textclass: "boxTxt sdTxt",
					textdata: data.string.biogas_txt
				}],
				othertxts:[{
					textclass: "biomasImgName stv",
					textdata: data.string.gas_stv
				},{
					textclass: "biomasImgName mxtnk",
					textdata: data.string.mixtnk
				},{
					textclass: "biomasImgName dgstr",
					textdata: data.string.digestr
				}]
			},
			{
				popupdivclass:"biofuelPop",
				imageblock:[{
					popimgclass:"sideImg",
					imagestoshow:[{
						imgclass: "full_bg bg2",
						imgid: 'bio_fuel_plant',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"sideTxtDiv",
					textclass: "boxTxt sdTxt",
					textdata: data.string.biofuel_txt
				}],
				othertxts:[{
					textclass: "biomasImgName drk sug biomasImgName1a",
					textdata: data.string.sugar
				},{
					textclass: "biomasImgName drk strch biomasImgName1a",
					textdata: data.string.starch
				},{
					textclass: "biomasImgName drk celulos biomasImgName1a",
					textdata: data.string.cellulose
				},{
					textclass: "biomasImgName drk plant biomasImgName1a",
					textdata: data.string.plant
				},{
					textclass: "biomasImgName drk gas biomasImgName1a",
					textdata: data.string.gas
				}]
			},
			{
				popupdivclass:"toiletPop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg tlt",
			      imgid: 'toilet',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.human_wst_txt
				}],
				othertxts:[{
					textclass: "iFactTxt",
					textdata: data.string.int_fact
				}]
			},
		],
		svgblock:[{
			svgblock:"vlgSvg"
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "girlSpk",
				imgid: 'girl',
				imgsrc: ""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'bubbles',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2spbox_1,
		}]
	},
	//slide 9-->fossiltown
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p2s5
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainer frImgCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim1",
	      imgclass: "imgInBox imBx1",
	      imgid: 'biofuel_town',
				imgsrc: "",
				textclass: "ImgNameBx iN1",
				textdata: data.string.biofl_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim2",
	      imgclass: "imgInBox imBx2",
	      imgid: 'solar_town',
				imgsrc: "",
				textclass: "ImgNameBx iN2",
				textdata: data.string.sun_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim3",
	      imgclass: "imgInBox imBx3",
	      imgid: 'fossil_town',
				imgsrc: "",
				textclass: "ImgNameBx iN3",
				textdata: data.string.fos_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim4",
	      imgclass: "imgInBox imBx4",
	      imgid: 'electric_town',
				imgsrc: "",
				textclass: "ImgNameBx iN3",
				textdata: data.string.elec_twn
			}]
		}]
	},
	//slide 10
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.fsl_twn
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "full_bg",
	      imgid: 'fossil__fuel_town',
				imgsrc: ""
			}]
		}]
	},
	//slide 11
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.fsl_twn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"showlater",
			textclass: "midWhite",
			textdata: data.string.fsl_twn_exp
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "full_bg",
	      imgid: 'fossil__fuel_town',
				imgsrc: ""
			}]
		}]
	},
	//slide 12
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.biofl_twn
		}],
		imageload:true,
		popup:[
			{
				popupdivclass:"lpgPop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg ktchn",
			      imgid: 'kitchen',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.lpg_txt
				}]
			},
			{
				popupdivclass:"planePop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg apln",
			      imgid: 'aeroplane',
						imgsrc: ""
					},{
			      imgclass: "cloud cld1",
			      imgid: 'cloud01',
						imgsrc: ""
					},{
			      imgclass: "cloud cld2",
			      imgid: 'cloud02',
						imgsrc: ""
					},{
			      imgclass: "cloud cld3",
			      imgid: 'cloud03',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.plane_txt
				}]
			},
			{
				popupdivclass:"petrolPop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg ptrl",
			      imgid: 'petrol_pump',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.petrol_txt
				}]
			},
			{
				popupdivclass:"coalPop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg col",
			      imgid: 'coal',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt tallerBtmPop",
					textclass: "boxTxt",
					textdata: data.string.coal_txt
				}]
			},
			{
				popupdivclass:"industryPop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg ind",
			      imgid: 'industry01',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt tallerBtmPop",
					textclass: "boxTxt",
					textdata: data.string.industry_txt
				}]
			},
			{
				popupdivclass:"ifdiv",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg tlt",
			      imgid: 'industry01',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.industry_txt
				}],
				othertxts:[{
					textclass: "iFactTxt",
					textdata: data.string.int_fact
				}]
			},
		],
		svgblock:[{
			svgblock:"vlgSvg"
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "girlSpk",
				imgid: 'girl',
				imgsrc: ""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'bubbles',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2spbox_2,
		}]
	},
	//slide 13-->solartown
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p2s5
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainer frImgCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim1",
	      imgclass: "imgInBox imBx1",
	      imgid: 'biofuel_town',
				imgsrc: "",
				textclass: "ImgNameBx iN1",
				textdata: data.string.biofl_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim2",
	      imgclass: "imgInBox imBx2",
	      imgid: 'solar_town',
				imgsrc: "",
				textclass: "ImgNameBx iN2",
				textdata: data.string.sun_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim3",
	      imgclass: "imgInBox imBx3",
	      imgid: 'fossil_town',
				imgsrc: "",
				textclass: "ImgNameBx iN3",
				textdata: data.string.fos_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim4",
	      imgclass: "imgInBox imBx3",
	      imgid: 'electric_town',
				imgsrc: "",
				textclass: "ImgNameBx iN3",
				textdata: data.string.elec_twn
			}]
		}]
	},
	//slide 14
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.sun_twn
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "full_bg",
	      imgid: 'solar_town',
				imgsrc: ""
			}]
		}]
	},
	//slide 15
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.sun_twn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"showlater",
			textclass: "midWhite",
			textdata: data.string.sol_twn_exp
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "full_bg",
	      imgid: 'solar_town',
				imgsrc: ""
			}]
		}]
	},
	//slide 16
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.sun_twn
		}],
		imageload:true,
		popup:[
			{
				popupdivclass:"clthdry",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg clth",
			      imgid: 'clothes',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.clth_txt
				}]
			},
			{
				popupdivclass:"solarPop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg apln",
			      imgid: 'solar_cooker',
						imgsrc: ""
					},{
			      imgclass: "arrowSolar arrRef",
			      imgid: 'arrow01',
						imgsrc: ""
					},{
			      imgclass: "arrowSolar arrcoker",
			      imgid: 'arrow01',
						imgsrc: ""
					},{
			      imgclass: "reflnGif",
			      imgid: 'arrow_reflection_solarcooker',
						imgsrc: ""
					}]
				}],
				othertxts:[{
					textclass: "biomasImgName drk refMat",
					textdata: data.string.ref_mat
				},{
					textclass: "biomasImgName drk stv",
					textdata: data.string.stove
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.sol_cook_txt
				}]
			},
			{
				popupdivclass:"grenhousePop",
				imageblock:[{
					popimgclass:"sideImg",
					imagestoshow:[{
			      imgclass: "full_bg grnhs",
			      imgid: 'green_house',
						imgsrc: ""
					},{
			      imgclass: "arrowgrnhouse",
			      imgid: 'arrow01',
						imgsrc: ""
					},{
			      imgclass: "reflnGif ref",
			      imgid: 'arrow_reflection_greenhouse',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"sideTxtDiv",
					textclass: "boxTxt sdTxt",
					textdata: data.string.grenhouse_txt
				}],
				othertxts:[{
					textclass: "biomasImgName drk refSun",
					textdata: data.string.ref_sun
				},{
					textclass: "biomasImgName drk incMat",
					textdata: data.string.inc_sun
				},{
					textclass: "biomasImgName drk trnsp",
					textdata: data.string.trns_shet
				},{
					textclass: "biomasImgName drk trpSun",
					textdata: data.string.trap_sun
				}],
			},
			{
				popupdivclass:"solarHousePop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg col",
			      imgid: 'solar_house',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt tallerBtmPop",
					textclass: "boxTxt",
					textdata: data.string.solarhouse_txt
				}]
			},
			{
				popupdivclass:"insulPop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg insul",
			      imgid: 'solar_water_heater',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt",
					textclass: "boxTxt",
					textdata: data.string.insul_txt
				}]
			},
			{
				popupdivclass:"fotocel_Pop",
				imageblock:[{
					popimgclass:"topFullImg",
					imagestoshow:[{
			      imgclass: "full_bg phtcl",
			      imgid: 'photocells',
						imgsrc: ""
					}]
				}],
				popupttx:[{
					poptxtclass:"btmPopTxt tallerBtmPop",
					textclass: "boxTxt",
					textdata: data.string.int_fct
				}],
				othertxts:[{
					textclass: "iFactTxt",
					textdata: data.string.int_fact
				}]
			},
		],
		svgblock:[{
			svgblock:"vlgSvg"
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "girlSpk",
				imgid: 'girl',
				imgsrc: ""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'bubbles',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2spbox_3,
		}]
	},
	//slide 17-->electrictown
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p2s5
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainer frImgCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim1",
	      imgclass: "imgInBox imBx1",
	      imgid: 'biofuel_town',
				imgsrc: "",
				textclass: "ImgNameBx iN1",
				textdata: data.string.biofl_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim2",
	      imgclass: "imgInBox imBx2",
	      imgid: 'solar_town',
				imgsrc: "",
				textclass: "ImgNameBx iN2",
				textdata: data.string.sun_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim3",
	      imgclass: "imgInBox imBx3",
	      imgid: 'fossil_town',
				imgsrc: "",
				textclass: "ImgNameBx iN3",
				textdata: data.string.fos_twn
			},{
				imgtoshowindiv:true,
				eachimgclass:"frimg fim4",
	      imgclass: "imgInBox imBx3",
	      imgid: 'electric_town',
				imgsrc: "",
				textclass: "ImgNameBx iN3",
				textdata: data.string.elec_twn
			}]
		}]
	},
	//slide 18
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.elec_twn
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "full_bg",
	      imgid: 'electric_town',
				imgsrc: ""
			}]
		}]
	},
	//slide 19
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.elec_twn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"showlater",
			textclass: "midWhite",
			textdata: data.string.el_twn_exp
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "full_bg",
	      imgid: 'electric_town',
				imgsrc: ""
			}]
		}]
	},
	//slide 20
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "twnName",
			textdata: data.string.sun_twn
		}],
		imageload:true,
		popup:[
		{
			popupdivclass:"geyserPop",
			imageblock:[{
				popimgclass:"sideImg",
				imagestoshow:[{
					imgclass: "full_bg gysr",
					imgid: 'water_geager',
					imgsrc: ""
				}]
			}],
			popupttx:[{
				poptxtclass:"sideTxtDiv",
				textclass: "boxTxt sdTxt",
				textdata: data.string.geyser_txt
			}]
		},
		{
			popupdivclass:"heaterPop",
			imageblock:[{
				popimgclass:"sideImg htr",
				imagestoshow:[{
					imgclass: "full_bg heatr",
					imgid: 'heater',
					imgsrc: ""
				}]
			}],
			popupttx:[{
				poptxtclass:"sideTxtDiv",
				textclass: "boxTxt sdTxt",
				textdata: data.string.heater_txt
			}]
		},
		{
			popupdivclass:"elstvPop",
			imageblock:[{
				popimgclass:"sideImg",
				imagestoshow:[{
					imgclass: "full_bg elstv",
					imgid: 'water_jug',
					imgsrc: ""
				}]
			}],
			popupttx:[{
				poptxtclass:"sideTxtDiv",
				textclass: "boxTxt sdTxt",
				textdata: data.string.elStv_txt
			}]
		},
		{
			popupdivclass:"chargePop",
			imageblock:[{
				popimgclass:"topFullImg",
				imagestoshow:[{
		      imgclass: "background car",
		      imgid: 'car',
					imgsrc: ""
				}]
			}],
			popupttx:[{
				poptxtclass:"btmPopTxt tallerBtmPop",
				textclass: "boxTxt",
				textdata: data.string.charge_txt
			}],
			othertxts:[{
				textclass: "iFactTxt",
				textdata: data.string.int_fact
			}]
		},
		],
		svgblock:[{
			svgblock:"vlgSvg"
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "girlSpk",
				imgid: 'girl',
				imgsrc: ""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'bubbles',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2spbox_4,
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cooking", src: imgpath+"cooking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drying", src: imgpath+"drying.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fire", src: imgpath+"fire.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drying-food", src: imgpath+"drying-food.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coal", src: imgpath+"coal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun_sky.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dung_cake", src: imgpath+"dung_cake.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coal_fire", src: imgpath+"coal_fire.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_talking_gif", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "coalcompact", src: imgpath+"coal1.png", type: createjs.AbstractLoader.IMAGE},

			{id: "biofuel_town", src: imgpath+"biofuel_town.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fossil_town", src: imgpath+"fossil_town.png", type: createjs.AbstractLoader.IMAGE},
			{id: "solar_town", src: imgpath+"solar_town.png", type: createjs.AbstractLoader.IMAGE},
			{id: "electric_town", src: imgpath+"electric_town.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg01", src: imgpath+"bio_fuel/bg01.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "bio_fuel_plant", src: imgpath+"bio_fuel/bio_fuel_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bio_mass", src: imgpath+"bio_fuel/bio_mass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smoke01", src: imgpath+"bio_fuel/smoke01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smoke02", src: imgpath+"bio_fuel/smoke02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "toilet", src: imgpath+"bio_fuel/toilet.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tempo", src: imgpath+"bio_fuel/safa_tempo.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fossil__fuel_town", src: imgpath+"fossil_twn/fossil__fuel_town.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "aeroplane", src: imgpath+"fossil_twn/aeroplane.png", type: createjs.AbstractLoader.IMAGE},
			{id: "industry01", src: imgpath+"fossil_twn/industry01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kitchen", src: imgpath+"fossil_twn/kitchen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "petrol_pump", src: imgpath+"fossil_twn/petrol_pump.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coal", src: imgpath+"fossil_twn/coal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud01", src: imgpath+"fossil_twn/cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud02", src: imgpath+"fossil_twn/cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud03", src: imgpath+"fossil_twn/cloud03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud04", src: imgpath+"fossil_twn/cloud04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud05", src: imgpath+"fossil_twn/cloud05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "industry01", src: imgpath+"fossil_twn/industry01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand-icon", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "solar_town", src: imgpath+"solar_twn/solar_town.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "clothes", src: imgpath+"solar_twn/clothes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "green_house", src: imgpath+"solar_twn/green_house.png", type: createjs.AbstractLoader.IMAGE},
			{id: "photocells", src: imgpath+"solar_twn/station.png", type: createjs.AbstractLoader.IMAGE},
			{id: "solar_cooker", src: imgpath+"solar_twn/solar_cooker.png", type: createjs.AbstractLoader.IMAGE},
			{id: "solar_house", src: imgpath+"solar_twn/solar_house.png", type: createjs.AbstractLoader.IMAGE},
			{id: "solar_water_heater", src: imgpath+"solar_twn/solar_water_heater.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow01", src: imgpath+"solar_twn/arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_reflection_solarcooker", src: imgpath+"solar_twn/arrow_reflection_solarcooker.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_reflection_greenhouse", src: imgpath+"solar_twn/arrow_reflection_greenhouse.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "electric_town", src: imgpath+"elec_twn/electric_town.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "water_geager", src: imgpath+"elec_twn/water_geager.png", type: createjs.AbstractLoader.IMAGE},
			{id: "car", src: imgpath+"elec_twn/car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "heater", src: imgpath+"elec_twn/heater.png", type: createjs.AbstractLoader.IMAGE},
			{id: "heater1", src: imgpath+"elec_twn/heater1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water_jug", src: imgpath+"elec_twn/water_jug.png", type: createjs.AbstractLoader.IMAGE},

			{id: "girl", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bubbles", src: imgpath+"bubbles-f_a.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p1_1", src: soundAsset+"s2_p1_1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p2_1", src: soundAsset+"s2_p2_1.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s2_p7", src: soundAsset+"s2_p7.ogg"},
			{id: "s2_p7_1", src: soundAsset+"s2_p7_1.ogg"},
			{id: "s2_p8", src: soundAsset+"s2_p8.ogg"},
			{id: "s2_p8_1_tyampu", src: soundAsset+"s2_p8_1_tyampu.ogg"},
			{id: "s2_p8_2_stove", src: soundAsset+"s2_p8_2_stove.ogg"},
			{id: "s2_p8_3_gobar", src: soundAsset+"s2_p8_3_gobar.ogg"},
			{id: "s2_p8_4_charpi", src: soundAsset+"s2_p8_4_charpi.ogg"},
			{id: "s2_p8_5_dhuwa", src: soundAsset+"s2_p8_5_dhuwa.ogg"},
			{id: "s2_p10", src: soundAsset+"s2_p10.ogg"},
			{id: "s2_p11", src: soundAsset+"s2_p11.ogg"},
			{id: "s2_p11_1", src: soundAsset+"s2_p11_1.ogg"},
			{id: "s2_p11_2", src: soundAsset+"s2_p11_2.ogg"},
			{id: "s2_p12", src: soundAsset+"s2_p12.ogg"},
			{id: "s2_p12_1_house", src: soundAsset+"s2_p12_1_house.ogg"},
			{id: "s2_p12_2_factory", src: soundAsset+"s2_p12_2_factory.ogg"},
			{id: "s2_p12_2_factory_1", src: soundAsset+"s2_p12_2_factory_1.ogg"},
			{id: "s2_p12_3_cars", src: soundAsset+"s2_p12_3_cars.ogg"},
			{id: "s2_p12_4_plane", src: soundAsset+"s2_p12_4_plane.ogg"},
			{id: "s2_p14", src: soundAsset+"s2_p14.ogg"},
			{id: "s2_p15", src: soundAsset+"s2_p15.ogg"},
			{id: "s2_p15_1", src: soundAsset+"s2_p15_1.ogg"},
			{id: "s2_p16", src: soundAsset+"s2_p16.ogg"},
			{id: "s2_p16_1_greenhouse", src: soundAsset+"s2_p16_1_greenhouse.ogg"},
			{id: "s2_p16_2_cooker", src: soundAsset+"s2_p16_2_cooker.ogg"},
			{id: "s2_p16_3_heater", src: soundAsset+"s2_p16_3_heater.ogg"},
			{id: "s2_p16_4_clothes", src: soundAsset+"s2_p16_4_clothes.ogg"},
			{id: "s2_p16_5_solarhouse", src: soundAsset+"s2_p16_5_solarhouse.ogg"},
			{id: "s2_p16_6_solarcharge", src: soundAsset+"s2_p16_6_solarcharge.ogg"},
			{id: "s2_p18", src: soundAsset+"s2_p18.ogg"},
			{id: "s2_p19", src: soundAsset+"s2_p19.ogg"},
			{id: "s2_p19_1", src: soundAsset+"s2_p19_1.ogg"},
			{id: "s2_p19_2", src: soundAsset+"s2_p19_2.ogg"},
			{id: "s2_p20", src: soundAsset+"s2_p20.ogg"},
			{id: "s2_p20_1_carcharge", src: soundAsset+"s2_p20_1_carcharge.ogg"},
			{id: "s2_p20_2_induction", src: soundAsset+"s2_p20_2_induction.ogg"},
			{id: "s2_p20_3_geyser", src: soundAsset+"s2_p20_3_geyser.ogg"},
			{id: "s2_p20_4_heater", src: soundAsset+"s2_p20_4_heater.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});girlCycling
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):'';
		put_speechbox_image(content, countNext);
		var arrCount = 0;
		var crossClkCount = 0;
		$(".fim1, .fim2, .fim3, .fim4").click(function(){
			createjs.Sound.stop();
			switch(countNext) {
			default:
				countNext++;
				templatecaller();
				break;
			}
		});
		switch(countNext){
			case 0:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p1");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play("s2_p1_1");
					current_sound_1.play();
					$(".btm").addClass("sldFrmLeft");
					current_sound_1.on('complete', function(){
						nav_button_controls(300);
					});
				});
			},1500);
			break;
			case 1:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p2");
				current_sound.play();
				$(".ttxt").addClass("sldFrmLeft");
				current_sound.on('complete', function(){
					$(".fim1").delay(500).animate({
						opacity:"1"
					},500,function(){
						$(".fim2").animate({
							opacity:"1"
						},500, function(){
							$(".fim3").animate({
								opacity:"1"
							},500);
							createjs.Sound.stop();
							current_sound_1 = createjs.Sound.play("s2_p2_1");
							current_sound_1.play();
							$(".btm").addClass("sldFrmLeft");
							current_sound_1.on('complete', function(){
								nav_button_controls(300);
							});
						});
					});
				});
				$(".fim1, .fim2, .fim3").css("pointer-events","none");
			break;
			case 3:
				createjs.Sound.stop();
				current_sound_1 = createjs.Sound.play("s2_p4");
				current_sound_1.play();
				current_sound_1.on('complete', function(){
					$(".girlSpkSec").attr("src",preload.getResult("girl").src);
					nav_button_controls(300);
				});
			break;
			case 4:
				sound_player("s2_p"+(countNext+1),1);
				$(".fim4,.fim2,.fim3").addClass("grayedOut");
			break;
			case 6:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p7");
				current_sound.play();
				current_sound.on("complete",function(){
					$(".showlater").addClass("fadeIn");
						createjs.Sound.stop();
						current_sound_1 = createjs.Sound.play("s2_p7_1");
						current_sound_1.play();
						current_sound_1.on('complete', function(){
							nav_button_controls(0);
						});
				});
			break;
			case 7:
				var s= Snap('#vlgSvg');
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p8");
					current_sound.play();
				var svg = Snap.load(preload.getResult("bg01").src, function ( loadedFragment ) {
					s.append(loadedFragment);

					current_sound.on('complete', function(){
						$("#tempo, #industry, #toilet, #cow, #house02").attr("class","highlight");
						$(".sp-1, .girlSpk").hide(0);
					});

					$("#tempo").click(function(){
						$(".tempoPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p8_1_tyampu", ".tempoPop");
					});
					$("#industry").click(function(){
						$(".biofuelPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p8_5_dhuwa", ".biofuelPop");
					});
					$("#toilet").click(function(){
						$(".toiletPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p8_4_charpi", ".toiletPop");
					});
					$("#cow").click(function(){
						$(".biogasPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p8_3_gobar", ".biogasPop");
					});
					$("#house02").click(function(){
						$(".stovePop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p8_2_stove", ".stovePop");
					});
				});
				$(".crossicon").click(function() {
					createjs.Sound.stop();
					if(!$(this).hasClass("clicked")){
						crossClkCount+=1;
						$(this).addClass("clicked");
						crossClkCount==5?nav_button_controls(100):'';
					}
					$(".pop").hide(0);
				});
			break;
			case 8:
				sound_player("s2_p"+(countNext+1),1);
				$(".fim4,.fim2,.fim1").addClass("grayedOut");
			break;
			case 10:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p11");
				current_sound.play();
				$(".showlater:eq(0)").css({opacity:"1"});
				current_sound.on('complete',function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p11_1");
					current_sound.play();
					$(".showlater:eq(1)").animate({opacity:"1"},500);
					current_sound.on('complete',function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s2_p11_2");
						current_sound.play();
						$(".showlater:eq(2)").animate({opacity:"1"},500);
						current_sound.on('complete',function(){
							nav_button_controls();
						});
					});
				});
			break;
			case 11:
			 	$(".coalPop").append("<img class='hndIcon' src='"+preload.getResult("hand-icon").src+"'/>")
				var s= Snap('#vlgSvg');
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p12");
					current_sound.play();
					var svg = Snap.load(preload.getResult("fossil__fuel_town").src, function ( loadedFragment ) {
					s.append(loadedFragment);

					current_sound.on('complete', function(){
						$("#kicthen, #fuelstation, #industry, #house02, #aeroplane").attr("class","highlight");
						$(".sp-1, .girlSpk").hide(0);
					});
					$("#kicthen").click(function(){
						$(".lpgPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p12_1_house", ".lpgPop");
					});
					$("#fuelstation").click(function(){
						$(".petrolPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p12_3_cars", ".petrolPop");
					});
					$("#industry").click(function(){
					$(".coalPop").children().removeClass("flip");
						$(".coalPop").show(0);
						$(this).removeAttr("class");
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s2_p12_2_factory");
							current_sound.play();
							current_sound.on('complete',function(){
								$(".hndIcon").show(0);
							});
							$(".hndIcon").click(function(){
								$(".hndIcon").hide(0);
								$(".coalPop").children().addClass("flip");
								$(".ifdiv").delay(1000).fadeIn();
								popSoundPlayer("s2_p12_2_factory_1", ".ifdiv");
							});
					});
					$("#aeroplane").click(function(){
						$(".planePop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p12_4_plane", ".planePop");
					});
					$("#house02").click(function(){
						$(".stovePop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p12_1_house", ".stovePop");
					});
				});
				$(".crossicon").click(function() {
					if(!$(this).hasClass("clicked")){
						crossClkCount+=1;
						$(this).addClass("clicked");
						crossClkCount==4?nav_button_controls(100):'';
					}
					$(".pop").hide(0);
				});
			break;
			case 12:
				$(".fim4,.fim3,.fim1").addClass("grayedOut");
			break;
			case 14:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p15");
				current_sound.play();
				$(".showlater:eq(0)").css({opacity:"1"});
				current_sound.on('complete',function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p15_1");
					current_sound.play();
					$(".showlater:eq(1)").animate({opacity:"1"},500);
					current_sound.on('complete',function(){
							nav_button_controls();
					});
				});
			break;
			case 15:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p16");
				current_sound.play();
				var s= Snap('#vlgSvg');
				var svg = Snap.load(preload.getResult("solar_town").src, function ( loadedFragment ) {
					s.append(loadedFragment);

					current_sound.on('complete', function(){
						$("#clothers, #solar_x5F_cooker, #green_x5F_house, #solar01, #water_x5F_heater,  #solar02").attr("class","highlight");
						$(".sp-1, .girlSpk").hide(0);
					});
					$("#clothers").click(function(){
						$(".clthdry").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p16_4_clothes", ".clthdry");
					});
					$("#solar_x5F_cooker").click(function(){
						$(".solarPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p16_2_cooker", ".solarPop");
					});
					$("#green_x5F_house").click(function(){
						$(".grenhousePop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p16_1_greenhouse", ".grenhousePop");
					});
					$("#solar01").click(function(){
						$(".solarHousePop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p16_5_solarhouse", ".solarHousePop");
					});
					$("#water_x5F_heater").click(function(){
						$(".insulPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p16_3_heater", ".insulPop");
					});
					$("#solar02").click(function(){
						$(".fotocel_Pop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p16_6_solarcharge", ".fotocel_Pop");
					});
				});
				$(".crossicon").click(function() {
					if(!$(this).hasClass("clicked")){
						crossClkCount+=1;
						$(this).addClass("clicked");
						crossClkCount==6?nav_button_controls(100):'';
					}
					$(".pop").hide(0);
				});
			break;
			case 16:
				$(".fim2,.fim3,.fim1").addClass("grayedOut");
			break;
			case 18:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p19");
				current_sound.play();
				$(".showlater:eq(0)").css({opacity:"1"});
				current_sound.on('complete',function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p19_1");
					current_sound.play();
					$(".showlater:eq(1)").animate({opacity:"1"},500);
					current_sound.on('complete',function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s2_p19_2");
						current_sound.play();
						$(".showlater:eq(2)").animate({opacity:"1"},500);
						current_sound.on('complete',function(){
							nav_button_controls();
						});
					});
				});
			break;
			case 19:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p20");
				current_sound.play();
				var s= Snap('#vlgSvg');
				var svg = Snap.load(preload.getResult("electric_town").src, function ( loadedFragment ) {
					s.append(loadedFragment);

					current_sound.on('complete', function(){
						$("#house02, #hosue01, #house03, #car").attr("class","highlight");
						$(".sp-1, .girlSpk").hide(0);
					});
					$("#house02").click(function(){
						$(".geyserPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p20_3_geyser", ".geyserPop");
					});
					$("#hosue01").click(function(){
						$(".heaterPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p20_4_heater", ".heaterPop");
					});
					$("#house03").click(function(){
						$(".elstvPop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p20_2_induction", ".elstvPop");
					});
					$("#car").click(function(){
						$(".chargePop").show(0);
						$(this).removeAttr("class");
						popSoundPlayer("s2_p20_1_carcharge", ".chargePop");
					});
				});
				$(".crossicon").click(function() {
					if(!$(this).hasClass("clicked")){
						crossClkCount+=1;
						$(this).addClass("clicked");
						crossClkCount==4?nav_button_controls(100):'';
					}
					$(".pop").hide(0);
				});
			break;
			default:
				sound_player("s2_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function popSoundPlayer(sound_id, popClass){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$(popClass).children(".crossicon").show(0);
			// $(".crossicon").show(0);
		});

	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?navigationcontroller():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}


	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('popup')){
			for(var i=0; i<content[count].popup.length;i++){
				if(content[count].popup[i].hasOwnProperty('imageblock'))
				{
					var imageblock = content[count].popup[i].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var j=0; j<imageClass.length; j++){
							var image_src = preload.getResult(imageClass[j].imgid).src;
							//get list of classes
							var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
								// alert(i);
						}
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/images/dnd_img/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content = [
	//start
	{
	contentnocenteradjust : true,
		extratextblock : [{
			textclass:"diyTxt",
			textdata : data.string.diy
		}],
		imageblockadditionalclass:'background',
		imageblock : [{
			imagestoshow : [{
				imgclass : "background inb",
				imgsrc : '',
				imgid : 'img_diy'
			}]
		}],
	},
	//page 1
	{
		contentblockadditionalclass : "green-bg",
		contentnocenteradjust : true,
		imageblockadditionalclass : "upperimageblockholder",
		sundar: true,
		extratextblock : [{
			textclass:"dndInstrn",
			textdata : data.string.dndtxt
		}],
		imageblock : [{
			imagestoshow : [{
				//1
				imgclass : "draggable biof  sliding hide im-1",
				imgsrc : '',
				imgid : 'firewood'
			}, {
				//2
				imgclass : "draggable biof  sliding hide im-2",
				imgsrc : '',
				imgid : 'dungcake'
			}, {
				//3
				imgclass : "draggable biof  sliding hide im-3",
				imgsrc : '',
				imgid : 'bio_mass'
			}, {
				//2_1
				imgclass : "draggable elec sliding hide im-5",
				imgsrc : '',
				imgid : 'heater'
			}, {
				//2_2
				imgclass : "draggable elec sliding hide im-6",
				imgsrc : '',
				imgid : 'water_geager'
			}, {
				//2_3
				imgclass : "draggable elec sliding hide im-7",
				imgsrc : '',
				imgid : 'water_jug'
			}, {
				//3_1
				imgclass : "draggable fosil sliding hide im-9",
				imgsrc : '',
				imgid : 'petrol'
			}, {
				//3_2
				imgclass : "draggable fosil sliding hide im-10",
				imgsrc : '',
				imgid : 'diesel'
			}, {
				//3_3
				imgclass : "draggable fosil sliding hide im-11",
				imgsrc : '',
				imgid : 'aeroplane'
			}, {
				//3_3
				imgclass : "draggable fosil sliding hide im-12",
				imgsrc : '',
				imgid : 'lpg'
			}],
			imagelabels : [{
				imagelabelclass : "draganddropinst my_font_big",
				imagelabeldata : data.string.p3text1
			}]
		}],
		specialdropdiv : [{
			droplabelclass : "biolabel my_font_big",
			droplabeldata : data.string.biofuel,
			dropdiv : "biodrop"
		}, {
			droplabelclass : "eleclabel my_font_big",
			droplabeldata : data.string.electricity,
			dropdiv : "elecdrop"
		}, {
			droplabelclass : "fosilLabel my_font_big",
			droplabeldata : data.string.fossil,
			dropdiv : "fosildrop"
		}]
	}
];

content[1].imageblock[0].imagestoshow.shufflearray();
for(var i=0; i< 4; i++){
	var classname = content[1].imageblock[0].imagestoshow[i].imgclass;
	classname = classname.replace(/hide/,'');
	content[1].imageblock[0].imagestoshow[i].imgclass = classname + ' draggableposition' + (i+1);
	console.log(content[1].imageblock[0].imagestoshow[i].imgclass);
}
$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "bg", src: imgpath+"diy/imgdiy.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sundar", src: 'images/sundar/correct-2.png', type: createjs.AbstractLoader.IMAGE},

			{id: "firewood", src: imgpath+"firewood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dungcake", src: imgpath+"dung_cake.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bio_mass", src: imgpath+"biogas.png", type: createjs.AbstractLoader.IMAGE},

			{id: "heater", src: imgpath+"electric_heater.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water_geager", src: imgpath+"electric_geyser.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water_jug", src: imgpath+"electric_kettle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img_diy", src: imgpath+"img_diy.png", type: createjs.AbstractLoader.IMAGE},

			{id: "petrol", src: imgpath+"petrol.png", type: createjs.AbstractLoader.IMAGE},
			{id: "diesel", src: imgpath+"diesel.png", type: createjs.AbstractLoader.IMAGE},
			{id: "aeroplane", src: imgpath+"airplane.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lpg", src: imgpath+"lpg_gas.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"s4_p2_inst.ogg"},
			//textboxes
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	function navigationcontroller(islastpageflag) {
		// typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);

		//call notifyuser
		// notifyuser($anydiv);

		switch(countNext){
			case 0:
				play_diy_audio();
				setTimeout(function(){$nextBtn.show(0);},2000);
				break;
			case 1:
				$('.sundar').attr('src', preload.getResult('sundar').src);
				sound_player('sound_1');
				$(".draggable").draggable({
					containment : "body",
					cursor : "grab",
					revert: true,
					appendTo : "body",
					helper : "clone",
					zindex : 1000,
					start: function(event, ui){
						$(ui.helper).addClass("disableanimation");
						$(ui.helper).css({"max-width": "18%",
						// 				  "max-height": "15%"
					});

					},
					stop: function(event, ui){
						$(ui.helper).removeClass("disableanimation");
						$(ui.helper).css("max-width", "");
					}
				});

				$('.biodrop').droppable({
					// accept : ".chew",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "biof" , "biodrop", ui.draggable);
					}
				});

				$('.elecdrop').droppable({
					// accept : ".lick",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "elec" , "elecdrop", ui.draggable);
					}
				});

				$('.fosildrop').droppable({
					// accept : ".swallow",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "fosil" , "fosildrop", ui.draggable);
					}
				});

				var divsdroppedcount = 0;
				function handleCardDrop(event, ui, classname, droppedon, dropped) {
					createjs.Sound.stop();
					if(dropped.hasClass(classname)){
						play_correct_incorrect_sound(1);
						dropped.draggable('option', 'revert', false);
						ui.draggable.draggable('disable');
						var dropped = ui.draggable;
						var droppedOn = $("."+droppedon);
						var count = 0;
						var top = 0;

							// count = $("."+droppedon+"> ."+classname).length;
							// top = (count < 2)? 10: 50;
							// if(count==0) left = 0;
							// if(count==1) left = 50;
							// if(count==2) left = 25;
							$(dropped).detach().css({
								"position" : "relative",
								// "top" : top+"%",
								// "left": left + "%",
								"width": "30%",
								"max-height": "40%",
								"flex": "0 0 35%",
								// "height": "auto",
								// "max-height": "70%"
							}).appendTo(droppedOn);

						// var $classtoshow;
						var $newEntry = $(".upperimageblockholder> .hide").eq(0);
						var $draggable3;
						var $draggable2;
						var $draggable1;
						if(dropped.hasClass("draggableposition4")){
							dropped.toggleClass("draggableposition4");
							$draggable3 = $(".draggableposition3");
							$draggable2 = $(".draggableposition2");
							$draggable1 = $(".draggableposition1");
							// $classtoshow = $(".draggableposition4").eq(0);

						}else if(dropped.hasClass("draggableposition3")){
							dropped.toggleClass("draggableposition3");
							$draggable2 = $(".draggableposition2");
							$draggable1 = $(".draggableposition1");
							// $classtoshow = $(".draggableposition3").eq(0);
						}else if(dropped.hasClass("draggableposition2")){
							dropped.toggleClass("draggableposition2");
							$draggable1 = $(".draggableposition1");
							// $classtoshow = $(".draggableposition2").eq(0);
						}else if(dropped.hasClass("draggableposition1")){
							dropped.toggleClass("draggableposition1");
							// $classtoshow = $(".draggableposition1").eq(0);
						}

						if($draggable3 != null){
							 $draggable3.removeClass("draggableposition3").addClass("draggableposition4");
						}
						if($draggable2 != null){
							 $draggable2.removeClass("draggableposition2").addClass("draggableposition3");
						}
						if($draggable1 != null){
							 $draggable1.removeClass("draggableposition1").addClass("draggableposition2");
						}
						if($newEntry != null){
							 $newEntry.removeClass("hide").addClass("draggableposition1");
						}

						divsdroppedcount++;
						if(divsdroppedcount == 10){
							$('.sundar').show(0);
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					} else {
						play_correct_incorrect_sound(0);
					}
				}
				break;
			default:
				break;
		}

	}

	/*=====  End of Templates Block  ======*/
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generalTemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//coverpage
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "cvTxt",
			textdata: data.lesson.chapter
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'cover_page',
				imgsrc: ""
			}]
		}]
	},
	//slide 2
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "midTxt sldFrmLeft",
			textdata: data.string.p1s2
		},{
			textclass: "sldUpTxt sldUpAnim",
			textdata: data.string.p1s2_1
		}],
	},
	//slide 3
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p1s2
		},{
			textclass: "s3Info fadIn",
			textdata: data.string.p1s3_1
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainer",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fim1",
	      imgclass: "imgInBox imBx1",
	      imgid: 'cooking',
				imgsrc: ""
			},{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fim2",
	      imgclass: "imgInBox imBx2",
	      imgid: 'drying',
				imgsrc: ""
			},{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fim3",
	      imgclass: "imgInBox imBx3",
	      imgid: 'fire',
				imgsrc: ""
			}]
		}]
	},
	//slide 4
	{
		contentblockadditionalclass:"cdm_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p1s2
		},{
			textclass: "dykTitle sldFrmLeft",
			textdata: data.string.p1s4
		},{
			textclass: "s3Info s4Info",
			textdata: data.string.p1s4_1
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainer",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fm1",
	      imgclass: "imgInBox imBx1",
	      imgid: 'cooking',
				imgsrc: ""
			},{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fm2",
	      imgclass: "imgInBox imBx2",
	      imgid: 'drying',
				imgsrc: ""
			},{
				imgtoshowindiv:true,
				eachimgclass:"flxImage fm3",
	      imgclass: "imgInBox imBx3",
	      imgid: 'fire',
				imgsrc: ""
			}]
		}]
	},
	//slide 5
	{
		contentblockadditionalclass:"gray_bg",
		extratextblock:[{
				textclass: "toptxt",
				textdata: data.string.p1s2
			},{
				textclass: "dykSecSide",
				textdata: data.string.p1s4
			},{
				textclass: "dykExpln",
				textdata: data.string.p1s5_1
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainerRhsBig",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"full_bg",
	      imgclass: "imgInBox",
	      imgid: 'drying-food',
				imgsrc: ""
			}],
			imagelabels:[{
				imgexpclass:"btmExpClass",
				imagelabelclass: "imgLblTxt",
				imagelabeldata: data.string.p1s6_1
			}]
		}]
	},
	//slide 6
	// {
	// 	contentblockadditionalclass:"gray_bg",
	// 	extratextblock:[{
	// 		textclass: "toptxt",
	// 		textdata: data.string.p1s2
	// 	},{
	// 		textclass: "dykSecSide",
	// 		textdata: data.string.p1s4
	// 	}
	// 	,{
	// 		textclass: "dykExpln",
	// 		textdata: data.string.p1s7_2
	// 	}],
	// 	imageblock:[{
	// 		imgindiv:true,
	// 		imgdivclass:"imagesContainerRhsBig",
	// 		imagestoshow:[{
	//       imgclass: "hdimg-hi1 s7Dry",
	//       imgid: 'drying',
	// 			imgsrc: ""
	// 		},{
	//       imgclass: "hdimg-hi2 s7Cook",
	//       imgid: 'cooking',
	// 			imgsrc: ""
	// 		},{
	//       imgclass: "hdimg-hi3 s7fire",
	//       imgid: 'fire',
	// 			imgsrc: ""
	// 		}],
	// 		imagelabels:[{
	// 			imgexpclass:"btmExpClass",
	// 			imagelabelclass: "imgLblTxt",
	// 			imagelabeldata: data.string.p1s7_1
	// 		}]
	// 	}]
	// },
	//slide 7
	{
		contentblockadditionalclass:"gray_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p1s2
		},{
			textclass: "dykSecSide",
			textdata: data.string.p1s4
		}
		,{
			textclass: "dykExpln sml3v",
			textdata: data.string.p1s8_2
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainerRhsBig",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"full_bg",
	      imgclass: "imgInBox",
	      imgid: 'boiling_water',
				imgsrc: ""
			}],
			imagelabels:[{
				imgexpclass:"btmExpClass",
				imagelabelclass: "imgLblTxt",
				imagelabeldata: data.string.p1s8_1
			}]
		}]
	},
	//slide 8
	{
		contentblockadditionalclass:"gray_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p1s2
		},{
			textclass: "dykSecSide",
			textdata: data.string.p1s4
		},{
			textclass: "dykExpln",
			textdata: data.string.p1s9_2
		},{
			textclass: "lifeprcsTxt lpt_1",
			textdata: data.string.heat_eng
		},{
			textclass: "lifeprcsTxt lpt_2",
			textdata: data.string.lightenergy
		},{
			textclass: "lifeprcsTxt lpt_3",
			textdata: data.string.oxygen
		},{
			textclass: "lifeprcsTxt lpt_4",
			textdata: data.string.carbondioxide
		},{
			textclass: "lifeprcsTxt lpt_5",
			textdata: data.string.water
		},{
			textclass: "lifeprcsTxt lpt_6",
			textdata: data.string.glucose
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainerRhsBig",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"full_bg",
	      imgclass: "imgInBox",
	      imgid: 'life_process_plant',
				imgsrc: ""
			}],
			imagelabels:[{
				imgexpclass:"btmExpClass",
				imagelabelclass: "imgLblTxt",
				imagelabeldata: data.string.p1s9_1
			}]
		}]
	},
	//slide 9
	{
		contentblockadditionalclass:"gray_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p1s2
		},{
			textclass: "dykSecSide",
			textdata: data.string.p1s4
		},{
			textclass: "dykExpln",
			textdata: data.string.p1s10_2
		}
	],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainerRhsBig",
			imagestoshow:[{
	      imgclass: "imgInBox",
	      imgid: 'bg',
				imgsrc: ""
			},{
	      imgclass: "arrow-1",
	      imgid: 'arrow',
				imgsrc: ""
			},{
	      imgclass: "arrow-2",
	      imgid: 'arrow',
				imgsrc: ""
			},{
	      imgclass: "arrow-3",
	      imgid: 'arrow',
				imgsrc: ""
			},{
	      imgclass: "arrow-4",
	      imgid: 'arrow',
				imgsrc: ""
			},{
	      imgclass: "cloud",
	      imgid: 'clod04',
				imgsrc: ""
			},{
	      imgclass: "cloud cloudSec",
	      imgid: 'clod04',
				imgsrc: ""
			},{
	      imgclass: "waves-1",
	      imgid: 'waves',
				imgsrc: ""
			},{
	      imgclass: "waves-2",
	      imgid: 'waves',
				imgsrc: ""
			},{
	      imgclass: "rain-1",
	      imgid: 'rain',
				imgsrc: ""
			},{
	      imgclass: "rain-2",
	      imgid: 'rain',
				imgsrc: ""
			},{
	      imgclass: "rain-3",
	      imgid: 'rain',
				imgsrc: ""
			}],
			imagelabels:[{
				imgexpclass:"btmExpClass",
				imagelabelclass: "imgLblTxt",
				imagelabeldata: data.string.p1s10_1
			}]
		}]
	},
	//slide 10
	{
		contentblockadditionalclass:"gray_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p1s2
		},{
			textclass: "dykSecSide",
			textdata: data.string.p1s4
		}
		,{
			textclass: "dykExpln",
			textdata: data.string.p1s11_2
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainerRhsBig",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"full_bg",
	      imgclass: "imgInBox",
	      imgid: 'blacksmith',
				imgsrc: ""
			}],
			imagelabels:[{
				imgexpclass:"btmExpClass",
				imagelabelclass: "imgLblTxt",
				imagelabeldata: data.string.p1s11_1
			}]
		}]
	},
	//slide 11
	{
		contentblockadditionalclass:"gray_bg",
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p1s2
		},{
			textclass: "dykSecSide",
			textdata: data.string.p1s4
		}
		,{
			textclass: "dykExpln",
			textdata: data.string.p1s12_2
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imagesContainerRhsBig",
			imagestoshow:[{
				imgtoshowindiv:true,
				eachimgclass:"full_bg",
	      imgclass: "imgInBox",
	      imgid: 'coal',
				imgsrc: ""
			}],
			imagelabels:[{
				imgexpclass:"btmExpClass",
				imagelabelclass: "imgLblTxt",
				imagelabeldata: data.string.p1s12_1
			}]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cooking", src: imgpath+"cooking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drying", src: imgpath+"drying.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fire", src: imgpath+"fire.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drying-food", src: imgpath+"drying-food.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boiling_water", src: imgpath+"boiling_water.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "life_process_plant", src: imgpath+"life_process_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water_cycle", src: imgpath+"water_cycle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blacksmith", src: imgpath+"blacksmith.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coal", src: imgpath+"coal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clod04", src: imgpath+"clod04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rain", src: imgpath+"rain.png", type: createjs.AbstractLoader.IMAGE},
			{id: "waves", src: imgpath+"waves.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p2_1", src: soundAsset+"s1_p2_1.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p4_1", src: soundAsset+"s1_p4_1.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p5_1", src: soundAsset+"s1_p5_1.ogg"},
			{id: "s1_p5_2", src: soundAsset+"s1_p5_2.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p6_1", src: soundAsset+"s1_p6_1.ogg"},
			{id: "s1_p6_2", src: soundAsset+"s1_p6_2.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p7_1", src: soundAsset+"s1_p7_1.ogg"},
			{id: "s1_p7_2", src: soundAsset+"s1_p7_2.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p8_1", src: soundAsset+"s1_p8_1.ogg"},
			{id: "s1_p8_2", src: soundAsset+"s1_p8_2.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p9_1", src: soundAsset+"s1_p9_1.ogg"},
			{id: "s1_p9_2", src: soundAsset+"s1_p9_2.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p10_1", src: soundAsset+"s1_p10_1.ogg"},
			{id: "s1_p10_2", src: soundAsset+"s1_p10_2.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "s1_p11_1", src: soundAsset+"s1_p11_1.ogg"},
			{id: "s1_p11_2", src: soundAsset+"s1_p11_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});girlCycling
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):'';
		put_speechbox_image(content, countNext);
		var arrCount = 0;

		function soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount){
			$(imgClassArr[arrCount]).addClass(clstoAddArray[arrCount]);
			createjs.Sound.stop();
			setTimeout(function(){
                current_sound = createjs.Sound.play(soundArray[arrCount]);
                current_sound.play();
                current_sound.on('complete', function(){
                    arrCount+=1;
                    // console.log(arrCount, imgClassArr.length);
                    if(arrCount < imgClassArr.length){
                        soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount);
                    }else{
                        nav_button_controls(500);
                    }
                });
			},2000);

		}
		switch(countNext){
			case 1:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on('complete', function(){
						navigationcontroller();
					});
				});
			},1500);
			break;
			case 2:
				sound_player("s1_p"+(countNext+1),1);
				$(".fim1").delay(500).animate({
					opacity:"1"
				},500,function(){
					$(".fim2").animate({
						opacity:"1"
					},500, function(){
						$(".fim3").animate({
							opacity:"1"
						},500);
					});
				});

			break;
			case 3:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on('complete', function(){
						navigationcontroller();
					});
				});
			},2000);
			break;
			case 4:
				var imgClassArr=[".imgLblTxt",".dykSecSide",".dykExpln"];
				var soundArray=["s1_p5","s1_p5_1","s1_p5_2"];
				var clstoAddArray=["fadIn","slidInDyk","slidInDyk"];
				soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount);
			break;
			// case 5:
			// 	var imgClassArr=[".imgLblTxt",".dykSecSide",".dykExpln"];
			// 	var soundArray=["s1_p6","s1_p5_1","s1_p6_2"];
			// 	var clstoAddArray=["fadIn","slidInDyk","slidInDyk"];
			//  $(".hdimg-hi2").animate({
			// 	 opacity:"1"
			//  },500,function(){
			// 	 $(".hdimg-hi1").animate({
			// 		 opacity:"1"
			// 	 },500, function(){
			// 		 $(".hdimg-hi3").animate({
			// 			 opacity:"1"
			// 		 },500,function(){
		 	// 			soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount);
			// 		 });
			// 	 });
			//  });
			// break;
			case 5:
				var imgClassArr=[".imgLblTxt",".dykSecSide",".dykExpln"];
				var soundArray=["s1_p7","s1_p5_1","s1_p7_2"];
				var clstoAddArray=["fadIn","slidInDyk","slidInDyk"];
				soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount);
			break;
			case 6:
				var imgClassArr=[".imgLblTxt",".dykSecSide",".dykExpln"];
				var soundArray=["s1_p8","s1_p5_1","s1_p8_2"];
				var clstoAddArray=["fadIn","slidInDyk","slidInDyk"];
				soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount);
			break;
			case 7:
				var imgClassArr=[".imgLblTxt",".dykSecSide",".dykExpln"];
				var soundArray=["s1_p9","s1_p5_1","s1_p9_2"];
				var clstoAddArray=["fadIn","slidInDyk","slidInDyk"];
				soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount);
			break;
			case 8:
				var imgClassArr=[".imgLblTxt",".dykSecSide",".dykExpln"];
				var soundArray=["s1_p10","s1_p5_1","s1_p10_2"];
				var clstoAddArray=["fadIn","slidInDyk","slidInDyk"];
				soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount);
			break;
			case 9:
				var imgClassArr=[".imgLblTxt",".dykSecSide",".dykExpln"];
				var soundArray=["s1_p11","s1_p5_1","s1_p11_2"];
				var clstoAddArray=["fadIn","slidInDyk","slidInDyk"];
				soundFdInAnim(imgClassArr, soundArray, clstoAddArray, arrCount);
			break;
			default:
				sound_player("s1_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}


	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('sideboxes')){
			for(var i=0; i<content[count].sideboxes.length;i++){
				if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
				{
					var imageblock = content[count].sideboxes[i].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var j=0; j<imageClass.length; j++){
							var image_src = preload.getResult(imageClass[j].imgid).src;
							//get list of classes
							var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
								// alert(i);
						}
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

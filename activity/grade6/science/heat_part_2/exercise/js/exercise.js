var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide 1
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1
		}],
		imgexc:[{
			imgexcontainerclass:"firstContainer",
			imgexcoptions:[{
				imgoptionscontainerclass:"opnContainer class1 op1",
				imgclass:"boximg bim1",
				imgid:"q1op1",
				imgsrc:'',
				optaddclass:"boxtxt bt1",
				optdata:data.string.exc_q1_op1
			},{
				imgoptionscontainerclass:"opnContainer op2",
				imgclass:"boximg bim2",
				imgid:"q1op2",
				imgsrc:'',
				optaddclass:"boxtxt bt2",
				optdata:data.string.exc_q1_op2
			},{
				imgoptionscontainerclass:"opnContainer class1 op3",
				imgclass:"boximg bim3",
				imgid:"q1op3",
				imgsrc:'',
				optaddclass:"boxtxt bt3",
				optdata:data.string.exc_q1_op3
			}]
		}]
	},
	// slide2
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q2
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q2_op1
			},{
				optaddclass:"class1 sec",
				optdata:data.string.exc_q2_op2
			},{
				optdata:data.string.exc_q2_op3
			},{
				optdata:data.string.exc_q2_op4
			}]
		}]
	},
	// slide3
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q3
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q3_op1
			},{
				optaddclass:"class1 sec",
				optdata:data.string.exc_q3_op2
			},{
				optdata:data.string.exc_q3_op3
			},{
				optdata:data.string.exc_q3_op4
			}]
		}]
	},
	// slide4
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q4
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q4_op1
			},{
				optdata:data.string.exc_q4_op2
			},{
				optdata:data.string.exc_q4_op3
			},{
				optdata:data.string.exc_q4_op4
			}]
		}]
	},
	// slide5
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q5
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q5_op1
			},{
				optdata:data.string.exc_q5_op2
			},{
				optdata:data.string.exc_q5_op3
			},{
				optdata:data.string.exc_q5_op4
			}]
		}]
	},
	// slide6
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q6
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q6_op1
			},{
				optdata:data.string.exc_q6_op2
			},{
				optdata:data.string.exc_q6_op3
			},{
				optdata:data.string.exc_q6_op4
			}]
		}]
	},
	// slide7
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q7
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q7_op1
			},{
				optdata:data.string.exc_q7_op2
			},{
				optdata:data.string.exc_q7_op3
			},{
				optdata:data.string.exc_q7_op4
			}]
		}]
	},
	// slide8
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q8
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q8_op1
			},{
				optdata:data.string.exc_q8_op2
			},{
				optdata:data.string.exc_q8_op3
			},{
				optdata:data.string.exc_q8_op4
			}]
		}]
	},
	// slide9
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q9
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q9_op1
			},{
				optdata:data.string.exc_q9_op2
			},{
				optdata:data.string.exc_q9_op3
			},{
				optdata:data.string.exc_q9_op4
			}]
		}]
	},
	// slide10
	{
		contentblockadditionalclass : "blueBg",
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q10
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_q10_op1
			},{
				optdata:data.string.exc_q10_op2
			},{
				optdata:data.string.exc_q10_op3
			},{
				optdata:data.string.exc_q10_op4
			}]
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "q1op1", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op2", src: imgpath+"rock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op3", src: imgpath+"petrol.png", type: createjs.AbstractLoader.IMAGE},


			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new RhinoTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
		content[countNext].imgload?put_image_sec(content, countNext):"";
	 	var corcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	testin.numberOfQuestions();

	 	var ansClicked = false;
	 	var wrngClicked = false;

	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
	    return randNum;
	  }
		$(".qn_top").prepend(countNext+1+". ");

	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
	 	switch(countNext){
		 case 0:
				randomize(".firstContainer");
			break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				randomize(".optionsdiv");
			break;
	 	}
		$(".opnContainer").click(function(){
			if($(this).hasClass("class1")){
				$(this).children(".corctopt").show(0);
				play_correct_incorrect_sound(1);
				corcount+=1;
				if(corcount==2){
					if(wrngClicked == false){
						testin.update(true);
					}
					$(".opnContainer").css("pointer-events","none");
					$nextBtn.show();
				}
				$(this).css("pointer-events","none");
				$(this).css({
					"pointer-events":"none",
					"background-color":"#93c47d"
				});
				$(this).find("p").css("background-color","#b6d7a8");
			}
			else{
				$(this).children(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				wrngClicked = true;
				$(this).css({
					"pointer-events":"none",
					"background-color":"#e06666"
				});
				$(this).find("p").css("background-color","#ea9999");
			}
		});
		$(".buttonsel").click(function(){
				// $(this).removeClass('forhover');
					if(ansClicked == false){
						/*class 1 is always for the right answer. updates scoreboard and disables other click if
						right answer is clicked*/
						if($(this).hasClass("class1")){
							// alert("here");
							corcount+=1;
							if(countNext==1||countNext==2){
								$(this).css("pointer-events","none");
								if(corcount==2){
									if(wrngClicked == false){
										testin.update(true);
									}
									$('.buttonsel').css("pointer-events","none");
									$nextBtn.show(0);
								}
							}else{
								if(wrngClicked == false){
									testin.update(true);
								}
								$('.buttonsel').css("pointer-events","none");
								$nextBtn.show(0);
							}
							play_correct_incorrect_sound(1);
							$(this).css({
								"background": "#bed62fff",
								"border":"5px solid #deef3c",
								"color":"white",
								'pointer-events': 'none'
							});
							$(this).siblings(".corctopt").show(0);
						}
						else{
							testin.update(false);
							play_correct_incorrect_sound(0);
							$(this).css({
								"background":"#FF0000",
								"border":"5px solid #980000",
								"color":"white",
								'pointer-events': 'none'
							});
							$(this).siblings(".wrngopt").show(0);
							wrngClicked = true;
						}
					}
				});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('imgexc')){
			var imageblock = content[count].imgexc[0];
			if(imageblock.hasOwnProperty('imgexcoptions')){
				var imageClass = imageblock.imgexcoptions;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	//  }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

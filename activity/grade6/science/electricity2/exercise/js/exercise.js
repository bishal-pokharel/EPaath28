var soundAsset = $ref+"/sounds/";
var imgpath = $ref+"/images/exercise/";

var content = [
//     //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.q1
            }

        ],
        dragdivclass:"dragdiv",
        dropdivclass:"dropdiv",
        dragdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "switch draggable opt1",
                        imgclass: "relativecls img1",
                        imgid: 'switchImg',
                        imgsrc: "",
                        imgans:"switch",
                    },
                    {
                        imgdiv: "bulb draggable opt2",
                        imgclass: "relativecls img2",
                        imgid: 'bulbImg',
                        imgsrc: imgpath + "bulb01.png",
                        imgans:"bulb",
                    },
                    {
                        imgdiv: "battery draggable opt3",
                        imgclass: "relativecls img3",
                        imgid: 'batteryImg',
                        imgsrc: imgpath + "battery01.png",
                        imgans:"battery",
                    },
                    {
                        imgdiv: "wire draggable opt4",
                        imgclass: "relativecls img4",
                        imgid: 'wireImg',
                        imgsrc: imgpath + "line.png",
                        imgans:"wire",
                    }
                ]
            }]
        }],
        dropdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "batterydrp commondropbox droppable",
                        imgclass: " img7",
                        imgid: 'batteryImg',
                        imgsrc: imgpath + "battery02.png",
                        imgans:"battery",
                        textblock:[
                            {
                                textdiv:"div1",
                                textclass:"centertext diyfont",
                                textdata:data.string.q1a1
                            }
                        ]
                    },
                    {
                        imgdiv: "switchdrp commondropbox droppable",
                        imgclass: " img5",
                        imgid: 'switchImg',
                        imgsrc: imgpath + "switch02.png",
                        imgans:"switch",
                        textblock:[
                            {
                                textdiv:"div2",
                                textclass:"centertext diyfont",
                                textdata:data.string.q1a2
                            }
                        ]
                    },
                    {
                        imgdiv: "wiredrp commondropbox droppable",
                        imgclass: " img8",
                        imgid: 'wireImg',
                        imgsrc: imgpath + "line.png",
                        imgans:"wire",
                        textblock:[
                            {
                                textdiv:"div3",
                                textclass:"centertext diyfont",
                                textdata:data.string.q1a3
                            }
                        ]
                    },
                    {
                        imgdiv: "bulbdrp commondropbox droppable",
                        imgclass: " img6",
                        imgid: 'bulbImg',
                        imgsrc: imgpath + "bulb02.png",
                        imgans:"bulb",
                        textblock:[
                            {
                                textdiv:"div4",
                                textclass:"centertext diyfont",
                                textdata:data.string.q1a4
                            }
                        ]
                    }
                ]
            }]
        }]
    },
// //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.q2
            }

        ],
        dragdivclass:"dragdiv1",
        dropdivclass:"dropdiv1",
        dragdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "switch1 draggable option1",
                        imgclass: " img1",
                        imgid: 'switchImg',
                        imgsrc: "",
                        imgans:"switch",
                    },
                    {
                        imgdiv: "bulb1 draggable option2",
                        imgclass: " img2",
                        imgid: 'bulbImg',
                        imgsrc: imgpath + "bulb01.png",
                        imgans:"bulb",
                    },
                    {
                        imgdiv: "battery1 draggable option3",
                        imgclass: " img3",
                        imgid: 'batteryImg',
                        imgsrc: imgpath + "battery01.png",
                        imgans:"battery",
                    },
                    {
                        imgdiv: "wire1 draggable option4",
                        imgclass: " img4",
                        imgid: 'wireImg',
                        imgsrc: imgpath + "line.png",
                        imgans:"wire",
                    }
                ]
            }]
        }],
        dropdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "batterydrp1 commondropbox1 droppable",
                        imgclass: " img7",
                        imgid: 'batteryImg',
                        imgsrc: imgpath + "battery02.png",
                        imgans:"battery",
                        textblock:[
                            {
                                textdiv:"div5",
                                textclass:"centertext content3",
                                textdata:data.string.q2a3
                            }
                        ]
                    },
                    {
                        imgdiv: "switchdrp1 commondropbox1 droppable",
                        imgclass: " img5",
                        imgid: 'switchImg',
                        imgsrc: imgpath + "switch02.png",
                        imgans:"switch",
                        textblock:[
                            {
                                textdiv:"div6",
                                textclass:"centertext content3",
                                textdata:data.string.q2a1
                            }
                        ]
                    },
                    {
                        imgdiv: "wiredrp1 commondropbox1 droppable",
                        imgclass: " img8",
                        imgid: 'wireImg',
                        imgsrc: imgpath + "line.png",
                        imgans:"wire",
                        textblock:[
                            {
                                textdiv:"div7",
                                textclass:"centertext content3",
                                textdata:data.string.q2a2
                            }
                        ]
                    },
                    {
                        imgdiv: "bulbdrp1 commondropbox1 droppable",
                        imgclass: " img6",
                        imgid: 'bulbImg',
                        imgsrc: imgpath + "bulb02.png",
                        imgans:"bulb",
                        textblock:[
                            {
                                textdiv:"div8",
                                textclass:"centertext content3",
                                textdata:data.string.q2a4
                            }
                        ]
                    }
                ]
            }]
        }]
    },
  //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.q3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "tipicon icon",
                    imgclass: "relativecls i1",
                    imgid: 'tipiconImg',
                    imgsrc: "",
                }
            ]
        }],
        dragdivclass:"dragdiv2",
        dropdivclass:"dropdiv2",
        dragdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "draggable commonoption op1",
                        imgclass: "relimg1",
                        imgid: 'circuit01Img',
                        imgsrc: imgpath + "circuit01.png",
                        imgans:"complete",
                    },
                    {
                        imgdiv: "draggable commonoption op2",
                        imgclass: "relimg2",
                        imgid: 'circuit02Img',
                        imgsrc: imgpath + "circuit02.png",
                        imgans:"incomplete",
                    },
                    {
                        imgdiv: "draggable commonoption op3",
                        imgclass: "relimg3",
                        imgid: 'circuitonImg',
                        imgsrc: imgpath + "circuiton.png",
                        imgans:"complete",
                    },
                    {
                        imgdiv: "draggable commonoption op4",
                        imgclass: "relimg4",
                        imgid: 'circuitoffImg',
                        imgsrc: imgpath + "circuitoff.png",
                        imgans:"incomplete",
                    },
                ]
            }]
        }],
        dropdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "circuit1 ansdiv1",
                        imgclass: "relativecls relimg1",
                        imgid: 'circuit01Img',
                        imgsrc: imgpath + "circuit01.png",
                        imgans:"complete",
                    },
                    {
                        imgdiv: "circuit2 ansdiv2",
                        imgclass: "relativecls relimg2",
                        imgid: 'circuit02Img',
                        imgsrc: imgpath + "circuit02.png",
                        imgans:"incomplete",
                    },
                    {
                        imgdiv: "circuiton ansdiv1",
                        imgclass: "relativecls relimg3",
                        imgid: 'circuitonImg',
                        imgsrc: imgpath + "circuiton.png",
                        imgans:"complete",
                    },
                    {
                        imgdiv: "circuitoff ansdiv2",
                        imgclass: "relativecls relimg4",
                        imgid: 'circuitoffImg',
                        imgsrc: imgpath + "circuitoff.png",
                        imgans:"incomplete",
                    }
                ]
            }],
            outerdiv:[
                {
                    textdiv:"completecircuit droppable",
                    textclass:" comcircuit",
                    textdata:data.string.q3a1,
                    ans:"complete",
                },
                {
                    textdiv:"incompletecircuit droppable",
                    textclass:"incomcircuit",
                    textdata:data.string.q3a2,
                    ans:"incomplete"
                }

            ]
        }],
        infoblock:[
            {
                textdiv:"tipiconmsg iconmsg maintitle",
                textclass:"content2 centertext",
                textdata:data.string.infomsg
            }
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.q4
            }

        ],
        dragdivclass:"dragdiv2",
        dropdivclass:"dropdiv2",
        dragdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "draggable commonoption1 o1",
                        imgclass: "relimg1",
                        imgid: 'q01Img',
                        imgsrc: imgpath + "circuit01.png",
                        imgans:"closed",
                    },
                    {
                        imgdiv: "draggable commonoption1 o2",
                        imgclass: "relimg2",
                        imgid: 'q02Img',
                        imgsrc: imgpath + "circuit02.png",
                        imgans:"open",
                    },
                    {
                        imgdiv: "draggable commonoption1 o3",
                        imgclass: "relimg3",
                        imgid: 'q03Img',
                        imgsrc: imgpath + "circuiton.png",
                        imgans:"open",
                    },
                    {
                        imgdiv: "draggable commonoption1 o4",
                        imgclass: "relimg4",
                        imgid: 'q04Img',
                        imgsrc: imgpath + "circuitoff.png",
                        imgans:"closed",
                    },
                    {
                        imgdiv: "draggable commonoption1 o5",
                        imgclass: "relimg5",
                        imgid: 'q05Img',
                        imgsrc: imgpath + "circuiton.png",
                        imgans:"open",
                    },
                    {
                        imgdiv: "draggable commonoption1 o6",
                        imgclass: "relimg6",
                        imgid: 'q06Img',
                        imgsrc: imgpath + "circuitoff.png",
                        imgans:"closed",
                    }
                ]
            }]
        }],
        dropdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "q01",
                        imgclass: "relativecls relimg1",
                        imgid: 'q01Img',
                        imgsrc: imgpath + "circuit01.png",
                        imgans:"closed",
                    },
                    {
                        imgdiv: "q02",
                        imgclass: "relativecls relimg2",
                        imgid: 'q02Img',
                        imgsrc: imgpath + "circuit02.png",
                        imgans:"open",
                    },
                    {
                        imgdiv: "q03",
                        imgclass: "relativecls relimg3",
                        imgid: 'q03Img',
                        imgsrc: imgpath + "circuiton.png",
                        imgans:"open",
                    },
                    {
                        imgdiv: "q04",
                        imgclass: "relativecls relimg4",
                        imgid: 'q04Img',
                        imgsrc: imgpath + "circuitoff.png",
                        imgans:"closed",
                    },
                    {
                        imgdiv: "q05",
                        imgclass: "relativecls relimg5",
                        imgid: 'q05Img',
                        imgsrc: imgpath + "circuiton.png",
                        imgans:"open",
                    },
                    {
                        imgdiv: "q06",
                        imgclass: "relativecls relimg6",
                        imgid: 'q06Img',
                        imgsrc: imgpath + "circuitoff.png",
                        imgans:"closed",
                    }
                ]
            }],
            outerdiv:[
                {
                    textdiv:"closeswitch droppable",
                    textclass:"closeswtch",
                    textdata:data.string.q4a1,
                    ans:"closed",
                },
                {
                    textdiv:"openswitch droppable",
                    textclass:"opnswtch",
                    textdata:data.string.q4a2,
                    ans:"open"
                }
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        ans:data.string.q5a1,
        uppertextblock: [
            {
                textclass: "content2 centertext mainquery",
                textdata: data.string.q5
            }

        ],
        imageblock: [{
          imagestoshow: [
                    {
                        imgdiv: "imagediv",
                        imgclass: "relativecls image1",
                        imgid: 'circuitonImg',
                        imgsrc: "",
                    },
              ]
        }],
        textblock:[
            {
                textdiv:"commontext choice1",
                textclass:"content3 centertext",
                textdata:data.string.q5a1
            },
            {
                textdiv:"commontext choice2",
                textclass:"content3 centertext",
                textdata:data.string.q5a2
            }
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        ans:data.string.q6a1,
        uppertextblock: [
            {
                textclass: "content2 centertext mainquery",
                textdata: data.string.q6
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imagediv",
                    imgclass: "relativecls image1",
                    imgid: 'submersibleImg',
                    imgsrc: "",
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commontext choicea",
                textclass:"content3 centertext",
                textdata:data.string.q6a1
            },
            {
                textdiv:"commontext choiceb",
                textclass:"content3 centertext",
                textdata:data.string.q6a2
            },
            {
                textdiv:"commontext choicec",
                textclass:"content3 centertext",
                textdata:data.string.q6a3
            }
        ]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        ans:data.string.q7a1,
        uppertextblock: [
            {
                textclass: "content2 centertext mainquery",
                textdata: data.string.q7
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imagediv",
                    imgclass: "relativecls image1",
                    imgid: 'circuitonImg',
                    imgsrc: "",
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commontext achoice",
                textclass:"content3 centertext",
                textdata:data.string.q7a1
            },
            {
                textdiv:"commontext bchoice",
                textclass:"content3 centertext",
                textdata:data.string.q7a2
            },
            {
                textdiv:"commontext cchoice",
                textclass:"content3 centertext",
                textdata:data.string.q7a3
            },
            {
                textdiv:"commontext dchoice",
                textclass:"content3 centertext",
                textdata:data.string.q7a4
            }
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        ans:data.string.q8a1,
        uppertextblock: [
            {
                textclass: "content2 centertext mainquery",
                textdata: data.string.q8
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imagediv imgbefore",
                    imgclass: "relativecls image1",
                    imgid: 'q08Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "imagediv imgafter",
                    imgclass: "relativecls image2",
                    imgid: 'q09Img',
                    imgsrc: "",
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commontext choice1",
                textclass:"content3 centertext",
                textdata:data.string.q8a1
            },
            {
                textdiv:"commontext choice2",
                textclass:"content3 centertext",
                textdata:data.string.q8a2
            }
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        ans:data.string.q9a1,
        uppertextblock: [
            {
                textclass: "content2 centertext mainquery",
                textdata: data.string.q9
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imagediv imgbefore",
                    imgclass: "relativecls image1",
                    imgid: 'q09Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "imagediv imgafter",
                    imgclass: "relativecls image2",
                    imgid: 'q10Img',
                    imgsrc: "",
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commontext achoice",
                textclass:"content3 centertext",
                textdata:data.string.q9a1
            },
            {
                textdiv:"commontext bchoice",
                textclass:"content3 centertext",
                textdata:data.string.q9a2
            },
            {
                textdiv:"commontext cchoice",
                textclass:"content3 centertext",
                textdata:data.string.q9a3
            },
            {
                textdiv:"commontext dchoice",
                textclass:"content3 centertext",
                textdata:data.string.q9a4
            }
        ],
        infoblock:[
            {
                textdiv:"s1  maintitle",
                textclass:"content3 centertext",
                textdata:data.string.s1
            },
            {
                textdiv:"s2  maintitle",
                textclass:"content3 centertext",
                textdata:data.string.s2
            }
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        ans:data.string.q10a1,
        uppertextblock: [
            {
                textclass: "content2 centertext mainquery",
                textdata: data.string.q10
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imagediv imgbefore",
                    imgclass: "relativecls image1",
                    imgid: 'q11Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "imagediv imgafter",
                    imgclass: "relativecls image2",
                    imgid: 'q12Img',
                    imgsrc: "",
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commontext achoice",
                textclass:"content3 centertext",
                textdata:data.string.q10a1
            },
            {
                textdiv:"commontext bchoice",
                textclass:"content3 centertext",
                textdata:data.string.q10a2
            },
            {
                textdiv:"commontext cchoice",
                textclass:"content3 centertext",
                textdata:data.string.q10a3
            },
            {
                textdiv:"commontext dchoice",
                textclass:"content3 centertext",
                textdata:data.string.q10a4
            }
        ],
        infoblock:[
            {
                textdiv:"info imgafter",
                textclass:"content3 centertext",
                textdata:data.string.info
            }
        ]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var scoreupdate = 11;
    var $total_page = content.length;
    // loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var score = new NumberTemplate();
    score.init($total_page-1);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverImg", src: imgpath + "a_08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "switchImg", src: imgpath + "switch02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bulbImg", src: imgpath + "bulb01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "batteryImg", src: imgpath + "battery01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wireImg", src: imgpath + "line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "circuit01Img", src: imgpath + "circuit01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "circuit02Img", src: imgpath + "circuit02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "circuitonImg", src: imgpath + "circuiton.png", type: createjs.AbstractLoader.IMAGE},
            {id: "circuitoffImg", src: imgpath + "circuitoff.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q01Img", src: imgpath + "q01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q02Img", src: imgpath + "q02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q03Img", src: imgpath + "q03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q04Img", src: imgpath + "q04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q05Img", src: imgpath + "q05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q06Img", src: imgpath + "q06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "submersibleImg", src: imgpath + "submersible_cable.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q08Img", src: imgpath + "q08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q09Img", src: imgpath + "q09.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q10Img", src: imgpath + "q10.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q11Img", src: imgpath + "q11.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q12Img", src: imgpath + "q12.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tipiconImg", src: "images/info_icon03.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            // {id: "sound_0", src: soundAsset + "p1_s0.ogg"},
            // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},
            // {id: "sound_2", src: soundAsset + "p1_s2.ogg"},
            // {id: "sound_3", src: soundAsset + "p1_s3.ogg"},
            // {id: "sound_4", src: soundAsset + "p1_s4.ogg"},
            // {id: "sound_5", src: soundAsset + "p1_s5.ogg"},
            // {id: "sound_6", src: soundAsset + "p1_s6.ogg"},
            // {id: "sound_7", src: soundAsset + "p1_s7.ogg"},
            // {id: "sound_8", src: soundAsset + "p1_s8.ogg"},
            // {id: "sound_9", src: soundAsset + "p1_s9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        score.init(content.length);
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        put_image2();
        put_image();
        score.numberOfQuestions();
        var questionnumber = countNext +1;
        $("#num_ques").text(questionnumber+". ");
        switch (countNext) {
            case 0:
                count=0;
                $(".commondropbox").find("img").hide();
                dragdrop("correctImg",false,4);
                break;
            case 1:
                count=0;
                $(".commondropbox1").find("img").hide();
                var optionclass = ["draggable option1","draggable option2","draggable option3","draggable option4"]
                shufflehint($(".dragdiv1"),optionclass);
                dragdrop("correctImg1",false,4);
                 break;
            case 2:
                count=0;
                $(".dropdiv2 img").hide();
                var optionclass = ["draggable commonoption op1","draggable commonoption op2","draggable commonoption op3","draggable commonoption op4"]
                shufflehint($(".dragdiv2"),optionclass);
                dragdrop("correctImg2",true,4);
                $(".iconmsg").hide();

                $(".icon").hover(
                    function(){
                        $(".iconmsg").show();
                    },
                    function(){
                        $(".iconmsg").hide();
                    }
                );
                break;
            case 3:
                count = 0;
                var optionclass = ["draggable commonoption1 o1","draggable commonoption1 o2","draggable commonoption1 o3","draggable commonoption1 o4","draggable commonoption1 o5","draggable commonoption1 o6"]
                shufflehint($(".dragdiv2"),optionclass);
                $(".dropdiv2 img").hide();
                dragdrop("correctImg2",true,6);
                break;
            case 4:
                shufflehint($(".rightdiv"),["commontext choice1","commontext choice2"]);
                checkans("ansImg");
                break;
            case 5:
                shufflehint($(".rightdiv"),["commontext choicea","commontext choiceb","commontext choicec"]);
                checkans("ansImg");
                break;
            case 6:
                shufflehint($(".rightdiv"),["commontext achoice","commontext bchoice","commontext cchoice","commontext dchoice"]);
                checkans("ansImg1");
                break;
            case 7:
                shufflehint($(".rightdiv"),["commontext choice1","commontext choice2"]);
                $(".imgafter").hide();
                checkans("ansImg1",$(".imgbefore"),$(".imgafter"));
                break;
            case 8:
                shufflehint($(".rightdiv"),["commontext achoice","commontext bchoice","commontext cchoice","commontext dchoice"]);
                $(".imgafter").hide();
                checkans("ansImg1",$(".imgbefore"),$(".imgafter"));
                break;
            case 9:
                shufflehint($(".rightdiv"),["commontext achoice","commontext bchoice","commontext cchoice","commontext dchoice"]);
                $(".imgafter").hide();
                checkans("ansImg1",$(".imgbefore"),$(".imgafter"),true);
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        // loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function put_image2() {
        if (content[count].hasOwnProperty('dragdiv')) {
            var contentCount = content[count].dragdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount,preload);
        }
        if (content[count].hasOwnProperty('dropdiv')) {
            var contentCount = content[count].dropdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount,preload);
        }
    }


    function shufflehint(optdiv,optionclass){
        for (var i = optdiv.children().length; i >= 0; i--) {
            optdiv.append(optdiv.children().eq(Math.random() * i | 0));
        }
        optdiv.children().removeClass();
        optdiv.children().each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).find("img").attr("data-answer")+"1");
        });
    }
    function dragdrop(correctImgcls,imgshow,countVal){
        var updateScore = 0;
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(this).css({"border":"none","background-color":"transparent"});

            },
            stop:function(event,ui){
                if(countNext>1){
                    $(this).css({"top":"","border":"2px solid","background-color":"white"});
                }
            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.find("img").attr("data-answer");
                var droppableans = $(this).attr("data-answer")
                if(draggableans.toString().trim() == (droppableans.toString().trim())) {
                    count++;
                    updateScore++;
                    $(this).removeClass("correctcss");
                   $(this).find("."+correctImgcls).remove();
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).find("img").show();
                    if(imgshow) {
                        var imgtoShow = ui.draggable.find("img").attr("class");
                        $("." + imgtoShow).show();
                    }
                    $(this).addClass("correctcss");
                   $(this).append("<img class='"+correctImgcls+"' src='images/right.png'/>");
                    (count == countVal)? navigationcontroller(countNext,$total_page):"";
                    if(updateScore == countVal && scoreupdate != countNext){
                        score.update(true);
                        scoreupdate = countNext;
                    }
                }
                else {
                    updateScore--;
                    play_correct_incorrect_sound(0);
                    // $(this).append("<img class='wrongImg' src='images/wrong.png'/>");
                    // ui.draggable.addClass("wrongcss");
                    // $(this).addClass("wrongcss");
                    score.update(false);

                }
            }
        });
    }
    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }
    function put_image() {
        var contentCount=content[countNext];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }
    function put_image2() {
        console.log("count value is "+count);
        if (content[countNext].hasOwnProperty('dragdiv')) {
            var contentCount = content[countNext].dragdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
        if (content[countNext].hasOwnProperty('dropdiv')) {
            var contentCount = content[countNext].dropdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = "";
                 imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function checkans(anscss,imgtohide,imgtoshow,moveup){
        var wrongclick = false;
        $(".commontext").on("click",function () {
            createjs.Sound.stop();
            if($(this).find('p').text().trim()==$(".objdiv").attr("data-answer").toString().trim()) {
                $(this).addClass("correctans");
                $(".commontext").addClass("avoid-clicks");
                $(this).append("<img class='"+anscss+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                imgtoshow?imgtoshow.show():"";
                imgtohide?imgtohide.hide():'';
                moveup?$(".imagediv").css("top","16%"):"";
                if(!wrongclick && scoreupdate != countNext){
                    score.update(true);
                    scoreupdate = countNext;
                }
                navigationcontroller();
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+anscss+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
                wrongclick = true;
                scoreupdate = countNext;
                score.update(false);
            }
        });
    }
});

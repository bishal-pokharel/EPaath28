var soundAsset = $ref+"/sounds/"+$lang+"/";
var imgpath = $ref+"/images/DIY/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv1 maintitle",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.diytitle
            }

        ],
        dragdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "switch draggable opt1",
                        imgclass: "relativecls img1",
                        imgid: 'switchImg',
                        imgsrc: imgpath + "switch01.png",
                        imgans:"switch",
                    },
                    {
                        imgdiv: "bulb draggable opt2",
                        imgclass: "relativecls img2",
                        imgid: 'bulbImg',
                        imgsrc: imgpath + "bulb01.png",
                        imgans:"bulb",
                    },
                    {
                        imgdiv: "battery draggable opt3",
                        imgclass: "relativecls img3",
                        imgid: 'batteryImg',
                        imgsrc: imgpath + "battery01.png",
                        imgans:"battery",
                    },
                    {
                        imgdiv: "wire draggable opt4",
                        imgclass: "relativecls img4",
                        imgid: 'wireImg',
                        imgsrc: imgpath + "line.png",
                        imgans:"wire",
                    }
                ]
            }]
        }],
        dropdiv: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "switchdrp droppable",
                        imgclass: "relativecls img5",
                        imgid: 'switch1Img',
                        imgsrc: imgpath + "switch02.png",
                        imgans:"switch",
                    },
                    {
                        imgdiv: "bulbdrp droppable",
                        imgclass: "relativecls img6",
                        imgid: 'bulb1Img',
                        imgsrc: imgpath + "bulb02.png",
                        imgans:"bulb",
                    },
                    {
                        imgdiv: "batterydrp droppable",
                        imgclass: "relativecls img7",
                        imgid: 'battery1Img',
                        imgsrc: imgpath + "battery02.png",
                        imgans:"battery",
                    },
                    {
                        imgdiv: "wiredrp droppable",
                        imgclass: "relativecls img8",
                        imgid: 'wire1Img',
                        imgsrc: imgpath + "line.png",
                        imgans:"wire",
                    },
                    {
                        imgdiv: "monkey",
                        imgclass: "relativecls img9",
                        imgid: 'monkeyImg',
                        imgsrc: imgpath + "welldone02.png",
                    }
                ]
            }]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverImg", src: imgpath + "a_08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "switchImg", src: imgpath + "switch03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "switch1Img", src: imgpath + "switch02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bulbImg", src: imgpath + "bulb03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bulb1Img", src: imgpath + "bulb03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "batteryImg", src: imgpath + "battery01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "battery1Img", src: imgpath + "battery02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wireImg", src: imgpath + "line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wire1Img", src: imgpath + "line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath + "welldone02.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s2_p2.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        put_image2(content, countNext, preload);
        switch (countNext) {
            case 1:
                sound_player("sound_1",false);
                shufflehint();
                dragdrop();
                $(".droppable").find("img").hide();
                break;
            default:
                play_diy_audio();
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function put_image2(content, count,preload) {
        if (content[count].hasOwnProperty('dragdiv')) {
            var contentCount = content[count].dragdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount,preload);
        }
        if (content[count].hasOwnProperty('dropdiv')) {
            var contentCount = content[count].dropdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount,preload);
        }
    }


    function shufflehint(){
        var optdiv = $(".dragdiv");

        for (var i = optdiv.children().length; i >= 0; i--) {
            optdiv.append(optdiv.children().eq(Math.random() * i | 0));
        }
        optdiv.children().removeClass();
        var optionclass = ["draggable opt1","draggable opt2","draggable opt3","draggable opt4"]
        optdiv.children().each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).find("img").attr("data-answer"));
        });
    }
    function dragdrop(){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
            stop:function(event,ui){
                $(".draggable").css("top","");

            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.find("img").attr("data-answer");
                if(draggableans.toString().trim() == ($(this).attr("data-answer").toString().trim())) {
                    count++;
                    // play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).find("img").show();
                    $(this).css("border","none");
                    // $(this).removeClass("droppable").addClass("correctcss relativecls");
                    // $(this).append("<img class='correctImg' src='images/right.png'/>");
                    // count++;
                    if(count>3){
                        $(".monkey").css("opacity",1);
                        navigationcontroller(countNext,$total_page,true);
                    }
                }
                else {
                    // play_correct_incorrect_sound(0);
                    // $(this).append("<img class='wrongImg' src='images/wrong.png'/>");
                    // ui.draggable.addClass("wrongcss");
                    // $(this).addClass("wrongcss");
                }
            }
        });
    }
});

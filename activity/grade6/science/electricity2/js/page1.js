var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        uppertextblockadditionalclass: "chapter",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.electricity
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage x hide2",
                    imgclass: "relativecls img1",
                    imgid: 'circuitoffImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "coverpage x hide1",
                    imgclass: "relativecls img2",
                    imgid: 'circuitonImg',
                    imgsrc: ""
                }

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.p1text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuitoff circuit hide2",
                    imgclass: "relativecls img1",
                    imgid: 'circuitoffImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circuiton circuit hide1",
                    imgclass: "relativecls img2",
                    imgid: 'circuitonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrowgif",
                    imgclass: "relativecls img3",
                    imgid: 'arrowgifImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "tipicon icon",
                    imgclass: "relativecls tipiconimg",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
              textdiv:"empty",
              textclass:"emptydiv",
              textdata:""
            },
            {
                textdiv:"switch maintitle",
                textclass:"content2 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"battery maintitle",
                textclass:"content2 centertext",
                textdata:data.string.battery
            },
            {
                textdiv:"wire maintitle",
                textclass:"content2 centertext",
                textdata:data.string.wire
            },
            {
                textdiv:"bulb maintitle",
                textclass:"content2 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"figelecrc maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figelecrc
            },
            {
                textdiv:"text1",
                textclass:"diyfont centertext",
                textdata:data.string.p1text2
            },
            {
                textdiv:"tipiconmsg iconmsg maintitle",
                textclass:"content2 centertext",
                textdata:data.string.tipiconmsg
            }
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv1 maintitle slideL",
        uppertextblock: [
            {
                textclass: "datafont centertext",
                textdata: data.string.p2text1
            }

        ],
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.titleBattery
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuiton1",
                    imgclass: "relativecls img1",
                    imgid: 'circuitonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "batteryimg",
                    imgclass: "relativecls img2",
                    imgid: 'batteryImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cellimg",
                    imgclass: "relativecls img3",
                    imgid: 'cellImg',
                    imgsrc: ""
                },
                // {
                //     imgdiv: "batarrowgif",
                //     imgclass: "relativecls img4",
                //     imgid: 'arrowgifImg',
                //     imgsrc: ""
                // },
            ]
        }],
        textblock:[
            {
                textdiv:"text2",
                textclass:"content4 centertext",
                textdata:data.string.p2text2
            }
        ],
        leftdivclass:"leftdiv",
        lefttextblock:[
            {
                textdiv:"switch1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"battery1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.battery
            },
            {
                textdiv:"wire1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.wire
            },
            {
                textdiv:"bulb1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"figelecrc1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figelecrc
            },
        ],
        rightdivclass:"rightdiv",
        righttextblock:[
            {
                textdiv:"figbat maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figbat
            },
            {
                textdiv:"figcell maintitle",
                textclass:"diyfont centertext",
                textdata:data.string.figcell
            }
        ]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.titleWire
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuiton1",
                    imgclass: "relativecls img1",
                    imgid: 'circuitonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "submersiblecable",
                    imgclass: "relativecls img2",
                    imgid: 'submersiblecableImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow1",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow2",
                    imgclass: "relativecls img4",
                    imgid: 'arrowImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"text2",
                textclass:"content4 centertext",
                textdata:data.string.p2text3
            }
        ],
        leftdivclass:"leftdiv",
        lefttextblock:[
            {
                textdiv:"switch1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"battery1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.battery
            },
            {
                textdiv:"wire1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.wire
            },
            {
                textdiv:"bulb1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"figelecrc1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figelecrc
            },
        ],
        rightdivclass:"rightdiv",
        righttextblock:[
            {
                textdiv:"figwires maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figwires
            },
            {
                textdiv:"copperiron maintitle",
                textclass:"content2 centertext",
                textdata:data.string.copperiron
            },
            {
                textdiv:"plastic maintitle",
                textclass:"content2 centertext",
                textdata:data.string.plastic
            },
        ]
    },

    // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.titleLightBulb
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuiton2 hide1",
                    imgclass: "relativecls img1",
                    imgid: 'circuitonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circuiton2 hide2",
                    imgclass: "relativecls img2",
                    imgid: 'circuitoffImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "lightarrowgif",
                    imgclass: "relativecls img3",
                    imgid: 'arrowgifImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "tipicon2 icon",
                    imgclass: "relativecls tipiconimg",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "bulboff",
                    imgclass: "relativecls img5",
                    imgid: 'bulboffImg',
                    imgsrc: ""
                }
            ]
        }],
        leftdivclass:"leftdiv",
        lefttextblock:[
            {
                textdiv:"empty2",
                textclass:"emptydiv",
                textdata:""
            },
            {
                textdiv:"lightswitch maintitle",
                textclass:"content2 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"lightbattery maintitle",
                textclass:"content2 centertext",
                textdata:data.string.battery
            },
            {
                textdiv:"lightwire maintitle",
                textclass:"content2 centertext",
                textdata:data.string.wire
            },
            {
                textdiv:"lightbulb maintitle",
                textclass:"content2 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"figconducting maintitle hide1",
                textclass:"content2 centertext",
                textdata:data.string.figcompelecir
            },
            {
                textdiv:"figconducting maintitle hide2",
                textclass:"content2 centertext",
                textdata:data.string.figincompelecir
            },
            {
                textdiv:"tipiconmsg2 iconmsg maintitle",
                textclass:"content4 centertext",
                textdata:data.string.tipiconmsg
            }
        ],
        rightdivclass:"rightdiv",
        ui:true,
        righttextblock:[
            {
                textdiv:"figlightbulb maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figlightbulb
            }
        ],
        textblock:[
            {
                textdiv:"text2",
                textclass:"content2 centertext",
                textdata:data.string.p2text7_1
            }
        ]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.titleLightBulb
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuitoff circuit hide2",
                    imgclass: "relativecls img1",
                    imgid: 'circuitoffImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circuiton circuit hide1",
                    imgclass: "relativecls img2",
                    imgid: 'circuitonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrowgif",
                    imgclass: "relativecls img3",
                    imgid: 'arrowgifImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "tipicon icon",
                    imgclass: "relativecls tipiconimg",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"empty",
                textclass:"emptydiv",
                textdata:""
            },
            {
                textdiv:"switch maintitle",
                textclass:"content2 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"battery maintitle",
                textclass:"content2 centertext",
                textdata:data.string.battery
            },
            {
                textdiv:"wire maintitle",
                textclass:"content2 centertext",
                textdata:data.string.wire
            },
            {
                textdiv:"bulb maintitle",
                textclass:"content2 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"figelecrc maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figcompelecir
            },
            {
                textdiv:"text1",
                textclass:"diyfont centertext",
                textdata:data.string.p2text7_2
            },
            {
                textdiv:"tipiconmsg iconmsg maintitle",
                textclass:"content2 centertext",
                textdata:data.string.tipiconmsg
            }
        ]
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.titleLightBulb
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuitoff circuit hide2",
                    imgclass: "relativecls img1",
                    imgid: 'circuitoffImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circuiton circuit hide1",
                    imgclass: "relativecls img2",
                    imgid: 'circuitonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrowgif",
                    imgclass: "relativecls img3",
                    imgid: 'arrowgifImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "tipicon icon",
                    imgclass: "relativecls tipiconimg",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"empty",
                textclass:"emptydiv",
                textdata:""
            },
            {
                textdiv:"switch maintitle",
                textclass:"content2 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"battery maintitle",
                textclass:"content2 centertext",
                textdata:data.string.battery
            },
            {
                textdiv:"wire maintitle",
                textclass:"content2 centertext",
                textdata:data.string.wire
            },
            {
                textdiv:"bulb maintitle",
                textclass:"content2 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"figelecrc maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figincompelecir
            },
            {
                textdiv:"text1",
                textclass:"diyfont centertext",
                textdata:data.string.p2text7_3
            },
            {
                textdiv:"tipiconmsg iconmsg maintitle",
                textclass:"content2 centertext",
                textdata:data.string.tipiconmsg
            }
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.titleSwitch
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuiton1",
                    imgclass: "relativecls img1",
                    imgid: 'circuitonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "switchoffimg",
                    imgclass: "relativecls img2",
                    imgid: 'switchoffImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "simpleswitch",
                    imgclass: "relativecls img3",
                    imgid: 'simpleswitchImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"text2",
                textclass:"content4 centertext",
                textdata:data.string.p2text5
            }
        ],
        leftdivclass:"leftdiv",
        lefttextblock:[
            {
                textdiv:"switch1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"battery1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.battery
            },
            {
                textdiv:"wire1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.wire
            },
            {
                textdiv:"bulb1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"figelecrc1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figelecrc
            },
        ],
        rightdivclass:"rightdiv",
        righttextblock:[
            {
                textdiv:"figswitch maintitle",
                textclass:"content2 centertext",
                textdata:data.string.figswitch
            },
            {
                textdiv:"righttext1 maintitle",
                textclass:"content2 centertext",
                textdata:data.string.p2text4
            },
        ]
    },

    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.titleSwitch
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuiton5 hide2",
                    imgclass: "relativecls img1",
                    imgid: 'circuitImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circuiton5 hide1",
                    imgclass: "relativecls img2",
                    imgid: 'circuit1Img',
                    imgsrc: ""
                },

                {
                    imgdiv: "arrow3 hide2",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow4 hide2",
                    imgclass: "relativecls img4",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow5 hide2",
                    imgclass: "relativecls img5",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow6 hide1",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow7 hide1",
                    imgclass: "relativecls img7",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow8 hide1",
                    imgclass: "relativecls img8",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrowgif1",
                    imgclass: "relativecls img9",
                    imgid: 'arrowgifImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "tipicon1 icon",
                    imgclass: "relativecls tipiconimg",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"empty1",
                textclass:"emptydiv",
                textdata:""
            },
            {
                textdiv:"bulb2 maintitle",
                textclass:"content4 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"cell maintitle",
                textclass:"content4 centertext",
                textdata:data.string.cell
            },
            {
                textdiv:"switch2 maintitle",
                textclass:"content4 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"metalcond maintitle",
                textclass:"content4 centertext",
                textdata:data.string.metalcond
            },
            {
                textdiv:"plasticncond maintitle",
                textclass:"content4 centertext",
                textdata:data.string.plasticcond
            },
            {
                textdiv:"break maintitle hide2",
                textclass:"content4 centertext",
                textdata:data.string.breakcond
            },
            {
                textdiv:"nonbreak maintitle hide1",
                textclass:"content4 centertext",
                textdata:data.string.nobreakcond
            },
            {
                textdiv:"figconducting1 maintitle hide2",
                textclass:"content4 centertext",
                textdata:data.string.figconducting
            },
            {
                textdiv:"figconducting1 maintitle hide1",
                textclass:"content4 centertext",
                textdata:data.string.fignonconducting
            },
            {
                textdiv:"tipiconmsg1 iconmsg maintitle",
                textclass:"content4 centertext",
                textdata:data.string.tipiconmsg
            },
            {
                textdiv:"text2",
                textclass:"content2 centertext",
                textdata:data.string.p2text6_2
            },
        ]
    },
    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv maintitle",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.titleSwitch
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "circuiton5 hide2",
                    imgclass: "relativecls img1",
                    imgid: 'circuitImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circuiton5 hide1",
                    imgclass: "relativecls img2",
                    imgid: 'circuit1Img',
                    imgsrc: ""
                },

                {
                    imgdiv: "arrow3 hide2",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow4 hide2",
                    imgclass: "relativecls img4",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow5 hide2",
                    imgclass: "relativecls img5",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow6 hide1",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow7 hide1",
                    imgclass: "relativecls img7",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow8 hide1",
                    imgclass: "relativecls img8",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrowgif1",
                    imgclass: "relativecls img9",
                    imgid: 'arrowgifImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "tipicon1 icon",
                    imgclass: "relativecls tipiconimg",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"empty1",
                textclass:"emptydiv",
                textdata:""
            },
            {
                textdiv:"bulb2 maintitle",
                textclass:"content4 centertext",
                textdata:data.string.bulb
            },
            {
                textdiv:"cell maintitle",
                textclass:"content4 centertext",
                textdata:data.string.cell
            },
            {
                textdiv:"switch2 maintitle",
                textclass:"content4 centertext",
                textdata:data.string.switch
            },
            {
                textdiv:"metalcond maintitle",
                textclass:"content4 centertext",
                textdata:data.string.metalcond
            },
            {
                textdiv:"plasticncond maintitle",
                textclass:"content4 centertext",
                textdata:data.string.plasticcond
            },
            {
                textdiv:"break maintitle hide2",
                textclass:"content4 centertext",
                textdata:data.string.breakcond
            },
            {
                textdiv:"nonbreak maintitle hide1",
                textclass:"content4 centertext",
                textdata:data.string.nobreakcond
            },
            {
                textdiv:"figconducting1 maintitle hide2",
                textclass:"content4 centertext",
                textdata:data.string.figconducting
            },
            {
                textdiv:"figconducting1 maintitle hide1",
                textclass:"content4 centertext",
                textdata:data.string.fignonconducting
            },
            {
                textdiv:"tipiconmsg1 iconmsg maintitle",
                textclass:"content4 centertext",
                textdata:data.string.tipiconmsg
            },
            {
                textdiv:"text2",
                textclass:"content2 centertext",
                textdata:data.string.p2text6_3
            },
        ]
    }

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "circuitoffImg", src: imgpath + "circuitoff.png", type: createjs.AbstractLoader.IMAGE},
            {id: "circuitonImg", src: imgpath + "arrowmoves01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "switchoffImg", src: imgpath + "switch_off.png", type: createjs.AbstractLoader.IMAGE},
            {id: "switchonImg", src: imgpath + "switch_on.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tipiconImg", src: "images/info_icon03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "batteryImg", src: imgpath + "battery.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cellImg", src: imgpath + "cell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "submersiblecableImg", src: imgpath + "submersible_cable.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath1 + "arrow01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "simpleswitchImg", src: imgpath + "simple_switch.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowgifImg", src: imgpath + "arrowmoves.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "circuitImg", src: imgpath + "circuit02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "circuit1Img", src: imgpath + "arrowmoves02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow1Img", src: imgpath1 + "arrow04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bulboffImg", src: imgpath + "bulb01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "switchoffImg", src: imgpath + "switch_off.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p10.ogg"},
            {id: "sound_11", src: soundAsset + "s1_p11.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_1",true);
                $(".hide1").hide();
                time = setInterval(function(){
                    $('.hide2').delay(500).toggle();
                    $(".contentblock").delay(500).toggleClass("coverback1");
                    $('.hide1').delay(500).toggle();
                },1500);
                break;
            case 1:
                count = 0;
                clearInterval(time);
                showhide($(".arrowgif"),$(".empty"),"sound_2");
                break;
            case 2:
                setTimeout(function () {
                    sound_player("sound_"+(countNext+1),true);
                },2000);
            case 5:
                count = 0;
                showhide($(".lightarrowgif"),$(".empty2"),"sound_6");
                break;
            case 6:
                count = 0;
                showhide($(".arrowgif"),$(".empty"),"sound_7");
                break;
            case 7:
                count = 0;
                showhide($(".arrowgif"),$(".empty"),"sound_8");
                break;
            case 9:
                count = 0;
                showhide($(".arrowgif1"),$(".empty1"),"sound_10");
                break;
            case 10:
                count = 0;
                showhide($(".arrowgif1"),$(".empty1"),"sound_11");
                break;
            default:
                sound_player("sound_"+(countNext+1),true);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function showhide(gifelem,clickelem,soundid){
        $(".hide1").hide();
        gifelem.empty();
        clickelem.click(function(){
            count==0?sound_player(soundid,true):navigationcontroller();
            count++;
            if($(this).hasClass("circuitgeton")){
                $(".hide2").fadeIn();
                $(".hide1").fadeOut();
                gifelem.empty();
                $(this).removeClass("circuitgeton");
            }
            else{
                $(".hide1").fadeIn();
                $(".hide2").fadeOut();
                // gifelem.append('<img class="relativecls" src="'+imgpath+'arrowmoves.gif">'); // reload it
                $(this).addClass("circuitgeton");
            }
        });
        $('ui > li').each(function(i){
            $(this).hide().delay(i*1000).fadeIn(2000);}
        );
        $(".iconmsg").hide();

        $(".icon").hover(
            function(){
                $(".iconmsg").show();
            },
            function(){
                $(".iconmsg").hide();
            }
        );
    }

});

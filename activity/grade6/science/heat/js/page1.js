var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "datafont centertext",
                textdata: data.string.chaptertitle
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'nexttitle',
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.p1text1
            },
            {
                textdiv:"figure animateCoffee",
                textclass: "content2 centertext",
                textdata: data.string.fig1
            },
            {
                textdiv:"figure1 show2",
                textclass: "content2 centertext",
                textdata: data.string.fig2
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "coffee animateCoffee",
                    imgclass: "img2",
                    imgid: 'coffeeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "vapour animateVapour",
                    imgclass: "relativecls img3",
                    imgid: 'vapourImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "coffee1 show2",
                    imgclass: "img4",
                    imgid: 'coffee1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "vapour1 show2",
                    imgclass: "relativecls img5",
                    imgid: 'vapourImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.p1text2
            },
            {
                textdiv:"figure2",
                textclass: "content2 centertext",
                textdata: data.string.fig2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "coffee2",
                    imgclass: "img2",
                    imgid: 'coffee2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "coffee2 show2",
                    imgclass: "img3",
                    imgid: 'coffee1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "vapour",
                    imgclass: "relativecls img4",
                    imgid: 'vapourImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.p1text3
            },
            {
                textdiv:"figure2",
                textclass: "content2 centertext",
                textdata: data.string.fig2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "coffee2",
                    imgclass: "img3",
                    imgid: 'coffee1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "vapour",
                    imgclass: "relativecls img4",
                    imgid: 'vapourImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.p1text4
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.yes
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.no
            },
            {
                textdiv:"correctmsg",
                textclass: "content2 centertext",
                textdata: data.string.p1text5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "coffee3",
                    imgclass: "img3",
                    imgid: 'coffee1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "vapour2",
                    imgclass: "relativecls img4",
                    imgid: 'vapourImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar3",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar4",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar5",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar6",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle1",
                textclass: "content1 centertext",
                textdata: data.string.remember
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig3
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig4
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coffee4",
                    imgclass: "img3",
                    imgid: 'coffee1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "vapour3",
                    imgclass: "relativecls img4",
                    imgid: 'vapourImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar7",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar8",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar9",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar10",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
        listblock:[
            {
                textclass:"content4",
                textdata:data.string.p1text6
            },
            {
                textclass:"content4",
                textdata:data.string.p1text7
            },
            {
                textclass:"content4",
                textdata:data.string.p1text8
            }
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle1",
                textclass: "content1 centertext",
                textdata: data.string.remember
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig3
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig4
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coffee4",
                    imgclass: "img3",
                    imgid: 'coffee1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "vapour3",
                    imgclass: "relativecls img4",
                    imgid: 'vapourImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar7",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar8",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar9",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar10",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
        listblock:[
            {
                textclass:"content4",
                textdata:data.string.p1text6
            },
            {
                textclass:"content4",
                textdata:data.string.p1text7
            },
            {
                textclass:"content4",
                textdata:data.string.p1text8
            }
        ]
    },
    //  slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle1",
                textclass: "content1 centertext",
                textdata: data.string.remember
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig3
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig4
            },
            {
                textdiv:"correctmsg1",
                textclass: "content3 centertext",
                textdata: data.string.fig5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coffee4",
                    imgclass: "img3",
                    imgid: 'coffee1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "vapour3",
                    imgclass: "relativecls img4",
                    imgid: 'vapourImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar7",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar8",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar9",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar10",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
        listblock:[
            {
                textclass:"content4",
                textdata:data.string.p1text6
            },
            {
                textclass:"content4",
                textdata:data.string.p1text7
            },
            {
                textclass:"content4",
                textdata:data.string.p1text8
            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath + "cover_page.png", type: createjs.AbstractLoader.IMAGE},
            {id: "malaImg", src: imgpath + "mala02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coffeeImg", src: imgpath + "coffee03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "vapourImg", src: imgpath + "smoke_effect01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "coffee1Img", src: imgpath + "coffee05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coffee2Img", src: imgpath + "coffee04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "red_arrow02.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_4a", src: soundAsset + "s1_p5_1.ogg"},
            {id: "sound_4b", src: soundAsset + "s1_p5_2.ogg"},
            {id: "sound_5a", src: soundAsset + "s1_p6_1.ogg"},
            {id: "sound_5b", src: soundAsset + "s1_p6_2.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p8.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 1:
                $(".show2, .nexttitle").css("opacity","0");
                $(".show2").delay(3500).animate({"opacity":"1"},1000);
                setTimeout(function(){
                  $(".nexttitle").animate({opacity:"1"},500);
                },3900);
                sound_player_nav("sound_"+countNext);
                break;
            case 2:
                $(".show2").css("opacity","0").delay(2500).animate({"opacity":"1"},1000);
                sound_player_nav("sound_"+countNext);
                break;
            case 4:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_4");
            current_sound.play();
            current_sound.on('complete', function(){
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("sound_4a");
              current_sound.play();
            });
                checkans(data.string.yes);
                break;
            case 5:
                $(".listdiv li").hide();
                $(".listdiv li").eq(0).delay(1000).fadeIn();
                $(".correctmsg1").eq(0).delay(2000).animate({"opacity":"1"},1000);
                $(".ar7 img").delay(2000).animate({"opacity":"1","width":"100%"},1000);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_5a");
                current_sound.play();
                current_sound.on('complete', function(){
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("sound_5b");
                  current_sound.play();
                  current_sound.on('complete', function(){
                    nav_button_controls(0);
                  });
                });
                break;
            case 6:
                $(".listdiv li").hide();
                $(".listdiv li").eq(0).show();
                $(".correctmsg1").eq(0).css({"opacity":"1"});
                $(".ar7 img").css({"opacity":"1","width":"100%"});
                $(".listdiv li").eq(1).delay(1000).fadeIn();
                $(".correctmsg1").eq(0).delay(2000).animate({"opacity":"0"},500);
                $(".correctmsg1").eq(1).delay(2000).animate({"opacity":"1"},500);
                $(".ar8 img").delay(2000).animate({"opacity":"1","width":"100%"},1000);
                setTimeout(function(){
                  sound_player_nav("sound_"+countNext);
                },1000);

                break;
            case 7:
                $(".listdiv li").hide();
                $(".listdiv li").eq(0).show();
                $(".correctmsg1").eq(0).css({"opacity":"0"});
                $(".correctmsg1").eq(1).css({"opacity":"1"});
                $(".ar7 img").css({"opacity":"1","width":"100%"});
                $(".ar8 img").css({"opacity":"1","width":"100%"});
                $(".listdiv li").eq(1).show();
                $(".listdiv li").eq(2).delay(1000).fadeIn();
                $(".correctmsg1").eq(1).delay(2000).animate({"opacity":"0"},500);
                $(".correctmsg1").eq(2).delay(2000).animate({"opacity":"1"},500);
                $(".ar9 img").delay(3000).animate({"opacity":"1","width":"100%"},1000);
                $(".ar10 img").delay(3500).animate({"opacity":"1","width":"100%"},1000);
                setTimeout(function(){
                  sound_player_nav("sound_"+countNext);
                },1000);
                break;
            default:
                sound_player_nav("sound_"+countNext);
                break;
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}

  	function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}

  	function sound_player_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls();
  		});
  	}

    function templateCaller() {checkans
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function checkans(ans) {
        $(".option").click(function () {
            if ($(this).find("p").text() == ans) {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                $(".correctmsg").delay(100).animate({"opacity":"1"},100);
                setTime = setInterval(function(){
                    $(".ar5 img").addClass("animatearrow");
                    setTimeout(function () {
                        $(".ar6 img").addClass("animatearrow");
                        // navigationcontroller(countNext, $total_page);
                    },1000);
                },1500);
                if(countNext==4){
                  sound_player_nav("sound_4b");
                }
            }
            else {
                $(this).addClass("wrongans avoid-clicks");
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
            }
        })
    }
});

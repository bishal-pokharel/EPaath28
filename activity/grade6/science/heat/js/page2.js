var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "chapter centertext",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text1
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blank',
                textdiv:"question1",
                textclass: "diyfont centertext",
                textdata: data.string.p2text2
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.p2text2_1
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.p2text2_2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup",
                    imgclass: "relativecls img2",
                    imgid: 'cupImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    // slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text1
            },
            {
                textdiv:"emptydiv1"
            },
            {
                textdiv:"question2",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blank',
                textdiv:"question3",
                textclass: "diyfont centertext",
                textdata: data.string.p2text3
            },
            {
                textdiv:"option opt3",
                textclass: "content1 centertext",
                textdata: data.string.p2text2_1
            },
            {
                textdiv:"option opt4",
                textclass: "content1 centertext",
                textdata: data.string.p2text2_2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup1",
                    imgclass: "relativecls img2",
                    imgid: 'cupImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text1
            },
            {
                textdiv:"emptydiv1"
            },
            {
                textdiv:"question2",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blank',
                textdiv:"question3",
                textclass: "diyfont centertext",
                textdata: data.string.p2text4
            },
            {
                textdiv:"option opt3",
                textclass: "content2 centertext",
                textdata: data.string.p2text4_1
            },
            {
                textdiv:"option opt4",
                textclass: "content2 centertext",
                textdata: data.string.p2text4_2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup1",
                    imgclass: "relativecls img2",
                    imgid: 'cupImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text5
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blank',
                textdiv:"subtitle",
                textclass: "diyfont centertext",
                textdata: data.string.p2text6
            },
            {
                textdiv:"showhide figure",
                textclass: "diyfont centertext",
                textdata: data.string.p2text7
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup3",
                    imgclass: "relativecls img2",
                    imgid: 'cupImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text8
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },

            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.yes
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.no
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup",
                    imgclass: "relativecls img2",
                    imgid: 'cup4Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text9
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blankfield',
                textdiv:"subtitle",
                textclass: "diyfont centertext",
                textdata: data.string.p2text10
            },
            {
                textdiv:"showhide figure",
                textclass: "diyfont centertext",
                textdata: data.string.p2text11
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup3",
                    imgclass: "relativecls img2",
                    imgid: 'cup4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text12
            },
            {
                textdiv:"emptydiv1"
            },
            {
                textdiv:"question2",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                textdiv:"option opt3",
                textclass: "content3 centertext",
                textdata: data.string.p2text12_1
            },
            {
                textdiv:"option opt4",
                textclass: "content3 centertext",
                textdata: data.string.p2text12_2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup1",
                    imgclass: "relativecls img2",
                    imgid: 'cup4Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text13
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blankfield',
                textdiv:"subtitle",
                textclass: "diyfont centertext",
                textdata: data.string.p2text14
            },
            {
                textdiv:"showhide figure",
                textclass: "content3 centertext",
                textdata: data.string.p2text15
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup3",
                    imgclass: "relativecls img2",
                    imgid: 'cupImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar3",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text16
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blank',
                textdiv:"question1",
                textclass: "diyfont centertext",
                textdata: data.string.p2text17
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.p2text17_1
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.p2text17_2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup",
                    imgclass: "relativecls img2",
                    imgid: 'cup2Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text16
            },
            {
                textdiv:"emptydiv1"
            },
            {
                textdiv:"question2",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blank',
                textdiv:"question3",
                textclass: "diyfont centertext",
                textdata: data.string.p2text18
            },
            {
                textdiv:"option opt3",
                textclass: "content1 centertext",
                textdata: data.string.p2text17_1
            },
            {
                textdiv:"option opt4",
                textclass: "content1 centertext",
                textdata: data.string.p2text17_2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup1",
                    imgclass: "relativecls img2",
                    imgid: 'cup2Img',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text16
            },
            {
                textdiv:"emptydiv1"
            },
            {
                textdiv:"question2",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blank',
                textdiv:"question3",
                textclass: "diyfont centertext",
                textdata: data.string.p2text19
            },
            {
                textdiv:"option opt3",
                textclass: "content2 centertext",
                textdata: data.string.p2text19_1
            },
            {
                textdiv:"option opt4",
                textclass: "content2 centertext",
                textdata: data.string.p2text19_2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup1",
                    imgclass: "relativecls img2",
                    imgid: 'cup2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar4",
                    imgclass: "relativecls img3",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text20
            },
            {
                textdiv:"subtitle",
                textclass: "diyfont centertext",
                textdata: data.string.p2text21
            },
            {
                textdiv:"showhide figure",
                textclass: "content2 centertext",
                textdata: data.string.p2text22
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup3",
                    imgclass: "relativecls img2",
                    imgid: 'cup3Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 13
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text23
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },

            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.yes
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.no
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup",
                    imgclass: "relativecls img2",
                    imgid: 'cup3Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 14
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text24
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blankfield',
                textdiv:"subtitle",
                textclass: "diyfont centertext",
                textdata: data.string.p2text25
            },
            {
                textdiv:"showhide figure",
                textclass: "content2 centertext",
                textdata: data.string.p2text26
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup3",
                    imgclass: "relativecls img2",
                    imgid: 'cup3Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 15
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text27
            },
            {
                textdiv:"emptydiv1"
            },
            {
                textdiv:"question2",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blank',
                textdiv:"question3",
                textclass: "diyfont centertext",
                textdata: data.string.p2text28
            },
            {
                textdiv:"option opt3",
                textclass: "content3 centertext",
                textdata: data.string.p2text28_1
            },
            {
                textdiv:"option opt4",
                textclass: "content3 centertext",
                textdata: data.string.p2text28_2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup1",
                    imgclass: "relativecls img2",
                    imgid: 'cup2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar4",
                    imgclass: "relativecls img3",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 16
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text29
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'blankfield',
                textdiv:"subtitle",
                textclass: "diyfont centertext",
                textdata: data.string.p2text30
            },
            {
                textdiv:"showhide figure",
                textclass: "content3 centertext",
                textdata: data.string.p2text31
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup3",
                    imgclass: "relativecls img2",
                    imgid: 'cup3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar5",
                    imgclass: "relativecls img3",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar6",
                    imgclass: "relativecls img4",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
    // slide 17
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content4 centertext",
                textdata: data.string.p2text32
            },
            {
                textdiv:"text1",
                textclass: "content2 centertext",
                textdata: data.string.p2text33
            },
            {
                textdiv:"text2",
                textclass: "content2 centertext",
                textdata: data.string.p2text34
            },
            {
                textdiv:"line",
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup_1",
                    imgclass: "relativecls img2",
                    imgid: 'cup1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup_2",
                    imgclass: "relativecls img3",
                    imgid: 'cup3Img',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 18
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p2text35
            },
            {
                textdiv:"text1",
                textclass: "content2 centertext",
                textdata: data.string.p2text36
            },
            {
                textdiv:"text2",
                textclass: "content2 centertext",
                textdata: data.string.p2text37
            },
            {
                textdiv:"line",
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup_1",
                    imgclass: "relativecls img2",
                    imgid: 'cup4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup_2",
                    imgclass: "relativecls img3",
                    imgid: 'cup5Img',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 18
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle1",
                textclass: "content2 centertext",
                textdata: data.string.p2text38
            },
            {
                textdiv:"text1_1",
                textclass: "content2 centertext",
                textdata: data.string.p2text36
            },
            {
                textdiv:"text2_1",
                textclass: "content2 centertext",
                textdata: data.string.p2text37
            },
            {
                textdiv:"line1",
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup_11",
                    imgclass: "relativecls img2",
                    imgid: 'cup1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "cup_22",
                    imgclass: "relativecls img3",
                    imgid: 'cup3Img',
                    imgsrc: ""
                },
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath + "bg_diy01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cupImg", src: imgpath + "cup_hand.png", type: createjs.AbstractLoader.IMAGE},
            {id: "malaImg", src: imgpath + "mala02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "red_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow1Img", src: imgpath + "red_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cup1Img", src: imgpath + "cup_hand_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cup2Img", src: imgpath + "ice_hand.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cup3Img", src: imgpath + "ice_hand_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cup4Img", src: imgpath + "cup_hand_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cup5Img", src: imgpath + "ice_hand_arrow.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0a", src: soundAsset + "s2_p2_1.ogg"},
            {id: "sound_0b", src: soundAsset + "s2_p2_2.ogg"},
            {id: "sound_4a", src: soundAsset + "s2_p5_1.ogg"},
            {id: "sound_4b", src: soundAsset + "s2_p5_2.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p6.ogg"},
            {id: "sound_6a", src: soundAsset + "s2_p7_1.ogg"},
            {id: "sound_6b", src: soundAsset + "s2_p7_2.ogg"},
            {id: "sound_7", src: soundAsset + "s2_p8.ogg"},
            {id: "sound_8a", src: soundAsset + "s2_p9_1.ogg"},
            {id: "sound_8b", src: soundAsset + "s2_p9_2.ogg"},
            {id: "sound_9", src: soundAsset + "s2_p10.ogg"},
            {id: "sound_12a", src: soundAsset + "s2_p13_1.ogg"},
            {id: "sound_12b", src: soundAsset + "s2_p13_2.ogg"},
            {id: "sound_13", src: soundAsset + "s2_p14.ogg"},
            {id: "sound_14a", src: soundAsset + "s2_p15_1.ogg"},
            {id: "sound_14b", src: soundAsset + "s2_p15_2.ogg"},
            {id: "sound_15", src: soundAsset + "s2_p16.ogg"},
            {id: "sound_16a", src: soundAsset + "s2_p17_1.ogg"},
            {id: "sound_16b", src: soundAsset + "s2_p17_2.ogg"},
            {id: "sound_17", src: soundAsset + "s2_p18.ogg"},
            {id: "sound_18", src: soundAsset + "s2_p19.ogg"},
            {id: "sound_19", src: soundAsset + "s2_p20.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
            play_diy_audio();
            nav_button_controls(2000);
            break;
            case 1:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_0a");
            current_sound.play();
            current_sound.on('complete', function(){
            createjs.Sound.stop();
        		current_sound = createjs.Sound.play("sound_0b");
        		current_sound.play();
        	});
                shufflehint();
                checkans(data.string.p2text2_1,"correctwrongimg");
                break;
            case 2:
                shufflehint();
                checkans(data.string.p2text2_2,"correctwrongimg");
                break;
            case 3:
                shufflehint();
                checkans(data.string.p2text4_1,"correctwrongimg1");
                break;
            case 4:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_4a");
            current_sound.play();
            current_sound.on('complete', function(){
              setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_4b");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(0);
                });
              },2400);
          });
          break;
            case 5:
            sound_player("sound_5");
                shufflehint();
                $(".option").css("top","61%");
                checkans(data.string.yes,"correctwrongimg");
                break;
            case 6:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_6a");
            current_sound.play();
            current_sound.on('complete', function(){
              setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_6b");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(0);
                });
              });
            });
            break;
            case 7:
            sound_player("sound_7");
                shufflehint();
                $(".option").css("top","61%");
                checkans(data.string.p2text12_1,"correctwrongimg");
                break;
            case 8:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_8a");
            current_sound.play();
            current_sound.on('complete', function(){
              setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_8b");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(0);
                });
              });
            });
            break;
            case 9:
            sound_player("sound_9");
                shufflehint();
                checkans(data.string.p2text17_1,"correctwrongimg");
                break;
            case 10:
                shufflehint();
                checkans(data.string.p2text17_2,"correctwrongimg");
                break;
            case 11:
                shufflehint();
                checkans(data.string.p2text19_1,"correctwrongimg");
                break;
            case 12:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_12a");
            current_sound.play();
            current_sound.on('complete', function(){
              setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_12b");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(0);
                });
              },2500);
          });
          break;
            case 13:
                sound_player("sound_13");
                shufflehint();
                $(".option").css("top","61%");
                checkans(data.string.yes,"correctwrongimg");
                break;
          case 14:
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("sound_14a");
          current_sound.play();
          current_sound.on('complete', function(){
            setTimeout(function(){
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("sound_14b");
              current_sound.play();
              current_sound.on('complete', function(){
                nav_button_controls(0);
              });
            });
          });
          break;
            case 15:
            sound_player("sound_15");
                shufflehint();
                checkans(data.string.p2text28_1,"correctwrongimg");
                break;
            case 16:
                shufflehint();
                $(".cup3").css("top","35%");
                $(".figure").css("bottom","2%");
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_16a");
                current_sound.play();
                current_sound.on('complete', function(){
                  setTimeout(function(){
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play("sound_16b");
                    current_sound.play();
                    current_sound.on('complete', function(){
                      nav_button_controls(0);
                    });
                  });
                });
                break;
            case 17:
            case 18:
            sound_player_nav("sound_"+countNext);
            break;
            case 19:
            setTimeout(function(){
              sound_player_nav("sound_"+countNext);
            },1500);
            break;
            default:
                setTimeout(()=>navigationcontroller(countNext, $total_page),5500);
                break;
        }
    }
    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}

  	function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}

  	function sound_player_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls();
  		});
  	}


    function templateCaller() {checkans
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optdiv = $(".contentblock");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1 show6","option opt2 show6"];
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function checkans(ans,imgcls) {
        $(".option").click(function () {
            if ($(this).find("p").text() == ans) {
              createjs.Sound.stop();
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).append("<img class='"+imgcls+"' src='images/right.png'/>");
                $(".blank").html(ans);
                navigationcontroller(countNext,$total_page);
                $(".ar4 img").addClass("animatearrow");
            }
            else {
              createjs.Sound.stop();
                $(this).addClass("wrongans avoid-clicks");
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
            }
        })
    }
});

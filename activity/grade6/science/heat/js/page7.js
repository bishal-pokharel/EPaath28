var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p7text1
            },
            {
                textdiv:"solid",
                textclass: "content2 centertext",
                textdata: data.string.solid
            },
            {
                textdiv:"liquid",
                textclass: "content2 centertext",
                textdata: data.string.liquid
            },
            {
                textdiv:"gas",
                textclass: "content2 centertext",
                textdata: data.string.gas
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "im1",
                    imgclass: "relativecls img2",
                    imgid: 'wireImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "im2",
                    imgclass: "relativecls img3",
                    imgid: 'milkImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "im3",
                    imgclass: "relativecls img4",
                    imgid: 'gasImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle",
                    imgclass: "relativecls img5",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p7text2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "solidreal",
                    imgclass: "relativecls img2",
                    imgid: 'solidrealImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "solidexp hide1",
                    imgclass: "relativecls img3",
                    imgid: 'solidexpImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle1 hide1",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle2 ",
                    imgclass: "relativecls img5",
                    imgid: 'candle1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p7text3
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "trans1 ",
                    imgclass: "relativecls img2",
                    imgid: 'solidrealImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "trans2 hide2 ",
                    imgclass: "relativecls img3",
                    imgid: 'liquidImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "c1 ",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1 hide1",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                }
            ]
        }],
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p7text4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "trans2 ",
                    imgclass: "relativecls img3",
                    imgid: 'liquidImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "trans3 hide3 ",
                    imgclass: "relativecls img4",
                    imgid: 'gas1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "trans1 ",
                    imgclass: "relativecls img2",
                    imgid: 'solidrealImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "c1 ",
                    imgclass: "relativecls img5",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "c2 hide1",
                    imgclass: "relativecls img6",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1 ",
                    imgclass: "relativecls img7",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2 hide2",
                    imgclass: "relativecls img8",
                    imgid: 'arrowImg',
                    imgsrc: ""
                }
            ]
        }],
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle1",
                textclass: "content2 centertext",
                textdata: data.string.p7text5
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "liq",
                    imgclass: "relativecls img3",
                    imgid: 'liquidImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "gs",
                    imgclass: "relativecls img4",
                    imgid: 'gas1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sol ",
                    imgclass: "relativecls img2",
                    imgid: 'solidrealImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "c1 ",
                    imgclass: "relativecls img5",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "c3",
                    imgclass: "relativecls img6",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1 ",
                    imgclass: "relativecls img7",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2 ",
                    imgclass: "relativecls img8",
                    imgid: 'arrowImg',
                    imgsrc: ""
                }
            ]
        }],
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p7text6
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "liq",
                    imgclass: "relativecls img3",
                    imgid: 'liquidImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "gs",
                    imgclass: "relativecls img4",
                    imgid: 'gas1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sol ",
                    imgclass: "relativecls img2",
                    imgid: 'solidrealImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1 ",
                    imgclass: "relativecls img7",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2 ",
                    imgclass: "relativecls img8",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar3 hide1 ",
                    imgclass: "relativecls img9",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                }
            ]
        }],
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p7text7
            },
            {
                textdiv:"text1",
                textclass: "content2 centertext",
                textdata: data.string.heating
            },
            {
                textdiv:"text2",
                textclass: "content2 centertext",
                textdata: data.string.cooling
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "liq",
                    imgclass: "relativecls img3",
                    imgid: 'liquidImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "gs",
                    imgclass: "relativecls img4",
                    imgid: 'gas1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sol ",
                    imgclass: "relativecls img2",
                    imgid: 'solidrealImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1 ",
                    imgclass: "relativecls img7",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2 ",
                    imgclass: "relativecls img8",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar3 ",
                    imgclass: "relativecls img9",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar4 hide1",
                    imgclass: "relativecls img10",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                }
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "malaImg", src: imgpath + "mala02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "milkImg", src: imgpath + "milk.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wireImg", src: imgpath + "sagging-wires_loop.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "gasImg", src: imgpath + "gas_loop.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "candleImg", src: imgpath + "candle.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "candle1Img", src: imgpath + "candle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "solidrealImg", src: imgpath + "solid_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "solidexpImg", src: imgpath + "liquid_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "curved_arrow_red.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow1Img", src: imgpath + "curved_arrow_blue.png", type: createjs.AbstractLoader.IMAGE},
            {id: "liquidImg", src: imgpath + "liquid_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "gas1Img", src: imgpath + "gas_new.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s7_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s7_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s7_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s7_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s7_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s7_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s7_p7.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 1:
                sound_player("sound_"+countNext);
                $(".hide1").hide();
                $(".candle2").click(function(){
                    nav_button_controls(200);
                    $(".slidreal").fadeOut(1000);
                    $(".solidexp").fadeIn(1000);
                    $(this).fadeOut(100);
                    $(".candle1").fadeIn(100);
                });
                break;
            case 2:
                $(".hide1").hide();
                $(".hide1").delay(3000).fadeIn(1000);
                $(".hide2").delay(2000).animate({"left":"42%"},1000);
                sound_nav("sound_"+(countNext));
                break;
            case 3:
                $(".trans2").css({"left":"42%"});
                $(".hide2").hide();
                $(".hide1").delay(2000).animate({"left":"50%"},1000);
                $(".hide2").delay(4500).fadeIn(1000);
                $(".hide3").delay(3500).animate({"left":"72%"},1000);
                sound_nav("sound_"+(countNext));
                break;
                case 4:
                setTimeout(function(){
                  sound_nav("sound_"+(countNext));
                },1500);
                break;

            case 5:
                $(".hide1").hide().delay(2000).fadeIn(1000);
                sound_nav("sound_"+(countNext));
                break;
            case 6:
                $(".hide1").hide().delay(2000).fadeIn(1000);
                sound_nav("sound_"+(countNext));
                break;
            default:
              sound_nav("sound_"+(countNext));
                break;
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.lessonEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}

  	function sound_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls();
  		});
  	}
    function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();

  	}

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

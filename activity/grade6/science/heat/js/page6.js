var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // // slide0
    // {
    //     contentnocenteradjust: true,
    //     contentblockadditionalclass: "bg",
    //     imageblock: [{
    //         imagestoshow: [
    //             {
    //                 imgdiv: "lady",
    //                 imgclass: "relativecls img1",
    //                 imgid: 'mala1Img',
    //                 imgsrc: ""
    //             },
    //             {
    //                 imgdiv: "dial fadeInEffect",
    //                 imgclass: "relativecls img2",
    //                 imgid: 'dialImg',
    //                 imgsrc: "",
    //                 textblock:[
    //                     {
    //                         textclass:"content3",
    //                         textdata:data.string.p6text1,
    //                     }
    //                 ]
    //             },
    //         ]
    //     }],
    // },
    // slide 1
    // {
    //     contentnocenteradjust: true,
    //     contentblockadditionalclass: "bg",
    //     imageblock: [{
    //         imagestoshow: [
    //             {
    //                 imgdiv: "lady1",
    //                 imgclass: "relativecls img1",
    //                 imgid: 'mala2Img',
    //                 imgsrc: ""
    //             },
    //             {
    //                 imgdiv: "dial1 fadeInEffect",
    //                 imgclass: "relativecls img2",
    //                 imgid: 'dialImg',
    //                 imgsrc: "",
    //                 textblock:[
    //                     {
    //                         textclass:"content3",
    //                         textdata:data.string.p6text2,
    //                     }
    //                 ]
    //             },
    //         ]
    //     }],
    // },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady",
                    imgclass: "relativecls img1",
                    imgid: 'mala1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content3",
                            textdata:data.string.p6text3,
                        }
                    ]
                },
            ]
        }],
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p6text4
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "milk",
                    imgclass: "relativecls img2",
                    imgid: 'milkImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady",
                    imgclass: "relativecls img1",
                    imgid: 'mala1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content3",
                            textdata:data.string.p6text5,
                        }
                    ]
                },
            ]
        }],
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p6text6
            },
            {
                textdiv:"fig1",
                textclass: "content2 centertext",
                textdata: data.string.p6text8
            },
            {
                textdiv:"fig2",
                textclass: "content2 centertext",
                textdata: data.string.p6text9
            },
            {
                textdiv:"desc",
                textclass: "content2 centertext",
                textdata: data.string.p6text7
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "thermo1",
                    imgclass: "relativecls img2",
                    imgid: 'thermo1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "thermo2",
                    imgclass: "relativecls img3",
                    imgid: 'thermo2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle1",
                textclass: "content2 centertext",
                textdata: data.string.p6text10
            },
            {
                textdiv:"fig1",
                textclass: "content2 centertext",
                textdata: data.string.p6text8
            },
            {
                textdiv:"fig2",
                textclass: "content2 centertext",
                textdata: data.string.p6text9
            },
            {
                textdiv:"desc",
                textclass: "content2 centertext",
                textdata: data.string.p6text7
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "thermo1",
                    imgclass: "relativecls img2",
                    imgid: 'thermo1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "thermo2",
                    imgclass: "relativecls img3",
                    imgid: 'thermo2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p6text11
            },
            {
                textdiv:"fig3",
                textclass: "content2 centertext",
                textdata: data.string.p6text12
            },
            {
                textdiv:"fig2",
                textclass: "content2 centertext",
                textdata: data.string.p6text9
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "milk1",
                    imgclass: "relativecls img2",
                    imgid: 'milkImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "thermo2",
                    imgclass: "relativecls img3",
                    imgid: 'thermo2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady1",
                    imgclass: "relativecls img1",
                    imgid: 'mala2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content3",
                            textdata:data.string.p6text13,
                        }
                    ]
                },
            ]
        }],
    },
//    slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady",
                    imgclass: "relativecls img1",
                    imgid: 'mala1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content3",
                            textdata:data.string.p6text14,
                        }
                    ]
                },
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "malaImg", src: imgpath + "mala02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mala1Img", src: imgpath + "mala03a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mala2Img", src: imgpath + "mala02a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "bubleee-01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "milkImg", src: imgpath + "milk.png", type: createjs.AbstractLoader.IMAGE},
            {id: "thermo1Img", src: imgpath + "thermo01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "thermo2Img", src: imgpath + "thermo02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "candleImg", src: imgpath + "candle.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "black_arrow03.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s6_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s6_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s6_p3.ogg"},
            {id: "sound_3a", src: soundAsset + "s6_p4_1.ogg"},
            {id: "sound_3b", src: soundAsset + "s6_p4_2.ogg"},
            {id: "sound_4", src: soundAsset + "s6_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s6_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s6_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s6_p8.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
          case 3:
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("sound_3a");
          current_sound.play();
          current_sound.on('complete', function(){
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("sound_3b");
          current_sound.play();
          current_sound.on('complete', function(){
            nav_button_controls(0);
          });
        });
        break;
        case 4:
        setTimeout(function(){
          sound_nav("sound_"+(countNext));
        },1500);
        break;
            default:
                sound_nav("sound_"+(countNext));
                break;
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}

  	function sound_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls();
  		});
  	}

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

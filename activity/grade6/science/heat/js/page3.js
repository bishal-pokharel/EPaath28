var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "chapter centertext",
                textdata: data.string.p3text1
            }

        ],
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady",
                    imgclass: "relativecls img1",
                    imgid: 'mala1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2",
                            textdata:data.string.p3text2,
                        }
                    ]
                },
            ]
        }],
    },
    // slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady1",
                    imgclass: "relativecls img1",
                    imgid: 'mala2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2",
                            textdata:data.string.p3text3,
                        }
                    ]
                },
            ]
        }],
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady",
                    imgclass: "relativecls img1",
                    imgid: 'mala1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2",
                            textdata:data.string.p3text4,
                        }
                    ]
                },
            ]
        }],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p3text4
            },
            {
                textdiv:"figure1 fadeInEffect",
                textclass: "content3 centertext",
                textdata: data.string.p3text5
            },
            {
                textdiv:"figure2 show2",
                textclass: "content3 centertext",
                textdata: data.string.p3text6
            },
            {
                textdiv:"text1 show2",
                textclass: "content2 centertext",
                textdata: data.string.p3text7
            },
            {
                textdiv:"emptydiv show2"
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "molecules1 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'm2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "molecules2 show2",
                    imgclass: "relativecls img3",
                    imgid: 'm1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1 show2",
                    imgclass: "relativecls img4",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p3text8
            },
            {
                textdiv:"figure1 show1",
                textclass: "content3 centertext",
                textdata: data.string.p3text9
            },
            {
                textdiv:"figure2 show2",
                textclass: "content3 centertext",
                textdata: data.string.p3text10
            },
            {
                textdiv:"text1",
                textclass: "content2 centertext",
                textdata: data.string.p3text7
            },
            {
                textdiv:"emptydiv1"
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "molecules1",
                    imgclass: "relativecls img2",
                    imgid: 'm2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "molecules2",
                    imgclass: "relativecls img3",
                    imgid: 'm1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img4",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "brac1 show1",
                    imgclass: "relativecls img5",
                    imgid: 'brac1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "brac2 show2",
                    imgclass: "relativecls img6",
                    imgid: 'brac2Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p3text11
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "molecules show1",
                    imgclass: "relativecls img2",
                    imgid: 'm2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "molecules show2",
                    imgclass: "relativecls img3",
                    imgid: 'm1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p3text12
            },
            {
                textdiv:"elementtext solidtext show1",
                textclass: "content2 centertext",
                textdata: data.string.solid
            },
            {
                textdiv:"elementtext liquidtext show2",
                textclass: "content2 centertext",
                textdata: data.string.liquid
            },
            {
                textdiv:"elementtext gastext show3",
                textclass: "content2 centertext",
                textdata: data.string.gas
            },
            {
                textdiv:"conclusion show4",
                textclass: "content2 centertext",
                textdata: data.string.p3text13
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle c1 show1",
                    imgclass: "relativecls img2",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle c2 show2",
                    imgclass: "relativecls img3",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "candle c3 show3",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "solid show1",
                    imgclass: "relativecls img5",
                    imgid: 'solidImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "liquid show2",
                    imgclass: "relativecls img6",
                    imgid: 'liquidImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "gas show3",
                    imgclass: "relativecls img7",
                    imgid: 'gasImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content3 centertext",
                textdata: data.string.p3text14
            },
            {
                textdiv:"text1 show2",
                textclass: "content2 centertext",
                textdata: data.string.p3text7
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "solid1 show1",
                    imgclass: "relativecls img2",
                    imgid: 'solid1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1 show2",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "solid2 show3",
                    imgclass: "relativecls img4",
                    imgid: 'solidImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady",
                    imgclass: "relativecls img1",
                    imgid: 'mala1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial2 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2",
                            textdata:data.string.p3text15,
                        }
                    ]
                },
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "malaImg", src: imgpath + "mala02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mala1Img", src: imgpath + "mala03a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mala2Img", src: imgpath + "mala02a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "bubleee-01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "m1Img", src: imgpath + "blue_molecules01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "m2Img", src: imgpath + "blue_molecules02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "red_arrow02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "brac1Img", src: imgpath + "bracket01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "brac2Img", src: imgpath + "bracket02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "candleImg", src: imgpath + "candle.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "solidImg", src: imgpath + "solid.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "liquidImg", src: imgpath + "liquid.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "gasImg", src: imgpath + "gas.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "solid1Img", src: imgpath + "solid1.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s3_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s3_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s3_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s3_p4.ogg"},
            {id: "sound_4a", src: soundAsset + "s3_p5_1.ogg"},
            {id: "sound_4b", src: soundAsset + "s3_p5_2.ogg"},
            {id: "sound_5a", src: soundAsset + "s3_p6_1.ogg"},
            {id: "sound_5b", src: soundAsset + "s3_p6_2.ogg"},
            {id: "sound_5c", src: soundAsset + "s3_p6_3.ogg"},
            {id: "sound_6", src: soundAsset + "s3_p7.ogg"},
            {id: "sound_7a", src: soundAsset + "s3_p8_1.ogg"},
            {id: "sound_7b", src: soundAsset + "s3_p8_2.ogg"},
            {id: "sound_7c", src: soundAsset + "s3_p8_3.ogg"},
            {id: "sound_7d", src: soundAsset + "s3_p8_4.ogg"},
            {id: "sound_7e", src: soundAsset + "s3_p8_5.ogg"},
            {id: "sound_8", src: soundAsset + "s3_p9.ogg"},
            {id: "sound_9", src: soundAsset + "s3_p10.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
          case 0:
          sound_player_nav("sound_0");
          break;
            case 4:
                $(".show1,.show2").hide();
                $(".show1").fadeIn(500);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_4a");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show2").fadeIn(500);
                current_sound = createjs.Sound.play("sound_4b");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(0);
                });
              });
                break;
            case 5:
               $(".molecules1,.molecules2").css("top","52%");
               $(".figure1,.figure2").css("bottom","63%");
               $(".ar1").css("top","67%");
               $(".text1").css("top","54%");
                $(".show1,.show2").hide();

                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_5a");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show1").fadeIn(500);
                current_sound = createjs.Sound.play("sound_5b");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show2").fadeIn(500);
                current_sound = createjs.Sound.play("sound_5c");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(0);
                });
              });
            });
                break;
            case 6:
                $(".show2").hide();
                $(".show1").delay(1000).animate({"width":"30%"},1000).fadeOut(1000);
                $(".show2").delay(1500).fadeIn(1500).css("width","30%");
                sound_player_nav("sound_"+(countNext));
                break;
            case 7:
                $(".show1,.show2,.show3,.show4").hide();

                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_7a");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show1").fadeIn(500);
                current_sound = createjs.Sound.play("sound_7b");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show2").fadeIn(500);
                current_sound = createjs.Sound.play("sound_7c");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show3").fadeIn(500);
                current_sound = createjs.Sound.play("sound_7d");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show4").fadeIn(500);
                current_sound = createjs.Sound.play("sound_7e");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(0);
                });
              });
            });
          });
        });
                break;
            case 8:
                $(".show1,.show2,.show3").hide();
                $(".show1").delay(1000).fadeIn(500);
                $(".show2").delay(2500).fadeIn(500);
                $(".show3").delay(3500).fadeIn(500);
                sound_player_nav("sound_"+(countNext));
                break;
            default:
                sound_player_nav("sound_"+(countNext));
                break;
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}

  	function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}

  	function sound_player_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls();
  		});
  	}
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

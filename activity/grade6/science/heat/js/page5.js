var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady",
                    imgclass: "relativecls img1",
                    imgid: 'mala1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag : true,
                            datahighlightcustomclass : 'colortext',
                            textclass:"content2",
                            textdata:data.string.p5text1,
                        }
                    ]
                },
            ]
        }],
    },
    // slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lady1",
                    imgclass: "relativecls img1",
                    imgid: 'mala2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2",
                            textdata:data.string.p5text2,
                        }
                    ]
                },
            ]
        }],
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p5text3
            },
            {
                textdiv:"figure show1",
                textclass: "content2 centertext",
                textdata: data.string.p5text4
            },
            {
                textdiv:"caption",
                textclass: "content3 centertext",
                textdata: data.string.p5text5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "jar",
                    imgclass: "relativecls img2",
                    imgid: 'jarImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p5text6
            },
            {
                textdiv:"figure show1",
                textclass: "content2 centertext",
                textdata: data.string.p5text7
            },
            {
                textdiv:"caption",
                textclass: "content3 centertext",
                textdata: data.string.p5text5
            },
            {
                textdiv:"caption1",
                textclass: "content3 centertext",
                textdata: data.string.p5text8
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "jar",
                    imgclass: "relativecls img2",
                    imgid: 'jar1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img4",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p5text9
            },
            {
                textdiv:"figureleft slideL",
                textclass: "content3 centertext",
                textdata: data.string.p5text7
            },
            {
                textdiv:"figureright slideR",
                textclass: "content3 centertext",
                textdata: data.string.p5text10
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "jarleft slideL",
                    imgclass: "relativecls img2",
                    imgid: 'jar1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "jarright slideR",
                    imgdiv: "jarright slideR",
                    imgclass: "relativecls img3",
                    imgid: 'jar2Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p5text11
            },
            {
                textdiv:"figure",
                textclass: "content2 centertext",
                textdata: data.string.p5text12
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "lastjar",
                    imgclass: "relativecls img2",
                    imgid: 'jarImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "lastjar1",
                    imgclass: "relativecls img3",
                    imgid: 'jar1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "lastjar2",
                    imgclass: "relativecls img4",
                    imgid: 'jar2Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p5text13
            },
            {
                textdiv:"figureleft",
                textclass: "content3 centertext",
                textdata: data.string.p5text14
            },
            {
                textdiv:"figureright",
                textclass: "content3 centertext",
                textdata: data.string.p5text15
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "jarleft",
                    imgclass: "relativecls img2",
                    imgid: 'coldwireImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "jarright",
                    imgclass: "relativecls img3",
                    imgid: 'saggingwireImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "playbtn",
                    imgclass: "relativecls img4",
                    imgid: 'playbtnImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.p5text16
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "talkingimg",
                    imgclass: "relativecls img1",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "im1",
                    imgclass: "relativecls img2",
                    imgid: 'jar1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "im2",
                    imgclass: "relativecls img3",
                    imgid: 'jar2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "im3",
                    imgclass: "relativecls img4",
                    imgid: 'coldwireImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "im4",
                    imgclass: "relativecls img5",
                    imgid: 'saggingwire1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "malaImg", src: imgpath + "mala02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mala1Img", src: imgpath + "mala03a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mala2Img", src: imgpath + "mala02a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "bubleee-01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "jarImg", src: imgpath + "bottle01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "jar1Img", src: imgpath + "bottle02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "jar2Img", src: imgpath + "bottle03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "black_arrow03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "saggingwireImg", src: imgpath + "sagging_wires.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "saggingwire1Img", src: imgpath + "sagging_wires.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coldwireImg", src: imgpath + "cold_tight_electric-wire.png", type: createjs.AbstractLoader.IMAGE},
            {id: "playbtnImg", src: imgpath + "play_btn.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_0", src: soundAsset + "s5_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s5_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s5_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s5_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s5_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s5_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s5_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s5_p8.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 2:
                $(".show1").hide().delay(1000).fadeIn(1000);
                sound_nav("sound_"+(countNext));
                break;
            case 3:
                $(".ar1").css("top","52%");
                $(".caption").css("top","46%");
                sound_nav("sound_"+(countNext));
                break;
            case 6:
                clearInterval(setTime);
                $(".jarleft, .figureleft").css("left","-45%");
                $(".jarright, .figureright").css("right","-45%");
                $(".jarleft, .figureleft").animate({
                  "left":"5%"
                },2000, function(){
                  $(".jarright, .figureright").animate({
                    "right":"5%"
                  },2000, function(){
                    showplaybtn();
                  });
                });
                $(".playbtn").click(function(){
                    clearInterval(setTime);
                    $(this).css("opacity","0");
                    $(".jarright").css("opacity","1");
                    showplaybtn();
                    $(".jarright").find("img").attr("src",preload.getResult("saggingwireImg").src);
                });
                sound_nav("sound_"+(countNext));
                break;
            case 7:
                $(".im1,.im2,.im3,.im4").hide();
                $(".im1").delay(1000).fadeIn(100);
                $(".im2").delay(1500).fadeIn(100);
                $(".im3").delay(2000).fadeIn(100);
                $(".im4").delay(2500).fadeIn(100);
                sound_nav("sound_"+(countNext));
                break;
            default:
            sound_nav("sound_"+(countNext));
                break;
        }
    }

  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function showplaybtn(){
        setTime = setInterval(function(){
            $(".playbtn").css("opacity","1.1");
            $(".jarright").css("opacity","0.7");
        },1800);
    }
});

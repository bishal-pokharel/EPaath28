var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/newcorrectedimages/";
var imgpath2 = $ref + "/images/ballexp/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "chapter centertext",
                textdata: data.string.p4text1
            }

        ],
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.query
            },
            {
                textdiv:"question show6",
                textclass: "content2 centertext",
                textdata: data.string.p4q1
            },
            {
                textdiv:"option opt1 show6",
                textclass: "content2 centertext",
                textdata: data.string.p4q1a1,
                ans:"correct"
            },
            {
                textdiv:"option opt2 show6",
                textclass: "content2 centertext",
                textdata: data.string.p4q1a2
            },
            {
                textdiv:"coldmetal naming",
                textclass: "content2 centertext",
                textdata: data.string.coldmetal
            },
            {
                textdiv:"ring naming",
                textclass: "content2 centertext",
                textdata: data.string.ring
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "metalball show1",
                    imgclass: "relativecls img1",
                    imgid: 'metalballImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "redball show3",
                    imgclass: "relativecls img2",
                    imgid: 'redballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "hotmetalball show5",
                    imgclass: "relativecls img3",
                    imgid: 'hotmetalballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "candle show3",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar1 show2",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar2 show4",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "blackar1 naming",
                    imgclass: "relativecls img7",
                    imgid: 'arrow2Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "blackar2 naming",
                    imgclass: "relativecls img8",
                    imgid: 'arrow3Img',
                    imgsrc: "",
                },
            ]
        }],
    },
    // slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.query
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.p4q2
            },
            {
                textdiv:"option opt1",
                textclass: "content2 centertext",
                textdata: data.string.p4q2a1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content2 centertext",
                textdata: data.string.p4q2a2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "metalball",
                    imgclass: "relativecls img1",
                    imgid: 'metalball1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "redball1",
                    imgclass: "relativecls img2",
                    imgid: 'redball1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "hotmetalball",
                    imgclass: "relativecls img3",
                    imgid: 'hotmetalball1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "candle",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: "",
                },
            ]
        }],
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.query
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.p4q3
            },
            {
                textdiv:"option opt1",
                textclass: "content2 centertext",
                textdata: data.string.p4q3a1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content2 centertext",
                textdata: data.string.p4q3a2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "metalball",
                    imgclass: "relativecls img1",
                    imgid: 'metalball1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "redball1",
                    imgclass: "relativecls img2",
                    imgid: 'redball1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "hotmetalball",
                    imgclass: "relativecls img3",
                    imgid: 'hotmetalball1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "candle",
                    imgclass: "relativecls img4",
                    imgid: 'candleImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img5",
                    imgid: 'arrowImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: "",
                },
            ]
        }],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.query
            },
            {
                textdiv:"question",
                textclass: "content3 centertext",
                textdata: data.string.p4q4
            },
            {
                textdiv:"option opt1",
                textclass: "content3 centertext",
                textdata: data.string.p4q4a1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content3 centertext",
                textdata: data.string.p4q4a2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "metalball show3",
                    imgclass: "relativecls img1",
                    imgid: 'metalball2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "coldmetalball show2",
                    imgclass: "relativecls img3",
                    imgid: 'coldmetalballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "redball2 show1",
                    imgclass: "relativecls img2",
                    imgid: 'redball1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar2 show2",
                    imgclass: "relativecls img6",
                    imgid: 'arrow1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar1 show3",
                    imgclass: "relativecls img5",
                    imgid: 'arrow1Img',
                    imgsrc: "",
                }
            ]
        }],
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.query
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.p4q5
            },
            {
                textdiv:"option opt1",
                textclass: "content3 centertext",
                textdata: data.string.p4q5a1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content3 centertext",
                textdata: data.string.p4q5a2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "metalball",
                    imgclass: "relativecls img1",
                    imgid: 'metalball1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "coldmetalball",
                    imgclass: "relativecls img3",
                    imgid: 'coldmetalballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "redball2",
                    imgclass: "relativecls img2",
                    imgid: 'redball1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img6",
                    imgid: 'arrow1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img5",
                    imgid: 'arrow1Img',
                    imgsrc: "",
                }
            ]
        }],
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.query
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.p4q6
            },
            {
                textdiv:"option opt1",
                textclass: "content3 centertext",
                textdata: data.string.p4q6a1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content3 centertext",
                textdata: data.string.p4q6a2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "metalball",
                    imgclass: "relativecls img1",
                    imgid: 'metalball1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "coldmetalball",
                    imgclass: "relativecls img3",
                    imgid: 'coldmetalballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "redball2",
                    imgclass: "relativecls img2",
                    imgid: 'redball1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img6",
                    imgid: 'arrow1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img5",
                    imgid: 'arrow1Img',
                    imgsrc: "",
                }
            ]
        }],
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.query
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.p4q7
            },
            {
                textdiv:"option opt1",
                textclass: "content3 centertext",
                textdata: data.string.p4q7a1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content3 centertext",
                textdata: data.string.p4q7a2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "metalball",
                    imgclass: "relativecls img1",
                    imgid: 'metalball1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "coldmetalball",
                    imgclass: "relativecls img3",
                    imgid: 'coldmetalballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "redball2",
                    imgclass: "relativecls img2",
                    imgid: 'redball1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar2",
                    imgclass: "relativecls img6",
                    imgid: 'arrow1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "ar1",
                    imgclass: "relativecls img5",
                    imgid: 'arrow1Img',
                    imgsrc: "",
                }
            ]
        }],
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "candleImg", src: imgpath + "candle.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "metalballImg", src: imgpath1 + "metal_ball_new.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "redballImg", src: imgpath1 + "metal_ball_02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "hotmetalballImg", src: imgpath1 + "metal_ball_01_new.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "metalball1Img", src: imgpath1 + "metal_ball_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "metalball2Img", src: imgpath1 + "metalball01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "redball1Img", src: imgpath1 + "metal_ball_02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hotmetalball1Img", src: imgpath1 + "metal_ball_01_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coldmetalballImg", src: imgpath1 + "metal_ball_022.png", type: createjs.AbstractLoader.IMAGE},

            {id: "arrowImg", src: imgpath1 + "blue_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow1Img", src: imgpath1 + "blue_arrow01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow2Img", src: imgpath1 + "black_arrow01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow3Img", src: imgpath1 + "black_arrow03.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s4_p1.ogg"},
            {id: "sound_1a", src: soundAsset + "s4_p2_1.ogg"},
            {id: "sound_1b", src: soundAsset + "s4_p2_2.ogg"},
            {id: "sound_2", src: soundAsset + "s4_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s4_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s4_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s4_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s4_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s4_p8.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
            sound_player_nav("sound_0");
            break;
            case 1:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_1a");
            current_sound.play();
            current_sound.on('complete', function(){
              setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_1b");
                current_sound.play();
              },7000);

            });
                $(".show1,.show2,.show3,.show4,.show5,.show6,.naming").hide();
                $(".show1").delay(3000).fadeIn(1000);
                $(".naming").delay(5500).fadeIn(1000);
                $(".show2").delay(6000).fadeIn(1000);
                $(".show3").delay(6000).fadeIn(1000);
                $(".show4").delay(9000).fadeIn(1000);
                $(".show5").delay(10000).fadeIn(1000);
                $(".show6").delay(12000).fadeIn(1000);
                shufflehint();
                checkans("correctwrongimg1");
                break;
            case 2:
            case 3:
            sound_player("sound_"+countNext);
                shufflehint();
                checkans("correctwrongimg1");
                break;
            case 4:
            sound_player("sound_"+countNext);
                $(".show1,.show2,.show3").hide();
                $(".show1").delay(1000).fadeIn(1000);
                $(".show2").delay(2000).fadeIn(1000);
                $(".show3").delay(3000).fadeIn(1000);
                shufflehint();
                checkans("correctwrongimg1");
                break;
            case 5:
            case 6:
            case 7:
            sound_player("sound_"+countNext);
                shufflehint();
                checkans("correctwrongimg1");
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}

  	function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}

  	function sound_player_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls();
  		});
  	}


    function templateCaller() {checkans
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optdiv = $(".contentblock");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1 show6","option opt2 show6"];
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function checkans(correctwrongimg) {
        $(".option").click(function () {
            if ($(this).attr("data-answer") == "correct") {
              createjs.Sound.stop();
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).append("<img class='"+correctwrongimg+"' src='images/right.png'/>");
                navigationcontroller(countNext,$total_page);
                countNext==4?$(".metalball").find("img").attr("src",preload.getResult("metalballImg").src):"";
            }
            else {
              createjs.Sound.stop();
                $(this).addClass("wrongans avoid-clicks");
                play_correct_incorrect_sound(0);
                $(this).append("<img class='"+correctwrongimg+"' src='images/wrong.png'/>");
            }
        })
    }
});

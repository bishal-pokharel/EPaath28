var imgpath = $ref+"/images/Exercise/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.query
            },
            {
                question:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.s1
            },
            {
                textdiv:"twoopt option opt1",
                textclass: "content3 centertext",
                textdata: data.string.s7qa1,
                ans:"correct"
            },
            {
                textdiv:"twoopt option opt2",
                textclass: "content3 centertext",
                textdata: data.string.s7qa2
            },
            {
                textdiv: "threeopt option1 opt11",
                textclass: "content3 centertext",
                textdata: data.string.s7qa1,
                ans: "correct"
            },
            {
                textdiv: "threeopt option1 opt22",
                textclass: "content3 centertext",
                textdata: data.string.s7qa2
            },
            {
                textdiv: "threeopt option1 opt33",
                textclass: "content3 centertext",
                textdata: data.string.s7qa3
            },
            {
                textdiv:"figure hide2",
                textclass: "content3 centertext",
                textdata: data.string.s1fig
            },
            {
                textdiv:"fig1 hide1",
                textclass: "content3 centertext",
                textdata: data.string.s1fig1
            },
            {
                textdiv:"fig2 hide1",
                textclass: "content3 centertext",
                textdata: data.string.s1fig2
            },
            {
                textdiv:"fig3 hide1",
                textclass: "content3 centertext",
                textdata: data.string.s1fig3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "mainimage1",
                    imgclass: "relativecls img1",
                    imgid: 'gasImg',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide last
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = 10;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var scoreupdate = 1;
    var score = new NumberTemplate();
    score.init($total_page);

    var solidarray = [];
    var liquidarray = [];
    var gasarray = [];
    var mainArray = [];



    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "sImg", src: imgpath + "chocolate.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lImg", src: imgpath + "bottle_hot_water.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "l1Img", src: imgpath + "bottle_cold_water.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "gImg", src: imgpath + "balloon02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "g1Img", src: imgpath + "balloon03.gif", type: createjs.AbstractLoader.IMAGE},

            // sounds
            // {id: "sound_0", src: soundAsset + "p1_s0.ogg"},
            // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},
            // {id: "sound_2", src: soundAsset + "p1_s2.ogg"},
            // {id: "sound_3", src: soundAsset + "p1_s3.ogg"},
            // {id: "sound_4", src: soundAsset + "p1_s4.ogg"},
            // {id: "sound_5", src: soundAsset + "p1_s5.ogg"},
            // {id: "sound_6", src: soundAsset + "p1_s6.ogg"},
            // {id: "sound_7", src: soundAsset + "p1_s7.ogg"},
            // {id: "sound_8", src: soundAsset + "p1_s8.ogg"},
            // {id: "20sound_9", src: soundAsset + "p1_s9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    // Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    // Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    // Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        countNext==0?randomizethequestion():'';
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = countNext==10?template(content[1]):template(content[0]);
        $board.html(html);
        score.numberOfQuestions();

        //dynamically arrange question
        if(countNext !=10) {
            var loadtemplate = mainArray.pop();
            var imgsrc = loadtemplate.slice(0, 1);
            if (loadtemplate.toString().startsWith("s")) {
                $(".mainimage1").addClass("mainimage").removeClass("mainimage1");
                $(".threeopt").hide();
                //figure naming hide and show
                $(".hide2").hide();
                $(".hide1").show();
            }
            else {
                $(".hide1").hide();
                $(".hide2").show()
                if(loadtemplate.toString().startsWith("g")){
                    $(".mainImage1").css({"top":"17%"});
                }
                var imgArray = ["l3", "l4", "l7", "g2","g3", "g5", "g7"];
                if (imgArray.indexOf(loadtemplate) >= 0) {
                    imgsrc = imgsrc + "1";
                }
                var optArray = ["l5", "l6", "l7", "g4", "g5", "g6", "g7"];
                if (optArray.indexOf(loadtemplate) >= 0) {
                    $(".twoopt").hide();
                    $(".threeopt").show();
                }
                else {
                    $(".twoopt").show();
                    $(".threeopt").hide();
                }
            }
            dynamicallyloadcontent(loadtemplate, imgsrc);

            shufflehint();
            shufflehint1();
            checkans(eval("data.string." + loadtemplate + "qa1"));

            switch (countNext) {
                case 10:
                    $(".coverboardfull").empty();
                    navigationcontroller();
                    break;
            }
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
  			countNext++;
  			score.gotoNext();
  			if(countNext < 10){
  				templateCaller();
  			} else {
  				$nextBtn.hide(0);
  			}
    });
    //
    // $refreshBtn.click(function(){
    //     templateCaller();
    // });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function shufflehint() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1","option opt2"]
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function shufflehint1() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option1").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option1").eq(Math.random() * i | 0));
        }
        var optionclass = ["option1 opt11","option1 opt22","option1 opt33"]
        optdiv.find(".option1").removeClass().addClass("curr");
        optdiv.find(".curr").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }


        function navigationcontroller(islastpageflag) {
            if (countNext == 0 && $total_page != 1) {
                $nextBtn.show(0);
            }
            else if ($total_page == 1) {
                $nextBtn.css('display', 'none');

                ole.footerNotificationHandler.lessonEndSetNotification();
            }
            else if (countNext > 0 && countNext < $total_page) {

                $nextBtn.show(0);
            }
            else if (countNext == $total_page - 1) {

                $nextBtn.css('display', 'none');
                // if lastpageflag is true
                // ole.footerNotificationHandler.pageEndSetNotification();
            }

        }

    function put_image() {
        var contentCount=content[countNext];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload);
        imageblockcontent=contentCount.hasOwnProperty('imageblock1');
        contentCount = imageblockcontent?contentCount.imageblock1[0]:false;
        imageblockcontent?dynamicimageload(imageblockcontent,contentCount):'';
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

  function checkans(ans){
      scoreupdate = 1;
        $(".option,.option1").click(function(){
            if($(this).find("p").text()==ans){
                console.log("answer check 1111 if"+scoreupdate);
                $(this).addClass("correctans");
                $(".option,.option1").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                navigationcontroller(countNext, $total_page);
                scoreupdate==1?score.update(true):'';
                navigationcontroller();
            }
            else{
                scoreupdate = 0;
                $(this).addClass("wrongans avoid-clicks");
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                score.update(false);

            }
      });
  }
    function randomizethequestion(){
        for(var i=0;i<3;i++){
            solidarray.push(generaterandomnum(["s1","s2","s3","s4","s5","s6","s7"],i==0?["s0"]:solidarray));
            liquidarray.push(generaterandomnum(["l1","l2","l3","l4","l5","l6","l7"],i==0?["l0"]:liquidarray));
            gasarray.push(generaterandomnum(["g1","g2","g3","g4","g5","g6","g7"],i==0?["g0"]:gasarray));
            if(i==2){
                gasarray.push(generaterandomnum(["g1","g2","g3","g4","g5","g6","g7"],i==0?["g0"]:gasarray));
                $.merge(mainArray,solidarray)
                $.merge(mainArray,liquidarray)
                $.merge(mainArray,gasarray)
                mainArray.shufflearray();
            }
        }

    }

    function generaterandomnum(array,excludenum){
        for(var i = 0; i<excludenum.length;i++) {
            array.splice(array.indexOf(excludenum[i]), 1);
        }
        var random = array[Math.floor(Math.random() * array.length)];
        return random;
    }
    function dynamicallyloadcontent(loadtemplate,imagesrc){
        $(".mainimage img").attr("src",preload.getResult(imagesrc+"Img").src);
        $(".mainimage1 img").attr("src",preload.getResult(imagesrc+"Img").src);
        $(".question p").find("span").eq(1).html(eval("data.string."+loadtemplate))
        $(".opt1 p").html(eval("data.string."+loadtemplate+"qa1"))
        $(".opt2 p").html(eval("data.string."+loadtemplate+"qa2"))
        $(".figure").html(eval("data.string."+loadtemplate+"fig"))

        $(".opt11 p").html(eval("data.string."+loadtemplate+"qa1"))
        $(".opt22 p").html(eval("data.string."+loadtemplate+"qa2"))
        $(".opt33 p").html(eval("data.string."+loadtemplate+"qa3"))

    }

});

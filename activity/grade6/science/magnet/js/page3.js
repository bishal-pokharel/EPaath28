var imgpath = $ref+"/images/";

var soundAsset = $ref+"/sounds/";
var soundAsset2 = $ref+"/sounds/"+$lang+"/";

var sound_1a = new buzz.sound(soundAsset2+"s3_p1_1.ogg");
var sound_2 = new buzz.sound(soundAsset2+"s3_p1_2.ogg");
var sound_3 = new buzz.sound(soundAsset2+"s3_p1_3.ogg");
var sound_4 = new buzz.sound(soundAsset2+"s3_p1_4.ogg");
var sound_5 = new buzz.sound(soundAsset2+"s3_p2.ogg");
var sound_6 = new buzz.sound(soundAsset2+"s3_p3.ogg");
var sound_7 = new buzz.sound(soundAsset2+"s3_p4.ogg");
var sound_8 = new buzz.sound(soundAsset2+"s3_p10.ogg");

var sound_en_1 = new buzz.sound((soundAsset + "magnets_1.ogg"));
var sound_en_2 = new buzz.sound((soundAsset + "magnets_2.ogg"));
var sound_en_3 = new buzz.sound((soundAsset + "magnets_3.ogg"));
var sound_en_4 = new buzz.sound((soundAsset + "magnets_4.ogg"));
var sound_en_5 = new buzz.sound((soundAsset + "magnets_5.ogg"));

var sound_group_en = [sound_en_1, sound_en_2, sound_en_3, sound_en_4, sound_en_5];

var sound_np_1 = new buzz.sound((soundAsset + "magnetsnp_1.ogg"));
var sound_np_2 = new buzz.sound((soundAsset + "magnetsnp_2.ogg"));
var sound_np_3 = new buzz.sound((soundAsset + "magnetsnp_3.ogg"));
var sound_np_4 = new buzz.sound((soundAsset + "magnetsnp_4.ogg"));
var sound_np_5 = new buzz.sound((soundAsset + "magnetsnp_5.ogg"));

var sound_group_np = [sound_np_1, sound_np_2, sound_np_3, sound_np_4, sound_np_5];

var sound_1 = new buzz.sound(soundAsset+"s2_p2_1.ogg");


var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.p1_s1
		}],
		imageblockadditionalclass: "imgblk_100",
		imageblock:[{
			imageblockclass: "magnetcontainer1 fadein1",
			imagestoshow:[
				{
					imgclass: "magnetimage ",
					imgsrc: imgpath+"magnet02.png"
				}
			],
      imagelabels:[{
        imagelabelclass: "magnetlabel fadein2",
        imagelabeldata: data.string.p3_s2
      }]
		},{
      imageblockclass: "magnetcontainer2 fadein1",
      imagestoshow:[
        {
          imgclass: "magnetimage fadein2",
          imgsrc: imgpath+"magnet01.png"
        }
      ],
      imagelabels:[{
        imagelabelclass: "magnetlabel fadein3",
        imagelabeldata: data.string.p3_s3
      }]
		},{
      imageblockclass: "magnetcontainer3 fadein1",
      imagestoshow:[
        {
          imgclass: "magnetimage fadein3",
          imgsrc: imgpath+"magnet03.png"
        }
      ],
      imagelabels:[{
        imagelabelclass: "magnetlabel fadein4",
        imagelabeldata: data.string.p3_s4
      }]
		}],
    lowertextblockadditionalclass: "ltb_100_top_70",
    lowertextblocknocenteradjust: true,
    lowertextblock:[
      {
        textclass: "description2",
        textdata: data.string.p3_s1
      }
    ]
	},{
		// slide1
		contentnocenteradjust: true,
		imageblockadditionalclass: "imgblk_100",
		imageblock:[{
      imageblockclass: "magnetcontainer fadein1",
      imagestoshow:[
        {
          imgclass: "magnetimage fadein2",
          imgsrc: imgpath+"natural-magnet.png"
        }
      ]
		}],
		lowertextblockadditionalclass: "ltb_100_top_70",
    lowertextblocknocenteradjust: true,
		lowertextblock:[
			{
				textclass: "description2",
				textdata: data.string.p3_s5
			}
		]
	},{
		// slide2
    contentnocenteradjust: true,
		contentblockadditionalclass: "content_100_map",
		lowertextblockadditionalclass: "ltb_100_top_70",
    lowertextblocknocenteradjust: true,
		lowertextblock:[
			{
				textclass: "description3",
        datahighlightflag: true,
        datahighlightcustomclass: "balckhighlight",
				textdata: data.string.p3_s6
			}
		]
	},{
    // slide3
		contentblockadditionalclass: "content_100_yellow",
		uppertextblock:[
			{
				textclass: "description2",
				textdata: data.string.p3_s7
			},	{
  				textclass: "description2",
  				textdata: data.string.p3_s8
  			}
		],

  },{
    // slide4
    contentnocenteradjust: true,
    contentblockadditionalclass: "content_100_yellow",
		imageblockadditionalclass: "imgblk_100",
		imageblock:[{
			imagestoshow:[{
        imgclass: "legendbg",
        imgsrc: imgpath+"shepherdboy01.png"
      }]
    }],
    lowertextblockadditionalclass: "ltb_100_top_70",
    lowertextblocknocenteradjust: true,
		lowertextblock:[
			{
				textclass: "description2",
				textdata: data.string.p3_s9
			}
		]
  },{
    // slide5
    contentnocenteradjust: true,
    contentblockadditionalclass: "content_100_yellow",
		imageblockadditionalclass: "imgblk_100",
		imageblock:[{
			imagestoshow:[{
        imgclass: "legendbg",
        imgsrc: imgpath+"shepherdboy02.png"
      }]
    }],
    lowertextblockadditionalclass: "ltb_100_top_70",
    lowertextblocknocenteradjust: true,
		lowertextblock:[
			{
				textclass: "description2",
				textdata: data.string.p3_s10
			}
		]
  },{
    // slide6
    contentnocenteradjust: true,
    contentblockadditionalclass: "content_100_yellow",
		imageblockadditionalclass: "imgblk_100",
		imageblock:[{
			imagestoshow:[{
        imgclass: "legendbg",
        imgsrc: imgpath+"shepherdboy03.png"
      }]
    }],
    lowertextblockadditionalclass: "ltb_100_top_70",
    lowertextblocknocenteradjust: true,
		lowertextblock:[
			{
				textclass: "description2",
				textdata: data.string.p3_s11
			}
		]
  },{
    // slide7
    contentnocenteradjust: true,
    contentblockadditionalclass: "content_100_yellow",
		imageblockadditionalclass: "imgblk_100",
		imageblock:[{
			imagestoshow:[{
        imgclass: "legendbg",
        imgsrc: imgpath+"shepherdboy04.png"
      }]
    }],
    lowertextblockadditionalclass: "ltb_100_top_70",
    lowertextblocknocenteradjust: true,
		lowertextblock:[
			{
				textclass: "description2",
				textdata: data.string.p3_s12
			}
		]
  },{
    // slide8
    contentnocenteradjust: true,
    contentblockadditionalclass: "content_100_yellow",
		imageblockadditionalclass: "imgblk_100",
		imageblock:[{
			imagestoshow:[{
        imgclass: "legendbg",
        imgsrc: imgpath+"shepherdboy04.png"
      }]
    }],
    lowertextblockadditionalclass: "ltb_100_top_70",
    lowertextblocknocenteradjust: true,
		lowertextblock:[
			{
				textclass: "description2",
				textdata: data.string.p3_s13
			}
		]
  },{
    // slide9
    contentblockadditionalclass: "content_100_yellow",
    uppertextblock:[
      {
        textclass: "description2",
        textdata: data.string.p3_s14
      }
    ]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  console.log($lang);

    if ($lang == 'np'){
      var sound_group_p1 = sound_group_np;
      console.log("This is the Nepali audio")
    } else if ($lang == 'en') {
      var sound_group_p1 = sound_group_en;
      console.log("This is the English audio")
    }





/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var sound_playing;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
		switch (countNext) {
      case 0:
					soundplayerfour(sound_2,sound_3,sound_4,sound_1a);
          $(".magnetcontainer1").delay(500).show(0);
          $(".magnetcontainer2").delay(1000).show(0);
          $(".magnetcontainer3").delay(1500).show(0);
          break;
      case 1:
				soundplayer(sound_5);
          $(".magnetcontainer2").show(0);
          break;
			case 2:
				soundplayer(sound_6);

				break;
			case 3:
				soundplayer(sound_7);

				break;
      case 4:
            sound_playing = sound_group_p1[0];
            $nextBtn.hide(0);
            sound_playing.bind("ended", function(){
              $nextBtn.show(0);
            });
            sound_playing.play();
          	break;
      case 5:
            sound_playing = sound_group_p1[1];
            $nextBtn.hide(0);
            sound_playing.bind("ended", function(){
              $nextBtn.show(0);
            });
            sound_playing.play();
          	break;
      case 6:
            sound_playing = sound_group_p1[2];
            $nextBtn.hide(0);
            sound_playing.bind("ended", function(){
              $nextBtn.show(0);
            });
            sound_playing.play();
          	break;
      case 7:
            sound_playing = sound_group_p1[3];
            $nextBtn.hide(0);
            sound_playing.bind("ended", function(){
              $nextBtn.show(0);
            });
            sound_playing.play();
          	break;
      case 8:
            sound_playing = sound_group_p1[4];
            $nextBtn.hide(0);
            sound_playing.bind("ended", function(){
              $nextBtn.show(0);
            });
            sound_playing.play();
          	break;
      case 9:
			soundplayer(sound_8);

            break;
			default:

		}

  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function soundplayer(i, next){
 		buzz.all().stop();
 		i.play().bind("ended",()=>{
 			if(!next) navigationcontroller();
 		});
 	}

 	function soundplayertwo(i, j, next){
 		buzz.all().stop();
 		i.play().bind("ended",()=>{
 			j.play().bind("ended",()=>{
 				if(!next) navigationcontroller();
 			});
 		});
 	}

 	function soundplayerthree(i, j, k, next){
 		buzz.all().stop();
 		i.play().bind("ended",()=>{
 			j.play().bind("ended",()=>{
 				k.play().bind("ended",()=>{
 				 if(!next) navigationcontroller();
 			 });
 			});
 		});
 	}

	function soundplayerfour(i, j, k, l, next){
		buzz.all().stop();
		i.play().bind("ended",()=>{
			j.play().bind("ended",()=>{
				k.play().bind("ended",()=>{
					l.play().bind("ended",()=>{
				 		if(!next) navigationcontroller();
					});
			 });
			});
		});
	}

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
    if(sound_playing != null){
      sound_playing.stop();
      sound_playing.unbind('ended');
    }
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
    if(sound_playing != null){
      sound_playing.stop();
      sound_playing.unbind('ended');
    }
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

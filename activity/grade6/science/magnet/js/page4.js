var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound(soundAsset+"s4_p1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s4_p2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s4_p3.ogg");
var sound_4 = new buzz.sound(soundAsset+"s4_p5.ogg");
var sound_5 = new buzz.sound(soundAsset+"s4_p6_1.ogg");
var sound_6 = new buzz.sound(soundAsset+"s4_p6_2.ogg");
var sound_7 = new buzz.sound(soundAsset+"s4_p8.ogg");
var sound_8 = new buzz.sound(soundAsset+"s4_p9.ogg");

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		additionalclasscontentblock : '',
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p4text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'magnet_middle',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'arrow_north arrow move_arrow',
					imgsrc: imgpath + "arrow.png",
				},
				{
					imgclass:'arrow_south arrow move_arrow',
					imgsrc: imgpath + "arrow.png",
				}
			],
			imagelabels : [
				{
					imagelabelclass : "magnet_label",
					imagelabeldata : data.string.p4text2
				},
			]
			},
		]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		additionalclasscontentblock : '',
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p4text3,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'magnet_middle',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'arrow_north arrow move_arrow',
					imgsrc: imgpath + "arrow.png",
				}
			],
			imagelabels : [
				{
					imagelabelclass : "magnet_label_north",
					imagelabeldata : data.string.npole
				},
			]
			},
		]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		additionalclasscontentblock : '',
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p4text4,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'magnet_middle',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'arrow_south arrow move_arrow',
					imgsrc: imgpath + "arrow.png",
				}
			],
			imagelabels : [
				{
					imagelabelclass : "magnet_label_south",
					imagelabeldata : data.string.spole
				},
			]
			},
		]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'zooming_effect',

		imageblock : [{
			imagestoshow : [
				{
					imgclass:'magnet_middle ',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'arrow_south arrow move_arrow',
					imgsrc: imgpath + "arrow.png",
				},
				{
					imgclass:'arrow_north arrow move_arrow',
					imgsrc: imgpath + "arrow.png",
				}
			],
			imagelabels : [
				{
					imagelabelclass : "magnet_label_south",
					imagelabeldata : data.string.spole
				},
				{
					imagelabelclass : "magnet_label_north",
					imagelabeldata : data.string.npole
				},
			]
			},
		]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		additionalclasscontentblock : '',
		uppertextblockadditionalclass: 'total_cover',
		uppertextblock : [{
			textdata : data.string.ttt,
			textclass : 'time_to_think'
		},
		{
			textdata : data.string.p4text5,
			textclass : 'think_question'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'ttt',
					imgsrc: "images/lokharke/squirrel_what_animated.svg",
				},
			],
			},
		]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p4text6,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'magnet_left',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'magnet_right',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'arrow_left sidearrow move_arrow_side',
					imgsrc: imgpath + "sidearrow.png",
				},
				{
					imgclass:'arrow_right sidearrow move_arrow_side_1',
					imgsrc: imgpath + "sidearrow.png",
				}
			],
			},
		]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		imageblock : [{
			imagestoshow : [
				//hiddenmagnet
				{
					imgclass:'magnet_left2 duplicate mgl_1',
					imgsrc: imgpath + "magnet02flipped.png",
				},
				{
					imgclass:'magnet_right2 duplicate_1 mgr_1',
					imgsrc: imgpath + "magnet02flipped.png",
				},
				{
					imgclass:'arrow_left sidearrow_1 move_arrow_side',
					imgsrc: imgpath + "sidearrow.png",
				},
				{
					imgclass:'arrow_right sidearrow_1 move_arrow_side_1',
					imgsrc: imgpath + "sidearrow.png",
				},
				{
					imgclass:'magnet_left2',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'magnet_right2',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'arrow_left2 sidearrow',
					imgsrc: imgpath + "sidearrow.png",
				},
				{
					imgclass:'arrow_right2 sidearrow',
					imgsrc: imgpath + "sidearrow.png",
				},
			],
			},
		]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		additionalclasscontentblock : '',
		uppertextblockadditionalclass: 'total_cover',
		uppertextblock : [
		{
			textdata : data.string.p4text8,
			textclass : 'think_question'
		}],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p4text9,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'magnet_left repel',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'magnet_right repel_1',
					imgsrc: imgpath + "magnet02flipped.png",
				},
				{
					imgclass:'arrow_left sidearrow move_arrow_side_2',
					imgsrc: imgpath + "sidearrow.png",
				},
				{
					imgclass:'arrow_right sidearrow move_arrow_side_3',
					imgsrc: imgpath + "sidearrow.png",
				}
			],
			},
		],
		lowertextblockadditionalclass: 'footer',
		lowertextblock : [{
			textdata : data.string.p4text11,
			textclass : ''
		}],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p4text9,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				//hiddenmagnet
				{
					imgclass:'magnet_left_r duplicate mgl_1',
					imgsrc: imgpath + "magnet02flipped.png",
				},
				{
					imgclass:'magnet_right_r duplicate_1 mgr_1',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'arrow_left sidearrow_1 move_arrow_side_2',
					imgsrc: imgpath + "sidearrow.png",
				},
				{
					imgclass:'arrow_right sidearrow_1 move_arrow_side_3',
					imgsrc: imgpath + "sidearrow.png",
				},
				{
					imgclass:'magnet_left_r',
					imgsrc: imgpath + "magnet02.png",
				},
				{
					imgclass:'magnet_right_r',
					imgsrc: imgpath + "magnet02flipped.png",
				},
				{
					imgclass:'arrow_left_r sidearrow',
					imgsrc: imgpath + "sidearrow.png",
				},
				{
					imgclass:'arrow_right_r sidearrow',
					imgsrc: imgpath + "sidearrow.png",
				},
			],
			},
		],
		lowertextblockadditionalclass: 'footer',
		lowertextblock : [{
			textdata : data.string.p4text11,
			textclass : ''
		},
		{
			textdata : data.string.p4text10,
			textclass : 'its_hidden'
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

  	var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();
	var go_next = 0;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			soundplayer(sound_1);
			break;
		case 1:
			soundplayer(sound_2);
			break;
		case 2:
			soundplayer(sound_3);
		break;
		case 4:
		soundplayer(sound_4);
		break;
		case 5:
			go_next = 0;
			soundplayer(sound_5);

			$('.sidearrow').hide(0);

			break;
		case 6:
			navigationcontroller();
			go_next = 0;
			$('.sidearrow_1').hide(0);
			break;
		case 7:
			soundplayer(sound_7);

			break;
		case 8:
			soundplayer(sound_8);

			break;
		case 9:
			go_next = 0;
			$('.sidearrow_1').hide(0);
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function soundplayer(i, next){
 		buzz.all().stop();
 		i.play().bind("ended",()=>{
 			if(!next) navigationcontroller();
 		});
 	}

 	function soundplayertwo(i, j, next){
 		buzz.all().stop();
 		i.play().bind("ended",()=>{
 			j.play().bind("ended",()=>{
 				if(!next) navigationcontroller();
 			});
 		});
 	}

	function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

		}
   }

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		// navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			case 5:
				$nextBtn.hide();
				if(!go_next){
					$('.sidearrow').show(0);
					$('.magnet_left').addClass('attract');
					$('.magnet_right').addClass('attract_1');
					$('.introduction>p').html(data.string.p4text7);
					soundplayer(sound_6);
					go_next++;
				} else {
					countNext++;
					templateCaller();
				}
				break;
			case 6:
				if(!go_next){
					$('.sidearrow_1').show(0);
					$('.mgl_1').css({
						'left': '10%', 'top': '60%'
					});
					$('.magnet_left2').removeClass('duplicate');
					$('.mgr_1').css({
						'left': '60%', 'top': '60%'
					});
					$('.mgr_1').removeClass('duplicate_1');
					$('.mgl_1').addClass('attract');
					$('.mgr_1').addClass('attract_1');
					$('.introduction>p').html(data.string.p4text7);
					go_next++;
				} else {
					countNext++;
					templateCaller();
				}
				break;
			case 9:
				if(!go_next){
					$('.sidearrow_1').show(0);
					$('.mgl_1').css({
						'left': '10%', 'top': '60%'
					});
					$('.mgl_1').removeClass('duplicate');
					$('.mgr_1').css({
						'left': '60%', 'top': '60%'
					});
					$('.mgr_1').removeClass('duplicate_1');
					$('.mgl_1').addClass('repel');
					$('.mgr_1').addClass('repel_1');
					$('.its_hidden').show(0);
					$nextBtn.hide(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				}
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

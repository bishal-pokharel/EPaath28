var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound(soundAsset+"s6_p1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s6_p2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s6_p3.ogg");
var sound_4 = new buzz.sound(soundAsset+"s6_p4.ogg");
var sound_5 = new buzz.sound(soundAsset+"s6_p5.ogg");
var sound_6 = new buzz.sound(soundAsset+"s6_p6.ogg");
var sound_7 = new buzz.sound(soundAsset+"s6_p7.ogg");
var sound_8 = new buzz.sound(soundAsset+"s6_p8.ogg");
var sound_9 = new buzz.sound(soundAsset+"s6_p9.ogg");
var sound_10 = new buzz.sound(soundAsset+"s6_p10.ogg");
var sound_11 = new buzz.sound(soundAsset+"s6_p11.ogg");
var sound_12 = new buzz.sound(soundAsset+"s6_p12.ogg");
var sound_13 = new buzz.sound(soundAsset+"s6_p13.ogg");

var content=[
	{
		//starting page
		uppertextblock : [
		{
			textclass : "firsttitle",
			textdata : data.string.p6text1
		}
		]
	},
	{
		//page1
		headerblock : [
		{
			textclass : "",
			textdata : data.string.p6text8
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "magnet",
				imgsrc : imgpath + "magnet02.png"
			},
			],
		}
		]
	},
	{
		contentblockadditionalclass: "contentwithbg",
		//page2
		uppertextblockadditionalclass: 'cover_all',
		uppertextblock : [
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "magnetearth",
				imgsrc : imgpath + "magnet02.png"
			},
			{
				imgclass : "earth cssfadein",
				imgsrc : imgpath + "earth.png"
			},
			],
		}
		],
	},
	{
		contentblockadditionalclass: "contentwithbg",
		//page3
		uppertextblockadditionalclass: 'cover_all',
		uppertextblock : [
			{
				textclass : "southtext",
				textdata : data.string.mnpole
			},
			{
				textclass : "northtext",
				textdata : data.string.mspole
			},
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text2
			},
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			},
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "arrowone",
				imgsrc : imgpath + "arrow02.png"
			},
			{
				imgclass : "arrowtwo",
				imgsrc : imgpath + "arrow02.png"
			},
			{
				imgclass : "magnetearth",
				imgsrc : imgpath + "magnet02.png"
			},
			{
				imgclass : "earth",
				imgsrc : imgpath + "earth.png"
			},
			],
		}
		],
	},
	{
		//page4
		contentblockadditionalclass: "contentwithbg",
		uppertextblockadditionalclass: 'cover_all',
		uppertextblock : [
			{
				textclass : "southtext",
				textdata : data.string.mnpole
			},
			{
				textclass : "northtext",
				textdata : data.string.mspole
			},
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			},
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text3
			},
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "arrowone",
				imgsrc : imgpath + "arrow02.png"
			},
			{
				imgclass : "arrowtwo",
				imgsrc : imgpath + "arrow02.png"
			},
			{
				imgclass : "magnetearth",
				imgsrc : imgpath + "magnet02.png"
			},
			{
				imgclass : "earth",
				imgsrc : imgpath + "earth.png"
			},
			],
		}
		],
	},
	{
		//page5
		contentblockadditionalclass : "contentwithbg",
		uppertextblockadditionalclass: 'cover_all',
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "earth",
				imgsrc : imgpath + "globe.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			},
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text5
			},
		]
	},
	{
		//page6
		contentblockadditionalclass : "contentwithbg",
		uppertextblockadditionalclass: 'cover_all',
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "insidemag",
				imgsrc : imgpath + "arrow01.png"
			},
			{
				imgclass : "earth fluc",
				imgsrc : imgpath + "earth.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			},
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text6
			},
		]
	},
	{
		//page7
		contentblockadditionalclass : "contentwithbg",
		uppertextblockadditionalclass: 'cover_all',
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "insidemag highzin",
				imgsrc : imgpath + "arrow01.png"
			},
			{
				imgclass : "earth half",
				imgsrc : imgpath + "earth.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			},
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text7
			},
		]
	},
	{
		//page8
		contentblockadditionalclass : "contentwithbg",
		uppertextblockadditionalclass: 'cover_all',
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "insidemag highzin",
				imgsrc : imgpath + "arrow01.png"
			},
			{
				imgclass : "earth half",
				imgsrc : imgpath + "earth.png"
			},
			{
				imgclass : "arrowone",
				imgsrc : imgpath + "arrow02.png"
			},
			{
				imgclass : "arrowtwo",
				imgsrc : imgpath + "arrow02.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "southtext",
				textdata : data.string.p6text9
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "northtext",
				textdata : data.string.p6text10
			},
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text7
			},
		]
	},
	{
		//page9
		contentblockadditionalclass : "contentwithbg",
		uppertextblockadditionalclass: 'cover_all',
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "insidemag highzin",
				imgsrc : imgpath + "arrow01.png"
			},
			{
				imgclass : "earth half",
				imgsrc : imgpath + "earth.png"
			},
			{
				imgclass : "magnet magrotate highzin",
				imgsrc : imgpath + "magnet02.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			},
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text11
			},
		]
	},
	{
		//page10
		contentblockadditionalclass : "contentwithbg",
		uppertextblockadditionalclass: 'cover_all',
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "earth",
				imgsrc : imgpath + "earth.png"
			},
			{
				imgclass : "magnetearth highzin",
				imgsrc : imgpath + "magnet02.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "northpole",
				textdata : data.string.npole
			},
			{
				textclass : "southpole",
				textdata : data.string.spole
			},
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text12
			},
		]
	},
	{
		//page11
		contentblockadditionalclass : "contentwithbg2",
		uppertextblockadditionalclass: 'cover_all',
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "earth",
				imgsrc : imgpath + "compass.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text13
			},
		]
	},
	{
		//page12
		contentblockadditionalclass : "contentwithbg2",
		uppertextblockadditionalclass: 'cover_all',
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "earth",
				imgsrc : imgpath + "compass.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "descriptionblock",
				textdata : data.string.p6text14
			},
		]
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;


  	var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();
	/*recalculateHeightWidth();*/

	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

		 function navigationcontroller(islastpageflag){
	   		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	 		if (countNext == 0 && $total_page != 1) {
	 			$nextBtn.show(0);
	 			$prevBtn.css('display', 'none');
	 		} else if ($total_page == 1) {
	 			$prevBtn.css('display', 'none');
	 			$nextBtn.css('display', 'none');

	 			// if lastpageflag is true
	 			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	 		} else if (countNext > 0 && countNext < $total_page - 1) {
	 			$nextBtn.show(0);
	 			$prevBtn.show(0);
	 		} else if (countNext == $total_page - 1) {
	 			$nextBtn.css('display', 'none');
	 			$prevBtn.show(0);

	 			// if lastpageflag is true
	 			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	 		}
	    }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		soundplayer(eval(`sound_${countNext+1}`));

		if(countNext==12) setTimeout(()=>ole.footerNotificationHandler.pageEndSetNotification(),10000);

		switch (countNext){
			case 1:
				$('.magnet').click(function(){
					$(this).addClass('magrotate');
					$nextBtn.delay(4000).show(0);
					$prevBtn.delay(4000).show(0);
				});
			break;
		}
	}

	function soundplayer(i, next){
		buzz.all().stop();
		i.play().bind("ended",()=>{
			if(!next) navigationcontroller();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

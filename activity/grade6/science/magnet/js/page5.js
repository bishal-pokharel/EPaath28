var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound(soundAsset+"s5_p2_1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s5_p2_2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s5_p3_1.ogg");
var sound_4 = new buzz.sound(soundAsset+"s5_p3_2.ogg");

var content = [

	//slide0

	{
		contentblocknocenteradjust : true,

	 	imageblockadditionalclass: 'cover_full',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "squirrel",
				imgsrc : 'images/diy/diy4.png',
			}],
			imagelabels: [{
				imagelabelclass : "font_diy",
				imagelabeldata : data.string.diy
			}]
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		additionalclasscontentblock : '',
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			datahighlightflag: true,
			textdata : data.string.p5text1,
			textclass : ''
		},
		{
			textdata : data.string.p5text7,
			textclass : 'remaining_text'
		}],
		qnablock:[{
			magnetpair:'pair_s_n attract_pair',
			magnet1class: 'magnet_l',
			magnet1src: imgpath + 'magnet02.png',
			magnet2class: 'magnet_r',
			magnet2src: imgpath + 'magnet02.png'
		},
		{
			magnetpair:'pair_n_n repel_pair',
			magnet1class: 'magnet_l',
			magnet1src: imgpath + 'magnet02flipped.png',
			magnet2class: 'magnet_r',
			magnet2src: imgpath + 'magnet02.png'
		},
		{
			magnetpair:'pair_s_s repel_pair',
			magnet1class: 'magnet_l',
			magnet1src: imgpath + 'magnet02.png',
			magnet2class: 'magnet_r',
			magnet2src: imgpath + 'magnet02flipped.png'
		},
		{
			magnetpair:'pair_n_s attract_pair',
			magnet1class: 'magnet_l',
			magnet1src: imgpath + 'magnet02flipped.png',
			magnet2class: 'magnet_r',
			magnet2src: imgpath + 'magnet02flipped.png'
		}],
		lowertextblockadditionalclass: 'feedback',
		lowertextblock : [
		{
			textdata : data.string.p5text5,
			textclass : ''
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		additionalclasscontentblock : '',
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p5text2,
			textclass : ''
		},
		{
			textdata : data.string.p5text7,
			textclass : 'remaining_text'
		}],
		qnablock:[{
			magnetpair:'pair_s_n attract_pair',
			magnet1class: 'magnet_l',
			magnet1src: imgpath + 'magnet02.png',
			magnet2class: 'magnet_r',
			magnet2src: imgpath + 'magnet02.png'
		},
		{
			magnetpair:'pair_n_n repel_pair',
			magnet1class: 'magnet_l',
			magnet1src: imgpath + 'magnet02flipped.png',
			magnet2class: 'magnet_r',
			magnet2src: imgpath + 'magnet02.png'
		},
		{
			magnetpair:'pair_s_s repel_pair',
			magnet1class: 'magnet_l',
			magnet1src: imgpath + 'magnet02.png',
			magnet2class: 'magnet_r',
			magnet2src: imgpath + 'magnet02flipped.png'
		},
		{
			magnetpair:'pair_n_s attract_pair',
			magnet1class: 'magnet_l',
			magnet1src: imgpath + 'magnet02flipped.png',
			magnet2class: 'magnet_r',
			magnet2src: imgpath + 'magnet02flipped.png'
		}],
		lowertextblockadditionalclass: 'feedback',
		lowertextblock : [
		{
			textdata : data.string.p5text6,
			textclass : ''
		}],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

  	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var go_next = 0;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			play_diy_audio();
			navigationcontroller();
			break;
		case 1:
			soundplayer(sound_1,1);
			$('.magnetpair').click(function(){
				var class_name = $(this).attr('class').toString().replace(/attract_pair/g, '');
				class_name = class_name.replace(/repel_pair/g, '');
				class_name = class_name.replace(/magnetpair/g, '');
				class_name = class_name.replace(/ /g, '');
				$(this).data('clicked', true);
				if($(this).hasClass('attract_pair')){
					$(this).css({'border': '0.2em solid #508432', 'pointer-events': 'none'});
					$(this).css({'background': '#FFF2CC'});
					$('.'+class_name+'>.magnet_l').addClass('attract');
					$('.'+class_name+'>.magnet_r').addClass('attract_1');
					play_correct_incorrect_sound(1);
				} else if($(this).hasClass('repel_pair')){
					$(this).css({'border': '0.2em solid #CC0000', 'pointer-events': 'none'});
					$('.'+class_name+'>.magnet_l').addClass('repel');
					$('.'+class_name+'>.magnet_r').addClass('repel_1');
					$('.correct').html(data.string.p5text4);
					play_correct_incorrect_sound(0);
				}
				for(var i =0 ; i<2; i++){
					if($('.attract_pair').eq(i).data('clicked') == false){
						$('.remaining_text').show(0);
						return true;
					}
				}
				$('.remaining_text').hide(0);
				setTimeout(function(){
					$('.magnetpair').css({'border': 'none', 'transition': '1s linear'});
					$('.attract_pair').css({'transform':'scale(1.3)'});
					$('.feedback').fadeIn(1000, ()=>soundplayer(sound_2));
					$('.repel_pair').addClass('fade_out');
					// $nextBtn.show(0);
				}, 1000);
			});
			break;
		case 2:
		soundplayer(sound_3);
			$('.magnetpair').click(function(){
				var class_name = $(this).attr('class').toString().replace(/attract_pair/g, '');
				class_name = class_name.replace(/repel_pair/g, '');
				class_name = class_name.replace(/magnetpair/g, '');
				class_name = class_name.replace(/ /g, '');
				$(this).data('clicked', true);
				if($(this).hasClass('attract_pair')){
					$(this).css({'border': '0.2em solid #CC0000', 'pointer-events': 'none'});
					$('.'+class_name+'>.magnet_l').addClass('attract');
					$('.'+class_name+'>.magnet_r').addClass('attract_1');
					$('.correct').html(data.string.p5text4);
					play_correct_incorrect_sound(0);
				} else if($(this).hasClass('repel_pair')){
					$(this).css({'border': '0.2em solid #508432', 'pointer-events': 'none'});
					$(this).css({'background': '#FFF2CC'});
					$('.'+class_name+'>.magnet_l').addClass('repel');
					$('.'+class_name+'>.magnet_r').addClass('repel_1');
					play_correct_incorrect_sound(1);
				}
				for(var i =0 ; i<2; i++){
					if($('.repel_pair').eq(i).data('clicked') == false){
						$('.remaining_text').show(0);
						return true;
					}
				}
				$('.remaining_text').hide(0);
				setTimeout(function(){
					$('.magnetpair').css({'border': 'none', 'transition': '1s linear'});
					$('.repel_pair').css({'transform':'scale(1.3)'});
					$('.feedback').fadeIn(1000, ()=>soundplayer(sound_4));
					$('.attract_pair').addClass('fade_out');
					setTimeout(()=>ole.footerNotificationHandler.pageEndSetNotification(),3900);
				}, 1000);
			});
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}


	function soundplayer(i, next){
 		buzz.all().stop();
 		i.play().bind("ended",()=>{
 			if(!next) navigationcontroller();
 		});
 	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

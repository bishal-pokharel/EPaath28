var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0

	{
		contentblocknocenteradjust : true,

	 	imageblockadditionalclass: 'cover_full',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "squirrel",
				imgsrc : 'images/diy/diy4.png',
			}],
			imagelabels: [{
				imagelabelclass : "font_diy",
				imagelabeldata : data.string.diy
			}]
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_main',
		uppertextblockadditionalclass: 'cover_full',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'introduction'
		}],
		draggableblockadditionalclass: '',
		draggableblock:[
			{
				imgclass: 'safetypin magnetic position_1',
				imgsrc: imgpath + 'DIY/safetypin.png',
				name: data.string.item_1,
				material: data.string.material_steel
			},
			{
				imgclass: 'marble nonmagnetic position_2',
				imgsrc: imgpath + 'DIY/marble.png',
				name: data.string.item_2,
				material: data.string.material_marble
			},
			{
				imgclass: 'screw magnetic position_3',
				imgsrc: imgpath + 'DIY/screw01.png',
				name: data.string.item_3,
				material: data.string.material_steel
			},
			{
				imgclass: 'cup nonmagnetic position_4',
				imgsrc: imgpath + 'DIY/plastic-glass.png',
				name: data.string.item_4,
				material: data.string.material_plastic
			},
			{
				imgclass: 'scissors magnetic position_5',
				imgsrc: imgpath + 'DIY/scissors.png',
				name: data.string.item_5,
				material: data.string.material_steel
			},
			{
				imgclass: 'toothbrush nonmagnetic position_6',
				imgsrc: imgpath + 'DIY/brush.png',
				name: data.string.item_6,
				material: data.string.material_plastic
			},
			{
				imgclass: 'ruler nonmagnetic position_7',
				imgsrc: imgpath + 'DIY/ruler.png',
				name: data.string.item_7,
				material: data.string.material_plastic
			},
			{
				imgclass: 'key magnetic position_8',
				imgsrc: imgpath + 'DIY/key.png',
				name: data.string.item_8,
				material: data.string.material_steel
			},
			{
				imgclass: 'nailcutter magnetic position_9',
				imgsrc: imgpath + 'DIY/nailcutter.png',
				name: data.string.item_9,
				material: data.string.material_steel
			},
			{
				imgclass: 'glass nonmagnetic position_10',
				imgsrc: imgpath + 'DIY/glass.png',
				name: data.string.item_10,
				material: data.string.material_glass
			},
			{
				imgclass: 'spoon magnetic position_11',
				imgsrc: imgpath + 'DIY/spoon.png',
				name: data.string.item_11,
				material: data.string.material_steel
			}
		],
		droppableblockadditionalclass:'',
		droppableblock:[
			{
				droppablediv_class: 'droppable_magnet',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text2,
			},
			{
				droppablediv_class: 'droppable_nonmagnet',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text3,
			},
		],
		extratextblock : [
		{
			textdata : data.string.p2text4,
			textclass : 'center_text'
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_main',
		uppertextblockadditionalclass: 'introduction_1',
		uppertextblock : [{
			textdata : data.string.p2text5,
			textclass : ''
		}],
		imageblockadditionalclass: 'cover_full',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "belt",
				imagelabeldata : '',
			}]
		}],
		draggableblockadditionalclass: 'back_face',
		draggableblock:[
			{
				imgclass: 'safetypin magnetic position_1',
				imgsrc: imgpath + 'DIY/safetypin.png',
				name: data.string.item_1,
				material: data.string.material_steel
			},
			{
				imgclass: 'marble nonmagnetic position_2',
				imgsrc: imgpath + 'DIY/marble.png',
				name: data.string.item_2,
				material: data.string.material_marble
			},
			{
				imgclass: 'screw magnetic position_3',
				imgsrc: imgpath + 'DIY/screw01.png',
				name: data.string.item_3,
				material: data.string.material_steel
			},
			{
				imgclass: 'cup nonmagnetic position_4',
				imgsrc: imgpath + 'DIY/plastic-glass.png',
				name: data.string.item_4,
				material: data.string.material_plastic
			},
			{
				imgclass: 'scissors magnetic position_5',
				imgsrc: imgpath + 'DIY/scissors.png',
				name: data.string.item_5,
				material: data.string.material_steel
			},
			{
				imgclass: 'toothbrush nonmagnetic position_6',
				imgsrc: imgpath + 'DIY/brush.png',
				name: data.string.item_6,
				material: data.string.material_plastic
			},
			{
				imgclass: 'ruler nonmagnetic position_7',
				imgsrc: imgpath + 'DIY/ruler.png',
				name: data.string.item_7,
				material: data.string.material_plastic
			},
			{
				imgclass: 'key magnetic position_8',
				imgsrc: imgpath + 'DIY/key.png',
				name: data.string.item_8,
				material: data.string.material_steel
			},
			{
				imgclass: 'nailcutter magnetic position_9',
				imgsrc: imgpath + 'DIY/nailcutter.png',
				name: data.string.item_9,
				material: data.string.material_steel
			},
			{
				imgclass: 'glass nonmagnetic position_10',
				imgsrc: imgpath + 'DIY/glass.png',
				name: data.string.item_10,
				material: data.string.material_glass
			},
			{
				imgclass: 'spoon magnetic position_11',
				imgsrc: imgpath + 'DIY/spoon.png',
				name: data.string.item_11,
				material: data.string.material_steel
			}
		],
		droppableblockadditionalclass:'',
		droppableblock:[
			{
				droppablediv_class: 'magnet_box',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text2,
			},
			{
				droppablediv_class: 'nonmagnet_box',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text3,
			},
			{
				droppablediv_class: 'magnet',
				imgclass: 'magnet_img',
				imgsrc: imgpath + 'magnet01.png',
				imagelabelclass: '',
				imagelabeldata: '',
			}
		]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_main',
		uppertextblockadditionalclass: 'introduction_1',
		uppertextblock : [{
			textdata : data.string.p2text6,
			textclass : ''
		}],
		imageblockadditionalclass: 'cover_full',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "belt",
				imagelabeldata : '',
			}]
		}],
		draggableblockadditionalclass: 'back_face',
		draggableblock:[
			{
				imgclass: 'safetypin magnetic position_1',
				imgsrc: imgpath + 'DIY/safetypin.png',
				name: data.string.item_1,
				material: data.string.material_steel
			},
			{
				imgclass: 'marble nonmagnetic position_2',
				imgsrc: imgpath + 'DIY/marble.png',
				name: data.string.item_2,
				material: data.string.material_marble
			},
			{
				imgclass: 'screw magnetic position_3',
				imgsrc: imgpath + 'DIY/screw01.png',
				name: data.string.item_3,
				material: data.string.material_steel
			},
			{
				imgclass: 'cup nonmagnetic position_4',
				imgsrc: imgpath + 'DIY/plastic-glass.png',
				name: data.string.item_4,
				material: data.string.material_plastic
			},
			{
				imgclass: 'scissors magnetic position_5',
				imgsrc: imgpath + 'DIY/scissors.png',
				name: data.string.item_5,
				material: data.string.material_steel
			},
			{
				imgclass: 'toothbrush nonmagnetic position_6',
				imgsrc: imgpath + 'DIY/brush.png',
				name: data.string.item_6,
				material: data.string.material_plastic
			},
			{
				imgclass: 'ruler nonmagnetic position_7',
				imgsrc: imgpath + 'DIY/ruler.png',
				name: data.string.item_7,
				material: data.string.material_plastic
			},
			{
				imgclass: 'key magnetic position_8',
				imgsrc: imgpath + 'DIY/key.png',
				name: data.string.item_8,
				material: data.string.material_steel
			},
			{
				imgclass: 'nailcutter magnetic position_9',
				imgsrc: imgpath + 'DIY/nailcutter.png',
				name: data.string.item_9,
				material: data.string.material_steel
			},
			{
				imgclass: 'glass nonmagnetic position_10',
				imgsrc: imgpath + 'DIY/glass.png',
				name: data.string.item_10,
				material: data.string.material_glass
			},
			{
				imgclass: 'spoon magnetic position_11',
				imgsrc: imgpath + 'DIY/spoon.png',
				name: data.string.item_11,
				material: data.string.material_steel
			}
		],
		droppableblockadditionalclass:'',
		droppableblock:[
			{
				droppablediv_class: 'magnet_box',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text2,
			},
			{
				droppablediv_class: 'nonmagnet_box',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text3,
			},
			{
				droppablediv_class: 'magnet',
				imgclass: 'magnet_img',
				imgsrc: imgpath + 'magnet01.png',
				imagelabelclass: '',
				imagelabeldata: '',
			},
		],
		lowertextblockadditionalclass: 'feedback',
		lowertextblock : [{
			textdata : '',
			textclass : 'feedback_1'
		},
		{
			textdata : '',
			textclass : 'feedback_2'
		}],
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_main',
		uppertextblockadditionalclass: 'introduction_1',
		uppertextblock : [{
			textdata : data.string.p2text8,
			textclass : ''
		}],
		imageblockadditionalclass: 'cover_full',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "belt",
				imagelabeldata : '',
			}]
		}],
		draggableblockadditionalclass: 'back_face',
		draggableblock:[
			{
				imgclass: 'safetypin magnetic position_1',
				imgsrc: imgpath + 'DIY/safetypin.png',
				name: data.string.item_1,
				material: data.string.material_steel
			},
			{
				imgclass: 'marble nonmagnetic position_2',
				imgsrc: imgpath + 'DIY/marble.png',
				name: data.string.item_2,
				material: data.string.material_marble
			},
			{
				imgclass: 'screw magnetic position_3',
				imgsrc: imgpath + 'DIY/screw01.png',
				name: data.string.item_3,
				material: data.string.material_steel
			},
			{
				imgclass: 'cup nonmagnetic position_4',
				imgsrc: imgpath + 'DIY/plastic-glass.png',
				name: data.string.item_4,
				material: data.string.material_plastic
			},
			{
				imgclass: 'scissors magnetic position_5',
				imgsrc: imgpath + 'DIY/scissors.png',
				name: data.string.item_5,
				material: data.string.material_steel
			},
			{
				imgclass: 'toothbrush nonmagnetic position_6',
				imgsrc: imgpath + 'DIY/brush.png',
				name: data.string.item_6,
				material: data.string.material_plastic
			},
			{
				imgclass: 'ruler nonmagnetic position_7',
				imgsrc: imgpath + 'DIY/ruler.png',
				name: data.string.item_7,
				material: data.string.material_plastic
			},
			{
				imgclass: 'key magnetic position_8',
				imgsrc: imgpath + 'DIY/key.png',
				name: data.string.item_8,
				material: data.string.material_steel
			},
			{
				imgclass: 'nailcutter magnetic position_9',
				imgsrc: imgpath + 'DIY/nailcutter.png',
				name: data.string.item_9,
				material: data.string.material_steel
			},
			{
				imgclass: 'glass nonmagnetic position_10',
				imgsrc: imgpath + 'DIY/glass.png',
				name: data.string.item_10,
				material: data.string.material_glass
			},
			{
				imgclass: 'spoon magnetic position_11',
				imgsrc: imgpath + 'DIY/spoon.png',
				name: data.string.item_11,
				material: data.string.material_steel
			}
		],
		droppableblockadditionalclass:'',
		droppableblock:[
			{
				droppablediv_class: 'magnet_box',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text2,
			},
			{
				droppablediv_class: 'nonmagnet_box',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text3,
			},
			{
				droppablediv_class: 'magnet',
				imgclass: 'magnet_img',
				imgsrc: imgpath + 'magnet01.png',
				imagelabelclass: '',
				imagelabeldata: '',
			}
		]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_main',
		uppertextblockadditionalclass: 'introduction_1',
		uppertextblock : [{
			textdata : data.string.p2text8,
			textclass : ''
		}],
		imageblockadditionalclass: 'cover_full',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "belt",
				imagelabeldata : '',
			}]
		}],
		draggableblockadditionalclass: 'back_face',
		draggableblock:[
			{
				imgclass: 'safetypin magnetic position_1',
				imgsrc: imgpath + 'DIY/safetypin.png',
				name: data.string.item_1,
				material: data.string.material_steel
			},
			{
				imgclass: 'marble nonmagnetic position_2',
				imgsrc: imgpath + 'DIY/marble.png',
				name: data.string.item_2,
				material: data.string.material_marble
			},
			{
				imgclass: 'screw magnetic position_3',
				imgsrc: imgpath + 'DIY/screw01.png',
				name: data.string.item_3,
				material: data.string.material_steel
			},
			{
				imgclass: 'cup nonmagnetic position_4',
				imgsrc: imgpath + 'DIY/plastic-glass.png',
				name: data.string.item_4,
				material: data.string.material_plastic
			},
			{
				imgclass: 'scissors magnetic position_5',
				imgsrc: imgpath + 'DIY/scissors.png',
				name: data.string.item_5,
				material: data.string.material_steel
			},
			{
				imgclass: 'toothbrush nonmagnetic position_6',
				imgsrc: imgpath + 'DIY/brush.png',
				name: data.string.item_6,
				material: data.string.material_plastic
			},
			{
				imgclass: 'ruler nonmagnetic position_7',
				imgsrc: imgpath + 'DIY/ruler.png',
				name: data.string.item_7,
				material: data.string.material_plastic
			},
			{
				imgclass: 'key magnetic position_8',
				imgsrc: imgpath + 'DIY/key.png',
				name: data.string.item_8,
				material: data.string.material_steel
			},
			{
				imgclass: 'nailcutter magnetic position_9',
				imgsrc: imgpath + 'DIY/nailcutter.png',
				name: data.string.item_9,
				material: data.string.material_steel
			},
			{
				imgclass: 'glass nonmagnetic position_10',
				imgsrc: imgpath + 'DIY/glass.png',
				name: data.string.item_10,
				material: data.string.material_glass
			},
			{
				imgclass: 'spoon magnetic position_11',
				imgsrc: imgpath + 'DIY/spoon.png',
				name: data.string.item_11,
				material: data.string.material_steel
			}
		],
		droppableblockadditionalclass:'',
		droppableblock:[
			{
				droppablediv_class: 'magnet_box',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text2,
			},
			{
				droppablediv_class: 'nonmagnet_box',
				imgclass: 'basket',
				imgsrc: imgpath + 'DIY/basket02.png',
				imagelabelclass: '',
				imagelabeldata: data.string.p2text3,
			},
			{
				droppablediv_class: 'magnet',
				imgclass: 'magnet_img',
				imgsrc: imgpath + 'magnet01.png',
				imagelabelclass: '',
				imagelabeldata: '',
			},
		],
		lowertextblockadditionalclass: 'feedback',
		lowertextblock : [{
			textdata : '',
			textclass : 'feedback_1'
		},
		{
			textdata : '',
			textclass : 'feedback_2'
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		tableblock:[
			{
				tableclass: "magnetic_objects_table",
				tablelabelclass: '',
				tablelabeldata: data.string.p2text2,
				tableclass: 'table_1',
				table:[
					{
						imgclass: 'safetypin t_img_1',
						imgsrc: imgpath + 'DIY/safetypin.png',
					},
					{
						imgclass: 'screw t_img_2',
						imgsrc: imgpath + 'DIY/screw01.png',
					},
					{
						imgclass: 'scissors t_img_3',
						imgsrc: imgpath + 'DIY/scissors.png',
					},
					{
						imgclass: 'key t_img_4',
						imgsrc: imgpath + 'DIY/key.png',
					},
					{
						imgclass: 'nailcutter t_img_5',
						imgsrc: imgpath + 'DIY/nailcutter.png',
					},
					{
						imgclass: 'spoon t_img_6',
						imgsrc: imgpath + 'DIY/spoon.png',
					}
				],
			},
			{
				tableclass: "non_magnetic_objects_table",
				tablelabelclass: '',
				tablelabeldata: data.string.p2text3,
				tableclass: 'table_2',
				table:[
					{
						imgclass: 'marble t_img_1',
						imgsrc: imgpath + 'DIY/marble.png',
					},
					{
						imgclass: 'cup t_img_2',
						imgsrc: imgpath + 'DIY/plastic-glass.png',
					},
					{
						imgclass: 'toothbrush t_img_3',
						imgsrc: imgpath + 'DIY/brush.png',
					},
					{
						imgclass: 'ruler t_img_4',
						imgsrc: imgpath + 'DIY/ruler.png',
					},

					{
						imgclass: 'glass t_img_5',
						imgsrc: imgpath + 'DIY/glass.png',
					}
				]
			}
		],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

  	var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();
		var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
        // sounds
            {id: "sound_1a", src: soundAsset + "s2_p2_1.ogg"},
            {id: "sound_1b", src: soundAsset + "s2_p2_2.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p3.ogg"},
            {id: "sound_3a", src: soundAsset + "s2_p4_1.ogg"},
            {id: "sound_3b", src: soundAsset + "s2_p4_2.ogg"},
            {id: "sound_4", src: soundAsset + "s2_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s2_p7.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

	var go_next = 0;

	var magnet_box = [];
	var nonmagnet_box = [];
	var droppable_elements;


	var timer;
	var object_positions = [];
	// var magnetic_items = count_magnetic(magnet_box);
	var magnet_index = 0;

	var text_feedback_1 = '';
	var text_feedback_2 = '';

	var global_time = null;
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;


   }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
		case 1:
			sound_player("sound_1a");
			magnet_box = [];
			nonmagnet_box = [];
			object_positions = [];
			var counter_magnetic = 0;
			var counter_nonmagnetic = 0;
			var counter_item = 0;
			$(".draggable").draggable({
                containment: "body",
                revert: "invalid",
                appendTo: "body",
                zindex: 1000,
                start: function(event, ui) {
                },
                stop: function(event, ui) {
                }
            });
            $('.droppable_magnet').droppable({
	        	// accept : ".magnetic",
                hoverClass: "hovered",
                drop: function(event, ui) {
                   ui.draggable.draggable('disable');
                   var str_class_name =  ui.draggable.attr('class');
                   str_class_name = str_class_name.split(' ')[1];
                   // console.log(str_class_name);
                   magnet_box.push(str_class_name);
                   var left_position = 3.5 + 13.7*(counter_magnetic%6);
	                $(ui.draggable).detach().css({
	                    "position": "absolute",
	                    "cursor": 'auto',
	                    "height": "35%",
	                    "width": "25%",
	                    "left" : left_position + '%',
	                    "top": '40%'
	                }).appendTo('.droppable_magnet');
	                counter_magnetic++;
	                counter_item++;
	                if(counter_item == 11){
	                	$('.center_text').show(0);
										sound_player_nav("sound_1b");

	                }
                }
            });
            $('.droppable_nonmagnet').droppable({
	        	// accept : ".nonmagnetic",
                hoverClass: "hovered",
                drop: function(event, ui) {
                   ui.draggable.draggable('disable');
                   var str_class_name =  ui.draggable.attr('class');
                   str_class_name = str_class_name.split(' ')[1];
                   // console.log(str_class_name);
                   nonmagnet_box.push(str_class_name);
                   var left_position = 3.5 + 13.7*(counter_nonmagnetic%6);
	                $(ui.draggable).detach().css({
	                    "position": "absolute",
	                    "cursor": 'auto',
	                    "height": "35%",
	                    "width": "25%",
	                    "left" : left_position + '%',
	                    "top": '40%'
	                }).appendTo('.droppable_nonmagnet');
	                counter_nonmagnetic++;
	                counter_item++;
	                if(counter_item == 11){
	                	$('.center_text').show(0);
	                	sound_player_nav("sound_1b");
	                }
                }
            });
			break;
		case 2:
			sound_player_nav("sound_2");
			send_to_box(magnet_box, 1);
			send_to_box(nonmagnet_box, 0);
			break;
		case 3:
				sound_player("sound_3a");
			go_next = 0;
			magnet_index = 0;
			object_positions = [];
			send_to_box(magnet_box, 1);
			send_to_box(nonmagnet_box, 0);
			$('.nonmagnet_box').fadeOut(1000);
			non_magnetic_text(magnet_box);
			for(var m=0; m<nonmagnet_box.length; m++){
				$('.'+nonmagnet_box[m]).fadeOut(1000);
			}
			global_time = setTimeout(function(){
				for(var m=0; m<magnet_box.length; m++){
					$('.'+magnet_box[m]).animate({
						'top': '20%'
						}, 3000);
				}
				$('.magnet_box').delay(1500).fadeOut(1500);
			}, 1000);
			global_time = setTimeout(function(){
				$('.draggable').css('filter','drop-shadow(0px 0px 3px gray)');
			}, 3000);
			global_time = setTimeout(function(){
				$('.draggable').css('filter','none');
				for(var m=0; m<magnet_box.length; m++){
					var left_position = 5 + m*(85-magnet_box.length*7.5)/(magnet_box.length-1) + m*(7.5);
					object_positions.push(left_position);
					timer = 1000 + m*500;
					$('.'+magnet_box[m]).animate({
						'top': '74%',
						'left': left_position + '%'
						}, timer);
				}
				$('.'+magnet_box[magnet_box.length-1]).promise().done(function() {
					after_laying(magnet_box);
				});
			}, 2500);
			break;
		case 4:
			sound_player_nav("sound_4");

			send_to_box(magnet_box, 1);
			send_to_box(nonmagnet_box, 0);
			break;
		case 5:

		// sound_player("sound_5");
			go_next = 0;
			magnet_index = 0;
			send_to_box(magnet_box, 1);
			send_to_box(nonmagnet_box, 0);
			$('.magnet_box').fadeOut(1000);
			magnetic_text(nonmagnet_box);
			object_positions = [];
			for(var m=0; m<magnet_box.length; m++){
				$('.'+magnet_box[m]).fadeOut(1000);
			}
			global_time = setTimeout(function(){
				for(var m=0; m<nonmagnet_box.length; m++){
					$('.'+nonmagnet_box[m]).animate({
						'top': '20%'
						}, 1000);
				}
				$('.nonmagnet_box').fadeOut(1500);
			}, 1000);
			global_time = setTimeout(function(){
				$('.draggable').css('filter','drop-shadow(0px 0px 3px gray)');
			}, 2000);
			global_time = setTimeout(function(){
				$('.draggable').css('filter','none');
				for(var m=0; m<nonmagnet_box.length; m++){
					var left_position = 5 + m*(85-nonmagnet_box.length*7.5)/(nonmagnet_box.length-1) + m*(7.5);
					object_positions.push(left_position);
					timer = 1000 + m*500;
					$('.'+nonmagnet_box[m]).animate({
						'top': '74%',
						'left': left_position + '%'
						}, timer);
				}
				$('.'+nonmagnet_box[nonmagnet_box.length-1]).promise().done(function() {
					after_laying(nonmagnet_box);
				});
			}, 2500);
			break;
		case 6:
			sound_player_nav("sound_6");
			setTimeout(()=>ole.footerNotificationHandler.pageEndSetNotification(),4500);
		break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function soundplayer(i, next){
		buzz.all().stop();
		i.play().bind("ended",()=>{
			if(!next) navigationcontroller();
		});
	}

	function soundplayertwo(i, j, next){
		buzz.all().stop();
		i.play().bind("ended",()=>{
			j.play().bind("ended",()=>{
				if(!next) navigationcontroller();
			});
		});
	}

	function soundplayerthree(i, j, k, next){
		buzz.all().stop();
		i.play().bind("ended",()=>{
			j.play().bind("ended",()=>{
				k.play().bind("ended",()=>{
				 if(!next) navigationcontroller();
			 });
			});
		});
	}

	function send_to_box(box, position){
		for(var m=0; m<box.length; m++){
			if(position){
				var left_position = 6.05 + 4.11*(m%6);
			} else {
				var left_position =  41.05 + 4.11*(m%6);
			}
			$('.'+box[m]).detach().css({
                "position": "absolute",
                "cursor": 'auto',
                "height": "13.5%",
                "width": "7.5%",
                "left" : left_position + '%',
                "top": '41%'
            }).appendTo('.imageblock');
		}
	}
	function count_magnetic(box){
		var magnetic_count = 0;
		for(var m=0; m<box.length; m++){
			if($('.'+box[m]).hasClass('magnetic')){
				magnetic_count++;
			}
		}
		return magnetic_count;
	}

	function non_magnetic_text(box){
		var non_magnetic_items_in_box = [];
		var non_magnetic_items_material = [];
		for(var m=0; m<box.length; m++){
			if($('.'+box[m]).hasClass('nonmagnetic')){
				non_magnetic_items_in_box.push($('.'+box[m]).data('name'));
				non_magnetic_items_material.push($('.'+box[m]).data('material'));
			}
		}
		var uniqueNames = [];
		$.each(non_magnetic_items_material, function(i, el){
			if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
		});
		non_magnetic_items_material = uniqueNames;
		var items_list_box = non_magnetic_items_in_box[0];
		var material_list_box = non_magnetic_items_material[0];
		for(var m = 1; m<non_magnetic_items_in_box.length; m++){
			if(m == non_magnetic_items_in_box.length-1 ){
				items_list_box += ' '+ data.string.mm_and +' ' + non_magnetic_items_in_box[m];
			} else{
				items_list_box += ', ' + non_magnetic_items_in_box[m];
			}
		}
		for(var m = 1; m<non_magnetic_items_material.length; m++){
			if(m == non_magnetic_items_material.length-1 ){
				material_list_box += ' '+ data.string.mm_and +' ' + non_magnetic_items_material[m];
			} else{
				material_list_box += ', ' + non_magnetic_items_material[m];
			}
		}

		if(box.length == 0){
			text_feedback_1 = '';
			text_feedback_2 = data.string.empty_box;
		} else if(non_magnetic_items_in_box.length == 0){
			text_feedback_1 = data.string.mm_correct;
			text_feedback_2 = data.string.mag_1;
		} else if(non_magnetic_items_in_box.length == 1){
			text_feedback_1 = data.string.mm_the+' '  + items_list_box + " " + data.string.mag_2;
			text_feedback_2 = data.string.mag_3+' ' + material_list_box +' '+data.string.mag_4;
		} else {
			text_feedback_1 = data.string.mm_the+' '  + items_list_box + " "+data.string.mag_5;
			text_feedback_2 = data.string.mag_6+' ' + material_list_box +' '+data.string.mag_7;
		}
	}

	function magnetic_text(box){
		var magnetic_items_in_box = [];
		var magnetic_items_material = [];
		for(var m=0; m<box.length; m++){
			if($('.'+box[m]).hasClass('magnetic')){
				magnetic_items_in_box.push($('.'+box[m]).data('name'));
				magnetic_items_material.push($('.'+box[m]).data('material'));
			}
		}
		var items_list_box = magnetic_items_in_box[0];
		var material_list_box = magnetic_items_material[0];
		for(var m = 1; m<magnetic_items_in_box.length; m++){
			if(m == magnetic_items_in_box.length-1 ){
				items_list_box += ' '+ data.string.mm_and +' ' + magnetic_items_in_box[m];
				material_list_box += ' '+ data.string.mm_and +' ' + magnetic_items_material[m];
			} else{
				items_list_box += ', ' + magnetic_items_in_box[m];
				material_list_box += ', ' + magnetic_items_material[m];
			}
		}
		if(box.length == 0){
			text_feedback_1 = '';
			text_feedback_2 = data.string.empty_box;
		} else if(magnetic_items_in_box.length == 0){
			text_feedback_1 = data.string.mm_correct;
			text_feedback_2 = data.string.nmag_1;
		} else if(magnetic_items_in_box.length == 1){
			text_feedback_1 = data.string.mm_the+' '  + items_list_box + " " + data.string.nmag_2;
			text_feedback_2 = data.string.nmag_3+' ' + material_list_box +' '+data.string.nmag_4;
		} else {
			text_feedback_1 = data.string.mm_the+' '  + items_list_box + " "+data.string.nmag_5;
			text_feedback_2 = data.string.nmag_6+' ' + magnetic_items_material[0] +' '+data.string.nmag_4;//remove [0] for all material
		}
	}

	function after_laying(box){
		var m = box.length-1;
		$('.magnet').animate({
				'top': '35%',
			}, 500);
		$( ".magnet" ).promise().done(function() {
			$('.magnet').animate({
					'left': object_positions[m] + '%'
			}, 500, "linear");
			$( ".magnet" ).promise().done(function() {
				if($('.'+box[m]).hasClass('magnetic')){
					$('.'+box[m]).animate({
						'top': '50%'
					}, 500);
					attatch_to_magnet('.magnet', '.'+box[m], magnet_index);
					magnet_index++;
				} else {
					$('.'+box[m]).animate({
						'color': 'red'
					}, 500);
				}
				magnet_hover(m-1);
				object_mover(m-1, box);
			});
		});
	}

	function magnet_hover(m){
		if(m>=0){
			$( ".magnet" ).promise().done(function() {
				$('.magnet').animate({
					'left': object_positions[m] + '%'
					}, 1000);
				m--;
				magnet_hover(m);
			});
		}
	}
	function object_mover(m, box){
		if(m>=0){
			$('.'+box[m+1]).promise().done(function() {
				if($('.'+box[m]).hasClass('magnetic')){
					$('.'+box[m]).animate({
						'top': '50%'
					}, 1000);
					attatch_to_magnet('.magnet', '.'+box[m], magnet_index);
					magnet_index++;
				} else {
					$('.'+box[m]).animate({
						'color': 'red'
					}, 1000);
				}
				m--;
				object_mover(m, box);
			});
		} else{
			nav_button_controls(1200);
		}
	}


	function attatch_to_magnet(magnet_class, object_class, index){
			var left_positions = ['75%', '60%', '45%', '0%', '-10%', '-20%', '-30%'];
			var top_positions = ['65%', '75%'];
			$(object_class).promise().done(function() {
				$(object_class).detach().css({
	                "position": "absolute",
	                "cursor": 'auto',
	                "height": "54%",
	                "width": "57.7%",
	                "left" : left_positions[index],
	                "top": top_positions[index%2]
	            }).appendTo(magnet_class);
			});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			case 3:
			// $prevBtn.hide(0);
			// $nextBtn.hide(0);
				if(!go_next){
					$('.feedback_1').html(text_feedback_1);
					$('.feedback_2').html(text_feedback_2);
					$('.feedback').show(0);
					go_next = 1;
					// sound_player_nav("sound_3b");
				} else {
					countNext++;
					templateCaller();
				}
				break;
			case 5:
				if(!go_next){
					$('.feedback_1').html(text_feedback_1);
					$('.feedback_2').html(text_feedback_2);
					$('.feedback').show(0);
					go_next = 1;
				} else {
					countNext++;
					templateCaller();
				}
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(global_time);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

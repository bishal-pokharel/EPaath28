var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound(soundAsset+"s7_p1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s7_p2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s7_p3.ogg");
var sound_4 = new buzz.sound(soundAsset+"s7_p4.ogg");
var sound_5 = new buzz.sound(soundAsset+"s7_p5.ogg");
var sound_6 = new buzz.sound(soundAsset+"s7_p6.ogg");
var sound_7 = new buzz.sound(soundAsset+"s7_p7.ogg");
var sound_8 = new buzz.sound(soundAsset+"s7_p8.ogg");
var sound_9 = new buzz.sound(soundAsset+"s7_p9.ogg");
var sound_10 = new buzz.sound(soundAsset+"s7_p10.ogg");
var sound_11 = new buzz.sound(soundAsset+"s7_p11.ogg");
var sound_13 = new buzz.sound(soundAsset+"s7_p13.ogg");

var content=[
	{
		//starting page
		contentblockadditionalclass: "contentwithbg2",
		uppertextblock : [
		{
			textclass : "firsttitle",
			textdata : data.string.p7text1
		}
		]
	},
	{
		//page1
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.p7text1
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "sevenimg",
				imgsrc : imgpath + "use/purse.jpg"
			},
			],
		}
		],
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "usetext",
				textdata : data.string.p7text2
			}
		]
	},
	{
		//page2
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.p7text1
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "sevenimg",
				imgsrc : imgpath + "use/door.jpg"
			},
			],
		}
		],
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "usetext",
				textdata : data.string.p7text3
			}
		]
	},
	{
		//page3
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.p7text1
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "sevenimg",
				imgsrc : imgpath + "use/hanging01.jpg"
			},
			],
		}
		],
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "usetext",
				textdata : data.string.p7text4
			}
		]
	},
	{
		//page4
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.p7text1
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "sevenimg",
				imgsrc : imgpath + "use/shirt01.jpg"
			},
			],
		}
		],
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "usetext",
				textdata : data.string.p7text5
			}
		]
	},
	{
		//page5
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.p7text1
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "sevenimg",
				imgsrc : imgpath + "use/garbage02.jpg"
			},
			],
		}
		],
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "usetext",
				textdata : data.string.p7text6
			}
		]
	},
	{
		//page6
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.p7text1
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "sevenimg",
				imgsrc : imgpath + "use/compass.jpg"
			},
			],
		}
		],
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "usetext",
				textdata : data.string.p7text7
			}
		]
	},
	{
		//page7
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.p7text1
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "multipleimg imgone",
				imgsrc : imgpath + "use/computer.png"
			},
			{
				imgclass : "multipleimg imgotwo",
				imgsrc : imgpath + "use/fan.png"
			},
			{
				imgclass : "multipleimg imgthree",
				imgsrc : imgpath + "use/fridge.png"
			},
			{
				imgclass : "multipleimg imgfour",
				imgsrc : imgpath + "use/iron.png"
			},
			{
				imgclass : "multipleimg imgfive",
				imgsrc : imgpath + "use/mobile.png"
			},
			{
				imgclass : "multipleimg imgsix",
				imgsrc : imgpath + "use/speaker.png"
			},
			{
				imgclass : "multipleimg imgsev",
				imgsrc : imgpath + "use/vacume-cleaner.png"
			},
			{
				imgclass : "multipleimg imgeight",
				imgsrc : imgpath + "use/watch.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				textclass : "utbtext",
				textdata : data.string.p7text8
			}
		]
	},
	{
		//page8
		contentblockadditionalclass: "contentwithbg",
		uppertextblock : [
		{
			textclass : "firsttitle",
			textdata : data.string.p7text9
		}
		]
	},
	{
		//page9
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.dyk
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "multipleimg dyk1",
				imgsrc : imgpath + "use/magnet01.png"
			},
			{
				imgclass : "multipleimg dyk2",
				imgsrc : imgpath + "use/magnet02.png"
			},
			{
				imgclass : "multipleimg dyk3",
				imgsrc : imgpath + "use/magnet03.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "utbtext",
				textdata : data.string.p7text10
			}
		]
	},
	{
		//page10
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.dyk
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "magnet1",
				imgsrc : imgpath + "use/brokenmagnet01.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "utbtext",
				textdata : data.string.p7text11
			}
		]
	},
	{
		//page10
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.dyk
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "magnet1",
				imgsrc : imgpath + "use/brokenmagnet01.png"
			},
			{
				imgclass : "magnet2",
				imgsrc : imgpath + "use/brokenmagnet02.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "utbtext",
				textdata : data.string.p7text11
			}
		]
	},
	{
		//page11
		contentblockadditionalclass: "contentwithbg",
		headerblock : [
		{
			textclass : "",
			textdata : data.string.dyk
		}
		],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "magnet1",
				imgsrc : imgpath + "use/brokenmagnet01.png"
			},
			{
				imgclass : "magnet2",
				imgsrc : imgpath + "use/brokenmagnet02.png"
			},
			{
				imgclass : "magnet2 cssfadein",
				imgsrc : imgpath + "use/brokenmagnet03.png"
			},
			],
		}
		],
		uppertextblock : [
			{
				textclass : "utbtext",
				textdata : data.string.p7text11
			},
			{
				textclass : "usetext",
				textdata : data.string.p7text12
			}
		]
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

  	var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		console.log(countNext);
	  if(countNext!=11)
		{
			soundplayer(eval(`sound_${countNext+1}`));
		}
		if(countNext==11) navigationcontroller();

	}

	function soundplayer(i, next){
		buzz.all().stop();
		i.play().bind("ended",()=>{
			if(!next) navigationcontroller();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		// navigationcontroller();

		generalTemplate();



	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

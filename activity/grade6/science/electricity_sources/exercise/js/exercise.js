var imgpath = $ref + "/images/exercise/";

var no_of_draggable = 4; //no of draggable to display at a time1
// Array.prototype.shufflearray = function() {
//     var i = this.length,
//         j, temp;
//     while (--i > 0) {
//         j = Math.floor(Math.random() * (i + 1));
//         temp = this[j];
//         this[j] = this[i];
//         this[i] = temp;
//     }
//     return this;
// };

var content = [
    //slide 0
    {
        contentblockadditionalclass: "brown_bg",
        contentnocenteradjust: true,
        textblockadditionalclass: 'instruction',
        textblock: [{
            textdata: data.string.ex1text1,
            textclass: 'ques'
        }],
        // draggableblockadditionalclass: 'frac_ques',
        draggableblock: [{
            draggables: [{
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "cell_2.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "dynamo.gif"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "solarpanel.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "lemon.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "torch.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "xo.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "bulb.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "allimg",
                imgsrc: imgpath + "mobile.png"
            }]
        }],
        droppableblock: [{
            droppables: [{
                headerdata: data.string.ex1text2,
                droppablecontainerclass: "",
                droppableclass: "drop_class_1",
                imgclass: "",

            }, {
                headerdata: data.string.ex1text3,
                droppablecontainerclass: "",
                droppableclass: "drop_class_2",
                imgclass: "",

            }]
        }]
    },

    //slide1
    {
      contentblockadditionalclass: "brown_bg",
      textblock: [{
          textdata: data.string.ex1text4,
          textclass: 'ques'
      }],
      images:[{
        imgclass: "img_center",
        imgsrc: imgpath + "cell.png"
      }],
      uppertextblock:[{
          textdata: '+',
          textclass: 'plus1'
      },{
          textdata: '-',
          textclass: 'minus1'
      },{
          textdata: '+',
          textclass: 'plus2'
      },{
          textdata: '-',
          textclass: 'minus2'
      },]
    },

    //slide2
    {
      contentblockadditionalclass: "brown_bg",
      textblock: [{
          textdata: data.string.ex1text5,
          textclass: 'ques'
      }],
      images:[{
        imgclass: "lalan",
        imgsrc: imgpath + "lalan.png"
      },{
        imgclass: "mobile_charging",
        imgsrc: imgpath + "mobile_charger.png"
      }],
      uppertextblock:[{
          textdata: data.string.ex1text6,
          textclass: 'desc_below_top'
      },{
          textdata: data.string.ex1text7,
          textclass: 'question'
      },{
          textdata: data.string.ex1text8,
          textclass: 'option1 correct'
      },{
          textdata:data.string.ex1text9,
          textclass: 'option2'
      },]
    },


    //slide3
    {
      contentblockadditionalclass: "brown_bg",
      images:[{
        imgclass: "lalan",
        imgsrc: imgpath + "lalan.png"
      },{
        imgclass: "lalan_image_1",
        imgsrc: imgpath + "mobile_charger03.png"
      },{
        imgclass: "lalan_image_2",
        imgsrc: imgpath + "mobile_charger.png"
      },{
        imgclass: "lalan_image_3",
        imgsrc: imgpath + "mobile_charger01.png"
      },{
        imgclass: "lalan_image_4",
        imgsrc: imgpath + "mobile_charger02.png"
      }],
      uppertextblock:[{
          textdata: data.string.ex1text10,
          textclass: 'top_text'
      },{
          textdata: data.string.ex1text11,
          textclass: 'lalan_text_1'
      },{
          textdata: data.string.ex1text12,
          textclass: 'lalan_text_2'
      },{
          textdata:data.string.ex1text13,
          textclass: 'lalan_text_3'
      },{
          textdata:data.string.ex1text14,
          textclass: 'lalan_text_4'
      }]
    },


    //slide4
    {
      contentblockadditionalclass: "brown_bg",
      images:[{
        imgclass: "left_image",
        imgsrc: imgpath + "img01.png"
      }],
      textblock: [{
          textdata: data.string.ex1text15,
          textclass: 'ques'
      }],
      uppertextblock:[{
          textdata: data.string.ex1text16,
          textclass: 'desc_below_top'
      },{
          textdata: data.string.ex1text17,
          textclass: 'question_right'
      },{
          textdata:data.string.ex1text18,
          textclass: 'option_m_1 correct'
      },{
          textdata:data.string.ex1text19,
          textclass: 'option_m_2'
      },{
          textdata:data.string.ex1text20,
          textclass: 'option_m_3'
      }]
    },

    //slide5
    {
      contentblockadditionalclass: "brown_bg",
      images:[{
        imgclass: "left_image_2",
        imgsrc: imgpath + "img02.png"
      }],
      uppertextblock:[{
          textdata: data.string.ex1text21,
          textclass: 'right_text'
      }]
    },

    //slide6
    {
      contentblockadditionalclass: "brown_bg",
      textblock: [{
          textdata: data.string.ex1text22,
          textclass: 'ques'
      }],
      images:[{
        imgclass: "left_image_2",
        imgsrc: imgpath + "img03.png"
      },{
        imgclass: "hint_image",
        imgsrc: imgpath + "dam01.png"
      },{
        imgclass: "dialogue_box",
        imgsrc: imgpath + "speechbubble.png"
      }],
      uppertextblock:[{
          textdata: data.string.ex1text24,
          textclass: 'option_m_1 option_slide_6_1 correct'
      },
      {
          textdata: data.string.ex1text25,
          textclass: 'option_m_2 option_slide_6_2'
      },
      {
          textdata: data.string.ex1text26,
          textclass: 'hint'
      },
      {
          textdata: data.string.ex1text23,
          textclass: 'dialogue_text'
      }]
    },

    //slide7
    {
      contentblockadditionalclass: "brown_bg",
      textblock: [{
          textdata: data.string.ex1text22,
          textclass: 'ques'
      }],
      images:[{
        imgclass: "left_image_2",
        imgsrc: imgpath + "img03.png"
      },{
        imgclass: "hint_image",
        imgsrc: imgpath + "dam01.png"
      },{
        imgclass: "dialogue_box",
        imgsrc: imgpath + "speechbubble.png"
      }],
      uppertextblock:[{
          textdata: data.string.ex1text29,
          textclass: 'option_m_1 option_slide_7_1 '
      },
      {
          textdata: data.string.ex1text28,
          textclass: 'option_m_2 option_slide_7_2 correct'
      },
      {
          textdata: data.string.ex1text30,
          textclass: 'option_m_3 option_slide_7_3'
      },
      {
          textdata: data.string.ex1text31,
          textclass: 'option_m_4 option_slide_7_4'
      },
      {
          textdata: data.string.ex1text26,
          textclass: 'hint'
      },
      {
          textdata: data.string.ex1text27,
          textclass: 'dialogue_text'
      }]
    },

      //slide8
    {
      contentblockadditionalclass: "brown_bg",
      textblock: [{
          textdata: data.string.ex1text22,
          textclass: 'ques'
      }],
      images:[{
        imgclass: "left_image_2",
        imgsrc: imgpath + "img03.png"
      },{
        imgclass: "hint_image",
        imgsrc: imgpath + "img05.png"
      },{
        imgclass: "dialogue_box",
        imgsrc: imgpath + "speechbubble.png"
      }],
      uppertextblock:[{
          textdata: data.string.ex1text34,
          textclass: 'option_m_1 option_slide_8_1 '
      },
      {
          textdata: data.string.ex1text33,
          textclass: 'option_m_2 option_slide_8_2 correct'
      },
      {
          textdata: data.string.ex1text26,
          textclass: 'hint'
      },
      {
          textdata: data.string.ex1text32,
          textclass: 'dialogue_text'
      }]
    },
    //slide9
  {
    contentblockadditionalclass: "brown_bg",
    images:[{
      imgclass: "bg_full",
      imgsrc: imgpath + "bg03.png"
    },{
      imgclass: "dialogue_box",
      imgsrc: imgpath + "speechbubble.png"
    }],
    uppertextblock:[
    {
        textdata: data.string.ex1text35,
        textclass: 'dialogue_text'
    }]
  }


];

var dummy_class = {
    draggableclass: "dummy_class hidden",
    imgclass: "",
    imgsrc: ""
};

/* Suffle content elements for draggable
 * add some dummy class so that flex behaves correctly
 * add position for first fixed no of draggables
 */
content[0].draggableblock[0].draggables.shufflearray();
for (var i = 1; i < no_of_draggable + 1; i++) {
    var asd = content[0].draggableblock[0].draggables[i - 1].draggableclass.split('"')[0].split('hidden');
    content[0].draggableblock[0].draggables[i - 1].draggableclass = asd[0] + 'position_' + i;
    content[0].draggableblock[0].draggables.push(dummy_class);
}


$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var no_egg = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);
    var numbertemplate = new NumberTemplate();
    numbertemplate.init($total_page-3);

    var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();
    Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
    Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());

    function navigationcontroller(islastpageflag) {
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
            alert("NavigationController : Hi Master, please provide a boolean parameter") :
            null;
    }

    var score = 0;
    /*random scoreboard eggs*/
    var wrngClicked = [false, false, false, false, false, false, false, false];


    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        numbertemplate.numberOfQuestions($total_page);

        $nextBtn.hide(0);
        $prevBtn.hide(0);
        var index = 0;
        ole.footerNotificationHandler.hideNotification();
        // $('#activity-page-total-slide').text('10');
        /*generate question no at the beginning of question*/
        switch (countNext) {
            case 0:
            $('.ques').css({'padding-right': '0'});
              $(".draggable").draggable({
                containment: "body",
                revert: "true",
                appendTo: "body",
                helper: "clone",
                zindex: 1000,
                start: function(event, ui) {
                    $(this).css({
                        "opacity": "0"
                    });
                    $(ui.helper).addClass("ui-draggable-helper");
                    $(ui.helper).removeClass("sliding");
                },
                stop: function(event, ui) {
                    $(this).css({
                        "opacity": "1"
                    });
                }
            });
            $('.drop_class_1').droppable({
                // accept : ".class_1",
                hoverClass: "hovered",
                drop: function(event, ui) {
                  if (ui.draggable.hasClass("class_1")) {
                      index++;
                      play_correct_incorrect_sound(1);
                      if (wrngClicked[index] == false) {
                        if(($(ui.draggable).data("dropped"))==false){
                          wrngClicked[index] = true;
                      }
                      }
                      handleCardDrop(event, ui, ".class_1", ".drop_class_1");
                  } else {
                      play_correct_incorrect_sound(0);
                      if(($(ui.draggable).data("dropped"))==false){
                        wrngClicked[index] = false;
                  }
                  }
                    // if(($(ui.draggable).data("dropped"))==false){
                    //     $(ui.draggable).data("dropped",true);
                    //     index++;
                    // }
                    console.log(wrngClicked);
                    if (index == 8){
                      navigationcontroller();
                        $('.draggableblock').hide(0);
                        if(wrngClicked[0] == true && wrngClicked[1] == true && wrngClicked[2] == true && wrngClicked[3] == true && wrngClicked[4] == true && wrngClicked[5] == true && wrngClicked[6] == true && wrngClicked[7] == true){
                          numbertemplate.update(true)
                        }
                        if(wrngClicked[0] == false || wrngClicked[1] == false || wrngClicked[2] == false || wrngClicked[3] == false || wrngClicked[4] == false || wrngClicked[5] == false || wrngClicked[6] == false || wrngClicked[7] == false){
                          numbertemplate.update(false)
                        }
                    }
                }
            });

            $('.drop_class_2').droppable({
              hoverClass: "hovered",
                drop: function(event, ui) {
                    if (ui.draggable.hasClass("class_2")) {
                        play_correct_incorrect_sound(1);
                        index++;
                        if (wrngClicked[index] == false) {
                          if(($(ui.draggable).data("dropped"))==false){
                            wrngClicked[index] = true;
                        }
                        }
                        handleCardDrop(event, ui, ".class_2", ".drop_class_2");
                    } else {
                        play_correct_incorrect_sound(0);
                        if(($(ui.draggable).data("dropped"))==false){
                          wrngClicked[index] = false;
                    }
                    }
                    // if(($(ui.draggable).data("dropped"))==false){
                    //     $(ui.draggable).data("dropped",true);
                    //     index++;
                    // }
                    console.log(wrngClicked);

                    if (index == 8){
                        navigationcontroller();
                        $('.draggableblock').hide(0);
                        if(wrngClicked[0] == true && wrngClicked[1] == true && wrngClicked[2] == true && wrngClicked[3] == true && wrngClicked[4] == true && wrngClicked[5] == true && wrngClicked[6] == true && wrngClicked[7] == true){
                          numbertemplate.update(true)
                        }
                        if(wrngClicked[0] == false || wrngClicked[1] == false || wrngClicked[2] == false || wrngClicked[3] == false || wrngClicked[4] == false || wrngClicked[5] == false || wrngClicked[6] == false || wrngClicked[7] == false){
                          numbertemplate.update(false)
                        }
                    }

                }
            });

            function topLeftCalculator(count) {
                var top = count % 3;
                var factor = Math.floor(count / 3);
                var height = 0;
                var left = 0;
                switch (top) {
                    case 0:
                        height = 20;
                        left = (factor > 0) ? ((factor * 5) + 20) : 40;
                        break;
                    case 1:
                        height = 30;
                        left = (factor > 0) ? ((factor * 7) + 30) : 50;
                        break;
                    case 2:
                        height = 40;
                        left = (factor > 0) ? ((factor * 9) + 40) : 60;
                        break;
                }

                var returnNumber = [height, left];
                return returnNumber;

            }

            function handleCardDrop(event, ui, classname, droppedOn) {
                ui.draggable.draggable('disable');
                var dropped = ui.draggable;
                // var to count no. of divs in the droppable div
                var drop_index = $(droppedOn + ">div").length;
                var top_position = drop_index;
                // var lef_position = drop_index * 32;
                $(ui.draggable).removeClass("sliding");
                $(ui.draggable).detach().css({
                    "position": "relative",
                    "cursor": 'auto',
                    "flex": "0 0 40%",
                    "height": "45%",
                    "border":"none"
                }).appendTo(droppedOn);
                var $newEntry = $(".draggableblock> .hidden").eq(0);

                var $draggable3;
                var $draggable2;
                var $draggable1;
                if (dropped.hasClass("position_4")) {
                    dropped.removeClass("position_4");
                    $draggable3 = $(".position_3");
                    $draggable2 = $(".position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_3")) {
                    dropped.removeClass("position_3");
                    $draggable2 = $(".position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_2")) {
                    dropped.removeClass("position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_1")) {
                    dropped.removeClass("position_1");
                }

                if ($draggable3 != null) {
                    $draggable3.removeClass("position_3").addClass("position_4");
                    $draggable3.removeClass('sliding');
                    setTimeout(function() {
                        $draggable3.addClass('sliding');
                    }, 1);
                }
                if ($draggable2 != null) {
                    $draggable2.removeClass("position_2").addClass("position_3");
                    $draggable2.removeClass('sliding');
                    setTimeout(function() {
                        $draggable2.addClass('sliding');
                    }, 1);
                }
                if ($draggable1 != null) {
                    $draggable1.removeClass("position_1").addClass("position_2");
                    $draggable1.removeClass('sliding');
                    setTimeout(function() {
                        $draggable1.addClass('sliding');
                    }, 1);
                }
                if ($newEntry != null) {
                    $newEntry.removeClass("hidden").addClass("position_1");
                }
                // egg_count = false;
            }
            break;

            case 1:
            var truecount =0;
            var nextbuttoncount =0;
            $('.plus1').click(function(){
              play_correct_incorrect_sound(1);
              $(this).css({'background':'#98c02e','border-color':'#deef3c','color':'white'});
              $('.plus1,.minus1').css('pointer-events','none');
              $('.minus1').fadeOut(500);
              setTimeout(function(){
                $('.plus1').animate({'top':'45.5%'},500);
              },200);
              truecount++;
              nextbuttoncount++;
              if(nextbuttoncount==2){
                navigationcontroller();
              }
              if(truecount==2){
                numbertemplate.update(true)
              }
              else{
                numbertemplate.update(false)
              }
            });
            $('.minus2').click(function(){
              play_correct_incorrect_sound(1);
              $(this).css({'background':'#98c02e','border-color':'#deef3c','color':'white'});
              $('.plus2,.minus2').css('pointer-events','none');
              $('.plus2').fadeOut(500);
              setTimeout(function(){
                $('.minus2').animate({'top':'45.5%'},500);
              },200);
              truecount++;
              nextbuttoncount++;
              if(nextbuttoncount==2){
                navigationcontroller();
              }
              if(truecount==2){
                numbertemplate.update(true);
              }
              else{
                numbertemplate.update(false);
              }
            });
            $('.minus1').click(function(){
              play_correct_incorrect_sound(0);
              $(this).css({'background':'#ff0000','border-color':'#980000','pointer-events':'none'});
              truecount--;
            });
            $('.plus2').click(function(){
              play_correct_incorrect_sound(0);
              $(this).css({'background':'#ff0000','border-color':'#980000','pointer-events':'none'});
              truecount--;
            });
            break;

            case 2:
            var top_val = (($('.ques').height()/$('.board').height() )* 100) +3+"%";
            var clickwrong =0;
        		console.log(top_val);
        		$('.desc_below_top').css({
        			'top':top_val
        		});
            $('.option1,.option2').click(function(){
        			if($(this).hasClass('correct')){
        				var $this = $(this);
        				var position = $this.position();
        				var width = $this.width();
        				var height = $this.height();
        				var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
        				var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
        				$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(1032%,39%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
        				$(this).css({'background':'#92AF3B','border-color':'#deef3c'});
        				$('.option1,.option2').css({'pointer-events':'none'})
        				play_correct_incorrect_sound(1);
                if(clickwrong==0){
                  numbertemplate.update(true);
                }

        				$nextBtn.show(0);
        			}
        			else{
                clickwrong++;
                numbertemplate.update(false);
        				var $this = $(this);
        				var position = $this.position();
        				var width = $this.width();
        				var height = $this.height();
        				var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
        				var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
        				$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(1032%,39%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
        				$(this).css({'background':'#ff0000','pointer-events':'none','border-color':'#980000','color':'black'});
        				play_correct_incorrect_sound(0);

        			}
        		});
            break;

            case 3:
            $('.ex-number-template-score').hide(0);
            navigationcontroller();
            break;

            case 4:
            var wrongclick=0;
            $('.ex-number-template-score').show(0);
            var top_val = (($('.ques').height()/$('.board').height() )* 100) +3+"%";
            console.log(top_val);
            $('.desc_below_top').css({
              'top':top_val
            });

            $('.option_m_1,.option_m_2,.option_m_3').click(function(){
              if($(this).hasClass('correct')){
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(642%,39%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
                $(this).css({'background':'#92AF3B','border-color':'#deef3c'});
                $('.option_m_1,.option_m_2,.option_m_3').css({'pointer-events':'none'})
                play_correct_incorrect_sound(1);
                if(wrongclick==0){
                  numbertemplate.update(true);
                }
                $nextBtn.show(0);
              }
              else{
                wrongclick++;
                numbertemplate.update(false);
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(642%,39%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
                $(this).css({'background':'#ff0000','pointer-events':'none','border-color':'#980000','color':'black'});
                play_correct_incorrect_sound(0);

              }
            });
            break;

            case 5:
            $('.ex-number-template-score').hide(0);
            navigationcontroller();
            break;


            case 6:
            var clickwrong=0;
            $('.ex-number-template-score').show(0);
            $('.hint_image').hide(0);
            $('.hint').mouseenter(function(){
              $('.hint_image').show(200);
              $('.left_image_2,.dialogue_box,.dialogue_text').animate({'opacity':'.5'});
            });

            $('.hint').mouseleave(function(){
              $('.hint_image').hide(200);
              $('.left_image_2,.dialogue_box,.dialogue_text').animate({'opacity':'1'});
            });

            $('.option_m_1,.option_m_2,.option_m_3,.option_m_4').click(function(){
              if($(this).hasClass('correct')){
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(302%,39%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
                $(this).css({'background':'#92AF3B','border-color':'#deef3c'});
                $('.option_m_1,.option_m_2').css({'pointer-events':'none'})
                play_correct_incorrect_sound(1);
                if(clickwrong==0){
                  numbertemplate.update(true);
                }

                $nextBtn.show(0);
              }
              else{
                clickwrong++;
                numbertemplate.update(false);
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(302%,39%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
                $(this).css({'background':'#ff0000','pointer-events':'none','border-color':'#980000','color':'black'});
                play_correct_incorrect_sound(0);

              }
            });
            break;

            case 7:
            var clickwrong=0;
            $('.ex-number-template-score').show(0);
            $('.hint_image').hide(0);
            $('.hint').mouseenter(function(){
              $('.hint_image').show(200);
              $('.left_image_2,.dialogue_box,.dialogue_text').animate({'opacity':'.5'});
            });

            $('.hint').mouseleave(function(){
              $('.hint_image').hide(200);
              $('.left_image_2,.dialogue_box,.dialogue_text').animate({'opacity':'1'});
            });

            $('.option_m_1,.option_m_2,.option_m_3,.option_m_4').click(function(){
              if($(this).hasClass('correct')){
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-8%,-107%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
                $(this).css({'background':'#92AF3B','border-color':'#deef3c'});
                $('.option_m_1,.option_m_2,.option_m_3,.option_m_4').css({'pointer-events':'none'})
                play_correct_incorrect_sound(1);
                if(clickwrong==0){
                  numbertemplate.update(true);
                }
                $nextBtn.show(0);
              }
              else{
                clickwrong++;
                numbertemplate.update(false);
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-8%,-107%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
                $(this).css({'background':'#ff0000','pointer-events':'none','border-color':'#980000','color':'black'});
                play_correct_incorrect_sound(0);

              }
            });
            break;

            case 8:
            $('.ex-number-template-score').show(0);
            var clickwrong=0;
            $('.hint_image').hide(0);
            $('.hint').mouseenter(function(){
              $('.hint_image').show(200);
              $('.left_image_2,.dialogue_box,.dialogue_text').animate({'opacity':'.5'});
            });

            $('.hint').mouseleave(function(){
              $('.hint_image').hide(200);
              $('.left_image_2,.dialogue_box,.dialogue_text').animate({'opacity':'1'});
            });
            $('.option_m_1,.option_m_2').click(function(){
              if($(this).hasClass('correct')){
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(12%,-501%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
                $(this).css({'background':'#92AF3B','border-color':'#deef3c'});
                $('.option_m_1,.option_m_2').css({'pointer-events':'none'})
                play_correct_incorrect_sound(1);
                if(clickwrong==0){
                  numbertemplate.update(true);
                }

                $nextBtn.show(0);
              }
              else{
                clickwrong++;
                numbertemplate.update(false);
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(12%,-411%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
                $(this).css({'background':'#ff0000','pointer-events':'none','border-color':'#980000','color':'black'});
                play_correct_incorrect_sound(0);

              }
            });
            break;

            case 3:
            $('.ex-number-template-score').hide(0);
            navigationcontroller();
            break;

            case 4:
            var wrongclick=0;
            $('.ex-number-template-score').show(0);
            var top_val = (($('.ques').height()/$('.board').height() )* 100) +3+"%";
            console.log(top_val);
            $('.desc_below_top').css({
              'top':top_val
            });

            $('.option_m_1,.option_m_2,.option_m_3').click(function(){
              if($(this).hasClass('correct')){
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(642%,39%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
                $(this).css({'background':'#92AF3B','border-color':'#deef3c'});
                $('.option_m_1,.option_m_2,.option_m_3').css({'pointer-events':'none'})
                play_correct_incorrect_sound(1);
                if(clickwrong==0){
                  numbertemplate.update(true);
                }
                $nextBtn.show(0);
              }
              else{
                clickwrong++;
                numbertemplate.update(false);
                var $this = $(this);
                var position = $this.position();
                var width = $this.width();
                var height = $this.height();
                var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
                var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
                $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(642%,39%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
                $(this).css({'background':'#ff0000','pointer-events':'none','border-color':'#980000','color':'black'});
                play_correct_incorrect_sound(0);

              }
            });
            break;

            case 9:
            $('.ex-number-template-score').hide(0);
            $nextBtn.show(0);
            break;

        }
    }


    function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();
        loadTimelineProgress($total_page,countNext+1);

    }

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */
	function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

    $nextBtn.on("click", function() {
        countNext++;
        if(countNext==3 || countNext==5 || countNext==9){
        }
        else{
          numbertemplate.gotoNext();
        }
        if(countNext==10){
          $('#activity-page-highlight-current').html('10');
        }
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
  	});

	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        	previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
{
	contentblockadditionalclass:"blue_bg",
		imageblock:[{
			imagestoshow:[{
				imgid:'girl_talking',
				imgclass:'girl_talking'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box',
			textdata:data.string.p3text1,
			textclass:'inside_text'
		}]
},

// slide1
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'battery1',
			imgclass:'mid_image fadesin'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p3text3,
		textclass:'text_desc'
	},{
		textdata:data.string.p3text4,
		textclass:'mid_desc fadesin'
	},{
		textdata:data.string.p3text2,
		textclass:'top_text'
	}]

},

// slide2
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'torch',
			imgclass:'mid_image fadesin'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p3text5,
		textclass:'text_desc'
	},{
		textdata:data.string.p3text6,
		textclass:'mid_desc fadesin'
	},{
		textdata:data.string.p3text2,
		textclass:'top_text'
	}]

},

// slide3
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'torch_gif',
			imgclass:'mid_left_image fadesin'
		},
		{
			imgid:'mobile_and_cell',
			imgclass:'fadesin mid_right_image '
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p3text5,
		textclass:'text_desc'
	},{
		textdata:data.string.p3text8,
		textclass:'mid_left_desc fadesin'
	},{
		textdata:data.string.p3text9,
		textclass:'mid_right_desc fadesin'
	},{
		textdata:data.string.p3text2,
		textclass:'top_text'
	}]

},

// slide4
{
contentblockadditionalclass:"cream",
	uppertextblock:[{
		textdata:data.string.p3text35,
		textclass:'top_text'
	}],
	imgclass1:'image_left',
	imgclass2:'image_right',
	imgclass3:'arrow1',
	imgclass4:'arrow2',
	textclass1:'text_up',
	textdata1:data.string.p3text10,
	textclass2:'desc_text',
	textdata2:data.string.p3text12,
	textclass3:'cell_class',
	textdata3:data.string.p3text11,
	textclass4:'battery_class',
	textdata4:data.string.p3text2,
	slider_div:[{
	}]

},

//slide5

{
	contentblockadditionalclass:"cream",
	uppertextblock:[{
		textdata:data.string.p3text16,
		textclass:'top_label fadesin'
	},{
		textdata:data.string.p3text17,
		textclass:'bot_label fadesin'
	},{
		textdata:data.string.p3text15,
		textclass:'top_text'
	},{
		textdata:data.string.p3text18,
		textclass:'bot_text'
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'cell1',
			imgclass:'center_image fadesin'
		}]
	}],
},

//slide6

{
	contentblockadditionalclass:"cream",
	uppertextblock:[{
		textdata:data.string.p3text15,
		textclass:'top_text'
	},{
		textdata:data.string.p3text19,
		textclass:'bot_text'
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'circuit',
			imgclass:'center_image fadesin'
		}]
	}],
},
//slide7
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'mobile_and_cell',
			imgclass:'mid_left_image_up fadesin'
		},
		{
			imgid:'mobile_charging',
			imgclass:'fadesin mid_right_image_up '
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p3text21,
		textclass:'mid_left_desc_up fadesin'
	},{
		textdata:data.string.p3text22,
		textclass:'mid_right_desc_up fadesin'
	},{
		textdata:data.string.p3text20,
		textclass:'top_text'
	},{
		textdata:data.string.p3text23,
		textclass:'bot_text'
	}]

},

//slide8

{
	contentblockadditionalclass:"cream",
	uppertextblock:[{
		textdata:data.string.p3text24,
		textclass:'top_text'
	},{
		textdata:data.string.p3text26,
		textclass:'bot_text'
	},{
		textdata:data.string.p3text16,
		textclass:'battery_label_1'
	},{
		textdata:data.string.p3text25,
		textclass:'battery_label_2'
	},{
		textdata:data.string.p3text17,
		textclass:'battery_label_3'
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'lebel_battery',
			imgclass:'center_image fadesin'
		},{
			imgclass:'fadesin battery_arrow_1 ',
			imgid:'arrow_black'
		},{
			imgclass:'fadesin battery_arrow_2 ',
			imgid:'arrow_black'
		},{
			imgclass:'fadesin battery_arrow_3 ',
			imgid:'arrow_black'
		}]
	}],
},

//slide9
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'lebel_battery',
			imgclass:'mid_left_image_up fadesin'
		},
		{
			imgid:'lemon_cell',
			imgclass:'fadesin mid_right_image_up '
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p3text27,
		textclass:'mid_left_desc_up fadesin'
	},{
		textdata:data.string.p3text28,
		textclass:'mid_right_desc_up fadesin'
	},{
		textdata:data.string.p3text30,
		textclass:'top_text'
	},{
		textdata:data.string.p3text29,
		textclass:'bot_text'
	}]

},
//
// //slide10
//
// {
// 	contentblockadditionalclass:"cream",
// 	uppertextblock:[{
// 		textdata:data.string.p3text31,
// 		textclass:'top_text'
// 	},{
// 		textdata:data.string.p3text34,
// 		textclass:'bot_text'
// 	},{
// 		textdata:data.string.p3text32,
// 		textclass:'diy_title'
// 	},{
// 		textdata:data.string.p3text33,
// 		textclass:'diy_desc',
// 		datahighlightflag:true,
// 		datahighlightcustomclass:'science_project'
// 	}],
// 	imageblock:[{
// 		imagestoshow:[{
// 			imgid:'fig_a_lemon_battery_with_bulb',
// 			imgclass:'center_image fadesin'
// 		}]
// 	}],
// },
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 4;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "text_box", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cell", src: imgpath+"battery01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch", src: imgpath+"cell.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mobile_and_cell", src: imgpath+"mobile_and_cell.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_talking", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "girl_talking1", src: imgpath+"girl_talking.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mobile_and_cell", src: imgpath+"mobile_and_cell.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch_gif", src: imgpath+"torchopen_inside_current_flow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cell1", src: imgpath+"battery01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "battery1", src: imgpath+"battery.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cell2", src: imgpath+"battery01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "battery2", src: imgpath+"battery_inside.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cell3", src: imgpath+"battery_cell_highlight.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "circuit", src: imgpath+"circuit.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "battery3", src: imgpath+"battery_cover.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rightbutton", src: imgpath+"right_btn.png", type: createjs.AbstractLoader.IMAGE},
			{id: "leftbutton", src: imgpath+"left_btn.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mobile_charging", src: imgpath+"mobile_charging.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_black", src: imgpath+"arrow1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lebel_battery", src: imgpath+"lebel_battery.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lemon_cell", src: imgpath+"killacoin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fig_a_lemon_battery_with_bulb", src: imgpath+"fig_a_lemon_battery_with_bulb.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"S3_P1.ogg"},
			{id: "sound_2", src: soundAsset+"S3_P2.ogg"},
			{id: "sound_3", src: soundAsset+"S3_P3.ogg"},
			{id: "sound_5_1", src: soundAsset+"S3_P5_1.ogg"},
			{id: "sound_5_2", src: soundAsset+"S3_P5_2.ogg"},
			{id: "sound_5_3", src: soundAsset+"S3_P5_3.ogg"},
			{id: "sound_7", src: soundAsset+"S3_P6.ogg"},
			{id: "sound_8", src: soundAsset+"S3_P7.ogg"},
			{id: "sound_9", src: soundAsset+"S3_P8.ogg"},
			{id: "sound_10", src: soundAsset+"S3_P9.ogg"},
			{id: "sound_11", src: soundAsset+"S3_P10.ogg"},
			{id: "sound_12", src: soundAsset+"S3_P11.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			case 1:
			case 2:
			sound_player('sound_'+(countNext+1));
			break;
			case 4:
			sound_player_intrnl_sld('sound_5_1',0,1,0);
			$('.image_left').attr('src',preload.getResult('cell1').src);
			$('.image_right').attr('src',preload.getResult('battery1').src);
			$('.arrow1').attr('src',preload.getResult('arrow').src);
			$('.arrow2').attr('src',preload.getResult('arrow').src);
			$('.prev_button').attr('src',preload.getResult('leftbutton').src);
			$('.next_button').attr('src',preload.getResult('rightbutton').src);
			$('.prev_button, .next_button').hide(0);
			// Next and previous functions starts
			var counter = 1;
			var text_array = ['p3text12','p3text13','p3text14'];
			$('.prev_button').click(function(){
				$('.prev_button').css('pointer-events','none');
				counter--;
				if(counter==0){
					counter=3;
				}
				if(counter==2){
					setTimeout(function(){
                        nav_button_controls(100);
                    },2000);
				}
				sound_player_intrnl_sld('sound_5_'+(counter),1,1,0);
				console.log(counter);
				$('.image_left').attr('src',preload.getResult('cell'+counter).src);
				$('.image_right').attr('src',preload.getResult('battery'+counter).src);
				$('.desc_text').html(eval('data.string.'+text_array[counter-1]));
				setTimeout(function(){
					$('.prev_button').css('pointer-events','auto');
				},200);
			});

			$('.next_button').click(function(){
				$('.next_button').css('pointer-events','none');
				counter++;
				// if(counter==4){
				// 	counter=1;
				// }
				// if(counter==3){
				// 	nav_button_controls(100);
				// 	$('.next_button').css({"pointer-events":"none","display":"none"});
				// }
				console.log(counter);
				counter!=3?sound_player_intrnl_sld('sound_5_'+(counter),1,1,0):sound_player_intrnl_sld('sound_5_'+(counter),1,0,1);
				$('.image_left').attr('src',preload.getResult('cell'+counter).src);
				$('.image_right').attr('src',preload.getResult('battery'+counter).src);
				$('.desc_text').html(eval('data.string.'+text_array[counter-1]));
				setTimeout(function(){
					$('.next_button').css('pointer-events','auto');
				},200);
			});

			// Next and previous functions ends

			break;

			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			sound_player('sound_'+(countNext+2));
			break;
			// case 10:
			// 			$('.diy_title').css('top','-30%');
			// 			$('.diy_desc').css('top','-21%');
			// 			setTimeout(function(){
			// 				$('.diy_title').animate({'top':'13.2%'},1000);
			// 				$('.diy_desc').animate({'top':'22%'},1000);
			// 				nav_button_controls(100);
			// 			},1000);
			// 			// $( '.science_project' ).attr( 'target','_blank' );
			// 			$('.science_project').click(function(){
			// 				window.open('subjects.html?sub=science&lang='+$lang+'&grade=6&filter=scienceproject', '_blank');
    	// 				// window.location.href ='subjects.html?sub=science&lang='+$lang+'&grade=6&filter=scienceproject';
			// 			});
			// 			break;
			default:
			nav_button_controls(100);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
            $(".girl_talking").attr("src",preload.getResult("girl_talking1").src)
            navigationcontroller();
		});
	}
	function sound_player_intrnl_sld(sound_id, prv,nxt,next){
	$('.prev_button, .next_button').hide(0);
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			// $('.prev_button, .next_button').show(0);
			prv?$('.prev_button').show(0):$('.prev_button').hide(0);
			nxt?$('.next_button').show(0):$('.next_button').hide(0);
			next?navigationcontroller():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
		function put_image_sec(content, count){
			if(content[count].hasOwnProperty('extraimgblock')){
				var imageblock = content[count].extraimgblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

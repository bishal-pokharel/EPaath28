var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
{
	contentblockadditionalclass:"blue_bg",
		imageblock:[{
			imagestoshow:[{
				imgid:'girl_talking',
				imgclass:'girl_talking'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box',
			textdata:data.string.p5text1,
			textclass:'inside_text'
		}]
},

// slide1
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'one_solar',
			imgclass:'mid_image'
		},{
			imgid:'bulb',
			imgclass:'bulb_bg'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p5text2,
		textclass:'top_text'
	},{
		textdata:data.string.p5text3,
		textclass:'bot_text down_up_anim'
	}]
},
// slide2
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'two_solar',
			imgclass:'mid_image'
		},{
			imgid:'bulb',
			imgclass:'bulb_bg'
		},{
			imgid:'mobile_charge',
			imgclass:'mobile_charge'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p5text2,
		textclass:'top_text'
	},{
		textdata:data.string.p5text4,
		textclass:'bot_text down_up_anim'
	}]
},
// slide3
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'three_solar',
			imgclass:'mid_image'
		},{
			imgid:'bulb',
			imgclass:'bulb_bg'
		},{
			imgid:'mobile_charge',
			imgclass:'mobile_charge'
		},{
			imgid:'tv_image',
			imgclass:'tv_image'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p5text2,
		textclass:'top_text'
	},{
		textdata:data.string.p5text5,
		textclass:'bot_text down_up_anim'
	}]
},

// slide4
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'girl_talking',
			imgclass:'girl_talking'
		}]
	}],
	speechbox:[{
		speechbox:'sp-1',
		imgid:'text_box',
		textdata:data.string.p5text6,
		textclass:'inside_text'
	}]
},

// slide5
{
	imageblock:[{
		imagestoshow:[{
			imgid:'night_bg',
			imgclass:'bg_full'
		},{
			imgid:'no_battery_house',
			imgclass:'mid_house'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p5text7,
		textclass:'top_text'
	}]
},

// slide6
{
	imageblock:[{
		imagestoshow:[{
			imgid:'cloudy_bg',
			imgclass:'bg_full'
		},{
			imgid:'no_battery_house',
			imgclass:'mid_house'
		},{
			imgid:'bulb',
			imgclass:'bulb_flicker'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p5text8,
		textclass:'top_text'
	}]
},

// slide7
{
	imageblock:[{
		imagestoshow:[{
			imgid:'sunneyday_bg',
			imgclass:'bg_full'
		},{
			imgid:'solar_to_battery',
			imgclass:'mid_house'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p5text9,
		textclass:'top_text'
	},{
		textdata:data.string.p5text10,
		textclass:'bot_text down_up_anim'
	}]
},

// slide8
{
	imageblock:[{
		imagestoshow:[{
			imgid:'cloudy_bg',
			imgclass:'bg_full'
		},{
			imgid:'battery_device_gif',
			imgclass:'mid_house'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p5text11,
		textclass:'top_text'
	},{
		textdata:data.string.p5text12,
		textclass:'bot_text down_up_anim'
	}]
},

// slide9
{
	imageblock:[{
		imagestoshow:[{
			imgid:'cloudy_bg',
			imgclass:'bg_full'
		},{
			imgid:'battery_device_gif',
			imgclass:'left_house'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p5text13,
		textclass:'top_text'
	},{
		textdata:data.string.p5text14,
		textclass:'sunnyday'
	},{
		textdata:data.string.p5text15,
		textclass:'night'
	},{
		textdata:data.string.p5text16,
		textclass:'cloudyday'
	},{
		textclass:'right_bg'
	},{
		textclass:'title',
		textdata:data.string.p5text16,
	}]
},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "text_box", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "one_solar", src: imgpath+"img02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "two_solar", src: imgpath+"img03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "three_solar", src: imgpath+"img04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_talking", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "girl_talking1", src: imgpath+"girl_talking.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mobile_charge", src: imgpath+"mobile_charge.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bulb", src: imgpath+"bulb.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tv_image", src: imgpath+"tv_image.png", type: createjs.AbstractLoader.IMAGE},
			{id: "no_light_house", src: imgpath+"hosue_cloudy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "all_glowing_gif", src: imgpath+"sunneyday.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "solar_to_battery", src: imgpath+"solar_to_battery.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "battery_device_gif", src: imgpath+"hosue_cloudy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cloudy_bg", src: imgpath+"cloudy_day_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "night_bg", src: imgpath+"night_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "night_bg", src: imgpath+"night_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunneyday_bg", src: imgpath+"sunneyday_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "no_battery_house", src: imgpath+"solar_panel02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"S5_P1.ogg"},
			{id: "sound_2", src: soundAsset+"S5_P2.ogg"},
			{id: "sound_3", src: soundAsset+"S5_P3.ogg"},
			{id: "sound_4", src: soundAsset+"S5_P4.ogg"},
			{id: "sound_4_2", src: soundAsset+"S5_P4_2.ogg"},
			{id: "sound_5", src: soundAsset+"S5_P5.ogg"},
			{id: "sound_6", src: soundAsset+"S5_P6.ogg"},
			{id: "sound_7", src: soundAsset+"S5_P7.ogg"},
			{id: "sound_8", src: soundAsset+"S5_P8.ogg"},
			{id: "sound_9", src: soundAsset+"S5_P9.ogg"},
			{id: "sound_10", src: soundAsset+"S5_P10.ogg"},
			{id: "sound_10_2", src: soundAsset+"S5_P10_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		put_speechbox_image(content, countNext);
		var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +3.5+"%";
		console.log(top_val);
		$('.title').css({
			'top':top_val
		});
		if(countNext<=8){
			sound_player('sound_'+(countNext+1));
		}
		switch(countNext){
			case 9:
			sound_player_nonav('sound_'+(countNext+1));
			$('.cloudyday').css({'pointer-events':'none','background':'grey'});
			$('.night,.cloudyday,.sunnyday').click(function(){
				$(this).css({'pointer-events':'none','background':'grey'});
				$('.night,.cloudyday,.sunnyday').not(this).css({'pointer-events':'auto','background':'#e56399'});
				if($(this).hasClass('sunnyday')){
					$('.left_house').attr('src',preload.getResult('all_glowing_gif').src);
					$('.bg_full').attr('src',preload.getResult('sunneyday_bg').src);
					$('.title').html(data.string.p5text14);
					nav_button_controls(100);
				}
				if($(this).hasClass('cloudyday')){
					$('.left_house').attr('src',preload.getResult('battery_device_gif').src);
					$('.bg_full').attr('src',preload.getResult('cloudy_bg').src);
					$('.title').html(data.string.p5text16);
				}
				if($(this).hasClass('night')){
					$('.left_house').attr('src',preload.getResult('battery_device_gif').src);
					$('.bg_full').attr('src',preload.getResult('night_bg').src);
					$('.title').html(data.string.p5text15);
				}
			});
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
            $(".girl_talking").attr("src",preload.getResult("girl_talking1").src)
            navigationcontroller();
		});
	}
	function sound_player_nonav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
		function put_image_sec(content, count){
			if(content[count].hasOwnProperty('extraimgblock')){
				var imageblock = content[count].extraimgblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

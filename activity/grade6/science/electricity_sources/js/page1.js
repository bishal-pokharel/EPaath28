var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//coverpage
	{
	extratextblock:[{
			textclass: "cover_text",
			textdata: data.string.covertxt
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'coverpage',
				imgclass:'bg_full'
			}]
		}]
},

//slide2
{
	contentblockadditionalclass:"cream_bg",
	extratextblock:[{
		textclass: "top_text",
		textdata: data.string.p1text1
	},{
			textclass: "mid_text",
			textdata: data.string.p1text2
		}]
},

//slide3
{
	contentblockadditionalclass:"cream_bg",
	extratextblock:[{
		textclass: "top_text",
		textdata: data.string.p1text3
	},{
			textclass: "below_top_text",
			textdata: data.string.p1text4
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'bg_image'
			}]
		}]
},

//slide4
{
	contentblockadditionalclass:"cream_bg",
	extratextblock:[{
		textclass: "top_text",
		textdata: data.string.p1text19
	},{
			textclass: "mid_text",
			textdata: data.string.p1text5
		}]
},

//slide5
{
	contentblockadditionalclass:"cream_bg",
		imageblock:[{
			imagestoshow:[{
				imgid:'girl_talking',
				imgclass:'girl_talking'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box',
			textdata:data.string.p1text6,
			textclass:'inside_text'
		}]
},



//slide7
{
		imageblock:[{
			imagestoshow:[{
				imgid:'exibithall',
				imgclass:'bg_image'
			},{
				imgid:'handicon',
				imgclass:'handicon1'
			},{
				imgid:'handicon',
				imgclass:'handicon2'
			},{
				imgid:'handicon',
				imgclass:'handicon3'
			},{
				imgid:'handicon',
				imgclass:'handicon4'
			},{
				imgid:'handicon',
				imgclass:'handicon5'
			}]
		}],
		extratextblock:[{
			textclass: "posture_text",
			textdata: data.string.p1text7
		},{
			textclass: "top_text",
			textdata: data.string.p1text8
		}],
		textdata1:data.string.p1text11,
		textdata2:data.string.p1text12,
		farmdiv:[{}]
},


];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var countclick = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg1", src: imgpath+"bgroom01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bgroom02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "exibithall", src: imgpath+"exibithall.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_talking", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: imgpath+"hand-icon-1.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "image3", src: imgpath+"cowmilk.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "image5", src: imgpath+"dust.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "image4", src: imgpath+"hay-cutter-animation.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "image1", src: imgpath+"coolingchamber.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "image2", src: imgpath+"watermachine.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cross", src: imgpath+"white_wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "title", src: soundAsset+"title.ogg"},
			{id: "sound_1", src: soundAsset+"S1_P2_1.ogg"},
			{id: "sound_2", src: soundAsset+"S1_P2_2.ogg"},
			{id: "sound_3", src: soundAsset+"S1_P3_1.ogg"},
			{id: "sound_4", src: soundAsset+"S1_P3_2.ogg"},
			{id: "sound_5", src: soundAsset+"S1_P4_1.ogg"},
			{id: "sound_6", src: soundAsset+"S1_P4_2.ogg"},
			{id: "sound_7", src: soundAsset+"S1_P5.ogg"},
			{id: "sound_6_1", src: soundAsset+"S1_P6_1.ogg"},
			{id: "sound_6_2", src: soundAsset+"S1_P6_CoolingChamber.ogg"},
			{id: "sound_6_3", src: soundAsset+"S1_P6_WaterPump.ogg"},
			{id: "sound_6_4", src: soundAsset+"S1_P6_MilkCow.ogg"},
			{id: "sound_6_5", src: soundAsset+"S1_P6_CutHay.ogg"},
			{id: "sound_6_6", src: soundAsset+"S1_P6_FanHusk.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		put_speechbox_image(content, countNext);
		var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +2+"%";
		console.log(top_val);
		$('.below_top_text').css({
			'top':top_val
		});
		switch(countNext){
			case 0:
			sound_player('title');
			break;
			case 1:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play('sound_1');
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				var current_sound1 = createjs.Sound.play('sound_2');
				current_sound1.play();
				current_sound1.on('complete', function(){
					navigationcontroller();
				});
			});
			break;
			case 2:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play('sound_3');
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				var current_sound1 = createjs.Sound.play('sound_4');
				current_sound1.play();
				current_sound1.on('complete', function(){
					navigationcontroller();
				});
			});
			break;
			case 3:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play('sound_5');
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				var current_sound1 = createjs.Sound.play('sound_6');
				current_sound1.play();
				current_sound1.on('complete', function(){
					navigationcontroller();
				});
			});
			break;
			case 4:
			sound_player('sound_7');
			break;
			case 5:
			sound_player_nonav('sound_6_1');
			$('.top_text').hide(0).fadeIn(1000);
			$('.handicon1, .handicon2, .handicon3, .handicon5, .handicon4 ').hide(0);
			setTimeout(function(){
				$('.handicon1, .handicon2, .handicon3, .handicon5, .handicon4').fadeIn(1000);
			},1000);
			$('.farmdiv').hide(0);
			farmdiv_function(1);
			farmdiv_function(2);
			farmdiv_function(3);
			farmdiv_function(4);
			farmdiv_function(5);
			$('.cross_button').click(function(){
				createjs.Sound.stop();
				$('.farmdiv').hide(500);
			});
			function farmdiv_function(number){
				$('.handicon'+number).click(function(){
					$('.cross_button').attr('src',preload.getResult('cross').src).hide(0);
					$('.background').attr('src',preload.getResult('image'+number).src);
					$('.desc1').html(eval('data.string.desc_texta_'+number)).css('left','-100%');
					$('.desc2').html(eval('data.string.desc_textb_'+number)).css('left','-100%');
					setTimeout(function(){
						$('.desc1,.desc2').animate({'left':'0%'},1000);
					},500);
					setTimeout(function(){
						$('.cross_button').fadeIn(200);
						sound_player_nonav('sound_6_'+(number+1));
					},1500);
					console.log(countclick);
					countclick++;
					if(countclick==5){
						nav_button_controls(100);
					}
					$('.farmdiv').show(500);
				});
			}
			break;
			default:
			nav_button_controls(100);
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationcontroller();
		});
	}
	function sound_player_nonav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('extraimgblock')){
			var imageblock = content[count].extraimgblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

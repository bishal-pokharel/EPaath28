var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
{
	contentblockadditionalclass:"cream",
		imageblock:[{
			imagestoshow:[{
				imgid:'girl_talking',
				imgclass:'girl_talking'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box',
			textdata:data.string.p4text1,
			textclass:'inside_text'
		}]
},

// slide1
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'pv_system',
			imgclass:'mid_image fadesin'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p4text2,
		textclass:'top_text'
	},{
		textdata:data.string.p4text5,
		textclass:'bot_text fadesin'
	},{
		textdata:data.string.p4text3,
		textclass:'did_you_know_text'
	},{
		textdata:data.string.p4text4,
		textclass:'dyk_text'
	}]
},

// slide2
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'solarpanel_gif',
			imgclass:'center_image fadesin'
		},{
			imgid:'sun',
			imgclass:'fadesin sun'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p4text2,
		textclass:'top_text'
	},{
		textdata:data.string.p4text11,
		textclass:'bot_text fadesin'
	},{
		textdata:data.string.p4text6,
		textclass:'tag_1 fadesin'
	},{
		textdata:data.string.p4text7,
		textclass:'tag_2 fadesin'
	},{
		textdata:data.string.p4text8,
		textclass:'tag_3 fadesin'
	},{
		textdata:data.string.p4text9,
		textclass:'tag_4 fadesin'
	},{
		textdata:data.string.p4text10,
		textclass:'tag_5 fadesin'
	}]
},

// slide3
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'solar',
			imgclass:'center_image fadesin'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p4text12,
		textclass:'top_text'
	},{
		textdata:data.string.p4text13,
		textclass:'bot_text fadesin'
	},]
},

// slide4
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'environment',
			imgclass:'right_img fadesin'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p4text12,
		textclass:'top_text'
	},{
		textdata:data.string.p4text14,
		textclass:'left_text fadesin',
	},{
		textclass:'bg_1',
	},{
		textclass:'bg_2',
	}]
},

// slide4
{
contentblockadditionalclass:"cream",
	imageblock:[{
		imagestoshow:[{
			imgid:'textbox01',
			imgclass:'dialogue_image fadesin'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p4text15,
		textclass:'top_text'
	},{
		textdata:data.string.p4text16,
		textclass:'fadesin mid_text ',
	}]
},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "text_box", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_talking", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "girl_talking1", src: imgpath+"girl_talking.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pv_system", src: imgpath+"pv_system.png", type: createjs.AbstractLoader.IMAGE},
			{id: "solarpanel_gif", src: imgpath+"solarpanel.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "environment", src: imgpath+"environment.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox01", src: imgpath+"textbox01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "solar", src: imgpath+"solar.jpg", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"S4_P1.ogg"},
			{id: "sound_2", src: soundAsset+"S4_P2.ogg"},
			{id: "sound_3", src: soundAsset+"S4_P3.ogg"},
			{id: "sound_4", src: soundAsset+"S4_P4.ogg"},
			{id: "sound_5", src: soundAsset+"S4_P5.ogg"},
			{id: "sound_6", src: soundAsset+"S4_P6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		put_speechbox_image(content, countNext);
		sound_player('sound_'+(countNext+1));
		switch(countNext){
			case 1:
			$('.did_you_know_text').css('top','-35%');
			$('.dyk_text').css('top','-26%');
			setTimeout(function(){
				$('.did_you_know_text').animate({'top':'13%'},1000);
				$('.dyk_text').animate({'top':'22%'},1000);
			},1000);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
            $(".girl_talking").attr("src",preload.getResult("girl_talking1").src)
            navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
		function put_image_sec(content, count){
			if(content[count].hasOwnProperty('extraimgblock')){
				var imageblock = content[count].extraimgblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

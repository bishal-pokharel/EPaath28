var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "cream",
		uppertextblock:[{
			textclass: "mid_top",
			textdata: data.string.p2text1
		},{
			textclass: "mid_bot fadesin",
			textdata: data.string.p2text2
		},{
			textclass: "top_fill",
		},{
			textclass: "bot_fill",
		}]
},

// slide1
{
	contentblockadditionalclass: "cream",
	uppertextblock:[{
		textclass: "top_text",
		textdata: data.string.p2text3
	},{
		textclass: "mid_bot fadesin",
		textdata: data.string.p2text4
	}]
},

// slide2
{
	contentblockadditionalclass: "cream",
	uppertextblock:[{
		textclass: "top_text",
		textdata: data.string.p2text3
	},{
		textclass: "mid_top_small fadesin",
		textdata: data.string.p2text5
	},{
		textclass: "label1 fadesin",
		textdata: data.string.p2text6
	},{
		textclass: "label2 fadesin",
		textdata: data.string.p2text7
	},{
		textclass: "label3 fadesin",
		textdata: data.string.p2text25
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'battery',
			imgclass:'image1'
		},{
			imgid:'dam',
			imgclass:'image2'
		},{
			imgid:'photocells',
			imgclass:'image3'
		}]
	}]
},

//slide3
{
	contentblockadditionalclass:"cream",
		imageblock:[{
			imagestoshow:[{
				imgid:'girl_talking',
				imgclass:'girl_talking'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box',
			textdata:data.string.p2text9,
			textclass:'inside_text'
		}]
},

//slide4
{
	contentblockadditionalclass:"cream",
		imageblock:[{
			imagestoshow:[{
				imgid:'girl_talking',
				imgclass:'girl_talking'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box',
			textdata:data.string.p2text10,
			textclass:'inside_text'
		}]
},

//slide5
{
	contentblockadditionalclass:"cream",
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'eroom1'
			},{
				imgclass:'bg_full show_later',
				imgid:'eroom2'
			},{
				imgclass:'hand_icon',
				imgid:'handicon'
			},{
				imgclass:'arrow_1',
				imgid:'arrow'
			},{
				imgclass:'arrow_2',
				imgid:'arrow'
			}]
		}],
		uppertextblock:[{
			textdata:data.string.p2text11,
			textclass:'top_text'
		},{
			textdata:data.string.p2text36,
			textclass:'bot_text'
		},{
			textdata:data.string.p2text26,
			textclass:'plug'
		}]
},
//slide6
{
	imageblock:[{
		imagestoshow:[{
			imgclass:'bg_full',
			imgid:'dam_final'
		},{
			imgclass:'bg_full show_later',
			imgid:'dam_final_gif'
		},{
			imgclass:'hand_icon_dam',
			imgid:'handicon'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p2text32,
		textclass:'top_text'
	}]
},
//slide7
{
	imageblock:[{
		imagestoshow:[{
			imgclass:'bg_full',
			imgid:'dam_final1'
		},{
			imgclass:'arrow_dam_1 laters',
			imgid:'arrow1'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p2text15,
		textclass:'top_text'
	},{
		textdata:data.string.p2text14,
		textclass:'label_damn_1 laters'
	}]
},
//slide8
{
	imageblock:[{
		imagestoshow:[{
			imgclass:'bg_full',
			imgid:'dam_final1'
		},{
			imgclass:'arrow_dam_1',
			imgid:'arrow1'
		},{
			imgclass:'arrow_dam_2 laters',
			imgid:'arrow1'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p2text37,
		textclass:'top_text'
	},{
		textdata:data.string.p2text14,
		textclass:'label_damn_1'
	},{
		textdata:data.string.p2text33,
		textclass:'label_damn_2 laters'
	}]
},
//slide9
{
	contentblockadditionalclass:"brown_bg",
	uppertextblock:[{
		textdata:data.string.p2text19,
		textclass:'label_photocell'
	},{
		textdata:data.string.p2text20,
		textclass:'bot_text fadesin'
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass:'solar_dark',
			imgid:'sunneyday_gif'
		},{
				imgclass:'arrow_photocell',
				imgid:'arrow'
			},{
			imgclass:'bg_full',
			imgid:'sunneyday_bg'
		}]
	}],
},
//slide10
{
	contentblockadditionalclass:"brown_bg",
	uppertextblock:[{
		textdata:data.string.p2text19,
		textclass:'label_photocell'
	},{
		textdata:data.string.p2text22,
		textclass:'bot_text fadesin'
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass:'solar_dark',
			imgid:'sunneyday_gif'
		},{
			imgclass:'solar_dark light',
			imgid:'solar_light'
		},{
				imgclass:'arrow_photocell',
				imgid:'arrow'
		},{
			imgclass:'bg_full',
			imgid:'sunneyday_bg'
		}]
	}]
},

//slide11
{
	contentblockadditionalclass:'bg_blue',
	uppertextblock:[{
		textclass:'top_text',
		textdata:data.string.p2text23
	},{
		textclass:'bot_text',
		textdata:data.string.p2text30
	},{
		textclass:'show_later_2 label_t_1',
		textdata:data.string.p2text27
	},{
		textclass:'show_later_2 label_t_2',
		textdata:data.string.p2text28
	},{
		textclass:'show_later_2 label_t_3',
		textdata:data.string.p2text29
	},{
		textclass:'bot_fill',
	}],
	imageblock:[{
		imagestoshow:[{
		imgclass:'torch torch1',
		imgid:'torch1'
	},{
		imgclass:'hand_icon_torch',
		imgid:'handicon'
	},{
		imgclass:'torch torch_inside',
		imgid:'torch_inside'
	},{
		imgclass:'show_later_2 arrow_t_1',
		imgid:'arrow1'
	},{
		imgclass:'show_later_2 arrow_t_2',
		imgid:'arrow1'
	},{
		imgclass:'show_later_2 arrow_t_3',
		imgid:'arrow1'
	}]
	}]
},


//slide12
{
	contentblockadditionalclass:'bg_blue',
	uppertextblock:[{
		textclass:'top_text',
		textdata:data.string.p2text23
	},{
		textclass:'bot_text',
		textdata:data.string.p2text30
	},{
		textclass:'show_later_2 label_t_1',
		textdata:data.string.p2text27
	},{
		textclass:'show_later_2 label_t_2',
		textdata:data.string.p2text28
	},{
		textclass:'show_later_2 label_t_3',
		textdata:data.string.p2text29
	},{
		textclass:'bot_fill',
	}],
	imageblock:[{
		imagestoshow:[{
		imgclass:'torch torch1',
		imgid:'torch_inside'
	},{
		imgclass:'hand_icon_torch',
		imgid:'handicon'
	},{
		imgclass:'torch torch_inside torch_gif',
		imgid:'torchopen_inside_current_flow'
	},{
		imgclass:'show_later_2 arrow_t_1',
		imgid:'arrow1'
	},{
		imgclass:'show_later_2 arrow_t_2',
		imgid:'arrow1'
	},{
		imgclass:'show_later_2 arrow_t_3',
		imgid:'arrow1'
	}]
	}]
},

// slide13
{
	contentblockadditionalclass: "cream",
	uppertextblock:[{
		textclass: "top_text",
		textdata: data.string.p2text3
	},{
		textclass: "mid_top_small fadesin",
		textdata: data.string.p2text34
	},{
		textclass: "label1 fadesin",
		textdata: data.string.p2text6
	},{
		textclass: "label2 fadesin",
		textdata: data.string.p2text7
	},{
		textclass: "label3 fadesin",
		textdata: data.string.p2text25
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'battery',
			imgclass:'image1'
		},{
			imgid:'dam',
			imgclass:'image2'
		},{
			imgid:'photocells',
			imgclass:'image3'
		}]
	}]
},

// slide14
{
	contentblockadditionalclass: "cream",
	uppertextblock:[{
		textclass: "top_text",
		textdata: data.string.p2text3
	},{
		textclass: "mid_top_small fadesin",
		textdata: data.string.p2text35
	},{
		textclass: "label1 fadesin",
		textdata: data.string.p2text6
	},{
		textclass: "label2 fadesin",
		textdata: data.string.p2text7
	},{
		textclass: "label3 fadesin",
		textdata: data.string.p2text25
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'battery',
			imgclass:'image1'
		},{
			imgid:'dam',
			imgclass:'image2'
		},{
			imgid:'photocells',
			imgclass:'image3'
		}]
	}]
},



];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "battery", src: imgpath+"battery.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dam", src: imgpath+"dam.png", type: createjs.AbstractLoader.IMAGE},
			{id: "photocells", src: imgpath+"photocells.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eroom1", src: imgpath+"eroom1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eroom2", src: imgpath+"eroom2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_talking", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_talking1", src: imgpath+"girl_talking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: imgpath+"hand-icon-1.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "solar_dark", src: imgpath+"solar_panel01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "solar_light", src: imgpath+"solar_panel02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow1", src: imgpath+"arrow1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "switch", src: imgpath+"switch.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch1", src: imgpath+"torch02a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch2", src: imgpath+"torch2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch_inside", src: imgpath+"torch_inside.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch_inside1", src: imgpath+"torch_inside1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dam_final", src: imgpath+"dam_final.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dam_final_gif", src: imgpath+"dam_final.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dam_final1", src: imgpath+"dam_final01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunneyday_bg", src: imgpath+"sunneyday_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunneyday_gif", src: imgpath+"sunneyday.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "torchopen_inside_current_flow", src: imgpath+"torchopen_inside_current_flow.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"S2_P1.ogg"},
			{id: "sound_2", src: soundAsset+"S2_P2.ogg"},
			{id: "sound_3", src: soundAsset+"S2_P3.ogg"},
			{id: "sound_4", src: soundAsset+"S2_P4.ogg"},
			{id: "sound_5", src: soundAsset+"S2_P5.ogg"},
			{id: "sound_6", src: soundAsset+"S2_P6_1.ogg"},
			{id: "sound_7", src: soundAsset+"S2_P6_2.ogg"},
			{id: "sound_8", src: soundAsset+"S2_P6_3.ogg"},
			{id: "sound_9", src: soundAsset+"S2_P7.ogg"},
			{id: "sound_10", src: soundAsset+"S2_P8_1.ogg"},
			{id: "sound_11", src: soundAsset+"S2_P9_1.ogg"},
			{id: "sound_12", src: soundAsset+"S2_P10.ogg"},
			{id: "sound_13", src: soundAsset+"S2_P11.ogg"},
			{id: "sound_14", src: soundAsset+"S2_P12.ogg"},
			{id: "sound_15", src: soundAsset+"S2_P12_1.ogg"},
			{id: "sound_16", src: soundAsset+"S2_P13.ogg"},
			{id: "sound_17", src: soundAsset+"S2_P14.ogg"},
			{id: "sound_18", src: soundAsset+"S2_P15.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			sound_player('sound_'+(countNext+1));
			break;

			case 5:
			sound_player_nonav('sound_6')
			$('.show_later').hide(0);
			$('.bot_text').css('bottom','-30%');
			$('.hand_icon').click(function(){
				$(this).hide(200);
				$('.top_text').html(data.string.p2text12);
				$('.show_later').fadeIn(200);
				setTimeout(function(){
					$('.bot_text').animate({'bottom':'0%'},400);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play('sound_7');
					current_sound.play();
					current_sound.on('complete', function(){
							var current_sound1 = createjs.Sound.play('sound_8');
							current_sound1.play();
							current_sound1.on('complete', function(){
								nav_button_controls(100);
							});
						});
				},400);
			});
			break;

			case 6:
			sound_player_nonav('sound_9')
			$('.show_later').hide(0);
			$('.hand_icon_dam').click(function(){
				$(this).hide(200);
				$('.show_later').show(0);
				setTimeout(function(){
					nav_button_controls(100);
				},8500);
			});
			break;

			case 7:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play('sound_10');
			current_sound.play();
			current_sound.on('complete', function(){
						nav_button_controls(100);
				});
			$('.laters').hide(0).fadeIn(1000);
			$('.top_text').css('top','-30%').animate({'top':'0%'},1000);
			break;
			case 8:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play('sound_11');
			current_sound.play();
			current_sound.on('complete', function(){
						nav_button_controls(100);
				});
			$('.laters').hide(0).fadeIn(1000);
			$('.top_text').css('top','-30%').animate({'top':'0%'},1000);
			break;

			case 9:
			sound_player('sound_12');
			break;

			case 10:
			sound_player('sound_13');
			$('.light').hide(0);
			break;

			case 11:
			sound_player_nonav('sound_14');
			$('.bot_text').css('bottom','-30%');
			$('.show_later,.torch_inside,.show_later_2').hide(0);
			$('.hand_icon_torch').click(function(){
				sound_player('sound_15');
				$(this).hide(0);
				$('.torch_inside,.show_later_2').fadeIn(600);
				$('.bot_text').animate({'bottom':'0%'},400);
			});
			break;

			case 12:
			sound_player('sound_16');
			$('.show_later,.torch_inside,.show_later_2').hide(0);
			$('.hand_icon_torch').hide(0);
			$('.torch_inside').fadeOut(600);
			$('.torch_inside,.show_later_2').fadeIn(600);
			$('.bot_text').html(data.string.p2text31);
			break;
			case 13:
			sound_player('sound_17');
			break;
			case 14:
			sound_player('sound_18');
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationcontroller();
			$(".girl_talking").attr("src",preload.getResult("girl_talking1").src)
		});
	}
	function sound_player_nonav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
		function put_image_sec(content, count){
			if(content[count].hasOwnProperty('extraimgblock')){
				var imageblock = content[count].extraimgblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

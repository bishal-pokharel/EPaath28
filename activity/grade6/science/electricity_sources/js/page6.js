var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
{
	contentblockadditionalclass:"blue_bg",
		imageblock:[{
			imagestoshow:[{
				imgid:'girl_talking',
				imgclass:'girl_talking'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box',
			textdata:data.string.p6text1,
			textclass:'inside_text'
		}]
},

// slide1
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'dam',
			imgclass:'bg_full'
		},{
			imgid:'arrow',
			imgclass:'arrow_gen fadesin'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p6text2,
		textclass:'top_text'
	},{
		textdata:data.string.p6text3,
		textclass:'desc_text fadesin'
	},{
		textdata:data.string.p6text2,
		textclass:'label_1'
	},{
		textdata:data.string.p6text4,
		textclass:'remember'
	},{
		textdata:data.string.p6text5,
		textclass:'remember_desc'
	}]
},

// slide2
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'dam',
			imgclass:'bg_full'
		},{
			imgid:'arrow',
			imgclass:'arrow_gen fadesin'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p6text2,
		textclass:'top_text'
	},{
		textdata:data.string.p6text6,
		textclass:'desc_text fadesin'
	},{
		textdata:data.string.p6text2,
		textclass:'label_1 fadesin'
	}]
},
// slide3
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'dam',
			imgclass:'bg_full'
		},{
			imgid:'arrow',
			imgclass:'arrow_gen fadesin'
		},{
			imgid:'arrow',
			imgclass:'fadesin arrow_gen2 '
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p6text2,
		textclass:'top_text'
	},{
		textdata:data.string.p6text7,
		textclass:'desc_text fadesin'
	},{
		textdata:data.string.p6text2,
		textclass:'label_1 fadesin'
	},{
		textdata:data.string.p6text8,
		textclass:'label_2 fadesin'
	},{
		textdata:data.string.p6text9,
		textclass:'dyk'
	},{
		textdata:data.string.p6text13,
		textclass:'dyk_content'
	}]
},

// slide4
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'dam_ricemill',
			imgclass:'bg_full'
		},{
			imgid:'arrow',
			imgclass:'arrow_gen fadesin'
		},{
			imgid:'arrow',
			imgclass:'fadesin arrow_gen2 '
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p6text11,
		textclass:'top_text'
	},{
		textdata:data.string.p6text12,
		textclass:'desc_text fadesin'
	},{
		textdata:data.string.p6text27,
		textclass:'label_1 fadesin'
	},{
		textdata:data.string.p6text8,
		textclass:'label_2 fadesin'
	}]
},

// slide5
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'girl_talking',
			imgclass:'girl_talking'
		}]
	}],
	speechbox:[{
		speechbox:'sp-1',
		imgid:'text_box',
		textdata:data.string.p6text14,
		textclass:'inside_text'
	}]
},

// slide6
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'dam_final',
			imgclass:'bg_full'
		},{
			imgid:'arrow',
			imgclass:'arrow_gen fadesin'
		},{
			imgid:'arrow',
			imgclass:'fadesin arrow_gen2 '
		},{
			imgid:'handicon',
			imgclass:'handicon '
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p6text2,
		textclass:'top_text'
	},{
		textdata:data.string.p6text15,
		textclass:'desc_text fadesin'
	},{
		textdata:data.string.p6text2,
		textclass:'label_1 fadesin'
	},{
		textdata:data.string.p6text8,
		textclass:'label_2 fadesin'
	}]
},

// slide7
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'dam_final_gif',
			imgclass:'bg_full'
		},{
			imgid:'arrow',
			imgclass:'arrow_gen fadesin'
		},{
			imgid:'arrow',
			imgclass:'fadesin arrow_gen2'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p6text2,
		textclass:'top_text'
	},{
		textdata:data.string.p6text16,
		textclass:'desc_text fadesin'
	},{
		textdata:data.string.p6text2,
		textclass:'label_1 fadesin'
	},{
		textdata:data.string.p6text8,
		textclass:'label_2 fadesin'
	}]
},

// slide8
{
contentblockadditionalclass:"blue_bg",
	imageblock:[{
		imagestoshow:[{
			imgid:'dam_final_gif',
			imgclass:'bg_full'
		},{
			imgid:'arrow',
			imgclass:'arrow_gen fadesin'
		},{
			imgid:'arrow',
			imgclass:'fadesin arrow_gen2'
		}]
	}],
	uppertextblock:[{
		textdata:data.string.p6text2,
		textclass:'top_text'
	},{
		textdata:data.string.p6text17,
		textclass:'desc_text fadesin'
	},{
		textdata:data.string.p6text2,
		textclass:'label_1 fadesin'
	},{
		textdata:data.string.p6text8,
		textclass:'label_2 fadesin'
	}]
},

// slide9
{
	contentblockadditionalclass:"blue_bg",
		imageblock:[{
			imagestoshow:[{
				imgid:'windmill_drawing',
				imgclass:'center_image'
			}]
		}],
	uppertextblock:[{
		textdata:data.string.p6text2,
		textclass:'top_text'
	},{
		textdata:data.string.p6text18,
		textclass:'desc_text fadesin'
	},{
		textdata:data.string.p6text19,
		textclass:'label_wind fadesin'
	},{
		textdata:data.string.p6text20,
		textclass:'label_wind1 fadesin'
	},{
		textdata:data.string.p6text21,
		textclass:'label_wind2 fadesin'
	},{
		textdata:data.string.p6text22,
		textclass:'label_wind3 fadesin'
	},{
		textdata:data.string.p6text23,
		textclass:'label_wind4 fadesin'
	},{
		textdata:data.string.p6text24,
		textclass:'label_wind5 fadesin'
	},{
		textdata:data.string.p6text25,
		textclass:'label_wind6 fadesin'
	},{
		textdata:data.string.p6text26,
		textclass:'label_wind7 fadesin'
	}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "text_box", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_talking", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "girl_talking1", src: imgpath+"girl_talking.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dam", src: imgpath+"dam_village.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dam_final_gif", src: imgpath+"dam_village.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dam_ricemill", src: imgpath+"dam_ricemill.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: imgpath+"hand-icon-1.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dam_final", src: imgpath+"dam_village.png", type: createjs.AbstractLoader.IMAGE},
			{id: "windmill_drawing", src: imgpath+"windmill_drawing_witharrow.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"S6_P1.ogg"},
			{id: "sound_2", src: soundAsset+"S6_P2.ogg"},
			{id: "sound_2_1", src: soundAsset+"S6_P2_1.ogg"},
			{id: "sound_3", src: soundAsset+"S6_P3.ogg"},
			{id: "sound_4", src: soundAsset+"S6_P4.ogg"},
			{id: "sound_5", src: soundAsset+"S6_P5.ogg"},
			{id: "sound_6", src: soundAsset+"S6_P6.ogg"},
			{id: "sound_7", src: soundAsset+"S6_P7.ogg"},
			{id: "sound_8", src: soundAsset+"S6_P8.ogg"},
			{id: "sound_9", src: soundAsset+"S6_P9.ogg"},
			{id: "sound_10", src: soundAsset+"S6_P10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		put_speechbox_image(content, countNext);
		var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +3.5+"%";
		console.log(top_val);
		$('.title').css({
			'top':top_val
		});
		if(countNext!=1 || countNext!=6){
			sound_player('sound_'+(countNext+1));
		}
		switch(countNext){

			case 1:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play('sound_2');
			current_sound.play();
			current_sound.on('complete', function(){
				// var current_sound1 = createjs.Sound.play('sound_2_1');
				// current_sound1.play();
				// current_sound1.on('complete', function(){
					navigationcontroller();
				// });
			});
			$('.remember').css('top','-50%');
			$('.ulclass').css('top','-42%');
			setTimeout(function(){
				$('.remember').animate({'top':'27%'},1000);
				$('.ulclass').animate({'top':'35%'},1000);
			},1500);
			break;

			case 3:
			$('.dyk').css('top','-50%');
			$('.dyk_content').css('top','-42%');
			setTimeout(function(){
				$('.dyk').animate({'top':'26%'},1000);
				$('.dyk_content').animate({'top':'30%'},1000);
			},1500);
			setTimeout(function(){
				nav_button_controls(100);
			},2500);
			break;

			case 6:
			sound_player_nonav('sound_'+(countNext+1));
			$('.handicon').click(function(){
				$(this).hide(0);
				$('.bg_full').attr('src',preload.getResult('dam_final_gif').src);
			});
			setTimeout(function(){
				nav_button_controls(100);
			},9200);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
            $(".girl_talking").attr("src",preload.getResult("girl_talking1").src)
            navigationcontroller();
		});
	}
	function sound_player_nonav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
		function put_image_sec(content, count){
			if(content[count].hasOwnProperty('extraimgblock')){
				var imageblock = content[count].extraimgblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

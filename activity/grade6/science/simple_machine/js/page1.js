var imgpath = $ref + '/images/';
var soundAsset = $ref+"/audio_"+$lang+"/";

var content;
content = [{
    // starting page
    contentblockadditionalclass: "contentwithbg",
    centertextblock: [{
        textclass: 'chapter-title',
        textdata: data.string.chapter_title
    }],
    imageblock: [{
        imagestoshow: [{
            imgsrc: imgpath + 'hammer_action.png',
            imgclass: 'hammer-action'
        }, {
            imgsrc: imgpath + 'scissors_action.png',
            imgclass: 'scissors-action'
        }, {
            imgsrc: imgpath + 'shovel_action.png',
            imgclass: 'shovel-action'
        }, {
            imgsrc: imgpath + 'wheelbarrow_action.png',
            imgclass: 'wheelbarrow-action'
        }, {
            imgsrc: imgpath + 'hammer.png',
            imgclass: 'hammer'
        }, {
            imgsrc: imgpath + 'scissors.png',
            imgclass: 'scissors'
        }, {
            imgsrc: imgpath + 'shovel.png',
            imgclass: 'shovel'
        }, {
            imgsrc: imgpath + 'wheelbarrow.png',
            imgclass: 'wheelbarrow'
        }]
    }]
}, {
    // slide 1
    contentblockadditionalclass: "contentwithbg",
    centertextblock: [{
        textclass: 'chapter-title',
        textdata: data.string.chapter_title
    }, {
        textdata: data.string.p1_text1
    }, {
        textdata: data.string.p1_text2
    }],
    imageblock: [{
        imagestoshow: [{
            imgsrc: imgpath + 'hammer_action.png',
            imgclass: 'hammer-action stopped'
        }, {
            imgsrc: imgpath + 'scissors_action.png',
            imgclass: 'scissors-action stopped'
        }, {
            imgsrc: imgpath + 'shovel_action.png',
            imgclass: 'shovel-action stopped'
        }, {
            imgsrc: imgpath + 'wheelbarrow_action.png',
            imgclass: 'wheelbarrow-action stopped'
        }, {
            imgsrc: imgpath + 'hammer.png',
            imgclass: 'hammer stopped'
        }, {
            imgsrc: imgpath + 'scissors.png',
            imgclass: 'scissors stopped'
        }, {
            imgsrc: imgpath + 'shovel.png',
            imgclass: 'shovel stopped'
        }, {
            imgsrc: imgpath + 'wheelbarrow.png',
            imgclass: 'wheelbarrow stopped'
        }]
    }]
}, {
    // slide 2
    contentblockadditionalclass: "contentwithbg",
    centertextblock: [{
        textclass: 'chapter-title',
        textdata: data.string.chapter_title
    }, {
        textdata: data.string.p1_text3
    }],
    imageblock: [{
        imagestoshow: [{
            imgsrc: imgpath + 'hammer_action.png',
            imgclass: 'hammer-action stopped'
        }, {
            imgsrc: imgpath + 'scissors_action.png',
            imgclass: 'scissors-action stopped'
        }, {
            imgsrc: imgpath + 'shovel_action.png',
            imgclass: 'shovel-action stopped'
        }, {
            imgsrc: imgpath + 'wheelbarrow_action.png',
            imgclass: 'wheelbarrow-action stopped'
        }, {
            imgsrc: imgpath + 'hammer.png',
            imgclass: 'hammer stopped'
        }, {
            imgsrc: imgpath + 'scissors.png',
            imgclass: 'scissors stopped'
        }, {
            imgsrc: imgpath + 'shovel.png',
            imgclass: 'shovel stopped'
        }, {
            imgsrc: imgpath + 'wheelbarrow.png',
            imgclass: 'wheelbarrow stopped'
        }]
    }]
}, {
    contentblockadditionalclass: "contentwithbg",
    uppertextblock: [{
        textdata: data.string.p1_text4
    }],
    imageblock: [{
        imagestoshow: [{
            imgsrc: imgpath + 'hammer_action.png',
            imgclass: 'blink'
        }, {
            imgsrc: imgpath + 'scissors_action.png',
            imgclass: 'blink'
        }, {
            imgsrc: imgpath + 'shovel_action.png',
            imgclass: 'blink'
        }, {
            imgsrc: imgpath + 'wheelbarrow_action.png',
            imgclass: 'blink'
        }, {
            imgsrc: imgpath + 'hammer.png',
            imgclass: 'blink'
        }, {
            imgsrc: imgpath + 'scissors.png',
            imgclass: 'blink'
        }, {
            imgsrc: imgpath + 'shovel.png',
            imgclass: 'blink'
        }, {
            imgsrc: imgpath + 'wheelbarrow.png',
            imgclass: 'blink'
        }, {
            imgsrc: imgpath + 'thinking.png',
            imgclass: 'thinking'
        }]
    }]
}];


// }

$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    recalculateHeightWidth();

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    /**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);
        vocabcontroller.findwords(countNext);

        $imageblock = $('.imageblock');

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
		    sound_player("sound_" + countNext);
        switch (countNext) {
            case 0:
            sound_nav("sound_" + countNext);
            nav_button_controls(3000);
            break;
        }
    }
  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
  function sound_nav(sound_id, next){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    // setTimeout(function(){
    total_page = content.length;
    // templateCaller();
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span class=" + stylerulename + ">";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

var imgpath = $ref + "/images/";
var museumPath = imgpath + "museum/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var	content = [
    {
        //slide 0
    contentblockadditionalclass: "contentwithbg",
        uppertextblock: [{
            textclass: "centertext",
            textdata: data.string.p3_text1
        }],
    },
    {
        // slide 1
    contentblockadditionalclass: "contentwithbg",
        imageyoo: [{
            images: [{
                imgsrc: museumPath + 'museum-for-simple-machine.png',
                imgclass: 'museumimg'

            }]
        }],
        texto :[{
            labelclass : 'museumtext',
            textdata: data.string.p3_text31

        }],
        uppertextblock: [{
            textdata: data.string.p3_text2
        }, {
            textdata: data.string.p3_text3
        }]
    }, {
        // slide 2
        uppertextblockadditionalclass: 'museum',
        uppertextblock: [{
            textclass: "protitile",
            textdata: data.string.p3_text4
        }],
        projectorlight: true,
        projectorsrc: museumPath + 'camera.png',
        imageblock: [{
            images: [{
                imgid: 'axe',
                imgsrc: museumPath + 'axe.png',
                imgclass: 'axe tools'
            }, {
                imgid: 'bauso',
                imgsrc: museumPath + 'bauso.png',
                imgclass: 'bauso tools'
            }, {
                imgid: 'dhiki',
                imgsrc: museumPath + 'dhiki.png',
                imgclass: 'dhiki tools'
            }, {
                imgid: 'doko',
                imgsrc: museumPath + 'doko.png',
                imgclass: 'doko tools'
            }, {
                imgid: 'gada',
                imgsrc: museumPath + 'gada.png',
                imgclass: 'gada tools'
            }, {
                imgid: 'hammer',
                imgsrc: museumPath + 'hammer.png',
                imgclass: 'hammer tools'
            }, {
                imgid: 'hammerbig',
                imgsrc: museumPath + 'hammerbig.png',
                imgclass: 'hammerbig tools'
            }, {
                imgid: 'hasiya',
                imgsrc: museumPath + 'hasiya.png',
                imgclass: 'hasiya tools'
            }, {
                imgid: 'jato',
                imgsrc: museumPath + 'jato.png',
                imgclass: 'jato tools'
            }, {
                imgid: 'lever',
                imgsrc: museumPath + 'lever.png',
                imgclass: 'lever tools'
            }, {
                imgid: 'plane',
                imgsrc: museumPath + 'plane.png',
                imgclass: 'plane tools'
            }, {
                imgid: 'pulley',
                imgsrc: museumPath + 'pulley.png',
                imgclass: 'pulley tools'
            }, {
                imgid: 'sharp',
                imgsrc: museumPath + 'sharp.png',
                imgclass: 'sharp tools'
            }]
        }],
        definitionblock: [{
            textclass: 'axe-title title',
            textdata: data.string.p3_text5
        }, {
            textclass: 'axe-desc desc',
            textdata: data.string.p3_text6
        }, {
            textclass: 'bauso-title title',
            textdata: data.string.p3_text7
        }, {
            textclass: 'bauso-desc desc',
            textdata: data.string.p3_text8
        }, {
            textclass: 'hasiya-title title',
            textdata: data.string.p3_text9
        }, {
            textclass: 'hasiya-desc desc',
            textdata: data.string.p3_text10
        }, {
            textclass: 'doko-title title',
            textdata: data.string.p3_text11
        }, {
            textclass: 'doko-desc desc',
            textdata: data.string.p3_text12
        }, {
            textclass: 'hammer-title title',
            textdata: data.string.p3_text13
        }, {
            textclass: 'hammer-desc desc',
            textdata: data.string.p3_text14
        }, {
            textclass: 'plane-title title',
            textdata: data.string.p3_text15
        }, {
            textclass: 'plane-desc desc',
            textdata: data.string.p3_text16
        }, {
            textclass: 'dhiki-title title',
            textdata: data.string.p3_text17
        }, {
            textclass: 'dhiki-desc desc',
            textdata: data.string.p3_text18
        }, {
            textclass: 'gada-title title',
            textdata: data.string.p3_text19
        }, {
            textclass: 'gada-desc desc',
            textdata: data.string.p3_text20
        }, {
            textclass: 'pulley-title title',
            textdata: data.string.p3_text21
        }, {
            textclass: 'pulley-desc desc',
            textdata: data.string.p3_text22
        }, {
            textclass: 'sharp-title title',
            textdata: data.string.p3_text23
        }, {
            textclass: 'sharp-desc desc',
            textdata: data.string.p3_text24
        }, {
            textclass: 'hammerbig-title title',
            textdata: data.string.p3_text25
        }, {
            textclass: 'hammerbig-desc desc',
            textdata: data.string.p3_text26
        }, {
            textclass: 'lever-title title',
            textdata: data.string.p3_text27
        }, {
            textclass: 'lever-desc desc',
            textdata: data.string.p3_text28
        }, {
            textclass: 'jato-title title',
            textdata: data.string.p3_text29
        }, {
            textclass: 'jato-desc desc',
            textdata: data.string.p3_text30
        }]
    }
];


$(function() {
	// var height = $(window).height();
	// var width = $(window).width();
	// $("#board").css({"width": width, "height": (height*580/960)});
	// function recursion(){
	// if(data.string != null){
	// } else{
	// recursion();
	// }
	// }
	// recursion();
	/*
		TODO: lets remove this
	*/
    var isFirefox = typeof InstallTrigger !== 'undefined';
	$(window).resize(function() {
		recalculateHeightWidth();
	});

	function recalculateHeightWidth() {
        // resizing fontsize of number with respect to numberblock
        var $num = $('p.number');
        var $numberBlock = $('.numberblock');
        var fontSize = parseInt($numberBlock.height() * 0.57)+'px';
        $num.css({'font-size': fontSize});

	}
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	recalculateHeightWidth();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// sounds
			{id: "sound_0", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "axe", src: soundAsset+"p3_tools/Axe.ogg"},
			{id: "bauso", src: soundAsset+"p3_tools/Hoe.ogg"},
			{id: "dhiki", src: soundAsset+"p3_tools/Traditional pounding tool.ogg"},
			{id: "doko", src: soundAsset+"p3_tools/Bamboo basket.ogg"},
			{id: "gada", src: soundAsset+"p3_tools/Wheel barrow.ogg"},
			{id: "hammer", src: soundAsset+"p3_tools/Hammer.ogg"},
			{id: "hammerbig", src: soundAsset+"p3_tools/Sledge hammer.ogg"},
			{id: "hasiya", src: soundAsset+"p3_tools/Sickle.ogg"},
			{id: "jato", src: soundAsset+"p3_tools/Traditional grinder.ogg"},
			{id: "lever", src: soundAsset+"p3_tools/Lever.ogg"},
			{id: "plane", src: soundAsset+"p3_tools/Inclined plane.ogg"},
			{id: "pulley", src: soundAsset+"p3_tools/Pulley.ogg"},
			{id: "sharp", src: soundAsset+"p3_tools/Peg.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    /**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
        vocabcontroller.findwords(countNext);
		$(".midtext2").css({
			"font-size":"1.7em"
		});
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

        var $projectorScreen = $('.projector-screen');
        var $projectorLight = $('.projector-light');
        var $contentBlock = $('.contentblock');
        var $imageBlock = $('.imageblock');
        $reloadBtn = $('.reload-btn');
        $stopBtn = $('.stop-btn');
        $smallnextbtn = $('.smnxt');

		switch (countNext) {
			case 0:
				sound_player("sound_"+countNext);
				break;
			case 1:
				sound_player("sound_"+countNext);
				break;
            case 2:
                var $tools = $('.tools');
                $tools.addClass('active');
                $reloadBtn.hide(0);
                $stopBtn.hide(0);
                $smallnextbtn.hide(0);
                var cnt=0;
				        sound_nav("sound_"+countNext);
                var handleToolClick = function() {
                    cnt++;

                    var time = 1000;
                    var $this = $(this);
                    var id = $this.attr('id');

                    var $titleText = $('.' + id + '-title');
                    var $descText = $('.' + id + '-desc');
                    var descData = $descText.html();
                    var className = $this[0].classList[0];
                    setTimeout(function(){
                      sound_nav(className);
                    },2500);

                    console.log("classnames", className);
                    if(cnt>=13){
                      nav_button_controls(1000);
                    }
                    $descText.html('');

                    $tools.off('click', handleToolClick);
                    $tools.removeClass('active');

                    $imageBlock.css({
                        filter: 'brightness(30%)'
                    });
                    console.log($projectorLight.attr('class'));
                    $projectorLight.addClass('animatelighttrigger');
                    $projectorScreen.addClass('activateProjectorScreen');

                    // Create image element for respective tool
                    var $image = $('<img>', {
                        class: 'projector-img',
                        src: museumPath + id + '.png'
                    });
                    $projectorScreen.html($image);    // append tool image on screen
                    setTimeout(function() {
                        $image.css({
                            'transform': 'scale(1) translate(-50%, -50%)',
                            'opacity': 1
                        });
                    }, time);

                    time += 1000;
                    setTimeout(function() {
                        $titleText.fadeIn(2000);
                    }, time);

                    time += 2000;

                    setTimeout(function() {
                        $descText.show(0);
                        $descText.html(descData);
                        vocabcontroller.findwords(countNext);
                        $smallnextbtn.show(0);
                        // animateType($descText, descData);
                    }, time);
                    $smallnextbtn.unbind("click");

                      $smallnextbtn.click(function(){
                          createjs.Sound.stop();
                                    $titleText.hide(0);
                                    $descText.hide(0);
                                    $(this).hide(0);
                                    $(".projector-screen").html("");

                                    var $gifImage = $('<img>', {
                                        class: 'projector-gif',
                                        src: museumPath + id + '_animation.gif'
                                    });
                                    $projectorScreen.html($gifImage);

                                     setTimeout(function() {
                                    $reloadBtn.show(0);
                                    $stopBtn.show(0);
                                    handleReloadAction();
                                    handleStopAction();
                                    }, 2000);
                                });



                };
                $tools.on('click', handleToolClick); // .tools click event

                function handleReloadAction() {
                    $reloadBtn.unbind("click");
                    var selector = '.projector-screen';
                    var img = $(selector).find('img');
                    var timestamp = new Date().getTime();
                    var imgSrc = img.attr('src')+'?'+timestamp;
                    $reloadBtn.on('click', function() {
                      var selector = '.projector-screen';
                      $(".projector-screen").html("");
                      // var newImg = $('<img>', {src: imgSrc, class: 'projector-gif'});
                      var newImg = $("<img src="+imgSrc+" class='projector-gif'>");
                      $projectorScreen.html(newImg);
                  });
                }

                function handleStopAction() {
                    $stopBtn.unbind("click");
                    $stopBtn.on('click', function() {
                        var selector = '.projector-screen';
                        // var img = $(selector).find('img');
                        // var imgSrc = img.attr('src');
                        // img.remove();
                        $(".projector-screen").html("");
                        $projectorLight.removeClass('animatelighttrigger');
                        $projectorScreen.removeClass('activateProjectorScreen');
                        $imageBlock.css({
                            filter: 'none'
                        });
                        $reloadBtn.hide(0);
                        $stopBtn.hide(0);
                        $tools.on('click', handleToolClick);
                        $tools.addClass('active');
                    });
                }

                break;
		}

        function animateType($el, data) {
           // if(isFirefox){
           //  var index = 0;
           //  var timer = setInterval(function() {
           //      if (!data[index]) {
           //          clearInterval(timer);
           //          $smallnextbtn.show(0);
           //      }
           //      $el.append(data[index++])
           //  }, 100);
           // }
           // else{
            //alert("slkfj");
            $el.html(data);
            var timer = setInterval(function() {
                    clearInterval(timer);
                    $smallnextbtn.show(0);
            }, 2000);
           // }

        }

	}
  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
  function sound_player(sound_id, next){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function(){
      nav_button_controls(0);
    });
  }
  function sound_nav(sound_id, next){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page,countNext+1);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        var $tableBlock = $('.tableblock');
		countNext--;
		templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


    total_page = content.length;
	// templateCaller();

});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";


	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring");

			texthighlightstarttag = "<span>";


			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


			$(this).html(replaceinstring);
		});
	}
}
/*=====  End of data highlight function  ======*/

//page 1

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var samePageFlag = false;
var correctCount = 0;
var islastpageflag = false;

var	content = [
    {
        //slide 0
        uppertextblock: [{
            textclass: "questext",
            textdata: data.string.p4_text1
        }],
        draggableblock: [{
            draggables: [{
                imgsrc: imgpath + 'sickle.png',
                draggableAdditionalClass: 'sickle'
            }, {
                imgsrc: imgpath + 'hammer.png',
                draggableAdditionalClass: 'hammer'
            }, {
                imgsrc: imgpath + 'scissors.png',
                draggableAdditionalClass: 'scissors'
            }, {
                imgsrc: imgpath + 'axe.png',
                draggableAdditionalClass: 'axe'
            }]
        }],
        droppableblock: [{
            droppables: [{
            }]
        }]
    },
    {
    contentblockadditionalclass: "contentwithbg",
        definitionblock: [{
            textdata: data.string.p4_text6
        }],
        imageblock: [{
            images: [{
                imgsrc: imgpath + 'cutting-paper.png',
                imgclass: 'summary-img'
            }, {
                imgsrc: imgpath + 'openbottlecap.png',
                imgclass: 'summary-img'
            }, {
                imgsrc: imgpath + 'paddy.png',
                imgclass: 'summary-img'
            }, {
                imgsrc: imgpath + 'takingwaterfromwell.png',
                imgclass: 'summary-img'
            }, {
                imgsrc: imgpath + 'woodcutting.png',
                imgclass: 'summary-img'
            }, {
                imgsrc: imgpath + 'stone.png',
                imgclass: 'summary-img'
            }]
        }]
    }
];


$(function() {
	// var height = $(window).height();
	// var width = $(window).width();
	// $("#board").css({"width": width, "height": (height*580/960)});
	// function recursion(){
	// if(data.string != null){
	// } else{
	// recursion();
	// }
	// }
	// recursion();

	function recalculateHeightWidth() {
		var heightresized = $(window).height();
		var widthresized = $(window).width();
		var factor = 960 / 580;
		var equivalentwidthtoheight = widthresized / factor;

		if (heightresized >= equivalentwidthtoheight) {
			$(".shapes_activity").css({
				"width": widthresized,
				"height": equivalentwidthtoheight
			});
		} else {
			$(".shapes_activity").css({
				"height": heightresized,
				"width": heightresized * 960 / 580
			});
		}
		// $(".shapes_activity").css({"left": "50%" ,
		// "height": "50%" ,
		// "-webkit-transform" : "translate(-50%, - 50%)",
		// "transform" : "translate(-50%, - 50%)"});

        // resizing fontsize of number with respect to numberblock
        var $num = $('p.number');
        var $numberBlock = $('.numberblock');
        var fontSize = parseInt($numberBlock.height() * 0.57)+'px';
        $num.css({'font-size': fontSize});

	}
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    var samePageCount = 1;
    var $draggables, $draggableBlock, $droppableBlock, $droppedContainer;
    var handleClick;
    var imageMap = [
        ['hammer', 'pictureframe', data.string.p4_text2],
        ['scissors', 'paper', data.string.p4_text3],
        ['axe', 'log', data.string.p4_text4],
        ['sickle', 'paddy01', data.string.p4_text5]
    ];
    var imageIndex = 0;

	loadTimelineProgress($total_page,countNext+1);

	recalculateHeightWidth();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// sounds
			{id: "sound_0", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_0_1", src: soundAsset+"p4_s0_1.ogg"},
			{id: "sound_0_2", src: soundAsset+"p4_s0_2.ogg"},
			{id: "sound_0_3", src: soundAsset+"p4_s0_3.ogg"},
			{id: "sound_1", src: soundAsset+"p4_s1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    /**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */
    function createDroppable() {
        $nextBtn.hide(0);
        $('.droppable-text').html(imageMap[imageIndex][2]);
        $droppableBlock
            .find('.droppable')
            .attr({src: imgpath + imageMap[imageIndex][1] + '.png'});
        $draggables.on('click', handleClick);
        $draggables.find('img').css({'cursor': 'pointer'});

        // Remove wrong icon
        $('.wrong-icon').remove();
    }

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
        vocabcontroller.findwords(countNext);
		$(".midtext2").css({
			"font-size":"1.7em"
		});
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        $draggables = $('.draggableblock .image-container');
        $draggableBlock = $('.draggableblock');
        $droppableBlock = $('.droppableblock');
        $droppedContainer = $('.dropped-container');

		switch (countNext) {
            case 0:
            	sound_player1("sound_"+countNext);
                createDroppable();
                samePageFlag = true;
                var left = 0;
                correctCount = 0;
                handleClick = function() {
                    var $this = $(this);
                    var $draggableImage = $this.find('.draggable');
                    console.log($draggableImage);
                    console.log(imageMap[imageIndex][0]);
                    if ($draggableImage.hasClass(imageMap[imageIndex][0])) {

                        $this.toggleClass('animateMove');
                        $draggables.off('click', handleClick);
                        $draggables.find('img').css({'cursor': 'default'});

                        setTimeout(function() {
                            $this.toggleClass('animateMove');
                            $this = $this.detach();
                            $droppedContainer.append($this);
                            $this.css({
                                'width': '30%',
                                'height': '65%',
                                'left': left + '%',
                                'top': '30%',
                                'opacity': 0
                            });
                            left += 25;
                            correctCount++;
                            $this.animate({
                                opacity: 1
                            }, 1000);
                        }, 1000);
                        createjs.Sound.stop();
                        play_correct_incorrect_sound(1);
                        $nextBtn.show(1000);
                    } else {
                        var $wrongIcon = $('<img>', {
                            src: imgpath + 'wrong.png',
                            class: 'wrong-icon'
                        });
                        createjs.Sound.stop();
                        play_correct_incorrect_sound(0);
                        $this.append($wrongIcon);
                    }
                };
                $draggables.one('click', handleClick);

                break;
            case 1:
            	sound_player("sound_"+countNext);
              break;

		}
	}
  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.lessonEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
  function sound_player(sound_id, next){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function(){
      nav_button_controls(0);
    });
  }
  function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
      if(samePageCount==1 || samePageCount==2 || samePageCount==3){
      $(".draggableblock").addClass("newpnt");
      }

		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page,countNext+1);
		// navigationController(islastpageflag);

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function() {
        console.log(samePageFlag);
        if (samePageFlag) {
            switch (samePageCount) {
                case 1:
                case 2:
                	 sound_player1("sound_0_"+ samePageCount);
                    imageIndex++;
                    createDroppable();
                    samePageCount++;
                    break;
                case 3:
                	sound_player1("sound_0_"+ samePageCount);
                    imageIndex++;
                    createDroppable();
                    samePageFlag = false;
                    countNext++;
                    break;
            }
        } else {
            templateCaller();
            // countNext++;
        }
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {var $tableBlock = $('.tableblock');
		countNext--;
		templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	// templateCaller();

});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";


	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring");

			texthighlightstarttag = "<span>";


			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


			$(this).html(replaceinstring);
		});
	}
}
/*=====  End of data highlight function  ======*/

//page 1

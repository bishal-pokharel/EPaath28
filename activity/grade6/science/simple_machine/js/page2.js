var imgpath = $ref + '/images/';
var soundAsset = $ref+"/audio_"+$lang+"/";
var sameSlideNavigation = false;
var $secondGifBox;

var content;
content = [{
    // slide 0
    uppertextblock: [{
        textdata: data.string.p2_text1
    }],
    imageblockadditionalclass: 'example',
    imageblock: [{
        images: [{
            imgsrc: imgpath + 'holding_paper.png',
            imagelabeldata: data.string.p2_text2
        }]
    }]
}, {
    // slide 1
    uppertextblock: [{
        textdata: data.string.p2_text3
    }],
}, {
    // slide 2
    imageblockadditionalclass: 'compare-example',
    imageblock: [{
        images: [{
            imgsrc: imgpath + 'lever_easy.gif',
            imagelabeldata: data.string.p2_text4,
            replay: true
        }, {
            imgsrc: imgpath + 'lever_hard.gif',
            imagelabeldata: data.string.p2_text5,
            replay: true
        }],
    }]
}, {
    // slide 3
    imageblockadditionalclass: 'compare-example',
    imageblock: [{
        images: [{
            imgsrc: imgpath + 'pulley_easy.gif',
            imagelabeldata: data.string.p2_text6,
            replay: true
        }, {
            imgsrc: imgpath + 'pulley_hard.gif',
            imagelabeldata: data.string.p2_text7,
            replay: true
        }],
    }]
}, {
    // slide 4
    imageblockadditionalclass: 'compare-example newcompare',
    imageblock: [{
        images: [{
            imgsrc: imgpath + 'wheel_easy.gif',
            imagelabeldata: data.string.p2_text8,
            replay: true
        }, {
            imgsrc: imgpath + 'wheel_hard.gif',
            imagelabeldata: data.string.p2_text9,
            replay: true
        }]
    }]
}, {
    // slide 5
    imageblockadditionalclass: 'compare-example',
    imageblock: [{
        images: [{
            imgsrc: imgpath + 'plane_easy.gif',
            imagelabeldata: data.string.p2_text10,
            replay: true
        }, {
            imgsrc: imgpath + 'plane_hard.gif',
            imagelabeldata: data.string.p2_text11,
            replay: true
        }]
    }]
}, {
    // slide 6
    imageblockadditionalclass: 'compare-example new-compare',
    imageblock: [{
        images: [{
            imgsrc: imgpath + 'door_easy.gif',
            imagelabeldata: data.string.p2_text12,
            replay: true
        }, {
            imgsrc: imgpath + 'door_hard.gif',
            imagelabeldata: data.string.p2_text13,
            replay: true
        }]
    }]
}, {
    // slide 7
    imageblockadditionalclass: 'compare-example',
    imageblock: [{
        images: [{
            imgsrc: imgpath + 'axe_easy.gif',
            imagelabeldata: data.string.p2_text14,
            replay: true
        }, {
            imgsrc: imgpath + 'wood with hammer01.gif',
            imagelabeldata: data.string.p2_text15,
            replay: true
        }]
    }]
}, {
    // slide 8
    centertextblock: [{
        textdata: data.string.p2_text16
    }],
    contentblockadditionalclass: 'summary',
    imageblockadditionalclass: 'summary',
    imageblock: [{
        imageblockrow: true,
        images: [{
            imgsrc: imgpath + 'hammer.png'
        }, {
            imgsrc: imgpath + 'scissors.png'
        }, {
            imgsrc: imgpath + 'lemon_squeezer.png'
        }, {
            imgsrc: imgpath + 'stapler.png'
        }]
    }, {
        imageblockrow: true,
        images: [{
            imgsrc: imgpath + 'shovel.png'
        }, {
            imgsrc: imgpath + 'screw.png'
        }]
    }, {
        imageblockrow: true,
        images: [{
            imgsrc: imgpath + 'wheelbarrow.png'
        }, {
            imgsrc: imgpath + 'lemon_squeezer.png'
        }, {
          imgclass:"newaxecss",
            imgsrc: imgpath + 'axe.png'
        }, {
            imgsrc: imgpath + 'pulley.png'
        }]
    }]
}];


// }

$(function() {
    // var height = $(window).height();.
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    recalculateHeightWidth();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_1_1", src: soundAsset+"p2_s1_1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_2_1", src: soundAsset+"p2_s2_1.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_3_1", src: soundAsset+"p2_s3_1.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_4_1", src: soundAsset+"p2_s4_1.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_5_1", src: soundAsset+"p2_s5_1.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
			{id: "sound_6_1", src: soundAsset+"p2_s6_1.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_7_1", src: soundAsset+"p2_s7_1.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    /**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);
        vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        var selector = '.imageblock.compare-example';
        $secondGifBox = $(selector).find('div').eq(1);

        switch (countNext) {
        	case 0:
        		sound_player("sound_"+ countNext);
        		break;
        	case 1:
        		sound_player("sound_"+ countNext);
				    break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
           		sound_player("sound_"+ countNext);
                showExample();
                handleReloadAction(0);
                handleReloadAction(1);
                $secondGifBox.css({'opacity': 0});
                sameSlideNavigation = true;
                break;
            case 8:
            	sound_player("sound_"+ countNext);
                showExample();
                handleReloadAction(0);
                handleReloadAction(1);
                $secondGifBox.css({'opacity': 0});
                sameSlideNavigation = false;
                break;
            default:
                break;
        }

        function showExample() {
            $secondGifBox.css({'opacity': 1});
        }

        function handleReloadAction(index) {
          $('.reload-btn').eq(index).on('click', function() {
              var selector = '.imageblock.compare-example';
              var div = $(selector).find('div').eq(index);
              var img = div.find('img');
              var timestamp = new Date().getTime();
              var imgSrc = img.attr('src')+'?'+timestamp;
              img.remove();
              var newImg = $('<img>', {src: imgSrc});
              div.append(newImg);
          });
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}
  	function sound_player(sound_id, next){
      console.log(sound_id);
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls(0);
  		});
  	}
    function sound_nav(sound_id, next){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
    }
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

    function handleSameSlideNavigation() {
      $nextBtn.hide(0);
      $prevBtn.hide(0);
        switch(countNext) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            	sound_player("sound_"+countNext+"_1");
                $secondGifBox.css({'opacity': 1});
                sameSlideNavigation = false;
                break;
        }
    }

    $nextBtn.on("click", function() {
        if (sameSlideNavigation) {
            handleSameSlideNavigation();
        } else {
            countNext++;
            templateCaller();
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
         if (sameSlideNavigation) {
            handleSameSlideNavigation();
        } else {
            countNext--;
            templateCaller();
        }

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);

});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span class=" + stylerulename + ">";

    		/* if footerNotificationHandler pageEndSetNotification was called then on click of
    		 previous slide button hide the footernotification */
    		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

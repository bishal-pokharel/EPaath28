Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
}

var imgpath = $ref + "/images/";

var content = [

    //ex1
    {
        exerciseblock: [{
            textdata: data.string.exe_text1,
            quesimg: true,
            ques_img: imgpath + "log.png",
            imageoptions: [{
                    forshuffle: "class4",
                    imgsrc: imgpath + "pulley.png"
                },
                {
                    forshuffle: "class2",
                    imgsrc: imgpath + "shovel.png"
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "screw.png"
                },
                {
                    forshuffle: "class1",
                    imgsrc: imgpath + "axe.png"
                }
            ],
            imageblockadditionalclass: 'display_image',
            imageblock: [{
                imgclass: 'hint_image',
                imgsrc: imgpath + "axe_easy.gif",
            }]

        }]
    },
    //ex2
    {
        exerciseblock: [{
            textdata: data.string.exe_text2,
            quesimg: true,
            sumanchangediv:"newimg1",
            ques_img: imgpath + "takingwaterfromwell.png",
            imageoptions: [{
                    forshuffle: "class1",
                    imgsrc: imgpath + "pulley.png"
                },
                {
                    forshuffle: "class2",
                    imgsrc: imgpath + "shovel.png"
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "screw.png"
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "axe.png"
                }
            ],
            imageblockadditionalclass: 'display_image',
            imageblock: [{
                imgclass: 'hint_image',
                imgsrc: imgpath + "pulley_easy.gif",
            }]

        }]
    },
    //ex3
    {
        exerciseblock: [{
            textdata: data.string.exe_text3,
            quesimg: true,
            ques_img: imgpath + "taking_rice.png",
            imageoptions: [{
                    forshuffle: "class2",
                    imgsrc: imgpath + "pulley.png"
                },
                {
                    forshuffle: "class1",
                    imgsrc: imgpath + "wheelbarrow.png"
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "screw.png"
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "axe.png"
                }
            ],
            imageblockadditionalclass: 'display_image',
            imageblock: [{
                imgclass: 'hint_image',
                imgsrc: imgpath + "takinhome.gif",
            }]
        }]
    },
    //ex4
    {
        exerciseblock: [{
            textdata: data.string.exe_text4,
            quesimg: true,
            ques_img: imgpath + "nailwall.png",
            imageoptions: [{
                    forshuffle: "class2",
                    imgsrc: imgpath + "pulley.png"
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "wheelbarrow.png"
                },
                {
                    forshuffle: "class1",
                    imgsrc: imgpath + "hammer.png"
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "axe.png"
                }
            ],
            imageblockadditionalclass: 'display_image',
            imageblock: [{
                imgclass: 'hint_image',
                imgsrc: imgpath + "takinnail.gif",
            }]
        }]
    },
    //ex5
    {
        exerciseblock: [{
            textdata: data.string.exe_text5,
            quesimg: true,
            ques_img: imgpath + "cutting crop.png",
            imageoptions: [{
                    forshuffle: "class2",
                    imgsrc: imgpath + "pulley.png"
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "wheelbarrow.png"
                },
                {
                    forshuffle: "class1",
                    imgsrc: imgpath + "sickle.png"
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "axe.png"
                }
            ],
            imageblockadditionalclass: 'display_image',
            imageblock: [{
                imgclass: 'hint_image',
                imgsrc: imgpath + "cutting crop.gif",
            }]
        }]
    },
    //ex6
    {
        exerciseblock: [{
            textdata: data.string.exe_text6,
            quesimg: true,
            ques_img: imgpath + "plane_hard.gif",
            imageoptions: [{
                    forshuffle: "class2",
                    imgsrc: imgpath + "pulley.png"
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "wheelbarrow.png"
                },
                {
                    forshuffle: "class1",
                    imgsrc: imgpath + "plane_easy.gif"
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "axe.png"
                }
            ],
            imageblockadditionalclass: 'display_image',
            imageblock: [{
                imgclass: 'hint_image',
                imgsrc: imgpath + "pushing tractor02.gif",
            }]
        }]
    },
    //ex7
    {
        exerciseblock: [{
            textdata: data.string.exe_text7,
            quesimg: true,
            ques_img: imgpath + "lever_hard.gif",
            imageoptions: [{
                    forshuffle: "class2",
                    imgsrc: imgpath + "pulley.png"
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "wheelbarrow.png"
                },
                {
                    forshuffle: "class1",
                    imgsrc: imgpath + "stone.png"
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "axe.png"
                }
            ],
            imageblockadditionalclass: 'display_image',
            imageblock: [{
                imgclass: 'hint_image',
                imgsrc: imgpath + "lever_easy.gif",
            }]
        }]
    }, {
        exerciseblock: [{
            textdata: data.string.exe_text8,
            quesimg: true,
            ques_img: imgpath + "dalo.png",
            imageoptions: [{
                    forshuffle: "class1",
                    imgsrc: imgpath + "pulley.png"
                },
                {
                    forshuffle: "class2",
                    imgsrc: imgpath + "shovel.png"
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "screw.png"
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "axe.png"
                }
            ],
            imageblockadditionalclass: 'display_image',
            imageblock: [{
                imgclass: 'hint_image',
                imgsrc: imgpath + "dalo.gif",
            }]

        }]
    }
];
//
// /*remove this for non random questions*/
content.shufflearray();


$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    /*for limiting the questions to 10*/
    var $total_page = content.length;
    /*var $total_page = content.length;*/

    function navigationcontroller(islastpageflag) {
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
            alert("NavigationController : Hi Master, please provide a boolean parameter") :
            null;
    }

    var score = 0;

    /*values in this array is same as the name of images of eggs in image folder*/
    var testin = new EggTemplate();

        //eggTemplate.eggMove(countNext);
    testin.init(8);
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        $nextBtn.hide(0);
        $prevBtn.hide(0);

        /*generate question no at the beginning of question*/
        $('#num_ques').html(countNext + 1 + '. ');

        /*for randomizing the options*/
        var parent = $(".droparea");
        var divs = parent.children();
        while (divs.length) {
            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
        }


        /*======= SCOREBOARD SECTION ==============*/

        /*random scoreboard eggs*/
        var ansClicked = false;
        var wrngClicked = false;

        $(".checkhere").click(function() {
            $(this).removeClass('forhoverimg');
            if (ansClicked == false) {

                /*class 1 is always for the right answer. updates scoreboard and disables other click if
                right answer is clicked*/
                if ($(this).hasClass("class1")) {
                    if (wrngClicked == false) {
                        testin.update(true);
                    }
                    play_correct_incorrect_sound(1);
                    $(this).css("background", "#6AA84F");
                    $(this).css("border", "5px solid #B6D7A8");
                    $(this).siblings(".corctopt").show(0);
                    $(".questionimg").css("display", "none")
                    $(".display_image").css("display", "block");

                    $('.checkhere').removeClass('forhoverimg');
                    ansClicked = true;

                    if (countNext != $total_page) {
                        setTimeout(function() {
                            $nextBtn.show(0);
                        }, 2000);
                    }

                } else {
                    testin.update(false);
                    play_correct_incorrect_sound(0);
                    $(this).css("background", "#EA9999");
                    $(this).css("border", "5px solid #efb3b3");
                    $(this).css("color", "#F66E20");
                    $(this).siblings(".wrngopt").show(0);
                    wrngClicked = true;
                }
            }
        });

        /*======= SCOREBOARD SECTION ==============*/
    }


    function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
        testin.gotoNext();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        	previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

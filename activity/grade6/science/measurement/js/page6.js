var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_dg1 = new buzz.sound((soundAsset + "p6_s0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p6_s1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p6_s2_1.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p6_s2_2.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p6_s3_1.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p6_s3_2.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p6_s4_1.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p6_s4_2.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p6_s5.ogg"));


var content=[
{
	//slide0
	uppertextblock : [
	{
		textclass : 'text2a fadein',
		textdata : data.string.p6text1
	}
	],
},
{
	//slide1
	additionalclasscontentblock:'mousebg',
	uppertextblock:[
		{
			textclass: 'surubutton',
			textdata : data.string.p6text2
		},
		{
			textclass: 'ratsprite'
		}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "point1",
				imgsrc : imgpath + "flag.png"

			},
			{
				imgclass : "point2",
				imgsrc : imgpath + "flag.png"

			}
		]
	}],
},
{
	//slide2
	additionalclasscontentblock:'mousebg',
	uppertextblock:[
		{
			textclass: 'desc1 fadein',
			textdata : data.string.p6text4
		},
		{
			textclass: 'desc2 fadein1',
			textdata : data.string.p6text5
		},
		{
			textclass: 'ratsprite1'
		}
	]
},
{
	//slide3
	uppertextblock:[
		{
			textclass: 'desc1 fadein',
			textdata : data.string.p6text6
		},
		{
			textclass: 'desc2 fadein',
			textdata : data.string.p6text7
		}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "clock fadein",
				imgsrc : imgpath + "clock.png"

			}
		]
	}]
},
{
	//slide4
	uppertextblock:[
		{
			textclass: 'centertext fadein',
			textdata : data.string.p6text19
		},
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text20
		}
	],
},
{
	//slide5
	second:data.string.p6text10,
	minute:data.string.p6text11,
	hour:data.string.p6text12,
	day:data.string.p6text13,
	convert:data.string.p6text13a,
	reset:data.string.p6text13b,
	number:data.string.p4text18,
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p6text8
		}
	],
	dropdownselector:[{
	}]

}];


$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$nextBtn.hide(0);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		$(".menuroll").one("click",function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 6)
				$nextBtn.show(0);
		});
		switch (countNext) {
			case 0:
				soundplayer(sound_dg1);
				break;
			case 1:
				soundplayer(sound_dg2,1);

				$('.surubutton').click(function(){

					$('.ratsprite').animate({
						left: "80%"
					},6000).addClass('walking').addClass('walking');

					setTimeout( function(){ $( ".ratsprite" ).removeClass('walking');
					$nextBtn.show(50);
					 }, 6000 );

					$(this).fadeOut(200);

				});

				break;
			case 2:
			$('.desc2').removeClass('fadein1').css({'opacity':'1'}).hide();
			buzz.all().stop();

			sound_dg3.play().bind("ended",function(){
				$('.desc2').show(200);
				sound_dg4.play().bind("ended",function(){
					navigationcontroller();
				});
			});
				break;
			case 3:
				buzz.all().stop();

				$('.desc2').removeClass('fadein1').css({'opacity':'1'}).hide();

				sound_dg5.play().bind("ended",function(){
					$('.desc2').show(200);
					sound_dg6.play().bind("ended",function(){
						navigationcontroller();
					});
				});
					break;
			case 4:
				buzz.all().stop();
				$('.centertext').removeClass('fadein').css({'opacity':'1'}).hide();
				sound_dg7.play().bind("ended",function(){
					$('.centertext').show(200);
					sound_dg8.play().bind("ended",function(){
						navigationcontroller();
					});
				});
				break;
			case 5:
				ole.footerNotificationHandler.hideNotification();
				soundplayer(sound_dg9,1);
				$('.convertbutton').click(function(){
					ole.footerNotificationHandler.lessonEndSetNotification();
					$('.inputclass').css("pointer-events","none");
					if($('#time').val()=='sec')
					{
						var input = parseInt($('.inputclass').val());
						$('.sec1').html(input + '');
						var min = input / 60;
						var min2 = (min+'').expandExponential();
						$('.min1').html(min2 + '');
						var hour = input / 3600 ;
						var hour2 = (hour+'').expandExponential();
						$('.hour1').html(hour2 + '');
						var day = input / 86400 ;
						var day2 = (day+'').expandExponential();
						$('.day1').html(day2 + ' ');
					}
					if($('#time').val()=='min')
					{
						var input = parseInt($('.inputclass').val());
						var sec = input * 60;
						var sec2 = (sec+'').expandExponential();
						$('.sec1').html(sec2 + ' ');
						$('.min1').html(input + ' ');
						var hour = input / 60 ;
						var hour2 = (hour+'').expandExponential();
						$('.hour1').html(hour2 + ' ');
						var day = input / 1440 ;
						var day2 = (day+'').expandExponential();
						$('.day1').html(day2 + ' ');
					}
					if($('#time').val()=='hour')
					{
						var input = parseInt($('.inputclass').val());
						var sec = input * 3600;
						var sec2 = (sec+'').expandExponential();
						$('.sec1').html(sec2 + ' ');
						var min = input * 60;
						var min2 = (min+'').expandExponential();
						$('.min1').html(min2 + ' ');
						$('.hour1').html(input + ' ');
						var day = input / 24 ;
						var day2 = (day+'').expandExponential();
						$('.day1').html(day2 + ' ');
					}
					if($('#time').val()=='day')
					{
						var input = parseInt($('.inputclass').val());
						var sec = input * 86400;
						var sec2 = (sec+'').expandExponential();
						$('.sec1').html(sec2 + ' ');
						var min = input * 1440;
						var min2 = (min+'').expandExponential();
						$('.min1').html(min2 + ' ');
						var hour = input * 24 ;
						var hour2 = (hour+'').expandExponential();
						$('.hour1').html(hour2 + ' ');
						$('.day1').html(input + ' ');
					}

				});
				$('.reset').click(function(){
					$('.inputclass').css("pointer-events","auto");
					$('.inputclass').closest('form').find("input[type=text], textarea").val("");
					$('.sec1').html('. ');
					$('.min1').html(' .');
					$('.hour1').html(' .');
					$('.day1').html(' .');
				});
				break;



		}
	}

	String.prototype.expandExponential = function(){
    return this.replace(/^([+-])?(\d+).?(\d*)[eE]([-+]?\d+)$/, function(x, s, n, f, c){
        var l = +c < 0, i = n.length + +c, x = (l ? n : f).length,
        c = ((c = Math.abs(c)) >= x ? c - x + l : 0),
        z = (new Array(c + 1)).join("0"), r = n + f;
        return (s || "") + (l ? r = z + r : r += z).substr(0, i += l ? z.length : 0) + (i < r.length ? "." + r.substr(i) : "");
    });
	};


	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
				if(!next) navigationcontroller();
		});
	}


	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_dg1 = new buzz.sound((soundAsset + "p4_s0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p4_s1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p4_s5.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p4_s6.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p4_s7.ogg"));


var content=[
{
	//slide0
	uppertextblock : [
	{
		textclass : 'text2a fadein',
		textdata : data.string.p4text1
	}
	],
},
{
	//slide1
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text2
		}
	],
	snapper: 'snapper',
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "pep fadein",
				imgsrc : imgpath + "door.png"

			},
			{
				imgclass: "draggable ruler",
				imgsrc : imgpath + "inchtape.png"
			},
			{
				imgclass: "meterimage",
				imgsrc : imgpath + "zoom_in.png"
			}
		]
	}],
},

{
	//slide2
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text3
		}
	],
	snapper: 'snapper',
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "pep fadein",
				imgsrc : imgpath + "door.png"

			},
			{
				imgclass: "ruler1",
				imgsrc : imgpath + "inchtape.png"
			},
			{
				imgclass: "meterimage",
				imgsrc : imgpath + "zoom_in.png"
			}
		]
	}]
},
{
	//slide3
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text6
		}
	],
	snapper: 'snapper',
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "pep fadein",
				imgsrc : imgpath + "1cm.png"

			}
		]
	}]
},
{
	//slide4
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text9a
		}
	],
	snapper: 'snapper',
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "pep fadein",
				imgsrc : imgpath + "table.png"

			}
		]
	}]
},
{
	//slide5
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text9b
		}
	],
	snapper: 'snapper',
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "pep fadein",
				imgsrc : imgpath + "bridge.png"

			},
			{
				imgclass : "pep1 fadein",
				imgsrc : imgpath + "bridge01.png"

			}
		]
	}]
},
// {
// 	//slide6
// 	uppertextblock:[
// 		{
// 			textclass: 'text2 fadein',
// 			textdata : data.string.p4text9c
// 		}
// 	],
// 	snapper: 'snapper',
// 	imageblock: [
// 	{
// 		imagetoshow: [
// 			{
// 				imgclass : "pep1 fadein",
// 				imgsrc : imgpath + "bridge01.png"
//
// 			}
// 		]
// 	}]
// },
{
	//slide7
	uppertextblock:[
		{
			textclass: 'centertext fadein',
			textdata : data.string.p4text19
		},
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text20
		}
	],
},
{
	//slide8
	mm:data.string.p4text10,
	cm:data.string.p4text11,
	m:data.string.p4text12,
	km:data.string.p4text13,
	convert:data.string.p4text13a,
	reset:data.string.p4text13b,
	milimiters:data.string.p4text14,
	centimeters:data.string.p4text15,
	meters:data.string.p4text16,
	kilometers:data.string.p4text17,
	number:data.string.p4text18,
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text9
		}
	],
	dropdownselector:[{

	}]

}];


$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$nextBtn.hide(0);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		$(".menuroll").one("click",function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 6)
				$nextBtn.show(0);
		});

		soundvalue = eval('sound_dg'+(countNext+1))
		if(countNext!=1&&countNext!=7) soundplayer(soundvalue);

		switch (countNext) {
			case 1:
				soundvalue = eval('sound_dg'+(countNext+1))
				soundplayer(soundvalue,1);
				$(".draggable").draggable({
		 			containment : ".generalTemplateblock",
		 			cursor : "grabbing",
		 			zIndex: 1000,
		 			snap: ".snapper",
		 			snapTolerance: 195,
		 			snapMode: "inner",
		 		});
		 		$( ".droppable" ).droppable({
			      drop: function( event, ui ) {
			      	$('.meterimage').addClass('fadein');
			        $( '.ruler' ).css("pointer-events","none");
							navigationcontroller();
			      }
			    });
				break;
			case 3:
				$('.why1').animate({'border-radius':'1em'}, 1500);
				break;
			case 4:
				$('.why1').animate({'border-radius':'1em'}, 1500);
				$('.why2').animate({'border-radius':'1em'}, 1500);
				break;
			case 5:
				$('.why1').animate({'border-radius':'1em'}, 1500);
				$('.pep').css({"top":"41%"});
				$('.pep1').css({"top":"75%"});
				break;
			case 7:
				ole.footerNotificationHandler.hideNotification();
				soundplayer(sound_dg8,1);
				$('.convertbutton').click(function(){
					ole.footerNotificationHandler.pageEndSetNotification();
					$('.inputclass').css("pointer-events","none");
					if($('#length').val()=='mm')
					{
						var input = parseInt($('.inputclass').val());
						$('.mm1').html(input + ' ');
						var cm = input / 10 ;
						$('.cm1').html(cm + ' ');
						var m = input / 1000 ;
						$('.m1').html(m + ' ');
						var km = input / 1000000 ;
						$('.km1').html(km + ' ');
					}
					if($('#length').val()=='cm')
					{
						var input = parseInt($('.inputclass').val());
						var mm = input * 10 ;
						$('.mm1').html(mm + ' ');
						$('.cm1').html(input + ' ');
						var m = input / 100 ;
						$('.m1').html(m + ' ');
						var km = input / 100000 ;
						$('.km1').html(km + ' ');
					}
					if($('#length').val()=='m')
					{
						var input = parseInt($('.inputclass').val());
						var mm = input * 1000 ;
						$('.mm1').html(mm + ' ');
						var cm = input * 10 ;
						$('.cm1').html(cm + ' ');
						$('.m1').html(input + ' ');
						var km = input / 1000 ;
						$('.km1').html(km + ' ');
					}
					if($('#length').val()=='km')
					{
						var input = parseInt($('.inputclass').val());
						var mm = input * 1000000 ;
						$('.mm1').html(mm + ' ');
						var cm = input * 100000 ;
						$('.cm1').html(cm + ' ');
						var m = input *1000 ;
						$('.m1').html(m + ' ');
						$('.km1').html(input + ' ');
					}
				});
				$('.reset').click(function(){
					$('.inputclass').css("pointer-events","auto");
					$('.inputclass').closest('form').find("input[type=text], textarea").val("");
					$('.mm1').html(' .');
					$('.cm1').html('. ');
					$('.m1').html(' .');
					$('.km1').html(' .');
				});

				break;


		}
	}

	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
				if(!next) navigationcontroller();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

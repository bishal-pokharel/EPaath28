var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_dg1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p2_s7_1.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p2_s7_2.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "p2_s7_3.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "p2_s8_1.ogg"));
var sound_dg12 = new buzz.sound((soundAsset + "p2_s8_2.ogg"));
var sound_dg13 = new buzz.sound((soundAsset + "p2_s8_3.ogg"));




var content=[
{
	//slide0
	uppertextblock : [
	{
		textclass : 'text2 fadein',
		textdata : data.string.p2text1
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "heat",
				imgsrc : imgpath + "measuring-human-temperature.png"

			},
			{
				imgclass : "pathi",
				imgsrc : imgpath + "paathi.png"

			},
			{
				imgclass : "mana",
				imgsrc : imgpath + "maana.png"

			},
			{
				imgclass : "handd",
				imgsrc : imgpath + "measuringwithhand.png"

			}
		]
	}],
},
{
	//slide1
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p2text2
		}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "pep fadein",
				imgsrc : imgpath + "people.png"

			}
		]
	}],
},

{
	//slide2
	uppertextblock:[
		{
			textclass: 'text2 opacity1',
			textdata : data.string.p2text2
		},
		{
			textclass: 'text3 fadein',
			textdata : data.string.p2text3
		}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "pep opacity1",
				imgsrc : imgpath + "people.png"

			}
		]
	}],
},
{
	//slide3
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p2text4
		},
		{
			textclass: 'why1 fadein',
			textdata : data.string.p2text5
		}
	]
},

{
	//slide4
	uppertextblock:[
		{
			textclass: 'text2 opacity1',
			textdata : data.string.p2text4
		},
		{
			textclass: 'why1 opacity1',
			textdata : data.string.p2text5
		},
		{
			textclass: 'why2 fadein',
			textdata : data.string.p2text6
		}
	]
},
{
	//slide5
	uppertextblock:[
		{
			textclass: 'text2 opacity1',
			textdata : data.string.p2text4
		},
		{
			textclass: 'why1 fadein',
			textdata : data.string.p2text7
		},
		{
			textclass: 'wieght1kg fadein',
			textdata : data.string.p2textkg
		}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "gorkha fadein",
				imgsrc : imgpath + "1kg-apple.png"

			}
		]
	}],
},

{
	//slide6
	uppertextblock:[
		{
			textclass: 'text2 opacity1',
			textdata : data.string.p2text4
		},
		{
			textclass: 'why1 fadein',
			textdata : data.string.p2text8
		}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "gorkha fadein",
				imgsrc : imgpath + "1mtr-dhaka.png"

			},
			{
				imgclass : "zoommeter",
				imgsrc : imgpath + "zoom_in.png"

			}
		]
	}],
},
{
	//slide7
	uppertextblock:[
		{
			textclass: 'text5 opacity1',
			textdata : data.string.p2text9
		},
		{
			textclass: 'why3 fadein',
			textdata : data.string.p2text10
		},
		{
			textclass: 'why4 fadein1',
			textdata : data.string.p2text11
		},
		{
			textclass: 'dot1',
			textdata : '.'
		},
		{
			textclass: 'dot2',
			textdata : '.'
		}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "scale",
				imgsrc : imgpath + "inchtape.png"

			},
			{
				imgclass : "scale1",
				imgsrc : imgpath + "inchtape.png"

			},
			{
				imgclass : "tape",
				imgsrc : imgpath + "inchtape.png"

			},
			{
				imgclass : "meter1 fadein4",
				imgsrc : imgpath + "zoom_in.png"

			},
			{
				imgclass : "meter2 fadein4",
				imgsrc : imgpath + "zoom_in.png"

			},
			{
				imgclass : "meter3 fadein4",
				imgsrc : imgpath + "zoom_in.png"

			},
			{
				imgclass : "globe",
				imgsrc : imgpath + "globe.png"

			}
		]
	}]
},
{
	//slide8
	uppertextblock:[
		{
			textclass: 'desc1 fadein',
			textdata : data.string.p2text12
		},
		{
			textclass: 'desc2 fadein',
			textdata : data.string.p2text13
		},
		{
			textclass: 'desc3 fadein',
			textdata : data.string.p2text14
		}
	]
}];


$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$nextBtn.hide(0);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		$(".menuroll").one("click",function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 6)
				$nextBtn.show(0);
		});
		if (countNext<7){
			soundvalue = eval('sound_dg'+(countNext+1))
			soundplayer(soundvalue);
		}
		switch (countNext) {

			case 3:
				$('.why1').animate({'border-radius':'1em'}, 1500);
				break;
			case 4:
				$('.why1').animate({'border-radius':'1em'}, 1500);
				$('.why2').animate({'border-radius':'1em'}, 1500);
				break;
			case 5:
				$('.why1').animate({'border-radius':'1em'}, 1500);
				break;
			case 6:
				$('.why1').animate({'border-radius':'1em'}, 1500);
				break;
			case 7:
				soundplayer(sound_dg8,1);
				setTimeout(()=>soundplayer(sound_dg9,1),5000);
				setTimeout(()=>soundplayer(sound_dg10),13000);

				$('.why3,.why4,.desc2,.desc1').animate({'border-radius':'1em'}, 1500);
				break;
			case 8:
			buzz.all().stop();
			clearTimeout();
			ole.footerNotificationHandler.hideNotification();
			$('.desc2,.desc3').hide(0);
				sound_dg11.play().bind("ended",function(){
					$('.desc2').show(200);
					sound_dg12.play().bind("ended",function(){
						$('.desc3').show(200);
						sound_dg13.play().bind("ended",function(){
							navigationcontroller();
						});
					});
				});

				$('.why1').animate({'border-radius':'1em'}, 1500);
				break;

		}
	}

	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
			if(!next) navigationcontroller();
		});
	}


	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_dg1 = new buzz.sound((soundAsset + "p5_s0.ogg"));
var sound_dg2_1 = new buzz.sound((soundAsset + "p5_s1_1.ogg"));
var sound_dg2_2 = new buzz.sound((soundAsset + "p5_s1_2.ogg"));
var sound_dg2_3 = new buzz.sound((soundAsset + "p5_s1_3.ogg"));
var sound_dg2_4 = new buzz.sound((soundAsset + "p5_s1_4.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p5_s2.ogg"));
var sound_dg4= new buzz.sound((soundAsset + "p5_s3.ogg"));



var content=[
{
	//slide0
	uppertextblock : [
	{
		textclass : 'text2a fadein',
		textdata : data.string.p5text1
	}
	],
},
{
	//slide1
	uppertextblock:[
		{
			textclass: 'desc1 fadein',
			textdata : data.string.p5text2
		},
		{
			textclass: 'desc2 fadein1',
			textdata : data.string.p5text3
		},
		{
			textclass: 'desc3 fadein1',
			textdata : data.string.p5text4
		},
		{
			textclass: 'desc4 fadein2',
			textdata : data.string.p5text5
		}
	]
},
{
	//slide2
	uppertextblock:[
		{
			textclass: 'centertext fadein',
			textdata : data.string.p5text19
		},
		{
			textclass: 'text2 fadein',
			textdata : data.string.p4text20
		}
	],
},
{
	//slide3
	gm:data.string.p5text10,
	kg:data.string.p5text11,
	quintal1:data.string.p5text12,
	tonne1:data.string.p5text13,
	convert:data.string.p5text13a,
	reset:data.string.p5text13b,
	grams:data.string.p5text14,
	kilograms:data.string.p5text15,
	quintal:data.string.p5text16,
	tonne:data.string.p5text17,
	number:data.string.p4text18,
	uppertextblock:[
		{
			textclass: 'text2 fadein',
			textdata : data.string.p5text6
		}
	],
	dropdownselector:[{
	}]

}];


$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$nextBtn.hide(0);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		$(".menuroll").one("click",function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 6)
				$nextBtn.show(0);
		});
		if (countNext!=1&&countNext!=3){
			soundvalue = eval('sound_dg'+(countNext+1))
			soundplayer(soundvalue);
		}
		switch (countNext) {
			case 1:
			$('.desc2,.desc3').removeClass('fadein1').css({"opacity":"1"});
			$('.desc4').removeClass('fadein2').css({"opacity":"1"});
			$('.desc2,.desc3,.desc4').hide(0);

					sound_dg2_1.play().bind("ended",function(){
						$('.desc2').show(200);
						sound_dg2_2.play().bind("ended",function(){
							$('.desc3').show(200);
							sound_dg2_3.play().bind("ended",function(){
								$('.desc4').show(200);
								sound_dg2_4.play().bind("ended",function(){
									navigationcontroller();
								});
							});
						});
					});
					break;

			case 3:
				ole.footerNotificationHandler.hideNotification();
				soundplayer(sound_dg4,1);

				$('.convertbutton').click(function(){
					ole.footerNotificationHandler.pageEndSetNotification();
					$('.inputclass').css("pointer-events","none");
					if($('#weight').val()=='gm')
					{
						var input = parseInt($('.inputclass').val());
						$('.gm1').html(input + ' ');
						var kg = input / 1000;
						var kg2 = (kg+'').expandExponential();
						$('.kg1').html(kg2 + ' ');
						var quintal = input / 10000 ;
						var quintal2 = (quintal+'').expandExponential();
						$('.qu1').html(quintal2 + ' ');
						var ton = input / 1000000 ;
						var ton2 = (ton+'').expandExponential();
						$('.ton1').html(ton2 + ' ');
					}
					if($('#weight').val()=='kg')
					{
						var input = parseInt($('.inputclass').val());
						var gm = input * 1000;
						var gm2 = (gm+'').expandExponential();
						$('.gm1').html(gm2 + ' ');
						$('.kg1').html(input + ' ');
						var quintal = input * .01;
						var quintal2 = (quintal+'').expandExponential();
						$('.qu1').html(quintal2 + ' ');
						var ton = input * .001;
						var ton2 = (ton+'').expandExponential();
						$('.ton1').html(ton2 + ' ');
					}
					if($('#weight').val()=='qu')
					{
						var input = parseInt($('.inputclass').val());
						var gm = input * 100000;
						var gm2 = (gm+'').expandExponential();
						$('.gm1').html(gm2 + ' ');
						var kg = input * 100;
						var kg2 = (kg+'').expandExponential();
						$('.kg1').html(kg2 + ' ');
						$('.qu1').html(input + ' ');
						var ton = input * .1;
						var ton2 = (ton+'').expandExponential();
						$('.ton1').html(ton2 + ' ');
					}
					if($('#weight').val()=='ton')
					{
						var input = parseInt($('.inputclass').val());
						var gm = input * 1000000;
						var gm2 = (gm+'').expandExponential();
						$('.gm1').html(gm2 + ' ');
						var kg = input * 1000;
						var kg2 = (kg+'').expandExponential();
						$('.kg1').html(kg2 + ' ');
						var quintal = input * 10;
						var quintal2 = (quintal+'').expandExponential();
						$('.qu1').html(quintal2 + ' ');
						$('.ton1').html(input + ' ');
					}

				});
				$('.reset').click(function(){
					$('.inputclass').css("pointer-events","auto");
					$('.inputclass').closest('form').find("input[type=text], textarea").val("");
					$('.gm1').html('.');
					$('.kg1').html('.');
					$('.qu1').html('.');
					$('.ton1').html('.');
				});
				break;

			case 7:
			$('.why3,.why4,.desc2,.desc1').animate({'border-radius':'1em'}, 1500);
			setTimeout(function(){
					$nextBtn.show(1000);
					},12500);
			break;
			case 8:
			setTimeout(function(){
					$nextBtn.show(0);
					},1000);
			$('.why1').animate({'border-radius':'1em'}, 1500);
			break;

			case 9:
			setTimeout(function(){
					$nextBtn.show(0);
					},1000);
			$('.why1').animate({'border-radius':'1em'}, 1500);
			$('.why2').animate({'border-radius':'1em'}, 1500);
			break;

		}
	}

	String.prototype.expandExponential = function(){
    return this.replace(/^([+-])?(\d+).?(\d*)[eE]([-+]?\d+)$/, function(x, s, n, f, c){
        var l = +c < 0, i = n.length + +c, x = (l ? n : f).length,
        c = ((c = Math.abs(c)) >= x ? c - x + l : 0),
        z = (new Array(c + 1)).join("0"), r = n + f;
        return (s || "") + (l ? r = z + r : r += z).substr(0, i += l ? z.length : 0) + (i < r.length ? "." + r.substr(i) : "");
    });
	};


		function soundplayer(i,next){
			buzz.all().stop();
			i.play().bind("ended",function(){
					if(!next) navigationcontroller();
			});
		}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

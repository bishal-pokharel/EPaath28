var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';
var sound_0 = new buzz.sound(soundAsset+"exer.ogg");

var content=[
	//slide 0
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.et5,
			textclass : 'weight',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'machine01.png'
		},
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option1 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et1
		},
		{
			optionclass:"option2 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et2
		},
		{
			optionclass:"option3 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et3
		}]
	},
	//slide1
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.et9,
			textclass : 'weight',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'machine01.png'
		},
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option3 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et6
		},
		{
			optionclass:"option2 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et7
		},
		{
			optionclass:"option1 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et8
		}]
	},
	//slide2
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.et10,
			textclass : 'inst',
		},
		{
			textclass:'ratsprite1'
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'road.png'
		},
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option3 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et11
		},
		{
			optionclass:"option1 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et13
		},
		{
			optionclass:"option2 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et12
		}]
	},
	//slide3
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'dhakatopi.png'
		},
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option3 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et16
		},
		{
			optionclass:"option2 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et15
		},
		{
			optionclass:"option1 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et14
		}]
	},
	//slide4
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		,
		{
			textdata : data.string.et17,
			textclass : 'length',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'table.png'
		},
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option3 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et20
		},
		{
			optionclass:"option2 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et19
		},
		{
			optionclass:"option1 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et18
		}]
	},
	//slide5
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		,
		{
			textdata : data.string.et21,
			textclass : 'height',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'pasang.png'
		},
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option1 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et22
		},
		{
			optionclass:"option2 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et23
		},
		{
			optionclass:"option3 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et24
		}]
	},
	//slide6
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.et25,
			textclass : 'weight',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'machine01.png'
		},
		{
			imgclass:'centerimage1',
			imgsrc:imgpath + 'apple.png'
		},
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option1 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et26
		},
		{
			optionclass:"option3 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et28
		},
		{
			optionclass:"option2 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et27
		}]
	},
	//slide7
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.et29,
			textclass : 'time',
		}
		],
		imageblock:[
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option1 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et30
		},
		{
			optionclass:"option2 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et31
		},
		{
			optionclass:"option3 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et32
		}]
	},
	//slide8
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.et33,
			textclass : 'length ',
		}
		],
		imageblock:[
		{
			imgclass:'centerimage',
			imgsrc:imgpath + 'distance.png'
		},
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option1 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et36
		},
		{
			optionclass:"option2 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et35
		},
		{
			optionclass:"option3 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et34
		}]
	},
	//slide9
	{
		contentblockadditionalclass:'bluebg',
		extratextblock : [
		{
			textdata : data.string.et4,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.et37,
			textclass : 'time ',
		}
		],
		imageblock:[
		{
			imgclass:'questionimage',
			imgsrc:imgpath + 'main-box.png'
		}],
		options:[{
			optionclass:"option1 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et40
		},
		{
			optionclass:"option3 options incorrect",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et38
		},
		{
			optionclass:"option2 options correct",
			imgclass:"optionimage",
			imgsrc:imgpath + 'yellow_box.png',
			textclass:"optiontext",
			textdata:data.string.et39
		}]
	}
];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();

	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		var wrong_clicked = false;
		$('.options').draggable({
                containment : ".generalTemplateblock",
                revert : "invalid",
                zIndex: 100000,
            });
		$(".questionimage").droppable({
                accept: ".correct,.incorrect",
                drop: function (event, ui){
                	if(ui.draggable.hasClass('correct'))
                	{
                		ui.draggable.css({"left": "37.4%","top": "43%"});
	                	$('.options').css({"pointer-events": "none"});
	                    $this = $(this);
		 				play_correct_incorrect_sound(1);
	                    dropfunc(event, ui, $this);
                	}
                	else{
						if(!wrong_clicked){
							wrong_clicked = true;
							rhino.update(false);
						}
		 				play_correct_incorrect_sound(0);
                		if(ui.draggable.hasClass('option1'))
                		{
            				ui.draggable.children('.optionimage').attr('src','activity/grade6/science/measurement/exercise/images/red_box.png');
                			ui.draggable.animate({"left": "6.25%",
													 "top": "72%"});
                		}
                		if(ui.draggable.hasClass('option2'))
                		{
            				ui.draggable.children('.optionimage').attr('src','activity/grade6/science/measurement/exercise/images/red_box.png');
                			ui.draggable.animate({"left": "37.5%",
													 "top": "72%"});
                		}
                		if(ui.draggable.hasClass('option3'))
                		{
            				ui.draggable.children('.optionimage').attr('src','activity/grade6/science/measurement/exercise/images/red_box.png');
                			ui.draggable.animate({"left": "68.75%",
													 "top": "72%"});
                		}


                	}


                }
            });

		function dropfunc(event, ui, $droppedOn){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$nextBtn.show(0);
            	ui.draggable.children('.optionimage').attr('src','activity/grade6/science/measurement/exercise/images/green_box.png');
		 		nav_button_controls(500);
            }
		$('.ratsprite1').animate({
			left: "60%"
		},6000).addClass('walking').addClass('walking');

		setTimeout( function(){ $( ".ratsprite1" ).removeClass('walking');
		 }, 6000 );

		switch(countNext){
			case 0:
				soundplayer(sound_0);
				break;
			case 3:
				$('.centerimage').css({"width":"35%"});
				break;
			case 4:
				$('.centerimage').css({"width":"35%","top":"16%"});
				break;
			case 5:
				$('.centerimage').css({"height":"37%","top":"14%","left":"36%","width": "17%"});
				break;
		}
	}

	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				// navigationcontroller();
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

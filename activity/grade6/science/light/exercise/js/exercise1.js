var imgpath = $ref + "/images/exe/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[

	//ex1
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques1,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q1ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q1ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-1',
						imgsrc: ""
					}
				]

			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques2,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q2ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q2ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-2',
						imgsrc: ""
					}
				]

			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques3,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q3ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q3ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-3',
						imgsrc: ""
					}
				]

			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques4,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q4ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q4ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-4',
						imgsrc: ""
					}
				]

			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques5,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q5ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q5ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-5',
						imgsrc: ""
					}
				]

			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques6,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q6ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q6ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-6',
						imgsrc: ""
					}
				]

			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques7,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q7ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q7ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-7',
						imgsrc: ""
					}
				]

			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques8,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q8ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q8ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-8',
						imgsrc: ""
					}
				]
			}
		]
	},
	//ex9
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques9,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q9ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q9ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-9',
						imgsrc: ""
					}
				]
			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques10,
        sentdata: data.string.exeins,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q10ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q10ansb,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgid : 'img-10',
						imgsrc: ""
					}
				]
			}
		]
	},
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//
			//images
			{id: "img-1", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-2", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3", src: imgpath+"03.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "img-4", src: imgpath+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-5", src: imgpath+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-6", src: imgpath+"06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-7", src: imgpath+"07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-8", src: imgpath+"08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-9", src: imgpath+"09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-10", src: imgpath+"10.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"Click_on_the_correct_answer.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new LampTemplate();

 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
    texthighlight($board);
		put_image(content, countNext);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;
		if(countNext==0){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_0");
			current_sound.play();
		}
		$(".buttonsel").click(function(){
			createjs.Sound.stop();
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
            var cortext = $(this).text().substring(3);
            $(".blankspace").text(cortext).css("color","green");

						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);

						/**
						 * TODO
						 * Instead of using css('prop_name', 'prop') multiple times better to call it once like
						 * css({
						 * 	'prop_name_1': 'prop_1',
						 * 	'prop_name_2': 'prop_2',
						 * })
						 */
						$(this).css("background","#bed62fff");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
						$(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						/**
						 * TODO
						 * Instead of using css('prop_name', 'prop') multiple times better to call it once like
						 * css({
						 * 	'prop_name_1': 'prop_1',
						 * 	'prop_name_2': 'prop_2',
						 * })
						 */
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();

		  // for (var i = 0; i < content.length; i++) {
		  //   slides(i);
		  //   $($('.totalsequence')[i]).html(i);
		  //   $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  // "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  // }
		  // function slides(i){
		  //     $($('.totalsequence')[i]).click(function(){
		  //       countNext = i;
		  //       templateCaller();
		  //       templateCaller();
		  //     });
		  //   }
	}

	function put_image(content, count){
		if(content[count].exerciseblock[0].hasOwnProperty('imageblock')){
			var imageblock = content[count].exerciseblock[0].imageblock;
				for(var i=0; i<imageblock.length; i++){
					var image_src = preload.getResult(imageblock[i].imgid).src;
					//get list of classes
					var classes_list = imageblock[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
			}
		}
	}

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

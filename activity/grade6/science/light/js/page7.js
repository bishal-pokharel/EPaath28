var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "greenbg",
		headerblockadditionalclass: "diy-header",
		headerblock:[
		{
			textdata: data.string.p7text1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "textboxcust",
				imgid : 'paper',
				imgsrc: ""
			}
		]
	}],
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "boldit",
			textclass: "pap-text",
			textdata: data.string.p7text2
		},
		{
			textclass: "pap-lst",
			textdata: data.string.p7text3
		}
	]
},
// slide1
{
	contentblockadditionalclass: "greenbg",
	singletext:[
		{
			textclass: "mid-text",
			textdata: data.string.p7text4
		}
	]
},
// slide2
{
	singletext:[
		{
			textclass: "texttype1",
			textdata: data.string.p7text5
		},
		{
			textclass: "captex-1",
			textdata: "A"
		},
		{
			textclass: "captex-2",
			textdata: "B"
		},
		{
			textclass: "captex-3",
			textdata: "C"
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover',
				imgsrc: ""
			},
			{
				imgclass: "primboard-1",
				imgid : 'priboard',
				imgsrc: ""
			},
			{
				imgclass: "primboard-2",
				imgid : 'priboard',
				imgsrc: ""
			},
			{
				imgclass: "primboard-3",
				imgid : 'priboard',
				imgsrc: ""
			},
			{
				imgclass: "hand-1",
				imgid : 'handicon',
				imgsrc: ""
			},
			{
				imgclass: "hand-2",
				imgid : 'handicon',
				imgsrc: ""
			},
			{
				imgclass: "hand-3",
				imgid : 'handicon',
				imgsrc: ""
			}
		]
	}]
},
// slide3
{
	singletext:[
		{
			textclass: "texttype1",
			textdata: data.string.p7text7
		},
		{
			textclass: "ncap-1",
			textdata: "A"
		},
		{
			textclass: "ncap-2",
			textdata: "B"
		},
		{
			textclass: "ncap-3",
			textdata: "C"
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover2',
				imgsrc: ""
			},
			{
				imgclass: "boardflow nboard-1",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "boardflow nboard-2",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "boardflow nboard-3",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "boardflow thecandle",
				imgid : 'candleoff',
				imgsrc: ""
			}
		]
	}]
},
// slide4
{
	singletext:[
		{
			textclass: "texttype1",
			textdata: data.string.p7text8
		},
		{
			textclass: "ncap-1",
			textdata: "A"
		},
		{
			textclass: "ncap-2",
			textdata: "B"
		},
		{
			textclass: "ncap-3",
			textdata: "C"
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover2',
				imgsrc: ""
			},
			{
				imgclass: "nboard-1",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "nboard-2",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "nboard-3",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "light-1",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-2",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-3",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-4",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "forw-1",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-2",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-3",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-4",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "thecandle forhover",
				imgid : 'candleoff',
				imgsrc: ""
			},
			{
				imgclass: "hand-4",
				imgid : 'handicon',
				imgsrc: ""
			},
			{
				imgclass: "hand-eye",
				imgid : 'eye',
				imgsrc: ""
			}
		]
	}]
},
// slide5
{
	singletext:[
		{
			textclass: "texttype1",
			textdata: data.string.p7text9
		},
		{
			textclass: "ncap-1",
			textdata: "A"
		},
		{
			textclass: "ncap-2",
			textdata: "B"
		},
		{
			textclass: "ncap-3",
			textdata: "C"
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover2',
				imgsrc: ""
			},
			{
				imgclass: "nboard-1",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "nboard-2",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "nboard-3",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "light-1",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-2",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-3",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-4",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "forw-1",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-2",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-3",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-4",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "thecandle",
				imgid : 'candleon',
				imgsrc: ""
			},
			{
				imgclass: "hand-eye",
				imgid : 'eye',
				imgsrc: ""
			}
		]
	}]
},
// slide6
{
	singletext:[
		{
			textclass: "texttype1",
			textdata: data.string.p7text10
		},
		{
			textclass: "ncap-1",
			textdata: "A"
		},
		{
			textclass: "ncap-2",
			textdata: "B"
		},
		{
			textclass: "ncap-3",
			textdata: "C"
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover2',
				imgsrc: ""
			},
			{
				imgclass: "nboard-1",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "nboard-2",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "nboard-3",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "light-1",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-2",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-3",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-4",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "forw-1",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-2",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-3",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-4",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "thecandle",
				imgid : 'candleoff',
				imgsrc: ""
			},
			{
				imgclass: "hand-eye",
				imgid : 'eye',
				imgsrc: ""
			},
			{
				imgclass: "wrong",
				imgid : 'wrong',
				imgsrc: ""
			}
		]
	}]
},
// slide7
{
	singletext:[
		{
			textclass: "texttype1",
			textdata: data.string.p7text11
		},
		{
			textclass: "ncap-1",
			textdata: "A"
		},
		{
			textclass: "ncap-2",
			textdata: "B"
		},
		{
			textclass: "ncap-3",
			textdata: "C"
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover2',
				imgsrc: ""
			},
			{
				imgclass: "nboard-1",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "nboard-2 nbalready",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "nboard-3",
				imgid : 'nboard',
				imgsrc: ""
			},
			{
				imgclass: "light-1",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-2",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-3",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "light-4",
				imgid : 'light',
				imgsrc: ""
			},
			{
				imgclass: "forw-1",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-2",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-3",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "forw-4",
				imgid : 'forw',
				imgsrc: ""
			},
			{
				imgclass: "thecandle forhover",
				imgid : 'candleoff',
				imgsrc: ""
			},
			{
				imgclass: "hand-4",
				imgid : 'handicon',
				imgsrc: ""
			},
			{
				imgclass: "hand-eye",
				imgid : 'eye',
				imgsrc: ""
			},
			{
				imgclass: "wrong",
				imgid : 'wrong',
				imgsrc: ""
			}
		]
	}]
},
// slide8
{
	contentblockadditionalclass: "greenbg",
	headerblockadditionalclass: "diy-header",
	headerblock:[
	{
		textdata: data.string.p7text12
	}
],
singletext:[
	{
		textclass: "forbgonly",
	},
	{
		textclass: "conc-text",
		textdata: data.string.p7text13
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "conimg",
			imgid : 'conc1',
			imgsrc: ""
		},
		{
			imgclass: "duckimg",
			imgid : 'duck',
			imgsrc: ""
		}
	]
}]
},
// slide9
{
	contentblockadditionalclass: "greenbg",
	headerblockadditionalclass: "diy-header",
	headerblock:[
	{
		textdata: data.string.p7text12
	}
],
singletext:[
	{
		textclass: "forbgonly",
	},
	{
		textclass: "conc-text",
		textdata: data.string.p7text14
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "conimg",
			imgid : 'conc2',
			imgsrc: ""
		},
		{
			imgclass: "duckimg",
			imgid : 'duck',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "paper", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover", src: imgpath+"bg_experiment.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover2", src: imgpath+"bg_experiment01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "priboard", src: imgpath+"card_board02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "holeboard", src: imgpath+"card_board01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nboard", src: imgpath+"board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "candleon", src: imgpath+"burning_candle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candleoff", src: imgpath+"not_burning_candle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "light", src: imgpath+"lightpass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "forw", src: imgpath+"arrow_green.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eye", src: imgpath+"eye.png", type: createjs.AbstractLoader.IMAGE},
			{id: "conc1", src: imgpath+"conclusion01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "conc2", src: imgpath+"conclusion02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck", src: "images/duckling-20.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p7_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p7_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p7_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p7_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p7_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p7_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p7_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p7_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p7_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p7_s9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("sound_"+countNext, 1);
			break;
			case 1:
				sound_player("sound_"+countNext, 1);
			break;
			case 2:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_"+countNext);
				current_sound.play();
				current_sound.on('complete', function(){
					$(".hand-1").fadeIn();
					$(".primboard-1").addClass("forhover");
				});
				$(".primboard-1, .hand-1").click(function(){
					if($(".primboard-1").hasClass("forhover")){
						$(".hand-1").hide(0);
						$(".hand-2").show(0);
						$(".primboard-2").addClass("forhover");
						$(".primboard-1").removeClass("forhover");
						$(".primboard-1").attr("src", preload.getResult("holeboard").src);
					}
				});

				$(".primboard-2, .hand-2").click(function(){
					if($(".primboard-2").hasClass("forhover")){
						$(".hand-2").hide(0);
						$(".hand-3").show(0);
						$(".primboard-3").addClass("forhover");
						$(".primboard-2").removeClass("forhover");
						$(".primboard-2").attr("src", preload.getResult("holeboard").src);
					}
				});

				$(".primboard-3, .hand-3").click(function(){
					if($(".primboard-3").hasClass("forhover")){
						$(".hand-3").hide(0);
						$(".primboard-3").removeClass("forhover");
						$(".primboard-3").attr("src", preload.getResult("holeboard").src);
						navigationcontroller();
					}
				});
			break;
			case 3:
				sound_player("sound_"+countNext, 1);
			break;
			case 4:
			$(".texttype1").hide(0);
				$(".thecandle, .hand-4").click(function(){
					$(".hand-4").hide(0);
					$(".thecandle, .hand-4").removeClass("forhover");
					$(".thecandle").attr("src", preload.getResult("candleon").src);
					$(".light-1").animate({
						"width": "20.3%"
					},2000, function(){
						$(".forw-1").fadeIn();
						$(".light-2").animate({
							"width": "20.3%"
						},2000, function(){
							$(".forw-2").fadeIn();
							$(".light-3").animate({
								"width": "20.3%"
							},2000, function(){
								$(".forw-3").fadeIn();
								$(".light-4").animate({
									"width": "20.3%"
								},2000, function(){
									$(".forw-4").fadeIn();
									$(".hand-eye").fadeIn();
									$(".texttype1").fadeIn();
									sound_player("sound_"+countNext, 1);
								});
							});
						});
					});
					// navigationcontroller();
				});
				break;
				case 5:
				sound_player("sound_"+countNext, 1);
				$(".light-1").css({
					"width": "20.3%"
				});
				$(".light-2").css({
					"width": "20.3%"
				});
				$(".light-3").css({
					"width": "20.3%"
				});
				$(".light-4").css({
					"width": "20.3%"
				});
				$(".hand-eye").fadeIn();
				$(".forw-1").fadeIn();

				$(".forw-2").fadeIn();

				$(".forw-3").fadeIn();

				$(".forw-4").fadeIn();
			break;
			case 6:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_"+countNext);
				current_sound.play();
				current_sound.on('complete', function(){
					$(".nboard-2").addClass("nbmove");
					$nextBtn.show(0);
				});
				break;
			case 7:
			$(".texttype1").hide(0);
				$(".thecandle, .hand-4").click(function(){
					$(".hand-4").hide(0);
					$(".thecandle, .hand-4").removeClass("forhover");
					$(".thecandle").attr("src", preload.getResult("candleon").src);
					$(".light-1").animate({
						"width": "20.3%"
					},2000, function(){
						$(".forw-1").fadeIn();
						$(".light-2").animate({
							"width": "20.3%"
						},2000, function(){
							$(".forw-2").fadeIn();
							$(".hand-eye").fadeIn();
							$(".wrong").show();
							$(".texttype1").show(0);
							sound_player("sound_"+countNext, 1);
							// sound_player("sound_1", 1);
						});
					});
					// navigationcontroller();
				});
			break;
			case 8:
				sound_player("sound_"+countNext, 1);
			break;
			case 9:
				sound_player("sound_"+countNext, 1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

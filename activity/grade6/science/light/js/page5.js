var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[
		{
			textclass: "covertext",
			textdata: data.string.diy
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'diybg',
				imgsrc: ""
			}
		]
	}]
},
//slide1
{
	contentblockadditionalclass: "purpbg",
	headerblockadditionalclass: "diy-header",
	headerblock:[
	{
		textdata: data.string.p5text1
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "painting",
			imgid : 'painting',
			imgsrc: ""
		},
		{
			imgclass: "objnew",
			imgid : 'bottle',
			imgsrc: ""
		}
	]
}],
singletext:[
	{
		textclass: "buttonsel forhover correct diybutton-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "buttonsel forhover diybutton-2",
		textdata: data.string.p4text12
	}
]
},
//slide2
{
	contentblockadditionalclass: "purpbg",
	headerblockadditionalclass: "diy-header",
	headerblock:[
	{
		textdata: data.string.p5text1
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "painting",
			imgid : 'painting',
			imgsrc: ""
		},
		{
			imgclass: "objnew",
			imgid : 'mug',
			imgsrc: ""
		},
		{
			imgclass: "objold",
			imgid : 'bottle',
			imgsrc: ""
		}
	]
}],
singletext:[
	{
		textclass: "buttonsel forhover diybutton-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "buttonsel forhover correct diybutton-2",
		textdata: data.string.p4text12
	}
]
},
//slide3
{
	contentblockadditionalclass: "purpbg",
	headerblockadditionalclass: "diy-header",
	headerblock:[
	{
		textdata: data.string.p5text1
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "painting",
			imgid : 'painting',
			imgsrc: ""
		},
		{
			imgclass: "objnew",
			imgid : 'glass',
			imgsrc: ""
		},
		{
			imgclass: "objold",
			imgid : 'mug',
			imgsrc: ""
		}
	]
}],
singletext:[
	{
		textclass: "buttonsel forhover correct diybutton-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "buttonsel forhover diybutton-2",
		textdata: data.string.p4text12
	}
]
},
//slide2
{
	contentblockadditionalclass: "purpbg",
	headerblockadditionalclass: "diy-header",
	headerblock:[
	{
		textdata: data.string.p5text1
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "painting",
			imgid : 'painting',
			imgsrc: ""
		},
		{
			imgclass: "objnew",
			imgid : 'wood',
			imgsrc: ""
		},
		{
			imgclass: "objold",
			imgid : 'glass',
			imgsrc: ""
		}
	]
}],
singletext:[
	{
		textclass: "buttonsel forhover diybutton-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "buttonsel forhover correct diybutton-2",
		textdata: data.string.p4text12
	}
]
},
//slide2
{
	contentblockadditionalclass: "purpbg",
	headerblockadditionalclass: "diy-header",
	headerblock:[
	{
		textdata: data.string.p5text1
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "painting",
			imgid : 'painting',
			imgsrc: ""
		},
		{
			imgclass: "objnew",
			imgid : 'book',
			imgsrc: ""
		},
		{
			imgclass: "objold",
			imgid : 'wood',
			imgsrc: ""
		}
	]
}],
singletext:[
	{
		textclass: "buttonsel forhover diybutton-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "buttonsel forhover correct diybutton-2",
		textdata: data.string.p4text12
	}
]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diybg", src: "images/diy_bg/a_08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "corrimg", src: "images/right.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "painting", src: imgpath+"painting.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bottle", src: imgpath+"bottle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mug", src: imgpath+"mug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glass", src: imgpath+"glasses01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wood", src: imgpath+"wood01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book", src: imgpath+"book.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p5_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p5_s1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			sound_player("sound_"+countNext);
			break;
			case 1:
			sound_player("sound_1");
			break;

		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("forhover")){
				$(this).removeClass('forhover');
					if($(this).hasClass("correct")){
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
	          $(this).css("color","white");
						// $(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						appender($(this),'corrimg');
						$('.buttonsel').removeClass('forhover forhoverimg');
						navigationcontroller();
					}
					else{
						play_correct_incorrect_sound(0);
						appender($(this),'incorrimg');
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						// $(this).siblings(".wrngopt").show(0);
					}
			}

			function appender($this, icon){
				if($this.hasClass("diybutton-1"))
					$(".coverboardfull").append("<img class='icon-one' src= '"+ preload.getResult(icon).src +"'>");
				else
					$(".coverboardfull").append("<img class='icon-two' src= '"+ preload.getResult(icon).src +"'>");
				}
			});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "blackbg",
		headerblockadditionalclass: "big-fong-header",
		headerblock:[
		{
			textdata: data.string.p2text12
		}
	],
	singletext:[
		{
			textclass: "texttype1",
			textdata: data.string.p4text1
		},
		{
			textclass: "imglabel-sun fi-1",
			textdata: data.string.p4text2
		},
		{
			textclass: "imglabel-elec fi-2",
			textdata: data.string.p4text3
		},
		{
			textclass: "imglabel-lamp fi-3",
			textdata: data.string.p4text4
		},
		{
			textclass: "texttype2",
			textdata: data.string.p4text5
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "ligimg-sun fi-1",
				imgid : 'transparent',
				imgsrc: ""
			},
			{
				imgclass: "ligimg-elec fi-2",
				imgid : 'translucent',
				imgsrc: ""
			},
			{
				imgclass: "ligimg-lamp fi-3",
				imgid : 'opaque',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	contentblockadditionalclass: "cbg1",
	headerblockadditionalclass: "big-fong-header cheader1",
	headerblock:[
	{
		textdata: data.string.p4text6
	}
],
singletext:[
	{
		textclass: "forbgonly",
	},
	{
		textclass: "texttype1 custext",
		textdata: data.string.p4text7
	},
	{
		textclass: "imglabel-sun newlabel fi-1",
		textdata: data.string.p4s2l1
	},
	{
		textclass: "imglabel-elec newlabel fi-2",
		textdata: data.string.p4s2l2
	},
	{
		textclass: "imglabel-lamp newlabel fi-3",
		textdata: data.string.p4s2l3
	},
	{
		textclass: "texttype2 custext newlt",
		textdata: data.string.p4text8
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "ligimg-sun newimg fi-1",
			imgid : 'glass',
			imgsrc: ""
		},
		{
			imgclass: "ligimg-elec newimg fi-2",
			imgid : 'water',
			imgsrc: ""
		},
		{
			imgclass: "ligimg-lamp newimg fi-3",
			imgid : 'air',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	contentblockadditionalclass: "cbg2",
	headerblockadditionalclass: "big-fong-header cheader1",
	headerblock:[
	{
		textdata: data.string.p4text9
	}
],
singletext:[
	{
		textclass: "forbgonly",
	},
	{
		textclass: "texttype1 custext",
		textdata: data.string.p4text10
	},
	{
		textclass: "imglabel-sun newlabel fi-1",
		textdata: data.string.p4s3l1
	},
	{
		textclass: "imglabel-elec newlabel fi-2",
		textdata: data.string.p4s3l2
	},
	{
		textclass: "imglabel-lamp newlabel fi-3",
		textdata: data.string.p4s3l3
	},
	{
		textclass: "texttype2 custext newlt",
		textdata: data.string.p4text11
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "ligimg-sun newimg fi-1",
			imgid : 'cglass',
			imgsrc: ""
		},
		{
			imgclass: "ligimg-elec newimg fi-2",
			imgid : 'tpaper',
			imgsrc: ""
		},
		{
			imgclass: "ligimg-lamp newimg fi-3",
			imgid : 'cplas',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	contentblockadditionalclass: "cbg3",
	headerblockadditionalclass: "big-fong-header cheader1",
	headerblock:[
	{
		textdata: data.string.p4text12
	}
],
singletext:[
	{
		textclass: "forbgonly",
	},
	{
		textclass: "texttype1 custext",
		textdata: data.string.p4text13
	},
	{
		textclass: "imglabel-sun newlabel fi-1",
		textdata: data.string.p4s4l1
	},
	{
		textclass: "imglabel-elec newlabel fi-2",
		textdata: data.string.p4s4l2
	},
	{
		textclass: "imglabel-lamp newlabel fi-3",
		textdata: data.string.p4s4l3
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "ligimg-sun newimg fi-1",
			imgid : 'wood',
			imgsrc: ""
		},
		{
			imgclass: "ligimg-elec newimg fi-2",
			imgid : 'wall',
			imgsrc: ""
		},
		{
			imgclass: "ligimg-lamp newimg fi-3",
			imgid : 'paper',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	contentblockadditionalclass: "cbg3",
	headerblockadditionalclass: "big-fong-header cheader1",
	headerblock:[
	{
		textdata: data.string.p4text12
	}
],
singletext:[
	{
		textclass: "forbgonly",
	},
	{
		textclass: "texttype1 custext",
		textdata: data.string.p4text14
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "light",
			imgid : 'light',
			imgsrc: ""
		},
		{
			imgclass: "torch",
			imgid : 'torch',
			imgsrc: ""
		},
		{
			imgclass: "wallsha",
			imgid : 'wall01',
			imgsrc: ""
		},
		{
			imgclass: "gagri01",
			imgid : 'gagri01',
			imgsrc: ""
		},
		{
			imgclass: "gagri02",
			imgid : 'gagri02',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "transparent", src: imgpath+"transparent.png", type: createjs.AbstractLoader.IMAGE},
			{id: "translucent", src: imgpath+"translucent.png", type: createjs.AbstractLoader.IMAGE},
			{id: "opaque", src: imgpath+"opaque.png", type: createjs.AbstractLoader.IMAGE},
			{id: "air", src: imgpath+"air.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water", src: imgpath+"water.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glass", src: imgpath+"glass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cglass", src: imgpath+"coloured-glass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tpaper", src: imgpath+"tracing-paper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cplas", src: imgpath+"coloured-plastic.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wood", src: imgpath+"wood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wall", src: imgpath+"wall.png", type: createjs.AbstractLoader.IMAGE},
			{id: "paper", src: imgpath+"paper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "light", src: imgpath+"light01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch", src: imgpath+"torch.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wall01", src: imgpath+"wall01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gagri01", src: imgpath+"gagri01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gagri02", src: imgpath+"gagri02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p4_s0.ogg"},
			{id: "fs-0-1", src: soundAsset+"p4_s0_1.ogg"},
			{id: "fs-0-2", src: soundAsset+"p4_s0_2.ogg"},
			{id: "fs-0-3", src: soundAsset+"p4_s0_3.ogg"},
			{id: "sound_0_end", src: soundAsset+"p4_s0_end.ogg"},
			{id: "sound_1", src: soundAsset+"p4_s1.ogg"},
			// {id: "fs-1-1", src: soundAsset+"sun.ogg"},
			{id: "fs-1-2", src: soundAsset+"elec.ogg"},
			{id: "fs-1-3", src: soundAsset+"lamps.ogg"},
			{id: "sound_1_end", src: soundAsset+"p4_s1_end.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s2.ogg"},
			// {id: "fs-2-1", src: soundAsset+"sun.ogg"},
			{id: "fs-2-2", src: soundAsset+"elec.ogg"},
			{id: "fs-2-3", src: soundAsset+"lamps.ogg"},
			{id: "sound_2_end", src: soundAsset+"p4_s2_end.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s3.ogg"},
			// {id: "fs-3-1", src: soundAsset+"sun.ogg"},
			{id: "fs-3-2", src: soundAsset+"elec.ogg"},
			{id: "fs-3-3", src: soundAsset+"lamps.ogg"},
			{id: "sound_4", src: soundAsset+"p4_s4.ogg"},
			// {id: "fs-1-1", src: soundAsset+"sun.ogg"},
			{id: "fs-1-2", src: soundAsset+"elec.ogg"},
			{id: "fs-1-3", src: soundAsset+"lamps.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				chain_fade("sound_"+countNext, 3);
			break;
			case 1:
			case 2:
				$(".fi-1,.fi-2,.fi-3,.fi-4,.texttype2").show();
                sound_player("sound_"+countNext, 0);
                current_sound.on('complete', function(){
                    sound_player("sound_"+countNext+"_end", 1);
                });
			break;
			// case 2:
			// 	chain_fade("sound_"+countNext, 3);
			// break;
			// case 3:
			// 	chain_fade("sound_"+countNext, 3);
			// break;
			case 3:
			case 4:
                $(".fi-1,.fi-2,.fi-3,.fi-4,.texttype2").show();
				sound_player("sound_"+countNext, 1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function chain_fade(sound_id, lastcount){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			countNums(lastcount);
		});
	}

	var Scount = 1;
	function countNums(lastcount){
		sound_player("fs-"+countNext+"-"+Scount, 0);
		$(".fi-"+Scount).fadeIn();
		if(Scount <= lastcount){
			setTimeout(function(){
				Scount++;
				countNums(lastcount);
			}, 2000);
		}
		else{
			Scount = 1;
			$(".texttype2").fadeIn();
			if(countNext < 3){
				sound_player("sound_"+countNext+"_end",1);
			}else{
				navigationcontroller();			
			}
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

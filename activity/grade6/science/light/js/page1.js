var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover blurit",
				imgid : 'cover',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	contentblockadditionalclass: "purp-bg",
	headerblockadditionalclass: "big-fong-header",
	headerblock:[
	{
		textdata: data.string.p1text2
	}
],
singletext:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "yellowtext",
		textclass: "texttype1",
		textdata: data.string.p1text3
	},
	{
		textclass: "imglabel-sun fi-1",
		textdata: data.string.p1text4
	},
	{
		textclass: "imglabel-elec fi-2",
		textdata: data.string.p1text5
	},
	{
		textclass: "imglabel-lamp fi-3",
		textdata: data.string.p1text6
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "ligimg-sun fi-1",
			imgid : 'sun',
			imgsrc: ""
		},
		{
			imgclass: "ligimg-elec fi-2",
			imgid : 'electricity',
			imgsrc: ""
		},
		{
			imgclass: "ligimg-lamp fi-3",
			imgid : 'lamps',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	headerblockadditionalclass: "big-fong-header",
	headerblock:[
	{
		textdata: data.string.p1text4
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bgsun',
			imgsrc: ""
		}
	]
}],
singletext:[
	{
		textclass: "ltb",
		textdata: data.string.p1text7
	}
]
},
// slide4
{
	headerblockadditionalclass: "big-fong-header",
	headerblock:[
	{
		textdata: data.string.p1text5
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		}
	]
}]
},
// slide5
{
	headerblockadditionalclass: "big-fong-header",
	headerblock:[
	{
		textdata: data.string.p1text8
	}
],
singletext:[
	{
		textclass: "switch-1"
	},
	{
		textclass: "switch-2"
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover-empty',
			imgsrc: ""
		},
		{
			imgclass: "elec-1",
			imgid : 'click-off-light1',
			imgsrc: ""
		},
		{
			imgclass: "elec-2",
			imgid : 'click-off-light2',
			imgsrc: ""
		},
		{
			imgclass: "elec-3",
			imgid : 'click-off-light3',
			imgsrc: ""
		},
		{
			imgclass: "hand-1",
			imgid : 'handicon',
			imgsrc: ""
		},
		{
			imgclass: "hand-2",
			imgid : 'handicon',
			imgsrc: ""
		}
	]
}]
},
// slide6
{
	headerblock:[
	{
		textdata: data.string.p1text9
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		}
	]
}]
},
// slide7
{
	contentblockadditionalclass: "purp-bg",
	headerblockadditionalclass: "big-fong-header",
	headerblock:[
	{
		textdata: data.string.p1text6
	}
],
singletext:[
	{
		textclass: "texttype1 yellowtext",
		textdata: data.string.p1text10
	},
	{
		textclass: "texttype2",
		textdata: data.string.p1text11
	}
],
flipcontainer:[
{
	sectioncontainer : "section seone",
	imgid : "flip-1",
	imgsrc : imgpath + "",
	animalname : data.string.p1text12,
	flipdisc: data.string.p1text15
},
{
	sectioncontainer : "section setwo",
	imgid : "flip-2",
	imgsrc : imgpath + "",
	animalname : data.string.p1text13,
	flipdisc: data.string.p1text16
}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "hand-3",
			imgid : 'handicon',
			imgsrc: ""
		},
		{
			imgclass: "hand-4",
			imgid : 'handicon',
			imgsrc: ""
		}
	]
}]
},
// slide8
{
	contentblockadditionalclass: "purp-bg",
	headerblockadditionalclass: "big-fong-header",
	singletext:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "yellowtext2",
		textclass: "texttype3",
		textdata: data.string.p1text17
	}
]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"electricity-room-02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover-empty", src: imgpath+"page_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "electricity", src: imgpath+"electrict_bulb.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lamps", src: imgpath+"lamps.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candle", src: imgpath+"candle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bgsun", src: imgpath+"bg_sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "click-off-light1", src: imgpath+"top_light02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "click-off-light2", src: imgpath+"side_lamp02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "click-off-light3", src: imgpath+"side_lamp04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "click-on-light1", src: imgpath+"top_light01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "click-on-light2", src: imgpath+"side_lamp01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "click-on-light3", src: imgpath+"side_lamp03.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_7_1", src: soundAsset+"p1_s7_1.ogg"},
			{id: "sound_7_2", src: soundAsset+"p1_s7_2.ogg"},
			{id: "sound_7_3", src: soundAsset+"p1_s7_3.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s8.ogg"},
			{id: "fs-1", src: soundAsset+"p1_s2_1.ogg"},
			{id: "fs-2", src: soundAsset+"p1_s2_2.ogg"},
			{id: "fs-3", src: soundAsset+"p1_s2_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("sound_"+countNext,1);
			break;

			case 1:
				sound_player("sound_"+countNext,1);
			break;

			case 2:
				chain_fade("sound_"+countNext, 3);
			break;

			case 3:
			sound_player("fs-1",0);
			setTimeout(function(){
				sound_player("sound_"+countNext,1);
				$(".ltb").fadeIn();
			},1500);
			break;

			case 4:
			sound_player("sound_"+countNext,1);
			break;

			case 5:
			var s1flag = false;
			var s2flag = false;

			sound_player("sound_"+countNext,0);
			setTimeout(function(){
				$(".hand-1").fadeIn();
				$(".hand-2").fadeIn();
			},2500);

			$(".switch-1").click(function(){
				$(".elec-1").attr("src", preload.getResult('click-on-light1').src);
				$(".hand-1").fadeOut();
				s1flag = true;
				c5nav();
			});

			$(".switch-2").click(function(){
				$(".elec-2").attr("src", preload.getResult('click-on-light2').src);
				$(".elec-3").attr("src", preload.getResult('click-on-light3').src);
				$(".hand-2").fadeOut();
				s2flag = true;
				c5nav();
			});

			$( ".hand-1" ).click(function() {
			  $( ".switch-1" ).trigger( "click" );
			});

			$( ".hand-2" ).click(function() {
			  $( ".switch-2" ).trigger( "click" );
			});

			function c5nav(){
				if(s1flag == true && s2flag == true)
					navigationcontroller();
			}
			break;

			case 6:
			sound_player("sound_"+countNext,1);
			break;

			case 7:
			$("#flip-1").attr("src", preload.getResult('candle').src);
			$("#flip-2").attr("src", preload.getResult('lamps').src);

			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_"+countNext);
			current_sound.play();
			current_sound.on('complete', function(){
				enableone();
			});

			function enableone(){
				$(".hand-3").fadeIn();
				$(".seone").addClass("hoverclass");
				$(".seone .card").click(function(){
					$(".hand-3").hide();
        	$(this).addClass("flipped");
        	$(this).parent().removeClass("hoverclass");
					current_sound = createjs.Sound.play("sound_"+countNext+"_1");
					current_sound.play();
					current_sound.on('complete', function(){
						enabletwo();
					});
	      });
			};

			function enabletwo(){
				$(".hand-4").fadeIn();
				$(".setwo").addClass("hoverclass");
				$(".setwo .card").click(function(){
					$(".hand-4").hide();
        	$(this).addClass("flipped");
        	$(this).parent().removeClass("hoverclass");
					current_sound = createjs.Sound.play("sound_"+countNext+"_2");
					current_sound.play();
					current_sound.on('complete', function(){
						lastfunction();
					});
	      });
			};

			$( ".hand-3" ).click(function() {
			  $( ".seone .card" ).trigger( "click" );
			});

			$( ".hand-4" ).click(function() {
			  $( ".setwo .card" ).trigger( "click" );
			});

			function lastfunction(){
				$(".texttype2").fadeIn();
				current_sound = createjs.Sound.play("sound_"+countNext+"_3");
				current_sound.play();
				current_sound.on('complete', function(){
					navigationcontroller();
				});
			}

			break;

			case 8:
			sound_player("sound_"+countNext,1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function chain_fade(sound_id, lastcount){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			countNums(lastcount);
		});
	}

	var Scount = 1;
	function countNums(lastcount){
		sound_player("fs-"+Scount, 0);
		$(".fi-"+Scount).fadeIn();
		if(Scount <= lastcount){
			setTimeout(function(){
				Scount++;
				countNums(lastcount);
			}, 2000);
		}
		else{
			navigationcontroller();
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

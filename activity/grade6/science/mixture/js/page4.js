var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/page4/";

var sound_0 = new buzz.sound((soundAsset + "recording1.ogg"));
var sound_1 = new buzz.sound((soundAsset + "recording2.ogg"));
var sound_2 = new buzz.sound((soundAsset + "recording3.ogg"));
var sound_3 = new buzz.sound((soundAsset + "recording4.ogg"));
var sound_4 = new buzz.sound((soundAsset + "recording5.ogg"));
var sound_5 = new buzz.sound((soundAsset + "recording6.ogg"));
var sound_6 = new buzz.sound((soundAsset + "recording7.ogg"));
var sound_7 = new buzz.sound((soundAsset + "recording8.ogg"));
var sound_8 = new buzz.sound((soundAsset + "recording9.ogg"));
var sound_9 = new buzz.sound((soundAsset + "recording10.ogg"));
var sound_10 = new buzz.sound((soundAsset + "recording11.ogg"));
var sound_11 = new buzz.sound((soundAsset + "recording12.ogg"));
var sound_12 = new buzz.sound((soundAsset + "recording13.ogg"));
var sound_13 = new buzz.sound((soundAsset + "recording14.ogg"));

var sound_list = [sound_0, sound_1, sound_2, sound_3, sound_4, sound_5, sound_6,sound_7, sound_8, sound_9, sound_10, sound_11, sound_12, sound_13];

var content=[
	{
		// slide0
		contentblockadditionalclass:'ole-background-gradient-brick',
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p4_s1
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "winnowing.png"
			}]
		}]
	},{
		// slide1
		contentblockadditionalclass:'ole-background-gradient-brick',
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p4_s2
		}],
		imageblock:[{
			imageblockclass: "imagediv1",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "winnowing_naglo.png"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p4_s3
			}]
		},{
			imageblockclass: "imagediv2",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "winnowing-01.png"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p4_s4
			}]
		}],

		infoimageblock:[{
			infoimageblockclass:"infoimagediv",
			imagestoshow:[{
				imgclass: "imageinfo",
				imgsrc: imgpath+ "niphaneko.gif"
			},{
				imgclass: "imageclose close1",
				imgsrc: imgpath+ "close.png"
			}],
			imagelabels:[{
				imagelabelclass: "infoimagelabel",
				imagelabeldata: data.string.p4_s5
			}]
		},{
			infoimageblockclass:"infoimagediv",
			imagestoshow:[{
				imgclass: "imageinfo",
				imgsrc: imgpath+ "magnet.gif"
			},{
				imgclass: "imageclose close2",
				imgsrc: imgpath+ "close.png"
			}],
			imagelabels:[{
				imagelabelclass: "infoimagelabel",
				imagelabeldata: data.string.p4_s6
			}]
		}]
	},{
		// slide2
		contentblockadditionalclass:'ole-background-gradient-brick',
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p4_s8
		},{
			textclass: "description fadein",
			textdata: data.string.p4_s9
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "01.png"
			}]
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		contentblockadditionalclass:'ole-background-gradient-brick',
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p4_s10
		}],
		imageblock:[{
			imageblockclass: "imagediv1",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "cloth.png"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p4_s11
			}]
		},{
			imageblockclass: "imagediv2",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "sieve.png"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p4_s12
			}]
		}],

		infoimageblock:[{
			infoimageblockclass:"infoimagediv",
			imagestoshow:[{
				imgclass: "imageinfo",
				imgsrc: imgpath+ "Lady.gif"
			},{
				imgclass: "imageclose close1",
				imgsrc: imgpath+ "close.png"
			}],
			imagelabels:[{
				imagelabelclass: "infoimagelabel",
				imagelabeldata: data.string.p4_s13
			}]
		},{
			infoimageblockclass:"infoimagediv",
			imagestoshow:[{
				imgclass: "imageinfo",
				imgsrc: imgpath+ "Lady02.gif"
			},{
				imgclass: "imageclose close2",
				imgsrc: imgpath+ "close.png"
			}],
			imagelabels:[{
				imagelabelclass: "infoimagelabel",
				imagelabeldata: data.string.p4_s14
			}]
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		contentblockadditionalclass:'ole-background-gradient-brick',
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p4_s16
		},{
			textclass: "description fadein",
			textdata: data.string.p4_s17
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "dirty_water.png"
			}]
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		contentblockadditionalclass: "content_lab_bg",
		imageblock:[{
			imageblockclass: "imagediv3 redborder",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "dirty_water_01.png"
			}]
		}],
		lowertextblockadditionalclass: "ltbadditional",
		lowertextblock: [{
			textclass: "description brownborder",
			textdata: data.string.p4_s18
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		contentblockadditionalclass: "content_lab_bg",
		imageblock:[{
			imageblockclass: "imagediv redborder",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "filter.gif"
			}]
		}],
		lowertextblockadditionalclass: "ltbadditional",
		lowertextblock: [{
			textclass: "description brownborder",
			textdata: data.string.p4_s19
		}]
	},{
		// slide7
		contentnocenteradjust: true,
		contentblockadditionalclass: "content_lab_bg",
		imageblock:[{
			imageblockclass: "imagediv4 redborder",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "making-soli.gif"
			}]
		}],
		lowertextblockadditionalclass: "ltbadditional",
		lowertextblock: [{
			textclass: "description brownborder",
			textdata: data.string.p4_s20
		}]
	},{
		// slide8
		contentnocenteradjust: true,
		contentblockadditionalclass: "content_lab_bg",
		imageblock:[{
			imageblockclass: "imagediv5 redborder",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "putting-waterto-filter.png"
			}]
		}],
		lowertextblockadditionalclass: "ltbadditional",
		lowertextblock: [{
			textclass: "description brownborder",
			textdata: data.string.p4_s21
		}]
	},{
		// slide9
		contentnocenteradjust: true,
		contentblockadditionalclass: "content_lab_bg",
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description brownborder",
			textdata: data.string.p4_s22
		}],
		imageblock:[{
			imageblockclass: "imagediv6 redborder",
			imagestoshow:[{
				imgclass: "image",
				imgsrc: imgpath+ "boilingwater.png"
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
				// if(countNext == $total_page - 1)
					// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.lessonEndSetNotification();
			// }, 1000);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var playing_audio;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
		switch(countNext){
			case 0:
				playing_audio = sound_list[countNext];
	    		$nextBtn.hide(0);
	    		playing_audio.play();
	    		playing_audio.bind("ended", function(){
	    			$nextBtn.show(0);
	    		});
	    		break;
			case 9:
				playing_audio = sound_list[countNext+4];
	    		$nextBtn.hide(0);
	    		playing_audio.play();
	    		playing_audio.bind("ended", function(){
	    			ole.footerNotificationHandler.lessonEndSetNotification();
	    		});
				break;
			case 2:
				playing_audio = sound_list[countNext+2];
	    		$nextBtn.hide(0);
	    		playing_audio.play();
	    		playing_audio.bind("ended", function(){
	    			$nextBtn.show(0);
	    		});
				break;
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
				playing_audio = sound_list[countNext+4];
	    		$nextBtn.hide(0);
	    		playing_audio.play();
	    		playing_audio.bind("ended", function(){
	    			$nextBtn.show(0);
	    		});
				break;
			case 1:
			case 3:
				var $infoimagediv = $(".infoimagediv");
				var $infoimageblock = $(".infoimageblock");
				$infoimageblock.hide(0);
				if(countNext == 1){
					playing_audio = sound_list[countNext];
				} else {
					playing_audio = sound_list[countNext+2];
				}
	    		$nextBtn.hide(0);
	    		playing_audio.play();
	    		$(".imagediv1").hide(0);
	    		$(".imagediv2").hide(0);

    			$(".imagediv1").delay(4000).show(0);
    			$(".imagediv2").delay(4000).show(0);
	    		// playing_audio.bind("ended", function(){
	    		// 	$(".imagediv1").show(0);
	    		// 	$(".imagediv2").show(0);
	    		// });
				var show1 = false;
				var show2 = false;

				$(".imagediv1").click(function(){
					$(this).css("border", "0.3em solid #1bad7f");
					$infoimageblock.show(0);
					$($infoimagediv[0]).show(0);
					show1 = true;
					if(countNext == 1){
						playing_audio = sound_list[countNext+1];
					} else {
						playing_audio = sound_list[countNext+3];
					}
		    		playing_audio.play();
		    		$(".close1").hide(0);
		    		playing_audio.bind("ended", function(){
			    		$(".close1").show(0);
		    		});
				});

				$(".imagediv2").click(function(){
					$(this).css("border", "0.3em solid #1bad7f");
					$infoimageblock.show(0);
					$($infoimagediv[1]).show(0);
					show2 = true;
					if(countNext == 1){
						playing_audio = sound_list[countNext+2];
					} else {
						playing_audio = sound_list[countNext+4];
					}
		    		playing_audio.play();
		    		$(".close2").hide(0);
		    		playing_audio.bind("ended", function(){
			    		$(".close2").show(0);
		    		});
				});

				$(".close1").click(function(){
					$($infoimagediv[0]).hide(0);
					$infoimageblock.hide(0);
					if(show1 && show2){
						$nextBtn.delay(500).show(0);
					}
				});

				$(".close2").click(function(){
					$($infoimagediv[1]).hide(0);
					$infoimageblock.hide(0);
					if(show1 && show2){
						$nextBtn.delay(500).show(0);
					}
				});

				break;
			default:
				break;
		}
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		playing_audio.stop();
		templateCaller();
	});

	$prevBtn.on('click', function() {
		playing_audio.stop();
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

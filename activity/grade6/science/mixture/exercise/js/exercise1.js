var imgpath = $ref+"/exercise/images/";
Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
	{
	//slide 0	
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques1,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "beans.png",
				labeltext: data.string.q1opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "milk.png",
				labeltext: data.string.q1opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "gold.png",
				labeltext: data.string.q1opt3
	
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "water.png",
				labeltext: data.string.q1opt4
	
			},
			]
		}
		]
	},
	{
	//slide 1	
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques2,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "filtration.png",
				labeltext: data.string.q2opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "centrifuging.png",
				labeltext: data.string.q2opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "winnowing.png",
				labeltext: data.string.q2opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "cooling.png",
				labeltext: data.string.q2opt4
	
			},

			]
		}
		]
	},
	{
	//slide 2
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques3,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "centrifuging.png",
				labeltext: data.string.q3opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "filtration.png",
				labeltext: data.string.q3opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "cooling.png",
				labeltext: data.string.q3opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "evaporation.png",
				labeltext: data.string.q3opt4
	
			},

			]
		}
		]
	},
	{
	//slide 3
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques4,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "sedimentation.png",
				labeltext: data.string.q4opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "boiling.png",
				labeltext: data.string.q4opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "using-magnet.png",
				labeltext: data.string.q4opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "evaporation.png",
				labeltext: data.string.q4opt4
	
			},

			]
		}
		]
	},
	{
	//slide 4
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques5,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "using-magnet.png",
				labeltext: data.string.q5opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "filtration.png",
				labeltext: data.string.q5opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "water.png",
				labeltext: data.string.q5opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "stone.png",
				labeltext: data.string.q5opt4
	
			},

			]
		}
		]
	},
	{
	//slide 5
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques6,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "wind.png",
				labeltext: data.string.q6opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "filtration.png",
				labeltext: data.string.q6opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "strainer.png",
				labeltext: data.string.q6opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "using-magnet.png",
				labeltext: data.string.q6opt4
	
			},

			]
		}
		]
	},
	{
	//slide 6
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques7,

			imageoptions: [
			{
				forshuffle: "options_sign correctone",
				imgsrc: imgpath + "gold.png",
				labeltext: data.string.q7opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "oilwater.png",
				labeltext: data.string.q7opt2
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "chocolates.png",
				labeltext: data.string.q7opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "saltwater.png",
				labeltext: data.string.q7opt4
	
			},

			]
		}
		]
	},
	{
	//slide 7
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques8,

			imageoptions: [
			{
				forshuffle: "options_sign correctone",
				imgsrc: imgpath + "strainer.png",
				labeltext: data.string.q8opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "using-magnet.png",
				labeltext: data.string.q8opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "filtration.png",
				labeltext: data.string.q8opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "evaporation.png",
				labeltext: data.string.q8opt4
	
			},

			]
		}
		]
	},
	{
	//slide 8
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques9,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "evaporation.png",
				labeltext: data.string.q9opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "winnowing.png",
				labeltext: data.string.q9opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "using-magnet.png",
				labeltext: data.string.q9opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "filtration.png",
				labeltext: data.string.q9opt4
	
			},

			]
		}
		]
	},
	{
	//slide 9
		contentblockadditionalclass:'ole-background-gradient-brick',
		exerciseblock: [
		{
			textdata: data.string.ques10,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "orangejuice.png",
				labeltext: data.string.q10opt1
			},
			{
				forshuffle: "options_sign incorrecttwo",
				imgsrc: imgpath + "water.png",
				labeltext: data.string.q10opt2
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "beans.png",
				labeltext: data.string.q10opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "fruits.png",
				labeltext: data.string.q10opt4
	
			},

			]
		}
		]
	}
	];

/*remove this for non random questions*/
content.shufflearray();

$(function (){	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	var score = 0;
	var testin = new RhinoTemplate();
   
	 	//eggTemplate.eggMove(countNext);
 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		
		switch(countNext) {
			default:
				var frac_1_n = 0;
				var frac_1_d = 0;
				var frac_2_n = 0;
				var frac_2_d = 0;
				var new_question1 = 0;
				var new_question2 = 0;
				var rand_multiplier_1 = 1;
				var rand_multiplier_2 = 1;
				var incorrect = false; 
				
				function correct_btn(current_btn){
					$('.options_sign').addClass('disabled');
					$('.hidden_sign').html($(current_btn).html());
					$('.hidden_sign').addClass('fade_in');
					$('.optionscontainer').removeClass('forHover');
					$(current_btn).addClass('option_true');
					$nextBtn.show(0);
                    play_correct_incorrect_sound(true);
					if(!incorrect){
						testin.update(true);
					}
				}
				function incorrect_btn(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).addClass('option_false');
					testin.update(false);
					incorrect = true; 
                    play_correct_incorrect_sound(false);
				}
				break;
		}

		$('.correctOne').click(function(){
			correct_btn(this);
		});

		$('.incorrectOne, .incorrectTwo').click(function(){
			incorrect_btn(this);
		});
	}
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();	
		testin.gotoNext();
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
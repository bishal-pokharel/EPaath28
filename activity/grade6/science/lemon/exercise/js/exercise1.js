Array.prototype.shufflearray = function() {
  var i = this.length,
    j,
    temp;
  while (--i > 0) {
    j = Math.floor(Math.random() * (i + 1));
    temp = this[j];
    this[j] = this[i];
    this[i] = temp;
  }
  return this;
};

var imgpath = $ref + "/exercise/images/";

var content = [
  //ex1
  {
    exerciseblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
        textdata: data.lesson.chapter
      }
    ]
  }
];

/*remove this for non random questions*/
//content.shufflearray();

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var countNext = 0;

  /*for limiting the questions to 10*/
  var $total_page = 1;

  function navigationcontroller(islastpageflag) {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
  }

  var score = 0;
  var testin = new NumberTemplate();

  testin.init(1);
  /*values in this array is same as the name of images of eggs in image folder*/
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    console.log("jjjjjjjjjj");
    $nextBtn.css("display", "block");
    $nextBtn.show();
    $prevBtn.hide();

    /*generate question no at the beginning of question*/
    testin.numberOfQuestions(1);
    testin.update(true);
    /*======= SCOREBOARD SECTION ==============*/
  }

  function templateCaller() {
    /*always hide next and previous navigation button unless
		explicitly called from inside a template*/

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
  }

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    countNext++;
    testin.gotoNext();
    templateCaller();
  });
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "mainbg",
		uppertextblock:[
		{
			textclass: "covertext chaptertitle",
			textdata: data.lesson.chapter
		}
	],
        imageblock:[{
            imagestoshow:[
                {
                    imgclass: "coverpage",
                    imgid : 'coverpageImg',
                    imgsrc: ""
                }
            ]
        }]
},
// slide1
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
singletext:[
	{
		textclass: "texttyp1",
		textdata: data.string.p1text2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "fouanim-01",
			imgid : 'frui1',
			imgsrc: ""
		},
		{
			imgclass: "fouanim-02",
			imgid : 'frui2',
			imgsrc: ""
		},
		{
			imgclass: "fouanim-03",
			imgid : 'frui3',
			imgsrc: ""
		},
		{
			imgclass: "fouanim-04",
			imgid : 'frui4',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
singletext:[
	{
		textclass: "texttyp1",
		textdata: data.string.p1text3
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "midimg",
			imgid : 'frui1',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
uppertextblockadditionalclass: "p1lefttext",
uppertextblock:[{
	textclass: "steps",
	textdata: data.string.p1text5
}],
singletext:[
	{
		textclass: "numcount",
		textdata: "1"
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "leftimg",
			imgid : 'stepimg1',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
uppertextblockadditionalclass: "p1lefttext",
uppertextblock:[{
	textclass: "steps",
	textdata: data.string.p1text5
}],
singletext:[
	{
		textclass: "numcount",
		textdata: "2"
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "leftimg",
			imgid : 'stepimg2',
			imgsrc: ""
		}
	]
}]
},
// slide5
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
uppertextblockadditionalclass: "p1lefttext",
uppertextblock:[{
	textclass: "steps",
	textdata: data.string.p1text5
}],
singletext:[
	{
		textclass: "numcount",
		textdata: "3"
	},
	{
		textclass: "addtext",
		textdata: data.string.p1text6
	},
	{
		textclass: "nailtxt",
		textdata: data.string.p1text10
	},
	{
		textclass: "cointxt",
		textdata: data.string.p1text11
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "leftimg",
			imgid : 'stepimg3',
			imgsrc: ""
		},
		{
			imgclass: "arrow1",
			imgid : 'arrow',
			imgsrc: ""
		},
		{
			imgclass: "arrow2",
			imgid : 'arrow',
			imgsrc: ""
		}
	]
}]
},
// slide6
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
uppertextblockadditionalclass: "p1lefttext",
uppertextblock:[{
	textclass: "steps",
	textdata: data.string.p1text5
}],
singletext:[
	{
		textclass: "numcount pnkcolor",
		textdata: "3"
	},
	{
		textclass: "addtext pnkcolor",
		textdata: data.string.p1text7
	},
	{
		textclass: "nailtxt",
		textdata: data.string.p1text10
	},
	{
		textclass: "cointxt",
		textdata: data.string.p1text11
	},
	{
		textclass: "figlabel2",
		textdata: data.string.p1text12
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "leftimg",
			imgid : 'stepimg3-2',
			imgsrc: ""
		},
		{
			imgclass: "arrow1",
			imgid : 'arrow',
			imgsrc: ""
		},
		{
			imgclass: "arrow2",
			imgid : 'arrow',
			imgsrc: ""
		}
	]
}]
},
// slide7
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
singletext:[
	{
		textclass: "numcount",
		textdata: "4"
	}
],
uppertextblockadditionalclass: "p1lefttext",
uppertextblock:[{
	textclass: "steps",
	textdata: data.string.p1text5
}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "leftimg widadjst",
			imgid : 'stepimg4',
			imgsrc: ""
		}
	]
}]
},
// slide8
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
singletext:[
	{
		textclass: "numcount pnkcolor",
		textdata: "4"
	},
	{
		textclass: "addtext pnkcolor",
		textdata: data.string.p1text8
	},
	{
		textclass: "figlabel2",
		textdata: data.string.p1text13
	}
],
uppertextblockadditionalclass: "p1lefttext",
uppertextblock:[{
	textclass: "steps",
	textdata: data.string.p1text5
}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "leftimg widadjst",
			imgid : 'stepimg4',
			imgsrc: ""
		}
	]
}]
},
// slide9
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
singletext:[
	{
		textclass: "numcount",
		textdata: "5"
	},
	{
		textclass: "figlabel2",
		textdata: data.string.p1text14
	}
],
uppertextblockadditionalclass: "p1lefttext",
uppertextblock:[{
	textclass: "steps",
	textdata: data.string.p1text5
}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "leftimg widadjst2",
			imgid : 'stepimg5',
			imgsrc: ""
		},

	]
}]
},
// slide10
{
	contentblockadditionalclass: "mainbg",
	headerblock:[
	{
		textdata: data.string.p1text1
	}
],
singletext:[
	{
		textclass: "numcount pnkcolor",
		textdata: "5"
	},
	{
		textclass: "addtext pnkcolor",
		textdata: data.string.p1text9
	}
],
uppertextblockadditionalclass: "p1lefttext",
uppertextblock:[{
	textclass: "steps",
	textdata: data.string.p1text5
}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "leftimg glow",
			imgid : 'stepimg6-1',
			imgsrc: ""
		},
		{
			imgclass: "leftimg widadjst",
			imgid : 'stepimg6-2',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "frui1", src: imgpath+"lemon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frui2", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frui3", src: imgpath+"potato.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frui4", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stepimg1", src: imgpath+"lemon01a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stepimg2", src: imgpath+"squeezing-lemon-darker-outline.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stepimg3", src: imgpath+"killacoin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stepimg3-2", src: imgpath+"killacoin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stepimg4", src: imgpath+"fig_a_lemon_battery.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stepimg5", src: imgpath+"led-bulb01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stepimg6-1", src: imgpath+"bulb.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stepimg6-2", src: imgpath+"fig_a_lemon_battery_without_bulb.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpageImg", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 3:
			$(".points li:eq(0)").fadeIn(1000);
			break;
			case 4:
			$(".points li:eq(0)").show(0);
			$(".points li:eq(1)").fadeIn(1000);
			break;
			case 5:
			$(".points li:eq(0)").show(0);
			$(".points li:eq(1)").show(0);
			$(".points li:eq(2)").fadeIn(1000);
			break;
			case 6:
			$(".points li:eq(0)").show(0);
			$(".points li:eq(1)").show(0);
			$(".points li:eq(2)").show(0);
			break;
			case 7:
			$(".points li:eq(0)").show(0);
			$(".points li:eq(1)").show(0);
			$(".points li:eq(2)").show(0);
			$(".points li:eq(3)").fadeIn(1000);
			break;
			case 8:
			$(".points li:eq(0)").show(0);
			$(".points li:eq(1)").show(0);
			$(".points li:eq(2)").show(0);
			$(".points li:eq(3)").show(0);
			break;
			case 9:
			$(".points li:eq(0)").show(0);
			$(".points li:eq(1)").show(0);
			$(".points li:eq(2)").show(0);
			$(".points li:eq(3)").show(0);
			$(".points li:eq(4)").fadeIn(1000);
			break;
			case 10:
			$(".points li:eq(0)").show(0);
			$(".points li:eq(1)").show(0);
			$(".points li:eq(2)").show(0);
			$(".points li:eq(3)").show(0);
			$(".points li:eq(4)").show(0);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

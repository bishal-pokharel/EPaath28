var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
            }
        ],
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques1,
                ans:data.string.animalcell
            }
        ],
    },
  //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
            }
        ],
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques2,
                ans:data.string.plantcell
            }
        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
            }
        ],
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques3,
                ans:data.string.animalcell
            }
        ],
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
            }
        ],
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques4,
                ans:data.string.animalcell
            }
        ],
    },

    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques5,
            }
        ] ,
        imageblock:[{
            imagestoshow:[{
                imgclass: "plant",
                imgsrc: "",
                imgid : 'plantcellImg',
            }]
        }],
        svgblock:[{
        	svgblock: 'plantcell'
             }
           ]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques6,
            }
        ] ,
        imageblock:[{
            imagestoshow:[{
                imgclass: "plant",
                imgsrc: "",
                imgid : 'plantcellImg',
            }]
        }],
        svgblock:[{
            svgblock: 'plantcell'
        }
        ]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques7,
            }
        ] ,
        imageblock:[{
            imagestoshow:[{
                imgclass: "plant",
                imgsrc: "",
                imgid : 'plantcellImg',
            }]
        }],
        svgblock:[{
            svgblock: 'plantcell'
        }
        ]
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques8,
            }
        ] ,
        imageblock:[{
            imagestoshow:[{
                imgclass: "plant",
                imgsrc: "",
                imgid : 'animalcellImg',
            }]
        }],
        svgblock:[{
            svgblock: 'animalcell'
        }
        ]
    },
    //slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques9,
            }
        ] ,
        imageblock:[{
            imagestoshow:[{
                imgclass: "plant",
                imgsrc: "",
                imgid : 'animalcellImg',
            }]
        }],
        svgblock:[{
            svgblock: 'animalcell'
        }
        ]
    },
    //slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        belowtext:[
            {
                textclass:"content centertext question",
                textdata: data.string.p1ques10,
            }
        ] ,
        imageblock:[{
            imagestoshow:[{
                imgclass: "plant",
                imgsrc: "",
                imgid : 'animalcellImg',
            }]
        }],
        svgblock:[{
            svgblock: 'animalcell'
        }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    var rhino = new RhinoTemplate();
    rhino.init(content.length);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "plantcellImg", src: imgpath+"plantcells1.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "animalcellImg", src: imgpath+"animalcells1.svg", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p3_s1.ogg"},
            {id: "sound_2_0", src: soundAsset+"p3_s2_0.ogg"},
            {id: "sound_2_1", src: soundAsset+"p3_s2_1.ogg"},
            {id: "sound_3", src: soundAsset+"p3_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p3_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p3_s5.ogg"},
            {id: "sound_8", src: soundAsset+"p3_s8.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templatecaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        // vocabcontroller.findwords(countNext);
        rhino.numberOfQuestions();
        put_image();
        switch(countNext){
            case 4:
                  loadplantcell();
                 break;
            case 5:
                loadplantcell();
                break;
            case 6:
                loadplantcell();
                break;
            case 7:
                loadanimalcell();
                break;
            case 8:
                loadanimalcell();
                break;
            case 9:
                loadanimalcell();
                break;
             default:
                 shufflehint();
                 checkans();
                 break;
         }
     }

function loadplantcell(){
    var s = Snap('#plantcell');
    var svg = Snap.load(preload.getResult('plantcellImg').src, function ( loadedFragment ) {
        s.append(loadedFragment);
        switch(countNext){
            case 4:
                checkans1("vacules","plantcell");
                break;
            case 5:
                checkans1("plastid","plantcell");
                break;
            case 6:
                checkans1("cellmembrane","plantcell");
                break;

        }

    });
}

    function loadanimalcell(){
        var s = Snap('#animalcell');
        var svg = Snap.load(preload.getResult('animalcellImg').src, function ( loadedFragment ) {
            s.append(loadedFragment);
            switch(countNext){
                case 7:
                    checkans1("nucleous","animalcell");
                    break;
                case 8:
                    checkans1("mitochondria","animalcell");
                    break;
                case 9:
                    checkans1("vacules","animalcell");
                    break;

            }

        });
    }

     function sound_player(sound_id,navigate) {
         createjs.Sound.stop();
         current_sound = createjs.Sound.play(sound_id);
         current_sound.play();
         current_sound.on('complete', function () {
             navigate?navigationcontroller():"";
         });
     }
    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

     function templatecaller() {
         $prevBtn.css('display', 'none');
         $nextBtn.css('display', 'none');
         generaltemplate();
         loadTimelineProgress($total_page, countNext + 1);
     }

     $nextBtn.on('click', function () {
         createjs.Sound.stop();
         clearTimeout(timeoutvar);
         switch (countNext) {
             default:
                 countNext++;
                 rhino.gotoNext();
                 templatecaller();
                 break;
         }
     });
     $refreshBtn.click(function(){
         templatecaller();
     });
     $prevBtn.on('click', function () {
         createjs.Sound.stop();
         clearTimeout(timeoutvar);
         countNext--;
         templatecaller();
         /* if footerNotificationHandler pageEndSetNotification was called then on click of
          previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function put_image() {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function shufflehint(){
        var optdiv = $(".option");

        for (var i = optdiv.children().length; i >= 0; i--) {
            optdiv.append(optdiv.children().eq(Math.random() * i | 0));
        }
        optdiv.children().removeClass();
        var optionclass = ["div1","div2"]
        optdiv.children().each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }

    function checkans(){
        $(".ansclick").click(function () {
            var currdiv = $(this).parent().parent();
            if($(this).attr("data-answer").toString()==$(".question").attr("data-answer").toString()){
                play_correct_incorrect_sound(1);
                currdiv.append("<img class='correctImg' src='images/right.png'/>");
                currdiv.find(".textdiv").addClass("correctcss");
                $(".option").addClass("avoid-clicks");
                navigationcontroller();
                rhino.update(true);
            }
            else{
                currdiv.append("<img class='correctImg' src='images/wrong.png'/>");
                currdiv.find(".textdiv").addClass("wrongcss");
                currdiv.addClass("avoid-clicks");
                rhino.update(false);
                play_correct_incorrect_sound(0);

            }
        })
    }

    function checkans1(ans,celltype){
      $(".cell").click(function(){
          if($(this).attr("id")==ans){
              play_correct_incorrect_sound(1);
              $("#"+celltype).find(".correctImg1").remove();
              $("#"+celltype).append("<img class='correctImg1' src='images/right.png'/>");
              $("#"+celltype).addClass("avoid-clicks");
              $(this).css("opacity","1");
              navigationcontroller();
              rhino.update(true);
          }
          else{
              play_correct_incorrect_sound(0);
              $("#"+celltype).find(".correctImg1").remove();
              $("#"+celltype).append("<img class='correctImg1' src='images/wrong.png'/>");
              $(this).addClass("avoid-clicks");
              rhino.update(false);
          }
      })
    }
});

var imgpath = $ref + "/images/ImagesDIY/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"text1",
        uppertextblock: [
            {
                textclass: "title centertext",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images diy",
                    imgclass: "relativecls diyimg",
                    imgid: 'diyImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"text2",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images animalcell",
                    imgclass: "animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "box draggable centertext opt1",
                        textclass: "opt11img",
                        textdata: data.string.vacuole
                    },
                    {
                        divclass: "box draggable centertext opt2",
                        textclass: "opt2img",
                        textdata: data.string.cytoplasm
                    },
                    {
                        divclass: "box draggable centertext opt3",
                        textclass: "opt3img",
                        textdata: data.string.nucleus
                    },
                    {
                        divclass: "box draggable centertext opt4",
                        textclass: "opt4img",
                        textdata: data.string.mitochondria
                    }

                ]
            }
        ],
        lowertext:[
            {
                textclass: "box centertext droppable dropbox",
                textdata: "",
                ans:data.string.nucleus
            },
            {
                textclass: "box centertext droppable dropbox1",
                textdata: "",
                ans:data.string.vacuole

            },
            {
                textclass: "box centertext droppable dropbox2",
                textdata: "",
                ans:data.string.mitochondria
            },
            {
                textclass: "box centertext droppable dropbox3",
                textdata: "",
                ans:data.string.cytoplasm

            }

        ]

    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"text2",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell",
                    imgclass: "plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "box draggable centertext opt1",
                        textclass: "opt11img",
                        textdata: data.string.plastid
                    },
                    {
                        divclass: "box draggable centertext opt2",
                        textclass: "opt2img",
                        textdata: data.string.cytoplasm
                    },
                    {
                        divclass: "box draggable centertext opt3",
                        textclass: "opt3img",
                        textdata: data.string.nucleus
                    },
                    {
                        divclass: "box draggable centertext opt4",
                        textclass: "opt4img",
                        textdata: data.string.cellwall
                    }

                ]
            }
        ],
        lowertext:[
            {
                textclass: "box centertext droppable dropbox4",
                textdata: "",
                ans:data.string.plastid
            },
            {
                textclass: "box centertext droppable dropbox5",
                textdata: "",
                ans:data.string.cytoplasm

            },
            {
                textclass: "box centertext droppable dropbox6",
                textdata: "",
                ans:data.string.nucleus
            },
            {
                textclass: "box centertext droppable dropbox7",
                textdata: "",
                ans:data.string.cellwall

            }

        ]

    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "diyImg", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
            {id: "animalcellImg", src: imgpath+"animalcell01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "plantcellImg", src: imgpath+"plantcell01.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p3_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p3_s1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templatecaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1",false);
                count=0;
                shufflehint();
                dragdrop();
                break;
            case 2:
                sound_player("sound_1",false);
                count=0;
                shufflehint();
                dragdrop();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templatecaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templatecaller();
                break;
        }
    });
    $refreshBtn.click(function(){
        templatecaller();
    });
    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templatecaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint(){
        var optdiv = $(".outerdiv");

        for (var i = optdiv.children().length; i >= 0; i--) {
            optdiv.append(optdiv.children().eq(Math.random() * i | 0));
        }
        optdiv.children().removeClass();
        var optionclass = ["box draggable centertext opt1","box draggable centertext opt2","box draggable centertext opt3","box draggable centertext opt4"]
        optdiv.children().each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function dragdrop(){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
            stop:function(event,ui){
                $(".draggable").css("top","");

            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                if(ui.draggable.text().toString().trim() == ($(this).attr("data-answer").toString().trim())) {
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).find("p").text(ui.draggable.text());
                    $(this).removeClass("droppable").addClass("correctcss relativecls");
                    $(this).append("<img class='correctImg' src='images/right.png'/>");
                    count++;
                    if(count>3){
                        navigationcontroller(countNext,$total_page);

                    }
                }
                else {
                    play_correct_incorrect_sound(0);
                    $(this).append("<img class='wrongImg' src='images/wrong.png'/>");
                    ui.draggable.addClass("wrongcss");
                    $(this).addClass("wrongcss");
                }
            }
        });
    }
});

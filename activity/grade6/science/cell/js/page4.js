var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
    {
        //slide 0
        leftblockadditionalclass: "s1card1",
        leftsidediv:[{
            textclass: "slideslide",
            textdata: data.string.animalcell
        }],
        rightblockadditionalclass: "s1card2",
        rightsidediv:[{
            textclass: "slideslide",
            textdata: data.string.plantcell
        }]
    },
    {
        //slide 1
        mainleft:[
            {
                maincaption: data.string.p4text1,
                lowerimg:[
                    {
                        insideimgcontainer: "animalcell",
                        insideimgimg: "animalcellimg",
                    },
                ]
            }
        ],
        mainright:[
            {
                maincaption: data.string.p4text1_1,
                lowerimg:[
                    {
                        insideimgcontainer: "plantcell",
                        insideimgimg: "plantcellimg",
                    },
                ]
            }
        ],
        leftblockadditionalclass: "s2card1",
        leftsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.animalcell
            }
        ],
        rightblockadditionalclass: "s2card2",
        rightsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.plantcell
            }
        ],

        svgblock:[
            {
            svgblock: 'plantcellsvg',
           },
            {
                svgblock: 'animalcellsvg',
            }
        ]
    },
    {
        //slide 2
        mainleft:[
            {
                maincaption: data.string.p4text2,
                lowerimg:[
                    {
                        insideimgcontainer: "animalcell",
                        insideimgimg: "animalcellimg",
                    },
                ]
            }
        ],
        mainright:[
            {
                maincaption: data.string.p4text2_1,
                lowerimg:[
                    {
                        insideimgcontainer: "plantcell",
                        insideimgimg: "plantcellimg",
                    },
                ]
            }
        ],
        leftblockadditionalclass: "s2card1",
        leftsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.animalcell
            }
        ],
        rightblockadditionalclass: "s2card2",
        rightsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.plantcell
            }
        ],
        svgblock:[
            {
                svgblock: 'plantcellsvg',
            },
            {
                svgblock: 'animalcellsvg',
            }
        ]
    },
    {
        //slide 3
        mainleft:[
            {
                maincaption: data.string.p4text3,
                lowerimg:[
                    {
                        insideimgcontainer: "animalcell",
                        insideimgimg: "animalcellimg",
                    },
                ]
            }
        ],
        mainright:[
            {
                maincaption: data.string.p4text3_1,
                lowerimg:[
                    {
                        insideimgcontainer: "plantcell",
                        insideimgimg: "plantcellimg",
                    },
                ]
            }
        ],
        leftblockadditionalclass: "s2card1",
        leftsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.animalcell
            }
        ],
        rightblockadditionalclass: "s2card2",
        rightsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.plantcell
            }
        ],
        svgblock:[
            {
                svgblock: 'plantcellsvg',
            },
            {
                svgblock: 'animalcellsvg',
            }
        ]
    },
    {
        //slide 4
        mainleft:[
            {
                maincaption: data.string.p4text4,
                lowerimg:[
                    {
                        insideimgcontainer: "animalcell",
                        insideimgimg: "animalcellimg",
                    },
                ]
            }
        ],
        mainright:[
            {
                maincaption: data.string.p4text4_1,
                lowerimg:[
                    {
                        insideimgcontainer: "plantcell",
                        insideimgimg: "plantcellimg",
                    },
                ]
            }
        ],
        leftblockadditionalclass: "s2card1",
        leftsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.animalcell
            }
        ],
        rightblockadditionalclass: "s2card2",
        rightsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.plantcell
            }
        ],
        svgblock:[
            {
                svgblock: 'plantcellsvg',
            },
            {
                svgblock: 'animalcellsvg',
            }
        ]
    },
    // {
    //     //slide 5
    //     mainleft:[
    //         {
    //             maincaption: data.string.p4text5,
    //             lowerimg:[
    //                 {
    //                     insideimgcontainer: "animalcell",
    //                     insideimgimg: "animalcellimg",
    //                 },
    //             ]
    //         }
    //     ],
    //     mainright:[
    //         {
    //             maincaption: data.string.p4text5_1,
    //             lowerimg:[
    //                 {
    //                     insideimgcontainer: "plantcell",
    //                     insideimgimg: "plantcellimg",
    //                 },
    //             ]
    //         }
    //     ],
    //     leftblockadditionalclass: "s2card1",
    //     leftsidediv:[
    //         {
    //             textclass: "slideslide",
    //             textdata: data.string.animalcell
    //         }
    //     ],
    //     rightblockadditionalclass: "s2card2",
    //     rightsidediv:[
    //         {
    //             textclass: "slideslide",
    //             textdata: data.string.plantcell
    //         }
    //     ],
    //     svgblock:[
    //         {
    //             svgblock: 'plantcellsvg',
    //         },
    //         {
    //             svgblock: 'animalcellsvg',
    //         }
    //     ]
    // },
    {
        //slide 6
        mainleft:[
            {
                maincaption: data.string.p4text6,
                lowerimg:[
                    {
                        insideimgcontainer: "animalcell",
                        insideimgimg: "animalcellimg",
                    },
                ]
            }
        ],
        mainright:[
            {
                maincaption: data.string.p4text6_1,
                lowerimg:[
                    {
                        insideimgcontainer: "plantcell",
                        insideimgimg: "plantcellimg",
                    },
                ]
            }
        ],
        leftblockadditionalclass: "s2card1",
        leftsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.animalcell
            }
        ],
        rightblockadditionalclass: "s2card2",
        rightsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.plantcell
            }
        ],
        svgblock:[
            {
                svgblock: 'plantcellsvg',
            },
            {
                svgblock: 'animalcellsvg',
            }
        ]
    },
    {
        //slide 7
        mainleft:[
            {
                mainrightadditionalclass:"opt1 option",
                maincaption: data.string.p4text1,
            },
            {
                mainrightadditionalclass:"opt2 option",
                maincaption: data.string.p4text2,
            },
            {
                mainrightadditionalclass:"opt3 option",
                maincaption: data.string.p4text3,
            },
            {
                mainrightadditionalclass:"opt4 option",
                maincaption: data.string.p4text4,
            },
            // {
            //     mainrightadditionalclass:"opt5 option",
            //     maincaption: data.string.p4text5,
            // },
            {
                mainrightadditionalclass:"opt5 option",
                maincaption: data.string.p4text6,
            }
        ],
        mainright:[
            {
                mainrightadditionalclass:"opt1 option",
                maincaption: data.string.p4text1_1,
            },
            {
                mainrightadditionalclass:"opt2 option",
                maincaption: data.string.p4text2_1,
            },
            {
                mainrightadditionalclass:"opt3 option",
                maincaption: data.string.p4text3_1,
            },
            {
                mainrightadditionalclass:"opt4 option",
                maincaption: data.string.p4text4_1,
            },
            // {
            //     mainrightadditionalclass:"opt5 option",
            //     maincaption: data.string.p4text5_1,
            // },
            {
                mainrightadditionalclass:"opt5 option",
                maincaption: data.string.p4text6_1,
            }
        ],
        leftblockadditionalclass: "s2card1",
        leftsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.animalcell
            }
        ],
        rightblockadditionalclass: "s2card2",
        rightsidediv:[
            {
                textclass: "slideslide",
                textdata: data.string.plantcell
            }
        ],
    },
    {
        //slide 8
        contentnocenteradjust: true,
        additionalclasscontentblock: "bg1",
        belowtext:[
            {
                textclass:"title text1",
                textdata: data.string.p4text7
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textcolor",
                textclass: "subtopic text2",
                textdata: data.string.p4text7_1
            },
            {
                textclass:"title content osttext",
                textdata: data.string.osttext
            },
            {
                textclass:"title content hentext",
                textdata: data.string.hentext
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images ostrichEgg",
                    imgclass: "relativecls ostrichEggimg",
                    imgid: 'ostrichEggImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images henEgg",
                    imgclass: "relativecls henEggimg",
                    imgid: 'henEggImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    {
        //slide 9
        contentnocenteradjust: true,
        additionalclasscontentblock: "bg1",
        belowtext:[
            {
                textclass:"title text1",
                textdata: data.string.p4text7
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textcolor",
                textclass: "content text2",
                textdata: data.string.p4text8
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images amoeba",
                    imgclass: "relativecls amoebaimg",
                    imgid: 'amoebaImg',
                    imgsrc: ""
                }
            ]
        }]
    }
];

$(function(){
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;
    var current_sound;
    var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "ostrichEggImg", src: imgpath+"egg01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "purplearrowImg", src: imgpath+"purple_arrow01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "plantcellImg", src: imgpath+"plantcells.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "animalcellImg", src: imgpath+"animalcells.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "amoebaImg", src: imgpath+"ameoba.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "henEggImg", src: imgpath+"egg02.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p4_s0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p4_s1_1.ogg"},
            {id: "sound_1_2", src: soundAsset+"p4_s1_2.ogg"},
            {id: "sound_2_1", src: soundAsset+"p4_s2_1.ogg"},
            {id: "sound_2_2", src: soundAsset+"p4_s2_2.ogg"},
            {id: "sound_3_1", src: soundAsset+"p4_s3_1.ogg"},
            {id: "sound_3_2", src: soundAsset+"p4_s3_2.ogg"},
            {id: "sound_4_1", src: soundAsset+"p4_s4_1.ogg"},
            {id: "sound_4_2", src: soundAsset+"p4_s4_2.ogg"},
            {id: "sound_5_1", src: soundAsset+"p4_s5_1.ogg"},
            {id: "sound_5_2", src: soundAsset+"p4_s5_2.ogg"},
            {id: "sound_7_1", src: soundAsset+"p4_s7_1.ogg"},
            {id: "sound_7_2", src: soundAsset+"p4_s7_2.ogg"},
            {id: "sound_7_3", src: soundAsset+"p4_s7_ostrichegg.ogg"},
            {id: "sound_7_4", src: soundAsset+"p4_s7_henegg.ogg"},
            {id: "sound_7_5", src: soundAsset+"p4_s8.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }

    function generalTemplate(){
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);
        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                loadnaimalcellplantcell();
                playsound("sound_1_1","sound_1_2");
                break;
            case 2:
                loadnaimalcellplantcell("cellmem","cellw","animalcellmem","cellwallanim");
                playsound("sound_2_1","sound_2_2");
                break;
            case 3:
                loadnaimalcellplantcell("mito",null,"mitoch",null);
                playsound("sound_3_1","sound_3_2");
                break;
            case 4:
                loadnaimalcellplantcell("plastida",null,"plastidan",null);
                navigationcontroller(countNext,$total_page);
                playsound("sound_4_1","sound_4_2");
                break;
            case 5:
                loadnaimalcellplantcell("vacul",null,"vacan",null);
                navigationcontroller(countNext,$total_page);
                playsound("sound_5_1","sound_5_2");
                break;
            case 7:
                sound_player("sound_7_1",false);
                setTimeout(function(){
                    sound_player("sound_7_2",false);
                   setTimeout(function(){
                       sound_player("sound_7_3",false);
                       setTimeout(function(){
                           sound_player("sound_7_4",true);
                       },2100);
                   },12000);
                },3000)
                break;
            case 8:
                sound_player("sound_7_1",false);
                setTimeout(function(){
                    sound_player("sound_7_5",true);
                },3000);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }

    function loadnaimalcellplantcell(highlight1,highlight2,class1,class2){
        var s = Snap('#animalcellsvg');
        var svg = Snap.load(preload.getResult('animalcellImg').src, function ( loadedFragment ) {
            s.append(loadedFragment);
            $("."+highlight1).attr("class",class1);
            $("."+highlight2).attr("class",class2);

        });
        var s1 = Snap('#plantcellsvg');
        var svg = Snap.load(preload.getResult('plantcellImg').src, function ( loadedFragment ) {
            s1.append(loadedFragment);
            $("."+highlight1).attr("class",class1);
            $("."+highlight2).attr("class",class2);

        });

    }
    function playsound(firstsound,secondsound){
        setTimeout(function(){
            createjs.Sound.stop();
            current_sound = createjs.Sound.play(firstsound);
            current_sound.play();
            current_sound.on('complete', function () {
                current_sound = createjs.Sound.play(secondsound);
                current_sound.play();
                current_sound.on('complete', function () {
                     navigationcontroller(countNext, $total_page);
                });
            });
        },2000);
    }
    function templateCaller(){
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);



        generalTemplate();
        /*
        for (var i = 0; i < content.length; i++) {
            slides(i);
            $($('.totalsequence')[i]).html(i);
            $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
                "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
        }
        function slides(i){
            $($('.totalsequence')[i]).click(function(){
                countNext = i;
                templateCaller();
            });
        }
        */

    }

    $nextBtn.on("click", function(){
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on("click", function(){
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
// document.addEventListener("contentloaded", function(){
    total_page = content.length;

});

/*===============================================
    =            data highlight function            =
===============================================*/
function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";


    if($alltextpara.length > 0){
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

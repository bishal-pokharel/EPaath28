var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"text1",
        uppertextblock: [
            {
                textclass: "title centertext",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images diy",
                    imgclass: "relativecls diyimg",
                    imgid: 'diyImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "content relativecls centertext",
                textdata: data.string.p5text1
            }
        ],
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext animalcell ansclick",
                        textdata: data.string.animalcell,
                        ans:data.string.animalcell,

                    }
                ]
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext plantcell ansclick",
                        textdata: data.string.plantcell,
                        ans:data.string.plantcell,
                    }
                ]
            }
        ],
        belowtext:[
            {
                textclass:"box question",
                textdata: data.string.p5q1,
                ans:data.string.animalcell
            }
        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "content relativecls centertext",
                textdata: data.string.p5text1
            }
        ],
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext animalcell ansclick",
                        textdata: data.string.animalcell,
                        ans:data.string.animalcell,

                    }
                ]
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext plantcell ansclick",
                        textdata: data.string.plantcell,
                        ans:data.string.plantcell,
                    }
                ]
            }
        ],
        belowtext:[
            {
                textclass:"box question",
                textdata: data.string.p5q2,
                ans:data.string.plantcell
            }
        ],
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "content relativecls centertext",
                textdata: data.string.p5text1
            }
        ],
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext animalcell ansclick",
                        textdata: data.string.animalcell,
                        ans:data.string.animalcell,

                    }
                ]
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext plantcell ansclick",
                        textdata: data.string.plantcell,
                        ans:data.string.plantcell,
                    }
                ]
            }
        ],
        belowtext:[
            {
                textclass:"box question",
                textdata: data.string.p5q3,
                ans:data.string.plantcell
            }
        ],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "content relativecls centertext",
                textdata: data.string.p5text1
            }
        ],
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext animalcell ansclick",
                        textdata: data.string.animalcell,
                        ans:data.string.animalcell,

                    }
                ]
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext plantcell ansclick",
                        textdata: data.string.plantcell,
                        ans:data.string.plantcell,
                    }
                ]
            }
        ],
        belowtext:[
            {
                textclass:"box question",
                textdata: data.string.p5q4,
                ans:data.string.animalcell
            }
        ],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "content relativecls centertext",
                textdata: data.string.p5text1
            }
        ],
        optionblock:[
            {
                optiondiv: "div1",
                imgdiv:"imgdiv",
                imgclass: "relativecls img animalcellimg ansclick",
                imgid: 'animalcellImg',
                imgsrc: imgpath+"animalcell.png",
                ans:data.string.animalcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext animalcell ansclick",
                        textdata: data.string.animalcell,
                        ans:data.string.animalcell,

                    }
                ]
            },
            {
                optiondiv: " div2",
                imgdiv:"imgdiv",
                imgclass: "relativecls img plantcellimg ansclick",
                imgid: 'plantcellImg',
                imgsrc: imgpath+"plantcell.png",
                ans:data.string.plantcell,
                textblock:[
                    {
                        textdiv:"textdiv",
                        textclass: "box centertext plantcell ansclick",
                        textdata: data.string.plantcell,
                        ans:data.string.plantcell,
                    }
                ]
            }
        ],
        belowtext:[
            {
                textclass:"box question",
                textdata: data.string.p5q5,
                ans:data.string.plantcell
            }
        ],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "diyImg", src: imgpath+"ImagesDIY/bg_diy.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p5_s0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p5_s1_1.ogg"},
            {id: "sound_1_2", src: soundAsset+"p5_s1_2.ogg"},
            {id: "sound_2", src: soundAsset+"p5_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p5_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p5_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p5_s5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templatecaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        $('.textdiv').mouseover(function(){
            $(this).addClass('hoverclass');
            $(this).parent().find('img').addClass("imghoverclass");
        });
        $('.textdiv').mouseout(function(){
            $(this).removeClass('hoverclass');
            $(this).parent().find('img').removeClass("imghoverclass");

        });
        $('.img').mouseover(function(){
            $(this).addClass('imghoverclass');
            $(this).parent().parent().find('.textdiv').addClass("hoverclass");
        });
        $('.img').mouseout(function(){
            $(this).removeClass('imghoverclass');
            $(this).parent().parent().find('.textdiv').removeClass("hoverclass");

        });
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1_1",false);
                shufflehint();
                checkans();
                break;
            case 2:
                sound_player("sound_2",false);
                shufflehint();
                checkans();
                break;
            case 3:
                sound_player("sound_3",false);
                shufflehint();
                checkans();
                break;
            case 4:
                sound_player("sound_4",false);
                shufflehint();
                checkans();
                break;
            case 5:
                sound_player("sound_5",false);
                shufflehint();
                checkans();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templatecaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templatecaller();
                break;
        }
    });
    $refreshBtn.click(function(){
        templatecaller();
    });
    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templatecaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint(){
        var optdiv = $(".option");

        for (var i = optdiv.children().length; i >= 0; i--) {
            optdiv.append(optdiv.children().eq(Math.random() * i | 0));
        }
        optdiv.children().removeClass();
        var optionclass = ["div1","div2"]
        optdiv.children().each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }

    function checkans(){
       $(".ansclick").click(function () {
           var currdiv = $(this).parent().parent();
           if($(this).attr("data-answer").toString()==$(".question").attr("data-answer").toString()){
               current_sound.stop();
               play_correct_incorrect_sound(1);
               currdiv.append("<img class='correctImg' src='images/right.png'/>");
               currdiv.find(".textdiv").addClass("correctcss");
               $(".option").addClass("avoid-clicks");
               navigationcontroller(countNext,$total_page);
           }
           else{
               current_sound.stop();
               play_correct_incorrect_sound(0);
               currdiv.append("<img class='correctImg' src='images/wrong.png'/>");
               currdiv.find(".textdiv").addClass("wrongcss");
               currdiv.addClass("avoid-clicks");
           }
       })
    }
});

var imgpath = $ref + "/images/ImagesDIY/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"centertext text1",
        uppertextblock: [
            {
                textclass: "content",
                textdata: data.string.p6text1
            },
            {
                textclass: "title fadeInEffect text2",
                textdata: data.string.p6text1_1
            }

        ],
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"centertext heading",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p6text2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1 zoomInEffect",
                    imgclass: "relativecls cellimg",
                    imgid: 'cellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " img2 zoomInEffect",
                    imgclass: "relativecls tissueimg",
                    imgid: 'tissueImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " img3 zoomInEffect",
                    imgclass: "relativecls organimg",
                    imgid: 'organImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " img4 zoomInEffect",
                    imgclass: "relativecls bodyimg",
                    imgid: 'bodyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow arrow1 zoomInEffect",
                    imgclass: "relativecls yellowarrowimg",
                    imgid: 'yellowarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " arrow arrow2 zoomInEffect",
                    imgclass: "relativecls yellowarrowimg",
                    imgid: 'yellowarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " arrow arrow3 zoomInEffect",
                    imgclass: "relativecls yellowarrowimg",
                    imgid: 'yellowarrowImg',
                    imgsrc: ""
                },
            ]
        }],
        belowtext:[
            {
                textdiv:"topic",
                textclass:"subtopic centertext",
                textdata: data.string.p6text2_1,
            },
            {
                textdiv:"cell",
                textclass:"content centertext",
                textdata: data.string.cell,
            },
            {
                textdiv:"tissue",
                textclass:"content centertext",
                textdata: data.string.tissue,
            },
            {
                textdiv:"organ",
                textclass:"content centertext",
                textdata: data.string.organ,
            },
            {
                textdiv:"humanbody",
                textclass:"content centertext",
                textdata: data.string.humanbody,
            },
        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"centertext heading",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p6text3
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "plant",
                    imgclass: "relativecls plantimg",
                    imgid: 'plantImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "merismetic",
                    imgclass: "merismeticimg",
                    imgid: 'merismeticImg',
                    imgsrc: ""
                }
            ]
        }],
        belowtext:[
            {
                textdiv:"div1",
                textclass:"subtopic centertext",
                textdata: data.string.p6text3_1,
            },
            {
                textdiv:"div2 zoomInEffect",
                textclass:"subtopic centertext",
                textdata: data.string.p6text3_2,
            }
        ],
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"centertext heading",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p6text4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "binaryfission",
                    imgclass: "relativecls binaryfissionimg",
                    imgid: 'binaryfissionImg',
                    imgsrc: ""
                },
            ]
        }],
        belowtext:[
            {
                textdiv:"div1",
                textclass:"subtopic centertext",
                textdata: data.string.p6text4_1,
            }
        ],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"centertext heading",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p6text5
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "regulate02",
                    imgclass: "relativecls regulate02img",
                    imgid: 'regulate02Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "regulate",
                    imgclass: "relativecls regulateimg",
                    imgid: 'regulateImg',
                    imgsrc: ""
                },
            ]
        }],
        belowtext:[
            {
                textdiv:"div1",
                textclass:"subtopic centertext",
                textdata: data.string.p6text5_1,
            },
            {
                textdiv:"div3 zoomInEffect",
                textclass:"subtopic centertext",
                textdata: data.string.p6text5_2,
            },
            {
                textdiv:"slideCo2",
                textclass:"subtopic centertext",
                textdata: data.string.co2,
                sub:true
            },
            {
                textdiv:"slideo2",
                textclass:"subtopic centertext",
                textdata: data.string.oxygen,
                sub:true
            }
        ],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"centertext heading",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p6text6
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bg_nutrients",
                    imgclass: "relativecls bg_nutrientsimg",
                    imgid: 'bg_nutrientsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "tree",
                    imgclass: "relativecls treeimg",
                    imgid: 'treeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls sunimg",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "yellowarrow",
                    imgclass: "relativecls yellowarrowimg",
                    imgid: 'yellowarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "bluearrow",
                    imgclass: "relativecls bluearrowimg",
                    imgid: 'bluearrowImg',
                    imgsrc: ""
                },
            ]
        }],
        belowtext:[
            {
                textdiv:"div1",
                textclass:"subtopic centertext",
                textdata: data.string.p6text6_1,
            },
            {
                textdiv:"food",
                textclass:"subtopic centertext",
                textdata: data.string.food,
            },
            {
                textdiv:"water",
                textclass:"subtopic centertext",
                textdata: data.string.water,
            },
            {
                textdiv:"minerals",
                textclass:"subtopic centertext",
                textdata: data.string.minerals,
            },
            {
                textdiv:"carbondioxide co",
                textclass:"subtopic centertext",
                textdata: data.string.co2,
                sub:true
            },
            {
                textdiv:"oxygen oxy",
                textclass:"subtopic centertext",
                textdata: data.string.oxygen,
                sub:true
            }
        ],
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"text1 fadeInEffect",
        uppertextblock: [
            {
                textclass: "centertext content",
                textdata: data.string.p6text7
            }
        ],
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1 im1 zoomInEffect",
                    imgclass: "relativecls cellimg",
                    imgid: 'cellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " img2 im2 zoomInEffect",
                    imgclass: "relativecls tissueimg",
                    imgid: 'tissueImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " img3 im3 zoomInEffect",
                    imgclass: "relativecls organimg",
                    imgid: 'organImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " img4 im4 zoomInEffect",
                    imgclass: "relativecls bodyimg",
                    imgid: 'bodyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow arrow1 arrimg zoomInEffect",
                    imgclass: "relativecls yellowarrowimg",
                    imgid: 'yellowarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " arrow arrow2 arrimg zoomInEffect",
                    imgclass: "relativecls yellowarrowimg",
                    imgid: 'yellowarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " arrow arrow3 arrimg zoomInEffect",
                    imgclass: "relativecls yellowarrowimg",
                    imgid: 'yellowarrowImg',
                    imgsrc: ""
                },
            ]
        }],
        belowtext:[
            {
                textdiv:"topic t1",
                textclass:"subtopic centertext",
                textdata: data.string.p6text8,
            },
            {
                textdiv:"topic1 t2 slideR",
                textclass:"subtopic centertext",
                textdata: data.string.p6text8_1,
            },
            {
                textdiv:"cell parts",
                textclass:"content centertext",
                textdata: data.string.cell,
            },
            {
                textdiv:"tissue parts",
                textclass:"content centertext",
                textdata: data.string.tissue,
            },
            {
                textdiv:"organ parts",
                textclass:"content centertext",
                textdata: data.string.organ,
            },
            {
                textdiv:"humanbody parts",
                textclass:"content centertext",
                textdata: data.string.humanbody,
            },
        ],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "cellImg", src: imgpath+"animalcell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tissueImg", src: imgpath+"humancell_01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "organImg", src: imgpath+"corazon-human.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "bodyImg", src: imgpath+"human.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowarrowImg", src: imgpath+"yellow_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "merismeticImg", src: imgpath+"meristematictissue-01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "plantImg", src: imgpath+"plant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "binaryfissionImg", src: imgpath+"binary_fission.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "regulateImg", src: imgpath+"regulate.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "regulate02Img", src: imgpath+"regulate02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_nutrientsImg", src: imgpath+"bg_transport-nutrients.png", type: createjs.AbstractLoader.IMAGE},
            {id: "treeImg", src: imgpath+"transportnutrients.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bluearrowImg", src: imgpath+"blue_arrow.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_0", src: soundAsset+"p6_s0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p6_s1_1.ogg"},
            {id: "sound_1_2", src: soundAsset+"p6_s1_2.ogg"},
            {id: "sound_1_3", src: soundAsset+"p6_s1_3.ogg"},
            {id: "sound_2_1", src: soundAsset+"p6_s2_1.ogg"},
            {id: "sound_2_2", src: soundAsset+"p6_s2_2.ogg"},
            {id: "sound_2_3", src: soundAsset+"p6_s2_3.ogg"},
            {id: "sound_3_1", src: soundAsset+"p6_s3_1.ogg"},
            {id: "sound_3_2", src: soundAsset+"p6_s3_2.ogg"},
            {id: "sound_4_1", src: soundAsset+"p6_s4_1.ogg"},
            {id: "sound_4_2", src: soundAsset+"p6_s4_2.ogg"},
            {id: "sound_4_3", src: soundAsset+"p6_s4_3.ogg"},
            {id: "sound_5_1", src: soundAsset+"p6_s5_1.ogg"},
            {id: "sound_5_2", src: soundAsset+"p6_s5_2.ogg"},
            {id: "sound_6", src: soundAsset+"p6_s6.ogg"},
            {id: "sound_7_1", src: soundAsset+"p6_s7_1.ogg"},
            {id: "sound_7_2", src: soundAsset+"p6_s7_2.ogg"},
            {id: "sound_7_3", src: soundAsset+"p6_s7_3.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templatecaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1_1",false);
                showImages("sound_1_2");
                break;
            case 2:
                play_sound_sequence(["sound_2_1","sound_2_2","sound_2_3"],true);
                break;
            case 3:
                play_sound_sequence(["sound_3_1","sound_3_2"],true);
                break;
            case 4:
                play_sound_sequence(["sound_4_1","sound_4_2","sound_4_3"],true);
                break;
            case 5:
                play_sound_sequence(["sound_5_1","sound_5_2"],true);
                break;
            case 6:
                sound_player("sound_6",true);
                break;
            case 7:
                showImages(null);
                play_sound_sequence(["sound_7_1","sound_7_3"],true)
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }


    function play_sound_sequence(soundarray, navflag){
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                setTimeout(function(){
                    play_sound_sequence(soundarray, navflag);
                },1000);
            }else{
                if(navflag)
                    navigationcontroller(countNext,$total_page,countNext==7?true:false);
            }

        });
    }


    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page,countNext>6?true:false):"";
        });
    }

    function sound_sequence(sound_id1,navigate) {

    }

    function templatecaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templatecaller();
                break;
        }
    });
    $refreshBtn.click(function(){
        templatecaller();
    });
    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templatecaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }


    function showImages(firstsound){
        $(".img1,.img2,.img3,.img4,.cell,.tissue,.organ,.humanbody,.arrow,.t2").hide();
        setTimeout(function(){
            $(".img1,.arrow1,.cell").show();
            setTimeout(function(){
                $(".img2,.arrow2,.tissue").show();
                setTimeout(function(){
                    firstsound?sound_player(firstsound,true):'';
                    $(".img3,.arrow3,.organ").show();
                    setTimeout(function(){
                        $(".img4,.humanbody").show();
                        setTimeout(function(){
                            $(".t2").show();
                        },1000)
                    },1000);
                },1000);
            },1000);
        },1000);
    }


});

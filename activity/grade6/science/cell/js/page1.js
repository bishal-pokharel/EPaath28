var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"coverpagetext centertext",
        uppertextblock: [
            {
                textclass: "chapter lessontitle",
                textdata: data.lesson.chapter
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images coverpage",
                    imgclass: "relativecls coverimg",
                    imgid: 'coverImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        listedblock: [
            {
                textclass: "subtopic text1 fadeInEffect",
                textdata: [
                    {
                    contenttext: data.string.p1text1,
                    }
                ]
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images boy",
                    imgclass: "relativecls boyimg",
                    imgid: 'boyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images scaleupImg humancell",
                    imgclass: "relativecls humancellimg",
                    imgid: 'humancellImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"slideUpEffect",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p1text3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images zoomInEffect animalcell",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"text2 fadeOutEffect",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p1text3
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images animalcell fadeOutEffect",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images boy2",
                    imgclass: "relativecls boyimg",
                    imgid: 'boyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images fish zoomInEffect",
                    imgclass: "relativecls fishimg",
                    imgid: 'fishImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images deer zoomInEffect",
                    imgclass: "relativecls deerimg",
                    imgid: 'deerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hen zoomInEffect",
                    imgclass: "relativecls henimg",
                    imgid: 'henImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images plant zoomInEffect",
                    imgclass: "relativecls plantimg",
                    imgid: 'plantImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"maindiv content",
                textdata:data.string.p1text4
            }
        ]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "images boy3",
                    imgclass: "relativecls boyimg",
                    imgid: 'boyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images fish",
                    imgclass: "relativecls fishimg",
                    imgid: 'fishImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images deer slideLeft animalcellshow",
                    imgclass: "relativecls deerimg",
                    imgid: 'deerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hen",
                    imgclass: "relativecls henimg",
                    imgid: 'henImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images plant",
                    imgclass: "relativecls plantimg",
                    imgid: 'plantImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hand animalcellshow",
                    imgclass: "relativecls handiconimg",
                    imgid: 'handicon',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow",
                    imgclass: "relativecls arrowimg",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images animalcelldiv",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow1",
                    imgclass: "relativecls arrowimg1",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images plantcelldiv",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"maindiv content textalignment",
                textdata:data.string.p1text5
            },
            {
                textdiv:"content animalcelltext celltext",
                textdata:data.string.animalcell
            },
            {
                textdiv:"content plantcelltext celltext",
                textdata:data.string.plantcell
            }
        ]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images deer1 fadeOutEffect",
                    imgclass: "relativecls deerimg",
                    imgid: 'deerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images plant fadeOutEffect",
                    imgclass: "relativecls plantimg",
                    imgid: 'plantImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow fadeOutEffect",
                    imgclass: "relativecls arrowimg",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images animalcelldiv",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow1 fadeOutEffect",
                    imgclass: "relativecls arrowimg1",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images plantcelldiv",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"content animalcelltext celltext",
                textdata:data.string.animalcell
            },
            {
                textdiv:"content plantcelltext celltext",
                textdata:data.string.plantcell
            }
        ]
    },
//slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images animalcelldiv1",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images plantcelldiv1",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"content animalcelltext1 celltext",
                textdata:data.string.animalcell
            },
            {
                textdiv:"content plantcelltext1 celltext",
                textdata:data.string.plantcell
            },
            {
                textdiv:"subtopic belowtext1 slideL",
                textdata:data.string.p1text6
            }
        ]
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images animalcelldiv1",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images plantcelldiv1",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"content animalcelltext1 celltext",
                textdata:data.string.animalcell
            },
            {
                textdiv:"content plantcelltext1 celltext",
                textdata:data.string.plantcell
            },
            {
                textdiv:"subtopic belowtext1 slideR",
                textdata:data.string.p1text7
            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "coverImg", src: imgpath+"coverpage.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "boyImg", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
            {id: "humancellImg", src: imgpath+"humancell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "animalcellImg", src: imgpath+"animalcell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fishImg", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
            {id: "deerImg", src: imgpath+"deer.png", type: createjs.AbstractLoader.IMAGE},
            {id: "henImg", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
            {id: "plantImg", src: imgpath+"plant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handicon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath+"arrow05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "plantcellImg", src: imgpath+"plantcell.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p1_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p1_s1.ogg"},
            {id: "sound_2", src: soundAsset+"p1_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p1_s3.ogg"},
            {id: "sound_3_0", src: soundAsset+"p1_s3_0.ogg"},
            {id: "sound_3_1", src: soundAsset+"p1_s3_1.ogg"},
            {id: "sound_3_2", src: soundAsset+"p1_s3_2.ogg"},
            {id: "sound_3_3", src: soundAsset+"p1_s3_3.ogg"},
            {id: "sound_3_4", src: soundAsset+"p1_s3_4.ogg"},
            {id: "sound_3_5", src: soundAsset+"p1_s3_5.ogg"},
            {id: "sound_4_1", src: soundAsset+"p1_s4_1.ogg"},
            {id: "sound_4_2", src: soundAsset+"p1_s4_2.ogg"},
            {id: "sound_6", src: soundAsset+"p1_s6.ogg"},
            {id: "sound_7", src: soundAsset+"p1_s7.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                animateboycell();
                break;
            case 2:
                animateCell();
                break;
            case 3:
                animateAnimals();
                break;
            case 4:
                animateAnimalPlant();
                break;
            case 5:
                animateAnimalPlantCell();
                break;
            case 6:
                setTimeout(function(){
                    sound_player("sound_6",true);
                },1000);
                break;
            case 7:
                setTimeout(function(){
                    sound_player("sound_7",true);
                },1000);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

    function animateboycell(){
        $(".text1,.humancell").hide(0);
        setTimeout(function(){
            $(".humancell").show(0);
            setTimeout(function(){
                $(".text1").show(0);
                sound_player("sound_1",true);
            },3000);
        },1800);
    }

   function animateCell(){
       $(".animalcell").hide(0);
       setTimeout(function(){
           $(".animalcell").show(0);
           sound_player("sound_2",true);
       },2000);
   }
   function animateAnimals(){
       $(".boy2,.fish,.deer,.hen,.plant,.maindiv").hide(0);
       setTimeout(function(){
           $(".boy2").show(0);
           setTimeout(function(){
               sound_player("sound_3_0",true);
               $(".maindiv").show(0);
               setTimeout(function(){
                   $(".fish").show(0);
                   sound_player("sound_3_1",true);
                   $(".maindiv").append("&nbsp;"+data.string.p1text4_1);
                   setTimeout(function () {
                       $(".deer").show(0);
                       sound_player("sound_3_2",true);
                       $(".maindiv").append("&nbsp;"+data.string.p1text4_2);
                       setTimeout(function () {
                           $(".hen").show(0);
                           sound_player("sound_3_3",true);
                           $(".maindiv").append("&nbsp;"+data.string.p1text4_3);
                           setTimeout(function () {
                               $(".plant").show(0);
                               sound_player("sound_3_4",true);
                               $(".maindiv").append("&nbsp;"+data.string.p1text4_4);
                               setTimeout(function () {
                                   $(".maindiv").append("&nbsp;"+data.string.p1text4_5).addClass("textalignment");
                                   sound_player("sound_3_5",true);
                                   },1300);
                           },2000);
                       },2000);
                   },2000);
               },1000);
           },3000);
       },2000);
   }

   function animateAnimalPlant(){
       $(".hand,.arrow,.animalcelldiv,.animalcelltext,.arrow1,.plantcelldiv,.plantcelltext").hide(0);
           $(".boy3,.fish,.hen,.maindiv").addClass("fadeOutEffect");
           setTimeout(function(){
               $(".deer").addClass("slideToLeft");
               setTimeout(function(){
                   $(".hand").show(0);
                   $(".animalcellshow").click(function(){
                       showAnimalCell();
                   });
               },3000)
           },500);

   }
   function showAnimalCell(){
       $(".arrow,.animalcelldiv,.animalcelltext").show(0);
       $(".hand").hide(0);
       sound_player("sound_4_1",false);
       setTimeout(function(){
           $(".deer,.hand").removeClass("animalcellshow");
           $(".plant,.hand").addClass("animalcellshow1");
           $(".hand").show(0).css("left","85%");
           $(".animalcellshow1").click(function(){
               $(".hand").empty();
               $(".plant,.hand").removeClass("animalcellshow1");
               $(".arrow1,.plantcelldiv,.plantcelltext").show(0);
               sound_player("sound_4_2",true);
           });
       },1000);
   }
   function animateAnimalPlantCell(){
      setTimeout(function(){
          $(".animalcelldiv").addClass("animalCellMove");
          $(".plantcelldiv").addClass("plantCellMove");
          $(".animalcelltext").addClass("animalTextMove");
          $(".plantcelltext").addClass("plantTextMove");
          navigationcontroller(countNext,$total_page);
      },500);
   }
});

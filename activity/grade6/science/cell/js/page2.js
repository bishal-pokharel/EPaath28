var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"text1",
        uppertextblock: [
            {
                textclass: "title centertext1",
                textdata: data.string.p2text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell slideL",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"text1",
        uppertextblock: [
            {
                textclass: "title centertext1",
                textdata: data.string.p2text2
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell1",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow1",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hand showInfo",
                    imgclass: "relativecls handiconimg",
                    imgid: 'handicon',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"parts boxcss",
                innerdiv:"centertext showInfo point",
                textdata:[
                    {
                        textclass:"content topic",
                        datacontent:data.string.p2text2_1
                    },
                    {
                        textclass:"box description",
                        datacontent:data.string.p2text2_2
                    }
                ]
            },
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell2",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images animalcell",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow3",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow4",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hand1 showInfo",
                    imgclass: "relativecls handiconimg",
                    imgid: 'handicon',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"parts1 boxcss",
                innerdiv:"centertext showInfo point",
                textdata:[
                    {
                        textclass:"content topic",
                        datacontent:data.string.p2text3
                    },
                    {
                        textclass:"box description",
                        datacontent:data.string.p2text3_1
                    }
                ]
            },
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell2",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images animalcell",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow5",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow6",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hand1 showInfo",
                    imgclass: "relativecls handiconimg",
                    imgid: 'handicon',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"parts1 boxcss",
                innerdiv:"centertext showInfo point",
                textdata:[
                    {
                        textclass:"content topic",
                        datacontent:data.string.p2text4
                    },
                    {
                        textclass:"box description",
                        datacontent:data.string.p2text4_1
                    }
                ]
            },
        ]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell2",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images animalcell",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow7",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow8",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hand1 showInfo",
                    imgclass: "relativecls handiconimg",
                    imgid: 'handicon',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"parts1 boxcss",
                innerdiv:"centertext showInfo point",
                textdata:[
                    {
                        textclass:"content topic",
                        datacontent:data.string.p2text5
                    },
                    {
                        textclass:"box description",
                        datacontent:data.string.p2text5_1
                    }
                ]
            },
        ]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell2",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images animalcell",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow9",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow10",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hand1 showInfo",
                    imgclass: "relativecls handiconimg",
                    imgid: 'handicon',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"parts1 boxcss",
                innerdiv:"centertext showInfo point",
                textdata:[
                    {
                        textclass:"content topic",
                        datacontent:data.string.p2text6
                    },
                    {
                        textclass:"box description",
                        datacontent:data.string.p2text6_1
                    }
                ]
            },
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell2",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images animalcell",
                    imgclass: "relativecls animalcellimg",
                    imgid: 'animalcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow11",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow12",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hand1 showInfo",
                    imgclass: "relativecls handiconimg",
                    imgid: 'handicon',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"parts1 boxcss",
                innerdiv:"centertext showInfo point",
                textdata:[
                    {
                        textclass:"content topic",
                        datacontent:data.string.p2text7
                    },
                    {
                        textclass:"box1 description",
                        datacontent:data.string.p2text7_1
                    }
                ]
            },
        ]
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images plantcell1",
                    imgclass: "relativecls plantcellimg",
                    imgid: 'plantcellImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images arrow13",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: " images hand showInfo",
                    imgclass: "relativecls handiconimg",
                    imgid: 'handicon',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"parts boxcss",
                innerdiv:"centertext showInfo point",
                textdata:[
                    {
                        textclass:"content topic",
                        datacontent:data.string.p2text8
                    },
                    {
                        textclass:"box description",
                        datacontent:data.string.p2text8_1
                    }
                ]
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "plantcellImg", src: imgpath+"plantcell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow1Img", src: imgpath+"arrow03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handicon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "animalcellImg", src: imgpath+"animalcell.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p2_s0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p2_s1_1.ogg"},
            {id: "sound_1_2", src: soundAsset+"p2_s1_2.ogg"},
            {id: "sound_1_3", src: soundAsset+"p2_s1_3.ogg"},
            {id: "sound_2_1", src: soundAsset+"p2_s2_1.ogg"},
            {id: "sound_2_2", src: soundAsset+"p2_s2_2.ogg"},
            {id: "sound_3_1", src: soundAsset+"p2_s3_1.ogg"},
            {id: "sound_3_2", src: soundAsset+"p2_s3_2.ogg"},
            {id: "sound_4_1", src: soundAsset+"p2_s4_1.ogg"},
            {id: "sound_4_2", src: soundAsset+"p2_s4_2.ogg"},
            {id: "sound_5_1", src: soundAsset+"p2_s5_1.ogg"},
            {id: "sound_5_2", src: soundAsset+"p2_s5_2.ogg"},
            {id: "sound_6_1", src: soundAsset+"p2_s6_1.ogg"},
            {id: "sound_6_2", src: soundAsset+"p2_s6_2.ogg"},
            {id: "sound_7_1", src: soundAsset+"p2_s7_1.ogg"},
            {id: "sound_7_2", src: soundAsset+"p2_s7_2.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templatecaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1_1",false);
                showInformation("sound_1_2","sound_1_3");
                break;
            case 2:
                $(".parts1").css("height","20%");
                showInformation1("sound_2_1","sound_2_2");
                break;
            case 3:
                showInformation1("sound_3_1","sound_3_2");
                break;
            case 4:
                showInformation1("sound_4_1","sound_4_2");
                break;
            case 5:
                showInformation1("sound_5_1","sound_5_2");
                break;
            case 6:
                showInformation1("sound_6_1","sound_6_2");
                break;
            case 7:
                sound_player("sound_7_1",false,".hand");
                showInformation(null,"sound_7_2");
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate,showElem) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
            $(showElem).show();
        });
    }


    function templatecaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templatecaller();
                break;
        }
    });
    $refreshBtn.click(function(){
        templatecaller();
    });
    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templatecaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

   function showInformation(firstsnd,secondsnd){
       $(".description,.hand").hide();
       setTimeout(function(){
           firstsnd?sound_player(firstsnd,false,".hand"):'';
       },3200);
        $(".showInfo").click(function(){
            $(this).removeClass("point");
          $(".parts").addClass("partsdesc");
            $(".hand").empty();
            setTimeout(function(){
                $(".description").show();
                sound_player(secondsnd,true);
            },2000);
        });

   }
    function showInformation1(firstsnd,secondsnd){
        $(".description,.hand1").hide();
        sound_player(firstsnd,false,".hand1");
        $(".showInfo").click(function(){
            $(this).removeClass("point");
            $(".parts1").addClass("partsdesc1");
            $(".hand1").empty();
            setTimeout(function(){
                $(".description").show();
                sound_player(secondsnd,true);
            },2000);
        });

    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//coverpage
	{
		contentblockadditionalclass:"blue_bg",
		extratextblock:[{
			textclass: "cvTxt",
			textdata: data.lesson.chapter
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'coverpage',
				imgsrc: ""
			}]
		}]
	},
	//slide 2
	{
		contentblockadditionalclass:"blue_bg",
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s2
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'mainBg',
				imgsrc: ""
			},{
				imgclass: "girl_skipping",
				imgid: 'girl_skipping',
				imgsrc: ""
			},{
				imgclass: "girlCycling",
				imgid: 'girl-in-cycle',
				imgsrc: ""
			},{
				imgclass: "girls_swinging",
				imgid: 'girls_swinging',
				imgsrc: ""
			},{
				imgclass: "dilak",
				imgid: 'dilak',
				imgsrc: ""
			},{
				imgclass: "playing_dog",
				imgid: 'playing_dog',
				imgsrc: ""
			}]
		}]
	},
	//slide 3
	{
		contentblockadditionalclass:"blue_bg",
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s3
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"allImgCont",
			imagestoshow:[{
	      imgclass: "background zoom",
	      imgid: 'mainBg',
				imgsrc: ""
			},{
				imgclass: "girl_skipping",
				imgid: 'girl_skipping',
				imgsrc: ""
			},
			{
				imgclass: "fotbal_gif",
				imgid: 'football',
				imgsrc: ""
			},
			{
				imgclass: "girls_swinging",
				imgid: 'girls_swinging',
				imgsrc: ""
			},{
				imgclass: "fotbal_png",
				imgid: 'football_png',
				imgsrc: ""
			},{
				imgclass: "playing_dog ",
				imgid: 'playing_dog',
				imgsrc: ""
			}]
		}]
	},
	//slide 4
	{
		contentblockadditionalclass:"blue_bg",
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s5
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'dilak_on_bench01',
				imgsrc: ""
			}]
		}]
	},
	//slide 5
	{
		contentblockadditionalclass:"blue_bg",
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s6
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'dilak_on_bench01',
				imgsrc: ""
			},{
	      imgclass: "swngGrl",
	      imgid: 'girls_swinging',
				imgsrc: ""
			}]
		}]
	},
	//slide 6
	{
		contentblockadditionalclass:"blue_bg",
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s7
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'dilak_on_bench',
				imgsrc: ""
			},{
	      imgclass: "ballS7",
	      imgid: 'ball',
				imgsrc: ""
			},{
	      imgclass: "melisha",
	      imgid: 'melisha_kick_ball_new',
				imgsrc: ""
			}]
		}]
	},
	//slide 7
	{
		contentblockadditionalclass:"blue_bg",
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s8
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'dilak_on_bench',
				imgsrc: ""
			},{
	      imgclass: "sliding_s8",
	      imgid: 'sliding',
				imgsrc: ""
			}]
		}]
	},
	//slide 8
	{
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s9
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'mainBg',
				imgsrc: ""
			},{
	      imgclass: "dilak_thinking",
	      imgid: 'dilak_thinking',
				imgsrc: ""
			},{
	      imgclass: "clouds9 cldfst",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "clouds9 cldSec",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "clouds9 cldThrd",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "hdnPic melisha_s9",
	      imgid: 'melisha',
				imgsrc: ""
			},{
	      imgclass: "hdnPic slide",
	      imgid: 'slide',
				imgsrc: ""
			},{
	      imgclass: "hdnPic swing_s9",
	      imgid: 'swingPng',
				imgsrc: ""
			}]
		}]
	},
	//slide 9
	{
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s10
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'mainBg',
				imgsrc: ""
			},{
	      imgclass: "dilak_thinking",
	      imgid: 'dilak_thinking',
				imgsrc: ""
			},{
	      imgclass: "clouds cldfst",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "clouds cldSec",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "clouds cldThrd",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "melisha_s9",
	      imgid: 'melisha',
				imgsrc: ""
			},{
	      imgclass: "slide",
	      imgid: 'slide',
				imgsrc: ""
			},{
	      imgclass: "swing_s9",
	      imgid: 'swingPng',
				imgsrc: ""
			}]
		}]
	},
	//slide 10
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p1s11_2
		}],
		sideboxes:[{
			sideboxclass:"leftBox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"purpleTxt",
				textclass: "sideBoxTxt",
				textdata: data.string.p1s11_1
			}]
		},
		{
			sideboxclass:"rightbox yellowbg",
			imageblock:[{
				imagestoshow:[{
		      imgclass: "sliding_s8 s11Sld sldgf",
		      imgid: 'sliding',
					imgsrc: ""
				},{
		      imgclass: "sliding_s8 s11Sld sldpng",
		      imgid: 'sliding_png',
					imgsrc: ""
				}]
			}]
		}]
		// imageblock:[{
		// 	imagestoshow:[{
	  //     imgclass: "background",
	  //     imgid: 'mainBg',
		// 		imgsrc: ""
		// 	}]
		// }]
	},
	//slide 11---------new
	{
		extratextblock:[{
			textclass: "btmtxt",
			textdata: data.string.p1s11_new
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'mainBg',
				imgsrc: ""
			},{
	      imgclass: "dilak_thinking",
	      imgid: 'dilak_thinking',
				imgsrc: ""
			},{
	      imgclass: "clouds9 cldfst",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "clouds9 cldSec",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "clouds9 cldThrd",
	      imgid: 'clouds',
				imgsrc: ""
			},{
	      imgclass: "hdnPic melisha_s9",
	      imgid: 'melisha',
				imgsrc: ""
			},{
	      imgclass: "hdnPic slide",
	      imgid: 'slide',
				imgsrc: ""
			},{
	      imgclass: "hdnPic swing_s9",
	      imgid: 'swingPng',
				imgsrc: ""
			}]
		}]
	},
	// //slide 11
	// {
	// 	imageload:true,
	// 	extratextblock:[{
	// 		textclass: "toptxt",
	// 		textdata: data.string.p1s12_1
	// 	}],
	// 	sideboxes:[{
	// 		sideboxclass:"leftBox yellowbg",
	// 		sideboxtxt:[{
	// 			datahighlightflag:"true",
	// 			datahighlightcustomclass:"purpleTxt",
	// 			textclass: "btmSec btmtxt creambg",
	// 			textdata: data.string.p1s12_2
	// 		}],
	// 		imageblock:[{
	// 			imagestoshow:[{
	// 	      imgclass: "grlSwingLeft swngGf",
	// 	      imgid: 'girls_swinging',
	// 				imgsrc: ""
	// 			},{
	// 	      imgclass: "grlSwingLeft swg",
	// 	      imgid: 'swingPng',
	// 				imgsrc: ""
	// 			}]
	// 		}]
	// 	},
	// 	{
	// 		sideboxclass:"rightbox whitebg",
	// 		sideboxtxt:[{
	// 			datahighlightflag:"true",
	// 			datahighlightcustomclass:"purpleTxt",
	// 			textclass: "btmSec btmtxt creambg",
	// 			textdata: data.string.p1s12_3
	// 		}],
	// 		imageblock:[{
	// 			imagestoshow:[{
	// 	      imgclass: "ballRight bl1",
	// 	      imgid: 'ball',
	// 				imgsrc: ""
	// 			},{
	// 	      imgclass: "ballRight bl2",
	// 	      imgid: 'ball',
	// 				imgsrc: ""
	// 			}],
	// 			imagelabels:[{
	// 				imagelabelclass: "figName fntop",
	// 				imagelabeldata: data.string.p1s12_4
	// 			},{
	// 				imagelabelclass: "figName fnbtm",
	// 				imagelabeldata: data.string.p1s12_5
	// 			}]
	// 		}]
	// 	}]
	// },
	// //slide 12
	// {
	// 	contentblockadditionalclass:"orangeBox",
	// 	imageload:true,
	// 	extratextblock:[{
	// 		textclass: "toptxt",
	// 		textdata: data.string.p1s13_1
	// 	},{
	// 		datahighlightflag:'true',
	// 		datahighlightcustomclass:"purpleTxt",
	// 		textclass: "btmtxt creambg",
	// 		textdata: data.string.p1s13_2
	// 	}],
	// 	imageblock:[{
	// 		imagestoshow:[{
	// 			imgclass: "flyGif",
	// 			imgid: 'fly',
	// 			imgsrc: ""
	// 		}]
	// 	}]
	// },
	// //slide 13
	// {
	// 	contentblockadditionalclass:"orangeBox",
	// 	imageload:true,
	// 	extratextblock:[{
	// 		textclass: "toptxt",
	// 		textdata: data.string.p1s14_1
	// 	},{
	// 		datahighlightflag:'true',
	// 		datahighlightcustomclass:"purpleTxt",
	// 		textclass: "btmtxt creambg left100",
	// 		textdata: data.string.p1s14_2
	// 	}],
	// 	imageblock:[{
	// 		imagestoshow:[{
	// 			imgclass: "grlCycleS14",
	// 			imgid: 'girl-in-cycle',
	// 			imgsrc: ""
	// 		}]
	// 	}]
	// },
	// //slide 14
	// {
	// 	contentblockadditionalclass:"greeenbg",
	// 	imageload:true,
	// 	extratextblock:[{
	// 		datahighlightflag:'true',
	// 		datahighlightcustomclass:"purpleTxt",
	// 		textclass: "toptxt",
	// 		textdata: data.string.p1s15_1
	// 	},{
	// 		datahighlightflag:'true',
	// 		datahighlightcustomclass:"purpleTxt",
	// 		textclass: "btmtxt creambg",
	// 		textdata: data.string.p1s15_2
	// 	}],
	// 	imageblock:[{
	// 		imagestoshow:[{
	// 			imgclass: "guleli",
	// 			imgid: 'guleli',
	// 			imgsrc: ""
	// 		},{
	// 			imgclass: "busBg",
	// 			imgid: 'bg_for_bus',
	// 			imgsrc: ""
	// 		},{
	// 			imgclass: "school_bus",
	// 			imgid: 'school_bus',
	// 			imgsrc: ""
	// 		}]
	// 	}]
	// },
	// //slide 15
	// {
	// 	contentblockadditionalclass:"greeenbg",
	// 	speechbox:[{
	// 		speechbox:"sp-1",
	// 		imgclass:"spImg",
	// 		imgid:"text_box",
	// 		datahighlightflag:'true',
	// 		datahighlightcustomclass:"purpleTxt",
	// 		textclass: "sptxt",
	// 		textdata: data.string.p1s16
	// 	}],
	// 	imageblock:[{
	// 		imagestoshow:[{
	// 			imgclass: "dilakLastPage",
	// 			imgid: 'dilak_01',
	// 			imgsrc: ""
	// 		}]
	// 	}]
	// },
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "mainBg", src: imgpath+"main_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-in-cycle", src: imgpath+"girl-in-cycle.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_skipping", src: imgpath+"girl_skipping.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girls_swinging", src: imgpath+"girls_swinging.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak", src: imgpath+"dilak.png", type: createjs.AbstractLoader.IMAGE},
			{id: "playing_dog", src: imgpath+"playing_dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_on_bench", src: imgpath+"dilak_on_bench.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_on_bench01", src: imgpath+"dilak_on_bench01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_thinking", src: imgpath+"dilak_thinking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "melisha", src: imgpath+"melisha.png", type: createjs.AbstractLoader.IMAGE},
			{id: "slide", src: imgpath+"slide.png", type: createjs.AbstractLoader.IMAGE},
			{id: "swingPng", src: imgpath+"girls-swinging.png", type: createjs.AbstractLoader.IMAGE},
			{id: "guleli", src: imgpath+"guleli.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_bus", src: imgpath+"bg_for_bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school_bus", src: imgpath+"school_bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_01", src: imgpath+"dilak_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fly", src: imgpath+"fly.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sliding", src: imgpath+"sliding.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "football", src: imgpath+"football.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "football_png", src: imgpath+"football.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sliding_png", src: imgpath+"sliding.png", type: createjs.AbstractLoader.IMAGE},
			{id: "melisha_kick_ball_new", src: imgpath+"melisha_kick_ball_new.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10_1", src: soundAsset+"s1_p10_1.ogg"},
			{id: "s1_p10_2", src: soundAsset+"s1_p10_2.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});girlCycling
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):'';
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 2:
				setTimeout(function(){
					$(".girls-swinging").remove();
				},3000);
				$(".fotbal_gif").delay(3000).show(0);
				$(".fotbal_png").delay(3100).hide(0);
					sound_player("s1_p"+(countNext+1),1);
				// nav_button_controls();
			break;
			case 7:
				sound_player("s1_p"+(countNext+1),1);
				$(".cldThrd").animate({
					opacity:"1"
				},1000,function(){
					$(".cldSec").animate({
						opacity:"1"
					},500,function(){
						$(".cldfst, .hdnPic").animate({
							opacity:"1"
						},500);
					});
				});
					// nav_button_controls();
			break;
			case 9:
				$(".sldgf").hide(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p10_1");
					current_sound.play();
					current_sound.on('complete', function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p10_2");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".sldpng").click(function(){
									$(".sldpng").hide(0);
									$(".sldgf").show(0);
									nav_button_controls(300);
							});
						});
					});
			break;
			case 10:
				sound_player("s1_p"+(countNext+1),1);
				$(".cldThrd").animate({
					opacity:"1"
				},1000,function(){
					$(".cldSec").animate({
						opacity:"1"
					},500,function(){
						$(".cldfst, .hdnPic").animate({
							opacity:"1"
						},500);
					});
				});
			break;
			default:
				sound_player("s1_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}


	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('sideboxes')){
			for(var i=0; i<content[count].sideboxes.length;i++){
				if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
				{
					var imageblock = content[count].sideboxes[i].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var j=0; j<imageClass.length; j++){
							var image_src = preload.getResult(imageClass[j].imgid).src;
							//get list of classes
							var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
								// alert(i);
						}
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

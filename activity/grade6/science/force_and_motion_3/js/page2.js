var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 1
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.s2_p1
		}],
		sideboxes:[{
			sideboxclass:"leftBox yellowbg",
			sideboxtxt:[{
				textclass: "s2SwingTxt",
				textdata: data.string.s2_p1_1
			},{
				textclass: "backFrth questrial-fntsz3 backTxt",
				textdata: data.string.back
			},{
				textclass: "backFrth questrial-fntsz3 frthTxt",
				textdata: data.string.forth
			}],
			imageblock:[{
				imagestoshow:[{
		      imgclass: "background",
		      imgid: 'bglhs',
					imgsrc: ""
				},{
		      imgclass: "grlSwingLeft swngGf",
		      imgid: 'girls_swinging',
					imgsrc: ""
				},{
		      imgclass: "grlSwingLeft swg",
		      imgid: 'swingPng',
					imgsrc: ""
				},{
		      imgclass: "arrows arwBack",
		      imgid: 'small_arrow02',
					imgsrc: ""
				},{
		      imgclass: "arrows arwfrth",
		      imgid: 'small_arrow02',
					imgsrc: ""
				},]
			}]
		},
		{
			sideboxclass:"rightbox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"obotxt",
				textclass: "s2Sidetxt",
				textdata: data.string.s2_p1_2
			}],
			// imageblock:[{
			// 	imagestoshow:[{
		  //     imgclass: "ballRight bl1",
		  //     imgid: 'ball',
			// 		imgsrc: ""
			// 	},{
		  //     imgclass: "ballRight bl2",
		  //     imgid: 'ball',
			// 		imgsrc: ""
			// 	}],
			// 	imagelabels:[{
			// 		imagelabelclass: "figName fntop",
			// 		imagelabeldata: data.string.p1s12_4
			// 	},{
			// 		imagelabelclass: "figName fnbtm",
			// 		imagelabeldata: data.string.p1s12_5
			// 	}]
			// }]
		}]
	},
	//slide 2
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.s2_p1
		}],
		sideboxes:[{
			sideboxclass:"leftBox yellowbg",
			sideboxtxt:[{
				textclass: "s2SwingTxt",
				textdata: data.string.s2_p2
			},{
				textclass: "updown questrial-fntsz3 uptxt",
				textdata: data.string.up
			},{
				textclass: "updown questrial-fntsz3 dwntxt",
				textdata: data.string.down
			}],
			imageblock:[{
				imagestoshow:[{
		      imgclass: "background",
		      imgid: 'bglhs',
					imgsrc: ""
				},{
		      imgclass: "grlSwingLeft width55 swngGf",
		      imgid: 'girl_skipping',
					imgsrc: ""
				},{
		      imgclass: "grlSwingLeft width55 swg",
		      imgid: 'girl_skipping_png',
					imgsrc: ""
				},{
		      imgclass: "arrows arwUp",
		      imgid: 'small_arrow02',
					imgsrc: ""
				},{
		      imgclass: "arrows arwDwn",
		      imgid: 'small_arrow02',
					imgsrc: ""
				},]
			}]
		},
		{
			sideboxclass:"rightbox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"obotxt",
				textclass: "s2Sidetxt",
				textdata: data.string.s2_p2_1
			}]
		}]
	},
	//slide 3
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.s2_p3
		}],
		sideboxes:[{
			sideboxclass:"leftBox yellowbg",
			imageblock:[{
				imagestoshow:[{
		      imgclass: "background",
		      imgid: 'bglhs',
					imgsrc: ""
				},{
		      imgclass: "ballKick",
		      imgid: 'football',
					imgsrc: ""
				}]
			}]
		},
		{
			sideboxclass:"rightbox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"obotxt",
				textclass: "s2Sidetxt",
				textdata: data.string.s2_p3_1
			}]
		}]
	},
	//slide 4
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.s2_p3
		}],
		sideboxes:[{
			sideboxclass:"leftBox yellowbg",
			imageblock:[{
				imagestoshow:[{
		      imgclass: "background",
		      imgid: 'bglhs',
					imgsrc: ""
				},{
		      imgclass: "ballKick",
		      imgid: 'sliding',
					imgsrc: ""
				}]
			}]
		},
		{
			sideboxclass:"rightbox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"obotxt",
				textclass: "s2Sidetxt",
				textdata: data.string.s2_p4
			}]
		}]
	},
	//slide 5----bus catapult
	{
		contentblockadditionalclass:"greeenbg",
		imageload:true,
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"purpleTxt",
			textclass: "toptxt",
			textdata: data.string.s2_p5
		},{
			datahighlightflag:'true',
			datahighlightcustomclass:"purpleTxt",
			textclass: "btmtxt creambg",
			textdata: data.string.s2_p5_1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "guleli",
				imgid: 'guleli',
				imgsrc: ""
			},{
				imgclass: "busBg",
				imgid: 'bg_for_bus',
				imgsrc: ""
			},{
				imgclass: "school_bus",
				imgid: 'school_bus',
				imgsrc: ""
			}]
		}]
	},
	//slide 6
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.s2_p6
		}],
		sideboxes:[{
			sideboxclass:"leftBox yellowbg",
			imageblock:[{
				imagestoshow:[{
		      imgclass: "background cycleBg",
		      imgid: 'bg02',
					imgsrc: ""
				},{
		      imgclass: "girlIncycle",
		      imgid: 'girl-in-cycle',
					imgsrc: ""
				}]
			}]
		},
		{
			sideboxclass:"rightbox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"purpleTxt",
				textclass: "s2Sidetxt",
				textdata: data.string.s2_p6_1
			}]
		}]
	},
	//slide 7-----ferris wheel animation left
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.s2_p6
		}],
		sideboxes:[{
			sideboxclass:"leftBox yellowbg",
			imageblock:[{
				imagestoshow:[{
		      imgclass: "background",
		      imgid: 'bglhs',
					imgsrc: ""
				},{
		      imgclass: "wheel",
		      imgid: 'rotation_wheel',
					imgsrc: ""
				}]
			}]
		},
		{
			sideboxclass:"rightbox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"purpleTxt",
				textclass: "s2Sidetxt",
				textdata: data.string.s2_p7
			}]
		}]
	},
	//slide 8
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.s2_p8
		}],
		sideboxes:[{
			sideboxclass:"leftBox orangeBox",
			imageblock:[{
				imagestoshow:[
				{
		      imgclass: "fly",
		      imgid: 'fly',
					imgsrc: ""
				}]
			}]
		},
		{
			sideboxclass:"rightbox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"purpleTxt",
				textclass: "s2Sidetxt",
				textdata: data.string.s2_p8_1
			}]
		}]
	},
	//slide 9
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.s2_p8
		}],
		sideboxes:[{
			sideboxclass:"leftBox orangeBox",
			imageblock:[{
				imagestoshow:[
				{
		      imgclass: "background",
		      imgid: 'bg01',
					imgsrc: ""
				},{
		      imgclass: "kiteFly",
		      imgid: 'flying_kite_yellow',
					imgsrc: ""
				}]
			}]
		},
		{
			sideboxclass:"rightbox creambg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"purpleTxt",
				textclass: "s2Sidetxt",
				textdata: data.string.s2_p9
			}]
		}]
	},

	//slide 5
	{
		contentblockadditionalclass:"greeenbg",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spImg",
			imgid:"text_box",
			datahighlightflag:'true',
			datahighlightcustomclass:"purpleTxt",
			textclass: "sptxt",
			textdata: data.string.s2_p10
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "dilakLastPage",
				imgid: 'dilak_01',
				imgsrc: ""
			}]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "mainBg", src: imgpath+"main_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-in-cycle", src: imgpath+"girl-in-cycle.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_skipping", src: imgpath+"girl_skipping.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_skipping_png", src: imgpath+"girl_skipping.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girls_swinging", src: imgpath+"girls_swinging.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "slide", src: imgpath+"slide.png", type: createjs.AbstractLoader.IMAGE},
			{id: "swingPng", src: imgpath+"girls-swinging.png", type: createjs.AbstractLoader.IMAGE},
			{id: "guleli", src: imgpath+"guleli.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_bus", src: imgpath+"bg_for_bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school_bus", src: imgpath+"school_bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fly", src: imgpath+"fly.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sliding", src: imgpath+"sliding.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "football", src: imgpath+"football.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "football_png", src: imgpath+"football.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sliding_png", src: imgpath+"sliding.png", type: createjs.AbstractLoader.IMAGE},
			{id: "small_arrow02", src: imgpath+"small_arrow02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bglhs", src: imgpath+"bglhs.png", type: createjs.AbstractLoader.IMAGE},
			{id: "football", src: imgpath+"football.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flying_kite_yellow", src: imgpath+"flying_kite_yellow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_01", src: imgpath+"dilak_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rotation_wheel", src: imgpath+"rotation_wheel01.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1_1", src: soundAsset+"s2_p1_1.ogg"},
			{id: "s2_p1_2", src: soundAsset+"s2_p1_2.ogg"},
			{id: "s2_p1_3", src: soundAsset+"s2_p1_3.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3_1", src: soundAsset+"s2_p3_1.ogg"},
			{id: "s2_p3_2", src: soundAsset+"s2_p3_2.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p6_1", src: soundAsset+"s2_p6_1.ogg"},
			{id: "s2_p6_2", src: soundAsset+"s2_p6_2.ogg"},
			{id: "s2_p7_1", src: soundAsset+"s2_p7_1.ogg"},
			{id: "s2_p7_2", src: soundAsset+"s2_p7_2.ogg"},
			{id: "s2_p8_1", src: soundAsset+"s2_p8_1.ogg"},
			{id: "s2_p8_2", src: soundAsset+"s2_p8_2.ogg"},
			{id: "s2_p9", src: soundAsset+"s2_p9.ogg"},
			{id: "s2_p10", src: soundAsset+"s2_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});girlCycling
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):'';
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				$(".swngGf, .s2SwingTxt").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p1_1");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p1_3");
					current_sound.play();
					current_sound.on('complete', function(){
						$(".s2SwingTxt").fadeIn(300);
						sound_player("s2_p1_2", 0);
						$(".grlSwingLeft").click(function(){
							$(".grlSwingLeft").hide(0);
							$(".swngGf").show(0);
							nav_button_controls(300);
						});
					});
				});
			break;
			case 1:
				$(".swngGf, .s2SwingTxt").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p2");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".s2SwingTxt").fadeIn(300);
						sound_player("s2_p1_2", 0);
						$(".grlSwingLeft").click(function(){
							$(".grlSwingLeft").hide(0);
							$(".swngGf").show(0);
							nav_button_controls(300);
						});
				});
				break;
			case 2:
				$(".swngGf").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p3_1");
				current_sound.play();
				current_sound.on('complete', function(){
						sound_player("s2_p3_2", 1);
						});
			break;
			// break;
			case 5:
			sound_player_duo("s2_p6_1","s2_p6_2",1);
				$(".purpleTxt:eq(1)").css({
					"font-size":"3vh",
					"color":"#000"
				});
			break;
			case 6:
				sound_player_duo("s2_p6_1","s2_p7_2",1);
			break;
			case 7:
				sound_player_duo("s2_p8_1","s2_p8_2",1);
			break;
			default:
				sound_player("s2_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}
	function sound_player_duo(sound_id1,sound_id2,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id1);
		current_sound.play();
		current_sound.on('complete', function(){
			sound_player(sound_id2, next);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}


	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('sideboxes')){
			for(var i=0; i<content[count].sideboxes.length;i++){
				if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
				{
					var imageblock = content[count].sideboxes[i].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var j=0; j<imageClass.length; j++){
							var image_src = preload.getResult(imageClass[j].imgid).src;
							//get list of classes
							var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
								// alert(i);
						}
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

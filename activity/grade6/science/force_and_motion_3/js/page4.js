var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"divtext text1",
				textclass: "textsInDiv colBlack",
				textdata: data.string.p3s0_1
			},{
				textindiv:true,
				textdivclass:"divtext text2",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_2
			},{
				textindiv:true,
				textdivclass:"divtext text3",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_3
			},{
				textindiv:true,
				textdivclass:"divtext text4",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_4
			},{
				textindiv:true,
				textdivclass:"divtext text5",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_5
			}
		]
	},
	// slide1
	{
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"divtext text1",
				textclass: "textsInDiv colBlack",
				textdata: data.string.p3s0_1
			},{
				textindiv:true,
				textdivclass:"divtext text2",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_2
			},{
				textindiv:true,
				textdivclass:"divtext text3",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_3
			},{
				textindiv:true,
				textdivclass:"divtext text4",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_4
			},{
				textindiv:true,
				textdivclass:"divtext text5",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_5
			}
		]
	},
	// slide2
	{
		extratextblock:[
			{
				textclass: "topBtmtxt toptxt",
				textdata: data.string.p3s1_1
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"redtxt",
				textclass: "topBtmtxt btmTxt",
				textdata: data.string.p3s1_2
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'park',
					imgsrc: ""
				}
			]
		}]
	},
	// slide3
	{
		extratextblock:[
			{
				textclass: "topBtmtxt toptxt",
				textdata: data.string.p3s2_1
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"redtxt",
				textclass: "topBtmtxt btmTxt",
				textdata: data.string.p3s1_2
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgDiv:true,
					imgdivclass:"imgcontainer topCont",
					imgclass: "contBall ball1",
					imgid : 'ball',
					imgsrc: ""
				},{
					imgDiv:true,
					imgdivclass:"imgcontainer midCont",
					imgclass: "contBall ball2",
					imgid : 'ball',
					imgsrc: ""
				},{
					imgDiv:true,
					imgdivclass:"imgcontainer btmCont",
					imgclass: "contBall ball3",
					imgid : 'ball',
					imgsrc: ""
				},{
					imgclass: "prevSLideBall ball4",
					imgid : 'ball',
					imgsrc: ""
				},{
					imgclass: "kick_ball",
					imgid : 'kick_ball',
					imgsrc: ""
				},{
					imgclass: "kick_ball kgif",
					imgid : 'kick_ball_gif',
					imgsrc: ""
				},{
					imgclass: "hitBall ",
					imgid : 'hit_ball',
					imgsrc: ""
				},{
					imgclass: "hitBall hgif",
					imgid : 'hit_ball_gif',
					imgsrc: ""
				},{
					imgclass: "handIcon-1",
					imgid : 'hand_icon',
					imgsrc: ""
				},{
					imgclass: "handIcon-2",
					imgid : 'hand_icon',
					imgsrc: ""
				},{
					imgclass: "handIcon-3",
					imgid : 'hand_icon',
					imgsrc: ""
				}
			]
		}]
	},
	// slide4
	{
		extratextblock:[
			{
				textclass: "topBtmtxt toptxt",
				textdata: data.string.p3s3_1
			}
		],
		imageblock:[{
			imagestoshow:[{
					imgclass: "background",
					imgid : 'bg_slide',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bg_slide',
					imgsrc: ""
				},{
					imgclass: "girls_swinging",
					imgid : 'girls-swingingPng',
					imgsrc: ""
				}
			]
		}]
	},
	// slide5
	{
		extratextblock:[
			{
				textclass: "topBtmtxt toptxt",
				textdata: data.string.p3s4_1
			}
		],
		imageblock:[{
			imagestoshow:[{
					imgclass: "background",
					imgid : 'bg_slide',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bg_slide',
					imgsrc: ""
				},{
					imgclass: "girls_swinging",
					imgid : 'girls_swinging',
					imgsrc: ""
				}
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass:"grenBg",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spImg",
			imgid:"text_box",
			textclass: "sptxt",
			textdata: data.string.p3s5_1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "dilakLastPage",
				imgid: 'dilak_01',
				imgsrc: ""
			}]
		}]
	},
	// slide7
	{
		contentblockadditionalclass:"grenBg",
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"textBox-white",
				datahighlightflag:true,
				datahighlightcustomclass:"serialTxt",
				textclass: "boxtxt",
				textdata: data.string.p4s1_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "dilak_pg4",
					imgid : 'dilak03',
					imgsrc: ""
				}
			]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "dilak02", src: imgpath+"dilak02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_slide", src: imgpath+"bg_slide.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girls_swinging", src: imgpath+"girls_swinging.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girls-swingingPng", src: imgpath+"girls-swinging.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_01", src: imgpath+"dilak_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kick_ball_gif", src: imgpath+"kick_ball.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "kick_ball", src: imgpath+"kick_ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hit_ball_gif", src: imgpath+"hit_ball.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hit_ball", src: imgpath+"hit_ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "park", src: imgpath+"park.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand_icon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak03", src: imgpath+"dilak03.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
			{id: "s4_p3_1", src: soundAsset+"s4_p3_1.ogg"},
			{id: "s4_p3_2", src: soundAsset+"s4_p3_2.ogg"},
			{id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p6", src: soundAsset+"s4_p6.ogg"},
			{id: "s4_p7", src: soundAsset+"s4_p7.ogg"},
			{id: "s4_p8", src: soundAsset+"s4_p8.ogg"},
			{id: "correct", src: "sounds/common/grade1/correct.ogg"},
			{id: "incorrect", src: "sounds/common/grade1/incorrect.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var kickCount = 0;
		switch(countNext){
			case 1:
			$(".text1,.text3,.text4,.text5").addClass("hideAnim");
			$(".text2").addClass("showAnim");
			setTimeout(function(){
					sound_player("s4_p2",1);
			},3000);
			break;
			case 3:
					sound_player("s4_p4",0);
				$(".kgif, .hgif").hide(0);
				$(".kick_ball, .handIcon-1").click(function(){
					createjs.Sound.stop();
					$(".ball1").addClass("ballMoveAnim");
						$(".kick_ball").hide(0);
						$(".kgif").show(0);
						$(this).css("pointer-events","none");
						$(".handIcon-1").hide(0);
						kickCount+=1;
						kickCount==3?nav_button_controls(2500):'';
				});

				$(".hitBall, .handIcon-2").click(function(){
					createjs.Sound.stop();
					$(".ball2").addClass("hitBallAnim");
						$(".hitBall").hide(0);
						$(".hgif").show(0);
						$(this).css("pointer-events","none");
						$(".handIcon-2").hide(0);
						kickCount+=1;
						kickCount==3?nav_button_controls(2500):'';
				});

				$(".handIcon-3, .ball4").click(function(){
					createjs.Sound.stop();
					$(".ball4").addClass("ballSlideSec");
					$(".ball3").addClass("ballThirdAnim");
					$(this).css("pointer-events","none");
					$(".handIcon-3").hide(0);
					kickCount+=1;
					kickCount==3?nav_button_controls(2500):'';
				});
			break;
			case 2:
				sound_player_duo("s4_p3_1","s4_p3_2", 1);
			break;
			case 7:
				sound_player("s4_p"+(countNext+1), 1);
				nav_button_controls(22000);
			break;
			default:
				sound_player("s4_p"+(countNext+1), 1);
			break;

		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}
	function sound_player_duo(sound_id_1,sound_id_2, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id_1);
		current_sound.play();
		current_sound.on('complete', function(){
			sound_player(sound_id_2, next);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$refreshBtn.click(function(){
		templatecaller();
	});

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 1
	{
		contentblockadditionalclass:"greeenbg",
		extratextblock:[{
			textclass: "cvPageTxt",
			textdata: data.string.p7s1_1
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "dilak_s1",
	      imgid: 'dilak_01',
				imgsrc: ""
			}]
		}],
	},
	//slide 2
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt orangeBg",
			textdata: data.string.p7s2_1
		}],
		sideboxes:[{
			sideboxclass:"leftBox lightGrenBg",
			sideboxtxt:[{
				datahighlightflag:true,
				datahighlightcustomclass:"underline",
				textclass: "midTXt",
				textdata: data.string.p7s2_2
			}]
		},
		{
			sideboxclass:"rightbox whitebg",
			sideboxtxt:[{
				textclass: "gravityTXt shwLtr",
				textdata: data.string.gravity
			}],
			imageblock:[{
				imagestoshow:[{
		      imgclass: "background",
		      imgid: 'mainBg',
					imgsrc: ""
				},{
		      imgclass: "melisha",
		      imgid: 'melisha',
					imgsrc: ""
				},{
		      imgclass: "arrow shwLtr",
		      imgid: 'arrow',
					imgsrc: ""
				},{
		      imgclass: "melB",
		      imgid: 'ball',
					imgsrc: ""
				}]
			}]
		}]
	},
	//slide 3
	{
		extratextblock:[{
			textclass: "toptxt orangeBg",
			textdata: data.string.p7s3_1
		},{
			textclass: "frimoTxt motion",
			textdata: data.string.motion
		},{
			textclass: "frimoTxt friction",
			textdata: data.string.friction
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'mainBg',
				imgsrc: ""
			},{
	      imgclass: "dilakKick",
	      imgid: 'football',
				imgsrc: ""
			},{
	      imgclass: "ar_s3 armtn",
	      imgid: 'arrow',
				imgsrc: ""
			},{
	      imgclass: "ar_s3 arfrn",
	      imgid: 'arrow',
				imgsrc: ""
			}
		]
		}]
	},
	//slide 4
	{
		contentblockadditionalclass:"lightGrenBg",
		imageload:true,
		extratextblock:[{
			textclass: "toptxt orangeBg",
			textdata: data.string.p7s4_1
		}],
		sideboxes:[{
			sideboxclass:"leftBoxSec",
			sideboxtxt:[{
				textclass: "BLwDvTxt",
				textdata: data.string.magnetism
			}],
			imageblock:[{
				imagestoshow:[{
		      imgclass: "boxImg bim",
		      imgid: 'magnetism',
					imgsrc: ""
				}]
			}]
		},
		{
			sideboxclass:"rightboxSec",
			sideboxtxt:[{
				textclass: "BLwDvTxt",
				textdata: data.string.stretching
			}],
			imageblock:[{
				imagestoshow:[{
		      imgclass: "boxImg bim1",
		      imgid: 'stretching',
					imgsrc: ""
				}]
			}]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "mainBg", src: imgpath+"main_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-in-cycle", src: imgpath+"girl-in-cycle.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "melisha", src: imgpath+"melisha_kick_ball_new.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_01", src: imgpath+"dilak_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_bus", src: imgpath+"bg_for_bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "magnetism", src: imgpath+"magnetism.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stretching", src: imgpath+"stretching.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"small_arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_walk01", src: imgpath+"boy_walk01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "football", src: imgpath+"football.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});girlCycling
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):'';
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				setTimeout(function(){
					$(".sp-1").animate({
						opacity:"1"
					},500);
				},1600);
				navigationcontroller();
			break;
			case 3:
			$(".leftBoxSec, .rightboxSec").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_0");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".leftBoxSec").fadeIn(500);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_0");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".rightboxSec").fadeIn(500);
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("sound_0");
								current_sound.play();
								nav_button_controls(1000);
						});
				});
			break;
			default:
				navigationcontroller();
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}


	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('sideboxes')){
			for(var i=0; i<content[count].sideboxes.length;i++){
				if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
				{
					var imageblock = content[count].sideboxes[i].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var j=0; j<imageClass.length; j++){
							var image_src = preload.getResult(imageClass[j].imgid).src;
							//get list of classes
							var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
								// alert(i);
						}
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

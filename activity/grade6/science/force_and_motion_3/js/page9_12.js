var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide 1
	{
		extratextblock:[{
			textclass:"tpBtm toptxt",
			textdata:data.string.p8s1_1
		},{
			textclass:"tpBtm btmtxt",
			textdata:data.string.p8s1_2
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"bigCity leftMoveAnim",
				imgid:"city_big",
				imgsrc:''
			},{
				imgclass:"dilak fstSld",
				imgid:"dilak_03",
				imgsrc:''
			}]
		}]
	},
	// slide 2
	{
			extratextblock:[{
				textclass:"tpBtm toptxt",
				// textdata:data.string.p8s2_1
			},{
				textclass:"tpBtm btmtxt ht20",
			}],
			exetype1:[{
				optionsdivclass:"optionsdiv",
				textdata:data.string.p8s2_1,
				exeoptions:[{
					optdata:data.string.same
				},{
					optaddclass:"class1",
					optdata:data.string.opposite
				}]
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass:"bigCity leftMoveAnim",
					imgid:"city_big",
					imgsrc:''
				},{
					imgclass:"dilak fstSld",
					imgid:"dilak_03",
					imgsrc:''
				}]
			}]
		},
	// slide 3
	{
		extratextblock:[{
			textclass:"tpBtm toptxt",
			textdata:data.string.p8s2_1
		},{
			textclass:"tpBtm btmtxt",
			textdata:data.string.p8s3_1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"bigCity leftMoveAnim",
				imgid:"city_big",
				imgsrc:''
			},{
				imgclass:"dilak fstSld",
				imgid:"dilak_03",
				imgsrc:''
			},{
				imgclass:"arrow arwFst_s3",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"arrow arwSec_s3",
				imgid:"white_arrow",
				imgsrc:''
			}],
			imagelabels:[{
				imagelabelclass:"arwtxt pusTxt",
				imagelabeldata:data.string.pforce
			},{
				imagelabelclass:"arwtxt fricTxt",
				imagelabeldata:data.string.fric
			}]
		}]
	},
	// slide 4
	{
		extratextblock:[{
			textclass:"tpBtm toptxt",
			textdata:data.string.p8s4_1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"bigCity leftMoveAnim",
				imgid:"city_big",
				imgsrc:''
			},{
				imgclass:"dilak fstSld",
				imgid:"dilak_03",
				imgsrc:''
			},{
				imgclass:"arrow arwFst_s4",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"arrow arwSec_s4",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"arrow arwThird_s4",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"wind-s4",
				imgid:"weather-wind",
				imgsrc:''
			}],
			imagelabels:[{
				imagelabelclass:"arwtxt pusTxt-4",
				imagelabeldata:data.string.pforce
			},{
				imagelabelclass:"arwtxt fricTxt-4",
				imagelabeldata:data.string.fric
			},{
				imagelabelclass:"arwtxt windTxt-4",
				imagelabeldata:data.string.windir
			}]
		}]
	},
	// slide 5
	{
		extratextblock:[{
			textclass:"tpBtm toptxt",
			textdata:data.string.p8s5_1
		},{
			textclass:"tpBtm btmtxt",
			textdata:data.string.p8s5_2
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"bigCity leftMoveAnim",
				imgid:"city_big",
				imgsrc:''
			},{
				imgclass:"dilak fstSld",
				imgid:"dilak_03",
				imgsrc:''
			},{
				imgclass:"arrow arwFst_s4",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"arrow arwSec_s4",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"arrow arwThird_s4",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"wind-s4",
				imgid:"weather-wind",
				imgsrc:''
			}],
			imagelabels:[{
				imagelabelclass:"arwtxt pusTxt-4",
				imagelabeldata:data.string.pforce
			},{
				imagelabelclass:"arwtxt fricTxt-4",
				imagelabeldata:data.string.fric
			},{
				imagelabelclass:"arwtxt windTxt-4",
				imagelabeldata:data.string.windir
			}]
		}]
	},
	// slide 6
	{
		extratextblock:[{
			textclass:"tpBtm toptxt",
			textdata:data.string.p8s6_1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"hill01",
				imgsrc:''
			},{
				imgclass:"dilak l0 fstSld",
				imgid:"dilak_03",
				imgsrc:''
			}]
		}]
	},
	// slide 7
	{
		extratextblock:[{
			textclass:"tpBtm toptxt",
			textdata:data.string.p8s7_1
		},{
			textclass:"tpBtm btmtxt",
			textdata:data.string.p8s7_2
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"hill01",
				imgsrc:''
			},{
				imgclass:"dilak l0 fstSld",
				imgid:"dilak_03",
				imgsrc:''
			},{
				imgclass:"arrowRght arSld_fric",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"arrow arSld_pusFrc",
				imgid:"white_arrow",
				imgsrc:''
			},{
				imgclass:"arrowRght arSld_gvt",
				imgid:"white_arrow",
				imgsrc:''
			}],
			imagelabels:[{
				imagelabelclass:"arwtxt pusTxt psh_angular-drkCol",
				imagelabeldata:data.string.pforce
			},{
				imagelabelclass:"arwtxt fricTxt-7-drkCol",
				imagelabeldata:data.string.fric
			},{
				imagelabelclass:"arwtxt gvtTxt-drkCol",
				imagelabeldata:data.string.gravity
			}]
		}]
	},
	// slide 8
	{
		contentblockadditionalclass:"greeenbg",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spImg",
			imgid:"text_box",
			textclass: "sptxt",
			textdata: data.string.p8s8_1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "dilakLastPage",
				imgid: 'dilak_01',
				imgsrc: ""
			}]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "city", src: imgpath+"city.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_03", src: imgpath+"boy_walk.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hill01", src: imgpath+"hill01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_01", src: imgpath+"dilak_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "white_arrow", src: imgpath+"white_arrow01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "small_arrow01", src: imgpath+"small_arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "small_arrow01", src: imgpath+"white_arrow02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weather-wind", src: imgpath+"weather-wind.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_walk01", src: imgpath+"boy_walk01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "city_big", src: imgpath+"city_big.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_0_end", src: soundAsset+"p2_s0_end.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_1_end", src: soundAsset+"p2_s1_end.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "fs-0-1", src: soundAsset+"p2_s0_1.ogg"},
			{id: "fs-0-2", src: soundAsset+"p2_s0_2.ogg"},
			{id: "fs-0-3", src: soundAsset+"p2_s0_3.ogg"},
			{id: "fs-1-1", src: soundAsset+"p2_s1_1.ogg"},
			{id: "fs-1-2", src: soundAsset+"p2_s1_2.ogg"},
			{id: "fs-1-3", src: soundAsset+"p2_s1_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		// randomize
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		switch(countNext){
			// case 0:
			// case 2:
			// case 3:
			// case 4:
			// 	$(".dilak").addClass("dilWalk");
			// 	navigationcontroller();
			// break;
			// case 1:
			// 	$(".dilak").addClass("dilWalk");
			// break;
			case 1:
				$prevBtn.hide(0);
				$nextBtn.hide(0);
			break;
			case 5:
			case 6:
			case 7:
				$(".dilak").addClass("dilWalkSec");
					navigationcontroller();
			break;
			default:
				navigationcontroller();

		}

	//handle mcq
	$(".buttonsel").click(function(){
		$(this).removeClass('forhover');
				/*class 1 is always for the right answer. updates scoreboard and disables other click if
				right answer is clicked*/
				if($(this).hasClass("class1")){
					play_correct_incorrect_sound(1);
					$(this).css({
						"background": "#98c02e",
						"border":"5px solid #ff0",
						"color":"#fff",
						'pointer-events': 'none'
					});
					$(this).siblings(".corctopt").show(0);
					$(".buttonsel").css("pointer-events","none")
					nav_button_controls(0);
				}
				else{
					play_correct_incorrect_sound(0);
					$(this).css({
						"background":"#FF0000",
						"border":"5px solid #980000",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(this).siblings(".wrngopt").show(0);
				}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide 0
	{
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"divtext text1",
				textclass: "textsInDiv colBlack",
				textdata: data.string.p3s0_1
			},{
				textindiv:true,
				textdivclass:"divtext text2",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_2
			},{
				textindiv:true,
				textdivclass:"divtext text3",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_3
			},{
				textindiv:true,
				textdivclass:"divtext text4",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_4
			},{
				textindiv:true,
				textdivclass:"divtext text5",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_5
			}
		]
	},
	// slide 1
	{
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"divtext text1",
				textclass: "textsInDiv colBlack",
				textdata: data.string.p3s0_1
			},{
				textindiv:true,
				textdivclass:"divtext text2",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_2
			},{
				textindiv:true,
				textdivclass:"divtext text3",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_3
			},{
				textindiv:true,
				textdivclass:"divtext text4",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_4
			},{
				textindiv:true,
				textdivclass:"divtext text5",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_5
			}
		]
	},
	// slide 2
	{
		extratextblock:[{
			textclass:"tpBtm toptxt",
			textdata:data.string.p10s5_1
		},{
			textclass:"tpBtm btmtxt",
			textdata:data.string.p10s5_2
		}],
		imageblock:[{
			imgindiv:true,
			imgcontainerclass:"ImgContDiv",
			imagestoshow:[{
				imgclass:"background",
				imgid:"bg_school",
				imgsrc:''
			},{
				imgclass:"filling_tube",
				imgid:"filling_tube",
				imgsrc:''
			}]
		}]
	},
	// slide 3
	{
		extratextblock:[{
			textclass:"tpBtm toptxt",
			textdata:data.string.p10s5_1
		},{
			textclass:"tpBtm btmtxt",
			textdata:data.string.p10s5_3
		}],
		imageblock:[{
			imgindiv:true,
			imgcontainerclass:"ImgContSec zoomedPump",
			imagestoshow:[{
				imgclass:"background",
				imgid:"bg_school",
				imgsrc:''
			},{
				imgclass:"filling_tube",
				imgid:"filling_tube",
				imgsrc:''
			}]
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "city", src: imgpath+"city.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_school", src: imgpath+"bg_school.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_01", src: imgpath+"dilak_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlInCycle_gif", src: imgpath+"girl-in-cycle.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "filling_tube", src: imgpath+"filling_tube.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "city_big", src: imgpath+"city_big.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-in-cycle01", src: imgpath+"girl-in-cycle01.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s7_p2", src: soundAsset+"s7_p2.ogg"},
			{id: "s7_p3_1", src: soundAsset+"s7_p3_1.ogg"},
			{id: "s7_p3_2", src: soundAsset+"s7_p3_2.ogg"},
			{id: "s7_p4", src: soundAsset+"s7_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		// randomize
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		switch(countNext){
			case 0:
				sound_player("s4_p1",1);
			break;
			case 1:
				$(".text1,.text2,.text3,.text4").addClass("hideAnim");
				$(".text5").addClass("showAnim");
					setTimeout(function(){
						sound_player("s7_p"+(countNext+1),1);
					},3000);
			break;
			case 2:
				$(".btmtxt").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s7_p3_1");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".btmtxt").fadeIn(300);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s7_p3_2");
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls();
					});
				});
			break;
			case 3:
				sound_player("s7_p4",1);
			break;
			default:
				// sound_player("s7_p4"+(countNext+1),1);
			break;

		}

	//handle mcq
	$(".buttonsel").click(function(){
		createjs.Sound.stop();
		$(this).removeClass('forhover');
				/*class 1 is always for the right answer. updates scoreboard and disables other click if
				right answer is clicked*/
				if($(this).hasClass("class1")){
					play_correct_incorrect_sound(1);
					$(this).css({
						"background": "#98c02e",
						"border":"5px solid #ff0",
						"color":"#fff",
						'pointer-events': 'none'
					});
					$(this).siblings(".corctopt").show(0);
					$(".buttonsel").css("pointer-events","none")
					nav_button_controls(0);
				}
				else{
					play_correct_incorrect_sound(0);
					$(this).css({
						"background":"#FF0000",
						"border":"5px solid #980000",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(this).siblings(".wrngopt").show(0);
				}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"divtext text1",
				textclass: "textsInDiv colBlack",
				textdata: data.string.p3s0_1
			},{
				textindiv:true,
				textdivclass:"divtext text2",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_2
			},{
				textindiv:true,
				textdivclass:"divtext text3",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_3
			},{
				textindiv:true,
				textdivclass:"divtext text4",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_4
			},{
				textindiv:true,
				textdivclass:"divtext text5",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_5
			}
		]
	},
	// slide1
	{
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"divtext text1",
				textclass: "textsInDiv colBlack",
				textdata: data.string.p3s0_1
			},{
				textindiv:true,
				textdivclass:"divtext text2",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_2
			},{
				textindiv:true,
				textdivclass:"divtext text3",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_3
			},{
				textindiv:true,
				textdivclass:"divtext text4",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_4
			},{
				textindiv:true,
				textdivclass:"divtext text5",
				textclass: "textsInDiv",
				textdata: data.string.p3s0_5
			}
		]
	},
	//slide 2
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p6s1_1
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'mainBg',
				imgsrc: ""
			},{
	      imgclass: "dilak_s1 ballhold",
	      imgid: 'dilak',
				imgsrc: ""
			},{
	      imgclass: "ball",
	      imgid: 'ball',
				imgsrc: ""
			},{
	      imgclass: "dilak_s1 std",
	      imgid: 'dilak_stand_up',
				imgsrc: ""
			}]
		}],
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spImg",
			imgid:"text_box",
			datahighlightflag:'true',
			datahighlightcustomclass:"purpleTxt",
			textclass: "sptxt",
			textdata: data.string.p6s1_2
		}],
	},
	//slide 3
	{
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p6s2_1
		}],
		imageblock:[{
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'bg_for_bus',
				imgsrc: ""
			},{
	      imgclass: "cycleGirlPng",
	      imgid: 'girl-in-cycle01',
				imgsrc: ""
			},{
	      imgclass: "cycleGirl",
	      imgid: 'girl-in-cycle',
				imgsrc: ""
			}]
		}]
	},
	//slide 4
	{
		extratextblock:[{
			textclass: "toptxt",
			textdata: data.string.p6s3_1
		},{
			textclass: "braketxt",
			textdata: data.string.brake
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"imgDiv cycleZm",
			imagestoshow:[{
	      imgclass: "background",
	      imgid: 'put_break',
				imgsrc: ""
			},{
	      imgclass: "arrow",
	      imgid: 'white_arrow01',
				imgsrc: ""
			},{
	      imgclass: "cycleGirlPng show",
	      imgid: 'girl-in-cycle01',
				imgsrc: ""
			}
		]
		}]
	},
	//slide 5
	{
		imageload:true,
		extratextblock:[{
			textclass: "toptxt h10",
			textdata: data.string.p6s4_1
		}],
		sideboxes:[{
			sideboxclass:"leftBox yellowbg left_neg_150",
			sideboxtxt:[{
				textclass: "btmSecfst btmtxt h15 creambg",
				textdata: data.string.p6s4_2
			}],
			imageblock:[{
				imagestoshow:[{
		      imgclass: "dilakKick hde",
		      imgid: 'football',
					imgsrc: ""
				},{
		      imgclass: "background",
		      imgid: 'bg01',
					imgsrc: ""
				}]
			}]
		},
		{
			sideboxclass:"rightbox whitebg left_150",
			sideboxtxt:[{
				textclass: "btmSecSec btmtxt creambg h15",
				textdata: data.string.p6s4_3
			}],
			imageblock:[{
				imagestoshow:[
				{
		      imgclass: "background bg1",
		      imgid: 'zebra_crossing',
					imgsrc: ""
				},{
		      imgclass: "grlcycpng",
		      imgid: 'girl-in-cycle01',
					imgsrc: ""
				},{
		      imgclass: "grlcycGif",
		      imgid: 'girl-in-cycle',
					imgsrc: ""
				}]
			}]
		}]
	},
	//slide 6
	{
		contentblockadditionalclass:"greeenbg",
		speechbox:[{
			speechbox:"sp-2",
			imgclass:"spImg",
			imgid:"text_box",
			textclass: "sptxt",
			textdata: data.string.p6s5_1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "dilakLastPage",
				imgid: 'dilak_01',
				imgsrc: ""
			}]
		}]
	},
	// slide7
	{
		contentblockadditionalclass:"grenBg",
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"textBox-white",
				datahighlightflag:true,
				datahighlightcustomclass:"pinkTxt",
				textclass: "boxtxt",
				textdata: data.string.p5_s7
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "dilak_pg4",
					imgid : 'dilak03',
					imgsrc: ""
				}
			]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "mainBg", src: imgpath+"main_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-in-cycle", src: imgpath+"girl-in-cycle.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak", src: imgpath+"dilak.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_01", src: imgpath+"dilak_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_bus", src: imgpath+"put_break.png", type: createjs.AbstractLoader.IMAGE},
			{id: "put_break", src: imgpath+"put_break.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak_stand_up", src: imgpath+"dilak_stand_up.png", type: createjs.AbstractLoader.IMAGE},
			{id: "white_arrow01", src: imgpath+"white_arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "football", src: imgpath+"football.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-in-cycle01", src: imgpath+"girl-in-cycle01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zebra_crossing", src: imgpath+"zebra_crossing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak03", src: imgpath+"dilak03.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3_1", src: soundAsset+"s5_p3_1.ogg"},
			{id: "s5_p3_2", src: soundAsset+"s5_p3_2.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p6_1", src: soundAsset+"s5_p6_1.ogg"},
			{id: "s5_p6_2", src: soundAsset+"s5_p6_2.ogg"},
			{id: "s5_p6_3", src: soundAsset+"s5_p6_3.ogg"},
			{id: "s5_p7", src: soundAsset+"s5_p7.ogg"},
			{id: "s5_p8", src: soundAsset+"s5_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});girlCycling
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):'';
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("s4_p1",1);
			break;
			case 1:
				setTimeout(function(){sound_player("s5_p"+(countNext+1),1);},3000);
				$(".text1,.text2,.text4,.text5").addClass("hideAnim");
				$(".text3").addClass("showAnim");
			break;
			case 2:
				$(".ballhold").hide(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s5_p3_1");
					current_sound.play();
					current_sound.on('complete', function(){
						$(".sp-1").animate({
							opacity:"1"
						},1000);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s5_p3_2");
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls();
						});
					});
				setTimeout(function(){
					$(".dilak_s1").hide(0);
					$(".ballhold").show(0);
				},1450);
			break;
			case 3:
				$(".background").css("top","4%");
				$(".cycleGirlPng").delay(2000).show(0);
				$(".cycleGirl").delay(2100).hide(0);
					sound_player("s5_p"+(countNext+1),1);
			break;
			case 4:
				$(".arrow, .braketxt").delay(2600).fadeIn(1000);
					sound_player("s5_p"+(countNext+1),1);
			break;
			case 5:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s5_p6_1");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".left_neg_150").animate({
						left:"0%"
					},4000,function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s5_p6_2");
						current_sound.play();
						$(".hde").show(0);
						current_sound.on('complete', function(){
							$(".left_150").animate({
								left:"50%"
							},4000, function(){
								$(".grlcycGif").addClass("cycleInAnim");
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("s5_p6_3");
									current_sound.play();
								setTimeout(function(){
									$(".grlcycpng").show();
									$(".grlcycGif").delay(100).hide(0);
								},2000);
								current_sound.on('complete', function(){
									nav_button_controls();
								});
							});
						});
					});
				});
			break;
			default:
				sound_player("s5_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}


	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('sideboxes')){
			for(var i=0; i<content[count].sideboxes.length;i++){
				if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
				{
					var imageblock = content[count].sideboxes[i].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var j=0; j<imageClass.length; j++){
							var image_src = preload.getResult(imageClass[j].imgid).src;
							//get list of classes
							var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
								// alert(i);
						}
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

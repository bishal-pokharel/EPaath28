var imgpath = $ref + "/images/images_for_diy/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var imgpath1 = $ref + "/images/images_for_diy/animation/";

var content=[
	// slide1
	{
		contentblockadditionalclass:"grenBg",
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"textBox-white",
				datahighlightflag:true,
				datahighlightcustomclass:"serialTxt",
				textclass: "boxtxt",
				textdata: data.string.p4s2_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "dilak_pg4",
					imgid : 'dilak03',
					imgsrc: ""
				}
			]
		}]
	},
	// slide2
	{
		contentblockadditionalclass:"grenBg",
		extratextblock:[
			{
				textindiv:true,
				textdivclass:"textBox-white box2",
				datahighlightflag:true,
				datahighlightcustomclass:"serialTxt",
				textclass: "boxtxt",
				textdata: data.string.p4s2_2
			},{
				textclass: "toptxt",
				textdata: data.string.p4s3_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "newton_meter",
					imgid : 'newton_meter',
					imgsrc: ""
				}
			]
		}]
	},
	// slide3
	{
		contentblockadditionalclass:"greenBg",
		extratextblock:[{
				textclass: "toptxt",
				textdata: data.string.p4s5_1
			},{
					datahighlightflag:true,
					datahighlightcustomclass:"weight",
					textclass: "infoTxt",
					textdata: data.string.p4s5_2
				},
		],
		// scalediv:[{
		// 	scaleclass:"weightScale",
		// 	actualscaleclass:"scaleReading"
		// }],
		dropDiv:[{
			dropdivClass:"droppable"
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "hoocks",
					imgid : 'hoocks',
					imgsrc: ""
				},{
					imgclass: "draggable book",
					imgid : 'book',
					imgsrc: ""
				},{
					imgclass: "draggable ring",
					imgid : 'ring',
					imgsrc: ""
				},{
					imgclass: "draggable stapler",
					imgid : 'stapler',
					imgsrc: ""
				},{
					imgclass: "draggable bottle",
					imgid : 'waterbottle',
					imgsrc: ""
				}
			]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var dragcount = 0;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "hoocks", src: imgpath1+"hook01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book", src: imgpath1+"book01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book01", src: imgpath1+"book01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ring", src: imgpath+"ring.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stapler", src: imgpath+"stapler.png", type: createjs.AbstractLoader.IMAGE},
			{id: "waterbottle", src: imgpath1+"bottle01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dilak03", src: imgpath+"dilak03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "newton_meter", src: imgpath+"newton_meter.gif", type: createjs.AbstractLoader.IMAGE},



			{id: "ringgif", src: imgpath1+"ring.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bookgif", src: imgpath1+"book.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bottlegif", src: imgpath1+"bottle.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "staplergif", src: imgpath1+"stapler.gif", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s8_p1_1", src: soundAsset+"s8_p1_1.ogg"},
			{id: "s8_p1_2", src: soundAsset+"s8_p1_2.ogg"},
			{id: "s8_p1_3", src: soundAsset+"s8_p1_3.ogg"},
			{id: "s8_p1_4", src: soundAsset+"s8_p1_4.ogg"},
			{id: "s8_p2_1", src: soundAsset+"s8_p2_1.ogg"},
			{id: "s8_p2_2", src: soundAsset+"s8_p2_2.ogg"},
			{id: "s8_p3", src: soundAsset+"s8_p3.ogg"},
			{id: "correct", src: "sounds/common/grade1/correct.ogg"},
			{id: "incorrect", src: "sounds/common/grade1/incorrect.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	var bokDrag = false, ringDrag= false, staplerDrag= false, btlDrag= false;
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				$(".serialTxt").css("opacity","0");
				$(".serialTxt:eq(0)").css("opacity","1");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s8_p1_1");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".serialTxt:eq(1)").css("opacity","1");
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s8_p1_2");
					current_sound.play();
					current_sound.on('complete', function(){
						$(".serialTxt:eq(2)").css("opacity","1");
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s8_p1_3");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".serialTxt:eq(3)").css("opacity","1");
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s8_p1_4");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(300);
							});
						});
					});
				});
			break;
			case 1:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s8_p2_1");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s8_p2_2");
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(300);
					});
				});
			break;
			case 2:
				sound_player("s8_p3",0);
				$(".draggable").draggable({
					revert:true,
					cursor:"move"
				});
				$(".droppable").droppable({
					accept:".draggable",
					hoverClass:"hoverclass",
					drop:function(event,ui){
						dropfunction(event,ui, $this);
					}
				});
			break;
			default:
				nav_button_controls(0);
			break;

		}

	}
	function dropfunction(event,ui, thisClass){
		createjs.Sound.stop();
		$(".draggable").show(0);
		$(".droppable").empty();
		ui.draggable.hide(0);
		$(".infoTxt").hide(0);
		$(".infoTxt").delay(1000).show(0);
		if(ui.draggable.hasClass("ring")){
			$(".hoocks").attr("src",preload.getResult('ringgif').src);
			$(".weight").text("0.5 N");
			$(".scaleReading").animate({
				height:"16%"
			},1000);
			!ringDrag?dragcount+=1:dragcount=dragcount;
			ringDrag = true;
			dragcount==4?nav_button_controls(300):'';
		}
		else if (ui.draggable.hasClass("book")) {
            $(".hoocks").attr("src",preload.getResult('bookgif').src);
            $(".weight").text("2 N");
			$(".scaleReading").animate({
				height:"43.5%"
			},1000);
			!bokDrag?dragcount+=1:dragcount=dragcount;
			bokDrag = true;
			dragcount==4?nav_button_controls(300):'';
		}
		else if (ui.draggable.hasClass("stapler")) {
            $(".hoocks").attr("src",preload.getResult('staplergif').src);

            $(".weight").text("1 N");
			$(".scaleReading").animate({
				height:"25.5%"
			},1000);
			!staplerDrag?dragcount+=1:dragcount=dragcount;
			staplerDrag = true;
			dragcount==4?nav_button_controls(300):'';
		}
		else if (ui.draggable.hasClass("bottle")) {
            $(".hoocks").attr("src",preload.getResult('bottlegif').src);
            $(".weight").text("4 N");
			$(".scaleReading").animate({
				height:"77.5%"
			},1000);
			!btlDrag?dragcount+=1:dragcount=dragcount;
			btlDrag = true;
			dragcount==4?nav_button_controls(300):'';
		}

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$refreshBtn.click(function(){
		templatecaller();
	});

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

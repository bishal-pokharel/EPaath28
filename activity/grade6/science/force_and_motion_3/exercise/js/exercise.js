var imgpath = $ref+"/images/images_for_Exercise/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide 1
	{
		contentblockadditionalclass : "greenbg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn1
		}],
		imgexc:[{
			imgexcontainerclass:"firstContainer",
			imgexcoptions:[{
				imgoptionscontainerclass:"opnContainer class1 op1",
				imgclass:"boximg bim1",
				imgid:"q1op1",
				imgsrc:'',
				optaddclass:"boxtxt bt1",
				optdata:data.string.ex1_op1
			},{
				imgoptionscontainerclass:"opnContainer op2",
				imgclass:"boximg bim2",
				imgid:"q1op2",
				imgsrc:'',
				optaddclass:"boxtxt bt2",
				optdata:data.string.ex1_op2
			},{
				imgoptionscontainerclass:"opnContainer op3",
				imgclass:"boximg bim3",
				imgid:"q1op3",
				imgsrc:'',
				optaddclass:"boxtxt bt3",
				optdata:data.string.ex1_op3
			}]
		}]
	},
	// slide 2
	{
		contentblockadditionalclass : "greenbg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn2
		}],
		imgexc:[{
			imgexcontainerclass:"firstContainer",
			imgexcoptions:[{
				imgoptionscontainerclass:"opnContainer class1 op1",
				imgclass:"boximg bim1",
				imgid:"q2op1",
				imgsrc:'',
				optaddclass:"boxtxt bt1",
				optdata:data.string.ex2_op1
			},{
				imgoptionscontainerclass:"opnContainer op2",
				imgclass:"boximg bim2",
				imgid:"q2op2",
				imgsrc:'',
				optaddclass:"boxtxt bt2",
				optdata:data.string.ex2_op2
			},{
				imgoptionscontainerclass:"opnContainer op3",
				imgclass:"boximg bim3",
				imgid:"q2op3",
				imgsrc:'',
				optaddclass:"boxtxt bt3",
				optdata:data.string.ex2_op3
			}]
		}]
	},
	// slide3
	{
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn3
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.ex3_op1
			},{
				optaddclass:"class1",
				optdata:data.string.ex3_op2
			},{
				optdata:data.string.ex3_op3
			},{
				optdata:data.string.ex3_op4
			}]
		}]
	},
	// slide4
	{
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn4
		}],
		exetype1:[{
			optionsdivclass:"optionsdivSec",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.ex4_op1
			},{
				optdata:data.string.ex4_op2
			},{
				optdata:data.string.ex4_op3
			}]
		}],
		imageblock:[{
			singleimagecontainerclass:"leftImgCont",
			imagestoshow:[{
				imgclass:"leftImg bgstretch",
				imgid:"bg02",
				imgsrc:'',
			},{
				imgclass:"cyclGif",
				imgid:"cycleGif",
				imgsrc:'',
			}]
		}]
	},
	// slide5
	{
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn5
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.ex5_op1
			},{
				optdata:data.string.ex5_op2
			},{
				optdata:data.string.ex5_op3
			},{
				optdata:data.string.ex5_op4
			}]
		}]
	},
	// slide6
	{
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn6
		}],
		exetype1:[{
			optionsdivclass:"optionsdivSec",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.ex6_op1
			},{
				optdata:data.string.ex6_op2
			},{
				optdata:data.string.ex6_op3
			}]
		}],
		imageblock:[{
			singleimagecontainerclass:"leftImgCont",
			imagestoshow:[{
				imgclass:"leftImg",
				imgid:"fan",
				imgsrc:'',
			}]
		}]
	},
	// slide7
	{
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn7
		}],
		exetype1:[{
			optionsdivclass:"optionsdivSec",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.ex7_op1
			},{
				optdata:data.string.ex7_op2
			},{
				optdata:data.string.ex7_op3
			}]
		}],
		imageblock:[{
			singleimagecontainerclass:"leftImgCont",
			imagestoshow:[{
				imgclass:"leftImg",
				imgid:"clock_pendulam",
				imgsrc:'',
			}]
		}]
	},
	// slide8
	{
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn8
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.ex8_op1
			},{
				optdata:data.string.ex8_op2
			},{
				optdata:data.string.ex8_op3
			},{
				optdata:data.string.ex8_op4
			}]
		}]
	},
	// slide9
	{
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn9
		}],
		exetype1:[{
			optionsdivclass:"optionsdivSec",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.ex9_op1
			},{
				optdata:data.string.ex9_op2
			},{
				optdata:data.string.ex9_op3
			}]
		}],
		imageblock:[{
			singleimagecontainerclass:"leftImgCont",
			imagestoshow:[{
				imgclass:"bgstretch busht20",
				imgid:"bg_for_bus",
				imgsrc:'',
			},{
				imgclass:"schoolbus",
				imgid:"school_bus",
				imgsrc:'',
			}]
		}]
	},
	// slide10
	{
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"topqn",
			qndata:data.string.clktxt
		},{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"secqn",
			qndata:data.string.qn10
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.ex10_op1
			},{
				optdata:data.string.ex10_op2
			},{
				optdata:data.string.ex10_op3
			},{
				optdata:data.string.ex10_op4
			}]
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "q1op1", src: imgpath+"q02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op2", src: imgpath+"q04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op3", src: imgpath+"q03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op4", src: imgpath+"q05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op5", src: imgpath+"q01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "q2op1", src: imgpath+"q06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q2op2", src: imgpath+"q07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q2op3", src: imgpath+"q08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q2op4", src: imgpath+"q14.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q2op5", src: imgpath+"q15.png", type: createjs.AbstractLoader.IMAGE},

			{id: "q4op1", src: imgpath+"q11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q4op2", src: imgpath+"q19.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q4op3", src: imgpath+"q18.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q4op4", src: imgpath+"q09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q4op5", src: imgpath+"q10.png", type: createjs.AbstractLoader.IMAGE},

			{id: "q8op1", src: imgpath+"box_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q8op2", src: imgpath+"box_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q8op3", src: imgpath+"box_03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg_for_bus", src: imgpath+"bg_for_bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school_bus", src: imgpath+"school_bus.png", type: createjs.AbstractLoader.IMAGE},

			{id: "q6img", src: imgpath+"q12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q7img", src: imgpath+"q13.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q9img", src: imgpath+"q04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fan", src: imgpath+"fan.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "clock_pendulam", src: imgpath+"clock_pendulam.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cycleGif", src: imgpath+"girl-in-cycle.gif", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "excSnd", src: soundAsset+"click_on_the_correct_option.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new RhinoTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
		content[countNext].imgload?put_image_sec(content, countNext):"";
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	testin.numberOfQuestions();

	 	var ansClicked = false;
	 	var wrngClicked = false;

	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
	    return randNum;
	  }
		$(".secqn").prepend(countNext+1+". ");
		function qnGenerator(pageNo, imgOpnArray, coropn, opn1, opn2, opn3){
			$(opn3).find("img:eq(0)").attr("src",preload.getResult('q'+pageNo+'op'+imgOpnArray[1]).src);
			$(opn3).find("p").html(eval('data.string.ex'+pageNo+'_op'+imgOpnArray[1]));

			$(opn2).find("img:eq(0)").attr("src",preload.getResult('q'+pageNo+'op'+imgOpnArray[0]).src);
			$(opn2).find("p").html(eval('data.string.ex'+pageNo+'_op'+imgOpnArray[0]));

			$(opn1).find("img:eq(0)").attr("src",preload.getResult('q'+pageNo+'op'+coropn).src);
			$(opn1).find("p").html(eval('data.string.ex'+pageNo+'_op'+coropn));

		}
	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
	 	switch(countNext){
		 case 0:
		 		sound_player("excSnd",0);
 				var imgOpnArray = [3,4,5];
				var coropn= rand_generator(2);
				// console.log(coropn);
				imgOpnArray.shufflearray();
				qnGenerator(1, imgOpnArray, coropn, ".op1",".op2",".op3");
				randomize(".firstContainer");
			break;
			case 1:
				var imgOpnArray = [2,3,4,5];
 				var coropn= 1;
 				imgOpnArray.shufflearray();
 				qnGenerator(2, imgOpnArray, coropn, ".op1",".op2",".op3");
 				randomize(".firstContainer");
			break;
			case 2:
				randomize(".optionsdiv");
			break;
			case 3:
				// var imgOpnArray = [4,5];
 				// var coropn= rand_generator(3);
 				// imgOpnArray.shufflearray();
 				// qnGenerator(4, imgOpnArray, coropn, ".op1",".op2",".op3");
 				randomize(".optionsdivSec");
			break;
			case 4:
				var opnArray =[2,3,4,5,6,7,8,9];
				opnArray.shufflearray();
				$(".optionscontainer:eq(1)").find("p").html(eval("data.string.ex5_op"+opnArray[0]));
				$(".optionscontainer:eq(2)").find("p").html(eval("data.string.ex5_op"+opnArray[1]));
				$(".optionscontainer:eq(3)").find("p").html(eval("data.string.ex5_op"+opnArray[2]));
				randomize(".optionsdiv");
			break;
			case 5:
				var opnArray =[2,3,4,5];
				opnArray.shufflearray();
				mcqOpnGenerator(6,opnArray );
				randomize(".optionsdivSec");
			break;
			case 6:
				randomize(".optionsdivSec");
			break;
			case 7:
				$(".op1").append("<p class='forceTxt b1f1'>"+data.string.force1+"</p>");
				$(".op2").append("<p class='forceTxt b1f1'>"+data.string.force1+"</p>");
				$(".op3").append("<p class='forceTxt b1f1'>"+data.string.force1+"</p>");
				$(".op1").append("<p class='forceTxt b1f2'>"+data.string.force2+"</p>");
				$(".op2").append("<p class='forceTxt b1f2'>"+data.string.force2+"</p>");
				$(".op3").append("<p class='forceTxt b3f2'>"+data.string.force2+"</p>");
				$(".op3").append("<p class='forceTxt b3f3'>"+data.string.force3+"</p>");
				randomize(".firstContainer");
			break;
			case 8:
				// var opnArray =[2,3,4,5];
				// opnArray.shufflearray();
				// mcqOpnGenerator(9,opnArray );
				randomize(".optionsdivSec");
			break;
	 	}
		function mcqOpnGenerator(qnNum,opnArray ){
			$(".optionscontainer:eq(1)").find("p").html(eval("data.string.ex"+qnNum+"_op"+opnArray[1]));
			$(".optionscontainer:eq(2)").find("p").html(eval("data.string.ex"+qnNum+"_op"+opnArray[2]));
		}
		$(".opnContainer").click(function(){
			createjs.Sound.stop();
			if($(this).hasClass("class1")){
				$(this).children(".corctopt").show(0);
				play_correct_incorrect_sound(1);
				if(wrngClicked == false){
					testin.update(true);
				}
				$(".opnContainer").css({
					"pointer-events":"none",
				});
				$nextBtn.show();
			}
			else{
				$(this).children(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				wrngClicked = true;
				$(this).css({
					"pointer-events":"none",
				});
			}
		});
		$(".buttonsel").click(function(){
			createjs.Sound.stop();
				$(this).removeClass('forhover');
					if(ansClicked == false){
						/*class 1 is always for the right answer. updates scoreboard and disables other click if
						right answer is clicked*/
						if($(this).hasClass("class1")){
							if(wrngClicked == false){
								testin.update(true);
							}
							play_correct_incorrect_sound(1);
							$(this).css({
								"background": "#bed62fff",
								"border":"5px solid #deef3c",
								"color":"white",
								'pointer-events': 'none'
							});
							$(this).siblings(".corctopt").show(0);
							$('.buttonsel').removeClass('forhover forhoverimg');
							$('.buttonsel').removeClass('clock-hover');
							ansClicked = true;
							if(countNext != $total_page)
							$nextBtn.show(0);
						}
						else{
							testin.update(false);
							play_correct_incorrect_sound(0);
							$(this).css({
								"background":"#FF0000",
								"border":"5px solid #980000",
								"color":"white",
								'pointer-events': 'none'
							});
							$(this).siblings(".wrngopt").show(0);
							wrngClicked = true;
						}
					}
				});
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('imgexc')){
			var imageblock = content[count].imgexc[0];
			if(imageblock.hasOwnProperty('imgexcoptions')){
				var imageClass = imageblock.imgexcoptions;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	//  }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang + "/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		extratextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-red',
			textclass:"quen",
			textdata:data.string.p3text1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv1 correct opti",
			exeoptions:[{
				optaddclass:"",
				optdata:data.string.n_10
			},{
				optaddclass:"",
				optdata:data.string.n_9
			},{
				optaddclass:"",
				optdata:data.string.n_8
			},{
				optaddclass:"",
				optdata:data.string.n_7
			},{
				optaddclass:"",
				optdata:data.string.n_6
			},{
				optaddclass:"",
				optdata:data.string.n_5
			},{
				optaddclass:"",
				optdata:data.string.n_4
			},{
				optaddclass:"",
				optdata:data.string.n_3
		},{
				optaddclass:"",
				optdata:data.string.n_2
	},{
				optaddclass:"",
				optdata:data.string.n_1
			}]
		},{
			optionsdivclass:"optionsdiv2  opti",
			exeoptions:[{
				optaddclass:"",
				optdata:data.string.n0
			},{
				optaddclass:"",
				optdata:data.string.n1
			},{
				optaddclass:"",
				optdata:data.string.n2
			},{
				optaddclass:"",
				optdata:data.string.n3
			},{
				optaddclass:"",
				optdata:data.string.n4
			},{
				optaddclass:"",
				optdata:data.string.n5
			},{
				optaddclass:"",
				optdata:data.string.n6
			},{
				optaddclass:"",
				optdata:data.string.n7
		},{
				optaddclass:"",
				optdata:data.string.n8
	},{
				optaddclass:"",
				optdata:data.string.n9
			},{
			optaddclass:"",
			optdata:data.string.n10
			}]
		}]
	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		extratextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-red',
			textclass:"quen",
			textdata:data.string.p3text2
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv1",
			exeoptions:[{
				optaddclass:"",
				optdata:data.string.n_10
			},{
				optaddclass:"",
				optdata:data.string.n_9
			},{
				optaddclass:"",
				optdata:data.string.n_8
			},{
				optaddclass:"",
				optdata:data.string.n_7
			},{
				optaddclass:"",
				optdata:data.string.n_6
			},{
				optaddclass:"",
				optdata:data.string.n_5
			},{
				optaddclass:"",
				optdata:data.string.n_4
			},{
				optaddclass:"",
				optdata:data.string.n_3
		},{
				optaddclass:"",
				optdata:data.string.n_2
	},{
				optaddclass:"",
				optdata:data.string.n_1
			}]
		},{
			optionsdivclass:"optionsdiv2  correct",
			exeoptions:[{
				optaddclass:"",
				optdata:data.string.n0
			},{
				optaddclass:"",
				optdata:data.string.n1
			},{
				optaddclass:"",
				optdata:data.string.n2
			},{
				optaddclass:"",
				optdata:data.string.n3
			},{
				optaddclass:"",
				optdata:data.string.n4
			},{
				optaddclass:"",
				optdata:data.string.n5
			},{
				optaddclass:"",
				optdata:data.string.n6
			},{
				optaddclass:"",
				optdata:data.string.n7
		},{
				optaddclass:"",
				optdata:data.string.n8
	},{
				optaddclass:"",
				optdata:data.string.n9
			},{
			optaddclass:"",
			optdata:data.string.n10
			}]
		}]
	},
	//slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		extratextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-red',
			textclass:"quen",
			textdata:data.string.integer
		}],
		exetype:[{
			optionsdivclass:"maindiv1",
			textclass:"neg",
			textdata:data.string.neg,
			exeoptn:[{
				optaddclass:"",
				optdata:data.string.n_10
			},{
				optaddclass:"",
				optdata:data.string.n_9
			},{
				optaddclass:"",
				optdata:data.string.n_8
			},{
				optaddclass:"",
				optdata:data.string.n_7
			},{
				optaddclass:"",
				optdata:data.string.n_6
			},{
				optaddclass:"",
				optdata:data.string.n_5
			},{
				optaddclass:"",
				optdata:data.string.n_4
			},{
				optaddclass:"",
				optdata:data.string.n_3
		},{
				optaddclass:"",
				optdata:data.string.n_2
	},{
				optaddclass:"",
				optdata:data.string.n_1
			}]
		},{
			optionsdivclass:"maindiv2",
			textclass:"neg",
			textdata:data.string.who,
			exeoptn:[{
				optaddclass:"",
				optdata:data.string.n0
			},{
				optaddclass:"",
				optdata:data.string.n1
			},{
				optaddclass:"",
				optdata:data.string.n2
			},{
				optaddclass:"",
				optdata:data.string.n3
			},{
				optaddclass:"",
				optdata:data.string.n4
			},{
				optaddclass:"",
				optdata:data.string.n5
			},{
				optaddclass:"",
				optdata:data.string.n6
			},{
				optaddclass:"",
				optdata:data.string.n7
		},{
				optaddclass:"",
				optdata:data.string.n8
	},{
				optaddclass:"",
				optdata:data.string.n9
			},{
			optaddclass:"",
			optdata:data.string.n10
			}]
		},{
			optionsdivclass:"maindiv3 a",
			exeoptn:[{
				optaddclass:"a",
				optdata:data.string.n0
			},{
				optaddclass:"a",
				optdata:data.string.n1
			},{
				optaddclass:"a",
				optdata:data.string.n2
			},{
				optaddclass:"a",
				optdata:data.string.n3
			},{
				optaddclass:"a",
				optdata:data.string.n4
			},{
				optaddclass:"a",
				optdata:data.string.n5
			},{
				optaddclass:"a",
				optdata:data.string.n6
			},{
				optaddclass:"a",
				optdata:data.string.n7
		},{
				optaddclass:"a",
				optdata:data.string.n8
	},{
				optaddclass:"a",
				optdata:data.string.n9
			},{
			optaddclass:"a",
			optdata:data.string.n10
			}]
		},{
			optionsdivclass:"maindiv4",
			exeoptn:[{
				optaddclass:"",
				optdata:data.string.n_10
			},{
				optaddclass:"",
				optdata:data.string.n_9
			},{
				optaddclass:"",
				optdata:data.string.n_8
			},{
				optaddclass:"",
				optdata:data.string.n_7
			},{
				optaddclass:"",
				optdata:data.string.n_6
			},{
				optaddclass:"",
				optdata:data.string.n_5
			},{
				optaddclass:"",
				optdata:data.string.n_4
			},{
				optaddclass:"",
				optdata:data.string.n_3
		},{
				optaddclass:"",
				optdata:data.string.n_2
	},{
				optaddclass:"",
				optdata:data.string.n_1
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'arr01',
				imgclass:'arrow1'
			},{
				imgid:'arr01',
				imgclass:'arrow2'
			}]
		}]
	},
	//slide 3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		extratextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-red',
			textclass:"quen",
			textdata:data.string.integer
		}],
		lowertextblockadditionalclass:"lowertext",
		lowertextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			textclass:"bottomtext1",
			textdata:data.string.p3text3
		},{
			datahighlightflag : true,
			datahighlightcustomclass :"underline",
			textclass:"bottomtext2",
			textdata:data.string.p3text4
		}],
		exetype:[{
		optionsdivclass:"wholeanim",
		textdata:data.string.who,
		textclass:"neg1",
		exeoptn:[{
			optcontainerextra:"box1",
			optaddclass:"",
			optdata:data.string.n0
		},{
				optcontainerextra:"box1",
			optaddclass:"",
			optdata:data.string.n1
		},{
			optaddclass:"",
								optcontainerextra:"box1",
			optdata:data.string.n2
		},{
			optaddclass:"",
								optcontainerextra:"box1",
			optdata:data.string.n3
		},{
			optaddclass:"",
								optcontainerextra:"box1",
			optdata:data.string.n4
		},{
			optaddclass:"",
								optcontainerextra:"box1",
			optdata:data.string.n5
		},{
			optaddclass:"",
								optcontainerextra:"box1",
			optdata:data.string.n6
		},{
			optaddclass:"",
								optcontainerextra:"box1",
			optdata:data.string.n7
	},{
			optaddclass:"",
								optcontainerextra:"box1",
			optdata:data.string.n8
},{
			optaddclass:"",
								optcontainerextra:"box1",
			optdata:data.string.n9
		},{
		optaddclass:"",
							optcontainerextra:"box1",
		optdata:data.string.n10
		}]
	},{
		optionsdivclass:"neganim",
		textdata:data.string.neg,
		textclass:"neg1",
		exeoptn:[{
			optaddclass:"",
								optcontainerextra:"box",
			optdata:data.string.n_10
		},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_9
		},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_8
		},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_7
		},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_6
		},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_5
		},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_4
		},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_3
	},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_2
},{					optcontainerextra:"box",
			optaddclass:"",
			optdata:data.string.n_1
		}]
	}]
},
//slide 4
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		textclass:"quen",
		textdata:data.string.p3text5
	}],
			imagedivblock:[{
				imagediv:"firstpage",
				imagestoshow:[{
					imgclass: 'well-done',
					imgid : 'welldone',
					imgsrc: '',
				}],
	exetype1:[{
		optionsdivclass:"options-ddv",
		exeoptions:[{
			optcontainerextra:"opti opti1 correct",
			optaddclass:"",
			optdata:data.string.n4
		},{
				optcontainerextra:"opti opti2 incrt",
			optaddclass:"",
			optdata:data.string.n6_11
		},{
			optcontainerextra:"opti opti3 correct",
			optaddclass:"",
			optdata:data.string.n56
		},{
				optcontainerextra:"opti opti4 incrt",
			optaddclass:"",
			optdata:data.string.n1_2
		},{
			optcontainerextra:"opti opti5 incrt",
			optaddclass:"",
			optdata:data.string.n6_7
		},{
				optcontainerextra:"opti opti6 incrt",
			optaddclass:"",
			optdata:data.string.n2_3
		},{
			optcontainerextra:"opti opti7 correct",
			optaddclass:"",
			optdata:data.string.n122
		},{
				optcontainerextra:"opti opti8 incrt",
			optaddclass:"",
			optdata:data.string.n4_7
	},{
				optcontainerextra:"opti opti9 correct",
			optaddclass:"",
			optdata:data.string.n0
},{
				optcontainerextra:"opti opti10 correct",
			optaddclass:"",
			optdata:data.string.n13
},{
				optcontainerextra:"opti opti11  correct",
			optaddclass:"",
			optdata:data.string.n_71
		}]
	}]
},{

		imagediv:"lastpage",
		imagestoshow:[{
			imgclass: 'well-done',
			imgid : 'welldone',
			imgsrc: '',
		}],
		imagelabels:true,
		imagelabels:[{
		imagelabelclass:"boxx boxx1",
		imagelabeldata:data.string.n4
	},{
		imagelabelclass:"boxx boxx2",
		imagelabeldata:data.string.n122
	},{
		imagelabelclass:"boxx boxx3",
		imagelabeldata:data.string.n56
	},{
		imagelabelclass:"boxx boxx4",
		imagelabeldata:data.string.n_71
	},{
		imagelabelclass:"boxx boxx5",
		imagelabeldata:data.string.n0
	},{
		imagelabelclass:"boxx boxx6",
		imagelabeldata:data.string.n13
	},{
		imagelabelclass:"great-job",
		imagelabeldata:data.string.greatjob
	},{
		datahighlightflag : true,
		datahighlightcustomclass : 'color-bold',
		imagelabelclass:"botttext",
		imagelabeldata:data.string.p3text6
	}]
	}]

}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"bg_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-3", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-2", src: imgpath+"green_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-4", src: imgpath+"red_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-1", src: imgpath+"orange_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-5", src: imgpath+"pink_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-3", src: imgpath+"balloon_blue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-2", src: imgpath+"balloon_green.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-4", src: imgpath+"balloon_red.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-1", src: imgpath+"balloon_orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-5", src: imgpath+"balloon_pink.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-diy", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arr01", src: imgpath+"arrow03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "twoarrow", src: imgpath+"arrow04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "welldone", src: imgpath+"welldone02.png", type: createjs.AbstractLoader.IMAGE},


			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3_1.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_4_sec", src: soundAsset+"s3_p5_sec.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		$nextBtn.hide(0);

		// random number generator
			function rand_generator(limit){
				var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
				return randNum;
			}

			/*for randomizing the options*/
			function randomize(parent){
				var parent = $(parent);
				var divs = parent.children();
				while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
			}


		switch(countNext) {
			case 0:
			case 1:
				sound_player("sound_"+(countNext));
					$(".option").click(function(){
							createjs.Sound.stop();
						if($(this).hasClass("correct")){
							$(this).children(".corctopt").show(0);
							play_correct_incorrect_sound(1);
							$(this).addClass('corrects');
							$(" .option").css("pointer-events","none");
							nav_button_controls(0);

						}else{
							$(this).children(".wrngopt").show(0);
							play_correct_incorrect_sound(0);
							$(this).addClass('incorrect');
							$(this).css("pointer-events","none");
						}
					});
					break;

				case 2:
				sound_player("sound_"+(countNext));
				setTimeout(function(){
					$(".maindiv3,.maindiv4").css("opacity","1");
				},3000);
				setTimeout(function(){
					$(".arrow1,.arrow2").css("opacity","1");
				},6000);
					nav_button_controls(7000);
				break;
				case 3:
					setTimeout(function(){
						sound_nav("sound_"+(countNext));
					}, 5000);
				$(".lowertext").delay(5000).show(0);
				break;
				case 4:
				randomize(".optionsdiv");
				sound_player("sound_"+(countNext));
				var count=0;
				$(".optionscontainerddv").click(function(){
					createjs.Sound.stop();
					if($(this).hasClass("correct")){
						$(this).children(".corctopt").show(0);
						play_correct_incorrect_sound(1);
						$(this).addClass('corrects');
						count++;
						if(count==6){
							setTimeout(function(){
								sound_nav("sound_"+(countNext)+"_sec");
							}, 7000);
							$(" .optionscontainerddv").css("pointer-events","none");
							$(".lastpage").show(0);
							$(".well-done").delay(5000).show(0);
							$(".quen,.incrt").hide(3000);
							// setTimeout(function(){
								$(".correct").hide(0);
							// },1000);
						}

					}else{
						$(this).children(".wrngopt").show(0);
						play_correct_incorrect_sound(0);
						$(this).addClass('incorrect');
						$(this).css("pointer-events","none");
					}
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

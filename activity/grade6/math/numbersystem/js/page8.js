var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide 0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:"monkey",
				imgclass:"diy-monkey",
				imgsrc:''
			}]
		}],
		extratextblock:[{
			textclass:"diy-textclass",
			textdata:data.string.diytext
		}]
	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		uppertextblockadditionalclass: 'instruction-1',
		uppertextblock:[{
			textdata: data.string.dragtext,
			textclass: "text",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			splitintofractionsflag: true,
			textdata: data.string.p8text1,
			divclass: "drag-item drag-1",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text2,
			divclass: "drag-item drag-2",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text3,
			divclass: "drag-item drag-3",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text4,
			divclass: "drag-item drag-4",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text5,
			divclass: "drag-item drag-5",
		}],
		dropdiv:[{
			dropdivcontainer:"dragContainer",
			dropbox:[{
				dropbox:"dropbox dropbox1",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox2",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox3",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox4",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox5",
				textclass:"box-drop",
			}]
		}]
	},
	//slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		uppertextblockadditionalclass: 'instruction-1',
		uppertextblock:[{
			textdata: data.string.dragtext,
			textclass: "text",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			splitintofractionsflag: true,
			textdata: data.string.p8text6,
			divclass: "drag-item drag-1",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text7,
			divclass: "drag-item drag-2",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text8,
			divclass: "drag-item drag-3",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text9,
			divclass: "drag-item drag-4 drop-box1",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text10,
			divclass: "drag-item drag-5",
		}],
		dropdiv:[{
			dropdivcontainer:"dragContainer",
			dropbox:[{
				dropbox:"dropbox dropbox6",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox7",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox8",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox9",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox10",
				textclass:"box-drop",
			}]
		}]
	},
	//slide 3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		uppertextblockadditionalclass: 'instruction-1',
		uppertextblock:[{
			textdata: data.string.dragtext,
			textclass: "text",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			splitintofractionsflag: true,
			textdata: data.string.p8text11,
			divclass: "drag-item drag-1",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text12,
			divclass: "drag-item drag-2",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text13,
			divclass: "drag-item drag-3",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text14,
			divclass: "drag-item drag-4 ",
		},{
			splitintofractionsflag: true,
			textdata: data.string.p8text15,
			divclass: "drag-item drag-5 drop-box1",
		}],
		dropdiv:[{
			dropdivcontainer:"dragContainer",
			dropbox:[{
				dropbox:"dropbox drpbx1",
				textclass:"box-drop",
			},{
				dropbox:"dropbox drpbx2",
				textclass:"box-drop",
			},{
				dropbox:"dropbox drpbx3",
				textclass:"box-drop",
			},{
				dropbox:"dropbox drpbx4",
				textclass:"box-drop",
			},{
				dropbox:"dropbox drpbx5",
				textclass:"box-drop",
			}]
		}]
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	//for questions
	var randomInt = 0, TopPositions, BotPositions;

	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "monkey", src: imgpath+"welldone02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow1", src: imgpath+"arrow05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow2", src: imgpath+"arrow06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow3", src: imgpath+"arrow07.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"s8_p2.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		$('.correct-icon').attr('src', preload.getResult('correct').src);

		switch(countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			sound_player("sound_1");
			$(".dragContainer").append("<img class='imgCls' src='"+preload.getResult("arrow1").src+"'/>");
				var positions = [1,2,3,4,5];
				positions.shufflearray();
				for(var i=1; i<=6; i++){
					$('.drag-'+i).addClass('drag-pos-'+positions[i-1]);
				}
				var itemCount = 0;
				$('.drag-item').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.dropbox5').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-1');
					}
				});
				$('.dropbox4').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-3');
					}
				});
				$('.dropbox3').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-4');
					}
				});
				$('.dropbox2').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-2');
					}
				});
				$('.dropbox1').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-5');
					}
				});
				function dropper(dropped, droppedOn, hasclass){

					if(dropped.hasClass(hasclass)){
						dropped.draggable('option', 'revert', false);
						play_correct_incorrect_sound(1);
							createjs.Sound.stop();
						itemCount++;
						dropped.draggable('disable');
						dropped.detach().css({
							'height': '100%',
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'border': '0.5vmin solid #cdeb58',
							'background-color': '#89d457',
							'pointer-events': 'none'
						}).appendTo(droppedOn);
						$(droppedOn).css({
							'border': 'none'
						});
						$(droppedOn).parents('img').show(0);
						if(itemCount==5){
							nav_button_controls(0);
						}
					} else{
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				}
				break;

				case 2:
				$(".dragContainer").append("<img class='imgCls' src='"+preload.getResult("arrow2").src+"'/>");
					var positions = [1,2,3,4,5];
					positions.shufflearray();
					for(var i=1; i<=6; i++){
						$('.drag-'+i).addClass('drag-pos-'+positions[i-1]);
					}
					var itemCount = 0;
					$('.drag-item').draggable({
						containment: ".board",
						cursor: "move",
						revert: true,
						appendTo: "body",
						zIndex: 50,
						start: function( event, ui ){
							// $(this).addClass('selected');
						},
						stop: function( event, ui ){
							// $(this).removeClass('selected');
						}
					});
					$('.dropbox6').droppable({
						drop:function(event, ui) {
							dropper(ui.draggable, $(this), 'drag-4');
						}
					});
					$('.dropbox7').droppable({
						drop:function(event, ui) {
							dropper(ui.draggable, $(this), 'drag-2');
						}
					});
					$('.dropbox8').droppable({
						drop:function(event, ui) {
							dropper(ui.draggable, $(this), 'drag-5');
						}
					});
					$('.dropbox9').droppable({
						drop:function(event, ui) {
							dropper(ui.draggable, $(this), 'drag-3');
						}
					});
					$('.dropbox10').droppable({
						drop:function(event, ui) {
							dropper(ui.draggable, $(this), 'drag-1');
						}
					});
					function dropper(dropped, droppedOn, hasclass){
						if(dropped.hasClass(hasclass)){
							dropped.draggable('option', 'revert', false);
							play_correct_incorrect_sound(1);
							itemCount++;
							dropped.draggable('disable');
							dropped.detach().css({
								'height': '100%',
								'top': '0%',
								'left': '0%',
								'width': '100%',
								'border': '0.5vmin solid #cdeb58',
								'background-color': '#89d457',
								'pointer-events': 'none'
							}).appendTo(droppedOn);
							$(droppedOn).css({
								'border': 'none'
							});
							$(droppedOn).parents('img').show(0);
							if(itemCount==5){
								nav_button_controls(0);
							}
						} else{
							play_correct_incorrect_sound(0);
						}
					}
					break;

					case 3:
					$(".dragContainer").append("<img class='imgCls' src='"+preload.getResult("arrow3").src+"'/>");
						var positions = [1,2,3,4,5];
						positions.shufflearray();
						for(var i=1; i<=6; i++){
							$('.drag-'+i).addClass('drag-pos-'+positions[i-1]);
						}
						var itemCount = 0;
						$('.drag-item').draggable({
							containment: ".board",
							cursor: "move",
							revert: true,
							appendTo: "body",
							zIndex: 50,
							start: function( event, ui ){
								// $(this).addClass('selected');
							},
							stop: function( event, ui ){
								// $(this).removeClass('selected');
							}
						});
						$('.drpbx1').droppable({
							drop:function(event, ui) {
								dropper(ui.draggable, $(this), 'drag-5');
							}
						});
						$('.drpbx2').droppable({
							drop:function(event, ui) {
								dropper(ui.draggable, $(this), 'drag-3');
							}
						});
						$('.drpbx3').droppable({
							drop:function(event, ui) {
								dropper(ui.draggable, $(this), 'drag-4');
							}
						});
						$('.drpbx4').droppable({
							drop:function(event, ui) {
								dropper(ui.draggable, $(this), 'drag-2');
							}
						});
						$('.drpbx5').droppable({
							drop:function(event, ui) {
								dropper(ui.draggable, $(this), 'drag-1');
							}
						});
						function dropper(dropped, droppedOn, hasclass){
							if(dropped.hasClass(hasclass)){
								dropped.draggable('option', 'revert', false);
								play_correct_incorrect_sound(1);
								itemCount++;
								dropped.draggable('disable');
								dropped.detach().css({
									'height': '100%',
									'top': '0%',
									'left': '0%',
									'width': '100%',
									'border': '0.5vmin solid #cdeb58',
									'background-color': '#89d457',
									'pointer-events': 'none'
								}).appendTo(droppedOn);
								$(droppedOn).css({
									'border': 'none'
								});
								$(droppedOn).parents('img').show(0);
								if(itemCount==5){
									nav_button_controls(0);
								}
							} else{
								play_correct_incorrect_sound(0);
							}
						}
						break;




			default:

				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

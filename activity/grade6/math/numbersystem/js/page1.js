var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang + "/";

var content=[{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-bg',
	extratextblock:[{
		textclass:"titl",
		textdata:data.lesson.chapter
	},{
		textclass:"grad",
		textdata:data.string.grade
	}],
	imagedivblock:[{
		imagediv:"coverpagediv",
		imagestoshow:[{
			imgclass: 'coverarrow',
			imgid : 'arrow05',
			imgsrc: '',
		}]
	}]
},{
	//slide 1
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-bg',
	imagedivblock:[{
		imagediv:"coverpagediv",
		imagestoshow:[{
			imgclass: 'cover-page',
			imgid : 'line',
			imgsrc: '',
		}],
			imagelabels:true,
			imagelabels:[{
			imagelabelclass:"rtionnum",
			imagelabeldata:data.string.rationalnum
		},{
			imagelabelclass:"intnum",
			imagelabeldata:data.string.integer
		},{
			imagelabelclass:"fracnum",
			imagelabeldata:data.string.fraction
		},{
			imagelabelclass:"negative",
			imagelabeldata:data.string.negativenum
		},{
			imagelabelclass:"whlenum",
			imagelabeldata:data.string.wholenum
		},{
			imagelabelclass:"natunum",
			imagelabeldata:data.string.naturalnum
		}]
	}]
	},
	//slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		imagedivblock:[{
				imagediv:"mainImgCont",
				svgblock:[{
					svgblock : "ovalsvg",
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"natural-num",
					imagelabeldata:data.string.naturalnum
				},{
					imagelabelclass:"whole-num",
					imagelabeldata:data.string.wholenum
				},{
					imagelabelclass:"int-num",
					imagelabeldata:data.string.integer
				},{
					imagelabelclass:"rational-num",
					imagelabeldata:data.string.rationalnum
				}]
			}]
	},
	//slide 3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		imagedivblock:[{
			imagediv:"container",
			imagestoshow:[{
				imgclass: 'school',
				imgid : 'school',
				imgsrc: '',
			},{
				imgclass: 'fairy',
				imgid : 'fairy',
				imgsrc: '',
			},{
				imgclass: 'ram',
				imgid : 'ram',
				imgsrc: '',
			},{
				imgclass: 'mohan',
				imgid : 'mohan',
				imgsrc: '',
			},{
				imgclass: 'mohan1',
				imgid : 'mohan1',
				imgsrc: '',
			},{
				imgclass: 'boy',
				imgid : 'boy',
				imgsrc: '',
			},{
				imgclass: 'sita',
				imgid : 'sita',
				imgsrc: '',
			},{
				imgclass: 'girl',
				imgid : 'girl',
				imgsrc: '',
			},{
				imgclass: 'gita',
				imgid : 'gita',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'dummy',
				speechbox: 'sp-1',
				textdata : data.string.p1text1,
				textclass : 'text',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}]
	},
	//slide 4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		imagedivblock:[{
			imagediv:"container",
			imagestoshow:[{
				imgclass: 'school',
				imgid : 'school',
				imgsrc: '',
			},{
				imgclass: 'fairy',
				imgid : 'fairy',
				imgsrc: '',
			},{
				imgclass: 'ram',
				imgid : 'ram',
				imgsrc: '',
			},{
				imgclass: 'mohan',
				imgid : 'mohan',
				imgsrc: '',
			},{
				imgclass: 'mohan1',
				imgid : 'mohan1',
				imgsrc: '',
			},{
				imgclass: 'boy',
				imgid : 'boy',
				imgsrc: '',
			},{
				imgclass: 'sita',
				imgid : 'sita',
				imgsrc: '',
			},{
				imgclass: 'girl',
				imgid : 'girl',
				imgsrc: '',
			},{
				imgclass: 'gita',
				imgid : 'gita',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'dummy',
				speechbox: 'sp-1',
				textdata : data.string.p1text2,
				textclass : 'text',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}]
	},
	//slide 5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		lowertextblockadditionalclass:"divnum",
		lowertextblock:[{
			textclass:"number1",
			textdata:data.string.n1
		},{
			textclass:"number2",
			textdata:data.string.n2
		},{
			textclass:"number3",
			textdata:data.string.n3
		},{
			textclass:"number4",
			textdata:data.string.n4
		},{
			textclass:"number5",
			textdata:data.string.n5
		},{
			textclass:"number6",
			textdata:data.string.n6
		},{
			textclass:"number7",
			textdata:data.string.n7
		}],
		imagedivblock:[{
			imagediv:"container",
			imagestoshow:[{
				imgclass: 'school',
				imgid : 'school',
				imgsrc: '',
			},{
				imgclass: 'fairy',
				imgid : 'fairy',
				imgsrc: '',
			},{
				imgclass: 'ram',
				imgid : 'ram',
				imgsrc: '',
			},{
				imgclass: 'mohan',
				imgid : 'mohan',
				imgsrc: '',
			},{
				imgclass: 'mohan1',
				imgid : 'mohan1',
				imgsrc: '',
			},{
				imgclass: 'boy',
				imgid : 'boy',
				imgsrc: '',
			},{
				imgclass: 'sita',
				imgid : 'sita',
				imgsrc: '',
			},{
				imgclass: 'girl',
				imgid : 'girl',
				imgsrc: '',
			},{
				imgclass: 'gita',
				imgid : 'gita',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'dummy',
				speechbox: 'sp-1',
				// textdata : data.string.p1text2,
				textclass : 'text',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}]
	},
	//slide 6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		imagedivblock:[{
			imagediv:"container",
			imagestoshow:[{
				imgclass: 'school',
				imgid : 'school',
				imgsrc: '',
			},{
				imgclass: 'fairy',
				imgid : 'fairy',
				imgsrc: '',
			},{
				imgclass: 'ram',
				imgid : 'ram',
				imgsrc: '',
			},{
				imgclass: 'mohan',
				imgid : 'mohan',
				imgsrc: '',
			},{
				imgclass: 'mohan1',
				imgid : 'mohan1',
				imgsrc: '',
			},{
				imgclass: 'boy',
				imgid : 'boy',
				imgsrc: '',
			},{
				imgclass: 'sita',
				imgid : 'sita',
				imgsrc: '',
			},{
				imgclass: 'girl',
				imgid : 'girl',
				imgsrc: '',
			},{
				imgclass: 'gita',
				imgid : 'gita',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'dummy',
				speechbox: 'sp-1',
				textdata : data.string.p1text3,
				textclass : 'text',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}]
	},
	//slide 7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		extratextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-pink',
			textclass:"top-text",
			textdata:data.string.p1text4
		},{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-pink',
			textclass:"toptext",
			textdata:data.string.p1text5
		},{
			textclass:"bottom-text",
			textdata:data.string.p1text9
		}],
		lowertextblockadditionalclass:"box-text",
		lowertextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-blue',
			textclass:"boxtext1",
			textdata:data.string.p1text6
		},{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-blue',
			textclass:"boxtext2",
			textdata:data.string.p1text7
		},{
			datahighlightflag : true,
			datahighlightcustomclass : 'color-blue',
			textclass:"boxtext3",
			textdata:data.string.p1text8
		}]
	},
	//slide 8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		extratextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'dummy',
			textclass:"top-txt",
			textdata:data.string.p1text10
		}],
		imagedivblock:[{
			imagediv:"imgContainer1",
			imagestoshow:[{
				imgclass: 'ground',
				imgid : 'ground',
				imgsrc: '',
			},{
				imgclass: 'puppy dog1',
				imgid : 'dog',
				imgsrc: '',
			},{
				imgclass: 'puppy dog2',
				imgid : 'dog',
				imgsrc: '',
			},{
				imgclass: 'puppy dog3',
				imgid : 'dog',
				imgsrc: '',
			},{
				imgclass: 'puppy dog4',
				imgid : 'dog',
				imgsrc: '',
			}],
				imagelabels:true,
				imagelabels:[{
				imagelabelclass:"imgcontext",
				imagelabeldata:data.string.p1text11
			},{
				imagelabelclass:"num num1",
				imagelabeldata:data.string.n1
			},{
				imagelabelclass:"num num2",
				imagelabeldata:data.string.n2
			},{
				imagelabelclass:"num num3",
				imagelabeldata:data.string.n3
			},{
				imagelabelclass:"num num4",
				imagelabeldata:data.string.n4
			}]
		},{
			imagediv:"imgContainer2",
			imagestoshow:[{
				imgclass: 'ground',
				imgid : 'ground',
				imgsrc: '',
		}]
	}]
	},
	//slide 9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		extratextblock:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'dummy',
			textclass:"top-txt",
			textdata:data.string.p1text10
		},{
			datahighlightflag : true,
			datahighlightcustomclass : 'dummy',
			textclass:"toptxt",
			textdata:data.string.p1text12
		}],
		imagedivblock:[{
			imagediv:"imgContainer1",
			imagestoshow:[{
				imgclass: 'ground',
				imgid : 'ground',
				imgsrc: '',
			},{
				imgclass: 'puppy dog1',
				imgid : 'dog',
				imgsrc: '',
			},{
				imgclass: 'puppy dog2',
				imgid : 'dog',
				imgsrc: '',
			},{
				imgclass: 'puppy dog3',
				imgid : 'dog',
				imgsrc: '',
			},{
				imgclass: 'puppy dog4',
				imgid : 'dog',
				imgsrc: '',
			}],
				imagelabels:true,
				imagelabels:[{
				imagelabelclass:"imgcontext",
				imagelabeldata:data.string.p1text11
			},{
				imagelabelclass:"num num1",
				imagelabeldata:data.string.n1
			},{
				imagelabelclass:"num num2",
				imagelabeldata:data.string.n2
			},{
				imagelabelclass:"num num3",
				imagelabeldata:data.string.n3
			},{
				imagelabelclass:"num num4",
				imagelabeldata:data.string.n4
			}]
		},{
			imagediv:"imgContainer2",
			imagestoshow:[{
				imgclass: 'ground',
				imgid : 'ground',
				imgsrc: '',
		}]
	}]
},
//slide 10
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		datahighlightflag : true,
		datahighlightcustomclass : 'dummy',
		textclass:"top-txt",
		textdata:data.string.p1text13
	},{
		datahighlightflag : true,
		datahighlightcustomclass : 'underline',
		textclass:"bottomtxt",
		textdata:data.string.p1text14
	}],
	imagedivblock:[{
		imagediv:"imgContainer1",
		imagestoshow:[{
			imgclass: 'ground',
			imgid : 'ground',
			imgsrc: '',
		},{
			imgclass: 'puppy dog1',
			imgid : 'dog',
			imgsrc: '',
		},{
			imgclass: 'puppy dog2',
			imgid : 'dog',
			imgsrc: '',
		},{
			imgclass: 'puppy dog3',
			imgid : 'dog',
			imgsrc: '',
		},{
			imgclass: 'puppy dog4',
			imgid : 'dog',
			imgsrc: '',
		}],
			imagelabels:true,
			imagelabels:[{
		// 	imagelabelclass:"imgcontext",
		// 	imagelabeldata:data.string.p1text11
		// },{
			imagelabelclass:"num num1",
			imagelabeldata:data.string.n1
		},{
			imagelabelclass:"num num2",
			imagelabeldata:data.string.n2
		},{
			imagelabelclass:"num num3",
			imagelabeldata:data.string.n3
		},{
			imagelabelclass:"num num4",
			imagelabeldata:data.string.n4
		}]
	},{
		imagediv:"imgContainer2",
		imagestoshow:[{
			imgclass: 'ground',
			imgid : 'ground',
			imgsrc: '',
		}]
	}]
},
//slide 11
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	imagedivblock:[{
		imagediv:"linediv",
		imagestoshow:[{
			imgclass: 'line',
			imgid : 'arrow',
			imgsrc: '',
		}],
		imagelabels:true,
		imagelabels:[{
		imagelabelclass:"numb numb1",
		imagelabeldata:data.string.n1
	},{
		imagelabelclass:"numb numb2",
		imagelabeldata:data.string.n2
	},{
		imagelabelclass:"numb numb3",
		imagelabeldata:data.string.n3
	},{
		imagelabelclass:"numb numb4",
		imagelabeldata:data.string.n4
	},{
		imagelabelclass:"numb numb5",
		imagelabeldata:data.string.n5
	},{
		imagelabelclass:"numb numb6",
		imagelabeldata:data.string.n6
	},{
		imagelabelclass:"numb numb7",
		imagelabeldata:data.string.n7
	},{
		imagelabelclass:"numb numb8",
		imagelabeldata:data.string.n8
	},{
		imagelabelclass:"numb numb9",
		imagelabeldata:data.string.n9
	},{
		imagelabelclass:"numb numb10",
		imagelabeldata:data.string.n10
	},{
		imagelabelclass:"numb numb11",
		imagelabeldata:data.string.n11
	}]
	}],
	extratextblock:[{
		textdata:data.string.p1text17,
		textclass:"text-1"
	},{
		textdata:data.string.p1text15,
		textclass:"text-2"
	},{
		textdata:data.string.p1text16,
		textclass:"text-3"
	}]
}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images


			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "svgimg", src: imgpath+"boxes.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "school", src: imgpath+"school_bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sita", src: imgpath+"play01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gita", src: imgpath+"play02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mohan", src: imgpath+"play04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"play05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"play03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ram", src: imgpath+"play06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mohan1", src: imgpath+"play07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ground", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line", src: imgpath+"line.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow05", src: imgpath+"arrow05.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p8_1.ogg"},
			{id: "sound_7_1", src: soundAsset+"s1_p8_2.ogg"},
			{id: "sound_8a", src: soundAsset+"s1_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s1_p9_2.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p12.ogg"},
			{id: "sound_10a", src: soundAsset+"s1_p11_1.ogg"},
			{id: "sound_10b", src: soundAsset+"s1_p11_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		var svgtoload;
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		switch(countNext) {
			case 0:
			sound_nav("sound_0");
			break;
			case 2:
			sound_nav("sound_"+(countNext));
			var s= Snap('#ovalsvg');
			var svg = Snap.load(preload.getResult("svgimg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});

			break;

			case 3:
			sound_nav("sound_"+(countNext));
			$(".dummy:eq(0)").animate({
				opacity:1
			},2000,function(){
				$(".dummy:eq(1)").animate({
					opacity:1
				},2000,function(){
					// nav_button_controls(0);
				});
			});
			break;

			case 4:
			sound_nav("sound_"+(countNext));
			$(".dummy:eq(0)").animate({
				opacity:1
			},2000,function(){
				$(".dummy:eq(1)").addClass("boldtext").animate({
					opacity:1
				},2000,function(){
					// nav_button_controls(0);
				});
			});
			break;

			case 5:
			setTimeout(function(){
				sound_nav("sound_"+(countNext));
			},1500);

		setTimeout(function(){
			$(".number1").css("opacity","1");
			$(".boy").addClass("scaleup");
			setTimeout(function(){
				$(".number2").css("opacity","1");
				$(".gita").addClass("scaleup");
				setTimeout(function(){
					$(".number3").css("opacity","1");
					$(".sita").addClass("scaleup");
					setTimeout(function(){
						$(".number4").css("opacity","1");
						$(".girl").addClass("scaleup");
						setTimeout(function(){
							$(".number5").css("opacity","1");
							$(".mohan1").addClass("scaleup");
							setTimeout(function(){
								$(".number6").css("opacity","1");
								$(".mohan").addClass("scaleup");
								setTimeout(function(){
									$(".number7").css("opacity","1");
									$(".ram").addClass("scaleup");
									setTimeout(function(){
											// nav_button_controls(0);
		},1500);
		},1500);
			},1500);
			},1500);
			},1500);
			},1500);
			},1500);
		},1000);
			break;
			case 6:
				sound_nav("sound_"+(countNext));
			break;
			case 7:
			// sound_nav("sound_"+(countNext));
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_7");
				current_sound.play();
				current_sound.on("complete", function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_7_1");
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(300);
					});
				});
				$(".top-text").animate({
					opacity: 1
				},1000,function(){
					$(".toptext").animate({
						opacity: 1
					},1000,function(){
						$(".box-text,.boxtext1").animate({
							opacity: 1
						},1000,function(){
							$(".boxtext2").animate({
								opacity: 1
							},1000,function(){
								$(".boxtext3").animate({
									opacity: 1
								},1000,function(){
									$(".bottom-text").animate({
										opacity: 1
									},1000,function(){
											// nav_button_controls(0);
									});
								});
							});
						});
					});
				});
			break;

			case 8:
			$(".dummy:eq(0)").addClass("boldline").animate({
				opacity: 1
			},1000,function(){
				$(".dummy:eq(1)").addClass("boldtext").animate({
					opacity: 1
				},1000,function(){
					$(".imgContainer1,.imgContainer2").animate({
						opacity: 1
					},1000,function(){
						$(".dog4,.num1").animate({
							opacity: 1
						},1000,function(){
							$(".dog3,.num2").animate({
								opacity: 1
							},1000,function(){
								$(".dog2,.num3").animate({
									opacity: 1
								},1000,function(){
									$(".dog1,.num4").animate({
										opacity: 1
									},1000,function(){
												nav_button_controls(0);
											});
								});
							});
						});
					});
				});
			});
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_8a");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_8b");
			current_sound.play();
			current_sound.on("complete", function(){
				});
			});
			break;

			case 9:
			$(".dummy,.imgContainer1,.imgContainer2,.dog1,.dog2,.dog3,.dog4,.num1,.num2,.num3,.num4").css("opacity","1");
			$(".dummy:eq(0)").addClass("underline");
			sound_nav("sound_"+(countNext));
			break;

			case 10:
			$(".dummy:eq(0)").addClass("boldline").animate({
				opacity: 1
			},1000,function(){
				$(".dummy:eq(1)").animate({
					opacity: 1
				},1000,function(){
					$(".dummy:eq(2)").animate({
						opacity: 1
					},1000,function(){
					$(".imgContainer1,.imgContainer2").animate({
						opacity: 1
								},1000,function(){
									$(".imgContainer1,.imgContainer2,.dog1,.dog2,.dog3,.dog4,.num1,.num2,.num3,.num4").animate({
										opacity:1
									},1000,function(){
										$(".bottomtxt").animate({
												opacity:1
										},1000,function(){
												// nav_button_controls(0);
										});
									});
								});
							});
						});
					});
					setTimeout(function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_10a");
						current_sound.play();
						current_sound.on("complete", function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_10b");
						current_sound.play();
						current_sound.on("complete", function(){
							nav_button_controls(0);
							});
						});
					},3000);

					break;
				case 11:
								sound_nav("sound_12")
								$(".text-1").fadeIn();

								$(".text-2").fadeIn();

								$(".text-3").fadeIn();
								$(".line").fadeIn();
								$(".numb1").delay(3500).show(0);
								$(".numb2").delay(4000).show(0);
								$(".numb3").delay(4500).show(0);
								$(".numb4").delay(5000).show(0);
								$(".numb5").delay(5500).show(0);
								$(".numb6").delay(6000).show(0);
								$(".numb7").delay(6500).show(0);
								$(".numb8").delay(7000).show(0);
								$(".numb9").delay(7500).show(0);
								$(".numb10").delay(8000).show(0);
								$(".numb11").delay(8500).show(0);
										// nav_button_controls(9000);

				break;

			default:
				nav_button_controls(0);
				break;
		}

	}

	// function anima(divcls, cls){
	// 	$("divcls").animate({
	// 		opacity:1
	// 	},1000,function(){
	// 			$("cls").addClass("scaleup");
	// 	},500,function(){
	// 		$("cls").removeClass("scaleup");
	// 	});
	// }

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
	if(content[count].hasOwnProperty('sideboxes')){
		for(var i=0; i<content[count].sideboxes.length;i++){
			if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
			{
				var imageblock = content[count].sideboxes[i].imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var j=0; j<imageClass.length; j++){
						var image_src = preload.getResult(imageClass[j].imgid).src;
						//get list of classes
						var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
							// alert(i);
					}
				}
			}
		}
	}
}
	function put_speechbox_image(content, count){
	if(content[count].hasOwnProperty('speechbox')){
		var speechbox = content[count].speechbox;
		for(var i=0; i<speechbox.length; i++){
			var image_src = preload.getResult(speechbox[i].imgid).src;
			console.log(image_src);
			var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
			$(selector).attr('src', image_src);
		}
	}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		// for (var i = 0; i < content.length; i++) {
		// 	slides(i);
		// 	$($('.totalsequence')[i]).html(i);
		// 	$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		// 		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
		// 	});
		// }
		// function slides(i){
		// 		$($('.totalsequence')[i]).click(function(){
		// 			countNext = i;
		// 			createjs.Sound.stop();
		// 			templateCaller();
		// 		});
		// 	}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 0

	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		imagedivblock:[{
				imagediv:"mainImgCont",
				svgblock:[{
				svgdivCnt:"svgCnt",
					svgblock : "ovalsvg",
				}],
        imagestoshow:[{
          imgid:"arrow",
          imgclass:"arro",
          imgsrc:''
        }],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"natural-num",
					imagelabeldata:data.string.numsys1
				},{
					imagelabelclass:"whole-num",
					imagelabeldata:data.string.numsys2
				},{
					imagelabelclass:"int-num",
					imagelabeldata:data.string.numsys4
				},{
					imagelabelclass:"rational-num",
					imagelabeldata:data.string.numsys3
				},{
          imagelabelclass:"n1",
					imagelabeldata:data.string.na
        },{
          imagelabelclass:"n2",
          imagelabeldata:data.string.nb
        },{
          imagelabelclass:"n3",
          imagelabeldata:data.string.nc
        }]
			}],
      textblock: [{
  					textdiv:"title-box",
  					textclass: "title",
  					textdata: data.string.p10text1
  				},{
            datahighlightflag : true,
            datahighlightcustomclass : 'bold',
            textdiv:"Summarybox",
            textclass: "title",
            textdata: data.string.p10text2
          }]

	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		imagedivblock:[{
				imagediv:"mainImgCont",
				svgblock:[{
				svgdivCnt:"svgCnt",
					svgblock : "ovalsvg",
				}],
        imagestoshow:[{
          imgid:"arrow",
          imgclass:"arro1",
          imgsrc:''
        },{
					imgid:"arrow",
					imgclass:"arro2",
					imgsrc:''
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"natural-num1",
					imagelabeldata:data.string.numsys1
				},{
					imagelabelclass:"whole-num1",
					imagelabeldata:data.string.numsys2
				},{
					imagelabelclass:"int-num",
					imagelabeldata:data.string.numsys4
				},{
					imagelabelclass:"rational-num",
					imagelabeldata:data.string.numsys3
				},{
          imagelabelclass:"n1",
					imagelabeldata:data.string.na
        },{
          imagelabelclass:"n2",
          imagelabeldata:data.string.nb
        },{
          imagelabelclass:"n3",
          imagelabeldata:data.string.nc
        },{
					imagelabelclass:"n0",
					imagelabeldata:data.string.n0
				}]
			}],
      textblock: [{
  					textdiv:"title-box",
  					textclass: "title",
  					textdata: data.string.p10text1
  				},{
            datahighlightflag : true,
            datahighlightcustomclass : 'bold',
            textdiv:"Summarybox",
            textclass: "title",
            textdata: data.string.p10text3
          }]
},
//slide 2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'backgr',
	imagedivblock:[{
			imagediv:"mainImgCont",
			svgblock:[{
			svgdivCnt:"svgCnt",
				svgblock : "ovalsvg",
			}],
			imagestoshow:[{
				imgid:"arrow",
				imgclass:"arro1",
				imgsrc:''
			},{
				imgid:"arrow",
				imgclass:"arro2",
				imgsrc:''
			},{
				imgid:"arrow",
				imgclass:"arro3",
				imgsrc:''
			}],
			imagelabels:true,
			imagelabels:[{
				imagelabelclass:"natural-num1",
				imagelabeldata:data.string.numsys1
			},{
				imagelabelclass:"whole-num2",
				imagelabeldata:data.string.numsys2
			},{
				imagelabelclass:"int-num1",
				imagelabeldata:data.string.numsys4
			},{
				imagelabelclass:"rational-num",
				imagelabeldata:data.string.numsys3
			},{
				imagelabelclass:"n1",
				imagelabeldata:data.string.na
			},{
				imagelabelclass:"n2",
				imagelabeldata:data.string.nb
			},{
				imagelabelclass:"n3",
				imagelabeldata:data.string.nc
			},{
				imagelabelclass:"n0",
				imagelabeldata:data.string.n0
			},{
				imagelabelclass:"nn1",
				imagelabeldata:data.string.n_1
			},{
				imagelabelclass:"nn4",
				imagelabeldata:data.string.n_4
			},{
				imagelabelclass:"nn10",
				imagelabeldata:data.string.n_10
			}]
		}],
		textblock: [{
					textdiv:"title-box",
					textclass: "title",
					textdata: data.string.p10text1
				},{
					datahighlightflag : true,
					datahighlightcustomclass : 'bold',
					textdiv:"Summarybox",
					textclass: "title",
					textdata: data.string.p10text4
				}]
},
//slide 3
{

		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		imagedivblock:[{
				imagediv:"mainImgCont",
				svgblock:[{
				svgdivCnt:"svgCnt",
					svgblock : "ovalsvg",
				}],
				imagestoshow:[{
					imgid:"arrow",
					imgclass:"arro1",
					imgsrc:''
				},{
					imgid:"arrow",
					imgclass:"arro2",
					imgsrc:''
				},{
					imgid:"arrow",
					imgclass:"arro3",
					imgsrc:''
				},{
					imgid:"arrow",
					imgclass:"arro4",
					imgsrc:''
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"natural-num1",
					imagelabeldata:data.string.numsys1
				},{
					imagelabelclass:"whole-num2",
					imagelabeldata:data.string.numsys2
				},{
					imagelabelclass:"int-num2",
					imagelabeldata:data.string.numsys4
				},{
					imagelabelclass:"rational-num1",
					imagelabeldata:data.string.numsys3
				},{
					imagelabelclass:"n1",
					imagelabeldata:data.string.na
				},{
					imagelabelclass:"n2",
					imagelabeldata:data.string.nb
				},{
					imagelabelclass:"n3",
					imagelabeldata:data.string.nc
				},{
					imagelabelclass:"n0",
					imagelabeldata:data.string.n0
				},{
					imagelabelclass:"nn1",
					imagelabeldata:data.string.n_1
				},{
					imagelabelclass:"nn4",
					imagelabeldata:data.string.n_4
				},{
					imagelabelclass:"nn10",
					imagelabeldata:data.string.n_10
				},{
					imagelabelclass:"txt1",
					splitintofractionsflag: true,
					imagelabeldata:data.string.p10text6
				},{
					imagelabelclass:"txt2",
					splitintofractionsflag: true,
					imagelabeldata:data.string.p10text7
				},{
					imagelabelclass:"txt3",
					splitintofractionsflag: true,
					imagelabeldata:data.string.p10text8
				},{
					imagelabelclass:"txt4",
					splitintofractionsflag: true,
					imagelabeldata:data.string.p10text9
				}]
			}],
			textblock: [{
						textdiv:"title-box",
						textclass: "title",
						textdata: data.string.p10text1
					},{
						datahighlightflag : true,
						datahighlightcustomclass : 'bold',
						textdiv:"Summary-box",
						textclass: "title",
						textdata: data.string.p10text5
					}]
}


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images


			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "svgimg", src: imgpath+"boxes.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "linesvg", src: imgpath+"arrow01.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0a", src: soundAsset+"s10_p1_1.ogg"},
			{id: "sound_0b", src: soundAsset+"s10_p1_2.ogg"},
			{id: "sound_1", src: soundAsset+"s10_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s10_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s10_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
				splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		var svgtoload;
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		switch(countNext) {
			case 0:
			case 1:
			case 2:
			case 3:
			var s= Snap('#ovalsvg');
			var svg = Snap.load(preload.getResult("svgimg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				if(countNext==0){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_0a");
					current_sound.play();
					current_sound.on("complete", function(){
						setTimeout(function(){
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("sound_0b");
							current_sound.play();
							current_sound.on("complete", function(){
							});
						},2500);
					});
				$(".n1,.n2,.n3").delay(5500).show(0);
				$(".arro").delay(6000).show(0);
					nav_button_controls(9000);
				}
				else if(countNext==1){
					$(".n1,.n2,.n3").show(0);
					$(".n0").delay(5500).show(0);
					$(".arro2").delay(6000).show(0);
						// nav_button_controls(7000);
						setTimeout(function(){
							sound_nav("sound_1");
						},5000);
				}
				else if(countNext==2){
					$(".n1,.n2,.n3,.n0,.arro2").show(0);
					$(".nn1,.nn4,.nn10").delay(5500).show(0);
					$(".arro3").delay(6000).show(0);
						// nav_button_controls(7000);
						setTimeout(function(){
							sound_nav("sound_2");
						},5000);
				}
				else{
					$(".n1,.n2,.n3,.n0,.arro2,.nn1,.nn4,.nn10,.arro3").show(0);
					$(".txt1,.txt2,.txt3,.txt4").delay(5500).show(0);
					$(".arro4").delay(6000).show(0);
					setTimeout(function(){
						sound_nav("sound_3");
					},5000);
				}
			break;

			default:

				nav_button_controls(0);
				break;
		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
	if(content[count].hasOwnProperty('sideboxes')){
		for(var i=0; i<content[count].sideboxes.length;i++){
			if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
			{
				var imageblock = content[count].sideboxes[i].imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var j=0; j<imageClass.length; j++){
						var image_src = preload.getResult(imageClass[j].imgid).src;
						//get list of classes
						var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
							// alert(i);
					}
				}
			}
		}
	}
}
	function put_speechbox_image(content, count){
	if(content[count].hasOwnProperty('speechbox')){
		var speechbox = content[count].speechbox;
		for(var i=0; i<speechbox.length; i++){
			var image_src = preload.getResult(speechbox[i].imgid).src;
			console.log(image_src);
			var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
			$(selector).attr('src', image_src);
		}
	}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		// for (var i = 0; i < content.length; i++) {
		// 	slides(i);
		// 	$($('.totalsequence')[i]).html(i);
		// 	$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		// 		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
		// 	});
		// }
		// function slides(i){
		// 		$($('.totalsequence')[i]).click(function(){
		// 			countNext = i;
		// 			createjs.Sound.stop();
		// 			templateCaller();
		// 		});
		// 	}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

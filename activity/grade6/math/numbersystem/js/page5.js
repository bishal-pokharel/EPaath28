var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 0

	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'backgr',
		imagedivblock:[{
				imagediv:"mainImgCont",
				svgblock:[{
				svgdivCnt:"svgCnt",
					svgblock : "ovalsvg",
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"natural-num",
					imagelabeldata:data.string.naturalnum
				},{
					imagelabelclass:"whole-num",
					imagelabeldata:data.string.wholenum
				},{
					imagelabelclass:"int-num",
					imagelabeldata:data.string.integer
				},{
					imagelabelclass:"rational-num",
					imagelabeldata:data.string.rationalnum
				}]
			}]
	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',
		coverboardadditionalclass:"back-ground",
		extratextblock:[{
			textclass:"toptext",
			textdata:data.string.fraction
		}],
		textblock: [
				{
						textdiv:"fracdiv fracdiv3",
						textclass: "text  frac-2",
						splitintofractionsflag: true,
						textdata: data.string.p5text1
				},
				{
						textdiv:"fracdiv fracdiv1 ",
						textclass: "text",
						textdata: data.string.p5text2
				},
				{
						textdiv:"fracdiv fracdiv2",
						textclass: "text",
						textdata: data.string.p5text5
				}],
				imagedivblock:[{
						imagediv:"div-div",
						imagestoshow:[{
							imgid:"arrow",
							imgclass:"arrow1",
							imgsrc:''
						},{
							imgid:"arrow",
							imgclass:"arrow2",
							imgsrc:''
						}],
						imagelabels:true,
						imagelabels:[{
							datahighlightflag : true,
							datahighlightcustomclass : 'dummy',
							imagelabelclass:"frac-text frac-1",
							splitintofractionsflag: true,
							imagelabeldata:data.string.f_1_2,
						},{
							imagelabelclass:"numera",
							imagelabeldata:data.string.p5text3,
						},{
							imagelabelclass:"demure",
							imagelabeldata:data.string.p5text4,
						}]
					}]
},
//slide 2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		textclass:"titletext",
		splitintofractionsflag: true,
		textdata:data.string.p5text6
	}],
	textblock: [
			{
					textdiv:"smallbox smallbox1",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_1_5
			},
			{
					textdiv:"smallbox smallbox2",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_2_5
			},
			{
					textdiv:"smallbox smallbox3",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_3_5
			},{
				textdiv:"smallbox smallbox4",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_4_5
			},{
				textdiv:"smallbox smallbox5",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_5_5
			},{
				textdiv:"middletext",
				textclass: "middle-text",
				textdata: data.string.p5text7
			}],
			imagedivblock:[{
					imagediv:"svgContainer",
					svgblock:[{
						svgdivCnt:"svgdivcontainer",
						svgblock : "svgline",
					}]
					}]
},
//slide 3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		textclass:"titletext",
		splitintofractionsflag: true,
		textdata:data.string.p5text6
	}],
	textblock: [
			{
					textdiv:"smallbox smallbox1",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_1_5
			},
			{
					textdiv:"smallbox smallbox2",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_2_5
			},
			{
					textdiv:"smallbox smallbox3",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_3_5
			},{
				textdiv:"smallbox smallbox4",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_4_5
			},{
				textdiv:"smallbox smallbox5",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_5_5
			},{
				textdiv:"middletext",
				textclass: "middle-text",
				textdata: data.string.p5text8
			}],
			imagedivblock:[{
					imagediv:"svgContainer",
					svgblock:[{
						svgdivCnt:"svgdivcontainer",
						svgblock : "svgline",
					}]
					}]
},
//slide 4
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		textclass:"titletext",
		splitintofractionsflag: true,
		textdata:data.string.p5text6
	}],
	textblock: [
			{
					textdiv:"smallbox smallbox1",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_1_5
			},
			{
					textdiv:"smallbox smallbox2",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_2_5
			},
			{
					textdiv:"smallbox smallbox3",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_3_5
			},{
				textdiv:"smallbox smallbox4",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_4_5
			},{
				textdiv:"smallbox smallbox5",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_5_5
			},{
				textdiv:"middletext",
				textclass: "middle-text",
				splitintofractionsflag: true,
				textdata: data.string.p5text9
			}],
			imagedivblock:[{
					imagediv:"svgContainer",
					svgblock:[{
						svgdivCnt:"svgdivcontainer",
						svgblock : "svgline",
					}]
					}]
},
//slide 5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		textclass:"titletext",
		splitintofractionsflag: true,
		textdata:data.string.p5text6
	}],
	textblock: [
			{
					textdiv:"smallbox smallbox1",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_1_5
			},
			{
					textdiv:"smallbox smallbox2",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_2_5
			},
			{
					textdiv:"smallbox smallbox3",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_3_5
			},{
				textdiv:"smallbox smallbox4",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_4_5
			},{
				textdiv:"smallbox smallbox5",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_5_5
			},{
				textdiv:"middletext",
				textclass: "middle-text",
				splitintofractionsflag: true,
				textdata: data.string.p5text10
			}],
			imagedivblock:[{
					imagediv:"svgContainer",
					svgblock:[{
						svgdivCnt:"svgdivcontainer",
						svgblock : "svgline",
					}]
					}]
},
//slide 6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		textclass:"titletext",
		splitintofractionsflag: true,
		textdata:data.string.p5text6
	}],
	textblock: [
			{
					textdiv:"smallbox smallbox1",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_1_5
			},
			{
					textdiv:"smallbox smallbox2",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_2_5
			},
			{
					textdiv:"smallbox smallbox3",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_3_5
			},{
				textdiv:"smallbox smallbox4",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_4_5
			},{
				textdiv:"smallbox smallbox5",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_5_5
			},{
				textdiv:"middletext",
				textclass: "middle-text",
				splitintofractionsflag: true,
				textdata: data.string.p5text11
			}],
			imagedivblock:[{
					imagediv:"svgContainer",
					svgblock:[{
						svgdivCnt:"svgdivcontainer",
						svgblock : "svgline",
					}]
					}]
},
//slide 7
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		textclass:"titletext",
		splitintofractionsflag: true,
		textdata:data.string.p5text6
	}],
	textblock: [
			{
					textdiv:"smallbox smallbox1",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_1_5
			},
			{
					textdiv:"smallbox smallbox2",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_2_5
			},
			{
					textdiv:"smallbox smallbox3",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_3_5
			},{
				textdiv:"smallbox smallbox4",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_4_5
			},{
				textdiv:"smallbox smallbox5",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_5_5
			},{
				textdiv:"middletext",
				textclass: "middle-text",
				splitintofractionsflag: true,
				textdata: data.string.p5text12
			}],
			imagedivblock:[{
					imagediv:"svgContainer",
					svgblock:[{
						svgdivCnt:"svgdivcontainer",
						svgblock : "svgline",
					}]
					}]
},
//slide 8
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',
	coverboardadditionalclass:"back-ground",
	extratextblock:[{
		textclass:"titletext",
		splitintofractionsflag: true,
		textdata:data.string.p5text6
	}],
	textblock: [
			{
					textdiv:"smallbox smallbox1",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_1_5
			},
			{
					textdiv:"smallbox smallbox2",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_2_5
			},
			{
					textdiv:"smallbox smallbox3",
					textclass: "txt",
					splitintofractionsflag: true,
					textdata: data.string.f_3_5
			},{
				textdiv:"smallbox smallbox4",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_4_5
			},{
				textdiv:"smallbox smallbox5",
				textclass: "txt",
					splitintofractionsflag: true,
				textdata: data.string.f_5_5
			},{
				textdiv:"middletext",
				textclass: "middle-text",
				splitintofractionsflag: true,
				textdata: data.string.p5text13
			}],
			imagedivblock:[{
					imagediv:"svgContainer",
					svgblock:[{
						svgdivCnt:"svgdivcontainer",
						svgblock : "svgline",
					}]
					}]
}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images


			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "svgimg", src: imgpath+"boxes.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "linesvg", src: imgpath+"arrow01.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s5_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s5_p2_1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p6", src: soundAsset+"s5_p6.ogg"},
			{id: "s5_p7", src: soundAsset+"s5_p7.ogg"},
			{id: "s5_p8", src: soundAsset+"s5_p8.ogg"},
			{id: "s5_p9", src: soundAsset+"s5_p9.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
				splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		var svgtoload;
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		switch(countNext) {
			case 0:
			sound_nav("sound_0");
			var s= Snap('#ovalsvg');
			var svg = Snap.load(preload.getResult("svgimg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});

			break;

			case 1:
			sound_nav("s5_p2");
			$(".toptext").fadeIn();
			$(".fracdiv3").fadeIn();
			$(".fracdiv1").fadeIn();
			$(".div-div").fadeIn();
			$(".fracdiv2").fadeIn();
			// nav_button_controls(1000);
			break;

			case 2:
			sound_nav("s5_p"+(countNext+1));
			var s= Snap('#svgline');
			var svg = Snap.load(preload.getResult("linesvg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				$(".svgContainer").append("<div class='numcontainer'/>");
				var leftcount =11;

				for(var i=0;i<=5;i++){
					$(".numcontainer").append("<div class='boxNum"+i+"'/>");
					(i>0 && i<=4)?$(".boxNum"+i).append("<p class='numNam'>"+i+"/5"+"</p>"):
					$(".boxNum"+i).append("<p class='numNam'>"+(i && 1)+"</p>");
				}

				for(var i=0;i<=5;i++){
					leftcount>30?leftcount+=1:'';
					$(".boxNum"+i).css({
						"left" : leftcount+"%"
					});
					leftcount+=14;
				}

				 $(".titletext").fadeIn();
					$(".smallbox").fadeIn();
					$(".middletext").fadeIn();

					$(".svgContainer").fadeIn(200);
					$("one").show(200);
					$(".boxNum0").delay(1000).show(0);
					$("six").show(200);
					$(".boxNum5").delay(1000).show(0);
					// nav_button_controls(2000);

			break;

			case 3:
			sound_nav("s5_p"+(countNext+1));
			$(".titletext,.smallbox").show(0);

			var s= Snap('#svgline');
			var svg = Snap.load(preload.getResult("linesvg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
					$(".one,.six,.boxNum0,.boxNum5").show(0);
					$(".two").delay(1000).show(0);
					$(".three").delay(2000).show(0);
					$(".four").delay(3000).show(0);
					$(".five").delay(4000).show(0);
					// nav_button_controls(4500);
				});
				$(".svgContainer").append("<div class='numcontainer'/>");
				var leftcount =11;

				for(var i=0;i<=5;i++){
					$(".numcontainer").append("<div class='boxNum"+i+"'/>");
					(i>0 && i<=4)?$(".boxNum"+i).append("<p class='numNam'>"+i+"/5"+"</p>"):
					$(".boxNum"+i).append("<p class='numNam'>"+(i && 1)+"</p>");
				}

				for(var i=0;i<=5;i++){
					leftcount>30?leftcount+=1:'';
					$(".boxNum"+i).css({
						"left" : leftcount+"%"
					});
					leftcount+=14;
				}

						 $(".middletext").fadeIn();
							// $(".svgContainer").fadeIn();

			break;

			case 4:
			sound_nav("s5_p"+(countNext+1));
				$(".titletext,.smallbox").show(0);
			var s= Snap('#svgline');
			var svg = Snap.load(preload.getResult("linesvg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
					$(".one,.boxNum0,.six,.boxNum5,.two,.three,.four,.five").show(0);
					$(".boxNum1").delay(1000).show(0);
					 $(".curve1").delay(2000).show(0);
					 // nav_button_controls(3000);
				});
				$(".svgContainer").append("<div class='numcontainer'/>");
				var leftcount =11;

				for(var i=0;i<=5;i++){
					$(".numcontainer").append("<div class='boxNum"+i+"'/>");
					(i>0 && i<=4)?$(".boxNum"+i).append("<p class='numNam'>"+i+"/5"+"</p>"):
					$(".boxNum"+i).append("<p class='numNam'>"+(i && 1)+"</p>");
				}

				for(var i=0;i<=5;i++){
					leftcount>30?leftcount+=1:'';
					$(".boxNum"+i).css({
						"left" : leftcount+"%"
					});
					leftcount+=14;
				}

						 $(".middletext").fadeIn();
							 // $(".svgContainer").fadeIn();
							 // $(".one,.boxNum0,.six,.boxNum5,.two,.three,.four,.five").show(0);


			break;

			case 5:
			sound_nav("s5_p"+(countNext+1));
			$(".titletext,.smallbox").show(0);
			var s= Snap('#svgline');
			var svg = Snap.load(preload.getResult("linesvg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
					$(".one,.boxNum0,.curve1,.six,.boxNum5,.two,.three,.four,.five,.boxNum1").show(0);
					$(".boxNum2").delay(1000).show(0);
					$(".curve2").delay(2000).show(0);
					// nav_button_controls(3000);

				});
				$(".svgContainer").append("<div class='numcontainer'/>");
				var leftcount =11;

				for(var i=0;i<=5;i++){
					$(".numcontainer").append("<div class='boxNum"+i+"'/>");
					(i>0 && i<=4)?$(".boxNum"+i).append("<p class='numNam'>"+i+"/5"+"</p>"):
					$(".boxNum"+i).append("<p class='numNam'>"+(i && 1)+"</p>");
				}

				for(var i=0;i<=5;i++){
					leftcount>30?leftcount+=1:'';
					$(".boxNum"+i).css({
						"left" : leftcount+"%"
					});
					leftcount+=14;
				}
				 $(".middletext").fadeIn();
				// $(".svgContainer").fadeIn();


			break;

			case 6:
			sound_nav("s5_p"+(countNext+1));
			$(".titletext,.smallbox").show(0);
			var s= Snap('#svgline');
			var svg = Snap.load(preload.getResult("linesvg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
					$(".one,.boxNum0,.six,.boxNum5,.two,.three,.four,.five,.boxNum1,.boxNum2,.curve1,.curve2").show(0);
					$(".boxNum3").delay(1000).show(0);
					$(".curve3").delay(2000).show(0);
					// nav_button_controls(3000);
				});
				$(".svgContainer").append("<div class='numcontainer'/>");
				var leftcount =11;

				for(var i=0;i<=5;i++){
					$(".numcontainer").append("<div class='boxNum"+i+"'/>");
					(i>0 && i<=4)?$(".boxNum"+i).append("<p class='numNam'>"+i+"/5"+"</p>"):
					$(".boxNum"+i).append("<p class='numNam'>"+(i && 1)+"</p>");
				}

				for(var i=0;i<=5;i++){
					leftcount>30?leftcount+=1:'';
					$(".boxNum"+i).css({
						"left" : leftcount+"%"
					});
					leftcount+=14;
				}

						 $(".middletext").fadeIn();

								// $(".svgContainer").fadeIn();


			break;

			case 7:
			sound_nav("s5_p"+(countNext+1));
			$(".titletext,.smallbox").show(0);
			var s= Snap('#svgline');
			var svg = Snap.load(preload.getResult("linesvg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
					$(".one,.boxNum0,.boxNum3,.six,.boxNum5,.two,.three,.four,.five,.boxNum1,.boxNum2,.curve1,.curve2,.curve3").show(0);
					$(".boxNum4").delay(1000).show(0);
					$(".curve4").delay(2000).show(0);
					// nav_button_controls(3000);
				});
				$(".svgContainer").append("<div class='numcontainer'/>");
				var leftcount =11;

				for(var i=0;i<=5;i++){
					$(".numcontainer").append("<div class='boxNum"+i+"'/>");
					(i>0 && i<=4)?$(".boxNum"+i).append("<p class='numNam'>"+i+"/5"+"</p>"):
					$(".boxNum"+i).append("<p class='numNam'>"+(i && 1)+"</p>");
				}

				for(var i=0;i<=5;i++){
					leftcount>30?leftcount+=1:'';
					$(".boxNum"+i).css({
						"left" : leftcount+"%"
					});
					leftcount+=14;
				}

						 $(".middletext").fadeIn();

								// $(".svgContainer").fadeIn();


			break;

			case 8:
			sound_nav("s5_p"+(countNext+1));
			$(".titletext,.smallbox").show(0);
			var s= Snap('#svgline');
			var svg = Snap.load(preload.getResult("linesvg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
					$(".one,.boxNum0,.boxNum3,.six,.boxNum5,.two,.three,.four,.five,.boxNum1,.boxNum2,.boxNum4,.curve1,.curve2,.curve3,.curve4").show(0);
					$(".curve5").delay(1000).show(0);
					// nav_button_controls(2000);
				});
				$(".svgContainer").append("<div class='numcontainer'/>");
				var leftcount =11;

				for(var i=0;i<=5;i++){
					$(".numcontainer").append("<div class='boxNum"+i+"'/>");
					(i>0 && i<=4)?$(".boxNum"+i).append("<p class='numNam'>"+i+"/5"+"</p>"):
					$(".boxNum"+i).append("<p class='numNam'>"+(i && 1)+"</p>");
				}

				for(var i=0;i<=5;i++){
					leftcount>30?leftcount+=1:'';
					$(".boxNum"+i).css({
						"left" : leftcount+"%"
					});
					leftcount+=14;
				}

						 $(".middletext").fadeIn();

								// $(".svgContainer").fadeIn();


			break;
			default:

				nav_button_controls(0);
				break;
		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
	if(content[count].hasOwnProperty('sideboxes')){
		for(var i=0; i<content[count].sideboxes.length;i++){
			if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
			{
				var imageblock = content[count].sideboxes[i].imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var j=0; j<imageClass.length; j++){
						var image_src = preload.getResult(imageClass[j].imgid).src;
						//get list of classes
						var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
							// alert(i);
					}
				}
			}
		}
	}
}
	function put_speechbox_image(content, count){
	if(content[count].hasOwnProperty('speechbox')){
		var speechbox = content[count].speechbox;
		for(var i=0; i<speechbox.length; i++){
			var image_src = preload.getResult(speechbox[i].imgid).src;
			console.log(image_src);
			var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
			$(selector).attr('src', image_src);
		}
	}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		// for (var i = 0; i < content.length; i++) {
		// 	slides(i);
		// 	$($('.totalsequence')[i]).html(i);
		// 	$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		// 		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
		// 	});
		// }
		// function slides(i){
		// 		$($('.totalsequence')[i]).click(function(){
		// 			countNext = i;
		// 			createjs.Sound.stop();
		// 			templateCaller();
		// 		});
		// 	}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

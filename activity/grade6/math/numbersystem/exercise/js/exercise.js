var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang + "/";
var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ditxt2,
			textclass: "title",
		}],
		imagedivblock:[{
				imagediv:"mainImgCont",
				svgblock:[{
					svgblock : "circlesvg",
				}]
			}]
	},

	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ditxt2,
			textclass: "title tit",
		}],
		extratextblock:[{
			textclass: "quest",
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"opti opti1",
				optaddclass:" ",
				optdata:data.string.dians1
			},{
					optcntextra:"opti opti2",
				optaddclass:"",
				optdata:data.string.dians2
			},{
				optcntextra:" opti opti3",
				optaddclass:"",
				optdata:data.string.dians3

			},{
					optcntextra:"opti opti4",
				optaddclass:"",
				optdata:data.string.dians4

			}]
		}]
	},
//slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ditxt3,
			textclass: "title newwidth",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: data.string.n6,
			divclass: "drag-item drag-1",
		},{
			textdata: data.string.n_2,
			divclass: "drag-item drag-2",
		},{
			textdata: data.string.n_8,
			divclass: "drag-item drag-3",
		},{
			textdata: data.string.n1,
			divclass: "drag-item drag-4",
		},{
			textdata: data.string.n3,
			divclass: "drag-item drag-5",
		}],
		dropdiv:[{
			dropdivcontainer:"dragContainer",
			dropbox:[{
				dropbox:"dropbox dropbox1",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox2",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox3",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox4",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox5",
				textclass:"box-drop",
			}]
		}],

	},
//slide 3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ditxt3,
			textclass: "title newwidth",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: data.string.n8,
			divclass: "drag-item drag-1",
		},{
			textdata: data.string.n_5,
			divclass: "drag-item drag-2",
		},{
			textdata: data.string.n_7,
			divclass: "drag-item drag-3",
		},{
			textdata: data.string.n_1,
			divclass: "drag-item drag-4",
		},{
			textdata: data.string.n4,
			divclass: "drag-item drag-5",
		}],
		dropdiv:[{
			dropdivcontainer:"dragContainer",
			dropbox:[{
				dropbox:"dropbox dropbox1 drp1",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox2 drp2",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox3 drp3",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox4 drp4",
				textclass:"box-drop",
			},{
				dropbox:"dropbox dropbox5 drp5",
				textclass:"box-drop",
			}]
		}],
	},
	//slide 4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ques11,
			textclass: "title tit",
		}],
		dropdiv:[{
			dropdivcontainer:"img1con",
		}],
		exetype2:[{
			optionsdivclss:"optidiv",
			exeoption:[{
				optcntextra:"correct",
				optaddclass:"opti opti1 ",
				optdata:""
			},{
				optcntextra:"ap2",
				optaddclass:"opti opti2",
				optdata:""
			},{
				optcntextra:"ap3",
				optaddclass:"opti opti3",
				optdata:""
			},{
				optcntextra:"ap4",
				optaddclass:"opti opti4",
				optdata:""
			}]
		}]
	},
	//slide 5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ques11,
			textclass: "title tit",
		}],
		dropdiv:[{
			dropdivcontainer:"img1con",
		}],
		exetype2:[{
			optionsdivclss:"optidiv",
			exeoption:[{
				optcntextra:"correct",
				optaddclass:"opti opti1 ",
				optdata:""
			},{
				optcntextra:"",
				optaddclass:"opti opti2",
				optdata:""
			},{
				optcntextra:"",
				optaddclass:"opti opti3",
				optdata:""
			},{
				optcntextra:"",
				optaddclass:"opti opti4",
				optdata:""
			}]
		}]
	},
	//slide 6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction lftlft',
		uppertextblock:[{
			textdata: data.string.question7,
			textclass: "title",
		}],
		diyblock:[{
			inputblock:[{

			}],
			diytxtblock:[{
				textclass: "check",
				textdata: data.string.check,
			}],
			svgblock:[{
				svgblock:"linesvg"
			}]
		}]

	},
	//slide 7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ditxt5,
			textclass: "title",
		}],
		extratextblock:[{
			textclass:"qxtn",
			textdata:data.string.ditxt6
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"underline",
			textclass:"bottm",
			textdata:data.string.ditxt7
		}],
		exetype2:[{
			optionsdivclss:"optidiv",
			exeoption:[{
				optcntextra:"correct",
				optaddclass:"opti opti1 ",
				optdata:data.string.yes
			},{
					optcntextra:"",
				optaddclass:"opti opti2",
				optdata:data.string.no
			}]
		}]
	},
	//slide 8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ditxt5,
			textclass: "title",
		}],
		extratextblock:[{
			textclass:"qxtn",
			textdata:data.string.ditxt8
		}],
		dropdiv:[{
			dropdivcontainer:"imgcntn",
		}],
		exetype2:[{
			optionsdivclss:"optidiv",
			exeoption:[{
				optcntextra:"",
				optaddclass:"opti opti1 ",
				optdata:data.string.dians1
			},{
					optcntextra:"",
				optaddclass:"opti opti2",
				optdata:data.string.dians2
			},
		{
			optcntextra:"",
		optaddclass:"opti opti3",
		optdata:data.string.dians4
		},{
			optcntextra:"correct",
		optaddclass:"opti opti4",
		optdata:data.string.dians3
		}]
		}]
	},
	//slide 9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.ditxt5,
			textclass: "title",
		}],
		extratextblock:[{
			textclass:"lstqnx",
			textdata:data.string.ditxt9
		}],
		lowertextblockadditionalclass:"divfornum",
		lowertextblock:[{
			textclass:"randum1",
			textdata:""
		},{
			textclass:"",
			textdata:""
		}],
		dropdiv:[{
			dropdivcontainer:"txt-box",
		},{
			dropdivcontainer:"squirrel",
		}],
		exetype2:[{
			optionsdivclss:"optidiv",
			exeoption:[{
				optcntextra:"opti opti1",
				optaddclass:" ",
				optdata:data.string.dians1
			},{
				optcntextra:"opti opti2",
				optaddclass:"",
				optdata:data.string.dians2
			},
		{
			optcntextra:"opti opti3",
		optaddclass:"",
		optdata:data.string.dians3
		},{
			optcntextra:"opti opti4",
		optaddclass:"",
		optdata:data.string.dians4
		}]
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var question_array = ['ditxt1a','ditxt1b','ditxt1c','ditxt1d'];
  var qno = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var lineNumArr = [1,2,3,4,5,6,7,8];
	lineNumArr.shufflearray();

	var score = 0;
	var scoring = new NumberTemplate();
	var wrng = false;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "bg-1", src: imgpath+"bg_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-3", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow1", src: imgpath+"q05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "txtbox", src: imgpath+"	text_b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow3", src: imgpath+"q08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"	squirrel.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lin1", src: imgpath+"	q05a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow03", src: imgpath+"arrow03.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "svgimg", src: imgpath+"circle.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"ex1_2.ogg"},
			{id: "sound_2", src: soundAsset+"ex1_3.ogg"},
			{id: "sound_4", src: soundAsset+"ex1_5.ogg"},
			{id: "sound_6", src: soundAsset+"ex1_7.ogg"},
			{id: "sound_7", src: soundAsset+"ex1_8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		setQues();
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
  function setQues(){
		qno =  randomGenerator(3);
		console.log("random number"+qno);
	}
	// random numberGenerator
	function randomGenerator(limit){
		var randNUm = Math.floor(Math.random() * (limit - 1 +1)) + 1;
		return randNUm;
	}
		// random numberGenerator
	function randomGenerator1(limit){
		var randNUm1 = Math.floor(Math.random() * (limit - 3 +1)) + 3;
		return randNUm1;
	}

	/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
// exeTemp.init(0);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);


		$board.html(html);
		texthighlight($board);
		/*generate question no at the beginning of question*/
		// exeTemp.numberOfQuestions(10);
		var wrngClicked = false;
		var multiCorrCounter = 0;
		var multiClick = 0;
		var svgtoload;

		/*generate question no at the beginning of question*/
		scoring.numberOfQuestions();

		switch(countNext) {
			case 0:
			console.log(">>>>>>>>>>>>"+question_array[qno]);
			console.log(">>>>>>>>>>>>"+qno);
			$('.title').html(eval("data.string."+question_array[qno]));
			randomGenerator();
			var s= Snap('#circlesvg');
			var svg = Snap.load(preload.getResult("svgimg").src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var i = 0;
					var j = 0;
					$(".option").hover(function(){
						    i++;
								$(this).css("cursor","pointer");
								var classname = $(this).addClass("pnt");
                var classname = $(this).attr("class");
								i>1?classname = $(this).attr("class"):"";
								var numberclass = i==1?classname.toString().split(" ")[0]:classname;
								switch(numberclass){
									case "natnum":
									$("#svg").find("g").not($(this)).not($(".natnum")).children().attr("fill","#0001");
									break;
									case "wholenum":
									$("#svg").find("g").not($(this)).not($(".natnum")).not($(".wholenum")).children().attr("fill","#0001");
									break;
									case "integer":
									$("#svg").find("g").not($(this)).not($(".natnum")).not($(".wholenum")).not($(".integer")).children().attr("fill","#0001");
									break;
									default:
									break;
								}
							},function(){
							 	templateCaller();
								i=0;
							});


          $(".option").click(function(){
						j++;
						var classname = $(this).attr("class");
						j>1?classname = $(this).attr("class"):"";
						var numberclass = j==1?classname.toString().split(" ")[0]:classname;
						j=0;
             if((numberclass=="natnum" && qno==0)||(numberclass=="wholenum" && qno==1)||(numberclass=="integer" && qno==2)||(numberclass=="rational" && qno==3)){
							 play_correct_incorrect_sound(1);
							$(" .option").css("pointer-events","none");
							 nav_button_controls(0);
						 		$(this).children(".corctopt").show(0);
								$(this).addClass('correct');
							 if(wrngClicked==false){
										 scoring.update(true);
							 }
							 $(this).unbind('mouseenter mouseleave');
						 }
						 else {
							 play_correct_incorrect_sound(0);
							 		$(this).children(".wrngopt").show(0);
									$(this).addClass('incorrect');
									$(this).css("pointer-events","none");
									wrngClicked=true;
						 }
					});
			});
						$(".instruction >p").prepend(countNext+1+". ");
			break;
			case 1:
			sound_player("sound_"+(countNext));
			randomize(".options-div");
			 qno =  randomGenerator(3);

		 question_array = ['dique1','dique2','dique3','dique4'];

			$('.quest').html(eval("data.string."+question_array[qno]));
			if(qno==0)
			{
				$(".opti1").addClass("correct");
			}else if (qno==1) {
				$(".opti2").addClass("correct");
			}else if (qno==2) {
				$(".opti3").addClass("correct");
			} else if (qno==3) {
				$(".opti4").addClass("correct");
			}
						$(".instruction >p").prepend(countNext+1+". ");
			break;

			case 2:
			case 3:
			sound_player("sound_"+(countNext));
			$(".dragContainer").append("<img class='imgCls' src='"+preload.getResult("arrow1").src+"'/>");
				var positions = [1,2,3,4,5];
				positions.shufflearray();
				for(var i=1; i<=6; i++){
					$('.drag-'+i).addClass('drag-pos-'+positions[i-1]);
				}
				var itemCount = 0;
				$('.drag-item').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.dropbox5').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-1');
					}
				});
				$('.dropbox2').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-2');
					}
				});
				$('.dropbox1').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-3');
					}
				});
				$('.dropbox3').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-4');
					}
				});
				$('.dropbox4').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-5');
					}
				});
				var count=0;
				function dropper(dropped, droppedOn, hasclass){
					if(dropped.hasClass(hasclass)){
						dropped.draggable('option', 'revert', false);
						play_correct_incorrect_sound(1);
						createjs.Sound.stop();
						itemCount++;
						dropped.draggable('disable');
						dropped.detach().css({
							'height': '100%',
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'border': '0.5vmin solid #cdeb58',
							'background-color': '#89d457',
							'pointer-events': 'none'
						}).appendTo(droppedOn);
						$(droppedOn).css({
							'border': 'none'
						});
						$(droppedOn).parents('img').show(0);
						if(itemCount==5){
							nav_button_controls(0);
							if(wrngClicked==false){
										scoring.update(true);
							}
						}
						createjs.Sound.stop();
					} else{
						createjs.Sound.stop();
						play_correct_incorrect_sound(0);
									wrngClicked=true;
					}
				}
						$(".instruction >p").prepend(countNext+1+". ");
				break;
				case 4:
				case 5:
				sound_player("sound_"+(countNext));
				randomize(".optidiv");
				$(".img1con").append("<img class='imgCls' src='"+preload.getResult("lin1").src+"'/>");
				$(".img1con").append("<div class='numcontainer'/>");

				boxNum	=	randomGenerator1(7);
				var lineNum  = randomGenerator(boxNum);

				$(".opti1").html((lineNum-1)+"/"+(boxNum-1));
				$(".opti2").html((lineNum+1)+"/"+(boxNum-1));
				$(".opti3").html((lineNum*2)+"/"+(boxNum-1));
				$(".opti4").html((lineNum+5)+"/"+(boxNum-1));
				 for(var i=1;i<=boxNum;i++){
						$(".numcontainer").append("<div class='box boxNum"+i+"'/>");

				 }
				 $(".boxNum"+lineNum).css("background-color","#f00 !important");
				$(".instruction >p").prepend(countNext+1+". ");
				$(".box:eq(0)").append("<p class='boxNumber'>"+0+"</p>");
				$(".box:eq("+(boxNum-1)+")").append("<p class='boxNumber'>"+(1)+"</p>");
				break;
				case 6:
				sound_player("sound_"+(countNext));
				var randNUm = Math.floor(Math.random()*(8-1+1) + 1);
				var s = Snap('#linesvg');
				var svg = Snap.load(preload.getResult("arrow03").src,function( loadedFragment){
					s.append(loadedFragment);
					$("#line-"+lineNumArr[0]).attr("stroke","#f00");
				});
				lineNumArr.splice(0,1);
				console.log(lineNumArr);
				$(".check").click(function(){
					var input1 = $(".input-1").val();
					var input2 = $(".input-2").val();
					$(".corincorimg").hide(0);
					$(".input-1,.input-2").removeClass("corClass");
					$(".input-1,.input-2").removeClass("incorClass");

					var numAns=  lineNumArr[0];
					console.log(numAns);
					checkAns (input1,input2,numAns,9);
				});
						$(".instruction >p").prepend(countNext+1+". ");
				break;
				case 7:
				sound_player("sound_"+(countNext));
				randomize(".optidiv");
				$(".instruction >p").prepend(countNext+1+". ");
				break;
				case 8:
				randomize(".optidiv");
				$(".imgcntn").append("<img class='clsimg' src='"+preload.getResult("arrow3").src+"'/>");
				$(".instruction >p").prepend(countNext+1+". ");
				break;

				case 9:
				randomize(".optidiv");
				$(".squirrel").append("<img class='squi' src='"+preload.getResult("squirrel").src+"'/>");
				$(".txt-box").append("<img class='txtb' src='"+preload.getResult("txtbox").src+"'/>");

				 qno =  randomGenerator(3);

			   question_array = ['dique_1','dique_2','dique_3','dique_4'];

				$('.divfornum').html(eval("data.string."+question_array[qno]));
				if(qno==0)
				{
					$(".opti1").addClass("correct");
				}else if (qno==1) {
					$(".opti2").addClass("correct");
				}else if (qno==2) {
					$(".opti3").addClass("correct");
				} else if (qno==3) {
					$(".opti4").addClass("correct");
				}
				$(".instruction >p").prepend(countNext+1+". ");
				break;

			default:
				nav_button_controls(0);
				break;
		}
		$(".options-container").click(function(){
			createjs.Sound.stop();
			if($(this).hasClass("correct")){
				$(this).children(".corctopt").show(0);
				play_correct_incorrect_sound(1);
				$(this).addClass('corrects');
				$(" .options-container").css("pointer-events","none");
				nav_button_controls(0);
				if(wrngClicked==false){
							scoring.update(true);
				}
				if(countNext==7){
					$(".bottm").show(0);
				}
			}
			else{
				$(this).children(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				$(this).addClass('incorrect');
				$(this).css("pointer-events","none");
				wrngClicked=true;
			}
		});

	}

	function checkAns (input1,input2,numrCor,denCor){
		createjs.Sound.stop();
		if(input1==numrCor){
			$(".input-1").addClass("corClass");
			if(input2==denCor){
				play_correct_incorrect_sound(1);
				$(".input-2").addClass("corClass");
				nav_button_controls(100);
				$(".corct").show();
				// alert("hi");
				if(wrng==false){
							scoring.update(true);
				}
			}else{
				$(".input-2").addClass("incorClass");
				play_correct_incorrect_sound(0);
				$(".wrng").show();
			wrng=true;
			}

		}else if (input2==denCor) {
			$(".input-2").addClass("corClass");
			$(".input-1").addClass("incorClass");
			play_correct_incorrect_sound(0);
			$(".wrng").show();
		wrng=true;
		}
		else {
			$(".input-1, .input-2").addClass("incorClass");
			play_correct_incorrect_sound(0);
			$(".wrng").show();
			wrng=true;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				// $prevBtn.show(0);
				$prevBtn.show(0);
				$nextBtn.show(0);
				// ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}


	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text23,
			textclass : 'lesson-title'
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p2text5,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p2text6,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p2text7,
			textclass : 'my_font_big fade_in_2 example'
		}],

	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p2text5,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p2text6,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p2text7,
			textclass : 'my_font_big  example'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big fade_in_3 example1'
		},
		{
			textdata : data.string.p2text10,
			textclass : 'my_font_big fade_in_3 example2'
		},
		{
			textdata : data.string.p2text11,
			textclass : 'my_font_big fade_in_3 example3'
		},
		{
			textdata : data.string.p2text12,
			textclass : 'my_font_big fade_in_3 example4'
		}],

	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p2text5,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p2text6,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p2text7,
			textclass : 'my_font_big  example'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big  example1'
		},
		{
			textdata : data.string.p2text10,
			textclass : 'my_font_big  example2'
		},
		{
			textdata : data.string.p2text11,
			textclass : 'my_font_big  example3'
		},
		{
			textdata : data.string.p2text12,
			textclass : 'my_font_big  example4'
		},
		{
			textdata : data.string.p2text13,
			textclass : 'my_font_big  monkeytext fade_in_2'
		}],
		imageblock :[{
		imagestoshow : [{
			imgclass : "monkey fade_in_2",
			imgsrc : imgpath + "expressions-01.png"
		},
		{
			imgclass : "cloud1 fade_in_2",
			imgsrc : imgpath + "cloud.png"
		}]
		}]

	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2 fade_in_2'
		},
		{
			textdata : data.string.p2text14,
			textclass : 'my_font_big descriptiontext fade_in_2'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimage fade_in_4",
			imgsrc : imgpath + "school_kids.png"
		},{
			imgclass : "bottomcenterimage fade_in_5",
			imgsrc : imgpath + "trangles01.png"
		},{
			imgclass : "bottomcenterimage7 fadein4",
			imgsrc : imgpath + "trangles02.png"
		},]
		}]


	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text14,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'P',
			textclass : 'my_font_big  fadein1 pclass'
		},
		{
			textdata : 'Q',
			textclass : 'my_font_big  fadein2 qclass'
		},
		{
			textdata : 'R',
			textclass : 'my_font_big  fadein3 rclass'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimage ",
			imgsrc : imgpath + "trangles02.png"
		},]
		}]


	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text14,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'P',
			textclass : 'my_font_big   pclass'
		},
		{
			textdata : 'Q',
			textclass : 'my_font_big   qclass'
		},
		{
			textdata : 'R',
			textclass : 'my_font_big   rclass'
		},
		{
			textdata : 'x cm',
			textclass : 'my_font_big   x1class fadein1'
		},
		{
			textdata : 'x cm',
			textclass : 'my_font_big   x2class fadein8'
		},
		{
			textdata : 'x cm',
			textclass : 'my_font_big   x3class fadein6'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimage1 ",
			imgsrc : imgpath + "trangles02.png"
		},
		{
			imgclass : "bottomcenterimage1 fadein1",
			imgsrc : imgpath + "temple02b.gif"
		},
		{
			imgclass : "bottomcenterimage1 fadein6",
			imgsrc : imgpath + "temple02a.gif"
		},
		{
			imgclass : "bottomcenterimage1 fadein8",
			imgsrc : imgpath + "temple02c.gif"
		}
		]
		}]


	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text14,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'P',
			textclass : 'my_font_big   pclass '
		},
		{
			textdata : 'Q',
			textclass : 'my_font_big   qclass'
		},
		{
			textdata : 'R',
			textclass : 'my_font_big   rclass'
		},
		{
			textdata : 'x cm',
			textclass : 'my_font_big   x1class '
		},
		{
			textdata : 'x cm',
			textclass : 'my_font_big   x2class '
		},
		{
			textdata : 'x cm',
			textclass : 'my_font_big   x3class '
		},
		{
			textdata : data.string.p2text15,
			textclass : 'my_font_big  example fade_in_2'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimage1 ",
			imgsrc : imgpath + "trangles02.png"
		},
		{
			imgclass : "bottomcenterimage1 ",
			imgsrc : imgpath + "temple02b.gif"
		},
		{
			imgclass : "bottomcenterimage1 ",
			imgsrc : imgpath + "temple02a.gif"
		},
		{
			imgclass : "bottomcenterimage1 ",
			imgsrc : imgpath + "temple02c.gif"
		}
		]
		}]


	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text16,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'X',
			textclass : 'my_font_big   xclass fade4s'
		},
		{
			textdata : 'Y',
			textclass : 'my_font_big   yclass fade4s'
		},
		{
			textdata : 'Z',
			textclass : 'my_font_big   zclass fade4s'
		},],
		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimage fade_in_4",
			imgsrc : imgpath + "hanger.png"
		},{
			imgclass : "bottomcenterimageodd fade4s",
			imgsrc : imgpath + "isosceles.png"
		}]
		}]

	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text16,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'X',
			textclass : 'my_font_big   xclass'
		},
		{
			textdata : 'Y',
			textclass : 'my_font_big   yclass'
		},
		{
			textdata : 'Z',
			textclass : 'my_font_big   zclass'
		},
		{
			textdata : 'a cm',
			textclass : 'my_font_big   a1class fadein1'
		},
		{
			textdata : 'a cm',
			textclass : 'my_font_big   a2class fadein8'
		},
		{
			textdata : 'b cm',
			textclass : 'my_font_big   b1class fadein6'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd ",
			imgsrc : imgpath + "isosceles.png"
		},
		{
			imgclass : "bottomcenterimageodd fadein1",
			imgsrc : imgpath + "isosceles02.gif"
		},
		{
			imgclass : "bottomcenterimageodd fadein6",
			imgsrc : imgpath + "isosceles01.gif"
		},
		{
			imgclass : "bottomcenterimageodd fadein8",
			imgsrc : imgpath + "isosceles03.gif"
		}
		]
		}]


	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text16,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'X',
			textclass : 'my_font_big   xclass'
		},
		{
			textdata : 'Y',
			textclass : 'my_font_big   yclass'
		},
		{
			textdata : 'Z',
			textclass : 'my_font_big   zclass'
		},
		{
			textdata : 'a cm',
			textclass : 'my_font_big   a1class'
		},
		{
			textdata : 'a cm',
			textclass : 'my_font_big   a2class'
		},
		{
			textdata : 'b cm',
			textclass : 'my_font_big   b1class'
		},
		{
			textdata : data.string.p2text17,
			textclass : 'my_font_big  example fade_in_2'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd ",
			imgsrc : imgpath + "isosceles.png"
		},
		{
			imgclass : "bottomcenterimageodd ",
			imgsrc : imgpath + "isosceles02.gif"
		},
		{
			imgclass : "bottomcenterimageodd",
			imgsrc : imgpath + "isosceles01.gif"
		},
		{
			imgclass : "bottomcenterimageodd",
			imgsrc : imgpath + "isosceles03.gif"
		}
		]
		}]


	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text18,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'K',
			textclass : 'my_font_big   kclass fade4s'
		},
		{
			textdata : 'L',
			textclass : 'my_font_big   lclass fade4s'
		},
		{
			textdata : 'M',
			textclass : 'my_font_big   mclass fade4s'
		},],
		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd2 fade_in_4",
			imgsrc : imgpath + "bread.png"
		},{
			imgclass : "bottomcenterimageodd1 fade4s",
			imgsrc : imgpath + "scalene.png"
		}]
		}]

	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text18,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'K',
			textclass : 'my_font_big   kclass '
		},
		{
			textdata : 'L',
			textclass : 'my_font_big   lclass '
		},
		{
			textdata : 'M',
			textclass : 'my_font_big   mclass '
		},
		{
			textdata : 'a cm',
			textclass : 'my_font_big   scalanesidename1 fadein8'
		},
		{
			textdata : 'b cm',
			textclass : 'my_font_big   scalanesidename2 fadein6'
		},
		{
			textdata : 'c cm',
			textclass : 'my_font_big   scalanesidename3 fadein1'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd1 ",
			imgsrc : imgpath + "scalene.png"
		},
		{
			imgclass : "bottomcenterimageodd1 fadein1",
			imgsrc : imgpath + "scalene01.gif"
		},
		{
			imgclass : "bottomcenterimageodd1 fadein6",
			imgsrc : imgpath + "scalene02.gif"
		},
		{
			imgclass : "bottomcenterimageodd1 fadein8",
			imgsrc : imgpath + "scalene03.gif"
		}
		]
		}]


	},
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text2'
		},
		{
			textdata : data.string.p2text18,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'K',
			textclass : 'my_font_big   kclass '
		},
		{
			textdata : 'L',
			textclass : 'my_font_big   lclass '
		},
		{
			textdata : 'M',
			textclass : 'my_font_big   mclass '
		},
		{
			textdata : 'a cm',
			textclass : 'my_font_big   scalanesidename1 '
		},
		{
			textdata : 'b cm',
			textclass : 'my_font_big   scalanesidename2 '
		},
		{
			textdata : 'c cm',
			textclass : 'my_font_big   scalanesidename3 '
		},
		{
			textdata : data.string.p2text19,
			textclass : 'my_font_big  example fade_in_2'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd1 ",
			imgsrc : imgpath + "scalene.png"
		},
		{
			imgclass : "bottomcenterimageodd1 ",
			imgsrc : imgpath + "scalene01.gif"
		},
		{
			imgclass : "bottomcenterimageodd1 ",
			imgsrc : imgpath + "scalene02.gif"
		},
		{
			imgclass : "bottomcenterimageodd1 ",
			imgsrc : imgpath + "scalene03.gif"
		}
		]
		}]


	},
	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : 'my_font_big  text3'
		},
		{
			textdata : data.string.p2text20,
			textclass : 'my_font_big descriptiontext2 fadein1'
		},
		{
			textdata : data.string.p2text21,
			textclass : 'my_font_big descriptiontext2 fadein2'
		},
		{
			textdata : data.string.p2text22,
			textclass : 'my_font_big descriptiontext2 fadein3'
		}],
		imageblockadditionalclass:'widthchange',
		imageblock :[{
		imagestoshow : [
		{
			imgclass : "rightimage fadein1",
			imgsrc : imgpath + "tranglesthreesides.png"
		},
		{
			imgclass : "rightimage fadein2",
			imgsrc : imgpath + "isoscelestwoside.png"
		},
		{
			imgclass : "rightimage fadein3",
			imgsrc : imgpath + "scalene.png"
		}
		]
		}]


	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext =0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			// soundsicon-orange
			{id: "sound_1", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_8", src: soundAsset+"s2_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s2_p9.ogg"},
			{id: "sound_11", src: soundAsset+"s2_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s2_p12.ogg"},
			{id: "sound_14", src: soundAsset+"s2_p14.ogg"},
			{id: "sound_15", src: soundAsset+"s2_p15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$('.side').hide(0);
		$('.angle').hide(0);

		switch (countNext) {
		case 3:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 4:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 5:
			sound_player_nav("sound_"+(countNext+1));
			nav_button_controls(3500);
			break;
		case 6:
			nav_button_controls(5500);
			break;
		case 8:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 9:
			nav_button_controls(5500);
			break;
		case 11:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 12:
			sound_player_nav("sound_"+(countNext+1));
			nav_button_controls(5500);
			break;
		case 12:
			nav_button_controls(3500);
			break;
		default:
			sound_player_nav("sound_"+(countNext+1));
			break;
		}
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

    $nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	// templateCaller();


	function table_resize(){
		$('td').height(0.12*$board.height());
	}

	$(window).resize(function() {
		table_resize();
	});
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text1,
			textclass : 'example normalfade1'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text2,
			textclass : 'example normalfade2'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p4text3,
			textclass : 'example normalfade3'
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'centwidth',
		uppertextblock : [
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text4,
			textclass : 'my_font_big polygonname'
		},
		{
			textdata : data.string.p1text1,
			textclass : 'my_font_big menu1'
		}],
		imageblock :[{
		imagestoshow : [{
			imgclass : "polygonplace ",
			imgsrc : imgpath + "triangle.png"
		}]
		}]

	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'centwidth',
		uppertextblock : [
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text6,
			textclass : 'my_font_big polygonname'
		},
		{
			textdata : data.string.p1text1,
			textclass : 'my_font_big menu1'
		},
		{
			textdata : data.string.p4text5,
			textclass : 'my_font_big menu2'
		}],
		imageblock :[{
		imagestoshow : [{
			imgclass : "polygonplace blurout",
			imgsrc : imgpath + "triangle.png"
		},{
			imgclass : "polygonplace blurin",
			imgsrc : imgpath + "quadrilateral.png"
		}]
		}]

	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'centwidth',
		uppertextblock : [
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text8,
			textclass : 'my_font_big polygonname'
		},
		{
			textdata : data.string.p1text1,
			textclass : 'my_font_big menu1'
		},
		{
			textdata : data.string.p4text5,
			textclass : 'my_font_big menu2'
		},
		{
			textdata : data.string.p4text7,
			textclass : 'my_font_big menu3'
		}],
		imageblock :[{
		imagestoshow : [{
			imgclass : "polygonplace blurout",
			imgsrc : imgpath + "quadrilateral.png"
		},{
			imgclass : "polygonplace blurin",
			imgsrc : imgpath + "pentagon.png"
		}]
		}]

	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'centwidth',
		uppertextblock : [
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text10,
			textclass : 'my_font_big polygonname'
		},
		{
			textdata : data.string.p1text1,
			textclass : 'my_font_big menu1'
		},
		{
			textdata : data.string.p4text5,
			textclass : 'my_font_big menu2'
		},
		{
			textdata : data.string.p4text7,
			textclass : 'my_font_big menu3'
		},
		{
			textdata : data.string.p4text9,
			textclass : 'my_font_big menu4'
		}],
		imageblock :[{
		imagestoshow : [{
			imgclass : "polygonplace blurout",
			imgsrc : imgpath + "pentagon.png"
		},{
			imgclass : "polygonplace blurin",
			imgsrc : imgpath + "hexagon.png"
		}]
		}]

	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'centwidth',
		uppertextblock : [
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text12,
			textclass : 'my_font_big polygonname'
		},
		{
			textdata : data.string.p1text1,
			textclass : 'my_font_big menu1'
		},
		{
			textdata : data.string.p4text5,
			textclass : 'my_font_big menu2'
		},
		{
			textdata : data.string.p4text7,
			textclass : 'my_font_big menu3'
		},
		{
			textdata : data.string.p4text9,
			textclass : 'my_font_big menu4'
		},
		{
			textdata : data.string.p4text11,
			textclass : 'my_font_big menu5'
		}],
		imageblock :[{
		imagestoshow : [{
			imgclass : "polygonplace blurout",
			imgsrc : imgpath + "hexagon.png"
		},
		{
			imgclass : "polygonplace blurin",
			imgsrc : imgpath + "heptagon.png"
		}]
		}]

	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'centwidth',
		uppertextblock : [
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text14,
			textclass : 'my_font_big polygonname octagontext'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text12,
			textclass : 'my_font_big polygonname heptagontext'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text10,
			textclass : 'my_font_big polygonname hexagontext'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text8,
			textclass : 'my_font_big polygonname pentagontext'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text6,
			textclass : 'my_font_big polygonname quadrilateraltext'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p4text4,
			textclass : 'my_font_big polygonname traingletext'
		},
		{
			textdata : data.string.p1text1,
			textclass : ' menu1'
		},
		{
			textdata : data.string.p4text5,
			textclass : ' menu2'
		},
		{
			textdata : data.string.p4text7,
			textclass : ' menu3'
		},
		{
			textdata : data.string.p4text9,
			textclass : ' menu4'
		},
		{
			textdata : data.string.p4text11,
			textclass : ' menu5'
		},
		{
			textdata : data.string.p4text13,
			textclass : ' menu6'
		},
		{
			textdata : data.string.p4text15,
			textclass : 'directioncss'
		}],

		imageblock :[{
			imagestoshow : [{
			imgclass : "polygonplace triangle",
			imgsrc : imgpath + "triangle.png"
		},{
			imgclass : "polygonplace quadrilateral",
			imgsrc : imgpath + "quadrilateral.png"
		},{

			imgclass : "polygonplace pentagon",
			imgsrc : imgpath + "pentagon.png"
		},{
			imgclass : "polygonplace hexagon",
			imgsrc : imgpath + "hexagon.png"
		},{
			imgclass : "polygonplace heptagon blurout",
			imgsrc : imgpath + "heptagon.png"
		},
		{
			imgclass : "polygonplace blurin octagon",
			imgsrc : imgpath + "octagon.png"
		}]
		}]

	},

];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext =0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			// soundsicon-orange
			{id: "sound_1", src: soundAsset+"s4_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s4_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s4_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s4_p7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);


		switch (countNext) {

		case 6:
			$nextBtn.hide(0);
			$('.triangle').hide(0);
			$('.quadrilateral').hide(0);
			$('.pentagon').hide(0);
			$('.hexagon').hide(0);
			$('.traingletext').hide(0);
			$('.quadrilateraltext').hide(0);
			$('.pentagontext').hide(0);
			$('.hexagontext').hide(0);
			$('.heptagontext').hide(0);
			var c=0;
			$('.menu1').hover(function(){
				$('.imageblock > img').removeClass('blurin').addClass('blurout');
				$('.textblock > .polygonname').removeClass('blurin').addClass('blurout');
				$('.triangle').addClass('blurin');
				$('.triangle').show(0);
				$('.traingletext').addClass('blurin');
				$('.traingletext').show(0);
				console.log(c);
				checkC();
			});
			$('.menu2').hover(function(){
				$('.imageblock > img').removeClass('blurin').addClass('blurout');
				$('.textblock > .polygonname').removeClass('blurin').addClass('blurout');
				$('.quadrilateral').addClass('blurin');
				$('.quadrilateral').show(0);
				$('.quadrilateraltext').addClass('blurin');
				checkC();
				console.log(c);
				$('.quadrilateraltext').show(0);
			});
			$('.menu3').hover(function(){
				$('.imageblock > img').removeClass('blurin').addClass('blurout');
				$('.textblock > .polygonname').removeClass('blurin').addClass('blurout');
				$('.pentagon').addClass('blurin');
				$('.pentagon').show(0);
				$('.pentagontext').addClass('blurin');
				checkC();
				console.log(c);
				$('.pentagontext').show(0);
			});
			$('.menu4').hover(function(){
				$('.imageblock > img').removeClass('blurin').addClass('blurout');
				$('.textblock > .polygonname').removeClass('blurin').addClass('blurout');
				$('.hexagon').addClass('blurin');
				$('.hexagon').show(0);
				$('.hexagontext').addClass('blurin');
				checkC();
				console.log(c);
				$('.hexagontext').show(0);
			});
			$('.menu5').hover(function(){
				$('.imageblock > img').removeClass('blurin').addClass('blurout');
				$('.textblock > .polygonname').removeClass('blurin').addClass('blurout');
				$('.heptagon').addClass('blurin');
				$('.heptagon').show(0);
				$('.heptagontext').addClass('blurin');
				checkC();
				console.log(c);
				$('.heptagontext').show(0);
			});
			$('.menu6').hover(function(){
				$('.imageblock > img').removeClass('blurin').addClass('blurout');
				$('.textblock > .polygonname').removeClass('blurin').addClass('blurout');
				$('.octagon').addClass('blurin');
				$('.octagon').show(0);
				$('.octagontext').addClass('blurin');
				checkC();
				console.log(c);
				$('.octagontext').show(0);
			});
			sound_player("sound_"+(countNext+1),true);
			break;

		default:
			sound_player_nav("sound_"+(countNext+1));
			current_sound.on("complete", function(){
				nav_button_controls(1000);
			});
            // nav_button_controls(1500);
			break;
		}

		function checkC(){
			c++;
			if(c>14)
			{
       			 ole.footerNotificationHandler.pageEndSetNotification();
			}
		}
	}

	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(100):"";
		});
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

    $nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	// templateCaller();


	function table_resize(){
		$('td').height(0.12*$board.height());
	}

	$(window).resize(function() {
		table_resize();
	});
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

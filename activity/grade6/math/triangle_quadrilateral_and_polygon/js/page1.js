var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "img1  fadeinn",
			imgsrc : imgpath + "scalene.png"
		},
		{
			imgclass : "img2  fadeinn1",
			imgsrc : imgpath + "quadrilateral.png"
		},
		{
			imgclass : "img3 fadeinn2",
			imgsrc : imgpath + "pentagon.png"
			
		},
		{
			imgclass : "img4 fadeinn3",
			imgsrc : imgpath + "hexagon.png"
			
		},
		{
			imgclass : "img5 fadeinn4",
			imgsrc : imgpath + "heptagon.png"
		},
		{
			imgclass : "img6 fadeinn5",
			imgsrc : imgpath + "octagon.png"
		}]
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big fade_in_2 example'
		}],

	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'my_font_big fade_in_1 example'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "trianglecss fadein1 ",
			imgsrc : imgpath + "matchstick01.png"
		},
		{
			imgclass : "trianglecss1 fadein2 ",
			imgsrc : imgpath + "matchstick02.png"
		},
		{
			imgclass : "trianglecss2 fadein3",
			imgsrc : imgpath + "matchstick03.png"
		}]
		}]

	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'my_font_big fade_in_1 example'
		}],
		imageblock :[{
		imagestoshow : [{
			imgclass : "trianglecss",
			imgsrc : imgpath + "matchstick03.png"
		}]
		}]

	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text6,
			textclass : 'my_font_big fade_in_1 example'
		},
		{
			textdata : data.string.p1text7,
			textclass : 'Aclass fadein1'
		},
		{
			textdata : 'B',
			textclass : 'Bclass fadein2'
		},
		{
			textdata : 'C',
			textclass : 'Cclass fadein3'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "trianglecss",
			imgsrc : imgpath + "matchstick03.png"
		},]
		}]


	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text6,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p1text7,
			textclass : 'Aclass '
		},
		{
			textdata : 'B',
			textclass : 'Bclass '
		},
		{
			textdata : 'C',
			textclass : 'Cclass '
		},
		{
			textdata : data.string.sides,
			textclass : 'button'
		},
		{
			textdata : data.string.angle,
			textclass : 'button1'
		},
		{
			textdata : data.string.p1text9,
			textclass : 'side'
		},
		{
			textdata : data.string.p1text10,
			textclass : 'angle'
		},
		{
			textdata : data.string.p1text11,
			textclass : 'hoverinstruction'
		}],
		arcdraw :[{
		}],
		imageblock :[{
		imagestoshow : [{
			imgclass : "trianglecss",
			imgsrc : imgpath + "matchstick03.png"
		},]
		}]


	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p1text12,
			textclass : 'my_font_big  dialogtext'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "monkey",
			imgsrc : imgpath + "expressions-01.png"
		},{
			imgclass : "cloud",
			imgsrc : imgpath + "cloud.png"
		}]
		}]

	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'nightsky opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p1text13,
			textclass : 'my_font_ultra_big toptitle'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "flagcss ",
			imgsrc : imgpath + "flag.png"
		},{
			imgclass : "firstpart blink_me1",
			imgsrc : imgpath + "flag01.png"
		},{
			imgclass : "secondpart blink_me2",
			imgsrc : imgpath + "flag02.png"
		},]
		}]


	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p1text14,
			textclass : 'my_font_ultra_big  toptitle'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "imagebackground nightsky",
			imgsrc : imgpath + "house01.png"
		},{
			imgclass : "imagebackground blink_me2",
			imgsrc : imgpath + "house02.png"
		},{
			imgclass : "imagebackground blink_me2",
			imgsrc : imgpath + "house03.png"
		},
		{
			imgclass : "imagebackground blink_me2",
			imgsrc : imgpath + "house04.png"
		},{
			imgclass : "imagebackground blink_me2",
			imgsrc : imgpath + "house05.png"
		},]
		}]


	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p1text14,
			textclass : 'my_font_ultra_big title toptitle'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "imagebackground",
			imgsrc : imgpath + "nepali_house.jpg"
		},{
			imgclass : "imagebackground blink_me2",
			imgsrc : imgpath + "nepali_house01.png"
		}]
		}]


	},
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p1text15,
			textclass : 'my_font_ultra_big title toptitle'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "imagebackground",
			imgsrc : imgpath + "pyramid01.jpg"
		},{
			imgclass : "imagebackground blink_me2",
			imgsrc : imgpath + "pyramid01a.png"
		},{
			imgclass : "imagebackground blink_me2",
			imgsrc : imgpath + "pyramid01b.png"
		}]
		}]


	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

		var preload;
		var current_sound;
		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images

				// soundsicon-orange
				{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
				{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
				{id: "sound_3", src: soundAsset+"s1_p3.ogg"},
				{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
				{id: "sound_5", src: soundAsset+"s1_p5.ogg"},
				{id: "sound_6", src: soundAsset+"s1_p6.ogg"},
				{id: "sound_7", src: soundAsset+"s1_p7.ogg"},
				{id: "sound_8", src: soundAsset+"s1_p8.ogg"},
				{id: "sound_9", src: soundAsset+"s1_p9.ogg"},
				{id: "sound_10", src: soundAsset+"s1_p10.ogg"},
				{id: "sound_11", src: soundAsset+"s1_p11.ogg"},
				{id: "s1_p6_hover", src: soundAsset+"s1_p6_hover.ogg"},
			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$('.side').hide(0);
		$('.angle').hide(0);

		switch (countNext) {
		case 0:
			sound_player_nav("sound_"+(countNext+1));
			// nav_button_controls(6000);
			break;
		case 1:
			sound_player_nav("sound_"+(countNext+1));
			// nav_button_controls(1000);
			break;
		case 2:
			sound_player_nav("sound_"+(countNext+1));
			// nav_button_controls(3000);
			break;
		case 3:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 4:
			sound_player_nav("sound_"+(countNext+1));
			// nav_button_controls(3000);
			break;
		case 5:
			$nextBtn.hide(0);
			var c = 0;
			$('.button').mouseenter(function()
				{
					$('.side').show(0);
					$('.trianglecss').addClass('zoomin');
				});
			$('.button').mouseleave(function(){
					$('.side').hide(0);
					$('.trianglecss').removeClass('zoomin');
			});
			$('.button1').mouseenter(function()
				{

					$('.angle').show(0);
					$('.arc1,.arc2,.arc3').addClass('drawarc');
					$('.Aclass,.Bclass,.Cclass').addClass('zoomin');
				});
			$('.button1').mouseleave(function(){
					$('.angle').hide(0);
					$('.arc1,.arc2,.arc3').removeClass('drawarc');
					$('.Aclass,.Bclass,.Cclass').removeClass('zoomin');
					$('.hoverinstruction').hide(0);
					$nextBtn.show(0);
			});
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_6");//s1_p6_hover
				current_sound.play();
			break;
		case 6:
			sound_player_nav("sound_"+(countNext+1));
		break;
		case 7:
		case 8:
		case 9:
			sound_player_nav("sound_"+(countNext+1));
			table_resize();
			break;
		case 10:
			sound_player_nav("sound_"+(countNext+1));
		break;
		default:
			nav_button_controls(1500);
			break;
		}
	}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}
		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				if(typeof click_class != 'undefined'){
					$(click_class).click(function(){
						current_sound.play();
					});
				}
				nav_button_controls(1000);
			});
		}
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

  $nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});



	total_page = content.length;
	// templateCaller();


	function table_resize(){
		$('td').height(0.12*$board.height());
	}

	$(window).resize(function() {
		table_resize();
	});
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

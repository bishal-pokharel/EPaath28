var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

  //slide0
  {
    hasheaderblock : false,
    contentblocknocenteradjust : true,
    contentblockadditionalclass : 'opalbackground',

    uppertextblockadditionalclass: ' my_font_big center-text',
    uppertextblock : [{
      datahighlightflag : true,
      datahighlightcustomclass : 'green',
      textdata : data.string.p5textheading,
      textclass : ''
    }]
  },

  //slide1
  {
      hasheaderblock : false,
      contentblocknocenteradjust : true,
      contentblockadditionalclass : 'opalbackground',

      uppertextblockadditionalclass: 'shop-text',
      uppertextblock: [

        {
          datahighlightflag : true,
          datahighlightcustomclass : 'green',
          textdata: data.string.p5text1,
          textclass: 'quitrangleheading'
        },
        {
         textdata : data.string.p5text2,
         textclass : 'my_font_big instuction1 example'
        },
        {
         textdata : data.string.p5text3,
         textclass : 'my_font_big instuction2 example'
        },
        {
         textdata : data.string.p5text4,
         textclass : 'my_font_big instuction3 example'
        },
        {
         textdata : data.string.p5text6,
         textclass : 'my_font_big instuction4 example'
        },
        {
         textdata : data.string.p5text5,
         textclass : 'my_font_big instuction5 example'
        },
        {
         textdata : data.string.p5text7,
         textclass : 'my_font_big instuction6 example'
        },
        {
         textdata : data.string.p5text8,
         textclass : 'my_font_big instuction7 example '
        },
        {
         textdata : data.string.p5text9,
         textclass : 'my_font_big instuction8 example'
        },

      ],
     fourthlineadditionalclass:'fourthline1',
     seconddotadditionalclass:'seconddot1',
      arc3stadditionalclass:'arcthird1',
     imageblock :[{
      imagestoshow : [

      {
      imgclass : "pencil",
      imgsrc : imgpath + "pencil1.png"
       },

       {
      imgclass : "scale",
      imgsrc : imgpath + "scale.png"
      },

       {
      imgclass : "compass",
      imgsrc : imgpath + "compas.png"
      }]
      }],
      lowertextblockadditionalclass:'absolutely',
      lowertextblock:[
        {
          textdata:'A',
          textclass:'name1'
        },
        {
          textdata:'B',
          textclass:'name2'
        },
        {
          textdata:'C',
          textclass:'name3'
        }
      ]


    },
    //slide2
  {
      hasheaderblock : false,
      contentblocknocenteradjust : true,
      contentblockadditionalclass : 'opalbackground',

      uppertextblockadditionalclass: 'shop-text',
      uppertextblock: [

        {
          datahighlightflag : true,
          datahighlightcustomclass : 'green',
          textdata: data.string.p5text11,
          textclass: 'quitrangleheading'
        },
        {
         textdata : data.string.p5text12,
         textclass : 'my_font_big instuction1 example'
        },
        {
         textdata : data.string.p5text13,
         textclass : 'my_font_big instuction2 example'
        },
        {
         textdata : data.string.p5text14,
         textclass : 'my_font_big instuction3 example'
        },
        {
         textdata : data.string.p5text15,
         textclass : 'my_font_big instuction4 example'
        },
        {
         textdata : data.string.p5text16,
         textclass : 'my_font_big instuction5 example'
        },
        {
         textdata : data.string.p5text17,
         textclass : 'my_font_big instuction6 example'
        },
        {
         textdata : data.string.p5text18,
         textclass : 'my_font_big instuction7 example '
        }
      ],
      arc1stadditionalclass:'arcfirst',
      arc2stadditionalclass:'arcsecond',
      arc3stadditionalclass:'arcthird',
      secondlineadditionalclass:'secondline',
      thirdlineadditionalclass:'thirdline',
      fourthlineadditionalclass:'fourthline',
      seconddotadditionalclass:'second-dot',

      imageblock :[{
      imagestoshow : [

      {
      imgclass : "pencil1",
      imgsrc : imgpath + "pencil1.png"
       },
       {
      imgclass : "scale1",
      imgsrc : imgpath + "scale.png"
      },
      {
      imgclass : "compass1",
      imgsrc : imgpath + "compas.png"
      },
      {
      imgclass : "protractor",
      imgsrc : imgpath + "protractor.png"
      }]
      }],

      lowertextblockadditionalclass:'absolutely',
      lowertextblock:[
        {
          textdata:'A',
          textclass:'name11'
        },
        {
          textdata:'B',
          textclass:'name22'
        },
        {
          textdata:'C',
          textclass:'name33'
        },
        {
          textdata:'D',
          textclass:'name44'
        }
      ]


    },



];

$(function() {

  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext =0;
  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
	var preload;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			// soundsicon-orange
			{id: "sound_1", src: soundAsset+"s5_p1.ogg"},
      {id: "sound_2_0", src: soundAsset+"s5_p2-01.ogg"},
      {id: "sound_2_1", src: soundAsset+"s5_p2-02.ogg"},
      {id: "sound_2_2", src: soundAsset+"s5_p2-03.ogg"},
      {id: "sound_2_3", src: soundAsset+"s5_p2-04.ogg"},
      {id: "sound_2_4", src: soundAsset+"s5_p2-05.ogg"},
      {id: "sound_2_5", src: soundAsset+"s5_p2-06.ogg"},
      {id: "sound_2_6", src: soundAsset+"s5_p2-07.ogg"},
      {id: "sound_2_7", src: soundAsset+"s5_p2-08.ogg"},
			{id: "sound_2_8", src: soundAsset+"s5_p2-09.ogg"},

      {id: "sound_3_0", src: soundAsset+"s5_p3-01.ogg"},
      {id: "sound_3_1", src: soundAsset+"s5_p3-02.ogg"},
      {id: "sound_3_2", src: soundAsset+"s5_p3-03.ogg"},
      {id: "sound_3_3", src: soundAsset+"s5_p3-04.ogg"},
      {id: "sound_3_4", src: soundAsset+"s5_p3-05.ogg"},
      {id: "sound_3_5", src: soundAsset+"s5_p3-06.ogg"},
      {id: "sound_3_6", src: soundAsset+"s5_p3-07.ogg"},
      {id: "sound_3_7", src: soundAsset+"s5_p3-08.ogg"},
			// {id: "sound_3_8", src: soundAsset+"s5_p3-09.ogg"},
			{id: "sound_3", src: soundAsset+"s5_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

  /*
   inorder to use the handlebar partials we need to register them
   to their respective handlebar partial pointer first
   */
  Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // controls the navigational state of the program
  // next btn is disabled for this page
  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
    // if lastpageflag is true
    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);

    $board.html(html);

    loadTimelineProgress($total_page, countNext + 1);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
     vocabcontroller.findwords(countNext);
    $('.side').hide(0);
    $('.angle').hide(0);

    // function sndInstrn(itn,total){
    //     createjs.Sound.stop();
    //     current_sound = createjs.Sound.play("sound_"+(countNext+1)+"_"+itn);//sound_2_1
    //     $(".instuction"+itn).fadeIn();
    //     current_sound.play();
    //     current_sound.on('complete',function(){
    //       itn+=1;
    //       if(itn<=total){
    //         sndInstrn(itn,total);
    //       }else{
    //         nav_button_controls(1000);
    //       }
    //     });
    // }

    switch (countNext) {
    case 0:
    createjs.Sound.stop();
    current_sound = createjs.Sound.play("sound_1");
    current_sound.play();
    current_sound.on('complete', function(){
      nav_button_controls();
    });
    break;
    case 1:
        sound_player_nav("sound_"+(countNext+1));
        $(".my_font_big").hide(0);
        createjs.Sound.stop();
        current_sound = createjs.Sound.play("sound_2_0");
        current_sound.play();
        current_sound.on('complete',function(){
          // sndInstrn(1,8);
          var current_sound1 = createjs.Sound.play("sound_2_1");
          current_sound1.play();
          $(".instuction1").fadeIn();
          $('.scale').addClass('making_firstline');
          current_sound1.on('complete',function(){
            $('.scale').removeClass('making_firstline');
            var current_sound2 = createjs.Sound.play("sound_2_2");
            current_sound2.play();
            $(".instuction2").fadeIn();
            $('.compass').addClass('measurecompass');
            current_sound2.on('complete',function(){
              var current_sound3 = createjs.Sound.play("sound_2_3");
              current_sound3.play();
              $(".instuction3").fadeIn();
              current_sound3.on('complete',function(){
                var current_sound4 = createjs.Sound.play("sound_2_4");
                current_sound4.play();
                $(".instuction4").fadeIn();
                $('.compass').addClass('rotatecompass');
                $(".arc").delay(1000).animate({'opacity':'1'},1000);
                current_sound4.on('complete',function(){
                  $(".instuction5").fadeIn();
                  var current_sound5 = createjs.Sound.play("sound_2_5");
                  current_sound5.play();
                  $(".arc2").delay(1000).addClass('show_arc');
                  $('.compass').addClass('rotateagain');
                  current_sound5.on('complete',function(){
                    $('.compass').addClass('moveback');
                    $(".instuction6").fadeIn();
                    var current_sound6 = createjs.Sound.play("sound_2_6");
                    current_sound6.play();
                    $('.name3').delay(1000).animate({'opacity':'1'},1000);
                    current_sound6.on('complete',function(){
                      $(".instuction7").fadeIn();
                      var current_sound7 = createjs.Sound.play("sound_2_7");
                      current_sound7.play();
                      current_sound7.on('complete',function(){
                        $('.second-line').addClass('draw2');
                        $('.pencil').addClass('pencil2');
                        $('.scale').addClass('making_secondline');

                        setTimeout(function(){
                          $('.pencil').addClass('pencil3');
                          $('.third-line').addClass('draw3');
                          $('.scale').addClass('making_thirdline');
                          $(".instuction8").delay(4000).fadeIn(1000,function(){
                            var current_sound8 = createjs.Sound.play("sound_2_8");
                            current_sound8.play();
                              current_sound8.on('complete',function(){
                                $('.pencil,.scale').animate({'opacity':'0'},1000);
                                nav_button_controls(100);
                            });
                          });
                        },2000);

                      });
                    });
                  });
                });
              });
            });
          });
        });
      break;
    case 2:
        $(".my_font_big").hide(0);
        setTimeout(function(){
          $('.pencil1').addClass('pencil11');
        },2600);
        createjs.Sound.stop();
        current_sound = createjs.Sound.play("sound_3_0");
        current_sound.play();
        current_sound.on('complete',function(){
          $('.instuction1').fadeIn(1000);
          var current_sound1 = createjs.Sound.play("sound_3_1");
          current_sound1.play();
          current_sound1.on('complete',function(){
            $('.scale1').animate({'opacity':'0'},1000);;
            $('.instuction2').fadeIn(1000);
            var current_sound2 = createjs.Sound.play("sound_3_2");
            current_sound2.play();
            $('.protractor').addClass('protector_anim');
            $('.pencil1').addClass('pencil222');
            setTimeout(function(){
              $('.protractor').removeClass('protector_anim');
              $('.pencil1').removeClass('pencil222 pencil11').addClass('pencil22');
              $('.second-line').addClass('draw22');
              $('.scale1').addClass('making_secondline1');
            },1500);
            $('.second-dot').delay(1000).animate({'opacity':'1'},1000);
            current_sound2.on('complete',function(){
              $('.pencil1').removeClass('pencil22');
              $('.compass1').removeClass('measurecompass1 rotatecompass1');
              $('.instuction3').fadeIn(1000);
              var current_sound3 = createjs.Sound.play("sound_3_3");
              current_sound3.play();
              $('.compass1').addClass('measurecompass1');
              setTimeout(function(){
                $('.compass1').addClass('rotatecompass1');
                $('.arcfirst').delay(500).animate({'opacity':'1'},1000);
              },1000);
              $('.name44').delay(2500).animate({'opacity':'1'},1000);
              current_sound3.on('complete',function(){
                $('.instuction4').fadeIn(1000);
                var current_sound4 = createjs.Sound.play("sound_3_4");
                current_sound4.play();
                $('.arcsecond').delay(800).animate({'opacity':'1'},500);
                $('.compass1').removeClass('measurecompass1 rotatecompass1').addClass('movetopointd');
                  // sndInstrn(1,7);
                  current_sound4.on('complete',function(){
                    $('.compass1').removeClass('movetopointd');
                    $('.instuction5').fadeIn(1000);
                    var current_sound5 = createjs.Sound.play("sound_3_5");
                    current_sound5.play();
                    $('.compass1').addClass('movetopointb');
                    $('.arcthird').delay(800).animate({'opacity':'1'},500);
                    $('.name33').delay(1300).animate({'opacity':'1'},500);
                    current_sound5.on('complete',function(){
                      $('.compass1').removeClass('movetopointb');
                      $('.instuction6').fadeIn(1000);
                      var current_sound6 = createjs.Sound.play("sound_3_6");
                      current_sound6.play();
                      $('.pencil1').addClass('pencil33');
                      $('.thirdline').addClass('draw33');
                      $('.scale1').addClass('making_thirdline1');
                      setTimeout(function(){
                        $('.pencil1').removeClass('pencil33').addClass('pencil44');
                        $('.fourthline').removeClass('draw33').addClass('draw33');
                        $('.scale1').removeClass('making_thirdline1 making_secondline1').css('opacity','1').addClass('making_fourthline1');
                      },3200);
                      current_sound6.on('complete',function(){
                        $('.instuction7').fadeIn(1000);
                        var current_sound7 = createjs.Sound.play("sound_3_7");
                        current_sound7.play();
                        current_sound7.on('complete',function(){
                          $('.scale1,.pencil1').css('opacity','0');
                          nav_button_controls(100);
                        });
                      });
                    });
                  });
                });
            });
          });
        });
      // nav_button_controls(25500);
      break;
    default:
      sound_player_nav("sound_"+(countNext+1));
      break;
    }
  }

  function sound_player(sound_id){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }
  function sound_player_nav(sound_id){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function(){
      if(typeof click_class != 'undefined'){
        $(click_class).click(function(){
          current_sound.play();
        });
      }
      nav_button_controls(1000);
    });
  }

  function nav_button_controls(delay_ms){
    setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.lessonEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }

  function templateCaller() {
    //convention is to always hide the prev and next button and show them based
    //on the convention or page index
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();

    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

  }

    $nextBtn.on("click", function() {
    switch(countNext){
      default:
        countNext++;
        templateCaller();
        break;
    }

  });

  $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
    countNext--;
    templateCaller();
  });

  total_page = content.length;
  // templateCaller();


  function table_resize(){
    $('td').height(0.12*$board.height());
  }

  $(window).resize(function() {
    table_resize();
  });
  /** function to check the key pressed is a valid number(1-9 and .) for the input box or not
   * event.key reurns the value of key pressed by user and it is converted to integer
   * event.target gets the element where event is occuring (usually a div)
   * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
   * input_class and button_classes should be something like '.class_name'
   * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
   */
  function input_box(input_class, max_number, button_class) {
    $(input_class).keydown(function(event){
        var charCode = (event.which) ? event.which : event.keyCode;
        /* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
        if(charCode === 13 && button_class!=null) {
            $(button_class).trigger("click");
      }
      var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
      //check if user inputs del, backspace or arrow keys
        if (!condition) {
          return true;
        }
        //check if user inputs more than one '.'
      if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
            return false;
        }
        //check . and 0-9 separately after checking arrow and other keys
        if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
          return false;
        }
        //check max no of allowed digits
        if (String(event.target.value).length >= max_number) {
          return false;
        }
        return true;
    });
  }


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
       use that or else use default 'parsedstring'*/
      $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
      ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

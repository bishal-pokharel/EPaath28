var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p3text13,
			textclass : 'lesson-title'
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p2text10,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p2text11,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p2text12,
			textclass : 'my_font_big fade_in_2 example'
		}],

	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p3text1,
			textclass : 'my_font_big descriptiontext fade_in_2'
		},
		{
			textdata : 'D',
			textclass : 'my_font_big  fadein2 pclass'
		},
		{
			textdata : 'E',
			textclass : 'my_font_big  fadein2 qclass'
		},
		{
			textdata : 'F',
			textclass : 'my_font_big  fadein2 rclass'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd4 fade_in_4",
			imgsrc : imgpath + "tranglesear.png"
		},{
			imgclass : "bottomcenterimage fadein2",
			imgsrc : imgpath + "90digreethreeside.png"
		}]
		}]


	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p3text1,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'D',
			textclass : 'my_font_big   pclassx'
		},
		{
			textdata : 'E',
			textclass : 'my_font_big   qclassx'
		},
		{
			textdata : 'F',
			textclass : 'my_font_big   rclassx'
		},{
			textdata : data.string.p3text2,
			textclass : 'my_font_big  example fade_in_2 smalltextforsmallscreen'
		}],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimagex ",
			imgsrc : imgpath + "90digreethreeside.png"
		}]
		}]


	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p3text3,
			textclass : 'fnt3hlf descriptiontext fade_in_2'
		},
		{
			textdata : 'K',
			textclass : 'my_font_big  fadein2 k2class'
		},
		{
			textdata : 'J',
			textclass : 'my_font_big  fadein2 j2class'
		},
		{
			textdata : 'L',
			textclass : 'my_font_big  fadein2 l2class'
		},
		],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd6 fade_in_4",
			imgsrc : imgpath + "agnle01.png"
		},{
			imgclass : "bottomcenterimageodd6 fadein2",
			imgsrc : imgpath + "90digree.png"
		}]
		}]


	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p3text3,
			textclass : 'fnt3hlf descriptiontext '
		},
		{
			textdata : 'K',
			textclass : 'my_font_big   k2class'
		},
		{
			textdata : 'J',
			textclass : 'my_font_big   j2class'
		},
		{
			textdata : 'L',
			textclass : 'my_font_big   l2class'
		},
		{
			textdata : data.string.p3text4,
			textclass : 'my_font_big  example fade_in_2 smalltextforsmallscreen'
		}],


		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd6 ",
			imgsrc : imgpath + "90digree.png"
		}]
		}]


	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big fade_in_2 example'
		},
		{
			textdata : data.string.p3text5,
			textclass : 'my_font_big descriptiontext fade_in_2'
		},
		{
			textdata : 'S',
			textclass : 'my_font_big  fadein2 s2class'
		},
		{
			textdata : 'R',
			textclass : 'my_font_big  fadein2 r2class'
		},
		{
			textdata : 'T',
			textclass : 'my_font_big  fadein2 t2class'
		},
		],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd7 fade_in_4",
			imgsrc : imgpath + "hanger.png"
		},{
			imgclass : "bottomcenterimageodd8 fadein2",
			imgsrc : imgpath + "90digreeontop.png"
		}]
		}]


	},

	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_ultra_big title'
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p2text8,
			textclass : 'my_font_big  example'
		},
		{
			textdata : data.string.p3text5,
			textclass : 'my_font_big descriptiontext '
		},
		{
			textdata : 'S',
			textclass : 'my_font_big   s2class'
		},
		{
			textdata : 'R',
			textclass : 'my_font_big   r2class'
		},
		{
			textdata : 'T',
			textclass : 'my_font_big   t2class'
		},
		{
			textdata : data.string.p3text12,
			textclass : 'my_font_big  example fade_in_2 smalltextforsmallscreen'
		}
		],

		imageblock :[{
		imagestoshow : [{
			imgclass : "bottomcenterimageodd8 ",
			imgsrc : imgpath + "90digreeontop.png"
		}]
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'opalbackground',

		uppertextblockadditionalclass: 'shop-text centwidth textsmall',
		uppertextblock : [
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'blue',
			textdata : data.string.p2text4,
			textclass : '  text3'
		},
		{
			textdata : data.string.p2text20,
			textclass : 'smallerfont descriptiontext2 normalfade1 '
		},
		{
			textdata : data.string.p2text21,
			textclass : 'smallerfont descriptiontext2 normalfade2 '
		},
		{
			textdata : data.string.p2text22,
			textclass : 'smallerfont descriptiontext2 normalfade3 '
		},
		{
			datahighlightflag : true,
			datahighlightcustomclass : 'green',
			textdata : data.string.p3text13,
			textclass : 'my_font_big angletitle text4'
		},
		{
			textdata : data.string.p3text10,
			textclass : 'smallerfont descriptiontext3 normalfade1'
		},
		{
			textdata : data.string.p3text7,
			textclass : 'smallerfont descriptiontext3 normalfade2'
		},
		{
			textdata : data.string.p3text9,
			textclass : 'smallerfont descriptiontext3 normalfade3'
		}],
		imageblockadditionalclass:'widthchange',
		imageblock :[{
		imagestoshow : [
		{
			imgclass : "rightimage normalfade1 ",
			imgsrc : imgpath + "tranglesthreesides.png"
		},
		{
			imgclass : "rightimage normalfade2  ",
			imgsrc : imgpath + "isoscelestwoside.png"
		},
		{
			imgclass : "rightimage normalfade3 ",
			imgsrc : imgpath + "scalene.png"
		},
		{
			imgclass : "rightimage1  normalfade1",
			imgsrc : imgpath + "90digreethreeside.png"
		},
		{
			imgclass : "rightimage1  normalfade2",
			imgsrc : imgpath + "90digree.png"
		},
		{
			imgclass : "rightimage1  normalfade3",
			imgsrc : imgpath + "90digreeontop.png"
		}
		]
		}]


	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext =0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			// soundsicon-orange
			{id: "sound_1", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$('.side').hide(0);
		$('.angle').hide(0);

		switch (countNext) {
		case 4:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 5:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 6:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 8:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 9:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 11:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 12:
			sound_player_nav("sound_"+(countNext+1));
			break;
		case 12:
			sound_player_nav("sound_"+(countNext+1));
			break;
		default:
			sound_player_nav("sound_"+(countNext+1));
			break;
		}
	}


	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

    $nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	// templateCaller();


	function table_resize(){
		$('td').height(0.12*$board.height());
	}

	$(window).resize(function() {
		table_resize();
	});
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

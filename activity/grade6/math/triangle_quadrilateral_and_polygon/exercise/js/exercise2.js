var imgpath = $ref+"/exercise/images/";

var content2=[

	//ex1
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						optdata: data.string.e2_1,
					},
					{
						optid: "2",
						optdata: data.string.e2_2,
					},
					{
						optid: "3",
						optdata: data.string.e2_3,
					},
					{
						optid: "4",
						optdata: data.string.e2_4,
					},
					{
						optid: "5",
						optdata: data.string.e2_5,
					},
					{
						optid: "6",
						optdata: data.string.e2_6,
					},
					{
						optid: "7",
						optdata: data.string.e2_7,
					}
				],
			}
		],
	},
	//ex2
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ains,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						optdata: data.string.e2_a1,
					},
					{
						optid: "2",
						optdata: data.string.e2_a2,
					},
					{
						optid: "3",
						optdata: data.string.e2_a3,
					},
					{
						optid: "4",
						optdata: data.string.e2_a4,
					},
					{
						optid: "5",
						optdata: data.string.e2_a5,
					},
					{
						optid: "6",
						optdata: data.string.e2_a6,
					},
					{
						optid: "7",
						optdata: data.string.e2_a7,
					}
				],
			}
		],
	},
];

/*remove this for non random questions*/
content2.shufflearray();
var content = [content2[0]];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	/*for limiting the questions to 10*/
	var $total_page = content.length;
	/*var $total_page = content.length;*/

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;

	var testin = new NumberTemplate();

	testin.init(7);
	$("#activity-page-total-slide").html('1');

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}

		var ansClicked = false;
		var wrngClicked = false;
		var ansorder = [];
		var correctans = 0;
		if($(countNext==6)){
			$("#activity-page-current-slide").html('1');
			$("#activity-page-total-slide").html('1');
		}

		$(".optionsdiv").sortable({
			placeholder: "drop-highlight",
			stop: function(event, ui) {
				ansorder = $(this).sortable('toArray');
				$(".submitbtn").show(0);
				// $(".instruction").html(productOrder);
			}
		});
		$(".submitbtn").click(function(){
			correctans = check_order(ansorder);
			if(ansClicked == false){
				testin.updatescore(correctans);
				ansClicked = true;
			}
		});
		function check_order(array){
			var correctones = 0;
			for(i=1; i<=array.length; i++){
				if(parseInt(array[i-1])==i){
					correctones++;
					$('#'+i).removeClass('incorrect').addClass('correct');
				} else{
					$('#'+i).removeClass('correct').addClass('incorrect');
				}
			}
			if(correctones==array.length){
				$( ".optionsdiv" ).sortable( "disable" );
				$('.optionscontainer').css('pointer-events', 'none');
				play_correct_incorrect_sound(1);
				$(".submitbtn").hide(0);
				$nextBtn.show(0);
			} else{
				play_correct_incorrect_sound(0);
			}
			return correctones;
		}
		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		for(i=0;i<7;i++){
			testin.gotoNext();
		}
		templateCaller();
			// testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

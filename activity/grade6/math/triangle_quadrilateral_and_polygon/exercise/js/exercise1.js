var imgpath = $ref+"/exercise/images/";

var content=[

	//ex1
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q1,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o1a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o1b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o1c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "1.png",
					}
				]

			}
		]
	},

	//ex2
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q2,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o2a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o2b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o2c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "2.png",
					}
				]

			}
		]
	},

	//ex3
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q3,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o3a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o3b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o3c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "3.png",
					}
				]

			}
		]
	},

	//ex4
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q4,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o4a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o4b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o4c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "4.png",
					}
				]

			}
		]
	},

	//ex5
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q5,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o5a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o5b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o5c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "5.png",
					}
				]

			}
		]
	},

	//ex6
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q6,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o6a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o6b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o6c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "6.png",
					}
				]

			}
		]
	},

	//ex7
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q7,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o7a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o7b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o7c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "7.png",
					}
				]

			}
		]
	},

	//ex8
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q8,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o8a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o8b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o8c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "8.png",
					}
				]

			}
		]
	},

	//ex9
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q9,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o9a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o9b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o9c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "9.png",
					}
				]

			}
		]
	},

	//ex10
	{
		contentblockadditionalclass: 'ole-background-gradient-rose',
		exerciseblock: [
			{
				textdata: data.string.e1_q10,

				exeoptions: [
					{
						optclass: "class1",
						optdata: data.string.e1_o10a,
					},
					{
						optclass: "class2",
						optdata: data.string.e1_o10b,
					},
					{
						optclass: "class3",
						optdata: data.string.e1_o10c,
					}],
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "10.png",
					}
				]
			}
		]
	}
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = content.length;
	/*var $total_page = content.length;*/

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;

	var testin = new LampTemplate();
   
	testin.init($total_page);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;

		$(".optionscontainer").click(function(){
				if(ansClicked == false){
					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).addClass("correct");
						$('.optionscontainer').css('pointer-events', 'none');
						$(this).children(".corctopt").show(0);
						$('.hint_image').show(0);
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
						$(this).addClass("incorrect");
						$(this).children(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

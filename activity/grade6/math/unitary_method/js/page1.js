var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		contentblockadditionalclass:'',
        contentblocknocenteradjust:"true",
		uppertextblockadditionalclass: 'lesson',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title'
		}],
        imageblockadditionalclass:"coverpage",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgsrc: imgpath+'cover_page.png',
                    imgid: "cover"
                },

            ]
        }]
	},

	//slide1
	{
		contentblockadditionalclass:'bg',
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big fade_in_2 example'
		}],

		imagetextblockadditionalclass: 'coverboard',
		imagetextblock: [
			{
				imagediv: 'candy candy_1 fade_in_2',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			}
		],
	},

	//slide2
	{
		contentblockadditionalclass:'bg',
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'my_font_big fade_in_1 example'
		}],

		imagetextblockadditionalclass: 'coverboard',
		imagetextblock: [
			{
				imagediv: 'candy candy_1',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			},
			{
				imagediv: 'candy candy_2 fade_in_1',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			}
		],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass:'bg',
		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'my_font_big fade_in_1 example'
		}],

		imagetextblockadditionalclass: 'coverboard',
		imagetextblock: [
			{
				imagediv: 'candy candy_1',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			},
			{
				imagediv: 'candy candy_2',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			},
			{
				imagediv: 'candy candy_3 fade_in_1',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			}
		],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'shop-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text6,
			textclass : 'my_font_big fade_in_1 example'
		}],

		inputblock: [{
			inputdiv : 'ques_row check_div fade_in_1',
			textdata: data.string.p1text7,
			textclass: 'my_font_big example',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default',
				}
			]
		}],

		extratextblock:[{
			textdata : data.string.p1text8,
			textclass : 'my_font_medium fade_in_1 hint_bottom',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_hint'
		}],

		imagetextblockadditionalclass: 'coverboard',
		imagetextblock: [
			{
				imagediv: 'candy candy_1',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			},
			{
				imagediv: 'candy candy_2',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			},
			{
				imagediv: 'candy candy_3',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			},
			{
				imagediv: 'candy candy_4 fade_in_1',
				imgclass: '',
				imgsrc: imgpath + "sweet01.png",
				textdata : data.string.p1text3,
				textclass : 'my_font_medium'
			}
		],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass:'bg',
		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p1text9,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text10,
			textclass : 'my_font_big fade_in_1 example'
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass:'bg',
		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p1text9,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text10,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text11,
			textclass : 'my_font_big example'
		}],

		table_div: 'new_table table_1',
		table_class:'full_table',
		tableblock: [
			{
				unit_table:'row_1 t_row',

				textdata1: data.string.price,
				textclass1: 'my_font_big t_head',

				imagediv2: 't_div',
				imgclass2: '',
				imgsrc2: imgpath + "sweet02.png",
				textdata2 : data.string.p1text12,
				textclass2 : 'my_font_small t_ex',

				imagediv3: 't_div',
				imgclass3: '',
				imgsrc3: imgpath + "four-candy.png",
				textdata3 : data.string.p1text13,
				textclass3 : 'my_font_medium t_ex',
			}
		]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass:'bg',
		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p1text9,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text10,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text18,
			textclass : 'my_font_big example'
		}],

		table_div: 'new_table table_1',
		table_class:'full_table',
		tableblock: [
			{
				unit_table:'row_1 t_row',

				textdata1: data.string.price,
				textclass1: 'my_font_big t_head',

				imagediv2: 't_div',
				imgclass2: '',
				imgsrc2: imgpath + "sweet02.png",
				textdata2 : data.string.p1text12,
				textclass2 : 'my_font_small t_ex',

				imagediv3: 't_div',
				imgclass3: '',
				imgsrc3: imgpath + "four-candy.png",
				textdata3 : data.string.p1text13,
				textclass3 : 'my_font_medium t_ex',
			},
			{
				unit_table:'row_1 t_row fade_in_1',

				textdata1: data.string.distance,
				textclass1: 'my_font_big t_head',

				imagediv2: 't_div',
				imgclass2: '',
				imgsrc2: imgpath + "one-rope.png",
				textdata2 : data.string.p1text14,
				textclass2 : 'my_font_small t_ex',

				imagediv3: 't_div',
				imgclass3: '',
				imgsrc3: imgpath + "four-rope.png",
				textdata3 : data.string.p1text15,
				textclass3 : 'my_font_medium t_ex',
			},
		]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass:'bg',
		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p1text9,
			textclass : 'my_font_ultra_big title'
		},
		{
			textdata : data.string.p1text10,
			textclass : 'my_font_big example'
		},
		{
			textdata : data.string.p1text19,
			textclass : 'my_font_big example fade_in_1'
		}],

		table_div: 'new_table table_1',
		table_class:'full_table',
		tableblock: [
			{
				unit_table:'row_1 t_row',

				textdata1: data.string.price,
				textclass1: 'my_font_big t_head',

				imagediv2: 't_div',
				imgclass2: '',
				imgsrc2: imgpath + "sweet02.png",
				textdata2 : data.string.p1text12,
				textclass2 : 'my_font_small t_ex',

				imagediv3: 't_div',
				imgclass3: '',
				imgsrc3: imgpath + "four-candy.png",
				textdata3 : data.string.p1text13,
				textclass3 : 'my_font_medium t_ex',
			},
			{
				unit_table:'row_1 t_row fade_in_1',

				textdata1: data.string.distance,
				textclass1: 'my_font_big t_head',

				imagediv2: 't_div',
				imgclass2: '',
				imgsrc2: imgpath + "one-rope.png",
				textdata2 : data.string.p1text14,
				textclass2 : 'my_font_small t_ex',

				imagediv3: 't_div',
				imgclass3: '',
				imgsrc3: imgpath + "four-rope.png",
				textdata3 : data.string.p1text15,
				textclass3 : 'my_font_medium t_ex',
			},
			{
				unit_table:'row_1 t_row fade_in_1',

				textdata1: data.string.time,
				textclass1: 'my_font_big t_head',

				imagediv2: 't_div',
				imgclass2: 'rhino_img_1',
				imgsrc2: imgpath + "rhino01.png",
				textdata2 : data.string.p1text16,
				textclass2 : 'my_font_small t_ex',

				imagediv3: 't_div',
				imgclass3: 'rhino_img',
				imgsrc3: imgpath + "rhino02.png",
				textdata3 : data.string.p1text17,
				textclass3 : 'my_font_medium t_ex',
			}
		]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

		var preload;
		var timeoutvar = null;
		var current_sound;


		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images
				// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

				// soundsicon-orange
	      {id: "sound_0", src: soundAsset+"s1_p1.ogg"},
				{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
				{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
				{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
				{id: "sound_4a", src: soundAsset+"s1_p5_1.ogg"},
				{id: "sound_4b", src: soundAsset+"s1_p5_2.ogg"},
				{id: "sound_5a", src: soundAsset+"s1_p6_1.ogg"},
				{id: "sound_5b", src: soundAsset+"s1_p6_2.ogg"},
				{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
				{id: "sound_7", src: soundAsset+"s1_p8.ogg"},
				{id: "sound_8", src: soundAsset+"s1_p9.ogg"},
			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 4:
		sound_player("sound_4a");
			input_box('.ole-template-input-box-default', 3, '.default_btn');
            $('.ole-template-input-box-default').css("color","black");
            $('.ole-template-check-btn-default').click(function(){
				if( parseInt( $('.ole-template-input-box-default').val() ) == 20){
					play_correct_incorrect_sound(1);
					$('.hint_bottom').show(0);
						createjs.Sound.stop();
						setTimeout(function(){
							sound_nav("sound_4b");
						},800);
					$('.ole-template-input-box-default' ).prop( "disabled", true );
					$('.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect').css("color","black");
					$('.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
				} else {
						createjs.Sound.stop();
					$('.hint_bottom').show(0);
					play_correct_incorrect_sound(0);
				}
			});
			break;
			case 5:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_5a");
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_5b");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
			break;
		case 6:
		case 7:
		case 8:
			table_resize();
			sound_nav("sound_"+(countNext));
			break;
		default:
          sound_nav("sound_"+(countNext));
			break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
				nav_button_controls();
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();


	function table_resize(){
		$('td').height(0.12*$board.height());
	}

	$(window).resize(function() {
		table_resize();
	});
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

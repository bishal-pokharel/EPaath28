var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'many_one',
		uppertextblock : [{
			textdata : data.string.p4text1,
			textclass : 'my_font_very_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_many_one'
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p4text2,
			textclass : 'my_font_very_big title'
		},
		{
			textdata : data.string.p4text3,
			textclass : 'my_font_medium example fade_in_1'
		}],
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p4text2,
			textclass : 'my_font_very_big title'
		},
		{
			textdata : data.string.p4text3,
			textclass : 'my_font_medium example'
		},
		{
			textdata : data.string.p4text4,
			textclass : 'my_font_medium example fade_in_1'
		}],
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p4text2,
			textclass : 'my_font_very_big title'
		},
		{
			textdata : data.string.p4text3,
			textclass : 'my_font_medium example'
		},
		{
			textdata : data.string.p4text4,
			textclass : 'my_font_medium example'
		},
		{
			textdata : data.string.p4text5,
			textclass : 'my_font_medium example-2 e1x'
		}],

		extratextblock:[{
			textdata : data.string.p4text6,
			textclass : 'my_font_big para_bottom bottom_1',
			datahighlightflag : true,
			datahighlightcustomclass : 'my_font_very_big'
		},
		{
			textdata : data.string.p4text7,
			textclass : 'my_font_big para_bottom bottom_2',
			datahighlightflag : true,
			datahighlightcustomclass : 'my_font_very_big'
		},
		{
			textdata : data.string.p4text8,
			textclass : 'my_font_big para_bottom bottom_3',
			datahighlightflag : true,
			datahighlightcustomclass : 'my_font_very_big'
		}],
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p4text2,
			textclass : 'my_font_very_big title'
		},
		{
			textdata : data.string.p4text9,
			textclass : 'my_font_big example-3 fade_in_1'
		}],
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p4text2,
			textclass : 'my_font_very_big title'
		},
		{
			textdata : data.string.p4text9,
			textclass : 'my_font_big example-3'
		}],

		extratextblock:[{
			textdata : data.string.p4text6,
			textclass : 'my_font_big para_bottom side_1',
			datahighlightflag : true,
			datahighlightcustomclass : 'my_font_very_big'
		},
		{
			textdata : data.string.p4text10,
			textclass : 'my_font_big side_2',
			datahighlightflag : true,
			datahighlightcustomclass : 'underlined'
		}],
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'define-text',
		uppertextblock : [{
			textdata : data.string.p4text2,
			textclass : 'my_font_very_big title'
		},
		{
			textdata : data.string.p4text9,
			textclass : 'my_font_big example-3'
		}],

		extratextblock:[{
			textdata : data.string.p4text6,
			textclass : 'my_font_big para_bottom side_1',
			datahighlightflag : true,
			datahighlightcustomclass : 'my_font_very_big'
		},
		{
			textdata : data.string.p4text10,
			textclass : 'my_font_big side_2',
			datahighlightflag : true,
			datahighlightcustomclass : 'underlined'
		},
		{
			textdata : data.string.p4text8,
			textclass : 'my_font_big para_bottom side_3',
			datahighlightflag : true,
			datahighlightcustomclass : 'my_font_very_big'
		},
		{
			textdata : data.string.p4text11,
			textclass : 'my_font_big side_4'
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

        uppertextblockadditionalclass: ' diytext center-text',
        uppertextblock : [{
            textdata : data.string.diytext,
            textclass : 'my_font_super_big'
        }],
        imagetextblock: [
            {
                imagediv: 'diy',
                imgclass: 'diybg',
                imgsrc: "images/diy_bg/a_07.png",
            }
        ]
	},


	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'question-text',
		uppertextblock : [{
			textdata : data.string.p2text24,
			textclass : 'my_font_very_big title-2'
		},
		{
			textdata : data.string.p4text12,
			textclass : 'my_font_medium my_ques',
		}],

		inputblock: [{
			inputdiv : 'ques_row check_div fade_in_1 div_0',
			textdata: data.string.p4textDIY1,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_1 fade_in_1',
			textdata: data.string.p4textDIY2,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_2 fade_in_1',
			textdata: data.string.p2text14,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_3 fade_in_1',
			textdata: data.string.p4text14,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_4 fade_in_1',
			textdata: data.string.p4text15new1,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		}
		],

		lowertextblockadditionalclass: 'hint-text hint_3',
		lowertextblock : [{
			textdata : data.string.p3text15,
			textclass : 'my_font_medium'
		},
		{
			textdata : data.string.p3text16,
			textclass : 'my_font_medium',
		},
		{
			textdata : data.string.p4text16,
			textclass : 'my_font_medium',
		},
		{
			textdata : data.string.p4text17,
			textclass : 'my_font_medium',
		}],

		lowertextblock2additionalclass: 'hint-text hint_5',
		lowertextblock2 : [{
			textdata : data.string.p2text17,
			textclass : 'my_font_medium'
		},
		{
			textdata : data.string.p2text18,
			textclass : 'my_font_medium',
		},
		{
			textdata : data.string.p4text18,
			textclass : 'my_font_medium',
		},
		{
			textdata : data.string.p4text19,
			textclass : 'my_font_medium',
		}],
	},

	// slide 9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass:'bg',

		uppertextblockadditionalclass: 'question-text',
		uppertextblock : [{
			textdata : data.string.p2text24,
			textclass : 'my_font_very_big title-2'
		},
		{
			textdata : data.string.p4text13,
			textclass : 'my_font_medium my_ques',
		}],

		inputblock: [{
			inputdiv : 'ques_row check_div fade_in_1 div_0',
			textdata: data.string.p4text15new,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_1 fade_in_1',
			textdata: data.string.p4textDIY2,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_2 fade_in_1',
			textdata: data.string.p2text14,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_3 fade_in_1',
			textdata: data.string.p4text14,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_4 fade_in_1',
			textdata: data.string.p4text15new2,
			textclass: 'my_font_medium example-2',
			inputclass: 'input_class my_font_medium',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		}
		],

		lowertextblockadditionalclass: 'hint-text hint_3',
		lowertextblock : [{
			textdata : data.string.p3text15,
			textclass : 'my_font_medium'
		},
		{
			textdata : data.string.p3text16,
			textclass : 'my_font_medium',
		},
		{
			textdata : data.string.p4text20,
			textclass : 'my_font_medium',
		},
		{
			textdata : data.string.p4text21,
			textclass : 'my_font_medium',
		}],

		lowertextblock2additionalclass: 'hint-text hint_5',
		lowertextblock2 : [{
			textdata : data.string.p2text17,
			textclass : 'my_font_medium'
		},
		{
			textdata : data.string.p2text18,
			textclass : 'my_font_medium',
		},
		{
			textdata : data.string.p4text22,
			textclass : 'my_font_medium',
		},
		{
			textdata : data.string.p4text23,
			textclass : 'my_font_medium',
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"s4_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_3a", src: soundAsset+"s4_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s4_p4_2.ogg"},
			{id: "sound_3c", src: soundAsset+"s4_p4_3.ogg"},
			{id: "sound_3d", src: soundAsset+"s4_p4_4.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p5.ogg"},
			{id: "sound_5a", src: soundAsset+"s4_p6_1.ogg"},
			{id: "sound_5b", src: soundAsset+"s4_p6_2.ogg"},
			{id: "sound_6a", src: soundAsset+"s4_p7_1.ogg"},
			{id: "sound_6b", src: soundAsset+"s4_p7_2.ogg"},
			{id: "sound_8", src: soundAsset+"s2_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 3:
			createjs.Sound.stop();
			$(".e1x").show(0);
			current_sound = createjs.Sound.play("sound_3a");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			$(".bottom_1").fadeIn(0);
			current_sound = createjs.Sound.play("sound_3b");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			$(".bottom_2").fadeIn(0);
			current_sound = createjs.Sound.play("sound_3c");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			$(".bottom_3").fadeIn(0);
			current_sound = createjs.Sound.play("sound_3d");
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
			});
		});
	});
});
			break;
		case 5:
			createjs.Sound.stop();
			$(".side_1").fadeIn(0);
				$(".underlined").eq(1).addClass("underline1");
			current_sound = createjs.Sound.play("sound_5a");
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				$(".side_2").fadeIn(0);
				current_sound = createjs.Sound.play("sound_5b");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
			break;
		case 6:
			$(".underlined").eq(1).addClass("underline1");
			$('.side_1, .side_2').show(0);
			createjs.Sound.stop();
			$(".side_3").fadeIn(0);
			current_sound = createjs.Sound.play("sound_6a");
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				$(".side_4").fadeIn(0);
				current_sound = createjs.Sound.play("sound_6b");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
			break;
		case 7:
			play_diy_audio();
      nav_button_controls(2000);
      break;

      case 8:
			sound_player("sound_8");
			input_box('.div_0>.input_class', 5, '.div_0>.check_btn');
			input_box('.div_1>.input_class', 5, '.div_1>.check_btn');
			input_box('.div_2>.input_class', 5, '.div_2>.check_btn');
			input_box('.div_3>.input_class', 5, '.div_3>.check_btn');
			input_box('.div_4>.input_class', 5, '.div_4>.check_btn');
			$('.div_0>.check_btn').click(function(){
				if( parseInt( $('.div_0>.input_class').val() ) == 1200){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
						createjs.Sound.stop();
                    play_correct_incorrect_sound(1);
                    $('.div_0>.input_class').prop('disabled', true);
					$('.div_0>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_0>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_1').css('display', 'flex');
						$('.div_1').fadeIn(1000);
						$('.hint_1').removeClass('change_color_1');
					});
				} else {
						createjs.Sound.stop();
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_0>.hint_btn').click(function(){
				$('.hint_1').addClass('change_color_1');
                $nextBtn.hide();
            });

			$('.div_1>.check_btn').click(function(){
				if( parseInt( $('.div_1>.input_class').val() ) == 20){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_1>.input_class').prop('disabled', true);
					$('.div_1>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_1>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_2').css('display', 'flex');
						$('.div_2').fadeIn(1000);
						$('.hint_2').removeClass('change_color_1');
					});
				} else {
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_1>.hint_btn').click(function(){
				$('.hint_2').addClass('change_color_1');
                $nextBtn.hide();
            });

			$('.div_2>.check_btn').click(function(){
				if( parseInt( $('.div_2>.input_class').val() ) == 60){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_2>.input_class').prop('disabled', true);
					$('.div_2>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_2>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_3').css('display', 'flex');
						$('.div_3').fadeIn(1000);
						$('.hint_3').fadeOut(1000);
					});
				} else {
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_2>.hint_btn').click(function(){
				$('.hint_3').fadeIn(1000);
                $nextBtn.hide();
            });

			$('.div_3>.check_btn').click(function(){
				if( parseInt( $('.div_3>.input_class').val() ) == 15){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_3>.input_class').prop('disabled', true);
					$('.div_3>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_3>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_4').css('display', 'flex');
						$('.div_4').fadeIn(1000);
                        $('.hint_3').fadeOut(1000);
						$('.hint_4').removeClass('change_color_1');
					});
				} else {
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_3>.hint_btn').click(function(){
				$('.hint_4').addClass('change_color_1');
                $nextBtn.hide();
            });

			$('.div_4>.check_btn').click(function(){
				if( parseInt( $('.div_4>.input_class').val() ) == 900){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_4>.input_class').prop('disabled', true);
					$('.div_4>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_4>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.hint_5').fadeOut(1000);
						nav_button_controls(1000);
					});
				} else {
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_4>.hint_btn').click(function(){
				$('.hint_5').fadeIn(1000);
                $nextBtn.hide();
            });
			break;
		case 9:
            $nextBtn.hide();
            input_box('.div_0>.input_class', 5, '.div_0>.check_btn');
			input_box('.div_1>.input_class', 5, '.div_1>.check_btn');
			input_box('.div_2>.input_class', 5, '.div_2>.check_btn');
			input_box('.div_3>.input_class', 5, '.div_3>.check_btn');
			input_box('.div_4>.input_class', 5, '.div_4>.check_btn');
			$('.div_0>.check_btn').click(function(){
				if( parseInt( $('.div_0>.input_class').val() ) == 12500){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_0>.input_class').prop('disabled', true);
					$('.div_0>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_0>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_1').css('display', 'flex');
						$('.div_1').fadeIn(1000);
						$('.hint_1').removeClass('change_color_1');
					});
				} else {
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_0>.hint_btn').click(function(){
                $nextBtn.hide();
                $('.hint_1').addClass('change_color_1');
			});

			$('.div_1>.check_btn').click(function(){
				if( parseInt( $('.div_1>.input_class').val() ) == 50){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_1>.input_class').prop('disabled', true);
					$('.div_1>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_1>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_2').css('display', 'flex');
						$('.div_2').fadeIn(1000);
						$('.hint_2').removeClass('change_color_1');
					});
				} else {
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_1>.hint_btn').click(function(){
				$('.hint_2').addClass('change_color_1');
                $nextBtn.hide();
            });

			$('.div_2>.check_btn').click(function(){
				if( parseInt( $('.div_2>.input_class').val() ) == 250){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_2>.input_class').prop('disabled', true);
					$('.div_2>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_2>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_3').css('display', 'flex');
						$('.div_3').fadeIn(1000);
						$('.hint_3').fadeOut(1000);
					});
				} else {
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_2>.hint_btn').click(function(){
				$('.hint_3').fadeIn(1000);
                $nextBtn.hide();
            });

			$('.div_3>.check_btn').click(function(){
				if( parseInt( $('.div_3>.input_class').val() ) == 65){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_3>.input_class').prop('disabled', true);
					$('.div_3>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_3>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_4').css('display', 'flex');
						$('.div_4').fadeIn(1000);
                        $('.hint_3').fadeOut(1000);
                        $('.hint_4').removeClass('change_color_1');
					});
				} else {
                    play_correct_incorrect_sound(1);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_3>.hint_btn').click(function(){
				$('.hint_4').addClass('change_color_1');
                $nextBtn.hide();
            });

			$('.div_4>.check_btn').click(function(){
				if( parseInt( $('.div_4>.input_class').val() ) == 16250){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
                    play_correct_incorrect_sound(1);
                    $('.div_4>.input_class').prop('disabled', true);
					$('.div_4>.default_btn').delay(1000).fadeOut(1000,function(){
						$('.div_4>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.hint_5').fadeOut(1000);
						nav_button_controls(1000);
					});
				} else {
                    play_correct_incorrect_sound(0);
                    $(this).addClass('incorrect');
				}
			});
			$('.div_4>.hint_btn').click(function(){
				$('.hint_5').fadeIn(1000);
                $nextBtn.hide();
            });
			break;
		default:
            sound_nav("sound_"+(countNext));
			break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
				nav_button_controls();
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

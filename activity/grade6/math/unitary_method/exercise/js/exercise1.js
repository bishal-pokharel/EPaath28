var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var checkpoint_positions = [];
var rhino_animation;
var screen_factor;
var current_question = 0;
var screen_position = 0;
var full_width;
var total_question = 10;

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};


var content = [

	//slide0
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext1,
			textclass : 'question',
		},
		{
			textdata : data.string.ext1a,
			textclass : 'hint',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]


	},
	//slide1
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext2,
			textclass : 'question',
		},
		{
			textdata : data.string.ext2a,
			textclass : 'hint',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]


	},
	//slide2
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext3,
			textclass : 'question',
		},
		{
			textdata : data.string.ext3a,
			textclass : 'hint',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]


	},
	//slide3
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext4,
			textclass : 'question',
		},
		{
			textdata : data.string.ext4a,
			textclass : 'hint',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]


	},
	//slide4
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext5,
			textclass : 'question',
		},
		{
			textdata : data.string.ext5a,
			textclass : 'hint',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]


	},
	//slide5
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext6,
			textclass : 'question',
		},
		{
			textdata : data.string.ext6a,
			textclass : 'hint',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]


	},
	//slide6
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext7,
			textclass : 'question',
		},
		{
			textdata : data.string.ext7a,
			textclass : 'hint2',
		},
		{
			textdata : data.string.ext7b,
			textclass : 'hint1',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]
	},

	//slide7
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext8,
			textclass : 'question',
		},
		{
			textdata : data.string.ext8a,
			textclass : 'hint2',
		},
		{
			textdata : data.string.ext8b,
			textclass : 'hint1',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]
	},

	//slide8
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext9,
			textclass : 'question',
		},
		{
			textdata : data.string.ext9a,
			textclass : 'hint2',
		},
		{
			textdata : data.string.ext9b,
			textclass : 'hint1',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]
	},

	//slide9
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		uppertextblock : [{
			textdata : data.string.ext10,
			textclass : 'question',
		},
		{
			textdata : data.string.ext10a,
			textclass : 'hint2',
		},
		{
			textdata : data.string.ext10b,
			textclass : 'hint1',
		},
		{
			textdata : data.string.check,
			textclass : 'checkbutton',
		}],
		inputdivclass: 'inputdiv',
		inputformclass: 'inputformclass',
		placeholdertext: data.string.placeholder,
		inputbox:[{

		}]
	},
];

// content.shufflearray();

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext =0;
	var $total_page = content.length;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	var rhino = new RhinoTemplate();
   
	rhino.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		rhino.numberOfQuestions();

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch (countNext) {
			case 0:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==72)
					{
		            	$(this).removeClass('incorrect').addClass('correct');
		            	nav_button_controls(1000);
						$(this).css('pointer-events', 'none');
						rhino.update(true);
						play_correct_incorrect_sound(true);
						$('.inputformclass').css('pointer-events', 'none');
					}
				else{
	            	$(this).addClass('incorrect');
					play_correct_incorrect_sound(false);
					rhino.update(false);
					$('.hint').addClass('fadein');
				}
			});
			break;

			case 1:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==2040)
				{
	            	nav_button_controls(1000);
						play_correct_incorrect_sound(true);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					$('.inputformclass').css('pointer-events', 'none');
	        $(this).removeClass('incorrect').addClass('correct');
				}

				else{
	            	$(this).addClass('incorrect');
					play_correct_incorrect_sound(false);
					rhino.update(false);
					$('.hint').addClass('fadein');
				}
			});
			break;

			case 2:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==100)
				{
	            	nav_button_controls(1000);
					play_correct_incorrect_sound(true);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					$('.inputformclass').css('pointer-events', 'none');
	            	$(this).removeClass('incorrect').addClass('correct');
				}

				else{
	            	$(this).addClass('incorrect');
					play_correct_incorrect_sound(false);
					rhino.update(false);
					$('.hint').addClass('fadein');
				}
			});
			break;

			case 3:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==10)
				{
	            	nav_button_controls(1000);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					play_correct_incorrect_sound(true);
					$('.inputformclass').css('pointer-events', 'none');
	            	$(this).removeClass('incorrect').addClass('correct');
				}

				else{
	            	$(this).addClass('incorrect');
					play_correct_incorrect_sound(false);
					rhino.update(false);
					$('.hint').addClass('fadein');
				}
			});
			break;

			case 4:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==100)
				{
	            	nav_button_controls(1000);
					play_correct_incorrect_sound(true);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					$('.inputformclass').css('pointer-events', 'none');
	            	$(this).removeClass('incorrect').addClass('correct');
				}

				else{
	            	$(this).addClass('incorrect');
					play_correct_incorrect_sound(false);
					rhino.update(false);
					$('.hint').addClass('fadein');
				}
			});
			break;

			case 5:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==31)
				{
	            	nav_button_controls(1000);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					play_correct_incorrect_sound(true);
					$('.inputformclass').css('pointer-events', 'none');
	            	$(this).removeClass('incorrect').addClass('correct');
				}

				else{
					play_correct_incorrect_sound(false);
	            	$(this).addClass('incorrect');
					rhino.update(false);
					$('.hint').addClass('fadein');
				}
			});
			break;

			case 6:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==900)
				{
	            	nav_button_controls(1000);
					play_correct_incorrect_sound(true);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					$('.inputformclass').css('pointer-events', 'none');
	            	$(this).removeClass('incorrect').addClass('correct');
				}

				else{
	            	$(this).addClass('incorrect');
					play_correct_incorrect_sound(false);
					rhino.update(false);
					$('.hint1,.hint2').addClass('fadein');
				}
			});
			break;

			case 7:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==2800)
				{
	            	nav_button_controls(1000);
					play_correct_incorrect_sound(true);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					$('.inputformclass').css('pointer-events', 'none');
	            	$(this).removeClass('incorrect').addClass('correct');
				}

				else{
					play_correct_incorrect_sound(false);
	            	$(this).addClass('incorrect');
					rhino.update(false);
					$('.hint1,.hint2').addClass('fadein');
				}
			});
			break;

			case 8:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==2400)
				{
					play_correct_incorrect_sound(true);
	            	nav_button_controls(1000);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					$('.inputformclass').css('pointer-events', 'none');
	            	$(this).removeClass('incorrect').addClass('correct');
				}

				else{
					play_correct_incorrect_sound(false);
	            	$(this).addClass('incorrect');
					rhino.update(false);
					$('.hint1,.hint2').addClass('fadein');
				}
			});
			break;

			case 9:
            $('.checkbutton').click(function(){
				var input = parseInt($('.inputformclass').val());
				if(input==14904)
				{
	            	nav_button_controls(1000);
					play_correct_incorrect_sound(true);
					$(this).css('pointer-events', 'none');
					rhino.update(true);
					$('.inputformclass').css('pointer-events', 'none');
	            	$(this).removeClass('incorrect').addClass('correct');
				}

				else{
	            	$(this).addClass('incorrect');
					play_correct_incorrect_sound(false);
					rhino.update(false);
					$('.hint1,.hint2').addClass('fadein');
				}
			});
			break;

		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				rhino.gotoNext();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $('button_class').trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

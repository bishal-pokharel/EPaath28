var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		uppertextblockadditionalclass: 'profit-block',
		uppertextblock : [{
			textdata : data.string.pprofit,
			textclass : 'my_font_ultra_big'
		}],
		lowertextblockadditionalclass: 'loss-block',
		lowertextblock : [{
			textdata : data.string.ploss,
			textclass : 'my_font_ultra_big'
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		uppertextblockadditionalclass: 'profit-block-2',
		uppertextblock : [{
			textdata : data.string.pprofit,
			textclass : 'profit-block-2-header my_font_ultra_big'
		},{
			textdata : data.string.p5text1,
			textclass : 'its_hidden my_font_medium text-1'
		},{
			textdata : data.string.p5text2,
			textclass : 'its_hidden my_font_big higlighted-text-p text-1'
		},{
			textdata : data.string.p5text3,
			textclass : 'its_hidden my_font_medium text-2'
		},{
			textdata : data.string.p5text4,
			textclass : 'its_hidden my_font_big higlighted-text-p text-2'
		}],
		lowertextblockadditionalclass: 'loss-block-2',
		lowertextblock : [{
			textdata : data.string.ploss,
			textclass : 'loss-block-2-header my_font_ultra_big'
		},{
			textdata : data.string.p5text5,
			textclass : 'its_hidden my_font_medium text-11'
		},{
			textdata : data.string.p5text6,
			textclass : 'its_hidden my_font_big higlighted-text-l text-11'
		},{
			textdata : data.string.p5text7,
			textclass : 'its_hidden my_font_medium text-12'
		},{
			textdata : data.string.p5text8,
			textclass : 'its_hidden my_font_big higlighted-text-l text-12'
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-brown-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.diytext,
			textclass : 'center-middle-title'
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue ",
		headerblock:[
		{
			textdata : data.string.p5text9,
		}
		],
		inputblock: [{
			inputdiv : 'equal-answer-block inputblock-1 fade_in_1',
			textdata: data.string.p5text10,
			textclass: 'my_font_big',
			textdata1: data.string.p5text11,
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		},
		{
			inputdiv : 'equal-answer-block inputblock-2',
			textdata: data.string.p5text12,
			textclass: 'my_font_big',
			textdata1: '',
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		},
		{
			inputdiv : 'equal-answer-block inputblock-3',
			textdata: data.string.p5text14,
			textclass: 'my_font_big',
			textdata1: data.string.p5text15,
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
		lowertextblockadditionalclass: 'answer-block its_hidden',
		lowertextblock : [{
			textdata : data.string.pprofit,
			textclass : 'ole-template-check-btn-default profit-btn'
		},{
			textdata : data.string.ploss,
			textclass : 'ole-template-check-btn-default  loss-btn'
		},{
			textdata: '',
			textclass: 'my_font_big cor-incor-text',
		},{
			textdata: data.string.p5text13,
			textclass: 'my_font_big hint-text',
		}],
		extratextblock : [{
			textdata : data.string.p5text16,
			textclass : 'conclusion my_font_big its_hidden'
		}],
		imageblock : [{
			imagestoshow: [{
				imgclass: "side-image-ques",
				imgsrc: imgpath + "redpandawithfluit.png",
			}],
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue ",
		headerblock:[
		{
			textdata : data.string.p5text17,
		}
		],
		inputblock: [

		{
			inputdiv : 'equal-answer-block inputblock-1 fade_in_1',
			textdata: data.string.p5text12,
			textclass: 'my_font_big',
			textdata1: '',
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		},
		{
			inputdiv : 'equal-answer-block inputblock-2',
			textdata: data.string.p5text10,
			textclass: 'my_font_big',
			textdata1: data.string.p5text18,
			textclass1: 'my_font_big hint-text fnt',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		},
		{
			inputdiv : 'equal-answer-block inputblock-3',
			textdata: data.string.p5text22,
			textclass: 'my_font_big',
			textdata1: data.string.p5text23,
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
		lowertextblockadditionalclass: 'answer-block its_hidden',
		lowertextblock : [{
			textdata : data.string.pprofit,
			textclass : 'ole-template-check-btn-default profit-btn'
		},{
			textdata : data.string.ploss,
			textclass : 'ole-template-check-btn-default  loss-btn'
		},{
			textdata: '',
			textclass: 'my_font_big cor-incor-text',
		},{
			textdata: data.string.p5text21,
			textclass: 'my_font_big hint-text',
		}],
		imageblock : [{
			imagestoshow: [{
				imgclass: "side-image-ques",
				imgsrc: imgpath + "sundariwithguitar.png",
			}],
		}],
		extratextblock : [{
			textdata : data.string.p5text19,
			textclass : 'conclusion my_font_big its_hidden'
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-brown-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p5text24,
			textclass : 'center-middle-title'
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'top-sp-text my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p5text25,
			textclass : ''
		}],
		extratextblock : [{
			textdata : data.string.p5text26,
			textclass : 'profit-text my_font_big'
		},{
			textdata : data.string.p5text27,
			textclass : 'loss-text my_font_big'
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'top-sp-text my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p5text25,
			textclass : ''
		}],
		draggableblock: [
			{
				textdata : data.string.p5text29,
				textclass : 'dragabble-text drag-1',
				dataname:"lossClass"
			},{
				textdata : data.string.p5text30,
				textclass : 'dragabble-text drag-2',
				dataname:"lossClass"
			},{
				textdata : data.string.p5text31,
				textclass : 'dragabble-text drag-3',
				dataname:"profitCLass"
			},{
				textdata : data.string.p5text32,
				textclass : 'dragabble-text drag-4',
				dataname:"profitCLass"
			}
		],
		droppableblock:[
			{
				droppableblockadditionalclass: 'drop-1',
			},
			{
				droppableblockadditionalclass: 'drop-2',
			},
			{
				droppableblockadditionalclass: 'drop-3',
			},
			{
				droppableblockadditionalclass: 'drop-4',
			}
		],

		extratextblock : [{
			textdata : data.string.p5text26,
			textclass : 'profit-text my_font_big'
		},{
			textdata : data.string.p5text27,
			textclass : 'loss-text my_font_big'
		},{
			textdata : data.string.p5text28,
			textclass : 'dragdrop-text my_font_big'
		}],
	},
	//slide 8
	{
		additionalclasscontentblock: "ole_temp_peach_background1",
		headerblockadditionalclass: "ole_temp_peach_header fix-height-header",
		headerblock:[
		{
			textdata : data.string.p5text42,
			textclass: ''
		}
		],
		summaryblockadditionalclass: "verticalSummary",
		summaryblock:[
			{
				summaryparentadditionalclass: 'centered-summary',
				summary:[{
					textdata : data.string.p5text33,
					textclass: 'my_font_big '
				},
				{
					textdata : data.string.p5text31,
					textclass: 'my_font_very_big formula-summary'
				}]
			},
			{
				summaryparentadditionalclass: 'centered-summary',
				summary:[{
					textdata : data.string.p5text34,
					textclass: 'my_font_big '
				},
				{
					textdata : data.string.p5text29,
					textclass: 'my_font_very_big formula-summary'
				}]
			},
		],
	},
	//slide 9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-brown-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.diytext,
			textclass : 'center-middle-title'
		}]
	},

	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue ",
		headerblock:[
		{
			textdata : data.string.p5text35,
		}
		],
		inputblock: [
		{
			inputdiv : 'equal-answer-block inputblock-4 fade_in_1',
			textdata: data.string.p5text37,
			textclass: 'my_font_big',
			textdata1: '',
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		},
		{
			inputdiv : 'equal-answer-block inputblock-5',
			textdata: data.string.p5text39,
			textclass: 'my_font_big',
			textdata1: '',
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		},
		{
			inputdiv : 'equal-answer-block inputblock-6',
			textdata: data.string.p5text38,
			textclass: 'my_font_big',
			textdata1: data.string.p5text29,
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
		extratextblock : [{
			textdata : data.string.p5text43,
			textclass : 'conclusion-3 my_font_big its_hidden'
		}],
	},

	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue ",
		headerblock:[
		{
			textdata : data.string.p5text36,
		}
		],
		inputblock: [
		{
			inputdiv : 'equal-answer-block inputblock-4 fade_in_1',
			textdata: data.string.p5text37,
			textclass: 'my_font_big',
			textdata1: data.string.p5text41,
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		},
		{
			inputdiv : 'equal-answer-block inputblock-5',
			textdata: data.string.p5text40,
			textclass: 'my_font_big',
			textdata1: '',
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		},
		{
			inputdiv : 'equal-answer-block inputblock-6',
			textdata: data.string.p5text38,
			textclass: 'my_font_big',
			textdata1: data.string.p5text31,
			textclass1: 'my_font_big hint-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
		extratextblock : [{
			textdata : data.string.p5text44,
			textclass : 'conclusion-3 my_font_big its_hidden'
		}],
		imageblock : [{
			imagestoshow: [{
				imgclass: "side-image-ques",
				imgsrc: imgpath + "sundarwithnotebook.png",
			}],
		}]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var selected_item = ['tortoise', 50];

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s1_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s1_p2pa", src: soundAsset+"s5_p2_profit_1.ogg"},
			{id: "s1_p2pb", src: soundAsset+"s5_p2_profit_2.ogg"},
			{id: "s1_p2la", src: soundAsset+"s5_p2_loss_1.ogg"},
			{id: "s1_p2lb", src: soundAsset+"s5_p2_loss_2.ogg"},
			{id: "s1_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p4_2", src: soundAsset+"s5_p4_2.ogg"},
			{id: "s1_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p5_2", src: soundAsset+"s5_p5_2.ogg"},
			{id: "s1_p6", src: soundAsset+"s5_p6.ogg"},
			{id: "s1_p7a", src: soundAsset+"s5_p7_1.ogg"},
			{id: "s1_p7b", src: soundAsset+"s5_p7_2.ogg"},
			{id: "s1_p8", src: soundAsset+"s5_p8.ogg"},
			{id: "s1_p9a", src: soundAsset+"s5_p9_1.ogg"},
			{id: "s1_p9b", src: soundAsset+"s5_p9_2.ogg"},
			{id: "s1_p9c", src: soundAsset+"s5_p9_3.ogg"},
			{id: "s1_p11", src: soundAsset+"s5_p11.ogg"},
			{id: "s5_p11_2", src: soundAsset+"s5_p11_2.ogg"},
			{id: "s1_p12", src: soundAsset+"s5_p12.ogg"},
			{id: "s5_p12_2", src: soundAsset+"s5_p12_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch (countNext) {
			case 0:
			sound_nav("s1_p1");
			break;
			case 1:
				setTimeout(function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p2pa");
					current_sound.play();
					$(".text-1").show(0);
					current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p2la");
					current_sound.play();
					$(".text-11").show(0);
					current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p2pb");
					current_sound.play();
					$(".text-2").show(0);
					current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p2lb");
					current_sound.play();
					$(".text-12").show(0);
					current_sound.on('complete', function(){
						nav_button_controls(0);
					});
					});
				});
			});
				}, 2500);
				break;
			case 2:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 3:
			sound_player("s1_p4");
				input_box('.inputblock-1>.ole-template-input-box-default', 5, '.inputblock-1>.check-button');
				input_box('.inputblock-2>.ole-template-input-box-default', 5, '.inputblock-2>.check-button');
				input_box('.inputblock-3>.ole-template-input-box-default', 5, '.inputblock-3>.check-button');

			 	$('.inputblock-1>.check-button').click(function(){
		        	if(parseInt($('.inputblock-1>.ole-template-input-box-default').val())==500){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-1>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-1>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-1>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-1>.right-wrong').show(0);
						$('.inputblock-1>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-1>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-1>.check-button').hide(0);
							$('.inputblock-1>.right-wrong').hide(0);
							$('.inputblock-1>.hint-text').fadeIn(200, function(){
								$('.inputblock-2').css('display', 'flex');
							});
						}, 1000);
						createjs.Sound.stop();
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-1>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint_1').addClass('highlighted-hint');
						$('.inputblock-1>.right-wrong').show(0);
						$('.inputblock-1>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });

		        $('.inputblock-2>.check-button').click(function(){
		        	if(parseInt($('.inputblock-2>.ole-template-input-box-default').val())==400){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-2>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-2>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-2>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-2>.right-wrong').show(0);
						$('.inputblock-2>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-2>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-2>.check-button').hide(0);
							$('.inputblock-2>.right-wrong').hide(0);
							$('.answer-block').show(0);
						}, 1000);
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-2>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint_2').addClass('highlighted-hint');
						$('.inputblock-2>.right-wrong').show(0);
						$('.inputblock-2>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });


		        $('.profit-btn').click(function(){
		        	$('.answer-block>.cor-incor-text').show(0);
					$('.loss-btn').removeClass('ole-template-check-btn-default-incorrect');
		        	$('.profit-btn, .loss-btn').css('pointer-events', 'none');
					$('.answer-block>.cor-incor-text').css('background-image', 'url(images/correct.png)');
					$(this).addClass('ole-template-check-btn-default-correct');
					play_correct_incorrect_sound(1);
					setTimeout(function(){
						$('.answer-block>.cor-incor-text').hide(0);
						$('.loss-btn').hide(0);
						$('.answer-block>.hint-text').fadeIn(200, function(){
							$('.inputblock-3').css('display', 'flex');
						});
					}, 1000);
		        });
		         $('.loss-btn').click(function(){
		        	$('.answer-block>.cor-incor-text').show(0);
					$('.answer-block>.cor-incor-text').css('background-image', 'url(images/wrong.png)');
					$(this).addClass('ole-template-check-btn-default-incorrect');
					play_correct_incorrect_sound(0);
		        });

		        $('.inputblock-3>.check-button').click(function(){
		        	if(parseInt($('.inputblock-3>.ole-template-input-box-default').val())==100){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-3>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-3>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-3>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-3>.right-wrong').show(0);
						$('.inputblock-3>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-3>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-3>.check-button').hide(0);
							$('.inputblock-3>.right-wrong').hide(0);
							$('.inputblock-3>.hint-text').fadeIn(200, function(){
								$('.conclusion').fadeIn(300, function(){
								sound_nav("s5_p4_2");
								});
							});;
						}, 1000);
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-3>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
						$('.inputblock-3>.right-wrong').show(0);
						$('.inputblock-3>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
		        break;
			case 4:
			sound_player("s1_p5");
				input_box('.inputblock-1>.ole-template-input-box-default', 5, '.inputblock-1>.check-button');
				input_box('.inputblock-2>.ole-template-input-box-default', 5, '.inputblock-2>.check-button');
				input_box('.inputblock-3>.ole-template-input-box-default', 5, '.inputblock-3>.check-button');

			 	$('.inputblock-1>.check-button').click(function(){
		        	if(parseInt($('.inputblock-1>.ole-template-input-box-default').val())==6000){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-1>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-1>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-1>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-1>.right-wrong').show(0);
						$('.inputblock-1>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-1>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-1>.check-button').hide(0);
							$('.inputblock-1>.right-wrong').hide(0);
							$('.inputblock-2').css('display', 'flex');
						}, 1000);
						createjs.Sound.stop();
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-1>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint_1').addClass('highlighted-hint');
						$('.inputblock-1>.right-wrong').show(0);
						$('.inputblock-1>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });

		        $('.inputblock-2>.check-button').click(function(){
		        	if(parseInt($('.inputblock-2>.ole-template-input-box-default').val())==4800){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-2>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-2>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-2>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-2>.right-wrong').show(0);
						$('.inputblock-2>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-2>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-2>.check-button').hide(0);
							$('.inputblock-2>.right-wrong').hide(0);
							$('.inputblock-2>.hint-text').fadeIn(200, function(){
								$('.answer-block').show(0);
							});
							$('.answer-block').show(0);
						}, 1000);
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-2>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint_2').addClass('highlighted-hint');
						$('.inputblock-2>.right-wrong').show(0);
						$('.inputblock-2>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });


		        $('.loss-btn').click(function(){
		        	$('.answer-block>.cor-incor-text').show(0);
					$('.profit-btn').removeClass('ole-template-check-btn-default-incorrect');
		        	$('.profit-btn, .loss-btn').css('pointer-events', 'none');
					$('.answer-block>.cor-incor-text').css('background-image', 'url(images/correct.png)');
					$(this).addClass('ole-template-check-btn-default-correct');
					play_correct_incorrect_sound(1);
					setTimeout(function(){
						$('.answer-block>.cor-incor-text').hide(0);
						$('.profit-btn').hide(0);
						$('.answer-block>.hint-text').fadeIn(200, function(){
							$('.inputblock-3').css('display', 'flex');
						});
					}, 1000);
		        });
		         $('.profit-btn').click(function(){
		        	$('.answer-block>.cor-incor-text').show(0);
					$('.answer-block>.cor-incor-text').css('background-image', 'url(images/wrong.png)');
					$(this).addClass('ole-template-check-btn-default-incorrect');
					play_correct_incorrect_sound(0);
		        });

		        $('.inputblock-3>.check-button').click(function(){
		        	if(parseInt($('.inputblock-3>.ole-template-input-box-default').val())==1200){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-3>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-3>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-3>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-3>.right-wrong').show(0);
						$('.inputblock-3>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-3>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-3>.check-button').hide(0);
							$('.inputblock-3>.right-wrong').hide(0);
							$('.inputblock-3>.hint-text').fadeIn(200, function(){
								$('.conclusion').fadeIn(300, function(){
									// nav_button_controls(200);
									sound_nav("s5_p5_2");
								});
							});;
						}, 1000);
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-3>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
						$('.inputblock-3>.right-wrong').show(0);
						$('.inputblock-3>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
		        break;
				case 5:
				sound_nav("s1_p"+(countNext+1));
				break;
				case 6:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p7a");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p7b");
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(0);
					});
				});
				break;
		    case 7:
				sound_player("s1_p8");
				var drop_count = 0;
				$(".dragabble-text").draggable({
		            containment: ".board",
		            revert: true,
		            appendTo: "body",
		            zIndex: 200,
				});
				$('.drop-1').droppable({
					accept : ".drag-1, .drag-2, .drag-3, .drag-4",
					drop: function(event, ui) {
						drop_function(event, ui, ".drop-1", ui.draggable, "profitCLass");
					}
				});

				$('.drop-2').droppable({
					accept : ".drag-1, .drag-2, .drag-3, .drag-4",
					drop: function(event, ui) {
						drop_function(event, ui, ".drop-2", ui.draggable, "profitCLass");
					}
				});

				$('.drop-3').droppable({
					accept : ".drag-1, .drag-2, .drag-3, .drag-4",
					drop: function(event, ui) {
						drop_function(event, ui, ".drop-3", ui.draggable, "lossClass");
					}
				});

				$('.drop-4').droppable({
					accept : ".drag-1, .drag-2, .drag-3, .drag-4",
					drop: function(event, ui) {
						drop_function(event, ui, ".drop-4", ui.draggable, "lossClass");
					}
				});
				function drop_function(event, ui, droppedon, draggable, correctCLass){
					// alert(draggable.attr("data-name")+".........."+correctCLass);
					if(draggable.attr("data-name") == correctCLass){
						createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						$(draggable).css({
								'position': 'absolute',
								'width': '100%',
								'height': '100%',
								'top': '0%',
								'left': '0%',
								'border-radius': '0vmin',
								'border': 'none',
								'background-color': '#93C47D',
							}).appendTo(droppedon);
						draggable.draggable('disable');
						drop_count++;
						if(drop_count >=4){
							nav_button_controls(100);
						}
						$(droppedon).droppable( "disable" );
					}else{
						createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				}
				break;
				case 8:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p9a");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p9b");
					current_sound.play();
					current_sound.on('complete', function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p9c");
						current_sound.play();
						current_sound.on('complete', function(){
						nav_button_controls(0);
					});
				});
			});
				break;
				case 9:
				play_diy_audio();
				nav_button_controls(2000);
				break;
			case 10:
			sound_player("s1_p11");
				input_box('.inputblock-4>.ole-template-input-box-default', 5, '.inputblock-4>.check-button');
				input_box('.inputblock-5>.ole-template-input-box-default', 5, '.inputblock-5>.check-button');
				input_box('.inputblock-6>.ole-template-input-box-default', 5, '.inputblock-6>.check-button');

			 	$('.inputblock-4>.check-button').click(function(){
		        	if(parseInt($('.inputblock-4>.ole-template-input-box-default').val())==900){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-4>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-4>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-4>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-4>.right-wrong').show(0);
						$('.inputblock-4>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-4>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-4>.check-button').hide(0);
							$('.inputblock-4>.right-wrong').hide(0);
							$('.inputblock-5').css('display', 'flex');
						}, 1000);
						createjs.Sound.stop();
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-4>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint_1').addClass('highlighted-hint');
						$('.inputblock-4>.right-wrong').show(0);
						$('.inputblock-4>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });

		        $('.inputblock-5>.check-button').click(function(){
		        	if(parseInt($('.inputblock-5>.ole-template-input-box-default').val())==50){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-5>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-5>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-5>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-5>.right-wrong').show(0);
						$('.inputblock-5>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-5>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-5>.check-button').hide(0);
							$('.inputblock-5>.right-wrong').hide(0);
							$('.inputblock-6').css('display', 'flex');
						}, 1000);
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-5>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint_2').addClass('highlighted-hint');
						$('.inputblock-5>.right-wrong').show(0);
						$('.inputblock-5>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
		        $('.inputblock-6>.check-button').click(function(){
		        	if(parseInt($('.inputblock-6>.ole-template-input-box-default').val())==850){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-6>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-6>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-6>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-6>.right-wrong').show(0);
						$('.inputblock-6>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-6>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-6>.check-button').hide(0);
							$('.inputblock-6>.right-wrong').hide(0);
							$('.inputblock-6>.hint-text').fadeIn(200, function(){
								$('.conclusion-3').fadeIn(300, function(){
								sound_nav("s5_p11_2");
								});
							});
						}, 1000);
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-6>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
						$('.inputblock-6>.right-wrong').show(0);
						$('.inputblock-6>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
		        break;
			case 11:
				sound_player("s1_p12");
				input_box('.inputblock-4>.ole-template-input-box-default', 5, '.inputblock-4>.check-button');
				input_box('.inputblock-5>.ole-template-input-box-default', 5, '.inputblock-5>.check-button');
				input_box('.inputblock-6>.ole-template-input-box-default', 5, '.inputblock-6>.check-button');

			 	$('.inputblock-4>.check-button').click(function(){
		        	if(parseInt($('.inputblock-4>.ole-template-input-box-default').val())==360){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-4>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-4>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-4>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-4>.right-wrong').show(0);
						$('.inputblock-4>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-4>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-4>.check-button').hide(0);
							$('.inputblock-4>.right-wrong').hide(0);
							$('.inputblock-4>.hint-text').fadeIn(200, function(){
								$('.inputblock-5').css('display', 'flex');
							});
						}, 1000);
						createjs.Sound.stop();
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-4>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint_1').addClass('highlighted-hint');
						$('.inputblock-4>.right-wrong').show(0);
						$('.inputblock-4>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });

		        $('.inputblock-5>.check-button').click(function(){
		        	if(parseInt($('.inputblock-5>.ole-template-input-box-default').val())==60){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-5>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-5>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-5>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-5>.right-wrong').show(0);
						$('.inputblock-5>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-5>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-5>.check-button').hide(0);
							$('.inputblock-5>.right-wrong').hide(0);
							$('.inputblock-6').css('display', 'flex');
						}, 1000);
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-5>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint_2').addClass('highlighted-hint');
						$('.inputblock-5>.right-wrong').show(0);
						$('.inputblock-5>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
		        $('.inputblock-6>.check-button').click(function(){
		        	if(parseInt($('.inputblock-6>.ole-template-input-box-default').val())==420){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-6>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-6>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-6>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.inputblock-6>.right-wrong').show(0);
						$('.inputblock-6>.right-wrong').attr('src', 'images/correct.png');
						$('.inputblock-6>.ole-template-input-box-default').blur();
						setTimeout(function(){
							$('.inputblock-6>.check-button').hide(0);
							$('.inputblock-6>.right-wrong').hide(0);
							$('.inputblock-6>.hint-text').fadeIn(200, function(){
								$('.conclusion-3').fadeIn(300, function(){
								sound_nav("s5_p12_2");
								});
							});
						}, 1000);
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-6>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
						$('.inputblock-6>.right-wrong').show(0);
						$('.inputblock-6>.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
		        break;
			default:

				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
				nav_button_controls(0);
		});
	}
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}
	function arrage_items_in_shelf(){
		for(var i=1; i<7; i++){
			var shelf_array = document.getElementsByClassName('shop_item_'+i);
			var total_in_array = shelf_array.length;
			for(var m=0; m<shelf_array.length; m++){
				var left_position = $('.shop_item_'+i).eq(0).position().left+m*$board.width()*0.06;
				$('.shop_item_'+i).eq(m).css({
					'left': left_position,
					'z-index':1 + (total_in_array-m)*1
				});
				// shelf_array[m].style.left = 1 + (total_in_array-m)*1;
			}
		}
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

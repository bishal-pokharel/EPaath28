var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		contentblockadditionalclass : 'ole_temp_peach_background1',
		uppertextblockadditionalclass:"titletext",
		uppertextblock:[{
			textclass:"texttit",
			textdata:data.lesson.chapter
		}],
		imageblockadditionalclass:'coverpage_css',
		imageblock :[{
		imagestoshow : [{
			imgclass : "coverpage",
			imgsrc : imgpath + "cover_page.png"
		}]
		}],
	},

	//slide1
	{
		contentblockadditionalclass : 'ole_temp_peach_background1',
		headerblockadditionalclass:'ole_temp_peach_header',
		headerblock: [{
			textdata : data.string.p1text1,
			textclass : 'center_align'
		}],
		imageblockadditionalclass:'imageblockclass',
		imageblock :[{
		imagestoshow : [{
			imgclass : "rhino",
			imgsrc : imgpath + "rhino_frontface.png"
		}]
		}],
		uppertextblockadditionalclass:'rhunotextblock',
		uppertextblock : [
		{
			textdata : data.string.p1text2,
			textclass : 'rhinodialogue1 dialogs right'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'rhinodialogue2 dialogs right'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'rhinodialogue3 dialogs right'
		},
		],

	},

	//slide2
{
		contentblockadditionalclass : 'ole_temp_peach_background1',
		headerblockadditionalclass:'ole_temp_peach_header',
		headerblock: [{
			textdata : data.string.p1text1,
			textclass : 'center_align'
		}],
		imageblockadditionalclass:'imageblockclass1',
		imageblock :[{
		imagestoshow : [{
			imgclass : "rhinoslide2",
			imgsrc : imgpath + "rhino01.png"
		},
		{
			imgclass : "money1 money",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "money2 money",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "money3 money",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "money4 money",
			imgsrc : imgpath + "rs500.png"
		},{
			imgclass : "zebra",
			imgsrc : imgpath + "zebra.png"
		},
		{
			imgclass : "watermelon",
			imgsrc : imgpath + "watermelon.png"
		}]
		}],
		uppertextblockadditionalclass:'rhunotextblockslide2',
		uppertextblock : [
		{
			textdata : data.string.p1text6,
			textclass : 'rhinodialogue3 dialogs right'
		},
		{
			datahighlightflag:true,
			datahighlightcustomclass:'orange',
			textdata : data.string.p1text7,
			textclass : 'rhinodialogue1 dialogs right'
		},
		{
			textdata : data.string.p1text8,
			textclass : 'rhinodialogue2 dialogs right'
		}],

	},

	//slide3
	{
		contentblockadditionalclass : 'ole_temp_peach_background1',
		headerblockadditionalclass:'ole_temp_peach_header',
		headerblock: [{
			textdata : data.string.p1text1,
			textclass : 'center_align'
		}],
		imageblockadditionalclass:'imageblockclass',
		imageblock :[{
		imagestoshow : [{
			imgclass : "rhino1",
			imgsrc : imgpath + "monkeyandrhyno.png"
		}]
		}],
		uppertextblockadditionalclass:'rhunotextblockslide4',
		uppertextblock : [{
			textdata : data.string.p1text9,
			textclass : 'rhinodialogue1 dialogs right'
		},
		{
			textdata : data.string.p1text10,
			textclass : 'rhinodialogue2 dialogs right'
		}],

	},


	//slide4
	{
		contentblockadditionalclass : 'ole_temp_peach_background1',
		headerblockadditionalclass:'ole_temp_peach_header',
		headerblock: [{
			textdata : data.string.p1text1,
			textclass : 'center_align'
		}],
		uppertextblockadditionalclass:'rhunotextblockslide3',
		uppertextblock : [
		{
			textdata : data.string.p1text11,
			textclass : 'rhinodialogue4 dialogs right'
		},
		{
			datahighlightflag:true,
			datahighlightcustomclass:'green',
			textdata : data.string.p1text12,
			textclass : 'rhinodialogue4 dialogs right'
		},
		{

			textdata : data.string.p1text13,
			textclass : 'rhinodialogue5 dialogs right'
		},
		{
			textdata : data.string.p1text14,
			textclass : 'rhinodialogue6 dialogs right'
		}],

		imageblockadditionalclass:'imageblockclass',
		imageblock :[{
		imagestoshow : [{
			imgclass : "rhinoslide4",
			imgsrc : imgpath + "rhuno.png"
		},
		{
			imgclass : "sundari",
			imgsrc : imgpath + "sundari.png"
		},
		{
			imgclass : "money5 moneyslide5",
			imgsrc : imgpath + "rs100.png"
		},
		{
			imgclass : "money6 moneyslide5",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "money7 moneyslide5",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "money8 moneyslide5",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "money9 moneyslide5",
			imgsrc : imgpath + "rs500.png"
		},
		{
			imgclass : "watermelon1",
			imgsrc : imgpath + "watermelon.png"
		}]
		}],


	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole_temp_peach_background1',
		uppertextblockadditionalclass : 'centwidthcentheight',
		uppertextblock : [{
			textdata : data.string.p1text15,
			textclass : 'question'
		},
		{
			textdata : data.string.p1text16,
			textclass : 'rightoption ole-template-check-btn-default'
		},
		{
			textdata : data.string.p1text18,
			textclass : 'orclass'
		},
		{
			textdata : data.string.p1text17,
			textclass : 'wrongoption ole-template-check-btn-default'
		},
		{
			textdata : data.string.p1text19,
			textclass : 'cp'
		},
		{
			textdata : data.string.p1text20,
			textclass : 'sp'
		}],
		imageblockadditionalclass:'imageblockclass1',
		imageblock :[{
		imagestoshow : [{
			imgclass : "moneyrowCP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP",
			imgsrc : imgpath + "rs500.png"
		},

		{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs500.png"
		}]
		}]
	},
		//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole_temp_peach_background1',
		uppertextblockadditionalclass : 'centwidthcentheight',
		uppertextblock : [{
			textdata : data.string.p1text21,
			textclass : 'question1'
		},
		{
			textdata : data.string.p1text19,
			textclass : 'cp'
		},
		{
			textdata : data.string.p1text20,
			textclass : 'sp'
		},
		{
			textdata : data.string.p1text24,
			textclass : 'hint-block'
		}],
		imageblockadditionalclass:'imageblockclass1',
		imageblock :[{
		imagestoshow : [{
			imgclass : "moneyrowCP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP",
			imgsrc : imgpath + "rs500.png"
		},

		{
			imgclass : "moneyrowSP moneytoblink",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP",
			imgsrc : imgpath + "rs500.png"
		}]
		}],
		inputblock:[{
			inputdiv:'profitmoney',
			inputclass:'inputbutton ole-template-input-box-default',
			buttonblock: [
				{
					textdata: data.string.checktext,
					textclass: 'ole-template-check-btn-default check',
				},
				{
				textdata : data.string.hinttext,
				textclass : 'hint ole-template-check-btn-default'
				}
			]

		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole_temp_peach_background1',
		uppertextblockadditionalclass : 'centwidthcentheight',
		uppertextblock : [{
			textdata : data.string.p1text25,
			textclass : 'question2'
		}]

	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole_temp_peach_background1',
		uppertextblockadditionalclass : 'centwidthcentheight',
		uppertextblock : [{
			textdata : data.string.p1text26,
			textclass : 'question'
		},
		{
			textdata : data.string.p1text16,
			textclass : 'rightoption ole-template-check-btn-default'
		},
		{
			textdata : data.string.p1text18,
			textclass : 'orclass'
		},
		{
			textdata : data.string.p1text17,
			textclass : 'wrongoption ole-template-check-btn-default'
		},
		{
			textdata : data.string.p1text19,
			textclass : 'cp'
		},
		{
			textdata : data.string.p1text20,
			textclass : 'sp'
		}],
		imageblockadditionalclass:'imageblockclass2',
		imageblock :[{
		imagestoshow : [{
			imgclass : "moneyrowCP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP1",
			imgsrc : imgpath + "rs500.png"
		},
		{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs100.png"
		},
		{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs500.png"
		}]
		}]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole_temp_peach_background1',
		uppertextblockadditionalclass : 'centwidthcentheight',
		uppertextblock : [{
			textdata : data.string.p1text21,
			textclass : 'question1'
		},
		{
			textdata : data.string.p1text19,
			textclass : 'cp'
		},
		{
			textdata : data.string.p1text20,
			textclass : 'sp'
		},
		{
			textdata : data.string.p1text27,
			textclass : 'hint-block'
		}],
		imageblockadditionalclass:'imageblockclass2',
		imageblock :[{
		imagestoshow : [{
			imgclass : "moneyrowCP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowCP1",
			imgsrc : imgpath + "rs500.png"
		},

		{
			imgclass : "moneyrowSP1 moneytoblink",
			imgsrc : imgpath + "rs100.png"
		},
		{
			imgclass : "moneyrowSP1 moneytoblink",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs100.png"
		},{
			imgclass : "moneyrowSP1",
			imgsrc : imgpath + "rs500.png"
		}]
		}],
		inputblock:[{
			inputdiv:'profitmoney',
			inputclass:'inputbutton ole-template-input-box-default',
			buttonblock: [
				{
					textdata: data.string.checktext,
					textclass: 'ole-template-check-btn-default check',
				},
				{
				textdata : data.string.hinttext,
				textclass : 'hint ole-template-check-btn-default'
				}
			]

		}]
	},

];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2a", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p2b", src: soundAsset+"s1_p2_1.ogg"},
			{id: "s1_p2c", src: soundAsset+"s1_p2_2.ogg"},
			{id: "s1_p2d", src: soundAsset+"s1_p2_3.ogg"},
			{id: "s1_p3a", src: soundAsset+"s1_p3_1.ogg"},
			{id: "s1_p3b", src: soundAsset+"s1_p3_2.ogg"},
			{id: "s1_p3c", src: soundAsset+"s1_p3_3.ogg"},
			{id: "s1_p4a", src: soundAsset+"s1_p4_1.ogg"},
			{id: "s1_p4b", src: soundAsset+"s1_p4_2.ogg"},
			{id: "s1_p5a", src: soundAsset+"s1_p5_1.ogg"},
			{id: "s1_p5b", src: soundAsset+"s1_p5_2.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		$('.side').hide(0);
		$('.angle').hide(0);

		switch (countNext) {
			case 1:
			$('.dialogs, .rhunotextblock').hide(0);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p2a");
							current_sound.play();
							current_sound.on('complete', function(){
							$('.rhunotextblock').show(0);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p2b");
							current_sound.play();
							$('.rhinodialogue1').fadeIn();
							current_sound.on('complete', function(){
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p2c");
							current_sound.play();
							$('.rhinodialogue2').fadeIn();
							current_sound.on('complete', function(){
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p2d");
							current_sound.play();
							$('.rhinodialogue3').fadeIn();
							current_sound.on('complete', function(){
								nav_button_controls(0);
							});
						});
					});
				});
		    break;

		    case 2:
			$('.dialogs').hide(0);
		    $('rhunotextblockslide2').hide(0);
		    $('rhunotextblockslide2').delay(500).fadeIn();
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p3a");
				current_sound.play();
				$('.rhinodialogue3').fadeIn();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p3b");
				current_sound.play();
				$('.rhinodialogue1').fadeIn();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p3c");
				current_sound.play();
				$('.rhinodialogue2').fadeIn();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
		});
		    break;

		    case 3:
			$('.dialogs').hide(0);
			$('rhunotextblockslide4').hide(0);
		    $('rhunotextblockslide4').delay(500).fadeIn();
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p4a");
				current_sound.play();
				$('.rhinodialogue1').fadeIn();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p4b");
				current_sound.play();
				$('.rhinodialogue2').fadeIn();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
		    break;

		    case 4:
			$('.dialogs').hide(0);
			$('rhunotextblockslide3').hide(0);
		    $('rhunotextblockslide3').delay(500).fadeIn();
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p5a");
				current_sound.play();
				$('.rhinodialogue4').fadeIn();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p5b");
				current_sound.play();
				$('.rhinodialogue5').fadeIn();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
		    break;

			case 5:
        sound_player("s1_p6");
				$('.moneyrowSP,.sp,.moneyrowCP,.cp').hide(0);
				$('.wrongoption').click(function(){
				createjs.Sound.stop();
				play_correct_incorrect_sound(0);
				$('.moneyrowSP,.sp,.moneyrowCP,.cp').fadeIn();
         		$(this).addClass('blendred');
   				});

    			$('.rightoption').click(function(){
						createjs.Sound.stop();
    			 play_correct_incorrect_sound(1);
					 nav_button_controls(200);
         	$(this).addClass('blendgreen');
          		$('.rightoption,.wrongoption').unbind("click");
				$('.moneyrowSP,.sp,.moneyrowCP,.cp').fadeIn();
          		$('p').css('pointer-events','none');

  		 		});
				break;

			case 6:
				sound_player("s1_p7");
				$('.right-wrong').hide(0);
		        $('.hint-block').hide(0);
		        $(".ole-template-input-box-default").keydown(function(event){
			    		var charCode = (event.which) ? event.which : event.keyCode;
			    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			    		if(charCode === 13 && $('.check')!=null) {
					        $('.check').trigger("click");
						}
						var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
						//check if user inputs del, backspace or arrow keys
			   			if (!condition) {
			    			return true;
			    		}
			    		//check if user inputs more than one '.'
						if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
			        		return false;
			    		}
			    		//check . and 0-9 separately after checking arrow and other keys
			    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
			    			return false;
			    		}
			    		//check max no of allowed digits
			    		if (String(event.target.value).length >= max_number) {
			    			return false;
			    		}
			  			return true;
					});
			 	$('.check').click(function(){
		        	if(parseInt($('.ole-template-input-box-default').val())==100){
		        		$('.check').removeClass('ole-template-check-btn-default-incorrect');
		        		$('.check').addClass('ole-template-check-btn-default-correct');
						nav_button_controls(100);
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
		        	} else {
									createjs.Sound.stop();
		        		$('.check').addClass('ole-template-check-btn-default-incorrect');
						play_correct_incorrect_sound(0);
		        	}
		        });
		        $('.hint').click(function(){
		        	$('.hint-block').show(0);
					$(this).css('pointer-events','none');
		        	$('.moneytoblink').addClass('blinkmoney');
		        });
				break;

			case 8:
				 sound_player("s1_p9");
				$('.moneyrowSP1,.sp,.moneyrowCP1,.cp').hide(0);
				$('.wrongoption').click(function(){
					createjs.Sound.stop();
				play_correct_incorrect_sound(0);
				$('.moneyrowSP1,.sp,.moneyrowCP1,.cp').fadeIn();
         		$(this).addClass('blendred');
   				});

    			$('.rightoption').click(function(){
						createjs.Sound.stop();
    			 play_correct_incorrect_sound(1);
      nav_button_controls(100);
         		$(this).addClass('blendgreen');
          		$('.rightoption,.wrongoption').unbind("click");
				$('.moneyrowSP1,.sp,.moneyrowCP1,.cp').fadeIn();
          		$('p').css('pointer-events','none');

  		 		});
				break;

			case 9:
			sound_player("s1_p10");
				$('.right-wrong').hide(0);
		        $('.hint-block').hide(0);

				$(".ole-template-input-box-default").keydown(function(event){
			    		var charCode = (event.which) ? event.which : event.keyCode;
			    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			    		if(charCode === 13 && $('.check')!=null) {
					        $('.check').trigger("click");
						}
						var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
						//check if user inputs del, backspace or arrow keys
			   			if (!condition) {
			    			return true;
			    		}
			    		//check if user inputs more than one '.'
						if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
			        		return false;
			    		}
			    		//check . and 0-9 separately after checking arrow and other keys
			    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
			    			return false;
			    		}
			    		//check max no of allowed digits
			    		if (String(event.target.value).length >= max_number) {
			    			return false;
			    		}
			  			return true;
					});

			 	$('.check').click(function(){
		        	if(parseInt($('.ole-template-input-box-default').val())==200){
		        		$('.check').removeClass('ole-template-check-btn-default-incorrect');
		        		$('.check').addClass('ole-template-check-btn-default-correct');
						nav_button_controls(100);
								createjs.Sound.stop();
						play_correct_incorrect_sound(1);
		        	} else {
										createjs.Sound.stop();
		        		$('.check').addClass('ole-template-check-btn-default-incorrect');
						play_correct_incorrect_sound(0);
						nav_button_controls(100);
		        	}
		        });
		        $('.hint').click(function(){
		        	$('.hint-block').show(0);
		        	$('.moneytoblink').addClass('blinkmoney');
					$(this).css('pointer-events','none');
		        });
				break;

		default:
			sound_nav("s1_p"+(countNext+1));
			break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
				nav_button_controls(0);
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

  $nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});



	total_page = content.length;
	templateCaller();


	function table_resize(){
		$('td').height(0.12*$board.height());
	}

	$(window).resize(function() {
		table_resize();
	});
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

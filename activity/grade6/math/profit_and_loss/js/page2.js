var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		//slide 0
		contentblockadditionalclass: "ole_temp_peach_background1",
		headerblockadditionalclass: "ole_temp_peach_header zindex",
		headerblock:[
		{
			textclass : 'center_align',
			textdata : data.string.profit
		},
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock fadein",
		uppertextblock:[
		{
			textdata: data.string.p2text1
		},
		{
			textdata: data.string.p2text2
		}
		],
		leftimgblock:[{
			imagetoshow:[
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs500.png"
			}
			]
		}
		],
		rightimgblock:[{
			imagetoshow:[
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs500.png"
			}
			]
		}
		],
		lowertextblock:[
		{
			textclass: "lefttext",
			textdata: data.string.leftt
		},
		{
			textclass: "righttext",
			textdata: data.string.rightt
		},
		{
			textclass: "pllowertext",
			textdata: data.string.p2text3
		}
		],
	},
	{
		//slide 1
		contentblockadditionalclass: "ole_temp_peach_background1",
		headerblockadditionalclass: "ole_temp_peach_header",
		headerblock:[
		{
			textdata : data.string.profit
		},
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock fadein",
		uppertextblock:[
		{
			textdata: data.string.p2text4
		},
		{
			textdata: data.string.p2text5
		}
		],
		leftimgblock:[{
			imagetoshow:[
			{
				imgclass: "money tohide",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money tohide",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money tohide",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money tohide",
				imgsrc: imgpath + "p2/rs500.png"
			}
			]
		}
		],
		rightimgblock:[{
			imagetoshow:[
			{
				imgclass: "money money1",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money money2",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money tohide",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money tohide",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money tohide",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money tohide",
				imgsrc: imgpath + "p2/rs500.png"
			}
			]
		}
		],
		lowertextblock:[
		{
			textclass: "lefttext",
			textdata: data.string.leftt
		},
		{
			textclass: "righttext",
			textdata: data.string.rightt
		}
		],
	},
	{
		//slide 2
		contentblockadditionalclass: "ole_temp_peach_background1",
		headerblockadditionalclass: "ole_temp_peach_header",
		headerblock:[
		{
			textdata : data.string.profit
		},
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock fadein",
		uppertextblock:[
		{
			textdata: data.string.p2text4
		},
		{
			textdata: data.string.p2text6
		}
		],
		ribadditionalclass: "moveit",
		rightimgblock:[{
			imagetoshow:[
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			},
			{
				imgclass: "money",
				imgsrc: imgpath + "p2/rs100.png"
			}
			]
		}
		],
		lowertextblock:[
		{
			textclass: "righttext moveit2",
			textdata: data.string.righta
		}
		],
	},
	{
		//slide3
		uppertextblock:[
		{
			textclass: "ole_temp_diytext",
			textdata: data.string.diy
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "ole_temp_diyimg",
				imgsrc: "images/lokharke/2.png",
			}
			]
		}
		],
	},
	{
		//slide4
		uppertextblockadditionalclass: 'question-text',
		uppertextblock : [{
			datahighlightflag:true,
			datahighlightcustomclass:"red_color",
			textdata : data.string.p2text7,
			textclass : 'my_font_very_big title-2'
		},
		{
			textdata : data.string.p2text8,
			textclass : 'my_font_big',
		}],

		inputblock: [{
			inputdiv : 'ques_row check_div fade_in_1 div_0',
			textdata: data.string.p2text9,
			textclass: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_1 fade_in_1',
			textdata: data.string.p2text10,
			textclass: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_2 fade_in_1',
			textdata: data.string.p2text11,
			textclass: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		}
		],

		lowertextblockadditionalclass: 'hint-text hint_3',
		lowertextblock : [{
			textdata : data.string.p2text12,
			textclass : 'my_font_big'
		}
		],
	},
	{
		//slide5
		uppertextblockadditionalclass: 'question-text',
		uppertextblock : [{
			datahighlightflag:true,
			datahighlightcustomclass:"red_color",
			textdata : data.string.p2text7,
			textclass : 'my_font_very_big title-2'
		},
		{
			textdata : data.string.p2text13,
			textclass : 'my_font_big',
		}],

		inputblock: [{
			inputdiv : 'ques_row check_div fade_in_1 div_0',
			textdata: data.string.p2text9,
			textclass: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_1 fade_in_1',
			textdata: data.string.p2text10,
			textclass: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_2 fade_in_1',
			textdata: data.string.p2text11,
			textclass: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'default_btn my_font_big check_btn',
				},
				{
					textdata: data.string.hint,
					textclass: 'default_btn my_font_big hint_btn',
				}
			]
		}
		],

		lowertextblockadditionalclass: 'hint-text hint_3',
		lowertextblock : [{
			textdata : data.string.p2text12,
			textclass : 'my_font_big'
		}
		],
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s1_p1a", src: soundAsset+"s2_p1_1.ogg"},
			{id: "s1_p1b", src: soundAsset+"s2_p1_2.ogg"},
			{id: "s1_p1c", src: soundAsset+"s2_p1_3.ogg"},
			{id: "s1_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s1_p4a", src: soundAsset+"s2_p5_1.ogg"},
			{id: "s1_p4b", src: soundAsset+"s2_p5_2.ogg"},
			{id: "s2_p5_3", src: soundAsset+"s2_p5_3.ogg"},
			{id: "s1_p5", src: soundAsset+"s2_p6.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch(countNext){
			case 0:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p1a");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p1b");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p1c");
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
					});
				});
			});
			break;
			case 1:
			sound_nav("s1_p2");
				$(".money1,.money2").addClass("blink").delay(3000);
				setTimeout(function(){
					$(".money1,.money2").removeClass("blink");
					$(".tohide").fadeOut();
					$(".righttext,.lefttext").fadeOut();
				}, 3000);
			break;
			case 2:
			sound_nav("s1_p3");
			break;
			case 3:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 4:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p4a");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p4b");
			current_sound.play();
		});
			input_box('.div_0>.input_class', 5, '.div_0>.check_btn');
			input_box('.div_1>.input_class', 5, '.div_1>.check_btn');
			input_box('.div_2>.input_class', 5, '.div_2>.check_btn');
			$('.div_0>.check_btn').click(function(){
				if( parseInt( $('.div_0>.input_class').val() ) == 400){
					createjs.Sound.stop()
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
					play_correct_incorrect_sound(1);
					$('.div_0>.input_class').prop('disabled', true);
					$('.div_0>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_0>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_1').css('display', 'flex');
						$('.div_1').fadeIn(1000);
						$('.hint_1').removeClass('change_color_1');
					});
				} else {
					createjs.Sound.stop()
					play_correct_incorrect_sound(0);
					$(this).addClass('incorrect');
				}
			});
			$('.div_0>.hint_btn').click(function(){
				$('.hint_1').addClass('change_color_1');
			});

			$('.div_1>.check_btn').click(function(){
				if( parseInt( $('.div_1>.input_class').val() ) == 350){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
					play_correct_incorrect_sound(1);
					$('.div_1>.input_class').prop('disabled', true);
					$('.div_1>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_1>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_2').css('display', 'flex');
						$('.div_2').fadeIn(1000);
						$('.hint_2').removeClass('change_color_1');
					});
				} else {
					play_correct_incorrect_sound(0);
					$(this).addClass('incorrect');
				}
			});
			$('.div_1>.hint_btn').click(function(){
				$('.hint_2').addClass('change_color_1');
			});


			$('.div_2>.check_btn').click(function(){
				if( parseInt( $('.div_2>.input_class').val() ) == 50){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
					play_correct_incorrect_sound(1);
					$('.div_2>.input_class').prop('disabled', true);
					$('.div_2>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_2>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.hint_3').fadeIn(1000);
							sound_nav("s2_p5_3");
						// nav_button_controls(1000);
					});
				} else {
					play_correct_incorrect_sound(0);
					$(this).addClass('incorrect');
				}
			});
			$('.div_2>.hint_btn').click(function(){
				$('.hint_3').fadeIn(1000);
			});
			break;

			case 5:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p4a");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p5");
			current_sound.play();
		});
			input_box('.div_0>.input_class', 5, '.div_0>.check_btn');
			input_box('.div_1>.input_class', 5, '.div_1>.check_btn');
			input_box('.div_2>.input_class', 5, '.div_2>.check_btn');
			$('.div_0>.check_btn').click(function(){
				if( parseInt( $('.div_0>.input_class').val() ) == 2500){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
					createjs.Sound.stop();
					play_correct_incorrect_sound(1);
					$('.div_0>.input_class').prop('disabled', true);
					$('.div_0>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_0>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_1').css('display', 'flex');
						$('.div_1').fadeIn(1000);
						$('.hint_1').removeClass('change_color_1');
					});
				} else {
					createjs.Sound.stop();
					play_correct_incorrect_sound(0);
					$(this).addClass('incorrect');
				}
			});
			$('.div_0>.hint_btn').click(function(){
				$('.hint_1').addClass('change_color_1');
			});

			$('.div_1>.check_btn').click(function(){
				if( parseInt( $('.div_1>.input_class').val() ) == 2000){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
						play_correct_incorrect_sound(1);
					$('.div_1>.input_class').prop('disabled', true);
					$('.div_1>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_1>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_2').css('display', 'flex');
						$('.div_2').fadeIn(1000);
						$('.hint_2').removeClass('change_color_1');
					});
				} else {
						play_correct_incorrect_sound(0);
					$(this).addClass('incorrect');
				}
			});
			$('.div_1>.hint_btn').click(function(){
				$('.hint_2').addClass('change_color_1');
			});


			$('.div_2>.check_btn').click(function(){
				if( parseInt( $('.div_2>.input_class').val() ) == 500){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
						play_correct_incorrect_sound(1);
					$('.div_2>.input_class').prop('disabled', true);
					$('.div_2>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_2>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.hint_3').fadeIn(1000);
							sound_nav("s2_p5_3");
						// nav_button_controls(1000);
					});
				} else {
						play_correct_incorrect_sound(0);
					$(this).addClass('incorrect');
				}
			});
			$('.div_2>.hint_btn').click(function(){
				$('.hint_3').fadeIn(1000);
			});
			break;
		}
	}

	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
				nav_button_controls(0);
		});
	}
	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext < 4)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		createjs.Sound.stop();
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		createjs.Sound.stop();
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
        	$.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename = $(this).attr("data-highlightcustomclass")) :
            (stylerulename = "parsedstring") ;

            $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
            (stylerulename2 = "parsedstring2") ;

            $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
            (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
        });
        }
    }
    /*=====  End of data highlight function  ======*/

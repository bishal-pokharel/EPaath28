Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
}

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";

var content = [

    //ex1
    {
        exerciseblock: [{
            // boardata: data.string.exe1,
            textdata:data.string.excqn,
            imageblockadditionalclass: "sundarblock",
            imageblock: [{
                imagestoshow: [{
                        imgclass: "sundarright",
                        imgsrc: imgpath + "sundarright.png"
                    },
                    {
                        imgclass: "sundarwrong",
                        imgsrc: imgpath + "sundarwrong.png"
                    }
                ]
            }],
            dropbox: [{
                    dropclass: 'drop1',
                }, {
                    dropclass: 'drop2'
                },
                {
                    dropclass: 'drop3'
                },
                {
                    dropclass: 'drop4'
                },
                {
                    dropclass: 'drop5'
                },
                {
                    dropclass: 'drop6'
                },
                {
                    dropclass: 'drop7'
                }
            ],
            stathere: [{

                    statclass: "randomnumberhere",
                    statdata: data.string.p4_s48
                },
                {
                    statclass: "here1",
                    statdata: data.string.p4_s24
                },
                {
                    statclass: "here2"

                },
                {
                    statclass: "here3"
                },
                {
                    statclass: "here4"
                }, {
                    statclass: "here5"
                },
                {
                    statclass: "here6"
                },
                {
                    statclass: "here7"
                }
            ]
        }],
        flexblockcontainers: [{
            flexblockadditionalclass: "primeList",
            flexblock: [{
                flexboxcolumnclass: "column1",
                flexblockcolumn: [{
                    flexboxrowclass: "rowheader",
                    textdata: data.string.prime_num
                }]
            }],

        }],

        primecontainers: [{
            primeblock: [{

            }]
        }]
    }
];
//
// /*remove this for non random questions*/
// content.shufflearray();


$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var askedQues = [];

    /*for limiting the questions to 10*/
    var $total_page = content.length;
    /*var $total_page = content.length;*/
    var preload;
  	var timeoutvar = null;
  	var current_sound;
  	var timeout1;
  	function init() {
  		//specify type otherwise it will load assests as XHR
  		manifest = [
  			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
  			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
  			//   ,
  			//images
  			// soundsicon-orange
  			{id: "sound_0", src: soundAsset+"ex.ogg"},

  		];
  		preload = new createjs.LoadQueue(false);
  		preload.installPlugin(createjs.Sound);//for registering sounds
  		preload.on("progress", handleProgress);
  		preload.on("complete", handleComplete);
  		preload.on("fileload", handleFileLoad);
  		preload.loadManifest(manifest, true);
  	}
  	function handleFileLoad(event) {
  		// console.log(event.item);
  	}
  	function handleProgress(event) {
  		$('#loading-text').html(parseInt(event.loaded*100)+'%');
  	}
  	function handleComplete(event) {
  		$('#loading-wrapper').hide(0);
  		//initialize varibales
  		current_sound = createjs.Sound.play('sound_1');
  		current_sound.stop();
  		// call main function
  		templateCaller();
  	}
  	//initialize
  	init();

    function navigationcontroller(islastpageflag) {
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
            alert("NavigationController : Hi Master, please provide a boolean parameter") :
            null;
    }

    var testin = new EggTemplate();

    testin.init(10);

    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[0]);
        $board.html(html);

        $nextBtn.hide(0);
        $prevBtn.hide(0);
        $(".contentblock").css({"background-image": "url('"+imgpath+"bg.png')",
                                    "background-size": "100% 100%"});
        switch (countNext) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            sound_player("sound_"+(countNext));
                mainFunction()
            break;
        }

        function mainFunction() {
            var returnss = randomCompositeNumber();
            askedQues.push(returnss);
            var profactor = primeFactors(returnss);
            $('#drop1').addClass("inserthere");
            $(".rownormal p").click(function(){
                handleClickEvent($(this));
            });

            var dropPos = 1;
            function handleClickEvent($this) {
                var droppedvalue = $this.html();
                if (returnss % droppedvalue == 0) {
                    var divided = returnss / droppedvalue;
                    $("#drop"+ dropPos).html(droppedvalue).addClass("rightfac").removeClass("wrongfac");
                    $("#drop" + (dropPos+1)).addClass('inserthere');
                    $('.sundarright').show(0);
                    createjs.Sound.stop();
                    play_correct_incorrect_sound(1);
                    $('.sundarwrong').hide(0);
                    setTimeout(function() {
                        $(".sundarright").fadeOut();
                    }, 700);
                    $(".here"+dropPos).html(divided).css({
                        'display': 'block'
                    });
                     var nowDivide = $('.here'+dropPos).html();
                    returnss = divided;
                    if (nowDivide == 1) {

                        $('#drop' + (dropPos+1)).removeClass('inserthere');
                        testin.update(true);
                         $(".rownormal p").off('click');
                         $nextBtn.show(0);
                    }
                    dropPos++;
                } else {
                  createjs.Sound.stop();
                    $("#drop"+ dropPos).html(droppedvalue).addClass("wrongfac");
                    $('.sundarwrong').show(0);
                    play_correct_incorrect_sound(0);
                    setTimeout(function() {
                       $(".sundarwrong").fadeOut();
                    }, 700)
                    testin.update(false);
                }
            }
        }

        function randomCompositeNumber() {
            var randomnumber = giveMeRandNo();
              console.log("rand no: "+ randomnumber);
            var counter = 0;
            while(isPrime(randomnumber) != false || isNewNo(randomnumber) == false){
              counter++;
              console.log(counter);
              randomnumber = giveMeRandNo();
            }
            $(".randomnumberhere > p ").addClass(randomnumber).html(randomnumber);
            return randomnumber;
        }

        function isNewNo(number) {
          for(i=0; i<askedQues.length; i++){
            if(number == askedQues[i])
              return false;
          }
          return true;
        }

        function giveMeRandNo(){
            if(countNext < 3)
              var randomnumber = Math.floor(Math.random() * 22 + 8);
            else if(countNext >= 3 && countNext < 6)
              var randomnumber = Math.floor(Math.random() * (70 - 31) + 31);
            else
              var randomnumber = Math.floor(Math.random() * (100 - 70) + 70);

            return randomnumber;
        }

        function isPrime(value) {
            if(value%2 == 0 || value%3 == 0 || value%5 == 0 || value%7 == 0)
              return false;
            return true;
        }
        function primeFactors(number) {
            var factors = [];
            for (i = 2; i <= number; i++) {
                while ((number % i) === 0) {
                    factors.push(i);
                    number /= i;
                }
            }

            var profactor = [];
            var pointer_index = 0;

            for(var i=0; i < factors.length; i++){
              if(profactor.indexOf(factors[i]) == -1){
                profactor[profactor.length] = factors[i];
              }
            }

            var randomnumber;
            while(profactor.length<4){
                randomnumber = Math.floor(Math.random()*17)+3;
                 if(profactor.indexOf(randomnumber) == -1){
                profactor[profactor.length] = randomnumber;
              }
            }

            var temp;
            var innertemp;

            for (var j = 0; j < profactor.length; j++) {
                // temp = document.createElement('div');
                // temp.classList.add('rownormal', j);
                temp = $("<div class='rownormal "+j+"'></div>");
                $(".column1").append(temp);
            }

            for (var i = 0; i < profactor.length; i++) {
                innertemp = $("<p>");
                // innertemp = document.createElement('p');
                innertemp.html(profactor[i]);
                $("."+i).append(innertemp);
                // document.getElementsByClassName(i)[0].appendChild(innertemp)
            }
            $(".rownormal").each(function() {
                var divs = $(this).find('div');
                for (var i = 0; i < divs.length; i++) {
                    $(divs[i]).appendTo(this);
                }
            })
            return profactor;
        }




    }
    function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}

    function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    }

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
        testin.gotoNext();

    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        	previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

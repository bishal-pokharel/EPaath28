var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			textclass:"pgNmTxt",
			textdata:data.string.p5s1txt
		}]
	},
	// slide 2
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p5s2txt
		}]
	},
	// slide 3
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p5s3txt
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[
				{
					optdata:data.string.p5s3_op1
				},{
					optaddclass:"class1",
					optdata:data.string.p5s3_op2
				},{
					optdata:data.string.p5s3_op3
				}]
		}],
	},
	// slide 4
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			txtboxdivclass:"topOPnsCont",
			txtsindiv:[	{
					textclass:"optnLikeTxt optlt_1",
					textdata:data.string.p5s4_op1
			},{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt",
					textclass:"optnLikeTxt optlt_2",
					textdata:data.string.p5s4_op2
			},{
					textclass:"optnLikeTxt optlt_3",
					textdata:data.string.p5s4_op3
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p5s4txt
		}],
	},
	// slide 5
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			txtboxdivclass:"topOPnsCont",
			txtsindiv:[	{
					textclass:"optnLikeTxt optlt_1",
					textdata:data.string.p5s4_op1
			},{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt",
					textclass:"optnLikeTxt optlt_2",
					textdata:data.string.p5s4_op2
			},{
					textclass:"optnLikeTxt optlt_3",
					textdata:data.string.p5s4_op3
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p5s4txt
		}],
		exetype1:[{
			optionsdivclass:"optionsdivSec",
			exeoptions:[
				{
					optcontainerextra:"opnCntSec",
					optdata:data.string.p5s5_op1,
					startposextra:"corIncorSec",
				},{
					optcontainerextra:"opnCntSec",
					optaddclass:"class1",
					optdata:data.string.p5s5_op2,
					startposextra:"corIncorSec",
				},{
					optcontainerextra:"opnCntSec",
					optdata:data.string.p5s5_op3,
					startposextra:"corIncorSec",
				},{
					optcontainerextra:"opnCntSec",
					optdata:data.string.p5s5_op4,
					startposextra:"corIncorSec",
				}]
		}],
	},
	// slide 6
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			txtboxdivclass:"topOPnsCont",
			txtsindiv:[	{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt",
					textclass:"optnLikeTxt",
					textdata:data.string.p5s6_op1
			},{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt",
					textclass:"optnLikeTxt optlt_2",
					textdata:data.string.p5s4_op2
			},{
					textclass:"optnLikeTxt optlt_3",
					textdata:data.string.p5s4_op3
			}]
		},
		{
				datahighlightflag:true,
				datahighlightcustomclass:"undline",
				textclass:"btmBxTxt",
				textdata:data.string.p5s6_txt2

		}]
	},
	// slide 7
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			txtboxdivclass:"topOPnsCont",
			txtsindiv:[	{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt",
					textclass:"optnLikeTxt",
					textdata:data.string.p5s6_op1
			},{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt",
					textclass:"optnLikeTxt optlt_2",
					textdata:data.string.p5s4_op2
			},{
					textclass:"optnLikeTxt optlt_1",
					textdata:data.string.p5s4_op3
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p5s7_txt
		}]
	},
	// slide 8
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			txtboxdivclass:"topOPnsCont",
			txtsindiv:[	{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt",
					textclass:"optnLikeTxt",
					textdata:data.string.p5s6_op1
			},{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt",
					textclass:"optnLikeTxt optlt_2",
					textdata:data.string.p5s4_op2
			},{
					datahighlightflag:true,
					datahighlightcustomclass:"pinkTxt smlfnt",
					textclass:"optnLikeTxt",
					textdata:data.string.p5s7_op3
			}]
		}]
	},
	// slide 9
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			textclass:"primeComp prime",
			textdata:data.string.prime_num
		},{
			textclass:"primeComp comp",
			textdata:data.string.comp
		},{
			textclass:"sumaryTxt",
			textdata:data.string.smry
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"pinkTxt",
			textclass:"primeComp primeSmry hidn",
			textdata:data.string.smry_prime
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"pinkTxt",
			textclass:"primeComp compSmry hidn",
			textdata:data.string.smry_comp
		}],
		speechbox:[{
			speechbox:'sp-1-s9spbox',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p5s9txt
		}]
	},
	//slide 10
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			}]
		}],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal bg_brwn",
							textdata: data.string.number
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l1
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.l2
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.l3
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l4
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.l5
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l6
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.l7
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l8
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l9
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l10
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal bg_brwn",
							textdata: data.string.factors
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.f1
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.f2
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.f3
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.f4
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.f5
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.f6
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.f7
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.f8
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.f9
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.f10
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal bg_brwn",
							textdata: data.string.factors_num
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l1
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.l2
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.l2
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l3
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.l2
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l4
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.l2
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l4
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l3
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.l4
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal bg_brwn",
							textdata: data.string.poc
						},{
							flexboxrowclass :"rownormal vrysmlfnt",
							textdata: data.string.npnc
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.prime
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.prime
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.composite
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.prime
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.composite
						},{
							flexboxrowclass :"rownormal fnt_pnk",
							textdata: data.string.prime
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.composite
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.composite
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.composite
						}
						]
					},
				]
			}
		]
	}


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var setout1;
	var timeout1;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds01", src: imgpath+"clouds01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "p4_s1", src: soundAsset+"p4_s1.ogg"},
			{id: "p4_s2", src: soundAsset+"p4_s2.ogg"},
			{id: "p4_s3", src: soundAsset+"p4_s3.ogg"},
			{id: "p4_s4", src: soundAsset+"p4_s4.ogg"},
			{id: "p4_s6", src: soundAsset+"p4_s6.ogg"},
			{id: "p4_s7", src: soundAsset+"p4_s7.ogg"},
			{id: "p4_s9_instruction", src: soundAsset+"p4_s9_instruction.ogg"},
			{id: "p4_s9_prime", src: soundAsset+"p4_s9_prime.ogg"},
			{id: "p4_s9_composite", src: soundAsset+"p4_s9_composite.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);
		var count=0;



		switch(countNext) {
			case 0:
				sound_player("p4_s"+(countNext+1),1);
			break;
			case 1:
			$(".ones").addClass("pulseHghlght");
				sound_player("p4_s"+(countNext+1),1);
			break;
			case 2:
			case 4:
				sound_player("p4_s"+(countNext+1),0);
			break;
			case 8:
				var clkCount = 0;
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("p4_s9_instruction");
					current_sound.play();
					current_sound.on('complete', function(){
						// next?nav_button_controls():;
						$(".primeComp").click(function(){
							$(this).addClass("hidn");
							if($(this).hasClass("prime")){
								$(".primeSmry").removeClass("hidn");
								$(".primeComp").css("pointer-events","none");
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("p4_s9_prime");
									current_sound.play();
									current_sound.on('complete', function(){
										$(".primeComp").css("pointer-events","auto");
										$(".primeSmry").css("pointer-events","none");
										clkCount+=1;
										clkCount==2?nav_button_controls():'';
									});
							}else if ($(this).hasClass("comp")) {
								$(".compSmry").removeClass("hidn");
								$(".primeComp").css("pointer-events","none");
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("p4_s9_composite");
									current_sound.play();
									current_sound.on('complete', function(){
										$(".primeComp").css("pointer-events","auto");
										$(".compSmry").css("pointer-events","none");
										clkCount+=1;
										clkCount==2?nav_button_controls():'';
									});
							}
						});
					});
			break;
			case 9:
			setTimeout(function(){
				if($(".rownormal").hasClass("fnt_pnk")){
					$(".fnt_pnk").animate({
						color:"#9900ff"
					},1000,function(){
						$(".fnt_pnk").animate({
							color:"#000"
						},500,function(){
							$(".rownormal").not($(".fnt_pnk")).animate({
								color:"#9900ff"

							},1000);
						});
					});
				}
			},1000);
				nav_button_controls(2000);
			break;
			case 7:
				nav_button_controls(100);
			break;
			default:
				sound_player("p4_s"+(countNext+1),1);
			break;
		}
		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).addClass("corClass");
				$(this).siblings(".corctopt").show(0);
				$(".buttonsel").css("pointer-events","none");
				nav_button_controls(100);
			}else{
				$(this).addClass("incorClass");
				$(this).siblings(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
			}
		});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
	 	dropedImgCount = 0;
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	 	dropedImgCount = 0;
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

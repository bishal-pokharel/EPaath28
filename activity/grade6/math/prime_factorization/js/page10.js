var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			textclass:"wbxTxt pnktxt wbt_1",
			textdata:data.string.p8s1_txt_1
		},{
			textclass:"wbxTxt pnktxt wbt_2",
			textdata:data.string.p8s1_txt_2
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tpTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s1_txt
		}]
	},
	// slide 2
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s2_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-ylwtxt-hidn  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			}]
		}],
	},
	// slide 3
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo hidn",
			textclass:"sideEqn",
			textdata:data.string.p10s3_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s3_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans-ylwtxt  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			}]
		}],
	},
	// slide 4---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo hidn",
			textclass:"sideEqn",
			textdata:data.string.p10s4_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s4_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwtxt s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid s-3_2",
					textdata:data.string.l15
				}]
			}]
		}],
	},
	// slide 5
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s5_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid s-3_2",
					textdata:data.string.l15
				}]
			}]
		}],
	},
	// slide 6---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo hidn",
			textclass:"sideEqn",
			textdata:data.string.p10s6_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s6_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid s-3_2",
					textdata:data.string.l15
				}]
			}]
		}],
	},
	// slide 7---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo",
			textclass:"sideEqn",
			textdata:data.string.p10s6_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s7_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwtxt s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid s-3_2",
					textdata:data.string.l15
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-4_1",
					textdata:data.string.l5
				},{
					textclass:"ansMid s-4_2",
					textdata:data.string.l5
				}]
			}]
		}],
	},
	// slide 8---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo hidn",
			textclass:"sideEqn",
			textdata:data.string.p10s8_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s8_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwtxt s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid s-3_2",
					textdata:data.string.l15
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-4_1",
					textdata:data.string.l5
				},{
					textclass:"ansMid s-4_2",
					textdata:data.string.l5
				}]
			}]
		}],
	},
	// slide 9---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo",
			textclass:"sideEqn",
			textdata:data.string.p10s8_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s9_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid s-3_2",
					textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-pnktxt s-4_1",
					textdata:data.string.l5
				},{
					textclass:"ansMid s-4_2",
					textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 10---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s10_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid s-3_2",
					textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-4_1",
					textdata:data.string.l5
				},{
					textclass:"ansMid s-4_2",
					textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 11---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo",
			textclass:"sideEqn",
			textdata:data.string.p10s11_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s11_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans-pnktxt  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l60
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-pnktxt s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-pnktxt s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid s-3_2",
					textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-pnktxt s-4_1",
					textdata:data.string.l5
				},{
					textclass:"ansMid s-4_2",
					textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 12---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"textInSp",
			textdata:data.string.p10s12_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid-ylwbg s-1_2",
					textdata:data.string.hasht
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-2_2",
					// textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-3_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-3_2",
					// textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-4_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-4_2",
					// textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 13---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"textInSp",
			textdata:data.string.p10s13_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid-ylwbg s-1_2",
					textdata:data.string.hasht
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-2_2",
					// textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-3_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-3_2",
					// textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-4_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-4_2",
					// textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 14---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"textInSp",
			textdata:data.string.p10s14_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans-ylwbg  s-1_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.hasht
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-2_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-2_2",
					// textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-3_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-3_2",
					// textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-4_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-4_2",
					// textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 15---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"textInSp",
			textdata:data.string.p10s15_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans-ylwbg  s-1_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.hasht
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-2_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-2_2",
					// textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-3_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-3_2",
					// textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-4_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-4_2",
					// textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 16---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"textInSp",
			textdata:data.string.p10s16_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.hasht
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-2_2",
					// textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-3_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-3_2",
					// textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-4_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-4_2",
					// textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid-ylwbg",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 17---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"textInSp",
			textdata:data.string.p10s17_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.hasht
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-2_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-2_2",
					// textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-3_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-3_2",
					// textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans s-4_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-4_2",
					// textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid-ylwbg",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
	// slide 18---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"textInSp",
			textdata:data.string.p10s18_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans-ylwbg  s-1_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.hasht
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-2_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-2_2",
					// textdata:data.string.l30
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-3_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-3_2",
					// textdata:data.string.l15
				}]
			},
			{
				fctxt:[{
					textclass:"lftans-ylwbg s-4_1",
					textdata:data.string.hasht
				},{
					textclass:"ansMid s-4_2",
					// textdata:data.string.l5
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},

	// slide 19
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s19_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-ylwtxt-hidn  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l420
				}]
			}]
		}],
	},
	// slide 20
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo hidn",
			textclass:"sideEqn",
			textdata:data.string.p10s3_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p10s20_txt
		}],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans-ylwtxt  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l420
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l210
				}]
			}]
		}],
	},
	// slide 21---sidediv
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"topTxt",
			textdata:data.string.p10txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"obo",
			textclass:"sideEqn",
			textdata:data.string.p10eqn_1
		}],
		// speechbox:[{
		// 	speechbox:'sp-1',
		// 	imgclass:'',
		// 	imgid:"clouds",
		// 	imgsrc:'',
		// 	datahighlightflag:true,
		// 	datahighlightcustomclass:"underline",
		// 	textclass:"textInSp",
		// 	textdata:data.string.p10s18_txt
		// }],
		divisionblock:[{
			divisionblkclass:"divisioncontainer",
			dblock:[{
				fctxt:[{
					textclass:"lftans  s-1_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-1_2",
					textdata:data.string.l420
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-2_1",
					textdata:data.string.l2
				},{
					textclass:"ansMid s-2_2",
					textdata:data.string.l210
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-3_1",
					textdata:data.string.l3
				},{
					textclass:"ansMid-hidn s-3_2",
					textdata:data.string.l105
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-4_1",
					textdata:data.string.l5
				},{
					textclass:"ansMid-hidn s-4_2",
					textdata:data.string.l35
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"lftans-hidn s-5_1",
					textdata:data.string.l7
				},{
					textclass:"ansMid-hidn s-5_2",
					textdata:data.string.l7
				}]
			},
			{
				factdivadditionalclass:"nobrdr",
				fctxt:[{
					textclass:"ansMid-hidn s-6_2",
					textdata:data.string.l1
				}]
			}
		]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var timeout1;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds01", src: imgpath+"clouds01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds02", src: imgpath+"clouds02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "multiply_black", src: imgpath+"multiply_black.png", type: createjs.AbstractLoader.IMAGE},
			{id: "multiply_yellow", src: imgpath+"multiply_yellow.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"1.ogg"},
			{id: "sound_1", src: soundAsset+"2.ogg"},
			{id: "sound_2", src: soundAsset+"3.ogg"},
			{id: "sound_3", src: soundAsset+"4.ogg"},
			{id: "sound_4", src: soundAsset+"5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);
		var count=0;

		function appendMul(toApendCls, clsNum){
			$(toApendCls).append("<img class='mulsgn mls"+clsNum+"' src='"+preload.getResult("multiply_black").src+"'>");
		}
		switch(countNext) {
			case 1:
				nav_button_controls(100);
			break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				sound_player_nav("sound_0");
				$(".obo:eq(0)").fadeIn(200);
				$(".obo:eq(1)").delay(500).fadeIn(200);
				$(".obo:eq(2)").delay(1000).fadeIn(200);
			break;
			case 20:
				showFactor(2,3,1);
			break;
			default:
				nav_button_controls(100);
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).addClass("corClass");
				$(this).siblings(".corctopt").show(0);
				$(".buttonsel").css("pointer-events","none");
				nav_button_controls(100);
			}else{
				$(this).addClass("incorClass");
				$(this).siblings(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
			}
		});
	}

	function showFactor(lftCount,rghtCount,divCount){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play("sound_0");
		current_sound.play();
		$(".sideEqn").html(eval("data.string.p10eqn_"+lftCount));
		current_sound.on('complete',function(){
			$(".s-"+lftCount+"_1, .s-"+rghtCount+"_2").fadeIn(100);
			$(".factdiv:eq("+divCount+")").removeClass("nobrdr");
			lftCount+=1;
			rghtCount+=1;
			divCount+=1;
			if(rghtCount<=6){
				showFactor(lftCount,rghtCount,divCount);
			}	else{
				nav_button_controls(100);
			}
		});

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeout1);
	 	dropedImgCount = 0;
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		clearTimeout(timeout1);
		templateCaller();
	 	dropedImgCount = 0;
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			textclass:"pgNmTxt",
			textdata:data.string.diy
		}]
	},
	// slide 2
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p6s2txt
		}]
	},
	// slide 3  --clkcolChange
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s3txt
		}],
	},
	// slide 4 --explanation
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft hidn',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s4txt
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p6s4txt_1
		}]
	},
	// slide 5--clkcolChange
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s5txt
		}],
	},
	// slide 6 --explanation
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft hidn',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s6txt
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p6s6txt_1
		}]
	},
	// slide 7--clkcolChange
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s7txt
		}],
	},
	// slide 8 --explanation
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft hidn',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s8txt
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p6s8txt_1
		}]
	},
	// slide 9--clkcolChange
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s9txt
		}],
	},
	// slide 10 --explanation
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft hidn',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s10txt
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p6s10txt_1
		}]
	},
	// slide 11
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft hidn',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s11txt
		}],
		// speechbox:[{
		// 	speechbox:'sp-2',
		// 	imgclass:'',
		// 	imgid:"text_box",
		// 	imgsrc:'',
		// 	textclass:"textInSp",
		// 	textdata:data.string.p6s10txt_1
		// }]
	},
	// slide 12
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		extratextblock:[{
			textclass:"tpexp",
			textdata:data.string.p6s11txt
		}],
		speechbox:[{
			speechbox:'sp-3',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p6s12txt
		}]
	},
	// slide 13
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		speechbox:[{
			speechbox:'sp-3',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p6s13txt
		}]
	},
	// slide 14
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		numberscontainer:[{
			numberblockclass:"hundContainer",
			textclass:"tpPrm",
			textdata:data.string.prime_num
		}],
		speechbox:[{
			speechbox:'sp-3',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p6s14txt
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var timeout1;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds01", src: imgpath+"clouds01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "p5_s2", src: soundAsset+"p5_s2.ogg"},
			{id: "p5_s3", src: soundAsset+"p5_s3.ogg"},
			{id: "p5_s4", src: soundAsset+"p5_s4.ogg"},
			{id: "p5_s5", src: soundAsset+"p5_s5.ogg"},
			{id: "p5_s6", src: soundAsset+"p5_s6.ogg"},
			{id: "p5_s7", src: soundAsset+"p5_s7.ogg"},
			{id: "p5_s8", src: soundAsset+"p5_s8.ogg"},
			{id: "p5_s9", src: soundAsset+"p5_s9.ogg"},
			{id: "p5_s10", src: soundAsset+"p5_s10.ogg"},
			{id: "p5_s11", src: soundAsset+"p5_s11.ogg"},
			{id: "p5_s12", src: soundAsset+"p5_s12.ogg"},
			{id: "p5_s13", src: soundAsset+"p5_s13.ogg"},
			{id: "p5_s14", src: soundAsset+"p5_s14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);
		var count=0;


		function numTableGenerator(){
			//function to create a table of numbers
			for(var i=1;i<=100;i++){
				$(".hundContainer").append("<div class='numdiv nd"+i+"'></div>");
			}
			for(var i=1;i<=100;i++){
				$(".nd"+i).append("<p class='number n'>"+i+"</p>");
			}
		}
		var count  = 1;

		//function to change the color of boxes according to number
		function bgChngr(count,num,bgClass){
			if(count%num==0){
				$(".nd"+count).addClass(bgClass);
				count+=1;
				if(count<=100){
					timeout1 = setTimeout(function(){
						bgChngr(count,num,bgClass);
					},100);
				}else{
					nav_button_controls(200);
				}
			}else{
				count+=1;
				bgChngr(count,num,bgClass);
			}
		}

		// function to control the bgcolor according to clks
		function numClkController(clkNum,num,bgClass){
			if((clkNum%num) == 0){
				play_correct_incorrect_sound(1);
				$(".numdiv").css("pointer-events","none");
				bgChngr(count,num,bgClass);
			}else{
				play_correct_incorrect_sound(0);
			}
		}


		function colorBg(numb,bgcol){
			for(var i=1;i<=100;i++){
				if((i%numb)==0){
					$(".nd"+i).addClass(bgcol);
				}
			}
		}

		function getPrimes(max) {
			var sieve = [], i, j, primes = [];
			for (i = 2; i <= max; ++i) {
					if (!sieve[i]) {
							// i has not been marked -- it is prime
							primes.push(i);
							for (j = i << 1; j <= max; j += i) {
									sieve[j] = true;
							}
					}
			}
			return primes;
		}
		function getComposite(max) {
			var sieve = [], i, j, comps = [];
			for (i = 2; i <= max; ++i) {
					if (!sieve[i]) {
							// i has not been marked -- it is prime
							// primes.push(i);
							for (j = i << 1; j <= max; j += i) {
									sieve[j] = true;
							}
					}else{
						comps.push(i);
					}
			}
			return comps;
		}
		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
				sound_player("p5_s"+(countNext+1),1);
			break;
			case 2:
			sound_player("p5_s"+(countNext+1),0);
				numTableGenerator();
				$(".sidedivprime").append("<p class='prmNum'>"+data.string.pr1+"</p>");
				var divNUm = 2;
				$(".numdiv").click(function(){
					clkNum  = $(this).find("p").text();
					numClkController(clkNum,divNUm,"pnkBg");
				});
			break;
			case 3:
				sound_player("p5_s"+(countNext+1),1);
				numTableGenerator();
				var divNUm = 2;
				colorBg(divNUm,"pnkBg");
					$(".sidedivprime").append("<p class='prmNum'>"+data.string.pr1+"</p>");
						$(".sp-2").animate({
							left:"60%"
						},1000,function(){
							$(".fish-fishLft").removeClass("hidn");

						});
			break;
			case 4:
				sound_player("p5_s"+(countNext+1),0);
				numTableGenerator();
				$(".sidedivprime").append("<p class='prmNum'>"+data.string.pr2+"</p>");
				colorBg(2,"pnkBg");
				$(".numdiv").click(function(){
					createjs.Sound.stop();
					clkNum  = $(this).find("p").text();
					numClkController(clkNum,3,"creamBg");
				});
			break;
			case 5:
				sound_player("p5_s"+(countNext+1),1);
				numTableGenerator();
				var divNUm = 3;
				colorBg(divNUm,"creamBg");
				colorBg(2,"pnkBg");
				$(".sidedivprime").append("<p class='prmNum'>"+data.string.pr2+"</p>");
					$(".sp-2").animate({
						left:"60%"
					},1000,function(){
						$(".fish-fishLft").removeClass("hidn");


					});

			break;
			case 6:
				sound_player("p5_s"+(countNext+1),0);
				numTableGenerator();
				$(".sidedivprime").append("<p class='prmNum'>"+data.string.pr3+"</p>");
				var divNUm = 3;
				colorBg(2,"pnkBg");
				colorBg(3,"creamBg");
				$(".numdiv").click(function(){
					createjs.Sound.stop();
					clkNum  = $(this).find("p").text();
					numClkController(clkNum,5,"grnBg");
				});
			break;
			case 7:
				sound_player("p5_s"+(countNext+1),1);
				numTableGenerator();
				colorBg(2,"pnkBg");
				colorBg(3,"creamBg");
				colorBg(5,"grnBg");
					$(".sidedivprime").append("<p class='prmNum'>"+data.string.pr3+"</p>");
						$(".sp-2").animate({
							left:"60%"
						},1000,function(){
							$(".fish-fishLft").removeClass("hidn");


						});

			break;
			case 8:
				sound_player("p5_s"+(countNext+1),0);
				$(".sidedivprime").append("<p class='prmNum'>"+data.string.pr4+"</p>");
				numTableGenerator();
				var divNUm = 3;
				colorBg(2,"pnkBg");
				colorBg(3,"creamBg");
				colorBg(5,"grnBg");
				$(".numdiv").click(function(){
						createjs.Sound.stop();
					clkNum  = $(this).find("p").text();
					numClkController(clkNum,7,"prpl");
				});
			break;
			case 9:
				sound_player("p5_s"+(countNext+1),1);
				numTableGenerator();
				colorBg(2,"pnkBg");
				colorBg(3,"creamBg");
				colorBg(5,"grnBg");
				colorBg(7,"prpl");
					$(".sidedivprime").append("<p class='prmNum'>"+data.string.pr4+"</p>");
						$(".sp-2").animate({
							left:"60%"
						},1000,function(){
							$(".fish-fishLft").removeClass("hidn");


						});

			break;
			case 10:
				sound_player("p5_s"+(countNext+1),1);
				numTableGenerator();
				colorBg(2,"pnkBg");
				colorBg(3,"creamBg");
				colorBg(5,"grnBg");
				colorBg(7,"prpl");
				var prmCont = getPrimes(100);
				var prmLength = (prmCont.length)-1;
				for(var i=0;i<=prmLength;i++){
					i<prmLength?$(".sidedivprime").append("<div class='prnDv'>"+prmCont[i]+","+"</div>"):$(".sidedivprime").append("<div class='prnDv'>"+prmCont[i]+"</div>");
				}

			break;
			case 11:

				numTableGenerator();
				var prmCont = getPrimes(100);
				var prmLength = (prmCont.length)-1;
				for(var i=0;i<=prmLength;i++){
					i<prmLength?$(".sidedivprime").append("<div class='prnDv'>"+prmCont[i]+","+"</div>"):$(".sidedivprime").append("<div class='prnDv'>"+prmCont[i]+"</div>");
					$(".nd"+prmCont[i]).addClass("prpl");
				}
					sound_player("p5_s"+(countNext+1),1);
			break;
			case 12:
				numTableGenerator();
				var compCont = getComposite(100);
				var compLength = (compCont.length)-1;
				console.log(compCont);
				$(".tpPrm").hide(0);
				for(var i=0;i<=compLength;i++){
					i<compLength?$(".sidedivprime").append("<div class='cmpDv'>"+compCont[i]+","+"</div>"):$(".sidedivprime").append("<div class='cmpDv'>"+compCont[i]+"</div>");
					$(".nd"+compCont[i]).addClass("prpl");
				}
					sound_player("p5_s"+(countNext+1),1);
			break;
			case 13:
				numTableGenerator();
				var prmCont = getPrimes(100);
				var compCont = getComposite(100);
				var prmLength = (prmCont.length)-1;
				for(var i=0;i<=prmLength;i++){
					i<prmLength?$(".sidedivprime").append("<div class='prnDv'>"+prmCont[i]+","+"</div>"):$(".sidedivprime").append("<div class='prnDv'>"+prmCont[i]+"</div>");
					$(".nd"+prmCont[i]).addClass("pnkBg");
				}
				for(var i=0;i<compCont.length;i++){
					$(".nd"+compCont[i]).addClass("red");
				}
					sound_player("p5_s"+(countNext+1),1);
			break;
			default:
				nav_button_controls(100);
		}
		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).addClass("corClass");
				$(this).siblings(".corctopt").show(0);
				$(".buttonsel").css("pointer-events","none");
				nav_button_controls(100);
			}else{
				$(this).addClass("incorClass");
				$(this).siblings(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
			}
		});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(0):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeout1);
	 	dropedImgCount = 0;
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		clearTimeout(timeout1);
		templateCaller();
	 	dropedImgCount = 0;
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

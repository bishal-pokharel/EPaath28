var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";

var preload;
var timeoutArr = [];
var timeoutvar = null;

var content = [

  //slide 0

  {
    extratextblock: [{
      textclass: "title",
      textdata: data.string.title,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "bg",
        imgsrc: '',
        imgid: 'bg',
      },
    ]
  }]
},

//slide 1

{

  bubblebox:[{
    bubbleimg: '',
    bubbleclass:'clouds',
    bubbletextclass: 'text1',
    bubbletextdata: data.string.p1text1,
    imgid: 'bg'
  }],

  extratextblock: [{
    textclass: "factor",
    textdata: data.string.factor,
    datahighlightflag: true,
    datahighlightcustomclass: 'grenLtr'
  },
  ],
  imageblock: [{
    imagestoshow: [{
      imgclass: "bg",
      imgsrc: '',
      imgid: 'bg',
    },
    {
      imgclass: "fish",
      imgsrc: '',
      imgid: 'fish',
    },
  ],

}]
},

//slide 2

{

  bubblebox:[
    {
      bubbleimg: '',
      bubbleclass:'clouds',
      bubbletextclass: 'text1',
      bubbletextdata: data.string.p1text2,
      imgid: 'bg'
    },
  ],

  imageblock: [{
    imagestoshow: [{
      imgclass: "bg",
      imgsrc: '',
      imgid: 'bg',
    },
    {
      imgclass: "fish",
      imgsrc: '',
      imgid: 'fish',
    },

  ],

}]
},

//slide 3

{


  extratextblock: [

    {

      textclass: 'info',
      textdata: data.string.p1text3,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'factor1',
      textdata: data.string.p1text8,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'factor2',
      textdata: data.string.p1text8,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'eq',
      textdata: data.string.p1text5,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'two1',
      textdata: data.string.p1text6,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'three1',
      textdata: data.string.p1text7,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'one',
      textdata: '1',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'two',
      textdata: '2',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'three',
      textdata: '3',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'four',
      textdata: '4',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'five',
      textdata: '5',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {

      textclass: 'six',
      textdata: '6',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    }

  ],

  bubblebox:[
    {
      bubbleimg: '',
      bubbleclass:'clouds',
      bubbletextclass: 'text1',
      bubbletextdata: data.string.p1text4,
    },
  ],

  imageblock: [{
    imagestoshow: [{
      imgclass: "bg",
      imgsrc: '',
      imgid: 'bg',
    },
    {
      imgclass: "fish",
      imgsrc: '',
      imgid: 'fish',
    },
    {
      imgclass: "rectanglebox",
      imgsrc: '',
      imgid: 'rectanglebox',
    },
    {
      imgclass: "shell1",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell2",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell3",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell4",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell5",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell6",
      imgsrc: '',
      imgid: 'shell',
    },
  ],

}]
},


//slide 4

{

  extratextblock: [
    {
      textclass: "info",
      textdata: data.string.p1text3,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },

    {
      textclass: "eq",
      textdata: data.string.p1text5,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },


  ],

  bubblebox:[
    {
      bubbleimg: '',
      bubbleclass:'clouds',
      bubbletextclass: 'text1',
      bubbletextdata: data.string.p1text9,
    },
  ],

  imageblock: [{
    imagestoshow: [
      {
        imgclass: "bg",
        imgsrc: '',
        imgid: 'bg',
      },
      {
        imgclass: "rectanglebox",
        imgsrc: '',
        imgid: 'rectanglebox',
      },
      {
        imgclass: "fish",
        imgsrc: '',
        imgid: 'fish',
      },
      {
        imgclass: "shell shell1",
        imgsrc: '',
        imgid: 'shell',
      },
      {
        imgclass: "shell shell2",
        imgsrc: '',
        imgid: 'shell',
      },
      {
        imgclass: "shell shell3",
        imgsrc: '',
        imgid: 'shell',
      },
      {
        imgclass: "shell4",
        imgsrc: '',
        imgid: 'shell',
      },
      {
        imgclass: "shell5",
        imgsrc: '',
        imgid: 'shell',
      },
      {
        imgclass: "shell6",
        imgsrc: '',
        imgid: 'shell',
      },
    ],

  }]
},

//slide 5

{

  extratextblock: [
    {
      textclass: "info",
      textdata: data.string.p1text3,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "factor1",
      textdata: data.string.p1text8,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "factor2",
      textdata: data.string.p1text8,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "eq",
      textdata: data.string.p1text11,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "two1",
      textdata: data.string.p1text12,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "three1",
      textdata: data.string.p1text13,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "one",
      textdata: '1',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "two",
      textdata: '2',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "three",
      textdata: '3',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "four",
      textdata: '4',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "five",
      textdata: '5',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "six",
      textdata: '6',
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    }

  ],

  bubblebox:[
    {
      bubbleimg: '',
      bubbleclass:'clouds',
      bubbletextclass: 'text1',
      bubbletextdata: data.string.p1text10,
    },
  ],

  imageblock: [{
    imagestoshow: [{
      imgclass: "bg",
      imgsrc: '',
      imgid: 'bg',
    },
    {
      imgclass: "fish",
      imgsrc: '',
      imgid: 'fish',
    },
    {
      imgclass: "rectanglebox1",
      imgsrc: '',
      imgid: 'rectanglebox1',
    },
    {
      imgclass: "shell1",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell2",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell3",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell4",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell5",
      imgsrc: '',
      imgid: 'shell',
    },
    {
      imgclass: "shell6",
      imgsrc: '',
      imgid: 'shell',
    },
  ],

}]
},

//slide 6

{

  extratextblock: [
    {
      textclass: "info",
      textdata: data.string.p1text3,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },

    {
      textclass: "eq",
      textdata: data.string.p1text11,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "eq1",
      textdata: data.string.p1text5,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "two11",
      textdata: data.string.p1text6,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "three11",
      textdata: data.string.p1text7,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "one11",
      textdata: data.string.p1text12,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "six11",
      textdata: data.string.p1text13,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "line1",
      textdata: data.string.p1text11,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "line2",
      textdata: data.string.p1text5,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "line3",
      textdata: data.string.p1text14,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },
    {
      textclass: "line4",
      textdata: data.string.p1text15,
      datahighlightflag: true,
      datahighlightcustomclass: 'grenLtr'
    },

  ],

  bubblebox:[
    {
      bubbleimg: '',
      bubbleclass:'clouds',
      bubbletextclass: 'text1',
      bubbletextdata: data.string.p1text16,
    },
  ],

  imageblock: [{
    imagestoshow: [{
      imgclass: "bg",
      imgsrc: '',
      imgid: 'bg',
    },
    {
      imgclass: "fish",
      imgsrc: '',
      imgid: 'fish',
    },
    {
      imgclass: "rectangleboxshell1",
      imgsrc: '',
      imgid: 'rectangleboxshell1',
    },
    {
      imgclass: "rectangleboxshell2",
      imgsrc: '',
      imgid: 'rectangleboxshell2',
    },

  ],

}]
},


]

$(function() {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  loadTimelineProgress($total_page, countNext + 1);


  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images

      {id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
      {id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
      {id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rectanglebox", src: imgpath+"rectangle_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "shell", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rectanglebox1", src: imgpath+"rectangle_box_six.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rectangleboxshell1", src: imgpath+"rectangle_box_shell.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rectangleboxshell2", src: imgpath+"rectangle_box_six_shell.png", type: createjs.AbstractLoader.IMAGE},


      // sounds
      {id: "sound_0", src: soundAsset+"p1_s1.ogg"},
      {id: "sound_1", src: soundAsset+"p1_s2.ogg"},
      {id: "sound_2", src: soundAsset+"p1_s3.ogg"},
      {id: "sound_3a", src: soundAsset+"p1_s4.ogg"},
      {id: "p1_s5", src: soundAsset+"p1_s5.ogg"},
      {id: "p1_s6", src: soundAsset+"p1_s6.ogg"},
      {id: "p1_s7", src: soundAsset+"p1_s7.ogg"},
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded*100)+'%');
  }
  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  /*===============================================
  =            data highlight function            =
  ===============================================*/
  /**

  What it does:
  - send an element where the function has to see
  for data to highlight
  - this function searches for all nodes whose
  data-highlight element is set to true
  -searches for # character and gives a start tag
  ;span tag here, also for @ character and replaces with
  end tag of the respective
  - if provided with data-highlightcustomclass value for highlight it
  applies the custom class or else uses parsedstring class

  E.g: caller : texthighlight($board);
  */
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }

  /*=====  End of data highlight function  ======*/

  /*===============================================
  =            user notification function        =
  ===============================================*/
  /**
  How to:
  - First set any html element with
  "data-usernotification='notifyuser'" attribute,
  and "data-isclicked = ''".
  - Then call this function to give notification
  */

  /**
  What it does:
  - You send an element where the function has to see
  for data to notify user
  - this function searches for all text nodes whose
  data-usernotification attribute is set to notifyuser
  - applies event handler for each of the html element which
  removes the notification style.
  */
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one('click', function() {
        /* Act on the event */
        $(this).attr('data-isclicked', 'clicked');
        $(this).removeAttr('data-usernotification');
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
  =            Navigation Controller Function            =
  ======================================================*/
  /**
  How To:
  - Just call the navigation controller if it is to be called from except the
  last page of lesson
  - If called from last page set the islastpageflag to true such that
  footernotification is called for continue button to navigate to exercise
  */

  /**
  What it does:
  - If not explicitly overriden the method for navigation button
  controls, it shows the navigation buttons as required,
  according to the total count of pages and the countNext variable
  - If for a general use it can be called from the templateCaller
  function
  - Can be put anywhere in the template function as per the need, if
  so should be taken out from the templateCaller function
  - If the total page number is
  */

  function navigationcontroller(islastpageflag){
    typeof islastpageflag === "undefined" ?
    islastpageflag = false :
    typeof islastpageflag != 'boolean'?
    alert("NavigationController : Hi Master, please provide a boolean parameter") :
    null;

}
  /*=====  End of user navigation controller function  ======*/

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
  =            Templates Block            =
  =======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var source = $("#general-template").html();
  var template = Handlebars.compile(source);

  function generalTemplate() {
    var html = template(content[countNext]);
    $board.html(html);
    vocabcontroller.findwords(countNext);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    put_image(content, countNext);


    // $nextBtn.show();
    $('.clouds').attr('src', preload.getResult('clouds').src);
    $('.rectangle').attr('src', preload.getResult('clouds').src);

    $('.clouds').hide();
    $('.text1').hide();
    $('.fish').hide();
    $('.info').hide();
    $('.shell1').hide();
    $('.shell2').hide();
    $('.shell3').hide();
    $('.shell4').hide();
    $('.shell5').hide();
    $('.shell6').hide();
    $('.one').hide();
    $('.two').hide();
    $('.three').hide();
    $('.four').hide();
    $('.five').hide();
    $('.six').hide();
    $('.rectanglebox').hide();
    $('.rectanglebox1').hide();
    $('.rectangleboxshell1').hide();
    $('.rectangleboxshell2').hide();
    $('.eq1').hide();
    $('.one11').hide();
    $('.three11').hide();
    $('.two11').hide();
    $('.six11').hide();
    $('.line1').hide();
    $('.line2').hide();
    $('.line3').hide();
    $('.line4').hide();

    $('.two1').hide();
    $('.three1').hide();
    $('.eq').hide();
    $('.factor1').hide();
    $('.factor2').hide();


    switch(countNext){

      case 0:
      case 1:
      case 2:
        sound_nav("sound_"+countNext);
        $('.clouds').show();
        $('.text1').show();
        $('.fish').show();
        $('.info').show();
      break;

      case 3:

      setTimeout(function () {
        $('.info').fadeIn(1000);
      },100);

      setTimeout(function () {
        $('.fish').fadeIn(1000);
      },1000);

      setTimeout(function () {
        $('.text1').fadeIn(1500);
        $('.clouds').fadeIn(1500);

      },2000);

      setTimeout(function () {
        $('.rectanglebox').fadeIn(1500);
        $('.two1').fadeIn(1500);
        $('.three1').fadeIn(1500);
        $('.eq').fadeIn(1500);

      },3000);

      setTimeout(function () {
        $('.shell1').fadeIn(1500);
        $('.one').fadeIn(1500);
      },4000);


      setTimeout(function () {
        $('.shell2').fadeIn(1500);
        $('.two').fadeIn(1500);

      },5000);

      setTimeout(function () {
        $('.shell3').fadeIn(1500);
        $('.three').fadeIn(1500);

      },6000);

      setTimeout(function () {
        $('.shell4').fadeIn(1500);
        $('.four').fadeIn(1500);

      },7000);
      setTimeout(function () {
        $('.shell5').fadeIn(1500);
        $('.five').fadeIn(1500);

      },8000);
      setTimeout(function () {
        $('.shell6').fadeIn(1500);
        $('.six').fadeIn(1500);

      },9000);

      setTimeout(function () {
        $('.factor1').fadeIn(1500);
        $('.two1').css({'background-color': '#fffd99',
        'width': '5%',
        'height': '8%','border-radius':'73px','padding':'1%','color':'black','z-index':'6'});
      },9500);

      setTimeout(function () {
        $('.three1').css({'background-color': '#fffd99',
        'width': '5%',
        'height': '8%','border-radius':'73px','padding':'1%','color':'black'});
        $('.factor2').fadeIn(1500);

      },11000);

      createjs.Sound.stop();
  		current_sound = createjs.Sound.play("sound_3a");
  		current_sound.play();
  		current_sound.on("complete", function(){
          nav_button_controls(0);
      });
      break;

      case 4:
      // sound_nav("sound_"+(countNext));case
        sound_nav("p1_s"+(countNext+1));
      $('.clouds').show();
      $('.text1').show();
      $('.fish').show();
      $('.info').show();
      $('.shell1').show();
      $('.shell2').show();
      $('.shell3').show();
      $('.shell4').show();
      $('.shell5').show();
      $('.shell6').show();
      $('.rectanglebox').show();

      setTimeout(function () {
        scatterShells()
      },1000);
      // nav_button_controls(1000);
      break;

      case 5:
        sound_nav("p1_s"+(countNext+1));

      $('.two1').css({'bottom': '20%','right':'43.8%','z-index':'5'});
      $('.three1').css({'bottom': '31%','right':'21.5%'});
      $('.eq').css({'right': '8%','bottom':'8%'});

      $('.two1').css({'background-color': '',
      'width': '',
      'height': '','border-radius':'','padding':'','color':''});

      $('.three1').css({'background-color': '',
      'width': '',
      'height': '','border-radius':'','padding':'','color':''});

      $('.factor1').css({'bottom': '12%', 'right': '43%'});
      $('.factor2').css({'right': '20%'});


      $('.shell1').css({'right': '36.8%'});
      $('.one').css({'right': '36.8%'});
      $('.shell2').css({'right': '30.4%'});
      $('.two').css({'right': '30.4%'});
      $('.shell3').css({'right': '24.1%'});
      $('.three').css({'right': '24.1%'});
      $('.shell4').css({'bottom': '20%','right':'17.8%'});
      $('.four').css({'bottom': '17%','right':'17.8%'});
      $('.shell5').css({'bottom': '20%','right':'11.6%'});
      $('.five').css({'bottom': '17%','right':'11.6%'});
      $('.shell6').css({'bottom': '20%','right':'5.2%'});
      $('.six').css({'bottom': '17%','right':'5.2%'});

        $('.info').show(0);

      setTimeout(function () {
        $('.fish').fadeIn(1000);
      },1000);

      setTimeout(function () {
        $('.text1').fadeIn(1500);
        $('.clouds').fadeIn(1500);
      },2000);

      setTimeout(function () {
        $('.rectanglebox1').fadeIn(1500);
        $('.two1').fadeIn(1500);
        $('.three1').fadeIn(1500);
        $('.eq').fadeIn(1500);
        // nav_button_controls(2000);
      },3000);

      setTimeout(function () {
        $('.shell1').fadeIn(1500);
        $('.one').fadeIn(1500);
      },4000);


      setTimeout(function () {
        $('.shell2').fadeIn(1500);
        $('.two').fadeIn(1500);

      },5000);

      setTimeout(function () {
        $('.shell3').fadeIn(1500);
        $('.three').fadeIn(1500);

      },6000);

      setTimeout(function () {
        $('.shell4').fadeIn(1500);
        $('.four').fadeIn(1500);

      },7000);
      setTimeout(function () {
        $('.shell5').fadeIn(1500);
        $('.five').fadeIn(1500);

      },8000);
      setTimeout(function () {
        $('.shell6').fadeIn(1500);
        $('.six').fadeIn(1500);

      },9000);

      setTimeout(function () {
        $('.factor1').fadeIn(1500);
        $('.two1').css({'background-color': '#fffd99',
        'width': '5%',
        'height': '8%','border-radius':'73px','padding':'1%','color':'black'});
      },9500);

      setTimeout(function () {
        $('.three1').css({'background-color': '#fffd99',
        'width': '5%',
        'height': '8%','border-radius':'73px','padding':'1%','color':'black'});
        $('.factor2').fadeIn(1500);

      },11000);

      setTimeout(function () {
        navigationcontroller();
      },12500)


      break;

      case 6:

      // $('.two1').css({'bottom': '51%',
      //     'left': '4%'});
      //
      // $('.three1').css({'bottom': '69%',
      //     'left': '19%'});

      $('.info').css({'top': '2%',
      'padding': '1%'});

      $('.eq').css({'top': '47%',
      'right': '12.2%'});

      $('.eq1').css({'top': '57%',
      'left': '6.2%'});

      setTimeout(function () {
        $('.info').fadeIn(1000);
      },100);

      setTimeout(function () {
        $('.rectangleboxshell1').fadeIn(1000);
      },1000);

      setTimeout(function () {
        $('.eq1').fadeIn(1500);

      },2000);

      setTimeout(function () {
        $('.two11').fadeIn(1500);
      },3000);

      setTimeout(function () {
        $('.three11').fadeIn(1500);
      },4000);


      setTimeout(function () {
        $('.rectangleboxshell2').fadeIn(1000);

      },5000);

      setTimeout(function () {
        $('.eq').fadeIn(1000);

      },6000);

      setTimeout(function () {
        $('.one11').fadeIn(1000);

      },7000);

      setTimeout(function () {
        $('.six11').fadeIn(1000);

      },8000);

      setTimeout(function () {
        $('.line1').fadeIn(1000);
      },9000);

      setTimeout(function () {
        $('.line2').fadeIn(1000);
      },10000);

      setTimeout(function () {
        $('.line3').fadeIn(1000);
      },11000);

      setTimeout(function () {
        $('.line4').fadeIn(1000);
      },12000);

      setTimeout(function () {
        $('.fish').fadeIn(1000);
      },13000);

      setTimeout(function () {
        sound_nav("p1_s"+(countNext+1));
        $('.text1').fadeIn(1500);
        $('.clouds').fadeIn(1500);
        // nav_button_controls(1000);
      },14000);
      break;


      default:

      break;

    }

  }

  function scatterShells(){

    $('.rectanglebox').hide();

    $('.shell').css({'bottom':'15%'});

    $('.shell1').css({'transform':'rotate(-25deg)','right':'21%'});
    $('.shell2').css({'transform':'rotate(20deg)','right':'15%'});
    $('.shell3').css({'transform':'rotate(-10deg)','right':'9%'});
    $('.shell4').css({'transform':'rotate(40deg)','right':'21%'});
    $('.shell5').css({'transform':'rotate(-40deg)'});
    $('.shell6').css({'right':'10%'});

    navigationcontroller();

  }

  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
  function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
  /*=====  End of Templates Block  ======*/

  /*==================================================
  =            Templates Controller Block            =
  ==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
  Motivation :
  - Make a single function call that handles all the
  template load easier

  How To:
  - Update the template caller with the required templates
  - Call template caller

  What it does:
  - According to value of the Global Variable countNext
  the slide templates are updated
  */

  function templateCaller() {
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page, countNext + 1);

    /*
    //   for (var i = 0; i < content.length; i++) {
    //     slides(i);
    //     $($('.totalsequence')[i]).html(i);
    //     $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
    //   "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
    //   }
    //   function slides(i){
    //       $($('.totalsequence')[i]).click(function(){
    //         countNext = i;
    //         templateCaller();
    //       });
    //     }
    */




    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  function put_image(content, count){
    if(content[count].hasOwnProperty('imageblock')){
      var imageblock = content[count].imageblock[0];
      if(imageblock.hasOwnProperty('imagestoshow')){
        var imageClass = imageblock.imagestoshow;
        for(var i=0; i<imageClass.length; i++){
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = ('.'+classes_list[classes_list.length-1]);
          $(selector).attr('src', image_src);
        }
      }
    }
  }


  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  //templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on('click', function() {
    console.log('nxt clicked');
    clearTimeout(timeoutvar);
    createjs.Sound.stop();
    for(var i=0; i<timeoutArr.length; i++){
      clearTimeout(timeoutArr[i]);
    }
    switch(countNext) {
      default:
      countNext++;
      templateCaller();
      break;
    }
  });

  $refreshBtn.click(function(){
    console.log('$refreshBtn clicked')
    templateCaller();
  });

  $prevBtn.on('click', function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    for(var i=0; i<timeoutArr.length; i++){
      clearTimeout(timeoutArr[i]);
    }
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  /*=====  End of Templates Controller Block  ======*/
});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			textclass:"wbxTxt pnktxt wbt_1",
			textdata:data.string.p8s1_txt_1
		},{
			textclass:"wbxTxt pnktxt wbt_2",
			textdata:data.string.p8s1_txt_2
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"pnktxt underline",
			textclass:"topTxt",
			textdata:data.string.p8s1_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s1_txt_3
		}]
	},
	// slide 2
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			textclass:"wbxTxt pnktxt wbt_1",
			textdata:data.string.p8s1_txt_1
		},{
			textclass:"wbxTxt pnktxt wbt_2",
			textdata:data.string.p8s1_txt_2
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"pnktxt underline",
			textclass:"topTxt",
			textdata:data.string.p8s1_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s2_txt
		}]
	},
	// slide 3
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s3_txt_1
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv hidn",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.hasht
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1 hidn",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2 hidn",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas ylwbx t60 ch-1 hidn",
					textclass:"childtxt ct-1",
					textdata:data.string.hasht
				},{
					childclass:"childclas ylwbx t60 ch-2 hidn",
					textclass:"childtxt ct-2",
					textdata:data.string.hasht
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra hidn",
				textdata:data.string.factors
			},{
				textclass:"brnchXtra hidn",
				textdata:data.string.branches
			}]
		}]
	},
	// slide 4 --2blocks tree
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s4_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.hasht
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.hasht
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.hasht
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2 hidn",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2 hidn",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas  ylwbx t30 ch-1_2 hidn",
					textclass:"childtxt ct-1_2",
					textdata:data.string.hasht
				},{
					childclass:"childclas ylwbx  t30 ch-2_2 hidn",
					textclass:"childtxt ct-2_2",
					textdata:data.string.hasht
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}]
	},
	// slide 5
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s5_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv whitebx-pnktxt pdBig",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l12
				}],
				// branchline:[{
				// 	branchlinecontainerclass:"branchCont t45 bc-1",
				// 	textclass:"branchLine bl-1",
				// },{
				// 	branchlinecontainerclass:"branchCont t45 bc-2",
				// 	textclass:"branchLine bl-2",
				// }],
				// childnode:[{
				// 	childclass:"childclas t60 ch-1",
				// 	textclass:"childtxt ct-1",
				// 	textdata:data.string.hasht
				// },{
				// 	childclass:"childclas t60 ch-2",
				// 	textclass:"childtxt ct-2",
				// 	textdata:data.string.hasht
				// }]
			}],
		}]
	},
	// slide 6
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s6_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1 fbBig",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l12
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45-hidn bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45-hidn bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 whitebx-pnktxt-hidn ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2
				},{
					childclass:"childclas t60 whitebx-pnktxt-hidn ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l6
				}]
			}],
		}]
	},
	// slide 7
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s7_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1 fbBig",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l12
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 whitebx-pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l6
				}]
			}],
		}]
	},
	// slide 8
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s8_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1 fbBig",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l12
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60  ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2
				},{
					childclass:"childclas t60 whitebx-pnktxt ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l6
				}]
			}],
		}]
	},
	// slide 9--2blocks tree
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s9_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l12
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 ch-1",
					textclass:"childtxt-pnktxt ct-1",
					textdata:data.string.l2
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l6
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont-hidn tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont-hidn tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas-hidn t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l2
				},{
					childclass:"childclas-hidn  t30 ch-2_2",
					textclass:"childtxt-pnktxt ct-2_2",
					textdata:data.string.l3
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}]
	},
	// slide 10--2blocks tree
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p81s0_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l12
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 ch-1",
					textclass:"childtxt-pnktxt ct-1",
					textdata:data.string.l2
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l6
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l2
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt-pnktxt ct-2_2",
					textdata:data.string.l3
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}]
	},
	// slide 11
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"pnktxt",
			textclass:"midEqn",
			textdata:data.string.p8s11_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s11_txt
		}]
	},
	// slide 12
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s12_txt
		}]
	},



	// slide 13
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s13_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv whitebx-pnktxt pdBig",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l60
				}],
				// branchline:[{
				// 	branchlinecontainerclass:"branchCont t45 bc-1",
				// 	textclass:"branchLine bl-1",
				// },{
				// 	branchlinecontainerclass:"branchCont t45 bc-2",
				// 	textclass:"branchLine bl-2",
				// }],
				// childnode:[{
				// 	childclass:"childclas t60 ch-1",
				// 	textclass:"childtxt ct-1",
				// 	textdata:data.string.hasht
				// },{
				// 	childclass:"childclas t60 ch-2",
				// 	textclass:"childtxt ct-2",
				// 	textdata:data.string.hasht
				// }]
			}],
		}]
	},
	// slide 14
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s14_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l60
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l30
				}]
			}],
		}]
	},
	// slide 15
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s15_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l60
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l30
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l2
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l15
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}]
	},
	// slide 16
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s16_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l60
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l30
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l15
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}]
	},
	// slide 17
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s17_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l60
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l30
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l15
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.p8s17_op1
			},{
				optaddclass:"class1",
				optdata:data.string.p8s17_op2
			},{
				optdata:data.string.p8s17_op3
			},{
				optdata:data.string.p8s17_op4
			}]
		}]
	},
	// slide 18
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s18_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l60
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l30
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l15
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		},
		{
			factorblockclass:"factorblock fb-3",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_3",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_3",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_3",
					textclass:"childtxt-pnktxt ct-1_3",
					textdata:data.string.l3,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_3",
					textclass:"childtxt-pnktxt ct-2_3",
					textdata:data.string.l5,
					extrachildtxt:[{
						textclass:"primetxt pt-2",
						textdata:data.string.prime
					}]
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}]
	},
	// slide 19
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"pnktxt",
			textclass:"midEqn",
			textdata:data.string.p8s19_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s19_txt
		}]
	},



	// slide 20
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s20_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv whitebx-pnktxt pdBig",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				// branchline:[{
				// 	branchlinecontainerclass:"branchCont t45 bc-1",
				// 	textclass:"branchLine bl-1",
				// },{
				// 	branchlinecontainerclass:"branchCont t45 bc-2",
				// 	textclass:"branchLine bl-2",
				// }],
				// childnode:[{
				// 	childclass:"childclas t60 ch-1",
				// 	textclass:"childtxt ct-1",
				// 	textdata:data.string.hasht
				// },{
				// 	childclass:"childclas t60 ch-2",
				// 	textclass:"childtxt ct-2",
				// 	textdata:data.string.hasht
				// }]
			}],
		}]
	},
	// slide 21
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s21_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l50
				}]
			}],
		}]
	},
	// slide 22
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s22_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l50
				}]
			}],
		}]
	},
	// slide 23
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s23_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l50
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt ct-1_2",
					textdata:data.string.qnmrk
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.qnmrk
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.p8s23_op1
			},{
				optaddclass:"class1",
				optdata:data.string.p8s23_op2
			},{
				optdata:data.string.p8s23_op3
			},{
				optdata:data.string.p8s23_op4
			}]
		}]
	},
	// slide 24
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s24_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l50
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l5,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l10
				}]
			}],
			// xtrainfotxt:[{
			// 	textclass:"fctrxtra fctSec hidn",
			// 	textdata:data.string.l10
			// }]
		}]
	},
	// slide 25
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s25_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l50
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l5,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l10
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		},
		{
			factorblockclass:"factorblock fb-3",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_3",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_3",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_3",
					textclass:"childtxt ct-1_3",
					textdata:data.string.l2,
					// extrachildtxt:[{
					// 	textclass:"primetxt",
					// 	textdata:data.string.prime
					// }]
				},{
					childclass:"childclas  t30 ch-2_3",
					textclass:"childtxt ct-2_3",
					textdata:data.string.l5,
					// extrachildtxt:[{
					// 	textclass:"primetxt pt-2",
					// 	textdata:data.string.prime
					// }]
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}]
	},
	// slide 26
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s26_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l50
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l5,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l10
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		},
		{
			factorblockclass:"factorblock fb-3",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_3",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_3",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_3",
					textclass:"childtxt ct-1_3",
					textdata:data.string.l2,
					// extrachildtxt:[{
					// 	textclass:"primetxt",
					// 	textdata:data.string.prime
					// }]
				},{
					childclass:"childclas  t30 ch-2_3",
					textclass:"childtxt ct-2_3",
					textdata:data.string.l5,
					// extrachildtxt:[{
					// 	textclass:"primetxt pt-2",
					// 	textdata:data.string.prime
					// }]
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opnCntSec",
				optaddclass:"class1",
				optdata:data.string.p8s26_op1
			},{
				optcontainerextra:"opnCntSec",
				optdata:data.string.p8s26_op2
			}]
		}]
	},
	// slide 27
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s27_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l50
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l5,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l10
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		},
		{
			factorblockclass:"factorblock fb-3",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_3",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_3",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_3",
					textclass:"childtxt-pnktxt ct-1_3",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_3",
					textclass:"childtxt-pnktxt ct-2_3",
					textdata:data.string.l5,
					extrachildtxt:[{
						textclass:"primetxt pt-2",
						textdata:data.string.prime
					}]
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}]
	},
	// slide 28
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s28_txt
		}],
		factorblock:[{
			factorblockclass:"factorblock fb-1",
			fctblk:[{
				fstparentdivclass:"parentdiv",
				parentnode:[{
					textclass:"factortxt",
					textdata:data.string.l100
				}],
				branchline:[{
					branchlinecontainerclass:"branchCont t45 bc-1",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont t45 bc-2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t60 pnktxt ch-1",
					textclass:"childtxt ct-1",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas t60 ch-2",
					textclass:"childtxt ct-2",
					textdata:data.string.l50
				}]
			}],
		},
		{
			factorblockclass:"factorblock fb-2",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_2",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_2",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_2",
					textclass:"childtxt-pnktxt ct-1_2",
					textdata:data.string.l5,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_2",
					textclass:"childtxt ct-2_2",
					textdata:data.string.l10
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		},
		{
			factorblockclass:"factorblock fb-3",
			fctblk:[{
				branchline:[{
					branchlinecontainerclass:"branchCont tp0 bc-1_3",
					textclass:"branchLine bl-1",
				},{
					branchlinecontainerclass:"branchCont tp0 bc-2_3",
					textclass:"branchLine bl-2",
				}],
				childnode:[{
					childclass:"childclas t30 ch-1_3",
					textclass:"childtxt-pnktxt ct-1_3",
					textdata:data.string.l2,
					extrachildtxt:[{
						textclass:"primetxt",
						textdata:data.string.prime
					}]
				},{
					childclass:"childclas  t30 ch-2_3",
					textclass:"childtxt-pnktxt ct-2_3",
					textdata:data.string.l5,
					extrachildtxt:[{
						textclass:"primetxt pt-2",
						textdata:data.string.prime
					}]
				}]
			}],
			xtrainfotxt:[{
				textclass:"fctrxtra fctSec hidn",
				textdata:data.string.factors
			}]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.p8s28_op1
			},{
				optaddclass:"class1",
				optdata:data.string.p8s28_op2
			},{
				optdata:data.string.p8s28_op3
			},{
				optdata:data.string.p8s28_op4
			}]
		}]
	},
	// slide 29
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"tptxt",
			textdata:data.string.p8s3_txt
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"pnktxt",
			textclass:"midEqn",
			textdata:data.string.p8s29_txt_1
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p8s29_txt
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var timeout1;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds01", src: imgpath+"clouds01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds02", src: imgpath+"clouds02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "multiply_black", src: imgpath+"multiply_black.png", type: createjs.AbstractLoader.IMAGE},
			{id: "multiply_yellow", src: imgpath+"multiply_yellow.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "p7_s1", src: soundAsset+"p7_s1.ogg"},
			{id: "p7_s2", src: soundAsset+"p7_s2.ogg"},
			{id: "p7_s3", src: soundAsset+"p7_s3.ogg"},
			{id: "p7_s4", src: soundAsset+"p7_s4.ogg"},
			{id: "p7_s5", src: soundAsset+"p7_s5.ogg"},
			{id: "p7_s6", src: soundAsset+"p7_s6.ogg"},
			{id: "p7_s7", src: soundAsset+"p7_s7.ogg"},
			{id: "p7_s8", src: soundAsset+"p7_s8.ogg"},
			{id: "p7_s9", src: soundAsset+"p7_s9.ogg"},
			{id: "p7_s10", src: soundAsset+"p7_s10.ogg"},
			{id: "p7_s11", src: soundAsset+"p7_s11.ogg"},
			{id: "p7_s12", src: soundAsset+"p7_s12.ogg"},
			{id: "p7_s13", src: soundAsset+"p7_s13.ogg"},
			{id: "p7_s14", src: soundAsset+"p7_s14.ogg"},
			{id: "p7_s15", src: soundAsset+"p7_s15.ogg"},
			{id: "p7_s16", src: soundAsset+"p7_s16.ogg"},
			{id: "p7_s17", src: soundAsset+"p7_s17.ogg"},
			{id: "p7_s18", src: soundAsset+"p7_s18.ogg"},
			{id: "p7_s19", src: soundAsset+"p7_s19.ogg"},
			{id: "p7_s20", src: soundAsset+"p7_s20.ogg"},
			{id: "p7_s21", src: soundAsset+"p7_s21.ogg"},
			{id: "p7_s22", src: soundAsset+"p7_s22.ogg"},
			{id: "p7_s23", src: soundAsset+"p7_s23.ogg"},
			{id: "p7_s24", src: soundAsset+"p7_s24.ogg"},
			{id: "p7_s25", src: soundAsset+"p7_s25.ogg"},
			{id: "p7_s26", src: soundAsset+"p7_s26.ogg"},
			{id: "p7_s27", src: soundAsset+"p7_s27.ogg"},
			{id: "p7_s28", src: soundAsset+"p7_s28.ogg"},
			{id: "p7_s29", src: soundAsset+"p7_s29.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/

	 	/*for randomizing the options*/
	 	function randomize(parent){
	 		var parent = $(parent);
	 		var divs = parent.children();
	 		while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 		}
	 	}
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);
		var count=0;

		function appendMul(toApendCls, clsNum){
			$(toApendCls).append("<img class='mulsgn mls"+clsNum+"' src='"+preload.getResult("multiply_black").src+"'>");
		}
		switch(countNext) {
			case 1:
				$(".wbt_2").animate({
					right:"-30%"
				},1000,function(){
					$(".wbt_1").animate({
						left:"35%"
					},1000);
				});
					sound_player("p7_s"+(countNext+1),1);
			break;
			case 2:
				appendMul(".ch-1",1);
				$(".parentdiv").show(0);
				$(".ch-1").delay(100).fadeIn(100);
				$(".ch-2").delay(250).fadeIn(100);
				$(".fctrxtra").delay(850).fadeIn(100);
					$(".bc-1").delay(1850).fadeIn(100);
					$(".bc-2").delay(2100).fadeIn(100);
					$(".brnchXtra").delay(3000).fadeIn(100);
						sound_player("p7_s"+(countNext+1),1);
			break;
			case 3:
				appendMul(".ch-1",1);
				appendMul(".ch-1_2",2);
				$(".ct-2").addClass("ylwcrcl");
				$(".bc-1_2").delay(1000).fadeIn(300);
				$(".bc-2_2").delay(1500).fadeIn(300);
				$(".ch-1_2").delay(2000).fadeIn(300);
				$(".ch-2_2").delay(2500).fadeIn(300);
				$(".fctSec").delay(3000).fadeIn(300);
					sound_player("p7_s"+(countNext+1),1);
			break;
			case 5:
				appendMul(".ch-1",1);
				$(".sp-1").css({
					"width":"49%",
					"height":"44%"
				});
				$(".mulsgn").css({"left":"121%"});
				$(".ch-1, .bc-1").delay(100).fadeIn(200);
					// $(".textInSp").html(eval("data.string.p8s6_txt_1"));
					$(".ch-2, .bc-2").delay(100).fadeIn(200);
						sound_player("p7_s"+(countNext+1),1);

				// createjs.Sound.stop();
				// current_sound = createjs.Sound.play("sound_0");
				// current_sound.play();
				// $(".ch-1, .bc-1").delay(100).fadeIn(200);
				// current_sound.on('complete',function(){
				// 	$(".textInSp").html(eval("data.string.p8s6_txt_1"));
				// 		createjs.Sound.stop();
				// 		current_sound = createjs.Sound.play("sound_0");
				// 		current_sound.play();
				// 		$(".ch-2, .bc-2").delay(100).fadeIn(200);
				// 		current_sound.on('complete',function(){
				// 			nav_button_controls(100);
				// 		});
				// });
			break;
			case 8:
				appendMul(".ch-1_2",2);
				$(".bc-1_2, .ch-1_2").delay(500).fadeIn(500);
				$(".bc-2_2, .ch-2_2").delay(1000).fadeIn(500);
					sound_player("p7_s"+(countNext+1),1);
			break;
			case 16:
			case 22:
			case 25:
			case 27:
				randomize(".optionsdiv");
					sound_player("p7_s"+(countNext+1),0);
			break;
			case 17:
			case 18:
				sound_player("p7_s"+(countNext+1),1);
			break;
			default:
				sound_player("p7_s"+(countNext+1),1);
			break;
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).addClass("corClass");
				$(this).siblings(".corctopt").show(0);
				$(".buttonsel").css("pointer-events","none");
				nav_button_controls(100);
			}else{
				$(this).addClass("incorClass");
				$(this).siblings(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
			}
		});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeout1);
	 	dropedImgCount = 0;
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		clearTimeout(timeout1);
		templateCaller();
	 	dropedImgCount = 0;
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

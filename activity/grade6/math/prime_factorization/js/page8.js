var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";

var content = [
  {
    uppertextblock:[{
      textclass:"diytxt",
      textdata:data.string.diytext
    }]
  },
  //slide 1
  {
      uppertextblockadditionalclass: "blackboard",
      contentnocenteradjust: true,
      contentblockadditionalclass:"bgimg",
      uppertextblock: [{
          textclass: "description2",
          datahighlightflag: true,
          datahighlightcustomclass: "custompinkhighlight",
          // textdata: data.string.dnd
      },
      {
          textid: "factorizevalue1",
          textdata: data.string.p6_s11
      }, {
          textid: "factorizevalue2",
          textdata: data.string.p6_s12
      }, {
          textid: "factorizevalue3",
          textdata: data.string.p6_s13
      }, {
          textid: "factorizevalue4",
          textdata: data.string.p6_s14
      }, {
          textid: "factorizevalue5",
          textdata: data.string.p6_s15
      }, {
          textid: "factorizevalue6",
          textdata: data.string.p6_s16
      }, {
          textid: "factorizevalue7",
          textdata: data.string.p6_s17
      }, {
          textid: "factorizevalue8",
          textdata: data.string.p6_s18
      }, {
          textid: "factorizevalue9",
          textdata: data.string.p6_s19
      }, {
          textid: "factorizevalueSo",
          textdata: data.string.p6_s29
      }],
      imageblockadditionalclass: "blackboard2imageblock",
      imageblock: [{
          imagestoshow: [{
              imgclass: "line1",
              imgsrc: imgpath + "line.png"
          }, {
              imgclass: "line2",
              imgsrc: imgpath + "line.png"
          }, {
              imgclass: "line3",
              imgsrc: imgpath + "line.png"
          }, {
              imgclass: "line4",
              imgsrc: imgpath + "line.png"
          }, {
              imgclass: "imagecorrect",
              imgsrc: imgpath + "sundarright.png"
          }, {
              imgclass: "imagewrong",
              imgsrc: imgpath + "sundarwrong.png"
          }]
      }],


      flexblockcontainers: [{
          flexblockadditionalclass: "primeList",
          flexblock: [{
              flexboxcolumnclass: "column1",
              flexblockcolumn: [{
                  flexboxrowclass: "rowheader",
                  textdata: data.string.prime_num
              }, {
                  flexboxrowclass: "rownormal",
                  textdata: data.string.p2_s45
              }, {
                  flexboxrowclass: "rownormal",
                  textdata: data.string.p2_s46
              }, {
                  flexboxrowclass: "rownormal",
                  textdata: data.string.p2_s48
              }, {
                  flexboxrowclass: "rownormal",
                  textdata: data.string.p2_s50
              }]
          }]
      }],

      lowertextblockadditionalclass: 'yellowlowertextblock5',
      lowertextblock: [{
          textclass: "description",
          datahighlightflag: true,
          datahighlightcustomclass: "custompinkhighlight",
          textdata: data.string.dnd
      }],
      playagainvalue: true,
      playagainvalue: data.string.p6_s25
  }
];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  var arrayset = [];
  // 2 to 100 because we want to generate prime numbers between 2 to 100
  for(var i = 2; i <= 100; i++){
  		arrayset[i-2] = i;
  }

  var index1 = 0;
  var index2;
  var arraynotcomplete = true;
  var divisor;
  // This is the code for sieve of Eratosthenes Algorithm concept
	while (index1 < arrayset.length) {
		divisor = arrayset[index1];
		index2 = index1;
		while (index2 < arrayset.length) {
			if (((arrayset[index2] % divisor) == 0) && (arrayset[index2] != divisor)) {
				arrayset.splice(index2, 1);
			} else {
				index2++;
			}
		}
		index1++;
	}

	console.log("array of prime numbers", arrayset);
  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);
  var preload;
	var timeoutvar = null;
	var current_sound;
	var timeout1;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"p8_s2.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

    var arrayset = [];
    // 2 to 100 because we want to generate prime numbers between 2 to 100
    for (var i = 2; i <= 100; i++) {
        arrayset[i - 2] = i;
    }

    var index1 = 0;
    var index2;
    var arraynotcomplete = true;
    var divisor;
    // This is the code for sieve of Eratosthenes Algorithm concept
    while (index1 < arrayset.length) {
        divisor = arrayset[index1];
        index2 = index1;
        while (index2 < arrayset.length) {
            if (((arrayset[index2] % divisor) == 0) && (arrayset[index2] != divisor)) {
                arrayset.splice(index2, 1);
            } else {
                index2++;
            }
        }
        index1++;
    }

    console.log("array of prime numbers", arrayset);
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

      What it does:
      - send an element where the function has to see
      for data to highlight
      - this function searches for all nodes whose
      data-highlight element is set to true
      -searches for # character and gives a start tag
      ;span tag here, also for @ character and replaces with
      end tag of the respective
      - if provided with data-highlightcustomclass value for highlight it
        applies the custom class or else uses parsedstring class

      E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    /*=====  End of data highlight function  ======*/

    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside) {
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if ($splitintofractions.length > 0) {
            $.each($splitintofractions, function(index, value) {
                $this = $(this);
                var tobesplitfraction = $this.html();
                if ($this.hasClass('fraction')) {
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                } else {
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
    /*===== split into fractions end =====*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside) {
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ?
            alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
            null;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if ($allnotifications.length > 0) {
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }
    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
       last page of lesson
     - If called from last page set the islastpageflag to true such that
       footernotification is called for continue button to navigate to exercise
     */

    /**
        What it does:
        - If not explicitly overriden the method for navigation button
          controls, it shows the navigation buttons as required,
          according to the total count of pages and the countNext variable
        - If for a general use it can be called from the templateCaller
          function
        - Can be put anywhere in the template function as per the need, if
          so should be taken out from the templateCaller function
        - If the total page number is
       */

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            // $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        }
    }
    /*=====  End of user navigation controller function  ======*/

    /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller() {
        var $instructionblock = $board.find("div.instructionblock");
        if ($instructionblock.length > 0) {
            var $contentblock = $board.find("div.contentblock");
            var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
            var instructionblockisvisibleflag;

            $contentblock.css('pointer-events', 'none');

            $toggleinstructionblockbutton.on('click', function() {
                instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
                if (instructionblockisvisibleflag == 'true') {
                    instructionblockisvisibleflag = 'false';
                    $contentblock.css('pointer-events', 'auto');
                } else if (instructionblockisvisibleflag == 'false') {
                    instructionblockisvisibleflag = 'true';
                    $contentblock.css('pointer-events', 'none');
                }

                $instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
            });
        }
    }
    /*=====  End of InstructionBlockController  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
    =            Templates Block            =
    =======================================*/
    /*=================================================
    =            general template function            =
    =================================================*/


    var arrayofprimefactorscopy = []; //for case 9
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        splitintofractions($board);
		vocabcontroller.findwords(countNext);
        switch (countNext) {
            case 0:
              $(".contentblock").css({"background-image": "url('"+imgpath+"img_diy.png')",
                                        "background-size": "100% 100%"});
                                        play_diy_audio();
            break;
            case 1:
            sound_player("sound_0");
            $(".contentblock").css({"background-image": "url('"+imgpath+"bg.png')",
                                        "background-size": "100% 100%"});
                var randomcompositenumber = 0;
                var arrayofprimefactors = [];
                var temp;
                $(".rownormal").click(function () {
                    $(".yellowlowertextblock").hide(0);
                });
                $('#playagain').click(function() {
                    templateCaller();
                });
                while (randomcompositenumber == 0) {
                    temp = Math.floor(Math.random() * (100 - 18) + 18);
                    if (arrayset.indexOf(temp) == -1) {
                        randomcompositenumber = temp;
                    }

                    arrayofprimefactors = generateprimefactors(randomcompositenumber);
                    if (arrayofprimefactors.length > 5) {
                        randomcompositenumber = 0;
                    }
                }

                arrayofprimefactorscopy = arrayofprimefactors;
                console.log("generated random number", randomcompositenumber);
                var arrayofprimefactors = generateprimefactors(randomcompositenumber);
                console.log("random prime factors", arrayofprimefactors);
                $("#factorizevalue1").html(randomcompositenumber);
                var $draggablep = $(".rownormal> p").html("");
                var temparray = [];
                var indextoprimefactorarray = 0;
                var primeunderconsideration = 0;
                while (temparray.length < $draggablep.length) {
                    if (indextoprimefactorarray < arrayofprimefactors.length) {
                        primeunderconsideration = arrayofprimefactors[indextoprimefactorarray];
                        if (temparray.indexOf(primeunderconsideration) == -1) {
                            temparray[temparray.length] = primeunderconsideration;
                        }
                        indextoprimefactorarray++;
                    } else {
                        primeunderconsideration = arrayset[Math.floor(Math.random() * (arrayset.length - 1))];
                        if (temparray.indexOf(primeunderconsideration) == -1) {
                            temparray[temparray.length] = primeunderconsideration;
                        }
                    }
                }
                var randomindex;
                for (var i = 0; i < $draggablep.length; i++) {
                    randomindex = Math.floor(Math.random() * (temparray.length - 1));
                    $($draggablep[i]).html(temparray[randomindex]);
                    temparray.splice(randomindex, 1);
                }

                $draggablep.addClass("draggable2").draggable({
                    containment: "body",
                    cursor: "grab",
                    revert: "invalid",
                    appendTo: "body",
                    helper: "clone",
                    zindex: 1000,
                    start: function(event, ui) {
                        ui.helper.css({
                            "font-size": "4.56vmin",
                            "color": "#fff"
                        });
                    },
                    stop: function(event, ui) {}
                }).css('cursor', 'grab');

                for (var i = 2; i < 10; i++) {
                    var $factorezedvalue = $('#factorizevalue' + i);
                    var index = i;
                    $factorezedvalue.html("<span class='answer" + i + "'><span class = 'droppable'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>");
                    $factorezedvalue.droppable({
                        accept: ".draggable2",
                        hoverClass: "hovered",
                        drop: function upondrop(event, ui) {
                            handleCardDrop2(event, ui, $(this));
                        }
                    });
                }

                function upondropcomplete(dontshownextbtn) {

                    dontshownextbtn = (dontshownextbtn == null) ? false : dontshownextbtn;
                    $('#factorizevalueSo').show(0);
                    var product = 1;
                    if (!dontshownextbtn) {
                        ole.footerNotificationHandler.pageEndSetNotification();
                        $('#playagain').show(0);
                        $('#factorizevalue1').html("<span class='purpletext'>" + $('#factorizevalue1').html() + " </span>");
                    } else {
                        var idx2 = 0;

                        while (idx2 < arrayofprimefactorscopy.length) {
                            product *= arrayofprimefactorscopy[idx2];
                            idx2++;
                        }
                        $('#factorizevalue1').html("<span class='purpletext'>" + product + " </span>");
                    }

                    for (var i = 0; i < arrayofprimefactorscopy.length; i++) {
                        if (i == 0) {
                            $('#factorizevalue1').append(" = <span class='greentext'>" + arrayofprimefactorscopy[i] + "</span>");
                        } else {
                            $('#factorizevalue1').append(" &times; <span class='greentext'>" + arrayofprimefactorscopy[i] + "</span>");
                        }
                    }

                    if (dontshownextbtn) {
                        return product;
                    }

                }

                function handleCardDrop2(event, ui, $this) {
                    console.log('a value was dropped');
                    var $dropped = $(ui.draggable);
                    var droppedValue = parseInt($dropped.html());
                    console.log('the index of the dropped on is', $this.attr('id'));

                    var idx = arrayofprimefactors.indexOf(droppedValue);
                    var newval;
                    if (idx != -1) {
                        arrayofprimefactors.splice(idx, 1);
                        var $correct = $('.imagecorrect');
                        switch ($this.attr('id')) {
                            case 'factorizevalue2':
                            case 'factorizevalue3':
                                console.log("dropped on ", '2-3');
                                if (arrayofprimefactors.length > 0) {
                                    $('#factorizevalue2').html("&nbsp;&nbsp;<span class='answer'>" + droppedValue + "</span>&nbsp;&nbsp;").css('background-color', '#15C402').droppable('disable');
                                    newval = parseInt($('#factorizevalue1').html()) / droppedValue;
                                    if (arrayofprimefactors.length > 1) {
                                        // if(arrayset.indexOf(newval) == -1){
                                        $('#factorizevalue3').html("&nbsp;&nbsp;<span class='answer'>" + newval + "</span>&nbsp;&nbsp;").droppable('disable').css('background-color', 'transparent');
                                        $('.line2').show(0);
                                        $('#factorizevalue4, #factorizevalue5').show(0);
                                        // }else{
                                        // $('#factorizevalue3').html(newval).css('color', '#0f0').droppable('disable');
                                        // $draggablep.draggable('disable');
                                        // upondropcomplete();
                                        // }
                                    }
                                } else {
                                    $('#factorizevalue3').html("&nbsp;&nbsp;<span class='answer'>" + droppedValue + "</span>&nbsp;&nbsp;").css('background-color', '#15C402').droppable('disable');
                                    $draggablep.draggable('disable');
                                    upondropcomplete();
                                }
                                $correct.show(0);
                                createjs.Sound.stop();
              					play_correct_incorrect_sound(1);
                                $correct.delay(1500).hide(0);
                                break;
                            case 'factorizevalue4':
                            case 'factorizevalue5':
                                console.log("dropped on ", '4-5');
                                if (arrayofprimefactors.length > 0) {
                                    $('#factorizevalue4').html("&nbsp;&nbsp;<span class='answer'>" + droppedValue + "</span>&nbsp;&nbsp;").css('background-color', '#15C402').droppable('disable');
                                    newval = parseInt($('#factorizevalue3>.answer').html()) / droppedValue;
                                    if (arrayofprimefactors.length > 1) {
                                        // if(arrayset.indexOf(newval) == -1){
                                        $('#factorizevalue5').html("&nbsp;&nbsp;<span class='answer'>" + newval + "</span>&nbsp;&nbsp;").droppable('disable').css('background-color', 'transparent');
                                        $('.line3').show(0);
                                        $('#factorizevalue6, #factorizevalue7').show(0);
                                        // }else{
                                        // $('#factorizevalue5').html(newval).css('color', '#0f0').droppable('disable');
                                        // $draggablep.draggable('disable');
                                        // upondropcomplete();
                                        // }
                                    }
                                } else {
                                    $('#factorizevalue5').html("&nbsp;&nbsp;<span class='answer'>" + droppedValue + "</span>&nbsp;&nbsp;").css('background-color', '#15C402').droppable('disable');
                                    $draggablep.draggable('disable');
                                    upondropcomplete();
                                }
                                $correct.show(0);
                                $correct.delay(1500).hide(0);
              					play_correct_incorrect_sound(1);
                                break;
                            case 'factorizevalue6':
                            case 'factorizevalue7':
                                console.log("dropped on ", '6-7');
                                if (arrayofprimefactors.length > 0) {
                                    $('#factorizevalue6').html("&nbsp;&nbsp;<span class='answer'>" + droppedValue + "</span>&nbsp;&nbsp;").css('background-color', '#15C402').droppable('disable');
                                    newval = parseInt($('#factorizevalue5>.answer').html()) / droppedValue;
                                    if (arrayofprimefactors.length > 1) {
                                        // if(arrayset.indexOf(newval) == -1){
                                        $('#factorizevalue7').html("&nbsp;&nbsp;<span class='answer'>" + newval + "</span>&nbsp;&nbsp;").droppable('disable').css('background-color', 'transparent');
                                        $('.line4').show(0);
                                        $('#factorizevalue8, #factorizevalue9').show(0);
                                        // }else{
                                        // $('#factorizevalue7').html(newval).css('color', '#0f0').droppable('disable');
                                        // $draggablep.draggable('disable');
                                        // upondropcomplete();
                                        // }
                                    }
                                } else {
                                    $('#factorizevalue7').html("&nbsp;&nbsp;<span class='answer'>" + droppedValue + "</span>&nbsp;&nbsp;").css('background-color', '#15C402').droppable('disable');
                                    $draggablep.draggable('disable');
                                    upondropcomplete();
                                }
                                $correct.show(0);
              					play_correct_incorrect_sound(1);
                                $correct.delay(1500).hide(0);
                                break;
                            case 'factorizevalue8':
                            case 'factorizevalue9':
                                console.log("dropped on ", '8-9');
                                if (arrayofprimefactors.length > 0) {
                                    $('#factorizevalue8').html("&nbsp;&nbsp;<span class='answer'>" + droppedValue + "</span>&nbsp;&nbsp;").css('background-color', '#15C402').droppable('disable');
                                    // newval = parseInt($('#factorizevalue7').html())/droppedValue;
                                    // $('#factorizevalue9').html(newval).css('color', '#0f0').droppable('disable');
                                    // $draggablep.draggable('disable');
                                    // upondropcomplete();
                                } else {
                                    $('#factorizevalue9').html("&nbsp;&nbsp;<span class='answer'>" + droppedValue + "</span>&nbsp;&nbsp;").css('background-color', '#15C402').droppable('disable');
                                    $draggablep.draggable('disable');
                                    upondropcomplete();
                                }
                                $correct.show(0);
              					play_correct_incorrect_sound(1);
                                $correct.delay(1500).hide(0);
                                break;
                            default:
                                break;
                        }
                    } else {
                        var $wrong = $('.imagewrong');
                        switch ($this.attr('id')) {
                            case 'factorizevalue2':
                            case 'factorizevalue3':
                                if (arrayofprimefactors.length != 1) {
                                    $('#factorizevalue2').html("&nbsp;&nbsp;" + droppedValue + "&nbsp;&nbsp;").css('background-color', '#EE6E73');
                                    setTimeout(function() {
                                        $('#factorizevalue2').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    }, 1500);
                                } else {
                                    $('#factorizevalue3').html("&nbsp;&nbsp;" + droppedValue + "&nbsp;&nbsp;").css('background-color', '#EE6E73');
                                    setTimeout(function() {
                                        $('#factorizevalue3').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    }, 1500);
                                }
                                $wrong.show(0);
              					play_correct_incorrect_sound(0);
                                $wrong.delay(1500).hide(0);
                                break;
                            case 'factorizevalue4':
                            case 'factorizevalue5':
                                console.log("dropped on ", '4-5');
                                if (arrayofprimefactors.length != 1) {
                                    $('#factorizevalue4').html("&nbsp;&nbsp;" + droppedValue + "&nbsp;&nbsp;").css('background-color', '#EE6E73');
                                    setTimeout(function() {
                                        $('#factorizevalue4').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    }, 1500);
                                } else {
                                    $('#factorizevalue5').html("&nbsp;&nbsp;" + droppedValue + "&nbsp;&nbsp;").css('background-color', '#EE6E73');
                                    setTimeout(function() {
                                        $('#factorizevalue5').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    }, 1500);
                                }
                                $wrong.show(0);
              					play_correct_incorrect_sound(0);
                                $wrong.delay(1500).hide(0);
                                break;
                            case 'factorizevalue6':
                            case 'factorizevalue7':
                                console.log("dropped on ", '6-7');
                                if (arrayofprimefactors.length != 1) {
                                    $('#factorizevalue6').html("&nbsp;&nbsp;" + droppedValue + "&nbsp;&nbsp;").css('background-color', '#EE6E73');
                                    setTimeout(function() {
                                        $('#factorizevalue6').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    }, 1500);
                                } else {
                                    $('#factorizevalue7').html("&nbsp;&nbsp;" + droppedValue + "&nbsp;&nbsp;").css('background-color', '#EE6E73');
                                    setTimeout(function() {
                                        $('#factorizevalue7').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    }, 1500);
                                }
                                $wrong.show(0);
              					play_correct_incorrect_sound(0);
                                $wrong.delay(1500).hide(0);
                                break;
                            case 'factorizevalue8':
                            case 'factorizevalue9':
                                console.log("dropped on ", '8-9');
                                if (arrayofprimefactors.length != 1) {
                                    $('#factorizevalue8').html("&nbsp;&nbsp;" + droppedValue + "&nbsp;&nbsp;").css('background-color', '#EE6E73');
                                    setTimeout(function() {
                                        $('#factorizevalue8').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    }, 1500);
                                } else {
                                    $('#factorizevalue9').html("&nbsp;&nbsp;" + droppedValue + "&nbsp;&nbsp;").css('background-color', '#EE6E73');
                                    setTimeout(function() {
                                        $('#factorizevalue9').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    }, 1500);
                                }
                                $wrong.show(0);
              					play_correct_incorrect_sound(0);
                                $wrong.delay(1500).hide(0);
                                break;
                            default:
                                break;
                        }
                    }
                }

                break;
            case 10:

                var product = upondropcomplete(true);
                var $rownormal2 = $('.rownormal2>p');
                $('#playagain').show(0).click(function() {
                    countNext = 9;
                    ole.footerNotificationHandler.hideNotification();
                    templateCaller();
                });
                for (var i = 0; i < arrayofprimefactorscopy.length; i++) {
                    $($rownormal2[i]).html(((arrayofprimefactorscopy[i] < 10) ? "&nbsp;&nbsp;&nbsp;" + arrayofprimefactorscopy[i] : arrayofprimefactorscopy[i]) + "<span class= 'borderleftbottom'>" + product + "</span>");
                    product /= arrayofprimefactorscopy[i];
                    if (arrayset.indexOf(product) != -1) {
                        $($rownormal2[i + 1]).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + product);
                        break;
                    }
                }
                setTimeout(function() {
                    ole.footerNotificationHandler.pageEndSetNotification();
                    $('#playagain').show(0);
                }, 2000);
                break;
            default:
                break;
        }
    }

    /*=====  End of Templates Block  ======*/

    function breakdownintoprimefactors2($description, startindex, endindex, valueatdescription) {
        var $p = $($description[startindex]);
        if (startindex == endindex) {
            return true;
        }

        if (valueatdescription == null) {
            valueatdescription = parseInt($p.html());
        }

        if (($p.html()).indexOf("=") == -1) {
            $p.append(" = ");
            breakdownintoprimefactors2($description, startindex, endindex, valueatdescription);
        } else if (arrayset.indexOf(valueatdescription) > -1) {
            $p.append(valueatdescription);
            breakdownintoprimefactors2($description, ++startindex, endindex);
        } else {
            var index = 0;
            while (index < arrayset.length) {
                if ((valueatdescription % arrayset[index]) == 0) {
                    $p.append(arrayset[index] + " &times; ");
                    valueatdescription /= arrayset[index];
                    break;
                }
                index++;
            }
            breakdownintoprimefactors2($description, startindex, endindex, valueatdescription);
        }

    }

    function generateprimefactors(number) {
        var primeindex = 0;
        var arrayofprimefactors = [];
        while (number > 1) {
            if ((number % arrayset[primeindex]) == 0) {
                number /= arrayset[primeindex];
                arrayofprimefactors[arrayofprimefactors.length] = arrayset[primeindex];
            } else {
                ++primeindex;
            }
        }
        return arrayofprimefactors;
    }


    function breakdownintoprimefactors($description, startindex, endindex, valueatdescription, makedraganddrop) {
        var $p = $($description[startindex]);
        makedraganddrop = (makedraganddrop == null) ? true : makedraganddrop;
        if (startindex == endindex) {
            if (makedraganddrop) {
                var answer = parseInt($($description[endindex]).html());
                var arrayofprimefactors = generateprimefactors(answer);
                // var tempanswer = answer;
                //
                // var primeindex = 0;
                //
                // while(tempanswer > 1){
                // if((tempanswer % arrayset[primeindex]) == 0){
                // tempanswer /= arrayset[primeindex];
                // arrayofprimefactors[arrayofprimefactors.length] = arrayset[primeindex];
                // }else{
                // ++primeindex;
                // }
                // }


                $($description[endindex]).append(" <span class= 'equal'>=</span><span class= 'notequal'>&NotEqual;</span> <span class = 'dropleft'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> &times; <span class = 'dropright'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> = <span class='result'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                var $equal = $('.equal');
                var $notequal = $('.notequal').hide(0);
                $('.dropleft').droppable({
                    accept: ".draggables",
                    hoverClass: "hovered",
                    drop: function upondrop(event, ui) {
                        handleCardDrop(event, ui, "dropleft");
                    }
                });

                $('.dropright').droppable({
                    accept: ".draggables",
                    hoverClass: "hovered",
                    drop: function upondrop(event, ui) {
                        handleCardDrop(event, ui, "dropright");
                    }
                });

                $(".rownormal > p").addClass('draggables focusscale').draggable({
                    containment: "body",
                    cursor: "grab",
                    revert: "invalid",
                    appendTo: "body",
                    helper: "clone",
                    zindex: 1000,
                    start: function(event, ui) {
                        ui.helper.css({
                            "font-size": "4.56vmin",
                            "color": "#6D9EEB"
                        });
                    },
                    stop: function(event, ui) {}
                }).css('cursor', 'grab');
                var correctanswer = false;
                $('.yellowlowertextblock').show(0);

                function handleCardDrop(event, ui, classname) {
                    $(".rownormal > p").removeClass('focusscale');
                    var dropped = ui.draggable;
                    var count = 0;
                    var top = 0;
                    var droppedvalue = $(dropped).html();
                    $("." + classname).html("   " + droppedvalue + "   ").css('color', '#ffffff');
                    var valueleft = parseInt($('.dropleft').html());
                    var valueright = parseInt($('.dropright').html());
                    var $result = $(".result");

                    if (!isNaN(valueleft) && !isNaN(valueright)) {
                        $result.html("   " + (valueleft * valueright) + "   ");
                        var temparrayofprimefactors = arrayofprimefactors;
                        var leftindex = temparrayofprimefactors.indexOf(valueleft);
                        var rightindex = temparrayofprimefactors.indexOf(valueright);
                        if (leftindex != -1 && rightindex != -1 && leftindex != rightindex) {
                            // correct
                            $equal.show(0);
                            $notequal.hide(0);
                            $('.dropright').css('background-color', '#15C402');
                            $('.dropleft').css('background-color', '#15C402');
                            $('.result').css('background-color', '#15C402');
                            $(".rownormal > p").draggable('disable');
                                ole.footerNotificationHandler.pageEndSetNotification();
                                $('#playagain').show(0);
                            $('.imagecorrect').show(0);
                            correctanswer = true;
                            play_correct_incorrect_sound(1);
                        } else {
                            //one or both are incorrect
                            $equal.hide(0);
                            $notequal.show(0);

                            if (leftindex == rightindex && leftindex != -1) {
                                $('.dropleft').css('background-color', '#15C402');
                                $('.dropright').css('background-color', '#EE6E73');
                                rightindex = -1;
                            } else {
                                (rightindex != -1) ? $('.dropright').css('background-color', '#15C402'): $('.dropright').css('background-color', '#EE6E73');
                                (leftindex != -1) ? $('.dropleft').css('background-color', '#15C402'): $('.dropleft').css('background-color', '#EE6E73');
                            }

                            $result.css('background-color', '#EE6E73');
                            //monkey image show
                            $('.imagewrong').show(0);
                            play_correct_incorrect_sound(0);
                            setTimeout(function() {
                                if (correctanswer)
                                    return true;
                                if (rightindex == -1 && leftindex == -1) {
                                    $('.dropright').html("&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    $('.dropleft').html("&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    $result.html("&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                } else if (rightindex == -1) {
                                    $('.dropright').html("&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    $result.html(valueleft).css('background-color', '#A082E1');
                                } else {
                                    $('.dropleft').html("&nbsp;&nbsp;&nbsp;&nbsp;").css('background-color', '#A082E1');
                                    $result.html(valueright).css('background-color', '#A082E1');
                                }
                                $('.imagewrong').hide(0);
                                $equal.show(0);
                                $notequal.hide(0);
                                //monkey image hide
                            }, 2000);
                        }


                    } else if (isNaN(valueleft)) {
                        $result.html(valueright);
                    } else if (isNaN(valueright)) {
                        $result.html(valueleft);
                    }

                    console.log($(dropped).html());
                }
            } else {
                ole.footerNotificationHandler.pageEndSetNotification();
                $('#playagain').show(0);
            }
            return true;
        }

        if (valueatdescription == null) {
            valueatdescription = parseInt($p.html());
        }

        if (($p.html()).indexOf("=") == -1) {
            setTimeout(function() {
                $p.append(" = ");
                breakdownintoprimefactors($description, startindex, endindex, valueatdescription, makedraganddrop);
                // breakdownintoprimefactors($description, startindex, endindex, valueatdescription);
            }, 400);
        } else if (arrayset.indexOf(valueatdescription) > -1) {
            setTimeout(function() {
                $p.append(valueatdescription);
            }, 400);
            setTimeout(function() {
                breakdownintoprimefactors($description, ++startindex, endindex, null, makedraganddrop);
                // breakdownintoprimefactors($description, ++startindex, endindex);
            }, 600);
        } else {
            var index = 0;
            while (index < arrayset.length) {
                if ((valueatdescription % arrayset[index]) == 0) {
                    $p.append(arrayset[index] + " x ");
                    valueatdescription /= arrayset[index];
                    break;
                }
                index++;
            }
            setTimeout(function() {
                breakdownintoprimefactors($description, startindex, endindex, valueatdescription, makedraganddrop);
                // breakdownintoprimefactors($description, startindex, endindex, valueatdescription);
            }, 400);
        }

    }

    function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}
    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
      Motivation :
      - Make a single function call that handles all the
        template load easier

      How To:
      - Update the template caller with the required templates
      - Call template caller

      What it does:
      - According to value of the Global Variable countNext
        the slide templates are updated
     */

    function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


        //call the slide indication bar handler for pink indicators
        loadTimelineProgress($total_page, countNext + 1);

        // just for development purpose to see total slide vs current slide number
        // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    /*=====  End of Templates Controller Block  ======*/
});

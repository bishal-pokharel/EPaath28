var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
			textclass:"pgNmTxt",
			textdata:data.string.prm_fact
		}]
	},
	// slide 2
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s2txt
		}]
	},
	// slide 3
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s3txt
		}]
	},
	// slide 4
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"clouds",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s4txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		}]
	},
	// slide 5
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s5txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}]
	},
	// slide 6
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s6txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		// extratextblock:[{
		// 	txtboxdivclass:"midEqnBlk",
		// 	txtsindiv:[{
		// 		datahighlightflag:true,
		// 		datahighlightcustomclass:"pnkTxt",
		// 		textclass:"eqn",
		// 		textdata:data.string.eqn
		// 	}]
		// }]
	},
	// slide 7
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s7txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		extratextblock:[{
			txtboxdivclass:"midEqnBlk",
			txtsindiv:[{
				datahighlightflag:true,
				datahighlightcustomclass:"ylwtxt",
				textclass:"eqn",
				textdata:data.string.eqn0
			}]
		}]
	},
	// slide 8
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s8txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		// extratextblock:[{
		// 	txtboxdivclass:"midEqnBlk",
		// 	txtsindiv:[{
		// 		datahighlightflag:true,
		// 		datahighlightcustomclass:"pnkTxt",
		// 		textclass:"eqn",
		// 		textdata:data.string.eqn0
		// 	}]
		// }]
	},
	// slide 9
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s9txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		extratextblock:[{
			txtboxdivclass:"midEqnBlk",
			txtsindiv:[{
				datahighlightflag:true,
				datahighlightcustomclass:"ylwtxt",
				textclass:"eqn",
				textdata:data.string.eqn1
			}]
		}]
	},
	// slide 10
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s10txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		// extratextblock:[{
		// 	txtboxdivclass:"midEqnBlk",
		// 	txtsindiv:[{
		// 		datahighlightflag:true,
		// 		datahighlightcustomclass:"pnkTxt",
		// 		textclass:"eqn",
		// 		textdata:data.string.eqn0
		// 	}]
		// }]
	},
	// slide 11
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s11txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		extratextblock:[{
			txtboxdivclass:"midEqnBlk",
			txtsindiv:[{
				datahighlightflag:true,
				datahighlightcustomclass:"ylwtxt",
				textclass:"eqn",
				textdata:data.string.eqn2
			}]
		}]
	},
	// slide 12
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s12txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		extratextblock:[{
			txtboxdivclass:"midEqnBlk",
			txtsindiv:[{
				datahighlightflag:true,
				datahighlightcustomclass:"pnkTxt",
				textclass:"eqn",
				textdata:data.string.eqn9_sec
			}]
		}]
	},
	// slide 13
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s13txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		extratextblock:[{
			txtboxdivclass:"midEqnBlk",
			txtsindiv:[{
				datahighlightflag:true,
				datahighlightcustomclass:"ylwtxt",
				textclass:"eqn",
				textdata:data.string.eqn3
			}]
		}]
	},
	// slide 14
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s14txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		extratextblock:[{
			txtboxdivclass:"midEqnBlk",
			txtsindiv:[{
				datahighlightflag:true,
				datahighlightcustomclass:"ylwtxt",
				textclass:"eqn",
				textdata:data.string.eqn3
			}]
		}]
	},
	// slide 15
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s15txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}]
	},
	// slide 16
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p7s16txt
		}],
		numberscontainer:[{
			numberblockclass:"primeContainer",
			textclass:"prmCmpTop",
			textdata:data.string.prime
		},{
			numberblockclass:"compContainer",
			datahighlightflag:true,
			datahighlightcustomclass:"grnBg",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}]
	},
	// slide 17
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishrght',
					imgid:'fish',
					imgsrc:""
			}
			// ,{
			// 		imgclass:'arwlst',
			// 		imgid:'arrow',
			// 		imgsrc:""
			// }
		]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:"clouds02",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"undline",
			textclass:"textInSp",
			textdata:data.string.p7s17txt
		}],
		numberscontainer:[{
			numberblockclass:"compContainer",
			datahighlightflag:true,
			datahighlightcustomclass:"grnBg",
			textclass:"prmCmpTop",
			textdata:data.string.composite
		}],
		extratextblock:[{
			textclass:"prmfctrCls",
			textdata:data.string.prm_fact
		}]
	},
	// slide 18
	{
		imageblock:[{
			imagestoshow:[{
					imgclass:'background',
					imgid:'bg',
					imgsrc:""
			},{
					imgclass:'fish-fishLft',
					imgid:'fish',
					imgsrc:""
			}]
		}],
		extratextblock:[{
		datahighlightflag:true,
		datahighlightcustomclass:"undline pnkTxt",
			textclass:"lstSldTxt",
			textdata:data.string.p7s18txt
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var timeout1;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds01", src: imgpath+"clouds01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds02", src: imgpath+"clouds02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "p6_s1", src: soundAsset+"p6_s1.ogg"},
			{id: "p6_s2", src: soundAsset+"p6_s2.ogg"},
			{id: "p6_s3", src: soundAsset+"p6_s3.ogg"},
			{id: "p6_s4", src: soundAsset+"p6_s4.ogg"},
			{id: "p6_s5", src: soundAsset+"p6_s5.ogg"},
			{id: "p6_s6", src: soundAsset+"p6_s6.ogg"},
			{id: "p6_s7", src: soundAsset+"p6_s7.ogg"},
			{id: "p6_s8", src: soundAsset+"p6_s8.ogg"},
			{id: "p6_s9", src: soundAsset+"p6_s9.ogg"},
			{id: "p6_s10", src: soundAsset+"p6_s10.ogg"},
			{id: "p6_s11", src: soundAsset+"p6_s11.ogg"},
			{id: "p6_s12", src: soundAsset+"p6_s12.ogg"},
			{id: "p6_s13", src: soundAsset+"p6_s13.ogg"},
			{id: "p6_s14", src: soundAsset+"p6_s14.ogg"},
			{id: "p6_s15", src: soundAsset+"p6_s15.ogg"},
			{id: "p6_s16", src: soundAsset+"p6_s16.ogg"},
			{id: "p6_s17", src: soundAsset+"p6_s17.ogg"},
			{id: "p6_s18", src: soundAsset+"p6_s18.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);
		var count=0;

		function getPrimes(max) {
			var sieve = [], i, j, primes = [];
			for (i = 2; i <= max; ++i) {
					if (!sieve[i]) {
							// i has not been marked -- it is prime
							primes.push(i);
							for (j = i << 1; j <= max; j += i) {
									sieve[j] = true;
							}
					}
			}
			return primes;
		}
		function getComposite(max) {
			var sieve = [], i, j, comps = [];
			for (i = 2; i <= max; ++i) {
					if (!sieve[i]) {
							for (j = i << 1; j <= max; j += i) {
									sieve[j] = true;
							}
					}else{
						comps.push(i);
					}
			}
			return comps;
		}
		function appendPrimes(){
			for(var i=0;i<prmArray.length; i++){
				$(".primeContainer").append("<div class='prmCnt pc"+i+"'></div>");
			}
			for(var i=0;i<prmArray.length; i++){
				$(".pc"+i).append("<p class='prmNum pn"+i+"'>"+prmArray[i]+"</p>");
			}
		}
		function appendComposite(){
			for(var i=0;i<cmpArray.length; i++){
				$(".compContainer").append("<div class='cmpCnt cmpC"+i+"'></div>");
			}
			for(var i=0;i<cmpArray.length; i++){
				$(".cmpC"+i).append("<p class='cmpNum cn"+i+"'>"+cmpArray[i]+"</p>");
			}
		}
		var prmArray = getPrimes(20);
		var cmpArray = getComposite(19);

		switch(countNext) {
			case 0:
			sound_player("p6_s"+(countNext+1),1);
			break;
			case 3:
				appendPrimes();
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 4:
				appendPrimes();
				appendComposite();
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 5:
				appendPrimes();
				appendComposite();
				arwHGhlight(0, false, primeArray);
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 6:
				var primeArray = [0];
				appendPrimes();
				appendComposite();
				arwHGhlight(0, true, primeArray);
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 7:
				appendPrimes();
				appendComposite();
				arwHGhlight(1, false, primeArray);
				$(".cn0").html(eval("data.string.eqn_0"));
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 8:
				var primeArray = [0,1];
				appendPrimes();
				appendComposite();
				arwHGhlight(1, true, primeArray);
				$(".cn0").html(eval("data.string.eqn_0"));
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 9:
				appendPrimes();
				appendComposite();
				arwHGhlight(2, false, primeArray);
				$(".cn0").html(eval("data.string.eqn_0"));
				$(".cn1").html(eval("data.string.eqn_1"));
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 10:
				var primeArray = [0];
				appendPrimes();
				appendComposite();
				arwHGhlight(2, true, primeArray);
				$(".cn0").html(eval("data.string.eqn_0"));
				$(".cn1").html(eval("data.string.eqn_1"));
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 11:
				appendPrimes();
				appendComposite();
				arwHGhlight(3, false, primeArray);
				for(var i=0;i<=2;i++){
					$(".cn"+i).html(eval("data.string.eqn_"+i));
				}
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 12:
				var primeArray = [1];
				appendPrimes();
				appendComposite();
				arwHGhlight(3, true, primeArray);
				for(var i=0;i<=2;i++){
					$(".cn"+i).html(eval("data.string.eqn_"+i));
				}
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 13:
				appendPrimes();
				appendComposite();
				arwHGhlight(4, false, primeArray);
				for(var i=0;i<=3;i++){
					$(".cn"+i).html(eval("data.string.eqn_"+i));
				}
				var letArray  = [10,12,14,15,16,18];
				arwEqnDisplayer(4, letArray, 0);
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 14:
				appendPrimes();
				appendComposite();
				for(var i=0;i<=9;i++){
					$(".cn"+i).html(eval("data.string.eqn_"+i));
				}
				sound_player("p6_s"+(countNext+1),1);
			break;
			case 15:
				$(".sp-2").hide(0);
				appendPrimes();
				appendComposite();
				for(var i=0;i<=9;i++){
					$(".cn"+i).html(eval("data.string.eqn_1_"+i));
				}
				function hghTxt(count){
						$(".grnBg:eq("+count+")").addClass("grn");
						count+=1;
						if(count<=9){
							timeout1 = setTimeout(function(){
								hghTxt(count);
							},500);
						}else{
							$(".sp-2").show(0);
							sound_player("p6_s"+(countNext+1),1);
						}
				}
				hghTxt(0);
			break;
			case 16:
				$(".prmfctrCls").hide(0);
				appendComposite();
				$(".compContainer").append("<img class='arwlst' src='"+preload.getResult("arrow").src+"'/>");
				for(var i=0;i<=9;i++){
					$(".cn"+i).html(eval("data.string.eqn_1_"+i));
					texthighlight($board);
				}
				$(".arwlst").animate({
					top:"93%"
				},2000, function(){
					$(".prmfctrCls").fadeIn(300);
				});

				sound_player("p6_s"+(countNext+1),1);
				break;
			case 17:
			sound_player("p6_s"+(countNext+1),1);
			break;
			default:
			sound_player("p6_s"+(countNext+1),1);
				break;
		}
		function arwHGhlight(cnt,prm, prAr){
			// showArrow with highlight in number and highlight prime number if true
			$(".cmpC"+cnt).append("<img class='arw ar"+cnt+"' src='"+preload.getResult("arrow").src+"'/>");
			$(".cn"+cnt).addClass("pnkTxt");
			if(prm){
				for(var i=0;i<prAr.length;i++){
					$(".pn"+prAr[i]).addClass("pnkTxt");
				}
			}
		}
		function arwEqnDisplayer(cnt, letArray, letCnt){
			$(".prmNum, .cmpNum").removeClass("pnkTxt");
			var prev = cnt-1;
			if(cnt>=1){
				$(".ar"+prev).hide(0);
				$(".cn"+prev).html(eval("data.string.eqn_"+prev));
			}
			$(".cmpC"+cnt).append("<img class='arw ar"+cnt+"' src='"+preload.getResult("arrow").src+"'/>");
			$(".eqn").html(eval("data.string.eqn"+cnt));
			$(".cn"+cnt).addClass("pnkTxt");
			for(var j=0;j<=prmArray.length;j++){
				if((letArray[letCnt]%prmArray[j])==0){
					$(".pn"+j).addClass("pnkTxt");
				}
			}
			texthighlight($board);
			cnt+=1;
			if(cnt<=10){
					// createjs.Sound.stop();
					// current_sound = createjs.Sound.play("sound_1");
					// current_sound.play();
					// current_sound.on('complete', function(){
					timeout1 = setTimeout(function(){
						letCnt+=1;
						arwEqnDisplayer(cnt, letArray,letCnt);
					},2000);
					// });
			}else{
				nav_button_controls(100);
			}
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).addClass("corClass");
				$(this).siblings(".corctopt").show(0);
				$(".buttonsel").css("pointer-events","none");
				nav_button_controls(100);
			}else{
				$(this).addClass("incorClass");
				$(this).siblings(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
			}
		});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(0):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeout1);
	 	dropedImgCount = 0;
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		clearTimeout(timeout1);
		templateCaller();
	 	dropedImgCount = 0;
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";

var preload;
var timeoutArr = [];
var timeoutvar = null;

var content = [

  //slide 0

    {
        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text34,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        }],
        imageblock: [{
            imagestoshow: [{
                  imgclass: "bg",
                  imgsrc: '',
                  imgid: 'bg',
                },
                {
                  imgclass: "fish",
                  imgsrc: '',
                  imgid: 'fish',
                },
                {
                  imgclass: "shell1",
                  imgsrc: '',
                  imgid: 'shell',
                },
                {
                  imgclass: "shell2",
                  imgsrc: '',
                  imgid: 'shell',
                },
                {
                  imgclass: "shell3",
                  imgsrc: '',
                  imgid: 'shell',
                },
                {
                  imgclass: "shell4",
                  imgsrc: '',
                  imgid: 'shell',
                },
                {
                  imgclass: "shell5",
                  imgsrc: '',
                  imgid: 'shell',
                },]
            }],

        bubblebox:[{
          bubbleboxadditionalclass:'bubblebox1',
          bubbleimg: '',
          bubbleclass:'clouds',
          bubbletextclass: 'text1',
          bubbletextdata: data.string.p1text35,

        },]
    },

    //slide 1

    {
          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          }],
          imageblock: [{
              imagestoshow: [{
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],

          bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text36,

          },]
    },

    //slide 2

    {

          lowertextblockadditionalclass: 'multipleDigitTotal',
          lowertextblock: [{
            textclass: "multipleTotal1",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
      		}],

          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multiple",
              textdata: data.string.p1text38,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multipeDigits",
              textdata: '2 x 3 =',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },

        ],

          imageblock: [{
              imagestoshow: [{
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],

          bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text37,

          },]
    },

    //slide 3

    {

          lowertextblockadditionalclass: 'multipleDigitTotal',
          lowertextblock: [{
            textclass: "multipleTotal1",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
          }],

          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multiple",
              textdata: data.string.p1text38,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multipeDigits",
              textdata: '2 x 3 =',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },

        ],

          imageblock: [{
              imagestoshow: [{
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],

          bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text39,

          },]
    },

    //slide 4

    {

          lowertextblockadditionalclass: 'multipleDigitTotal',
          lowertextblock: [{
            textclass: "multipleTotal1",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
          }],

          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multiple",
              textdata: data.string.p1text38,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multipeDigits",
              textdata: '2 x 3 =',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },

        ],

          imageblock: [{
              imagestoshow: [{
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],

          bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text48,

          },]
    },

    //slide 5

    {
          uppertextblockadditionalclass: 'multipleDigitTotal',
          uppertextblock:[{
            textclass: "multipleTotal1",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
          }],

          lowertextblockadditionalclass: 'multipleDiv',
          lowertextblock: [
            {
              textclass: "twoMultiple multiple1",
              textdata: '2 x 0 = 0',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple2",
              textdata: '2 x 1 = 2',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple3",
              textdata: '2 x 2 = 4',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple4",
              textdata: '2 x 3 = 6',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple5",
              textdata: '2 x 4 = 8',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple6",
              textdata: '2 x 5 = 10',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple7",
              textdata: '2 x 6 = 12',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
        ],

          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multiple",
              textdata: data.string.p1text38,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multipeDigits",
              textdata: '2 x 3 =',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },

        ],

        imageblock: [{
              imagestoshow: [{
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],
    },

    //slide 6

    {
          lowertextblockadditionalclass: 'multipleDiv1',
          lowertextblock: [
            {
              textclass: "twoMultiple multiple1",
              textdata: data.string.p1text40,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple2",
              textdata: data.string.p1text41,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple3",
              textdata: data.string.p1text42,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple4",
              textdata: data.string.p1text43,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple5",
              textdata: data.string.p1text44,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple6",
              textdata: data.string.p1text45,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple7",
              textdata: data.string.p1text46,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
        ],

          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multipel",
              textdata: data.string.p1text38,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
        ],

        bubblebox:[
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text47,

        },
        ],

        imageblock: [{
              imagestoshow: [{
                    imgclass: "whitearrow",
                    imgsrc: '',
                    imgid: 'whitearrows',
                  },
                  {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],

    },

    //slide 7

    {
          lowertextblockadditionalclass: 'multipleDiv1',
          lowertextblock: [
            {
              textclass: "twoMultiple multiple1",
              textdata: data.string.p1text40,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple2",
              textdata: data.string.p1text41,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple3",
              textdata: data.string.p1text42,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple4",
              textdata: data.string.p1text43,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple5",
              textdata: data.string.p1text44,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple6",
              textdata: data.string.p1text45,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple7",
              textdata: data.string.p1text46,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
        ],

          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multipel",
              textdata: data.string.p1text38,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
        ],

        bubblebox:[
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text49,

        },
        ],

        imageblock: [{
              imagestoshow: [{
                    imgclass: "whitearrow",
                    imgsrc: '',
                    imgid: 'whitearrows',
                  },
                  {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],

    },

    //slide 8

    {
          lowertextblockadditionalclass: 'multipleDiv1',
          lowertextblock: [
            {
              textclass: "twoMultiple multiple1",
              textdata: data.string.p1text40,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple2",
              textdata: data.string.p1text41,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple3",
              textdata: data.string.p1text42,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple4",
              textdata: data.string.p1text43,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple5",
              textdata: data.string.p1text44,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple6",
              textdata: data.string.p1text45,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple7",
              textdata: data.string.p1text46,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
        ],

          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multipel",
              textdata: data.string.p1text38,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
        ],

        bubblebox:[
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text50,

        }
        // {
        //     bubbleboxadditionalclass:'bubblebox3',
        //     bubbleimg: '',
        //     bubbleclass:'cloudtop',
        //     bubbletextclass: 'text2',
        //     bubbletextdata: data.string.p1text49,

        // },
        ],

        imageblock: [{
              imagestoshow: [{
                    imgclass: "whitearrow",
                    imgsrc: '',
                    imgid: 'whitearrows',
                  },
                  {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],

    },

    //slide 9

    {
          lowertextblockadditionalclass: 'multipleDiv1',
          lowertextblock: [
            {
              textclass: "twoMultiple multiple1",
              textdata: data.string.p1text40,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple2",
              textdata: data.string.p1text41,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple3",
              textdata: data.string.p1text42,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple4",
              textdata: data.string.p1text43,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple5",
              textdata: data.string.p1text44,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple6",
              textdata: data.string.p1text45,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
            {
              textclass: "twoMultiple multiple7",
              textdata: data.string.p1text46,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
        ],

          extratextblock: [{
              textclass: "info",
              textdata: data.string.p1text34,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "multipel",
              textdata: data.string.p1text38,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
        ],

        bubblebox:[
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text51,

        }
        // {
        //     bubbleboxadditionalclass:'bubblebox3',
        //     bubbleimg: '',
        //     bubbleclass:'cloudtop',
        //     bubbletextclass: 'text2',
        //     bubbletextdata: data.string.p1text50,

        // },
        ],

        imageblock: [{
              imagestoshow: [{
                    imgclass: "whitearrow",
                    imgsrc: '',
                    imgid: 'whitearrows',
                  },
                  {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                  },
                  {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                  },
                  {
                    imgclass: "shell1",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell2",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell3",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell4",
                    imgsrc: '',
                    imgid: 'shell',
                  },
                  {
                    imgclass: "shell5",
                    imgsrc: '',
                    imgid: 'shell',
                  },]
              }],

    },

    //slide 10

    {

        lowertextblock: [
            {
              textclass: "info1",
              textdata: data.string.p1text52,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
            },
        ],
        uppertextblockadditionalclass: 'droppable box',
        ans: 'correct',
        uppertextblock:[{
          textclass: "",
          textdata: '',
          datahighlightflag: true,
          datahighlightcustomclass: 'grenLtr'
        }],

        imageblock: [{
              imagestoshow: [
                  {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',

                  },
                  {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                  },],

              }],

        textimage:[
            {
                textimageadditionalclass:'shellDiv1',
                imgclass:'shell',
                textclass: 'text1',
                textdata: data.string.p1text59,
                ans: 'correct'
            },
            {
              textimageadditionalclass:'shellDiv2',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text60,
              ans: 'incorrect'
            },
            {
              textimageadditionalclass:'shellDiv3',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text61,
              ans: 'correct'
            },
            {
              textimageadditionalclass:'shellDiv4',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text62,
              ans: 'correct'
            },
            {
              textimageadditionalclass:'shellDiv5',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text63,
              ans: 'incorrect'
            },
            {
              textimageadditionalclass:'shellDiv6',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text64,
              ans: 'incorrect'
            },
            {
              textimageadditionalclass:'shellDiv7',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text65,
              ans: 'incorrect'
            },
            {
              textimageadditionalclass:'shellDiv8',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text66,
              ans: 'incorrect'
            },
            {
              textimageadditionalclass:'shellDiv9',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text67,
              ans: 'correct'
            },
            {
              textimageadditionalclass:'shellDiv10',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text68,
              ans: 'incorrect'
            },
            {
              textimageadditionalclass:'shellDiv11',
              imgclass:'shell',
              textclass: 'text1',
              textdata: data.string.p1text69,
              ans: 'correct'
            }
        ],

    },

    //slide 11

    {

        lowertextblockadditionalclass: 'multipleDiv5',
        lowertextblock: [
          {
            textclass: "divmultiple",
            textdata: data.string.p1text38,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
          }
        ],

        bubblebox:[
        {
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text70,

        }],

        uppertextblockadditionalclass: 'droppable box',
        uppertextblock:[{
          textclass: "",
          textdata: '',
          datahighlightflag: true,
          datahighlightcustomclass: 'grenLtr'
        }],

        extratextblock: [{
            textclass: "multiple11",
            textdata: data.string.p1text54,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "multiple22",
            textdata: data.string.p1text55,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "multiple33",
            textdata: data.string.p1text56,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "multiple44",
            textdata: data.string.p1text57,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "multiple55",
            textdata: data.string.p1text58,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        ],

      imageblock: [{
          imagestoshow: [
                  {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',

                  },
                  {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                  },],

              }],

      textimage:[
                  {
                      textimageadditionalclass:'draggable shellDiv1111',
                      imgclass:'shell',
                      textclass: 'text1',
                      textdata: data.string.p1text59,
                      ans: 'correct'
                  },
                  {
                    textimageadditionalclass:'draggable shellDiv33',
                    imgclass:'shell',
                    textclass: 'text1',
                    textdata: data.string.p1text61,
                    ans: 'correct'
                  },
                  {
                    textimageadditionalclass:'draggable shellDiv44',
                    imgclass:'shell',
                    textclass: 'text1',
                    textdata: data.string.p1text62,
                    ans: 'correct'
                  },
                  {
                    textimageadditionalclass:'draggable shellDiv99',
                    imgclass:'shell',
                    textclass: 'text1',
                    textdata: data.string.p1text67,
                    ans: 'correct'
                  },
                  {
                    textimageadditionalclass:'draggable shellDiv111',
                    imgclass:'shell',
                    textclass: 'text1',
                    textdata: data.string.p1text69,
                    ans: 'correct'
                  }
              ]

      },

]

$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            //images

            {id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rectanglebox", src: imgpath+"rectangle_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "shell", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rectanglebox1", src: imgpath+"rectangle_box_six.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rectangleboxshell1", src: imgpath+"rectangle_box_shell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rectangleboxshell2", src: imgpath+"rectangle_box_six_shell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "whitearrows", src: imgpath+"white_arrows.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cloudtop", src: imgpath+"clouds05.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "p3_s1_1", src: soundAsset+"p3_s1_1.ogg"},
            {id: "p3_s1_2", src: soundAsset+"p3_s1_2.ogg"},
            {id: "p3_s2", src: soundAsset+"p3_s2.ogg"},
            {id: "p3_s3", src: soundAsset+"p3_s3.ogg"},
            {id: "p3_s4", src: soundAsset+"p3_s4.ogg"},
            {id: "p3_s5", src: soundAsset+"p3_s5.ogg"},
            {id: "p3_s6", src: soundAsset+"p3_s6.ogg"},
            {id: "p3_s7", src: soundAsset+"p3_s7.ogg"},
            {id: "p3_s8", src: soundAsset+"p3_s8.ogg"},
            {id: "p3_s9", src: soundAsset+"p3_s9.ogg"},
            {id: "p3_s10", src: soundAsset+"p3_s10.ogg"},
            {id: "p3_s11", src: soundAsset+"p3_s11.ogg"},
            {id: "p3_s12", src: soundAsset+"p3_s12.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
     =            Handlers and helpers Block            =
     ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

     What it does:
     - send an element where the function has to see
     for data to highlight
     - this function searches for all nodes whose
     data-highlight element is set to true
     -searches for # character and gives a start tag
     ;span tag here, also for @ character and replaces with
     end tag of the respective
     - if provided with data-highlightcustomclass value for highlight it
     applies the custom class or else uses parsedstring class

     E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/

    /*===============================================
    =            user notification function        =
    ===============================================*/
    /**
     How to:
     - First set any html element with
     "data-usernotification='notifyuser'" attribute,
     and "data-isclicked = ''".
     - Then call this function to give notification
     */

    /**
     What it does:
     - You send an element where the function has to see
     for data to notify user
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function notifyuser($notifyinside) {
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if ($allnotifications.length > 0) {
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }

    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */

    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

    }

    /*=====  End of user navigation controller function  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
     =            Templates Block            =
     =======================================*/
    /*=================================================
     =            general template function            =
     =================================================*/
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);

    function generalTemplate() {
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        put_image(content, countNext);


        // $nextBtn.show();
        $('.clouds').attr('src', preload.getResult('clouds').src);
        $('.cloudsright').attr('src', preload.getResult('clouds').src);
        $('.cloudtop').attr('src', preload.getResult('cloudtop').src);
        $('.rectangle').attr('src', preload.getResult('clouds').src);
        $('.shell').attr('src', preload.getResult('shell').src);

        $('.bubblebox1').hide();
        $('.info').hide();
        $('.multiple').hide();
        $('.multipeDigits').hide();
        $('.multipleDigitTotal').hide();
        $('.multipleDiv').hide();
        $('.multiple1').hide();
        $('.multiple2').hide();
        $('.multiple3').hide();
        $('.multiple4').hide();
        $('.multiple5').hide();
        $('.multiple6').hide();
        $('.multiple7').hide();

        $('.multipel').hide();
        $('.whitearrow').hide();
        $('.multipeDigits').hide();
        $('.multipleDigitTotal').hide();
        $('.bubblebox2').hide();
        $('.bubblebox3').hide();


      switch(countNext){

			case 0:

          setTimeout(function(){
              $('.bubblebox1').fadeIn(1500);
              sound_player("p3_s1_1",0);
              current_sound.on("complete",function(){
                $('.info').fadeIn(100);
                sound_player("p3_s1_2",1);
              });
              
          },200);

          break;

			case 1:
              $('.info').show();
              setTimeout(function(){
                  $('.bubblebox1').fadeIn(1500);
              },200);
              sound_player("p3_s"+(countNext+1),1);
              break;
			case 2:
              $('.info').show();
              setTimeout(function(){
                  $('.bubblebox1').fadeIn(1500);
              },200);
              setTimeout(function(){
                  $('.multiple').fadeIn(1500);
              },800);
              setTimeout(function(){
                  $('.multipeDigits').fadeIn(1500);
                  $('.multipleDigitTotal').fadeIn(1500);
              },400);
              sound_player("p3_s"+(countNext+1),1);

              break;

			case 3:
      case 4:
              $('.info').show();

              $('.multiple').show();
              $('.multipeDigits').show();
              $('.multipleDigitTotal').show();

              setTimeout(function(){
                  $('.bubblebox1').fadeIn(1500);
              },200);
              sound_player("p3_s"+(countNext+1),1);
              break;

      case 5:
              $('.info').show();

              $('.multiple').show();
              $('.multipeDigits').show();
              $('.multipleDigitTotal').show();
              $('.bubblebox1').hide();
              $('.multipleDiv').show();

              setTimeout(function(){
                  $('.multiple1').fadeIn(1500);
              },200);
              setTimeout(function(){
                  $('.multiple2').fadeIn(1500);
              },800);
              setTimeout(function(){
                  $('.multiple3').fadeIn(1500);
              },1400);
              setTimeout(function(){
                  $('.multiple4').fadeIn(1500);
              },2000);
              setTimeout(function(){
                  $('.multiple5').fadeIn(1500);
              },2600);
              setTimeout(function(){
                  $('.multiple6').fadeIn(1500);
              },3200);
              setTimeout(function(){
                  $('.multiple7').fadeIn(1500);
              },3600);
              sound_player("p3_s"+(countNext+1),1);

              break;


      case 6:

            $('.info').show();

            setTimeout(function(){
                $('.multipel').fadeIn(1500);
            },200);
            setTimeout(function(){
                $('.whitearrow').fadeIn(1500);
            },800);
            setTimeout(function(){
                $('.multiple1').fadeIn(1500);
            },1200);
            setTimeout(function(){
                $('.multiple2').fadeIn(1500);
            },1800);
            setTimeout(function(){
                $('.multiple3').fadeIn(1500);
            },2400);
            setTimeout(function(){
                $('.multiple4').fadeIn(1500);
            },3000);
            setTimeout(function(){
                $('.multiple5').fadeIn(1500);
            },3600);
            setTimeout(function(){
                $('.multiple6').fadeIn(1500);
            },4000);
            setTimeout(function(){
                $('.multiple7').fadeIn(1500);
            },4600);
            setTimeout(function(){
                $('.bubblebox2').fadeIn(1500);
                sound_player("p3_s"+(countNext+1),1);
            },5000);
            break;

      case 7:

            $('.info').show();

            setTimeout(function(){
                $('.multipel').fadeIn(1500);
            },200);
            setTimeout(function(){
                $('.whitearrow').fadeIn(1500);
            },800);
            setTimeout(function(){
                $('.multiple1').fadeIn(1500);
            },1200);
            setTimeout(function(){
                $('.multiple2').fadeIn(1500);
            },1800);
            setTimeout(function(){
                $('.multiple3').fadeIn(1500);
            },2400);
            setTimeout(function(){
                $('.multiple4').fadeIn(1500);
            },3000);
            setTimeout(function(){
                $('.multiple5').fadeIn(1500);
            },3600);
            setTimeout(function(){
                $('.multiple6').fadeIn(1500);
            },4000);
            setTimeout(function(){
                $('.multiple7').fadeIn(1500);
            },4600);
            setTimeout(function(){
                $('.bubblebox2').fadeIn(1500);
                sound_player("p3_s"+(countNext+1),1);
            },5000);
            break;

      case 8:

            $('.info').show();

            $('.multipel').show();
            $('.whitearrow').show();
            $('.multiple1').show();
            $('.multiple2').show();
            $('.multiple3').show();
            $('.multiple4').show();
            $('.multiple5').show();
            $('.multiple6').show();
            $('.multiple7').show();

            setTimeout(function(){
                $('.bubblebox2').fadeIn(1500);
            },1000);
            setTimeout(function(){
                $('.bubblebox3').fadeIn(1500);
            },1700);

            sound_player("p3_s"+(countNext+1),1);
            break;

      case 9:

            $('.info').show();

            $('.multipel').show();
            $('.whitearrow').show();
            $('.multiple1').show();
            $('.multiple2').show();
            $('.multiple3').show();
            $('.multiple4').show();
            $('.multiple5').show();
            $('.multiple6').show();
            $('.multiple7').show();

            setTimeout(function(){
                $('.bubblebox2').fadeIn(1500);
            },1000);
            setTimeout(function(){
                $('.bubblebox3').fadeIn(1500);
            },1700);

            sound_player("p3_s"+(countNext+1),1);
            break;

      case 10:
          sound_player("p3_s"+(countNext+1),0);
          var count = 0;
          var leftDst = 23;
          $(".textimage").addClass("hoverclass");
          $(".textimage").click(function () {
            // alert(leftDst);
            var value  = $(this).find("p").text();
            if(value%2 == 0){
              count+=1;
              play_correct_incorrect_sound(1);
              $(this).removeClass("hoverclass");
              $(this).addClass("nopntr");
              $(".textimage").css("pointer-events","none");
              $(this).animate({
                bottom:"65%",
                left:leftDst+"%"
              },1000,function(){
                $(".textimage").css("pointer-events","auto");
                leftDst+=11;
              });
              count==5?nav_button_controls(1500):'';
            }else{
              play_correct_incorrect_sound(0);
            }
            // alert(value);
          });

            // dragdrop();

      break;

      case 11:
          sound_player("p3_s"+(countNext+1),1);

            $('.bubblebox1').show();
            // sound_player("p3_s"+(countNext+1),1);
            // nav_button_controls(300);
            break;

      default:


          break;

        }

    }

    function dragdrop(){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var newimg = ui.draggable.attr("data-answer").toString().trim();
                var hideimg = ui.draggable.attr("class").toString().trim();
                console.log('newimg',hideimg);

                if(newimg == ($(this).attr("data-answer").toString().trim())) {
                    // play_correct_incorrect_sound(1);



                }
            }
        });
    }

    function scatterShells(){

        $('.rectanglebox').hide();

        $('.shell').css({'bottom':'15%'});

        $('.shell1').css({'transform':'rotate(-25deg)','right':'21%'});
        $('.shell2').css({'transform':'rotate(20deg)','right':'15%'});
        $('.shell3').css({'transform':'rotate(-10deg)','right':'9%'});
        $('.shell4').css({'transform':'rotate(40deg)','right':'21%'});
        $('.shell5').css({'transform':'rotate(-40deg)'});
        $('.shell6').css({'right':'10%'});

        navigationcontroller();

    }

    function nav_button_controls(delay_ms){
        timeoutvar = setTimeout(function(){
            if(countNext==0){
                $nextBtn.show(0);
            } else if( countNext>0 && countNext == $total_page-1){
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else{
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        },delay_ms);
    }
    function sound_player(sound_id,next){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
      current_sound.on("complete",function(){
        next?nav_button_controls(0):'';
      });
  	}
    function sound_player_nav(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();

    }
    /*=====  End of Templates Block  ======*/

    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
     Motivation :
     - Make a single function call that handles all the
     template load easier

     How To:
     - Update the template caller with the required templates
     - Call template caller

     What it does:
     - According to value of the Global Variable countNext
     the slide templates are updated
     */

    function templateCaller() {
        /*always hide next and previous navigation button unless
         explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call the template
        generalTemplate();

        //call the slide indication bar handler for pink indicators
        loadTimelineProgress($total_page, countNext + 1);

        /*
        //   for (var i = 0; i < content.length; i++) {
        //     slides(i);
        //     $($('.totalsequence')[i]).html(i);
        //     $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
        //   "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
        //   }
        //   function slides(i){
        //       $($('.totalsequence')[i]).click(function(){
        //         countNext = i;
        //         templateCaller();
        //       });
        //     }
        */




        // just for development purpose to see total slide vs current slide number
        // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
    }

    function put_image(content, count){
        if(content[count].hasOwnProperty('imageblock')){
            var imageblock = content[count].imageblock[0];
            if(imageblock.hasOwnProperty('imagestoshow')){
                var imageClass = imageblock.imagestoshow;
                for(var i=0; i<imageClass.length; i++){
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.'+classes_list[classes_list.length-1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    //templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        console.log('nxt clicked');
        clearTimeout(timeoutvar);
        createjs.Sound.stop();
        for(var i=0; i<timeoutArr.length; i++){
            clearTimeout(timeoutArr[i]);
        }
        switch(countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        console.log('$refreshBtn clicked')
        templateCaller();
    });

    $prevBtn.on('click', function() {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        for(var i=0; i<timeoutArr.length; i++){
            clearTimeout(timeoutArr[i]);
        }
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

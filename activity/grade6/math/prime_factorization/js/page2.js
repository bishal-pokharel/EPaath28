var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";

var preload;
var timeoutArr = [];
var timeoutvar = null;


var content = [

    //slide 0

    {
        extratextblock: [{
            textclass: "diy",
            textdata: data.string.diytext,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        }],
        imageblock: [{
            imagestoshow: [
			{
                imgclass: "bg",
                imgsrc: '',
                imgid: 'bg',
            },
			{
				imgclass: "fish",
				imgsrc: '',
				imgid: 'fish',
			},
			{
				imgclass: "shell1",
				imgsrc: '',
				imgid: 'shell',
			},
			{
				imgclass: "shell2",
				imgsrc: '',
				imgid: 'shell',
			},
			{
				imgclass: "shell3",
				imgsrc: '',
				imgid: 'shell',
			},
			{
				imgclass: "shell4",
				imgsrc: '',
				imgid: 'shell',
			},
			{
				imgclass: "shell5",
				imgsrc: '',
				imgid: 'shell',
			},
            ]
        }]
    },

    //slide 1

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text18,

        },
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text20,

        },

        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "draggable shelltest1 ",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "draggable shelltest2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'second'
                },
                {
                    imgclass: "draggable shelltest3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'third'
                },
                {
                    imgclass: "draggable shelltest4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fourth'
                },

                {
                    imgclass: "draggable shelltest5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fifth'
                },
                {
                    imgclass: "draggable shelltest6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'sixth'
                },
                {
                    imgclass: "draggable shelltest7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'seventh'
                },
                {
                    imgclass: "draggable shelltest8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eighth'
                },
                {
                    imgclass: "draggable shelltest9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'ninth'
                },
                {
                    imgclass: "draggable shelltest10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'tenth'
                },
                {
                    imgclass: "draggable shelltest11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eleventh'
                },
                {
                    imgclass: "draggable shelltest12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'twelveth'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[
           {
            tableblkclass: "test1",
            tblextraclass:"tblClass",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        }
                    ]
                },
                {
                    tablerowclass:"row2",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                    ]
                },
                {
                    tablerowclass:"row3",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                }
            ]
        }]

    },

    //slide 2

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text20,

        },
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay1",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay2",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

        tbldivname: 'tableDiv',

        table:[{
            tableblkclass: "test",
            tblextraclass:"tblClass",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        }
                    ]
                },
                {
                    tablerowclass:"row2",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                    ]
                },
                {
                    tablerowclass:"row3",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                }
            ]
        }]

    },

    //slide 3

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text21,

        },
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay1",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay2",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

        tbldivname: 'tableDiv',

        table:[
          {
            tableblkclass: "test",
            tblextraclass:"tblClass",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        }
                    ]
                },
                {
                    tablerowclass:"row2",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                    ]
                },
                {
                    tablerowclass:"row3",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                }
            ]
        }
      ]

    },

    //slide 4

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text22,

        },
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay1",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay2",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

        tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        }
                    ]
                },
                {
                    tablerowclass:"row2",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                    ]
                },
                {
                    tablerowclass:"row3",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                }
            ]
        }]

    },

    //slide 5

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text23,
          },
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
      ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "draggable shelltest1 ",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "draggable shelltest2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'second'
                },
                {
                    imgclass: "draggable shelltest3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'third'
                },
                {
                    imgclass: "draggable shelltest4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fourth'
                },

                {
                    imgclass: "draggable shelltest5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fifth'
                },
                {
                    imgclass: "draggable shelltest6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'sixth'
                },
                {
                    imgclass: "draggable shelltest7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'seventh'
                },
                {
                    imgclass: "draggable shelltest8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eighth'
                },
                {
                    imgclass: "draggable shelltest9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'ninth'
                },
                {
                    imgclass: "draggable shelltest10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'tenth'
                },
                {
                    imgclass: "draggable shelltest11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eleventh'
                },
                {
                    imgclass: "draggable shelltest12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'twelveth'
                },

            ]
        }],

    },

    //slide 6

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text18,

        },
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text20,

        },
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },


       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "draggable shelltest1 ",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "draggable shelltest2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'second'
                },
                {
                    imgclass: "draggable shelltest3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'third'
                },
                {
                    imgclass: "draggable shelltest4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fourth'
                },

                {
                    imgclass: "draggable shelltest5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fifth'
                },
                {
                    imgclass: "draggable shelltest6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'sixth'
                },
                {
                    imgclass: "draggable shelltest7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'seventh'
                },
                {
                    imgclass: "draggable shelltest8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eighth'
                },
                {
                    imgclass: "draggable shelltest9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'ninth'
                },
                {
                    imgclass: "draggable shelltest10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'tenth'
                },
                {
                    imgclass: "draggable shelltest11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eleventh'
                },
                {
                    imgclass: "draggable shelltest12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'twelveth'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test2",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                }
            ],
        }
      ],


    },

    //slide 7

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text24,

        },

        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "correct option1",
            textdata: '1 x 12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "incorrect option2",
            textdata: '3 x 4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay3",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay4",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
              ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test2",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                }
            ],
        }],
    },

    //slide 8

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text25,

        },

        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

        {
            textclass: "factorDisplay3",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay4",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
              ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test2",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                }
            ],
        }],
    },

    //slide 9

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text26,

        },
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn3",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn4",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

        {
            textclass: "factorDisplay3",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay4",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
              ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test2",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                }
            ],
        }],
    },

    //slide 10

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text27,

        },

        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn3",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn4",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
      ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltest1 ",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltest2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'second'
                },
                {
                    imgclass: "shelltest3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'third'
                },
                {
                    imgclass: "shelltest4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fourth'
                },

                {
                    imgclass: "shelltest5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fifth'
                },
                {
                    imgclass: "shelltest6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'sixth'
                },
                {
                    imgclass: "shelltest7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'seventh'
                },
                {
                    imgclass: "shelltest8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eighth'
                },
                {
                    imgclass: "shelltest9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'ninth'
                },
                {
                    imgclass: "shelltest10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'tenth'
                },
                {
                    imgclass: "shelltest11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eleventh'
                },
                {
                    imgclass: "shelltest12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'twelveth'
                },

              ]
        }],

    },

    //slide 11

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text18,

        },
        {
            bubbleboxadditionalclass:'bubblebox2',
            bubbleimg: '',
            bubbleclass:'cloudsright',
            bubbletextclass: 'text2',
            bubbletextdata: data.string.p1text20,

        },

        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn3",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn4",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "draggable shelltest1 ",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "draggable shelltest2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'second'
                },
                {
                    imgclass: "draggable shelltest3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'third'
                },
                {
                    imgclass: "draggable shelltest4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fourth'
                },

                {
                    imgclass: "draggable shelltest5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'fifth'
                },
                {
                    imgclass: "draggable shelltest6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'sixth'
                },
                {
                    imgclass: "draggable shelltest7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'seventh'
                },
                {
                    imgclass: "draggable shelltest8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eighth'
                },
                {
                    imgclass: "draggable shelltest9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'ninth'
                },
                {
                    imgclass: "draggable shelltest10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'tenth'
                },
                {
                    imgclass: "draggable shelltest11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'eleventh'
                },
                {
                    imgclass: "draggable shelltest12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'twelveth'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test3",
            tablerow:[
                  {
                      tablerowclass:"row1",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                      ]
                  },
                  {
                      tablerowclass:"row2",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },
                      ]
                  }
            ]
        }]

    },

    //slide 12

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factorDisplay5",
              textdata: '2',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factorDisplay6",
              textdata: '6',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text28,
        }
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn3",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn4",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "incorrect option1",
            textdata: '8',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "correct option2",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test3",
            tablerow:[
                  {
                    tablerowclass:"row1",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                      ]
                  },
                  {
                      tablerowclass:"row2",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },
                      ]
                  }
            ]
        }]

    },

    //slide 13

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factorDisplay5",
              textdata: '2',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factorDisplay6",
              textdata: '6',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text29,
        }
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn3",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn4",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "incorrect option1",
            textdata: '1 x 12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "correct option2",
            textdata: '2 x 6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "fish1",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test3",
            tablerow:[
                  {
                      tablerowclass:"row1",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                      ]
                  },
                  {
                      tablerowclass:"row2",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },
                      ]
                  }
            ]
        }]

    },

    //slide 14

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factorDisplay5",
              textdata: '2',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factorDisplay6",
              textdata: '6',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text30,
        }
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn3",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn4",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test3",
            tablerow:[
                  {
                      tablerowclass:"row1",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                      ]
                  },
                  {
                      tablerowclass:"row2",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },
                      ]
                  }
            ]
        }]

    },

    //slide 15

    {

        lowertextblockadditionalclass: 'factorDiv',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },

      ],


        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text31,
        }
        ],

        extratextblock: [{
            textclass: "info",
            textdata: data.string.p1text17,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn1",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn2",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn3",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn4",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn5",
            textdata: '2',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn6",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay5",
            textdata: '2',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay6",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[{
            tblextraclass:"tblClass",
            tableblkclass: "test3",
            tablerow:[
                  {
                      tablerowclass:"row1",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                      ]
                  },
                  {
                      tablerowclass:"row2",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },
                      ]
                  }
            ]
        }]

    },

    //slide 16

    {

        lowertextblockadditionalclass: 'factorDiv1',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },

      ],

        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text32,
        }
        ],

        extratextblock: [
        {
            textclass: "info",
            textdata: data.string.p1text19,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn11",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn22",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn33",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn44",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn55",
            textdata: '2',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn66",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[
          {
            tblextraclass:"tblClass",
            tableblkclass: "test4",
            tablerow:[
                  {
                      tablerowclass:"row1",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                      ]
                  },
                  {
                      tablerowclass:"row2",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },
                      ]
                  }
              ]
          },
          {
            tableblkclass: "test5",
            tblextraclass:"tblClass",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        }
                    ]
                },
                {
                    tablerowclass:"row2",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                    ]
                },
                {
                    tablerowclass:"row3",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                  }
            ]
          },
          {
              tblextraclass:"tblClass",
              tableblkclass: "test6",
              tablerow:[{
                tablerowclass:"row1",
                tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },

                      ]
                  }
              ],
          }


        ]

    },

    //slide 17

    {

        lowertextblockadditionalclass: 'factorDiv1',
        lowertextblock:[
          {
              textclass: "factor",
              textdata: data.string.p1text19,
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },
          {
              textclass: "factor1",
              textdata: '',
              datahighlightflag: true,
              datahighlightcustomclass: 'grenLtr'
          },

      ],

        bubblebox:[{
            bubbleboxadditionalclass:'bubblebox1',
            bubbleimg: '',
            bubbleclass:'clouds',
            bubbletextclass: 'text1',
            bubbletextdata: data.string.p1text33,
        }
        ],

        extratextblock: [
        {
            textclass: "info",
            textdata: data.string.p1text19,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn11",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn22",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn33",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn44",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn55",
            textdata: '2',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplayIn66",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay11",
            textdata: '4',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay22",
            textdata: '3',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay33",
            textdata: '1',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay44",
            textdata: '12',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay55",
            textdata: '2',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        {
            textclass: "factorDisplay66",
            textdata: '6',
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

       ],
        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'bg',
                },
                {
                    imgclass: "fish",
                    imgsrc: '',
                    imgid: 'fish',
                },
                {
                    imgclass: "shelltestfill1",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill2",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill3",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill4",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill5",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill6",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill7",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill8",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill9",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill10",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill11",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },
                {
                    imgclass: "shelltestfill12",
                    imgsrc: '',
                    imgid: 'shell',
                    ans: 'first'
                },

            ]
        }],

      //  tbldivname: 'tableDiv',

        table:[
          {
            tblextraclass:"tblClass",
            tableblkclass: "test4",
            tablerow:[
                  {
                      tablerowclass:"row1",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                      ]
                  },
                  {
                      tablerowclass:"row2",
                      tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },
                      ]
                  }
              ]
          },
          {
            tableblkclass: "test5",
            tblextraclass:"tblClass",
            tablerow:[
                {
                    tablerowclass:"row1",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count0",
                            textdata:'',
                            ans: 'first'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count1",
                            textdata:'',
                            ans: 'second'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count2",
                            textdata:'',
                            ans: 'third'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count3",
                            textdata:'',
                            ans: 'fourth'
                        }
                    ]
                },
                {
                    tablerowclass:"row2",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count4",
                            textdata:'',
                            ans: 'fifth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count5",
                            textdata:'',
                            ans: 'sixth'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count6",
                            textdata:'',
                            ans: 'seventh'
                        },
                        {
                            tabledataclass:"droppable cell ",
                            textclass:"count7",
                            textdata:'',
                            ans: 'eighth'
                        },
                    ]
                },
                {
                    tablerowclass:"row3",
                    tabledata:[
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count8",
                            textdata:'',
                            ans: 'ninth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count9",
                            textdata:'',
                            ans: 'tenth'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count10",
                            textdata:'',
                            ans: 'eleventh'
                        },
                        {
                            tabledataclass:"droppable cell",
                            textclass:"count11",
                            textdata:'',
                            ans: 'twelveth'
                        },

                    ]
                  }
            ]
          },
          {
              tblextraclass:"tblClass",
              tableblkclass: "test6",
              tablerow:[{
                tablerowclass:"row1",
                tabledata:[
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count0",
                              textdata:'',
                              ans: 'first'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count1",
                              textdata:'',
                              ans: 'second'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count2",
                              textdata:'',
                              ans: 'third'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count3",
                              textdata:'',
                              ans: 'fourth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count4",
                              textdata:'',
                              ans: 'fifth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count5",
                              textdata:'',
                              ans: 'sixth'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count6",
                              textdata:'',
                              ans: 'seventh'
                          },
                          {
                              tabledataclass:"droppable cell ",
                              textclass:"count7",
                              textdata:'',
                              ans: 'eighth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count8",
                              textdata:'',
                              ans: 'ninth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count9",
                              textdata:'',
                              ans: 'tenth'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count10",
                              textdata:'',
                              ans: 'eleventh'
                          },
                          {
                              tabledataclass:"droppable cell",
                              textclass:"count11",
                              textdata:'',
                              ans: 'twelveth'
                          },

                      ]
                  }
              ],
          }


        ]

    },

]

$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    loadTimelineProgress($total_page, countNext + 1);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            //images

            {id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rectanglebox", src: imgpath+"rectangle_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "shell", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rectanglebox1", src: imgpath+"rectangle_box_six.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rectangleboxshell1", src: imgpath+"rectangle_box_shell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rectangleboxshell2", src: imgpath+"rectangle_box_six_shell.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "p2_s2", src: soundAsset+"p2_s2.ogg"},
            {id: "p2_s3", src: soundAsset+"p2_s3.ogg"},
            {id: "p2_s4", src: soundAsset+"p2_s4.ogg"},
            {id: "p2_s5", src: soundAsset+"p2_s5.ogg"},
            {id: "p2_s6", src: soundAsset+"p2_s6.ogg"},
            {id: "p2_s7", src: soundAsset+"p2_s7.ogg"},
            {id: "p2_s8", src: soundAsset+"p2_s8.ogg"},
            {id: "p2_s9", src: soundAsset+"p2_s9.ogg"},
            {id: "p2_s10", src: soundAsset+"p2_s10.ogg"},
            {id: "p2_s11", src: soundAsset+"p2_s11.ogg"},
            {id: "p2_s12", src: soundAsset+"p2_s12.ogg"},
            {id: "p2_s13", src: soundAsset+"p2_s13.ogg"},
            {id: "p2_s14", src: soundAsset+"p2_s14.ogg"},
            {id: "p2_s15", src: soundAsset+"p2_s15.ogg"},
            {id: "p2_s16", src: soundAsset+"p2_s16.ogg"},
            {id: "p2_s17", src: soundAsset+"p2_s17.ogg"},
            {id: "p2_s18", src: soundAsset+"p2_s18.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
     =            Handlers and helpers Block            =
     ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

     What it does:
     - send an element where the function has to see
     for data to highlight
     - this function searches for all nodes whose
     data-highlight element is set to true
     -searches for # character and gives a start tag
     ;span tag here, also for @ character and replaces with
     end tag of the respective
     - if provided with data-highlightcustomclass value for highlight it
     applies the custom class or else uses parsedstring class

     E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/

    /*===============================================
    =            user notification function        =
    ===============================================*/
    /**
     How to:
     - First set any html element with
     "data-usernotification='notifyuser'" attribute,
     and "data-isclicked = ''".
     - Then call this function to give notification
     */

    /**
     What it does:
     - You send an element where the function has to see
     for data to notify user
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function notifyuser($notifyinside) {
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if ($allnotifications.length > 0) {
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }

    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */

    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

    }

    /*=====  End of user navigation controller function  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
     =            Templates Block            =
     =======================================*/
    /*=================================================
     =            general template function            =
     =================================================*/
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);

    function generalTemplate() {
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        put_image(content, countNext);

        $('.clouds').attr('src', preload.getResult('clouds').src);
        $('.cloudsright').attr('src', preload.getResult('clouds').src);
        // $('.rectangle').attr('src', preload.getResult('clouds').src);
        $('.balloon').attr('src', preload.getResult('shell').src);
        var count1 = 0;

        function dragdrop(){
            $(".draggable").draggable({
                containment: "body",
                revert: true,
                appendTo: "body"
            });
            $('.droppable').droppable({
                accept : ".draggable",
                hoverClass: "hovered",
                drop: function(event, ui) {
                    var newimg = ui.draggable.attr("data-answer").toString().trim();
                    var hideimg = ui.draggable.attr("class").toString().trim();
                    console.log('newimg',hideimg);

                    if(newimg == ($(this).attr("data-answer").toString().trim())) {
                        // play_correct_incorrect_sound(1);

                        count1 = count1 + 1;

                        console.log('count: '+count1);
                        play_correct_incorrect_sound(1);
                        $('.shelltest'+count1).hide();
                        $('.shelltestfill'+count1).show();
                        $('.count'+(count1-1)).css({'background-color':'transparent'});

                        highlightNextImageAndCell(count1);

                        if (count1 == 12){
                          nav_button_controls(0);

                        }

                    }

                }
            });
        }

        function highlightNextImageAndCell(count){

            var count1 = 'count'+count;

            $('.'+count1).css({'background-color': '#fffd99','width':'100%','height':'100%'});
            $('.shelltest'+(count+1)).css({'border':'3px solid #ffff00'});

        }
        switch(countNext){

            case 0:
            play_diy_audio();
            nav_button_controls(2000);
            break;

            case 1:
              sound_player("p2_s"+(countNext+1),0);
              $('.fish1').hide();
              $('.bubblebox2').hide();
              $('.shelltest1').css({'border': '3px solid #ffff00'});
              $('.count0').css({'background-color': '#fffd99','width':'100%','height':'100%'});
              $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').hide();
              dragdrop();
            break;

            case 2:
              sound_player("p2_s"+(countNext+1),1);

                $('.fish1').hide();
                $('.bubblebox2').hide();
                $('.factorDisplay1').hide();
                $('.factorDisplay2').hide();
                setTimeout(function(){
                      $('.fish1').fadeIn(1500);
                      $('.bubblebox2').fadeIn(1500);
                },100);
                setTimeout(function(){
                  $('.factorDisplay1').fadeIn(1500);
                },1500);
                setTimeout(function(){
                  $('.factorDisplay2').fadeIn(1500);
                },1700);

                // nav_button_controls(2500);

                break;

            case 3:

              $('.fish1').show();
              $('.bubblebox2').hide();
              $('.factorDisplay1').show();
              $('.factorDisplay2').show();
              setTimeout(function(){
                    //$('.fish1').fadeIn(1500);
                    $('.bubblebox2').fadeIn(1500);
              },100);
              sound_player("p2_s"+(countNext+1),1);

              break;

            case 4:
              $('.fish1').show();
              $('.bubblebox2').hide();
              $('.factorDisplay1').show();
              $('.factorDisplay2').show();
              setTimeout(function(){
                    //$('.fish1').fadeIn(1500);
                    $('.bubblebox2').fadeIn(1500);
              },100);
              sound_player("p2_s"+(countNext+1),1);
            break;
            case 5:
              $(".draggable").css("pointer-events","none");

              $('.fish1').hide();
              $('.bubblebox1').hide();
              $('.factorDisplay1').show();
              $('.factorDisplay2').show();
              setTimeout(function(){
                    //$('.fish1').fadeIn(1500);
                    $('.bubblebox1').fadeIn(1500);
              },100);
              sound_player("p2_s"+(countNext+1),1);
            break;

            case 6:
              count1 = 0;
               $('.fish1').hide();
               $('.bubblebox1').hide();
               $('.fish1').hide();
               $('.bubblebox2').hide();
               setTimeout(function(){
                     //$('.fish1').fadeIn(1500);
                     $('.bubblebox1').fadeIn(1500);
               },100);
               $('.shelltest1').css({'border': '3px solid #ffff00'});
               $('.count0').css({'background-color': '#fffd99','width':'100%','height':'100%'});

               $('.tableDiv').css({'top':'25%','width':'65%','left':'8%','height':'10%'});
               $('.shelltestfill1').css({'width': '5%','height':'8%','top':'26%','left':'8.2%'});
               $('.shelltestfill2').css({'width': '5%','height':'8%','top':'26%','left':'13.8%'});
               $('.shelltestfill3').css({'width': '5%','height':'8%','top':'26%','left':'18.9%'});
               $('.shelltestfill4').css({'width': '5%','height':'8%','top':'26%','left':'24.7%'});
               $('.shelltestfill5').css({'width': '5%','height':'8%','top':'26%','left':'30%'});
               $('.shelltestfill6').css({'width': '5%','height':'8%','top':'26%','left':'35.3%'});
               $('.shelltestfill7').css({'width': '5%','height':'8%','top':'26%','left':'40.6%'});
               $('.shelltestfill8').css({'width': '5%','height':'8%','top':'26%','left':'46%'});
               $('.shelltestfill9').css({'width': '5%','height':'8%','top':'26%','left':'51.5%'});
               $('.shelltestfill10').css({'width': '5%','height':'8%','top':'26%','left':'56.8%'});
               $('.shelltestfill11').css({'width': '5%','height':'8%','top':'26%','left':'62.3%'});
               $('.shelltestfill12').css({'width': '5%','height':'8%','top':'26%','left':'67.8%'});

               $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').hide();


              $('.cell').css({'width':'0%'});

              dragdrop();
              sound_player("p2_s"+(countNext+1),0);
            break;

            case 7:

               $('.fish1').hide();
               $('.bubblebox1').hide();
               $('.fish1').hide();
               $('.bubblebox2').hide();
               $('.shelltest1').css({'border': '3px solid #ffff00'});

               setTimeout(function(){
                     //$('.fish1').fadeIn(1500);
                     $('.bubblebox1').fadeIn(1500);
               },100);

               $('.tableDiv').css({'top':'25%','width':'65%','left':'8%','height':'10%'});
               $('.shelltestfill1').css({'width': '5%','height':'8%','top':'26%','left':'8.2%'});
               $('.shelltestfill2').css({'width': '5%','height':'8%','top':'26%','left':'13.8%'});
               $('.shelltestfill3').css({'width': '5%','height':'8%','top':'26%','left':'18.9%'});
               $('.shelltestfill4').css({'width': '5%','height':'8%','top':'26%','left':'24.7%'});
               $('.shelltestfill5').css({'width': '5%','height':'8%','top':'26%','left':'30%'});
               $('.shelltestfill6').css({'width': '5%','height':'8%','top':'26%','left':'35.3%'});
               $('.shelltestfill7').css({'width': '5%','height':'8%','top':'26%','left':'40.6%'});
               $('.shelltestfill8').css({'width': '5%','height':'8%','top':'26%','left':'46%'});
               $('.shelltestfill9').css({'width': '5%','height':'8%','top':'26%','left':'51.5%'});
               $('.shelltestfill10').css({'width': '5%','height':'8%','top':'26%','left':'56.8%'});
               $('.shelltestfill11').css({'width': '5%','height':'8%','top':'26%','left':'62.3%'});
               $('.shelltestfill12').css({'width': '5%','height':'8%','top':'26%','left':'67.8%'});

               $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').show();
               $('.cell').css({'width':'0%'});

               checkans();
               sound_player("p2_s"+(countNext+1),0);

               break;

            case 8:

               $('.fish1').hide();
               $('.bubblebox1').hide();
               $('.fish1').hide();

               setTimeout(function(){
                 $('.bubblebox1').fadeIn(1500);
               },1000)


               $('.shelltest1').css({'border': '3px solid #ffff00'});

               $('.tableDiv').css({'top':'25%','width':'65%','left':'8%','height':'10%'});
               $('.shelltestfill1').css({'width': '5%','height':'8%','top':'26%','left':'8.2%'});
               $('.shelltestfill2').css({'width': '5%','height':'8%','top':'26%','left':'13.8%'});
               $('.shelltestfill3').css({'width': '5%','height':'8%','top':'26%','left':'18.9%'});
               $('.shelltestfill4').css({'width': '5%','height':'8%','top':'26%','left':'24.7%'});
               $('.shelltestfill5').css({'width': '5%','height':'8%','top':'26%','left':'30%'});
               $('.shelltestfill6').css({'width': '5%','height':'8%','top':'26%','left':'35.3%'});
               $('.shelltestfill7').css({'width': '5%','height':'8%','top':'26%','left':'40.6%'});
               $('.shelltestfill8').css({'width': '5%','height':'8%','top':'26%','left':'46%'});
               $('.shelltestfill9').css({'width': '5%','height':'8%','top':'26%','left':'51.5%'});
               $('.shelltestfill10').css({'width': '5%','height':'8%','top':'26%','left':'56.8%'});
               $('.shelltestfill11').css({'width': '5%','height':'8%','top':'26%','left':'62.3%'});
               $('.shelltestfill12').css({'width': '5%','height':'8%','top':'26%','left':'67.8%'});

               $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').show();
               $('.cell').css({'width':'0%'});
               sound_player("p2_s"+(countNext+1),1);

               break;

            case 9:

               $('.fish1').hide();
               $('.bubblebox1').hide();
               $('.fish1').hide();
               $('.factorDisplayIn3').hide();
               $('.factorDisplayIn4').hide();

               setTimeout(function(){
                 $('.bubblebox1').fadeIn(1000);
               },1000);

               setTimeout(function(){
                 $('.factorDisplayIn3').fadeIn(1000);
               },1500);

               setTimeout(function(){
                 $('.factorDisplayIn4').fadeIn(1000);
               },2000);


               $('.shelltest1').css({'border': '3px solid #ffff00'});

               $('.tableDiv').css({'top':'25%','width':'65%','left':'8%','height':'10%'});
               $('.shelltestfill1').css({'width': '5%','height':'8%','top':'26%','left':'8.2%'});
               $('.shelltestfill2').css({'width': '5%','height':'8%','top':'26%','left':'13.8%'});
               $('.shelltestfill3').css({'width': '5%','height':'8%','top':'26%','left':'18.9%'});
               $('.shelltestfill4').css({'width': '5%','height':'8%','top':'26%','left':'24.7%'});
               $('.shelltestfill5').css({'width': '5%','height':'8%','top':'26%','left':'30%'});
               $('.shelltestfill6').css({'width': '5%','height':'8%','top':'26%','left':'35.3%'});
               $('.shelltestfill7').css({'width': '5%','height':'8%','top':'26%','left':'40.6%'});
               $('.shelltestfill8').css({'width': '5%','height':'8%','top':'26%','left':'46%'});
               $('.shelltestfill9').css({'width': '5%','height':'8%','top':'26%','left':'51.5%'});
               $('.shelltestfill10').css({'width': '5%','height':'8%','top':'26%','left':'56.8%'});
               $('.shelltestfill11').css({'width': '5%','height':'8%','top':'26%','left':'62.3%'});
               $('.shelltestfill12').css({'width': '5%','height':'8%','top':'26%','left':'67.8%'});

               $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').show();
               $('.cell').css({'width':'0%'});
               sound_player("p2_s"+(countNext+1),1);

               break;

            case 10:

                  $('.fish1').hide();
                  $('.bubblebox1').show();
                  $('.fish1').hide();
                  $('.factorDisplayIn3').show();
                  $('.factorDisplayIn4').show();

                  $('.tableDiv').css({'top':'25%','width':'65%','left':'8%','height':'10%'});
                  $('.shelltestfill1').css({'width': '5%','height':'8%','top':'26%','left':'8.2%'});
                  $('.shelltestfill2').css({'width': '5%','height':'8%','top':'26%','left':'13.8%'});
                  $('.shelltestfill3').css({'width': '5%','height':'8%','top':'26%','left':'18.9%'});
                  $('.shelltestfill4').css({'width': '5%','height':'8%','top':'26%','left':'24.7%'});
                  $('.shelltestfill5').css({'width': '5%','height':'8%','top':'26%','left':'30%'});
                  $('.shelltestfill6').css({'width': '5%','height':'8%','top':'26%','left':'35.3%'});
                  $('.shelltestfill7').css({'width': '5%','height':'8%','top':'26%','left':'40.6%'});
                  $('.shelltestfill8').css({'width': '5%','height':'8%','top':'26%','left':'46%'});
                  $('.shelltestfill9').css({'width': '5%','height':'8%','top':'26%','left':'51.5%'});
                  $('.shelltestfill10').css({'width': '5%','height':'8%','top':'26%','left':'56.8%'});
                  $('.shelltestfill11').css({'width': '5%','height':'8%','top':'26%','left':'62.3%'});
                  $('.shelltestfill12').css({'width': '5%','height':'8%','top':'26%','left':'67.8%'});

                  $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').hide();
                  $('.cell').css({'width':'0%'});
                  sound_player("p2_s"+(countNext+1),1);
                  break;

            case 11:
                  count1 = 0;

                  $('.cell').css({'width':'0%'});
                  $('.fish1').hide();
                  $('.bubblebox2').hide();
                  $('.shelltest1').css({'border': '3px solid #ffff00'});
                  $('.count0').css({'background-color': '#fffd99','width':'100%','height':'100%'});

                  $('.tableDiv').css({'top':'15%','width':'50%','left':'24%','height':'25%'});
                  $('.shelltestfill1').css({'top':'16%','left':'25%'});
                  $('.shelltestfill2').css({'top':'16%','left':'33.5%'});
                  $('.shelltestfill3').css({'top':'16%','left':'41.5%'});
                  $('.shelltestfill4').css({'top':'16%','left':'50%'});
                  $('.shelltestfill5').css({'top':'16%','left':'58%'});
                  $('.shelltestfill6').css({'top':'16%','left':'66.5%'});
                  $('.shelltestfill7').css({'top':'29%','left':'25%'});
                  $('.shelltestfill8').css({'top':'29%','left':'33%'});
                  $('.shelltestfill9').css({'top':'29%','left':'42%'});
                  $('.shelltestfill10').css({'top':'29%','left':'50%'});
                  $('.shelltestfill11').css({'top':'29%','left':'58%'});
                  $('.shelltestfill12').css({'top':'29%','left':'66.5%'});

                  $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').hide();

                  dragdrop();
                  sound_player("p2_s"+(countNext+1),0);
                  break;

            case 12:


                        $('.cell').css({'width':'0%'});
                        $('.fish1').hide();
                        $('.bubblebox2').hide();
                        $('.shelltest1').css({'border': '3px solid #ffff00'});
                      //  $('.count0').css({'background-color': '','width':'100%','height':'100%'});

                        $('.tableDiv').css({'top':'15%','width':'50%','left':'24%','height':'25%'});
                        $('.shelltestfill1').css({'top':'16%','left':'25%'});
                        $('.shelltestfill2').css({'top':'16%','left':'33.5%'});
                        $('.shelltestfill3').css({'top':'16%','left':'41.5%'});
                        $('.shelltestfill4').css({'top':'16%','left':'50%'});
                        $('.shelltestfill5').css({'top':'16%','left':'58%'});
                        $('.shelltestfill6').css({'top':'16%','left':'66.5%'});
                        $('.shelltestfill7').css({'top':'29%','left':'25%'});
                        $('.shelltestfill8').css({'top':'29%','left':'33%'});
                        $('.shelltestfill9').css({'top':'29%','left':'42%'});
                        $('.shelltestfill10').css({'top':'29%','left':'50%'});
                        $('.shelltestfill11').css({'top':'29%','left':'58%'});
                        $('.shelltestfill12').css({'top':'29%','left':'66.5%'});

                        $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').show();

                        checkans1();
                        sound_player("p2_s"+(countNext+1),0);

                        break;

            case 13:


                        $('.cell').css({'width':'0%'});
                        $('.fish1').hide();
                        $('.bubblebox2').hide();
                        $('.shelltest1').css({'border': '3px solid #ffff00'});
                      //  $('.count0').css({'background-color': '','width':'100%','height':'100%'});

                        $('.tableDiv').css({'top':'15%','width':'50%','left':'24%','height':'25%'});
                        $('.shelltestfill1').css({'top':'16%','left':'25%'});
                        $('.shelltestfill2').css({'top':'16%','left':'33.5%'});
                        $('.shelltestfill3').css({'top':'16%','left':'41.5%'});
                        $('.shelltestfill4').css({'top':'16%','left':'50%'});
                        $('.shelltestfill5').css({'top':'16%','left':'58%'});
                        $('.shelltestfill6').css({'top':'16%','left':'66.5%'});
                        $('.shelltestfill7').css({'top':'29%','left':'25%'});
                        $('.shelltestfill8').css({'top':'29%','left':'33%'});
                        $('.shelltestfill9').css({'top':'29%','left':'42%'});
                        $('.shelltestfill10').css({'top':'29%','left':'50%'});
                        $('.shelltestfill11').css({'top':'29%','left':'58%'});
                        $('.shelltestfill12').css({'top':'29%','left':'66.5%'});

                        $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').show();

                        checkans1();
                        sound_player("p2_s"+(countNext+1),0);

                        break;

              case 14:


                          $('.cell').css({'width':'0%'});
                          $('.fish1').hide();
                          $('.bubblebox2').hide();
                          $('.shelltest1').css({'border': '3px solid #ffff00'});
                        //  $('.count0').css({'background-color': '','width':'100%','height':'100%'});

                          $('.tableDiv').css({'top':'15%','width':'50%','left':'24%','height':'25%'});
                          $('.shelltestfill1').css({'top':'16%','left':'25%'});
                          $('.shelltestfill2').css({'top':'16%','left':'33.5%'});
                          $('.shelltestfill3').css({'top':'16%','left':'41.5%'});
                          $('.shelltestfill4').css({'top':'16%','left':'50%'});
                          $('.shelltestfill5').css({'top':'16%','left':'58%'});
                          $('.shelltestfill6').css({'top':'16%','left':'66.5%'});
                          $('.shelltestfill7').css({'top':'29%','left':'25%'});
                          $('.shelltestfill8').css({'top':'29%','left':'33%'});
                          $('.shelltestfill9').css({'top':'29%','left':'42%'});
                          $('.shelltestfill10').css({'top':'29%','left':'50%'});
                          $('.shelltestfill11').css({'top':'29%','left':'58%'});
                          $('.shelltestfill12').css({'top':'29%','left':'66.5%'});

                          $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').show();

                          sound_player("p2_s"+(countNext+1),1);

                          break;

                case 15:

                            $('.factorDiv').css({'height':'39%'});
                            $('.cell').css({'width':'0%'});
                            $('.fish1').hide();
                            $('.bubblebox2').hide();
                            $('.shelltest1').css({'border': '3px solid #ffff00'});
                          //  $('.count0').css({'background-color': '','width':'100%','height':'100%'});

                            $('.tableDiv').css({'top':'15%','width':'50%','left':'24%','height':'25%'});
                            $('.shelltestfill1').css({'top':'16%','left':'25%'});
                            $('.shelltestfill2').css({'top':'16%','left':'33.5%'});
                            $('.shelltestfill3').css({'top':'16%','left':'41.5%'});
                            $('.shelltestfill4').css({'top':'16%','left':'50%'});
                            $('.shelltestfill5').css({'top':'16%','left':'58%'});
                            $('.shelltestfill6').css({'top':'16%','left':'66.5%'});
                            $('.shelltestfill7').css({'top':'29%','left':'25%'});
                            $('.shelltestfill8').css({'top':'29%','left':'33%'});
                            $('.shelltestfill9').css({'top':'29%','left':'42%'});
                            $('.shelltestfill10').css({'top':'29%','left':'50%'});
                            $('.shelltestfill11').css({'top':'29%','left':'58%'});
                            $('.shelltestfill12').css({'top':'29%','left':'66.5%'});

                            $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').show();

                            sound_player("p2_s"+(countNext+1),1);

                            break;

                case 16:

                            $('.factorDiv').css({'height':'39%'});
                            $('.cell').css({'width':'0%'});
                            $('.fish1').hide();
                            $('.bubblebox2').hide();
                            $('.shelltest1').css({'border': '3px solid #ffff00'});
                            //  $('.count0').css({'background-color': '','width':'100%','height':'100%'});

                            $('.tableDiv').css({'top':'15%','width':'50%','left':'24%','height':'25%'});
                            $('.shelltestfill1').css({'top':'16%','left':'25%'});
                            $('.shelltestfill2').css({'top':'16%','left':'33.5%'});
                            $('.shelltestfill3').css({'top':'16%','left':'41.5%'});
                            $('.shelltestfill4').css({'top':'16%','left':'50%'});
                            $('.shelltestfill5').css({'top':'16%','left':'58%'});
                            $('.shelltestfill6').css({'top':'16%','left':'66.5%'});
                            $('.shelltestfill7').css({'top':'29%','left':'25%'});
                            $('.shelltestfill8').css({'top':'29%','left':'33%'});
                            $('.shelltestfill9').css({'top':'29%','left':'42%'});
                            $('.shelltestfill10').css({'top':'29%','left':'50%'});
                            $('.shelltestfill11').css({'top':'29%','left':'58%'});
                            $('.shelltestfill12').css({'top':'29%','left':'66.5%'});

                            $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').hide();

                            sound_player("p2_s"+(countNext+1),1);

                            break;

                case 17:

                            $('.factorDiv').css({'height':'39%'});
                            $('.cell').css({'width':'0%'});
                            $('.fish1').hide();
                            $('.bubblebox2').hide();
                            $('.shelltest1').css({'border': '3px solid #ffff00'});
                            //  $('.count0').css({'background-color': '','width':'100%','height':'100%'});

                            $('.tableDiv').css({'top':'15%','width':'50%','left':'24%','height':'25%'});
                            $('.shelltestfill1').css({'top':'16%','left':'25%'});
                            $('.shelltestfill2').css({'top':'16%','left':'33.5%'});
                            $('.shelltestfill3').css({'top':'16%','left':'41.5%'});
                            $('.shelltestfill4').css({'top':'16%','left':'50%'});
                            $('.shelltestfill5').css({'top':'16%','left':'58%'});
                            $('.shelltestfill6').css({'top':'16%','left':'66.5%'});
                            $('.shelltestfill7').css({'top':'29%','left':'25%'});
                            $('.shelltestfill8').css({'top':'29%','left':'33%'});
                            $('.shelltestfill9').css({'top':'29%','left':'42%'});
                            $('.shelltestfill10').css({'top':'29%','left':'50%'});
                            $('.shelltestfill11').css({'top':'29%','left':'58%'});
                            $('.shelltestfill12').css({'top':'29%','left':'66.5%'});

                            $('.shelltestfill1,.shelltestfill2,.shelltestfill3,.shelltestfill4,.shelltestfill5,.shelltestfill6,.shelltestfill7,.shelltestfill8,.shelltestfill9,.shelltestfill10,.shelltestfill11,.shelltestfill12').hide();

                            sound_player("p2_s"+(countNext+1),1);

                            break;


            default:

                break;

        }

    }

  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
  function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
    current_sound.on('complete', function(){
      next?nav_button_controls(300):'';
    });
	}

    /*=====  End of Templates Block  ======*/

    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
     Motivation :
     - Make a single function call that handles all the
     template load easier

     How To:
     - Update the template caller with the required templates
     - Call template caller

     What it does:
     - According to value of the Global Variable countNext
     the slide templates are updated
     */

    function checkans(){

      // correct and incorrect actions for all
      $('.option1,.option2').click(function(){
          if($(this).hasClass('correct'))
          {
              var $this = $(this);
              var position = $this.position();
              var width = $this.width();
              var height = $this.height();
              // var centerX = ((position.left + width / 2)*100)+'%';
              // var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
              $('<img style="right:31%;top:78%;position:absolute;width:5%;transform:translate(-34%,26%)" src="'+imgpath +'correct.png" />').insertAfter(this);
              $('.option1,.option2').css({"pointer-events":"none"});
              $(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
              play_correct_incorrect_sound(1);

              nav_button_controls(200);

          }
          else {
              var $this = $(this);
              var position = $this.position();
              var width = $this.width();
              var height = $this.height();
              // var centerX = ((position.left + width / 2)*100)+'%';
              // var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
              $('<img style="right:8%;top:78%;position:absolute;width:5%;transform:translate(-34%,26%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
              play_correct_incorrect_sound(0);
              $(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
          }
      });


    }

    function checkans1(){

      // correct and incorrect actions for all
      $('.option1,.option2').click(function(){
          if($(this).hasClass('incorrect'))
          {
              var $this = $(this);
              var position = $this.position();
              var width = $this.width();
              var height = $this.height();
              // var centerX = ((position.left + width / 2)*100)+'%';
              // var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
              $('<img style="right:31%;top:78%;position:absolute;width:5%;transform:translate(-34%,26%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
              $(this).css({"background":"rgb(168, 33, 36)","color":"white","border-color":"#980000"});
              play_correct_incorrect_sound(0);

          }
          else {
              var $this = $(this);
              var position = $this.position();
              var width = $this.width();
              var height = $this.height();
              // var centerX = ((position.left + width / 2)*100)+'%';
              // var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
              $('<img style="right:8%;top:78%;position:absolute;width:5%;transform:translate(-34%,26%)" src="'+imgpath +'correct.png" />').insertAfter(this);
              $('.option1,.option2').css({"pointer-events":"none"});
              play_correct_incorrect_sound(1);
              $(this).css({"background":"rgb(190,214,47)","color":"white","pointer-events":"none","border-color":"#e0ff59"});
              nav_button_controls(100);
          }
      });


    }

    function templateCaller() {
        /*always hide next and previous navigation button unless
         explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call the template
        generalTemplate();

        //call the slide indication bar handler for pink indicators
        loadTimelineProgress($total_page, countNext + 1);

        // just for development purpose to see total slide vs current slide number
        // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
    }

    function put_image(content, count){
        if(content[count].hasOwnProperty('imageblock')){
            var imageblock = content[count].imageblock[0];
            if(imageblock.hasOwnProperty('imagestoshow')){
                var imageClass = imageblock.imagestoshow;
                for(var i=0; i<imageClass.length; i++){
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.'+classes_list[classes_list.length-1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    //templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        console.log('nxt clicked');
        clearTimeout(timeoutvar);
        createjs.Sound.stop();
        for(var i=0; i<timeoutArr.length; i++){
            clearTimeout(timeoutArr[i]);
        }
        switch(countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        console.log('$refreshBtn clicked');
        count1 = 0;
        templateCaller();
    });

    $prevBtn.on('click', function() {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        for(var i=0; i<timeoutArr.length; i++){
            clearTimeout(timeoutArr[i]);
        }
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

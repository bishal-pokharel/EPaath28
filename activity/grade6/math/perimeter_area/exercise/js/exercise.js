var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/p1/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text10,
			textclass: "text-ques ques-data",
			datahighlightflag : true,
			datahighlightcustomclass : 'font-blue',
		},{
			textdata: data.string.p1text14,
			textclass: "bt-feedback"
		}],

		uppertextblockadditionalclass: 'text-hint',
		uppertextblock:[{
			textdata: data.string.p1text12,
			textclass: "",
		},{
			textdata: data.string.p1text13,
			textclass: "",
			datahighlightflag : true,
			datahighlightcustomclass : 'non-visible',
		}],

		inputblock:[{
			inputdiv: 'input-1',
			textclass1: '',
			textdata1: data.string.p1text11,
			buttonclass: '',
			inputdata: '',
			unitdata: data.string.m,
			buttondata: data.string.submit,
		}],
		polygon:[{
			polygonclass: 'tri-2',
			side:[{
				sidediv: 'tri-side2 t2-side-1',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side2 t2-side-2',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side2 t2-side-3',
				sidelabel: data.string.m5
			}],
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'para-1',
		uppertextblock:[{
			textclass: ' ques-data',
			textdata: data.string.p1text19,
			datahighlightflag : true,
			datahighlightcustomclass : 'text-1-hl',
		},{
			textdata: data.string.p1text19a,
		}],
		extratextblock:[{
			textclass: 'last-full its_hidden',
			textdata: data.string.p1text21,
		}],
		inputblock:[{
			inputdiv: 'input-2',
			textclass1: 'leqtext',
			textdata1: data.string.p1text22,
			buttonclass: '',
			inputdata: '',
			unitdata: data.string.cm,
			buttondata: data.string.submit,
		}],
		lowertextblockadditionalclass: 'hint-formula',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p1text20,
			splitintofractionsflag: true,
		}],
		polygon:[{
			polygonclass: 'tri-3',
			side:[{
				sidediv: 'tri-side2 t2-side-1',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-2',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-3',
				sidelabel: data.string.l
			}],
		}],
	},




	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',

		extratextblock:[{
			textdata: data.string.p2text18,
			textclass: "ques-text  ques-data",
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		polygon:[{
			polygonclass: 'ques-poly rect-count',
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],

		inputblock:[{
			inputdiv: 'input-3',
			textclass1: '',
			textdata1: data.string.area,
			buttonclass: '',
			inputdata: '',
			unitdata: data.string.cmsq,
			buttondata: data.string.submit,
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',

		extratextblock:[{
			textdata: data.string.p2text19,
			textclass: "ques-text  ques-data",
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		},{
			textdata: data.string.p2text20,
			textclass: "hint-text"
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],

		inputblock:[{
			inputdiv: 'input-3',
			textclass1: '',
			textdata1: data.string.area,
			buttonclass: 'btn-2',
			inputdata: '',
			unitdata: data.string.cmsq,
			buttondata: data.string.submit,
		}]
	},

	 // slide4
	 {
		 contentnocenteradjust: true,
		 contentblockadditionalclass: 'bg-4',
		 extratextblock:[{
			 textclass: 'ques-text-1  ques-data',
			 textdata: data.string.p3text17,
			 datahighlightflag : true,
			 datahighlightcustomclass : 'hl-brown',
		 },{
			 textdata: data.string.m3,
			 textclass: "tag tag-3"
		 },{
			 textdata: data.string.m9,
			 textclass: "tag tag-4"
		 }],
		 uppertextblockadditionalclass: 'hint-text-4',
		 uppertextblock:[{
			 textclass: '',
			 textdata: data.string.p3text18,
		 },{
			 textclass: '',
			 textdata: data.string.p3text19,
		 },{
			 textclass: '',
			 textdata: data.string.p3text20,
			 datahighlightflag : true,
			 datahighlightcustomclass : 'hl-blue',
		 }],
		 inputblock:[{
			 inputdiv: 'input-4',
			 textclass1: '',
			 textdata1: data.string.area,
			 buttonclass: 'btn-2',
			 inputdata: '',
			 unitdata: data.string.msq,
			 buttondata: data.string.submit,
		 }],

		 imageblock:[{
			 imagestoshow : [{
				 imgclass : "garden",
				 imgsrc : '',
				 imgid : 'garden-1'
			 },
			 {
				 imgclass : "arrow-1",
				 imgsrc : '',
				 imgid : 'h-arr'
			 },{
				 imgclass : "arrow-2",
				 imgsrc : '',
				 imgid : 'v-arr'
			 }]
		 }],
	 },
	 	// slide5
	 	{
	 		contentnocenteradjust: true,
	 		contentblockadditionalclass: 'bg-3',

	 		lowertextblockadditionalclass: 'road-2',
	 		lowertextblock:[{
	 			textclass: '',
	 			textdata: data.string.p3text31,
	 		}],
	 		extratextblock:[{
	 			textclass: 'ques-text-1 qt-2 ques-data',
	 			textdata: data.string.e1text11,
	 		},{
	 			textclass: 'road-1',
	 			textdata: '',
	 		},{
	 			textclass: 'tag tag-2m',
	 			textdata: data.string.m2,
	 		}],


 		 uppertextblockadditionalclass: 'hint-text-4 hint-text-5',
 		 uppertextblock:[{
 			 textclass: '',
 			 textdata: data.string.e1text1,
 		 },{
 			 textclass: '',
 			 textdata: data.string.e1text2,
 		 },{
 			 textclass: '',
 			 textdata: data.string.e1text3,
 			 datahighlightflag : true,
 			 datahighlightcustomclass : 'hl-blue',
 		 }],

	 		inputblock:[{
	 			inputdiv: 'input-5',
	 			textclass1: '',
	 			textdata1: data.string.length,
	 			buttonclass: 'btn-2',
	 			inputdata: '',
	 			unitdata: data.string.m,
	 			buttondata: data.string.submit,
	 		}],
	 		imageblock:[{
	 			imagestoshow : [{
	 				imgclass : "pond-2",
	 				imgsrc : '',
	 				imgid : 'pond'
	 			},{
	 				imgclass : "garden-2",
	 				imgsrc : '',
	 				imgid : 'garden-1'
	 			},{
	 				imgclass : "arrow-4",
	 				imgsrc : '',
	 				imgid : 'v-arr2'
	 			}]
	 		}],
	 	},
	 // slide6
	 {
		 contentnocenteradjust: true,
		 contentblockadditionalclass: 'bg-6',
		 extratextblock:[{
			 textclass: 'ques-text-6  ques-data',
			 textdata: data.string.e1text4,
			 datahighlightflag : true,
			 datahighlightcustomclass : 'hl-black',
		 },{
			 textdata: data.string.p4text2a,
			 textclass: "tag tag-6"
		 },{
			 textdata: data.string.p4text2b,
			 textclass: "tag tag-6a"
		 }],
		 uppertextblockadditionalclass: 'hint-text-4 hint-text-6',
		 uppertextblock:[{
			 textclass: '',
			 textdata: data.string.p4text2c,
		 },{
			 textclass: '',
			 textdata: data.string.p4text2d,
		 },{
			 textclass: '',
			 textdata: data.string.p4text2e,
			 datahighlightflag : true,
			 datahighlightcustomclass : 'hl-blue',
		 }],
		 inputblock:[{
			 inputdiv: 'input-6',
			 textclass1: '',
			 textdata1: data.string.area,
			 buttonclass: 'btn-6',
			 inputdata: '',
			 unitdata: data.string.msq,
			 buttondata: data.string.submit,
		 }],

		 imageblock:[{
			 imagestoshow : [
			 {
				 imgclass : "arrow-6",
				 imgsrc : '',
				 imgid : 'h-arr'
			 },{
				 imgclass : "arrow-6a",
				 imgsrc : '',
				 imgid : 'v-arr'
			 }]
		 }],
	 },


	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-6',
		extratextblock:[{
			textclass: 'ques-text-6  ques-data',
			textdata: data.string.e1text5,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-black',
		},{
			textdata: data.string.p4text6a,
			textclass: "tag tag-7"
		},{
			textdata: data.string.p4text6b,
			textclass: "tag tag-7a"
		}],
		uppertextblockadditionalclass: 'hint-text-4 hint-text-7',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p4text6c,
		},{
			textclass: '',
			textdata: data.string.p4text6d,
		},{
			textclass: '',
			textdata: data.string.p4text6e,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		inputblock:[{
			inputdiv: 'input-7',
			textclass1: '',
			textdata1: data.string.e1text8,
			buttonclass: 'btn-7',
			inputdata: '',
			unitdata: data.string.msq,
			buttondata: data.string.submit,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "pond-7",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "arrow-7",
				imgsrc : '',
				imgid : 'h-arr'
			},{
				imgclass : "arrow-7a",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},

 // slide8
 {
 	contentnocenteradjust: true,
 	contentblockadditionalclass: 'bg-6',
 	extratextblock:[{
 		textclass: 'ques-text-6  ques-data',
 		textdata: data.string.e1text6,
 		datahighlightflag : true,
 		datahighlightcustomclass : 'hl-black',
 	},{
		textdata: data.string.p4text2a,
		textclass: "tag tag-6"
	},{
		textdata: data.string.p4text2b,
		textclass: "tag tag-6a"
	},{
		textdata: data.string.e1text12,
		textclass: "pond-area-text",
 		datahighlightflag : true,
 		datahighlightcustomclass : 'hl-blue',
	}],
 	uppertextblockadditionalclass: 'hint-text-4 hint-text-8',
 	uppertextblock:[{
 		textclass: '',
 		textdata: data.string.e1text6c,
 	},{
 		textclass: '',
 		textdata: data.string.e1text6d,
 	}],
 	inputblock:[{
 		inputdiv: 'input-8',
 		textclass1: '',
 		textdata1: data.string.e1text9,
 		buttonclass: 'btn-7',
 		inputdata: '',
 		unitdata: data.string.msq,
 		buttondata: data.string.submit,
 	}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "pond-7",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "arrow-6",
				imgsrc : '',
				imgid : 'h-arr'
			},{
				imgclass : "arrow-6a",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
 },
  // slide9
  {
  	contentnocenteradjust: true,
  	contentblockadditionalclass: 'bg-6',
  	extratextblock:[{
  		textclass: 'ques-text-6  ques-data',
  		textdata: data.string.e1text7,
  		datahighlightflag : true,
  		datahighlightcustomclass : 'hl-black',
  	},{
 		textdata: data.string.p4text6a,
 		textclass: "tag tag-7"
 	},{
 		textdata: data.string.p4text6b,
 		textclass: "tag tag-7a"
 	}],
  	uppertextblockadditionalclass: 'hint-text-4 hint-text-9',
  	uppertextblock:[{
  		textclass: '',
  		textdata: data.string.e1text7a,
  	},{
  		textclass: '',
  		textdata: data.string.e1text7b,
  	},{
  		textclass: '',
  		textdata: data.string.e1text7c,
  	},{
  		textclass: '',
  		textdata: data.string.e1text7d,
  	}],
  	inputblock:[{
  		inputdiv: 'input-7',
  		textclass1: '',
  		textdata1: data.string.e1text10,
  		buttonclass: 'btn-7',
  		inputdata: '',
  		unitdata: data.string.m,
  		buttondata: data.string.submit,
  	}],

 		imageblock:[{
 			imagestoshow : [{
 				imgclass : "pond-7",
 				imgsrc : '',
 				imgid : 'pond'
 			},{
 				imgclass : "fence",
 				imgsrc : '',
 				imgid : 'fence'
 			},{
 				imgclass : "arrow-7",
 				imgsrc : '',
 				imgid : 'h-arr'
 			},{
 				imgclass : "arrow-7a",
 				imgsrc : '',
 				imgid : 'v-arr'
 			}]
 		}],
  },
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var scoring = new NumberTemplate();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var index = 0;
	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "graph", src: imgpath+"graph.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "pond", src: imgpath+"p3/pond.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fence", src: imgpath+"p4/fence-icon.png", type: createjs.AbstractLoader.IMAGE},

			{id: "v-arr", src: imgpath+"p3/varr.png", type: createjs.AbstractLoader.IMAGE},
			{id: "v-arr2", src: imgpath+"p3/varr2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "h-arr", src: imgpath+"p3/harr.png", type: createjs.AbstractLoader.IMAGE},

			{id: "garden", src: imgpath+"p3/garden.png", type: createjs.AbstractLoader.IMAGE},
			{id: "garden-1", src: imgpath+"p3/flower.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		scoring.init($total_page);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	var quesNums = ['1. ','2. ','3. ','4. ','5. ','6. ','7. ','8. ','9. ','10. '];
	if($lang=='np'){
		quesNums = ['१) ','२) ','३) ','४) ','५) ','६) ','७) ','८) ','९) ','१०) '];
	}
	var wrong_clicked = 0;

	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		put_image(content, countNext);
		$('.ques-data').html(quesNums[countNext]+$('.ques-data').html());
		wrong_clicked = 0;
		$prevBtn.hide(0);

		switch(countNext) {
			case 0:
				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 15);
				});
				break;
			case 1:
				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 40);
				});
				break;
				case 2:
					var num_arr = ['1','2','3','4','5','6','7','8','9','10','11',
									'12','13','14','15','16','17','18', '19', '20', '21', '22', '23', '24'];
					for(var i=0; i<24; i++){
						$('.rect-count').append('<div class="rect-num-block3"><p></p></div>');
					}
					for(var i=0; i<24; i++){
						$('.rect-num-block3').eq(i).children('p').html(num_arr[i]);
					}
					var s = Snap('#bar-graph');
					var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
						s.append(loadedFragment);
					});
					input_box('.ans-input-box', 2, '.check-btn');
					$('.check-btn').click(function(){
						checker_2('.ans-input-box', 24, show_hint_1(true));
					});
					function show_hint_1(isCorrect){
							$('.rect-num-block3').css('opacity', '1');
					}
					break;
				case 3:
					var hintCount = 0;
					var s = Snap('#bar-graph');
					var boxes = [];
					var triangles = [];
					var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						//to resive whole svg
						boxes.push(Snap.select('#rect-36'));
						boxes.push(Snap.select('#rect-46'));
						boxes.push(Snap.select('#rect-56'));
						boxes.push(Snap.select('#rect-65'));
						boxes.push(Snap.select('#rect-66'));

						triangles.push(Snap.select('#tri-37-b'));
						triangles.push(Snap.select('#tri-57-a'));
						triangles.push(Snap.select('#tri-55-b'));
						triangles.push(Snap.select('#tri-64-b'));
						for(var i = 0; i<boxes.length; i++){
							boxes[i].attr({'fill': '#B6D7A8', 'fill-opacity':'1'});
						}
						for(var i = 0; i<triangles.length; i++){
							triangles[i].attr({
								'fill': '#B6D7A8',
								'opacity': '1'
							});
						}
						flip_triangle(triangles[0]);
					} );
					input_box('.ans-input-box', 2, '.check-btn');
					$('.check-btn').click(function(){
						checker_2('.ans-input-box', 7, show_hint_3(true));
					});
					function show_hint_3(isCorrect){
						$('.hint-text').show(0);
					}
					break;
				case 4:
					$('.arrow-2').css({
															'height': '74%',
															'right': '43%',
															'bottom': '11%'
					});
					$('.arrow-1').css({
															'width': '36%',
															'right': '5%',
															'bottom': '8%'
					});
					$('.tag-3').css({
															'right': '23%',
															'bottom': '6%'
					});
					$('.tag-4').css({
															'right': '42%',
															'bottom': '46%'
					});

					input_box('.ans-input-box', 2, '.check-btn');
					$('.check-btn').click(function(){
						checker_2('.ans-input-box', 27, show_hint_4(true));
					});
					function show_hint_4(isCorrect){
						$('.hint-text-4').show(0);
					}
					break;
				case 5:
					input_box('.ans-input-box', 2, '.check-btn');
					$('.check-btn').click(function(){
						checker_2('.ans-input-box', 9, show_hint_5(true));
					});
					function show_hint_5(isCorrect){
						$('.hint-text-4').show(0);
					}
					break;
				case 6:
					input_box('.ans-input-box', 3, '.check-btn');
					$('.check-btn').click(function(){
						checker_2('.ans-input-box', 120, show_hint_6(true));
					});
					function show_hint_6(isCorrect){
						$('.hint-text-4').show(0);
					}
					break;
				case 7:
					input_box('.ans-input-box', 3, '.check-btn');
					$('.check-btn').click(function(){
						checker_2('.ans-input-box', 30, show_hint_7(true));
					});
					function show_hint_7(isCorrect){
						$('.hint-text-4').show(0);
					}
					break;
				case 8:
					input_box('.ans-input-box', 3, '.check-btn');
					$('.check-btn').click(function(){
						checker_2('.ans-input-box', 90, show_hint_8(true));
					});
					function show_hint_8(isCorrect){
						$('.hint-text-4').show(0);
					}
					break;
				case 9:
					input_box('.ans-input-box', 3, '.check-btn');
					$('.check-btn').click(function(){
						checker_2('.ans-input-box', 22, show_hint_9(true));
					});
					function show_hint_9(isCorrect){
						$('.hint-text-4').show(0);
					}
					break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function checker(inputclass, ans1){
		var ip_1 = $(inputclass).val();
		if(ip_1 === ''){
			return false;
		} else{
			$('.hint-formula').fadeIn(500);
			ip_1 = parseInt($(inputclass).val());
			if(ip_1==ans1){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				$('.input-right-wrong').attr('src',preload.getResult('correct').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(1);
				$('.check-btn').css({
					'background-color': '#98C02E',
					'border': '0.5vmin solid #DEEF3C',
					'color': 'white',
					'pointer-events': 'none'
				});
				$('.last-full').fadeIn(500);
				$('.bt-feedback, .text-hint').fadeIn(500);
				$('.check-btn').unbind('click');
				$('.ans-input-box').trigger('blur');
				$('.ans-input-box').prop('disabled', true);
				$nextBtn.show(0);
			} else{
				scoring.update(false);
				wrong_clicked++;
				$('.input-right-wrong').attr('src',preload.getResult('incorrect').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(0);
				$('.check-btn').css({
					'background-color': '#FF0000',
					'border': '0.5vmin solid #980000',
					'color': 'white'
				});
				$('.text-hint').fadeIn(500);
			}
		}
	}
	function checker_2(inputclass, ans1, func){
		var ip_1 = $(inputclass).val();
		if(ip_1 === ''){
			return false;
		} else{
			ip_1 = parseInt($(inputclass).val());
			if(ip_1==ans1){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				$('.input-right-wrong').attr('src',preload.getResult('correct').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(1);
				$('.check-btn').css({
					'background-color': '#98C02E',
					'border': '0.5vmin solid #DEEF3C',
					'color': 'white',
					'pointer-events': 'none'
				});
				$('.bt-feedback, .text-hint').fadeIn(500);
				$('.check-btn').unbind('click');
				$('.ans-input-box').trigger('blur');
				$('.ans-input-box').prop('disabled', true);
				$nextBtn.show(0);
			} else{
				scoring.update(false);
				wrong_clicked++;
				$('.input-right-wrong').attr('src',preload.getResult('incorrect').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(0);
				$('.check-btn').css({
					'background-color': '#FF0000',
					'border': '0.5vmin solid #980000',
					'color': 'white'
				});
				$('.text-hint').fadeIn(500);
			}
		}
	}

	function flip_triangle(triangle){
		var cx = triangle.attr('points')[0];
		var cy = triangle.attr('points')[1];
		var transform_val = 'rotate(90deg,'+ cx + ',' + cy + ')';
		triangle.attr({'transform': transform_val});
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});


	$prevBtn.on('click', function() {
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

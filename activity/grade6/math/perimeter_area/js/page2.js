var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang + "/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textclass: 'text-1',
			textdata: data.string.p2text1,
		},{
			textclass: 'tag tag-1',
			textdata: data.string.cm3,
		},{
			textclass: 'tag tag-2',
			textdata: data.string.cmbr6,
		},{
			textclass: 'tag tag-3',
			textdata: data.string.cm3,
		},{
			textclass: 'tag tag-4',
			textdata: data.string.cmbr3,
		}],
		polygon:[{
			polygonclass: 'rect-big rb-1',
		},{
			polygonclass: 'rect-small rs-1',
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textclass: 'text-1',
			textdata: data.string.p2text1,
		},{
			textclass: 'text-2',
			textdata: data.string.p2text2,
		}],
		polygon:[{
			polygonclass: 'rect-big rb-1',
		},{
			polygonclass: 'rect-small rs-1',
		},{
			polygonclass: 'rect-big rb-2 fade-in-1',
		},{
			polygonclass: 'rect-small rs-2 fade-in-1',
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textclass: 'text-1',
			textdata: data.string.p2text3,
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2',
		},{
			polygonclass: 'rect-small rs-2',
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textclass: 'text-1',
			textdata: data.string.p2text3,
		},{
			textclass: 'text-2',
			textdata: data.string.p2text4,
		},{
			textclass: 'incorrect-fb',
			textdata: data.string.p2text6,
		},{
			textclass: 'correct-fb',
			textdata: data.string.p2text5,
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2 rect-opt highlight-rect',
			imgclass: 'rect-correct-incorrect right-opt'
		},{
			polygonclass: 'rect-small rs-2  rect-opt highlight-rect',
			imgclass: 'rect-correct-incorrect wrong-opt'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey-fb",
					imgsrc : '',
					imgid : 'monkey-correct'
				},
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'para-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p2text7,
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2 ',
			imgclass: 'rect-correct-incorrect right-opt'
		},{
			polygonclass: 'rect-small rs-2 ',
			imgclass: 'rect-correct-incorrect wrong-opt'
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'para-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p2text7,
		},{
			textclass: 'p-yellow-text',
			textdata: data.string.p2text8,
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2 ',
			imgclass: 'rect-correct-incorrect right-opt'
		},{
			polygonclass: 'rect-small rs-2 ',
			imgclass: 'rect-correct-incorrect wrong-opt'
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'para-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p2text7,
		},{
			textclass: 'p-yellow-text',
			textdata: data.string.p2text8,
		}],
		extratextblock:[{
			textclass: 'unit-sq-text',
			textdata: data.string.p2text9,
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2 ',
			imgclass: 'rect-correct-incorrect right-opt'
		},{
			polygonclass: 'rect-small rs-2 ',
			imgclass: 'rect-correct-incorrect wrong-opt'
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'sp-1',
		uppertextblock:[{
			textclass: '',
			textdata : data.string.p2text10,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey-1",
					imgsrc : '',
					imgid : 'monkey-1'
				},
			]
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2 ',
			imgclass: 'rect-correct-incorrect right-opt'
		},{
			polygonclass: 'rect-small rs-2 ',
			imgclass: 'rect-correct-incorrect wrong-opt'
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'sp-1',
		uppertextblock:[{
			textclass: 'count-text',
			textdata : ' ',
		},{
			textclass: 'hidet-1',
			textdata : data.string.p2text11,
		},{
			textclass: 'hidet-2',
			textdata : data.string.p2text12,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey-1",
					imgsrc : '',
					imgid : 'monkey-1'
				},
			]
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2 rect-count',
			imgclass: 'rect-correct-incorrect right-opt'
		},{
			polygonclass: 'rect-small rs-2 ',
			imgclass: 'rect-correct-incorrect wrong-opt'
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'sp-1',
		uppertextblock:[{
			textclass: 'count-text',
			textdata : ' ',
		},{
			textclass: 'hidet-1',
			textdata : data.string.p2text13,
		},{
			textclass: 'hidet-2',
			textdata : data.string.p2text14,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey-1",
					imgsrc : '',
					imgid : 'monkey-1'
				},
			]
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2 rect-count rect-count-1',
		},{
			polygonclass: 'rect-small rs-2 rect-count rect-count-2',
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'sp-1',
		uppertextblock:[{
			textclass: '',
			textdata : data.string.p2text15,
		},{
			textclass: 'text-center',
			textdata : data.string.p2text16,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		},{
			textclass: 'text-center',
			textdata : data.string.p2text17,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-red',
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey-1",
					imgsrc : '',
					imgid : 'monkey-1'
				},
			]
		}],
		polygon:[{
			polygonclass: 'rect-big rb-2 rect-count rect-count-1',
		},{
			polygonclass: 'rect-small rs-2 rect-count rect-count-2',
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],
	},
	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "diy-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-diy'
				},
			]
		}],
	},
	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[{
			textdata: data.string.p2text18,
			textclass: "ques-text",
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sundar",
					imgsrc : '',
					imgid : 'monkey-incorrect'
				},
			]
		}],
		polygon:[{
			polygonclass: 'ques-poly rect-count',
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],

		inputblock:[{
			inputdiv: 'input-1',
			textclass1: '',
			textdata1: data.string.area,
			buttonclass: '',
			inputdata: '',
			unitdata: data.string.cmsq,
			buttondata: data.string.check,
		}]
	},
	// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[{
			textdata: data.string.p2text19,
			textclass: "ques-text",
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		},{
			textdata: data.string.hint,
			textclass: "hint-btn"
		},{
			textdata: data.string.p2text20,
			textclass: "hint-text"
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "correct-feedback",
					imgsrc : '',
					imgid : 'correct'
				},
			]
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],

		inputblock:[{
			inputdiv: 'input-1',
			textclass1: '',
			textdata1: data.string.area,
			buttonclass: 'btn-2',
			inputdata: '',
			unitdata: data.string.cmsq,
			buttondata: data.string.check,
		}]
	},
	// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[{
			textdata: data.string.p2text21,
			textclass: "diy-text my_font_very_big",
		}],
	},
	// slide15
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[{
			textdata: data.string.p2text19,
			textclass: "ques-text",
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		},{
			textdata: data.string.hint,
			textclass: "hint-btn"
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "correct-feedback",
					imgsrc : '',
					imgid : 'correct'
				},
			]
		}],
		svgblock: [{
			svgblock: 'bar-graph',
		}],

		inputblock:[{
			inputdiv: 'input-1',
			textclass1: '',
			textdata1: data.string.area,
			buttonclass: 'btn-2',
			inputdata: '',
			unitdata: data.string.cmsq,
			buttondata: data.string.check,
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var index = 0;
	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;
	var my_interval;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "graph", src: imgpath+"graph.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "bg-diy", src: imgpath+"bg_diy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tb", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},

			{id: "monkey-1", src: "images/sundar/normal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey-correct", src: "images/sundar/correct-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey-incorrect", src: "images/sundar/incorrect-2.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_3a", src: soundAsset+"s2_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s2_p4_2.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s2_p8.ogg"},
			{id: "sound_8a", src: soundAsset+"s2_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s2_p9_2.ogg"},
			{id: "sound_8c", src: soundAsset+"s2_p9_3.ogg"},
			{id: "sound_9a", src: soundAsset+"s2_p10_1.ogg"},
			{id: "sound_9b", src: soundAsset+"s2_p10_2.ogg"},
			{id: "sound_9c", src: soundAsset+"s2_p10_3.ogg"},
			{id: "sound_10", src: soundAsset+"s2_p11.ogg"},
			{id: "s2_p15", src: soundAsset+"s2_p15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		splitintofractions($board);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
			case 1:
			case 2:
			case 4:

				sound_player("sound_"+(countNext));
				break;
			case 3:
				sound_nav("sound_3a");
				$('.wrong-opt').attr('src', preload.getResult('incorrect').src);
				$('.right-opt').attr('src', preload.getResult('correct').src);
				$('.rect-opt').hover(function(){
					$('.rect-opt').removeClass('highlight-rect');
				}, function(){

				});
				$('.rect-small').click(function(){
					createjs.Sound.stop();
					play_correct_incorrect_sound(0);
					$('.incorrect-fb').show(0);
					$('.text-1').addClass('text-1a');
					$(this).css('pointer-events', 'none');
					$('.text-1').show(0);
					$('.text-2').hide(0);
					$('.monkey-fb').show(0);
					$('.wrong-opt').show(0);
					$('.monkey-fb').attr('src', preload.getResult('monkey-incorrect').src);
				});
				$('.rect-big').click(function(){
					// play_correct_incorrect_sound(1);
					$('.incorrect-fb').hide(0);
					$('.rect-opt').css('pointer-events', 'none');
					$('.correct-fb').show(0);
					$('.text-1').removeClass('text-1a');
					$('.text-1').show(0);
					$('.text-2').hide(0);
					$('.monkey-fb').show(0);
					$('.right-opt').show(0);
					$('.monkey-fb').attr('src', preload.getResult('monkey-correct').src);
					sound_player("sound_3b");
				});
				break;
			case 5:
				sound_player("sound_"+(countNext));
				var s = Snap('#bar-graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				break;
			case 6:
			sound_player("sound_"+(countNext));
				var s = Snap('#bar-graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var unit_sq = Snap.select('#rect-91');
					unit_sq.attr({
							'fill': '#E69138',
							'fill-opacity': '1',
							'opacity': '1'
						});
					unit_sq.addClass('highlight-rect');
				});
				break;
			case 7:
				sound_player("sound_"+(countNext));
				$('.sp-1').css({
					'background-image': 'url('+ preload.getResult('tb').src +')',
					'background-size': '100% 100%'
				});
				var s = Snap('#bar-graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				break;
			case 8:

				$('.sp-1').css({
					'background-image': 'url('+ preload.getResult('tb').src +')',
					'background-size': '100% 100%'
				});
				count = 0;
				var prev_str = '';
				var s = Snap('#bar-graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				var num_arr = ['1','2','3','4','5','6','7','8','9','10','11',
								'12','13','14','15','16','17','18'];
				var tb_num_arr = ['1, ','2, ','3, ','4, ','5, ','6, ','7, ','8, ','9, ','10, ','11, ',
								'12, ','13, ','14, ','15, ','16, ','17, ','18.'];
				for(var i=0; i<18; i++){
					$('.rect-count').append('<div class="rect-num-block"><p></p></div>');
				}
				for(var i=0; i<18; i++){
					$('.rect-num-block').eq(i).children('p').html(num_arr[i]);
				}
				my_interval = setInterval(function(){
					$('.sp-1>.count-text').html(prev_str+tb_num_arr[count]);
					prev_str += tb_num_arr[count];
					$('.rect-num-block').eq(count).css('opacity', '1');
					count++;
					if(count>17){
						$('.hidet-1').fadeIn(500, function(){
							$('.hidet-2').fadeIn(500, function(){

							});
						});
						clearInterval(my_interval);
					}
				}, 200);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_8a");
				current_sound.play();
				current_sound.on("complete", function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_8b");
					current_sound.play();
					current_sound.on("complete", function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_8c");
						current_sound.play();
						current_sound.on("complete", function(){
							nav_button_controls(0);
						});
					});
				});
				break;
			case 9:

				$('.sp-1').css({
					'background-image': 'url('+ preload.getResult('tb').src +')',
					'background-size': '100% 100%'
				});
				count = 0;
				var prev_str = '';
				var s = Snap('#bar-graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				var num_arr = ['1','2','3','4','5','6','7','8','9','10','11',
								'12','13','14','15','16','17','18'];
				var num_arr2 = ['1','2','3','4','5','6','7','8','9'];
				var tb_num_arr2 = ['1, ','2, ','3, ','4, ','5, ','6, ','7, ','8, ','9.'];
				for(var i=0; i<18; i++){
					$('.rect-count-1').append('<div class="rect-num-block2"><p></p></div>');
				}
				for(var i=0; i<9; i++){
					$('.rect-count-2').append('<div class="rect-num-block"><p></p></div>');
				}
				for(var i=0; i<18; i++){
					$('.rect-num-block2').eq(i).children('p').html(num_arr[i]);
				}
				for(var i=0; i<9; i++){
					$('.rect-num-block').eq(i).children('p').html(num_arr[i]);
				}
				my_interval = setInterval(function(){
					$('.sp-1>.count-text').html(prev_str+tb_num_arr2[count]);
					prev_str += tb_num_arr2[count];
					$('.rect-num-block').eq(count).css('opacity', '1');
					count++;
					if(count>8){
						$('.hidet-1').fadeIn(500, function(){
							$('.hidet-2').fadeIn(500, function(){

							});
						});
						clearInterval(my_interval);
					}
				}, 200);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_9a");
				current_sound.play();
				current_sound.on("complete", function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_9b");
					current_sound.play();
					current_sound.on("complete", function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_9c");
						current_sound.play();
						current_sound.on("complete", function(){
							nav_button_controls(0);
						});
					});
				});
				break;
			case 10:
			sound_player("sound_"+countNext);
				$('.sp-1').css({
					'background-image': 'url('+ preload.getResult('tb').src +')',
					'background-size': '100% 100%'
				});
				count = 0;
				var prev_str = '';
				var s = Snap('#bar-graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				var num_arr = ['1','2','3','4','5','6','7','8','9','10','11',
								'12','13','14','15','16','17','18'];
				var tb_num_arr2 = ['1, ','2, ','3, ','4, ','5, ','6, ','7, ','8, ','9.'];
				for(var i=0; i<18; i++){
					$('.rect-count-1').append('<div class="rect-num-block2"><p></p></div>');
				}
				for(var i=0; i<9; i++){
					$('.rect-count-2').append('<div class="rect-num-block2"><p></p></div>');
				}
				for(var i=0; i<18; i++){
					$('.rect-count-1>.rect-num-block2').eq(i).children('p').html(num_arr[i]);
				}
				for(var i=0; i<9; i++){
					$('.rect-count-2>.rect-num-block2').eq(i).children('p').html(num_arr[i]);
				}
				break;
			case 11:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 12:
				$prevBtn.show(0);
				var num_arr = ['1','2','3','4','5','6','7','8','9','10','11',
								'12','13','14','15','16','17','18', '19', '20', '21', '22', '23', '24'];
				for(var i=0; i<24; i++){
					$('.rect-count').append('<div class="rect-num-block3"><p></p></div>');
				}
				for(var i=0; i<24; i++){
					$('.rect-num-block3').eq(i).children('p').html(num_arr[i]);
				}
				var s = Snap('#bar-graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 24, display_numbers);
				});
				function display_numbers(isCorrect){
					$('.rect-num-block3').css('opacity', '1');
					if(isCorrect){
						$('.sundar').show(0);
						$('.sundar').attr('src', preload.getResult('monkey-correct').src);
					} else{
						$('.sundar').show(0);
						$('.sundar').attr('src', preload.getResult('monkey-incorrect').src);
					}
				}
				break;
			case 13:
				$prevBtn.show(0);
				var hintCount = 0;
				var hint_1, hint_2, hint_3;
				var s = Snap('#bar-graph');
				var boxes = [];
				var triangles = [];
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					boxes.push(Snap.select('#rect-36'));
					boxes.push(Snap.select('#rect-46'));
					boxes.push(Snap.select('#rect-56'));
					boxes.push(Snap.select('#rect-65'));
					boxes.push(Snap.select('#rect-66'));

					triangles.push(Snap.select('#tri-37-b'));
					triangles.push(Snap.select('#tri-57-a'));
					triangles.push(Snap.select('#tri-55-b'));
					triangles.push(Snap.select('#tri-64-b'));
					for(var i = 0; i<boxes.length; i++){
						boxes[i].attr({'fill': '#B6D7A8', 'fill-opacity':'1'});
					}
					for(var i = 0; i<triangles.length; i++){
						triangles[i].attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
					}
					flip_triangle(triangles[0]);
					$('.hint-btn').hover(function(){
						if(hintCount==0){
							$('.hint-text').show(0);
							hint_1();
							timeoutvar = setTimeout(function(){
								hint_2();
							}, 1000);
							timeoutvar2 = setTimeout(function(){
								hint_3();
							}, 2000);
						}
					}, function(){
						clearTimeout(timeoutvar);
						clearTimeout(timeoutvar2);
						reset_triangle();
					});
					reset_triangle = function(){
						for(var i=1; i<=100; i++){
							Snap.select('#tri-'+i+'-b').attr('opacity', '0');
							Snap.select('#tri-'+i+'-a').attr('opacity', '0');
							Snap.select('#rect-'+i).attr('fill-opacity', '0');
						};
						for(var i = 0; i<boxes.length; i++){
							boxes[i].attr({'fill': '#B6D7A8', 'fill-opacity':'1'});
						}
						for(var i = 0; i<triangles.length; i++){
							triangles[i].attr({
								'fill': '#B6D7A8',
								'opacity': '1'
							});
						}
						flip_triangle(triangles[0]);
					};
					hint_1 = function(){
						Snap.select('#tri-64-b').attr('opacity', '0');
						Snap.select('#tri-57-a').attr('opacity', '0');
						Snap.select('#tri-54-b').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-47-a').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
					};

					hint_2 = function(){
						Snap.select('#tri-54-b').attr({
							'opacity': '0'
						});
						Snap.select('#rect-55').attr({
							'fill': '#B6D7A8',
							'fill-opacity': '1',
							'opacity': '1'
						});
						Snap.select('#tri-47-a').attr({
							'opacity': '0'
						});
						Snap.select('#tri-48-a').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
					};

					hint_3 = function(){
						Snap.select('#tri-48-a').attr({
							'opacity': '0'
						});
						Snap.select('#rect-37').attr({
							'fill': '#B6D7A8',
							'fill-opacity': '1',
							'opacity': '1'
						});
					};
				} );
				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 7, display_numbers2);
				});
				function display_numbers2(isCorrect){
					if(isCorrect){
						$('.hint-btn').hide(0);
						$('.correct-feedback').show(0);
						$('.correct-feedback').attr('src', preload.getResult('correct').src);
					} else{
						$('.hint-btn').trigger('click');
						$('.correct-feedback').show(0);
						$('.correct-feedback').attr('src', preload.getResult('incorrect').src);
					}
				}
				break;
			case 15:
				$prevBtn.show(0);
				var hintCount = 0;
				var hint_1, hint_2, hint_3;
				var s = Snap('#bar-graph');
				var boxes = [];
				var triangles = [];
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					boxes.push(Snap.select('#rect-45'));
					boxes.push(Snap.select('#rect-54'));
					boxes.push(Snap.select('#rect-55'));
					boxes.push(Snap.select('#rect-65'));
					boxes.push(Snap.select('#rect-66'));
					boxes.push(Snap.select('#rect-75'));

					triangles.push(Snap.select('#tri-35-b'));
					triangles.push(Snap.select('#tri-44-b'));
					triangles.push(Snap.select('#tri-53-b'));
					triangles.push(Snap.select('#tri-67-a'));
					triangles.push(Snap.select('#tri-76-a'));
					triangles.push(Snap.select('#tri-85-a'));
					for(var i = 0; i<boxes.length; i++){
						boxes[i].attr({'fill': '#B6D7A8', 'fill-opacity':'1'});
					}
					for(var i = 0; i<triangles.length; i++){
						triangles[i].attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
					}
					$('.hint-btn').hover(function(){
						if(hintCount==0){
							$('.hint-text').show(0);
							hint_1();
							timeoutvar = setTimeout(function(){
								hint_2();
							}, 1000);
							timeoutvar2 = setTimeout(function(){
								hint_3();
							}, 2000);
						}
					}, function(){
						clearTimeout(timeoutvar);
						clearTimeout(timeoutvar2);
						reset_triangle();
					});
					reset_triangle = function(){
						for(var i=1; i<=100; i++){
							Snap.select('#tri-'+i+'-b').attr('opacity', '0');
							Snap.select('#tri-'+i+'-a').attr('opacity', '0');
							Snap.select('#rect-'+i).attr('fill-opacity', '0');
						};
						for(var i = 0; i<boxes.length; i++){
							boxes[i].attr({'fill': '#B6D7A8', 'fill-opacity':'1'});
						}
						for(var i = 0; i<triangles.length; i++){
							triangles[i].attr({
								'fill': '#B6D7A8',
								'opacity': '1'
							});
						}
					};
					hint_1 = function(){
						Snap.select('#tri-35-b').attr('opacity', '0');
						Snap.select('#tri-36-b').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-67-a').attr('opacity', '0');
						Snap.select('#tri-57-a').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-53-b').attr('opacity', '0');
						Snap.select('#tri-53-a').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-85-a').attr('opacity', '0');
						Snap.select('#tri-85-b').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
					};

					hint_2 = function(){
						Snap.select('#tri-36-b').attr('opacity', '0');
						Snap.select('#tri-46-b').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-57-a').attr('opacity', '0');
						Snap.select('#tri-56-a').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-53-a').attr('opacity', '0');
						Snap.select('#tri-43-a').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-85-b').attr('opacity', '0');
						Snap.select('#tri-86-b').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
					};

					hint_3 = function(){
						Snap.select('#tri-46-b').attr('opacity', '0');
						Snap.select('#tri-56-b').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-43-a').attr('opacity', '0');
						Snap.select('#tri-44-a').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
						Snap.select('#tri-86-b').attr('opacity', '0');
						Snap.select('#tri-76-b').attr({
							'fill': '#B6D7A8',
							'opacity': '1'
						});
					};
				} );
				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 9, display_numbers3);
				});
				function display_numbers3(isCorrect){
					if(isCorrect){
						$('.hint-btn').hide(0);
						$('.correct-feedback').show(0);
						$('.correct-feedback').attr('src', preload.getResult('correct').src);
					} else{
						$('.hint-btn').trigger('click');
						$('.correct-feedback').show(0);
						$('.correct-feedback').attr('src', preload.getResult('incorrect').src);
					}
				}
				break;
				case 14:
				sound_player('s2_p15');
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function flip_triangle(triangle){
		var cx = triangle.attr('points')[0];
		var cy = triangle.attr('points')[1];
		var transform_val = 'rotate(90deg,'+ cx + ',' + cy + ')';
		triangle.attr({'transform': transform_val});
	}
	function checker(inputclass, ans1, func){
		var ip_1 = $(inputclass).val();
		if(ip_1 === ''){
			return false;
		} else{
			ip_1 = parseInt($(inputclass).val());
			if(ip_1==ans1){
				$('.input-right-wrong').attr('src',preload.getResult('correct').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(1);
				$('.check-btn').css({
					'background-color': '#98C02E',
					'border': '0.5vmin solid #DEEF3C',
					'color': 'white',
					'pointer-events': 'none'
				});
				$('.bt-feedback, .text-hint').fadeIn(500);
				$('.check-btn').unbind('click');
				$('.inputformclass').trigger('blur');
				nav_button_controls(0);
				func(true);
			} else{
				$('.input-right-wrong').attr('src',preload.getResult('incorrect').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(0);
				$('.check-btn').css({
					'background-color': '#FF0000',
					'border': '0.5vmin solid #980000',
					'color': 'white'
				});
				$('.text-hint').fadeIn(500);
				func(false);
			}
		}
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearInterval(my_interval);
		clearTimeout(timeoutvar2);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		clearInterval(my_interval);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

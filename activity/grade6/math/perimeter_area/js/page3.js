var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/" +$lang +"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textclass: 'text-1',
			textdata: data.string.p3text1,
		},
		{
			textclass: 'text-1 newbot',
			textdata: data.string.p3text1a,
		},
		{
			textclass: 'big-formula',
			textdata: data.string.p3text2,
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p3text3,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'pond'
			}]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p3text3,
		},{
			textclass: '',
			textdata: data.string.p3text4,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'pond'
			}]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p3text3,
		},{
			textclass: '',
			textdata: data.string.p3text4,
		},{
			textclass: '',
			textdata: data.string.p3text5,
		}],
		extratextblock:[{
			textclass: 'tag tag-1',
			textdata: data.string.p3text8,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "arrow-1",
				imgsrc : '',
				imgid : 'h-arr'
			}]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p3text3,
		},{
			textclass: '',
			textdata: data.string.p3text4,
		},{
			textclass: '',
			textdata: data.string.p3text5,
		},{
			textclass: '',
			textdata: data.string.p3text6,
		}],
		extratextblock:[{
			textclass: 'tag tag-1',
			textdata: data.string.p3text8,
		},{
			textclass: 'tag tag-2',
			textdata: data.string.p3text7,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "arrow-1",
				imgsrc : '',
				imgid : 'h-arr'
			},{
				imgclass : "arrow-2",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p3text3,
		},{
			textclass: '',
			textdata: data.string.p3text4,
		},{
			textclass: '',
			textdata: data.string.p3text5,
		},{
			textclass: '',
			textdata: data.string.p3text6,
		},{
			textclass: 'ptext-1',
			textdata: data.string.p3text9,
		}],
		extratextblock:[{
			textclass: 'tag tag-1',
			textdata: data.string.p3text8,
		},{
			textclass: 'tag tag-2',
			textdata: data.string.p3text7,
		},{
			textclass: 'what-area',
			textdata: data.string.p3text10,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "arrow-1",
				imgsrc : '',
				imgid : 'h-arr'
			},{
				imgclass : "arrow-2",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: 'text-center',
			textdata: data.string.p3text11,
		},{
			textclass: 'text-center',
			textdata: data.string.p3text12,
		}],
		extratextblock:[{
			textclass: 'tag tag-1 zoomer-1',
			textdata: data.string.p3text8,
		},{
			textclass: 'tag tag-2 zoomer-2',
			textdata: data.string.p3text7,
		},{
			textclass: 'what-area',
			textdata: data.string.p3text10,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "arrow-1",
				imgsrc : '',
				imgid : 'h-arr'
			},{
				imgclass : "arrow-2",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: 'text-center',
			textdata: data.string.p3text11,
		},{
			textclass: 'text-center',
			textdata: data.string.p3text12,
		}],
		lowertextblockadditionalclass: 'top-area',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p3text13,
		},{
			textclass: '',
			textdata: data.string.p3text14,
		},{
			textclass: '',
			textdata: data.string.p3text15,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		extratextblock:[{
			textclass: 'tag tag-1 ',
			textdata: data.string.p3text8,
		},{
			textclass: 'tag tag-2 ',
			textdata: data.string.p3text7,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "arrow-1",
				imgsrc : '',
				imgid : 'h-arr'
			},{
				imgclass : "arrow-2",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: 'text-center',
			textdata: data.string.p3text11,
		},{
			textclass: 'text-center',
			textdata: data.string.p3text12,
		},{
			textclass: 'dont-1',
			textdata: data.string.p3text16,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		lowertextblockadditionalclass: 'top-area',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p3text13,
		},{
			textclass: '',
			textdata: data.string.p3text14,
		},{
			textclass: '',
			textdata: data.string.p3text15,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		extratextblock:[{
			textclass: 'tag tag-1 ',
			textdata: data.string.p3text8,
		},{
			textclass: 'tag tag-2 ',
			textdata: data.string.p3text7,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "arrow-1",
				imgsrc : '',
				imgid : 'h-arr'
			},{
				imgclass : "arrow-2",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},
	// slide9
	// {
	// 	contentnocenteradjust: true,
	// 	contentblockadditionalclass: 'bg-diy',
	// 	extratextblock:[{
	// 		textclass: 'diy-text',
	// 		textdata: data.string.diytext,
	// 	}],
	//
	// 	imageblock:[{
	// 		imagestoshow : [{
	// 			imgclass : "monkey-2",
	// 			imgsrc : '',
	// 			imgid : 'monkey'
	// 		}]
	// 	}],
	// },

	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',
		extratextblock:[{
			textclass: 'ques-text',
			textdata: data.string.p3text17,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-brown',
		},{
			textdata: data.string.hint,
			textclass: "hint-btn"
		},{
			textdata: data.string.m3,
			textclass: "tag tag-3"
		},{
			textdata: data.string.m9,
			textclass: "tag tag-4"
		}],
		uppertextblockadditionalclass: 'hint-text',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p3text18,
		},{
			textclass: '',
			textdata: data.string.p3text19,
		},{
			textclass: '',
			textdata: data.string.p3text20,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		inputblock:[{
			inputdiv: 'input-1',
			textclass1: '',
			textdata1: data.string.area,
			buttonclass: 'btn-2',
			inputdata: '',
			unitdata: data.string.msq,
			buttondata: data.string.check,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey-fb",
				imgsrc : '',
				imgid : 'monkey-incorrect'
			},{
				imgclass : "garden",
				imgsrc : '',
				imgid : 'garden-1'
			},
			{
				imgclass : "arrow-1",
				imgsrc : '',
				imgid : 'h-arr'
			},{
				imgclass : "arrow-2",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},
	// slide11
	// {
	// 	contentnocenteradjust: true,
	// 	contentblockadditionalclass: 'bg-3',
	//
	// 	uppertextblockadditionalclass: 'speech-monkey sp-2',
	// 	uppertextblock:[{
	// 		textclass: '',
	// 		textdata: data.string.p3text21,
	// 	},{
	// 		textclass: 'dont-1',
	// 		textdata: data.string.p3text22,
	// 	}],
	//
	// 	imageblock:[{
	// 		imagestoshow : [{
	// 			imgclass : "monkey-3",
	// 			imgsrc : '',
	// 			imgid : 'monkey'
	// 		},{
	// 			imgclass : "garden",
	// 			imgsrc : '',
	// 			imgid : 'garden-1'
	// 		}]
	// 	}],
	// },

	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',

		uppertextblockadditionalclass: 'speech-monkey sp-3',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p3text23,
		},{
			textclass: 'dont-1',
			textdata: data.string.p3text24,
		}],
		lowertextblockadditionalclass: 'road-1',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p3text29,
		}],
		extratextblock:[{
			textdata: data.string.p3text28,
			textclass: "tag tag-5"
		}],
		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey-4",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "pond-2",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "garden-2",
				imgsrc : '',
				imgid : 'garden-1'
			},{
				imgclass : "arrow-3",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},
	// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',
		uppertextblockadditionalclass: 'formula',
		uppertextblock:[{
			textclass: 'for-a',
			textdata: data.string.a,
		},{
			textclass: 'for-l',
			textdata: data.string.l,
		},{
			textclass: 'for-eq',
			textdata: data.string.eq,
		},{
			textclass: 'for-x',
			textdata: data.string.x,
		},{
			textclass: 'for-b',
			textdata: data.string.b,
		}],
		lowertextblockadditionalclass: 'road-1',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p3text29,
		}],
		extratextblock:[{
			textdata: data.string.p3text29,
			textclass: "tag tag-5"
		},{
			textclass: 'modify-btn',
			textdata: data.string.modify,
		},{
			textclass: 'last-feedback',
			textdata: data.string.p3text25,
		}],
		imageblock:[{
			imagestoshow : [{
				imgclass : "pond-2",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "garden-2",
				imgsrc : '',
				imgid : 'garden-1'
			},{
				imgclass : "arrow-3",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},
	// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',

		lowertextblockadditionalclass: 'road-1',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p3text29,
		}],
		extratextblock:[{
			textdata: data.string.p3text28,
			textclass: "tag tag-5"
		},{
			textclass: 'using-formula',
			textdata: data.string.p3text26,
		},{
			textclass: 'ans-formula',
			textdata: data.string.p3text27,
			splitintofractionsflag: true,
		}],
		imageblock:[{
			imagestoshow : [{
				imgclass : "pond-2",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "garden-2",
				imgsrc : '',
				imgid : 'garden-1'
			},{
				imgclass : "arrow-3",
				imgsrc : '',
				imgid : 'v-arr'
			}]
		}],
	},

	// slide15
	// {
	// 	contentnocenteradjust: true,
	// 	contentblockadditionalclass: 'bg-diy2',
	// 	extratextblock:[{
	// 		textclass: 'diy-text font-black',
	// 		textdata: data.string.diytext,
	// 	}],
	//
	// 	imageblock:[{
	// 		imagestoshow : [{
	// 			imgclass : "monkey-2",
	// 			imgsrc : '',
	// 			imgid : 'monkey'
	// 		}]
	// 	}],
	// },

	// slide16
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',

		lowertextblockadditionalclass: 'road-2',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p3text31,
		}],
		extratextblock:[{
			textclass: 'ques-text qt-2',
			textdata: data.string.p3text30,
		},{
			textdata: data.string.hint,
			textclass: "hint-btn hint-btn-2"
		},{
			textclass: 'road-1',
			textdata: '',
		},{
			textclass: 'tag tag-2m',
			textdata: data.string.m2,
		},{
			textclass: 'hint-2 ans-formula',
			textdata: data.string.p3text32,
			splitintofractionsflag: true,
		}],

		inputblock:[{
			inputdiv: 'input-2',
			textclass1: '',
			textdata1: data.string.length,
			buttonclass: 'btn-2',
			inputdata: '',
			unitdata: data.string.m,
			buttondata: data.string.check,
		}],
		imageblock:[{
			imagestoshow : [{
				imgclass : "correct-last",
				imgsrc : '',
				imgid : 'incorrect'
			},{
				imgclass : "pond-2",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "garden-2",
				imgsrc : '',
				imgid : 'garden-1'
			},{
				imgclass : "arrow-4",
				imgsrc : '',
				imgid : 'v-arr2'
			}]
		}],
	},

	// slide17
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',

		extratextblock:[{
			textclass: 'text-1 text-last',
			textdata: data.string.p3text33,
		},{
			textclass: 'road-1',
			textdata: '',
		},{
			textclass: 'road-2',
			textdata: '',
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "pond-2",
				imgsrc : '',
				imgid : 'pond'
			},{
				imgclass : "garden-2",
				imgsrc : '',
				imgid : 'garden-1'
			}]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var index = 0;
	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;
	var my_interval;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "monkey", src: imgpath+"p3/sundari.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pond", src: imgpath+"p3/pond.png", type: createjs.AbstractLoader.IMAGE},

			{id: "garden", src: imgpath+"p3/garden.png", type: createjs.AbstractLoader.IMAGE},
			{id: "garden-1", src: imgpath+"p3/flower.png", type: createjs.AbstractLoader.IMAGE},

			{id: "v-arr", src: imgpath+"p3/varr.png", type: createjs.AbstractLoader.IMAGE},
			{id: "v-arr2", src: imgpath+"p3/varr2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "h-arr", src: imgpath+"p3/harr.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tb-1", src: imgpath+"p3/textbox.png", type: createjs.AbstractLoader.IMAGE},

			{id: "monkey-1", src: "images/sundar/normal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey-correct", src: "images/sundar/correct-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey-incorrect", src: "images/sundar/incorrect-2.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			// sounds

			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p14", src: soundAsset+"s3_p14.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p7.ogg"},

			{id: "sound_8", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s3_p12.ogg"},
			{id: "sound_14", src: soundAsset+"s3_p15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		splitintofractions($board);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
				$('.newbot').css({
													'left':'50%',
													'top':'70%'
				});
					sound_nav("s3_p1");

				break;
			case 7:
			case 12:
			nav_button_controls(1000);
			break;
			case 9:
				$('.arrow-2').css({
														'height': '74%',
														'right': '43%',
														'bottom': '11%'
				});
				$('.arrow-1').css({
														'width': '36%',
														'right': '5%',
														'bottom': '8%'
				});
				$('.tag-3').css({
														'right': '23%',
														'bottom': '6%'
				});
				$('.tag-4').css({
														'right': '42%',
														'bottom': '46%'
				});

				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 27, monkey_display);
				});
				function monkey_display(isCorrect){
					$('.hint-text').show(0);
					if(isCorrect){
						$('.monkey-fb').show(0);
						$('.monkey-fb').attr('src', preload.getResult('monkey-correct').src);
					} else{
						$('.monkey-fb').show(0);
						$('.monkey-fb').attr('src', preload.getResult('monkey-incorrect').src);
					}
				}
				$('.hint-btn').click(function(){
					$('.hint-text').show(0);
				});
				break;
			case 11:

				$('.modify-btn').click(function(){
					$(this).fadeOut(500, function(){
						$('.formula').addClass('formula-anim');
						$('.for-a').addClass('for-a-anim');
						$('.for-b').addClass('for-b-anim');
						$('.for-x').addClass('for-x-anim');
						$('.for-l').addClass('for-l-anim');
						$('.last-feedback').delay(4000).fadeIn(500, function(){
							sound_nav("sound_"+(countNext));
						});
					});
				});
				break;
			case 13:
				sound_player("s3_p14");
				$('.qt-2').css({"top":"-3%",
												"left":"11%"});

				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 9, hint_display);
				});
				function hint_display(isCorrect){
					$('.hint-2').show(0);
					if(isCorrect){
						$('.correct-last').show(0);
						$('.correct-last').attr('src', preload.getResult('correct').src);
					} else{
						$('.correct-last').show(0);
						$('.correct-last').attr('src', preload.getResult('incorrect').src);
					}
				}
				$('.hint-btn').click(function(){
					$('.hint-2').show(0);
				});
				break;
			default:
				$('.speech-monkey').css({
					'background-image': 'url('+ preload.getResult('tb-1').src +')',
					'background-size': '100% 100%'
				});
				sound_nav("sound_"+(countNext));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete",function(){
			nav_button_controls(0);
		});
	}
	function checker(inputclass, ans1, func){
		createjs.Sound.stop();
		var ip_1 = $(inputclass).val();
		if(ip_1 === ''){
			return false;
		} else{
			ip_1 = parseInt($(inputclass).val());
			if(ip_1==ans1){
				$('.input-right-wrong').attr('src',preload.getResult('correct').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(1);
				$('.check-btn').css({
					'background-color': '#98C02E',
					'border': '0.5vmin solid #DEEF3C',
					'color': 'white',
					'pointer-events': 'none'
				});
				$('.bt-feedback, .text-hint').fadeIn(500);
				$('.check-btn').unbind('click');
				$('.inputformclass').trigger('blur');
				nav_button_controls(0);
				func(true);
			} else{
				$('.input-right-wrong').attr('src',preload.getResult('incorrect').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(0);
				$('.check-btn').css({
					'background-color': '#FF0000',
					'border': '0.5vmin solid #980000',
					'color': 'white'
				});
				$('.text-hint').fadeIn(500);
				func(false);
			}
		}
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearInterval(my_interval);
		clearTimeout(timeoutvar2);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		clearInterval(my_interval);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/" +$lang +"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-b1',
		uppertextblockadditionalclass: 'speech-monkey sp-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p4text1,
		}],

		imageblock:[{
			imagestoshow : [{
				imgclass : "monkey",
				imgsrc : '',
				imgid : 'monkey'
			}]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-b1',

		extratextblock:[{
			textdata: data.string.p4text2a,
			textclass: "label-tag label-1a",
		},{
			textdata: data.string.p4text2b,
			textclass: "label-tag label-1b",
		},{
			textdata: data.string.hint,
			textclass: "hint-btn"
		}],

		uppertextblockadditionalclass: 'hint-text',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p4text2c,
		},{
			textclass: '',
			textdata: data.string.p4text2d,
		},{
			textclass: '',
			textdata: data.string.p4text2e,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		lowertextblockadditionalclass: 'speech-monkey sp-2',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p4text2,
		}],

		inputblock:[{
			inputdiv: 'input-1',
			textclass1: '',
			textdata1: data.string.area,
			buttonclass: 'btn-2',
			inputdata: '',
			unitdata: data.string.msq,
			buttondata: data.string.check,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arr-1v",
					imgsrc : '',
					imgid : 'v-arr'
				},
				{
					imgclass : "arr-1h",
					imgsrc : '',
					imgid : 'h-arr'
				},
				{
					imgclass : "monkey-2",
					imgsrc : '',
					imgid : 'monkey'
				}
			]
		}],
		garden:[{
			gardenclass: 'garden-full',
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p4text5a,
			textclass: "label-tag label-ca",
		},{
			textdata: data.string.p4text5b,
			textclass: "label-tag label-cb",
		},{
			textdata: data.string.hint,
			textclass: "hint-btn hint-btn-2"
		}],

		uppertextblockadditionalclass: 'hint-text hint-text-2',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p4text5c,
		},{
			textclass: '',
			textdata: data.string.p4text5d,
		},{
			textclass: '',
			textdata: data.string.p4text5e,
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue',
		}],
		lowertextblockadditionalclass: 'instruction',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p4text4,
		}],

		inputblock:[{
			inputdiv: 'input-2',
			textclass1: '',
			textdata1: data.string.area,
			buttonclass: 'btn-2',
			inputdata: '',
			unitdata: data.string.msq,
			buttondata: data.string.check,
		}],
		imageblock:[{
			imagestoshow : [
				// {
					// imgclass : "arr-cv",
					// imgsrc : '',
					// imgid : 'v-arr'
				// },
				// {
					// imgclass : "arr-ch",
					// imgsrc : '',
					// imgid : 'h-arr'
				// },
				{
					imgclass : "drag-item road-1 itm-2",
					imgsrc : '',
					imgid : 'road-1'
				},
				{
					imgclass : "drag-item road-2 itm-3",
					imgsrc : '',
					imgid : 'road-2'
				},
				{
					imgclass : "drag-item pond itm-1",
					imgsrc : '',
					imgid : 'pond'
				},
				{
					imgclass : "drag-item nursery itm-0",
					imgsrc : '',
					imgid : 'nursery'
				},
				{
					imgclass : "drag-item fountain itm-4",
					imgsrc : '',
					imgid : 'fountain'
				},
				{
					imgclass : "current-image",
					imgsrc : '',
					imgid : 'pond'
				}
			]
		}],
		garden:[{
			gardenclass: 'garden-2',
		}]
	},
	// slidelast
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.submit,
			textclass: "last-submit-btn",
		}],
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.p4ins,
			textclass: "",
		}],

		lowertextblockadditionalclass: 'speech-monkey sp-last',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p4text10,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey-last",
					imgsrc : '',
					imgid : 'monkey'
				},
				{
					imgclass : "drag-item road-1",
					imgsrc : '',
					imgid : 'road-1'
				},
				{
					imgclass : "drag-item road-2",
					imgsrc : '',
					imgid : 'road-2'
				},
				{
					imgclass : "drag-item pond",
					imgsrc : '',
					imgid : 'pond'
				},
				{
					imgclass : "drag-item nursery",
					imgsrc : '',
					imgid : 'nursery'
				},
				{
					imgclass : "drag-item fountain",
					imgsrc : '',
					imgid : 'fountain'
				}
			]
		}],
		garden:[{
			gardenclass: 'garden-2',
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var index = 0;
	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "monkey", src: imgpath+"p3/sundari.png", type: createjs.AbstractLoader.IMAGE},
			{id: "v-arr", src: imgpath+"p3/varr.png", type: createjs.AbstractLoader.IMAGE},
			{id: "h-arr", src: imgpath+"p3/harr.png", type: createjs.AbstractLoader.IMAGE},

			{id: "pond", src: imgpath+"p4/pond.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pond-2", src: imgpath+"p4/pond.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fence-icon", src: imgpath+"p4/fence-icon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fence", src: imgpath+"p4/fence.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nursery", src: imgpath+"p4/nursery.png", type: createjs.AbstractLoader.IMAGE},
			{id: "road-1", src: imgpath+"p4/road-block-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "road-2", src: imgpath+"p4/road-block-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fountain", src: imgpath+"p4/fountain.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tb-1", src: imgpath+"p3/textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: imgpath+"p3/textbox-flipped.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s4_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s4_p3.ogg"},
			// {id: "sound_3_1", src: soundAsset+"s4_p3_1.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p4.ogg"},
			{id: "congrat", src: soundAsset+"congrat.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
				sound_nav("sound_1");
				putSpeechBoxImage(false);
				break;
			case 1:
				sound_player("sound_2");
				putSpeechBoxImage(false);
				createGardenGrid();
				$('.garden-grid').hide(0);
				input_box('.ans-input-box', 3, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 120, false, hint_display_p1);
				});
				function hint_display_p1(isCorrect){
					$('.hint-text').show(0);
					if(isCorrect){
						setTimeout(function(){
							$('.input-1').fadeOut(500);
							$('.hint-btn').fadeOut(500);
							$('.hint-text').fadeOut(500);
							$('.label-tag').fadeOut(500);
							$('.arr-1h').fadeOut(500);
							$('.arr-1v').fadeOut(500);
							$('.speech-monkey').fadeOut(500, function(){
								$('.speech-monkey>p').html(data.string.p4text3);
								$('.garden-grid').fadeIn(500);
								$('.garden').addClass('garden-move');
								$('.speech-monkey').fadeIn(500, function(){
									nav_button_controls(0);
								});
							});
						}, 1000);
					}
				}
				$('.hint-btn').click(function(){
					$('.hint-text').show(0);
				});
				break;
			case 2:
				sound_player("sound_3");
				$('.garden-grid').hide(0);
				input_box('.ans-input-box', 3, '.check-btn');

				var answer, ansCount = 0, currentItem, widthUnit = 65/12, heightUnit = 85/10;
				var labelH =[data.string.p4text5a, data.string.p4text6a, data.string.p4text7a, data.string.p4text8a, data.string.p4text9a];
				var labelV =[data.string.p4text5b, data.string.p4text6b, data.string.p4text7b, data.string.p4text8b, data.string.p4text9b];
				var hint1Arr =[data.string.p4text5c, data.string.p4text6c, data.string.p4text7c, data.string.p4text8c, data.string.p4text9c];
				var hint2Arr =[data.string.p4text5d, data.string.p4text6d, data.string.p4text7d, data.string.p4text8d, data.string.p4text9d];
				var hint3Arr =[data.string.p4text5e, data.string.p4text6e, data.string.p4text7e, data.string.p4text8e, data.string.p4text9e];
				var itemWidth = [4,6,1,2,2];
				var itemHeight = [5,5,2,1,3];
				var imgArr = ['nursery', 'pond', 'road-1', 'road-2', 'fountain'];

				$('.drag-item').click(function(){

					$('.label-ca, .label-cb').hide(0);
					$('.current-image').hide(0);
					$('.input-2').hide(0);
					$('.hint-btn').hide(0);
					$('.hint-text').hide(0);
					resetInputs('.ans-input-box');

					currentItem = $(this);
					var itemClass = $(this).attr('class');
					var itemNum = parseInt(itemClass.split(/itm-/)[1]);
					var itemW = itemWidth[itemNum]*widthUnit;
					var itemH = itemHeight[itemNum]*heightUnit;
					answer = itemWidth[itemNum]*itemHeight[itemNum];
					$('.hint-text-2>p').eq(0).html(hint1Arr[itemNum]);
					$('.hint-text-2>p').eq(1).html(hint2Arr[itemNum]);
					$('.hint-text-2>p').eq(2).html(hint3Arr[itemNum]);
					$('.label-ca').html(labelH[itemNum]);
					$('.label-cb').html(labelV[itemNum]);
					$('.current-image').attr('src', preload.getResult(imgArr[itemNum]).src);
					$('.current-image').css({
						'width': itemW+'%',
						'height': itemH+'%',
					});
					$('.label-ca').css({
						'bottom': (50 + itemH/2)+'%',
					});
					$('.label-cb').css({
						'left': (68 + itemW/2)+'%',
					});
					// $('.arr-cv').css({
						// 'left': (70 + itemW/2)+'%',
						// 'height': itemH+'%',
					// });
					// $('.arr-ch').css({
						// 'bottom': (47 + itemH/2)+'%',
						// 'width': itemW+'%',
					// });
					// $('.arr-cv, .arr-ch').show(0);
					$('.label-ca, .label-cb').show(0);
					$('.current-image').show(0);
					$('.input-2').css('display', 'flex');
					$('.hint-btn').show(0);
				});
				//dynamically binding click on check-btn
				$('.board').on('click', '.check-btn', function(){
					checker('.ans-input-box', answer, false, hint_display);
				});
				function hint_display(isCorrect){
					$('.hint-text').show(0);
					if(isCorrect){
						timeoutvar = setTimeout(function(){
							ansCount++;
							currentItem.addClass('done-click');
							if(ansCount>=5){
								nav_button_controls(100);
							}
						}, 1000);
					}
				}
				$('.hint-btn').click(function(){
					$('.hint-text').show(0);
				});
				break;
			case 3:
				sound_player("sound_4");
				putSpeechBoxImage(false);
				var occupyArr = [];
				var dX = 100/12, dY = 10;
				var initX = 35, initY = 15;
				var numX = 0, numY = 0;
				var occupyX = 0, occupyY = 0;
				var dropWidth = 0, dropHeight = 0;
				var offX = 0, offY = 0;
				var prevX = 0, prevY = 0;
				var isClone = false;

				createGardenGrid();
				function calcPosition(draggedItem){
					var offset = draggedItem.offset();
					var offset2 = $('.garden').offset();
					offX = offset.left - offset2.left;
					offY = offset.top - offset2.top;
				}
				function calcPosition2(draggedItem){
					var offset = draggedItem.position();
					offX = offset.left;
					offY = offset.top;
				}
				function calcPrevPosition(draggedItem){
					var offset = draggedItem.position();
					prevX = offset.left;
					prevY = offset.top;
					prevX = (prevX/$('.garden').width())*100;
					prevY = (prevY/$('.garden').height())*100;
					prevX = Math.round(prevX/dX);
					prevY = Math.round(prevY/dY);
				}
				function calcElementSize(droppedElement){
					if(droppedElement.hasClass('pond')==true){
							numX = 6;
							numY = 5;
						} else if(droppedElement.hasClass('road-1')==true){
							numX = 1;
							numY = 2;
						} else if(droppedElement.hasClass('road-2')==true){
							numX = 2;
							numY = 1;
						} else if(droppedElement.hasClass('nursery')==true){
							numX = 4;
							numY = 5;
						} else if(droppedElement.hasClass('fountain')==true){
							numX = 2;
							numY = 3;
						}
				}
				// check if area is open to drop
				function updateOccupyArr(isClone, droppedElement){
					calcElementSize(droppedElement);
					if(isClone){
						for(var j=prevY; j<prevY+numY; j++){
							for(var i=prevX; i<prevX+numX; i++){
								var index = j*12+i;
								occupyArr[index] = 0;
								// enable this to check
								// $(".gg-"+j+'-'+i).css('background', 'green');
							}
						}
						for(var j=occupyY; j<occupyY+numY; j++){
							for(var i=occupyX; i<occupyX+numX; i++){
								var index = j*12+i;
								if(occupyArr[index]==1){
									for(var k=prevY; k<prevY+numY; k++){
										for(var l=prevX; l<prevX+numX; l++){
											var index = k*12+l;
											occupyArr[index] = 1;
											// enable this to check
											// $(".gg-"+k+'-'+l).css('background', 'red');
										}
									}
									return false;
								}
							}
						}
						for(var j=occupyY; j<occupyY+numY; j++){
							for(var i=occupyX; i<occupyX+numX; i++){
								var index = j*12+i;
								occupyArr[index] = 1;
								// $(".gg-"+j+'-'+i).css('background', 'red');
							}
						}
					} else{
						for(var j=occupyY; j<occupyY+numY; j++){
							for(var i=occupyX; i<occupyX+numX; i++){
								var index = j*12+i;
								if(occupyArr[index]==1){
									return false;
								}
							}
						}
						for(var j=occupyY; j<occupyY+numY; j++){
							for(var i=occupyX; i<occupyX+numX; i++){
								var index = j*12+i;
								occupyArr[index] = 1;
								// $(".gg-"+j+'-'+i).css('background', 'red');
							}
						}
					}
					return true;
				}

				$( '.drag-item' ).draggable({
					containment: ".garden",
					cursor: "move",
					helper: 'clone',
					revert: true,
					appendTo: "body",
					start: function( event, ui ){
						isClone = false;
						ui.helper.addClass('dragging');
						$('.garden').addClass('garden-up');
					},
					stop: function( event, ui ){
						ui.helper.removeClass('dragging');
						$('.garden').removeClass('garden-up');
					},
					drag: function(event, ui){
						calcPosition(ui.helper);
					}
				});
				$('.garden').droppable({
					drop:function(event, ui) {
						//offset for first drag
						offX = (offX/$('.garden').width())*100;
						offY = (offY/$('.garden').height())*100;
						occupyX = Math.round(offX/dX);
						occupyY = Math.round(offY/dY);
						if(occupyX<0) occupyX =0;
						if(occupyY<0) occupyY =0;
						offX = occupyX*dX;
						offY = occupyY*dY;

						if(ui.draggable.hasClass('clonedElement')){
							var currentElement = ui.draggable;
							calcElementSize(currentElement);
							dropWidth = numX*dX;
							dropHeight = numY*dY;
							if(updateOccupyArr(true, currentElement)){
								currentElement.appendTo($(this));
								currentElement.removeClass('clone-drag');
								currentElement.css({
									'position': 'absolute',
									'height': dropHeight+'%',
									'width': dropWidth+'%',
									'top': offY + '%',
									'left': offX + '%'
								});
								currentElement.draggable('option', 'revert', false);
								currentElement.draggable('destroy');
								setTimeout(function(){
									currentElement.draggable({
										containment: ".garden",
										cursor: "move",
										revert: true,
										appendTo: "body",
										start: function( event, ui ){
											isClone = true;
											calcPrevPosition($(this));
											$(this).addClass('clone-drag');
											$('.garden').addClass('garden-up');
										},
										stop: function( event, ui ){
											$(this).removeClass('clone-drag');
											$('.garden').removeClass('garden-up');
										},
										drag: function(event, ui){
											calcPosition2($(this));
										}
									});
								}, 17);

							}
						} else{
							var droppedElement = ui.helper.clone();
							droppedElement.removeClass('dragging');
							droppedElement.addClass('clonedElement');
							$('.garden').removeClass('garden-up');
							if(updateOccupyArr(false, droppedElement)){
								droppedElement.draggable({
									containment: ".garden",
									cursor: "move",
									revert: true,
									appendTo: "body",
									start: function( event, ui ){
										isClone = true;
										calcPrevPosition($(this));
										$(this).addClass('clone-drag');
										$('.garden').addClass('garden-up');
									},
									stop: function( event, ui ){
										$(this).removeClass('clone-drag');
										$('.garden').removeClass('garden-up');
									},
									drag: function(event, ui){
										calcPosition2($(this));
									}
								});
								calcElementSize(droppedElement);
								dropWidth = numX*dX;
								dropHeight = numY*dY;
								droppedElement.css({
									'position': 'absolute',
									'height': dropHeight+'%',
									'width': dropWidth+'%',
									'top': offY + '%',
									'left': offX + '%'
								});
								droppedElement.appendTo($(this));
								ui.draggable.draggable('option', 'revert', false);
							}
						}
					}
				});
				$('.last-submit-btn').click(function(){
					$(this).hide(0);
					$('.bg-1').addClass('anim-bg-blue');
					$('.garden').addClass('garden-last-anim');
					$('.instruction').fadeOut(500);
					$('.drag-item').css('pointer-events', 'none');
					$('.drag-item:not(.clonedElement)').fadeOut(1500, function(){
						$('.sp-last').fadeIn(500);
						$('.monkey-last').fadeIn(500);
						sound_nav("congrat");
						// nav_button_controls(0);
					});
				});

				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete",function(){
			nav_button_controls(0);
		});
	}

	function putSpeechBoxImage(flipped){
		if(flipped){
			$('.speech-monkey').css({
				'background-image': 'url('+ preload.getResult('tb-2').src +')',
				'background-size': '100% 100%'
			});
		} else{
			$('.speech-monkey').css({
				'background-image': 'url('+ preload.getResult('tb-1').src +')',
				'background-size': '100% 100%'
			});
		}
	}
	function createGardenGrid(){
		for(var j=0; j<10;j++){
			for(var i=0; i<12;i++){
				$('.garden').append('<div class="garden-grid gg-"'+j+'-'+'i'+'"></div>');
			}
		}
	}
	function resetInputs(inputclass){
		$(inputclass).val('');
		$('.check-btn').css({
			'background-color': '',
			'border': '',
			'color': '',
			'pointer-events': ''
		});
		$('.check-btn').bind('click');
	}
	function checker(inputclass, ans1, goNext, func){
		createjs.Sound.stop();
		var ip_1 = $(inputclass).val();
		if(ip_1 === ''){
			return false;
		} else{
			ip_1 = parseInt($(inputclass).val());
			if(ip_1==ans1){
				$('.input-right-wrong').attr('src',preload.getResult('correct').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(1);
				$('.check-btn').css({
					'background-color': '#98C02E',
					'border': '0.5vmin solid #DEEF3C',
					'color': 'white',
					'pointer-events': 'none'
				});
				$('.bt-feedback, .text-hint').fadeIn(500);
				$('.check-btn').unbind('click');
				$('.inputformclass').trigger('blur');
				if(goNext) nav_button_controls(0);
				func(true);
			} else{
				$('.input-right-wrong').attr('src',preload.getResult('incorrect').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(0);
				$('.check-btn').css({
					'background-color': '#FF0000',
					'border': '0.5vmin solid #980000',
					'color': 'white'
				});
				$('.text-hint').fadeIn(500);
				func(false);
			}
		}
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

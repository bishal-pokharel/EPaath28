var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang + "/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'cover'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text1,
			textclass: "text-1",
		}],
		polygon:[{
			polygonclass: 'rect-1',
			side:[{
				sidediv: 'rect-side rect-top side-anim-v',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-bottom side-anim-v',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-left side-anim-h',
				sidelabel: data.string.m3
			},{
				sidediv: 'rect-side rect-right side-anim-h',
				sidelabel: data.string.m3
			}]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "text-2",
		}],
		polygon:[{
			polygonclass: 'rect-1',
			side:[{
				sidediv: 'rect-side rect-top',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-bottom',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-left',
				sidelabel: data.string.m3
			},{
				sidediv: 'rect-side rect-right',
				sidelabel: data.string.m3
			}]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "text-2",
		}],

		uppertextblockadditionalclass: 'text-3',
		uppertextblock:[{
			textdata: data.string.p1text3,
			textclass: "",
		},,{
			textdata: data.string.p1text4,
			textclass: "non-visible",
		}],
		polygon:[{
			polygonclass: 'rect-1',
			side:[{
				sidediv: 'rect-side rect-top',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-bottom',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-left',
				sidelabel: data.string.m3
			},{
				sidediv: 'rect-side rect-right',
				sidelabel: data.string.m3
			}]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "text-2",
		}],

		uppertextblockadditionalclass: 'text-3',
		uppertextblock:[{
			textdata: data.string.p1text3,
			textclass: "",
		},{
			textdata: data.string.p1text4,
			textclass: "",
			datahighlightflag : true,
			datahighlightcustomclass : 'non-visible',
		}],
		polygon:[{
			polygonclass: 'rect-1',
			side:[{
				sidediv: 'rect-side rect-top',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-bottom',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-left',
				sidelabel: data.string.m3
			},{
				sidediv: 'rect-side rect-right',
				sidelabel: data.string.m3
			}]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "text-2",
		},{
			textdata: data.string.p1text5,
			textclass: "bt-text",
		}],

		uppertextblockadditionalclass: 'text-3',
		uppertextblock:[{
			textdata: data.string.p1text3,
			textclass: "",
		},{
			textdata: data.string.p1text4,
			textclass: "",
			datahighlightflag : true,
			datahighlightcustomclass : 'non-visible',
		}],
		polygon:[{
			polygonclass: 'rect-1',
			side:[{
				sidediv: 'rect-side rect-top',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-bottom',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-left',
				sidelabel: data.string.m3
			},{
				sidediv: 'rect-side rect-right',
				sidelabel: data.string.m3
			}],
			imgclass : "pacman",
			imgid : 'pacman'
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text6,
			textclass: "text-1",
		}],
		polygon:[{
			polygonclass: 'rect-1',
			side:[{
				sidediv: 'rect-side rect-top side-thick-v',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-bottom side-thick-v',
				sidelabel: data.string.m5
			},{
				sidediv: 'rect-side rect-left side-thick-h',
				sidelabel: data.string.m3
			},{
				sidediv: 'rect-side rect-right side-thick-h',
				sidelabel: data.string.m3
			}]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text6,
			textclass: "text-1",
		}],
		polygon:[{
			polygonclass: 'tri-1',
			side:[{
				sidediv: 'tri-side t-side-1 t-side-anim',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side t-side-2 t-side-anim',
				sidelabel: data.string.m3
			},{
				sidediv: 'tri-side t-side-3 t-side-anim',
				sidelabel: data.string.m4
			}]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text6a,
			textclass: "text-2",
		}],
		polygon:[{
			polygonclass: 'tri-1',
			side:[{
				sidediv: 'tri-side t-side-1',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side t-side-2',
				sidelabel: data.string.m3
			},{
				sidediv: 'tri-side t-side-3',
				sidelabel: data.string.m4
			}]
		}]
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text6a,
			textclass: "text-2",
		}],

		uppertextblockadditionalclass: 'text-3',
		uppertextblock:[{
			textdata: data.string.p1text7,
			textclass: "",
		},{
			textdata: data.string.p1text8,
			textclass: "non-visible",
		}],
		polygon:[{
			polygonclass: 'tri-1',
			side:[{
				sidediv: 'tri-side t-side-1',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side t-side-2',
				sidelabel: data.string.m3
			},{
				sidediv: 'tri-side t-side-3',
				sidelabel: data.string.m4
			}]
		}]
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text6a,
			textclass: "text-2",
		}],

		uppertextblockadditionalclass: 'text-3',
		uppertextblock:[{
			textdata: data.string.p1text7,
			textclass: "",
		},{
			textdata: data.string.p1text8,
			textclass: "",
			datahighlightflag : true,
			datahighlightcustomclass : 'non-visible',
		}],
		polygon:[{
			polygonclass: 'tri-1',
			side:[{
				sidediv: 'tri-side t-side-1',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side t-side-2',
				sidelabel: data.string.m3
			},{
				sidediv: 'tri-side t-side-3',
				sidelabel: data.string.m4
			}]
		}]
	},
	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text6a,
			textclass: "text-2",
		},{
			textdata: data.string.p1text9,
			textclass: "bt-text",
		}],

		uppertextblockadditionalclass: 'text-3',
		uppertextblock:[{
			textdata: data.string.p1text7,
			textclass: "",
		},{
			textdata: data.string.p1text8,
			textclass: "",
			datahighlightflag : true,
			datahighlightcustomclass : 'non-visible',
		}],
		polygon:[{
			polygonclass: 'tri-1',
			side:[{
				sidediv: 'tri-side t-side-1',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side t-side-2',
				sidelabel: data.string.m3
			},{
				sidediv: 'tri-side t-side-3',
				sidelabel: data.string.m4
			}],
			imgclass : "pacman",
			imgid : 'pacman'
		}]
	},
	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p1text10,
			textclass: "text-ques",
			datahighlightflag : true,
			datahighlightcustomclass : 'font-blue',
		},{
			textdata: data.string.p1text14,
			textclass: "bt-feedback"
		}],

		uppertextblockadditionalclass: 'text-hint',
		uppertextblock:[{
			textdata: data.string.p1text12,
			textclass: "",
		},{
			textdata: data.string.p1text13,
			textclass: "",
			datahighlightflag : true,
			datahighlightcustomclass : 'non-visible',
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'diy-bg'
				}
			]
		}],
		inputblock:[{
			inputdiv: 'input-1',
			textclass1: '',
			textdata1: data.string.p1text11,
			buttonclass: '',
			inputdata: '',
			unitdata: data.string.m,
			buttondata: data.string.check,
		}],
		polygon:[{
			polygonclass: 'tri-2',
			side:[{
				sidediv: 'tri-side2 t2-side-1',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side2 t2-side-2',
				sidelabel: data.string.m5
			},{
				sidediv: 'tri-side2 t2-side-3',
				sidelabel: data.string.m5
			}],
		}]
	},
	// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'para-1',
		uppertextblock:[{
			textdata: data.string.p1text15,
			datahighlightflag : true,
			datahighlightcustomclass : 'text-1-hl',
		}],
		polygon:[{
			polygonclass: 'tri-3',
			side:[{
				sidediv: 'tri-side2 t2-side-1',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-2',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-3',
				sidelabel: data.string.l
			}],
		}],
	},
	// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'para-1',
		uppertextblock:[{
			textdata: data.string.p1text15,
			datahighlightflag : true,
			datahighlightcustomclass : 'text-1-hl',
		},{
			textdata: data.string.p1text16,
		},{
			textclass: 'for-box',
			textdata: data.string.p1text17,
		}],
		polygon:[{
			polygonclass: 'tri-3',
			side:[{
				sidediv: 'tri-side2 t2-side-1',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-2',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-3',
				sidelabel: data.string.l
			}],
		}],
	},
	// slide15
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'para-1',
		uppertextblock:[{
			textdata: data.string.p1text15,
			datahighlightflag : true,
			datahighlightcustomclass : 'text-1-hl',
		},{
			textdata: data.string.p1text16,
		},{
			textclass: 'for-box',
			textdata: data.string.p1text17,
		}],
		extratextblock:[{
			textclass: 'last-full',
			textdata: data.string.p1text18,
		},{
			textclass: 'modify-btn',
			textdata: data.string.modify,
		}],
		lowertextblockadditionalclass: 'formula formula-anim',
		lowertextblock:[{
			textclass: 'for-3  for-3-anim',
			textdata: data.string.num3,
		},{
			textclass: 'for-p for-p-anim',
			textdata: data.string.p,
		},{
			textclass: 'for-eq',
			textdata: data.string.eq,
		},{
			textclass: 'for-l for-l-anim',
			textdata: data.string.l,
		}],
		polygon:[{
			polygonclass: 'tri-3',
			side:[{
				sidediv: 'tri-side2 t2-side-1',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-2',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-3',
				sidelabel: data.string.l
			}],
		}],
	},
	// slide16
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'para-1 font-small-para',
		uppertextblock:[{
			textdata: data.string.p1text19,
			datahighlightflag : true,
			datahighlightcustomclass : 'text-1-hl',
		},{
			textdata: data.string.p1text19a,
		}],
		extratextblock:[{
			textclass: 'last-full its_hidden',
			textdata: data.string.p1text21,
		}],
		inputblock:[{
			inputdiv: 'input-2',
			textclass1: 'leqtext',
			textdata1: data.string.p1text22,
			buttonclass: '',
			inputdata: '',
			unitdata: data.string.cm,
			buttondata: data.string.check,
		}],
		lowertextblockadditionalclass: 'hint-formula',
		lowertextblock:[{
			textclass: '',
			textdata: data.string.p1text20,
			splitintofractionsflag: true,
		}],
		polygon:[{
			polygonclass: 'tri-3',
			side:[{
				sidediv: 'tri-side2 t2-side-1',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-2',
				sidelabel: data.string.l
			},{
				sidediv: 'tri-side2 t2-side-3',
				sidelabel: data.string.l
			}],
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var index = 0;
	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"cover_perimeter.png", type: createjs.AbstractLoader.IMAGE},

			{id: "ghost-1", src: imgpath+"ghost/1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ghost-2", src: imgpath+"ghost/2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ghost-3", src: imgpath+"ghost/3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ghost-4", src: imgpath+"ghost/4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "diy-bg", src: imgpath+"box-2.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p12.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		splitintofractions($board);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
			case 1:
			case 2:
			case 6:
			case 8:
			sound_player("sound_"+(countNext));
			break;
			case 5:
					sound_player("sound_"+(countNext));
				animate_ghost();
				$('.pacman').click(function(){
					animate_ghost();
				});
				function animate_ghost(){
					$('.pacman').addClass('zoomghost');
					$('.pacman').attr('src',preload.getResult('ghost-1').src);
					$('.pacman').animate({
						'left': '100%',
					}, 2000, function(){
						$('.pacman').attr('src',preload.getResult('ghost-2').src);
						$('.pacman').animate({
							'top': '0%',
						}, 1200, function(){
							$('.pacman').attr('src',preload.getResult('ghost-3').src);
							$('.pacman').animate({
								'left': '0%',
							}, 2000, function(){
								$('.pacman').attr('src',preload.getResult('ghost-4').src);
								$('.pacman').animate({
									'top': '100%',
								}, 1200, function(){
									$('.pacman').attr('src',preload.getResult('ghost-1').src);

									$('.pacman').removeClass('zoomghost');
								});
							});
						});
					});
				}
				break;
			case 11:
				sound_player("sound_"+(countNext));
				animate_ghost2();
				$('.pacman').click(function(){
					animate_ghost2();
				});
				function animate_ghost2(){
					$('.pacman').addClass('zoomghost');
					$('.pacman').attr('src',preload.getResult('ghost-1').src);
					$('.pacman').animate({
						'left': '100%',
					}, 2000, function(){
						$('.pacman').attr('src',preload.getResult('ghost-2').src);
						$('.pacman').animate({
							'top': '8%',
							'left': '63%',
						}, 1200, function(){
							$('.pacman').attr('src',preload.getResult('ghost-4').src);
							$('.pacman').animate({
								'top': '100%',
								'left': '0%',
							}, 2000, function(){
								$('.pacman').attr('src',preload.getResult('ghost-1').src);

								$('.pacman').removeClass('zoomghost');
							});
						});
					});
				}
				break;
			case 12:

				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker('.ans-input-box', 15);
				});
				break;
			case 13:
				$prevBtn.show(0);
				$('.tri-3 p').addClass('blink-anim');
				nav_button_controls(1000);
				break;
			case 15:
				$prevBtn.show(0);
				$('.modify-btn').click(function(){
					$(this).unbind('click');
					$(this).fadeOut(100, function(){
						$('.formula').show(0);
					});
				});
				nav_button_controls(1000);
				break;
			case 16:

				input_box('.ans-input-box', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker2('.ans-input-box', 40);
				});
				function checker2(inputclass, ans1){
					var ip_1 = $(inputclass).val();
					if(ip_1 === ''){
						return false;
					} else{
						ip_1 = parseInt($(inputclass).val());
						if(ip_1==ans1){
							$('.input-right-wrong').attr('src',preload.getResult('correct').src);
							$('.input-right-wrong').css('opacity', '1');
							play_correct_incorrect_sound(1);
							$('.check-btn').css({
								'background-color': '#98C02E',
								'border': '0.5vmin solid #DEEF3C',
								'color': 'white',
								'pointer-events': 'none'
							});
							$('.last-full, .text-hint').fadeIn(500);
							$('.check-btn').unbind('click');
							$('.inputformclass').trigger('blur');
							nav_button_controls(200);
						} else{
							$('.input-right-wrong').attr('src',preload.getResult('incorrect').src);
							$('.input-right-wrong').css('opacity', '1');
							play_correct_incorrect_sound(0);
							$('.check-btn').css({
								'background-color': '#FF0000',
								'border': '0.5vmin solid #980000',
								'color': 'white'
							});
							$('.formula-hint').fadeIn(500);
						}
					}
				}
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function checker(inputclass, ans1){
		var ip_1 = $(inputclass).val();
		if(ip_1 === ''){
			return false;
		} else{
			ip_1 = parseInt($(inputclass).val());
			if(ip_1==ans1){
				$('.input-right-wrong').attr('src',preload.getResult('correct').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(1);
				$('.check-btn').css({
					'background-color': '#98C02E',
					'border': '0.5vmin solid #DEEF3C',
					'color': 'white',
					'pointer-events': 'none'
				});
				// $(inputclass).css({
					// 'border-color': 'rgba(93, 150, 32, 0.9)',
					// 'color': 'rgba(93, 150, 32, 0.9)',
					// 'pointer-events': 'none'
				// });
				$('.bt-feedback, .text-hint').fadeIn(500);
				$('.check-btn').unbind('click');
				$('.inputformclass').trigger('blur');
				nav_button_controls(200);
			} else{
				$('.input-right-wrong').attr('src',preload.getResult('incorrect').src);
				$('.input-right-wrong').css('opacity', '1');
				play_correct_incorrect_sound(0);
				$('.check-btn').css({
					'background-color': '#FF0000',
					'border': '0.5vmin solid #980000',
					'color': 'white'
				});
				$('.text-hint').fadeIn(500);
				// $(inputclass).css({
					// 'border-color': 'rgba(186, 35, 35, 0.9)',
					// 'color': 'rgba(186, 35, 35, 0.9)',
				// });
			}
		}
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textclass : 'topbox',
				datahighlightflag:true,
        datahighlightcustomclass:"color",
				// textdata : data.string.qn1,
			}
		],
		optionsblockadditionalclass: 'img_options',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				// textdata:data.string.p4q1
			}],
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{},{}]
		}]
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textclass : 'topbox',
				datahighlightflag:true,
        datahighlightcustomclass:"color",
				textdata : data.string.qn1,
			}
		],
		optionsblockadditionalclass: 'img_options',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				// textdata:data.string.p4q1
			}],
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{},{}]
		}]
	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textclass : 'topbox',
				datahighlightflag:true,
        datahighlightcustomclass:"color",
				textdata : data.string.qn1,
			}
		],
		optionsblockadditionalclass: 'img_options',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				// textdata:data.string.p4q1
			}],
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{},{}]
		}]
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textclass : 'topbox',
				datahighlightflag:true,
        datahighlightcustomclass:"color",
				textdata : data.string.qn1,
			}
		],
		optionsblockadditionalclass: 'img_options',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				// textdata:data.string.p4q1
			}],
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{},{}]
		}]
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textclass : 'topbox',
				datahighlightflag:true,
        datahighlightcustomclass:"color",
				textdata : data.string.qn1,
			}
		],
		optionsblockadditionalclass: 'img_options',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				// textdata:data.string.p4q1
			}],
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{},{}]
		}]
	},

];


$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $("#activity-page-next-btn-enabled");
	var $prevBtn  = $("#activity-page-prev-btn-enabled");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;
	var qnNUmArray=[1,2,3,4,5];
	qnNUmArray.shufflearray();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl1", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
	var rhino = new NumberTemplate();

	rhino.init($total_page);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$("#box_icon_rhino").hide(0);
				//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	   }

		// $('.topbox').prepend(countNext+1+". ");
		var wrong_clicked = false;
		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(this).siblings('.corctopt').show(0);
				$(".buttonsel").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
				$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).siblings('.wrngopt').show(0);
				$(this).css({
					'background-color': '#FF0000',
					'border': '3px solid #980000',
					'pointer-events':'none'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
		switch (countNext){
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
				$(".topbox").html(eval("data.string.ex2q"+qnNUmArray[0]));
				$('.topbox').prepend(countNext+1+". ");
				$(".buttonsel:eq(0)").html(eval("data.string.ex2_q"+qnNUmArray[0]+"_op1"));
				$(".buttonsel:eq(1)").html(eval("data.string.ex2_q"+qnNUmArray[0]+"_op2"));
				$(".buttonsel:eq(2)").html(eval("data.string.ex2_q"+qnNUmArray[0]+"_op3"));
				$(".buttonsel:eq(3)").html(eval("data.string.ex2_q"+qnNUmArray[0]+"_op4"));
				randomize(".optionsdiv");
				qnNUmArray.splice(0,1);
				console.log(qnNUmArray);
			break;
		}
	}
	/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	// templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 5){
			templateCaller();
			rhino.gotoNext();
		}else{
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

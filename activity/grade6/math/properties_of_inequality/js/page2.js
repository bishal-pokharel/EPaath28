var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
// slide0
	{
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"bg_diy",
				imgsrc:"",
			}]
		}],
		extratextblock:[{
			textclass:'diytxt',
			textdata:data.string.diy
		}]
	},
// slide1
	{
		contentblockadditionalclass: 'bg-blue',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				textdata:data.string.p2s1txt
			},{
				ques_class:"midqn",
				// textdata:data.string.p2s1txt
			}],

			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{}],
			tipblock:[{
				tiptxt:[{
					textclass:"exmpTxt",
					textdata:data.string.exmp_txt
				}]
			}]
		}],
	},
// slide2
	{
		contentblockadditionalclass: 'bg-blue',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				textdata:data.string.p2s1txt
			},{
				ques_class:"midqn",
				// textdata:data.string.p2s1txt
			}],

			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{}],
			tipblock:[{
				tiptxt:[{
					textclass:"exmpTxt",
					textdata:data.string.exmp_txt
				}]
			}]
		}],
	},
// slide3
	{
		contentblockadditionalclass: 'bg-blue',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				textdata:data.string.p2s1txt
			},{
				ques_class:"midqn",
				// textdata:data.string.p2s1txt
			}],

			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{}],
			tipblock:[{
				tiptxt:[{
					textclass:"exmpTxt",
					textdata:data.string.exmp_txt
				}]
			}]
		}],
	},
// slide4
	{
		contentblockadditionalclass: 'bg-blue',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				textdata:data.string.p2s1txt
			},{
				ques_class:"midqn",
				// textdata:data.string.p2s1txt
			}],

			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{}],
			tipblock:[{
				tiptxt:[{
					textclass:"exmpTxt",
					textdata:data.string.exmp_txt
				}]
			}]
		}],
	},
// slide5
	{
		contentblockadditionalclass: 'bg-blue',
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
				textdata:data.string.p2s1txt
			},{
				ques_class:"midqn",
				// textdata:data.string.p2s1txt
			}],

			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{}],
			tipblock:[{
				tiptxt:[{
					textclass:"exmpTxt",
					textdata:data.string.exmp_txt
				}]
			}]
		}],
	},
// slide6
	{
		contentblockadditionalclass: 'bg-blue',
		sumaryblk:[{
			datahighlightflag:true,
			datahighlightcustomclass:"smry-1",
			textclass:"sumaryTxt-1",
			textdata:data.string.sumary
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"smry-2",
			textclass:"sumaryTxt-2",
			textdata:data.string.sumary
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"smry-3",
			textclass:"sumaryTxt-3",
			textdata:data.string.sumary
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"smry-4",
			textclass:"sumaryTxt-4",
			textdata:data.string.sumary
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"smry-5",
			textclass:"sumaryTxt-5",
			textdata:data.string.sumary
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	var qn1Ar,qn2Ar,qn3Ar,qn4Ar,qn5Ar=[];
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hasta", src: imgpath+"hasta dai-15.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankbg", src: imgpath+"board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankdai", src: imgpath+"bank-hasta-dai-03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankerTlk", src: imgpath+"banker-talking-06-06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hastaTlk", src: imgpath+"hasta-dai-talking05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box1", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box2", src: imgpath+"textbox01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "board", src: imgpath+"board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel re-design-08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "example", src: imgpath+"example.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl4", src: imgpath+"girl4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tip_icon", src: "images/tip_icon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "white_wrong", src: "images/white_wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_diy", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},


			// soundsa
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	//  var eqArray = ['<','=','>','='];
	 var eqArray = ['<','>','=','='];
	 var twComp = ["<=",">="]
	 var lstSgn = twComp.shufflearray();
	 eqArray.push(lstSgn[0]);
	 eqArray.shufflearray();
	//  var cmprArray = eqArray.shufflearray();

	var eqlSecFlg = false;

	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		vocabcontroller.findwords(countNext);
		$(".midqn").prepend((countNext+1)+". ");
		var opnSgn  = ['<','=','>'];
		var sgnArray = ["-","+"];
		var val;
		var qnCount = 1;
		$(".tipimg").append("<img class='qnexmp'src='"+preload.getResult("example").src+"'/>");
		$(".exccontainer").append("<img class='tipIcon'src='"+preload.getResult("tip_icon").src+"'/>");
		$(".tipblkclass").append("<img class='cross' src='"+preload.getResult("white_wrong").src+"'/>");


		function RandNumGenerator(trFlag){
			// function to generate random numbers with or without decimal
			var qnArray=[];
			if(trFlag)
				for(var i=0;i<=9;i++){
					randNum = Math.floor(Math.random()*(200- (-200)+1)  - 200);
					qnArray.push(randNum);
				}
			else {
				for(var i=0;i<=9;i++){
					randNum = Math.round(Math.random()*(20000- (-20000)+1)  - 20000)/100;
					qnArray.push(randNum);
				}
			}
			return(qnArray);
		}

		function qnAppender(qnArray, val){
			// function to html the values
			var sgn1=eqArray[0],sgn2=opnSgn[1],sgn3=opnSgn[2];
			sgn1==opnSgn[1]?sgn2=opnSgn[0]:
			sgn1==opnSgn[2]?sgn3=opnSgn[0]:"";

			if(val==0){
				$(".midqn").html(qnArray[0]+" "+eval("data.string.ndtxt")+" "+qnArray[1]);
				$(".buttonsel:eq(0)").html(qnArray[0] +" "+ eqArray[0] +" "+ qnArray[1]);
				$(".buttonsel:eq(1)").html(qnArray[0] +" "+ sgn2 +" "+ qnArray[1]);
				$(".buttonsel:eq(2)").html(qnArray[0] +" "+ sgn3 +" "+ qnArray[1]);
				randomize(".optionsdiv");
			}
			else if (val==1) {
				$(".midqn").html(qnArray[0]+" "+eval("data.string.ndtxt")+" "+qnArray[0]);
				$(".buttonsel:eq(0)").html(qnArray[0] +" "+ eqArray[0] +" "+ qnArray[0]);
				$(".buttonsel:eq(1)").html(qnArray[0] +" "+ sgn2 +" "+ qnArray[0]);
				$(".buttonsel:eq(2)").html(qnArray[0] +" "+ sgn3 +" "+ qnArray[0]);
				randomize(".optionsdiv");
			}
			else if (val==2) {
				$(".midqn").html(qnArray[0]+" "+eval("data.string.ndtxt")+" "+qnArray[0]);
				$(".buttonsel:eq(0)").html(qnArray[0] +" "+ eqArray[0] +" ("+(qnArray[0]- (qnArray[0]/10))+") + ("+(qnArray[0]/10).toFixed(2)+")");
				$(".buttonsel:eq(1)").html(qnArray[0] +" "+ sgn2 +" "+ qnArray[0]);
				$(".buttonsel:eq(2)").html(qnArray[0] +" "+ sgn3 +" "+ qnArray[0]);
				randomize(".optionsdiv");
			}
			else if (val==3) {
				$(".midqn").html(qnArray[0]+" "+eval("data.string.ndtxt")+" "+qnArray[1]);
				var fstAdnTrm = qnArray[1]-(qnArray[1]/10);
				var secAdnTrm = (qnArray[1]/10);
				$(".buttonsel:eq(0)").html(qnArray[0] +" < ("+ fstAdnTrm.toFixed(2)+") + ("+secAdnTrm.toFixed(2)+")");
				$(".buttonsel:eq(1)").html(qnArray[0] +" = ("+ fstAdnTrm.toFixed(2)+") + ("+secAdnTrm.toFixed(2)+")");
				$(".buttonsel:eq(2)").html(qnArray[0] +" > ("+ fstAdnTrm.toFixed(2)+") + ("+secAdnTrm.toFixed(2)+")");
				randomize(".optionsdiv");
			}else{
				$(".midqn").html(qnArray[0]+" "+eval("data.string.ndtxt")+" "+qnArray[1]);
				var fstAdnTrm = qnArray[1]-(qnArray[1]/10);
				var secAdnTrm = (qnArray[1]/10);
				$(".buttonsel:eq(0)").html(qnArray[0] +" > "+ fstAdnTrm.toFixed(2)+" + "+secAdnTrm.toFixed(2));
				$(".buttonsel:eq(1)").html(qnArray[0] +" = "+ fstAdnTrm.toFixed(2)+" + "+secAdnTrm.toFixed(2));
				$(".buttonsel:eq(2)").html(qnArray[0] +" < "+ fstAdnTrm.toFixed(2)+" + "+secAdnTrm.toFixed(2));
				randomize(".optionsdiv");
			}

		}

		function qnParser(condition, randNumArray,qnNum){
			// function to generate questions according to inequallity signs
			switch (condition) {
				case 0:
					var qnArray = randNumArray.sort(function(a, b){return b-a});
					qn1Ar = [qnArray[0],qnArray[1],eqArray[0]];
					console.log(qn1Ar);
					;
					qnAppender(qnArray, 0);
				break;
				case 1:
					var qnArray = randNumArray.sort(function(a, b){return a-b});
					qn2Ar = [qnArray[0],qnArray[1],eqArray[0]];
					console.log(qn1Ar);
					qnAppender(qnArray, 0);
				break;
				case 2:
					if(eqlSecFlg==true){
						var qnArray = randNumArray.sort(function(a, b){return a-b});
						qn3Ar = [qnArray[0],qnArray[0],eqArray[0]];
						qnAppender(qnArray, 1);
					}else{
						var qnArray = randNumArray.sort(function(a, b){return a-b});
						qn4Ar = [qnArray[0],qnArray[0],eqArray[0]];
						qnAppender(qnArray, 2);
					}
				break;
				case 3:
					var qnArray = randNumArray.sort(function(a, b){return a-b});
					qn5Ar = [qnArray[0],qnArray[1],"<"];
					qnAppender(qnArray, 3);
				break;
				case 4:
					var qnArray = randNumArray.sort(function(a, b){return b-a});
					qn5Ar = [qnArray[0],qnArray[1],">"];
					qnAppender(qnArray, 4);
				break;
				default:

			}
		}
		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			// console.log(eqArray);
				// var randNumArray = RandNumGenerator(0);
				countNext==1?sound_player("s2_p2"):'';
				var randNumArray;
				if(eqArray[0] == '>'){
					randNumArray = RandNumGenerator(0);
					qnParser(0,randNumArray,qnCount);
				}else if (eqArray[0] == '<') {
					randNumArray = RandNumGenerator(0);
					qnParser(1,randNumArray,qnCount);
				}else if (eqArray[0] == '=') {
					if(eqlSecFlg==false){
						eqlSecFlg = true;
						randNumArray = RandNumGenerator(0);
					}else if (eqlSecFlg = true) {
						eqlSecFlg=false;
						randNumArray = RandNumGenerator(1);
					}
					// eqlSecFlg==false?eqlSecFlg = true:eqlSecFlg=false;
					qnParser(2,randNumArray,qnCount);
				}else {
					if(eqArray[0] == '<='){
						randNumArray = RandNumGenerator(1);
						qnParser(3,randNumArray,qnCount);
					}else{
						randNumArray = RandNumGenerator(1);
						qnParser(4,randNumArray,qnCount);
					}
				}
				eqArray.splice(0,1);
				qnCount+=1;
				$(".tipIcon").click(function(){
					$(".tipblkclass").show(0);
				});
				$(".cross").click(function(){
					$(".tipblkclass").hide(0);
				});
			break;
			case 6:
			$(".summaryblkcntr").append("<p class='smryWrd'>"+data.string.sumaryTtx+"</p>");
				$(".summaryblkcntr").append("<img class='grl4' src='"+preload.getResult("girl4").src+"'/img>");
				$(".smry-1:eq(0)").text(qn1Ar[0]+ " " +data.string.andd+ " "  +qn1Ar[1]);
				$(".smry-1:eq(1)").text(qn1Ar[0]+" "+qn1Ar[2]+" "+qn1Ar[1]);

				$(".smry-2:eq(0)").text(qn2Ar[0]+ " " +data.string.andd+ " "  +qn2Ar[1]);
				$(".smry-2:eq(1)").text(qn2Ar[0]+" "+qn2Ar[2]+" "+qn2Ar[1]);

				$(".smry-4:eq(0)").text(qn3Ar[0]+ " " +data.string.andd+ " "  +qn3Ar[0]);
				$(".smry-4:eq(1)").text(qn3Ar[0]+"= ("+(qn3Ar[0]- (qn3Ar[0]/10)).toFixed(2)+") + ("+(qn3Ar[0]/10).toFixed(2)+")");

				$(".smry-3:eq(0)").text(qn4Ar[0]+ " " +data.string.andd+ " "  +qn4Ar[0]);
				$(".smry-3:eq(1)").text(qn4Ar[0]+" "+qn4Ar[2]+" "+qn4Ar[0]);

				$(".smry-5:eq(0)").text(qn5Ar[0]+ " " +data.string.andd+ " "  +qn5Ar[1]);
				$(".smry-5:eq(1)").text(qn5Ar[0] +" "+qn5Ar[2]+" ("+( qn5Ar[1]-(qn5Ar[1]/10)).toFixed(2)+") + ("+(qn5Ar[1]/10).toFixed(2)+")");
				nav_button_controls(1000);
			break;
			default:
				$prevBtn.show(0);
				nav_button_controls(1000);
			break;
		}
		$(".buttonsel").click(function(){
			createjs.Sound.stop();
			if($(this).hasClass("class1")){
				$(this).siblings(".corctopt").show(0);
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
				$(".buttonsel").css("pointer-events","none");
				$(this).css({
					"background":"#98c02e",
					"border":"4px solid #ff0"
				});
			}
			else{
				$(this).siblings(".wrngopt").show(0);
				$(this).css("pointer-events","none");
				play_correct_incorrect_sound(0);
				$(this).css({
					"background":"#f00",
					"border":"4px solid #ff9988"
				});
			}
		});
	}
	/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

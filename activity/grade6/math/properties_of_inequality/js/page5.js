var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
// slide0
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha',
			imgid : 'asha',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox1',
		imgid:'dialogbox',
		textdata:data.string.p5text2,
		textclass:'insidetext'
	}]
},

// slide1
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha',
			imgid : 'asha',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox1',
		imgid:'dialogbox',
		textdata:data.string.p5text3,
		textclass:'insidetext'
	}]
},

//slide2
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text4,
		textclass: "middle_text ",
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text5,
		textclass:'insidetext1',
		datahighlightflag:true,
		datahighlightcustomclass:'underlined'
	}]

},

//slide3
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text6,
		textclass: "middle_text ",
		datahighlightflag:true,
		datahighlightcustomclass:'appear_later'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text7,
		textclass:'insidetext1',
		datahighlightflag:true,
		datahighlightcustomclass:'underlined'
	}]

},

//slide4
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text6,
		textclass: "middle_text ",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text8,
		textclass:'insidetext1',
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}]

},

//slide5
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text9,
		textclass: "middle_text ",
		datahighlightflag:true,
		datahighlightcustomclass:'appear_later'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text10,
		textclass:'insidetext1'
	}]

},

//slide6
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text12,
		textclass: "middle_text ",
		datahighlightflag:true,
		datahighlightcustomclass:'appear_later'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text13,
		textclass:'insidetext1'
	}]

},

//slide7
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text12,
		textclass: "middle_text ",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text14,
		textclass:'insidetext1'
	}]

},

//slide8
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text12,
		textclass: "middle_text ",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text15,
		textclass:'insidetext1'
	}]

},

//slide9
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text6,
		textclass: "middle_text ",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text8,
		textclass:'insidetext1'
	}]

},

//slide10
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},
	{
		textdata:  data.string.p5text6,
		textclass: "top_below_text",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	},{
		textdata:  data.string.p5text17,
		textclass: "middle_text pink",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		 }
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text18,
		textclass:'insidetext1'
	}]

},

//slide11
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},
	{
		textdata:  data.string.p5text6,
		textclass: "top_below_text",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	},{
		textdata:  data.string.p5text20,
		textclass: "middle_text pink",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		},
		{
		 imgclass: 'incorrect',
		 imgid : 'incorrect',
		 imgsrc: '',
		}
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text19,
		textclass:'insidetext1'
	}]

},

//slide12
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},
	{
		textdata:  data.string.p5text6,
		textclass: "top_below_text",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	},{
		textdata:  data.string.p5text21,
		textclass: "middle_text pink",
		datahighlightflag:true,
		datahighlightcustomclass:'fdoutEqn'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		}
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text22,
		textclass:'insidetext1',
		datahighlightflag:true,
		datahighlightcustomclass:'underlined'
	}]

},

//slide13
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},
	{
		textdata:  data.string.p5text6,
		textclass: "top_below_text",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	},{
		textdata:  data.string.p5s14txt,
		textclass: "middle_text pink",
		datahighlightflag:true,
		datahighlightcustomclass:'black'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		}
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text23,
		textclass:'insidetext1',
	}]

},

//slide14
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},
	{
		textdata:  data.string.p5text6,
		textclass: "top_below_text",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		}
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text24,
		textclass:'insidetext1',
	}]

},

//slide15
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},
	{
		textdata:  data.string.p5text6,
		textclass: "top_below_text",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	},{
		textdata:  data.string.p5text25,
		textclass: "middle_text pink",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		}
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text30,
		textclass:'insidetext1',
	}]

},

//slide16
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},
	{
		textdata:  data.string.p5text6,
		textclass: "top_below_text",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	},{
		textdata:  data.string.p5text26,
		textclass: "middle_text pink",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		}
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text31,
		textclass:'insidetext1',
	}]

},

//slide17
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},
	{
		textdata:  data.string.p5text6,
		textclass: "top_below_text",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	},{
		textdata:  data.string.p5text27,
		textclass: "middle_text pink",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		}
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text28,
		textclass:'insidetext1',
	}]

},

//slide18
{
	contentblockadditionalclass:'light_bg',
	extratextblock:[{
		textdata:  data.string.p5text1,
		textclass: "top_text ",
	},{
		textdata:  data.string.p5text6,
		textclass: "middle_text pink",
		datahighlightflag:true,
		datahighlightcustomclass:'none'
	}],
	imageblock:[{
		imagestoshow : [
		 {
			imgclass: 'asha_circle',
			imgid : 'asha_circle',
			imgsrc: '',
		}
		]
	}],
	speechbox:[{
		speechbox:'speechbox2',
		imgid:'dialogbox_blue',
		textdata:data.string.p5text29,
		textclass:'insidetext1',
	}]

},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "asha", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha_circle", src: imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialogbox", src: "images/textbox/white/tl-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialogbox_blue", src: "images/textbox/blue/text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			// soundsa
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p6", src: soundAsset+"s5_p6.ogg"},
			{id: "s5_p7", src: soundAsset+"s5_p7.ogg"},
			{id: "s5_p8", src: soundAsset+"s5_p8.ogg"},
			{id: "s5_p9", src: soundAsset+"s5_p9.ogg"},
			{id: "s5_p10", src: soundAsset+"s5_p10.ogg"},
			{id: "s5_p11", src: soundAsset+"s5_p11.ogg"},
			{id: "s5_p12", src: soundAsset+"s5_p12.ogg"},
			{id: "s5_p13", src: soundAsset+"s5_p13.ogg"},
			{id: "s5_p14", src: soundAsset+"s5_p14.ogg"},
			{id: "s5_p15", src: soundAsset+"s5_p15.ogg"},
			{id: "s5_p16", src: soundAsset+"s5_p16.ogg"},
			{id: "s5_p17", src: soundAsset+"s5_p17.ogg"},
			{id: "s5_p18", src: soundAsset+"s5_p18.ogg"},
			{id: "s5_p19", src: soundAsset+"s5_p19.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		vocabcontroller.findwords(countNext);
		$('.appear_later').css('opacity','0').animate({'opacity':'1'},500);
		$('.speechbox1').css('opacity','0').animate({'opacity':'1'},500);
		$('.speechbox2').css('opacity','0').animate({'opacity':'1'},500);
		switch(countNext) {
			case 0:
			case 1:
				sound_player("s5_p"+(countNext+1),1);
			break;
			case 12:
				sound_player("s5_p"+(countNext+1),1);
				$(".fdoutEqn").find("span").css("color","#000");
				setTimeout(function(){
					$(".fdoutEqn").animate({
						'opacity':'0'
					},500, function(){
						$(".fdoutEqn").html(eval("data.string.p5text21Sec"));
						$(".fdoutEqn").css("opacity","1");
						texthighlight($board);
						$(".fdoutEqn").children().find("span").css("color","#000");
						// nav_button_controls(500);
					});
				},1000);
			break;
			case 18:
				sound_player("s5_p"+(countNext+1),1);
				$('speechbox2').css({'height':'27%','top':'60%'});
				// nav_button_controls(100);
			break;
			default:
				// nav_button_controls(100);
					sound_player("s5_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//alert("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_label_image(content, count){
		if(content[count].hasOwnProperty('imageandlabel')){
			var imageandlabel = content[count].imageandlabel;
			for(var i=0; i<imageandlabel.length; i++){
				var image_src = preload.getResult(imageandlabel[i].imgid).src;
				console.log(image_src);
				var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

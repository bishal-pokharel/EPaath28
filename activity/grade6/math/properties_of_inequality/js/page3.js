var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text1,
			textclass: "top_text ",
		}],
		imageblock:[{
			imagestoshow : [
			 {
				imgclass: 'asha',
				imgid : 'asha',
				imgsrc: '',
			 }
			]
		}],
		speechbox:[{
			speechbox:'speechbox1',
			imgid:'dialogbox',
			textdata:data.string.p3text2,
			textclass:'insidetext'
		}]

	},

	// slide1
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text3,
			textclass: "top_text ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "below_top_text ",
		},{
			textdata:  data.string.p3text5,
			textclass: "middle_text ",
		}],
		imageblock:[{
			imagestoshow : [
			 {
				imgclass: 'asha_circle',
				imgid : 'asha_circle',
				imgsrc: '',
			 }
			]
		}],
		speechbox:[{
			speechbox:'speechbox2',
			imgid:'dialogbox_blue',
			textdata:data.string.p3text6,
			textclass:'insidetext1'
		}]

	},

	// slide2
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text3,
			textclass: "top_text ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "below_top_text ",
		},{
			textdata:  data.string.p3text5,
			textclass: "middle_text ",
		}],
		imageblock:[{
			imagestoshow : [
			 {
				imgclass: 'asha_circle',
				imgid : 'asha_circle',
				imgsrc: '',
			 }
			]
		}],
		speechbox:[{
			speechbox:'speechbox2',
			imgid:'dialogbox_blue',
			textdata:data.string.p3text7,
			textclass:'insidetext1'
		}]

	},

	// slide3
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text3,
			textclass: "top_text ",
		},
		{
			textdata:  data.string.p3text14,
			textclass: "below_top_text ",
			datahighlightflag:true,
			datahighlightcustomclass:'appear_later'
		},{
			textdata:  data.string.p3text5,
			textclass: "middle_text ",
		}],
		imageblock:[{
			imagestoshow : [
			 {
				imgclass: 'asha_circle',
				imgid : 'asha_circle',
				imgsrc: '',
			 }
			]
		}],
		speechbox:[{
			speechbox:'speechbox2',
			imgid:'dialogbox_blue',
			textdata:data.string.p3text8,
			textclass:'insidetext1'
		}]

	},

	// slide4
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text3,
			textclass: "top_text ",
		},
		{
			textdata:  data.string.p3text14,
			textclass: "below_top_text ",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text15,
			textclass: "middle_text ",
			datahighlightflag:true,
			datahighlightcustomclass:'appear_later'
		}],
		imageblock:[{
			imagestoshow : [
			 {
				imgclass: 'asha_circle',
				imgid : 'asha_circle',
				imgsrc: '',
			 }
			]
		}],
		speechbox:[{
			speechbox:'speechbox2',
			imgid:'dialogbox_blue',
			textdata:data.string.p3text9,
			textclass:'insidetext1'
		}]

	},

	// slide5
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text3,
			textclass: "top_text ",
		},
		{
			textdata:  data.string.p3text14,
			textclass: "below_top_text ",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text16,
			textclass: "middle_text ",
			datahighlightflag:true,
			datahighlightcustomclass:'appear_later'
		}],
		imageblock:[{
			imagestoshow : [
			 {
				imgclass: 'asha_circle',
				imgid : 'asha_circle',
				imgsrc: '',
			 }
			]
		}],
		speechbox:[{
			speechbox:'speechbox2',
			imgid:'dialogbox_blue',
			textdata:data.string.p3text10,
			textclass:'insidetext1'
		}]

	},

	// slide6
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text3,
			textclass: "top_text ",
		},
		{
			textdata:  data.string.p3text14,
			textclass: "below_top_text ",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text17,
			textclass: "middle_text ",
			datahighlightflag:true,
			datahighlightcustomclass:'appear_later'
		}],
		imageblock:[{
			imagestoshow : [
			 {
				imgclass: 'asha_circle',
				imgid : 'asha_circle',
				imgsrc: '',
			 }
			]
		}],
		speechbox:[{
			speechbox:'speechbox2',
			imgid:'dialogbox_blue',
			textdata:data.string.p3text11,
			textclass:'insidetext1'
		}]

	},



		// slide7
		{
			contentblockadditionalclass:'light_bg',
			extratextblock:[{
				textdata:  data.string.p3text3,
				textclass: "top_text ",
			},
			{
				textdata:  data.string.p3text4,
				textclass: "below_top_text "
			},{
				textdata:  data.string.p3text12,
				textclass: "middle_text ",
				datahighlightflag:true,
				datahighlightcustomclass:'appear_later'
			}],
			imageblock:[{
				imagestoshow : [
				 {
					imgclass: 'asha_circle',
					imgid : 'asha_circle',
					imgsrc: '',
				 }
				]
			}],
			speechbox:[{
				speechbox:'speechbox2',
				imgid:'dialogbox_blue',
				textdata:data.string.p3text13,
				textclass:'insidetext1'
			}]

		},

		// slide8
		{
			contentblockadditionalclass:'light_bg',
			extratextblock:[{
				textdata:  data.string.p3text3,
				textclass: "top_text ",
			},
			{
				textdata:  data.string.p3text18,
				textclass: "below_top_text ",
				datahighlightflag:true,
				datahighlightcustomclass:'appear_later'
			},{
				textdata:  data.string.p3text19,
				textclass: "middle_text ",
			}],
			imageblock:[{
				imagestoshow : [
				 {
					imgclass: 'asha_circle',
					imgid : 'asha_circle',
					imgsrc: '',
				 }
				]
			}],
			speechbox:[{
				speechbox:'speechbox2',
				imgid:'dialogbox_blue',
				textdata:data.string.p3text8,
				textclass:'insidetext1'
			}]

		},


			// slide9
			{
				contentblockadditionalclass:'light_bg',
				extratextblock:[{
					textdata:  data.string.p3text3,
					textclass: "top_text ",
				},
				{
					textdata:  data.string.p3text18,
					textclass: "below_top_text ",
					datahighlightflag:true,
					datahighlightcustomclass:'none'
				},{
					textdata:  data.string.p3text20,
					textclass: "middle_text ",
					datahighlightflag:true,
					datahighlightcustomclass:'appear_later'
				}],
				imageblock:[{
					imagestoshow : [
					 {
						imgclass: 'asha_circle',
						imgid : 'asha_circle',
						imgsrc: '',
					 }
					]
				}],
				speechbox:[{
					speechbox:'speechbox2',
					imgid:'dialogbox_blue',
					textdata:data.string.p3text23,
					textclass:'insidetext1'
				}]

			},

			// slide10
			{
				contentblockadditionalclass:'light_bg',
				extratextblock:[{
					textdata:  data.string.p3text3,
					textclass: "top_text ",
				},
				{
					textdata:  data.string.p3text18,
					textclass: "below_top_text ",
					datahighlightflag:true,
					datahighlightcustomclass:'none'
				},{
					textdata:  data.string.p3text21,
					textclass: "middle_text ",
					datahighlightflag:true,
					datahighlightcustomclass:'appear_later'
				}],
				imageblock:[{
					imagestoshow : [
					 {
						imgclass: 'asha_circle',
						imgid : 'asha_circle',
						imgsrc: '',
					 }
					]
				}],
				speechbox:[{
					speechbox:'speechbox2',
					imgid:'dialogbox_blue',
					textdata:data.string.p3text24,
					textclass:'insidetext1'
				}]

			},

			// slide11
			{
				contentblockadditionalclass:'light_bg',
				extratextblock:[{
					textdata:  data.string.p3text3,
					textclass: "top_text ",
				},
				{
					textdata:  data.string.p3text18,
					textclass: "below_top_text ",
					datahighlightflag:true,
					datahighlightcustomclass:'none'
				},{
					textdata:  data.string.p3text22,
					textclass: "middle_text ",
					datahighlightflag:true,
					datahighlightcustomclass:'appear_later'
				}],
				imageblock:[{
					imagestoshow : [
					 {
						imgclass: 'asha_circle',
						imgid : 'asha_circle',
						imgsrc: '',
					 }
					]
				}],
				speechbox:[{
					speechbox:'speechbox2',
					imgid:'dialogbox_blue',
					textdata:data.string.p3text25,
					textclass:'insidetext1'
				}]

			},

			// slide12
			{
				contentblockadditionalclass:'light_bg',
				extratextblock:[{
					textdata:  data.string.p3text3,
					textclass: "top_text ",
				},
				{
					textdata:  data.string.p3text4,
					textclass: "below_top_text "
				},{
					textdata:  data.string.p3text26,
					textclass: "middle_text ",
					datahighlightflag:true,
					datahighlightcustomclass:'appear_later'
				}],
				imageblock:[{
					imagestoshow : [
					 {
						imgclass: 'asha_circle',
						imgid : 'asha_circle',
						imgsrc: '',
					 }
					]
				}],
				speechbox:[{
					speechbox:'speechbox2',
					imgid:'dialogbox_blue',
					textdata:data.string.p3text13,
					textclass:'insidetext1'
				}]

			},

			// slide13
			{
				contentblockadditionalclass:'light_bg',
				extratextblock:[{
					textdata:  data.string.p3text3,
					textclass: "top_text ",
				},
				{
					textdata:  data.string.p3text31,
					textclass: "below_top_text ",
					datahighlightflag:true,
					datahighlightcustomclass:'appear_later'
				},{
					textdata:  data.string.p3text27,
					textclass: "middle_text ",
				}],
				imageblock:[{
					imagestoshow : [
					 {
						imgclass: 'asha_circle',
						imgid : 'asha_circle',
						imgsrc: '',
					 }
					]
				}],
				speechbox:[{
					speechbox:'speechbox2',
					imgid:'dialogbox_blue',
					textdata:data.string.p3text8,
					textclass:'insidetext1'
				}]

			},


				// slide14
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text31,
						textclass: "below_top_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'none'
					},{
						textdata:  data.string.p3text28,
						textclass: "middle_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'appear_later'
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text32,
						textclass:'insidetext1'
					}]

				},

				// slide15
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text31,
						textclass: "below_top_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'none'
					},{
						textdata:  data.string.p3text29,
						textclass: "middle_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'appear_later'
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text33,
						textclass:'insidetext1'
					}]

				},

				// slide16
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text31,
						textclass: "below_top_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'none'
					},{
						textdata:  data.string.p3text30,
						textclass: "middle_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'appear_later'
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
					 ]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text34,
						textclass:'insidetext1'
					}]

				},

				// slide17
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text35,
						textclass: "below_top_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'underlined'
					},{
						textdata:  data.string.p3text26,
						textclass: "middle_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'none'
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text36,
						textclass:'insidetext1'
					}]

				},

				// slide18
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text4,
						textclass: "below_top_text ",
					},{
						textdata:  data.string.p3text37,
						textclass: "middle_text ",
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text38,
						textclass:'insidetext1'
					}]

				},

				// slide19
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text4,
						textclass: "below_top_text ",
					},{
						textdata:  data.string.p3text37,
						textclass: "middle_text ",
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text39,
						textclass:'insidetext1'
					}]

				},

				// slide20
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text40,
						textclass: "below_top_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'appear_later'
					},{
						textdata:  data.string.p3text37,
						textclass: "middle_text ",
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text8,
						textclass:'insidetext1'
					}]

				},

				// slide21
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text40,
						textclass: "below_top_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'none'
					},{
						textdata:  data.string.p3text41,
						textclass: "middle_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'appear_later'
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text44,
						textclass:'insidetext1'
					}]

				},

				// slide22
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text40,
						textclass: "below_top_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'none'
					},{
						textdata:  data.string.p3text42,
						textclass: "middle_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'appear_later'
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text45,
						textclass:'insidetext1'
					}]

				},

				// slide23
				{
					contentblockadditionalclass:'light_bg',
					extratextblock:[{
						textdata:  data.string.p3text3,
						textclass: "top_text ",
					},
					{
						textdata:  data.string.p3text40,
						textclass: "below_top_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'none'
					},{
						textdata:  data.string.p3text43,
						textclass: "middle_text ",
						datahighlightflag:true,
						datahighlightcustomclass:'appear_later'
					}],
					imageblock:[{
						imagestoshow : [
						 {
							imgclass: 'asha_circle',
							imgid : 'asha_circle',
							imgsrc: '',
						 }
						]
					}],
					speechbox:[{
						speechbox:'speechbox2',
						imgid:'dialogbox_blue',
						textdata:data.string.p3text46,
						textclass:'insidetext1'
					}]

				},

				// slide24
				{
				  contentblockadditionalclass:'light_bg',
				  extratextblock:[{
				    textdata:  data.string.p3text3,
				    textclass: "top_text ",
				  },
				  {
				    textdata:  data.string.p3text4,
				    textclass: "below_top_text "
				  },{
				    textdata:  data.string.p3text47,
				    textclass: "middle_text ",
				    datahighlightflag:true,
				    datahighlightcustomclass:'appear_later'
				  }],
				  imageblock:[{
				    imagestoshow : [
				     {
				      imgclass: 'asha_circle',
				      imgid : 'asha_circle',
				      imgsrc: '',
				     }
				    ]
				  }],
				  speechbox:[{
				    speechbox:'speechbox2',
				    imgid:'dialogbox_blue',
				    textdata:data.string.p3text48,
				    textclass:'insidetext1'
				  }]

				},

				// slide25
				{
				  contentblockadditionalclass:'light_bg',
				  extratextblock:[{
				    textdata:  data.string.p3text3,
				    textclass: "top_text ",
				  },
				  {
				    textdata:  data.string.p3text51,
				    textclass: "below_top_text ",
				    datahighlightflag:true,
				    datahighlightcustomclass:'appear_later'
				  },{
				    textdata:  data.string.p3text50,
				    textclass: "middle_text ",
				  }],
				  imageblock:[{
				    imagestoshow : [
				     {
				      imgclass: 'asha_circle',
				      imgid : 'asha_circle',
				      imgsrc: '',
				     }
				    ]
				  }],
				  speechbox:[{
				    speechbox:'speechbox2',
				    imgid:'dialogbox_blue',
				    textdata:data.string.p3text8,
				    textclass:'insidetext1'
				  }]

				},


				  // slide26
				  {
				    contentblockadditionalclass:'light_bg',
				    extratextblock:[{
				      textdata:  data.string.p3text3,
				      textclass: "top_text ",
				    },
				    {
				      textdata:  data.string.p3text51,
				      textclass: "below_top_text ",
				      datahighlightflag:true,
				      datahighlightcustomclass:'none'
				    },{
				      textdata:  data.string.p3text52,
				      textclass: "middle_text ",
				      datahighlightflag:true,
				      datahighlightcustomclass:'appear_later'
				    }],
				    imageblock:[{
				      imagestoshow : [
				       {
				        imgclass: 'asha_circle',
				        imgid : 'asha_circle',
				        imgsrc: '',
				       }
				      ]
				    }],
				    speechbox:[{
				      speechbox:'speechbox2',
				      imgid:'dialogbox_blue',
				      textdata:data.string.p3text55,
				      textclass:'insidetext1'
				    }]

				  },

				  // slide27
				  {
				    contentblockadditionalclass:'light_bg',
				    extratextblock:[{
				      textdata:  data.string.p3text3,
				      textclass: "top_text ",
				    },
				    {
				      textdata:  data.string.p3text51,
				      textclass: "below_top_text ",
				      datahighlightflag:true,
				      datahighlightcustomclass:'none'
				    },{
				      textdata:  data.string.p3text53,
				      textclass: "middle_text ",
				      datahighlightflag:true,
				      datahighlightcustomclass:'appear_later'
				    }],
				    imageblock:[{
				      imagestoshow : [
				       {
				        imgclass: 'asha_circle',
				        imgid : 'asha_circle',
				        imgsrc: '',
				       }
				      ]
				    }],
				    speechbox:[{
				      speechbox:'speechbox2',
				      imgid:'dialogbox_blue',
				      textdata:data.string.p3text56,
				      textclass:'insidetext1'
				    }]

				  },

				  // slide28
				  {
				    contentblockadditionalclass:'light_bg',
				    extratextblock:[{
				      textdata:  data.string.p3text3,
				      textclass: "top_text ",
				    },
				    {
				      textdata:  data.string.p3text51,
				      textclass: "below_top_text ",
				      datahighlightflag:true,
				      datahighlightcustomclass:'none'
				    },{
				      textdata:  data.string.p3text54,
				      textclass: "middle_text ",
				      datahighlightflag:true,
				      datahighlightcustomclass:'appear_later'
				    }],
				    imageblock:[{
				      imagestoshow : [
				       {
				        imgclass: 'asha_circle',
				        imgid : 'asha_circle',
				        imgsrc: '',
				       }
				      ]
				    }],
				    speechbox:[{
				      speechbox:'speechbox2',
				      imgid:'dialogbox_blue',
				      textdata:data.string.p3text57,
				      textclass:'insidetext1'
				    }]

				  },

					// slide29
					{
					  contentblockadditionalclass:'light_bg',
					  extratextblock:[{
					    textdata:  data.string.p3text3,
					    textclass: "top_text ",
					  },
					  {
					    textdata:  data.string.p3text4,
					    textclass: "below_top_text "
					  },{
					    textdata:  data.string.p3text58,
					    textclass: "middle_text ",
					    datahighlightflag:true,
					    datahighlightcustomclass:'appear_later'
					  }],
					  imageblock:[{
					    imagestoshow : [
					     {
					      imgclass: 'asha_circle',
					      imgid : 'asha_circle',
					      imgsrc: '',
					     }
					    ]
					  }],
					  speechbox:[{
					    speechbox:'speechbox2',
					    imgid:'dialogbox_blue',
					    textdata:data.string.p3text59,
					    textclass:'insidetext1'
					  }]

					},

					// slide30
					{
					  contentblockadditionalclass:'light_bg',
					  extratextblock:[{
					    textdata:  data.string.p3text3,
					    textclass: "top_text ",
					  },
					  {
					    textdata:  data.string.p3text61,
					    textclass: "below_top_text ",
					    datahighlightflag:true,
					    datahighlightcustomclass:'appear_later'
					  },{
					    textdata:  data.string.p3text60,
					    textclass: "middle_text ",
					  }],
					  imageblock:[{
					    imagestoshow : [
					     {
					      imgclass: 'asha_circle',
					      imgid : 'asha_circle',
					      imgsrc: '',
					     }
					    ]
					  }],
					  speechbox:[{
					    speechbox:'speechbox2',
					    imgid:'dialogbox_blue',
					    textdata:data.string.p3text8,
					    textclass:'insidetext1'
					  }]

					},


					  // slide31
					  {
					    contentblockadditionalclass:'light_bg',
					    extratextblock:[{
					      textdata:  data.string.p3text3,
					      textclass: "top_text ",
					    },
					    {
					      textdata:  data.string.p3text61,
					      textclass: "below_top_text ",
					      datahighlightflag:true,
					      datahighlightcustomclass:'none'
					    },{
					      textdata:  data.string.p3text62,
					      textclass: "middle_text ",
					      datahighlightflag:true,
					      datahighlightcustomclass:'appear_later'
					    }],
					    imageblock:[{
					      imagestoshow : [
					       {
					        imgclass: 'asha_circle',
					        imgid : 'asha_circle',
					        imgsrc: '',
					       }
					      ]
					    }],
					    speechbox:[{
					      speechbox:'speechbox2',
					      imgid:'dialogbox_blue',
					      textdata:data.string.p3text65,
					      textclass:'insidetext1'
					    }]

					  },

					  // slide32
					  {
					    contentblockadditionalclass:'light_bg',
					    extratextblock:[{
					      textdata:  data.string.p3text3,
					      textclass: "top_text ",
					    },
					    {
					      textdata:  data.string.p3text61,
					      textclass: "below_top_text ",
					      datahighlightflag:true,
					      datahighlightcustomclass:'none'
					    },{
					      textdata:  data.string.p3text63,
					      textclass: "middle_text ",
					      datahighlightflag:true,
					      datahighlightcustomclass:'appear_later'
					    }],
					    imageblock:[{
					      imagestoshow : [
					       {
					        imgclass: 'asha_circle',
					        imgid : 'asha_circle',
					        imgsrc: '',
					       }
					      ]
					    }],
					    speechbox:[{
					      speechbox:'speechbox2',
					      imgid:'dialogbox_blue',
					      textdata:data.string.p3text66,
					      textclass:'insidetext1'
					    }]

					  },

					  // slide33
					  {
					    contentblockadditionalclass:'light_bg',
					    extratextblock:[{
					      textdata:  data.string.p3text3,
					      textclass: "top_text ",
					    },
					    {
					      textdata:  data.string.p3text61,
					      textclass: "below_top_text ",
					      datahighlightflag:true,
					      datahighlightcustomclass:'none'
					    },{
					      textdata:  data.string.p3text64,
					      textclass: "middle_text ",
					      datahighlightflag:true,
					      datahighlightcustomclass:'appear_later'
					    }],
					    imageblock:[{
					      imagestoshow : [
					       {
					        imgclass: 'asha_circle',
					        imgid : 'asha_circle',
					        imgsrc: '',
					       }
					      ]
					    }],
					    speechbox:[{
					      speechbox:'speechbox2',
					      imgid:'dialogbox_blue',
					      textdata:data.string.p3text67,
					      textclass:'insidetext1'
					    }]

					  },

						// slide34
						{
							contentblockadditionalclass:'light_bg',
							extratextblock:[{
								textdata:  data.string.p3text3,
								textclass: "top_text ",
							},
							{
								textdata:  data.string.p3text68,
								textclass: "below_top_text ",
								datahighlightflag:true,
								datahighlightcustomclass:'underlined'
							},{
								textdata:  data.string.p3text58,
								textclass: "middle_text ",
								datahighlightflag:true,
								datahighlightcustomclass:'none'
							}],
							imageblock:[{
								imagestoshow : [
								 {
									imgclass: 'asha_circle',
									imgid : 'asha_circle',
									imgsrc: '',
								 }
								]
							}],
							speechbox:[{
								speechbox:'speechbox2',
								imgid:'dialogbox_blue',
								textdata:data.string.p3text69,
								textclass:'insidetext1'
							}]

						},

						// slide35
						{
							contentblockadditionalclass:'light_bg',
							extratextblock:[{
								textdata:  data.string.p3text3,
								textclass: "top_text ",
							}],
							imageblock:[{
								imagestoshow : [
								 {
									imgclass: 'asha',
									imgid : 'asha',
									imgsrc: '',
								 }
								]
							}],
							speechbox:[{
								speechbox:'speechbox3',
								imgid:'dialogbox',
								textdata:data.string.p3text70,
								textclass:'insidetext'
							}]

						},

						// slide36
						{
							contentblockadditionalclass:'light_bg',
							extratextblock:[{
								textdata:  data.string.p3text3,
								textclass: "top_text ",
							}],
							imageblock:[{
								imagestoshow : [
								 {
									imgclass: 'asha',
									imgid : 'asha',
									imgsrc: '',
								 }
								]
							}],
							speechbox:[{
								speechbox:'speechbox3',
								imgid:'dialogbox',
								textdata:data.string.p3text71,
								textclass:'insidetext'
							}]

						},

						// slide37
						{
							contentblockadditionalclass:'light_bg',
							extratextblock:[{
								textdata:  data.string.p3text3,
								textclass: "top_text blur",
							},{
								textdata:  data.string.p3text73,
								textclass: "addition_text blur",
								datahighlightflag:true,
								datahighlightcustomclass:'text_bg'
							},{
								textdata:  data.string.p3text72,
								textclass: "subtraction_text blur",
								datahighlightflag:true,
								datahighlightcustomclass:'text_bg'
							}],
							imageblock:[{
								imagestoshow : [
								 {
									imgclass: 'asha',
									imgid : 'asha',
									imgsrc: '',
								 }
								]
							}],
							speechbox:[{
								speechbox:'speechbox3',
								imgid:'dialogbox',
								textdata:data.string.p3text74,
								textclass:'insidetext'
							}]

						},

						// slide38
						{
							contentblockadditionalclass:'light_bg',
							extratextblock:[{
								textdata:  data.string.p3text3,
								textclass: "top_text",
							},{
								textdata:  data.string.p3text73,
								textclass: "addition_text",
								datahighlightflag:true,
								datahighlightcustomclass:'text_bg'
							},{
								textdata:  data.string.p3text72,
								textclass: "subtraction_text",
								datahighlightflag:true,
								datahighlightcustomclass:'text_bg'
							}]
						},




];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type oth			{id: "dialogbox", src: "images/textbox/white/tl-1.png", type: createjs.AbstractLoader.IMAGE},
		manifest = [
			//images
			{id: "asha", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha_circle", src: imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialogbox", src: "images/textbox/white/tl-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialogbox_blue", src: "images/textbox/blue/text_box01.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p7", src: soundAsset+"s3_p7.ogg"},
			{id: "s3_p8", src: soundAsset+"s3_p8.ogg"},
			{id: "s3_p9", src: soundAsset+"s3_p9.ogg"},
			{id: "s3_p10", src: soundAsset+"s3_p10.ogg"},
			{id: "s3_p11", src: soundAsset+"s3_p11.ogg"},
			{id: "s3_p12", src: soundAsset+"s3_p12.ogg"},
			{id: "s3_p13", src: soundAsset+"s3_p13.ogg"},
			{id: "s3_p14", src: soundAsset+"s3_p14.ogg"},
			{id: "s3_p15", src: soundAsset+"s3_p15.ogg"},
			{id: "s3_p16", src: soundAsset+"s3_p16.ogg"},
			{id: "s3_p17", src: soundAsset+"s3_p17.ogg"},
			{id: "s3_p18", src: soundAsset+"s3_p18.ogg"},
			{id: "s3_p19", src: soundAsset+"s3_p19.ogg"},
			{id: "s3_p19", src: soundAsset+"s3_p19.ogg"},
			{id: "s3_p20", src: soundAsset+"s3_p20.ogg"},
			{id: "s3_p21", src: soundAsset+"s3_p21.ogg"},
			{id: "s3_p22", src: soundAsset+"s3_p22.ogg"},
			{id: "s3_p23", src: soundAsset+"s3_p23.ogg"},
			{id: "s3_p24", src: soundAsset+"s3_p24.ogg"},
			{id: "s3_p25", src: soundAsset+"s3_p25.ogg"},
			{id: "s3_p26", src: soundAsset+"s3_p26.ogg"},
			{id: "s3_p27", src: soundAsset+"s3_p27.ogg"},
			{id: "s3_p28", src: soundAsset+"s3_p28.ogg"},
			{id: "s3_p29", src: soundAsset+"s3_p29.ogg"},
			{id: "s3_p30", src: soundAsset+"s3_p30.ogg"},
			{id: "s3_p31", src: soundAsset+"s3_p31.ogg"},
			{id: "s3_p32", src: soundAsset+"s3_p32.ogg"},
			{id: "s3_p33", src: soundAsset+"s3_p33.ogg"},
			{id: "s3_p34", src: soundAsset+"s3_p34.ogg"},
			{id: "s3_p35", src: soundAsset+"s3_p35.ogg"},
			{id: "s3_p36", src: soundAsset+"s3_p36.ogg"},
			{id: "s3_p37", src: soundAsset+"s3_p37.ogg"},
			{id: "s3_p38", src: soundAsset+"s3_p38.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		$('.appear_later').css('opacity','0').animate({'opacity':'1'},500);
		$('.speechbox1').css('opacity','0').animate({'opacity':'1'},500);
		$('.speechbox2').css('opacity','0').animate({'opacity':'1'},500);
		$('.speechbox3').css('opacity','0').animate({'opacity':'1'},500);
		// $nextBtn.show();
			//switch case for each slides
			switch (countNext) {
				case 0:
					sound_player("s3_p1",1);
				break;
				case 17:
				$('.below_top_text').css('top','22%');
					sound_player("s3_p"+(countNext+1),1);
				break;
				case 35:
				case 36:
				case 37:
					sound_player("s3_p"+(countNext+1),1);
				break;
				case 38:
					nav_button_controls(300);
				break;
				default:
					// $prevBtn.show(0);
					// nav_button_controls(1000);
						sound_player("s3_p"+(countNext+1),1);
					break;
			}
	}

		function sound_player(sound_id,next){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				next?nav_button_controls(300):'';
			});
		}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}


	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//alert("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}


	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
				$('.appear_later').css('opacity','0').animate({'opacity':'1'},500);

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

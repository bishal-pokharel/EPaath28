// **************************************************************
					// THIS IS PAGE 7
// **************************************************************
var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
// slide0
	{
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"bg_diy",
				imgsrc:"",
			}]
		}],
		extratextblock:[{
			textclass:'diytxt',
			textdata:data.string.diy
		}]
	},
//slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		exetype1:[{
			qntxt:[{
				ques_class:"topQn",
			},{
				ques_class:"topQn_fst",
				textdata:data.string.clktxt
			}],
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"class1",
			},{},{},{}]
		}]
	},
	//slide2
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "thebg1",
			exetype1:[{
				qntxt:[{
					ques_class:"topQn",
				},{
					ques_class:"topQn_fst",
					textdata:data.string.clktxt
				}],
				optionsdivclass:"optionsdiv",
				exeoptions:[{
					optaddclass:"class1",
				},{},{},{}]
			}]
		},
	//slide3
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "thebg1",
			exetype1:[{
				qntxt:[{
					ques_class:"topQn",
				},{
					ques_class:"topQn_fst",
					textdata:data.string.clktxt
				}],
				optionsdivclass:"optionsdiv",
				exeoptions:[{
					optaddclass:"class1",
				},{},{},{}]
			}]
		},
	//slide4
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "thebg1",
			exetype1:[{
				qntxt:[{
					ques_class:"topQn",
				},{
					ques_class:"topQn_fst",
					textdata:data.string.clktxt
				}],
				optionsdivclass:"optionsdiv",
				exeoptions:[{
					optaddclass:"class1",
				},{},{},{}]
			}]
		},
	//slide5
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "thebg1",
			exetype1:[{
				qntxt:[{
					ques_class:"topQn",
				},{
					ques_class:"topQn_fst",
					textdata:data.string.clktxt
				}],
				optionsdivclass:"optionsdiv",
				exeoptions:[{
					optaddclass:"class1",
				},{},{},{}]
			}],
		},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var qnNUmArray = [1,2,3,4,5];
	qnNUmArray.shufflearray();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl1", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_diy", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s7_p2", src: soundAsset+"s7_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		var count=1;
		var corcnt=0;

		var parent=$(".op"+count);
		//alert(parent);
		var divs = parent.children().not(".qnclass");
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	    }
		$(".buttonsel").click(function(){
			createjs.Sound.stop();
			/*class 1 is always for the right answer. updates scoreboard and disables other click if
			right answer is clicked*/

			if($(this).hasClass("class1") && $(this).hasClass("forhover")){
				play_correct_incorrect_sound(1);
				$(this).css({
					"background":"#98c02e",
					"color":"white",
					"border":"5px solid #deef3c",
					"pointer-events":"none"
				});
				$(".buttonsel").css("pointer-events","none");
				$(this).siblings(".corctopt").show(0);
				nav_button_controls(100);
			}
			else{
				play_correct_incorrect_sound(0) ;
				$(this).css({
					"background":"#FF0000",
					"color":"white",
					"border":"5px solid #98000 ",
					"pointer-events":"none"
				});
				$(this).siblings(".wrngopt").show(0);
				}
		});
		switch (countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls();
			break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				countNext==1?sound_player("s7_p2",0):'';
				$(".topQn").html(eval("data.string.p6q"+qnNUmArray[0]));
				$(".buttonsel:eq(0)").html(eval("data.string.p6_q"+qnNUmArray[0]+"_op1"));
				$(".buttonsel:eq(1)").html(eval("data.string.p6_q"+qnNUmArray[0]+"_op2"));
				$(".buttonsel:eq(2)").html(eval("data.string.p6_q"+qnNUmArray[0]+"_op3"));
				$(".buttonsel:eq(3)").html(eval("data.string.p6_q"+qnNUmArray[0]+"_op4"));
				qnNUmArray.splice(0,1);
				randomize(".optionsdiv");
				console.log(qnNUmArray);
			break;
			default:
				// $prevBtn.show(0);
				// nav_button_controls(1000);
			break;
		}
	}

 	/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

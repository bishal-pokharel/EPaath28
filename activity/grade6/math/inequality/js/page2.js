var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',

		extratextblock:[{
			textdata: data.string.p2text1,
			textclass: "diy-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg',
				imgclass:'background'
			}]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text2,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
				}]
	},{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'line',
				imgclass:'yellowline',
				imgsrc: '',
			}]
		}],
			speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p2text4,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "tex",
					}]
	},{
		//slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'line',
				imgclass:'yellowline',
				imgsrc: '',
			}]
		}],
			speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p2text5,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "tex",
					}],
	},{
		//slide 4
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'line',
				imgclass:'yellowline',
				imgsrc: '',
			}]
		}],
			speechbox:[{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text6,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "tex",
					}],
	},{
		//slide 5
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'line',
				imgclass:'yellowline',
				imgsrc: '',
			}]
		}],
			speechbox:[{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlight-text',
					speechbox: 'sp-1',
					textdata : data.string.p2text7,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "tex",
					}],
	},{
		//slide 6
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg2',
				imgclass:'background'
			},{
				imgid:'jug',
				imgclass:'jug1',
				imgsrc: '',
			},{
				imgid:'line',
				imgclass:'yellowline1',
				imgsrc: '',
			}]
		}],
				extratextblock:[{
					textdata: data.string.p2text3,
					textclass: "tex1",
				},{
					textdata: data.string.p2text9,
					textclass: "tex2",
				}]
	},{
		//slide 7
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text10,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
				}]
	},{
		//slide 8
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline1',
				imgsrc: '',
			}]
		}],
			speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p2text11,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "whitetext",
					},{
						textdata: data.string.p2text12,
						textclass: "whitetext1",
					}]
	},{
			contentnocenteradjust: true,
			contentblockadditionalclass: 'diy-bg',
			imageblock:[{
				imagestoshow:[{
					imgid:'bg1',
					imgclass:'background'
				},{
					imgid:'jug1',
					imgclass:'jug',
					imgsrc: '',
				},{
					imgid:'whiteline',
					imgclass:'whiteline',
					imgsrc: '',
				},{
					imgid:'whiteline',
					imgclass:'whiteline1',
					imgsrc: '',
				}]
			}],
				speechbox:[{
						speechbox: 'sp-1',
						textdata : data.string.p2text5,
						textclass : 'text',
						imgclass: 'box',
						imgid : 'spbox',
						imgsrc: '',
				}],
				extratextblock:[{
					textdata: data.string.p2text3,
					textclass: "whitetext",
				},{
					textdata: data.string.p2text12,
					textclass: "whitetext1",
				}]

	},{
		//slide9
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline1',
				imgsrc: '',
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text13,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "whitetext",
					},{
						textdata: data.string.p2text12,
						textclass: "whitetext1",
					}]
	},{
		//slide 10
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline1',
				imgsrc: '',
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight-text',
					speechbox: 'sp-1',
					textdata : data.string.p2text14,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "whitetext",
					},{
						textdata: data.string.p2text12,
						textclass: "whitetext1",
					}]
	},{
		//slide 11
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg2',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug1',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'yellowline1',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline2',
				imgsrc: '',
			}]
		}],
				extratextblock:[{
					textdata: data.string.p2text3,
					textclass: "whitetext2",
				},{
					textdata: data.string.p2text12,
					textclass: "whitetext3",
				},{
					textdata: data.string.p2text15,
					textclass: "tex2",
				}]
	},{
		//slide 12
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text16,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
				}]
	},{
		//slide 13
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline1',
				imgsrc: '',
			}]
		}],
			speechbox:[{
				// datahighlightflag : true,
				// datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text17,
					textclass : 'text1',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "whitetext",
					},{
						textdata: data.string.p2text12,
						textclass: "whitetext1",
					}]
	},{
		//slide 14
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline1',
				imgsrc: '',
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text5,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "whitetext",
					},{
						textdata: data.string.p2text12,
						textclass: "whitetext1",
					},{
						textdata: data.string.p2text18,
						textclass: "hint-box",
					},{
						datahighlightflag : true,
						datahighlightcustomclass : 'color-highlight',
						textdata: data.string.p2text19,
						textclass: "hidden-box",
					}]
	},{
		//slide 15
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',

		extratextblock:[{
			textdata: data.string.p2text20,
			textclass: "cen-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg',
				imgclass:'background'
			}]
		}]
	},{
		//slide 16
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline1',
				imgsrc: '',
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text21,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "whitetext",
					},{
						textdata: data.string.p2text12,
						textclass: "whitetext1",
					},{
						textdata: data.string.p2text22,
						textclass: "text-1",
					}]
	},{
		//slide 17
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline1',
				imgsrc: '',
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text23,
					textclass : 'text1',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "whitetext",
					},{
						textdata: data.string.p2text12,
						textclass: "whitetext1",
					},{
						textdata: data.string.p2text24,
						textclass: "text-1",
					}]
	},{
		//slide 18
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'bg1',
				imgclass:'background'
			},{
				imgid:'jug1',
				imgclass:'jug',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline',
				imgsrc: '',
			},{
				imgid:'whiteline',
				imgclass:'whiteline1',
				imgsrc: '',
			}]
		}],
			speechbox:[{
				datahighlightflag : true,
				datahighlightcustomclass : 'highlighttext',
					speechbox: 'sp-1',
					textdata : data.string.p2text25,
					textclass : 'text',
					imgclass: 'box',
					imgid : 'spbox',
					imgsrc: '',
			}],
					extratextblock:[{
						textdata: data.string.p2text3,
						textclass: "whitetext",
					},{
						textdata: data.string.p2text12,
						textclass: "whitetext1",
					}]
	},{
		//slide 19
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'top-text-block',
		uppertextblock:[{
			textdata: data.string.p2text26,
			textclass: "top-text",
		}],
		table:[

					{
						tablehead:[{

							textclass:"content",
							textdata:data.string.symbol
						},{
							textclass:"content",
							textdata:data.string.word
						},{
							textclass:"content",
							textdata:data.string.example
						}],
						firstrowdata	:[
									{
										datahighlightflag : true,
										datahighlightcustomclass : 'symbols',
											textclass:"first",
											textdata:data.string.isequal
									},
									{
										datahighlightflag : true,
										datahighlightcustomclass : 'symbol',
											textclass:"first",
											textdata:data.string.p1text6
									},
									{
											textclass:"first",
											textdata:data.string.p1text10
									}
							],
							 secondrowdata:[
									{
										datahighlightflag : true,
										datahighlightcustomclass : 'symbol',
											textclass:"content",
											textdata:data.string.isnotequal
									},
									{
											textclass:"content",
											textdata:data.string.p1text7
									},
									{
											textclass:"content",
											textdata:data.string.p1text11 + data.string.notequal + data.string.n9
									}
							],
							thirdrowdata:[
									{
										datahighlightflag : true,
										datahighlightcustomclass : 'symbol',
											textclass:"content",
											textdata:data.string.islessthan
									},
									{
											textclass:"content",
											textdata:data.string.p1text8
									},
									{
											textclass:"content",
											textdata:data.string.p1text18
									}
							],
							fourrowdata:[
									{
										datahighlightflag : true,
										datahighlightcustomclass : 'symbol',
											textclass:"content",
											textdata:data.string.isgreaterthan
									},
									{
											textclass:"content",
											textdata:data.string.p1text9
									},
									{
											textclass:"content",
											textdata:data.string.p1text19
									}],
									fifthrowdata:[
											{
												datahighlightflag : true,
												datahighlightcustomclass : 'symbol',
													textclass:"content",
													textdata:data.string.greaterthanorequal
											},
											{
													textclass:"content",
													textdata:data.string.p1text16
											},
											{
													textclass:"content",
													textdata:data.string.p4text31
											}
									],
									sixrowdata:[
											{
												datahighlightflag : true,
												datahighlightcustomclass : 'symbol',
													textclass:"content",
													textdata:data.string.lessthanorequal
											},
											{
													textclass:"content",
													textdata:data.string.p1text17
											},
											{
													textclass:"content",
													textdata:data.string.p4text32
											}]
					}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "fulcrum", src: imgpath+"fulcrum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug", src: imgpath+"jug01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line", src: imgpath+"yellow_line.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug1", src: imgpath+"jug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "whiteline", src: imgpath+"white_line.png", type: createjs.AbstractLoader.IMAGE},


			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s2_p7", src: soundAsset+"s2_p7.ogg"},
			{id: "s2_p8", src: soundAsset+"s2_p8.ogg"},
			{id: "s2_p9", src: soundAsset+"s2_p9.ogg"},
			{id: "s2_p10", src: soundAsset+"s2_p10.ogg"},
			{id: "s2_p11", src: soundAsset+"s2_p11.ogg"},
			{id: "s2_p12", src: soundAsset+"s2_p12.ogg"},
			{id: "s2_p13", src: soundAsset+"s2_p13.ogg"},
			{id: "s2_p14", src: soundAsset+"s2_p14.ogg"},
			{id: "s2_p15", src: soundAsset+"s2_p15.ogg"},
			{id: "s2_p16", src: soundAsset+"s2_p16.ogg"},
			{id: "s2_p17", src: soundAsset+"s2_p17.ogg"},
			{id: "s2_p18", src: soundAsset+"s2_p18.ogg"},
			{id: "s2_p19", src: soundAsset+"s2_p19.ogg"},
			{id: "s2_p19", src: soundAsset+"s2_p19.ogg"},
			{id: "s2_p20", src: soundAsset+"s2_p20.ogg"},
			{id: "s2_p21", src: soundAsset+"s2_p21.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		$(".text-1").hide(0);
		switch(countNext) {
			case 15:
				sound_nav("s2_p"+(countNext+1));
			$(".hint-box")
				.mouseenter(function(){
				$(".hidden-box").show(0);
			})
				.mouseleave(function(){
				$(".hidden-box").hide(0);
		});
		case 17:
		case 18:
		$(".text-1").delay(3000).show(0);
			sound_nav("s2_p"+(countNext+1));
		break;
		default:
			sound_nav("s2_p"+(countNext+1));
		break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
	if(content[count].hasOwnProperty('speechbox')){
		var speechbox = content[count].speechbox;
		for(var i=0; i<speechbox.length; i++){
			var image_src = preload.getResult(speechbox[i].imgid).src;
			console.log(image_src);
			var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
			$(selector).attr('src', image_src);
		}
	}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

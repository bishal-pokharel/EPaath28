var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',

		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "diy-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg-diy',
				imgclass:'background'
			}]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.p3text1,
			textclass: "eins-1",
		},{
			textdata: data.string.p5text8,
			textclass: "practice-more",
		}],
		imagetextblockadditionalclass: 'option-block-1',
		imagetextblock: [
			{
				imagediv: 'option-item-1 opt-1',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			},
			{
				imagediv: 'option-item-1 opt-2',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			},
			{
				imagediv: 'option-item-1 opt-3',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			},
			{
				imagediv: 'option-item-1 opt-4',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			},
			{
				imagediv: 'option-item-1 opt-5',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			}
		],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"bg_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-3", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-2", src: imgpath+"green_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-4", src: imgpath+"red_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-1", src: imgpath+"orange_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-5", src: imgpath+"pink_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-3", src: imgpath+"balloon_blue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-2", src: imgpath+"balloon_green.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-4", src: imgpath+"balloon_red.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-1", src: imgpath+"balloon_orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-5", src: imgpath+"balloon_pink.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-diy", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},


			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		$nextBtn.hide(0);
		switch(countNext) {
			case 0:
			play_diy_audio(0);
			setTimeout(function(){
				$nextBtn.show(0);
			},2000);
			break;
			case 1:
				sound_player("s3_p2");
				var wrongClicked = 0;
				var rightClicked = 0;
				var numCorrect = 0;
				var questions = [
					['20 > 20', false],
					['20 = 20', true],
					['3 ≠ 3', false],
					['3 < 9', true],
					['-1 < 10', true],
					['10 > 1', true],
					['10 + 2 ≠ 12', false]
				];
				questions.shufflearray();
				$('.bg-1').css({
					'background-image': 'url('+preload.getResult('bg-1').src+')'
				});
				$('.opt-1>img').attr('src', preload.getResult('balloon-1').src);
				$('.opt-2>img').attr('src', preload.getResult('balloon-2').src);
				$('.opt-3>img').attr('src', preload.getResult('balloon-3').src);
				$('.opt-4>img').attr('src', preload.getResult('balloon-4').src);
				$('.opt-5>img').attr('src', preload.getResult('balloon-5').src);

				for(var i=0; i<5; i++){
					if(questions[i][1]){
						$('.opt-'+(i+1)).addClass('correct-option');
						numCorrect++;
					}
					$('.opt-'+(i+1)+'>p').text(questions[i][0]);
				}
				$(".option-item-1").click(function(){
					createjs.Sound.stop();
					if($(this).hasClass("correct-option")){
						rightClicked++;
						console.log(rightClicked);
						console.log(numCorrect);
						if(rightClicked==numCorrect){
							$(".option-item-1").css('pointer-events','none');
							nav_button_controls(0);
							$('.practice-more').show(0);
						}
						$(this).css('pointer-events','none');
			 			play_correct_incorrect_sound(1);
						$(this).addClass('go-up');
					}
					else{
			 			play_correct_incorrect_sound(0);
						$(this).css('pointer-events','none');
						var balloonClass = $(this).attr('class');
						var balloonIdx = balloonClass.substring((balloonClass.length - 1));
						$(this).children('img').attr('src', preload.getResult('pop-'+balloonIdx).src);
						$(this).children('p').hide(0);
						$(this).children('img').addClass('go-down');
					}
				});
				$('.practice-more').click(function(){
					countNext=1;
					templateCaller();
				});
				break;
			default:
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

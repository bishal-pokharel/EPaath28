var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p8text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p8text2,
			textclass: "s1-text-1",
		}],
		lowertextblockadditionalclass: 'bottom-text-dark',
		lowertextblock:[{
			textdata: data.string.p8text5,
			textclass: "",
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold',
		}],
		drawblock: [{
			drawblock: 'numline-block numline-3',
			markcontainerclass: 'mark-container-2'
		},{
			drawblock: 'numline-block numline-4',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p8text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p8text6,
			textclass: "s1-text-1",
		}],
		lowertextblockadditionalclass: 'bottom-text-dark',
		lowertextblock:[{
			textdata: data.string.p8text7,
			textclass: "",
		},{
			textdata: data.string.p8text8,
			textclass: "",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-3',
			markcontainerclass: 'mark-container-2'
		},{
			drawblock: 'numline-block numline-4',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p8text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p8text21,
			textclass: "s2-text-1",
		}],
		lowertextblockadditionalclass: 'bottom-text-light',
		lowertextblock:[{
			textdata: data.string.p8text9,
			textclass: "",
		},{
			textdata: data.string.p8text10,
			textclass: "",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-5',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p8text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p8text21,
			textclass: "s2-text-1",
		},{
			textdata: data.string.p8text21,
			textclass: "s2-text-2",
		}],
		lowertextblockadditionalclass: 'bottom-text-light',
		lowertextblock:[{
			textdata: data.string.p8text11,
			textclass: "ptext-1 ",
		},{
			textdata: data.string.p8text12,
			textclass: "ptext-2 its-hidden",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-5',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p8text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p8text13,
			textclass: "s2-text-3",
		},{
			textdata: data.string.p8text14,
			textclass: "s2-text-4 fade-in-2",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-6',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p8text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p8text13,
			textclass: "s2-text-3",
		}],
		listblockadditionalclass:'bottom-texts',
		listclass: 'bottom-list',
		listblock:[{
			textdata: data.string.p8text15,
			textclass: "text-list-1 its-hidden",
		},{
			textdata: data.string.p8text16,
			textclass: "text-list-2 its-hidden",
		},{
			textdata: data.string.p8text17,
			textclass: "text-list-3 its-hidden",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-6',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p8text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p8text13,
			textclass: "s2-text-3",
		}],
		lowertextblockadditionalclass:'bottom-texts bottom-texts-2',
		lowertextblock:[{
			textdata: data.string.p8text18,
			textclass: "fade-in-1",
		},{
			textdata: data.string.p8text19,
			textclass: "fade-in-3",
		},{
			textdata: data.string.p8text20,
			textclass: "fade-in-5",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-6',
			markcontainerclass: 'mark-container-2'
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "arrow-left", src: imgpath+"arrow-left.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow-right", src: imgpath+"arrow-right.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s8_p1_1", src: soundAsset+"s8_p1_1.ogg"},
			{id: "s8_p1_2", src: soundAsset+"s8_p1_2.ogg"},
			{id: "s8_p2_1", src: soundAsset+"s8_p2_1.ogg"},
			{id: "s8_p2_2", src: soundAsset+"s8_p2_2.ogg"},
			{id: "s8_p1", src: soundAsset+"s8_p1.ogg"},
			{id: "s8_p3", src: soundAsset+"s8_p3.ogg"},
			{id: "s8_p6", src: soundAsset+"s8_p6.ogg"},
			{id: "s8_p4_1", src: soundAsset+"s8_p4_1.ogg"},
			{id: "s8_p4_2", src: soundAsset+"s8_p4_2.ogg"},
			{id: "s8_p5_1", src: soundAsset+"s8_p5_1.ogg"},
			{id: "s8_p5_2", src: soundAsset+"s8_p5_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
			case 1:
				sound_player_duo("s8_p"+(countNext+1)+"_1","s8_p"+(countNext+1)+"_2");
				// if(countNext==1) $prevBtn.show(0);
				initializeSVG();
				createSolution('numline-3', 4, true, true, data.string.p8text3, false);
				createSolution('numline-4', 10, false, false, data.string.p8text4,false);
				// nav_button_controls(1000);
				break;
			case 2:
				// $prevBtn.show(0);
				sound_player("s8_p3");
				initializeSVG();
				createSolution('numline-5', 4, true, true, data.string.p8text3, true);
				createSolution('numline-5', 10, false, false, data.string.p8text4, true);
				// nav_button_controls(1000);
				break;
			case 3:
				sound_player_duo("s8_p"+(countNext+1)+"_1","s8_p"+(countNext+1)+"_2");
				initializeSVG();
				createSolution('numline-5', 4, true, true, data.string.p8text3, true);
				createSolution('numline-5', 10, false, false, data.string.p8text4, true);
				//replace image by svg
				var lineL, lineR, pointL, pointR;
				var s = Snap('.roof-left');
				$('.roof-left>img').remove();
				var svg = Snap.load(preload.getResult('arrow-left').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					lineL  = Snap.select('#line-left');
					pointL = Snap.select("#point-left");
					lineL.attr('x2', 150);
					lineL.transform( 't182,0');
					pointL.transform( 't187,0 s0.5,1');
					$('.roof-left>svg').css({
						right: '0%',
						left: 'auto',
						width: '280%',
					});
				});
				var s1 = Snap('.roof-right');
				$('.roof-right>img').remove();
				var svg1= Snap.load(preload.getResult('arrow-right').src, function ( loadedFragment ) {
					s1.append(loadedFragment);
					lineR  = Snap.select('#line-right');
					pointR = Snap.select("#point-right");
					lineR.attr('x2', 130);
					pointR.transform( 't-177,0 s0.5,1');
					$('.roof-right>svg').css({
						width: '280%',
					});
					$('.ptext-2').fadeIn(1000, function(){
						combineArrow();
						$('.roof-right>p,.roof-left>p').addClass('fade-out-0');
						$('.s2-text-2').addClass('fade-in-1');
						// nav_button_controls(1000);
					});
				});
				function combineArrow(){
					lineR.animate({'x2': 332}, 500);
					pointR.animate({'x2': 332, 'transform' : 't0,0'}, 500, function(){
						pointR.animate({'transform' : 't50,0 s0.5,0'}, 500);
					});
					lineL.animate({'x2': 332}, 500);
					pointL.animate({'x2': 332, 'transform' : 't0,0'}, 500, function(){
						pointL.animate({'transform' : 't-50,0 s0.5,0'}, 500);
					});
					lineL.animate({'x2': 332, 'transform' : 't0,0'}, 500);
				}
				break;
			case 4:
				sound_player_duo("s8_p"+(countNext+1)+"_1","s8_p"+(countNext+1)+"_2");
				initializeSVG();
				createSolution2('numline-6', 4, true, 10, false);
				// nav_button_controls(2000);
				break;
			case 5:
				$prevBtn.show(0);
				sound_player('s8_p6');
				initializeSVG();
				createSolution2('numline-6', 4, true, 10, false);
				$('.text-list-1').fadeIn(1000, function(){
					$('.mark-filled-circle').addClass('disc-highlight');
					timeoutvar = setTimeout(function(){
						$('.text-list-2').fadeIn(2000, function(){
							$('.mark-hollow-circle').addClass('dot-highlight');
							timeoutvar = setTimeout(function(){
								$('.text-list-3').fadeIn(2000, function(){
									for(var i=4; i<10; i++){
										$('.label-'+i).addClass('label-highlight');
										nav_button_controls(2000);
									}
								});
							}, 2000);
						});
					}, 2000);
				});
				break;
			case 6:
				$prevBtn.show(0);
				initializeSVG();
				createSolution2('numline-6', 4, true, 10, false);
				nav_button_controls(6000);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function sound_player_duo(sound_id_1,sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id_1);
		current_sound.play();
		current_sound.on('complete', function(){
			sound_player(sound_id_2);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function initializeSVG(){
		var lineMidY = 45;
		for(var i=0; i<21; i++){
			var markElementTop = document.createElement("div");
			var markElementBottom = document.createElement("div");
			var leftPos = 2+i*4.8;

			markElementTop.className = 'mark-element top-mark-'+(i-10);
			markElementTop.style.left =  leftPos+'%';
			$('.mark-container').append(markElementTop);

			markElementBottom.className = 'mark-element bottom-mark-'+(i-10);
			markElementBottom.style.left =  leftPos+'%';
			$('.label-container').append(markElementBottom);

			var markLine = document.createElement("div");
			markLine.className = 'mark-line line-'+i;

			var markLabel = document.createElement("p");
			markLabel.textContent = i-10;
			markLabel.className = 'mark-label label-'+(i-10);
			if(i<10){
				markLabel.className +=' negative-number-label';
			} else if(i>10){
				markLabel.className +=' positive-number-label';
			} else{
				markLabel.className +=' zero-number-label';
			}

			$('.top-mark-'+(i-10)).append(markLine);
			$('.bottom-mark-'+(i-10)).append(markLabel);
		}
	}


	function createSlantedLine(direction, parent){
		var slantedLine = document.createElement("div");
		var angle = Math.atan($(parent).height()/$(parent).width());
		var height = 90/Math.sin(angle);
		slantedLine.className = 'slanted-line';
		slantedLine.style.height = height+'%';
		if(direction=='left'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle*-1 +'rad)';
		}
		if(direction=='right'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle +'rad)';
		}
		$(parent).append(slantedLine);
	}
	function createRoof(direction, parent, text, isHalf){
		var roof = document.createElement("div");
		if(isHalf){
			roof.className = 'roof-2';
		}else{
			roof.className = 'roof-1';
		}
		var roofText = document.createElement("p");
		roofText.textContent = text;
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.className += ' roof-left';
			roof.style.right = '100%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.className += ' roof-right';
			roof.style.left = '100%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(roofText);
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function createStraightLine(parent){
		var slantedLine = document.createElement("div");
		slantedLine.className = 'straight-line';
		$(parent).append(slantedLine);
	}
	function createRoof2(direction, parent, text, isHalf){
		var roof = document.createElement("div");
		if(isHalf){
			roof.className = 'roof-2';
		}else{
			roof.className = 'roof-1';
		}
		var roofText = document.createElement("p");
		roofText.textContent = text;
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.className += ' roof-left';
			roof.style.right = '50%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.className += ' roof-right';
			roof.style.left = '50%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(roofText);
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function createSolution(numline, number, isGreater, isEqual, text, isHalf){
		var dotCircle = document.createElement("div");
		var direction = 'left';
		if(isGreater) direction='right';
		if(isEqual){
			dotCircle.className = 'mark-filled-circle';
			createStraightLine('.'+numline+' .top-mark-'+number);
			createRoof2(direction, '.'+numline+' .top-mark-'+number, text, isHalf);
		}else{
			dotCircle.className = 'mark-hollow-circle';
			createSlantedLine(direction, '.'+numline+' .top-mark-'+number);
			createRoof(direction, '.'+numline+' .top-mark-'+number, text, isHalf);
		}
		$('.'+numline+' .top-mark-'+number).append(dotCircle);
	}
	function createSolution2(numline, lowLimit, isEqualLow, HighLimit, isEqualHigh){
		var dotCircleLow = document.createElement("div");
		var dotCircleHigh = document.createElement("div");
		var roof = document.createElement("div");
		roof.className = 'join-roof';
		var spaceUnit = 4.8;
		var slantedFactor1 = 0, slantedFactor2 = 0;
		if(!isEqualLow) slantedFactor1 = 0.5*spaceUnit;
		if(!isEqualHigh) slantedFactor2 = 0.5*spaceUnit;
		roof.style.width = (HighLimit-lowLimit)*spaceUnit-(slantedFactor1+slantedFactor2)+'%';
		roof.style.left = (lowLimit+10)*spaceUnit+slantedFactor1+2+'%';
		if(isEqualLow){
			dotCircleLow.className = 'mark-filled-circle';
			createStraightLine('.'+numline+' .top-mark-'+lowLimit);
		}else{
			dotCircleLow.className = 'mark-hollow-circle';
			createSlantedLine('right', '.'+numline+' .top-mark-'+lowLimit);
		}
		if(isEqualHigh){
			dotCircleHigh.className = 'mark-filled-circle';
			createStraightLine('.'+numline+' .top-mark-'+HighLimit);
		}else{
			dotCircleHigh.className = 'mark-hollow-circle';
			createSlantedLine('left', '.'+numline+' .top-mark-'+HighLimit);
		}
		$('.'+numline+' .mark-container').append(roof);
		$('.'+numline+' .top-mark-'+lowLimit).append(dotCircleLow);
		$('.'+numline+' .top-mark-'+HighLimit).append(dotCircleHigh);
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

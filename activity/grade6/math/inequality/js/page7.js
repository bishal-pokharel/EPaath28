var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'instruction-1',
		uppertextblock:[{
			textdata: data.string.p7text1,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: data.string.p7text2,
			divclass: "drag-item drag-1",
		},{
			textdata: data.string.p7text3,
			divclass: "drag-item drag-4",
		},{
			textdata: data.string.p7text4,
			divclass: "drag-item drag-2",
		},{
			textdata: data.string.p7text5,
			divclass: "drag-item drag-3",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container container-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container container-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container container-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container container-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-4',
			hasCorrectIncorrect: true
		}]
	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'instruction-1',
		uppertextblock:[{
			textdata: data.string.p7text1,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: '',
			divclass: "drag-item drag-1",
		},{
			textdata: '',
			divclass: "drag-item drag-2",
		},{
			textdata: '',
			divclass: "drag-item drag-3",
		},{
			textdata: '',
			divclass: "drag-item drag-4",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-4',
			hasCorrectIncorrect: true
		}]
	},
	//slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'instruction-1 instruction-small',
		uppertextblock:[{
			textdata: data.string.p7text6,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p7text7,
			textclass: "practice-more",
		}],
		drawblockwrapper:'dd-wrapper wrapper-2',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container solution-box drop-1'
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container solution-box drop-2'
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container solution-box drop-3'
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container solution-box drop-4'
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	//for questions
	var randomInt = 0, TopPositions, BotPositions;

	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "arrow-left", src: imgpath+"arrow-left.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow-right", src: imgpath+"arrow-right.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s7_p1", src: soundAsset+"s7_p1.ogg"},
			{id: "s7_p3", src: soundAsset+"s7_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		$('.correct-icon').attr('src', preload.getResult('correct').src);

		switch(countNext) {
			case 0:
				sound_player("s7_p1");
				var positions = [1,2,3,4];
				positions.shufflearray();
				for(var i=1; i<5; i++){
					$('.drag-'+i).addClass('drag-pos-'+positions[i-1]);
				}
				initializeSVG();
				createSolution('container-1', 4, true, true, data.string.p5text36);
				createSolution('container-2', -4, true, false, data.string.p5text36);
				createSolution('container-3', -1, false, false, data.string.p5text36);
				createSolution('container-4', 1, true, true, data.string.p5text36);
				var itemCount = 0;
				$('.drag-item').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.drop-1').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-1');
					}
				});
				$('.drop-2').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-2');
					}
				});
				$('.drop-3').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-3');
					}
				});
				$('.drop-4').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-4');
					}
				});
				function dropper(dropped, droppedOn, hasclass){
					if(dropped.hasClass(hasclass)){
						dropped.draggable('option', 'revert', false);
						play_correct_incorrect_sound(1);
						itemCount++;
						dropped.draggable('disable');
						dropped.detach().css({
							'height': '100%',
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'border': '0.5vmin solid #cdeb58',
							'background-color': '#89d457',
							'pointer-events': 'none'
						}).appendTo(droppedOn);
						$(droppedOn).css({
							'border': 'none'
						});
						$(droppedOn).children('img').show(0);
						if(itemCount>3){
							$nextBtn.show(0);
						}
					} else{
						play_correct_incorrect_sound(0);
					}
				}
				break;
			case 1:
				randomInt = Math.round(Math.random()*6);
				if(Math.random()>0.5) randomInt *= -1;

				TopPositions = [1,2,3,4];
				TopPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.drag-'+i).addClass('drag-pos-'+TopPositions[i-1]);
				}

				$('.drag-1>p').html('x ≥ '+randomInt);
				$('.drag-2>p').html('x > '+randomInt);
				$('.drag-3>p').html('x ≤ '+randomInt);
				$('.drag-4>p').html('x < '+randomInt);

				BotPositions = [1,2,3,4];
				BotPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.ddc-'+i).addClass('container-'+BotPositions[i-1]);
				}

				initializeSVG();
				createSolution('ddc-1', randomInt, true, true, data.string.p5text36);
				createSolution('ddc-2', randomInt, true, false, data.string.p5text36);
				createSolution('ddc-3', randomInt, false, true, data.string.p5text36);
				createSolution('ddc-4', randomInt, false, false, data.string.p5text36);
				var itemCount = 0;
				$('.drag-item').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.drop-1').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-1');
					}
				});
				$('.drop-2').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-2');
					}
				});
				$('.drop-3').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-3');
					}
				});
				$('.drop-4').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-4');
					}
				});
				function dropper(dropped, droppedOn, hasclass){
					if(dropped.hasClass(hasclass)){
						dropped.draggable('option', 'revert', false);
						play_correct_incorrect_sound(1);
						itemCount++;
						dropped.draggable('disable');
						dropped.detach().css({
							'height': '100%',
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'border': '0.5vmin solid #cdeb58',
							'background-color': '#89d457',
							'pointer-events': 'none'
						}).appendTo(droppedOn);
						$(droppedOn).css({
							'border': 'none'
						});
						$(droppedOn).children('img').show(0);
						if(itemCount>3){
							$nextBtn.show(0);
						}
					} else{
						play_correct_incorrect_sound(0);
					}
				}
				break;
			case 2:
				sound_player("s7_p3");
				var solnText = document.createElement("p");
				$('.solution-box').append(solnText);
				$('.drop-1>p').html('x ≥ '+randomInt);
				$('.drop-2>p').html('x > '+randomInt);
				$('.drop-3>p').html('x ≤ '+randomInt);
				$('.drop-4>p').html('x < '+randomInt);

				for(var i=1; i<5; i++){
					$('.ddc-'+i).addClass('container-'+BotPositions[i-1]);
				}

				initializeSVG();
				createSolution('ddc-1', randomInt, true, true, data.string.p5text36);
				createSolution('ddc-2', randomInt, true, false, data.string.p5text36);
				createSolution('ddc-3', randomInt, false, true, data.string.p5text36);
				createSolution('ddc-4', randomInt, false, false, data.string.p5text36);
				$('.practice-more').click(function(){
					countNext--;
					templateCaller();
				});
			default:
				$prevBtn.show(0);
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function initializeSVG(){
		var lineMidY = 45;
		for(var i=0; i<21; i++){
			var markElementTop = document.createElement("div");
			var markElementBottom = document.createElement("div");
			var leftPos = 2+i*4.8;

			markElementTop.className = 'mark-element top-mark-'+(i-10);
			markElementTop.style.left =  leftPos+'%';
			$('.dd-mark-container').append(markElementTop);

			markElementBottom.className = 'mark-element bottom-mark-'+(i-10);
			markElementBottom.style.left =  leftPos+'%';
			$('.dd-label-container').append(markElementBottom);

			var markLine = document.createElement("div");
			markLine.className = 'mark-line line-'+i;

			var markLabel = document.createElement("p");
			markLabel.textContent = i-10;
			markLabel.className = 'mark-label label-'+(i-10);
			if(i<10){
				markLabel.className +=' negative-number-label';
			} else if(i>10){
				markLabel.className +=' positive-number-label';
			} else{
				markLabel.className +=' zero-number-label';
			}

			$('.top-mark-'+(i-10)).append(markLine);
			$('.bottom-mark-'+(i-10)).append(markLabel);
		}
	}


	function createSlantedLine(direction, parent){
		var slantedLine = document.createElement("div");
		var angle = Math.atan($(parent).height()/$(parent).width())/2;
		var height = 112/Math.sin(angle);
		slantedLine.className = 'slanted-line';
		slantedLine.style.height = height+'%';
		angle = angle/3;
		if(direction=='left'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle*-1 +'rad)';
		}
		if(direction=='right'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle +'rad)';
		}
		$(parent).append(slantedLine);
	}
	function createRoof(direction, parent){
		var roof = document.createElement("div");
		roof.className = 'roof-1';
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.style.right = '100%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.style.left = '100%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function createStraightLine(parent){
		var slantedLine = document.createElement("div");
		slantedLine.className = 'straight-line';
		$(parent).append(slantedLine);
	}
	function createRoof2(direction, parent){
		var roof = document.createElement("div");
		roof.className = 'roof-1';
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.style.right = '50%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.style.left = '50%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function placeDropBox(direction, isEqual, parent, number){
		var someOffset = 0;
		if(isEqual) someOffset = -2.5;
		if(direction=='right'){
			$(parent+ ' .droppable-container').css({
				'left': 5+someOffset+(10+number)*4.4+'%'
			});
		}else{
			$(parent+ ' .droppable-container').css({
				'right': 5+someOffset+(10-number)*4.4+'%'
			});
		}
	}
	function createSolution(numline, number, isGreater, isEqual, text){
		var dotCircle = document.createElement("div");
		var direction = 'left';
		if(isGreater) direction='right';
		if(isEqual){
			dotCircle.className = 'mark-filled-circle';
			createStraightLine('.'+numline+' .top-mark-'+number);
			createRoof2(direction, '.'+numline+' .top-mark-'+number, text);
			placeDropBox(direction, isEqual, '.'+numline, number);
		}else{
			dotCircle.className = 'mark-hollow-circle';
			createSlantedLine(direction, '.'+numline+' .top-mark-'+number);
			createRoof(direction, '.'+numline+' .top-mark-'+number, text);
			placeDropBox(direction, isEqual, '.'+numline, number);
		}
		$('.'+numline+' .top-mark-'+number).append(dotCircle);
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',

		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "diy-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg-diy',
				imgclass:'background'
			}]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'eins-2',
		uppertextblock:[{
			textdata: data.string.p5text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "eques-2",
		}],
		lowertextblockadditionalclass: 'option-block-2',
		lowertextblock:[{
			textdata: '',
			textclass: "option-item-2 opt-1",
		},{
			textdata: '',
			textclass: "option-item-2 opt-2",
		},{
			textdata: '',
			textclass: "option-item-2 opt-3",
		},{
			textdata: '',
			textclass: "option-item-2 opt-4",
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'eins-2',
		uppertextblock:[{
			textdata: data.string.p5text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "eques-2",
		}],
		lowertextblockadditionalclass: 'option-block-2',
		lowertextblock:[{
			textdata: '',
			textclass: "option-item-2 opt-1",
		},{
			textdata: '',
			textclass: "option-item-2 opt-2",
		},{
			textdata: '',
			textclass: "option-item-2 opt-3",
		},{
			textdata: '',
			textclass: "option-item-2 opt-4",
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'eins-2',
		uppertextblock:[{
			textdata: data.string.p5text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "eques-2",
		}],
		lowertextblockadditionalclass: 'option-block-2',
		lowertextblock:[{
			textdata: '',
			textclass: "option-item-2 opt-1",
		},{
			textdata: '',
			textclass: "option-item-2 opt-2",
		},{
			textdata: '',
			textclass: "option-item-2 opt-3",
		},{
			textdata: '',
			textclass: "option-item-2 opt-4",
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'summary',
		uppertextblock:[{
			textdata: data.string.p5text2,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p5text8,
			textclass: "practice-more",
		}],
		listblockadditionalclass: 'summary-listblock',
		listclass: 'summary-list',
		listblock:[{
			textclass1: "s1-item-1 si-1",
			textclass2: "s1-item-2 si-2",
			textclass3: "s1-item-3 si-3",
		},{
			textclass1: "s2-item-1 si-1",
			textclass2: "s2-item-2 si-2",
			textclass3: "s2-item-3 si-3",
		},{
			textclass1: "s3-item-1 si-1",
			textclass2: "s3-item-2 si-2",
			textclass3: "s3-item-3 si-3",
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
		var $pageEndBtn= $("#activity-page-continue-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var quesNum = ['Q1: ','Q2: ','Q3: '];
	//selecting 3 number arrays for question
	var arr1, arr2, arr3, allArray, textArray=[], solutionArray=[], choiceArray = [], signArray=[];
	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "fulcrum", src: imgpath+"fulcrum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-diy", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
			play_diy_audio();
			setTimeout(function(){
				$nextBtn.show(0);
			},2000);
			break;
			case 1:
			case 2:
			case 3:
			countNext==1?sound_player("s5_p"+(countNext+1)):'';
				//generate questions
				if(countNext==1){
					arr1 = generateNumArr();
					arr2 = generateNumArr();
					arr3 = generateNumArr();
					while(arraysEqual(arr1, arr2)){
						arr2 = generateNumArr();
					}
					while(arraysEqual(arr2, arr3) || arraysEqual(arr1, arr3)){
						arr3 = generateNumArr();
					}
					allArray = [arr1, arr2, arr3];
					console.log(allArray);
				}
				var arrIdx = countNext-1;
				//make one of them x
				var xChooser = Math.round(Math.random()*2);
				choiceArray.push(xChooser);
				allArray[arrIdx][xChooser] = 'x';
				//choose signs
				var sign1 = ' < ';
				var sign2 = ' < ';
				if(Math.random()>0.5) sign1 = ' <= ';
				if(Math.random()>0.5) sign2 = ' <= ';

				function swapSign(sign){
					var newString = sign;
					if(sign.includes('<')) newString = sign.replace('<', '>');
					if(sign.includes('>')) newString = sign.replace('>', '<');
					return newString;
				}
				function useThisSign(sign){
					var newString = sign;
					if(sign.includes('<=')) newString = sign.replace('<=', '\u2264');
					if(sign.includes('>=')) newString = sign.replace('>=', '\u2265');
					return newString;
				}
				signArray.push([sign1, sign2]);

				var questionText = allArray[arrIdx][0]+useThisSign(sign1)+allArray[arrIdx][1]+data.string.and+allArray[arrIdx][0]+useThisSign(sign2)+allArray[arrIdx][2];
				// var questionText = allArray[arrIdx][0]+useThisSign(sign1)+allArray[arrIdx][1]+'    and '+allArray[arrIdx][0]+useThisSign(sign2)+allArray[arrIdx][2];
				if(xChooser==1){
					questionText = allArray[arrIdx][1]+useThisSign(swapSign(sign1))+allArray[arrIdx][0]+data.string.and+allArray[arrIdx][1]+useThisSign(sign2)+allArray[arrIdx][2];
				} else if(xChooser==2){
					questionText = allArray[arrIdx][2]+useThisSign(swapSign(sign1))+allArray[arrIdx][0]+data.string.and+allArray[arrIdx][2]+useThisSign(swapSign(sign2))+allArray[arrIdx][1];
				}
				textArray.push(questionText);
				$('.eques-2').text(quesNum[countNext-1]+questionText);

				var correctAns = allArray[arrIdx][0]+useThisSign(sign1)+allArray[arrIdx][1]+useThisSign(sign2)+allArray[arrIdx][2];
				solutionArray.push(correctAns);

				var opt1 = allArray[arrIdx][2]+useThisSign(sign1)+allArray[arrIdx][1]+useThisSign(sign2)+allArray[arrIdx][0];
				var opt2 = allArray[arrIdx][0]+useThisSign(sign1)+allArray[arrIdx][2]+useThisSign(sign2)+allArray[arrIdx][1];
				var opt3 = allArray[arrIdx][0]+useThisSign(swapSign(sign1))+allArray[arrIdx][1]+useThisSign(swapSign(sign2))+allArray[arrIdx][2];
				var options = [[correctAns,true],[opt1,false],[opt2,false],[opt3,false]];
				options.shufflearray();
				var optNumber = ['a) ','b) ','c) ','d) ','e) '];
				for(var i=0; i<4; i++){
					if(options[i][1]) $('.opt-'+(i+1)).addClass('correct-option');
					$('.opt-'+(i+1)).text(optNumber[i]+options[i][0]);
				}
				$(".option-item-2").click(function(){
					createjs.Sound.stop();
					if($(this).hasClass("correct-option")){
						$(this).siblings(".corctopt").show(0);
			 			play_correct_incorrect_sound(1);
						$(this).parent().addClass('correct-ans');
						$(".opns").css('pointer-events','none');
				nav_button_controls(0);
					}
					else{
						$(this).siblings(".wrngopt").show(0);
			 			play_correct_incorrect_sound(0);
						$(this).parent().addClass('incorrect-ans');
					}
				});
				break;
			case 4:
				sound_player("s5_p"+(countNext+1));
				for(var i=1;i<4;i++){
					$('.s'+i+'-item-1').text(textArray[i-1]);
					$('.s'+i+'-item-2').text(generateExplanation(i-1));
					$('.s'+i+'-item-3').text(data.string.p5text9+solutionArray[i-1]);
				}
				nav_button_controls(1000);
				$('.practice-more').click(function(){
					countNext=0;
					templateCaller();
					$pageEndBtn.css("display","none");
				});
				function generateExplanation(index){
									if($lang=="np"){
											var xIs = data.string.p5text6;
											var signText1 = decodeSignText(signArray[index][0]);
											var signText2 = decodeSignText(signArray[index][1]);
											var explanation = xIs+" " + allArray[index][1] + signText1 + ' ' +data.string.chha + data.string.p5text10+ allArray[index][2] + signText2 + ' ' +data.string.chha1;
											if(choiceArray[index]==1){
												signText1 = decodeSignText(swapSign(signArray[index][0]));
												explanation = xIs+" " + allArray[index][0]+ signText1 + ' ' + data.string.chha + data.string.p5text10 + allArray[index][2] + signText2 + ' ' +data.string.chha1;
											} else if(choiceArray[index]==2){
												signText1 = decodeSignText(swapSign(signArray[index][0]));
												signText2 = decodeSignText(swapSign(signArray[index][1]));
												explanation = xIs+" " + allArray[index][0] + signText1 + ' ' + data.string.chha + data.string.p5text10 + allArray[index][1] + signText2 + ' ' +data.string.chha1;
											}
											return explanation;
										}
										else{
											var xIs = data.string.p5text6;
											var signText1 = decodeSignText(signArray[index][0]);
											var signText2 = decodeSignText(signArray[index][1]);
											var explanation = xIs + signText1 + allArray[index][1]+data.string.p5text10 + signText2 + allArray[index][2];
											if(choiceArray[index]==1){
												signText1 = decodeSignText(swapSign(signArray[index][0]));
												explanation = xIs + signText1 + allArray[index][0]+data.string.p5text11 + signText2 + allArray[index][2];
											} else if(choiceArray[index]==2){
												signText1 = decodeSignText(swapSign(signArray[index][0]));
												signText2 = decodeSignText(swapSign(signArray[index][1]));
												explanation = xIs + signText1 + allArray[index][0]+data.string.p5text10 + signText2 + allArray[index][1];
											}
											return explanation;
										}
							}
				break;
			default:
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function generateNumArr(){
		var numArray = [];
		for(var i=-10; i<=10; i++){
			numArray.push(i);
		}
		numArray.shufflearray();
		//select 3 numbers
		var selNumArray = numArray.slice(0, 3);
		selNumArray.sort(sortNumber);
		return selNumArray;
	}
	function arraysEqual(arr1, arr2) {
		if (arr1.length !== arr2.length) return false;
		for (var i = arr1.length; i--; ){
			if (arr1[i] !== arr2[i]) return false;
		}
		return true;
	}

	function sortNumber(a, b) {
		return a - b;
	}

	function decodeSignText(sign){
		var solutionText = '';
		switch(sign){
			case ' < ':
				solutionText = data.string.p5text4+' ';
				break;
			case ' <= ':
				solutionText = data.string.p5text4+' '+data.string.p5text5+' ';
				break;
			case ' > ':
				solutionText = data.string.p5text3+' ';
				break;
			case ' >= ':
				solutionText = data.string.p5text3+' '+data.string.p5text5+' ';
				break;
		}
		return solutionText;
	}
	if($lang=="np"){
		function decodeSignText(sign){
			var solutionText = '';
			switch(sign){
				case ' < ':
					solutionText = ' '+ data.string.p5text4;
					break;
				case ' <= ':
					solutionText = ' '+data.string.p5text4+' '+data.string.p5text5;
					break;
				case ' > ':
					solutionText = ' '+data.string.p5text3;
					break;
				case ' >= ':
					solutionText =' '+ data.string.p5text3+' '+data.string.p5text5;
					break;
			}
			return solutionText;
		}
	}
	function templateCaller() {
		console.log("Hello");
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page,countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					// templateCaller();
				});
			}
	*/


	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		if(countNext>1) countNext=1;
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

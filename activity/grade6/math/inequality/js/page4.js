var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',

		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "cen-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg',
				imgclass:'background'
			}]
		}]
	},{
		//slide 1
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[{
					textdata: data.string.p4text2,
					textclass: "text",
				}]
	},{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[{
					textdata: data.string.p4text3,
					textclass: "top-text-1",
				}],
				listblockadditionalclass:'bottom-texts',
				titledata: data.string.p4text4,
				titleclass:'title',
				datahighlightflag : true,
				datahighlightcustomclass : 'small-text',
				listclass: 'bottom-list',
				listblock:[{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlighttext',
					textdata: data.string.p4text5,
					textclass: "text-list-1 text-hidden",
				},{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlight-text',
					textdata: data.string.p4text6,
					textclass: "text-list-2 text-hidden",
				},{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlighttext',
					textdata: data.string.p4text7,
					textclass: "text-list-3 text-hidden",
				}	,{
					textdata: data.string.p4text8,
					textclass: "text-list-4 text-hidden",
				}],
				imageblock:[{
					imagestoshow:[{
						imgid:'mohan',
						imgclass:'boy'
					},{
						imgid:'marble',
						imgclass:'marble'
					}]
				}]
	},{
		//slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[{
					textdata: data.string.p4text9,
					textclass: "text-1",
				}],
				listblockadditionalclass:'box-texts',
				listclass: 'text-list',
				listblock:[{
					textdata: data.string.p4text10,
					textclass: "list-1",
				},{
					textdata: data.string.p4text11,
					textclass: "list-2",
				},{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlight-text',
					textdata: data.string.p4text12,
					textclass: "list-3",
				}],
				imageblock:[{
					imagestoshow:[{
						imgid:'mohan',
						imgclass:'boy'
					},{
						imgid:'marble',
						imgclass:'marble'
					}]
				}]
	},{
		//slide 4
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[{
					textdata: data.string.p4text9,
					textclass: "text-1",
				}],
				listblockadditionalclass:'box-texts',
				listclass: 'text-list',
				listblock:[{
					textdata: data.string.p4text13,
					textclass: "list-1",
				},{
					textdata: data.string.p4text14,
					textclass: "list-2",
				},{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlight-text',
					textdata: data.string.p4text15,
					textclass: "list-3",
				},{
					textdata: data.string.p4text16,
					textclass: "list-4",
				},{
					textdata: data.string.p4text17,
					textclass: "list-5",
				}],
				imageblock:[{
					imagestoshow:[{
						imgid:'mohan',
						imgclass:'boy'
					},{
						imgid:'marble',
						imgclass:'marble'
					}]
				}]
	},{
		//slide 5
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		listblockadditionalclass:'que-box',
		listclass: 'que-list',
		listblock:[{
			textdata: data.string.p4text18,
			textclass: "que-list-1",
		},{
			datahighlightflag : true,
				datahighlightcustomclass : 'highlight',
			textdata: data.string.p4text19,
			textclass: "que-list-2",
		},{
			textdata: data.string.p4text20,
			textclass: "que-list-3",
		},{
			textdata: data.string.p4text25,
					textclass: "bottom-text newcss",
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1 correct",
				optaddclass:"",
				optdata:data.string.p4text21
			},{
					optcontainerextra:"opti opti2 ",
				optaddclass:"",
				optdata:data.string.p4text22
			},{
				optcontainerextra:"opti opti3",
				optaddclass:"",
				optdata:data.string.p4text23
			},{
					optcontainerextra:"opti opti4",
				optaddclass:"",
				optdata:data.string.p4text24
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mohan',
				imgclass:'boy'
			},{
				imgid:'marble',
				imgclass:'marble'
			}]
		}]
	},{
		//slide 6
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[{
					textdata: data.string.p4text26,
					textclass: "title-text",
				}],
				listblockadditionalclass:'box-texts',
				listclass: 'text-list',
				listblock:[{
					textdata: data.string.p4text27,
					textclass: "list-a",
				},{
					textdata: data.string.p4text28,
					textclass: "list-a list-b",
				},{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlight-text',
					textdata: data.string.p4text29,
					textclass: "list-c",
				},{
					datahighlightflag : true,
					datahighlightcustomclass : 'highlight-text',
					textdata: data.string.p4text30,
					textclass: "list-d",
				}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "fulcrum", src: imgpath+"fulcrum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mohan", src: imgpath+"mohan01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble", src: imgpath+"six_marble.png", type: createjs.AbstractLoader.IMAGE},




			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
			{id: "s4_p3", src: soundAsset+"s4_p3_1.ogg"},
			{id: "s4_p3_2", src: soundAsset+"s4_p3_2.ogg"},
			{id: "s4_p4", src: soundAsset+"s4_p4_1.ogg"},
			{id: "s4_p4_2", src: soundAsset+"s4_p4_2.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p6", src: soundAsset+"s4_p6.ogg"},
			{id: "s4_p7", src: soundAsset+"s4_p7_1.ogg"},
			{id: "s4_p6_feedback", src: soundAsset+"s4_p6_feedback.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		// random number generator
			function rand_generator(limit){
				var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
				return randNum;
			}

			/*for randomizing the options*/
			function randomize(parent){
				var parent = $(parent);
				var divs = parent.children();
				while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
			}

		$prevBtn.hide(0);
		$nextBtn.hide(0);
		switch(countNext) {
			case 0:
			case 1:
				sound_nav("s4_p"+(countNext+1));
			break;
			case 2:
			$(".top-text-1").show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s4_p3");
				current_sound.play();
				current_sound.on("complete", function(){
					$(".bottom-texts,.boy,.marble").show(0);
					$(".text-list-1").show(0);
					$(".text-list-2").show(0);
					$(".text-list-3").show(0);
					$(".text-list-4").show(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s4_p3_2");
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
					});
				});
				break;
				case 3:
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s4_p4");
					current_sound.play();
					current_sound.on("complete", function(){
						$(".box-texts,.boy,.marble").show(0);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s4_p4_2");
						current_sound.play();
						current_sound.on("complete", function(){
							nav_button_controls(0);
						});
					});
				break;
				case 4:
					sound_nav("s4_p"+(countNext+1));
					$(".box-texts,.boy,.marble").delay(6000).show(0);
				break;
				case 5:
					sound_player("s4_p"+(countNext+1));
					$(".boy,.marble").show(0);
				  randomize(".optionsdiv");
				break;
				case 6:
				$(".box-texts").show(0);
				$(".list-c").delay(2000).show(0);
				$(".list-d").delay(4000).show(0);
					sound_nav("s4_p"+(countNext+1));
						break;
			default:
				nav_button_controls(0);
				break;
		}


    $(".optionscontainer").click(function(){
		createjs.Sound.stop();
				// sound_nav('s4_p6_feedback');
		  			if($(this).hasClass("correct")){
		  				$(this).children(".corctopt").show(0);
						  play_correct_incorrect_sound(true);
						 setTimeout(function(){
							sound_nav('s4_p6_feedback');
						 },1500);
								// $(this).addClass('corrects');
		  				$(" .optionscontainer").css("pointer-events","none");
		  					nav_button_controls(0);
								$(".bottom-text").show(0);

		  			}else{
		  				$(this).children(".wrngopt").show(0);
		  				play_correct_incorrect_sound(false);
							// $(this).addClass('incorrect');
		  				$(this).css("pointer-events","none");
					  }
					 
		  		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p6text2,
			textclass: "negative-number fade-in-0 its-hidden",
		},{
			textdata: data.string.p6text3,
			textclass: "positive-number fade-in-0 its-hidden",
		},{
			textdata: data.string.p6text9,
			textclass: "zero-number fade-in-0 its-hidden",
		},{
			textdata: data.string.p6text4,
			textclass: "line-caption-1",
		}],
		listblockadditionalclass:'prop-list its-hidden',
		titledata: data.string.p6text5,
		titleclass: "list-prop-0",
		listclass: 'list-1',
		listblock:[{
			textdata: data.string.p6text6,
			textclass: "list-prop-1 its-hidden",
		},{
			textdata: data.string.p6text7,
			textclass: "list-prop-2 its-hidden",
		},{
			textdata: data.string.p6text8,
			textclass: "list-prop-3 its-hidden",
		}],
		drawblock: [{
			drawblock: 'numline-block',
			mainlineclass: 'extra-long-line',
			labelcontainerclass: 'its-hidden'
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : " its-hidden arrow-red",
					imgsrc : '',
					imgid : 'arrow-red'
				},
				{
					imgclass : "its-hidden arrow-blue",
					imgsrc : '',
					imgid : 'arrow-blue'
				}
			]
		}],
		extratextblock:[{
			textdata: data.string.p6text10,
			textclass: "s2-text-1",
		},{
			textdata: data.string.p6text11,
			textclass: "less-than its-hidden",
		},{
			textdata: data.string.p6text12,
			textclass: "greater-than its-hidden",
		},{
			textdata: data.string.p6text2,
			textclass: "negative-number bottom-captions",
		},{
			textdata: data.string.p6text3,
			textclass: "positive-number bottom-captions",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-5',
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arrow-red",
					imgsrc : '',
					imgid : 'arrow-red'
				},
				{
					imgclass : "arrow-blue",
					imgsrc : '',
					imgid : 'arrow-blue'
				}
			]
		}],
		extratextblock:[{
			textdata: data.string.p6text13,
			textclass: "s3-text s3-text-1",
		}, {
			textdata: data.string.p6text14,
			textclass: "s3-text s3-text-2",
		}, {
			textdata: data.string.p6text15,
			textclass: "s3-text s3-text-3",
		}, {
			textdata: data.string.p6text16,
			textclass: "s3-text s3-text-4",
		}, {
			textdata: data.string.p6text17,
			textclass: "s3-text s3-text-5",
		}, {
			textdata: data.string.p6text18,
			textclass: "s3-text s3-text-6",
		},{
			textdata: data.string.p6text11,
			textclass: "less-than",
		},{
			textdata: data.string.p6text12,
			textclass: "greater-than",
		},{
			textdata: data.string.p6text2,
			textclass: "negative-number bottom-captions",
		},{
			textdata: data.string.p6text3,
			textclass: "positive-number bottom-captions",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-5',
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p6text19,
			textclass: "text-1",
		}],
		lowertextblockadditionalclass: 'bottom-texts',
		lowertextblock:[{
			textdata: data.string.p6text20,
			textclass: "",
		},{
			textdata: data.string.p6text21,
			textclass: "",
		},{
			textdata: data.string.p6text22,
			textclass: "",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-2',
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p6text19,
			textclass: "text-1",
		}],
		listblockadditionalclass:'bottom-texts font-andika',
		titledata: data.string.p6text23,
		titleclass:'bottom-big-title',
		datahighlightflag : true,
		datahighlightcustomclass : 'small-text',
		listclass: 'bottom-list',
		listblock:[{
			textdata: data.string.p6text24,
			textclass: "text-list-1 its-hidden",
		},{
			textdata: data.string.p6text25,
			textclass: "text-list-2 its-hidden",
		},{
			textdata: data.string.p6text26,
			textclass: "text-list-3 its-hidden",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-2',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p6text19,
			textclass: "text-1",
		}],
		lowertextblockadditionalclass:'bottom-texts bottom-text-2 font-andika',
		lowertextblock:[{
			textdata: data.string.p6text28,
			textclass: "",
		},{
			textdata: data.string.p6text29,
			textclass: "",
		},{
			textdata: data.string.p6text30,
			textclass: "",
		},{
			textdata: data.string.p6text31,
			textclass: "",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-2',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p6text19,
			textclass: "text-1",
		}],
		listblockadditionalclass:'bottom-texts bottom-texts-3 font-andika',
		titledata: data.string.p6text32,
		titleclass:'bottom-big-title',
		datahighlightflag : true,
		datahighlightcustomclass : 'small-text',
		listclass: 'bottom-list bottom-list-3',
		listblock:[{
			textdata: data.string.p6text33,
			textclass: "text-list-1 its-hidden",
			datahighlightflag : true,
			datahighlightcustomclass : 'text-list-1a its-hidden',
		},{
			textdata: data.string.p6text34,
			textclass: "text-list-2 its-hidden",
		},{
			textdata: data.string.p6text35,
			textclass: "text-list-3 its-hidden",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-2',
			markcontainerclass: 'mark-container-2'
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'top-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p6text37,
			textclass: "text-1",
		}],
		drawblock: [{
			drawblock: 'numline-block numline-3',
			markcontainerclass: 'mark-container-2'
		},{
			drawblock: 'numline-block numline-4',
			markcontainerclass: 'mark-container-2'
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null, timeoutvar1=null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "arrow-blue", src: imgpath+"blue_arrow.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow-red", src: imgpath+"red_arrow.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "arrow-left", src: imgpath+"arrow-left.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow-right", src: imgpath+"arrow-right.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s6_p1_1", src: soundAsset+"s6_p1_1.ogg"},
			{id: "s6_p1_2", src: soundAsset+"s6_p1_2.ogg"},
			{id: "s6_p1_3", src: soundAsset+"s6_p1_3.ogg"},
			{id: "s6_p1_4", src: soundAsset+"s6_p1_4.ogg"},
			{id: "s6_p1_5", src: soundAsset+"s6_p1_5.ogg"},
			{id: "s6_p2", src: soundAsset+"s6_p2.ogg"},
			{id: "s6_p3", src: soundAsset+"s6_p3.ogg"},
			{id: "s6_p4", src: soundAsset+"s6_p4.ogg"},
			{id: "s6_p5_1", src: soundAsset+"s6_p5_1.ogg"},
			{id: "s6_p5_2", src: soundAsset+"s6_p5_2.ogg"},
			{id: "s6_p5_3", src: soundAsset+"s6_p5_3.ogg"},
			{id: "s6_p5_4", src: soundAsset+"s6_p5_4.ogg"},
			{id: "s6_p6", src: soundAsset+"s6_p6.ogg"},
			{id: "s6_p7_1", src: soundAsset+"s6_p7_1.ogg"},
			{id: "s6_p7_2", src: soundAsset+"s6_p7_2.ogg"},
			{id: "s6_p7_3", src: soundAsset+"s6_p7_3.ogg"},
			{id: "s6_p7_4", src: soundAsset+"s6_p7_4.ogg"},
			{id: "s6_p8", src: soundAsset+"s6_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
				initializeSVG1();
				function initializeSVG1(){
					var lineMidY = 45;
					for(var i=0; i<201; i++){
						var markElementTop = document.createElement("div");
						var markElementBottom = document.createElement("div");
						var leftPos = i*.50;

						markElementTop.className = 'mark-element mark-'+i;
						markElementTop.style.left =  leftPos+'%';
						$('.mark-container').append(markElementTop);

						markElementBottom.className = 'mark-element mark-'+i;
						markElementBottom.style.left =  leftPos+'%';
						$('.label-container').append(markElementBottom);

						var markLine = document.createElement("div");
						markLine.className = 'mark-line line-'+i;

						var markLabel = document.createElement("p");
						markLabel.textContent = i-100;
						markLabel.className = 'mark-label label-'+i;
						if(i<100){
							markLabel.className +=' negative-number-label';
						} else if(i>100){
							markLabel.className +=' positive-number-label';
						} else{
							markLabel.className +=' zero-number-label';
						}

						markElementTop.append(markLine);
						markElementBottom.append(markLabel);
					}
					for(var i=0; i<201; i++){
						var newLeftPos = ((i*0.5)*10-449.5)*0.99;
						$('.mark-'+i).animate({
							left: newLeftPos+'%'
						}, 4000);
					}

						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s6_p1_1");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.label-container').fadeIn(1000);
								$('.prop-list').fadeIn(1000);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s6_p1_2");
							current_sound.play();
							current_sound.on('complete', function(){
								$('.list-prop-1').fadeIn(1000);
								$('.zero-number').show(0);
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s6_p1_3");
								current_sound.play();
								current_sound.on('complete', function(){
									 $('.list-prop-2').fadeIn(1000);
									 $('.positive-number').show(0);
									 $('.positive-number-label').addClass('label-highlight');
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("s6_p1_4");
									current_sound.play();
									current_sound.on('complete', function(){
										$('.list-prop-3').fadeIn(1000);
										$('.negative-number').show(0);
										$('.positive-number-label').removeClass('label-highlight');
										$('.negative-number-label').addClass('label-highlight');
										createjs.Sound.stop();
										current_sound = createjs.Sound.play("s6_p1_5");
										current_sound.play();
										current_sound.on('complete', function(){
											nav_button_controls(300);
										});
									});
							});
						});
					});
				}
				break;
			case 1:
				initializeSVG();
				sound_player("s6_p2");
				$('.arrow-red').fadeIn(1000, function(){
					$('.less-than').fadeIn(1000, function(){
						$('.arrow-blue').fadeIn(1000, function(){
							$('.greater-than').fadeIn(1000, function(){
								// nav_button_controls(0);
							});
						});
					});
				});
				break;
			case 2:
				sound_player("s6_p3");
				initializeSVG();
				$('.s3-text-2').fadeIn(1000, function(){
					$('.label-2, .label-3').addClass('number-highlight');
					timeoutvar=setTimeout(function(){
						$('.label-2, .label-3').removeClass('number-highlight');
						$('.s3-text-3').fadeIn(1000, function(){
							$('.label-3, .label-2').addClass('number-highlight');
						});
					}, 2000);
				});
				timeoutvar1=setTimeout(function(){
					$('.s3-text-4').fadeIn(1000, function(){
						$('.label--8, .label--7').addClass('number-highlight');
						timeoutvar1=setTimeout(function(){
							$('.label--8, .label--7').removeClass('number-highlight');
							$('.s3-text-5').fadeIn(1000, function(){
								$('.label--8, .label--7').addClass('number-highlight');
								timeoutvar1=setTimeout(function(){
									$('.s3-text-6').fadeIn(1000, function(){
										nav_button_controls(0);
									});
								}, 2000);
							});
						}, 2000);
					});
				}, 4500);
				break;
			case 3:
				initializeSVG();
				sound_player("s6_p4");
						// nav_button_controls(1000);
				break;
			case 4:
				initializeSVG();
				// sound_player("s6_p5");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s6_p5_1");
				current_sound.play();
				current_sound.on('complete', function(){
					$('.text-list-1').fadeIn(100);
					var hollowCircle = document.createElement("div");
					hollowCircle.className = 'mark-hollow-circle';
					$('.top-mark-10').append(hollowCircle);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s6_p5_2");
					current_sound.play();
					current_sound.on('complete', function(){
						$('.text-list-2').fadeIn(100);
						createSlantedLine('left', '.top-mark-10');
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s6_p5_3");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.text-list-3').fadeIn(100);
							createRoof('left', '.top-mark-10', data.string.p6text27);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s6_p5_4");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(300);
							});
						});
					});
				});
				break;
			case 5:
				initializeSVG();
				sound_player("s6_p6");
				var hollowCircle = document.createElement("div");
				hollowCircle.className = 'mark-hollow-circle';
				$('.top-mark-10').append(hollowCircle);
				createSlantedLine('left', '.top-mark-10');
				createRoof('left', '.top-mark-10', data.string.p6text27);
						// nav_button_controls(1000);
				break;
			case 6:
				initializeSVG();
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s6_p7_1");
				current_sound.play();
				current_sound.on('complete', function(){
				var hollowCircle = document.createElement("div");
				hollowCircle.className = 'mark-hollow-circle';
				$('.text-list-1').fadeIn(100);
					$('.top-mark-4').append(hollowCircle);
					$('.text-list-1a').fadeIn(100);
					hollowCircle.style.backgroundColor = '#000000';
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s6_p7_2");
					current_sound.play();
					current_sound.on('complete', function(){
						$('.text-list-2').fadeIn(100);
						createStraightLine('.top-mark-4');
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s6_p7_3");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.text-list-3').fadeIn(100);
							createRoof2('right', '.top-mark-4', data.string.p6text36);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s6_p7_4");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(300);
							});
						});
					});
				});
				break;
			case 7:
				initializeSVG();
				sound_player("s6_p8");
				createSolution('numline-3', 4, true, true, data.string.p6text36);
				createSolution('numline-4', 10, false, false, data.string.p6text27);
					// nav_button_controls(0);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(300);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function initializeSVG(){
		var lineMidY = 45;
		for(var i=0; i<21; i++){
			var markElementTop = document.createElement("div");
			var markElementBottom = document.createElement("div");
			var leftPos = 2+i*4.8;

			markElementTop.className = 'mark-element top-mark-'+(i-10);
			markElementTop.style.left =  leftPos+'%';
			$('.mark-container').append(markElementTop);

			markElementBottom.className = 'mark-element bottom-mark-'+(i-10);
			markElementBottom.style.left =  leftPos+'%';
			$('.label-container').append(markElementBottom);

			var markLine = document.createElement("div");
			markLine.className = 'mark-line line-'+i;

			var markLabel = document.createElement("p");
			markLabel.textContent = i-10;
			markLabel.className = 'mark-label label-'+(i-10);
			if(i<10){
				markLabel.className +=' negative-number-label';
			} else if(i>10){
				markLabel.className +=' positive-number-label';
			} else{
				markLabel.className +=' zero-number-label';
			}

			$('.top-mark-'+(i-10)).append(markLine);
			$('.bottom-mark-'+(i-10)).append(markLabel);
		}
	}


	function createSlantedLine(direction, parent){
		var slantedLine = document.createElement("div");
		var angle = Math.atan($(parent).height()/$(parent).width());
		var height = 90/Math.sin(angle);
		slantedLine.className = 'slanted-line';
		slantedLine.style.height = height+'%';
		if(direction=='left'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle*-1 +'rad)';
		}
		if(direction=='right'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle +'rad)';
		}
		$(parent).append(slantedLine);
	}
	function createRoof(direction, parent, text){
		var roof = document.createElement("div");
		roof.className = 'roof-1';
		var roofText = document.createElement("p");
		roofText.textContent = text;
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.style.right = '100%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.style.left = '100%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(roofText);
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function createStraightLine(parent){
		var slantedLine = document.createElement("div");
		slantedLine.className = 'straight-line';
		$(parent).append(slantedLine);
	}

	function createRoof2(direction, parent, text){
		var roof = document.createElement("div");
		roof.className = 'roof-1';
		var roofText = document.createElement("p");
		roofText.textContent = text;
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.style.right = '50%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.style.left = '50%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(roofText);
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function createSolution(numline, number, isGreater, isEqual, text){
		var dotCircle = document.createElement("div");
		var direction = 'left';
		if(isGreater) direction='right';
		if(isEqual){
			dotCircle.className = 'mark-filled-circle';
			createStraightLine('.'+numline+' .top-mark-'+number);
			createRoof2(direction, '.'+numline+' .top-mark-'+number, text);
		}else{
			dotCircle.className = 'mark-hollow-circle';
			createSlantedLine(direction, '.'+numline+' .top-mark-'+number);
			createRoof(direction, '.'+numline+' .top-mark-'+number, text);
		}
		$('.'+numline+' .top-mark-'+number).append(dotCircle);
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar1);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',

		extratextblock:[{
			textdata: data.string.p9text1,
			textclass: "diy-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg-diy',
				imgclass:'background'
			}]
		}]
	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'instruction-1',
		uppertextblock:[{
			textdata: data.string.p9text2,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: '',
			divclass: "drag-item drag-1",
		},{
			textdata: '',
			divclass: "drag-item drag-2",
		},{
			textdata: '',
			divclass: "drag-item drag-3",
		},{
			textdata: '',
			divclass: "drag-item drag-4",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-4',
			hasCorrectIncorrect: true
		}]
	},
	//slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'instruction-1 instruction-small',
		uppertextblock:[{
			textdata: data.string.p7text6,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p7text7,
			textclass: "practice-more",
		}],
		drawblockwrapper:'dd-wrapper wrapper-2',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container solution-box drop-1'
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container solution-box drop-2'
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container solution-box drop-3'
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container solution-box drop-4'
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	//for questions
	var question1, question2, question3, question4, TopPositions, BotPositions;

	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bg-diy", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s9_p1", src: soundAsset+"s9_p1.ogg"},
			{id: "s9_p2", src: soundAsset+"s9_p2.ogg"},
			{id: "s9_p3_1", src: soundAsset+"s9_p3_1.ogg"},
			{id: "s9_p3_2", src: soundAsset+"s9_p3_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		$('.correct-icon').attr('src', preload.getResult('correct').src);

		switch(countNext) {
			case 0:
			play_diy_audio(0);
			setTimeout(function(){
				$nextBtn.show(0);
			},2000);
			break;
			case 1:
				sound_player("s9_p2");
				question1 = randomPairGenerate();
				question2 = randomPairGenerate();
				question3 = randomPairGenerate();
				question4 = randomPairGenerate();
				while(arraysEqual(question1, question2)){
					question2 = randomPairGenerate();
				}
				while(arraysEqual(question3, question2)||arraysEqual(question3, question1)){
					question3 = randomPairGenerate();
				}
				while(arraysEqual(question4, question3)||arraysEqual(question4, question2)||arraysEqual(question4, question1)){
					question4 = randomPairGenerate();
				}
				function randomPairGenerate(){
					var randomInt1 = Math.round(Math.random()*10);
					var randomInt2 = Math.round(Math.random()*10);
					if(Math.random()>0.5) randomInt1 *= -1;
					while(randomInt2==randomInt1){
						randomInt2 = Math.round(Math.random()*10);
						if(Math.random()>0.5) randomInt2 *= -1;
					}
					if(randomInt1==10&&randomInt2>7){randomInt2=7;}
					if(randomInt1==-10&&randomInt2<-7){randomInt2=-7;}
					if(randomInt1<randomInt2){
						return [randomInt1, randomTrueFalse(), randomInt2, randomTrueFalse()];
					} else{
						return [randomInt2, randomTrueFalse(), randomInt1, randomTrueFalse()];
					}
				}
				function randomTrueFalse(){
					if(Math.random()>0.5) {
						return true;
					}else{
						return false;
					}
				}
				function arraysEqual(arr1, arr2) {
					if (arr1.length !== arr2.length) return false;
					for (var i = arr1.length; i--; ){
						if (arr1[i] !== arr2[i]) return false;
					}
					return true;
				}
				function questionTextGenerate(question){
					var firstSign = '<';
					var secondSign = '<';
					var string = '';
					if(question[1]) firstSign ='\u2264';
					if(question[3]) secondSign ='\u2264';
					string = question[0]+firstSign+'x'+secondSign+question[2];
					return string;
				}

				TopPositions = [1,2,3,4];
				TopPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.drag-'+i).addClass('drag-pos-'+TopPositions[i-1]);
				}

				$('.drag-1>p').text(questionTextGenerate(question1));
				$('.drag-2>p').text(questionTextGenerate(question2));
				$('.drag-3>p').text(questionTextGenerate(question3));
				$('.drag-4>p').text(questionTextGenerate(question4));

				BotPositions = [1,2,3,4];
				BotPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.ddc-'+i).addClass('container-'+BotPositions[i-1]);
				}

				initializeSVG();
				createSolution2('ddc-1', question1[0], question1[1], question1[2], question1[3]);
				createSolution2('ddc-2', question2[0], question2[1], question2[2], question2[3]);
				createSolution2('ddc-3', question3[0], question3[1], question3[2], question3[3]);
				createSolution2('ddc-4', question4[0], question4[1], question4[2], question4[3]);
				var itemCount = 0;
				$('.drag-item').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.drop-1').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-1');
					}
				});
				$('.drop-2').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-2');
					}
				});
				$('.drop-3').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-3');
					}
				});
				$('.drop-4').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-4');
					}
				});
				function dropper(dropped, droppedOn, hasclass){
					createjs.Sound.stop();
					if(dropped.hasClass(hasclass)){
						dropped.draggable('option', 'revert', false);
						play_correct_incorrect_sound(1);
						itemCount++;
						dropped.draggable('disable');
						dropped.detach().css({
							'height': '100%',
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'border': '0.5vmin solid #cdeb58',
							'background-color': '#89d457',
							'pointer-events': 'none'
						}).appendTo(droppedOn);
						$(droppedOn).css({
							'border': 'none'
						});
						$(droppedOn).children('img').show(0);
						if(itemCount>3){
							$nextBtn.show(0);
						}
					} else{
						play_correct_incorrect_sound(0);
					}
				}
				break;
			case 2:
				var solnText = document.createElement("p");
				$('.solution-box').append(solnText);
				$('.drop-1>p').text(questionTextGenerate(question1));
				$('.drop-2>p').text(questionTextGenerate(question2));
				$('.drop-3>p').text(questionTextGenerate(question3));
				$('.drop-4>p').text(questionTextGenerate(question4));

				BotPositions = [1,2,3,4];
				BotPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.ddc-'+i).addClass('container-'+BotPositions[i-1]);
				}

				for(var i=1; i<5; i++){
					$('.ddc-'+i).addClass('container-'+BotPositions[i-1]);
				}

				initializeSVG();
				createSolution2('ddc-1', question1[0], question1[1], question1[2], question1[3]);
				createSolution2('ddc-2', question2[0], question2[1], question2[2], question2[3]);
				createSolution2('ddc-3', question3[0], question3[1], question3[2], question3[3]);
				createSolution2('ddc-4', question4[0], question4[1], question4[2], question4[3]);

				$('.practice-more').click(function(){
					countNext--;
					templateCaller();
				});
					nav_button_controls(2000);
				break;
			default:
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function initializeSVG(){
		var lineMidY = 45;
		for(var i=0; i<21; i++){
			var markElementTop = document.createElement("div");
			var markElementBottom = document.createElement("div");
			var leftPos = 2+i*4.8;

			markElementTop.className = 'mark-element top-mark-'+(i-10);
			markElementTop.style.left =  leftPos+'%';
			$('.dd-mark-container').append(markElementTop);

			markElementBottom.className = 'mark-element bottom-mark-'+(i-10);
			markElementBottom.style.left =  leftPos+'%';
			$('.dd-label-container').append(markElementBottom);

			var markLine = document.createElement("div");
			markLine.className = 'mark-line line-'+i;

			var markLabel = document.createElement("p");
			markLabel.textContent = i-10;
			markLabel.className = 'mark-label label-'+(i-10);
			if(i<10){
				markLabel.className +=' negative-number-label';
			} else if(i>10){
				markLabel.className +=' positive-number-label';
			} else{
				markLabel.className +=' zero-number-label';
			}

			$('.top-mark-'+(i-10)).append(markLine);
			$('.bottom-mark-'+(i-10)).append(markLabel);
		}
	}


	function createSlantedLine(direction, parent){
		var slantedLine = document.createElement("div");
		var angle = Math.atan($(parent).height()/$(parent).width());
		var height = 185/Math.sin(angle);
		slantedLine.className = 'slanted-line';
		slantedLine.style.height = height+'%';
		angle /= 5;
		if(direction=='left'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle*-1 +'rad)';
		}
		if(direction=='right'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle +'rad)';
		}
		$(parent).append(slantedLine);
	}
	function createRoof(direction, parent, text, isHalf){
		var roof = document.createElement("div");
		if(isHalf){
			roof.className = 'roof-2';
		}else{
			roof.className = 'roof-1';
		}
		var roofText = document.createElement("p");
		roofText.textContent = text;
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.className += ' roof-left';
			roof.style.right = '100%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.className += ' roof-right';
			roof.style.left = '100%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(roofText);
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function createStraightLine(parent){
		var slantedLine = document.createElement("div");
		slantedLine.className = 'straight-line';
		$(parent).append(slantedLine);
	}
	function createRoof2(direction, parent, text, isHalf){
		var roof = document.createElement("div");
		if(isHalf){
			roof.className = 'roof-2';
		}else{
			roof.className = 'roof-1';
		}
		var roofText = document.createElement("p");
		roofText.textContent = text;
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.className += ' roof-left';
			roof.style.right = '50%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.className += ' roof-right';
			roof.style.left = '50%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(roofText);
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function createSolution2(numline, lowLimit, isEqualLow, HighLimit, isEqualHigh){
		var dotCircleLow = document.createElement("div");
		var dotCircleHigh = document.createElement("div");
		var roof = document.createElement("div");
		roof.className = 'join-roof';
		var spaceUnit = 4.8;
		var slantedFactor1 = 0, slantedFactor2 = 0;
		if(!isEqualLow) slantedFactor1 = 0.5*spaceUnit;
		if(!isEqualHigh) slantedFactor2 = 0.5*spaceUnit;
		var roofLeft = (lowLimit+10)*spaceUnit+slantedFactor1+2;
		var roofWidth = (HighLimit-lowLimit)*spaceUnit-(slantedFactor1+slantedFactor2);
		roof.style.width = roofWidth+'%';
		roof.style.left = roofLeft+'%';
		//for dropbox
		$('.'+numline+ ' .droppable-container').css({
			'left': (lowLimit+10)*spaceUnit+(HighLimit-lowLimit)*spaceUnit/2-2+'%'
		});
		if(isEqualLow){
			dotCircleLow.className = 'mark-filled-circle';
			createStraightLine('.'+numline+' .top-mark-'+lowLimit);
		}else{
			dotCircleLow.className = 'mark-hollow-circle';
			createSlantedLine('right', '.'+numline+' .top-mark-'+lowLimit);
		}
		if(isEqualHigh){
			dotCircleHigh.className = 'mark-filled-circle';
			createStraightLine('.'+numline+' .top-mark-'+HighLimit);
		}else{
			dotCircleHigh.className = 'mark-hollow-circle';
			createSlantedLine('left', '.'+numline+' .top-mark-'+HighLimit);
		}
		$('.'+numline+' .dd-mark-container').append(roof);
		$('.'+numline+' .top-mark-'+lowLimit).append(dotCircleLow);
		$('.'+numline+' .top-mark-'+HighLimit).append(dotCircleHigh);
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

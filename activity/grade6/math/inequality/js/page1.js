var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[{
	//slide 0
	contentnocenteradjust: true,
	contentblockadditionalclass: 'diy-bg',
	imageblock:[{
		imagestoshow:[{
			imgid:'coverpage',
			imgclass:'background'
		}]
	}],
	extratextblock:[{
		textdata: data.string.inequality,
		textclass: "cover-page",
	}]
},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',

		listblockadditionalclass:'bottom-texts font-andika',
		titledata: data.string.p1text1,
		titleclass:'bottom-big-title',
		datahighlightflag : true,
		datahighlightcustomclass : 'small-text',
		listclass: 'bottom-list',
		listblock:[{
			textdata: data.string.p1text2,
			textclass: "text-list-1 text-hidden",
		},{
			textdata: data.string.p1text3,
			textclass: "text-list-2 text-hidden",
		},{
			textdata: data.string.p1text4,
	 		textclass: "text-list-3 text-hidden",
		}	,{
			textdata: data.string.p1text5,
			textclass: "text-list-4 text-hidden",
		}],
		table:[

					{
						tablehead:[{

							textclass:"content",
							textdata:data.string.symbol
						},{
							textclass:"content",
							textdata:data.string.word
						},{
							textclass:"content",
							textdata:data.string.example
						}],
						firstrowdata	:[
									{
										datahighlightflag : true,
										datahighlightcustomclass : 'symbol',
											textclass:"first",
											textdata:data.string.isequal
									},
									{
											textclass:"first",
											textdata:data.string.p1text6
									},
									{
											textclass:"first",
											textdata:data.string.p1text10
									}
							],
							 secondrowdata:[
									{
										datahighlightflag : true,
										datahighlightcustomclass : 'symbol',
											textclass:"content",
											textdata:data.string.isnotequal
									},
									{
											textclass:"content",
											textdata:data.string.p1text7
									},
									{
											textclass:"content",
											textdata:data.string.p1text11 + data.string.notequal + data.string.n9
									}
							],
							thirdrowdata:[
									{
												datahighlightflag : true,
												datahighlightcustomclass : 'symbol',
											textclass:"content",
											textdata:data.string.islessthan
									},
									{
											textclass:"content",
											textdata:data.string.p1text8
									},
									{
											textclass:"content",
											textdata:data.string.p1text18
									}
							],
							fourrowdata:[
									{
										datahighlightflag : true,
										datahighlightcustomclass : 'symbol',
											textclass:"content",
											textdata:data.string.isgreaterthan
									},
									{
											textclass:"content",
											textdata:data.string.p1text9
									},
									{
											textclass:"content",
											textdata:data.string.p1text19
									}
							]
					}
			]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageload:true,
		uppertextblockadditionalclass: 'top-text-block',
		uppertextblock:[{
			textdata: data.string.p1text20,
			textclass: "top-text",
		}],
		sideboxes:[{
		sideboxclass:"leftBoxSec",
		imageblock:[{
			imagestoshow:[
			{
					imgclass:'box1',
					imgid:'leftarrow',
				}]
		}],
		sideboxtxt:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'symbols',
			textdata: data.string.p1text14,
			textclass: "left-text1",
		},{
			datahighlightflag : true,
			datahighlightcustomclass : 'symbols',
			textdata: data.string.p1text15,
			textclass: "left-text2",
		}]
	},
	{
		sideboxclass:"rightboxSec",
		imageblock:[{
			imagestoshow:[{
				imgclass:'box2',
				imgid:'rightarrow',
			}]
		}],
		sideboxtxt:[{
			datahighlightflag : true,
			datahighlightcustomclass : 'symbols',
			textdata: data.string.p1text15,
			textclass: "right-text1",
		},{
			datahighlightflag : true,
			datahighlightcustomclass : 'symbols',
			textdata: data.string.p1text14,
			textclass: "right-text2",
		}]
	}],
	extratextblock:[{
		textdata: data.string.p1text12,
		textclass: "left-text",
	},{
		textdata: data.string.p1text13,
		textclass: "right-text",
	}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "fulcrum", src: imgpath+"fulcrum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "leftarrow", src: imgpath+"big_arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rightarrow", src: imgpath+"big_arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			
			// sounds
			{id: "s1_p3_greatersmaller", src: soundAsset+"s1_p3_greatersmaller.ogg"},
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p2_1", src: soundAsset+"s1_p2_1.ogg"},
			{id: "s1_p2_2", src: soundAsset+"s1_p2_2.ogg"},
			{id: "s1_p2_3", src: soundAsset+"s1_p2_3.ogg"},
			{id: "s1_p2_4", src: soundAsset+"s1_p2_4.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):'';

		$prevBtn.hide(0);
		$nextBtn.hide(0);
		    // $(".text-hidden").prepend(countNext+". ");

		$(".text-hidden, .bottom-big-title, .tdbox1 ,.tdbox2, .tdbox3, .tdbox4, .thbox").hide(0);
		switch(countNext) {
		case 1:
		$(".bottom-big-title").delay(1000).show(0);
			// sound_nav("s1_p"+(countNext+1));
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p2");
				current_sound.play();
				current_sound.on("complete", function(){var current_sound = createjs.Sound.play('s1_p3');
				current_sound.play();
				$(".text-list-1, .tdbox1,.thbox").show(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p2_1");
					current_sound.play();
					current_sound.on("complete", function(){
					$(".text-list-2, .tdbox2").show(0);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p2_2");
						current_sound.play();
						current_sound.on("complete", function(){
						$(".text-list-3, .tdbox3").show(0);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p2_3");
							current_sound.play();
							current_sound.on("complete", function(){
							$(".text-list-4 , .tdbox4").show(0);
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s1_p2_4");
								current_sound.play();
								current_sound.on("complete", function(){
									nav_button_controls(0);
								});
							});
						});
					});
				});
		break;
		case 0:
			sound_nav("s1_p"+(countNext+1));
		break;
		case 2:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play('s1_p3');
			current_sound.play();
			current_sound.on("complete", function(){
				var current_sound2 = createjs.Sound.play('s1_p3_greatersmaller');
				current_sound2.play();
				current_sound2.on("complete", function(){
					nav_button_controls(0);
			});
		});
		break;
		default:
		nav_button_controls(0);
		break;
		}

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
	if(content[count].hasOwnProperty('sideboxes')){
		for(var i=0; i<content[count].sideboxes.length;i++){
			if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
			{
				var imageblock = content[count].sideboxes[i].imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var j=0; j<imageClass.length; j++){
						var image_src = preload.getResult(imageClass[j].imgid).src;
						//get list of classes
						var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
							// alert(i);
					}
				}
			}
		}
	}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		// for (var i = 0; i < content.length; i++) {
		// 	slides(i);
		// 	$($('.totalsequence')[i]).html(i);
		// 	$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		// 		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
		// 	});
		// }
		// function slides(i){
		// 		$($('.totalsequence')[i]).click(function(){
		// 			countNext = i;
		// 			createjs.Sound.stop();
		// 			templateCaller();
		// 		});
		// 	}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

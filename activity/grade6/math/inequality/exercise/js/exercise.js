var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata: data.string.eins1,
			textclass: "eins-1",
		}],
		imagetextblockadditionalclass: 'option-block-1',
		imagetextblock: [
			{
				imagediv: 'option-item-1 opt-1',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			},
			{
				imagediv: 'option-item-1 opt-2',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			},
			{
				imagediv: 'option-item-1 opt-3',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			},
			{
				imagediv: 'option-item-1 opt-4',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			},
			{
				imagediv: 'option-item-1 opt-5',
				imgclass: '',
				imgsrc: "",
				textdata : '',
				textclass : ''
			}
		],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'eins-2',
		uppertextblock:[{
			textdata: data.string.eins2,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "eques-2",
		}],
		lowertextblockadditionalclass: 'option-block-2',
		lowertextblock:[{
			textdata: '',
			textclass: "option-item-2 opt-1",
		},{
			textdata: '',
			textclass: "option-item-2 opt-2",
		},{
			textdata: '',
			textclass: "option-item-2 opt-3",
		},{
			textdata: '',
			textclass: "option-item-2 opt-4",
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'eins-2',
		uppertextblock:[{
			textdata: data.string.eins2,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "eques-2",
		}],
		lowertextblockadditionalclass: 'option-block-2',
		lowertextblock:[{
			textdata: '',
			textclass: "option-item-2 opt-1",
		},{
			textdata: '',
			textclass: "option-item-2 opt-2",
		},{
			textdata: '',
			textclass: "option-item-2 opt-3",
		},{
			textdata: '',
			textclass: "option-item-2 opt-4",
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'eins-2',
		uppertextblock:[{
			textdata: data.string.eins2,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "eques-2",
		}],
		lowertextblockadditionalclass: 'option-block-2',
		lowertextblock:[{
			textdata: '',
			textclass: "option-item-2 opt-1",
		},{
			textdata: '',
			textclass: "option-item-2 opt-2",
		},{
			textdata: '',
			textclass: "option-item-2 opt-3",
		},{
			textdata: '',
			textclass: "option-item-2 opt-4",
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'eins-3',
		uppertextblock:[{
			textdata: data.string.eins3,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: '',
			divclass: "drag-item drag-1",
		},{
			textdata: '',
			divclass: "drag-item drag-2",
		},{
			textdata: '',
			divclass: "drag-item drag-3",
		},{
			textdata: '',
			divclass: "drag-item drag-4",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-4',
			hasCorrectIncorrect: true
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'eins-3',
		uppertextblock:[{
			textdata: data.string.eins3,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: '',
			divclass: "drag-item drag-1",
		},{
			textdata: '',
			divclass: "drag-item drag-2",
		},{
			textdata: '',
			divclass: "drag-item drag-3",
		},{
			textdata: '',
			divclass: "drag-item drag-4",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-4',
			hasCorrectIncorrect: true
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'eins-3',
		uppertextblock:[{
			textdata: data.string.eins3,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: '',
			divclass: "drag-item drag-1",
		},{
			textdata: '',
			divclass: "drag-item drag-2",
		},{
			textdata: '',
			divclass: "drag-item drag-3",
		},{
			textdata: '',
			divclass: "drag-item drag-4",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container drop-4',
			hasCorrectIncorrect: true
		}]
	},
	//slide 7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'eins-3',
		uppertextblock:[{
			textdata: data.string.eins3,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: '',
			divclass: "drag-item drag-1",
		},{
			textdata: '',
			divclass: "drag-item drag-2",
		},{
			textdata: '',
			divclass: "drag-item drag-3",
		},{
			textdata: '',
			divclass: "drag-item drag-4",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-4',
			hasCorrectIncorrect: true
		}]
	},
	//slide 8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'eins-3',
		uppertextblock:[{
			textdata: data.string.eins3,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: '',
			divclass: "drag-item drag-1",
		},{
			textdata: '',
			divclass: "drag-item drag-2",
		},{
			textdata: '',
			divclass: "drag-item drag-3",
		},{
			textdata: '',
			divclass: "drag-item drag-4",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-4',
			hasCorrectIncorrect: true
		}]
	},
	//slide 9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblockadditionalclass: 'eins-3',
		uppertextblock:[{
			textdata: data.string.eins3,
			textclass: "",
		}],
		draggableblockadditionalclass: 'drag-drop-container',
		draggableblock:[{
			textdata: '',
			divclass: "drag-item drag-1",
		},{
			textdata: '',
			divclass: "drag-item drag-2",
		},{
			textdata: '',
			divclass: "drag-item drag-3",
		},{
			textdata: '',
			divclass: "drag-item drag-4",
		}],
		drawblockwrapper:'dd-wrapper',
		drawblock: [{
			drawblockcontainer: 'dd-num-container ddc-1',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-1',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-2',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-2',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-3',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-3',
			hasCorrectIncorrect: true
		},{
			drawblockcontainer: 'dd-num-container ddc-4',
			drawblock: 'dd-numline-block',
			droppablediv: 'droppable-container-2 drop-4',
			hasCorrectIncorrect: true
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new NumberTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "bg-1", src: imgpath+"bg_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-3", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-2", src: imgpath+"green_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-4", src: imgpath+"red_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-1", src: imgpath+"orange_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-5", src: imgpath+"pink_balloon_blast.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-3", src: imgpath+"balloon_blue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-2", src: imgpath+"balloon_green.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-4", src: imgpath+"balloon_red.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-1", src: imgpath+"balloon_orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-5", src: imgpath+"balloon_pink.png", type: createjs.AbstractLoader.IMAGE},

			{id: "arrow-left", src: imgpath+"arrow-left.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow-right", src: imgpath+"arrow-right.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var quesCount = ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8', 'Q9', 'Q10'];
	if($lang=='np') quesCount = ['१) ', '२) ', '३) ', '४) ', '५) ', '६) ', '७) ', '८) ', '९) ', '१०) '];

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		var option_position = [1,2];
		option_position.shufflearray();
		for(var op=0; op<2; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}

		// $('#num_ques').html(quesCount[countNext]);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);


		var wrong_clicked = 0;

		switch(countNext) {
			case 0:
				var wrongClicked = 0;
				var rightClicked = 0;
				var numCorrect = 0;
				var questions = [
					['20 > 20', false],
					['20 = 20', true],
					['3 ≠ 3', false],
					['3 < 9', true],
					['-1 < 10', true],
					['10 > 1', true],
					['10 + 2 ≠ 12', false]
				];
				questions.shufflearray();
				$('.bg-1').css({
					'background-image': 'url('+preload.getResult('bg-1').src+')'
				});
				$('.opt-1>img').attr('src', preload.getResult('balloon-1').src);
				$('.opt-2>img').attr('src', preload.getResult('balloon-2').src);
				$('.opt-3>img').attr('src', preload.getResult('balloon-3').src);
				$('.opt-4>img').attr('src', preload.getResult('balloon-4').src);
				$('.opt-5>img').attr('src', preload.getResult('balloon-5').src);

				for(var i=0; i<5; i++){
					if(questions[i][1]){
						$('.opt-'+(i+1)).addClass('correct-option');
						numCorrect++;
					}
					$('.opt-'+(i+1)+'>p').text(questions[i][0]);
				}
				$(".option-item-1").click(function(){
					if($(this).hasClass("correct-option")){
						rightClicked++;
						console.log(rightClicked);
						console.log(numCorrect);
						if(rightClicked==numCorrect){
							if(wrongClicked==0) scoring.update(true);
							$(".option-item-1").css('pointer-events','none');
							$nextBtn.show(0);
						}
						$(this).css('pointer-events','none');
			 			play_correct_incorrect_sound(1);
						$(this).addClass('go-up');
					}
					else{
						if(wrongClicked==0){
							scoring.update(false);
						}
			 			play_correct_incorrect_sound(0);
						var balloonClass = $(this).attr('class');
						var balloonIdx = balloonClass.substring((balloonClass.length - 1));
						$(this).children('img').attr('src', preload.getResult('pop-'+balloonIdx).src);
						$(this).children('p').hide(0);
						$(this).children('img').addClass('go-down');
						wrongClicked++;
					}
				});
				break;
			case 1:
			case 2:
			case 3:
				var wrongClicked = 0;
				var numArray = [];
				for(var i=-10; i<=10; i++){
					numArray.push(i);
				}
				numArray.shufflearray();
				//select 3 numbers
				var selNumArray = numArray.slice(0, 3);
				//to sort array
				//using sortNumber callback to sort both positive and negative numbers
				selNumArray.sort(sortNumber);
				//make one of them x
				var xChooser = Math.round(Math.random()*2);
				selNumArray[xChooser] = 'x';
				//choose signs
				var sign1 = '<';
				var sign2 = '<';
				if(Math.random()>0.5) sign1 = '<';
				if(Math.random()>0.5) sign2 = '<=';

				function swapSign(sign){
					var newString = sign;
					if(sign.includes('<')) newString = sign.replace('<', '>');
					if(sign.includes('>')) newString = sign.replace('>', '<');
					return newString;
				}

				function useThisSign(sign){
					var newString = sign;
					if(sign.includes('<=')) newString = sign.replace('<=', '\u2264');
					if(sign.includes('>=')) newString = sign.replace('>=', '\u2265');
					return newString;
				}
				console.log(xChooser);
				console.log(swapSign(sign2));
				var questionText = selNumArray[0]+useThisSign(sign1)+selNumArray[1]+data.string.and+selNumArray[0]+useThisSign(sign2)+selNumArray[2];
				// var questionText = selNumArray[0]+useThisSign(sign1)+selNumArray[1]+'    and '+selNumArray[0]+useThisSign(sign2)+selNumArray[2];
				if(xChooser==1){
					questionText = selNumArray[1]+useThisSign(swapSign(sign1))+selNumArray[0]+data.string.and+selNumArray[1]+useThisSign(sign2)+selNumArray[2];
				} else if(xChooser==2){
					questionText = selNumArray[2]+useThisSign(swapSign(sign1))+selNumArray[0]+data.string.and+selNumArray[2]+useThisSign(swapSign(sign2))+selNumArray[1];
				}
				var correctAns = selNumArray[0]+useThisSign(sign1)+selNumArray[1]+useThisSign(sign2)+selNumArray[2];
				var opt1 = selNumArray[2]+useThisSign(sign1)+selNumArray[1]+useThisSign(sign2)+selNumArray[0];
				var opt2 = selNumArray[0]+useThisSign(sign1)+selNumArray[2]+useThisSign(sign2)+selNumArray[1];
				var opt3 = selNumArray[0]+useThisSign(swapSign(sign1))+selNumArray[1]+useThisSign(swapSign(sign2))+selNumArray[2];
				var options = [[correctAns,true],[opt1,false],[opt2,false],[opt3,false]];
				options.shufflearray();
				var optNumber = ['a) ','b) ','c) ','d) ','e) '];
				$('.eques-2').text(questionText);
				for(var i=0; i<4; i++){
					if(options[i][1]) $('.opt-'+(i+1)).addClass('correct-option');
					$('.opt-'+(i+1)).text(optNumber[i]+options[i][0]);
				}
				$(".option-item-2").click(function(){
					if($(this).hasClass("correct-option")){
						if(wrongClicked==0){
							scoring.update(true);
						}
						$(this).siblings(".corctopt").show(0);
			 			play_correct_incorrect_sound(1);
						$(this).parent().addClass('correct-ans');
						$(".opns").css('pointer-events','none');
						$nextBtn.show(0);
					}
					else{
						if(wrongClicked==0){
							scoring.update(false);
						}
								$(this).siblings(".wrngopt").show(0);
			 			play_correct_incorrect_sound(0);
						$(this).parent().addClass('incorrect-ans');
						wrongClicked++;
					}
				});
				break;
			case 4:
			case 5:
			case 6:
				var wrongClicked = 0;
				var randomInt = Math.round(Math.random()*6);
				if(Math.random()>0.5) randomInt *= -1;

				var TopPositions = [1,2,3,4];
				TopPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.drag-'+i).addClass('drag-pos-'+TopPositions[i-1]);
				}

				$('.drag-1>p').html('x ≥ '+randomInt);
				$('.drag-2>p').html('x > '+randomInt);
				$('.drag-3>p').html('x ≤ '+randomInt);
				$('.drag-4>p').html('x < '+randomInt);

				var BotPositions = [1,2,3,4];
				BotPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.ddc-'+i).addClass('container-'+BotPositions[i-1]);
				}

				initializeSVG();
				createSolution('ddc-1', randomInt, true, true);
				createSolution('ddc-2', randomInt, true, false);
				createSolution('ddc-3', randomInt, false, true);
				createSolution('ddc-4', randomInt, false, false);
				var itemCount = 0;
				$('.drag-item').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.drop-1').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-1');
					}
				});
				$('.drop-2').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-2');
					}
				});
				$('.drop-3').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-3');
					}
				});
				$('.drop-4').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-4');
					}
				});
				function dropper(dropped, droppedOn, hasclass){
					if(dropped.hasClass(hasclass)){
						dropped.draggable('option', 'revert', false);
						play_correct_incorrect_sound(1);
						itemCount++;
						dropped.draggable('disable');
						dropped.detach().css({
							'height': '100%',
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'border': '0.5vmin solid #cdeb58',
							'background-color': '#89d457',
							'pointer-events': 'none'
						}).appendTo(droppedOn);
						$(droppedOn).css({
							'border': 'none'
						});
						$(droppedOn).children('img').show(0);
						if(itemCount>3){
							if(wrongClicked==0) scoring.update(true);
							$nextBtn.show(0);
						}
					} else{
						if(wrongClicked==0){
							scoring.update(false);
						}
						wrongClicked++;
						play_correct_incorrect_sound(0);
					}
				}
				break;
			case 7:
			case 8:
			case 9:
				var wrongClicked = 0;
				var question1, question2, question3, question4, TopPositions, BotPositions;
				question1 = randomPairGenerate();
				question2 = randomPairGenerate();
				question3 = randomPairGenerate();
				question4 = randomPairGenerate();
				while(arraysEqual(question1, question2)){
					question2 = randomPairGenerate();
				}
				while(arraysEqual(question3, question2)||arraysEqual(question3, question1)){
					question3 = randomPairGenerate();
				}
				while(arraysEqual(question4, question3)||arraysEqual(question4, question2)||arraysEqual(question4, question1)){
					question4 = randomPairGenerate();
				}

				TopPositions = [1,2,3,4];
				TopPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.drag-'+i).addClass('drag-pos-'+TopPositions[i-1]);
				}

				$('.drag-1>p').text(questionTextGenerate(question1));
				$('.drag-2>p').text(questionTextGenerate(question2));
				$('.drag-3>p').text(questionTextGenerate(question3));
				$('.drag-4>p').text(questionTextGenerate(question4));

				BotPositions = [1,2,3,4];
				BotPositions.shufflearray();
				for(var i=1; i<5; i++){
					$('.ddc-'+i).addClass('container-'+BotPositions[i-1]);
				}

				initializeSVG();
				createSolution2('ddc-1', question1[0], question1[1], question1[2], question1[3]);
				createSolution2('ddc-2', question2[0], question2[1], question2[2], question2[3]);
				createSolution2('ddc-3', question3[0], question3[1], question3[2], question3[3]);
				createSolution2('ddc-4', question4[0], question4[1], question4[2], question4[3]);
				var itemCount = 0;
				$('.drag-item').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.drop-1').droppable({
					drop:function(event, ui) {
						dropper2(ui.draggable, $(this), 'drag-1');
					}
				});
				$('.drop-2').droppable({
					drop:function(event, ui) {
						dropper2(ui.draggable, $(this), 'drag-2');
					}
				});
				$('.drop-3').droppable({
					drop:function(event, ui) {
						dropper2(ui.draggable, $(this), 'drag-3');
					}
				});
				$('.drop-4').droppable({
					drop:function(event, ui) {
						dropper2(ui.draggable, $(this), 'drag-4');
					}
				});
				function dropper2(dropped, droppedOn, hasclass){
					if(dropped.hasClass(hasclass)){
						dropped.draggable('option', 'revert', false);
						play_correct_incorrect_sound(1);
						itemCount++;
						dropped.draggable('disable');
						dropped.detach().css({
							'height': '100%',
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'border': '0.5vmin solid #cdeb58',
							'background-color': '#89d457',
							'pointer-events': 'none'
						}).appendTo(droppedOn);
						$(droppedOn).css({
							'border': 'none'
						});
						$(droppedOn).children('img').show(0);
						if(itemCount>3){
							if(wrongClicked==0){
								scoring.update(true);
							}
							if(countNext<$total_page){
								$nextBtn.show(0);
							}else{
								ole.footerNotificationHandler.lessonEndSetNotification();
							}
						}
					} else{
						if(wrongClicked==0){
							scoring.update(false);
						}
						wrongClicked++;
						play_correct_incorrect_sound(0);
					}
				}
				break;
			default:
				// nav_button_controls(0);
				break;
		}
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.option-pos-1, .option-pos-2').off('mouseenter mouseleave');
				$('.center-sundar').attr('src', preload.getResult(correct_images[rand_img]).src);
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				var rand_img = Math.floor(Math.random()*correct_images.length);
				var classname_monkey = $(this).parent().attr('class').replace(/main-container/, '');
				classname_monkey = classname_monkey.replace(/ /g, '');
				$('.'+classname_monkey).off('mouseenter mouseleave');
				$('.center-sundar').attr('src', preload.getResult(incorrect_images[rand_img]).src);
				scoring.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});

		$prevBtn.hide(0);
	}
	//functions

	function sortNumber(a, b) {
		return a - b;
	}

	function initializeSVG(){
		var lineMidY = 45;
		for(var i=0; i<21; i++){
			var markElementTop = document.createElement("div");
			var markElementBottom = document.createElement("div");
			var leftPos = 2+i*4.8;

			markElementTop.className = 'mark-element top-mark-'+(i-10);
			markElementTop.style.left =  leftPos+'%';
			$('.dd-mark-container').append(markElementTop);

			markElementBottom.className = 'mark-element bottom-mark-'+(i-10);
			markElementBottom.style.left =  leftPos+'%';
			$('.dd-label-container').append(markElementBottom);

			var markLine = document.createElement("div");
			markLine.className = 'mark-line line-'+i;

			var markLabel = document.createElement("p");
			markLabel.textContent = i-10;
			markLabel.className = 'mark-label label-'+(i-10);
			if(i<10){
				markLabel.className +=' negative-number-label';
			} else if(i>10){
				markLabel.className +=' positive-number-label';
			} else{
				markLabel.className +=' zero-number-label';
			}

			$('.top-mark-'+(i-10)).append(markLine);
			$('.bottom-mark-'+(i-10)).append(markLabel);
		}
	}


	function createSlantedLine(direction, parent){
		var slantedLine = document.createElement("div");
		var angle = Math.atan($(parent).height()/$(parent).width());
		var height = 200/Math.sin(angle);
		slantedLine.className = 'slanted-line';
		slantedLine.style.height = height+'%';
		angle /= 5;
		if(direction=='left'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle*-1 +'rad)';
		}
		if(direction=='right'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle +'rad)';
		}
		$(parent).append(slantedLine);
	}

	function createSlantedLine2(direction, parent){
		var slantedLine = document.createElement("div");
		var angle = Math.atan($(parent).height()/$(parent).width())/2;
		var height = 103/Math.sin(angle);
		slantedLine.className = 'slanted-line-2';
		slantedLine.style.height = height+'%';
		angle = angle/2.8;
		if(direction=='left'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle*-1 +'rad)';
		}
		if(direction=='right'){
			slantedLine.style.transform = 'translateX(-50%) rotate('+ angle +'rad)';
		}
		$(parent).append(slantedLine);
	}
	function createRoof(direction, parent){
		var roof = document.createElement("div");
		roof.className = 'roof-1';
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.style.right = '100%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.style.left = '100%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function createStraightLine(parent){
		var slantedLine = document.createElement("div");
		slantedLine.className = 'straight-line';
		$(parent).append(slantedLine);
	}
	function createStraightLine2(parent){
		var slantedLine = document.createElement("div");
		slantedLine.className = 'straight-line-2';
		$(parent).append(slantedLine);
	}
	function createRoof2(direction, parent){
		var roof = document.createElement("div");
		roof.className = 'roof-1';
		var arrowImg = document.createElement("img");
		if(direction=='left'){
			roof.style.right = '50%';
			arrowImg.src = preload.getResult('arrow-left').src;
		}
		if(direction=='right'){
			roof.style.left = '50%';
			arrowImg.src = preload.getResult('arrow-right').src;
		}
		roof.append(arrowImg);
		$(parent).append(roof);
	}
	function placeDropBox(direction, isEqual, parent, number){
		var someOffset = 0;
		if(isEqual) someOffset = -2.5;
		if(direction=='right'){
			$(parent+ ' .droppable-container').css({
				'left': 5+someOffset+(10+number)*4.4+'%'
			});
		}else{
			$(parent+ ' .droppable-container').css({
				'right': 5+someOffset+(10-number)*4.4+'%'
			});
		}
	}
	function createSolution(numline, number, isGreater, isEqual){
		var dotCircle = document.createElement("div");
		var direction = 'left';
		if(isGreater) direction='right';
		if(isEqual){
			dotCircle.className = 'mark-filled-circle';
			createStraightLine('.'+numline+' .top-mark-'+number);
			createRoof2(direction, '.'+numline+' .top-mark-'+number, '');
			placeDropBox(direction, isEqual, '.'+numline, number);
		}else{
			dotCircle.className = 'mark-hollow-circle';
			createSlantedLine(direction, '.'+numline+' .top-mark-'+number);
			createRoof(direction, '.'+numline+' .top-mark-'+number, '');
			placeDropBox(direction, isEqual, '.'+numline, number);
		}
		$('.'+numline+' .top-mark-'+number).append(dotCircle);
	}
	function createSolution2(numline, lowLimit, isEqualLow, HighLimit, isEqualHigh){
		var dotCircleLow = document.createElement("div");
		var dotCircleHigh = document.createElement("div");
		var roof = document.createElement("div");
		roof.className = 'join-roof';
		var spaceUnit = 4.8;
		var slantedFactor1 = 0, slantedFactor2 = 0;
		if(!isEqualLow) slantedFactor1 = 0.5*spaceUnit;
		if(!isEqualHigh) slantedFactor2 = 0.5*spaceUnit;
		var roofLeft = (lowLimit+10)*spaceUnit+slantedFactor1+2;
		var roofWidth = (HighLimit-lowLimit)*spaceUnit-(slantedFactor1+slantedFactor2);
		roof.style.width = roofWidth+'%';
		roof.style.left = roofLeft+'%';
		//for dropbox
		$('.'+numline+ ' .droppable-container-2').css({
			'left': (lowLimit+10)*spaceUnit+(HighLimit-lowLimit)*spaceUnit/2-2+'%'
		});
		if(isEqualLow){
			dotCircleLow.className = 'mark-filled-circle';
			createStraightLine2('.'+numline+' .top-mark-'+lowLimit);
		}else{
			dotCircleLow.className = 'mark-hollow-circle';
			createSlantedLine2('right', '.'+numline+' .top-mark-'+lowLimit);
		}
		if(isEqualHigh){
			dotCircleHigh.className = 'mark-filled-circle';
			createStraightLine2('.'+numline+' .top-mark-'+HighLimit);
		}else{
			dotCircleHigh.className = 'mark-hollow-circle';
			createSlantedLine2('left', '.'+numline+' .top-mark-'+HighLimit);
		}
		$('.'+numline+' .dd-mark-container').append(roof);
		$('.'+numline+' .top-mark-'+lowLimit).append(dotCircleLow);
		$('.'+numline+' .top-mark-'+HighLimit).append(dotCircleHigh);
	}







	function randomPairGenerate(){
		var randomInt1 = Math.round(Math.random()*10);
		var randomInt2 = Math.round(Math.random()*10);
		if(Math.random()>0.5) randomInt1 *= -1;
		while(randomInt2==randomInt1){
			randomInt2 = Math.round(Math.random()*10);
			if(Math.random()>0.5) randomInt2 *= -1;
		}
		if(randomInt1==10&&randomInt2>7){randomInt2=7;}
		if(randomInt1==-10&&randomInt2<-7){randomInt2=-7;}
		if(randomInt1<randomInt2){
			return [randomInt1, randomTrueFalse(), randomInt2, randomTrueFalse()];
		} else{
			return [randomInt2, randomTrueFalse(), randomInt1, randomTrueFalse()];
		}
	}
	function randomTrueFalse(){
		if(Math.random()>0.5) {
			return true;
		}else{
			return false;
		}
	}
	function arraysEqual(arr1, arr2) {
		if (arr1.length !== arr2.length) return false;
		for (var i = arr1.length; i--; ){
			if (arr1[i] !== arr2[i]) return false;
		}
		return true;
	}
	function questionTextGenerate(questionInput){
		var firstSign = '<';
		var secondSign = '<';
		var string = '';
		if(questionInput[1]) firstSign ='\u2264';
		if(questionInput[3]) secondSign ='\u2264';
		string = questionInput[0]+firstSign+'x'+secondSign+questionInput[2];
		return string;
	}



	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";

/*
	content 1 for compulsary questions
	content 2 for not compulsary questions
	content for actual questions;

	content 2 is randomized and spliced then merged with content 1
*/
var content1=[

	//ex1
	{
		exerciseblock: [
			{
				textdata: data.string.exques1,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q1ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q1ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q1ansc,
					}],
			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
				textdata: data.string.exques2,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q2ansa,
					},
					{optclass:"",
						forshuffle: "class2",
						optdata: data.string.q2ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q2ansc,
					}]
			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
				textdata: data.string.exques3,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q3ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q3ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q3ansc,
					}]
			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
				textdata: data.string.exques4,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q4ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q4ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q4ansc,
					}],
			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
				textdata: data.string.exques5,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q5ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q5ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q5ansc,
					}]
			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
				textdata: data.string.exques6,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q6ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q6ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q6ansc,
					}]
			}
		]
	},
];

var content2= [
	//ex7
	{
		exerciseblock: [
			{
				textdata: data.string.exques7,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q7ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q7ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q7ansc,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "7.png",
					}
				]

			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
				textdata: data.string.exques8,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q8ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q8ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q8ansc,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "8.png",
					}
				]

			}
		]
	},
	//ex9
	{
		exerciseblock: [
			{
				textdata: data.string.exques9,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q9ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q9ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q9ansc,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "9.png",
					}
				]

			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
				textdata: data.string.exques10,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q10ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q10ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q10ansc,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "10.png",
					}
				]

			}
		]
	},
	//ex11
	{
		exerciseblock: [
			{
				textdata: data.string.exques11,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q11ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q11ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q11ansc,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "11.png",
					}
				]

			}
		]
	},
	//ex12
	{
		exerciseblock: [
			{
				textdata: data.string.exques12,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q12ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q12ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q12ansc,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "12.png",
					}
				]

			}
		]
	},
];

/*remove this for non random questions*/
content2.shufflearray();
content2.splice(3,2);

var content = content1.concat(content2);
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();

 	testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		parent = $(".optionsdiv_2");
		divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		var ansClicked = false;
		var wrngClicked = false;

		$(".optionscontainer").click(function(){
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("correct")){
						play_correct_incorrect_sound(1);
						if(wrngClicked == false){
							testin.update(true);
						}
						$(this).children(".corctopt").show(0);
						$('.hint_image').show(0);
          	$(this).addClass('corrects');
            $(" .optionscontainer").css("pointer-events","none");
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					  else{
						testin.update(false);
						play_correct_incorrect_sound(0);
            $(this).addClass('incorrect');
            $(this).css("pointer-events","none");
						$(this).children(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

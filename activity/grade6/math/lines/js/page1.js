var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [{
	//starting page
	contentblocknocenteradjust: true,
	contentblockadditionalclass: 'firstpagebackground',
	uppertextblock: [{
		textclass: "firsttitle",
		textdata: data.string.firsttitle
	}]

}, {
	//page 1
	lowertextblock: [
	{
		textclass: 'instruction-text',
		textdata: data.string.p1_s1
	}],
	containscanvaselement: true
}, {
	//page 2
	lowertextblock: [
	{
		textclass: 'instruction-text',
		textdata: data.string.p1_s2
	}],
	containscanvaselement: true
}, {
	//page 3
	lowertextblock: [{
		textclass: 'instruction-text',
		textdata: data.string.p1_s3
	}, {
		textclass: 'instruction-text',
		textdata: ''
	}],
	containscanvaselement: true
}, {
	//page 4
	lowertextblock: [{
		textclass: 'instruction-text',
		textdata: data.string.p1_s5
	}],
	upperblock:[{
		textclass: 'hiddeno',
		textdata: data.string.o
	}],
	imageblock: [{
		imagestoshow: [{
			imgsrc: imgpath + 'sundar_face01.png',
			imgclass: 'face-01'
		}, {
			imgsrc: imgpath + 'sundar_face02.png',
			imgclass: 'face-02'
		}]
	}],
	containscanvaselement: true
}, {
	//page 5
	def:"",
	definitionblock: [{
		content: data.string.p1_s9
	}, {
		content: data.string.p1_s10
	}, {
		content: data.string.p1_s11
	}],
	imageblockadditionalclass: 'summary-imageblock',
	imageblock: [{
		imagestoshow: [{
			imgsrc: imgpath + 'teaching01.png',
			imgclass: 'teaching-img'
		}, {
			imgsrc: imgpath + 'intersecting-lines.png',
			imgclass: 'summary-img'
		}],
		imagelabels: [{
			imagelabelclass: 'summary-label',
			imagelabeldata: data.string.p1_s12
		}]
	}],

}];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);


	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"S1_P1.ogg"},
      {id: "s1_p2", src: soundAsset+"S1_P2.ogg"},
      {id: "s1_p3", src: soundAsset+"S1_P3.ogg"},
      {id: "s1_p4a", src: soundAsset+"S1_P4_1.ogg"},
		  {id: "s1_p4b", src: soundAsset+"S1_P4_2.ogg"},
			{id: "s1_p5", src: soundAsset+"S1_P5.ogg"},
			{id: "s1_p5a", src: soundAsset+"S1_P5_2.ogg"},
			{id: "s1_p5b", src: soundAsset+"S1_P5_1.ogg"},
			{id: "s1_p6a", src: soundAsset+"S1_P6_1.ogg"},
			{id: "s1_p6b", src: soundAsset+"S1_P6_2.ogg"},
			{id: "s1_p6c", src: soundAsset+"S1_P6_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);
		vocabcontroller.findwords(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$('.help-button').hide(0);

        $prevBtn.hide(0);
        $nextBtn.hide(0);

		var $canvas = $('#customcanvasbase');
		if ($canvas[0] !== undefined) {
			var ctx = $canvas[0].getContext('2d');
			var canvasHeight = $board.height() * 0.75;
			var canvasWidth = $board.width();
			ctx.canvas.height = canvasHeight;
			ctx.canvas.width = canvasWidth;
		}

		// Draw a line with line data
		function drawLine(line) {
			// line AB
			ctx.beginPath();
			ctx.moveTo(line.x1, line.y1);
			ctx.lineTo(line.x2, line.y2);
			ctx.strokeStyle = line.color;
			ctx.lineWidth = 5;
			ctx.stroke();
		}

		function drawText(text, x, y) {
			ctx.font='1em Arial';
			ctx.fillStyle = '#000000'
			ctx.fillText(text, x, y);
		}

		var lineAB = {
			x1: canvasWidth*0.15,
			y1: canvasHeight*0.7,
			x2: canvasWidth*0.48,
			y2: canvasHeight*0.2,
			color: '#71A1EC'
		};

		var lineCD = {
			x1: canvasWidth*0.52,
			y1: canvasHeight*0.20,
			x2: canvasWidth*0.85,
			y2: canvasHeight*0.7,
			color: '#8E7CC3',
		};

		switch(countNext){
            case 0:
              	sound_player_nav("s1_p"+(countNext+1));
                break;

			case 1:
			 	sound_player_nav("s1_p"+(countNext+1));
				drawLine(lineAB);
				drawText('A', lineAB.x1 - canvasWidth * 0.02, lineAB.y1);
				drawText('B', lineAB.x2 - canvasWidth * 0.02, lineAB.y2);
                // showNavs();
                break;

			case 2:
			 	sound_player_nav("s1_p"+(countNext+1));
				drawLine(lineAB);
				drawText('A', lineAB.x1 - canvasWidth * 0.02, lineAB.y1);
				drawText('B', lineAB.x2 - canvasWidth * 0.02, lineAB.y2);

				drawLine(lineCD);
				drawText('C', lineCD.x1 + canvasWidth * 0.007, lineCD.y1);
				drawText('D', lineCD.x2 + canvasWidth * 0.007, lineCD.y2);
                // showNavs();
				break;

			case 3:
				// sound_player_nav("s1_p4a");
				drawLine(lineAB);
				drawLine(lineCD);
				// later added here for test
				ctx.clearRect(0, 0, canvasWidth, canvasHeight);
				lineAB.x1 += 1;
				lineAB.x2 += 1;
				lineCD.x1 -= 1;
				lineCD.x2 -= 1;
				drawLine(lineAB);
				drawLine(lineCD);
				drawText('A', lineAB.x1 - canvasWidth * 0.02, lineAB.y1);
				drawText('B', lineAB.x2 - canvasWidth * 0.02, lineAB.y2);
				drawText('C', lineCD.x1 - canvasWidth * -0.005, lineCD.y1);
				drawText('D', lineCD.x2 - canvasWidth * -0.005, lineCD.y2);
				// later added here for test ends
				function animateLines() {
					ctx.clearRect(0, 0, canvasWidth, canvasHeight);
					lineAB.x1 += 1;
					lineAB.x2 += 1;
					lineCD.x1 -= 1;
					lineCD.x2 -= 1;
					drawLine(lineAB);
					drawLine(lineCD);
					drawText('A', lineAB.x1 - canvasWidth * 0.02, lineAB.y1);
					drawText('B', lineAB.x2 - canvasWidth * 0.02, lineAB.y2);
					drawText('C', lineCD.x1 - canvasWidth * -0.005, lineCD.y1);
					drawText('D', lineCD.x2 - canvasWidth * -0.005, lineCD.y2);

					if (lineAB.x2 <= 0.65 * canvasWidth) {
						requestAnimationFrame(animateLines);

					} else {

						var secondPara = $('.instruction-text').eq(1);
						secondPara.html(data.string.p1_s4);
						secondPara.css({opacity: 1});
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p4b");
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls(0);
						});
					}
				}
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p4a");
				current_sound.play();
				current_sound.on('complete', function(){
					animateLines();
					// nav_button_controls(0);
				});
				break;

			case 4:
				sound_player_nav("s1_p"+(countNext+1));
				var face01 = $('.face-01');
				var face02 = $('.face-02');

				face01.hide(0);
				face02.hide(0);

				// Draw all lines and texts as was in the end of last slide
				function drawLines() {
					lineAB.x1 = 0.65 * canvasWidth - (lineAB.x2 - lineAB.x1);
					lineAB.x2 = 0.65 * canvasWidth;
					lineCD.x2 = 0.35 * canvasWidth + (lineCD.x2 - lineCD.x1);
					lineCD.x1 = 0.35 * canvasWidth;
					drawLine(lineAB);
					drawLine(lineCD);
					drawText('A', lineAB.x1 - canvasWidth * 0.02, lineAB.y1);
					drawText('B', lineAB.x2 - canvasWidth * 0.02, lineAB.y2);
					drawText('C', lineCD.x1 - canvasWidth * -0.005, lineCD.y1);
					drawText('D', lineCD.x2 - canvasWidth * -0.005, lineCD.y2);
				};

				var mouseX = 0;
				var mouseY = 0;
				var tolerance = 10;

				// Update mouse coordinates on mouse move
				$canvas.on('mousemove', function(e) {
					var mousePoint = getMousePos($canvas[0], e);
					mouseX = mousePoint.x;
					mouseY = mousePoint.y;
				});

				/**
				 * Calculate the perpendicular distance of a point
				 * to a line with two points given
				 */
				function pDistToLine(line, x, y) {
					var dy = line.y2 - line.y1;
					var dx = line.x2 - line.x1
					var c = line.x2 * line.y1 - line.y2 * line.x1;
					return Math.abs((dy*x - dx*y + c) / Math.sqrt(dy*dy + dx*dx));
				}

				/**
				 * Calculate the distance between two points
				 */
				function distance(point1, point2) {
					var dx = point2.x - point1.x;
					var dy = point2.y - point1.y;
					return Math.sqrt(dx*dx + dy*dy);
				}

				/**
				 * Calculate the point on the line nearest to a point
				 */
				function pNearToLine(line, x, y) {
					var b = line.x2 - line.x1;
					var a = line.y1 - line.y2;
					// var c = line.x1 * (line.y2 - line.y1) - line.y2 * (line.x2 - line.x1);
					var c = line.x1*line.y2 - line.x2*line.y1;
					return {
						x: (b * (b*x - a*y) - a*c) / (a*a + b*b),
						y: (a * (a*y - b*x) - b*c) / (a*a + b*b)
					};
				}

				/**
				 * Draw a dot on a point
				 */
				function drawDot(point, color) {
					// Sometimes point is undefined when clicked outside the canvas
					if (point === undefined) return;
					ctx.beginPath();
					ctx.arc(point.x, point.y, 10, 0, 2 * Math.PI, true);
					ctx.fillStyle = color;
					ctx.fill();
				}

				/**
				 * Draw wrong dots from points stored in array
				 */
				function drawWrongDots() {
					if (wrongClickedPoints.length > 0) {
						wrongClickedPoints.forEach(function(point) {
							drawDot(point, '#D42C16');
						}) ;
					}
				}

				/**
				 * Find intersection point of two lines
				 */
				function intersectOfLines(line1, line2) {
					var a = line1.x1*line1.y2 - line1.y1*line1.x2;
					var b = line2.x1*line2.y2 - line2.y1*line2.x2;
					var numeratorX = a*(line2.x1-line2.x2) - (line1.x1-line1.x2)*b;
					var numeratorY = a*(line2.y1-line2.y2) - (line1.y1-line1.y2)*b;
					var denomenator = (line1.x1-line1.x2)*(line2.y1-line2.y2) - (line1.y1-line1.y2) * (line2.x1-line2.x2);
					return {
						x: numeratorX / denomenator,
						y: numeratorY / denomenator
					}
				}

				var wrongClickedPoints = [];
				var correctClick = false;
				var exceedTryLimit = false;
				$canvas.on('click', function () {
					// Checking if the clicked point is near the intersection point
					if (distance({x: mouseX, y: mouseY}, intersection) < tolerance) {
						correctClick = true;
						face01.show(0);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p5a");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".uppertxt").show(0);
						nav_button_controls(2000);
					});
					} else {
						var pointOnLine;
						if (pDistToLine(lineAB, mouseX, mouseY) < tolerance) {
							pointOnLine = pNearToLine(lineAB, mouseX, mouseY);
						}  else if (pDistToLine(lineCD, mouseX, mouseY) < tolerance){
							pointOnLine = pNearToLine(lineCD, mouseX, mouseY);
						}
						wrongClickedPoints.push(pointOnLine);

						if (wrongClickedPoints.length > 3) {
							exceedTryLimit = true;
						}
					}
				});

				// Draw lines and dots leaving wrong dots for correct choice
				function handleCorrectClick() {
					ctx.clearRect(0, 0, canvasWidth, canvasHeight);
					drawLines();
					drawDot(intersection, '#33BE00');
					$('.lowertextblock p').html(data.string.p1_s7);
                    // showNavs();
				}

				// draw lines initially and set intersection point
				drawLines();
				var intersection = intersectOfLines(lineAB, lineCD);

				/**
				 * Recursive function to update mouse hover/click effect
				 */
				function mouseUpdate() {
					ctx.clearRect(0, 0, canvasWidth, canvasHeight);
					drawLines();
					drawWrongDots();

					if (distance({x: mouseX, y: mouseY}, intersection) < tolerance) {
						drawDot(intersection, '#f9a21d');
					} else if (pDistToLine(lineAB, mouseX, mouseY) < tolerance
						&& mouseX >= lineAB.x1 && mouseX <= lineAB.x2
						&& mouseY <= lineAB.y1 && mouseY >= lineAB.y2) {
						// Point on line nearest to mouse
						var point = pNearToLine(lineAB, mouseX, mouseY);
						drawDot(point, '#f9a21d');
					} else if (pDistToLine(lineCD, mouseX, mouseY) < tolerance
						&& mouseX >= lineCD.x1 && mouseX <= lineCD.x2
						&& mouseY >= lineCD.y1 && mouseY <= lineCD.y2) {
						// Point on line nearest to mouse
						var point = pNearToLine(lineCD, mouseX, mouseY);
						drawDot(point, '#f9a21d');
					}
					if (correctClick) {
						handleCorrectClick();
						return;
					}
					if (exceedTryLimit) {
						drawWrongDots();
						$('.help-button').show(0);
						return;
					}

					requestAnimationFrame(mouseUpdate);
				}

				mouseUpdate();

				$('.help-button').on('click', function() {
					handleCorrectClick();
					face02.show(0);
						$(".uppertxt").show(0);
					$('.lowertextblock p').html(data.string.p1_s6);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p5b");
					current_sound.play();
					current_sound.on('complete', function(){
					nav_button_controls(0);
				});
				});

				break; // end of case 4

						case 5:
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p6a");
						current_sound.play();
						$(".def >p:eq(0)").show(0);
						current_sound.on('complete', function(){
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p6b");
							current_sound.play();
							$(".def >p:eq(1)").show(0);
							current_sound.on('complete', function(){
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s1_p6c");
								current_sound.play();
								$(".def >p:eq(2)").show(0);
								current_sound.on('complete', function(){
									nav_button_controls(0);
								});
							});
						});
						break;


		}

	}

		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}

		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				if(countNext==3 || countNext==4){
					$prevBtn.hide(0);
					$nextBtn.hide(0);
				}else{
					nav_button_controls();
				}

			});
		}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

/**
 * Get exact mouse position in canvas
 */
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

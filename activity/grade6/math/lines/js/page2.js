var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
/*
	TODO : formatting and syntax
*/
var content = [{
	//slide 0
	containscanvaselement: true,
	lowertextblock: [{
		textdata: data.string.p2_s1,
		textclass: 'instruction-text'
	}],
	imageblock: [{
		imagestoshow: [{
			imgsrc: imgpath + 'sundar_face01.png',
			imgclass: 'face-01'
		}, {
			imgsrc: imgpath + 'sundar_face02.png',
			imgclass: 'face-02'
		}]
	}]
}, {
	//page 1
	def:"",
	definitionblock: [{
		content: data.string.p2_s5
	}, {
		content: data.string.p2_s6
	}, {
		content: data.string.p2_s7
	}],
	imageblockadditionalclass: 'summary-imageblock',
	imageblock: [{
		imagestoshow: [{
			imgsrc: imgpath + 'teaching01.png',
			imgclass: 'teaching-img'
		}, {
			imgsrc: imgpath + 'perpendicular-lines.png',
			imgclass: 'summary-img'
		}],
		imagelabels: [{
			imagelabelclass: 'summary-label',
			imagelabeldata: data.string.p2_s8
		}]
	}]
}
];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s2_p1", src: soundAsset+"S2_P1.ogg"},
		  {id: "s2_p1a", src: soundAsset+"S2_P1_2.ogg"},
     	{id: "s2_p2a", src: soundAsset+"S2_P2_1.ogg"},
			{id: "s2_p2b", src: soundAsset+"S2_P2_2.ogg"},
			{id: "s2_p2c", src: soundAsset+"S2_P2_3.ogg"},
     	{id: "help", src: soundAsset+"help.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		$('.help-button').hide(0);

		//call notifyuser
		// notifyuser($anydiv);

        $prevBtn.hide(0);
        $nextBtn.hide(0);

        function showNavs() {
            $prevBtn.show(0);
            $nextBtn.show(0);
        }


		// Initializing canvas
		var $canvas = $('#customcanvasbase');
		// Checking if the canvas exists
		var canvas = $canvas[0]; // Canvas element
		if (canvas !== undefined) {
			var ctx = canvas.getContext('2d');
			var canvasHeight = $board.height() * 0.75;
			var canvasWidth = $board.width();
			ctx.canvas.height = canvasHeight;
			ctx.canvas.width = canvasWidth;

			// Initializing mouse variables
			var mouse = {x: 0, y: 0}
			var tolerance = 10;
			var mousePressed = false;
			var dragged = false;
			var dy = 0;
			var lastAngle = 0;	// angle saved on last mouse clicked
			var dAngle = 0;	 // Difference of angle formed by current click and last angle
			var correctAngle = false; // Boolean for whether the mouse is released on right angle

			// Update mouse coordinates on mouse move event
			$canvas.on('mousemove', function(e) {
				var point = getMousePos(canvas, e);
				mouse.x = point.x;
				mouse.y = point.y;
				if (pDistToLine(lineCD, mouse) < tolerance) {
					dragged = mousePressed;
				}
				if (dragged) {
					dAngle = Math.atan2(mouse.y-intersection.y, mouse.x-intersection.x)-lastAngle;
				}
			});
			$canvas.on('mousedown', function(e) {
				mousePressed = true;
				lastAngle = Math.atan2(mouse.y-intersection.y, mouse.x-intersection.x);
			});
			$canvas.on('mouseup', function(e) {
				if (isCorrectAngle(dAngle)) {
					correctAngle = true;
				} else {
					mousePressed = false;
					dragged = false;
					lastAngle = 0;
					dAngle = 0;
				}
			});

            function isCorrectAngle(angle) {
                return (angle >= 0.68 && angle <= 0.78)
                    || (angle <= -2.35 && angle >= -2.45)
            }

		}

		// Define lines
		var lineAB = {
			point1: {x: 0.3*canvasWidth, y: 0.7*canvasHeight},
			point2: {x: 0.7*canvasWidth, y: 0.3*canvasHeight},
			color: '#6D9EEB',
			width: 5,
			firstLetter: 'A',
			secondLetter: 'B'
		};
		var lineCD = {
			point1: {x: 0.3*canvasWidth, y: 0.3*canvasHeight},
			point2: {x: 0.7*canvasWidth, y: 0.7*canvasHeight},
			color: '#8E7CC3',
			width: 5,
			firstLetter: 'C',
			secondLetter: 'D'
		};

		var intersection = intersectOfLines(lineAB, lineCD);

		// New points for line CD after translation
		var newPoint1 = {
			x: lineCD.point1.x-intersection.x,
			y: lineCD.point1.y-intersection.y
		};
		var newPoint2 = {
			x: lineCD.point2.x-intersection.x,
			y: lineCD.point2.y-intersection.y
		};

		var face01 = $('.face-01');
		var face02 = $('.face-02');
		var definition = $('.lowertextblock p');

		switch (countNext) {
			case 0:
			sound_player_nav("s2_p"+(countNext+1));

				face01.hide(0);
				face02.hide(0);

				var helpTimer = setTimeout(function () {
                    $('.help-button').show(0);
                }, 10000)

				function updateCanvas() {
					ctx.clearRect(0, 0, canvasWidth, canvasHeight);
					ctx.shadowColor = 'black';
					ctx.shadowBlur = 0; // Always 0 for line CD
					ctx.shadowOffsetX = 0;
					ctx.shadowOffsetY = 0;

					drawLine(ctx, lineAB.point1, lineAB.point2, lineAB.color, lineAB.width);
					drawAB();

					// Set shadow blur when mouse is near the line or is being dragged
					if (pDistToLine(lineCD, mouse) < tolerance || dragged) {
						ctx.shadowOffsetX = 3;
						ctx.shadowOffsetY = 3;
						ctx.shadowBlur = 5;
						lineCD.color = '#FEC211';
						$canvas.css({'cursor': 'grab'});
					} else {
						lineCD.color = '#8E7CC3';
						ctx.shadowBlur = 0;
						$canvas.css({'cursor': 'default'});
					}

					if (dragged) {
						$canvas.css({'cursor': 'grabbing'});
						ctx.save();
						ctx.translate(intersection.x, intersection.y);
						ctx.rotate(dAngle);

						if (isCorrectAngle(dAngle)) {
							ctx.shadowColor = 'red';
						}
						drawLine(ctx, newPoint1, newPoint2, lineCD.color, lineCD.width);
						clearShadow();
						drawText(ctx, lineCD.firstLetter, newPoint1, '#000');
						drawText(ctx, lineCD.secondLetter, newPoint2, '#000');
						ctx.restore();
					} else {
						drawLine(ctx, lineCD.point1, lineCD.point2, lineCD.color, lineCD.width);

						clearShadow();
						drawCD();
					}

					if (correctAngle) {
            clearInterval(helpTimer);
						clearShadow();
						handleCorrectAngle();
						face01.show(0);
						$('.help-button').hide(0);
						definition.html(data.string.p2_s2);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("help");
						current_sound.play();
						current_sound.on('complete', function(){
						nav_button_controls(0);
						});
						return; // terminate recursion when mouse released on correct angle
					}

					requestAnimationFrame(updateCanvas);
				}
				requestAnimationFrame(updateCanvas);

				$('.help-button').click( function() {
					correctAngle = true;
					// face02.show(0);
					// $('.instruction-text').html(data.string.p2_s2);
				});

				break;
				case 1:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p2a");
				current_sound.play();
				$(".def >p:eq(0)").show(0);
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p2b");
				current_sound.play();
				$(".def >p:eq(1)").show(0);
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p2c");
				current_sound.play();
				$(".def >p:eq(2)").show(0);
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
		});
				break;
		}

        //function to draw context after correctAngle is set to true
		function handleCorrectAngle() {
			ctx.clearRect(0, 0, canvasWidth, canvasHeight);
			dAngle = (dAngle <= -2.35 && dAngle >= -2.45) ? -2.40855 : 0.733038;
			ctx.shadowBlur = 0;
			drawLine(ctx, lineAB.point1, lineAB.point2, lineAB.color, lineAB.width);
			drawAB();
			ctx.save();
			ctx.translate(intersection.x, intersection.y);
			ctx.rotate(dAngle);
			ctx.shadowBlur = 10;
			ctx.shadowColor = 'red';
			drawLine(ctx, newPoint1, newPoint2, lineCD.color, lineCD.width);
			drawText(ctx, lineCD.firstLetter, newPoint1, '#000');
			drawText(ctx, lineCD.secondLetter, newPoint2, '#000');

			ctx.restore();
			drawDot(ctx, intersection, '#93C47D');
            drawRect();
		}

        // Draw rectangle
        function drawRect() {
            ctx.strokeStyle = ''
            ctx.translate(intersection.x, intersection.y);
            ctx.rotate(64 * Math.PI / 180);
            ctx.rect(-20, -20, 40, 40);
            ctx.stroke();
        }

		// Wrapper functions for drawText
		// used to draw all leterrs of line
		function drawAB() {
			var pointA = {x: lineAB.point1.x - 0.02*canvasWidth, y: lineAB.point1.y + 0.02*canvasHeight};
			drawText(ctx, lineAB.firstLetter, pointA, '#000');
			var pointB = {x: lineAB.point2.x, y: lineAB.point2.y};
			drawText(ctx, lineAB.secondLetter, pointB, '#000');
		}
		function drawCD() {
			var pointC = {x: lineCD.point1.x - 0.02*canvasWidth, y: lineCD.point1.y + 0.02*canvasHeight};
			drawText(ctx, lineCD.firstLetter, pointC, '#000');
			var pointD = {x: lineCD.point2.x, y: lineCD.point2.y + 0.02*canvasHeight};
			drawText(ctx, lineCD.secondLetter, pointD, '#000');
		}

		// Reset shadow for letters C and D
		function clearShadow() {
			ctx.shadowBlur = 0;
			ctx.shadowOffsetX = 0;
			ctx.shadowOffsetY = 0;
		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(countNext==0){
				$prevBtn.hide(0);
				$nextBtn.hide(0);
			}else{
				nav_button_controls();
			}

		});
	}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
		/*always hide next and previous navigation button unless
			explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page, countNext + 1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
		if(countNext==0){
			definition.html(data.string.p2_s2);
		}
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

/**
 * General function to draw line with two points
 */
function drawLine(ctx, point1, point2, color, width) {
	ctx.beginPath();
	ctx.moveTo(point1.x, point1.y);
	ctx.lineTo(point2.x, point2.y);
	ctx.strokeStyle = color;
	ctx.lineWidth = width;
	ctx.stroke();

}

/**
 * General function to draw text at a point
 */
function drawText(ctx, text, point, color) {
	ctx.beginPath();
	ctx.font='1em Arial';
	ctx.fillStyle = color
	ctx.fillText(text, point.x, point.y);
}

/**
 * Calculate the perpendicular distance of a point
 * to a line with two points given
 */
function pDistToLine(line, point) {
	var dy = line.point2.y - line.point1.y;
	var dx = line.point2.x - line.point1.x
	var c = line.point2.x * line.point1.y - line.point2.y * line.point1.x;
	return Math.abs((dy*point.x - dx*point.y + c) / Math.sqrt(dy*dy + dx*dx));
}

/**
 * Draw a dot on a point
 */
function drawDot(ctx, point, color) {
	if (point === undefined) return;
	ctx.beginPath();
	ctx.arc(point.x, point.y, 10, 0, 2 * Math.PI, true);
	ctx.fillStyle = color;
	ctx.fill();
}

/**
 * Get exact mouse position in canvas
 */
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();2
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

/**
 * Find intersection point of two lines
 */
function intersectOfLines(line1, line2) {
	var a = line1.point1.x*line1.point2.y - line1.point1.y*line1.point2.x;
	var b = line2.point1.x*line2.point2.y - line2.point1.y*line2.point2.x;
	var numeratorX = a*(line2.point1.x-line2.point2.x) - (line1.point1.x-line1.point2.x)*b;
	var numeratorY = a*(line2.point1.y-line2.point2.y) - (line1.point1.y-line1.point2.y)*b;
	var denomenator = (line1.point1.x-line1.point2.x)*(line2.point1.y-line2.point2.y) - (line1.point1.y-line1.point2.y) * (line2.point1.x-line2.point2.x);
	return {
		x: numeratorX / denomenator,
		y: numeratorY / denomenator
	}
}

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
/*
	TODO: formatting
*/
var content = [{
	//slide 0
	contentblockadditionalclass: 'background',
	contentblocknocenteradjust : true,
	uppertextblock : [{
		textclass : "firsttitle",
		textdata : data.string.p4_s1
	}]
}, {	// slide 1
	contentblockadditionalclass: 'slide2bg',
	datahighlightflag: true,
	definitionblock: [{
		content: data.string.p1_s8 + ': ' + data.string.p1_s9
	}, {
		content: data.string.p2_s4 + ': ' + data.string.p2_s5
	}, {
		content: data.string.p3_s6 + ': ' + data.string.p3_s7
	}],
	imageblock: [{
		imagestoshow: [{
			imgclass: 'monkey-img',
			imgsrc: imgpath + 'teaching01.png'
		}, {
			imgclass: 'stick-img',
			imgsrc: imgpath + 'stick.png'
		}]
	}]
}, {	//slide 2
	contentblockadditionalclass: 'slide2bg',
	datahighlightflag: true,
	definitionblock: [{
		content: data.string.p1_s8 + ': ' + data.string.p1_s9
	}, {
		content: data.string.p2_s4 + ': ' + data.string.p2_s5
	}, {
		content: data.string.p3_s6 + ': ' + data.string.p3_s7
	}],
	imageblock: [{
		imagestoshow: [{
			imgclass: 'dialog-img',
			imgsrc: imgpath + 'dialogbox.png'
		}, {
			imgclass: 'stick-img',
			imgsrc: imgpath + 'stick.png'
		}, {
			imgclass: 'iamok-img',
			imgsrc: imgpath + 'i_am_ok.png'
		}, {
			imgclass: 'handwithstick-img',
			imgsrc: imgpath + 'hand_with_stick.png'
		}, {
			imgclass: 'intersecting-img',
			imgsrc: imgpath + 'intersecting.png'
		}, {
			imgclass: 'perpendicular-img',
			imgsrc: imgpath + 'perpendicular.png'
		}, {
			imgclass: 'parallel-img',
			imgsrc: imgpath + 'parallel.png'
		}],
		imagelabels: [{
			imagelabelclass: 'dialog-text',
			imagelabeldata: data.string.p4_s2
		}],
		spriteimg: [{
			spriteimgclass: 'monkey-sprite monkey-sprite-animate'
		}]
	}]
}];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s4_p1", src: soundAsset+"S4_P1.ogg"},
      {id: "s4_p2", src: soundAsset+"S4_P2.ogg"},
			{id: "s4_p3", src: soundAsset+"S4_P3_1.ogg"},
			{id: "s4_p4", src: soundAsset+"S4_P3_2.ogg"},
			{id: "s4_p5", src: soundAsset+"Intersecting.ogg"},
			{id: "s4_p6", src: soundAsset+"Perpendicular.ogg"},
			{id: "s4_p7", src: soundAsset+"Parallellines.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();



	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call notifyuser
		// notifyuser($anydiv);



		switch (countNext) {
			case 0:
			sound_player_nav("s4_p"+(countNext+1));
			break;
			case 1:
			$(".def >p:eq(0)").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s4_p5");
			current_sound.play();
			current_sound.on('complete', function(){
			$(".def >p:eq(1)").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s4_p6");
			current_sound.play();
			current_sound.on('complete', function(){
			$(".def >p:eq(2)").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s4_p7");
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
			});
			});
			});
			break;
			case 2:
			$(".def>p").show(0);
        ole.footerNotificationHandler.hideNotification();
        var count = 0;
				sound_player_nav("s4_p3");
				var handWithStickImg = $('.handwithstick-img');
				var iAmOkImg = $('.iamok-img');
				var stick = $('.stick-img');
				var dialogImg = $('.dialog-img');
				var dialogText = $('.dialog-text');
				var monkeySprite = $('.monkey-sprite');
				var definition = $('.definitionblock p');
				var intersectingImg = $('.intersecting-img');
				var perpendicularImg = $('.perpendicular-img');
				var parallelImg = $('.parallel-img');

				handWithStickImg.hide(0);
				iAmOkImg.hide(0);
				intersectingImg.hide(0);
				perpendicularImg.hide(0);
				parallelImg.hide(0);

				var timeout = setTimeout(function() {
					stick.addClass('stick-img-blink');
				}, 5000);

				stick.hover(function() {
					clearTimeout(timeout);
					stick.removeClass('stick-img-blink');
					stick.css({
						'border-color': 'yellow',
						'cursor': 'grab'
					});
				}, function() {
					stick.css({
						'border-color': 'white',
						'cursor': 'pointer'
					});
				});

				stick.on('click', function() {
					dialogText.html(data.string.p4_s3);
					sound_player_nav("s4_p4");
					monkeySprite.removeClass('monkey-sprite-animate');
					monkeySprite.addClass('monkey-fall-sprite');
					monkeySprite.addClass('monkey-fall-sprite-animate');
					stick.unbind('mouseenter mouseleave');
					stick.css({'border-color': 'white'});
					stick.css({
						'transform': 'translateX(50%)'
					});

					setTimeout(function() {
						dialogText.html(data.string.p4_s4);
					}, 3000);
					setTimeout(function() {
						monkeySprite.hide(0);
						dialogText.css({'top': '48%'});
						dialogImg.css({'top': '43%'});
						dialogText.html(data.string.p4_s5);
						iAmOkImg.show(0);
					}, 4000);
					setTimeout(function() {
						iAmOkImg.hide(0);
						dialogText.html(data.string.p4_s6);
						handWithStickImg.show(0);
					}, 6500);
					setTimeout(function() {
						handWithStickImg.hide(0);
						definition.addClass('definitionHover');

						definition.eq(0).one('click', function() {
							dialogText.hide(0);
							dialogImg.hide(0);
							intersectingImg.show(0);
							$(this).addClass('blue-border');
							$(this).css({'cursor': 'default'});
                            count++;
                            checkCount();
						});

						definition.eq(1).one('click', function() {
							dialogText.hide(0);
							dialogImg.hide(0);
							perpendicularImg.show(0);
							$(this).addClass('blue-border');
							$(this).css({'cursor': 'default'})
                            count++;
                            checkCount();
						});

						definition.eq(2).one('click', function() {
							dialogText.hide(0);
							dialogImg.hide(0);
							parallelImg.show(0);
							$(this).addClass('blue-border');
							$(this).css({'cursor': 'default'})
                            count++;
                            checkCount();
						});
					}, 7500)
					stick.hide(0);;
				});

                function checkCount() {
                    console.log(count);
                    if (count >= 3) {
											$prevBtn.show(0);
                        ole.footerNotificationHandler.lessonEndSetNotification();
                    }
                }
			break;
		}



		// Wrapper function for drawText
		// used to draw all leters of line
		function drawAB() {
			var pointA = {x: lineAB.point1.x - 0.02*canvasWidth, y: lineAB.point1.y + 0.02*canvasHeight};
			drawText(ctx, lineAB.firstLetter, pointA, '#000');
			var pointB = {x: lineAB.point2.x, y: lineAB.point2.y};
			drawText(ctx, lineAB.secondLetter, pointB, '#000');
		}

		function findSlope(line) {
			return (line.point2.y - line.point1.y) / (line.point2.x - line.point1.x);
		}

		function findY(slope, line, x) {
			return slope * (x-line.point1.x) + line.point1.y;
		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(countNext==2){
				      ole.footerNotificationHandler.hideNotification();
			}else{
							nav_button_controls();
			}
		});
	}
	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
		/*always hide next and previous navigation button unless
			explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page, countNext + 1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [{
	//slide 0
	containscanvaselement: true,
	lowertextblock: [{
		textdata: data.string.p3_s1
	}],
	imageblock: [{
		imagestoshow: [{
			imgsrc: imgpath + 'sundar_face01.png',
			imgclass: 'face-01'
		}, {
			imgsrc: imgpath + 'sundar_face02.png',
			imgclass: 'face-02'
		}]
	}]
}, {
	//slide 1
	containscanvaselement: true,
	lowertextblock: [{
		textdata: data.string.p3_s4
	}],
	imageblockadditionalclass:"dotimg",
	imageblock: [{
			imagestoshow: [{
				imgsrc: imgpath + 'dot.png',
				imgclass: 'dot1 dot'
			}, {
				imgsrc: imgpath + 'dot.png',
				imgclass: 'dot2 dot'
			},{
				imgsrc: imgpath + 'dot.png',
				imgclass: 'dot3 dot'
			},{
				imgsrc: imgpath + 'dot.png',
				imgclass: 'dot4 dot'
			}, {
				imgsrc: imgpath + 'dot.png',
				imgclass: 'dot5 dot'
			},{
				imgsrc: imgpath + 'dot.png',
				imgclass: 'dot6 dot'
			},{
				imgsrc: imgpath + 'dot.png',
				imgclass: 'dot7 dot'
			}]
		}]
}, {
	//slide 2
	containscanvaselement: true,
	lowertextblock: [{
		textdata: data.string.p3_s5
	}]
}, {
	//page 3
	definitionblock: [{
		content: data.string.p3_s7
	}, {
		content: data.string.p3_s8
	}, {
		content: data.string.p3_s9
	}],
	imageblockadditionalclass: 'summary-imageblock',
	imageblock: [{
		imagestoshow: [{
			imgsrc: imgpath + 'teaching01.png',
			imgclass: 'teaching-img'
		}, {
			imgsrc: imgpath + 'parallel-lines.png',
			imgclass: 'summary-img'
		}],
		imagelabels: [{
			imagelabelclass: 'summary-label',
			imagelabeldata: data.string.p3_s10
		}]
	}]
}];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);


	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s3_p1", src: soundAsset+"S3_P1.ogg"},
      {id: "s3_p1a", src: soundAsset+"S3_P1_2.ogg"},
      {id: "s3_pb", src: soundAsset+"S3_P1_1.ogg"},
      {id: "s3_p2", src: soundAsset+"S3_P2.ogg"},
			{id: "s3_p3", src: soundAsset+"S3_P3.ogg"},
			{id: "s3_p4a", src: soundAsset+"S3_P4_1.ogg"},
			{id: "s3_p4b", src: soundAsset+"S3_P4_2.ogg"},
			{id: "s3_p4c", src: soundAsset+"S3_P4_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();


	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		$('.help-button').hide(0);

		//call notifyuser
		// notifyuser($anydiv);

        $prevBtn.hide(0);
        $nextBtn.hide(0);

        function showNavs() {
            $prevBtn.show(0);
            $nextBtn.show(0);
        }

		// Initializing canvas
		var $canvas = $('#customcanvasbase');
		// Checking if the canvas exists
		var canvas = $canvas[0]; // Canvas element
		if (canvas !== undefined) {
			var ctx = canvas.getContext('2d');
			var canvasHeight = $board.height() * 0.75;
			var canvasWidth = $board.width();
			ctx.canvas.height = canvasHeight;
			ctx.canvas.width = canvasWidth;

			// Initializing mouse variables
			var mouse = {x: 0, y: 0}
			var tolerance = 10;
			var mousePressed = false;
		}

		// Update mouse coordinates on mouse move
		$canvas.on('mousemove', function(e) {
			var mousePoint = getMousePos($canvas[0], e);
			mouse.x = mousePoint.x;
			mouse.y = mousePoint.y;
		});

		var correctClick = false,
			line1Click = false,
			line2Click = false,
			line3Click = false;
		$canvas.on('click', function(e) {
			if (pDistToLine(lineCD, mouse) < tolerance) {
				correctClick = true;
			} else if (pDistToLine(line1, mouse) < tolerance) {
				line1Click = true;
			} else if (pDistToLine(line2, mouse) < tolerance) {
				line2Click = true;
			} else if (pDistToLine(line3, mouse) < tolerance) {
				line3Click = true;
			}
		});

		// Define lines
		var lineAB = {
			point1: {x: 0.3*canvasWidth, y: 0.7*canvasHeight},
			point2: {x: 0.7*canvasWidth, y: 0.3*canvasHeight},
			color: '#00949E',
			width: 5,
			firstLetter: 'A',
			secondLetter: 'B'
		};

		// Making a copy of point A and point B
		var fixedPointA = $.extend({}, lineAB.point1),
			fixedPointB = $.extend({}, lineAB.point2);

		// Correct line
		var lineCD = {
			point1: {x: 0.45*canvasWidth, y: 0.75*canvasHeight},
			point2: {x: 0.85*canvasWidth, y: 0.35*canvasHeight},
			color: '#FFD966',
			width: 5,
			firstLetter: 'C',
			secondLetter: 'D'
		}

		// Making a copy of point C and point D
		var fixedPointC = $.extend({}, lineCD.point1),
			fixedPointD = $.extend({}, lineCD.point2);

		// Options
		var line1 = {
			point1: {x: 0.3*canvasWidth, y: 0.3*canvasHeight},
			point2: {x: 0.7*canvasWidth, y: 0.7*canvasHeight},
			color: '#000',
			width: 5
		};
		var line2 = {
			point1: {x: 0.25*canvasWidth, y: 0.70*canvasHeight},
			point2: {x: 0.55*canvasWidth, y: 0.10*canvasHeight},
			color: 'blue',
			width: 5
		};
		var line3 = {
			point1: {x: 0.65*canvasWidth, y: 0.15*canvasHeight},
			point2: {x: 0.90*canvasWidth, y: 0.50*canvasHeight},
			color: 'pink',
			width: 5
		};

		var helpClick = false;
		$('.help-button').on('click', function() {
			helpClick = true;
		});

		var face01 = $('.face-01');
		var face02 = $('.face-02');

		face01.hide(0);
		face02.hide(0);

		switch (countNext) {
			case 0:
				sound_player("s3_p"+(countNext+1));
				function updateCanvas() {
					setShadow(ctx, 0, 0, 0)
					ctx.clearRect(0, 0, canvasWidth, canvasHeight);
					ctx.shadowColor = '#FAF6EA';
					drawLine(ctx, lineAB);
					drawAB();
					if (pDistToLine(lineCD, mouse) < tolerance) {
						lineCD.color = '#FF9500';
					}
					else{
						lineCD.color = '#FFD966';
					}


					// lineCD
					if (pDistToLine(lineCD, mouse) < tolerance) {
						if (correctClick) lineCD.color = '#5FB703';

					} else {
						setShadow(ctx, 0, 0, 0);
					}
					drawLine(ctx, lineCD);

					// line1
					// Check for line1Click too so that the line 1 once clicked
					// will keep on showing shadowBlur
					if (pDistToLine(line1,mouse) <tolerance) {
						line1.color = '#FF9500';
					}
					else{
						line1.color = '#000';
					}
					if (pDistToLine(line1, mouse) < tolerance || line1Click) {
					//setShadow(ctx, 3, 3, 5);
						if (line1Click) line1.color = 'red';
					} else {
						setShadow(ctx, 0, 0, 0);
					}
					drawLine(ctx, line1);

					// line2
					if (pDistToLine(line2,mouse) <tolerance) {
						line2.color = '#FF9500';
					}
					else{
						line2.color = 'blue';
					}
					if (pDistToLine(line2, mouse) < tolerance || line2Click) {
					//setShadow(ctx, 3, 3, 5);
						if (line2Click) line2.color = 'red';
					} else {
						setShadow(ctx, 0, 0, 0);
					}
					drawLine(ctx, line2);

					// line3

					if (pDistToLine(line3,mouse) < tolerance) {
						line3.color = '#FF9500';
					}
					else{
						line3.color = 'pink';
					}
					if (pDistToLine(line3, mouse) < tolerance || line3Click) {
					//setShadow(ctx, 3, 3, 5);
						if (line3Click) line3.color = 'red';
					} else {
						setShadow(ctx, 0, 0, 0);
					}
					drawLine(ctx, line3);

					// Handle incorrect click
					var incorrectClick = line1Click || line2Click || line3Click;
					if (incorrectClick) {
						$('.help-button').show(0);
					}

					// Handle correct click
					if (correctClick || helpClick) {
						setShadow(ctx, 5, 3, 3);
						lineCD.color = '#5eB703'
						drawLine(ctx, lineCD);

						// Check if help clicked was clicked to reveal answer
						if (helpClick) {
							face02.show(0);
							createjs.Sound.stop();
							$('.lowertextblock p').html(data.string.p3_s2);
							// current_sound = createjs.Sound.play("s3_pb");
							// current_sound.play();
							// current_sound.on('complete', function(){
													$nextBtn.show(0);
												// });
						} else {
							face01.show(0);
							$('.lowertextblock p').html(data.string.p3_s3);
							sound_player_nav("s3_p1a");
						}
						// createjs.Sound.stop();
						// current_sound = createjs.Sound.play("s3_p1a");
						// current_sound.play();
						// current_sound.on('complete', function(){
						$canvas.css({'cursor': 'default'});
                        // $nextBtn.show(0);
											// });
						return;
					}

					requestAnimationFrame(updateCanvas);
				}
				updateCanvas();

				break;

			case 1:
				sound_player_nav("s3_p"+(countNext+1));
				var slopeLines = [findSlope(lineAB), findSlope(line1), findSlope(line2), findSlope(line3)];
				var lines = [lineAB, line1, line2, line3];
				var temp;
				var speed = 2;

				function updateLines() {
					ctx.clearRect(0, 0, canvasWidth, canvasHeight);

					// Draw lien AB and CD
					drawLine(ctx, lineAB);
					drawLine(ctx, lineCD);

					// Draw letter A and B
					drawText(ctx, lineAB.firstLetter, fixedPointA, '#000');
					drawText(ctx, lineAB.secondLetter, fixedPointB, '#000');

					for (var i = 0; i < lines.length; i++) {
						temp = lines[i].point1.x - speed;
						lines[i].point1.y = findY(slopeLines[i], lines[i], temp);
						lines[i].point1.x -= speed;

						temp = lines[i].point2.x + speed;
						lines[i].point2.y = findY(slopeLines[i], lines[i], temp);
						lines[i].point2.x += speed;
						drawLine(ctx, lines[i]);
					}

					// Termination condition
					if (line1.point1.x <= 0 || line1.point2.x >= canvasWidth) {

						return;
					}

					requestAnimationFrame(updateLines);
				}
				updateLines();
				setTimeout(function(){
					$(".dot").show(0);
				},3300);
				break;

			case 2:
				// sound_player_nav("s3_p"+(countNext+1));
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s3_p3");
				current_sound.play();
				current_sound.on('complete', function(){
					drawText(ctx, lineCD.firstLetter, fixedPointC, '#000');
					drawText(ctx, lineCD.secondLetter, fixedPointD, '#000');
					nav_button_controls(1000);
					});
				var linesArray = [lineAB, lineCD];
				var slopeLinesArray = [findSlope(lineAB), findSlope(lineCD)];
				var speed = 2;
				function elongateLines() {
					// Draw letter A and B at fixed points
					drawText(ctx, lineAB.firstLetter, fixedPointA, '#000');
					drawText(ctx, lineAB.secondLetter, fixedPointB, '#000');

					// Draw letters C and D at pixed points
					// 	drawText(ctx, lineCD.firstLetter, fixedPointC, '#000');
					// 	drawText(ctx, lineCD.secondLetter, fixedPointD, '#000');



					for (var i = 0; i < linesArray.length; i++) {
						temp = linesArray[i].point1.x - speed;
						linesArray[i].point1.y = findY(slopeLinesArray[i], linesArray[i], temp);
						linesArray[i].point1.x -= speed;

						temp = linesArray[i].point2.x + speed;
						linesArray[i].point2.y = findY(slopeLinesArray[i], linesArray[i], temp);
						linesArray[i].point2.x += speed;
						drawLine(ctx, linesArray[i]);
					}

					// Termination condition
					if (lineAB.point1.y >= canvasHeight || lineAB.point2.y <= 0) {
						return;
					}

					requestAnimationFrame(elongateLines);
				}
				elongateLines();
				break;
				case 3:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s3_p4a");
				current_sound.play();
				$(".def >p:eq(0)").show(0);
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s3_p4b");
					current_sound.play();
					$(".def >p:eq(1)").show(0);
					current_sound.on('complete', function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s3_p4c");
						current_sound.play();
						$(".def >p:eq(2)").show(0);
						current_sound.on('complete', function(){
							nav_button_controls(0);
						});
					});
				});
				break;
		}



		// Wrapper function for drawText
		// used to draw all letrers of line
		function drawAB() {
			var pointA = {x: lineAB.point1.x - 0.02*canvasWidth, y: lineAB.point1.y + 0.02*canvasHeight};
			drawText(ctx, lineAB.firstLetter, pointA, '#000');
			var pointB = {x: lineAB.point2.x, y: lineAB.point2.y};
			drawText(ctx, lineAB.secondLetter, pointB, '#000');
		}

		function findSlope(line) {
			return (line.point2.y - line.point1.y) / (line.point2.x - line.point1.x);
		}

		function findY(slope, line, x) {
			return slope * (x-line.point1.x) + line.point1.y;
		}

	}

		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}

		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
					nav_button_controls();
			});
		}
	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
		/*always hide next and previous navigation button unless
			explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page, countNext + 1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

/**
 * General function to draw line with two points
 */
function drawLine(ctx, line) {
	ctx.beginPath();
	ctx.moveTo(line.point1.x, line.point1.y);
	ctx.lineTo(line.point2.x, line.point2.y);
	ctx.strokeStyle = line.color;
	ctx.lineWidth = line.width;
	ctx.stroke();

}

/**
 * General function to draw text at a point
 */
function drawText(ctx, text, point, color) {
	ctx.beginPath();
	ctx.font='1em Arial';
	ctx.fillStyle = color
	ctx.fillText(text, point.x, point.y);
}

/**
 * Clear shadow
 */
function setShadow(ctx, offsetX, offsetY, blur) {
	ctx.shadowOffsetX = offsetX;
	ctx.shadowOffsetY = offsetY;
	ctx.shadowBlur = 0;
}

/**
 * Calculate the perpendicular distance of a point
 * to a line with two points given
 */
function pDistToLine(line, point) {
	var dy = line.point2.y - line.point1.y;
	var dx = line.point2.x - line.point1.x
	var c = line.point2.x * line.point1.y - line.point2.y * line.point1.x;
	return Math.abs((dy*point.x - dx*point.y + c) / Math.sqrt(dy*dy + dx*dx));
}

/**
 * Draw a dot on a point
 */
function drawDot(ctx, point, color) {
	if (point === undefined) return;
	ctx.beginPath();
	ctx.arc(point.x, point.y, 10, 0, 2 * Math.PI, true);
	ctx.fillStyle = color;
	ctx.fill();
}

/**
 * Get exact mouse position in canvas
 */
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

/**
 * Find intersection point of two lines
 */
function intersectOfLines(line1, line2) {
	var a = line1.point1.x*line1.point2.y - line1.point1.y*line1.point2.x;
	var b = line2.point1.x*line2.point2.y - line2.point1.y*line2.point2.x;
	var numeratorX = a*(line2.point1.x-line2.point2.x) - (line1.point1.x-line1.point2.x)*b;
	var numeratorY = a*(line2.point1.y-line2.point2.y) - (line1.point1.y-line1.point2.y)*b;
	var denomenator = (line1.point1.x-line1.point2.x)*(line2.point1.y-line2.point2.y) - (line1.point1.y-line1.point2.y) * (line2.point1.x-line2.point2.x);
	return {
		x: numeratorX / denomenator,
		y: numeratorY / denomenator
	}
}

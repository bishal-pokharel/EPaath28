var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[

	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cover",
					imgid : 'cover',
					imgsrc : '',
				}
			]
		}]
	},

	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',
		extratextblock:[
			{
				textdata: data.lesson.chapter,
				textclass: "title",
			},
			{
				textdata: data.string.p1s1,
				textclass: "p1s1",
			}
		],
	},
	// slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass:'bg-blue',
        extratextblock:[
            {
                textdata:  data.string.p3s1,
                textclass: "toptxt ",
            },
            {
                textdata:  data.string.p3s1fst,
                textclass: "text fst ",
            },
            {
                textdata:  data.string.p3s1sec,
                textclass: "text sec ",
            },
            {
                textdata:  data.string.p3s1thrd,
                textclass: "text thrd",
            },
						{
								textdata:  data.string.p1pra,
								textclass: "toptxt down",
						}
        ]

    },

	// slide3
    {
        extratextblock:[{
            textdata:  data.string.p2text1,
            textclass: "text-1",
        }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti",
                    imgsrc : '',
                    imgid : 'niti'
                },
                {
                    imgclass : "book",
                    imgsrc : '',
                    imgid : 'bookcloud'
                },
            ]
        }],
    },


    // slide4
    {
        extratextblock:[{
            textdata:  data.string.p2text2,
            textclass: "text-1",
        }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti-1",
                    imgsrc : '',
                    imgid : 'niti1'
                },
                {
                    imgclass : "asha",
                    imgsrc : '',
                    imgid : 'asha'
                },
                {
                    imgclass : "rs500",
                    imgsrc : '',
                    imgid : 'rs100'
                },
            ]
        }],
    },

    // slide5
    {
        extratextblock:[{
            textdata:  data.string.p2text3,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text4,
                textclass: "principal",
            },
            {
                textdata:  data.string.p2text5,
                textclass: "interest",
            },
            {
                textdata:  data.string.p2text6,
                textclass: "rate",
            },
            {
                textdata:  data.string.p2text7,
                textclass: "borrowed",
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti",
                    imgsrc : '',
                    imgid : 'niti'
                },
                {
                    imgclass : "rs500-1",
                    imgsrc : '',
                    imgid : 'rs100'
                },
            ]
        }],
    },

    // slide6
    {
        extratextblock:[{
            textdata:  data.string.p2text8,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text4,
                textclass: "principal",
            },
            {
                textdata:  data.string.p2text7,
                textclass: "borrowed",
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti",
                    imgsrc : '',
                    imgid : 'niti'
                },
                {
                    imgclass : "rs500-1",
                    imgsrc : '',
                    imgid : 'rs100'
                },
            ]
        }],
    },

    // slide7
    {
        extratextblock:[{
            textdata:  data.string.p2text11,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text12,
                textclass: "text-2",
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti-1",
                    imgsrc : '',
                    imgid : 'niti1'
                },
                {
                    imgclass : "asha",
                    imgsrc : '',
                    imgid : 'asha'
                },
                {
                    imgclass : "emptycloud",
                    imgsrc : '',
                    imgid : 'emptycloud'
                },
            ]
        }],
    },
    //slide 8
    {
        extratextblock:[{
            textdata:  data.string.p2text13,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text4,
                textclass: "principal",
            },
            {
                textdata:  data.string.p2text5,
                textclass: "interest",
            },
            {
                textdata:  data.string.p2text6,
                textclass: "rate",
            },
            {
                textdata:  data.string.p2text7,
                textclass: "borrowed",
            },
            {
                textdata:  data.string.p2text18,
                textclass: "reutrned borrowed",
            },
            {
                textdata:  data.string.p2text17,
                textclass: "addtext ",
                datahighlightflag:true,
                datahighlightcustomclass:'bigfont'
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti",
                    imgsrc : '',
                    imgid : 'niti'
                },
                {
                    imgclass : "rs500-1",
                    imgsrc : '',
                    imgid : 'rs100'
                },
                {
                    imgclass : "rs500-2",
                    imgsrc : '',
                    imgid : 'rs100'
                },
            ]
        }],
    },
//	slide 9
    {
        extratextblock:[{
            textdata:  data.string.p2text19,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text7,
                textclass: "borrowed",
            },
            {
                textdata:  data.string.p2text18,
                textclass: "reutrned borrowed",
            },
            {
                textdata:  data.string.p2text17,
                textclass: "addtext ",
                datahighlightflag:true,
                datahighlightcustomclass:'bigfont'
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti",
                    imgsrc : '',
                    imgid : 'niti'
                },
                {
                    imgclass : "rs500-1",
                    imgsrc : '',
                    imgid : 'rs100'
                },
                {
                    imgclass : "rs500-2",
                    imgsrc : '',
                    imgid : 'rs100'
                },
            ]
        }],
    },
//	slide 10
    {
        extratextblock:[{
            textdata:  data.string.p2text21,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text12,
                textclass: "ten",
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti1",
                    imgsrc : '',
                    imgid : 'niti02'
                },
                {
                    imgclass : "cloud1",
                    imgsrc : '',
                    imgid : 'emptycloud'
                },
            ]
        }],
    },
    //slide 11
    {
        contentblockadditionalclass:'yellow',
        extratextblock:[{
            textdata:  data.string.p2text22,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text23,
                textclass: "board-text fadein",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "blackboard",
                    imgsrc : '',
                    imgid : 'bg-blackboard'
                },
            ]
        }],
    },
//    slide 12
    {
        contentblockadditionalclass:'yellow',
        extratextblock:[{
            textdata:  data.string.p2text25a,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text23,
                textclass: "board-text shown",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            },
            {
                textdata:  data.string.p2text24,
                textclass: "board-text righttext fadein",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "blackboard",
                    imgsrc : '',
                    imgid : 'bg-blackboard'
                },
            ]
        }],
    },
		//newslide
		{
				contentblockadditionalclass:'yellow',
				extratextblock:[{
						splitintofractionsflag: true,
						textdata:  data.string.p2interest,
						textclass: "text-1",
				},
						{
								textdata:  data.string.p2text23,
								textclass: "board-text shown",
								datahighlightflag: true,
								datahighlightcustomclass: 'underlined'
						},
						{
								textdata:  data.string.p2text24,
								textclass: "board-text righttext shown",
								datahighlightflag: true,
								datahighlightcustomclass: 'underlined'
						},
						{
								splitintofractionsflag: true,
								textdata:  data.string.p2text25_anew,
								textclass: "board-text midtext whiteborder fadein",
								datahighlightflag: true,
								datahighlightcustomclass: 'underlined'
						}],
				imageblock:[{
						imagestoshow : [
								{
										imgclass : "blackboard",
										imgsrc : '',
										imgid : 'bg-blackboard'
								},
						]
				}],
		},
    //slide 13
    {
        contentblockadditionalclass:'yellow',
        extratextblock:[{
            splitintofractionsflag: true,
            textdata:  data.string.p2text26,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text23_a,
                textclass: "board-text fadein",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            },
            {
                textdata:  data.string.p2text24,
                textclass: "board-text righttext shown",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            },
            {
                splitintofractionsflag: true,
                textdata:  data.string.p2text25_a,
                textclass: "board-text midtext whiteborder fadeinmed",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "blackboard",
                    imgsrc : '',
                    imgid : 'bg-blackboard'
                },
            ]
        }],
    },
//    slide 14
    {
        contentblockadditionalclass:'yellow',
        extratextblock:[{
            splitintofractionsflag: true,
            textdata:  data.string.p2text27,
            textclass: "text-1",
        },
            {
                textdata:  data.string.p2text23_a,
                textclass: "board-text shown",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            },
            {
                textdata:  data.string.p2text23_b,
                textclass: "board-text1 shown",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            },
            {
                textdata:  data.string.p2text24,
                textclass: "board-text righttext shown",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            },
            {
                splitintofractionsflag: true,
                textdata:  data.string.p2text25_a,
                textclass: "board-text midtext whiteborder shown",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            },
            {
                splitintofractionsflag: true,
                textdata:  data.string.p2text25_b,
                textclass: "board-text midtext1 whiteborder fadeinlong",
                datahighlightflag: true,
                datahighlightcustomclass: 'underlined'
            }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "blackboard",
                    imgsrc : '',
                    imgid : 'bg-blackboard'
                },
            ]
        }],
    },
//    slide 15
    {
        extratextblock:[{
            textdata:  data.string.p2text32,
            textclass: "text-1",
        },
            {
                textdata:  "1",
                textclass: "principal",
            },
            {
                textdata:  "10",
                textclass: "interest",
            },
            {
                textdata:  "100",
                textclass: "rate",
            }
            ],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti",
                    imgsrc : '',
                    imgid : 'niti'
                },
            ]
        }],
    },
    //slide 16
    {
        extratextblock:[{
            textdata:  data.string.p2text29,
            textclass: "text-1",
        }],
        imageblock:[{
            imagestoshow : [
                {
                    imgclass : "bg_full",
                    imgsrc : '',
                    imgid : 'bg-1'
                },
                {
                    imgclass : "niti-2",
                    imgsrc : '',
                    imgid : 'niti'
                },
                {
                    imgclass : "asha",
                    imgsrc : '',
                    imgid : 'asha'
                },
                {
                    imgclass : "rs500-3",
                    imgsrc : '',
                    imgid : 'rs100'
                },
                {
                    imgclass : "rs10",
                    imgsrc : '',
                    imgid : 'rs10'
                },
            ]
        }],
    },
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hasta", src: imgpath+"hasta dai-15.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankbg", src: imgpath+"bank.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankdai", src: imgpath+"bank-hasta-dai-03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankerTlk", src: imgpath+"banker-talking-06-06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hastaTlk", src: imgpath+"hasta-dai-talking05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box1", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box2", src: imgpath+"textbox01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "board", src: imgpath+"board.png", type: createjs.AbstractLoader.IMAGE},
            {id: "squirrel", src: imgpath+"squirrel re-design-08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg-1", src: imgpath+"bg08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "niti", src: imgpath+"niti.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bookcloud", src: imgpath+"bookcloud.png", type: createjs.AbstractLoader.IMAGE},
            {id: "niti1", src: imgpath+"niti03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "asha", src: imgpath+"asha03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bookcloud", src: imgpath+"bookcloud.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rs500", src: imgpath+"rs500.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rs50", src: imgpath+"rs50.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rs100", src: imgpath+"rs100.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rs10", src: imgpath+"rs10.png", type: createjs.AbstractLoader.IMAGE},
            {id: "emptycloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
            {id: "niti02", src: imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg-blackboard", src: imgpath+"blackboard.png", type: createjs.AbstractLoader.IMAGE},

            // soundsa
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p14.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p15.ogg"},
			{id: "sound_15", src: soundAsset+"s1_p16.ogg"},
			{id: "sound_16a", src: soundAsset+"s1_p17_1.ogg"},
			{id: "sound_16b", src: soundAsset+"s1_p17_2.ogg"},
			{id: "sound_17", src: soundAsset+"s1_p18.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_label_image(content, countNext);
		put_card_image(content, countNext);
		put_speechbox_image(content, countNext);
		vocabcontroller.findwords(countNext);
        splitintofractions($board);

        $(".buttonsel").click(function(){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/

					if($(this).hasClass("class1") && $(this).hasClass("forhover")){
						createjs.Sound.stop();
						$(".buttonsel").removeClass('forhover');
						play_correct_incorrect_sound(1);
						$(this).css("background","#98c02e");
						$(this).css("border","5px solid #deef3c");
                        $(this).css("color","white");
						$(this).siblings(".corctopt").show(0);
				        $nextBtn.show(0);
					}
					else{
						createjs.Sound.stop();
						if($(this).hasClass("forhover")){
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
						}
					}
	});

		switch(countNext) {
			case 2:
			$('.down').css({"top":"75%"});
			sound_player("sound_"+(countNext));
				break;

			case 5:
								sound_player_nav("sound_"+(countNext));
                $('.text-1').css('bottom','77%');
                $('.principal,.interest,.rate').click(function(){
									createjs.Sound.stop();
                    if($(this).hasClass('principal'))
                    {
                        var $this = $(this);
                        var position = $this.position();
                        var width = $this.width();
                        var height = $this.height();
                        var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
                        var centerY = ((position.top + height)*100)/$board.height()+'%';
                        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'correct.png" />').insertAfter(this);
                        $(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
                        $('.principal,.interest,.rate').css({"pointer-events":"none"});
												play_correct_incorrect_sound(1);

                        nav_button_controls(200);
                    }
                    else{
                        var $this = $(this);
                        var position = $this.position();
                        var width = $this.width();
                        var height = $this.height();
                        var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
                        var centerY = ((position.top + height)*100)/$board.height()+'%';
                        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
                        $(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
												play_correct_incorrect_sound(0);

									  }
                });

				break;

			case 8:
								sound_player_nav("sound_"+(countNext));
                $('.text-1').css('bottom','77%');
                $('.principal,.interest,.rate').click(function(){
									createjs.Sound.stop();
                    if($(this).hasClass('rate'))
                    {
                        var $this = $(this);
                        var position = $this.position();
                        var width = $this.width();
                        var height = $this.height();
                        var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
                        var centerY = ((position.top + height)*100)/$board.height()+'%';
                        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'correct.png" />').insertAfter(this);
                        $(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
                        $('.principal,.interest,.rate').css({"pointer-events":"none"});
                        play_correct_incorrect_sound(1);
                        nav_button_controls(200);
                    }
                    else{
                        var $this = $(this);
                        var position = $this.position();
                        var width = $this.width();
                        var height = $this.height();
                        var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
                        var centerY = ((position.top + height)*100)/$board.height()+'%';
                        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
                        $(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
                        play_correct_incorrect_sound(0);
                    }
                });
				break;

            case 14:
            case 15:
                $(".underlined").eq(1).addClass("nounderline");
                $(".whiteborder").find(".bottom").addClass("bottombrd");
								sound_player("sound_"+(countNext));
                break;
            case 16:
								sound_player_nav("sound_16a");
                $('.principal,.interest,.rate').click(function(){
										createjs.Sound.stop();
                    if($(this).hasClass('interest'))
                    {
                        play_correct_incorrect_sound(1);
                        $(".text-1").html(data.string.p2text33);
                        var $this = $(this);
                        var position = $this.position();
                        var width = $this.width();
                        var height = $this.height();
                        var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
                        var centerY = ((position.top + height)*100)/$board.height()+'%';
                        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'correct.png" />').insertAfter(this);
                        $(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
                        $('.principal,.interest,.rate').css({"pointer-events":"none"});
                        setTimeout(function(){
													sound_player("sound_16b");
												},700);
                    }
                    else{
                        play_correct_incorrect_sound(0);
                        var $this = $(this);
                        var position = $this.position();
                        var width = $this.width();
                        var height = $this.height();
                        var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
                        var centerY = ((position.top + height)*100)/$board.height()+'%';
                        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
                        $(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
                    }
                });
							break;
			default:
				sound_player("sound_"+(countNext));
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//alert("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_label_image(content, count){
		if(content[count].hasOwnProperty('imageandlabel')){
			var imageandlabel = content[count].imageandlabel;
			for(var i=0; i<imageandlabel.length; i++){
				var image_src = preload.getResult(imageandlabel[i].imgid).src;
				console.log(image_src);
				var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		// for testing purpose
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		// testing purpose code ends
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
});

var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 0	
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.excq1,
				textclass : 'topbox',
				datahighlightflag:true,
	            datahighlightcustomclass:"color",
			}
		],
		extratextblock:[
			{
				textdata : data.string.exctext2,
				textclass : 'passage2',
			},
			{
				textdata : data.string.exctext1,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
		    textdata : data.string.xq1op3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.xq1op2,
			optionclass : 'class2'
		},
		{
			textdata : data.string.xq1op1,
			optionclass : 'class1'
			
		}
		]
	},
	//slide 1
	
	{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq2,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q2exctext2,
				textclass : 'passage2',
			},
				{
					textdata : data.string.q2exctext1 ,
					textclass : 'passage1',
				}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				textdata : data.string.xq2op1,
				optionclass : 'class1'
			},
			{
				textdata : data.string.xq2op2,
				optionclass : 'class2'
			},
			{
				textdata : data.string.xq2op3,
				optionclass : 'class3'
			}],
		},
		//slide 2
		{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq3,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q3exctext2 ,
				textclass : 'passage2',
			},
				{
					textdata : data.string.q3exctext1,
					textclass : 'passage1',
				}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				textdata : data.string.xq3op2,
				optionclass : 'class2'
			},
			{
	
				textdata : data.string.xq3op1,
				optionclass : 'class1'			
			},
			{
				textdata : data.string.xq3op3,
				optionclass : 'class3'
			}],
		},
		//slide 3
		{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq4,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q4exctext1,
				textclass : 'passage1',
			},
				{
					textdata : data.string.q4exctext2,
					textclass : 'passage2',
				}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				textdata : data.string.xq4op3,
				optionclass : 'class3'
			},
			{
				textdata : data.string.xq4op2,
				optionclass : 'class2'
			},
			{
	
				textdata : data.string.xq4op1,
				optionclass : 'class1'			
			}],
		},
		//slide 4
		{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq5,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q5exctext1,
				textclass : 'passage1',
			},
				{
					textdata : data.string.q5exctext2,
					textclass : 'passage2',
				}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [
			{
				textdata : data.string.xq5op3,
				optionclass : 'class3'			
			},
			{
				textdata : data.string.xq5op2,
				optionclass : 'class2'
			},
			{
				textdata : data.string.xq5op1,
				optionclass : 'class1'			
			}],
		},
			//slide 5
		{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq6,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q6exctext1,
				textclass : 'passage1',
			},{
				textdata : data.string.q6exctext2,
				textclass : 'passage2',
			}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [
			{
				textdata : data.string.xq6op3,
				optionclass : 'class3'			
			},
			{
				textdata : data.string.xq6op2,
				optionclass : 'class2'
			},
			{
				textdata : data.string.xq6op1,
				optionclass : 'class1'		
			}],
		},
				//slide 6
		{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq7,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q7exctext1,
				textclass : 'passage1',
			},
			{
				textdata : data.string.q7exctext2,
				textclass : 'passage2',
			}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [
			{
				textdata : data.string.xq7op3,
				optionclass : 'class3'			
			},
			{
				textdata : data.string.xq7op2,
				optionclass : 'class2'
			},
			{
				textdata : data.string.xq7op1,
				optionclass : 'class1'		
			}],
		},		//slide 4
		{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq8,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q8exctext1,
				textclass : 'passage1',
			},
			{
				textdata : data.string.q8exctext2,
				textclass : 'passage2',
			}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [
			{
				textdata : data.string.xq8op3,
				optionclass : 'class3'			
			},
			{
				textdata : data.string.xq8op2,
				optionclass : 'class2'
			},
			{
				textdata : data.string.xq8op1,
				optionclass : 'class1'		
			}],
		},
				//slide 4
		{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq9,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q9exctext1,
				textclass : 'passage1',
			},
			{
				textdata : data.string.q9exctext2,
				textclass : 'passage2',
			}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [
			{
				textdata : data.string.xq9op3,
				optionclass : 'class3'			
			},
			{
				textdata : data.string.xq9op2,
				optionclass : 'class2'
			},
			{
				textdata : data.string.xq9op1,
				optionclass : 'class1'		
			}],
		},
				//slide 4
		{	
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [
			{
				textdata : data.string.excq10,
				textclass : 'topbox',
				datahighlightflag:true,
				datahighlightcustomclass:"color",
			}],
			extratextblock:[
			{
				textdata : data.string.q10exctext1,
				textclass : 'passage1',
			},
			{
				textdata : data.string.q10exctext2,
				textclass : 'passage2',
			}],
			optionsblockadditionalclass: 'img_options',
			optionsblock : [
			{
				textdata : data.string.xq10op3,
				optionclass : 'class3'			
			},
			{
				textdata : data.string.xq10op2,
				optionclass : 'class2'
			},
			{
				textdata : data.string.xq10op1,
				optionclass : 'class1'		
			}],
		}
	
		
	
];

//content.shufflearray();

$(function () 
{	
	var $board    = $('.board');
	var $nextBtn  = $("#activity-page-next-btn-enabled");
	var $prevBtn  = $("#activity-page-prev-btn-enabled");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
	var rhino = new RhinoTemplate();
   
	rhino.init($total_page);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);		
		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$("#box_icon_rhino").hide(0);
				//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	   } 
        
        $('.passage1').hide(0);
        $('.passage2').hide(0);
		$('.topbox').prepend(countNext+1+". ");
		var wrong_clicked = false;
		$(".optioncontainer").click(function(){
			$('.passage1').show(0);
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				
        		$('.passage2').show(0);
				$(this).children('.correct-icon').show(0);
				$(".optioncontainer").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).children('.incorrect-icon').show(0);
				$(this).css({
					'background-color': '#FF0000',
					'border': '3px solid #980000'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		}); 
		switch (countNext){
			case 0:
					var random = Math.floor(Math.random() * (49 - 26 + 1)) + 26;
					$('.interestnumber').html(random*100);
					$('.interestnumber1').html((random*.1*100).toFixed(2));
					$('.interestnumber2').html((random*.01*100).toFixed(2));
					$('.interestnumber3').html(random*100);
			break;
			case 1:
					var random = Math.floor(Math.random() * (75 - 50 + 1)) + 50;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.15*100).toFixed(2));
					$('.interestnumber1').html((random*.3*100).toFixed(2));
					$('.interestnumber2').html((random*.03*100).toFixed(2));
					$('.interestnumber3').html((random*.15*100).toFixed(2));
			
			break;
			case 2:
					var random = Math.floor(Math.random() * (75 - 50 + 1)) + 50;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.1*100).toFixed(2));
					$('.interestnumber1').html((random*.5*100).toFixed(2));
					$('.interestnumber2').html((random*.05*100).toFixed(2));
					$('.interestnumber3').html((random*.1*100).toFixed(2));
			
			break;
			case 3:
					var random = Math.floor(Math.random() * (100 - 80 + 1)) + 80;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.12*100).toFixed(2));
					$('.interestnumber1').html((random*1.08*100).toFixed(2));
					$('.interestnumber2').html((random*.108*100).toFixed(2));
					$('.interestnumber3').html((random*.12*100).toFixed(2));
			
			break;
			case 4:
					var random = Math.floor(Math.random() * (100 - 80 + 1)) + 80;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.12*100).toFixed(2));					
					$('.interestnumberhint2').html((random*.01*100).toFixed(2));
					$('.interestnumber1').html((random*.06*100).toFixed(2));
					$('.interestnumber2').html((random*.6*100).toFixed(2));
					$('.interestnumber3').html((random*.12*100).toFixed(2));
			
			break;
			case 5:
					var random = Math.floor(Math.random() * (60 - 40 + 1)) + 80;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.03*100).toFixed(2));					
					$('.interestnumberhint2').html((random*.0025*100).toFixed(2));
					$('.interestnumber1').html((random*.015*100).toFixed(2));
					$('.interestnumber2').html((random*.15*100).toFixed(2));
					$('.interestnumber3').html((random*.3*100).toFixed(2));
			
			break;
			case 6:
					var random = Math.floor(Math.random() * (60 - 40 + 1)) + 40;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.12*100).toFixed(2));					
					$('.interestnumberhint2').html((random*.01*100).toFixed(2));
					$('.interestnumber1').html((random*.03*100).toFixed(2));
					$('.interestnumber2').html((random*1.2*100).toFixed(2));
					$('.interestnumber3').html((random/10*100).toFixed(2));
			
			break;
			case 7:
					var random = Math.floor(Math.random() * (90 - 70 + 1)) + 70;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.06*100).toFixed(2));					
					$('.interestnumberhint2').html((random*.005*100).toFixed(2));
					$('.interestnumber1').html((random*.04*100).toFixed(2));
					$('.interestnumber2').html((random*.4*100).toFixed(2));
					$('.interestnumber3').html((random*.06*100).toFixed(2));
			
			break;
			case 8:
					var random = Math.floor(Math.random() * (90 - 70 + 1)) + 70;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.14*100).toFixed(2));					
					$('.interestnumberhint2').html((random*(.14/12)*100).toFixed(2));
					$('.interestnumber1').html((random*.105*100).toFixed(2));
					$('.interestnumber2').html((random*1.05*100).toFixed(2));
					$('.interestnumber3').html((random*.14*100).toFixed(2));
			
			break;
			case 9:
					var random = Math.floor(Math.random() * (100 - 80 + 1)) + 80;
					$('.interestnumber').html(random*100);					
					$('.interestnumberhint1').html((random*.15*100).toFixed(2));					
					$('.interestnumberhint2').html((random*(.15/12)*100).toFixed(2));
					$('.interestnumber1').html((random*.125*100).toFixed(2));
					$('.interestnumber2').html((random*1.25*100).toFixed(2));
					$('.interestnumber3').html((random*100/10).toFixed(2));
			
			break;
		}
	}
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	
});
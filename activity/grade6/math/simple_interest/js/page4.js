var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
//slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",

		extratextblock:[
		{
           textclass: "uptxt",
		   textdata:data.string.p4s1
		}
	],	
	exerciseblock:[
		{
		exerciseblock_container: "block1",
		optionsdivclass:"optionsdiv op1",
		questioncls:[
				{			
					ques_class:"question qn1",
					textdata:data.string.p4qn1,
				}
		],
		exeoptions:[
				{
				  forshuffle:"class1",
				  optdata:data.string.p4qn1op1
				},
				{
					forshuffle:"class2",
					optdata:data.string.p4qn1op2
				},
				{
					forshuffle:"class3",
					optdata:data.string.p4qn1op3
				}
		    ]
		},
		{
		exerciseblock_container: "block2",
		optionsdivclass:"optionsdiv op2",
		questioncls:[
				{			
					ques_class:"question qn2",
					textdata:data.string.p4qn2,
				}
		],
		exeoptions:[
				{
				  forshuffle:"class1",
				  optdata:data.string.p4qn2op1
				},
				{
					forshuffle:"class2",
					optdata:data.string.p4qn2op2
				},
				{
					forshuffle:"class3",
					optdata:data.string.p4qn2op3
				}
		    ]
		},
		{
		exerciseblock_container: "block3",			
		// ques_class:"question qn3",
		// textdata:data.string.p4qn3,
		optionsdivclass:"optionsdiv op3",
		questioncls:[
				{			
					ques_class:"question qn3",
					textdata:data.string.p4qn3,
				}
		],
		exeoptions:[
				{
				  forshuffle:"class1",
				  optdata:data.string.p4qn3op1
				},
				{
					forshuffle:"class2",
					optdata:data.string.p4qn3op2
				},
				{
					forshuffle:"class3",
					optdata:data.string.p4qn3op3
				}
		    ]
		}

	]

},
//slide1
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bgclr",

		extratextblock:[
		{
           textclass: "uptxt",
		   textdata:data.string.p4s1
		},
		{
           textclass: "midtxt",
		   textdata:data.string.p4s1midtxt
		}
	]

},//slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",

		extratextblock:[
		{
           textclass: "uptxt",
		   textdata:data.string.p4qntxt
		}
	],	
	exerciseblock:[
		{
		exerciseblock_container: "block1",
		optionsdivclass:"optionsdiv op1",
		questioncls:[
				{			
					ques_class:"question qn1",
					textdata:data.string.p4qn4,
				}
		],
		exeoptions:[
				{
				  forshuffle:"class1",
				  optdata:data.string.p4qn4op1
				},
				{
					forshuffle:"class2",
					optdata:data.string.p4qn4op2
				},
				{
					forshuffle:"class3",
					optdata:data.string.p4qn4op3
				}
		    ],
		hintcls:[
				{			
					hintclass:"hint hint1",
					textdata:data.string.p4qn4lstxt,
				}
		 	]
		},
		{
		exerciseblock_container: "block2 s2",
		optionsdivclass:"optionsdiv op2",
		questioncls:[
				{			
					ques_class:"question qn2",
					textdata:data.string.p4qn5,
				}
		],
		exeoptions:[
				{
				  forshuffle:"class1",
				  optdata:data.string.p4qn5op1
				},
				{
					forshuffle:"class2",
					optdata:data.string.p4qn5op2
				},
				{
					forshuffle:"class3",
					optdata:data.string.p4qn5op3
				}
		    ],
		hintcls:[
				{			
					hintclass:"hint hint2",
					textdata:data.string.p4qn5lstxt,
				}
		 	]
		}

	]

}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl1", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		
		var count=1;
		var corcnt=0;
		
		var parent=$(".op"+count);
		//alert(parent);
		var divs = parent.children().not(".qnclass");
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	    }	
		$(".buttonsel").click(function(){
			/*class 1 is always for the right answer. updates scoreboard and disables other click if
			right answer is clicked*/

			if($(this).hasClass("class1") && $(this).hasClass("forhover")){
				count++;	
				play_correct_incorrect_sound(1);
				//ramdomoze and show
				var parent=$(".op"+count);
				//alert(parent);
				var divs = parent.children().not(".qnclass");
				while (divs.length) {
			    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			    }			
	            $('.block'+count).show(0);	            
			    $('.hint'+(count-1)).show(0); 
			    //randomoze and show end      	
				$(this).css("background","#98c02e");
				$(this).css("border","5px solid #deef3c");
                $(this).css("color","white");                
				$(this).siblings(".corctopt").show(0);
                var pgcls='.op'+(count-1);
                corcnt--;
				navcntrl(corcnt);
                $(pgcls+" > .optionscontainer ").children().removeClass("forhover");
				  
			}
			else{
				if($(this).hasClass("forhover")){
				play_correct_incorrect_sound(0);
				$(this).css("background","#FF0000");
				$(this).css("border","5px solid #980000");
				$(this).css("color","white");
				$(this).siblings(".wrngopt").show(0);
					wrngClicked = true;
					}
				}
			});
			function navcntrl(corcnt){
                	//alert(corcnt);
                	if(corcnt==0){
						nav_button_controls(100);             		
                	}
                }
		switch (countNext) {
			case 0:
				$('.block2').hide(0);
				$('.block3').hide(0);
				corcnt=3;
			break;
			case 1:
				nav_button_controls(100);
				break;
			case 2:
			    $('.hint1').hide(0);			    
			    $('.hint2').hide(0);
				$('.block2').hide(0);
				corcnt=2;
			break;
			default:
				$prevBtn.show(0);
				nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

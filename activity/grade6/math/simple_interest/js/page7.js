var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass:'bg',
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p7s0,
			textclass : 'txt1',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow : [
			 {			 	
				imgclass: 'squirrel',
				imgid : 'squirrel',
				imgsrc: '',
			 }
			]
		}]
		
	},
	// slide1
	{
		contentblockadditionalclass:'bg',
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p7s1,
			textclass : 'txt1',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow : [
			 {			 	
				imgclass: 'squirrel',
				imgid : 'squirrel',
				imgsrc: '',
			 }
			]
		}]
		
	},
	// slide2
	{
		contentblockadditionalclass:'bg',
		extratextblock:[
			{
				
				datahighlightflag:true,
				datahighlightcustomclass:"red",
				textdata:  data.string.p7s2,
				textclass: "toptxt ",
			}
		],
		speechbox:[{
			speechbox: 'sp-2',
			datahighlightflag:true,
			datahighlightcustomclass:"red",
			textdata : data.string.p7s2box,
			textclass : 'txt1',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow : [
			 {			 	
				imgclass: 'squirrel1',
				imgid : 'squirrel',
				imgsrc: '',
			 }
			]
		}]
		
	},
// slide3
	{
		contentblockadditionalclass:'bg',
		extratextblock:[
			{
				datahighlightflag:true,
				datahighlightcustomclass:"red",
				textdata:  data.string.p7s3,
				textclass: "toptxt ",
			}
		],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p7s3box,
			textclass : 'txt1 red',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow : [
			 {			 	
				imgclass: 'squirrel1',
				imgid : 'squirrel',
				imgsrc: '',
			 }
			]
		}]
		
	},
// slide4
	{
		contentblockadditionalclass:'bg',
		extratextblock:[
			{
				datahighlightflag:true,
				datahighlightcustomclass:"red",
				textdata:  data.string.p7s4,
				textclass: "toptxt ",
			},
			{
				textdata:  data.string.p7s4box,
				textclass: "btmtxt",
			}
		]		
	},
// slide5
	{
		contentblockadditionalclass:'bg',
		extratextblock:[
			{
				datahighlightflag:true,
				datahighlightcustomclass:"red",
				textdata:  data.string.p7s5,
				textclass: "toptxt ",
			}
		],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p7s5box,
			textclass : 'txt1 red',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow : [
			 {			 	
				imgclass: 'squirrel1',
				imgid : 'squirrel',
				imgsrc: '',
			 }
			]
		}]
		
	},
// slide6
	{
		contentblockadditionalclass:'bg',
		extratextblock:[
			{
				datahighlightflag:true,
				datahighlightcustomclass:"red",
				textdata:  data.string.p7s6,
				textclass: "toptxt ",
			}
		],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p7s6box,
			textclass : 'txt1 red',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow : [
			 {			 	
				imgclass: 'squirrel1',
				imgid : 'squirrel',
				imgsrc: '',
			 }
			]
		}]
		
	},
// slide7
	{
		contentblockadditionalclass:'bg',
		extratextblock:[
			{
				datahighlightflag:true,
				datahighlightcustomclass:"red",
				textdata:  data.string.p7s7,
				textclass: "toptxt ",
			},
			{
				textdata:  data.string.p7s7box,
				textclass: "btmtxt-s7",
			}
		]		
	},
	// slide8
	{
		contentblockadditionalclass:'bg',
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p7s8,
			textclass : 'txt1 red',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow : [
			 {			 	
				imgclass: 'squirrel',
				imgid : 'squirrel',
				imgsrc: '',
			 }
			]
		}]
		
	},
	// slide9
	{
		contentblockadditionalclass:'bg',
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p7s9,
			textclass : 'txt1',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow : [
			 {			 	
				imgclass: 'squirrel',
				imgid : 'squirrel',
				imgsrc: '',
			 }
			]
		}]
		
	},
// slide10
	{
		contentblockadditionalclass:'bg',
		extratextblock:[
			{
				textdata:  data.string.p7s10,
				textclass: "toptxt ",
			},
			{
				textdata:  data.string.p7s10box,
				textclass: "btmtxt-s7",
			}
		]		
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg1", src: imgpath+"bg09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel re-design-08.png", type: createjs.AbstractLoader.IMAGE},
			
			{id: "box1", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "sound_1", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
			switch (countNext) {
				case 0:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 1:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 2:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 3:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 4:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 5:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 6:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 7:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 8:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 9:
					$prevBtn.show(0);
					$nextBtn.show(0);
				break;
				case 10:
					$prevBtn.show(0);
					nav_button_controls(1000);
				break;
				default:
				$prevBtn.show(0);
					nav_button_controls(1000);
					break;
			}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			// nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//alert("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

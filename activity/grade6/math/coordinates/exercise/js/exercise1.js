var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var prev_answers = [];

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
	//slide 0	
	{	
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.etext1,
			textclass : 'instruction'
		}],
		numberlineadditionalclass: 'ones_line',
		numberline : [
			{
				textdata : data.string.etext2,
				textclass : 'num_9',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_8',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_7',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_6',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_5',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_4',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_3',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_2',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_1',
			},
			{
				textdata : data.string.etext2,
				textclass : 'num_0',
			}
		],
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
    var current_sound;
	var $total_page = 10;
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "sound_1", src: soundAsset+"ex_instr.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();
	// ---- variables for canvas --------	
	var canvas;
    var canvas_2;
    var canvas_3;
    
    var ctx;
    var ctx_2;
    var ctx_3;
    
    var arr_answers = [];
    var arr_incorrect = [];
    var arr_correct = [];
    
    var current_x;
    var current_y;
    
    var current_incorrect_x;
    var current_incorrect_y;
    var hint_number = 0;
    
    var dot_radius = 5;
    
    var draw_green_circle = false;
    
	/*  
	 *  Generate random co-ordinates for the questions
	 */
	for( var i =0; i<10; i++){
		var c1 = ole.getRandom(1, 12, 0)[0];
		var c2 = ole.getRandom(1, 12, 0)[0];
		var arr_temp = [c1, c2];
		var k = arr_answers.length;
		if(arr_answers.length != 0){
			for(var j =0; j < k; j++){
				while( (arr_temp[0] == arr_answers[j][0]) && (arr_temp[1] == arr_answers[j][1]) ){
					c2 = ole.getRandom(1, 12, 0)[0];
					arr_temp = [c1, c2];
				}
			}
			arr_answers.push(arr_temp);
		} else {
			arr_answers.push(arr_temp);
		}
	}
	
	
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	var score = 0;
	var testin = new EggTemplate();
   
 	testin.init(10);
    function sound_player(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);
		
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
		$('.exefin').hide(0);
		
		canvas = document.getElementById("canvasdrawblock");
	    canvas_2 = document.getElementById("canvasgridblock");
	    canvas_3 = document.getElementById("canvashintblock");
	        
		ctx = canvas.getContext("2d");
	    ctx_2 = canvas_2.getContext("2d");
	    ctx_3 = canvas_3.getContext("2d");
		countNext==0?sound_player("sound_1"):"";
		switch(countNext) {
			default:
				for(var i = 0; i<10; i++){
					$('.num_'+i).html('(' + arr_answers[i][0] + ', ' + arr_answers[i][1] + ')');
				}
				init_canvas();
				break;
		}
		
		//click function for options
		
		$('.line_element>p').click(function(){
			draw_green_circle = false;
			redraw_canvas();
			$('.line_element>p').removeClass('number_highlight');
			$(this).addClass('number_highlight');
			current_x = parseInt( $(this).html().substring($(this).html().lastIndexOf("(")+1, $(this).html().lastIndexOf(",")) );
			current_y = parseInt( $(this).html().substring($(this).html().lastIndexOf(" ")+1, $(this).html().lastIndexOf(")")) );
			$('#canvasdrawblock').css('pointer-events', 'all');
		});
		
		$('#canvasdrawblock').click(function(e){
			var mousePos = getMousePos(canvas, e);
			plot_on_canvas(mousePos.x, mousePos.y);
		});
		
		//egg functions
		
		$('#egg' + countNext).addClass('eggmove');
		
	}
	
	/*	canvas functions	*/
	function getMousePos(canvas, evt) {
	 	var rect = canvas.getBoundingClientRect();
	 	return {
	 		x: evt.clientX - rect.left,
	 		y: evt.clientY - rect.top
	 	};
	}
	
	function init_canvas(){
		canvasheight = ($board.height() * 0.75);
		canvaswidth = ($board.height() * 0.75);
		
		canvas.height = canvasheight;
		canvas.width = canvaswidth;
		
		canvas_2.height = canvasheight;
		canvas_2.width = canvaswidth;
		
		canvas_3.height = canvasheight;
		canvas_3.width = canvaswidth;
		
		dot_radius = Math.ceil(canvaswidth/100);
		
		ctx.clearRect(0, 0, canvaswidth, canvasheight);
		ctx_2.clearRect(0, 0, canvaswidth, canvasheight);
		ctx_3.clearRect(0, 0, canvaswidth, canvasheight);
		
		
		// for grid
		for (var i=1; i< 14; i++){
	    	ctx_2.beginPath();
	    	ctx_2.strokeStyle = '#8EDEDE';
	    	ctx_2.moveTo(i*canvas.width/14, canvas.height/14);
	    	ctx_2.lineTo(i*canvas.width/14, canvas.height - canvas.height/14);
	    	ctx_2.stroke();
		}
		for (var i=1; i< 14; i++){
	    	ctx_2.beginPath();
	    	ctx_2.strokeStyle = '#8EDEDE';
	    	ctx_2.moveTo(canvas.width/14, i*canvas.height/14);
	    	ctx_2.lineTo(canvas.width - canvas.width/14,  i*canvas.height/14);
	    	ctx_2.stroke();
		}
		
		// for numbers in grid
		for (var m=0; m < 13; m++){
			ctx_2.fillStyle = '#555555';
    		ctx_2.font="2vmin Georgia";
			ctx_2.fillText(m, (m+1)*canvas.width/14 - 5, canvas.height - canvas.height/28);
		}
		for (var m=1; m < 13; m++){
			ctx_2.fillStyle = '#555555';
    		ctx_2.font="2vmin Georgia";
			ctx_2.fillText((13-m), canvas.width/28, m*canvas.height/14 + 3);
		}
		
		//for big axes
		ctx_2.beginPath();
    	ctx_2.strokeStyle = '#8EDEDE';
    	ctx_2.fillStyle = '#8EDEDE';	
    	ctx_2.moveTo(canvas.width/14, 0);
    	ctx_2.lineTo(canvas.width*9/140, canvas.height*7/140);	
    	ctx_2.lineTo(canvas.width*11/140, canvas.height*7/140); 
    	ctx_2.fill();
    	ctx_2.moveTo(canvas.width/14, canvas.height*7/140);
    	ctx_2.lineTo(canvas.width/14, canvas.height/14);
    	ctx_2.stroke();
    	
    	ctx_2.moveTo(canvas.width, 13*canvas.height/14);
    	ctx_2.lineTo(canvas.width*133/140, canvas.height*131/140);	
    	ctx_2.lineTo(canvas.width*133/140, canvas.height*129/140); 
    	ctx_2.fill();
    	ctx_2.moveTo(canvas.width*135/140, 13*canvas.height/14);
    	ctx_2.lineTo(13*canvas.width/14, 13*canvas.height/14);
    	ctx_2.stroke();
	}  	
	
	function plot_on_canvas(x,y){
		//check if point is inside the graph
		var condition_inside_canvas = (x >= (canvas.width/14 - canvas.width/52))&&(x <= (13*canvas.width/14 + canvas.width/52))&&(y >= (canvas.height/14 - canvas.height/52))&&(y <= (13*canvas.height/14 + canvas.height/52));
		if( condition_inside_canvas ){
			var x_pos = Math.round( ((x/(canvas.width/14))-1)*100 )/100;
			var y_pos = Math.round( (13-(y/(canvas.height/14)))*100 )/100;
			//bool to define the acceptable range from coordinate points
			var condition_2 = ( (x_pos-Math.round(x_pos)) < 0.3 ) && ( (y_pos-Math.round(y_pos)) < 0.3 );
			if(condition_2){
				//checking ig answer is correct
				var correct_plot = ( Math.round(x_pos) == current_x) && ( Math.round(y_pos) == current_y );
				var temp_1 = [Math.round(x_pos), Math.round(y_pos)];
				current_sound.stop();
				play_correct_incorrect_sound(1);
				if(correct_plot){
					//eggs and canvas
					if(hint_number == 0){	
						$('.number_highlight').addClass('correct_border');
						$('.number_highlight~.right').show(0);
						arr_correct.push(temp_1);
						testin.update(true);						
						draw_green_circle = false;
					} else {
						draw_green_circle = true;
						arr_incorrect.push([current_x, current_y]);
						testin.update(false);						
						$('.number_highlight').addClass('incorrect_border');
						$('.number_highlight~.wrong').show(0);
					}
					//reset hint number
					hint_number = 0;
					$('#canvasdrawblock').css('pointer-events', 'none');
                    current_sound.stop();
					play_correct_incorrect_sound(1);
					testin.gotoNext();
					
					countNext++;
					//removing current incorrect dots
					current_incorrect_x = 100;
					current_incorrect_y = 100;
					//check if all answers are correct
					$('.line_element>p').css('pointer-events', 'all');
				} else {
                    current_sound.stop();
					play_correct_incorrect_sound(0);
					draw_green_circle = false;
					play_correct_incorrect_sound(0);
					current_incorrect_x = temp_1[0];
					current_incorrect_y = temp_1[1];					
					if(hint_number == 0){
						hint_number = 1;
						$('.line_element>p').css('pointer-events', 'none');
					} else if(hint_number == 1) {
						hint_number = 2;
					} else if(hint_number > 1) {
						//removing current incorrect dots
						current_incorrect_x = 100;
						current_incorrect_y = 100;
						arr_incorrect.push([current_x, current_y]);
						testin.update(false);						
						$('#canvasdrawblock').css('pointer-events', 'none');
						$('.line_element>p').css('pointer-events', 'all');	
						$('.number_highlight').addClass('incorrect_border');
						$('.number_highlight~.wrong').show(0);
						countNext++;
						hint_number = 0;
					}
				}
				
				show_hint(hint_number);
				redraw_canvas();
			}	
		}	
	}
	function show_hint(hint){
		ctx_3.clearRect(0, 0, canvaswidth, canvasheight);
		if(hint==1){
			var x_pos = canvas.width/14 + current_x*canvas.width/14;
			var y_pos = canvas.height/14 + (12 - current_y)*canvas.height/14; 
			ctx_3.lineWidth = dot_radius;
	    	ctx_3.strokeStyle = '#93C47D';
			ctx_3.beginPath();
	    	ctx_3.moveTo(canvas.width/14, 13*canvas.height/14);
	    	ctx_3.lineTo(canvas.width/14 + current_x*canvas.width/14, 13*canvas.height/14);
	    	ctx_3.stroke();
	    	ctx_3.beginPath();
	    	ctx_3.moveTo(canvas.width/14, 13*canvas.height/14);
	    	ctx_3.lineTo(canvas.width/14, canvas.height/14 + (12 - current_y)*canvas.height/14);
	    	ctx_3.stroke();
		} else if(hint == 2){
			var x_pos = canvas.width/14 + current_x*canvas.width/14;
			var y_pos = canvas.height/14 + (12 - current_y)*canvas.height/14; 
			
			ctx_3.lineWidth = dot_radius;
	    	ctx_3.strokeStyle = '#93C47D';
			ctx_3.beginPath();
	    	ctx_3.moveTo(canvas.width/14, 13*canvas.height/14);
	    	ctx_3.lineTo(canvas.width/14 + current_x*canvas.width/14, 13*canvas.height/14);
	    	ctx_3.stroke();
	    	ctx_3.beginPath();
	    	ctx_3.moveTo(canvas.width/14, 13*canvas.height/14);
	    	ctx_3.lineTo(canvas.width/14, canvas.height/14 + (12 - current_y)*canvas.height/14);
	    	ctx_3.stroke();
	    	
			ctx_3.fillStyle = '#6D9EEB';
    		ctx_3.font="2vmin Georgia";
			ctx_3.fillText('(' + current_x  + ', ' + current_y + ')', x_pos - canvas.width/28, y_pos - canvas.height/28);
			ctx_3.beginPath();
	        ctx_3.arc(x_pos, y_pos, dot_radius, 0, Math.PI*2, true);
			ctx_3.fill();
		}
	}
	function redraw_canvas(){
		ctx.clearRect(0, 0, canvaswidth, canvasheight);
		plot_points(arr_correct, '#93C47D', dot_radius);
		plot_points(arr_incorrect, '#EA9999', dot_radius);
		plot_points([[current_incorrect_x, current_incorrect_y]], 'red', dot_radius);
		if(draw_green_circle){
			plot_points([[current_x, current_y]], '#93C47D', dot_radius );
		}
	}
	function plot_points(plot_arr, color, radius){
		for (plot_item of plot_arr) {
			var x_pos = canvas.width/14 + plot_item[0]*canvas.width/14;
			var y_pos = canvas.height/14 + (12-plot_item[1])*canvas.height/14; 
			ctx.fillStyle = color;
    		ctx.font="2vmin Georgia";
			ctx.fillText('(' + plot_item[0]  + ', ' + plot_item[1] + ')', x_pos - canvas.width/28, y_pos - canvas.height/28);
			draw_circle(x_pos, y_pos, color, radius);
		}
	}
	function draw_circle(x1, y1, color, radius){
        ctx.beginPath();
        ctx.arc(x1, y1, radius, 0, Math.PI*2, true);
        ctx.fillStyle = color;
		ctx.fill();
	}
	$(window).resize(function() {
		init_canvas();
		redraw_canvas();
	});

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

	}


	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 11){
			templateCaller();
		}		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
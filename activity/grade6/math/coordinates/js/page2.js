var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [{
	//starting page
    contentblocknocenteradjust : true,
    contentblockadditionalclass: "bluebg",
	uppertextblock : [{
		textclass : "firsttitle",
		textdata : data.string.p2_intro
	}]

}, {
	//page 1
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s1
	},{
		textclass : "descriptiontext2",
		datahighlightflag : true,
		textdata : data.string.p2_s2
	},{
		textclass : "descriptiontext4 descriptiontext4fadein",
		datahighlightflag : true,
		textdata : data.string.p2_s3
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "froggy blinkscaleupdown",
			imgsrc : imgpath + "frog-boy.png"
		},{
			imgclass: "arrow",
			imgsrc : imgpath + "yellow_arrow.png"
		}]
	}]
},
{
	//page 2
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "descriptiontext4 descriptiontext4dragup",
		datahighlightflag : true,
		textdata : data.string.p2_s3
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "froggy",
			imgsrc : imgpath + "frog-boy.png"
		}]
	}],
	inputfields: true,
	textclass: "coordinatequestion",
	textdata: data.string.p2_s5,
	inputtypes:[{
		inputtype: "text",
		inputtypeclass: "coordinateinput",
		inputtypename: "coordinateinputname",
		preinputspanclass: "coordinatespanclass",
		preinputspanvalue: data.string.right
	},{
		inputtype: "button",
		inputtypeclass: "checkbtnclass highlightonhover",
		inputtypename: "checkbtnname",
		inputtypevalue: data.string.check_button
	},{
		inputtype: "button",
		inputtypeclass: "hintbtnclass highlightonhover1",
		inputtypename: "hintbtnname",
		inputtypevalue: data.string.hint_button
	}],
	additionalinfotextblockclass : "coordinatesmoved",
	additionalinfotextblock: [{
		textclass : "descriptiontext1 hide",
		datahighlightflag : true,
		textdata : data.string.p2_s7
	}
	]
},
 {
 	//page 3
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s3
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "froggy",
			imgsrc : imgpath + "frog-boy.png"
		}]
	}],
	inputfields: true,
	textclass: "coordinatequestion",
	textdata: data.string.p2_s6,
	inputtypes:[{
		inputtype: "text",
		inputtypeclass: "coordinateinput",
		inputtypename: "coordinateinputname",
		preinputspanclass: "coordinatespanclass",
		preinputspanvalue: data.string.top
	},{
		inputtype: "button",
		inputtypeclass: "checkbtnclass highlightonhover",
		inputtypename: "checkbtnname",
		inputtypevalue: data.string.check_button
	},{
		inputtype: "button",
		inputtypeclass: "hintbtnclass highlightonhover",
		inputtypename: "hintbtnname",
		inputtypevalue: data.string.hint_button
	}],
	additionalinfotextblockclass : "coordinatesmoved",
	additionalinfotextblock: [{
		textclass : "descriptiontext1 show",
		datahighlightflag : true,
		textdata : data.string.p2_s7
	},{
		textclass : "descriptiontext1 hide",
		datahighlightflag : true,
		textdata : data.string.p2_s8
	}
	]
}, {

	//page 4
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s9
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "froggy",
			imgsrc : imgpath + "frog-boy.png"
		}, {
			imgclass : "diamond",
			imgsrc : imgpath + "gem.png"
		}]
	}],
	additionalinfotextblockclass : "coordinatesmoved",
	additionalinfotextblock: [{
		textclass : "descriptiontext1 show",
		datahighlightflag : true,
		textdata : data.string.p2_s7
	},{
		textclass : "descriptiontext1 show",
		datahighlightflag : true,
		textdata : data.string.p2_s8
	}
	]
}, {
	//page 5
        contentblocknocenteradjust : true,
        contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "firsttitle",
		datahighlightflag : true,
		textdata : data.string.p2_s10
	}]
},{
	//page 6
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s11
	}]
}, {

	//page 7
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "descriptiontext4",
		datahighlightflag : true,
		textdata : data.string.p2_s12
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "froggy",
			imgsrc : imgpath + "frog-boy.png"
		}]
	}],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s4
	}, {
		imagelabelclass : "coordinategraph",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s13
	}]
	}]
},{
	//page 8
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "coordinategraph1",
		datahighlightflag : true,
		textdata : data.string.p2_s13
	},{
		textclass : "descriptiontext4",
		datahighlightflag : true,
		textdata : data.string.p2_s14
	}],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start startfadeinout",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s4
	},{
		imagelabelclass : "start originfadeinout",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s36
	}]
	}]
},{
	//page 9
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg1",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s13
	},{
		textclass : "descriptiontext4",
		datahighlightflag : true,
		textdata : data.string.p2_s15
	}],
	additionalinfotextblockclass : "coordinatesmoved",
	additionalinfotextblock: [{
		textclass : "descriptiontext1 show",
		datahighlightflag : true,
		textdata : data.string.p2_s7
	},{
		textclass : "descriptiontext1 show",
		datahighlightflag : true,
		textdata : data.string.p2_s8
	}
	],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s36
	}, {
		imagelabelclass : "x_axis blinkscaleupdown",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s37
	}]
	}]
},{
	//page 10
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg1",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s13
	},{
		textclass : "descriptiontext4",
		datahighlightflag : true,
		textdata : data.string.p2_s16
	}],
	additionalinfotextblockclass : "coordinatesmoved",
	additionalinfotextblock: [{
		textclass : "descriptiontext1 show",
		datahighlightflag : true,
		textdata : data.string.p2_s7
	},{
		textclass : "descriptiontext1 show",
		datahighlightflag : true,
		textdata : data.string.p2_s8
	}
	],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s36
	}, {
		imagelabelclass : "x_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s37
	}, {
		imagelabelclass : "y_axis blinkscaleupdown",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s38
	}]
	}]
},{
	//page 11
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg1",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s13
	}],
	additionalinfotextblockclass : "coordinatesmoved2",
	additionalinfotextblock: [{
		textclass : "descriptiontext_xaxis startfadeinout",
		datahighlightflag : true,
		textdata : data.string.p2_s7
	},{
		textclass : "descriptiontext_xaxis originfadeinout",
		datahighlightflag : true,
		textdata : data.string.p2_s39
	},{
		textclass : "descriptiontext_yaxis",
		datahighlightflag : true,
		textdata : data.string.p2_s8
	}
	],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s36
	}, {
		imagelabelclass : "x_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s37
	}, {
		imagelabelclass : "y_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s38
	}]
	}],
	lowertextblockadditionalclass: "alignbottom",
	lowertextblock : [{
			textclass : "descriptiontext1",
			datahighlightflag : true,
			textdata : data.string.p2_s17
		},{
			textclass : "descriptiontext1 descriptiontext4fadein",
			datahighlightflag : true,
			textdata : data.string.p2_s18
		},{
			textclass : "descriptiontext1 descriptiontext4fadein",
			datahighlightflag : true,
			textdata : data.string.p2_newtext
		}]
},{
	//page 12
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg1",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s13
	}],
	additionalinfotextblockclass : "coordinatesmoved2",
	additionalinfotextblock: [
	{
		textclass : "descriptiontext_xaxis",
		datahighlightflag : true,
		textdata : data.string.p2_s39
	},{
		textclass : "descriptiontext_yaxis startfadeinout",
		datahighlightflag : true,
		textdata : data.string.p2_s8
	},{
		textclass : "descriptiontext_yaxis originfadeinout",
		datahighlightflag : true,
		textdata : data.string.p2_s40
	}
	],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s36
	}, {
		imagelabelclass : "x_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s37
	}, {
		imagelabelclass : "y_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s38
	}]
	}],
	lowertextblockadditionalclass: "alignbottom",
	lowertextblock : [{
			textclass : "descriptiontext1",
			datahighlightflag : true,
			textdata : data.string.p2_s19
		},{
			textclass : "descriptiontext1 descriptiontext4fadein",
			datahighlightflag : true,
			textdata : data.string.p2_s20
		},{
			textclass : "descriptiontext1 descriptiontext4fadein",
			datahighlightflag : true,
			textdata : data.string.p2_newtext1
		}]
},{
	//page 13
	containscanvaselement: true,
	contentblockadditionalclass: "bluebg1",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s13
	}],
	additionalinfotextblockclass : "coordinatesmoved2",
	additionalinfotextblock: [
	{
		textclass : "descriptiontext_xaxis",
		datahighlightflag : true,
		textdata : data.string.p2_s39
	},{
		textclass : "descriptiontext_yaxis",
		datahighlightflag : true,
		textdata : data.string.p2_s40
	}
	],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s36
	}, {
		imagelabelclass : "x_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s37
	}, {
		imagelabelclass : "y_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s38
	}, {
		imagelabelclass : "coordinatedescription2",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s22
	}]
	}],
	lowertextblockadditionalclass: "alignbottom",
	lowertextblock : [{
			textclass : "descriptiontext1",
			datahighlightflag : true,
			textdata : data.string.p2_s21
		}]
}
];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);
  var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s2_p1", src: soundAsset+"S2_P1.ogg"},
			{id: "s2_p2", src: soundAsset+"S2_P2.ogg"},
			{id: "s2_p3", src: soundAsset+"S2_P3.ogg"},
			{id: "s2_p4", src: soundAsset+"S2_P4.ogg"},
			{id: "s2_p5", src: soundAsset+"S2_P5.ogg"},
			{id: "s2_p6", src: soundAsset+"S2_P6.ogg"},
			{id: "s2_p7", src: soundAsset+"S2_P7.ogg"},
			{id: "s2_p8", src: soundAsset+"S2_P8.ogg"},
      {id: "s2_p9", src: soundAsset+"S2_P9.ogg"},
      {id: "s2_p10", src: soundAsset+"S2_P10.ogg"},
      {id: "s2_p11", src: soundAsset+"S2_P11.ogg"},
      {id: "s2_p12", src: soundAsset+"S2_P12.ogg"},
      {id: "s2_p13", src: soundAsset+"S2_P13.ogg"},
      {id: "s2_p14", src: soundAsset+"S2_P14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		//call notifyuser
		// notifyuser($anydiv);
		var drawxycoordinatelines = new Event("drawxycoordinatelines");
		if(countNext > 0 && countNext !=5){
			$nextBtn.hide(0);
			initaitedrawingoncanvas($board, $nextBtn, countNext, drawxycoordinatelines);
		}

		switch(countNext){
      case 0:
      	sound_player_nav("s2_p"+(countNext+1));
        break;
			case 1:
				sound_player_nav("s2_p"+(countNext+1));
				break;
			case 2:
      setTimeout(function(){
        sound_player_nav("s2_p"+(countNext+1));
      },3000);
      var ans = 5;
      var $checkbtn = $(".checkbtnclass");
      var $hintbtn = $(".hintbtnclass");
      var $input = $(".coordinateinput");
      var answered = false;
      $hintbtn.click(function (){
        //trigger event
        if(answered)
          return false;
        $hintbtn.toggleClass("highlightonhover");
        $hintbtn.css('background-color', '#7AC185');
        $(".hide").toggleClass("show").toggleClass("hide");
        // $input[0].value = ans;
        document.dispatchEvent(drawxycoordinatelines);
      });

      $checkbtn.click(function(){
        if(answered)
          return false;
        var value = parseInt($input[0].value);
        if(value == ans){
          //trigger event show
          $checkbtn.toggleClass("highlightonhover");
          $checkbtn.css('background-color', '#7AC185');
          document.dispatchEvent(drawxycoordinatelines);
          $(".hide").toggleClass("show").toggleClass("hide");
          nav_button_controls(1000);
          answered = true;
          play_correct_incorrect_sound(1);

        }else{
          $checkbtn.toggleClass("highlightonhover").css('background-color', '#FF3071');
          play_correct_incorrect_sound(0);
        }
      });
      break;
			case 3:
      setTimeout(function(){
        sound_player_nav("s2_p"+(countNext+1));
      },1000);
				var ans = 4;
				var $checkbtn = $(".checkbtnclass");
				var $hintbtn = $(".hintbtnclass");
				var $input = $(".coordinateinput");
				var answered = false;
				$hintbtn.click(function (){
					//trigger event
					if(answered)
						return false;
					$hintbtn.toggleClass("highlightonhover");
					$hintbtn.css('background-color', '#7AC185');
					$(".hide").toggleClass("show").toggleClass("hide");
					// $input[0].value = ans;
					document.dispatchEvent(drawxycoordinatelines);
				});

				$checkbtn.click(function(){
					if(answered)
						return false;
					var value = parseInt($input[0].value);
					if(value == ans){
						//trigger event show
						$checkbtn.toggleClass("highlightonhover");
						$checkbtn.css('background-color', '#7AC185');
						document.dispatchEvent(drawxycoordinatelines);
						$(".hide").toggleClass("show").toggleClass("hide");
						nav_button_controls(1000);
						answered = true;
						play_correct_incorrect_sound(1);

					}else{
						$checkbtn.toggleClass("highlightonhover").css('background-color', '#FF3071');
						play_correct_incorrect_sound(0);
					}
				});
				break;
      case 4:
          sound_player_nav("s2_p"+(countNext+1));
          break;
			case 5:
			     sound_player_nav("s2_p"+(countNext+1));
				break;
      case 6:
         sound_player_nav("s2_p"+(countNext+1));
        break;
      case 7:
      sound_player_nav("s2_p"+(countNext+1));
        break;
      case 8:
      sound_player_nav("s2_p"+(countNext+1));
        break;
      case 9:
      sound_player_nav("s2_p"+(countNext+1));
        break;
      case 10:
      sound_player_nav("s2_p"+(countNext+1));
        break;
      case 11:
      sound_player_nav("s2_p"+(countNext+1));
        break;
      case 12:
      sound_player_nav("s2_p"+(countNext+1));
        break;
      case 13:
      sound_player_nav("s2_p"+(countNext+1));
        break;
			default:
      nav_button_controls(0);
				break;
		}



	}
  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(countNext==2 || countNext==3){
				$nextBtn.hide(0);
				$prevBtn.hide(0);
			}else{
				nav_button_controls(300);
			}

		});
	}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			loadTimelineProgress($total_page, countNext + 1);
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

function initaitedrawingoncanvas($board, $nextBtn, countNext, drawxycoordinatelines) {
	var $customcanvasbase = $("#customcanvasbase");
	var $customcanvasgrid = $('#customcanvasgrid');

	var ctxbase = $customcanvasbase[0].getContext('2d');
	var ctxgrid = $customcanvasgrid[0].getContext('2d');

	var canvasheight = $board.height() * 0.64;
	var canvaswidth = $board.width() * 0.61;

	ctxbase.canvas.height = canvasheight;
	ctxbase.canvas.width = canvaswidth;

	ctxgrid.canvas.height = canvasheight;
	ctxgrid.canvas.width = canvaswidth;

	/*
	 *Ashish Gurung
	 * resize optimization begin
	 * This function resize end has been copied from stackover flow solution
	 *	@link: http://stackoverflow.com/questions/5489946/jquery-how-to-wait-for-the-end-of-resize-event-and-only-then-perform-an-ac
	 *	this is implemented because if the simple .resize() function is used then the canvas redraw function gets called too may times
	 *	which may hog the processor resources over exteded period of time
	 */
	var rtime;
	var timeout = false;
	var delta = 200;
	var resizefactoralongy;
	var resizefactoralongx;
	$(window).resize(function() {
	    rtime = new Date();
	    if (timeout === false) {
	        timeout = true;
	        setTimeout(resizeend, delta);
	    }
	});

	function redraw () {
			var oldcanvasheight = canvasheight;

		    canvasheight = $board.height() * 0.6;
			canvaswidth = $board.width() * 0.7;

			ctxbase.canvas.height = canvasheight;
			ctxbase.canvas.width = canvaswidth;

			ctxgrid.canvas.height = canvasheight;
			ctxgrid.canvas.width = canvaswidth;

			// drawdots(ctxbase, ctxgrid, canvasheight, canvaswidth);
			drawoncanvas();
	}

	function resizeend() {
		resizefactoralongy = ($board.height() * 0.6)/ canvasheight;
		resizefactoralongx = ($board.width() * 0.7)/ canvaswidth;
	    if (new Date() - rtime < delta) {
	    	redraw();
	        setTimeout(resizeend, delta);
	    } else {
	        timeout = false;
	    	redraw();
	    }
	}

/*
 * Ashish Gurung
 * resize optimization end
 */

	// drawdots(ctxbase, ctxgrid, canvasheight, canvaswidth);

	function drawoncanvas(){
		drawgrid(ctxbase, ctxgrid, canvasheight, canvaswidth);
		switch(countNext){
			case 1:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, true);
				// drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 2:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, true);
				document.addEventListener("drawxycoordinatelines", function(){
					drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5);
					drawDiamonds(ctxgrid, canvasheight, canvaswidth, true);
				});
				// drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 3:
				drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, true);
				document.addEventListener("drawxycoordinatelines", function(){
					drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
					drawDiamonds(ctxgrid, canvasheight, canvaswidth, true);
				});
				// drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 4:
				drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				// drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 5:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth);
				// drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 6:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth);
				// drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 7:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth);
				setTimeout(function(){
					drawaxes(ctxbase, canvasheight, canvaswidth);
				}, 1000);
				break;
			case 8:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 9:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 10:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 11:
				drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 12:
				drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 13:
				drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			default:
				break;
		}
	}
	drawoncanvas();



}



/*
 * Ashish Gurung
 * the functions below are for drawing on canvas
 */
function drawaxes( ctxbase, height, width){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	var linewidth = Math.round(unitxaxis * 0.04);
	linewidth = (linewidth < 2) ? 2 : linewidth;
	ctxbase.lineWidth = linewidth+"";
	ctxbase.strokeStyle = "black";


	// window.requestAnimationFrame(animatefunction);
	ctxbase.beginPath();
	ctxbase.moveTo(unitxaxis, 6 * unityaxis);
	ctxbase.lineTo(unitxaxis + (unitxaxis * 7.5), 6 * unityaxis);
	ctxbase.stroke();

	ctxbase.beginPath();
	ctxbase.moveTo(width - unitxaxis*0.7,  unityaxis * 5.9);
	ctxbase.lineTo(width - unitxaxis*0.3,  unityaxis * 6);
	ctxbase.lineTo(width - unitxaxis*0.7,  unityaxis * 6.1);
	ctxbase.closePath();
	ctxbase.fill();

	ctxbase.beginPath();
	ctxbase.moveTo(unitxaxis , height - unityaxis+linewidth/2);
	ctxbase.lineTo(unitxaxis , unityaxis * 0.5);
	ctxbase.stroke();

	ctxbase.beginPath();
	ctxbase.moveTo( unitxaxis*1.065,  unityaxis * 0.8);
	ctxbase.lineTo( unitxaxis*1, unityaxis * 0.2);
	ctxbase.lineTo( unitxaxis*0.935, unityaxis * 0.8);
	ctxbase.closePath();
	ctxbase.fill();

}

function drawlineAlongXY(ctxgrid, height, width, movealongX, movealongY){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	ctxgrid.clearRect(0, 0, width, height);
	var linewidth = Math.round(unitxaxis * 0.1);
	linewidth = (linewidth < 2) ? 2 : linewidth;
	ctxgrid.lineWidth = linewidth+"";
	ctxgrid.strokeStyle = "rgba(24, 255, 255, 0.5)";
	if(movealongX != null){
		ctxgrid.beginPath();
		ctxgrid.moveTo(unitxaxis,  unityaxis * 6);
		ctxgrid.lineTo(unitxaxis + (unitxaxis * movealongX)+linewidth/2,  unityaxis * 6);
		ctxgrid.stroke();
	}
	if(movealongY != null){
		ctxgrid.beginPath();
		ctxgrid.moveTo(unitxaxis * 6, 6*unityaxis-linewidth/2);
		ctxgrid.lineTo(unitxaxis * 6, (6 - movealongY)*unityaxis);
		ctxgrid.stroke();
	}
}

function drawgrid(ctxbase, ctxgrid, height, width) {
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	ctxbase.clearRect(0, 0, width, height);
	ctxgrid.clearRect(0, 0, width, height);
	var linewidth = Math.round(unitxaxis * 0.1);
	linewidth = (linewidth < 2) ? 2 : linewidth;
	ctxbase.lineWidth = linewidth+"";
	ctxbase.strokeStyle = "#D9D9D9";

	var count = 0;
	while (count < 8) {
		count++;
		ctxbase.beginPath();
		ctxbase.moveTo(unitxaxis * count, unityaxis - linewidth/2);
		ctxbase.lineTo(unitxaxis * count, unityaxis * 6 + linewidth/2);
		ctxbase.stroke();
	}
	count = 0;
	while (count < 6) {
		count++;
		ctxbase.beginPath();
		ctxbase.moveTo(unitxaxis - linewidth /2 , unityaxis * count);
		ctxbase.lineTo(unitxaxis * 8, unityaxis * count);
		ctxbase.stroke();
	}
}

function drawDiamonds(ctxgrid, height, width, drawdiamond){

	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	// ctxgrid.clearRect(0, 0, width, height);

	var radius = Math.round(unitxaxis * 0.15);
	radius = (radius < 4) ? 4 : radius;
	ctxgrid.beginPath();
	ctxgrid.arc( unitxaxis , height - unityaxis, radius, 0, 2* Math.PI, false);
	ctxgrid.fillStyle = "yellow";
	ctxgrid.fill();
	if (drawdiamond != null) {
		if (drawdiamond) {
			function loadImages(sources, callback) {
				var images = {};
				var loadedImages = 0;
				var numImages = 0;

				for (var src in sources) {
					numImages++;
				}

				for (var src in sources) {
					images[src] = new Image();
					images[src].onload = function() {
						if (++loadedImages >= numImages) {
							callback(images);
						}
					};
					images[src].src = sources[src];
				}
			}

			var sources = {
				leaf : imgpath + "gem.png"
			};

			loadImages(sources, function(images) {
				ctxgrid.drawImage(images.leaf, unitxaxis * 6 - radius, unityaxis * 2 - radius, 2 * radius, 2 * radius);
			});
		} else {
			ctxgrid.beginPath();
			ctxgrid.arc(unitxaxis * 6, unityaxis * 2, radius, 0, 2 * Math.PI, false);
			ctxgrid.fillStyle = "black";
			ctxgrid.fill();
		}
	}
}

function drawdots(ctxbase, ctxgrid , height, width) {
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	ctxbase.clearRect(0, 0, width, height);
	ctxgrid.clearRect(0, 0, width, height);

	var circleradius = 1;

	if (width > 700) {
		circleradius = 5;
	} else if (width > 300 && width <= 700) {
		circleradius = 3;
	}

	function loadImages(sources, callback) {
		var images = {};
		var loadedImages = 0;
		var numImages = 0;

		for (var src in sources) {
			numImages++;
		}

		for (var src in sources) {
			images[src] = new Image();
			images[src].onload = function() {
				if (++loadedImages >= numImages) {
					callback(images);
				}
			};
			images[src].src = sources[src];
		}
	}

	var sources = {
		leaf : imgpath + "gem.png",
		froggy : imgpath + "frog-boy.png"
	};

	loadImages(sources, function(images) {
		for (var i = 1; i < 9; i++) {
			for (var j = 1; j < 7; j++) {
				ctxbase.drawImage(images.leaf, unitxaxis * (i - 0.5), unityaxis * (j - 0.5), unitxaxis, unityaxis);
			}
		}
		ctxgrid.drawImage(images.froggy, 0, unityaxis * 5, unitxaxis, unityaxis);
		window.requestAnimationFrame(animateFroggy);
		var animatealongxstep = unitxaxis /80;
		var animatealongystep = unityaxis /80;
		var movealongxflag = true;
		var count= 0;
		function animateFroggy(){
			count++;
			ctxgrid.clearRect(0, 0, width, height);
			var movex;
			var movey;
			var remainder;
			if(movealongxflag){
				movex = unitxaxis*.5+animatealongxstep* count;
				movey = 0;
				remainder = count%80;
				if(remainder > 40){
					movey = unityaxis*5- 2*animatealongystep*(80- remainder);
				}else{
					movey = unityaxis*5- 2*animatealongystep*remainder;
				}
				ctxgrid.drawImage(images.froggy, movex, movey, unitxaxis, unityaxis);
				if(movex >= (unitxaxis*4.5)){
					movealongxflag = false;
					count = 0;
				}
			}else{
				movex = unitxaxis*4.5;
				movey = (unityaxis * 5) - (animatealongystep * count);
				remainder = count%80;
				// if(remainder > 50){
					// movex = movex- 1*(animatealongxstep*(100- remainder));
				// }else{
					// movex = movex- 1*(animatealongxstep*remainder);
				// }
					ctxgrid.drawImage(images.froggy, movex, movey, unitxaxis, unityaxis);
			}

			if(remainder == 0 && !movealongxflag){
				setTimeout(function(){
					window.requestAnimationFrame(animateFroggy);
				}, 500);
			}else if(!(movey <= (2*unityaxis))){
				window.requestAnimationFrame(animateFroggy);
			}
		}

	});
}

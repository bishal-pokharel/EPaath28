var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [{
	//starting page
	contentblocknocenteradjust : true,
	contentblockadditionalclass: "bluebg",
	uppertextblock : [{
		textclass : "firsttitle",
		textdata : data.string.p3_s1
	}]

}, {
	//page 1
	containscanvaselement: true,
	customcanvasbase: "customcanvasbase",
	customcanvasgrid: "customcanvasgrid",
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p3_s2
	},{
		textclass : "descriptiontext4 descriptiontext4fadein",
		datahighlightflag : true,
		textdata : data.string.p3_s3
	}],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s36
	}, {
		imagelabelclass : "x_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s37
	},, {
		imagelabelclass : "y_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p2_s38
	}]
	}]
},
{
	//page 2
	containscanvaselement: true,
	customcanvasbase: "customcanvasbase",
	customcanvasgrid: "customcanvasgrid",
	contentblockadditionalclass: "bluebg",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p3_s2
	}],
	imageblock :[{
		imagelabels : [{
		imagelabelclass : "start",
		datahighlightflag : true,
		imagelabeldata : data.string.p3_s6
	}, {
		imagelabelclass : "x_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p3_s4
	},, {
		imagelabelclass : "y_axis",
		datahighlightflag : true,
		imagelabeldata : data.string.p3_s5
	},{
		imagelabelclass : "coordinatedescription2",
		datahighlightflag : true,
		imagelabeldata : data.string.p3_s9
	}]
	}],
	lowertextblockadditionalclass: "alignbottom",
	lowertextblock : [{
			textclass : "descriptiontext1 movealongxfadein",
			datahighlightflag : true,
			textdata : data.string.p3_s7
		},{
			textclass : "descriptiontext1 movealongyfadein",
			datahighlightflag : true,
			textdata : data.string.p3_s8
		}]
},
 {
 	//page 3
	containscanvaselement: true,
	customcanvasbase: "customcanvasbase",
	customcanvasgrid: "customcanvasgrid",
	contentblockadditionalclass: "bluebgfadeaway",
	uppertextblock : [
	{
	textclass : "descriptiontext1",
	datahighlightflag : true,
	textdata : data.string.p3_s2
	}],
	imageblock :[{
	imagelabels : [{
	imagelabelclass : "start startfadeaway",
	datahighlightflag : true,
	imagelabeldata : data.string.p3_s6
	}, {
	imagelabelclass : "x_axis",
	datahighlightflag : true,
	imagelabeldata : data.string.p3_s4
	},, {
	imagelabelclass : "y_axis",
	datahighlightflag : true,
	imagelabeldata : data.string.p3_s5
	}]
	}],
	lowertextblockadditionalclass: "alignbottom",
	lowertextblock :[{
		textclass : "descriptiontext1 movealongxfadein",
		datahighlightflag : true,
		textdata : data.string.p3_s18
	}, {
		textclass : "descriptiontext1 movealongyfadein",
		datahighlightflag : true,
		textdata : data.string.p3_s19
	}]
}, {

	//page 4
	containscanvaselement: true,
	customcanvasbase: "customcanvasbase1",
	customcanvasgrid: "customcanvasgrid1",

	containsdraggables: [{
		customdraggableid: "firstdraggable",
		imgclass: "imageicon",
		imgsrc: imgpath + "circle.png",
		imagelabelclass: "coordinate",
		imagelabeldata: data.string.p3_s32
	},{
		customdraggableid: "seconddraggable",
		imgclass: "imageicon",
		imgsrc: imgpath + "circle.png",
		imagelabelclass: "coordinate",
		imagelabeldata: data.string.p3_s33
	},{
		customdraggableid: "thirddraggable",
		imgclass: "imageicon",
		imgsrc: imgpath + "circle.png",
		imagelabelclass: "coordinate",
		imagelabeldata: data.string.p3_s34
	}],

	containsdroppables:[{
		customdropableclass: "firstdroppable"
	},{
		customdropableclass: "seconddroppable"
	},{
		customdropableclass: "thirddroppable"
	}],

	lowertextblockadditionalclass: "alignbottom",
	lowertextblock :[{
		textclass : "descriptiontext5",
		datahighlightflag : true,
		textdata : data.string.p3_s28
	}, {
		textclass : "descriptiontext6",
		datahighlightflag : true,
		textdata : data.string.p3_s29
	}],

	inputfields : true,
	inputtypes : [{
		inputtype : "button",
		inputtypevalue : data.string.hint_button,
		inputtypeclass : "hint1 highlightonhover"
	}]
}, {
	//page 5
	containscanvaselement: true,
	customcanvasbase: "customcanvasbase1",
	customcanvasgrid: "customcanvasgrid1",

	containsdraggables: [{
		customdraggableid: "firstdraggable",
		imgclass: "imageicon",
		imgsrc: imgpath + "circle.png",
		imagelabelclass: "coordinate",
		imagelabeldata: data.string.p3_s32
	},{
		customdraggableid: "seconddraggable",
		imgclass: "imageicon",
		imgsrc: imgpath + "circle.png",
		imagelabelclass: "coordinate",
		imagelabeldata: data.string.p3_s35
	},{
		customdraggableid: "thirddraggable",
		imgclass: "imageicon",
		imgsrc: imgpath + "circle.png",
		imagelabelclass: "coordinate",
		imagelabeldata: data.string.p3_s36
	}],

	containsdroppables:[{
		customdropableclass: "firstdroppable"
	},{
		customdropableclass: "forthdroppable"
	},{
		customdropableclass: "fifthdroppable"
	},{
		customdropableclass: "sixthdroppable"
	}],

	lowertextblockadditionalclass: "alignbottom",
	lowertextblock :[{
		textclass : "descriptiontext7",
		datahighlightflag : true,
		textdata : data.string.p3_s30
	}, {
		textclass : "descriptiontext7",
		datahighlightflag : true,
		textdata : data.string.p3_s31
	}],

	inputfields : true,
	inputtypes : [{
		preinputspanvalue: data.string.p3_s37,
		preinputspanclass: "frontbracket",
		inputtype : "text",
		inputtypeclass : "xcoordinate",
		inputspanclass: "comma",
		inputspanvalue: data.string.p3_s38
	},{
		inputtype : "text",
		inputtypeclass : "ycoordinate",
		inputspanclass: "backbracket",
		inputspanvalue: data.string.p3_s39
	},{
		inputtype : "button",
		inputtypevalue : data.string.check_button,
		inputtypeclass : "check highlightonhover"
	},{
		inputtype : "button",
		inputtypevalue : data.string.hint_button,
		inputtypeclass : "hint highlightonhover"
	}
	]
}
];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
    var correctclick = false;

    loadTimelineProgress($total_page, countNext + 1);
		var preload;
		var timeoutvar = null;
		var current_sound;


		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images
				// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

				// soundsicon-orange
				{id: "s3_p1", src: soundAsset+"S3_P1.ogg"},
				{id: "s3_p2", src: soundAsset+"S3_P2.ogg"},
				{id: "s3_p3", src: soundAsset+"S3_P3.ogg"},
				{id: "s3_p4", src: soundAsset+"S3_P4.ogg"},
				{id: "s3_p5", src: soundAsset+"S3_P5.ogg"},
				{id: "s3_p5a", src: soundAsset+"S3_P5_2.ogg"},
				{id: "s3_p6", src: soundAsset+"S3_P6.ogg"},

			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();


	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		//call notifyuser
		// notifyuser($anydiv);
		var drawxycoordinatelines = new Event("drawxycoordinatelines");
		if(countNext > 0){
			$nextBtn.hide(0);
			initaitedrawingoncanvas($board, $nextBtn, countNext, drawxycoordinatelines);
		}
		correctclick = false;
		switch(countNext){
			case 0:
			sound_player_nav("s3_p"+(countNext+1));
			break;
			case 1:
			sound_player_nav("s3_p"+(countNext+1));
			break;
			case 2:
			sound_player_nav("s3_p"+(countNext+1));
			break;
			case 3:
			sound_player_nav("s3_p"+(countNext+1));
			break;
			case 4:
			sound_player_nav("s3_p"+(countNext+1));
			break;
			case 5:
			$(".descriptiontext7:nth-child(1)").html(data.string.p3_s28).show(0);
			break;
			default:
				break;
		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			// if(countNext==1 ){
			// 	$nextBtn.hide(0);
			// 	$prevBtn.hide(0);
			// }else{
			if(countNext!=1 && countNext!=4) nav_button_controls(300);
			// }
		});
	}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			loadTimelineProgress($total_page, countNext + 1);
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/


function initaitedrawingoncanvas($board, $nextBtn, countNext, drawxycoordinatelines) {
	var $customcanvasbase = $("#customcanvasbase");
	var $customcanvasgrid = $('#customcanvasgrid');

	var ctxbase = $customcanvasbase[0].getContext('2d');
	var ctxgrid = $customcanvasgrid[0].getContext('2d');

	var canvasheight = $board.height() * 0.74;
	var canvaswidth = $board.width() * 0.58;

	ctxbase.canvas.height = canvasheight;
	ctxbase.canvas.width = canvaswidth;

	ctxgrid.canvas.height = canvasheight;
	ctxgrid.canvas.width = canvaswidth;

	var droppedcoordinates = [];

	/*
	 *Ashish Gurung
	 * resize optimization begin
	 * This function resize end has been copied from stackover flow solution
	 *	@link: http://stackoverflow.com/questions/5489946/jquery-how-to-wait-for-the-end-of-resize-event-and-only-then-perform-an-ac
	 *	this is implemented because if the simple .resize() function is used then the canvas redraw function gets called too may times
	 *	which may hog the processor resources over exteded period of time
	 */
	var rtime;
	var timeout = false;
	var delta = 200;
	var resizefactoralongy;
	var resizefactoralongx;
	$(window).resize(function() {
	    rtime = new Date();
	    if (timeout === false) {
	        timeout = true;
	        setTimeout(resizeend, delta);
	    }
	});

	function redraw () {
			var oldcanvasheight = canvasheight;

		    canvasheight = $board.height() * 0.6;
			canvaswidth = $board.width() * 0.7;

			ctxbase.canvas.height = canvasheight;
			ctxbase.canvas.width = canvaswidth;

			ctxgrid.canvas.height = canvasheight;
			ctxgrid.canvas.width = canvaswidth;

			// drawdots(ctxbase, ctxgrid, canvasheight, canvaswidth);
			drawoncanvas();

			if(droppedcoordinates.length > 0){
				for (var i =0; i< droppedcoordinates.length;i++){
			 		drawupondrop(droppedcoordinates[i][0], droppedcoordinates[i][1], ctxgrid, canvasheight, canvaswidth);
			 	}
			 }
	}

	function resizeend() {
		resizefactoralongy = ($board.height() * 0.6)/ canvasheight;
		resizefactoralongx = ($board.width() * 0.7)/ canvaswidth;
	    if (new Date() - rtime < delta) {
	    	redraw();
	        setTimeout(resizeend, delta);
	    } else {
	        timeout = false;
	    	redraw();
	    }
	}

/*
 * Ashish Gurung
 * resize optimization end
 */

	// handling coordinate drop
	function handleCardDrop(event, ui) {
		ui.draggable.draggable('disable');
		var dropped = ui.draggable;
		var droppedOn = $(this);
		var count = 0;
		var top = 0;
		switch(dropped.attr("id")) {
		case "firstdraggable":
				drawupondrop(2, 4, ctxgrid, canvasheight, canvaswidth);
				droppedcoordinates.push([2, 4]);
				play_correct_incorrect_sound(1);
			break;
		case "seconddraggable":
			if (countNext == 4) {
				drawupondrop(6, 4, ctxgrid, canvasheight, canvaswidth);
				droppedcoordinates.push([6, 4]);
				play_correct_incorrect_sound(1);
			} else {
				drawupondrop(2, 1, ctxgrid, canvasheight, canvaswidth);
				droppedcoordinates.push([2, 1]);
				play_correct_incorrect_sound(1);
			}
			break;
		case "thirddraggable":
		if (countNext == 4) {
				drawupondrop(4, 2, ctxgrid, canvasheight, canvaswidth);
				droppedcoordinates.push([4, 2]);
				play_correct_incorrect_sound(1);
			} else {
				drawupondrop(5, 4, ctxgrid, canvasheight, canvaswidth);
				droppedcoordinates.push([5, 4]);
				play_correct_incorrect_sound(1);
			}

			break;
		default:
			break;
		}
		$(dropped).detach();

		if(droppedcoordinates.length >= 3){

			drawtriangle(ctxgrid, canvasheight, canvaswidth, droppedcoordinates);

			if(countNext == 5){
				$(".descriptiontext7:nth-child(1)").html(data.string.p3_s30).show(0).toggleClass("highlightp");
				console.log("why");
				$(".xcoordinate , .ycoordinate, .check, .frontbracket, .comma, .backbracket").show(0);
				setTimeout(()=>sound_player("s3_p6"),800);

			}else{
				$(".descriptiontext6").show(0).toggleClass("highlightp");

				setTimeout(()=>sound_player_nav("s3_p5a"),800);
				nav_button_controls(5500);
			}
		}

	}

	// drawdots(ctxbase, ctxgrid, canvasheight, canvaswidth);

	function drawoncanvas(){
		drawgrid(ctxbase, ctxgrid, canvasheight, canvaswidth);
		switch(countNext){
			case 1:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				setTimeout(function(){
					mousecontroller(ctxgrid, canvasheight, canvaswidth, $customcanvasgrid, $nextBtn);
				}, 1000);
				break;
			case 2:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth);
				drawcoordinate6_3(ctxgrid, canvasheight, canvaswidth);

				drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 6);
				drawcoordinate6_3(ctxgrid, canvasheight, canvaswidth);
				setTimeout(function(){
					drawDiamonds(ctxgrid, canvasheight, canvaswidth);
				});

				setTimeout(function(){
					drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 6, 3);
					drawcoordinate6_3(ctxgrid, canvasheight, canvaswidth);
					drawDiamonds(ctxgrid, canvasheight, canvaswidth);
					setTimeout(function(){
						drawcoordinate6_3(ctxgrid, canvasheight, canvaswidth);
						drawDiamonds(ctxgrid, canvasheight, canvaswidth);
					},1000);
				}, 2000);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 3:
				var unitxaxis = Math.round(canvaswidth / 9);
				var unityaxis = Math.round(canvasheight / 7);
				var linewidth1 = Math.round(unitxaxis * 0.04);
				linewidth1 = (linewidth1 < 2) ? 2 : linewidth1;
				var linewidth2 = Math.round(unitxaxis * 0.1);
				linewidth2 = (linewidth2 < 2) ? 2 : linewidth2;
				var radius = Math.round(unitxaxis * 0.15);
				radius = (radius < 4) ? 4 : radius;
				var count = 0;
				// as request animation frame has too high FPS I have decided to use setTimout itself
				var FPS = 10;
				function animationtosimplegraph(){
					linewidth1 = (linewidth1 < 1) ? 1 : linewidth1 - 0.25;
					linewidth2 = (linewidth2 < 1) ? 1 : linewidth2 - 0.25;
					radius = (radius < 6) ? 6 : radius - 0.25;
					// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, null, null, linewidth);
					drawgrid(ctxbase, ctxgrid, canvasheight, canvaswidth, linewidth2);
					drawDiamonds(ctxgrid, canvasheight, canvaswidth, null, radius);
					drawaxes(ctxbase, canvasheight, canvaswidth, linewidth1);
					count++;
					$customcanvasbase.css({
						"background-color": "rgba(147, 196, 125, "+((1/count > 0.05)? 1/count : 0)+")",
						"box-shadow": "0px 0px 0px #FFFFFF"
						});

					if((linewidth1 > 1 || radius > 3 || linewidth2 > 1) && count < 45){
						setTimeout(function(){
							animationtosimplegraph();
						}, 2300/FPS);
					}else{
						drawnumbers(ctxbase, canvasheight, canvaswidth);
					}
				}
				animationtosimplegraph();
				// window.requestAnimationFrame(animationtosimplegraph);
				break;
			case 4:
				drawgrid(ctxbase, ctxgrid, canvasheight, canvaswidth, 1);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, null, 5);
				drawaxes(ctxbase, canvasheight, canvaswidth, 1);
				drawnumbers(ctxbase, canvasheight, canvaswidth);
				// drawaxes(ctxbase, canvasheight, canvaswidth);

				// make draggable
				// make dropable here that triggers canvas draw
				$(".draggablecordinate").draggable({
					containment : "board",
					cursor : "crosshair",
					revert : "invalid",
					// appendTo : "board",
					// helper : "clone",
					zindex : 10000
				});

				$(".firstdroppable").droppable({
					accept: "#firstdraggable",
					hoverCalss: "hovered",
					drop: handleCardDrop
				});

				$(".seconddroppable").droppable({
					accept: "#seconddraggable",
					hoverCalss: "hovered",
					drop: handleCardDrop
				});

				$(".thirddroppable").droppable({
					accept: "#thirddraggable",
					hoverCalss: "hovered",
					drop: handleCardDrop
				});
				var hintshowing = false;
				$(".hint1").click(function (){
					if(hintshowing)
						return false;
					hintshowing = true;
					if($("#firstdraggable")[0]){
						$(".firstdroppable").toggleClass("dropablecoordinatehighlight");
						setTimeout(function(){
							$(".firstdroppable").toggleClass("dropablecoordinatehighlight");
							hintshowing = false;
						}, 5000);
					}else if($("#seconddraggable")[0]){
						$(".seconddroppable").toggleClass("dropablecoordinatehighlight");
						setTimeout(function(){
							$(".seconddroppable").toggleClass("dropablecoordinatehighlight");
							hintshowing = false;
						}, 5000);
					}else if($("#thirddraggable")[0]){
						$(".thirddroppable").toggleClass("dropablecoordinatehighlight");
						setTimeout(function(){
							$(".thirddroppable").toggleClass("dropablecoordinatehighlight");
							hintshowing = false;
						}, 5000);
					}
				});
				break;
			case 5:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawgrid(ctxbase, ctxgrid, canvasheight, canvaswidth, 1);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, null, 5);
				drawaxes(ctxbase, canvasheight, canvaswidth, 1);
				drawnumbers(ctxbase, canvasheight, canvaswidth);
				// drawaxes(ctxbase, canvasheight, canvaswidth);
				// make draggable
				// make dropable here that triggers canvas draw
				$(".draggablecordinate").draggable({
					containment : "board",
					cursor : "crosshair",
					revert : "invalid",
					// appendTo : "board",
					helper : "clone",
					zindex : 10000
				});

				$(".firstdroppable").droppable({
					accept: "#firstdraggable",
					hoverCalss: "hovered",
					drop: handleCardDrop
				});

				$(".forthdroppable").droppable({
					accept: "#thirddraggable",
					hoverCalss: "hovered",
					drop: handleCardDrop
				});

				$(".fifthdroppable").droppable({
					accept : "#seconddraggable",
					hoverCalss : "hovered",
					drop : handleCardDrop
				});
				var hintshowing = false;
				$(".hint").click(function() {
					if (hintshowing)
						return false;
					hintshowing = true;
					if (droppedcoordinates.length == 3) {
						$(".sixthdroppable").toggleClass("dropablecoordinatehighlight");
							setTimeout(function() {
								$(".sixthdroppable").toggleClass("dropablecoordinatehighlight");
								hintshowing = false;
							}, 5000);
					} else {
						if ($("#firstdraggable")[0]) {
							$(".firstdroppable").toggleClass("dropablecoordinatehighlight");
							setTimeout(function() {
								$(".firstdroppable").toggleClass("dropablecoordinatehighlight");
								hintshowing = false;
							}, 5000);
						} else if ($("#seconddraggable")[0]) {
							$(".fifthdroppable").toggleClass("dropablecoordinatehighlight");
							setTimeout(function() {
								$(".fifthdroppable").toggleClass("dropablecoordinatehighlight");
								hintshowing = false;
							}, 5000);
						} else if ($("#thirddraggable")[0]) {
							$(".forthdroppable").toggleClass("dropablecoordinatehighlight");
							setTimeout(function() {
								$(".forthdroppable").toggleClass("dropablecoordinatehighlight");
								hintshowing = false;
							}, 5000);
						}
					}
				});

				var $checkbtn = $(".check");
				var $inputx = $(".xcoordinate");
				var $inputy = $(".ycoordinate");
				var answered = false;

				$checkbtn.click(function(){
					if(answered)
						return false;
					var valuex = parseInt($inputx[0].value);
					var valuey = parseInt($inputy[0].value);
					if(valuex == 5 && valuey == 1){
						//trigger event show
						$checkbtn.toggleClass("highlightonhover");
						$checkbtn.css('background-color', '#7AC185').addClass("avoid-cliks");
						$(".descriptiontext6:nth-child(2)").show(0).toggleClass("highlightp");
						var temp = [
												[2,4],

											 [2,1],

											 [5,1],

											 [5,4]
										 ];
										 // console.log([droppedcoordinates[0][0], droppedcoordinates[0][1]]);
						// if((droppedcoordinates[0][0]==5 && droppedcoordinates[1][1]==4) || droppedcoordinates[0][1]==1){
						// 	temp = [
						// 							[droppedcoordinates[0][0], droppedcoordinates[0][1]],
						// 						 // [droppedcoordinates[1][0], droppedcoordinates[1][1]],
						//
						// 						 [5,1],
						//
						// 						 [droppedcoordinates[2][0], droppedcoordinates[2][1]]
						// 					 ];
						// }
						// console.log([droppedcoordinates[0][0], droppedcoordinates[1][1]]);
						// if((droppedcoordinates[0][0]==5 && droppedcoordinates[1][1]==1)){
						// 	temp = [
						// 							[droppedcoordinates[0][0], droppedcoordinates[0][1]],
						// 						 [droppedcoordinates[1][0], droppedcoordinates[1][1]],
						//
						// 						 [5,1],
						//
						// 						 [droppedcoordinates[2][0], droppedcoordinates[2][1]]
						// 					 ];
						// }
						console.log(temp);



						droppedcoordinates = temp;
						answered = true;
						drawtriangle(ctxgrid, canvasheight, canvaswidth, temp);
						play_correct_incorrect_sound(1);
						setTimeout(function(){
							nav_button_controls(0);
						});
					}else{
						$checkbtn.toggleClass("highlightonhover").css('background-color', '#FF3071');
						play_correct_incorrect_sound(0);
					}
				});

				break;
			case 6:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawgrid(ctxbase, ctxgrid, canvasheight, canvaswidth, 1);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, null, 5);
				drawaxes(ctxbase, canvasheight, canvaswidth, 1);
				drawnumbers(ctxbase, canvasheight, canvaswidth);
				// drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 7:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth);
				setTimeout(function(){
					drawaxes(ctxbase, canvasheight, canvaswidth);
				}, 1000);
				break;
			case 8:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 9:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 10:
				// drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 11:
				drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			case 12:
				drawlineAlongXY(ctxgrid, canvasheight, canvaswidth, 5, 4);
				drawDiamonds(ctxgrid, canvasheight, canvaswidth, false);
				drawaxes(ctxbase, canvasheight, canvaswidth);
				break;
			default:
				break;
		}
	}
	drawoncanvas();



}



/*
 * Ashish Gurung
 * the functions below are for drawing on canvas
 */
function drawtriangle(ctxgrid, height, width, droppedcoordinates){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	console.log(" x-axis : "+ unitxaxis, " y-axis : "+ unityaxis);
	ctxgrid.beginPath();
	var linewidth = Math.round(unitxaxis * 0.04);
	linewidth = (linewidth < 2) ? 2 : linewidth;
	ctxgrid.lineWidth = linewidth+"";
	ctxgrid.moveTo( unitxaxis * (droppedcoordinates[0][0] + 1) , unityaxis * (7 -(droppedcoordinates[0][1] + 1)));
	for(var i = 1; i < droppedcoordinates.length ; i++){
		ctxgrid.lineTo( unitxaxis * (droppedcoordinates[i][0] + 1) , unityaxis * (7 -(droppedcoordinates[i][1] + 1)));
	}
	ctxgrid.closePath();
	ctxgrid.fillStyle = "#D9D9D9";
	ctxgrid.fill();

	ctxgrid.strokeStyle = "#D9D9D9";
	ctxgrid.stroke();

	for(var i = 0 ; i < droppedcoordinates.length; i++){
		drawupondrop(droppedcoordinates[i][0], droppedcoordinates[i][1], ctxgrid, height, width);
	}
}


function drawupondrop(xcoordinate, ycoordinate, ctxgrid, height, width){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	// ctxgrid.clearRect(0, 0, width, height);

	var radius = Math.round(unitxaxis * 0.1);
	radius = (radius < 2) ? 2 : radius;

	ctxgrid.beginPath();
	ctxgrid.arc( unitxaxis * (xcoordinate + 1) , unityaxis * (7 -(ycoordinate + 1)) , radius *2, 0, 2* Math.PI, false);
	ctxgrid.fillStyle = "#D9D9D9";
	ctxgrid.fill();

	ctxgrid.beginPath();
	ctxgrid.arc( unitxaxis * (xcoordinate + 1) , unityaxis * (7 -(ycoordinate + 1)) , radius *1.5, 0, 2* Math.PI, false);
	ctxgrid.fillStyle = "purple";
	ctxgrid.fill();
}


function drawnumbers(ctxbase, height, width){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);

	ctxbase.strokeStyle = "#D9D9D9";
	ctxbase.font = unityaxis * .4+"px Arial";
	var count = 0;
	var string = data.string.p3_s10;
	ctxbase.textAlign = "right";
    ctxbase.fillText(count, unitxaxis - unitxaxis*.2, unityaxis *( 6.5));
	function drawnumbers(){
		switch(count) {
		case 1:
			string = data.string.p3_s11;
			break;
		case 2:
			string = data.string.p3_s12;
			break;
		case 3:
			string = data.string.p3_s13;
			break;
		case 4:
			string = data.string.p3_s14;
			break;
		case 5:
			string = data.string.p3_s15;
			break;
		case 6:
			string = data.string.p3_s16;
			break;
		case 7:
			string = data.string.p3_s17;
			break;
		default:
			break;
		}
		count ++;
		if (count <= 5){
			ctxbase.textAlign = "right";
			ctxbase.fillText(count, unitxaxis - unitxaxis*.2, unityaxis *( 6 - count) + unityaxis*.2);
		}

		if(count <= 7){
			ctxbase.textAlign = "center";
			ctxbase.fillText(count, unitxaxis * (count+1), unityaxis *( 6.5));
			setTimeout(function(){
				drawnumbers();
			}, 1000/5);// 20 is the fpsfor animation

		}
	}

	drawnumbers();
}

function drawcoordinate6_3(ctxgrid, height, width){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	// ctxgrid.clearRect(0, 0, width, height);

	var radius = Math.round(unitxaxis * 0.1);
	radius = (radius < 2) ? 2 : radius;

	ctxgrid.beginPath();
	ctxgrid.arc( unitxaxis * 7 , unityaxis * 3 , radius *2, 0, 2* Math.PI, false);
	ctxgrid.fillStyle = "#D9D9D9";
	ctxgrid.fill();

	ctxgrid.beginPath();
	ctxgrid.arc( unitxaxis * 7 , unityaxis * 3 , radius *1.5, 0, 2* Math.PI, false);
	ctxgrid.fillStyle = "purple";
	ctxgrid.fill();
}


function mousecontroller(ctxgrid, height, width, $customcanvasgrid, $nextBtn){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	// ctxgrid.clearRect(0, 0, width, height);

	var radius = Math.round(unitxaxis * 0.1);
	var posold = {
		x: 0, y:0
	};
	var incorrect = [0, 0, 0];
	var correct = false;


	var centers = [{
		h : 4,
		k : 1
	}, {
		h : 7,
		k : 3
	}, {
		h : 8,
		k : 5
	}];

	radius = (radius < 2) ? 2 : radius;

	for(var i = 0; i < 3; i++){
		ctxgrid.beginPath();
		ctxgrid.arc( unitxaxis * centers[i].h, unityaxis * centers[i].k, radius * 2, 0, 2* Math.PI, false);
		ctxgrid.fillStyle = "#D9D9D9";
		ctxgrid.fill();
		ctxgrid.beginPath();
		ctxgrid.arc( unitxaxis * centers[i].h, unityaxis * centers[i].k, radius * 1.5, 0, 2* Math.PI, false);
		ctxgrid.fillStyle = "green";
		ctxgrid.fill();
	}


	$customcanvasgrid.on("mouseover, mousemove", function(e){
		var pos = getMousePos(this, e);
		if(Math.abs(pos.x - posold.x) > 5 || Math.abs(pos.y - posold.y) > 5){
			posold = pos;
			checkifmousefallsinradiusofdots(pos, true);
		}

	});

	$customcanvasgrid.on("click", function(e){
		var test = false;
		var pos = getMousePos(this, e);
		!test?test = checkifmousefallsinradiusofdots(pos, false):"";
	});

	function checkifmousefallsinradiusofdots(pos, fromhoverormove) {

			//centers of the three circles 1, 2, 3

			var posincenterrange = false;
			// compare to first circle center:(3, 5)
			// compare to second circle center: (6, 3)
			// compare to third circle center: (7, 1)
			var range = (unitxaxis > unityaxis) ? unityaxis : unitxaxis;
			var calculatedradius;
			var count = 0;
			while (!posincenterrange && count < 3 && !correctclick) {
				console.log("This is while loop"+correctclick);
				// (x - h)^2 + (y - k)^2 = r^2
				ctxgrid.beginPath();
				ctxgrid.arc(unitxaxis * centers[count].h, unityaxis * centers[count].k, radius* 2, 0, 2 * Math.PI, false);
				ctxgrid.fillStyle = "#D9D9D9";
				ctxgrid.fill();

				ctxgrid.beginPath();
				ctxgrid.arc(unitxaxis * centers[count].h, unityaxis * centers[count].k, radius*1.5, 0, 2 * Math.PI, false);
				if (incorrect[count] == 1 && !correctclick) {
					ctxgrid.fillStyle = "red";
					ctxgrid.fill();
					count++;
					continue;
				} else if (correct && count == 1) {
					ctxgrid.fillStyle = "purple";
					ctxgrid.fill();
					count++;
					continue;
				}else if (!correctclick){
					ctxgrid.fillStyle = "green";
				}
				ctxgrid.fill();

				calculatedradius = Math.pow((pos.x - centers[count].h * unitxaxis), 2) + Math.pow((pos.y - centers[count].k * unityaxis), 2);
				if (calculatedradius < Math.pow(range, 2)) {
					ctxgrid.beginPath();
					ctxgrid.arc(unitxaxis * centers[count].h, unityaxis * centers[count].k, radius *1.5, 0, 2 * Math.PI, false);
					console.log("from or hive"+fromhoverormove);
					if (fromhoverormove) {
						ctxgrid.fillStyle = "yellow";
					} else if (count == 1) {
						correct = true;
						ctxgrid.fillStyle = "purple";
						nav_button_controls(500);
						play_correct_incorrect_sound(1);
						correctclick = true;
					} else {
						ctxgrid.fillStyle = "red";
						incorrect[count] = 1;
						 play_correct_incorrect_sound(0);
					}

					ctxgrid.fill();
					posincenterrange = true;
				}
				count++;
		}
	}

}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

function drawaxes( ctxbase, height, width, passedlinewidth){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	var linewidth = Math.round(unitxaxis * 0.04);
	linewidth = (linewidth < 2) ? 2 : linewidth;
	if(passedlinewidth != null){
		linewidth = passedlinewidth;
	}
	ctxbase.lineWidth = linewidth+"";
	ctxbase.strokeStyle = "black";


	// window.requestAnimationFrame(animatefunction);
	ctxbase.beginPath();
	ctxbase.moveTo(unitxaxis, 6 * unityaxis);
	ctxbase.lineTo(unitxaxis + (unitxaxis * 7.5), 6 * unityaxis);
	ctxbase.stroke();

	ctxbase.beginPath();
	ctxbase.moveTo(width - unitxaxis*0.7,  unityaxis * 5.9);
	ctxbase.lineTo(width - unitxaxis*0.3,  unityaxis * 6);
	ctxbase.lineTo(width - unitxaxis*0.7,  unityaxis * 6.1);
	ctxbase.closePath();
	ctxbase.fill();

	ctxbase.beginPath();
	ctxbase.moveTo(unitxaxis , height - unityaxis+linewidth/2);
	ctxbase.lineTo(unitxaxis , unityaxis * 0.5);
	ctxbase.stroke();

	ctxbase.beginPath();
	ctxbase.moveTo( unitxaxis*1.065,  unityaxis * 0.8);
	ctxbase.lineTo( unitxaxis*1, unityaxis * 0.2);
	ctxbase.lineTo( unitxaxis*0.935, unityaxis * 0.8);
	ctxbase.closePath();
	ctxbase.fill();

}

function drawlineAlongXY(ctxgrid, height, width, movealongX, movealongY, passedlinewidth){
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	ctxgrid.clearRect(0, 0, width, height);
	var linewidth = Math.round(unitxaxis * 0.1);
	linewidth = (linewidth < 2) ? 2 : linewidth;
	if(passedlinewidth != null)
		linewidth = passedlinewidth;
	ctxgrid.lineWidth = linewidth+"";
	var coloralpha = 0;
	function animateline(){
		coloralpha += 0.01;
		ctxgrid.strokeStyle = "rgba(24, 255, 255, "+coloralpha+")";
	if(movealongX != null){
		ctxgrid.beginPath();
		ctxgrid.moveTo(unitxaxis,  unityaxis * 6);
		ctxgrid.lineTo(unitxaxis + (unitxaxis * movealongX)+linewidth/2,  unityaxis * 6);
		ctxgrid.stroke();
	}
	if(movealongY != null){
		ctxgrid.beginPath();
		ctxgrid.moveTo(unitxaxis * (movealongX+1), 6*unityaxis-linewidth/2);
		ctxgrid.lineTo(unitxaxis * (movealongX+1), (6 - movealongY)*unityaxis);
		ctxgrid.stroke();
	}
	if(coloralpha < 0.2)
		window.requestAnimationFrame(animateline);
	}
	window.requestAnimationFrame(animateline);

}

function drawgrid(ctxbase, ctxgrid, height, width, passedlinewidth) {
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	ctxbase.clearRect(0, 0, width, height);
	ctxgrid.clearRect(0, 0, width, height);
	var linewidth = Math.round(unitxaxis * 0.1);
	linewidth = (linewidth < 2) ? 2 : linewidth;
	ctxbase.strokeStyle = "#D9D9D9";
	if(passedlinewidth != null){
		linewidth = passedlinewidth;
		ctxbase.strokeStyle = "#888888";
	}
	ctxbase.lineWidth = linewidth+"";


	var count = 0;
	while (count < 8) {
		count++;
		ctxbase.beginPath();
		ctxbase.moveTo(unitxaxis * count, unityaxis - linewidth/2);
		ctxbase.lineTo(unitxaxis * count, unityaxis * 6 + linewidth/2);
		ctxbase.stroke();
	}
	count = 0;
	while (count < 6) {
		count++;
		ctxbase.beginPath();
		ctxbase.moveTo(unitxaxis - linewidth /2 , unityaxis * count);
		ctxbase.lineTo(unitxaxis * 8, unityaxis * count);
		ctxbase.stroke();
	}
}

function drawDiamonds(ctxgrid, height, width, drawdiamond, passedradius){

	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	// ctxgrid.clearRect(0, 0, width, height);

	var radius = Math.round(unitxaxis * 0.15);
	radius = (radius < 4) ? 4 : radius;
	if(passedradius != null)
		radius = passedradius;
	ctxgrid.beginPath();
	ctxgrid.arc( unitxaxis , unityaxis*6, radius, 0, 2* Math.PI, false);
	ctxgrid.fillStyle = "yellow";
	ctxgrid.fill();
	if (drawdiamond != null) {
		if (drawdiamond) {
			function loadImages(sources, callback) {
				var images = {};
				var loadedImages = 0;
				var numImages = 0;

				for (var src in sources) {
					numImages++;
				}

				for (var src in sources) {
					images[src] = new Image();
					images[src].onload = function() {
						if (++loadedImages >= numImages) {
							callback(images);
						}
					};
					images[src].src = sources[src];
				}
			}

			var sources = {
				leaf : imgpath + "gem.png"
			};

			loadImages(sources, function(images) {
				ctxgrid.drawImage(images.leaf, unitxaxis * 6 - radius, unityaxis * 2 - radius, 2 * radius, 2 * radius);
			});
		} else {
			ctxgrid.beginPath();
			ctxgrid.arc(unitxaxis * 6, unityaxis * 2, radius, 0, 2 * Math.PI, false);
			ctxgrid.fillStyle = "black";
			ctxgrid.fill();
		}
	}
}

function drawdots(ctxbase, ctxgrid , height, width) {
	var unitxaxis = Math.round(width / 9);
	var unityaxis = Math.round(height / 7);
	ctxbase.clearRect(0, 0, width, height);
	ctxgrid.clearRect(0, 0, width, height);

	var circleradius = 1;

	if (width > 700) {
		circleradius = 5;
	} else if (width > 300 && width <= 700) {
		circleradius = 3;
	}

	function loadImages(sources, callback) {
		var images = {};
		var loadedImages = 0;
		var numImages = 0;

		for (var src in sources) {
			numImages++;
		}

		for (var src in sources) {
			images[src] = new Image();
			images[src].onload = function() {
				if (++loadedImages >= numImages) {
					callback(images);
				}
			};
			images[src].src = sources[src];
		}
	}

	var sources = {
		leaf : imgpath + "gem.png",
		froggy : imgpath + "frog-boy.png"
	};

	loadImages(sources, function(images) {
		for (var i = 1; i < 9; i++) {
			for (var j = 1; j < 7; j++) {
				ctxbase.drawImage(images.leaf, unitxaxis * (i - 0.5), unityaxis * (j - 0.5), unitxaxis, unityaxis);
			}
		}
		ctxgrid.drawImage(images.froggy, 0, unityaxis * 5, unitxaxis, unityaxis);
		window.requestAnimationFrame(animateFroggy);
		var animatealongxstep = unitxaxis /80;
		var animatealongystep = unityaxis /80;
		var movealongxflag = true;
		var count= 0;
		function animateFroggy(){
			count++;
			ctxgrid.clearRect(0, 0, width, height);
			var movex;
			var movey;
			var remainder;
			if(movealongxflag){
				movex = unitxaxis*.5+animatealongxstep* count;
				movey = 0;
				remainder = count%80;
				if(remainder > 40){
					movey = unityaxis*5- 2*animatealongystep*(80- remainder);
				}else{
					movey = unityaxis*5- 2*animatealongystep*remainder;
				}
				ctxgrid.drawImage(images.froggy, movex, movey, unitxaxis, unityaxis);
				if(movex >= (unitxaxis*4.5)){
					movealongxflag = false;
					count = 0;
				}
			}else{
				movex = unitxaxis*4.5;
				movey = (unityaxis * 5) - (animatealongystep * count);
				remainder = count%80;
				// if(remainder > 50){
					// movex = movex- 1*(animatealongxstep*(100- remainder));
				// }else{
					// movex = movex- 1*(animatealongxstep*remainder);
				// }
					ctxgrid.drawImage(images.froggy, movex, movey, unitxaxis, unityaxis);
			}

			if(remainder == 0 && !movealongxflag){
				setTimeout(function(){
					window.requestAnimationFrame(animateFroggy);
				}, 500);
			}else if(!(movey <= (2*unityaxis))){
				window.requestAnimationFrame(animateFroggy);
			}
		}

	});
}
});

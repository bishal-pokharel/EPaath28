Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 0
		{
			exampleimage: [
				{
					examplesrc: imgpath + "croc-and-congas01.gif",
					exampleclass: "animal1"
				},
				{
					examplesrc: imgpath + "red-panda-on-recorder01.gif",
					exampleclass: "animal2"
				},
				{
					examplesrc: imgpath + "rhino-on-keyboard01.gif",
					exampleclass: "animal3"
				},
				{
					examplesrc: imgpath + "snow-leopardess-on-sax01.gif",
					exampleclass: "animal4"
				},
				{
					examplesrc: imgpath + "sundari-maracas01.gif",
					exampleclass: "animal5"
				},
				{
					examplesrc: imgpath + "clouds_basket_game_background.jpg",
					exampleclass: "background"
				},
			]

		},


];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;

	/*values in this array is same as the name of images of eggs in image folder*/
	var imageArray = ["01","02","03","04","05","06","07","08","09","10"];

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
		$('.exefin').hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }


		/*======= SCOREBOARD SECTION ==============*/

		$('#egg' + countNext).addClass('eggmove');


		/*random scoreboard eggs*/
		var i = Math.floor(Math.random() * imageArray.length);
		var randImg = imageArray[i];
		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							$("#egg"+countNext).attr("src", "images/eggs/" + randImg +".png").removeClass('eggmove').attr("select","yes");
							$(".exefin").append("<img class='eggs' src = 'images/eggs/" + randImg + ".png'> </img>");
							imageArray.splice(i,1);
							score++;
						}
						$(this).css("background","#6AA84F");
						$(this).css("border","5px solid #B6D7A8");
						$(this).siblings(".corctopt").css("visibility","visible");
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						$("#egg"+countNext).attr("src", "images/eggs/egg_wrong.png").removeClass('eggmove');
						$(this).css("background","#EA9999");
						$(this).css("border","5px solid #efb3b3");
						$(this).css("color","#F66E20");
						$(this).siblings(".wrngopt").css("visibility","visible");
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if (countNext == 8){
			$('#score').html(score);
			$('[select=yes]').fadeTo(1000,0).hide(0);
			$('.exefin').show(0);
			$('.contentblock').hide(0);
			$('.congratulation').show(0);

		}
		else
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
	//slide 0
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.p4text1
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext2",
			textclass : "defintext",
			textdata : data.string.p4text2
		}
		],
	},
	{
	//slide 1
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.p4text1
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext2",
			textclass : "defintext",
			textdata : data.string.p4text2
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "void",
			textclass : "leftdefintext",
			textdata : data.string.p4text3
		}
		],
	},
	{
	//slide 2
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.p4text1
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext2",
			textclass : "defintext",
			textdata : data.string.p4text2
		},
		{
			datahighlightflag: true,
			textclass : "leftdefintext",
			textdata : data.string.p4text3
		},
		{
			datahighlightflag: true,
			textclass : "rightdefintext",
			textdata : data.string.p4text4
		}
		],
	},
	{
	//slide 3
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.p4text1
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext2",
			textclass : "defintext letshigh",
			textdata : data.string.p4text2
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext3",
			textclass : "leftdefintext",
			textdata : data.string.p4text3
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext3",
			textclass : "rightdefintext",
			textdata : data.string.p4text4
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "",
			textclass : "bottomtext letshigh",
			textdata : data.string.p4text5
		}
		],
	},
	{
	//slide 4
        uppertextblock:[
            {
                textclass : "centertext diytitle",
                textdata : data.string.diy
            }
        ],
        imageblock:[{
            imagetoshow: [
                {
                    imgclass: "diyimage",
                    imgsrc: "images/diy_bg/a_07.png"
                }
            ]
        }]
	},
	{
	//slide 5
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.p4text6,
		},
		{
			textclass : "desctext",
			textdata : data.string.p4text7,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "forexe",
			textclass : "centerdefintext",
			textdata : data.string.p4exe1,
		},
		{
			textclass : "mybuttons btnhover nobelongbtn",
			textdata : data.string.check,
		},
		{
			textclass : "exeques",
			textdata : data.string.p4text8,
		}
		],
		inputtext:[
			{
				inputclass: "exebox"
			}
		]
	},
	{
	//slide 6
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.p4text6,
		},
		{
			textclass : "desctext",
			textdata : data.string.p4text9,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "forexe",
			textclass : "centerdefintext",
			textdata : data.string.p4exe2,
		},
		{
			textclass : "mybuttons btnhover nobelongbtn",
			textdata : data.string.check,
		},
		{
			textclass : "exeques",
			textdata : data.string.p4text10,
		}
		],
		inputtext:[
			{
				inputclass: "exebox"
			}
		]
	},
	{
	//slide 7
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext2",
			textclass : "centertext",
			textdata : data.string.p4text11,
		}
		],
	},
	{
	//slide 7
		uppertextblock:[
		{
			textclass : "centertext",
			textdata : data.string.p4text12,
		}
		],
	},
	{
	//slide 8
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.p4text13
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textclass : "defintext",
			textdata : data.string.p4text14
		},
		{
			textclass : "inftext blueborder",
			textdata : data.string.p4text15
		},
		],
	},
	{
	//slide 9
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.p4text13
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textclass : "defintext",
			textdata : data.string.p4text14
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext2",
			textclass : "defintextinf",
			textdata : data.string.p4text16
		},
		{
			textclass : "inftext blueborder",
			textdata : data.string.p4text15
		},
		{
			textclass : "fintext redborder",
			textdata : data.string.p4text17
		},
		],
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
			{id: "s4_p3", src: soundAsset+"s4_p3.ogg"},
			{id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p6", src: soundAsset+"s4_p6.ogg"},
			{id: "s4_p7", src: soundAsset+"s4_p7.ogg"},
			{id: "s4_p8", src: soundAsset+"s4_p8.ogg"},
			{id: "s4_p9", src: soundAsset+"s4_p9.ogg"},
			{id: "s4_p10", src: soundAsset+"s4_p10.ogg"},
			{id: "s4_p11", src: soundAsset+"s4_p11.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program

	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext){
			case 0:
			sound_player_nav("s4_p"+(countNext+1));
			break;
			case 1:
			sound_player_nav("s4_p"+(countNext+1));
			break;
			case 2:
			sound_player_nav("s4_p"+(countNext+1));
			break;
			case 3:
			sound_player_nav("s4_p"+(countNext+1));
			break;
			case 4:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 5:
			sound_player_nav("s4_p"+(countNext+1));
			$(".desctext").hide(0);
			$(".exebox").keypress(function (e) {
				if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});
			$('.nobelongbtn').click(function(){
				createjs.Sound.stop();
				$(".desctext").show(0);

				if($(".exebox").val() == 26){
					$(".nobelongbtn").css("background","#93C47D");
					play_correct_incorrect_sound(1);
					nav_button_controls(1000);
				}
				else{
					$(".nobelongbtn").css("background","#E06666");
					play_correct_incorrect_sound(0);
				}
			});

			break;

			case 6:
			sound_player_nav("s4_p"+(countNext+1));
			$(".desctext").hide(0);
			$(".exebox").keypress(function (e) {
				if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});
			$('.nobelongbtn').click(function(){
				createjs.Sound.stop();
				$(".desctext").show(0);

				if($(".exebox").val() == 10){
					$(".nobelongbtn").css("background","#93C47D");
					play_correct_incorrect_sound(1);
					nav_button_controls(1000);
				}
				else{
					$(".nobelongbtn").css("background","#E06666");
					play_correct_incorrect_sound(0);
				}
			});

			break;
			case 7:
			sound_player_nav("s4_p"+(countNext+1));
			break;
			case 8:
			sound_player_nav("s4_p"+(countNext+1));
			break;
			case 9:
			sound_player_nav("s4_p"+(countNext+1));
			break;
			case 10:
			sound_player_nav("s4_p"+(countNext+1));
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(countNext==5 || countNext==6){
				$nextBtn.hide(0);
				$prevBtn.hide(0);
			}else{
				nav_button_controls(0);
			}

		});
	}


	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		if(countNext != 5 && countNext != 6)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

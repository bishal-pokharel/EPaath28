var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
	//slide 8
		uppertextblock:[
		{
			textclass : "centertext diytitle",
			textdata : data.string.diy
		}
		],
        imageblock:[{
            imagetoshow: [
                {
                    imgclass: "diyimage",
                    imgsrc: "images/diy_bg/a_07.png"
                }
            ]
        }]
	},
	{
	//slide 9
		uppertextblock:[
		{
			textclass : "centertitle",
			textdata : data.string.p2mainques,
			datahighlightflag: true,
		}
		],
		quesHidAnsBlock:[
		{
			questioncontainer: "forhover",
			answercontainer: "forclick",
			chilrenadditionalclass1: "incorrect",
			question1: data.string.p2ques1,
			answer1: data.string.p2ans1,

			chilrenadditionalclass2: "incorrect",
			question2: data.string.p2ques2,
			answer2: data.string.p2ans2,

			chilrenadditionalclass3: "incorrect",
			question3: data.string.p2ques3,
			answer3: data.string.p2ans3,

			chilrenadditionalclass4: "correct",
			question4: data.string.p2ques4,
			answer4: data.string.p2ans4,

		}
		]
	},
	{
	//slide 10
		uppertextblock:[
		{
			textclass : "centertitle",
			textdata : data.string.p2mainques2,
			datahighlightflag: true,
		}
		],
		quesHidAnsBlock:[
		{
			questioncontainer: "forhover",
			answercontainer: "forclick",

			chilrenadditionalclass1: "incorrect",
			question1: data.string.p2ques5,
			answer1: data.string.p2ans5,

			chilrenadditionalclass2: "incorrect",
			question2: data.string.p2ques6,
			answer2: data.string.p2ans6,

			chilrenadditionalclass3: "incorrect",
			question3: data.string.p2ques6,
			answer3: data.string.p2ans7,

			chilrenadditionalclass4: "correct",
			question4: data.string.p2ques8,
			answer4: data.string.p2ans8,
		}
		]
	},

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;


		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images
				// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

				// soundsicon-orange
				{id: "s2_p2", src: soundAsset+"s2_p10.ogg"},
				{id: "s2_p3", src: soundAsset+"s2_p11.ogg"},
			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();


	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program

	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext){
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			sound_player_nav("s2_p"+(countNext+1));
			case 2:
			sound_player_nav("s2_p"+(countNext+1));
			$(".centertitle").css({"top":"5%","font-size":"1.9vw"})
			var corFlag = false;
			var parent = $(".parentcontainer");
			var divs = parent.children();
			while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}

			$(".children > p:nth-of-type(1)").one("click", function(){
				createjs.Sound.stop();
				if(corFlag == false){
				$(this).parent().css("visibility","visible");
				$(this).siblings("p").hide(0).delay(500).fadeIn("slow");
				$(this).removeClass("forhover");

				if($(this).parent().hasClass("correct")){
					play_correct_incorrect_sound(1);
					$(this).addClass("childcorr");
					$(this).parent().addClass("parcorr");
					corFlag = true;
					console.log(corFlag);
					setTimeout(function(){
							$(".incorrect").addClass("parinc").css("visibility","visible");
							$(".incorrect > .forhover").addClass("childinc").removeClass("forhover");
						nav_button_controls(1000);
							$(".incorrect > .forclick").hide(0).delay(500).fadeIn("slow").removeClass("forclick");
					}, 2000);
				}
				else{
                    play_correct_incorrect_sound(0);
                    console.log(corFlag);
						$(this).addClass("childinc");
						$(this).parent().addClass("parinc");
						$(this).siblings().removeClass("forclick");
					}
				}
			});
			break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(countNext==9 || countNext==10){
				$nextBtn.hide(0);
				$prevBtn.hide(0);
			}else{
				nav_button_controls(0);
			}

		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		if(countNext != 10)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";

var content=[

	// slide0

	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[
			{
				textdata: data.string.p5text1,
				textclass: "eins-1",
			},
			{
				textclass: "axisVerticalFirst",
				textdata: '',
				datahighlightflag: true,
				datahighlightcustomclass: 'grenLtr'
			},
			{
				textclass: "axisHorizontalFirst",
				textdata: '',
				datahighlightflag: true,
				datahighlightcustomclass: 'grenLtr'
			},
		],

        imageblock: [{
            imagestoshow: [
            	{
               	 	imgclass: "heart",
               	 	imgsrc: '',
                	imgid: 'b',
            	},
                {
                    imgclass: "correct",
                    imgsrc: '',
                    imgid: 'correct',
                },
                {
                    imgclass: "incorrect",
                    imgsrc: '',
                    imgid: 'incorrect',
                },
            ]
        }]
	},

    // slide1

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: 'bg-1',

        extratextblock:[
            {
                textdata: data.string.p5text1,
                textclass: "eins-1",
            },
            {
                textclass: "axisVerticalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "heart",
                    imgsrc: '',
                    imgid: 'u',
                },
                {
                    imgclass: "correct",
                    imgsrc: '',
                    imgid: 'correct',
                },
                {
                    imgclass: "incorrect",
                    imgsrc: '',
                    imgid: 'incorrect',
                },
            ]
        }]
    },

    // slide2

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: 'bg-1',

        extratextblock:[
            {
                textdata: data.string.p5text1,
                textclass: "eins-1",
            },
            {
                textclass: "axisVerticalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "heart",
                    imgsrc: '',
                    imgid: 'a',
                },
                {
                    imgclass: "correct",
                    imgsrc: '',
                    imgid: 'correct',
                },
                {
                    imgclass: "incorrect",
                    imgsrc: '',
                    imgid: 'incorrect',
                },
            ]
        }]
    },

    // slide3

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: 'bg-1',

        extratextblock:[
            {
                textdata: data.string.p5text1,
                textclass: "eins-1",
            },
            {
                textclass: "axisVerticalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "heart",
                    imgsrc: '',
                    imgid: 'tree',
                },
                {
                    imgclass: "correct",
                    imgsrc: '',
                    imgid: 'correct',
                },
                {
                    imgclass: "incorrect",
                    imgsrc: '',
                    imgid: 'incorrect',
                },
            ]
        }]
    },

    // slide4

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: 'bg-1',

        extratextblock:[
            {
                textdata: data.string.p5text1,
                textclass: "eins-1",
            },
            {
                textclass: "axisVerticalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "heart",
                    imgsrc: '',
                    imgid: 'x',

                },
                {
                    imgclass: "correct",
                    imgsrc: '',
                    imgid: 'correct',
                },
                {
                    imgclass: "incorrect",
                    imgsrc: '',
                    imgid: 'incorrect',
                },
            ]
        }]
    },

    // slide5

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: 'bg-1',

        extratextblock:[
            {
                textdata: data.string.p5text1,
                textclass: "eins-1",
            },
            {
                textclass: "axisVerticalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "heart",
                    imgsrc: '',
                    imgid: 'm',
                },
                {
                    imgclass: "correct",
                    imgsrc: '',
                    imgid: 'correct',
                },
                {
                    imgclass: "incorrect",
                    imgsrc: '',
                    imgid: 'incorrect',
                },
            ]
        }]
    },

    // slide6

    {

        extratextblock: [{
            textclass: "eins-1",
            textdata: data.string.p7text1,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "draggable firstHalfImage",
                    imgsrc: '',
                    imgid: 'hRight',
										ans: 'correct'
                },
                {
                    imgclass: "draggable middleHalfImage",
                    imgsrc: '',
                    imgid: 'hBottom',
										ans: 'incorrect'
                },
                {
                    imgclass: "draggable lastHalfImage",
                    imgsrc: '',
                    imgid: 'hTop',
										ans: 'incorrect'
                },
                {
                    imgclass: 'droppable test1',
                    imgsrc: '',
                    imgid: 'hLeft',
										ans: 'correct'
								},
								{
                    imgclass: 'test1 first',
                    imgsrc: '',
                    imgid: 'hFullLR'
                },

            ]
        }]
    },

    // slide7

    {

        extratextblock: [{
            textclass: "eins-1",
            textdata: data.string.p7text1,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "draggable firstHalfImage",
                    imgsrc: '',
                    imgid: 'nTop',
										ans: 'incorrect'
                },
                {
                    imgclass: "draggable middleHalfImage",
                    imgsrc: '',
                    imgid: 'nBottom',
										ans: 'incorrect'
                },
                {
                    imgclass: "draggable lastHalfImage",
                    imgsrc: '',
                    imgid: 'nRight',
										ans: 'correct'
                },
                {
                    imgclass: 'droppable test1',
                    imgsrc: '',
                    imgid: 'nLeft',
										ans: 'correct'
                },
								{
                    imgclass: 'test1 first',
                    imgsrc: '',
                    imgid: 'nFullLR'
                }

            ]
        }]
    },

    // slide8

    {

        extratextblock: [{
            textclass: "eins-1",
            textdata: data.string.p7text1,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "draggable firstHalfImage",
                    imgsrc: '',
                    imgid: 'tTop',
										ans: 'incorrect'
                },
                {
                    imgclass: "draggable middleHalfImage",
                    imgsrc: '',
                    imgid: 'tBottom',
										ans: 'incorrect'
                },
                {
                    imgclass: "draggable lastHalfImage",
                    imgsrc: '',
                    imgid: 'tRight',
										ans: 'correct'
                },

                {
                    imgclass: 'droppable test1',
                    imgsrc: '',
                    imgid: 'tLeft',
										ans: 'correct'
								},
								{
										imgclass: 'test1 first',
										imgsrc: '',
										imgid: 'tFullLR'
								},
            ]
        }]
    },

    // slide9

    {

        extratextblock: [{
            textclass: "eins-1",
            textdata: data.string.p7text1,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "draggable firstHalfImage",
                    imgsrc: '',
                    imgid: 'yRight',
										ans: 'correct'
                },
                {
                    imgclass: "draggable middleHalfImage",
                    imgsrc: '',
                    imgid: 'yBottom',
										ans: 'incorrect'
                },
                {
                    imgclass: "draggable lastHalfImage",
                    imgsrc: '',
                    imgid: 'yTop',
										ans: 'incorrect'
                },

                {
                    imgclass: 'droppable test1',
                    imgsrc: '',
                    imgid: 'yLeft',
										ans: 'correct'
                },
								{
										imgclass: 'test1 first',
										imgsrc: '',
										imgid: 'yFullLR'
								},

            ]
        }]
    },

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new NumberTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [

            //
            {id: "b", src: imgpath+"b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "u", src: imgpath+"u.png", type: createjs.AbstractLoader.IMAGE},
            {id: "a", src: imgpath+"a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree", src: imgpath+"tree.png", type: createjs.AbstractLoader.IMAGE},
            {id: "m", src: imgpath+"m.png", type: createjs.AbstractLoader.IMAGE},
            {id: "x", src: imgpath+"x.png", type: createjs.AbstractLoader.IMAGE},

            {id: "hLeft", src: imgpath+"h_left.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hFullLR", src: imgpath+"h_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hRight", src: imgpath+"h_right.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hTop", src: imgpath+"h_top.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hFullTB", src: imgpath+"cloud_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hBottom", src: imgpath+"h_buttom.png", type: createjs.AbstractLoader.IMAGE},

            {id: "nLeft", src: imgpath+"n_left.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nFullLR", src: imgpath+"n_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nRight", src: imgpath+"n_right.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nTop", src: imgpath+"n_top.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nFullTB", src: imgpath+"pluss_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nBottom", src: imgpath+"n_buttom.png", type: createjs.AbstractLoader.IMAGE},

            {id: "tLeft", src: imgpath+"t_left.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tFullLR", src: imgpath+"t_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tRight", src: imgpath+"t_right.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tTop", src: imgpath+"t_top.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tFullTB", src: imgpath+"pluss_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tBottom", src: imgpath+"t_buttom.png", type: createjs.AbstractLoader.IMAGE},

            {id: "yLeft", src: imgpath+"y_left.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yFullLR", src: imgpath+"y_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yRight", src: imgpath+"y_right.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yTop", src: imgpath+"y_top.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yFullTB", src: imgpath+"pluss_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yBottom", src: imgpath+"y_buttom.png", type: createjs.AbstractLoader.IMAGE},





            {id: "starTop", src: imgpath+"pink_star02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "starBottom", src: imgpath+"pink_star01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "starFullTB", src: imgpath+"pink_star_full.png", type: createjs.AbstractLoader.IMAGE},

            {id: "yellowStarTop", src: imgpath+"y_star_top.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowStarBottom", src: imgpath+"y_star_buttom.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowStarTB", src: imgpath+"y_star_full01.png", type: createjs.AbstractLoader.IMAGE},

            {id: "yellowStarLeft", src: imgpath+"y_star_left.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowStarRight", src: imgpath+"y_star_right.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowStarLR", src: imgpath+"y_star_full.png", type: createjs.AbstractLoader.IMAGE},

            {id: "ovalTop", src: imgpath+"ovel_top.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ovalBottom", src: imgpath+"ovel_buttom.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ovalFullTB", src: imgpath+"ovel_full.png", type: createjs.AbstractLoader.IMAGE},


            {id: "hexagon", src: imgpath+"hexagon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "square", src: imgpath+"squire.png", type: createjs.AbstractLoader.IMAGE},
            {id: "star", src: imgpath+"star.png", type: createjs.AbstractLoader.IMAGE},
            {id: "greenDottedLine", src: imgpath+"green_doted_line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "redDottedLine", src: imgpath+"red_dotedline.png", type: createjs.AbstractLoader.IMAGE},
            {id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
            {id: "incorrect", src: imgpath+"wrong.png", type: createjs.AbstractLoader.IMAGE},

            {id: "bg-1", src: imgpath+"bg_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-3", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-2", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-4", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-1", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-5", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-3", src: imgpath+"balloon_blue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-2", src: imgpath+"balloon_green.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-4", src: imgpath+"balloon_red.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-1", src: imgpath+"balloon_orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon-5", src: imgpath+"balloon_pink.png", type: createjs.AbstractLoader.IMAGE},

			{id: "arrow-left", src: imgpath+"arrow-left.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow-right", src: imgpath+"arrow-right.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
      // sounds
			{id: "ex1_1", src: soundAsset+"ex1_1.ogg"},
      {id: "ex1_7", src: soundAsset+"ex1_7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*===============================================
    =            user notification function        =
    ===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var quesCount = ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8', 'Q9', 'Q10'];
	if($lang=='np') quesCount = ['१) ', '२) ', '३) ', '४) ', '५) ', '६) ', '७) ', '८) ', '९) ', '१०) '];

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		var count = 0;
        var $total_page = content.length;

		$board.html(html);

        put_image(content, countNext);
        put_speechbox_image(content,countNext);

        loadTimelineProgress($total_page, countNext + 1);

        var wrong_clicked = 0;

        $('.correct').hide();
        $('.incorrect').hide();
        $refreshBtn.hide();

        var done1 = false;
        var done2 = false;
        var done3 = false;
        var done4 = false;

        var clicked = 0;

        switch(countNext){

            case 0:
						case 4:
							countNext==0?sound_player("ex1_1"):'';
                $('.axisVerticalFirst').css('left','48%');

                $('.axisVerticalFirst').click(function () {
									createjs.Sound.stop();

                    $('.axisVerticalFirst').css('border','3.2px dashed #f1593f');
                    $('.axisHorizontalFirst').css('border','3.2px dashed black');
                  //  $('.axisHorizontalFirst').css('pointer-events','none');
                    $('.correct').hide();
                    $('.incorrect').show();

                    clicked = clicked + 1;

                    console.log('clicked',clicked);
										play_correct_incorrect_sound(0);

                  //  $nextBtn.show();
                    scoring.update(false);

                });

                $('.axisHorizontalFirst').click(function () {
									createjs.Sound.stop();

                    clicked = clicked + 1;

                    console.log('clicked',clicked);

                    $('.axisHorizontalFirst').css('border','3.2px dashed #b9d32f');
                    $('.axisVerticalFirst').css('border','3.2px dashed black');
                    $('.axisVerticalFirst').css('pointer-events','none');
                    $('.correct').show();
                    $('.incorrect').hide();
										play_correct_incorrect_sound(1);
                    $refreshBtn.hide();

                    if (clicked == 2){
                        $nextBtn.show();
                        scoring.update(false);
												$('.axisHorizontalFirst').css('pointer-events','none');
                    }else{
                        $nextBtn.show();
                        scoring.update(true);
                    }

                });

                break;

            case 1:
            case 2:
            case 3:
						case 5:

                clicked = 0;


                // For First Choice

                $('.axisVerticalFirst').click(function () {

                    clicked = clicked + 1;

                    $('.axisVerticalFirst').css('border','3.2px dashed #b9d32f');
                    $('.axisHorizontalFirst').css('border','3.2px dashed black');
                    $('.axisHorizontalFirst').css('pointer-events','none');
                    $('.correct').show();
                    $('.incorrect').hide();
										play_correct_incorrect_sound(1);

                    if (clicked == 2){
                        $nextBtn.show();
                        scoring.update(false);
												$('.axisVerticalFirst').css('pointer-events','none');
                    }else{
                        $nextBtn.show();
                        scoring.update(true);
                    }
                });

                $('.axisHorizontalFirst').click(function () {

                    clicked = clicked + 1;

                    $('.axisHorizontalFirst').css('border','3.2px dashed #f1593f');
                    $('.axisVerticalFirst').css('border','3.2px dashed black');
                    // $('.axisVerticalFirst').css('pointer-events','none');
                    $('.correct').hide();
                    $('.incorrect').show();
										play_correct_incorrect_sound(0);

                    //$nextBtn.show();
                    scoring.update(false);

                });

                break;

            case 6:
						case 7:
						case 8:
            case 9:
							countNext==6?sound_player("ex1_7"):'';

                var clicked = 0;

								$(".first,.second,.third,.fourth").hide();

								dragdrop();

                break;



            default:

               // nav_button_controls(100);
                break; // end of case 4

        }


    }

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}

    function put_image(content, count){
        if(content[count].hasOwnProperty('imageblock')){
            var imageblock = content[count].imageblock[0];
            if(imageblock.hasOwnProperty('imagestoshow')){
                var imageClass = imageblock.imagestoshow;
                for(var i=0; i<imageClass.length; i++){
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.'+classes_list[classes_list.length-1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

		function dragdrop(){

       var clicked = 0;

				$(".draggable").draggable({
						containment: "body",
						revert: true,
						appendTo: "body",
						zindex: 10,
				});
				$('.droppable').droppable({
						accept : ".draggable",
						hoverClass: "hovered",
						drop: function(event, ui) {
							createjs.Sound.stop();
								var newimg = ui.draggable.attr("data-answer").toString().trim();

                console.log('newimg'+ newimg, 'thisimg '+($(this).attr("data-answer").toString().trim()));

                clicked = clicked + 1;

								if(newimg == ($(this).attr("data-answer").toString().trim())) {
									 // play_correct_incorrect_sound(1);
										ui.draggable.hide(0);
										$(this).find("p").text(ui.draggable.text());
										$(this).removeClass("droppable").addClass("correctcss relativecls");
										$(this).append("<img class='correctImg' src='images/right.png'/>");
										$("."+newimg).show();
										$(this).hide();
										play_correct_incorrect_sound(1);
										$('.draggable').css('pointer-events','none');

										if(clicked > 1){
											scoring.update(false);
											$nextBtn.show();

										}else{
											scoring.update(true);
											$nextBtn.show();
										}


  									console.log('clicked ' +clicked);

										$('.first').show();

										count++;



										if(count == 4){
												navigationcontroller(true);
										}
								}
								else {
									 // play_correct_incorrect_sound(0);



										$(this).append("<img class='wrongImg' src='images/wrong.png'/>");
										ui.draggable.addClass("wrongcss");
										$(this).addClass("wrongcss");
										play_correct_incorrect_sound(0);
									//	$nextBtn.show();
										scoring.update(false);

								}
						}
				});
		}

		  function sound_player(sound_id,next){
		    createjs.Sound.stop();
		    current_sound = createjs.Sound.play(sound_id);
		    current_sound.play();
		  }
    function put_speechbox_image(content, count){
        if(content[count].hasOwnProperty('speechbox')){
            var speechbox = content[count].speechbox;
            for(var i=0; i<speechbox.length; i++){
                var image_src = preload.getResult(speechbox[i].imgid).src;
                console.log(image_src);
                var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
                $(selector).attr('src', image_src);
            }
        }
    }

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

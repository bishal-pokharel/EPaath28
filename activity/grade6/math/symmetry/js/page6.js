var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";

var preload;
var timeoutArr = [];
var timeoutvar = null;

var content = [


    //slide 0

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [{
            textclass: "instruction",
            textdata: data.string.p6text1,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },
        ],

        imageblock: [{
            imagestoshow: [{
                imgclass: "butterflyReflection",
                imgsrc: '',
                imgid: 'full_horizontal_mirror',
            },
            ]
        }]
    },

    //slide 1

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [
            {
                textclass: "instruction",
                textdata: data.string.p6text2,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "butterflyReflection",
                    imgsrc: '',
                    imgid: 'full_horizontal_mirror',
                },
                {
                    imgclass: "butterflyFold",
                    imgsrc: '',
                    imgid: 'fold_horizontal_mirror',
                },
            ]
        }]
    },

    //slide 2

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [
            {
                textclass: "instruction",
                textdata: data.string.p6text3,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "instructionDraw1",
                textdata: data.string.p6text4,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },

            {
                textclass: "textReal",
                textdata: data.string.p1text6,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "mirrorLine",
                textdata: data.string.mirrorline,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "textImage",
                textdata: data.string.p2text1,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "butterflyFold",
                    imgsrc: '',
                    imgid: 'fold_horizontal_mirror',
                },
                {
                    imgclass: "blackLine011",
                    imgsrc: '',
                    imgid: 'blackDottedLine',
                },
            ]
        }]
    },

    //slide 3

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [
            {
                textclass: "instruction",
                textdata: data.string.p6text5,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "instruction1",
                textdata: data.string.p6txt5,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "twoheadedbutterfly",
                    imgsrc: '',
                    imgid: 'butter_fly_twoheaded',
                },
            ]
        }]
    },

    //slide 4


    {
        contentblockadditionalclass: 'bg',
        extratextblock: [
            {
                textclass: "instruction",
                textdata: data.string.p6text6,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "instruction1",
                textdata: data.string.p6txt6,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "redLine",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "greenLine",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "butterflyReflection",
                    imgsrc: '',
                    imgid: 'full_horizontal_mirror',
                },
                {
                    imgclass: "redLine",
                    imgsrc: '',
                    imgid: 'redDottedLine',
                },
                {
                    imgclass: "greenLine",
                    imgsrc: '',
                    imgid: 'greenDottedLine',
                },
                {
                    imgclass: "correctFirst",
                    imgsrc: '',
                    imgid: 'correct',
                },
                {
                    imgclass: "incorrectFirst",
                    imgsrc: '',
                    imgid: 'wrong',
                },

            ]
        }]
    },

    //slide 5

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [
            {
                textclass: "instruction",
                textdata: data.string.p6text7,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "instructionDraw1",
                textdata: data.string.p6text8,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "instructionDraw11",
                textdata: data.string.p6txt8,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisOfSymmetry1",
                textdata: data.string.axis,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisOfSymmetry2",
                textdata: data.string.axis,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "butterflyReflection",
                    imgsrc: '',
                    imgid: 'full_horizontal_mirror',
                },
                {
                    imgclass: "greenLine",
                    imgsrc: '',
                    imgid: 'greenDottedLine',
                },
            ]
        }]
    },

    //slide 6

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [
            {
                textclass: "instruction",
                textdata: data.string.p6text9,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "instructionDraw1",
                textdata: data.string.p6text10,
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },


        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "butterflyFoldRight",
                    imgsrc: '',
                    imgid: 'verticalFold',
                },
                {
                    imgclass: "butterflyFoldLeft",
                    imgsrc: '',
                    imgid: 'fold_horizontal_mirror1',
                },
                {
                    imgclass: "greenLine01",
                    imgsrc: '',
                    imgid: 'greenDottedLine',
                },
                {
                    imgclass: "blackLine01",
                    imgsrc: '',
                    imgid: 'blackDottedLine',
                },
            ]
        }]
    },

]

$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            //images

            {id: "half_horizontal_butterfly", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "half_horizontal_mirror", src: imgpath+"img02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "full_horizontal_mirror", src: imgpath+"img03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fold_horizontal_mirror", src: imgpath+"butter_fly_twoheaded.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "fold_horizontal_mirror1", src: imgpath+"butter_fly_twoheaded1.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butter_fly_twoheaded", src: imgpath+"butter_fly_twoheaded.png", type: createjs.AbstractLoader.IMAGE},



            {id: "highlight_real_image", src: imgpath+"highlight_real_image.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "highlight_mirror_image", src: imgpath+"highlight_mirror_image.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly_with_reflection", src: imgpath+"butterfly02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pencil", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cut_butterfly", src: imgpath+"cut_butterfly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly", src: imgpath+"butterfly_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly_left", src: imgpath+"butterfly_left.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly_right", src: imgpath+"butterfly_right.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mirror_straight", src: imgpath+"mirror.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dotted_line", src: imgpath+"dotedline.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellow_dotted_line", src: imgpath+"yellow_doted_line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fullbutterfly", src: imgpath+"fullbutterfly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "greenDottedLine", src: imgpath+"green_doted_line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "redDottedLine", src: imgpath+"red_dotedline.png", type: createjs.AbstractLoader.IMAGE},
            {id: "blackDottedLine", src: imgpath+"black_doted_line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "verticalFold", src: imgpath+"butter_fly_twoheaded01.gif", type: createjs.AbstractLoader.IMAGE},

            {id: "butterflyanimationloop", src: imgpath+"butterflyanimationloop.gif", type: createjs.AbstractLoader.IMAGE},


            {id: "left_butterfly", src: imgpath+"img04c.png", type: createjs.AbstractLoader.IMAGE},
            {id: "full_butterfly", src: imgpath+"img04a_and_b.png", type: createjs.AbstractLoader.IMAGE},

            {id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wrong", src: imgpath+"wrong.png", type: createjs.AbstractLoader.IMAGE},



            // sounds
            {id: "s6_p1", src: soundAsset+"s6_p1.ogg"},
            {id: "s6_p2", src: soundAsset+"s6_p2.ogg"},
            {id: "s6_p3", src: soundAsset+"s6_p3.ogg"},
            {id: "s6_p4", src: soundAsset+"s6_p4.ogg"},
            {id: "s6_p5", src: soundAsset+"s6_p5.ogg"},
            {id: "s6_p6", src: soundAsset+"s6_p6.ogg"},
            {id: "s6_p7", src: soundAsset+"s6_p7.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
     =            Handlers and helpers Block            =
     ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

     What it does:
     - send an element where the function has to see
     for data to highlight
     - this function searches for all nodes whose
     data-highlight element is set to true
     -searches for # character and gives a start tag
     ;span tag here, also for @ character and replaces with
     end tag of the respective
     - if provided with data-highlightcustomclass value for highlight it
     applies the custom class or else uses parsedstring class

     E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/

    /*===============================================
    =            user notification function        =
    ===============================================*/
    /**
     How to:
     - First set any html element with
     "data-usernotification='notifyuser'" attribute,
     and "data-isclicked = ''".
     - Then call this function to give notification
     */

    /**
     What it does:
     - You send an element where the function has to see
     for data to notify user
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function notifyuser($notifyinside) {
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if ($allnotifications.length > 0) {
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }

    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */

    function nav_button_controls(delay_ms){
        timeoutvar = setTimeout(function(){
            if(countNext==0){
                $nextBtn.show(0);
            } else if( countNext>0 && countNext == $total_page-1){
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else{
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        },delay_ms);
    }

    function navigationcontroller(islastpageflag){
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ?
                ole.footerNotificationHandler.lessonEndSetNotification() :
                ole.footerNotificationHandler.pageEndSetNotification() ;
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            // islastpageflag ?
            //     ole.footerNotificationHandler.lessonEndSetNotification() :
            //     ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }



    /*=====  End of user navigation controller function  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
     =            Templates Block            =
     =======================================*/
    /*=================================================
     =            general template function            =
     =================================================*/
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);

    function generalTemplate() {
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        put_image(content, countNext);
        put_speechbox_image(content,countNext);

        $('.help-button').hide(0);

        $prevBtn.hide(0);
        $nextBtn.hide(0);

        function showNavs() {
            $prevBtn.show(0);
            $nextBtn.show(0);
        }

        var svgtoload;
        var lineCount;

        $('.butterflyFold').hide();
        //$('.butterflyNoReflection').show();

        switch(countNext){

            case 0:
            case 3:
            case 5:
              sound_player("s6_p"+(countNext+1),1);
            break;

            case 1:
                setTimeout(function(){
                    $('.butterflyFold').show();
                    $('.butterflyReflection').css('visibility','hidden');

                }, 1000);
                  sound_player("s6_p"+(countNext+1),1);
                break;

            case 2:
                $('.butterflyFold').show();
                  sound_player("s6_p"+(countNext+1),1);
                break;

            case 4:

                $('.incorrectFirst').hide();
                $('.correctFirst').hide();
                  sound_player("s6_p"+(countNext+1),0);

                $('.greenLine').click(function () {

                    $('.correctFirst').show();
                    $('.incorrectFirst').hide();
                    play_correct_incorrect_sound(1);
                    nav_button_controls(100);
                });

                $('.redLine').click(function () {

                    $('.incorrectFirst').show();
                    $('.correctFirst').hide();
                    play_correct_incorrect_sound(0);

                });

                break;

            default:
              sound_player("s6_p"+(countNext+1),1);
                break; // end of case 4

        }

    }

        function sound_player(sound_id,next){
          createjs.Sound.stop();
          current_sound = createjs.Sound.play(sound_id);
          current_sound.play();
          current_sound.on('complete', function(){
            next?nav_button_controls(300):'';
          });
        }
    /*=====  End of Templates Block  ======*/

    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
     Motivation :
     - Make a single function call that handles all the
     template load easier

     How To:
     - Update the template caller with the required templates
     - Call template caller

     What it does:
     - According to value of the Global Variable countNext
     the slide templates are updated
     */

    function templateCaller() {
        /*always hide next and previous navigation button unless
         explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();

        //call the slide indication bar handler for pink indicators
        loadTimelineProgress($total_page, countNext + 1);

        /*
        //   for (var i = 0; i < content.length; i++) {
        //     slides(i);
        //     $($('.totalsequence')[i]).html(i);
        //     $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
        //   "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
        //   }
        //   function slides(i){
        //       $($('.totalsequence')[i]).click(function(){
        //         countNext = i;
        //         templateCaller();
        //       });
        //     }
        */




        // just for development purpose to see total slide vs current slide number
        // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
    }

    function put_image(content, count){
        if(content[count].hasOwnProperty('imageblock')){
            var imageblock = content[count].imageblock[0];
            if(imageblock.hasOwnProperty('imagestoshow')){
                var imageClass = imageblock.imagestoshow;
                for(var i=0; i<imageClass.length; i++){
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.'+classes_list[classes_list.length-1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function put_speechbox_image(content, count){
        if(content[count].hasOwnProperty('speechbox')){
            var speechbox = content[count].speechbox;
            for(var i=0; i<speechbox.length; i++){
                var image_src = preload.getResult(speechbox[i].imgid).src;
                console.log(image_src);
                var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
                $(selector).attr('src', image_src);
            }
        }
    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    //templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        console.log('nxt clicked');
        clearTimeout(timeoutvar);
        createjs.Sound.stop();
        for(var i=0; i<timeoutArr.length; i++){
            clearTimeout(timeoutArr[i]);
        }
        switch(countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        console.log('$refreshBtn clicked')
        templateCaller();
    });

    $prevBtn.on('click', function() {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        for(var i=0; i<timeoutArr.length; i++){
            clearTimeout(timeoutArr[i]);
        }
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

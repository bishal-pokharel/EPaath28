var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";

var preload;
var timeoutArr = [];
var timeoutvar = null;

var content = [


    //slide 0

    {
        extratextblock: [{
            textclass: "diy",
            textdata: data.string.diy,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        }
      ],
      imageblock: [{
          imagestoshow: [{
              imgclass: "diyImage",
              imgsrc: '',
              imgid: 'diy',
          }]
        }]
    },

    //slide 1

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [{
            textclass: "instruction",
            textdata: data.string.p5text1,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

            {
                textclass: "axisVerticalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisVerticalMiddle",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalMiddle",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },{
                textclass: "axisVerticalLast",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalLast",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],

        imageblock: [{
            imagestoshow: [{
                imgclass: "heart",
                imgsrc: '',
                imgid: 'heart',
            },
            {
                imgclass: "heartFold",
                imgsrc: '',
                imgid: 'heartFold',
            },
            {
                imgclass: "bLetter",
                imgsrc: '',
                imgid: 'b',
            },
            {
                imgclass: "bFold",
                imgsrc: '',
                imgid: 'bFold',
            },
            {
                imgclass: "star",
                imgsrc: '',
                imgid: 'star',
            },
            {
                imgclass: "starFold",
                imgsrc: '',
                imgid: 'starFold',
            },
            {
                imgclass: "correctFirst",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectFirst",
                imgsrc: '',
                imgid: 'incorrect',
            },
            {
                imgclass: "correctMiddle",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectMiddle",
                imgsrc: '',
                imgid: 'incorrect',
            },
            {
                imgclass: "correctLast",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectLast",
                imgsrc: '',
                imgid: 'incorrect',
            },

            ]
        }]
    },

    //slide 2

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [{
            textclass: "instruction",
            textdata: data.string.p5text1,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

            {
                textclass: "axisVerticalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisVerticalMiddle1",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalMiddle",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },{
                textclass: "axisVerticalLast",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalLast1",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],

        imageblock: [{
            imagestoshow: [{
                imgclass: "heart",
                imgsrc: '',
                imgid: 'head',
            },
            {
                imgclass: "heartFold",
                imgsrc: '',
                imgid: 'headFold',
            },
            {
                imgclass: "bLetter",
                imgsrc: '',
                imgid: 'circle',
            },
            {
                imgclass: "bFold",
                imgsrc: '',
                imgid: 'circleFold',
            },
            {
                imgclass: "star",
                imgsrc: '',
                imgid: 'triangle',
            },
            {
                imgclass: "starFold",
                imgsrc: '',
                imgid: 'triangleFold',
            },
            {
                imgclass: "correctFirst",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectFirst",
                imgsrc: '',
                imgid: 'incorrect',
            },
            {
                imgclass: "correctMiddle",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectMiddle",
                imgsrc: '',
                imgid: 'incorrect',
            },
            {
                imgclass: "correctLast",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectLast",
                imgsrc: '',
                imgid: 'incorrect',
            },

            ]
        }]
    },

    //slide 3

    {
        contentblockadditionalclass: 'bg',
        extratextblock: [{
            textclass: "instruction",
            textdata: data.string.p5text1,
            datahighlightflag: true,
            datahighlightcustomclass: 'grenLtr'
        },

            {
                textclass: "axisVerticalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalFirst",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisVerticalMiddle",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalMiddle1",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },{
                textclass: "axisVerticalLast",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
            {
                textclass: "axisHorizontalLast1",
                textdata: '',
                datahighlightflag: true,
                datahighlightcustomclass: 'grenLtr'
            },
        ],

        imageblock: [{
            imagestoshow: [
            {
                imgclass: "heart",
                imgsrc: '',
                imgid: 'heart01',
            },
            {
                imgclass: "heartFold",
                imgsrc: '',
                imgid: 'heart01Fold',
            },
            {
                imgclass: "bLetter",
                imgsrc: '',
                imgid: 'arrow',
            },
            {
                imgclass: "bFold",
                imgsrc: '',
                imgid: 'arrowFold',
            },
            {
                imgclass: "star",
                imgsrc: '',
                imgid: 'square',
            },
            {
                imgclass: "starFold",
                imgsrc: '',
                imgid: 'squareFold',
            },
            {
                imgclass: "correctFirst",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectFirst",
                imgsrc: '',
                imgid: 'incorrect',
            },
            {
                imgclass: "correctMiddle",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectMiddle",
                imgsrc: '',
                imgid: 'incorrect',
            },
            {
                imgclass: "correctLast",
                imgsrc: '',
                imgid: 'correct',
            },
            {
                imgclass: "incorrectLast",
                imgsrc: '',
                imgid: 'incorrect',
            },

            ]
        }]
    },

]

$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

   // loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            //images

            {id: "arrow", src: imgpath+"up_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowFold", src: imgpath+"up_arrow.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "diy", src: imgpath+"a_24.png", type: createjs.AbstractLoader.IMAGE},

            {id: "circle", src: imgpath+"circle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "circleFold", src: imgpath+"circle.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "b", src: imgpath+"b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bFold", src: imgpath+"b.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "head", src: imgpath+"head.png", type: createjs.AbstractLoader.IMAGE},
            {id: "headFold", src: imgpath+"dog.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "heart", src: imgpath+"heart.png", type: createjs.AbstractLoader.IMAGE},
            {id: "heartFold", src: imgpath+"heart.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "heart01", src: imgpath+"heart01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "heart01Fold", src: imgpath+"heart01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "hexagon", src: imgpath+"hexagon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "square", src: imgpath+"squire.png", type: createjs.AbstractLoader.IMAGE},
            {id: "squareFold", src: imgpath+"squire.gif", type: createjs.AbstractLoader.IMAGE},

            {id: "triangle", src: imgpath+"triangle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "triangleFold", src: imgpath+"triangle.gif", type: createjs.AbstractLoader.IMAGE},


            {id: "star", src: imgpath+"star.png", type: createjs.AbstractLoader.IMAGE},
            {id: "starFold", src: imgpath+"star.gif", type: createjs.AbstractLoader.IMAGE},

            {id: "greenDottedLine", src: imgpath+"green_doted_line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "redDottedLine", src: imgpath+"red_dotedline.png", type: createjs.AbstractLoader.IMAGE},
            {id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
            {id: "incorrect", src: imgpath+"wrong.png", type: createjs.AbstractLoader.IMAGE},


            {id: "highlight_real_image", src: imgpath+"highlight_real_image.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "highlight_mirror_image", src: imgpath+"highlight_mirror_image.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly_with_reflection", src: imgpath+"butterfly02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pencil", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cut_butterfly", src: imgpath+"cut_butterfly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly", src: imgpath+"butterfly_full.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly_left", src: imgpath+"butterfly_left.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly_right", src: imgpath+"butterfly_right.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mirror_straight", src: imgpath+"mirror.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dotted_line", src: imgpath+"dotedline.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellow_dotted_line", src: imgpath+"yellow_doted_line.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fullbutterfly", src: imgpath+"fullbutterfly.png", type: createjs.AbstractLoader.IMAGE},

            {id: "butterflyanimationloop", src: imgpath+"butterflyanimationloop.gif", type: createjs.AbstractLoader.IMAGE},


            {id: "left_butterfly", src: imgpath+"img04c.png", type: createjs.AbstractLoader.IMAGE},
            {id: "full_butterfly", src: imgpath+"img04a_and_b.png", type: createjs.AbstractLoader.IMAGE},

            {id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wrong", src: imgpath+"wrong.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
          	{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        // call main function
        templateCaller();
    }


    //initialize
    init();

    /*==================================================
     =            Handlers and helpers Block            =
     ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

     What it does:
     - send an element where the function has to see
     for data to highlight
     - this function searches for all nodes whose
     data-highlight element is set to true
     -searches for # character and gives a start tag
     ;span tag here, also for @ character and replaces with
     end tag of the respective
     - if provided with data-highlightcustomclass value for highlight it
     applies the custom class or else uses parsedstring class

     E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/

    /*===============================================
    =            user notification function        =
    ===============================================*/
    /**
     How to:
     - First set any html element with
     "data-usernotification='notifyuser'" attribute,
     and "data-isclicked = ''".
     - Then call this function to give notification
     */

    /**
     What it does:
     - You send an element where the function has to see
     for data to notify user
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function notifyuser($notifyinside) {
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if ($allnotifications.length > 0) {
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }

    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */

    function nav_button_controls(delay_ms){
        timeoutvar = setTimeout(function(){
            if(countNext==0){
                $nextBtn.show(0);
            } else if( countNext>0 && countNext == $total_page-1){
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else{
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        },delay_ms);
    }

    function navigationcontroller(islastpageflag){
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ?
                ole.footerNotificationHandler.lessonEndSetNotification() :
                ole.footerNotificationHandler.pageEndSetNotification() ;
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            // islastpageflag ?
            //     ole.footerNotificationHandler.lessonEndSetNotification() :
            //     ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }



    /*=====  End of user navigation controller function  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
     =            Templates Block            =
     =======================================*/
    /*=================================================
     =            general template function            =
     =================================================*/
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);

    function generalTemplate() {
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        put_image(content, countNext);
        put_speechbox_image(content,countNext);

        $('.help-button').hide(0);

        $prevBtn.hide(0);
        $nextBtn.hide(0);

        function showNavs() {
            $prevBtn.show(0);
            $nextBtn.show(0);


        }

        var svgtoload;
        var lineCount;


        // function scaleAndChangeColor($value) {
        //    $value.css('border', '2.3px dashed #b9d32f')
        //    $value.css('visibility', 'visible');
        // }

        $('.correctFirst').hide();
        $('.incorrectFirst').hide();
        $('.correctMiddle').hide();
        $('.incorrectMiddle').hide();
        $('.correctLast').hide();
        $('.incorrectLast').hide();

        var correct1 = false;
        var correct2 = false;
        var correct3 = false;

        $('.heartFold').hide();
        $('.bFold').hide();
        $('.starFold').hide();

        switch(countNext){

            case 0:
              play_diy_audio();
              nav_button_controls(2000);
            break;

            case 1:
              sound_player("s5_p2");

                // $('.heartFold').hide();
                // $('.bFold').hide();
                // $('.starFold').hide();

                // For First Choice

                $('.axisVerticalFirst').click(function () {
                  createjs.Sound.stop();


                    $('.axisVerticalFirst').css('border','3.2px dashed #b9d32f');
                    $('.axisHorizontalFirst').css('border','3.2px dashed black');
                    $('.axisHorizontalFirst').css('pointer-events','none');
                    $('.correctFirst').show();
                    $('.incorrectFirst').hide();

                     $('.heartFold').show();
                     $('.heart').hide();

                    play_correct_incorrect_sound(1);
                    correct1 = true;

                    check();


                    // foldImage();


                });

                $('.axisHorizontalFirst').click(function () {
                  createjs.Sound.stop();


                    $('.axisHorizontalFirst').css('border','3.2px dashed #f1593f');
                    $('.axisVerticalFirst').css('border','3.2px dashed black');
                    $('.correctFirst').hide();
                    $('.incorrectFirst').show();

                    play_correct_incorrect_sound(0);
                    correct1 = false;

                    check();

                    // foldImage();


                });



                // For Middle Choice

                $('.axisVerticalMiddle').click(function () {
                  createjs.Sound.stop();


                    $('.axisVerticalMiddle').css('border','3.2px dashed #f1593f');
                    $('.axisHorizontalMiddle').css('border','3.2px dashed black');
                    $('.correctMiddle').hide();
                    $('.incorrectMiddle').show();
                    correct2 = false;
                    play_correct_incorrect_sound(0);

                    check();

                   // foldImage();


                });

                $('.axisHorizontalMiddle').click(function () {
                  createjs.Sound.stop();


                    $('.axisHorizontalMiddle').css('border','3.2px dashed #b9d32f');
                    $('.axisVerticalMiddle').css('border','3.2px dashed black');
                    $('.axisVerticalMiddle').css('pointer-events','none');
                    $('.correctMiddle').show();
                    $('.incorrectMiddle').hide();


                     $('.bFold').show();
                     $('.bLetter').hide();
                     play_correct_incorrect_sound(1);

                    correct2 = true;

                    check();

                    // foldImage();


                });

                // For Last Choice

                $('.axisVerticalLast').click(function () {
                  createjs.Sound.stop();


                    $('.axisVerticalLast').css('border','3.2px dashed #b9d32f');
                    $('.axisHorizontalLast').css('border','3.2px dashed black');
                    $('.axisHorizontalLast').css('pointer-events','none');
                    $('.correctLast').show();
                    $('.incorrectLast').hide();
                    correct3 = true;

                    play_correct_incorrect_sound(1);

                    $('.starFold').show();
                    $('.star').hide();


                    check();

                    // foldImage();


                });

                $('.axisHorizontalLast').click(function () {
                  createjs.Sound.stop();


                    $('.axisHorizontalLast').css('border','3.2px dashed #f1593f');
                    $('.axisVerticalLast').css('border','3.2px dashed black');
                    $('.correctLast').hide();
                    $('.incorrectLast').show();
                    correct3 = false;
                    play_correct_incorrect_sound(0);

                    check();

                    // foldImage();


                });

                // foldImage = function () {
                //     $('.bLetter').attr(src,preload.getResult(''));
                // };

                check = function () {
                    if (correct1 === true && correct2 === true && correct3 === true){
                        nav_button_controls();
                    }
                }



                break;

            case 2:

                // $('.heartFold').hide();
                // $('.bFold').hide();
                // $('.starFold').hide();

                // For First Choice

                $('.axisVerticalFirst').click(function () {


                    $('.axisVerticalFirst').css('border','3.2px dashed #b9d32f');
                    $('.axisHorizontalFirst').css('border','3.2px dashed black');
                    $('.axisHorizontalFirst').css('pointer-events','none');
                    $('.correctFirst').show();
                    $('.incorrectFirst').hide();
                    correct1 = true;

                    $('.heartFold').show();
                    $('.heart').hide();
                    play_correct_incorrect_sound(1);

                    check();


                    // foldImage();


                });

                $('.axisHorizontalFirst').click(function () {


                    $('.axisHorizontalFirst').css('border','3.2px dashed #f1593f');
                    $('.axisVerticalFirst').css('border','3.2px dashed black');
                    $('.correctFirst').hide();
                    $('.incorrectFirst').show();
                    correct1 = false;
                    play_correct_incorrect_sound(0);

                    check();

                    // foldImage();


                });



                // For Middle Choice

                $('.axisVerticalMiddle1').click(function () {


                    $('.axisVerticalMiddle1').css('border','3.2px dashed #f1593f');
                    $('.axisHorizontalMiddle').css('border','3.2px dashed black');
                    $('.correctMiddle').hide();
                    $('.incorrectMiddle').show();
                    correct2 = false;
                    play_correct_incorrect_sound(0);

                    check();

                    // foldImage();


                });

                $('.axisHorizontalMiddle').click(function () {


                    $('.axisHorizontalMiddle').css('border','3.2px dashed #b9d32f');
                    $('.axisVerticalMiddle1').css('border','3.2px dashed black');
                    $('.axisVerticalMiddle1').css('pointer-events','none');
                    $('.correctMiddle').show();
                    $('.incorrectMiddle').hide();
                    correct2 = true;
                    play_correct_incorrect_sound(1);

                    check();

                    $('.bFold').show();
                    $('.bLetter').hide();

                    // foldImage();

                });

                // For Last Choice

                $('.axisVerticalLast').click(function () {

                    console.log('m here');


                    $('.axisVerticalLast').css('border','3.2px dashed #b9d32f');
                    $('.axisHorizontalLast1').css('border','3.2px dashed black');
                    $('.axisHorizontalLast1').css('pointer-events','none');
                    $('.correctLast').show();
                    $('.incorrectLast').hide();
                    correct3 = true;
                    play_correct_incorrect_sound(1);

                    check();

                    $('.starFold').show();
                    $('.star').hide();

                    // foldImage();


                });

                $('.axisHorizontalLast1').click(function () {


                    $('.axisHorizontalLast1').css('border','3.2px dashed #f1593f');
                    $('.axisVerticalLast').css('border','3.2px dashed black');
                    $('.correctLast').hide();
                    $('.incorrectLast').show();
                    correct3 = false;
                    play_correct_incorrect_sound(0);
                    check();

                    // foldImage();


                });

                // foldImage = function () {
                //     $('.bLetter').attr(src,preload.getResult(''));
                // };

                check = function () {
                    if (correct1 === true && correct2 === true && correct3 === true){
                        nav_button_controls();
                    }
                }

                break;

            case 3:

                // $('.heartFold').hide();
                // $('.bFold').hide();
                // $('.starFold').hide();

                // For First Choice

                $('.axisVerticalFirst').click(function () {


                    $('.axisVerticalFirst').css('border','3.2px dashed #f1593f');
                    $('.axisHorizontalFirst').css('border','3.2px dashed black');
                    $('.correctFirst').hide();
                    $('.incorrectFirst').show();
                    correct1 = false;
                    play_correct_incorrect_sound(0);

                    check();


                    // foldImage();


                });

                $('.axisHorizontalFirst').click(function () {


                    $('.axisHorizontalFirst').css('border','3.2px dashed #b9d32f');
                    $('.axisVerticalFirst').css('border','3.2px dashed black');
                    $('.axisVerticalFirst').css('pointer-events','none');
                    $('.correctFirst').show();
                    $('.incorrectFirst').hide();
                    correct1 = true;
                    play_correct_incorrect_sound(1);

                    $('.heartFold').show();
                    $('.heart').hide();

                    check();

                    // foldImage();


                });



                // For Middle Choice

                $('.axisVerticalMiddle').click(function () {


                    $('.axisVerticalMiddle').css('border','3.2px dashed #b9d32f');
                    $('.axisHorizontalMiddle1').css('border','3.2px dashed black');
                    $('.axisHorizontalMiddle1').css('pointer-events','none');
                    $('.correctMiddle').show();
                    $('.incorrectMiddle').hide();
                    correct2 = true;
                    play_correct_incorrect_sound(1);

                    $('.bFold').show();
                    $('.bLetter').hide();

                    check();

                    // foldImage();


                });

                $('.axisHorizontalMiddle1').click(function () {


                    $('.axisHorizontalMiddle1').css('border','3.2px dashed #f1593f');
                    $('.axisVerticalMiddle').css('border','3.2px dashed black');
                    $('.correctMiddle').hide();
                    $('.incorrectMiddle').show();
                    correct2 = false;
                    play_correct_incorrect_sound(0);

                    check();

                    // foldImage();


                });

                // For Last Choice

                $('.axisVerticalLast').click(function () {


                    $('.axisVerticalLast').css('border','3.2px dashed #b9d32f');
                    $('.axisHorizontalLast1').css('border','3.2px dashed black');
                    $('.axisHorizontalLast1').css('pointer-events','none');
                    $('.correctLast').show();
                    $('.incorrectLast').hide();
                    correct3 = true;
                    play_correct_incorrect_sound(1);

                    $('.starFold').show();
                    $('.star').hide();

                    check();

                    // foldImage();


                });

                $('.axisHorizontalLast1').click(function () {

                    $('.axisHorizontalLast1').css('border','3.2px dashed #f1593f');
                    $('.axisVerticalLast').css('border','3.2px dashed black');
                    $('.correctLast').hide();
                    $('.incorrectLast').show();
                    correct3 = false;
                    play_correct_incorrect_sound(0);
                    check();

                    // foldImage();

                });

                // foldImage = function () {
                //     $('.bLetter').attr(src,preload.getResult(''));
                // };

                check = function () {
                    if (correct1 === true && correct2 === true && correct3 === true){
                       // $prevBtn.show(0);
                        nav_button_controls();
                    }
                }



                break;

            default:

                nav_button_controls(100);
                break; // end of case 4


        }

    }

    /*=====  End of Templates Block  ======*/

    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
     Motivation :
     - Make a single function call that handles all the
     template load easier

     How To:
     - Update the template caller with the required templates
     - Call template caller

     What it does:
     - According to value of the Global Variable countNext
     the slide templates are updated
     */

     function sound_player(sound_id, next){
       createjs.Sound.stop();
       current_sound = createjs.Sound.play(sound_id);
       current_sound.play();
     }

    function templateCaller() {
        /*always hide next and previous navigation button unless
         explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();

        //call the slide indication bar handler for pink indicators
        loadTimelineProgress($total_page, countNext + 1);

        /*
        //   for (var i = 0; i < content.length; i++) {
        //     slides(i);
        //     $($('.totalsequence')[i]).html(i);
        //     $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
        //   "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
        //   }
        //   function slides(i){
        //       $($('.totalsequence')[i]).click(function(){
        //         countNext = i;
        //         templateCaller();
        //       });
        //     }
        */




        // just for development purpose to see total slide vs current slide number
        // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
    }

    function put_image(content, count){
        if(content[count].hasOwnProperty('imageblock')){
            var imageblock = content[count].imageblock[0];
            if(imageblock.hasOwnProperty('imagestoshow')){
                var imageClass = imageblock.imagestoshow;
                for(var i=0; i<imageClass.length; i++){
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.'+classes_list[classes_list.length-1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function put_speechbox_image(content, count){
        if(content[count].hasOwnProperty('speechbox')){
            var speechbox = content[count].speechbox;
            for(var i=0; i<speechbox.length; i++){
                var image_src = preload.getResult(speechbox[i].imgid).src;
                console.log(image_src);
                var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
                $(selector).attr('src', image_src);
            }
        }
    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    //templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        console.log('nxt clicked');
        clearTimeout(timeoutvar);
        createjs.Sound.stop();
        for(var i=0; i<timeoutArr.length; i++){
            clearTimeout(timeoutArr[i]);
        }
        switch(countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        console.log('$refreshBtn clicked')
        templateCaller();
    });

    $prevBtn.on('click', function() {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        for(var i=0; i<timeoutArr.length; i++){
            clearTimeout(timeoutArr[i]);
        }
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

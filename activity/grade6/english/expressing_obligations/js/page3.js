var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_0 = new buzz.sound((soundAsset + "p3_s0.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p3_s5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p3_s6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p3_s7.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p3_s8.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p3_s9.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p3_s10.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p3_s11.ogg"));
var listob = new buzz.sound((soundAsset + "listob.ogg"));


var sounds=[ sound_0, sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11];

var content=[
 {
  	//page 0
  	contentnocenteradjust: true,
    lowertextblock: [{
      textclass: "fingertext",
  		textdata: data.string.p3_s37
  	}],
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},
      {
  			imgclass: "students1",
  			imgsrc : imgpath+"students.png"
  		}]
  	}]
  },{
  	//page 1
  	contentnocenteradjust: true,
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},
      {
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p3_s18
  		}]
  	}]
  },{
  	//page 2
  	contentnocenteradjust: true,
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},{
  			imgclass: "cloudimg",
  			imgsrc: imgpath+"trippin-08.png"
  		},{
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
      imagelabels: [{
    		datahighlightflag: true,
    		datahighlightcustomclass: "jpt",
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p3_s24
  		}]
  	}]
  },{
  	//page 3
  	contentnocenteradjust: true,
    lowertextblock: [{
      textclass: "uppertext",
  		textdata: data.string.p3_s38
  	}],
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},{
  			imgclass: "cloudimg",
  			imgsrc: imgpath+"trippin-08.png"
  		},
      {
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
  		imagelabels: [{
    		datahighlightflag: true,
    		datahighlightcustomclass: "for_click",
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p3_s24
  		}]
  	}],
    boardblock:[{
      boardhead:[{
        textclass: "boardtitle",
        textdata: data.string.toc_8
      }],
      boardtext:[
        {
          textclass: "boardtext currentbtext",
          textdata: data.string.p3_s39
        },
      ]
    }]
  },{
  	//page 4
  	contentnocenteradjust: true,
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},
      {
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		},{
  			imgclass: "cloudimg",
  			imgsrc: imgpath+"putting book-06-06.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
      imagelabels: [{
    		datahighlightflag: true,
    		datahighlightcustomclass: "jpt",
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p3_s25
  		}]
  	}]
  },{
  	//page 5
  	contentnocenteradjust: true,
    lowertextblock: [{
      textclass: "uppertext",
  		textdata: data.string.p3_s38
  	}],
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},{
  			imgclass: "cloudimg",
  			imgsrc: imgpath+"putting book-06-06.png"
  		},
      {
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
  		imagelabels: [{
    		datahighlightflag: true,
    		datahighlightcustomclass: "for_click",
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p3_s25
  		}]
  	}],
    boardblock:[{
      boardhead:[{
        textclass: "boardtitle",
        textdata: data.string.toc_8
      }],
      boardtext:[
        {
          textclass: "boardtext",
          textdata: data.string.p3_s39
        },
        {
          textclass: "boardtext currentbtext",
          textdata: data.string.p3_s40
        }
      ]
    }]
  },{
  	//page 6
  	contentnocenteradjust: true,
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},{
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		},{
  			imgclass: "cloudimg",
  			imgsrc: imgpath+"library final-09.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
      imagelabels: [{
    		datahighlightflag: true,
    		datahighlightcustomclass: "jpt",
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p3_s26
  		}]
  	}]
  },{
  	//page 7
  	contentnocenteradjust: true,
    lowertextblock: [{
      textclass: "uppertext",
  		textdata: data.string.p3_s38
  	}],
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},{
  			imgclass: "cloudimg",
  			imgsrc: imgpath+"library final-09.png"
  		},
      {
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
  		imagelabels: [{
    		datahighlightflag: true,
    		datahighlightcustomclass: "for_click",
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p3_s26
  		}]
  	}],
    boardblock:[{
      boardhead:[{
        textclass: "boardtitle",
        textdata: data.string.toc_8
      }],
      boardtext:[
        {
          textclass: "boardtext",
          textdata: data.string.p3_s39
        },
        {
          textclass: "boardtext",
          textdata: data.string.p3_s40
        },
        {
          textclass: "boardtext currentbtext",
          textdata: data.string.p3_s41
        }
      ]
    }]
  },{
  	//page 8
  	contentnocenteradjust: true,
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},{
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		},{
  			imgclass: "cloudimg",
  			imgsrc: imgpath+"library final-09.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
      imagelabels: [{
    		datahighlightflag: true,
    		datahighlightcustomclass: "jpt",
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p3_s27
  		}]
  	}]
  },{
  	//page 9
  	contentnocenteradjust: true,
    lowertextblock: [{
      textclass: "uppertext",
  		textdata: data.string.p3_s38
  	}],
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},{
  			imgclass: "cloudimg",
  			imgsrc: imgpath+"library final-09.png"
  		},
      {
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
  		imagelabels: [{
    		datahighlightflag: true,
    		datahighlightcustomclass: "for_click",
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p3_s27
  		}]
  	}],
    boardblock:[{
      boardhead:[{
        textclass: "boardtitle",
        textdata: data.string.toc_8
      }],
      boardtext:[
        {
          textclass: "boardtext",
          textdata: data.string.p3_s39
        },
        {
          textclass: "boardtext",
          textdata: data.string.p3_s40
        },
        {
          textclass: "boardtext",
          textdata: data.string.p3_s41
        },
        {
          textclass: "boardtext currentbtext",
          textdata: data.string.p3_s42
        }
      ]
    }]
  },{
  	//page 10
  	contentnocenteradjust: true,
    contentblockadditionalclass: "yellback",
    lowertextblock: [{
      textclass: "uppertext",
  		textdata: data.string.p3_s43
  	}],
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc: imgpath+"saahil-14.png"
  		}]
  	}],
    boardadditionalclass: "boardanim",
    boardblock:[{
      boardhead:[{
        textclass: "boardtitle",
        textdata: data.string.toc_8
      }],
      boardtext:[
        {
          textclass: "boardtext",
          textdata: data.string.p3_s39
        },
        {
          textclass: "boardtext",
          textdata: data.string.p3_s40
        },
        {
          textclass: "boardtext",
          textdata: data.string.p3_s41
        },
        {
          textclass: "boardtext",
          textdata: data.string.p3_s42
        }
      ]
    }]
  },{
  	//page 11
  	contentnocenteradjust: true,
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "dialogimage",
  			imgsrc : imgpath+"library.png"
  		},{
  			imgclass: "teacher",
  			imgsrc: imgpath+"miss-50.png"
  		},{
  			imgclass: "librarian",
  			imgsrc: imgpath+"saahil-14.png"
  		},
      {
  			imgclass: "students2",
  			imgsrc : imgpath+"kids.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p3_s28
  		}]
  	}]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("boardcontent", $("#boardcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 2500);

		}
	}
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  var playing_audio;
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);
	$nextBtn.hide(0);
	if(countNext==10){
			playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				listob.play();
				listob.bind('ended', function(){
					$nextBtn.show();
			});
		});
	}
	switch(countNext){
		case 0:
		case 1:
		case 2:
		case 4:
		case 6:
		case 8:
		case 11:
			playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				countNext==11?ole.footerNotificationHandler.pageEndSetNotification():$nextBtn.delay(200).show(0);
			});
			break;
		case 5:
            playing_audio.stop();
            playing_audio = sounds[3];
            playing_audio.play();
            $(".dialogcontent2 > span:nth-of-type(2)").click(function(){
                $(this).css("background","#7eb67e").removeClass("for_click");
                setTimeout(function(){
                    $(".boardblock").addClass("boardanim");
                    setTimeout(function(){
                        $(".currentbtext").fadeIn();
                    },2000);
                    $(".dialogcontainer1_b").hide(0);
                    $nextBtn.show(0);
                },2000);
                play_correct_incorrect_sound(1);
            });
            $(".dialogcontent2 > span:nth-of-type(1)").click(function(){
                play_correct_incorrect_sound(0);
                $(this).css("background","#f27d7d").removeClass("for_click");
            });
            break;
    case 3:
    case 7:
    case 9:
        playing_audio.stop();
        playing_audio = sounds[3];
        playing_audio.play();
      $(".dialogcontent2 > span:nth-of-type(1)").click(function(){
        $(this).css("background","#7eb67e").removeClass("for_click");
        setTimeout(function(){
        $(".boardblock").addClass("boardanim");
        setTimeout(function(){
          $(".currentbtext").fadeIn();
        },2000);
        $(".dialogcontainer1_b").hide(0);
            $nextBtn.show(0);
        },2000);
          play_correct_incorrect_sound(1);
      });
      $(".dialogcontent2 > span:nth-of-type(2)").click(function(){
          play_correct_incorrect_sound(0);
          $(this).css("background","#f27d7d").removeClass("for_click");
      });
    break;

		default:
			break;
	}
  }

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call the template
			generaltemplate();

			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
			playing_audio.stop();
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
			playing_audio.stop();
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

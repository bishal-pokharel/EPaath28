var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_0 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p1_s9.ogg"));
var havaudio = new buzz.sound((soundAsset + "p1_s8b.ogg"));
var qtips = new buzz.sound((soundAsset + "qtips.ogg"));


var sounds=[ sound_0, sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_8, sound_9];

var content=[
 {
  	//page 0
  	contentblockadditionalclass: "coverbg",
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p1_heading
  	}]
  },{
  	//page 1
  	contentblockadditionalclass: "skybackground",
  	uppertextblockadditionalclass: "firsttitle2",
  	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p1_s1,
  		datahighlightflag: true,
  		datahighlightcustomclass: "underline_highlight"
  	}]
  },{
  	//page 3
  	contentblockadditionalclass: "skybackground",
  	uppertextblockadditionalclass: "firsttitle2",
  	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p1_s3,
  		datahighlightflag: true,
  		datahighlightcustomclass: "underline_highlight"
  	},{
  		textclass: "maintitle2",
  		textdata: data.string.p1_s4,
  		datahighlightflag: true,
  		datahighlightcustomclass: "underline_highlight"
  	}]
  },{
  	//page 4
  	contentblockadditionalclass: "dottedbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p1_s5,
  		datahighlightflag: true,
  		datahighlightcustomclass: "underline_highlight"
  	}],
  	flipperimageblock: [{
  			imgcontainerdiv: "container1",
  			front:[{
  				imagelabels:[{
  					imagelabelclass: "container_label",
  					imagelabeldata: data.string.p1_s6
  				}]
  			}],
  			back:[{
  				imagestoshow:[{
  					imgclass: "image",
  					imgsrc: imgpath+ "prem-15.png"
  				}],
  				dialoglabels:[{
  					dialogimagelabelclass: "dialoglabel",
  					dilaogimagelabeldata: data.string.p1_s11
  				}]
  			}]
  	},{
  			imgcontainerdiv: "container2",
  			front:[{
  				imagelabels:[{
  					imagelabelclass: "container_label",
  					imagelabeldata: data.string.p1_s7
  				}]
  			}],
  			back:[{
  				imagestoshow:[{
  					imgclass: "image",
  					imgsrc: imgpath+ "sagar-06.png"
  				}],
  				dialoglabels:[{
  					dialogimagelabelclass: "dialoglabel",
  					dilaogimagelabeldata: data.string.p1_s12
  				}]
  			}]
  	},{
  			imgcontainerdiv: "container3",
  			front:[{
  				imagelabels:[{
  					imagelabelclass: "container_label",
  					imagelabeldata: data.string.p1_s8
  				}]
  			}],
  			back:[{
  				imagestoshow:[{
  					imgclass: "image mirror_image",
  					imgsrc: imgpath+ "rumi-15.png"
  				}],
  				dialoglabels:[{
  					dialogimagelabelclass: "dialoglabel",
  					dilaogimagelabeldata: data.string.p1_s13
  				}]
  			}]
  	},{
  			imgcontainerdiv: "container4",
  			front:[{
  				imagelabels:[{
  					imagelabelclass: "container_label",
  					imagelabeldata: data.string.p1_s9
  				}]
  			}],
  			back:[{
  				imagestoshow:[{
  					imgclass: "image mirror_image",
  					imgsrc: imgpath+ "Niti-03.png"
  				}],
  				dialoglabels:[{
  					dialogimagelabelclass: "dialoglabel",
  					dilaogimagelabeldata: data.string.p1_s14
  				}]
  			}]
  	},{
  		imgcontainerdiv: "container_instructions",
  		front:[{
  			imagestoshow:[{
				imgclass: "caterpillar",
				imgsrc: imgpath+ "caterpillar.gif"
			}],
			imagelabels:[{
				imagelabelclass: "dialoglabel_instruction",
				imagelabeldata: data.string.p1_s10
			}]
  		}]
  	}],
  	lowertextblockadditionalclass: "moreinfo",
  	lowertextblock:[{
  			textclass: "heading",
  			textdata: data.string.p1_s15
  		},{
  			listcontent: true,
  			listcontent:[{
  				textclass: "listcontent",
  				textdata: data.string.p1_s16
  			},{
  				textclass: "listcontent",
  				textdata: data.string.p1_s17
  			},{
  				textclass: "listcontent",
  				textdata: data.string.p1_s18
  			},{
  				textclass: "listcontent",
  				textdata: data.string.p1_s19
  			}]
  		},{
  			textclass: "close_button",
  			textdata: "&nbsp;"
  		}
  	],
  	learn_more: true
  },{
  	//page 5
  	contentblockadditionalclass: "dottedbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p1_s20,
  		datahighlightflag: true,
  		datahighlightcustomclass: "underline_highlight"
  	}],
  	flipperimageblock: [{
  		imgcontainerdiv: "container_instructions_2",
  		front:[{
  			imagestoshow:[{
				imgclass: "boy",
				imgsrc: imgpath+ "sagar-06.png"
			}],
			imagelabels:[{
				imagelabelclass: "dialoglabel_6_1",
				imagelabeldata: data.string.p1_s21
			},{
				imagelabelclass: "dialoglabel_6_2",
				imagelabeldata: data.string.p1_s22
			},{
				imagelabelclass: "dialoglabel_6_3",
				imagelabeldata: data.string.p1_s23
			},{
				imagelabelclass: "dialoglabel_6_4",
				imagelabeldata: data.string.p1_s24
			},{
				imagelabelclass: "dialoglabel_6_5",
				imagelabeldata: data.string.p1_s25
			},{
				imagelabelclass: "dialoglabel_6_6",
				imagelabeldata: data.string.p1_s26
			}]
  		}]
  	}],
  	lowertextblockadditionalclass: "lowertextblock_addn",
  	lowertextblock:[{
  			textclass: "heading2",
  			textdata: data.string.p1_s27
  	}]
  },{
  	//page 6
  	contentblockadditionalclass: "dottedbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p1_s28,
  		datahighlightflag: true,
  		datahighlightcustomclass: "underline_highlight"
  	}],
  	flipperimageblock: [{
  		imgcontainerdiv: "container_instructions_2",
  		front:[{
  			imagestoshow:[{
				imgclass: "boy",
				imgsrc: imgpath+ "sagar-06.png"
			}],
			imagelabels:[{
				imagelabelclass: "dialoglabel_2_1",
				imagelabeldata: data.string.p1_s29
			},{
				imagelabelclass: "dialoglabel_2_2",
				imagelabeldata: data.string.p1_s30
			},{
				imagelabelclass: "dialoglabel_6_5",
				imagelabeldata: data.string.p1_s32
			},{
				imagelabelclass: "dialoglabel_6_6",
				imagelabeldata: data.string.p1_s33
			}]
  		}]
  	}],
  	lowertextblockadditionalclass: "lowertextblock_addn",
  	lowertextblock:[{
  			textclass: "heading2",
  			textdata: data.string.p1_s31
  	}]
  },
  {
   	//page 0
   	contentblockadditionalclass: "skybackground",
   	uppertextblockadditionalclass: "firsttitle2",
   	uppertextblock: [{
   		textclass: "maintitle2",
   		textdata: data.string.p2_s1,
   		datahighlightflag: true,
   		datahighlightcustomclass: "underline_highlight"
   	}]
   },{
   	//page 1
   	contentblockadditionalclass: "dottedbackground",
   	contentnocenteradjust: true,
   	uppertextblock: [{
   		textclass: "maintitle2",
   		textdata: data.string.p2_s2,
   		datahighlightflag: true,
   		datahighlightcustomclass: "underline_highlight"
   	}],
   	flipperimageblock: [{
   			imgcontainerdiv: "container1",
   			front:[{
   				imagelabels:[{
   					imagelabelclass: "container_label",
   					imagelabeldata: data.string.p2_s3
   				}]
   			}],
   			back:[{
   				imagestoshow:[{
   					imgclass: "image mirror_image",
   					imgsrc: imgpath+ "rumi-15.png"
   				}],
   				dialoglabels:[{
   					dialogimagelabelclass: "dialoglabel",
   					dilaogimagelabeldata: data.string.p2_s4
   				}]
   			}]
   	},{
   			imgcontainerdiv: "container2",
   			front:[{
   				imagelabels:[{
   					imagelabelclass: "container_label",
   					imagelabeldata: data.string.p2_s4
   				}]
   			}],
   			back:[{
   				imagestoshow:[{
   					imgclass: "image",
   					imgsrc: imgpath+ "prem-15.png"
   				}],
   				dialoglabels:[{
   					dialogimagelabelclass: "dialoglabel",
   					dilaogimagelabeldata: data.string.p1_s12
   				}]
   			}]
   	},{
   			imgcontainerdiv: "container3",
   			front:[{
   				imagelabels:[{
   					imagelabelclass: "container_label",
   					imagelabeldata: data.string.p2_s5
   				}]
   			}],
   			back:[{
   				imagestoshow:[{
   					imgclass: "image",
   					imgsrc: imgpath+ "sagar-06.png"
   				}],
   				dialoglabels:[{
   					dialogimagelabelclass: "dialoglabel",
   					dilaogimagelabeldata: data.string.p1_s13
   				}]
   			}]
   	},{
   			imgcontainerdiv: "container4",
   			front:[{
   				imagelabels:[{
   					imagelabelclass: "container_label",
   					imagelabeldata: data.string.p2_s6
   				}]
   			}],
   			back:[{
   				imagestoshow:[{
   					imgclass: "image mirror_image",
   					imgsrc: imgpath+ "rumi-15.png"
   				}],
   				dialoglabels:[{
   					dialogimagelabelclass: "dialoglabel",
   					dilaogimagelabeldata: data.string.p1_s14
   				}]
   			}]
   	},{
   		imgcontainerdiv: "container_instructions",
   		front:[{
 			imagelabels:[{
 				imagelabelclass: "dialoglabel_instruction2",
 				imagelabeldata: data.string.p2_s7
 			}]
   		}]
   	}]
   },{
   	//page 2
   	contentblockadditionalclass: "dottedbackground",
   	contentnocenteradjust: true,
   	uppertextblock: [{
   		textclass: "maintitle2",
   		textdata: data.string.p2_s2,
   		datahighlightflag: true,
   		datahighlightcustomclass: "underline_highlight"
   	}],
   	flipperimageblock: [{
   			imgcontainerdiv: "container1",
   			front:[{
   				imagelabels:[{
   					imagelabelclass: "container_label",
   					imagelabeldata: data.string.p2_s3
   				}]
   			}],
   			back:[{
   				imagestoshow:[{
   					imgclass: "image mirror_image",
   					imgsrc: imgpath+ "Niti-03.png"
   				}],
   				dialoglabels:[{
   					dialogimagelabelclass: "dialoglabel",
   					dilaogimagelabeldata: data.string.p2_s9
   				}]
   			}]
   	},{
   			imgcontainerdiv: "container2",
   			front:[{
   				imagelabels:[{
   					imagelabelclass: "container_label",
   					imagelabeldata: data.string.p2_s4
   				}]
   			}],
   			back:[{
   				imagestoshow:[{
   					imgclass: "image",
   					imgsrc: imgpath+ "prem-15.png"
   				}],
   				dialoglabels:[{
   					dialogimagelabelclass: "dialoglabel",
   					dilaogimagelabeldata: data.string.p2_s10
   				}]
   			}]
   	},{
   			imgcontainerdiv: "container3",
   			front:[{
   				imagelabels:[{
   					imagelabelclass: "container_label",
   					imagelabeldata: data.string.p2_s5
   				}]
   			}],
   			back:[{
   				imagestoshow:[{
   					imgclass: "image",
   					imgsrc: imgpath+ "sagar-06.png"
   				}],
   				dialoglabels:[{
   					dialogimagelabelclass: "dialoglabel",
   					dilaogimagelabeldata: data.string.p2_s11
   				}]
   			}]
   	},{
   			imgcontainerdiv: "container4",
   			front:[{
   				imagelabels:[{
   					imagelabelclass: "container_label",
   					imagelabeldata: data.string.p2_s6
   				}]
   			}],
   			back:[{
   				imagestoshow:[{
   					imgclass: "image mirror_image",
   					imgsrc: imgpath+ "rumi-15.png"
   				}],
   				dialoglabels:[{
   					dialogimagelabelclass: "dialoglabel",
   					dilaogimagelabeldata: data.string.p2_s12
   				}]
   			}]
   	},{
   		imgcontainerdiv: "container_instructions",
   		front:[{
   			imagestoshow:[{
 				imgclass: "caterpillar",
 				imgsrc: imgpath+ "caterpillar.gif"
 			}],
 			imagelabels:[{
 				imagelabelclass: "dialoglabel_instruction",
 				imagelabeldata: data.string.p2_s8
 			}]
   		}]
   	}],
   	lowertextblockadditionalclass: "moreinfo",
   	lowertextblock:[{
   			textclass: "heading2",
   			datahighlightflag: true,
   			datahighlightcustomclass: "newcusthigh",
   			textdata: data.string.p2_s13
   		},{
   			textclass: "heading2",
   			datahighlightflag: true,
   			datahighlightcustomclass: "newcusthigh2",
   			textdata: data.string.p2_s14
   		},{
   			textclass: "heading2",
   			datahighlightflag: true,
   			datahighlightcustomclass: "newcusthigh2",
   			textdata: data.string.p2_s15
   		},{
   			textclass: "close_button",
   			textdata: "&nbsp;"
   		}
   	],
   	learn_more: true
   }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 2500);

		}
	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var playing_audio = sounds[0];
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);
	$nextBtn.hide(0);
	if (countNext==7) {
		$(".front,.back").css("cursor","auto");
		
	}
	switch(countNext){
		case 0:
		case 1:
		case 2:
            playing_audio.stop();
            playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				$nextBtn.delay(200).show(0);
			});
			break;
		case 3:
			$(".close_button").hide();
			$(".caterpillar,.dialoglabel_instruction").hide().delay(17000).fadeIn(100);
            playing_audio.stop();
            playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				
			});
			var count = 0;
			$nextBtn.hide(0);
			var $learn_more = $(".learn_more");
			$(".container1, .container2, .container3, .container4").click(function(){
				var $this = $(this);
				$this.toggleClass("flipped");
				$this.addClass("yes");
				$(".container_instructions").hide(0);
				if($(".yes").length >= 4){
					setTimeout(function(){
						$(".learn_more").show(0);
					}, 1500);
				}
			});

			$learn_more.click(function(){
				qtips.play();
				qtips.bind('ended', function(){
					$(".close_button").show();
			});
				$(".moreinfo").show(500);
			});

			$(".close_button").click(function(){
				$nextBtn.show(0);
				$(".moreinfo").hide(500);
			});
			break;
		case 4:
            playing_audio.stop();
            playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				$nextBtn.delay(200).show(0);
			});
			break;
		case 5:
            playing_audio.stop();
            playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				$nextBtn.delay(200).show(0);
			});
			break;
	  case 6:
	  playing_audio.stop();
	  playing_audio = sounds[countNext];
		playing_audio.play();
		playing_audio.bind('ended', function(){
			$nextBtn.delay(200).show(0);
		});
		break;
  		case 7:
            playing_audio.stop();
            playing_audio = sounds[countNext];
  			playing_audio.play();
  			playing_audio.bind('ended', function(){
				havaudio.play();
				havaudio.bind('ended', function(){
				  $nextBtn.delay(200).show(0);
				});
  			});
  			break;
  		case 8:
            playing_audio.stop();
            playing_audio = sounds[countNext];
            playing_audio.play();
            var count = 0;
  			$nextBtn.hide(0);
  			var $learn_more = $(".learn_more");
  			$(".container1, .container2, .container3, .container4").click(function(){
  				var $this = $(this);
  				$this.toggleClass("flipped");
  				$this.addClass("yes");
  				$(".container_instructions").hide(0);
  				if($(".yes").length >= 4){
  					setTimeout(function(){
  						$(".learn_more").show(0);
  					}, 1500);
  				}
  			});

  			$learn_more.click(function(){
  				$(".moreinfo").show(500);
  			});

  			$(".close_button").click(function(){
  				ole.footerNotificationHandler.pageEndSetNotification();
  				$(".moreinfo").hide(500);
  			});
  			break;
		default:
			break;
	}
  }
 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call the template
			generaltemplate();

			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
			playing_audio.stop();

		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

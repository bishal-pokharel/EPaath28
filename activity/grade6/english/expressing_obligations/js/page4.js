var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_0 = new buzz.sound((soundAsset + "p4_s0.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p4_s1_0.ogg"));
var sound_1_1 = new buzz.sound((soundAsset + "p4_s1_1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p4_s2_0.ogg"));
var sound_2_1 = new buzz.sound((soundAsset + "p4_s2_1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p4_s5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p4_s6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p4_s7.ogg"));
var sound_7_1 = new buzz.sound((soundAsset + "p4_s7_1.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p4_s8.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p4_s9.ogg"));
var sound_9_1 = new buzz.sound((soundAsset + "p4_s9_1.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p4_s10.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p4_s11.ogg"));

var sounds=[ sound_0, sound_1, sound_2, sound_3, sound_4,sound_5,sound_6, sound_7, sound_8,sound_9,sound_10,sound_11];


var content=[
  {
   	//page 0
   	contentnocenteradjust: true,
   	contentblockadditionalclass: "orange_bg",
    uppertextblock: [{
      textclass: "uptext",
  		textdata: data.string.p4_s20
  	}],
   	imageblock:[{
 		imagestoshow:[{
   			imgclass: "sagar1 mirror_rotate",
   			imgsrc : imgpath+"sagar-03-bag.png"
   		},{
 			imgclass: "copy_table_1",
 			imgsrc : imgpath+"notebook-06.png"
   		},{
 			imgclass: "one_girl_table",
 			imgsrc : imgpath+"Niti-15.png"
    },{
        imgclass: "table",
        imgsrc : imgpath+"table-05.png"
      }]
   	}]
   },{
    	//page 1
    	contentnocenteradjust: true,
    	contentblockadditionalclass: "orange_bg",
      lowertextblock: [{
        textclass: "fingertext",
    		textdata: data.string.p4_s21
    	}],
    	imageblock:[{
  		imagestoshow:[{
    			imgclass: "sagar1 mirror_rotate",
    			imgsrc : imgpath+"sagar-03-bag.png"
    		},{
  			imgclass: "copy_table_1",
  			imgsrc : imgpath+"notebook-06.png"
    		},{
  			imgclass: "one_girl_table",
  			imgsrc : imgpath+"Niti-15.png"
    		},{
      			imgclass: "handimg",
      			imgsrc : imgpath+"handpointing01.png"
      		},{
        			imgclass: "table",
        			imgsrc : imgpath+"table-05.png"
        		}]
    	},{
    		imgcontainerdiv: "dialogcontainer1",
    		imagelabels: [{
    			imagelabelclass: "dialogcontent",
    			imagelabeldata: data.string.p4_s1
    		}]
    	}]
    },{
  	//page 2
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
    lowertextblock: [{
      textclass: "fingertext",
      textdata: data.string.p4_s22
    }],
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "sagar1 mirror_rotate",
  			imgsrc : imgpath+"sagar-16-bag.png"
  		},{
			imgclass: "copy_table_1",
			imgsrc : imgpath+"notebook-06.png"
  		},{
			imgclass: "one_girl_table",
			imgsrc : imgpath+"Niti-03.png"
    },{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
          imgclass: "handimg",
          imgsrc : imgpath+"handpointing01.png"
        }]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s2
  		}]
  	}]
  },{
  	//page 3
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "sagar1 mirror_rotate",
  			imgsrc : imgpath+"sagar-03-bag.png"
  		},{
			imgclass: "copy_table_1",
			imgsrc : imgpath+"notebook-06.png"
  		},{
			imgclass: "one_girl_table",
			imgsrc : imgpath+"Niti-15.png"
    },{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s3
  		}]
  	}]
  },{
  	//page 4
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "fourkids_table_1",
  			imgsrc : imgpath+"sagar-14.png"
  		},{
  			imgclass: "fourkids_table_2",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		},{
  			imgclass: "bag",
  			imgsrc : imgpath+"bag.png"
  		},{
    			imgclass: "table",
    			imgsrc : imgpath+"table-05.png"
    		}]
  	},{
  		imgcontainerdiv: "dialogcontainer3_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s4
  		}]
  	}]
  },{
  	//page 5
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "fourkids_table_1 mirror_rotate",
  			imgsrc : imgpath+"sagar-03.png"
  		},{
  			imgclass: "fourkids_table_2",
  			imgsrc : imgpath+"rumi-04.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		},{
  			imgclass: "bag",
  			imgsrc : imgpath+"bag.png"
  		},{
    			imgclass: "table",
    			imgsrc : imgpath+"table-05.png"
    		}]
  	},{
  		imgcontainerdiv: "dialogcontainer3",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent3",
  			imagelabeldata: data.string.p4_s5
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer3_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s4
  		}]
  	}],
    boardadditionalclass: "boardanim",
    boardblock:[{
      uptextdata: data.string.p4_s23,
      downtextdata: data.string.p4_s24,
      leftaddclass: "correct",
      leftbtn: "Sagar",
      rightbtn: "Sheela"
    }]
  },{
  	//page 6
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "purple_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "fourkids_table_1",
  			imgsrc : imgpath+"sagar-14.png"
  		},{
  			imgclass: "fourkids_table_2",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		},{
  			imgclass: "bag",
  			imgsrc : imgpath+"bag.png"
  		},{
    			imgclass: "table",
    			imgsrc : imgpath+"table-05.png"
    		}]
  	},{
  		imgcontainerdiv: "dialogcontainer3_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s6
  		}]
  	}]
  },{
  	//page 7
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "teacher_boy_girl_2 ",
			imgsrc : imgpath+"sagar-14-bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s9
  		}]
  	}],
    boardadditionalclass: "boardanim",
    boardblock:[{
      uptextdata: data.string.p4_s25,
      downtextdata: data.string.p4_s26,
      leftaddclass: "correct",
      leftbtn: "Yes",
      rightbtn: "No"
    }]
  },{
  	//page 8
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "teacher_boy_girl_2 ",
			imgsrc : imgpath+"sagar-14-bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s9
  		}]
  	}],
    boardadditionalclass: "boardanim",
    boardblock:[{
      uptextdata: data.string.p4_s27,
      downtextdata: data.string.p4_s28,
      rightaddclass: "correct",
      leftbtn: "To drink water",
      rightbtn: "To learn about Asia"
    }]
  },{
  	//page 9
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "teacher_boy_girl_2 ",
			imgsrc : imgpath+"sagar-14-bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2_bb mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent22",
  			imagelabeldata: data.string.p4_s10
  		}]
  	}],
    boardadditionalclass: "boardanim",
    boardblock:[{
      uptextdata: data.string.p4_s29,
      downtextdata: data.string.p4_s30,
      rightaddclass: "correct",
      leftbtn: "Give permission",
      rightbtn: "Deny permission"
    }]
  },{
  	//page 10
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "teacher_boy_girl_2 ",
			imgsrc : imgpath+"sagar-14-bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2_bb mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent22",
  			imagelabeldata: data.string.p4_s10
  		}]
  	}],
    boardadditionalclass: "boardanim",
    boardblock:[{
      uptextdata: data.string.p4_s31,
      downtextdata: data.string.p4_s32,
      leftaddclass: "correct",
      leftbtn: "To research Africa",
      rightbtn: "To go home"
    }]
  },{
  	//page 10
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "teacher_boy_girl_2 ",
			imgsrc : imgpath+"sagar-14-bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s11
  		}]
  	}]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("boardcontent", $("#boardcontent-partial").html());
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.pageEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
		}
	}

    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }

  var playing_audio=sounds[0];
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    $(".forhover").click(function(){
      var btn = $(this);
      if(btn.hasClass("forhover")){
        var posIt = btn.position();
        if(btn.hasClass("correct")){
          $(".corrbtn").show(0).css("left",posIt.left + (btn.width()/2));
          btn.addClass("corclass");
          $(".blckdown").show(0);
          play_correct_incorrect_sound(true);
          $(".forhover").removeClass("forhover");
            $nextBtn.delay(200).show(0);
        }
        else{
          btn.removeClass("forhover");
          $(".incobtn").show(0).css("left",posIt.left + (btn.width()/2));
          btn.addClass("incclass");
          play_correct_incorrect_sound(false);
        }
      }
    });

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);
	$nextBtn.hide(0);
	switch(countNext){
		case 0:
		case 3:
		case 4:
        case 6:
        case 11:
            playing_audio.stop();
			playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
                countNext==11?ole.footerNotificationHandler.pageEndSetNotification():$nextBtn.delay(200).show();
			});
			break;
		case 1:
			playaudioseq([sound_1,sound_1_1]);
			break;
        case 2:
            playaudioseq([sound_2,sound_2_1]);
            break;
		case 5:
		case 7:
		case 8:
		case 9:
		case 10:
            playing_audio.stop();
            $(".boardblock").hide().css({"height":"64%","top":"35%"});
			$(".blckdown").css({"top":"70%"});
			if(countNext == 8 || countNext==10){
                $(".boardblock").show();
                setTimeout(function () {
                    playing_audio.stop();
                    playing_audio = sounds[countNext];
                    playing_audio.play();
                    playing_audio.bind('ended', function () {
                        $(".boardblock").show();
                    });
                },2000)
            }
            else {
                playing_audio.stop();
                playing_audio = sounds[countNext];
                playing_audio.play();
                playing_audio.bind('ended', function () {
                    $(".boardblock").show();
                    if( countNext ==9){
                        setTimeout(function () {
                            playing_audio.stop();
                            playing_audio = eval("sound_"+countNext+"_1");
                            playing_audio.play();
                        },2000)
					}
                });
            }
            break;
            // playing_audio.stop();
            // playing_audio = sounds[countNext];
            // playing_audio.play();
            // playing_audio.bind('ended', function(){
            //     playing_audio = sound_1_1;
            //     playing_audio.play();
            //     playing_audio.bind('ended', function(){
            //         $nextBtn.delay(200).show(0);
            //     });
            // });
		default:
			break;
	}
  }
	function playaudioseq(sound1){
        playing_audio.stop();
        playing_audio = sound1[0];
        sound1.splice(0,1);
        playing_audio.play();
        playing_audio.bind('ended', function() {
        	if(sound1.length>0)
        	   playaudioseq(sound1);
        	else
                $nextBtn.delay(200).show(0);

        });
	}
 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generaltemplate();

			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		playing_audio.stop();
		countNext--;
		templateCaller();
		sound_l_1.stop();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
// slide1
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "middletext fntbg",
		textdata: data.string.p1text15
	}
]
},
// slide2
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "redbox",
		textdata: data.string.p1text18
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "downman",
			imgid : 'sittingman',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-1',
	textclass: "answer",
	textdata: data.string.p1text19,
	imgclass: '',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide3
{
	contentblockadditionalclass: "thebg1",
	singletext:[
		{
			textclass: "redbox",
			textdata: data.string.p1text20
		},
		{
			textclass: "yellowbox",
			textdata: data.string.p1text21
		},
		{
			textclass: "bluebox",
			textdata: data.string.p1text22
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "micon-one",
			imgid : 'list',
			imgsrc: ""
		},
		{
			imgclass: "micon-two",
			imgid : 'detective',
			imgsrc: ""
		},
		{
			imgclass: "micon-three",
			imgid : 'clock',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "redbox",
		textdata: data.string.p1text23
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "downman",
			imgid : 'sittingman',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-1',
	textclass: "answer",
	textdata: data.string.p1text24,
	imgclass: '',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide5
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "middletext",
		textdata: data.string.p1text25
	}
]
},
// slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "formovetext alreadyup",
		textdata: data.string.p1text26
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		datahighlightcustomclass2: "hightext2",
		textclass: "mi-right-text",
		textdata: data.string.p1text27
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "formovegirl alreadyup",
			imgid : 'smallgirl',
			imgsrc: ""
		},
		{
			imgclass: "bedroom1",
			imgid : 'gopal',
			imgsrc: ""
		}
	]
}]
},
// slide7
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "formovetext alreadyup",
		textdata: ""
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		datahighlightcustomclass2: "hightext2",
		textclass: "mi-right-text",
		textdata: data.string.p1text28
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "formovegirl alreadyup",
			imgid : 'smallgirl',
			imgsrc: ""
		},
		{
			imgclass: "bedroom1",
			imgid : 'gopal_01',
			imgsrc: ""
		}
	]
}]
},
// slide8
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "formovetext alreadyup",
		textdata: data.string.p1text29
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "no-text",
		textclass: "mi-right-text",
		textdata: data.string.p1text30
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "formovegirl alreadyup",
			imgid : 'smallgirl',
			imgsrc: ""
		},
		{
			imgclass: "bedroom1",
			imgid : 'raining',
			imgsrc: ""
		}
	]
}]
},
// slide9
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	singletext:[
	{
		textclass: "formovetext alreadyup textcolchng",
		textdata: data.string.p1text31
	},
	{
		textclass: "buttonsel2 forhover2 correct newdiybtn-1",
		textdata: data.string.p1text32
	},
	{
		textclass: "buttonsel2 forhover2 newdiybtn-2",
		textdata: data.string.p1text33
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "formovegirl alreadyup girlcolchng",
			imgid : 'smallgirl',
			imgsrc: ""
		}
	]
}]
},
// slide10
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	singletext:[
	{
		textclass: "fakebtn-1 corr-fake",
		textdata: data.string.p1text32
	},
	{
		textclass: "fakebtn-2",
		textdata: data.string.p1text33
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "girlspcl",
			imgid : 'girlspcl',
			imgsrc: ""
		},
		{
			imgclass: "boyspcl",
			imgid : 'boyspcl',
			imgsrc: ""
		}
	]
}],
uppertextblock:[
	{
		textclass: "brownbg",
	}
],
speechbox:[
	{
		speechbox: 'sp-2',
		textclass: "answer",
		textdata: data.string.p1text34,
		imgclass: 'flipped',
		imgid : 'tb-2',
		imgsrc: '',
		// audioicon: true,
	},
	{
		speechbox: 'sp-3',
		textclass: "answer",
		textdata: data.string.p1text35,
		imgclass: '',
		imgid : 'tb-2',
		imgsrc: '',
		// audioicon: true,
	}
]
},
// slide11
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	singletext:[
	{
		textclass: "formovetext alreadyup textcolchng",
		textdata: data.string.p1text36
	},
	{
		textclass: "buttonsel2 forhover2 correct newdiybtn-1",
		textdata: data.string.p1text37
	},
	{
		textclass: "buttonsel2 forhover2 newdiybtn-2",
		textdata: data.string.p1text38
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "formovegirl alreadyup girlcolchng",
			imgid : 'smallgirl',
			imgsrc: ""
		}
	]
}]
},
// slide12
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	singletext:[
	{
		textclass: "fakebtn-1",
		textdata: data.string.p1text37
	},
	{
		textclass: "fakebtn-2 corr-fake",
		textdata: data.string.p1text38
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "girlspcl",
			imgid : 'girlspcl',
			imgsrc: ""
		},
		{
			imgclass: "boyspcl",
			imgid : 'boyspcl',
			imgsrc: ""
		}
	]
}],
uppertextblock:[
	{
		textclass: "brownbg",
	}
],
speechbox:[
	{
		speechbox: 'sp-2',
		textclass: "answer",
		textdata: data.string.p1text39,
		imgclass: 'flipped',
		imgid : 'tb-2',
		imgsrc: '',
		// audioicon: true,
	},
	{
		speechbox: 'sp-3',
		textclass: "answer",
		textdata: data.string.p1text40,
		imgclass: '',
		imgid : 'tb-2',
		imgsrc: '',
		// audioicon: true,
	}
]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "seema", src: imgpath+"girl01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sittingman", src: imgpath+"man.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud1", src: imgpath+"magnify.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud2", src: imgpath+"note_book.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smallgirl", src: imgpath+"girl03a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bedroom", src: imgpath+"bed_room.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bedroom01", src: imgpath+"bed_room01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clock", src: imgpath+"clock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "detective", src: imgpath+"detective.png", type: createjs.AbstractLoader.IMAGE},
			{id: "list", src: imgpath+"list.png", type: createjs.AbstractLoader.IMAGE},
			{id: "raining", src: imgpath+"raining.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlspcl", src: imgpath+"girl03b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boyspcl", src: imgpath+"boyl01a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shoeplayer", src: imgpath+"shoe-laces-untied.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cryshoeplayer", src: imgpath+"shoe-laces-untied_boy_crying.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gopal", src: imgpath+"gopal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gopal_01", src: imgpath+"gopal_01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1_0", src: soundAsset+"s3_p2_1.ogg"},
			{id: "sound_1_1", src: soundAsset+"s3_p2_2.ogg"},
			{id: "sound_3_0", src: soundAsset+"s3_p3_1.ogg"},
			{id: "sound_3_1", src: soundAsset+"s3_p3_2.ogg"},
			{id: "sound_3_2", src: soundAsset+"s3_p3_3.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4_1.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p4_2.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p6_1.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p6_2.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_11", src: soundAsset+"s3_p8_1.ogg"},
			{id: "sound_12", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_13", src: soundAsset+"s3_p10_1.ogg"},
			{id: "sound_14", src: soundAsset+"s3_p10_2.ogg"},
			{id: "sound_15", src: soundAsset+"s3_p11.ogg"},
			{id: "sound_16", src: soundAsset+"s3_p12_1.ogg"},
			{id: "sound_17", src: soundAsset+"s3_p12_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("sound_1");
			break;
			case 1:
				chain_fade("sound_1_0", 1);
			break;
			case 2:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3_0");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3_1");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3_2");
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
			});
		});
	});
			break;
			case 3:
				sound_player1("sound_4");
				setTimeout(function(){
				sound_player("sound_5");
			},16500);
			break;
			case 4:
			sound_player("sound_6");
			break;
			case 5:
			sound_player1("sound_7");
			setTimeout(function(){
					sound_player("sound_8");
			},5000);
			break;
			case 6:
			sound_player("sound_9");
			break;
			case 7:
			sound_player1("sound_11");
			setTimeout(function(){
					sound_player("sound_10");
			},6000);
			break;
			case 8:
			sound_player1("sound_12");
			break;
			case 9:
			sound_player1("sound_13");
			setTimeout(function(){
					sound_player("sound_14");
			},6000);
			break;
			case 10:
			sound_player1("sound_15");
			break;
			case 11:
			sound_player1("sound_16");
			setTimeout(function(){
					sound_player("sound_17");
			},12000);
			break;
			default:
			nav_button_controls(1000);
			break;
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("forhover")){
				$(this).removeClass('forhover');
					if($(this).hasClass("correct")){
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
						$(this).css("color","white");
						// $(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						appender($(this),'corrimg');
						$('.buttonsel').removeClass('forhover forhoverimg');
						nav_button_controls(200);
					}
					else{
						play_correct_incorrect_sound(0);
						appender($(this),'incorrimg');
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						// $(this).siblings(".wrngopt").show(0);
					}
			}

			function appender($this, icon){
				if($this.hasClass("diybutton-1"))
					$(".coverboardfull").append("<img class='icon-one' src= '"+ preload.getResult(icon).src +"'>");
				else
					$(".coverboardfull").append("<img class='icon-two' src= '"+ preload.getResult(icon).src +"'>");
				}
			});

			$(".buttonsel2").click(function(){
				console.log("lksjfl");
				if($(this).hasClass("forhover2")){
					$(this).removeClass('forhover2');
						if($(this).hasClass("correct")){
							play_correct_incorrect_sound(1);
							$(this).css("background","#bed62f");
							$(this).css("border","5px solid #deef3c");
							$(this).css("color","white");
							// $(this).siblings(".corctopt").show(0);
							//$('.hint_image').show(0);
							appender($(this),'corrimg');
							$('.buttonsel2').removeClass('forhover2 forhoverimg');
							nav_button_controls(200);
						}
						else{
							play_correct_incorrect_sound(0);
							appender($(this),'incorrimg');
							$(this).css("background","#FF0000");
							$(this).css("border","5px solid #980000");
							$(this).css("color","white");
							// $(this).siblings(".wrngopt").show(0);
						}
				}

				function appender($this, icon){
					if($this.hasClass("newdiybtn-1"))
						$(".coverboardfull").append("<img class='icon2-one' src= '"+ preload.getResult(icon).src +"'>");
					else
						$(".coverboardfull").append("<img class='icon2-two' src= '"+ preload.getResult(icon).src +"'>");
					}
				});
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0)
		});
	}
	function sound_player1(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	var Scount = 0;
	function chain_fade(sound_id, lastcount){
		console.log(Scount);
		$(".fademe"+Scount).fadeIn();
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			// countNums(lastcount);
			if(Scount < lastcount){
				Scount++;
				chain_fade("sound_1_"+Scount, lastcount);
			}
			else{
			nav_button_controls(0);
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templatecaller();
	});
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

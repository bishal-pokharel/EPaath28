var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		singletext:[
		{
			textclass: "middletext diyTxt",
			textdata: data.string.diy
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'bg_diy',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text2
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'fronthouse',
				imgsrc: ""
			}
		]
	}]
},
// slide2
{
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "skybg",
				imgid : 'skybg1',
				imgsrc: ""
			},
			{
				imgclass: "skybg2",
				imgid : 'skybg2',
				imgsrc: ""
			},
			{
				imgclass: "fullbg zoomforanim",
				imgid : 'anim-bg',
				imgsrc: ""
			},
			{
				imgclass: "swi-hour",
				imgid : 'hour',
				imgsrc: ""
			},
			{
				imgclass: "swi-min",
				imgid : 'min',
				imgsrc: ""
			}
		]
	}]
},
// slide3
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text3
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg zoomout",
				imgid : 'fronthouse2',
				imgsrc: ""
			}
		]
	}]
},
// slide4
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text4
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg zoomin",
				imgid : 'fronthouse2',
				imgsrc: ""
			}
		]
	}],
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p2text5,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
		// audioicon: true,
	}]
},
// slide5
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text6
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg zoomin2",
				imgid : 'fronthouse2',
				imgsrc: ""
			}
		]
	}]
},
// slide6
{
	contentblockadditionalclass: "thebg2",
	singletext:[
	{
		textclass: "middletext6 whitetext zoomInEntry",
		textdata: data.string.p2text7
	}
	]
},
// slide7
{
	singletext:[
	{
		textclass: "formovetext",
		textdata: data.string.p2text8
	},
	{
		textclass: "buttonsel forhover correct diybutton-1",
		textdata: data.string.p2text9
	},
	{
		textclass: "buttonsel forhover diybutton-2",
		textdata: data.string.p2text10
	},
	{
		textclass: "buttonsel forhover diybutton-3",
		textdata: data.string.p2text11
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "formovegirl",
				imgid : 'smallgirl',
				imgsrc: ""
			},
			{
				imgclass: "bedroom1",
				imgid : 'ghantaghars',
				imgsrc: ""
			}
		]
	}]
},
// slide8
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text12
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'footprint',
				imgsrc: ""
			}
		]
	}]
},
// slide9
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text13
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'footprint2',
				imgsrc: ""
			}
		]
	}]
},
// slide10
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text14
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'traveller',
				imgsrc: ""
			},
			{
				imgclass: "boyslide",
				imgid : 'thinkboy',
				imgsrc: ""
			}
		]
	}]
},
// slide11
{
	singletext:[
	{
		textclass: "formovetext",
		textdata: data.string.p2text15
	},
	{
		textclass: "buttonsel forhover correct diybutton-1",
		textdata: data.string.p2text16
	},
	{
		textclass: "buttonsel forhover diybutton-2",
		textdata: data.string.p2text17
	},
	{
		textclass: "buttonsel forhover diybutton-3",
		textdata: data.string.p2text18
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "formovegirl",
				imgid : 'smallgirl',
				imgsrc: ""
			},
			{
				imgclass: "bedroom1",
				imgid : 'floposid',
				imgsrc: ""
			}
		]
	}]
},
// slide12
{
	contentblockadditionalclass: "thebg3",
	speechbox:[{
		speechbox: 'sp-2',
		textclass: "answer",
		textdata: data.string.p2text19,
		imgclass: '',
		imgid : 'tb-2',
		imgsrc: '',
		// audioicon: true,
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "thinkboy",
				imgid : 'thinkboy',
				imgsrc: ""
			}
		]
	}]
},
// slide13
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text20
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'tv',
				imgsrc: ""
			}
		]
	}]
},
// slide14
{
	contentblockadditionalclass: "thebg3",
	speechbox:[{
		speechbox: 'sp-3',
		textclass: "answer",
		textdata: data.string.p2text21,
		imgclass: 'flipped',
		imgid : 'tb-1',
		imgsrc: '',
		// audioicon: true,
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'maa1',
				imgsrc: ""
			}
		]
	}]
},
// slide15
{
	contentblockadditionalclass: "thebg3",
	speechbox:[{
		speechbox: 'sp-4',
		textclass: "answer",
		textdata: data.string.p2text22,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
		// audioicon: true,
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'maa2',
				imgsrc: ""
			}
		]
	}]
},
// slide16
{
	singletext:[
	{
		textclass: "formovetext",
		textdata: data.string.p2text15
	},
	{
		textclass: "buttonsel forhover diybutton-1",
		textdata: data.string.p2text23
	},
	{
		textclass: "buttonsel forhover correct diybutton-2",
		textdata: data.string.p2text24
	},
	{
		textclass: "buttonsel forhover diybutton-3",
		textdata: data.string.p2text25
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "formovegirl",
				imgid : 'smallgirl',
				imgsrc: ""
			},
			{
				imgclass: "bedroom1",
				imgid : 'rahul01',
				imgsrc: ""
			}
		]
	}]
},
// slide17
{
	contentblockadditionalclass: "thebg3",
	speechbox:[{
		speechbox: 'sp-5',
		textclass: "answer",
		textdata: data.string.p2text26,
		imgclass: 'flip-all',
		imgid : 'tb-2',
		imgsrc: '',
		// audioicon: true,
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "thinkboy",
				imgid : 'thinkboy',
				imgsrc: ""
			},
			{
				imgclass: "tileimg-11",
				imgid : 'tileimg1',
				imgsrc: ""
			},
			{
				imgclass: "tileimg-12",
				imgid : 'tileimg2',
				imgsrc: ""
			},
			{
				imgclass: "tileimg-13",
				imgid : 'tileimg3',
				imgsrc: ""
			}
		]
	}]
},
// slide18
{
	singletext:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "undertxt",
		textclass: "formovetext twolines",
		textdata: data.string.p2text27
	},
	{
		textclass: "mi_ques",
		textdata: data.string.p2text28
	},
	{
		textclass: "buttonsel forhover correct diybutton-21",
		textdata: data.string.p2text29
	},
	{
		textclass: "buttonsel forhover diybutton-22",
		textdata: data.string.p2text30

	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "formovegirl",
				imgid : 'smallgirl',
				imgsrc: ""
			},
			{
				imgclass: "bedroom1",
				imgid : 'bg615',
				imgsrc: ""
			}
		]
	}]
},
// slide19
{
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "fullbg",
			imgid : 'talking_with_girl',
			imgsrc: ""
		}
	]
	}],
	speechbox:[{
		speechbox: 'sp-6',
		textclass: "answer",
		textdata: data.string.p2text31,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
		// audioicon: true,
	}]
},
// slide20
{
	singletext:[
	{
		textclass: "formovetext",
		textdata: data.string.p2text32
	},
	{
		textclass: "mi_ques",
		textdata: data.string.p2text33
	},
	{
		textclass: "buttonsel forhover correct diybutton-21",
		textdata: data.string.p2text34
	},
	{
		textclass: "buttonsel forhover diybutton-22",
		textdata: data.string.p2text35

	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "formovegirl",
				imgid : 'smallgirl',
				imgsrc: ""
			},
			{
				imgclass: "bedroom1",
				imgid : 'slide72a',
				imgsrc: ""
			}
		]
	}]
},
// slide21
{
	singletext:[
	{
		textclass: "formovetext",
		textdata: data.string.p2text36
	},
	{
		textclass: "mi_ques",
		textdata: data.string.p2text37
	},
	{
		textclass: "buttonsel forhover correct diybutton-21",
		textdata: data.string.p2text38
	},
	{
		textclass: "buttonsel forhover diybutton-22",
		textdata: data.string.p2text39

	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "formovegirl",
				imgid : 'smallgirl',
				imgsrc: ""
			},
			{
				imgclass: "bedroom1",
				imgid : 'playing_ball',
				imgsrc: ""
			}
		]
	}]
},
// slide22
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text40
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'flogirl',
				imgsrc: ""
			}
		]
	}]
},
// slide23
{
		singletext:[
		{
			textclass: "whiteleft",
			textdata: data.string.p2text41
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg",
				imgid : 'flogirl2',
				imgsrc: ""
			}
		]
	}]
},
// slide24
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text42
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg zoomin3",
				imgid : 'flogirl3',
				imgsrc: ""
			}
		]
	}],
	speechbox:[{
		speechbox: 'sp-7',
		textclass: "answer",
		textdata: data.string.p2text43,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
		// audioicon: true,
	}]
},
// slide25
{
	singletext:[
	{
		textclass: "formovetext",
		textdata: data.string.p2text44
	},
	{
		textclass: "mi_ques",
		textdata: data.string.p2text45
	},
	{
		textclass: "buttonsel forhover diybutton-21",
		textdata: data.string.p2text46
	},
	{
		textclass: "buttonsel forhover correct diybutton-22",
		textdata: data.string.p2text47

	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "formovegirl",
				imgid : 'smallgirl',
				imgsrc: ""
			},
			{
				imgclass: "bedroom1",
				imgid : 'mum',
				imgsrc: ""
			}
		]
	}]
},
// slide26
{
	singletext:[
	{
		textclass: "whiteleft",
		textdata: data.string.p2text48
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "fullbg zoomin3",
				imgid : 'ravipot',
				imgsrc: ""
			}
		]
	}]
},
// slide27
{
	contentblockadditionalclass: "thebg2",
	singletext:[
	{
		textclass: "middletext whitetext",
		textdata: data.string.p2text49
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "solved",
				imgid : 'solved',
				imgsrc: ""
			}
		]
	}]
},
// slide28
{
	contentblockadditionalclass: "thebg4",
	singletext:[
	{
		textclass: "lastmove",
		textdata: data.string.p2text50
	},
	{
		textclass: "mi_ques2",
		textdata: data.string.p2text51
	},
	{
		textclass: "textfade1",
		textdata: data.string.p2text52
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "thinkgirl",
				imgid : 'thinkgirl',
				imgsrc: ""
			}
		]
	}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fronthouse", src: imgpath+"planting.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fronthouse2", src: imgpath+"front_of_house.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smallgirl", src: imgpath+"girl03a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ghantaghars", src: imgpath+"ghanta_ghar02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim-bg", src: imgpath+"animation/ghanta_ghar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "skybg2", src: imgpath+"animation/bg_pink.png", type: createjs.AbstractLoader.IMAGE},
			{id: "skybg1", src: imgpath+"animation/bg_blue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hour", src: imgpath+"animation/needle02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "min", src: imgpath+"animation/needle01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "footprint", src: imgpath+"flowerpot02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "footprint2", src: imgpath+"footprint.png", type: createjs.AbstractLoader.IMAGE},
			{id: "traveller", src: imgpath+"traveller.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thinkboy", src: imgpath+"boy_thinking01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thinkgirl", src: imgpath+"thinking_nit01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "floposid", src: imgpath+"flowerpot01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tv", src: imgpath+"tv_news.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "maa1", src: imgpath+"ravi_and_mum01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "maa2", src: imgpath+"ravi_and_mum02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rahul01", src: imgpath+"rahul01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tileimg1", src: imgpath+"bg4oclock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tileimg2", src: imgpath+"bg6oclock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tileimg3", src: imgpath+"bg3oclock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "talking_with_girl", src: imgpath+"talking_with_girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flogirl", src: imgpath+"girl_with_flower01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flogirl2", src: imgpath+"girl_with_flower02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flogirl3", src: imgpath+"girl_with_flower03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mum", src: imgpath+"mum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ravipot", src: imgpath+"ravil_with_flowerpot01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "solved", src: imgpath+"solved.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg615", src: imgpath+"bg615oclock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "slide72a", src: imgpath+"slide72a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "playing_ball", src: imgpath+"playing_ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_diy", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},

			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: imgpath+'cloud02.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "stamp", src: soundAsset+"stamp.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s4_p5.ogg"},
			{id: "sound_5_1", src: soundAsset+"s4_p5_1.ogg"},
			{id: "sound_6", src: soundAsset+"s4_p6.ogg"},
			{id: "sound_8", src: soundAsset+"s4_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s4_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s4_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s4_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s4_p12.ogg"},
			{id: "sound_13", src: soundAsset+"s4_p13.ogg"},
			{id: "sound_14", src: soundAsset+"s4_p14.ogg"},
			{id: "sound_15", src: soundAsset+"s4_p15.ogg"},
			{id: "sound_16", src: soundAsset+"s4_p16.ogg"},
			{id: "sound_17", src: soundAsset+"s4_p17.ogg"},
			{id: "sound_18", src: soundAsset+"s4_p18.ogg"},
			{id: "sound_19", src: soundAsset+"s4_p19.ogg"},
			{id: "sound_19_1", src: soundAsset+"s4_p19_1.ogg"},
			{id: "sound_19_2", src: soundAsset+"s4_p19_2.ogg"},
			{id: "sound_20", src: soundAsset+"s4_p20.ogg"},
			{id: "sound_21_1", src: soundAsset+"s4_p21_1.ogg"},
			{id: "sound_21_2", src: soundAsset+"s4_p21_2.ogg"},
			{id: "sound_22", src: soundAsset+"s4_p22.ogg"},
			{id: "sound_22_1", src: soundAsset+"s4_p22_1.ogg"},
			{id: "sound_23", src: soundAsset+"s4_p23.ogg"},
			{id: "sound_24", src: soundAsset+"s4_p24.ogg"},
			{id: "sound_24_1", src: soundAsset+"s4_p24_1.ogg"},
			{id: "sound_25", src: soundAsset+"s4_p25.ogg"},
			{id: "sound_25_1", src: soundAsset+"s4_p25_1.ogg"},
			{id: "sound_26", src: soundAsset+"s4_p26.ogg"},
			{id: "sound_26_1", src: soundAsset+"s4_p26_1.ogg"},
			{id: "sound_27", src: soundAsset+"s4_p27.ogg"},
			{id: "sound_29", src: soundAsset+"s4_p29_1.ogg"},
			{id: "sound_29_0", src: soundAsset+"s4_p29_2.ogg"},
			{id: "sound_29_1", src: soundAsset+"s4_p29_3.ogg"},
			{id: "sound_29_2", src: soundAsset+"s4_p29_4.ogg"},
			{id: "sound_29_3", src: soundAsset+"s4_p29_5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
				sound_player("sound_2");
				break;
			case 3:
				sound_player("sound_4");
				break;
			case 4:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_5");
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_5_1");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
				break;
				case 5:
					sound_player("sound_6");
					break;
			case 7:
				sound_player1("sound_8");
				break;
			case 8:
				sound_player("sound_9");
				break;
				case 9:
					sound_player("sound_10");
					break;
			case 10:
				sound_player("sound_11");
				break;
				case 11:
					sound_player1("sound_12");
					break;
			case 12:
				sound_player("sound_13");
				break;
			case 13:
				sound_player("sound_14");
				break;
			case 14:
				sound_player("sound_15");
				break;
			case 15:
				sound_player("sound_16");
				break;
			case 16:
				sound_player1("sound_17");
				break;
			case 17:
				sound_player("sound_18");
				break;
			case 18:
				sound_player1("sound_19");
				setTimeout(function(){
					sound_player1("sound_19_1");
					setTimeout(function(){
						sound_player1("sound_19_2");
					},9000);
				},2000);
				break;
			case 19:
				sound_player("sound_20");
				break;
			case 20:
				sound_player1("sound_21_1");
				setTimeout(function(){
					sound_player1("sound_21_2");
				},6000);
				break;
				case 21:
					sound_player1("sound_22");
					setTimeout(function(){
						sound_player1("sound_22_1");
					},6000);
					break;
			case 22:
				sound_player("sound_23");
				break;
			case 23:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_24");
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_24_1");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
				break;
			case 24:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_25_1");
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_25");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
				break;
		case 25:
		createjs.Sound.stop();
		current_sound = createjs.Sound.play("sound_26");
		current_sound.play();
		current_sound.on('complete', function(){
			sound_player1("sound_26_1");
		});
			break;
		case 26:
		sound_player("sound_27");
		break;
		case 27:
		setTimeout(function(){
			sound_player("stamp");
			$(".solved").show(0);
		},1000);

		break;
		case 28:
			sound_player1("sound_29");
			setTimeout(function(){
			chain_fade("sound_29_0", 3);
		},10000);

		break;
			default:
			nav_button_controls(3200);
			break;
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("forhover")){
				$(this).removeClass('forhover');
					if($(this).hasClass("correct")){
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
						$(this).css("color","white");
						// $(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						appender($(this),'corrimg');
						$('.buttonsel').removeClass('forhover forhoverimg');
						nav_button_controls(0);
					}
					else{
						play_correct_incorrect_sound(0);
						appender($(this),'incorrimg');
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						// $(this).siblings(".wrngopt").show(0);
					}
			}

			function appender($this, icon){
				if($this.hasClass("diybutton-1"))
					$(".coverboardfull").append("<img class='icon-one' src= '"+ preload.getResult(icon).src +"'>");
				else if($this.hasClass("diybutton-2"))
					$(".coverboardfull").append("<img class='icon-two' src= '"+ preload.getResult(icon).src +"'>");
				else if($this.hasClass("diybutton-3"))
					$(".coverboardfull").append("<img class='icon-three' src= '"+ preload.getResult(icon).src +"'>");
				else if($this.hasClass("diybutton-21"))
					$(".coverboardfull").append("<img class='icon-four' src= '"+ preload.getResult(icon).src +"'>");
				else
					$(".coverboardfull").append("<img class='icon-five' src= '"+ preload.getResult(icon).src +"'>");
				}
			});
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	var Scount = 0;
	function chain_fade(sound_id, lastcount){
		console.log(Scount);
		$(".fademe"+Scount).fadeIn();
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			// countNums(lastcount);
			if(Scount < lastcount){
				Scount++;
				chain_fade("sound_29_"+Scount, lastcount);
			}
			else{
				nav_button_controls(0);
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templatecaller();
	});
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text4,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class2'

		}],
	},

	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text8,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class2'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class1'

		}],
	},

	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text5,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class2'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class1'

		}],
	},

	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text6,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class2'

		}],
	},

	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text7,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class2'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class1'

		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text9,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class2'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class1'

		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text10,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class2'

		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text11,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class2'

		}],
	},
	//slide 8
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text12,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class2'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class1'

		}],
	},
	//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ex2text13,
				textclass : 'topbox',
			}
		],
		extratextblock:[
			{
				textdata : data.string.ex2text3,
				textclass : 'passage1',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [
		{
			textdata : data.string.ex2text1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.ex2text2,
			optionclass : 'class2'

		}],
	},
];

content.shufflearray();

$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $("#activity-page-next-btn-enabled");
	var $prevBtn  = $("#activity-page-prev-btn-enabled");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

		var preload;
		var timeoutvar = null;
		var current_sound;

		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				//images

				// sounds
				{id: "sound_0", src: soundAsset+"exe_2.ogg"},


			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templatecaller();
		}
		//initialize
		init();

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new EggTemplate();

	rhino.init($total_page);

  //for shuffling two categories of questions
    //     var mainarr=[];
		// var firstqns=[0,1,2,3,4];
		// var secqns=[5,6,7,8,9]
		// var fqn=firstqns.shufflearray();
		// var sqn=secqns.shufflearray();
		// var arr=mainarr.concat(fqn);
		// var arr2=arr.concat(sqn);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$("#box_icon_rhino").hide(0);
				//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		// while (divs.length) {
	  //   	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	  //  }

		if(countNext==0){
			setTimeout(function(){
				sound_player1("sound_0");
			},500);
		}
		$('.topbox').prepend(countNext+1+". ");
		var wrong_clicked = false;
		$(".optioncontainer").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(this).children('.correct-icon').show(0);
				$(".optioncontainer").css('pointer-events', 'none');
				$(this).css({
					'border': '.5vmin solid #C3DF36',
					'background-color': '#92AF3B',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).children('.incorrect-icon').show(0);
				$(this).css({
					'background-color': '#FF0000',
					'border': '.5vmin solid #980000'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

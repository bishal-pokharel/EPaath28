Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";

var content=[

	//ex1
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques1,
        optionsdiv:"optionsdiv",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q1ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q1ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q1ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q1ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "rainy-monsoon-afternoon.jpg",
					}
				]

			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques2,
        optionsdiv:"optionsdiv",

				exeoptions: [
					{
						forshuffle: "class1",
						// optdata: data.string.q2ansa,
					},
					{
						forshuffle: "class2",
						// optdata: data.string.q2ansb,
					},
					{
						forshuffle: "class3",
						// optdata: data.string.q2ansc,
					},
					{
						forshuffle: "class4",
						// optdata: data.string.q2ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image hntImg_1',
						imgsrc: imgpath + "crossing-bridge-covering-nose.png",
					},{
						imgclass:'hint_image hntImg_2',
						imgsrc: imgpath + "child-sick.jpg",
					}
				]

			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques3,
        optionsdiv:"optionsdiv",

				exeoptions: [
					{
						forshuffle: "class1",
						// optdata: data.string.q3ansa,
					},
					{
						forshuffle: "class2",
						// optdata: data.string.q3ansb,
					},
					{
						forshuffle: "class3",
						// optdata: data.string.q3ansc,
					},
					{
						forshuffle: "class4",
						// optdata: data.string.q3ansd,
					}]

			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques4,
        optionsdiv:"optionsdiv",

				exeoptions: [
					{
						forshuffle: "class1",
						// optdata: data.string.q4ansa,
					},
					{
						forshuffle: "class2",
						// optdata: data.string.q4ansb,
					},
					{
						forshuffle: "class3",
						// optdata: data.string.q4ansc,
					},
					{
						forshuffle: "class4",
						// optdata: data.string.q4ansd,
					}]

			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques5,
        optionsdiv:"optionsdiv",
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q5ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q5ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q5ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q5ansd,
					}]

			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques6,
        sentdata: data.string.sentques6,
        optionsdiv:"optionsdiv",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q6ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q6ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q6ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q6ansd,
					}]

			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques7,
        sentdata: data.string.sentques7,
        optionsdiv:"optionsdiv",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q7ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q7ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q7ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q7ansd,
					}]

			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques8,
        sentdata: data.string.sentques8,
        optionsdiv:"optionsdiv",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q8ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q8ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q8ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q8ansd,
					}]

			}
		]
	},
	//ex9
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques9,
        sentdata: data.string.sentques9,
        optionsdiv:"optionsdivSec",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q9ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q9ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q9ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q9ansd,
					}]

			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques10,
        sentdata: data.string.sentques10,
        optionsdiv:"optionsdivSec",

				exeoptions: [
					{
						forshuffle: "fnt4 class1",
						optdata: data.string.q10ansa,
					},
					{
						forshuffle: "fnt4 class2",
						optdata: data.string.q10ansb,
					},
					{
						forshuffle: "fnt4 class3",
						optdata: data.string.q10ansc,
					},
					{
						forshuffle: "fnt4 class4",
						optdata: data.string.q10ansd,
					}]
			}
		]
	},
];

/*remove this for non random questions*/
//content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();

 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
    texthighlight($board);

		$nextBtn.hide();
		$prevBtn.hide();

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
    function randomize(parentclass){
  		var parent = $(parentclass);
  		var divs = parent.children();
      while (divs.length) {
            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
      }
    }

		var ansClicked = false;
		var wrngClicked = false;
    function rand_generator(limit){
      var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
      return randNum;
    }
    function qnGenerator(qnNUm, randNUm){
      $(".question").html((countNext+1)+". "+(eval('data.string.exques'+qnNUm+'_'+randNUm)));
      $(".optionscontainer:eq(0)").children().html(eval('data.string.q'+qnNUm+'ansa_'+randNUm));
      $(".optionscontainer:eq(1)").children().html(eval('data.string.q'+qnNUm+'ansb_'+randNUm));
      $(".optionscontainer:eq(2)").children().html(eval('data.string.q'+qnNUm+'ansc_'+randNUm));
      $(".optionscontainer:eq(3)").children().html(eval('data.string.q'+qnNUm+'ansd_'+randNUm));
    }
    switch (countNext) {
      case 1:
        $(".hint_image").hide(0);
        var randNUm = rand_generator(2);
        $(".hntImg_"+randNUm).show(0);
        var qnNUm = 2;
        qnGenerator(qnNUm, randNUm);
        randomize(".optionsdiv");
      break;
      case 2:
        var randNUm = rand_generator(3);
        var qnNUm = 3;
        qnGenerator(qnNUm, randNUm);
        randomize(".optionsdiv");
      break;
      case 3:
        var randNUm = rand_generator(2);
        var qnNUm = 4;
        qnGenerator(qnNUm, randNUm);
        randomize(".optionsdiv");
      break;
      case 9:
        var randNUm = rand_generator(2);
        var qnNUm = 10;
        qnGenerator(qnNUm, randNUm);
        randomize(".optionsdivSec");
      break;
      case 0:
      case 4:
      case 5:
      case 6:
      case 7:
        randomize(".optionsdiv");
      break;
      case 8:
      break;
      default:

    }

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62fff");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
						$(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show();
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p3text1,
			textclass: "text-1",
		}],

		svgblock: [{
			svgblock: 'svg-1',
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-2',
		uppertextblock:[{
			textdata: data.string.p3text2,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'top-slider slide-in-anim fadeInRight',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'slide-1'
				},
				{
					imgclass : "girl-slider",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "slide-in-anim2 fadeInLeft bottom-slider",
					imgsrc : '',
					imgid : 'slide-2'
				},
			]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p3text3,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-extended',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'room'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "boy-1",
					imgsrc : '',
					imgid : 'boy-1'
				},
				{
					imgclass : "grandma-1",
					imgsrc : '',
					imgid : 'grandma-1'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'fade-in-2 sp-2',
			textdata : data.string.p3text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'margintop-speech',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-extended room-zoom',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'room'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "boy-1",
					imgsrc : '',
					imgid : 'boy-1'
				},
				{
					imgclass : "grandma-1",
					imgsrc : '',
					imgid : 'grandma-1'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		uppertextblockadditionalclass: 'dboxes db-1',
		uppertextblock:[{
			textdata: data.string.p3text6,
			textclass: "",
		}],
		lowertextblockadditionalclass: 'dboxes db-2',
		lowertextblock:[{
			textdata: data.string.p3text7,
			textclass: "",
		},{
			textdata: data.string.p3text8,
			textclass: "margintop-2",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-i1",
					imgsrc : '',
					imgid : 'girl-3'
				},
				{
					imgclass : "grandma-i1",
					imgsrc : '',
					imgid : 'grandma-2'
				}
			]
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		extratextblock:[{
			textdata: '',
			textclass: "bottom-extra-layer",
		}],
		uppertextblockadditionalclass: 'db-3',
		uppertextblock:[{
			textdata: data.string.p3text9,
			textclass: "dbc-1",
		},{
			textdata: data.string.p3text10,
			textclass: "dbc-2",
		}],
		lowertextblockadditionalclass: 'dboxes db-4',
		lowertextblock:[{
			textdata: data.string.p3text11,
			textclass: "",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy-i1",
					imgsrc : '',
					imgid : 'boy-2'
				},
				{
					imgclass : "grandma-i2",
					imgsrc : '',
					imgid : 'grandma-2'
				}
			]
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',

		uppertextblockadditionalclass: 'dboxes db-5',
		uppertextblock:[{
			textdata: data.string.p3text12,
			textclass: "",
		}],
		lowertextblockadditionalclass: 'dboxes db-6',
		lowertextblock:[{
			textdata: data.string.p3text13,
			textclass: "",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-i2",
					imgsrc : '',
					imgid : 'girl-4'
				},
				{
					imgclass : "grandma-i3",
					imgsrc : '',
					imgid : 'grandma-2'
				}
			]
		}],

		svgblock: [{
			svgblock: 'map',
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "frontpage", src: imgpath+"p1/coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "namrada", src: imgpath+"p3/narmada_parikarma.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "slide-1", src: imgpath+"p3/01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "slide-2", src: imgpath+"p3/02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "room", src: imgpath+"p3/room.png", type: createjs.AbstractLoader.IMAGE},
			{id: "map", src: imgpath+"p3/east_west.svg", type: createjs.AbstractLoader.IMAGE},


			{id: "girl-1", src: imgpath+"girl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"girl06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-1", src: imgpath+"boy05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-1", src: imgpath+"grandmatalking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-1-gif", src: imgpath+"grandmatalking.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-3", src: imgpath+"talkgirl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3-gif", src: imgpath+"talkgirl02.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-4", src: imgpath+"girl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-4-gif", src: imgpath+"girl02-gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-5", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-5-gif", src: imgpath+"girl03-gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-2", src: imgpath+"boy01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-3", src: imgpath+"boy04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-2", src: imgpath+"grandmatalking01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-2-gif", src: imgpath+"grandmatalking01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "boytalking01", src: imgpath+"boytalking01.gif", type: createjs.AbstractLoader.IMAGE},


			{id: "textbox", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p4_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s3.ogg"},
			{id: "sound_4a", src: soundAsset+"p4_s4a.ogg"},
			{id: "sound_4b", src: soundAsset+"p4_s4b.ogg"},
			{id: "sound_5a", src: soundAsset+"p4_s5a.ogg"},
			{id: "sound_5b", src: soundAsset+"p4_s5b.ogg"},
			{id: "sound_6a", src: soundAsset+"p4_s6a.ogg"},
			{id: "sound_6b", src: soundAsset+"p4_s6b.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				sound_nav('sound_0');
				var s = Snap('#svg-1');
				var route;
				var svg = Snap.load(preload.getResult('namrada').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					route		= Snap.select('#path1');
					timeoutvar = setTimeout(function(){
						route.addClass('road-anim');
					}, 1000);
				} );
				break;
			case 1:
				sound_nav('sound_1');
				$prevBtn.show(0);
				break;
			case 2:
				$prevBtn.show(0);
				conversation('.speechbox', 'sound_2', '.grandma-1', 'grandma-1-gif', 'grandma-1');
				break;
			case 3:
				setTimeout(function(){
					sound_nav('sound_3');
				},2000);
				$prevBtn.show(0);
				break;
			case 4:
				$prevBtn.show(0);
				conversation2('.db-1', 'sound_4a', '.girl-i1', 'girl-3-gif', 'girl-3', function(){
					conversation('.null', 'sound_4b', '.grandma-i1', 'grandma-2-gif', 'grandma-2', true);
				});
				break;
			case 5:
				$prevBtn.show(0);
				conversation2('.db-1', 'sound_5a', '.grandma-i2',  'grandma-2-gif', 'grandma-2', function(){
					conversation('.null', 'sound_5b', '.boy-i1', 'boytalking01', 'boy-2', true);
				});
				break;
			case 6:
				$prevBtn.show(0);
				var s = Snap('#map');
				var arrow;
				var svg = Snap.load(preload.getResult('map').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					arrow		= Snap.select('#arrow');
					timeoutvar = setTimeout(function(){
						arrow.addClass('arrow-anim');
					}, 1000);
				} );
				conversation2('.db-1', 'sound_6a', '.grandma-i3',  'grandma-2-gif', 'grandma-2', function(){
					conversation('.null', 'sound_6b', '.girl-i2', 'girl-4-gif', 'girl-4', true);
				});
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_1');
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function conversation2(speech_class, sound_data, imageclass, gif_src, png_src, callback){
		console.log(callback);
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
			callback();
		});
	}
	function conversation(speech_class, sound_data, imageclass, gif_src, png_src, flag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
			if(!flag){
				$(speech_class).click(function(){
					sound_gif_player(speech_class, sound_data, imageclass, gif_src, png_src);
				});
			}
			nav_button_controls(0);
		});
	}
	function sound_gif_player(speech_class, sound_data, imageclass, gif_src, png_src){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

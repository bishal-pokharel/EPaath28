var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2text5,
			datahighlightflag : true,
			datahighlightcustomclass : 'margintop-speech',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-full',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'room'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-1'
				},
				{
					imgclass : "boy-1",
					imgsrc : '',
					imgid : 'boy-1'
				},
				{
					imgclass : "grandma-1",
					imgsrc : '',
					imgid : 'grandma-1'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p2text6,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox3',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-full',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'room-2'
				},
				{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "boy-2",
					imgsrc : '',
					imgid : 'boy-2'
				},
				{
					imgclass : "grandma-2",
					imgsrc : '',
					imgid : 'grandma-2'
				}
			]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p2text7,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-full',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'riverside'
				},
				{
					imgclass : "bg-full room-fade-out",
					imgsrc : '',
					imgid : 'room-2'
				},
				{
					imgclass : "girl-3",
					imgsrc : '',
					imgid : 'girl-3'
				},
				{
					imgclass : "boy-3",
					imgsrc : '',
					imgid : 'boy-3'
				},
				{
					imgclass : "grandma-3",
					imgsrc : '',
					imgid : 'grandma-1'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'sp-4',
			textdata : data.string.p2text8,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox4',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-full',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'sea'
				},
				{
					imgclass : "flow-gif",
					imgsrc : '',
					imgid : 'flow-gif'
				},
				{
					imgclass : "girl-4",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "boy-4",
					imgsrc : '',
					imgid : 'boy-3'
				},
				{
					imgclass : "grandma-4",
					imgsrc : '',
					imgid : 'grandma-2'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'sp-5',
			textdata : data.string.p2text9,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: 'its_hidden',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
			svgblock: 'speech-svg'
		}],
		imagedivblock:[{
			imagediv : 'bg-full',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'sea'
				},
				{
					imgclass : "flow-gif",
					imgsrc : '',
					imgid : 'flow-gif'
				},
				{
					imgclass : "girl-4",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "boy-4",
					imgsrc : '',
					imgid : 'boy-3'
				},
				{
					imgclass : "grandma-4",
					imgsrc : '',
					imgid : 'grandma-1'
				}
			]
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'text-6',
			textdata : data.string.p2text10,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox-svg',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-full memory-anim',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'swim-1'
				},
				{
					imgclass : "swim",
					imgsrc : '',
					imgid : 'swim'
				},
			]
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-brown',

		speechbox:[{
			speechbox: 'text-6',
			textdata : data.string.p2text11,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox-svg',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-room-1',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'room-2'
				},
				{
					imgclass : "girl-4",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "boy-4",
					imgsrc : '',
					imgid : 'boy-3'
				},
				{
					imgclass : "grandma-4",
					imgsrc : '',
					imgid : 'grandma-2'
				}
			]
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-swim",
					imgsrc : '',
					imgid : 'swim-2'
				},
			]
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-brown',

		speechbox:[{
			speechbox: 'sp-6',
			textdata : data.string.p2text12,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox4',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-full',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'room-2'
				},
				{
					imgclass : "girl-4",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "boy-4",
					imgsrc : '',
					imgid : 'boy-3'
				},
				{
					imgclass : "grandma-4",
					imgsrc : '',
					imgid : 'grandma-2'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "room", src: imgpath+"p2/room01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "room-2", src: imgpath+"p2/room02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "riverside", src: imgpath+"p2/riverside.png", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-1", src: imgpath+"girl05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "swim-gif", src: imgpath+"swim.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "swim", src: imgpath+"swim.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-1", src: imgpath+"boy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-1", src: imgpath+"grandmum02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-1-gif", src: imgpath+"grandmum02-gif.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-2", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2-gif", src: imgpath+"girl03-gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-2", src: imgpath+"boy03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-2", src: imgpath+"grandmum04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-3", src: imgpath+"girl04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-3", src: imgpath+"boy04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sea", src: imgpath+"p2/sea.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flow-gif", src: imgpath+"waterflow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "swim-1", src: imgpath+"p2/swimming01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "swim-2", src: imgpath+"p2/swimming02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "textbox-svg", src: imgpath+"textboxes.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox2", src: imgpath+"textbox-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox3", src: "images/textbox/white/tr-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox4", src: imgpath+"textbox-4.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5_s6.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s7.ogg"},
			{id: "sound_splash", src: soundAsset+"splash.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				conversation('.speechbox', 'sound_0', '.grandma-1', 'grandma-1-gif', 'grandma-1');
				break;
			case 1:
				$prevBtn.show(0);
				conversation('.speechbox', 'sound_1', '.girl-2', 'girl-2-gif', 'girl-2');
				break;
			case 2:
				$prevBtn.show(0);
				conversation('.speechbox', 'sound_2', '.grandma-3', 'grandma-1-gif', 'grandma-1');
				break;
			case 3:
				$prevBtn.show(0);
				conversation('.speechbox', 'sound_3', '.girl-4', 'girl-2-gif', 'girl-2');
				break;
			case 4:
				$prevBtn.show(0);
				var s = Snap('#speech-svg');
				var box, box2, pbox1, pbox2, morphBox;
				var svg = Snap.load(preload.getResult('textbox-svg').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					box			= Snap.select('#path01');
					box2		= Snap.select("#path02");
					pbox1		= box.attr('path');
					pbox2		= box2.attr('path');
					box.attr('display', 'block');
					box2.attr('display', 'none');
					morphBox = function(){
						box.animate({ d: pbox2, fill: '#EFEFEF', strokeWidth: 0 }, 2000, mina.linear);
					};
				} );
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_4');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.grandma-4').addClass('goOutLeft');
					$('.girl-4').addClass('goOutRight');
					$('.boy-4').addClass('goOutRight');
					$('.speechbox').addClass('speechAnimBottom');
					morphBox();
				});
				conversation('.null', 'sound_4', '.grandma-4', 'grandma-1-gif', 'grandma-1');
				break;
			case 5:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_5');
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(500);
					// $('.grayed-bg').addClass('memory-anim');
					var randStr = new Date().getTime();
					$('.swim').attr('src', preload.getResult('swim-gif').src+'?'+randStr);
					timeoutvar = setTimeout(function(){
						createjs.Sound.play('sound_splash');
					}, 1800);
				});
				timeoutvar = setTimeout(function(){
					// $('.swim').attr('src', preload.getResult('swim-gif').src);
					
				}, 3800);
				break;
			case 6:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_6');
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(500);
				});
				break;
			case 7:
				$prevBtn.show(0);
				conversation('.speechbox', 'sound_7', '.girl-4', 'girl-2-gif', 'girl-2');
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_1');
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function conversation(speech_class, sound_data, imageclass, gif_src, png_src){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
			$(speech_class).click(function(){
				sound_gif_player(speech_class, sound_data, imageclass, gif_src, png_src);
			});
			nav_button_controls(0);
		});
	}
	function sound_gif_player(speech_class, sound_data, imageclass, gif_src, png_src){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			case 6:
				$nextBtn.hide(0);
				$prevBtn.hide(0);
				$('.bg-swim').addClass('ending-anim');
				$('.bg-room-1').addClass('room-in-anim');
				$('.text-6').fadeOut(500);
				timeoutvar = setTimeout(function(){
					countNext++;
					templateCaller();
				}, 3300);
			break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

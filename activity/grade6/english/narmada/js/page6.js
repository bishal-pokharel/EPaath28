var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-4',

		// uppertextblockadditionalclass: 'dboxes db-7',
		// uppertextblock:[{
			// textdata: data.string.p3text15,
			// textclass: "",
		// },{
			// textdata: data.string.p3text16,
			// textclass: "margintop-10",
		// }],
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p3text17,
			datahighlightflag : true,
			datahighlightcustomclass : 'margintop-speech',
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-i4",
					imgsrc : '',
					imgid : 'girl-3'
				},
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-5',

		speechbox:[{
			speechbox: 'sp-4',
			textdata : data.string.p3text18,
			datahighlightflag : true,
			datahighlightcustomclass : 'marginbottom-speech',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "grandma-i4",
					imgsrc : '',
					imgid : 'grandma-2'
				},
				{
					imgclass : "school",
					imgsrc : '',
					imgid : 'school'
				},
			]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-5',

		speechbox:[{
			speechbox: 'sp-5',
			textdata : data.string.p3text19,
			datahighlightflag : true,
			datahighlightcustomclass : 'margin-speech',
			imgclass: '',
			textclass : '',
			imgid : 'textbox5',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "grandma-i5",
					imgsrc : '',
					imgid : 'grandma-2'
				},
				{
					imgclass : "women",
					imgsrc : '',
					imgid : 'women'
				},
			],
			imagelabels:[{
				imagelabelclass:'img-label label-1',
				imagelabeldata: data.string.p3label1,
			}]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-5',

		speechbox:[{
			speechbox: 'sp-6',
			textdata : data.string.p3text22,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "grandma-i6",
					imgsrc : '',
					imgid : 'grandma-2'
				},
				{
					imgclass : "dozer",
					imgsrc : '',
					imgid : 'dozer'
				},
			],
			imagelabels:[{
				imagelabelclass:'img-label label-2',
				imagelabeldata: data.string.p3label2,
			}]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'last-block',
		uppertextblock:[{
			textdata: data.string.p3text23,
			textclass: "",
		},{
			textdata: data.string.p3text24,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'sp-7',
			textdata : data.string.p3text25,
			datahighlightflag : true,
			datahighlightcustomclass : 'margintop-speech-small',
			imgclass: '',
			textclass : '',
			imgid : 'textbox4',
			imgsrc: '',
			// audioicon: true,
		}],
		imagedivblock:[{
			imagediv : 'bg-extended',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'room'
				},
				{
					imgclass : "girl-last",
					imgsrc : '',
					imgid : 'girl-5'
				},
				{
					imgclass : "boy-last",
					imgsrc : '',
					imgid : 'boy-3'
				},
				{
					imgclass : "grandma-last",
					imgsrc : '',
					imgid : 'grandma-1'
				}
			]
		}],
	},
	//creative commons
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text0c,
			textclass: "creds",
		},{
			textdata: data.string.end,
			textclass: "end",
		},{
			textdata: data.string.credit,
			textclass: "credit",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'frontpage'
				}]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "room", src: imgpath+"p3/room.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school", src: imgpath+"p3/school.png", type: createjs.AbstractLoader.IMAGE},
			{id: "women", src: imgpath+"p3/women.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dozer", src: imgpath+"p3/dozer.jpg", type: createjs.AbstractLoader.IMAGE},


			{id: "grandma-1", src: imgpath+"grandmatalking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-1-gif", src: imgpath+"grandmatalking.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-3", src: imgpath+"talkgirl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3-gif", src: imgpath+"talkgirl02.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-4", src: imgpath+"girl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-4-gif", src: imgpath+"girl02-gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-5", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-5-gif", src: imgpath+"girl03-gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-3", src: imgpath+"boy04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-2", src: imgpath+"grandmatalking01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-2-gif", src: imgpath+"grandmatalking01.gif", type: createjs.AbstractLoader.IMAGE},


			{id: "textbox", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox4", src: imgpath+"textbox-4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox5", src: imgpath+"textbox-5.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frontpage", src: imgpath+"p1/coverpage.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p6_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p6_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p6_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p6_s3.ogg"},
			{id: "sound_4_1", src: soundAsset+"p6_s4_1.ogg"},
			{id: "sound_4_2", src: soundAsset+"p6_s4_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				conversation('.none', 'sound_0', '.girl-i4', 'girl-3-gif', 'girl-3', true);
				break;
			case 1:
				$prevBtn.show(0);
				conversation('.none', 'sound_1', '.grandma-i4', 'grandma-2-gif', 'grandma-2', true);
				break;
			case 2:
				$prevBtn.show(0);
				conversation('.none', 'sound_2', '.grandma-i5', 'grandma-2-gif', 'grandma-2', true);
				break;
			case 3:
				$prevBtn.show(0);
				conversation('.none', 'sound_3', '.grandma-i6', 'grandma-2-gif', 'grandma-2', true);
				break;
			case 4:
				$prevBtn.show(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_4_1");
					current_sound.play();
					current_sound.on("complete", function(){
						$(".girl-last").attr('src', preload.getResult("girl-5-gif").src);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("sound_4_2");
							current_sound.play();
							current_sound.on("complete", function(){
								$(".girl-last").attr('src', preload.getResult("girl-5").src);
								nav_button_controls(300);
							});
					});
				break;
			default:
				nav_button_controls(100);
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function conversation2(speech_class, sound_data, imageclass, gif_src, png_src, callback){
		console.log(callback);
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
			callback();
		});
	}
	function conversation(speech_class, sound_data, imageclass, gif_src, png_src, flag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
			if(!flag){
				$(speech_class).click(function(){
					sound_gif_player(speech_class, sound_data, imageclass, gif_src, png_src);
				});
			}
			nav_button_controls(0);
		});
	}
	function sound_gif_player(speech_class, sound_data, imageclass, gif_src, png_src){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

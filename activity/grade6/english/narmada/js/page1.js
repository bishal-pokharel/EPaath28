var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.lesson_title,
			textclass: "lesson-title",
		},{
			textdata: data.string.p1text0a,
			textclass: "p1text0 fsta",
		},{
			textdata: data.string.p1text0b,
			textclass: "p1text0 fstb",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'frontpage'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1text1,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-1'
				},{
					imgclass : "boy-1",
					imgsrc : '',
					imgid : 'boy-1'
				}
			]
		}],
		svgblock: [{
			svgblock: 'svg-1',
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p1text3,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},{
					imgclass : "boy-2",
					imgsrc : '',
					imgid : 'boy-1'
				}
			]
		}],
		svgblock: [{
			svgblock: 'svg-nepal',
		},{
			svgblock: 'svg-india',
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p1text2,
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "boy-2",
					imgsrc : '',
					imgid : 'boy-1'
				}
			]
		}],
		svgblock: [{
			svgblock: 'svg-2',
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		speechbox:[{
			speechbox: 'fade-out sp-2',
			textdata : data.string.p1text2,
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		},{
			speechbox: 'fade-in-3 sp-4',
			textdata : data.string.p1text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fade-out girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "fade-out boy-2",
					imgsrc : '',
					imgid : 'boy-1'
				},
				{
					imgclass : "fade-in-3 girl-3",
					imgsrc : '',
					imgid : 'girl-full'
				},
				{
					imgclass : "fade-in-3 grandma-1",
					imgsrc : '',
					imgid : 'grandma-1'
				},
			]
		}],
		svgblock: [{
			svgblock: 'svg-zoom',
		}],
	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		speechbox:[{
			speechbox: 'sp-5',
			textdata : data.string.p1text5,
			datahighlightflag : true,
			datahighlightcustomclass : '',
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'frontpage'
				},
				{
					imgclass : "girl-4",
					imgsrc : '',
					imgid : 'girl-1'
				},
				{
					imgclass : "boy-4",
					imgsrc : '',
					imgid : 'boy-1'
				}
			]
		}],
	},

	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'last-p1-text',
		uppertextblock:[{
			textdata: data.string.p1text6,
			textclass: "",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-5",
					imgsrc : '',
					imgid : 'girl-1'
				},
				{
					imgclass : "wobble-new bubble-1",
					imgsrc : '',
					imgid : 'textbox2'
				},
				{
					imgclass : "grandma-2",
					imgsrc : '',
					imgid : 'grandma-2'
				},
				{
					imgclass : "wobble-new1 bubble-2",
					imgsrc : '',
					imgid : 'textbox3'
				},
			]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "frontpage", src: imgpath+"p1/coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-1", src: imgpath+"p1/im01.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+"p1/im02.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+"p1/nepal.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "im-4", src: imgpath+"p1/india.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-1", src: imgpath+"girl01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-1-gif", src: imgpath+"girl01-gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"girl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2-gif", src: imgpath+"girl02-gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3-gif", src: imgpath+"girl03-gif.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-full", src: imgpath+"girltalking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-full-gif", src: imgpath+"girltalking.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "boy-1", src: imgpath+"boy01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-1", src: imgpath+"grandmum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-2", src: imgpath+"grandmum02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma-2-gif", src: imgpath+"grandmum02-gif.png", type: createjs.AbstractLoader.IMAGE},

			{id: "lastpage", src: imgpath+"p1/lastpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river", src: imgpath+"p1/narmada.jpg", type: createjs.AbstractLoader.IMAGE},

			{id: "textbox", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox2", src: imgpath+"textbox-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox3", src: imgpath+"textbox-3.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				sound_nav('sound_1');
				break;
			case 1:
				$prevBtn.show(0);
				var s = Snap('#svg-1');
				var nepal, india;
				var svg = Snap.load(preload.getResult('im-1').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					nepal			= Snap.select('#pathnepal');
					india			= Snap.select('#pathindia');
					timeoutvar = setTimeout(function(){
						india.addClass('hl-map');
					}, 1000);
					timeoutvar2 = setTimeout(function(){
						india.removeClass('hl-map');
						nepal.addClass('hl-map');
						nav_button_controls(14000);
					}, 3000);
				} );
				conversation('.speechbox', 'sound_2', '.girl-1', 'girl-1-gif', 'girl-1');
				break;
			case 2:
				$prevBtn.show(0);
				$('#svg-india').hide(0);
				$('#svg-nepal').hide(0);
				var s = Snap('#svg-nepal');
				var outline, grs = [], texts = [];
				var svg = Snap.load(preload.getResult('im-3').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					outline			= Snap.select('#outline');
					$('#svg-nepal').fadeIn(1000);
					for(var i=1; i<8; i++){
						grs.push(Snap.select('#p'+i));
						texts.push(Snap.select('#gt-'+i));
					}
					console.log(grs);
					console.log(texts);
					for(var i=0; i<7; i++){
						grs[i].addClass('fill-anim-'+i);
						texts[i].addClass('fade-anim-'+i);
					}
					outline.attr({'display': 'block', 'stroke-widht': '2'});
				} );

				var s2 = Snap('#svg-india');
				var outlines = [];
				var svg = Snap.load(preload.getResult('im-4').src, function ( loadedFragment ) {
					s2.append(loadedFragment);
					setTimeout(function(){
						$('#svg-india').fadeIn(1000);
						nav_button_controls(8000);
					}, 9000);
				} );
				conversation('.speechbox', 'sound_3', '.girl-2', 'girl-2-gif', 'girl-2');
				break;
			case 3:
				$prevBtn.show(0);
				var s = Snap('#svg-2');
				var ahmedabad, road, textraj, dotraj, raj, dotahm, totalsvg;
				var svg = Snap.load(preload.getResult('im-2').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					totalsvg		= Snap.select('#total');
					dotraj			= Snap.select('#point');
					dotahm			= Snap.select('#gpoint');
					textraj			= Snap.select('#textraj');
					raj				= Snap.select('#raj');
					ahmedabad		= Snap.select('#ahmedabad');
					road			= Snap.select('#road');
					timeoutvar = setTimeout(function(){
						dotraj.addClass('hl-dot');
					}, 1000);
					timeoutvar2 = setTimeout(function(){
						console.log(totalsvg);
						totalsvg.addClass('zoom-map');
						raj.addClass('normal-state');
						raj.addClass('hl-map2');
						textraj.attr('opacity', '1');
						nav_button_controls(6000);
					}, 3000);
				} );
				conversation('.speechbox', 'sound_4', '.girl-2', 'girl-2-gif', 'girl-2');
				break;
			case 4:
				$prevBtn.show(0);
				var s = Snap('#svg-zoom');
				var ahmedabad, road, textraj, dotraj, raj, dotahm, totalsvg;
				var svg = Snap.load(preload.getResult('im-2').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					totalsvg		= Snap.select('#total');
					dotraj			= Snap.select('#point');
					dotahm			= Snap.select('#gpoint');
					textraj			= Snap.select('#textraj');
					raj				= Snap.select('#raj');
					ahmedabad		= Snap.select('#ahmedabad');
					road			= Snap.select('#road');
					raj.addClass('normal-state');
					dotahm.addClass('fade-map');
					textraj.attr('opacity', '1');
					ahmedabad.addClass('fade-map');
					road.addClass('road-anim');
					nav_button_controls(8000);
				} );
				timeoutvar = setTimeout(function(){
					conversation('.speechbox', 'sound_5', '.girl-3', 'girl-full-gif', 'girl-2');
				}, 4000);
				break;
			case 5:
				$prevBtn.show(0);
				conversation('.speechbox', 'sound_6', '.girl-4', 'girl-1', 'girl-1');
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_'+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function conversation(speech_class, sound_data, imageclass, gif_src, png_src){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
			$(speech_class).click(function(){
				sound_gif_player(speech_class, sound_data, imageclass, gif_src, png_src);
			});
			nav_button_controls(0);
		});
	}
	function sound_gif_player(speech_class, sound_data, imageclass, gif_src, png_src){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$(imageclass).attr('src', preload.getResult(gif_src).src);
		current_sound.on("complete", function(){
			$(imageclass).attr('src', preload.getResult(png_src).src);
		});
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

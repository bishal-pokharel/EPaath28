var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "text-center",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic1',
				imgclass:'top_image'
			}]
		}]

	},

	// slide1
	{
		contentblockadditionalclass:'sky_bg',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "text-top",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'boy_big_tommy',
				imgclass:'boy_big_tommy'
			}]
		}],
			speechbox:[{
				speechbox: 'sp-2',
				textdata : data.string.p2text3,
				imgclass: 'flipped-h',
				textclass : 'text_inside',
				imgid : 'sp-dialogue1',
				imgsrc: '',
				// audioicon: true,
			}],

	},

	// slide2
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text5,
			textclass: "top_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text6,
			textclass: "right_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text7,
			textclass: "answer",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic1',
				imgclass:'left_image'
			},{
				imgid:'bellygif',
				imgclass:'bellygif'
			}]
		}]

	},

	// slide3
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text8,
			textclass: "top_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text9,
			textclass: "right_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text10,
			textclass: "options opt1",
		},{
			textdata: data.string.p2text11,
			textclass: "options opt2 correct",
		},{
			textdata: data.string.p2text12,
			textclass: "options opt3",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic2',
				imgclass:'left_image'
			},
				{
					imgid:'earguyimg',
                    imgclass:'earguy'

                }
			]
		}]

	},

	// slide4
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text13,
			textclass: "top_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text14,
			textclass: "right_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text15,
			textclass: "options opt1",
		},{
			textdata: data.string.p2text16,
			textclass: "options opt2 ",
		},{
			textdata: data.string.p2text17,
			textclass: "options opt3 correct",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic3',
				imgclass:'left_image'
			}]
		}]

	},

	// slide5
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text18,
			textclass: "top_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text19,
			textclass: "right_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text20,
			textclass: "options opt1 correct",
		},{
			textdata: data.string.p2text21,
			textclass: "options opt2 ",
		},{
			textdata: data.string.p2text22,
			textclass: "options opt3 ",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'headache',
				imgclass:'left_image'
			}]
		}]

	},

	// slide6
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text23,
			textclass: "top_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text24,
			textclass: "right_question",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text25,
			textclass: "options opt1 correct",
		},{
			textdata: data.string.p2text26,
			textclass: "options opt2 ",
		},{
			textdata: data.string.p2text27,
			textclass: "options opt3 ",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bounce',
				imgclass:'left_image'
			}]
		}]

	},

	// slide7
	{
		contentblockadditionalclass:'sky_bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'boy_big_tommy',
				imgclass:'boy_big_tommy'
			}]
		}],
			speechbox:[{
				speechbox: 'sp-2',
				textdata : data.string.p2text28,
				imgclass: 'flipped-h',
				textclass : 'text_inside',
				imgid : 'sp-dialogue1',
				imgsrc: '',
				// audioicon: true,
			}],

	},

	// slide8
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text30,
			textclass: "top_question",
		},{
			textdata: data.string.p2text31,
			textclass: "mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'squirrel',
				imgclass:'squirrel'
			}]
		}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p2text32,
				imgclass: 'flipped-h',
				textclass : 'text_inside',
				imgid : 'sp-dialogue1',
				imgsrc: '',
				// audioicon: true,
			}],

	},

	// slide9
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text30,
			textclass: "top_question",
		},{
			textdata: data.string.p2text31,
			textclass: "mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'squirrel',
				imgclass:'squirrel'
			}]
		}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p2text33,
				imgclass: 'flipped-h',
				textclass : 'text_inside',
				imgid : 'sp-dialogue1',
				imgsrc: '',
				// audioicon: true,
			}],

	},

	// slide10
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text30,
			textclass: "top_question",
		},{
			textdata: data.string.p2text31,
			textclass: "mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'squirrel',
				imgclass:'squirrel'
			}]
		}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p2text34,
				imgclass: 'flipped-h',
				textclass : 'text_inside',
				imgid : 'sp-dialogue1',
				imgsrc: '',
				// audioicon: true,
			}],

	},

	// slide11
	{
		contentblockadditionalclass:'yellow_bg',
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2text35,
			imgclass: 'flipped-h',
			textclass : 'text_inside',
			imgid : 'sp-dialogue1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'squirrel',
				imgclass:'squirrel'
			}]
		}]

	},

	// slide12
	{
		contentblockadditionalclass:'yellow_bg',
		uppertextblock:[{
			textdata: data.string.p2text36,
			textclass: "top_question",
		},{
			textdata: data.string.p2text37,
			textclass: "mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p2text38,
			textclass: "options opta1 correct",
		},{
			textdata: data.string.p2text39,
			textclass: "options opta2 ",
		},{
			textdata: data.string.p2text40,
			textclass: "options opta3 ",
		}],


	},


		// slide13
		{
			contentblockadditionalclass:'yellow_bg',
			uppertextblock:[{
				textdata: data.string.p2text41,
				textclass: "top_question",
			},{
				textdata: data.string.p2text42,
				textclass: "mid_text",
				datahighlightflag:true,
				datahighlightcustomclass:'underlined'
			},{
				textdata: data.string.p2text43,
				textclass: "options opta1 ",
			},{
				textdata: data.string.p2text44,
				textclass: "options opta2 correct",
			},{
				textdata: data.string.p2text45,
				textclass: "options opta3 ",
			}],


		},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "pic1", src: imgpath+"08.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic2", src: imgpath+"09.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic3", src: imgpath+"06.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pic4", src: imgpath+"06.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "headache", src: imgpath+"headache.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bounce", src: imgpath+"bounce.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_big_tommy", src: imgpath+"boy_big_tommy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bellygif", src: imgpath+"belly2.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "earguyimg", src: imgpath+"ears.gif", type: createjs.AbstractLoader.IMAGE},
			//tobe done
			{id: "sp-dialogue", src: imgpath+"thinking_cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_2_1", src: soundAsset+"s2_p2_1.ogg"},
			{id: "sound_2_2", src: soundAsset+"s2_p2_2.ogg"},
			{id: "sound_3_1", src: soundAsset+"s2_p3_1.ogg"},
			{id: "sound_3_2", src: soundAsset+"s2_p3_2.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s2_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s2_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s2_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s2_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s2_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s2_p12.ogg"},
			{id: "sound_13", src: soundAsset+"s2_p13.ogg"},
			{id: "sound_14", src: soundAsset+"s2_p14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);


		$('.options').click(function(){
			createjs.Sound.stop();
			if($(this).hasClass('correct')){
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = ((((position.top + height)*100)/$('.coverboardfull').height()+2)+2)+'%';
				$(this).css({'background':'#98C02E','border-radius':'2vmin','border':'.5vmin solid #DEEF3C'});
				$('.options').css('pointer-events','none');
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;transform:translate(0%,-347%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = ((((position.top + height)*100)/$('.coverboardfull').height()+5)+1)+'%';
				$(this).css({'background':'#FF0000','border-radius':'2vmin','border':'.5vmin solid #980000'});
				$(this).css('pointer-events','none');
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;transform:translate(0%,-347%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				play_correct_incorrect_sound(0);
			}
		});
		var top_val = (($('.top_question').height()/$('.coverboardfull').height() )* 100) +6.5+"%";
		$('.left_image,.mid_text').css({
			'top':top_val
		});
		switch(countNext) {
			case 1:
                playaudioseq(["sound_2_1","sound_2_2"]);
				break;
            case 2:
                playaudioseq(["sound_3_1","sound_3_2"]);
                break;
			case 3:
			case 4:
			case 5:
			case 6:
			case 12:
			case 13:
                sound_play_click('sound_'+(countNext+1),false);
				break;
            case 0:
            case 2:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
                sound_play_click('sound_'+(countNext+1),true);
				// nav_button_controls(1000);
				break;
			case 6:
			$('.left_image').css('background','white');
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, navigate){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			navigate?nav_button_controls(0):"";
		});
	}
    function playaudioseq(sound1){
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play(sound1[0]);
        sound1.splice(0,1);
        current_sound.on("complete", function(){
            if(sound1.length>0)
                playaudioseq(sound1);
            else
                nav_button_controls(0);
        });
    }
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

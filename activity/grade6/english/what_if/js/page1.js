var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.what,
			textclass: "what",
		},{
			textdata: data.string.if,
			textclass: "if",
		},
		{
			textdata: data.string.p1text1,
			textclass: "bottomtext"
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'l_leg',
				imgclass:'l_leg'
			},
			{
				imgid:'r_leg',
				imgclass:'r_leg'
			},
			{
				imgid:'r_hand',
				imgclass:'r_hand'
			},
			{
				imgid:'l_hand',
				imgclass:'l_hand'
			},
			{
				imgid:'head',
				imgclass:'head'
			}]
		}]
	},

	// slide1
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "center_text",
		}],
	},

	// slide2
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text3,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic1',
				imgclass:'top_img'
			}]
		}]
	},

	// slide3
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text4,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic2',
				imgclass:'top_img'
			},
			{
				imgid:'brushing',
				imgclass:'brushinggif'
			}]
		}]
	},

	// slide4
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text5,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic3',
				imgclass:'top_img'
			},
			{
				imgid:'neckgif',
				imgclass:'neck'
			}]
		}]
	},

	// slide5
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text6,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic4',
				imgclass:'top_img'
			},
			{
				imgid:'leggif',
				imgclass:'leg'
			}]
		}]
	},

	// slide6
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text7,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic5',
				imgclass:'top_img'
			}]
		}]
	},

	// slide7
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text8,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic6',
				imgclass:'top_img'
			},
			{
				imgid:'eating_grass',
				imgclass:'top_img eating_grass'
			}]
		}]
	},

	// slide8
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text9,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic7',
				imgclass:'top_img'
			},{
				imgid:'belly2',
				imgclass:'top_img belly'
			}]
		}]
	},

	// slide9
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text10,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic8',
				imgclass:'top_img'
			},{
				imgid:'birdgif',
				imgclass:'bird'
			},{
				imgid:'ears',
				imgclass:'earguy'
			}]
		}]
	},

	// slide10
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text11,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic9',
				imgclass:'top_img'
			},
			{
				imgid:'soundwave',
				imgclass:'soundwave'
			}]
		}]
	},

	// slide11
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text12,
			textclass: "bot_poem_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic10',
				imgclass:'top_img'
			},{
				imgid:'mouth',
				imgclass:'mouth'
			}]
		}]
	},

	// slide12
	{
		contentblockadditionalclass: 'cream_bg',
		uppertextblock:[{
			textdata: data.string.p1text13,
			textclass: "mid_text",
		},{
			textdata: data.string.p1text14,
			textclass: "low_text_1",
		},{
			textdata: data.string.p1text15,
			textclass: "low_text_2",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pic8',
				imgclass:'top_img'
			},{
				imgid:'birdgif',
				imgclass:'bird'
			},{
				imgid:'ears',
				imgclass:'earguy'
			}]
		}]
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "l_hand", src: imgpath+"hand01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "r_hand", src: imgpath+"hand02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "l_leg", src: imgpath+"leg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "r_leg", src: imgpath+"leg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "head", src: imgpath+"head.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pic1", src: imgpath+"02.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic2", src: imgpath+"03.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic3", src: imgpath+"04.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic4", src: imgpath+"bg_for_leg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pic5", src: imgpath+"06.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pic6", src: imgpath+"07.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic7", src: imgpath+"08.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic8", src: imgpath+"09.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic9", src: imgpath+"10.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "pic10", src: imgpath+"11.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "neckgif", src: imgpath+"neck_gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "brushing", src: imgpath+"brushing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "leggif", src: imgpath+"leg.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "birdgif", src: imgpath+"bird.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "belly2", src: imgpath+"belly2.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "ears", src: imgpath+"ears.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mouth", src: imgpath+"mouth.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "eating_grass", src: imgpath+"eating_grass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "soundwave", src: imgpath+"soundmove.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				$prevBtn.hide(0);
				setTimeout(function(){sound_play_click('sound_1');
                },2000);
				break;
			case 1:
				sound_play_click('sound_2');
				break;
			case 2:
				sound_play_click('sound_3');
				break;
			case 3:
				sound_play_click('sound_4');
				break;
			case 4:
				sound_play_click('sound_5');
				$('.bottomtext').css({"bottom":"-30%"}).animate({"bottom":"0%"},500);
				break;
			case 5:
				sound_play_click('sound_6');
				break;
			case 6:
				sound_play_click('sound_7');
				$('.top_img').css('width','106%');
				break;
			case 7:
				sound_play_click('sound_8');
				break;
			case 8:
				sound_play_click('sound_9');
				break;
			case 9:
			case 10:
			case 11:
                sound_play_click('sound_'+(countNext+1));
                break;
			case 12:
                sound_play_click('sound_'+(countNext+1));
                $('.top_img,.bird,.earguy').css('opacity','.5');
			nav_button_controls(100);
			break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

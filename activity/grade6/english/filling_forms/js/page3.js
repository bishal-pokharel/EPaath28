var imgpath = $ref + "/images/imagesfordiy/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "objdiv",
        uppertextblock: [
            {
                textclass: "title centertext1",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv",
                    imgclass: "relativecls diyimg",
                    imgid: 'diyImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "dial1",
        uppertextblock: [
            {
                textclass: "diyfont centertext",
                textdata: data.string.p3text1
            }

        ],
        imageblock: [{
            commonImgDiv:"imgdiv1",
            imagestoshow: [
                {

                    imgdiv: "girldiv",
                    imgclass: "relativecls girlimg",
                    imgid: 'girlImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }],
        questionblock: [
            {
                textdiv:"div1 centertext relativecls",
                textclass: "content2 ",
                textdata: data.string.ques1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.greeting
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "diyfont centertext",
                textdata: data.string.introduction
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.leavetaking
            }

        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "dial1",
        uppertextblock: [
            {
                textclass: "diyfont centertext",
                textdata: data.string.p3text2
            }

        ],
        imageblock: [{
            commonImgDiv:"imgdiv1",
            imagestoshow: [
                {

                    imgdiv: "girldiv",
                    imgclass: "relativecls girlgrpimg",
                    imgid: 'girlgrpImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }],
        questionblock: [
            {
                textdiv:"div1 centertext relativecls",
                textclass: "content2 ",
                textdata: data.string.ques1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "diyfont centertext",
                textdata: data.string.greeting
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.introduction
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.leavetaking
            }

        ],
    },
// slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "dial2",
        uppertextblock: [
            {
                textclass: "diyfont centertext",
                textdata: data.string.p3text3
            }

        ],
        imageblock: [{
            commonImgDiv:"imgdiv1",
            imagestoshow: [
                {

                    imgdiv: "girldiv",
                    imgclass: "relativecls telephoneimg",
                    imgid: 'telephoneImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv1",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }],
        questionblock: [
            {
                textdiv:"div1 centertext relativecls",
                textclass: "content2",
                textdata: data.string.ques1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.greeting
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.introduction
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "diyfont centertext",
                textdata: data.string.leavetaking
            }

        ],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "dial3",
        uppertextblock: [
            {
                textclass: "diyfont centertext",
                textdata: data.string.p3text4
            }

        ],
        imageblock: [{
            commonImgDiv:"imgdiv1",
            imagestoshow: [
                {

                    imgdiv: "grpdiv",
                    imgclass: "relativecls grpimg",
                    imgid: 'grpImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv2",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg1',
                    imgsrc: ""
                },
            ]
        }],
        questionblock: [
            {
                textdiv:"div1 centertext relativecls",
                textclass: "content2 ",
                textdata: data.string.ques1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.greeting
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.introduction
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "diyfont centertext",
                textdata: data.string.leavetaking
            }

        ],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "dial2",
        uppertextblock: [
            {
                textclass: "diyfont centertext",
                textdata: data.string.p3text5
            }

        ],
        imageblock: [{
            commonImgDiv:"imgdiv1",
            imagestoshow: [
                {

                    imgdiv: "grlboydiv",
                    imgclass: "relativecls grlboyimg",
                    imgid: 'grlboyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv1",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }],
        questionblock: [
            {
                textdiv:"div1 centertext relativecls",
                textclass: "content2 ",
                textdata: data.string.ques1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.greeting
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.introduction
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "diyfont centertext",
                textdata: data.string.leavetaking
            }

        ],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "diyImg", src: imgpath + "bg_diy.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlImg", src: imgpath + "girl.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath + "buble-27.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg1", src: imgpath + "bubble-19.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlgrpImg", src: imgpath + "girlgrp.png", type: createjs.AbstractLoader.IMAGE},
            {id: "telephoneImg", src: imgpath + "telephone.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grpImg", src: imgpath + "grp.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grlboyImg", src: imgpath + "grlboy.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "p3_s2", src: soundAsset + "p3_s2.ogg"},
            {id: "p3_s3", src: soundAsset + "p3_s3.ogg"},
            {id: "p3_s4", src: soundAsset + "p3_s4.ogg"},
            {id: "p3_s5", src: soundAsset + "p3_s5.ogg"},
            {id: "p3_s6", src: soundAsset + "p3_s6.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                play_diy_audio();
                navigationcontroller(countNext, $total_page);
                break;
            case 1:
                sound_player("p3_s2",false);
                shufflehint(data.string.introduction);
                checkans();
                break;
            case 2:
                sound_player("p3_s3",false);
                shufflehint(data.string.greeting);
                checkans();
                break;
            case 3:
                sound_player("p3_s4",false);
                shufflehint(data.string.leavetaking);
                checkans();
                break;
            case 4:
                sound_player("p3_s5",false);
                shufflehint(data.string.introduction);
                checkans();
                break;
            case 5:
                sound_player("p3_s6",false);
                shufflehint(data.string.leavetaking);
                checkans();
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateImg(){
        $(".div2,.imgdivright,.div4").hide(0);
        setTimeout(function(){
            $(".div2,.imgdivright,.div4").show(0);
        },2500);
    }
    function shufflehint(correctans) {
        var optiondiv = $(".option");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["commonbtn option1","commonbtn option2","commonbtn option3","commonbtn option4"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().trim()==correctans){
                $this.addClass("correct")
            }
        });

    }
    function checkans(){
        $(".commonbtn ").on("click",function () {
            createjs.Sound.stop();
            if($(this).hasClass("correct") ) {
                $(this).addClass("correctans");
                $(".commonbtn").addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext,$total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }

});

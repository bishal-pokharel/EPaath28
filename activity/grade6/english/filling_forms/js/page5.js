var imgpath = $ref + "/images/Page04/";
var imgpath1 = $ref + "/images/imagesfordiy/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "bgtitle fadeInEffect",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.p5text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "bgtitle1 fadeInEffect",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.p5text2
            }

        ],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        textblock:[
            {
               textdiv:"objdiv fadeInEffect",
               textclass:"content2 centertext",
               textdata:data.string.p5text3
            },
            {
                textdiv:"objdiv1 fadeInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p5text4
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "recept1",
                    imgclass: "relativecls recept1img",
                    imgid: 'recept1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "reception",
                    imgclass: "relativecls receptionimg",
                    imgid: 'receptionImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "reception1",
                    imgclass: "relativecls reception1img",
                    imgid: 'reception1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "recept2",
                    imgclass: "relativecls recept2img",
                    imgid: 'recept2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "lady",
                    imgclass: "relativecls ladyimg",
                    imgid: 'ladyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv fadeInEffect",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv1 fadeInEffect",
                    imgclass: "relativecls boximg1",
                    imgid: 'boxImg1',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover1",
        textblock:[
            {
                textdiv:"objdivmala fadeInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p5text5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background ",
                    imgclass: "relativecls backgroundimg",
                    imgid: 'backgroundImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ladymala ",
                    imgclass: "relativecls lady2img",
                    imgid: 'lady2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ladymala rec1",
                    imgclass: "relativecls lady1img",
                    imgid: 'lady1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdivmala fadeInEffect",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        textblock:[
            {
                textdiv:"dial1 fadeInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p5text6
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background1 ",
                    imgclass: "relativecls backgroundimg",
                    imgid: 'background1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "mala",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mala rec1",
                    imgclass: "relativecls mala1img",
                    imgid: 'mala1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ranu",
                    imgclass: "relativecls ranuimg",
                    imgid: 'ranuImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdial fadeInEffect",
                    imgclass: "relativecls boximg",
                    imgid: 'dialImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        textblock:[
            {
                textdiv:"dial2 fadeInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p5text7
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background1 ",
                    imgclass: "relativecls backgroundimg",
                    imgid: 'background1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "mala",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ranu1",
                    imgclass: "relativecls ranuimg",
                    imgid: 'ranu1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ranu1 rec1",
                    imgclass: "relativecls ranu2img",
                    imgid: 'ranu2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdial1 fadeInEffect",
                    imgclass: "relativecls boximg",
                    imgid: 'dialImg',
                    imgsrc: ""
                },
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var dataFilled = [{"dateOfVisit":"","nameOfPatient":"","gender":"","age":"","address":"","reason":"vacination","":""}]
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverImg", src: imgpath + "school-building02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "recept1Img", src: imgpath + "part02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "recept2Img", src: imgpath + "part01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ladyImg", src: imgpath + "talking_phone01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath + "buble-27.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg1", src: imgpath + "namas-07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "receptionImg", src: imgpath + "talking-phone.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "reception1Img", src: imgpath + "talking-phone.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lady1Img", src: imgpath + "talking_phone01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "lady2Img", src: imgpath + "talking_phone01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "backgroundImg", src: imgpath + "bg_new01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "background1Img", src: imgpath + "bg_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ranuImg", src: imgpath + "ranu.png", type: createjs.AbstractLoader.IMAGE},
            {id: "malaImg", src: imgpath + "mala-at-home.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mala1Img", src: imgpath + "mala_at_home.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "recep_talking-24.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ranu2Img", src: imgpath + "happy_girl.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "ranu1Img", src: imgpath + "happy_girl01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyImg", src: imgpath1 + "a_08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "healthpostImg", src: imgpath1 + "healthpost.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handiconImg", src: imgpath1+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "healthpostlogoImg", src: imgpath1+"health_post_logo.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s5_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s5_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s5_p3_1.ogg"},
            {id: "sound_3", src: soundAsset + "s5_p3_2.ogg"},
            {id: "sound_4", src: soundAsset + "s5_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s5_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s5_p6.ogg"},
            {id: "sound_telephone", src: soundAsset + "telephone.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
            sound_player('sound_0');
            break;
            case 1:
                sound_player('sound_1');
                setTimeout(function(){
                    sound_player('sound_telephone',true);
                    setTimeout(function(){
                        navigationcontroller(countNext, $total_page);
                    },2000);
                },2000);
                break;
            case 2:
                $(".objdiv,.objdiv1,.boxdiv,.boxdiv1").hide();
                $(".recept2,.lady").animate({'bottom':'0%'},1000,function(){
                  $(".objdiv,.boxdiv").show(200,function(){
                    createjs.Sound.stop();
                    var current_sound1 = createjs.Sound.play('sound_2');
                    current_sound1.play();
                    current_sound1.on('complete', function () {
                      $(".reception1").animate({'top':'27%'},1000);
                      $(".recept1").animate({'top':'0%'},1000,function(){
                        $(".objdiv1,.boxdiv1").show(200,function(){
                        createjs.Sound.stop();
                        var current_sound2 = createjs.Sound.play('sound_3');
                        current_sound2.play();
                        current_sound2.on('complete', function () {
                          navigationcontroller(countNext, $total_page);
                        });
                        });
                      });
                    });
                  // $(".recept1,.reception").show(0,function(){
                    // $(".boxdiv1,.objdiv1").show(0);
                  // });
                });
                });

                break;
            case 3:
                $(".boxdivmala,.objdivmala").hide();
                setTimeout(function(){
                   $(".boxdivmala,.objdivmala").show(200,function(){
                     sound_player('sound_4');
                   });
                },2000);
                break;
            case 4:
                sound_player('sound_5',true);
                break;
            case 5:
                sound_player('sound_6',true);
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            $(".rec1").fadeOut(100)
             countNext!=1?navigationcontroller(countNext, $total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

});

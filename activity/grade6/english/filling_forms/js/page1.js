var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var imgpath1 = $ref+"/images/Animation/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "handImg",
                    imgclass: "relativecls handImgimg",
                    imgid: 'handImgImg',
                    imgsrc: ""
                }

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv fadeInEffect",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p1text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdivnew fadeInEffect",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p1text2
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage c1 zoomCoverImage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverpageImg1',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "dial1",
        uppertextblock: [
            {
                textclass: "content3 centertext",
                textdata: data.string.p1text3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1",
                    imgclass: "relativecls bgimg1",
                    imgid: 'bgimg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ],
        }],
        imageblock1: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "father",
                        imgclass: "relativecls fatherimg",
                        imgid: 'fatherImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "niti",
                        imgclass: " relativecls nitiimg",
                        imgid: 'nitiImg',
                        imgsrc: ""
                    },  {
                        imgdiv: "niti rec1",
                        imgclass: " relativecls nitiimg1",
                        imgid: 'nitiImg1',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "asha",
                        imgclass: " relativecls ashaimg",
                        imgid: 'ashaImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "mala",
                        imgclass: "relativecls malaimg",
                        imgid: 'malaImg',
                        imgsrc: ""
                    },

                ]
            }]
        }]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "dial2",
        uppertextblock: [
            {
                textclass: "content3 centertext",
                textdata: data.string.p1text4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1",
                    imgclass: "relativecls bgimg1",
                    imgid: 'bgimg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv1",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg1',
                    imgsrc: ""
                },
            ],
        }],
        imageblock1: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "father",
                        imgclass: "relativecls fatherimg",
                        imgid: 'fatherImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "niti2",
                        imgclass: " relativecls nitiimg",
                        imgid: 'nitiImg3',
                        imgsrc: ""
                    },  {
                        imgdiv: "asha ",
                        imgclass: " relativecls ashaimg1",
                        imgid: 'ashaImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "asha rec1",
                        imgclass: " relativecls ashaimg",
                        imgid: 'ashaImg1',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "mala",
                        imgclass: "relativecls malaimg",
                        imgid: 'malaImg',
                        imgsrc: ""
                    },

                ]
            }]
        }]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "dial3",
        uppertextblock: [
            {
                textclass: "content3 centertext",
                textdata: data.string.p1text5
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1",
                    imgclass: "relativecls bgimg1",
                    imgid: 'bgimg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv2",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ],
        }],
        imageblock1: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "father",
                        imgclass: "relativecls fatherimg",
                        imgid: 'fatherImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "niti",
                        imgclass: " relativecls nitiimg",
                        imgid: 'nitiImg2',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "asha",
                        imgclass: " relativecls ashaimg",
                        imgid: 'ashaImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "mala",
                        imgclass: "relativecls malaimg",
                        imgid: 'malaImg',
                        imgsrc: ""
                    },

                ]
            }]
        }]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "dial4",
        uppertextblock: [
            {
                textclass: "content3 centertext",
                textdata: data.string.p1text6
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div4",
                    imgclass: "relativecls bgimg1",
                    imgid: 'bgimg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv3",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg1',
                    imgsrc: ""
                },
            ],
        }],
        imageblock1: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "father",
                        imgclass: "relativecls fatherimg",
                        imgid: 'fatherImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "niti2",
                        imgclass: " relativecls nitiimg",
                        imgid: 'nitiImg3',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "asha",
                        imgclass: " relativecls ashaimg",
                        imgid: 'ashaImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "mala1",
                        imgclass: "relativecls malaimg",
                        imgid: 'malaImg1',
                        imgsrc: ""
                    },

                ]
            }]
        }]
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "dial4",
        uppertextblock: [
            {
                textclass: "content3 centertext",
                textdata: data.string.p1text7
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div4",
                    imgclass: "relativecls bgimg1",
                    imgid: 'bgimg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv3",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg1',
                    imgsrc: ""
                },
            ],
        }],
        imageblock1: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "father",
                        imgclass: "relativecls fatherimg",
                        imgid: 'fatherImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "niti2",
                        imgclass: " relativecls nitiimg",
                        imgid: 'nitiImg3',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "asha",
                        imgclass: " relativecls ashaimg",
                        imgid: 'ashaImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "mala1",
                        imgclass: "relativecls malaimg",
                        imgid: 'malaImg1',
                        imgsrc: ""
                    },

                ]
            }]
        }]
    },
    //slid8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "dial5",
        uppertextblock: [
            {
                textclass: "content3 centertext",
                textdata: data.string.p1text8
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div4",
                    imgclass: "relativecls bgimg1",
                    imgid: 'bgimg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv4",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ],
        }],
        imageblock1: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "father",
                        imgclass: "relativecls fatherimg",
                        imgid: 'fatherImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "niti2",
                        imgclass: " relativecls nitiimg",
                        imgid: 'nitiImg3',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "asha",
                        imgclass: " relativecls ashaimg",
                        imgid: 'ashaImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "mala",
                        imgclass: "relativecls malaimg",
                        imgid: 'malaImg',
                        imgsrc: ""
                    },

                ]
            }]
        }]
    },
    //slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass: "dial2",
        uppertextblock: [
            {
                textclass: "content3 centertext",
                textdata: data.string.p1text9
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1",
                    imgclass: "relativecls bgimg1",
                    imgid: 'bgimg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv1",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg1',
                    imgsrc: ""
                },
            ],
        }],
        imageblock1: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "father",
                        imgclass: "relativecls fatherimg",
                        imgid: 'fatherImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "niti2",
                        imgclass: " relativecls nitiimg",
                        imgid: 'nitiImg3',
                        imgsrc: ""
                    },  {
                        imgdiv: "asha",
                        imgclass: " relativecls ashaimg1",
                        imgid: 'ashaImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "asha1 rec1",
                        imgclass: " relativecls ashaimg",
                        imgid: 'ashaImg1',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "mala",
                        imgclass: "relativecls malaimg",
                        imgid: 'malaImg',
                        imgsrc: ""
                    },

                ]
            }]
        }]
    },
    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass:"bg1",
        uppertextblockadditionalclass: "dial1",
        uppertextblock: [
            {
                textclass: "content3 centertext",
                textdata: data.string.p1text10
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1",
                    imgclass: "relativecls bgimg1",
                    imgid: 'bgimg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boxdiv",
                    imgclass: "relativecls boximg",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ],
        }],
        imageblock1: [{
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "father",
                        imgclass: "relativecls fatherimg",
                        imgid: 'fatherImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "niti ",
                        imgclass: " relativecls nitiimg",
                        imgid: 'nitiImg',
                        imgsrc: ""
                    },  {
                        imgdiv: "niti rec1",
                        imgclass: " relativecls nitiimg1",
                        imgid: 'nitiImg1',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "asha",
                        imgclass: " relativecls ashaimg",
                        imgid: 'ashaImg',
                        imgsrc: ""
                    },
                    {
                        imgdiv: "mala",
                        imgclass: "relativecls malaimg",
                        imgid: 'malaImg',
                        imgsrc: ""
                    },

                ]
            }]
        }]
    },
    //slide 11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass:"bg2",
        textblock: [
            {
                textdiv:"infodiv1",
                textclass: "content1 centertext info1",
                textdata: data.string.p1text11
            },
            {
                textdiv:"infodiv2",
                textclass: "content2  centertext info2",
                textdata: data.string.p1text12
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "tipicon",
                    imgclass: "relativecls tipiconimg ",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                },
            ],
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverImg", src: imgpath + "cover-page-2.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handImgImg", src: imgpath + "hand.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coverpageImg", src: imgpath + "school-building.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coverpageImg1", src: imgpath + "school-building01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgimg1", src: imgpath + "school01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath + "buble-27.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg1", src: imgpath + "bubble-19.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fatherImg", src: imgpath + "raj01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nitiImg", src: imgpath1 + "niti.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nitiImg1", src: imgpath1 + "niti.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "nitiImg2", src: imgpath1 + "niti01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nitiImg3", src: imgpath + "niti03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaImg", src: imgpath1 + "asha.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaImg1", src: imgpath1 + "asha.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "malaImg", src: imgpath + "mala02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "malaImg1", src: imgpath + "mala04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tipiconImg", src: "images/notes.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p10.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p11.ogg"},
            {id: "sound_11", src: soundAsset + "s1_p12.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
              sound_player('sound_0' );
              break;

            case 1:
              sound_player('sound_1' );
              break;


            case 2:
              sound_player('sound_2' );
              break;

            case 3:
                sound_player('sound_3');
                $(".dial1,.boxdiv,.niti1").delay(100).animate({"opacity":"1"},100);
                break;
            case 4:
                sound_player('sound_4');
                $(".dial2,.boxdiv1,.asha1").delay(100).animate({"opacity":"1"},100);
                break;
            case 5:
                $(".dial3,.boxdiv2").animate({"opacity":"1"},1000,function(){
                  sound_player('sound_5');
                });
                break;
            case 6:
                $(".dial4,.boxdiv3").delay(100).animate({"opacity":"1"},1000,function(){
                  sound_player('sound_6');
                });
                break;
            case 7:
                $(".dial4,.boxdiv3").delay(100).animate({"opacity":"1"},1000,function(){
                  sound_player('sound_7');
                });
                break;
            case 8:
                $(".dial5,.boxdiv4").delay(100).animate({"opacity":"1"},1000,function(){
                  sound_player('sound_8');
                });
                break;
            case 9:
                $(".img1").css({"width":"129%"});
                $(".dial2").css({
                    "width": "38%",
                    "top": "3%",
                    "left": "20%"
                });
                $(".boxdiv1").css({
                    "width": "43%",
                    "top": "0%",
                    "left": "18%"
                });
                $(".dial2,.boxdiv1,.asha1").delay(1000).animate({"opacity":"1"},100,function(){
                    sound_player('sound_9');
                });
                break;
            case 10:
                $(".bg1").css("width","120%");
                $(".img1").css({"width":"129%"});
                $(".dial1").css({
                            "width": "38%",
                            "top": "7%",
                            "left": "15%"
                            });
                $(".boxdiv").css({
                    "width": "43%",
                    "top": "0%",
                    "left": "12%"
                });
                sound_player('sound_10');

                // $(".niti").delay(1000).animate({"opacity":"0"},1000);
                $(".dial1,.boxdiv,.niti1").delay(100).animate({"opacity":"1"},100)

                break;
            case 11:
                $(".infodiv1,.infodiv2").hide();
                setTimeout(function(){
                    $(".tipicon").addClass("blinkEffect");
                },1000);
                $(".tipicon").click(function(){
                    $(".infodiv1,.infodiv2").show(0,function(){
                        sound_player('sound_11');
                    });
                    $(this).hide();
                    navigationcontroller(countNext, $total_page);
                });
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            $(".rec1").fadeOut(100);
             navigationcontroller(countNext, $total_page) ;
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

});

var imgpath = $ref + "/images/Page04/";
var imgpath1 = $ref + "/images/imagesfordiy/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "back",
        textblock:[
            {
                textdiv:"diytext",
                textclass:"chapter centertext",
                textdata:data.string.diy
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "diy",
                    imgclass: "relativecls diyimg",
                    imgid: 'diyImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "back",
        textblock:[
            {
                textdiv:"text1",
                textclass:"content centertext",
                textdata:data.string.p5text8
            },
            {
                textdiv:"text2",
                textclass:"content2 centertext",
                textdata:data.string.p5text9
            },
            {
                textdiv:"emptydiv clickform",
                textclass:"centertext",
                textdata:""
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "healthpost",
                    imgclass: "relativecls healthpostimg",
                    imgid: 'healthpostImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "handicon clickform",
                    imgclass: "relativecls handiconimg ",
                    imgid: 'handiconImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "diybg",
        coverboardadditionalclass:"fontfamilyseparate",
        textblock:[
            {
                textdiv:"emptydiv1",
                textclass:"centertext",
                textdata:""
            },
            {
                textdiv:"topic1",
                textclass:"content2 centertext",
                textdata:data.string.diy1
            },
            {
                textdiv:"topic2",
                textclass:"content2 centertext",
                textdata:data.string.diy2
            },
            {
                textdiv:"datevisit",
                textclass:"content3 centertext",
                textdata:data.string.diy3,
                textbox:[
                    {
                        textboxcls:"box1"
                    }
                ]
            },
            {
                textdiv:"patientname",
                textclass:"content3 centertext",
                textdata:data.string.diy4,
                textbox:[
                    {
                        textboxcls:"box2"
                    }
                ]
            },
            {
                textdiv:"gender",
                textclass:"content3 centertext",
                textdata:data.string.gender,
                radiodiv:"radiodiv",
                radiobtn:[
                    {
                        radioopt:data.string.gopt1,
                        radioname:"gender",
                        radiocls:"content3"
                    },
                    {
                        radioopt:data.string.gopt2,
                        radioname:"gender",
                        radiocls:"content3"
                    },
                    {
                        radioopt:data.string.gopt3,
                        radioname:"gender",
                        radiocls:"content3"
                    }
                ]
            },
            {
                textdiv:"age",
                textclass:"content3 centertext",
                textdata:data.string.diy6,
                textbox:[
                    {
                        textboxcls:"box3"
                    }
                ]
            },
            {
                textdiv:"address",
                textclass:"content3 centertext",
                textdata:data.string.diy7,
                textbox:[
                    {
                        textboxcls:"box4"
                    }
                ]
            },
            {
                textdiv:"reason",
                textclass:"content3 centertext",
                textdata:data.string.reason,
                checkbox:[{
                    chdiv:"checkboxdiv",
                    chdata: [
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt1,
                            chname:"reasondr",
                            newline:true
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt2,
                            chname:"reasondr"
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt3,
                            chname:"reasondr",
                            newlineend:true

                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt4,
                            chname:"reasondr",
                            newline:true
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt5,
                            chname:"reasondr"
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt6,
                            chname:"reasondr",
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.other,
                            chname:"reasondr",
                            newlineend:true
                        }
                    ]
                }
                ]
            },
            {
                textdiv:"vaccine",
                textclass:"content3 centertext",
                textdata:data.string.vaccination,
                checkbox:[{
                    chdiv:"checkboxdiv1",
                    chdata: [
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt1,
                            chname:"reasondr1",
                            newline:true
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt2,
                            chname:"reasondr1"
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt3,
                            chname:"reasondr1",
                            newlineend:true

                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt4,
                            chname:"reasondr1",
                            newline:true
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt5,
                            chname:"reasondr1",
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.other,
                            chname:"reasondr1",
                            newlineend:true
                        }
                    ]
                }
                ]
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "healthpostlogo",
                    imgclass: "relativecls healthpostlogoimg",
                    imgid: 'healthpostlogoImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "diybg",
        coverboardadditionalclass:"fontfamilyseparate",
        textblock:[
            {
                textdiv:"info",
                textclass:"content2 centertext",
                textdata:data.string.infofilled
            },
            {
                textdiv:"emptydiv1",
                textclass:"centertext",
                textdata:""
            },
            {
                textdiv:"topic1",
                textclass:"content2 centertext",
                textdata:data.string.diy1
            },
            {
                textdiv:"topic2",
                textclass:"content2 centertext",
                textdata:data.string.diy2
            },
            {
                textdiv:"datevisit",
                textclass:"content3 centertext",
                textdata:data.string.diy3,
                textbox:[
                    {
                        textboxcls:"box1 avoid-clicks"
                    }
                ]
            },
            {
                textdiv:"patientname",
                textclass:"content3 centertext",
                textdata:data.string.diy4,
                textbox:[
                    {
                        textboxcls:"box2 avoid-clicks"
                    }
                ]
            },
            {
                textdiv:"gender",
                textclass:"content3 centertext",
                textdata:data.string.gender,
                radiodiv:"radiodiv avoid-clicks",
                radiobtn:[
                    {
                        radioopt:data.string.gopt1,
                        radioname:"gender",
                        radiocls:"content3"
                    },
                    {
                        radioopt:data.string.gopt2,
                        radioname:"gender",
                        radiocls:"content3"
                    },
                    {
                        radioopt:data.string.gopt3,
                        radioname:"gender",
                        radiocls:"content3"
                    }
                ]
            },
            {
                textdiv:"age",
                textclass:"content3 centertext",
                textdata:data.string.diy6,
                textbox:[
                    {
                        textboxcls:"box3 avoid-clicks"
                    }
                ]
            },
            {
                textdiv:"address",
                textclass:"content3 centertext",
                textdata:data.string.diy7,
                textbox:[
                    {
                        textboxcls:"box4 avoid-clicks"
                    }
                ]
            },
            {
                textdiv:"reason",
                textclass:"content3 centertext",
                textdata:data.string.reason,
                checkbox:[{
                    chdiv:"checkboxdiv avoid-clicks",
                    chdata: [
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt1,
                            chname:"reasondr",
                            newline:true
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt2,
                            chname:"reasondr"
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt3,
                            chname:"reasondr",
                            newlineend:true

                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt4,
                            chname:"reasondr",
                            newline:true
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt5,
                            chname:"reasondr"
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.ropt6,
                            chname:"reasondr",
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.other,
                            chname:"reasondr",
                            newlineend:true
                        }
                    ]
                }
                ]
            },
            {
                textdiv:"vaccine",
                textclass:"content3 centertext",
                textdata:data.string.vaccination,
                checkbox:[{
                    chdiv:"checkboxdiv1 avoid-clicks",
                    chdata: [
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt1,
                            chname:"reasondr1",
                            newline:true
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt2,
                            chname:"reasondr1"
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt3,
                            chname:"reasondr1",
                            newlineend:true

                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt4,
                            chname:"reasondr1",
                            newline:true
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.vopt5,
                            chname:"reasondr1",
                        },
                        {
                            chboxcls: "content3",
                            chboxopt: data.string.other,
                            chname:"reasondr1",
                            newlineend:true
                        }
                    ]
                }
                ]
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "healthpostlogo",
                    imgclass: "relativecls healthpostlogoimg",
                    imgid: 'healthpostlogoImg',
                    imgsrc: ""
                }
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var dataFilled = {};
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverImg", src: imgpath + "school-building02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "recept1Img", src: imgpath + "reception01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "recept2Img", src: imgpath + "reception02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ladyImg", src: imgpath + "talking_phone01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath + "buble-27.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg1", src: imgpath + "namas-07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "receptionImg", src: imgpath + "talking-phone.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "reception1Img", src: imgpath + "talking-phone.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lady1Img", src: imgpath + "talking_phone01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "backgroundImg", src: imgpath + "bg_new01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "background1Img", src: imgpath + "bg_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ranuImg", src: imgpath + "ranu.png", type: createjs.AbstractLoader.IMAGE},
            {id: "malaImg", src: imgpath + "mala-at-home.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mala1Img", src: imgpath + "mala_at_home.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "recep_talking-24.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ranu2Img", src: imgpath + "happy_girl.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "ranu1Img", src: imgpath + "happy_girl01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyImg", src: imgpath1 + "a_08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "healthpostImg", src: imgpath1 + "healthpost.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handiconImg", src: imgpath1+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "healthpostlogoImg", src: imgpath1+"health_post_logo.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s6_p2_1.ogg"},
            {id: "sound_1", src: soundAsset + "s6_p2_2.ogg"},
            {id: "sound_2", src: soundAsset + "s6_p4.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                play_diy_audio();
                navigationcontroller(countNext, $total_page);
                break;
            case 1:

                createjs.Sound.stop();
                var current_sound1 = createjs.Sound.play('sound_0');
                current_sound1.play();
                current_sound1.on('complete', function () {
                    createjs.Sound.stop();
                    var current_sound2 = createjs.Sound.play('sound_1');
                    current_sound2.play();
                    current_sound2.on('complete', function () {
                    navigationcontroller(countNext, $total_page);
                  });
                });
                $(".clickform").click(function(){
                    $nextBtn.click();
                })
                break;
            case 2:
                $(".box1").keyup(function(){
                    dataFilled["dateOfVisit"]=$(this).val();
                });
                $(".box2").keyup(function(){
                    dataFilled["nameOfPatient"]=$(this).val();
                });
                $(".radiodiv").click(function(){
                    dataFilled["gender"]=$('input[name="gender"]:checked').val();
                });
                $(".box3").keyup(function(){
                    dataFilled["age"]=$(this).val();
                });
                $(".box4").keyup(function(){
                    dataFilled["address"]=$(this).val();
                });

                $(".checkboxdiv").click(function(){
                    var chkArray =[];
                    $("input[name='reasondr']:checked").each(function() {
                        chkArray.push($(this).val());
                    });
                    dataFilled["reasondr"]= chkArray.join(',') ;
                });
                $(".checkboxdiv1").click(function(){
                    var chkArray =[];
                    $("input[name='reasondr1']:checked").each(function() {
                        chkArray.push($(this).val());
                    });
                    dataFilled["reasondr1"]= chkArray.join(',') ;
                });
                navigationcontroller(countNext, $total_page);
                break;
            case 3:
                sound_player('sound_2');
                $(".coverboardfull div").not(".info").css("opacity","0");
                $("input.coverboardfull,input[type='text']").css("opacity","0");
                $(".info").delay(3000).animate({"opacity":"0"},2000,"linear");
                $(".coverboardfull div,input[type='text']").not(".info").delay(3000).animate({"opacity":"1"},2000,"linear");
                $("input.coverboardfull").delay(3000).css("opacity","0.5");

                $(".box1").val(dataFilled["dateOfVisit"]);
                $(".box2").val(dataFilled["nameOfPatient"]);
                $("input[name='gender'][value='"+dataFilled["gender"]+"']").attr("checked",true);
                $(".box3").val(dataFilled["age"]);
                $(".box4").val(dataFilled["address"]);
                var dataarray=dataFilled["reasondr"]?dataFilled["reasondr"].toString().split(","):'';
                $("input[name='reasondr']").val(dataarray);
                 dataarray=dataFilled["reasondr1"]?dataFilled["reasondr1"].toString().split(","):'';
                $("input[name='reasondr1']").val(dataarray);
                navigationcontroller(countNext, $total_page);
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller(countNext, $total_page);
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

});

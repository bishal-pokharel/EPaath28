var imgpath = $ref + "/images/Fillingform/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "handicon clickform",
                    imgclass: "relativecls handiconimg ",
                    imgid: 'handiconImg',
                    imgsrc: ""
                },
            ],
        }],
        textblock:[
            {
                textdiv:"div1",
                textclass:"content centertext",
                textdata:data.string.p4text1
            },
            {
                textdiv:"div2 slideL",
                textclass:"content centertext",
                textdata:data.string.p4text2
            },
            {
                textdiv:"div3 clickform",
                textclass:"centertext",
                textdata:""
            }
        ]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "formmainfirst",
                    imgclass: "relativecls formmainimg ",
                    imgid: 'formmainImg',
                    imgsrc: ""
                },
            ],
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "formmain",
                    imgclass: "relativecls formmainimg ",
                    imgid: 'formmainImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "formname clickform",
                    imgclass: "relativecls formnameimg ",
                    imgid: 'formnameImg',
                    imgsrc: ""
                },
            ],
        }]
    },
    // slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "formmain1",
                    imgclass: "relativecls formmain1img ",
                    imgid: 'formmain1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "blankname",
                    imgclass: "relativecls blanknameimg ",
                    imgid: 'blanknameImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "blankgender",
                    imgclass: "relativecls blankgenderimg ",
                    imgid: 'blankgenderImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "blankdob",
                    imgclass: "relativecls blankdobimg ",
                    imgid: 'blankdobImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "fullname",
                    imgclass: "relativecls fullnameimg ",
                    imgid: 'fullnameImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "fulldob",
                    imgclass: "relativecls fulldobimg ",
                    imgid: 'fulldobImg',
                    imgsrc: ""
                }

            ],
        }],
        textblock:[
            {
                textdiv:"divblk1 hidediv",
                textclass:"content2 centertext",
                textdata:data.string.p4text3
            },
            {
                textdiv:"divblk2 hidediv",
                textclass:"content2 centertext",
                textdata:data.string.p4text4
            },
            {
                textdiv:"divblk3 hidediv",
                textclass:"content3 centertext",
                textdata:data.string.p4text5
            },
            {
                textdiv:"nameinfo",
                textclass:"content2 centertext",
                textdata:data.string.nameinfo
            },
            {
                textdiv:"fullgender",
                textclass:"content2 centertext",
                textdata:"",
            },
            {
                textdiv:"genderinfo",
                textclass:"content2 centertext",
                textdata:data.string.genderinfo
            },
            {
                textdiv:"dobinfo",
                textclass:"content2 centertext",
                textdata:data.string.dobinfo
            },
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "formwithphoto",
                    imgclass: "relativecls formwithphotoimg ",
                    imgid: 'formwithphotoImg',
                    imgsrc: ""
                },
            ],
        }],
        textblock:[
            {
                textdiv:"photoinfo zoomInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p4text6
            },
            {
                textdiv:"fullgender1",
                textclass:"centertext",
                textdata:""
            },
            {
                textdiv:"blankdiv",
                textclass:"centertext",
                textdata:""
            },
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "formwithphoto1",
                    imgclass: "relativecls formwithphotoimg ",
                    imgid: 'formwithphoto1Img',
                    imgsrc: ""
                },
            ],
        }],
        textblock:[
            {
                textdiv:"nextinfo zoomInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p4text7
            }
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "otherdetailsblank",
                    imgclass: "relativecls otherdetailsblankimg ",
                    imgid: 'otherdetailsblankImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "fathernameborder",
                    imgclass: "relativecls fathernameborderimg ",
                    imgid: 'fathernameborderImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filled",
                    imgclass: "relativecls fathernameborder1img ",
                    imgid: 'fathernameborder1Img',
                    imgsrc: ""
                },
            ],
        }],
        textblock:[
            {
                textdiv:"parentsinfo zoomInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p4text8
            }
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "otherdetailsblank",
                    imgclass: "relativecls otherdetailsblankimg ",
                    imgid: 'otherdetailsblankImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filled",
                    imgclass: "relativecls fathernameborder1img ",
                    imgid: 'fathernameborder1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "addressborder",
                    imgclass: "relativecls addressborderimg ",
                    imgid: 'addressborderImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledaddress",
                    imgclass: "relativecls addressborder1img ",
                    imgid: 'addressborder1Img',
                    imgsrc: ""
                },
            ],
        }],
        textblock:[
            {
                textdiv:"addressinfo zoomInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p4text9
            }
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "otherdetailsblank",
                    imgclass: "relativecls otherdetailsblankimg ",
                    imgid: 'otherdetailsblankImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filled",
                    imgclass: "relativecls fathernameborder1img ",
                    imgid: 'fathernameborder1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledaddress",
                    imgclass: "relativecls addressborder1img ",
                    imgid: 'addressborder1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "gradeborder",
                    imgclass: "relativecls gradeborderimg ",
                    imgid: 'gradeborderImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledgrade",
                    imgclass: "relativecls filledgradeimg ",
                    imgid: 'filledgradeImg',
                    imgsrc: ""
                },
            ],
        }],
        textblock:[
            {
                textdiv:"gradeinfo zoomInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p4text10
            }
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "otherdetailsblank",
                    imgclass: "relativecls otherdetailsblankimg ",
                    imgid: 'otherdetailsblankImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filled",
                    imgclass: "relativecls fathernameborder1img ",
                    imgid: 'fathernameborder1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledaddress",
                    imgclass: "relativecls addressborder1img ",
                    imgid: 'addressborder1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledgrade",
                    imgclass: "relativecls filledgradeimg ",
                    imgid: 'filledgradeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "telephoneborder",
                    imgclass: "relativecls telephoneimg ",
                    imgid: 'telephoneImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledtelephone",
                    imgclass: "relativecls filledtelephoneimg ",
                    imgid: 'filledtelephoneImg',
                    imgsrc: ""
                },
            ],
        }],
        textblock:[
            {
                textdiv:"telephoneinfo zoomInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p4text11
            }
        ]
    },
    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "otherdetailsblank",
                    imgclass: "relativecls otherdetailsblankimg ",
                    imgid: 'otherdetailsblankImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filled",
                    imgclass: "relativecls fathernameborder1img ",
                    imgid: 'fathernameborder1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledaddress",
                    imgclass: "relativecls addressborder1img ",
                    imgid: 'addressborder1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledgrade",
                    imgclass: "relativecls filledgradeimg ",
                    imgid: 'filledgradeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filledtelephone",
                    imgclass: "relativecls filledtelephoneimg ",
                    imgid: 'filledtelephoneImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dateborder",
                    imgclass: "relativecls dateimg ",
                    imgid: 'dateImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "filleddate",
                    imgclass: "relativecls filledteledateimg ",
                    imgid: 'filledteledateImg',
                    imgsrc: ""
                },
            ],
        }],
        textblock:[
            {
                textdiv:"dateinfo zoomInEffect",
                textclass:"content2 centertext",
                textdata:data.string.p4text12
            }
        ]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // {id: "coverImg", src: imgpath + "cover-page-2.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handiconImg", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "formmainImg", src: imgpath+"form_main.png", type: createjs.AbstractLoader.IMAGE},
            {id: "formmain1Img", src: imgpath+"form_main01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "formnameImg", src: imgpath+"name.png", type: createjs.AbstractLoader.IMAGE},
            {id: "blanknameImg", src: imgpath+"ranu-lama01_a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "blankdobImg", src: imgpath+"date-of-birth01_a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "blankgenderImg", src: imgpath+"gender_a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fullnameImg", src: imgpath+"ranu-lama.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fulldobImg", src: imgpath+"date-of-birth.png", type: createjs.AbstractLoader.IMAGE},
            {id: "formwithphotoImg", src: imgpath+"form_with_photo_02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "formwithphoto1Img", src: imgpath+"form_with_photo_01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "otherdetailsblankImg", src: imgpath+"otherdetails02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fathernameborderImg", src: imgpath+"father_name.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fathernameborder1Img", src: imgpath+"father_name_b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "addressborderImg", src: imgpath+"grade6_b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "addressborder1Img", src: imgpath+"grade6_a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "filledgradeImg", src: imgpath+"grade07_a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "gradeborderImg", src: imgpath+"grade07_b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "telephoneImg", src: imgpath+"address01_b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "filledtelephoneImg", src: imgpath+"address01_a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dateImg", src: imgpath+"signature_b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "filledteledateImg", src: imgpath+"signature_a.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_0", src: soundAsset + "s4_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s4_p4_1.ogg"},
            {id: "sound_3", src: soundAsset + "s4_p4_2.ogg"},
            {id: "sound_4", src: soundAsset + "s4_p4_3.ogg"},
            {id: "sound_5", src: soundAsset + "s4_p4_date_of_birth.ogg"},
            {id: "sound_6", src: soundAsset + "s4_p4_name_of_applicant.ogg"},
            {id: "sound_7", src: soundAsset + "s4_p5.ogg"},
            {id: "sound_8", src: soundAsset + "s4_p6.ogg"},
            {id: "sound_9", src: soundAsset + "s4_p7.ogg"},
            {id: "sound_10", src: soundAsset + "s4_p8.ogg"},
            {id: "sound_11", src: soundAsset + "s4_p9.ogg"},
            {id: "sound_12", src: soundAsset + "s4_p10.ogg"},
            {id: "sound_13", src: soundAsset + "s4_p11.ogg"},
            {id: "sound_14", src: soundAsset + "s4_p4_gender.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player('sound_0');
                $(".clickform").click(function(){
                    $nextBtn.click();
                });
                break;
            case 2:
                $(".clickform").click(function(){
                    $nextBtn.click();
                });
                break;
            case 3:
                createjs.Sound.stop();
                var current_sound1 = createjs.Sound.play('sound_2');
                current_sound1.play();
                current_sound1.on('complete', function () {
                  var current_sound2 = createjs.Sound.play('sound_3');
                  current_sound2.play();
                  current_sound2.on('complete', function () {
                    var current_sound3 = createjs.Sound.play('sound_4');
                      current_sound3.play();
                      current_sound3.on('complete', function () {
                      navigationcontroller(countNext, $total_page);
                    });
                  });
                });
                $(".nameInfo,.fullname,.fullgender,.genderinfo,.dobInfo,.fulldob").hide();
                var countVal = 0;
                $(".blankname").click(function(){
                    sound_player1('sound_6');
                   countVal++;
                    $(".nameInfo").show();
                    $(".fullname").fadeIn(700);
                    $(".genderinfo,.dobinfo,.hidediv").hide();
                    $(this).hide();
                });
                $(".blankgender").click(function(){
                    sound_player1('sound_14');
                    countVal++;
                    $(".fullgender").show();
                    $(".genderinfo").fadeIn(700);
                    $(".nameinfo,.dobinfo,.hidediv").hide();
                    $(this).css("pointer-events","none");
                });
                $(".blankdob").click(function(){
                    sound_player1('sound_5');
                    countVal++;
                    $(".dobInfo").show();
                    $(".fulldob").fadeIn(700);
                    $(".genderinfo,.nameinfo,.hidediv").hide();
                    $(this).hide();
                });
                    navigationcontroller(countNext, $total_page);
                break;
                case 4:
                sound_player('sound_7');
                break;

                case 5:
                sound_player('sound_8');
                break;
            case 6:
                $(".parentsinfo,.filled").hide();
                $(".fathernameborder").click(function(){
                    sound_player('sound_9');
                    $(".parentsinfo,.filled").show();
                    $(this).hide();
                });
                break;
            case 7:
                $(".addressinfo,.filledaddress").hide();
                $(".addressborder").click(function(){
                    sound_player('sound_10');
                    $(".addressinfo,.filledaddress").show();
                    $(this).hide();
                });
                break;
            case 8:
                $(".gradeinfo,.filledgrade").hide();
                $(".gradeborder").click(function(){
                    sound_player('sound_11');
                    $(".gradeinfo,.filledgrade").show();
                    $(this).hide();
                });
                break;
            case 9:
                $(".telephoneinfo,.filledtelephone").hide();
                $(".telephoneborder").click(function(){
                    sound_player('sound_12');
                    $(".telephoneinfo,.filledtelephone").show();
                    $(this).hide();
                });
                break;
            case 10:
                $(".dateinfo,.filleddate").hide();
                $(".dateborder").click(function(){
                    sound_player('sound_13');
                    $(".dateinfo,.filleddate").show();
                    $(this).hide();
                });
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
          navigationcontroller(countNext, $total_page);
        });
    }

    function sound_player1(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

});

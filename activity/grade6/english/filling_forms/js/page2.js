var soundAsset = $ref+"/sounds/";
var imgpath = $ref+"/images/Page03/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv",
        uppertextblock: [
            {
                textclass: "diyfont centertext",
                textdata: data.string.p2text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha1",
                    imgclass: "relativecls ashaimg",
                    imgid: 'ashaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha1 rec1",
                    imgclass: "relativecls asha1img",
                    imgid: 'asha1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "mala",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "receptionist",
                    imgclass: "relativecls receptionistimg",
                    imgid: 'receptionistImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1",
                    imgclass: "relativecls dial1img",
                    imgid: 'dial1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv1",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.p2text2
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls ashaimg",
                    imgid: 'ashaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mala",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "receptionist1",
                    imgclass: "relativecls receptionistimg",
                    imgid: 'receptionist1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "receptionist1 rec1",
                    imgclass: "relativecls receptionist1img",
                    imgid: 'receptionist2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial2",
                    imgclass: "relativecls dial1img",
                    imgid: 'dial2Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv1",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.p2text3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls ashaimg",
                    imgid: 'ashaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mala",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "receptionist1",
                    imgclass: "relativecls receptionistimg",
                    imgid: 'receptionist1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "receptionist1 rec1",
                    imgclass: "relativecls receptionist1img",
                    imgid: 'receptionist2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial3",
                    imgclass: "relativecls dial1img",
                    imgid: 'dial2Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.p2text4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashafront",
                    imgclass: "relativecls ashaimg",
                    imgid: 'ashaformImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashafront rec1",
                    imgclass: "relativecls asha1img",
                    imgid: 'ashaform1Img',
                    imgsrc: ""
                },

                {
                    imgdiv: "mala",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "receptionist",
                    imgclass: "relativecls receptionistimg",
                    imgid: 'receptionistImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial4",
                    imgclass: "relativecls dial1img",
                    imgid: 'dial1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv2",
        uppertextblock: [
            {
                textclass: "diyfont centertext",
                textdata: data.string.p2text5
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "father",
                    imgclass: "relativecls fatherimg",
                    imgid: 'fatherImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls nitiimg",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashaform",
                    imgclass: "relativecls ashaformimg",
                    imgid: 'ashaformImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashaform1 rec1",
                    imgclass: "relativecls ashaform1img",
                    imgid: 'ashaform1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "malascl",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial5",
                    imgclass: "relativecls dial1img",
                    imgid: 'dial2Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "objdiv",
        uppertextblock: [
            {
                textclass: "diyfont centertext",
                textdata: data.string.p2text6
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "father",
                    imgclass: "relativecls fatherimg",
                    imgid: 'fatherImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti1",
                    imgclass: "relativecls nitiimg",
                    imgid: 'niti1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti1 rec1",
                    imgclass: "relativecls niti2img",
                    imgid: 'niti2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashaform",
                    imgclass: "relativecls ashaformimg",
                    imgid: 'ashaformImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "malascl",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1",
                    imgclass: "relativecls dial1img",
                    imgid: 'dial1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "father",
                    imgclass: "relativecls fatherimg",
                    imgid: 'fatherImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti1",
                    imgclass: "relativecls nitiimg",
                    imgid: 'niti1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashaform",
                    imgclass: "relativecls ashaformimg",
                    imgid: 'ashaformImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "malascl",
                    imgclass: "relativecls malaimg",
                    imgid: 'malaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbye1",
                    imgclass: "relativecls dialimg",
                    imgid: 'dial1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbye2",
                    imgclass: "relativecls dial1img",
                    imgid: 'dial2Img',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"div1",
                textclass:"content2 centertext",
                textdata:data.string.p2text7
            },
            {
                textdiv:"div2",
                textclass:"content2 centertext",
                textdata:data.string.p2text8
            }
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass:"bg2",
        textblock: [
            {
                textdiv:"infodiv1",
                textclass: "content1 centertext info1",
                textdata: data.string.p2text9
            },
            {
                textdiv:"infodiv2",
                textclass: "content2  centertext info2",
                textdata: data.string.p2text10
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "tipicon",
                    imgclass: "relativecls tipiconimg ",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                },
            ],
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverImg", src: imgpath + "bg_reception.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coverImg1", src: imgpath + "school-building02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaImg", src: imgpath + "asha.png", type: createjs.AbstractLoader.IMAGE},
            {id: "receptionistImg", src: imgpath + "receptionist03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "receptionist1Img", src: imgpath + "receptionist01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "receptionist2Img", src: imgpath + "rasika.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "malaImg", src: imgpath + "mala05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "asha1Img", src: imgpath + "asha.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src: imgpath + "buble-27.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial2Img", src: imgpath + "bubble-19.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fatherImg", src: imgpath + "raj03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nitiImg", src: imgpath + "niti03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "niti1Img", src: imgpath + "niti.png", type: createjs.AbstractLoader.IMAGE},
            {id: "niti2Img", src: imgpath + "niti.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaformImg", src: imgpath + "asha02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaform1Img", src: imgpath + "asha02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "tipiconImg", src: "images/notes.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s2_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s2_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s2_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s2_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s2_p7_1.ogg"},
            {id: "sound_7", src: soundAsset + "s2_p7_2.ogg"},
            {id: "sound_8", src: soundAsset + "s2_p8.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                $(".asha").hide();
                sound_player('sound_0');
                break;
                case 1:
                    sound_player('sound_1');
                    break;
            case 6:
            createjs.Sound.stop();
            var current_sound1 = createjs.Sound.play('sound_6');
            current_sound1.play();
            current_sound1.on('complete', function () {
              var current_sound2 = createjs.Sound.play('sound_7');
              current_sound2.on('complete', function () {
              navigationcontroller(countNext, $total_page);
              });
            });
            break;
            case 7:
                $(".infodiv1,.infodiv2").hide();
                setTimeout(function(){
                    $(".tipicon").addClass("blinkEffect");
                },1000);
                $(".tipicon").click(function(){
                    sound_player('sound_8');
                    $(".infodiv1,.infodiv2").show();
                    $(this).hide();
                    navigationcontroller(countNext, $total_page);
                });
                break;
            default:
                sound_player('sound_'+countNext);
                break;
        }
    }


    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            $(".rec1").fadeOut(100)
          navigationcontroller(countNext, $total_page);
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/orgSounds/";

var content1=[
	// slide1
	{
		contentblockadditionalclass: "first",
		extratextblock:[{
			textclass: "title",
			textdata: data.string.e1s1
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				},
				{
					imgclass : "adj draggable img1",
					imgid : 'hot',
				},
				{
					imgclass : "com draggable img2",
					imgid : 'hotter',
				},
				{
					imgclass : "sup draggable img3",
					imgid : 'hottest',
				}
			]
			},
			{
			imgblkdivclass:"drpclass",
			imagestoshow:[
				{
					imgclass : "droppable cart1",
					imgid : 'cart_adjective',
				},
				{
					imgclass : "droppable cart2",
					imgid : 'cart_comparative',
				},
				{
					imgclass : "droppable cart3",
					imgid : 'cart_superlative',
				},
				]
			}
		],
	},
	// slide2
	{
		contentblockadditionalclass: "first",
		extratextblock:[{
			textclass: "title",
			textdata: data.string.e1s1
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				},
				{
					imgclass : "adj draggable img1",
					imgid : 'long',
				},
				{
					imgclass : "com draggable img2",
					imgid : 'longer',
				},
				{
					imgclass : "sup draggable img3",
					imgid : 'longest',
				}
			]
			},
			{
			imgblkdivclass:"drpclass",
			imagestoshow:[
				{
					imgclass : "droppable cart1",
					imgid : 'cart_adjective',
				},
				{
					imgclass : "droppable cart2",
					imgid : 'cart_comparative',
				},
				{
					imgclass : "droppable cart3",
					imgid : 'cart_superlative',
				},
				]
			}
		],
	},
	// slide3
	{
		contentblockadditionalclass: "first",
		extratextblock:[{
			textclass: "title",
			textdata: data.string.e1s1
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				},
				{
					imgclass : "adj draggable img1",
					imgid : 'thick',
				},
				{
					imgclass : "com draggable img2",
					imgid : 'thicker',
				},
				{
					imgclass : "sup draggable img3",
					imgid : 'thickest',
				}
			]
			},
			{
			imgblkdivclass:"drpclass",
			imagestoshow:[
				{
					imgclass : "droppable cart1",
					imgid : 'cart_adjective',
				},
				{
					imgclass : "droppable cart2",
					imgid : 'cart_comparative',
				},
				{
					imgclass : "droppable cart3",
					imgid : 'cart_superlative',
				},
				]
			}
		],
	},
	// slide4
	{
		contentblockadditionalclass: "first",
		extratextblock:[{
			textclass: "title",
			textdata: data.string.e1s1
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				},
				{
					imgclass : "adj draggable img1",
					imgid : 'many',
				},
				{
					imgclass : "com draggable img2",
					imgid : 'more',
				},
				{
					imgclass : "sup draggable img3",
					imgid : 'most',
				}
			]
			},
			{
			imgblkdivclass:"drpclass",
			imagestoshow:[
				{
					imgclass : "droppable cart1",
					imgid : 'cart_adjective',
				},
				{
					imgclass : "droppable cart2",
					imgid : 'cart_comparative',
				},
				{
					imgclass : "droppable cart3",
					imgid : 'cart_superlative',
				},
				]
			}
		],
	},
	// slide5
	{
		contentblockadditionalclass: "first",
		extratextblock:[{
			textclass: "title",
			textdata: data.string.e1s1
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				},
				{
					imgclass : "adj draggable img1",
					imgid : 'high',
				},
				{
					imgclass : "com draggable img2",
					imgid : 'higher',
				},
				{
					imgclass : "sup draggable img3",
					imgid : 'highest',
				}
			]
			},
			{
			imgblkdivclass:"drpclass",
			imagestoshow:[
				{
					imgclass : "droppable cart1",
					imgid : 'cart_adjective',
				},
				{
					imgclass : "droppable cart2",
					imgid : 'cart_comparative',
				},
				{
					imgclass : "droppable cart3",
					imgid : 'cart_superlative',
				},
				]
			}
		],
	},
];
var content2=[
	//slide6
	{
		extratextblock:[
		{
			textdata: data.string.e1s5,
			textclass: "title1",
		},
		{
			textdata: data.string.e1s6,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s11,
			textclass: "choice1",
		},
		{
			textdata: data.string.e1s12,
			textclass: "choice2 correctans",
		},
		{
			textdata: data.string.e1s13,
			textclass: "choice3",
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				}
			]
			},
			{
			imgblkdivclass:"mcqclass",
			imagestoshow : [
				{
					imgclass : "big",
					imgid : 'big',
				},
				{
					imgclass : "bigger",
					imgid : 'bigger',
				}]
			}]
	},
	//slide7
	{
		extratextblock:[
		{
			textdata: data.string.e1s5,
			textclass: "title1",
		},
		{
			textdata: data.string.e1s7,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s16,
			textclass: "choice1 correctans",
		},
		{
			textdata: data.string.e1s15,
			textclass: "choice2",
		},
		{
			textdata: data.string.e1s14,
			textclass: "choice3",
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				}
			]
			},
			{
			imgblkdivclass:"mcqclass",
			imagestoshow : [
				{
					imgclass : "ostrich",
					imgid : 'ostrich',
				}]
			}]
	},
	//slide8
	{
		extratextblock:[
		{
			textdata: data.string.e1s5,
			textclass: "title1",
		},
		{
			textdata: data.string.e1s8,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s17,
			textclass: "choice1",
		},
		{
			textdata: data.string.e1s18,
			textclass: "choice2 correctans",
		},
		{
			textdata: data.string.e1s19,
			textclass: "choice3",
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				}
			]
			},
			{
			imgblkdivclass:"mcqclass",
			imagestoshow : [
				{
					imgclass : "big",
					imgid : 'small',
				},
				{
					imgclass : "bigger",
					imgid : 'smaller',
				}]
			}]
	},
	//slide9
	{
		extratextblock:[
		{
			textdata: data.string.e1s5,
			textclass: "title1",
		},
		{
			textdata: data.string.e1s9,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s20,
			textclass: "choice1",
		},
		{
			textdata: data.string.e1s22,
			textclass: "choice2",
		},
		{
			textdata: data.string.e1s21,
			textclass: "choice3 correctans",
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				}
			]
			},
			{
			imgblkdivclass:"mcqclass",
			imagestoshow : [
				{
					imgclass : "girls",
					imgid : 'smart',
				},
				{
					imgclass : "girls1",
					imgid : 'smarter',
				}]
			}]
	},
	//slide10
	{
		extratextblock:[
		{
			textdata: data.string.e1s5,
			textclass: "title1",
		},
		{
			textdata: data.string.e1s10,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s25,
			textclass: "choice1 correctans",
		},
		{
			textdata: data.string.e1s24,
			textclass: "choice2",
		},
		{
			textdata: data.string.e1s23,
			textclass: "choice3",
		}],
		imageblock:[{
			imgblkdivclass:"drgclass",
			imagestoshow : [
				{
					imgclass : "bg_main",
					imgid : 'bg_exercise',
				}
			]
			},
			{
			imgblkdivclass:"mcqclass",
			imagestoshow : [
				{
					imgclass : "big",
					imgid : 'faster',
				},
				{
					imgclass : "bigger",
					imgid : 'fastest',
				}]
			}]
	},
];
content1.shufflearray();
content2.shufflearray();
content = content1.concat(content2);

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new NumberTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg_exercise", src: imgpath+"bg_execise.png", type: createjs.AbstractLoader.IMAGE},
			{id: "big", src: imgpath+"big.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bigger", src: imgpath+"bigger.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cart_adjective", src: imgpath+"cart_adjective.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cart_comparative", src: imgpath+"cart_comparative.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cart_superlative", src: imgpath+"cart_superlative.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fast", src: imgpath+"fast.png", type: createjs.AbstractLoader.IMAGE},
			{id: "faster", src: imgpath+"faster.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fastest", src: imgpath+"fastest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "high", src: imgpath+"high.png", type: createjs.AbstractLoader.IMAGE},
			{id: "higher", src: imgpath+"higher.png", type: createjs.AbstractLoader.IMAGE},
			{id: "highest", src: imgpath+"highest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hot", src: imgpath+"hot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hotter", src: imgpath+"hotter.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hottest", src: imgpath+"hottest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "long", src: imgpath+"long.png", type: createjs.AbstractLoader.IMAGE},
			{id: "longer", src: imgpath+"longer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "longest", src: imgpath+"longest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "many", src: imgpath+"many.png", type: createjs.AbstractLoader.IMAGE},
			{id: "more", src: imgpath+"more.png", type: createjs.AbstractLoader.IMAGE},
			{id: "most", src: imgpath+"most.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smart", src: imgpath+"smart.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smarter", src: imgpath+"smarter.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thick", src: imgpath+"thick.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thicker", src: imgpath+"thicker.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thickest", src: imgpath+"thickest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ostrich", src: imgpath+"ostrich.png", type: createjs.AbstractLoader.IMAGE},
			{id: "small", src: imgpath+"small.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smaller", src: imgpath+"smaller.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: $ref + "/images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: $ref + "/images/incorrect.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "ex_s1", src: soundAsset+"ex_s1.ogg"},
			{id: "click", src: soundAsset+"click.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();


	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			// $prevBtn.show(0);
		}
		else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			setTimeout(function(){scoring.gotoNext();
			$('.ex-number-template-score').hide();
		},2500);
			// $prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
			// if(countNext == $total_page - 1)
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 1000);
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;
	var total_count = 0;
	var no_of_incorr = 0;
	var no_of_select = 0;
	var updater = 1;

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		$prevBtn.hide(0);
		countNext==0?sound_player("ex_s1"):'';
		countNext==5?sound_player("click"):'';
		no_of_incorr = 0;
		no_of_select = 0;
		total_count = $('.blank-space').length;

		dragdrop();
		shufflehint();
		threeOptionsFiller();

	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j=0;j<content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function dragdrop(){
			var counter = 0;
			var wrongFlag = 0;
			var updater = 1;
			$(".draggable").draggable({
					containment: "body",
					revert: true,
					appendTo: "body",
					zindex: 10,
					start:function(event, ui){
						$(this).removeClass('wrongcss');
						$('.wrongImg,.wrongImg1,.wrongImg2').hide(0);
					}
			});
			$('.droppable').droppable({
					accept : ".draggable",
					hoverClass: "hovered",
					drop: function(event,ui) {
						createjs.Sound.stop();
						if(ui.draggable.hasClass('adj') && $(this).hasClass('cart1')){
							ui.draggable.addClass('added').css({"pointer-events":"none"});
							play_correct_incorrect_sound(1);
							$('.drpclass').prepend("<img class='correctImg' src='images/correct_incorrect_icons/correct.png'/>");
							counter++;
						}
						else if(ui.draggable.hasClass('com') && $(this).hasClass('cart2')){
							ui.draggable.addClass('added2').css({"pointer-events":"none"});
							play_correct_incorrect_sound(1);
							$('.drpclass').prepend("<img class='correctImg1' src='images/correct_incorrect_icons/correct.png'/>");
							$('.correctImg1').css({"left":"50%"});
							counter++;
						}
						else if(ui.draggable.hasClass('sup') && $(this).hasClass('cart3')){
							ui.draggable.addClass('added3').css({"pointer-events":"none"});
							play_correct_incorrect_sound(1);
							$('.drpclass').prepend("<img class='correctImg2' src='images/correct_incorrect_icons/correct.png'/>");
							$('.correctImg2').css({"left":"79%"});
							counter++;
						}
					else{
							ui.draggable.addClass('wrongcss');
							wrongFlag=1;
							play_correct_incorrect_sound(0);
							 ui.draggable.hasClass('img1')?$('.drgclass').prepend("<img class='wrongImg' src='images/correct_incorrect_icons/incorrect.png'/>"):
							 ui.draggable.hasClass('img2')?$('.drgclass').prepend("<img class='wrongImg1' src='images/correct_incorrect_icons/incorrect.png'/>"):
							 ui.draggable.hasClass('img3')?$('.drgclass').prepend("<img class='wrongImg2' src='images/correct_incorrect_icons/incorrect.png'/>"):"";
						 }
						 console.log(counter);
						 if (counter==3){
							 navigationcontroller();
							 if(wrongFlag==0){
								 scoring.update(true);
							 }
						 }
				}
			});
		}

		function threeOptionsFiller(){
			updater = 1;
			$('.choice1,.choice2,.choice3').click(function(){
				createjs.Sound.stop();
					if($(this).hasClass('correctans')){
						play_correct_incorrect_sound(1);
						$(this).addClass('corrclass');
						$('.choice1,.choice2,.choice3').addClass('nopoint');
						$('.ansfiller').text($(this).text()).css({"text-decoration":"underline dotted"});
						navigationcontroller();
						console.log(updater);
						if(updater==1){
							scoring.update(true);
						}
					}
				else{
					$(this).addClass('incorrclass');
					play_correct_incorrect_sound(0);
					updater = 0;
				}
			});
		}
  function shufflehint(){
      var optdiv = $(".drgclass");

      for (var i = optdiv.find(".draggable").length; i >= 0; i--) {
          optdiv.append(optdiv.find(".draggable").eq(Math.random() * i | 0));
      }
			optdiv.find(".adj").removeClass().addClass("current adj");
			optdiv.find(".com").removeClass().addClass("current com");
      optdiv.find(".sup").removeClass().addClass("current sup");
      var optionclass = ["draggable img1","draggable img2","draggable img3"]
      optdiv.find(".current").each(function (index) {
          $(this).addClass(optionclass[index]);
      });
  }
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// navigationcontroller();
		generalTemplate();
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({
				'color' : 'red',
				"height" : "4.3vmin",
				"width" : "4.3vmin",
				"cursor" : "pointer",
				"text-align" : "center"
			});
		}
		function slides(i) {
			$($('.totalsequence')[i]).click(function() {
				countNext = i;
				templateCaller();
			});
		}
		*/
		loadTimelineProgress($total_page,countNext+1);
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

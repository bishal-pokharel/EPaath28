var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/orgSounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-amethyst',
		headerblockadditionalclass: "simpleheader",
		uppertextblock:[
			{
				textclass: "p2title fade_in",
				textdata: data.string.p4text1
			}
		],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		headerblockadditionalclass: "simpleheader",
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p4text2,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		speechbox:[{
			datahighlightflag: true,
      datahighlightcustomclass: "color_highlight",
			speechbox: 'sp-2',
			textdata : data.string.p4text3,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-3',
		speechbox:[{
			datahighlightflag: true,
			datahighlightcustomclass: "color_highlight",
			speechbox: 'sp-3',
			textdata : data.string.p4text4,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-4',
		speechbox:[{
			datahighlightflag: true,
			datahighlightcustomclass: "color_highlight",
			speechbox: 'sp-4',
			textdata : data.string.p4text5,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-5',
		speechbox:[{
			speechbox: 'sp-5',
			textdata : data.string.p4text6,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		headerblockadditionalclass: "simpleheader",
		uppertextblock:[
			{
				textclass: "p2ques fade_in",
				textdata: data.string.p4text7
			},
			{
				textclass: "diybutto-1 forhover correct",
				textdata: data.string.q1opt1
			},
			{
				textclass: "diybutto-2 forhover",
				textdata: data.string.q1opt2
			},
			{
				textclass: "diybutto-3 forhover",
				textdata: data.string.q1opt3
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "school",
					imgsrc : '',
					imgid : 'school'
				},
			]
		}],
		textwithblank:[{
			textdata : data.string.qfront1,
			remain : data.string.qback1
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		headerblockadditionalclass: "simpleheader",
		uppertextblock:[
			{
				textclass: "p2ques fade_in",
				textdata: data.string.p4text7
			},
			{
				textclass: "diybutto-1 forhover",
				textdata: data.string.diff
			},
			{
				textclass: "diybutto-2 forhover correct",
				textdata: data.string.mordiff
			},
			{
				textclass: "diybutto-3 forhover",
				textdata: data.string.mosdiff
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "school",
					imgsrc : '',
					imgid : 'sweep'
				},
			]
		}],
		textwithblank:[{
			textdata : data.string.qfront2,
			remain : data.string.qback2
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		headerblockadditionalclass: "simpleheader",
		uppertextblock:[
			{
				textclass: "p2ques fade_in",
				textdata: data.string.p4text7
			},
			{
				textclass: "diybutto-1 forhover",
				textdata: data.string.diff
			},
			{
				textclass: "diybutto-2 forhover",
				textdata: data.string.mordiff
			},
			{
				textclass: "diybutto-3 forhover correct",
				textdata: data.string.mosdiff
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "school",
					imgsrc : '',
					imgid : 'umbre'
				},
			]
		}],
		textwithblank:[{
			textdata : data.string.qfront3,
			remain : data.string.qback3
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "girl-1", src: imgpath+"girl_standing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"girl_talking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3", src: imgpath+"girl_pointing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-4", src: imgpath+"reena04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "menu", src: imgpath+"menu.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school", src: imgpath+"school.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sweep", src: imgpath+"girl-with-umbrella.png", type: createjs.AbstractLoader.IMAGE},
			{id: "umbre", src: imgpath+"bg_flood.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tl-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
			{id: "s4_p3", src: soundAsset+"s4_p3.ogg"},
			{id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p6", src: soundAsset+"s4_p6.ogg"},
			{id: "s4_p7", src: soundAsset+"s4_p7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	 function navigationcontroller(islastpageflag){
		 typeof islastpageflag === "undefined" ?
		 islastpageflag = false :
		 typeof islastpageflag != 'boolean'?
		 alert("NavigationController : Hi Master, please provide a boolean parameter") :
		 null;

			 if(countNext == 0 && $total_page!=1){
			 // $nextBtn.show(0);
			 $prevBtn.css('display', 'none');
			 }

			 else if($total_page == 1){
			 $prevBtn.css('display', 'none');
			 $nextBtn.css('display', 'none');

			 // if lastpageflag is true
			 islastpageflag ?
			 ole.footerNotificationHandler.lessonEndSetNotification() :
			 ole.footerNotificationHandler.lessonEndSetNotification() ;
			 }

			 else if(countNext > 0 && countNext < $total_page-1){
			 $nextBtn.show(0);
			 $prevBtn.show(0);
			 }

			 else if(countNext == $total_page-1){
			 $nextBtn.css('display', 'none');
			 $prevBtn.show(0);

			 // if lastpageflag is true
			 // islastpageflag ?
			 // ole.footerNotificationHandler.lessonEndSetNotification() :
			 // ole.footerNotificationHandler.pageEndSetNotification() ;
			 }
	 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockdiybutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockdiybutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		vocabcontroller.findwords(countNext);

		$(".close_diybutton").click(function(){
			$nextBtn.show(0);
			$(".moreinfo").hide(500);
		});
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		switch (countNext) {
			case 6:
			case 7:
			case 8:
				sound_player("s4_p"+(countNext+1),1);
				$('.forhover').click(function(){
					createjs.Sound.stop();
					if($(this).hasClass("correct")){
						//correct_btn(this);
						nav_diybutton_controls();
						$(this).removeClass("forhover").addClass("corr");
						$(this).siblings().removeClass("forhover");
						var texto = $(this).text();
						$('.dashedLine').html(texto);
						$('.dashedLine').html(texto).addClass('dsLine');
						play_correct_incorrect_sound(1);
					}
					else{
						play_correct_incorrect_sound(0);
						$(this).removeClass("forhover").addClass("incor");

					}
				});
			break;
			default:
				sound_player("s4_p"+(countNext+1),1);
			break;

		}
	}
	function nav_diybutton_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}


	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_diybutton_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){

		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent1 = content[count].livinnonlivin[0];
			if(lncontent1.hasOwnProperty('imageblock')){
				var imageblock = lncontent1.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent2 = content[count].livinnonlivin[1];
			if(lncontent2.hasOwnProperty('imageblock')){
				var imageblock = lncontent2.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

			var lncontent3 = content[count].livinnonlivin[2];
			if(lncontent3.hasOwnProperty('imageblock')){
				var imageblock = lncontent3.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext < 6)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide diybutton hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

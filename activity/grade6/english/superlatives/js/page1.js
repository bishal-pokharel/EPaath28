var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/orgSounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "treebg",
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "thetitle",
		},
	{
		textclass: "girl-sprite still"
	}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "treebg",
		uppertextblock:[
		{
			textclass: "girl-sprite pointing"
		}],
		speechbox:[{
			datahighlightflag: true,
			datahighlightcustomclass: "color_highlight",
	  	speechbox: 'dialogue1',
	  	textdata :data.string.p1text1,
	  	textclass : 'arrangetext',
	  	imgid : 'tb-2',
	  	imgsrc: '',
	  	// audioicon: true,
	 	}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "treebg",
		uppertextblock:[
		{
			textclass: "girl-sprite walking"
		}],
		speechbox:[{
			datahighlightflag: true,
			datahighlightcustomclass: "color_highlight",
	  	speechbox: 'dialogue2',
	  	textdata :data.string.p1text2,
	  	imgclass: 'invert',
	  	textclass : 'arrangetext',
	  	imgid : 'tb-2',
	  	imgsrc: '',
	  	// audioicon: true,
	 	}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "treebg",
		uppertextblock:[
		{
			textclass: "girl-sprite walking2"
		}],
		speechbox:[{
			datahighlightflag: true,
			datahighlightcustomclass: "color_highlight",
	  	speechbox: 'dialogue3',
	  	textdata :data.string.p1text3,
	  	imgclass: 'invert',
	  	textclass : 'arrangetext',
	  	imgid : 'tb-2',
	  	imgsrc: '',
	  	// audioicon: true,
	 	}],
	},
	//slide 4
	{
		contentblockadditionalclass: "creambg",
		headerblockadditionalclass: "simpleheader",
		headerblock:[
		{
			textdata :data.lesson.chapter,
		}],
		uppertextblock:[
			{
				textclass: "uptext fade_in",
				textdata: data.string.p1text7,
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "textbox-tall",
				textdata: data.string.p1text4,
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext2",
				textclass: "textbox-taller",
				textdata: data.string.p1text5,
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext3",
				textclass: "textbox-tallest",
				textdata: data.string.p1text6,
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "talltr fade_in1",
					imgid : 'talltree',
					imgsrc: ""
				},
				{
					imgclass: "tallertr fade_in2",
					imgid : 'tallertree',
					imgsrc: ""
				},
				{
					imgclass: "tallesttr fade_in3",
					imgid : 'tallesttree',
					imgsrc: ""
				}
			]
		}]
	},
	//slide5
	{
		contentblockadditionalclass: "ole-background-gradient-opal",
		uppertextblock:[
		{
			textclass: "middletext fade_in",
			textdata: data.string.p1text8,
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: "creambg",
		headerblockadditionalclass: "simpleheader",
		headerblock:[
		{
			textdata :data.lesson.chapter,
		}],
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "color_highlight",
				textclass: "downtext",
				textdata: data.string.p1text9,
			},
			{
				textclass: "big",
				textdata: data.string.big,
			},
			{
				textclass: "bigger",
				textdata: data.string.bigger,
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "croc",
					imgid : 'croc',
					imgsrc: ""
				},
				{
					imgclass: "rhino",
					imgid : 'rhino',
					imgsrc: ""
				}
			]
		}]
	},
	//slide 7
	{
		contentblockadditionalclass: "creambg",
		headerblockadditionalclass: "simpleheader",
		headerblock:[
		{
			textdata :data.lesson.chapter,
		}],
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "color_highlight",
				textclass: "downtext",
				textdata: data.string.p1text10,
			},
			{
				textclass: "big",
				textdata: data.string.big,
			},
			{
				textclass: "bigger",
				textdata: data.string.bigger,
			},
			{
				textclass: "biggest",
				textdata: data.string.biggest,
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "croc",
					imgid : 'croc',
					imgsrc: ""
				},
				{
					imgclass: "rhino",
					imgid : 'rhino',
					imgsrc: ""
				},
				{
					imgclass: "elep",
					imgid : 'elep',
					imgsrc: ""
				}
			]
		}]
	},
	//slide 8
	{
		contentblockadditionalclass: "creambg",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata :data.lesson.chapter,
			},
			{
				textdata :data.string.adj,
			}],
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext2",
				textclass: "uptext",
				textdata: data.string.p1text11,
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg",
					imgid : 'croc',
					imgsrc: ""
				},
			]
		}]
	},
	//slide 9
	{
		contentblockadditionalclass: "creambg",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata :data.lesson.chapter,
			},
			{
				textdata :data.string.adj,
			}],
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext2",
				textclass: "uptext",
				textdata: data.string.p1text12,
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext2",
				textclass: "downtext",
				textdata: data.string.p1text13,
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg21",
					imgid : 'croc',
					imgsrc: ""
				},
				{
					imgclass: "midimg22",
					imgid : 'rhino',
					imgsrc: ""
				},
			]
		}]
	},
	//slide 10
	{
		contentblockadditionalclass: "creambg",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata :data.lesson.chapter,
			},
			{
				textdata :data.string.adj,
			}],
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext2",
				textclass: "uptext",
				textdata: data.string.p1text14,
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext2",
				textclass: "downtext",
				textdata: data.string.p1text15,
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg31",
					imgid : 'croc',
					imgsrc: ""
				},
				{
					imgclass: "midimg32",
					imgid : 'rhino',
					imgsrc: ""
				},
				{
					imgclass: "midimg33",
					imgid : 'elep',
					imgsrc: ""
				},
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "girl", src: imgpath+"girl_pointing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "talltree", src: imgpath+"tall_tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tallertree", src: imgpath+"taller_tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tallesttree", src: imgpath+"tallest_tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "croc", src: imgpath+"alligator.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+"rhino.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elep", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	// islastpageflag ?
 	// ole.footerNotificationHandler.lessonEndSetNotification() :
 	// ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		vocabcontroller.findwords(countNext);
	  	$nextBtn.hide(0);
	  	$prevBtn.hide(0);
		switch(countNext){
			case 2:
				$(".dialogue2").hide(0);
				setTimeout(function(){
					$(".dialogue2").fadeIn(200);
					sound_player("s1_p"+(countNext+1),1);
				},4000);
			break;
			case 3:
				$(".dialogue3").hide(0);
				setTimeout(function(){
					$(".dialogue3").fadeIn(200);
					sound_player("s1_p"+(countNext+1),1);
				},4000);
			break;
			case 4:
				sound_player("s1_p"+(countNext+1),1);
				$(".textbox-tall, .textbox-taller, .textbox-tallest, .talltr, .tallertr, .tallesttr").hide(0);
				$(".textbox-tall, .talltr").delay(10673).fadeIn(300);
				$(".textbox-taller, .tallertr").delay(23580).fadeIn(300);
				$(".textbox-tallest, .tallesttr").delay(36580).fadeIn(300);
				$(".hightext1").eq(1).css({"color": "#1471be"});
				$(" .hightext2").eq(1 ).css({"color": "#1471be"});
				$(" .hightext3").eq(1 ).css({"color": "#1471be"});
				$(".hightext1").eq(2).css({"color": "#1471be"});
			break;
			case 8:
				sound_player("s1_p"+(countNext+1),1);
				$(" .hightext2").eq(0 ).css({"color": "#1471be"});
				$(" .hightext2").eq(1 ).css({"color": "#1471be"});
			break;
			case 9:
				sound_player("s1_p"+(countNext+1),1);
				$(" .hightext2").eq(0 ).css({"color": "#1471be"});
				$(" .hightext2").eq(1 ).css({"color": "#1471be"});
				$(" .hightext2").eq(2 ).css({"color": "#1471be"});
				$(" .hightext2").eq(3 ).css({"color": "#1471be"});
				$(" .hightext2").eq(5 ).css({"color": "#1471be"});
			break;
			case 10:
				sound_player("s1_p"+(countNext+1),1);
				$(" .hightext2").eq(0 ).css({"color": "#1471be"});
				$(" .hightext2").eq(1 ).css({"color": "#1471be"});
				$(" .hightext2").eq(2 ).css({"color": "#1471be"});
				$(" .hightext2").eq(3 ).css({"color": "#1471be"});
				$(" .hightext2").eq(4 ).css({"color": "#1471be"});
				$(" .hightext2").eq(6 ).css({"color": "#1471be"});
			break;
			default:
				sound_player("s1_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

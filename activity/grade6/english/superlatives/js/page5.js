var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var quesblock=[
	{
		//0
		hbquesadditionalclass: "quesbox-2",
		hbquesblock:[{
			questype: "theques-type1",
			quesins: data.string.quesins,
			front: data.string.ques2front,
			remain: data.string.ques2back,
			optionstype: "optcontainer-1",
			theopt1: data.string.ques2opt1,
			theopt2: data.string.ques2opt2,
			theopt3: data.string.ques2opt3,
			corr3: "correct"
		}],
	},
	{
		//1
		hbquesadditionalclass: "quesbox-3",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques3front,
			remain: data.string.ques3back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques3opt1,
			theopt2: data.string.ques3opt2,
			theopt3: data.string.ques3opt3,
			corr2: "correct"
		}],
	},
	{
		//2
		hbquesadditionalclass: "quesbox-4",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques4front,
			remain: data.string.ques4back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques4opt1,
			theopt2: data.string.ques4opt2,
			theopt3: data.string.ques4opt3,
			corr2: "correct"
		}],
	},
	{
		//3
		hbquesadditionalclass: "quesbox-5",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques5front,
			remain: data.string.ques5back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques5opt1,
			theopt2: data.string.ques5opt2,
			theopt3: data.string.ques5opt3,
			corr3: "correct"
		}],
	},
	{
		//4
		hbquesadditionalclass: "quesbox-6",
		hbquesblock:[{
			questype: "theques-type3",
			quesins: data.string.quesins,
			front: data.string.ques6front,
			remain: data.string.ques6back,
			optionstype: "optcontainer-3",
			theopt1: data.string.ques6opt1,
			theopt2: data.string.ques6opt2,
			theopt3: data.string.ques6opt3,
			corr1: "correct"
		}],
	},
	{
		//5
		hbquesadditionalclass: "quesbox-7",
		hbquesblock:[{
			questype: "theques-type3",
			quesins: data.string.quesins,
			front: data.string.ques7front,
			remain: data.string.ques7back,
			optionstype: "optcontainer-3",
			theopt1: data.string.ques7opt1,
			theopt2: data.string.ques7opt2,
			theopt3: data.string.ques7opt3,
			corr2: "correct"
		}],
	},
	{
		//6
		dragadditionalclass: "quesbox-8",
		dragblock:[{
			imgsrc: imgpath + "questions/running.png",
			questype: "theques-type2",
			quesins: data.string.dragins,
			dragtext: "Fastest",
			corr3: "correct"
		}]
	},
	{
		//7
		hbquesadditionalclass: "quesbox-9",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques9front,
			remain: data.string.ques9back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques9opt1,
			theopt2: data.string.ques9opt2,
			theopt3: data.string.ques9opt3,
			corr1: "correct"
		}],
	},
	{
		//8

	},
	{
		//9
		hbquesadditionalclass: "quesbox-11",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques11front,
			remain: data.string.ques11back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques11opt1,
			theopt2: data.string.ques11opt2,
			theopt3: data.string.ques11opt3,
			corr2: "correct"
		}],
	},
	{
		//10

	},

	{
		//11
		dragadditionalclass: "quesbox-8",
		dragblock:[{
			imgsrc: imgpath + "questions/elephant.png",
			questype: "theques-type2",
			quesins: data.string.dragins,
			dragtext: "Biggest",
			corr3: "correct"
		}]
	},
	{
		//12

	},
	{
		//13
		hbquesadditionalclass: "quesbox-15",
		hbquesblock:[{
			questype: "theques-type3",
			quesins: data.string.quesins,
			front: data.string.ques15front,
			remain: data.string.ques15back,
			optionstype: "optcontainer-3",
			theopt1: data.string.ques15opt1,
			theopt2: data.string.ques15opt2,
			theopt3: data.string.ques15opt3,
			corr2: "correct"
		}],
	},
	{
		//14
		hbquesadditionalclass: "quesbox-16",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques16front,
			remain: data.string.ques16back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques16opt1,
			theopt2: data.string.ques16opt2,
			theopt3: data.string.ques16opt3,
			corr3: "correct"
		}],
	},
	{
		//15
		hbquesadditionalclass: "quesbox-17",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques17front,
			remain: data.string.ques17back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques17opt1,
			theopt2: data.string.ques17opt2,
			theopt3: data.string.ques17opt3,
			corr2: "correct"
		}],
	},
	{
		//16
		hbquesadditionalclass: "quesbox-18",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques18front,
			remain: data.string.ques18back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques18opt1,
			theopt2: data.string.ques18opt2,
			theopt3: data.string.ques18opt3,
			corr3: "correct"
		}],
	},
	{
		//17
		dragadditionalclass: "quesbox-8",
		dragblock:[{
			imgsrc: imgpath + "questions/watermelion.png",
			questype: "theques-type2",
			quesins: data.string.dragins,
			dragtext: "Healthy",
			corr1: "correct"
		}]
	},
	{
		//18
		hbquesadditionalclass: "quesbox-20",
		hbquesblock:[{
			questype: "theques-type3",
			quesins: data.string.quesins,
			front: data.string.ques20front,
			remain: data.string.ques20back,
			optionstype: "optcontainer-3",
			theopt1: data.string.ques20opt1,
			theopt2: data.string.ques20opt2,
			theopt3: data.string.ques20opt3,
			corr2: "correct"
		}],
	},
	{
		//19
		hbquesadditionalclass: "quesbox-21",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques21front,
			remain: data.string.ques21back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques21opt1,
			theopt2: data.string.ques21opt2,
			theopt3: data.string.ques21opt3,
			corr1: "correct"
		}],
	},
	{
		//20
		hbquesadditionalclass: "quesbox-22",
		hbquesblock:[{
			questype: "theques-type3",
			quesins: data.string.quesins,
			front: data.string.ques22front,
			remain: data.string.ques22back,
			optionstype: "optcontainer-3",
			theopt1: data.string.ques22opt1,
			theopt2: data.string.ques22opt2,
			theopt3: data.string.ques22opt3,
			corr2: "correct"
		}],
	},
	{
		//21
	},
	{
		//22
		hbquesadditionalclass: "quesbox-24",
		hbquesblock:[{
			questype: "theques-type3",
			quesins: data.string.quesins,
			front: data.string.ques24front,
			remain: data.string.ques24back,
			optionstype: "optcontainer-3",
			theopt1: data.string.ques24opt1,
			theopt2: data.string.ques24opt2,
			theopt3: data.string.ques24opt3,
			corr2: "correct"
		}],
	},
	{
		//23
	},
	{
		//24
		hbquesadditionalclass: "quesbox-26",
		hbquesblock:[{
			questype: "theques-type2",
			quesins: data.string.quesins,
			front: data.string.ques26front,
			remain: data.string.ques26back,
			optionstype: "optcontainer-2",
			theopt1: data.string.ques26opt1,
			theopt2: data.string.ques26opt2,
			theopt3: data.string.ques26opt3,
			corr2: "correct"
		}],
	},
	{
	//25
	},
	{
		//26
		hbquesadditionalclass: "quesbox-28",
		hbquesblock:[{
			questype: "theques-type3",
			quesins: data.string.quesins,
			front: data.string.ques28front,
			remain: data.string.ques28back,
			optionstype: "optcontainer-3",
			theopt1: data.string.ques28opt1,
			theopt2: data.string.ques28opt2,
			theopt3: data.string.ques28opt3,
			corr2: "correct"
		}],
	},

]

var content=[
	// slide0
	{
		contentblockadditionalclass: 'diyback',
		uppertextblock:[
			{
				textclass: "diytext",
				textdata: data.string.diy
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "pencil",
					imgsrc : '',
					imgid : 'pencil'
				},
			]
		}],
	},
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		insblock:[
			{
				institle: "BOARD GAME",
			}
		],
		uppertextblock:[
			{
				textclass: "thelives",
				textdata: data.string.lives
			},
			{
				textclass: "suandco",
				textdata: "SUPERLATIVES AND COMPARATIVES"
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "dice rndbtn",
					imgsrc : '',
					imgid : 'dice'
				},
				{
					imgclass : "arrow",
					imgsrc : '',
					imgid : 'arrow'
				},
				{
					imgclass : "icons-help",
					imgsrc : '',
					imgid : 'help'
				},
				{
					imgclass : "icons-music",
					imgsrc : '',
					imgid : 'music'
				},
				{
					imgclass : "icons-sound",
					imgsrc : '',
					imgid : 'sound'
				},

			]
		}],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal box1",
						},{
							flexboxrowclass :"rownormal box10",
						},{
							flexboxrowclass :"rownormal box11",
						},{
							flexboxrowclass :"rownormal box20",
						},{
							flexboxrowclass :"rownormal box21",
						},{
							flexboxrowclass :"rownormal box30",
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal box2",
						},{
							flexboxrowclass :"rownormal box9",
						},{
							flexboxrowclass :"rownormal box12",
						},{
							flexboxrowclass :"rownormal box19",
						},{
							flexboxrowclass :"rownormal box22",
						},{
							flexboxrowclass :"rownormal box29",
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal box3",
						},{
							flexboxrowclass :"rownormal box8",
						},{
							flexboxrowclass :"rownormal box13",
						},{
							flexboxrowclass :"rownormal box18",
						},{
							flexboxrowclass :"rownormal box23",
						},{
							flexboxrowclass :"rownormal box28",
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal box4",
						},{
							flexboxrowclass :"rownormal box7",
						},{
							flexboxrowclass :"rownormal box14",
						},{
							flexboxrowclass :"rownormal box17",
						},{
							flexboxrowclass :"rownormal box24",
						},{
							flexboxrowclass :"rownormal box27",
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal box5",
						},{
							flexboxrowclass :"rownormal box6",
						},{
							flexboxrowclass :"rownormal box15",
						},{
							flexboxrowclass :"rownormal box16",
						},{
							flexboxrowclass :"rownormal box25",
						},{
							flexboxrowclass :"rownormal box26",
						}
						]
					}

				]
			}
		]
	},
	{
		contentblockadditionalclass: 'ole-background-gradient-midnight',
		uppertextblock:[
			{
				textclass: "lasttext",
				textdata: data.string.gover
			},
			{
				textclass: "okaybtn",
			textdata: "Try Again "
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "lastimg",
					imgsrc : '',
					imgid : 'loser'
				},
			]
		}],
	},
	{
		contentblockadditionalclass: 'ole-background-gradient-kiwi',
		uppertextblock:[
			{
				textclass: "lasttext",
				textdata: data.string.cong
			},
			{
				textclass: "okaybtn",
			textdata: "Play Again"
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "lastimg",
					imgsrc : '',
					imgid : 'winner'
				},
			]
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $board2 = $('.quesboard');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var $total_page = content.length;

	var preload;
	var timeoutvar = null;
	var not_going_back_flag = '';
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			{id: "sweep", src: imgpath+"board/btn.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dice", src: imgpath+"dice/6.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"board/arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "heart", src: imgpath+"red-heart.png", type: createjs.AbstractLoader.IMAGE},
			{id: "help", src: imgpath+"q.png", type: createjs.AbstractLoader.IMAGE},
			{id: "music", src: imgpath+"music.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sound", src: imgpath+"sound.png", type: createjs.AbstractLoader.IMAGE},
			{id: "winner", src: imgpath+"winning_prze.png", type: createjs.AbstractLoader.IMAGE},
			{id: "loser", src: "images/sundar/incorrect-3.png", type: createjs.AbstractLoader.IMAGE},

			{id: "box1", src: imgpath+"board/01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box2", src: imgpath+"board/02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box3", src: imgpath+"board/03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box4", src: imgpath+"board/04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box5", src: imgpath+"board/05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box6", src: imgpath+"board/06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box7", src: imgpath+"board/07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box8", src: imgpath+"board/08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box9", src: imgpath+"board/09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box10", src: imgpath+"board/10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box11", src: imgpath+"board/11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box12", src: imgpath+"board/12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box13", src: imgpath+"board/13.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box14", src: imgpath+"board/14.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box15", src: imgpath+"board/15.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box16", src: imgpath+"board/16.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box17", src: imgpath+"board/17.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box18", src: imgpath+"board/18.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box19", src: imgpath+"board/19.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box20", src: imgpath+"board/20.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box21", src: imgpath+"board/21.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box22", src: imgpath+"board/22.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box23", src: imgpath+"board/23.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box24", src: imgpath+"board/24.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box25", src: imgpath+"board/25.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box26", src: imgpath+"board/26.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box27", src: imgpath+"board/27.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box28", src: imgpath+"board/28.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box29", src: imgpath+"board/29.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box30", src: imgpath+"board/30.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg1", src: imgpath+"questions/bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"questions/bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"questions/bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"questions/bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"questions/bg09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"questions/bg08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"questions/bg07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg8", src: imgpath+"questions/bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg9", src: imgpath+"questions/bg10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg10", src: imgpath+"questions/bg12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg11", src: imgpath+"questions/bg18.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg12", src: imgpath+"questions/bg17.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg13", src: imgpath+"questions/bg16.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg14", src: imgpath+"questions/bg14.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg15", src: imgpath+"questions/bg19.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg16", src: imgpath+"questions/bg20.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg17", src: imgpath+"questions/bg21.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg18", src: imgpath+"questions/bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg19", src: imgpath+"questions/bg13.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tl-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "roll", src: soundAsset+"roll.ogg"},
			{id: "slidegatti", src: soundAsset+"jump.ogg"},
			{id: "bgmusic", src: soundAsset+"ukulele.ogg"},
			{id: "rightans", src: soundAsset+"correct.ogg"},
			{id: "wrongans", src: soundAsset+"131657__bertrof__game-sound-wrong.ogg"},
			{id: "gtstart", src: soundAsset+"gotostart.ogg"},
			{id: "reverse", src: soundAsset+"lostlife.ogg"},
			{id: "complete", src: soundAsset+"success.ogg"},
			{id: "gameover", src: soundAsset+"gameover.ogg"},
			{id: "s5_p2", src: soundAsset+"orgSounds/s5_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	 function navigationcontroller(islastpageflag){
		 typeof islastpageflag === "undefined" ?
		 islastpageflag = false :
		 typeof islastpageflag != 'boolean'?
		 alert("NavigationController : Hi Master, please provide a boolean parameter") :
		 null;

			 if(countNext == 0 && $total_page!=1){
			 $nextBtn.show(0);
			 $prevBtn.css('display', 'none');
			 }

			 else if($total_page == 1){
			 $prevBtn.css('display', 'none');
			 $nextBtn.css('display', 'none');

			 // if lastpageflag is true
			 islastpageflag ?
			 ole.footerNotificationHandler.pageEndSetNotification() :
			 ole.footerNotificationHandler.pageEndSetNotification() ;
			 }

			 else if(countNext > 0 && countNext < $total_page-1){
			 $nextBtn.show(0);
			 $prevBtn.show(0);
			 }

			 else if(countNext == $total_page-1){
			 $nextBtn.css('display', 'none');
			 $prevBtn.show(0);

			 // if lastpageflag is true
			 islastpageflag ?
			 ole.footerNotificationHandler.pageEndSetNotification() :
			 ole.footerNotificationHandler.pageEndSetNotification() ;
			 }
	 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockdiybutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockdiybutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var source2 = $("#ques-template").html();
		var template2 = Handlebars.compile(source2);
		var html2 = template2(quesblock[6]);
		var soundFlag = false;
		var musicFlag = false;
		// $(".quesboard").html(html2);
		// $(".quesboard").show(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		vocabcontroller.findwords(countNext);

		$(".close_diybutton").click(function(){
			$nextBtn.show(0);
			$(".moreinfo").hide(500);
		});

		function sound_player(sound_id){
		if(soundFlag == false){
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}
		}

		function bg_audio(sound_id){
			if(musicFlag == false){
				current_sound = createjs.Sound.play(sound_id);
				current_sound.play();
				current_sound.on("complete", handleComplete);
				function handleComplete(event){
					bg_audio("bgmusic");
				}
			}
		}

		switch (countNext) {
			case 0:
				play_diy_audio();
			break;
			 case 1:
		 		createjs.Sound.stop();
		 		current_sound = createjs.Sound.play("s5_p2");
		 		current_sound.play();
			 // {
	 		// if(soundFlag == false){
	 		// 	current_sound = createjs.Sound.play(sound_id);
	 		// 	current_sound.play();
	 		// }
	 		// }
			 $(".box1").css("background", "url("+preload.getResult('box1').src+" )no-repeat center center / 100% 100%");
			 $(".box2").css("background", "url("+preload.getResult('box2').src+" )no-repeat center center / 100% 100%");
			 $(".box3").css("background", "url("+preload.getResult('box3').src+" )no-repeat center center / 100% 100%");
			 $(".box4").css("background", "url("+preload.getResult('box4').src+" )no-repeat center center / 100% 100%");
			 $(".box5").css("background", "url("+preload.getResult('box5').src+" )no-repeat center center / 100% 100%");
			 $(".box6").css("background", "url("+preload.getResult('box6').src+" )no-repeat center center / 100% 100%");
			 $(".box7").css("background", "url("+preload.getResult('box7').src+" )no-repeat center center / 100% 100%");
			 $(".box8").css("background", "url("+preload.getResult('box8').src+" )no-repeat center center / 100% 100%");
			 $(".box9").css("background", "url("+preload.getResult('box9').src+" )no-repeat center center / 100% 100%");
			 $(".box10").css("background", "url("+preload.getResult('box10').src+" )no-repeat center center / 100% 100%");
			 $(".box11").css("background", "url("+preload.getResult('box11').src+" )no-repeat center center / 100% 100%");
			 $(".box12").css("background", "url("+preload.getResult('box12').src+" )no-repeat center center / 100% 100%");
			 $(".box13").css("background", "url("+preload.getResult('box13').src+" )no-repeat center center / 100% 100%");
			 $(".box14").css("background", "url("+preload.getResult('box14').src+" )no-repeat center center / 100% 100%");
			 $(".box15").css("background", "url("+preload.getResult('box15').src+" )no-repeat center center / 100% 100%");
			 $(".box16").css("background", "url("+preload.getResult('box16').src+" )no-repeat center center / 100% 100%");
			 $(".box17").css("background", "url("+preload.getResult('box17').src+" )no-repeat center center / 100% 100%");
			 $(".box18").css("background", "url("+preload.getResult('box18').src+" )no-repeat center center / 100% 100%");
			 $(".box19").css("background", "url("+preload.getResult('box19').src+" )no-repeat center center / 100% 100%");
			 $(".box20").css("background", "url("+preload.getResult('box20').src+" )no-repeat center center / 100% 100%");
			 $(".box21").css("background", "url("+preload.getResult('box21').src+" )no-repeat center center / 100% 100%");
			 $(".box22").css("background", "url("+preload.getResult('box22').src+" )no-repeat center center / 100% 100%");
			 $(".box23").css("background", "url("+preload.getResult('box23').src+" )no-repeat center center / 100% 100%");
			 $(".box24").css("background", "url("+preload.getResult('box24').src+" )no-repeat center center / 100% 100%");
			 $(".box25").css("background", "url("+preload.getResult('box25').src+" )no-repeat center center / 100% 100%");
			 $(".box26").css("background", "url("+preload.getResult('box26').src+" )no-repeat center center / 100% 100%");
			 $(".box27").css("background", "url("+preload.getResult('box27').src+" )no-repeat center center / 100% 100%");
			 $(".box28").css("background", "url("+preload.getResult('box28').src+" )no-repeat center center / 100% 100%");
			 $(".box29").css("background", "url("+preload.getResult('box29').src+" )no-repeat center center / 100% 100%");
			 $(".box30").css("background", "url("+preload.getResult('box30').src+" )no-repeat center center / 100% 100%");

			 var alllefts = [10, 30, 50, 70, 90];
			var alltops = [8.33333333335, 25, 41.6666666668, 58.3333333335, 75, 91.6666666669];

			$(".icons-help").click(function(){
				$(".ins-box").show(500);
			});

			$(".icons-sound").click(function(){
				if(soundFlag == false){
					soundFlag = true;
					$(this).attr("src",imgpath+"sound01.png");
				}
				else{
					soundFlag = false;
					$(this).attr("src",imgpath+"sound.png");
				}
			});

			$(".icons-music").click(function(){
				if(musicFlag == false){
					createjs.Sound.stop();
					musicFlag = true;
					$(this).attr("src",imgpath+"music01.png");
				}
				else{
					musicFlag = false;
					$(this).attr("src",imgpath+"music.png");
					bg_audio("bgmusic");
				}
			});

			$(".okaybtn").click(function(){
				$(".ins-box").hide(500);
 		 		createjs.Sound.stop();
			 bg_audio("bgmusic");
			});

			var arraypath = [];
			var ultopar = true;
			for(i=0; i<6; i++){
				if(ultopar == true){
					for(j=0; j<5; j++){
						arraypath.push([alltops[i],alllefts[j]]);
					}
					ultopar = false;
				}
				else{
					for(j=4; j>=0; j--){
						arraypath.push([alltops[i],alllefts[j]]);
					}
					ultopar = true;
				}

			}
			var steps;
			var currentstep = 1;
			var diceFlag = true;
			var reversestep;
			$(".rndbtn").click(function(){
				if(diceFlag == true){
					sound_player("roll");
					steps = Math.floor(Math.random() * 6) + 1;
					//steps = 1;
					$(".arrow").hide(0);
					//alert(steps);
					$(".rndbtn").attr('src',imgpath + '/dice/giphy.gif').addClass("rolleffect");
					setTimeout(function(){
						$(".rndbtn").attr('src',imgpath + '/dice/'+ steps + '.png').removeClass("rolleffect");
						movegatti();
					}, 1000);
				}
				diceFlag = false;
			});

			$(".testcor").click(function(){
				diceFlag = true;
				$(".arrow").show(0);
			});

			function movegatti(){
				sound_player("slidegatti");
				$( ".gatti" ).animate({
							left: arraypath[currentstep][1] + "%",
							top: arraypath[currentstep][0] + "%",
						}, 1000, function() {
							//continuemove();
							steps--;
							if(currentstep <= 29){
								currentstep++;
								console.log("currentstep="+currentstep);
							}
							else {
								currentstep--;
								//movegattirev(steps);
							}
							if(steps!=0 && currentstep <= 29){
								movegatti();
							}
							else if(steps!=0 && currentstep > 29){
								countNext = 3;
								templateCaller();
							}
							else{
								donerolling(currentstep);
							}
						});
			}

			function donerolling(finno){
				console.log(finno);
				if(finno == 1){
					$(".arrow").show(0);
				}
				else if(finno == 10){
					sound_player("reverse");
					movegattirev(8, 7, 8);
				}
				else if(finno == 12){
					diceFlag = true;
					$(".arrow").show(0);
					//movegattirev(8, 6, 7);
				}
				else if(finno == 14){
					steps = 1;

						movegatti();
				}
				else if(finno == 23){
					diceFlag = true;
					$(".arrow").show(0);
					//movegattirev(8, 6, 7);
				}
				else if(finno == 25){
					sound_player("reverse");
					movegattirev(23, 20, 21);
				}
				else if(finno == 27){
					if(not_going_back_flag==''){
						currentstep = 0;
						diceFlag = true;
						//$(".arrow").show(0);
						sound_player("gtstart");
						steps = 1;
						movegatti();
					}
					not_going_back_flag='yes';
				}
				else if(finno == 29){
					sound_player("reverse");
					movegattirev(27, 25, 26);
				}
				else if(finno > 29){
					countNext = 3;
					templateCaller();
				}
				else{
					var html2 = template2(quesblock[finno-2]);
					$(".quesboard").html(html2);
					$(".quesboard").show(0);

					$(".quesbox-2").css("background", "url("+preload.getResult('bg1').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-3").css("background", "url("+preload.getResult('bg2').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-4").css("background", "url("+preload.getResult('bg3').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-5").css("background", "url("+preload.getResult('bg4').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-6").css("background", "url("+preload.getResult('bg5').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-7").css("background", "url("+preload.getResult('bg6').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-8").css("background", "url("+preload.getResult('bg7').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-9").css("background", "url("+preload.getResult('bg8').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-11").css("background", "url("+preload.getResult('bg9').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-15").css("background", "url("+preload.getResult('bg10').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-16").css("background", "url("+preload.getResult('bg11').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-17").css("background", "url("+preload.getResult('bg12').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-18").css("background", "url("+preload.getResult('bg13').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-20").css("background", "url("+preload.getResult('bg14').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-21").css("background", "url("+preload.getResult('bg15').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-22").css("background", "url("+preload.getResult('bg16').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-24").css("background", "url("+preload.getResult('bg17').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-26").css("background", "url("+preload.getResult('bg18').src+" )no-repeat center center / 100% 100%");
					$(".quesbox-28").css("background", "url("+preload.getResult('bg19').src+" )no-repeat center center / 100% 100%");


					if(finno == 8 || finno == 13 || finno == 19){
						$(".dragthis").draggable({
							containment : ".quesboard",
							revert : true,
							cursor : "move",
							zIndex: 100000,
						});

						$(".dropone").droppable({
							hoverClass : 'hover-active',
							drop:function(event, ui) {
								dropper(ui.draggable, $(this));
							}
						});
						$(".droptwo").droppable({
							hoverClass : 'hover-active',
							drop:function(event, ui) {
								dropper(ui.draggable, $(this));
							}
						});
						$(".dropthr").droppable({
							hoverClass : 'hover-active',
							drop:function(event, ui) {
								dropper(ui.draggable, $(this));
							}
						});

						function dropper(dropped, droppedOn){
							if(droppedOn.hasClass("correct")){
								 dropped.draggable('option', 'revert', false);
								 dropped.hide(0);
								 thisiscorrect();
								 if(droppedOn.hasClass("dropone"))
								 	$('.dragblock ').prepend('<img class="theicons1" src="images/correct.png" />');
								else	if(droppedOn.hasClass("droptwo"))
									$('.dragblock ').prepend('<img class="theicons2" src="images/correct.png" />');
								else
									$('.dragblock ').prepend('<img class="theicons3" src="images/correct.png" />');
							} else{
								thisiswrong();
								if(droppedOn.hasClass("dropone"))
								 $('.dragblock ').prepend('<img class="theicons1" src="images/wrong.png" />');
							 else	if(droppedOn.hasClass("droptwo"))
								 $('.dragblock ').prepend('<img class="theicons2" src="images/wrong.png" />');
							 else
								 $('.dragblock ').prepend('<img class="theicons3" src="images/wrong.png" />');
							}
						}
					}
					ansclicked();
				}
			}

			function movegattirev(reversestep, destination, currste){
				sound_player("slidegatti");
				currentstep = currste;
				$( ".gatti" ).animate({
							left: arraypath[reversestep][1] + "%",
							top: arraypath[reversestep][0] + "%",
						}, 1000, function() {
							if(reversestep != destination){
								reversestep--;
								movegattirev(reversestep, destination, currste);
							}
							else{
								donerolling(currentstep);
								//alert("done");
							}
						});
			}

			console.log(arraypath);

			var lives = 5;
			function ansclicked(){
				$('.forhover').click(function(){
					if($(this).hasClass("forhover")){
						if($(this).hasClass("correct")){
							//correct_btn(this);
							$(".dashedLine").text($(this).text()).addClass("correctans");
							//navigationcontroller();
							$(this).removeClass("forhover").addClass("corr");
							$(this).siblings().removeClass("forhover");
							thisiscorrect();
						}
						else{
							$(this).removeClass("forhover").addClass("incor");
							thisiswrong();
						}
					}

				});
			}

			function thisiscorrect(){
				$(".close1").show(0);
				$(".close1").click(function(){
					$(this).parent().parent().fadeOut();
					diceFlag = true;
					$(".arrow").show(0);
				});
				sound_player("rightans");
			}

			function thisiswrong(){
				sound_player("wrongans");
				//play_correct_incorrect_sound(0);
				$("#heart"+lives).attr('src',imgpath + 'black-heart.png');
				lives--;
				if(lives == 0){
					countNext = 2;
					templateCaller();
				}
			}
			break;
			case 2:
			createjs.Sound.stop();
			sound_player("gameover");
				$(".okaybtn").click(function(){
					countNext = 1;
					templateCaller();
				});
                ole.footerNotificationHandler.pageEndSetNotification() ;
                break;
			case 3:
			createjs.Sound.stop();
			sound_player("complete");
				$(".okaybtn").click(function(){
					countNext = 1;
					templateCaller();
                });
                ole.footerNotificationHandler.pageEndSetNotification() ;
                break;
		}
	}
	function nav_diybutton_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_diybutton_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){

		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent1 = content[count].livinnonlivin[0];
			if(lncontent1.hasOwnProperty('imageblock')){
				var imageblock = lncontent1.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent2 = content[count].livinnonlivin[1];
			if(lncontent2.hasOwnProperty('imageblock')){
				var imageblock = lncontent2.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

			var lncontent3 = content[count].livinnonlivin[2];
			if(lncontent3.hasOwnProperty('imageblock')){
				var imageblock = lncontent3.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide diybutton hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/orgSounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-amethyst',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2text1
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}]
	},
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-amethyst',
		uppertextblock:[
			{
				textclass: "p2text",
				textdata: data.string.p2text2
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}],
		tableblock : [
		{
			tableclass: "mytable",
			checkclass: "check",
			check: data.string.p2text22,
			colclass1: "column1",
			tableheading: [
			{
				h1: data.string.h1,
				h2: data.string.h2,
				h3: data.string.h3
			}],
			tablebody: [
				//1st row
				{
					c1: data.string.c1r1,
					c2: data.string.c2r1,
					c3: data.string.c3r1
				},

				//2nd row
				{
					c1: data.string.c1r2,
					c2: data.string.c2r2,
					c3: data.string.c3r2
				},

				//3rd row
				{
					c1: data.string.c1r3,
					c2: data.string.c2r3,
					c3: data.string.c3r3
				},

				//4th row
				{
					c1: data.string.c1r4,
					c2: data.string.c2r4,
					c3: data.string.c3r4
				},

				//4th row
				{
					c1: data.string.c1r5,
					c2: data.string.c2r5,
					c3: data.string.c3r5
				},

				//5th row
				{
					c1: data.string.c1r6,
					c2: data.string.c2r6,
					c3: data.string.c3r6
				},
				//6h row
				{
					c1: data.string.c1r7,
					c2: data.string.c2r7,
					c3: data.string.c3r7
				},
				//7th row
				{
					c1: data.string.c1r8,
					c2: data.string.c2r8,
					c3: data.string.c3r8
				},
				]
			}
		],
		lowertextblockadditionalclass: "moreinfo",
  	lowertextblock:[{
  			textclass: "heading",
  			textdata: data.string.tip1
  		},{
	  			textclass: "heading",
	  			textdata: data.string.tip2
	  		},{
		  			textclass: "heading",
		  			textdata: data.string.tip3
		  		},{
  			textclass: "close_button",
  			textdata: "&nbsp;"
  		}
  	],
		learn_more: true
	},
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-amethyst',
		uppertextblock:[
			{
				textclass: "p2text",
				textdata: data.string.p2text3
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}],
		tableblock : [
		{
			tableclass: "mytable",
			checkclass: "check",
			check: data.string.p2text22,
			colclass1: "column1",
			tableheading: [
			{
				h1: data.string.h1,
				h2: data.string.h2,
				h3: data.string.h3
			}],
			tablebody: [
				//1st row
				{
					c1: data.string.o2c1r1,
					c2: data.string.o2c2r1,
					c3: data.string.o2c3r1
				},

				//2nd row
				{
					c1: data.string.o2c1r2,
					c2: data.string.o2c2r2,
					c3: data.string.o2c3r2
				},

				//3rd row
				{
					c1: data.string.o2c1r3,
					c2: data.string.o2c2r3,
					c3: data.string.o2c3r3
				},

				//4th row
				{
					c1: data.string.o2c1r4,
					c2: data.string.o2c2r4,
					c3: data.string.o2c3r4
				},

				//5th row
				{
					c1: data.string.o2c1r5,
					c2: data.string.o2c2r5,
					c3: data.string.o2c3r5
				},

				//6th row
				{
					c1: data.string.o2c1r6,
					c2: data.string.o2c2r6,
					c3: data.string.o2c3r6
				},
				//7th row
				{
					c1: data.string.o2c1r7,
					c2: data.string.o2c2r7,
					c3: data.string.o2c3r7
				},
				//8th row
				{
					c1: data.string.o2c1r8,
					c2: data.string.o2c2r8,
					c3: data.string.o2c3r8
				},
				]
			}
		],
		lowertextblockadditionalclass: "moreinfo",
  	lowertextblock:[{
  			textclass: "heading",
  			textdata: data.string.tip4
  		},{
		  		datahighlightflag: true,
		  		datahighlightcustomclass: "boldit",
	  			textclass: "heading",
	  			textdata: data.string.tip5
	  		},{
  			textclass: "close_button",
  			textdata: "&nbsp;"
  		}
  	],
		learn_more: true
	},
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-amethyst',
		uppertextblock:[
			{
				textclass: "p2text",
				textdata: data.string.p2text4
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}],
		tableblock : [
		{
			tableclass: "mytable",
			checkclass: "check",
			check: data.string.p2text22,
			colclass1: "column1",
			tableheading: [
			{
				h1: data.string.h1,
				h2: data.string.h2,
				h3: data.string.h3
			}],
			tablebody: [
				//1st row
				{
					c1: data.string.o3c1r1,
					c2: data.string.o3c2r1,
					c3: data.string.o3c3r1
				},

				//2nd row
				{
					c1: data.string.o3c1r2,
					c2: data.string.o3c2r2,
					c3: data.string.o3c3r2
				},

				//3rd row
				{
					c1: data.string.o3c1r3,
					c2: data.string.o3c2r3,
					c3: data.string.o3c3r3
				},

				//4th row
				{
					c1: data.string.o3c1r4,
					c2: data.string.o3c2r4,
					c3: data.string.o3c3r4
				},

				//4th row
				{
					c1: data.string.o3c1r5,
					c2: data.string.o3c2r5,
					c3: data.string.o3c3r5
				},

				//5th row
				{
					c1: data.string.o3c1r6,
					c2: data.string.o3c2r6,
					c3: data.string.o3c3r6
				},
				//6th row
				{
					c1: data.string.o3c1r7,
					c2: data.string.o3c2r7,
					c3: data.string.o3c3r7
				},
				//7th row
				{
					c1: data.string.o3c1r8,
					c2: data.string.o3c2r8,
					c3: data.string.o3c3r8
				},
				]
			}
		],
		lowertextblockadditionalclass: "moreinfo",
  	lowertextblock:[{
  			textclass: "heading",
  			textdata: data.string.tip4
  		},{
		  		datahighlightflag: true,
		  		datahighlightcustomclass: "boldit",
	  			textclass: "heading",
	  			textdata: data.string.tip5
	  		},{
  			textclass: "close_button",
  			textdata: "&nbsp;"
  		}
  	],
	},
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-amethyst',
		uppertextblock:[
			{
				textclass: "p2text",
				textdata: data.string.p2text5
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}],
		tableblock : [
		{
			tableclass: "mytable",
			checkclass: "check",
			check: data.string.p2text22,
			colclass1: "column1",
			tableheading: [
			{
				h1: data.string.h1,
				h2: data.string.h2,
				h3: data.string.h3
			}],
			tablebody: [
				//1st row
				{
					c1: data.string.o4c1r1,
					c2: data.string.o4c2r1,
					c3: data.string.o4c3r1
				},

				//2nd row
				{
					c1: data.string.o4c1r2,
					c2: data.string.o4c2r2,
					c3: data.string.o4c3r2
				},

				//4th row
				{
					c1: data.string.o4c1r4,
					c2: data.string.o4c2r4,
					c3: data.string.o4c3r4
				},

				//4th row
				{
					c1: data.string.o4c1r5,
					c2: data.string.o4c2r5,
					c3: data.string.o4c3r5
				}
				]
			}
		],
		lowertextblockadditionalclass: "moreinfo",
  	lowertextblock:[{
  			textclass: "heading",
  			textdata: data.string.tip4
  		},{
		  		datahighlightflag: true,
		  		datahighlightcustomclass: "boldit",
	  			textclass: "heading",
	  			textdata: data.string.tip5
	  		},{
  			textclass: "close_button",
  			textdata: "&nbsp;"
  		}
  	],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "girl-1", src: imgpath+"girl_talking.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tl-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p2_1", src: soundAsset+"s2_p2_1.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p3_1", src: soundAsset+"s2_p3_1.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	 function navigationcontroller(islastpageflag){
		 typeof islastpageflag === "undefined" ?
		 islastpageflag = false :
		 typeof islastpageflag != 'boolean'?
		 alert("NavigationController : Hi Master, please provide a boolean parameter") :
		 null;

			 if(countNext == 0 && $total_page!=1){
			 // $nextBtn.show(0);
			 $prevBtn.css('display', 'none');
			 }

			 else if($total_page == 1){
			 $prevBtn.css('display', 'none');
			 $nextBtn.css('display', 'none');

			 // if lastpageflag is true
			 islastpageflag ?
			 ole.footerNotificationHandler.lessonEndSetNotification() :
			 ole.footerNotificationHandler.lessonEndSetNotification() ;
			 }

			 else if(countNext > 0 && countNext < $total_page-1){
			 $nextBtn.show(0);
			 $prevBtn.show(0);
			 }

			 else if(countNext == $total_page-1){
			 $nextBtn.css('display', 'none');
			 $prevBtn.show(0);

			 // if lastpageflag is true
			 // islastpageflag ?
			 // ole.footerNotificationHandler.lessonEndSetNotification() :
			 // ole.footerNotificationHandler.pageEndSetNotification() ;
			 }
	 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		vocabcontroller.findwords(countNext);

		$(".close_button").click(function(){
			$nextBtn.show(0);
			$(".moreinfo").hide(500);
		});
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		switch (countNext) {
			case 1:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
					$learn_more = $(".learn_more");
					$learn_more.click(function(){
						$(".close_button").hide(0);
						$(".moreinfo").show(500);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s2_p"+(countNext+1)+"_1");
							current_sound.play();
							current_sound.on('complete', function(){
								$(".close_button").show(0);
								nav_button_controls(300);
							});
					});
				});
			// $learn_more = $(".learn_more");
			// $learn_more.click(function(){
			// 	$(".moreinfo").show(500);
			// });
			break;
				case 2:
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p"+(countNext+1));
					current_sound.play();
					current_sound.on('complete', function(){
						$learn_more = $(".learn_more");
						$learn_more.click(function(){
							$(".close_button").hide(0);
							$(".moreinfo").show(500);
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s2_p"+(countNext+1)+"_1");
								current_sound.play();
								current_sound.on('complete', function(){
									$(".close_button").show(0);
									nav_button_controls(300);
								});
						});
					});
				// $nextBtn.hide(0);
				// $learn_more = $(".learn_more");
				// $learn_more.click(function(){
				// 	$(".moreinfo").show(500);
				// });
			break;
			default:
				sound_player("s2_p"+(countNext+1),1);
			break;


		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){

		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent1 = content[count].livinnonlivin[0];
			if(lncontent1.hasOwnProperty('imageblock')){
				var imageblock = lncontent1.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent2 = content[count].livinnonlivin[1];
			if(lncontent2.hasOwnProperty('imageblock')){
				var imageblock = lncontent2.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

			var lncontent3 = content[count].livinnonlivin[2];
			if(lncontent3.hasOwnProperty('imageblock')){
				var imageblock = lncontent3.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

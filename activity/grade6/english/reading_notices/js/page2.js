var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var soundcontent;

var content=[
	{
		// slide0
		uppertextblock:[{
			textclass: "description_main",
			textdata: data.string.lesson_title
		}]

	},{
		// slide1
		contentnocenteradjust: true,
		imageblock: [{
			imgcontainerdiv: "shrunk",
			imagestoshow: [{
				imgclass: "notice_board",
				imgid: "noticeboard"
			},{
				imgclass: "notice",
				imgid: "culturalprogram"
			}]
		},{
			imagestoshow: [{
				imgclass: "dolma_class",
				imgid: "dolma"
			},{
				imgclass: "rita_class",
				imgid: "rita"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialogue1",
			dialogimageclass: "dialogue_bubble_mirror",
			dialogueimgid: "textbox01b",
			dialogcontentclass: "dialogue_description",
			dialogcontent: data.string.p2_s0
		},{
			dialogcontainer: "dialogue2",
			dialogimageclass: "dialogue_bubble",
			dialogueimgid: "textbox02b",
			dialogcontentclass: "dialogue_description",
			dialogcontent: data.string.p2_s1
		}]

	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main2",
			textdata: data.string.p2_s2
		}],
		imageblock: [{
			imagestoshow: [{
				imgclass: "notice_animate",
				imgid: "culturalprogram01"
			},{
				imgclass: "dolma_thinking",
				imgid: "dolma05"
			}],
			imagelabels: [{
				imagelabelclass: "circular1",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "circular2",
				imagelabeldata: "&nbsp;"
			}]
		},{
			imgcontainerdiv: "container_right_options ",
			imagelabels: [{
				imagelabelclass: "options",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "options",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "options yes",
				imagelabeldata: data.string.p2_s6
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog",
			dialogimageclass: "dialog_image1 ",
			dialogueimgid: "clouds",
			dialogcontentclass: "textaligh_center",
			dialogcontent: data.string.p2_s3
		}]

	},{
		// slide3
		contentnocenteradjust: true,

		imageblock: [{
			imgcontainerdiv: "mustard_bg",
			imagestoshow: [{
				imgclass: "dolma02_b",
				imgid: "dolma02"
			}],
			imagelabels: [{
				imagelabelclass: "mustard_bg_label",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_pink_hilight",
				imagelabeldata: data.string.p2_s7
			}]
		},{
			// imagestoshow: [{
				// imgclass: "notice_animated1",
				// imgid: "culturalprogram01"
			// }],
			imagelabels: [{
				imagelabelclass: "mustard_bg_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_pink_hilight",
				imagelabeldata: data.string.p2_s8
			}]
		},{
			imgcontainerdiv: "container_center pink_border",
			imagelabels: [{
				imagelabelclass: "options yes",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "options",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "options",
				imagelabeldata: data.string.p2_s11
			}]
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		imageblock: [{
			imgcontainerdiv: "shrunk",
			imagestoshow: [{
				imgclass: "notice_board",
				imgid: "noticeboard"
			},{
				imgclass: "notice",
				imgid: "culturalprogram"
			}]
		},{
			imagestoshow: [{
				imgclass: "dolma_class",
				imgid: "dolma"
			},{
				imgclass: "rita_class",
				imgid: "rita"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialogue1",
			dialogimageclass: "dialogue_bubble_mirror",
			dialogueimgid: "textbox01b",
			dialogcontentclass: "dialogue_description",
			dialogcontent: data.string.p2_s20
		},{
			dialogcontainer: "dialogue2",
			dialogimageclass: "dialogue_bubble",
			dialogueimgid: "textbox02b",
			dialogcontentclass: "dialogue_description",
			dialogcontent: data.string.p2_s21
		}]

	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main3",
			textdata: data.string.p2_s14
		}],
		imageblock: [{
			imgcontainerdiv: "container_left ",
			imagestoshow: [{
				imgclass: "notice_animated2",
				imgid: "culturalprogram01"
			}]
		},{
			imgcontainerdiv: "container_right pink_border",
			imagelabels: [{
				imagelabelclass: "options yes",
				imagelabeldata: data.string.p2_s15
			},{
				imagelabelclass: "options",
				imagelabeldata: data.string.p2_s16
			},{
				imagelabelclass: "options",
				imagelabeldata: data.string.p2_s17
			}]
		}]

	},{
		// slide6
		contentnocenteradjust: true,
		imageblock: [{
			imgcontainerdiv: "shrunk",
			imagestoshow: [{
				imgclass: "notice_board",
				imgid: "noticeboard"
			},{
				imgclass: "notice",
				imgid: "culturalprogram"
			}]
		},{
			imagestoshow: [{
				imgclass: "dolma_class",
				imgid: "dolma"
			},{
				imgclass: "rita_class",
				imgid: "rita"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialogue1",
			dialogimageclass: "dialogue_bubble_mirror",
			dialogueimgid: "textbox01b",
			dialogcontentclass: "dialogue_description",
			dialogcontent: data.string.p2_s18
		},{
			dialogcontainer: "dialogue2",
			dialogimageclass: "dialogue_bubble",
			dialogueimgid: "textbox02b",
			dialogcontentclass: "dialogue_description",
			dialogcontent: data.string.p2_s19
		}]

	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "noticeboard", src: imgpath+"noticeboard.png", type: createjs.AbstractLoader.IMAGE},
			{id: "culturalprogram", src: imgpath+"culturalprogram.png", type: createjs.AbstractLoader.IMAGE},
			{id: "culturalprogram01", src: imgpath+"culturalprogram01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma", src: imgpath+"dolma.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma01", src: imgpath+"dolma01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma02", src: imgpath+"dolma02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma05", src: imgpath+"dolma05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma03a", src: imgpath+"dolma03a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rita", src: imgpath+"rita.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rita01", src: imgpath+"rita01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rita02", src: imgpath+"rita02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "musicnode01", src: imgpath+"musicnode01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "musicnode02", src: imgpath+"musicnode02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "musicnode03", src: imgpath+"musicnode03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ribon_blue", src: imgpath+"ribon_blue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ribon_green", src: imgpath+"ribon_green.png", type: createjs.AbstractLoader.IMAGE},
			{id: "star_bg", src: imgpath+"star_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox01b", src: imgpath+"textbox01b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox02b", src: imgpath+"textbox02b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dad", src: imgpath+"dad.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1_0", src: soundAsset+"p2_s1_0.ogg"},
			{id: "sound_1_1", src: soundAsset+"p2_s1_1.ogg"},
			{id: "sound_2_0", src: soundAsset+"p2_s2_0.ogg"},
			{id: "sound_2_1", src: soundAsset+"p2_s2_1.ogg"},
			{id: "sound_3_0", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_3_1", src: soundAsset+"p2_s3_1.ogg"},
			{id: "sound_4_0", src: soundAsset+"p2_s4_0.ogg"},
			{id: "sound_4_1", src: soundAsset+"p2_s4_1.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6_0", src: soundAsset+"p2_s6_0.ogg"},
			{id: "sound_6_1", src: soundAsset+"p2_s6_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var animationinprogress = false;

  var sound_data;
  var timeourcontroller;

  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));

	    function addClicker(selector, correctcountmax, in_correct_sign_idx){
	    	var allanswered = false;
	    	var correctcount = 0;
	    	var $selector = $("."+selector);

	    	if($selector.length < 5){
				var randomindex;
		    	var parentClass = ($selector.parent().attr('class').split(/\s+/))[0];
		    	var $parent = $("."+parentClass);
		    	$parent.html("");
		    	while($selector.length >0){
		    		randomindex = Math.floor(Math.random() * ($selector.length + 1));
			    	$parent.append($($selector[randomindex]));
			    	$selector.splice(randomindex, 1);
		    	}
			}

	    	$("."+selector).click(function(){
	    		if(allanswered){
	    			return allanswered;
	    		}
	    		var $this = $(this);
	    		if ($this.hasClass("yes") && !($this.hasClass("clicked"))) {
	    			$this.addClass("clicked");
	    			play_correct_incorrect_sound(true);
	    			$this.addClass("correct");
	    			$this.append('<img class="correctsign'+ in_correct_sign_idx+'" src = "'+ preload.getResult("correct").src +'">');
	    			correctcount++;
	    			if(correctcount >= correctcountmax){
	    				allanswered = true;
		    			navigationcontroller();
	    				switch(selector){
	    					case "grey_hilight":
			    				$("."+selector).addClass("disable1");
	    						break;
	    					case "options":
			    				$("."+selector).addClass("disable2");
	    						break;
	    					default:
	    						break;
	    				}
	    			}
	    		} else if (!($this.hasClass("clicked"))) {
	    			$this.addClass("clicked");
	    			$this.append('<img class="incorrectsign'+ in_correct_sign_idx+'" src = "'+ preload.getResult("wrong").src +'">');
	    			play_correct_incorrect_sound(false);
	    			$this.addClass("incorrect");
	    		}
    		});
	    }

	    switch(countNext){
	    	case 1:
	    		$(".notice_board").css("opacity", "0.6");
	    		var arraysound = ["sound_"+countNext+"_0", "sound_"+countNext+"_1"];
	    		$(".dialogue2").hide(0);
	    		play_sound_sequence(arraysound, true, ["", "dialogue2"]);
	    		break;
	    	// case 3:
	    		// var arraysound = ["sound_"+countNext+"_0", "sound_"+countNext+"_1"];
	    		// $(".mustard_bg_label2").hide(0);
	    		// play_sound_sequence(arraysound, true, ["", "mustard_bg_label2"]);
	    		// break;
	    	case 4:
	    		$(".notice_board").css("opacity", "0.6");
	    		var arraysound = ["sound_"+countNext+"_0", "sound_"+countNext+"_1"];
	    		$(".dialogue2").hide(0);
	    		play_sound_sequence(arraysound, true, ["", "dialogue2"]);
	    		break;
	    	case 6:
    			$(".notice_board").css("opacity", "0.6");
    			$(".dialogue2").hide(0);
	    		var arraysound = ["sound_"+countNext+"_0", "sound_"+countNext+"_1"];
	    		play_sound_sequence(arraysound, true, ["", "dialogue2"]);
	    		break;
	    	case 5:
	    		sound_player("sound_"+countNext, false);
	    		addClicker("options", 1, "2");
	    		break;
	    	case 3:
	    		var arraysound = ["sound_"+countNext+"_0", "sound_"+countNext+"_1"];
	    		$(".mustard_bg_label2").hide(0);
	    		play_sound_sequence(arraysound, false, ["", "mustard_bg_label2"]);
	    		addClicker("options", 1, "2");
	    		break;
	    	case 2:
	    		$(".container_right_options, .dialog, .dolma_thinking, .circular1, .circular2").hide(0);
	    		$(".dialog, .circular1, .circular2").addClass("bounceIn");
	    		timeourcontroller = setTimeout(function(){
		    		$(".dolma_thinking").show(0);
		    		$(".circular1").delay(400).show(0);
		    		$(".circular2").delay(800).show(0);
		    		$(".dialog").delay(1200).show(0);
		    		$(".container_right_options").delay(1600).show(0);
	    		}, 8600);
	    		var arraysound = ["sound_"+countNext+"_0", "sound_"+countNext+"_1"];
	    		play_sound_sequence(arraysound, false);
	    		addClicker("options", 1, "2");
	    		break;
	    	default:
	    		sound_player("sound_"+countNext, true);
	    		break;
	    }
   }


/*=====  End of Templates Block  ======*/

function play_sound_sequence(soundarray, navflag, elementarray){
	if(elementarray == null){
		elementarray = [];
	}
	if(elementarray.length > 0){
		(elementarray[0] == "")? true : $("."+elementarray[0]).fadeIn(300);
	}
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(soundarray[0]);
	soundarray.splice( 0, 1);
	current_sound.on("complete", function(){
		if(soundarray.length > 0){
			elementarray.splice(0, 1);
			play_sound_sequence(soundarray, navflag, elementarray);
		}else{
			if(navflag)
			navigationcontroller();
		}

	});
}

function sound_player(sound_id, navflag){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(navflag)
			navigationcontroller();
		});
	}


function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio_en/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "exercisestart",
		div:true,
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.exercise_title
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "tipicon tipclick",
                    imgclass: "relativecls tipiconimg ",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "hand tipclick",
                    imgclass: "relativecls handimg",
                    imgid: 'handImg',
                    imgsrc: ""
                }
            ]
        }],
        tipblock:[
            {
                textdiv:"tipdiv zoomInEffect",
                textclass:"subtopic centertext",
                textdata:data.string.exercise_title2
            }
        ]
    },
	//slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle",
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.e_s
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftcolour1",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour1",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
            ]
        }],
		leftdiv:[
			{
                textdiv: "droppable drop1",
				textclass:"box1 relativecls centertext",
                textdata: "",
                placeholdermsg:data.string.e_s1,
				ans:data.string.e_s2
			},
            {
                textdiv: "droppable drop2",
                textclass:"box1 relativecls centertext",
                textdata: "",
                placeholdermsg:data.string.e_s3,
                ans:data.string.e_s4

            },
            {
                textdiv: "droppable drop3",
                textclass:"box1 relativecls centertext",
                textdata: "",
                placeholdermsg:data.string.e_s5,
                ans:data.string.e_s6
            },
            {
                textdiv: "droppable drop4",
                textclass:"box1 relativecls centertext",
                textdata: "",
                placeholdermsg:data.string.e_s7,
                ans:data.string.e_s8
            },
            {
                textdiv: "droppable drop5",
                textclass:"box1 relativecls centertext",
                textdata: "",
                placeholdermsg:data.string.e_s9,
                ans:data.string.e_s10
            },
            {
                textdiv: "info1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s3
            },
            {
                textdiv: "info2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s5
            },
            {
                textdiv: "info3",
                textclass:"box1 centertext relativecls",
                textdata: data.string.e_s7
            },
          ],
		rightdiv:[
            {
                textdiv: "dragtitle",
                textclass:"box1 centertext relativecls",
                textdata:data.string.e_s0
            },
            {
                textdiv: "draggable opt1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s2
            },
            {
                textdiv: "draggable opt2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s4
            },
            {
                textdiv: "draggable opt3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s6
            },
            {
                textdiv: "draggable opt4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s8
            },
            {
                textdiv: "draggable opt5",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s10
            },
		]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "answer",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pink",
                    imgclass: "relativecls pinkballonimg ",
                    imgid: 'pinkballonImg',
                    imgsrc: ""
                },
				{
                    imgdiv: "blue",
                    imgclass: "relativecls blueballonimg",
                    imgid: 'blueballonImg',
                    imgsrc: ""
				}
            ]
        }],
        textblock:[
            {
                textdiv: "droppable1 ans1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s2,
            },
            {
                textdiv: "droppable1 ans2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s4,

            },
            {
                textdiv: "droppable1 ans3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s6
            },
            {
                textdiv: "droppable1 ans4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s8,
            },
            {
                textdiv: "droppable1 ans5",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s10,
            },
            {
                textdiv: "ansinfo ansinfo1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s3
            },
            {
                textdiv: "ansinfo ansinfo2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.e_s5
            },
            {
                textdiv: "ansinfo ansinfo3",
                textclass:"box2 centertext relativecls",
                textdata: data.string.e_s7
            },
        ],

    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery",
		ans:data.string.q1_o1,
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftcolour1",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour1",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
            ]
        }],
        leftdiv:[
            {
                textdiv: "droppable drop1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s2
            },
            {
                textdiv: "droppable drop2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s4
            },
            {
                textdiv: "droppable drop3",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s6
            },
            {
                textdiv: "droppable drop4",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s8
            },
            {
                textdiv: "droppable drop5",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s10
            },
            {
                textdiv: "info1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s3
            },
            {
                textdiv: "info2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s5
            },
            {
                textdiv: "info3",
                textclass:"box1 centertext relativecls",
                textdata: data.string.e_s7
            },
        ],
        rightdiv:[
            {
                textdiv: "dragtitle",
                textclass:"box1 centertext relativecls",
                textdata:data.string.e_s00
            },
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q1_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q1_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q1_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q1_o4
            }
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery",
        ans:data.string.q2_o1,
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q2
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftcolour1",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour1",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
            ]
        }],
        leftdiv:[
            {
                textdiv: "droppable drop1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s2
            },
            {
                textdiv: "droppable drop2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s4
            },
            {
                textdiv: "droppable drop3",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s6
            },
            {
                textdiv: "droppable drop4",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s8
            },
            {
                textdiv: "droppable drop5",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s10
            },
            {
                textdiv: "info1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s3
            },
            {
                textdiv: "info2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s5
            },
            {
                textdiv: "info3",
                textclass:"box1 centertext relativecls",
                textdata: data.string.e_s7
            },
        ],
        rightdiv:[
            {
                textdiv: "dragtitle",
                textclass:"box1 centertext relativecls",
                textdata:data.string.e_s00
            },
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q2_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q2_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q2_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q2_o4
            }
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery",
        ans:data.string.q3_o1,
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftcolour1",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour1",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
            ]
        }],
        leftdiv:[
            {
                textdiv: "droppable drop1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s2
            },
            {
                textdiv: "droppable drop2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s4
            },
            {
                textdiv: "droppable drop3",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s6
            },
            {
                textdiv: "droppable drop4",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s8
            },
            {
                textdiv: "droppable drop5",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s10
            },
            {
                textdiv: "info1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s3
            },
            {
                textdiv: "info2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s5
            },
            {
                textdiv: "info3",
                textclass:"box1 centertext relativecls",
                textdata: data.string.e_s7
            },
        ],
        rightdiv:[
            {
                textdiv: "dragtitle",
                textclass:"box1 centertext relativecls",
                textdata:data.string.e_s00
            },
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q3_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q3_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q3_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q3_o4
            }
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery",
        ans:data.string.q4_o1,
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftcolour1",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour1",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
            ]
        }],
        leftdiv:[
            {
                textdiv: "droppable drop1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s2
            },
            {
                textdiv: "droppable drop2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s4
            },
            {
                textdiv: "droppable drop3",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s6
            },
            {
                textdiv: "droppable drop4",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s8
            },
            {
                textdiv: "droppable drop5",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s10
            },
            {
                textdiv: "info1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s3
            },
            {
                textdiv: "info2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s5
            },
            {
                textdiv: "info3",
                textclass:"box1 centertext relativecls",
                textdata: data.string.e_s7
            },
        ],
        rightdiv:[
            {
                textdiv: "dragtitle",
                textclass:"box1 centertext relativecls",
                textdata:data.string.e_s00
            },
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q4_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q4_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q4_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q4_o4
            }
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery",
        ans:data.string.q5_o1,
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q5
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftcolour1",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour1",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
            ]
        }],
        leftdiv:[
            {
                textdiv: "droppable drop1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s2
            },
            {
                textdiv: "droppable drop2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s4
            },
            {
                textdiv: "droppable drop3",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s6
            },
            {
                textdiv: "droppable drop4",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s8
            },
            {
                textdiv: "droppable drop5",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s10
            },
            {
                textdiv: "info1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s3
            },
            {
                textdiv: "info2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s5
            },
            {
                textdiv: "info3",
                textclass:"box1 centertext relativecls",
                textdata: data.string.e_s7
            },
        ],
        rightdiv:[
            {
                textdiv: "dragtitle",
                textclass:"box1 centertext relativecls",
                textdata:data.string.e_s00
            },
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q5_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q5_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q5_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q5_o4
            }
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery",
        ans:data.string.q9_o1,
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q9
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftcolour1",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour1",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
            ]
        }],
        leftdiv:[
            {
                textdiv: "droppable drop1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s2
            },
            {
                textdiv: "droppable drop2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s4
            },
            {
                textdiv: "droppable drop3",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s6
            },
            {
                textdiv: "droppable drop4",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s8
            },
            {
                textdiv: "droppable drop5",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s10
            },
            {
                textdiv: "info1",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s3
            },
            {
                textdiv: "info2",
                textclass:"box1 relativecls centertext",
                textdata: data.string.e_s5
            },
            {
                textdiv: "info3",
                textclass:"box1 centertext relativecls",
                textdata: data.string.e_s7
            },
        ],
        rightdiv:[
            {
                textdiv: "dragtitle",
                textclass:"box1 centertext relativecls",
                textdata:data.string.e_s00
            },
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q9_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q9_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q9_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q9_o4
            }
        ]
    },

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery heightadjust",
        ans:data.string.q6_o1,
        rightdivclass:"coverallpart",
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q6
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pinkballooon",
                    imgclass: "relativecls pinkballonimg ",
                    imgid: 'pinkballonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "blueballoon",
                    imgclass: "relativecls blueballonimg",
                    imgid: 'blueballonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "leftcolour",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },

            ]
        }],
        rightdiv:[
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q6_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q6_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q6_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q6_o4
            }
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery heightadjust",
        ans:data.string.q7_o1,
        rightdivclass:"coverallpart",
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q7
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pinkballooon",
                    imgclass: "relativecls pinkballonimg ",
                    imgid: 'pinkballonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "blueballoon",
                    imgclass: "relativecls blueballonimg",
                    imgid: 'blueballonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "leftcolour",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },

            ]
        }],
        rightdiv:[
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q7_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q7_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q7_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q7_o4
            }
        ]
    },
    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery heightadjust",
        ans:data.string.q8_o1,
        rightdivclass:"coverallpart",
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q8
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pinkballooon",
                    imgclass: "relativecls pinkballonimg ",
                    imgid: 'pinkballonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "blueballoon",
                    imgclass: "relativecls blueballonimg",
                    imgid: 'blueballonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "leftcolour",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },

            ]
        }],
        rightdiv:[
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q8_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q8_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q8_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q8_o4
            }
        ]
    },
    //slide 11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"exercisetitle mainquery heightadjust",
        ans:data.string.q9_o1,
        rightdivclass:"coverallpart",
        uppertextblock: [
            {
                textclass: "box centertext",
                textdata: data.string.q9zz
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pinkballooon",
                    imgclass: "relativecls pinkballonimg ",
                    imgid: 'pinkballonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "blueballoon",
                    imgclass: "relativecls blueballonimg",
                    imgid: 'blueballonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "leftcolour",
                    imgclass: "relativecls img1 ",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rightcolour",
                    imgclass: "relativecls img2",
                    imgid: 'colourdotsImg',
                    imgsrc: ""
                },

            ]
        }],
        rightdiv:[
            {
                textdiv: "option o1",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q9_o1
            },
            {
                textdiv: "option o2",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q9_o2
            },
            {
                textdiv: "option o3",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q9_o3
            },
            {
                textdiv: "option o4",
                textclass:"box2 relativecls centertext",
                textdata: data.string.q9_o4
            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    // loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    var score = new NumberTemplate();
    score.init(11);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "pinkballonImg", src: imgpath+"pink_balloon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "blueballonImg", src: imgpath+"blue_balloon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "colourdotsImg", src: imgpath+"colourdots.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tipiconImg", src: "images/tip_icon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handImg", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

            {id: "sound_0", src: soundAsset+"exe_1.ogg"},
            {id: "sound_op", src: soundAsset+"clickcor.ogg"},
            {id: "sound_1", src: soundAsset+"exe_celebration.ogg"},
            {id: "sound_2", src: soundAsset+"Quick_tip.ogg"},
            {id: "s2_p2", src: soundAsset+"exc.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templatecaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        $(".ex-number-template-score").show();
        vocabcontroller.findwords(countNext);
        countNext<13?put_image(content, countNext):'';
        score.numberOfQuestions();
        countNext==3 ? sound_player("sound_op",false) : '';

        switch(countNext){
            case 0:
                sound_player("sound_0",false);
                $(".ex-number-template-score,.tipdiv").hide();
                $(".hand").delay(2000).animate({"opacity":"1"},1000);
                $(".tipclick").click(function(){
                    sound_player("sound_2",true);
                    $(".tipclick").hide();
                    $(".tipdiv").show();
                });
                break;
            case 1:
                count=0;
                    sound_player("s2_p2",false);
                shufflehint();
                dragdrop();
                break;
            default:
                shufflehint1();
                checkans();
                $(".leftdiv >div").css({"background-color":"white","border":"none"});
                break;
        }
    }
    function put_image(content, count) {
        if (content[count].hasOwnProperty('imageblock')) {
            for (var j = 0; j < content[count].imageblock.length; j++) {
                var imageblock = content[count].imageblock[j];
                if (imageblock.hasOwnProperty('imagestoshow')) {
                    var imageClass = imageblock.imagestoshow;
                    for (var i = 0; i < imageClass.length; i++) {
                        var image_src = preload.getResult(imageClass[i].imgid).src;
                        //get list of classes
                        var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                        var selector = ('.' + classes_list[classes_list.length - 1]);
                        $(selector).attr('src', image_src);
                    }
                }
            }
        }
    }
    function navigationcontroller(islastpageflag){
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():"";
        });
    }


    function templatecaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        // loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templatecaller();
                (countNext!=1 && countNext!=3)?score.gotoNext():'';
                break;
        }
    });
    $refreshBtn.click(function(){
        templatecaller();
    });
    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templatecaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint(){
        var optdiv = $(".rightdiv");

        for (var i = optdiv.find(".draggable").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".draggable").eq(Math.random() * i | 0));
        }
        optdiv.find(".draggable").removeClass().addClass("current");
        var optionclass = ["draggable opt1","draggable opt2","draggable opt3","draggable opt4","draggable opt5"]
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function shufflehint1(){
        var optdiv = $(".rightdiv");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        optdiv.find(".option").removeClass().addClass("current");
        var optionclass = ["option o1","option o2","option o3","option o4"]
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function dragdrop(){
        var wrongclick1 = false;
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event,ui) {
                if(ui.draggable.text().toString().trim() == ($(this).attr("data-answer").toString().trim())) {
                    createjs.Sound.stop();
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).find("p").text(ui.draggable.text());
                    $(this).addClass("correctcss");
                    $(this).append("<img class='correctImg' src='images/right.png'/>");
                    count++;
                    if(count>4){
                        countNext++;
                        templatecaller();
                        // navigationcontroller();
                        !wrongclick1?score.update(true):'';
                        sound_player("sound_1",true)
                    }
                }
                else {
                    createjs.Sound.stop();
                    play_correct_incorrect_sound(0);
                    $(this).append("<img class='wrongImg' src='images/wrong.png'/>");
                    ui.draggable.addClass("wrongcss");
                    $(this).addClass("wrongcss");
                    score.update(false);
                    wrongclick1 = true;
                }
            }
        });
    }
    function checkans(){
        var wrongclick = false;
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).find('p').text().trim()==$(".mainquery").attr("data-answer").toString().trim()) {
                $(this).addClass("correctcss");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='correctImg' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                !wrongclick?score.update(true):'';
                navigationcontroller();
            }
            else{
                $(this).addClass("wrongcss");
                $(this).append("<img class='wrongImg' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
                score.update(false);
                wrongclick = true;
            }
        });
    }
});

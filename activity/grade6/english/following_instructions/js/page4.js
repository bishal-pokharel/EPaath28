var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image",
				imgid: "a_22"
			}]
		}],
		lowertextblocknocenteradjust: true,
		lowertextblock: [{
			textclass: "description_instruction",
			textdata: data.string.p4_s0
		}]

	},{
		// slide1
		contentblockadditionalclass: "grey_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p4_s1
		}],
		imageblock:[{
			imgcontainerdiv: "imagecontainer",
			imagestoshow: [{
				imgclass: "image1",
				imgid: "boat"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				imagelabeldata: data.string.p4_s2
			}]
		},{
			imgcontainerdiv: "imagecontainer",
			imagestoshow: [{
				imgclass: "image2",
				imgid: "fish"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				imagelabeldata: data.string.p4_s3
			}]
		},{
			imgcontainerdiv: "imagecontainer",
			imagestoshow: [{
				imgclass: "image3",
				imgid: "tulip"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				imagelabeldata: data.string.p4_s4
			}]
		}]
	}
];

var content_boat = [{
		// slide0
		contentblockadditionalclass : "green_bg",
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		uppertextblock: [{
			textclass: "description_instruction2",
			textdata: data.string.p4_s2_ucase
		}]
	},{
		// slide1
		contentblockadditionalclass: "green_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "instruction_container",
		uppertextblock: [{
			textclass: "description2 des4_1",
			textdata: data.string.p4_s5
		},{
			textclass: "description2 des4_2",
			textdata: data.string.p4_s6
		},{
			textclass: "description2 des4_3",
			textdata: data.string.p4_s7
		},{
			textclass: "description2 des4_4",
			textdata: data.string.p4_s8
		}],
		imageblock:[{
			imgcontainerdiv: "instruction_imagecontainer",
			imagestoshow: [{
				imgclass: "image4",
				imgid: "boat01"
			}]
		}]
	},{
		// slide2
		contentblockadditionalclass: "green_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "instruction_container",
		uppertextblock: [{
			textclass: "description2 des4_1",
			textdata: data.string.p4_s9
		},{
			textclass: "description2 des4_2",
			textdata: data.string.p4_s10
		},{
			textclass: "description2 des4_3",
			textdata: data.string.p4_s11
		},{
			textclass: "description2 des4_4",
			textdata: data.string.p4_s12
		}],
		imageblock:[{
			imgcontainerdiv: "instruction_imagecontainer",
			imagestoshow: [{
				imgclass: "image4",
				imgid: "boat02"
			}]
		}]
	}];

var content_fish = [{
		// slide0
		contentblockadditionalclass : "grey_bg",
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		uppertextblock: [{
			textclass: "description_instruction2",
			textdata: data.string.p4_s3_ucase
		}]
	},{
		// slide1
		contentblockadditionalclass: "grey_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "instruction_container",
		uppertextblock: [{
			textclass: "description2 des3_1",
			textdata: data.string.p4_s13
		},{
			textclass: "description2 des3_2",
			textdata: data.string.p4_s14
		},{
			textclass: "description2 des3_3",
			textdata: data.string.p4_s15
		}],
		imageblock:[{
			imgcontainerdiv: "instruction_imagecontainer",
			imagestoshow: [{
				imgclass: "image4_b",
				imgid: "fish01"
			}]
		}]
	},{
		// slide2
		contentblockadditionalclass: "grey_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "instruction_container",
		uppertextblock: [{
			textclass: "description2 des2_1",
			textdata: data.string.p4_s16
		},{
			textclass: "description2 des2_2",
			textdata: data.string.p4_s17
		}],
		imageblock:[{
			imgcontainerdiv: "instruction_imagecontainer",
			imagestoshow: [{
				imgclass: "image4_b",
				imgid: "fish02"
			}]
		}]
	}];

var content_tulip = [{
		// slide0
		contentblockadditionalclass : "peach_bg",
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		uppertextblock: [{
			textclass: "description_instruction2",
			textdata: data.string.p4_s4_ucase
		}]
	},{
		// slide1
		contentblockadditionalclass: "peach_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "instruction_container",
		uppertextblock: [{
			textclass: "description2 des5_1",
			textdata: data.string.p4_s18
		},{
			textclass: "description2 des5_2",
			textdata: data.string.p4_s19
		},{
			textclass: "description2 des5_3",
			textdata: data.string.p4_s20
		},{
			textclass: "description2 des5_4",
			textdata: data.string.p4_s21
		},{
			textclass: "description2 des5_5",
			textdata: data.string.p4_s22
		}],
		imageblock:[{
			imgcontainerdiv: "instruction_imagecontainer",
			imagestoshow: [{
				imgclass: "image4",
				imgid: "tulip_top"
			}]
		}]
	},{
		// slide2
		contentblockadditionalclass: "peach_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "instruction_container",
		uppertextblock: [{
			textclass: "description2 des4_1",
			textdata: data.string.p4_s23
		},{
			textclass: "description2 des4_2",
			textdata: data.string.p4_s24
		},{
			textclass: "description2 des4_3",
			textdata: data.string.p4_s25
		},{
			textclass: "description2 des4_4",
			textdata: data.string.p4_s26
		}],
		imageblock:[{
			imgcontainerdiv: "instruction_imagecontainer",
			imagestoshow: [{
				imgclass: "image4",
				imgid: "tulip_buttom"
			}]
		}]
	}];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "a_22", src: "images/diy_bg/a_22.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table_top_view", src: imgpath+"table_top_view.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boat", src: imgpath+"orig_boat2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"orig_fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tulip", src: imgpath+"orig_tulip.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boat01", src: imgpath+"boat01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boat02", src: imgpath+"boat02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish01", src: imgpath+"fish01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish02", src: imgpath+"fish02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tulip_top", src: imgpath+"tulip_top.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tulip_buttom", src: imgpath+"tulip_buttom.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog8", src: imgpath+"dog8.png", type: createjs.AbstractLoader.IMAGE},
			{id: "yadav1", src: imgpath+"yadav1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "akemi2", src: imgpath+"akemi2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "origami_bubble2", src: imgpath+"origami_bubble2.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s0", src: soundAsset+"p4_s0.ogg"},
			{id: "s1", src: soundAsset+"p4_s1.ogg"},
			{id: "boats1", src: soundAsset+"/boat/0.ogg"},
			{id: "boats2", src: soundAsset+"/boat/1.ogg"},
			{id: "boats3", src: soundAsset+"/boat/2.ogg"},
			{id: "fishs1", src: soundAsset+"/fish/0.ogg"},
			{id: "fishs2", src: soundAsset+"/fish/1.ogg"},
			{id: "fishs3", src: soundAsset+"/fish/2.ogg"},
			{id: "tulips1", src: soundAsset+"/tulip/0.ogg"},
			{id: "tulips2", src: soundAsset+"/tulip/1.ogg"},
			{id: "tulips3", src: soundAsset+"/tulip/2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.les0sonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var timeoutcontroller;

  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
	    	case 0:
				sound_player("s0");
	    		break;
	    	case 1:
				sound_player("s1");
	    		$(".image_label1").click(function(){
	    			var $this = $(this);
	    			$this.addClass("disable");
	    			$toplayerdiv.show(0);
	    			switch($this.html()){
	    				case "Boat":
	    					$toplayerdiv.addClass("green_bg").removeClass("grey_bg peach_bg");
	    					all3clicked[0] = true;
	    					clickindex = 1;
	    					break;
	    				case "Fish":
	    					$toplayerdiv.addClass("grey_bg").removeClass("green_bg peach_bg");
	    					all3clicked[1] = true;
	    					clickindex = 2;
	    					break;
	    				case "Tulip":
	    					$toplayerdiv.addClass("peach_bg").removeClass("green_bg grey_bg");
	    					all3clicked[2] = true;
	    					clickindex = 3;
	    					break;
	    				default:
	    					break;
	    			}
	    			active_generaltemplate2 = true;
	    			templateCaller2();
	    		});
				break;
	    	default:
	    		break;
	   }
  }

   var active_generaltemplate2 = false;
   var countNext2 = 0;
   var clickindex = 0;
   var $toplayerdiv = $(".toplayerdiv");
   var all3clicked = ["false", "false", "false"];

  function generalTemplate2() {
  		var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html;
		vocabcontroller.findwords(countNext2);
	    switch(clickindex){
	    	case 1:
					sound_player("boats"+(countNext2+1));
	    		html = template(content_boat[countNext2]);
	    		$toplayerdiv.html(html);
	    		put_image(content_boat, countNext2);
	    		break;
	    	case 2:
				sound_player("fishs"+(countNext2+1));
	    		html = template(content_fish[countNext2]);
	    		$toplayerdiv.html(html);
	    		put_image(content_fish, countNext2);
	    	 	break;
	    	case 3:
				sound_player("tulips"+(countNext2+1));
	    		html = template(content_tulip[countNext2]);
	    		$toplayerdiv.html(html);
	    		put_image(content_tulip, countNext2);
	    		break;
	    	default:
	    		break;
	    }

	    $nextBtn.show(0);
	    if(countNext2 > 0){
	    	$prevBtn.show(0);
	    }
  }

/*=====  End of Templates Block  ======*/

function sound_player(sound_id){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		// if($total_page > (countNext+1)){
			// $nextBtn.show(0);
		// }else{
			// ole.footerNotificationHandler.pageEndSetNotification();
		// }
	});
}

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		// if(content[count].hasOwnProperty("containsdialog")){
			// for(var j = 0; j < content[count].containsdialog.length; j++){
				// var containsdialog = content[count].containsdialog[j];
				// console.log("imageblock", imageblock);
				// if(containsdialog.hasOwnProperty('dialogueimgid')){
						// var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						// //get list of classes
						// var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						// var selector = ('.'+classes_list[classes_list.length-1]);
						// $(selector).attr('src', image_src);
				// }
			// }
		// }
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller2() {
  		/*always hide next and previous navigation button unless
	    explicitly called from inside a template*/
	    $prevBtn.css('display', 'none');
	    $nextBtn.css('display', 'none');

	    // call navigation controller
	    // navigationcontroller();

	    // call the template
	    generalTemplate2();
  }

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			clearTimeout(timeoutcontroller);
			if(active_generaltemplate2){
				createjs.Sound.stop();
				if(countNext2 == 2){
			    	countNext2 = 0;
			    	active_generaltemplate2 = false;
			    	$toplayerdiv.hide(0);
			    	$nextBtn.hide(0);
			    	timeoutcontroller = setTimeout(function(){
		    			if(all3clicked[0] == true && all3clicked[1] == true && all3clicked[2] == true){
		    				ole.footerNotificationHandler.pageEndSetNotification();
		    			}
		    		}, 400);
			   } else {
					countNext2++;
					templateCaller2();
			   }
			} else {
				countNext++;
				templateCaller();
			}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		if(active_generaltemplate2){
			countNext2--;
			templateCaller2();
		} else {
			countNext--;
			templateCaller();
		}

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

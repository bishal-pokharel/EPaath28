var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "akemi2",
				imgid: "akemi"
			}]
		},{
			imgcontainerdiv: "rotateimgblk rotateimgblk2",
			imagestoshow: [{
				imgclass: "orig_boat_class",
				imgid: "orig_boat"
			},{
				imgclass: "orig_butterfly_class",
				imgid: "orig_butterfly"
			},{
				imgclass: "orig_frog_class",
				imgid: "orig_frog"
			},{
				imgclass: "orig_swan_class",
				imgid: "orig_swan"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog3_b",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "origami_bubble2",
			dialogcontentclass: "textfill2",
			dialogcontent: data.string.title
		}]
	},{
		// slide0
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "red_dot",
			textdata: "&nbsp;"
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "map",
				imgid: "nepal_map"
			},{
				imgclass: "yadav",
				imgid: "yadav1"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1 mirror_image_X",
			dialogueimgid: "origami_bubble1",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1_s0
		}]
	},{
		// slide1
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "full_img",
				imgid: "abc"
			}]
		}],
		lowertextblockadditionalclass : "lowertextblockadditionalclass",
		lowertextblock: [{
			textclass: "description_peach",
			textdata: data.string.p1_s2
		}]
	},{
		// slide2
		contentblockadditionalclass: "purplebg2",
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "akemi",
				imgid: "akemi"
			}]
		}],
		lowertextblockadditionalclass : "lowertextblockadditionalclass2",
		lowertextblock: [{
			textclass: "description",
			textdata: data.string.p1_s3
		}]
	},{
		// slide3
		contentblockadditionalclass: "purplebg2",
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "akemi",
				imgid: "akemi"
			}]
		}],
		lowertextblockadditionalclass : "lowertextblockadditionalclass2",
		lowertextblock: [{
			textclass: "description",
			textdata: data.string.p1_s4
		}]
	},{
		// slide4
		contentblockadditionalclass: "purplebg2",
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "full_img",
				imgid: "living_room"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog2",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "origami_bubble1",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1_s5
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "purpleanimationsquare rotate_square_anim",
			textdata: "&nbsp;"
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "akemi2",
				imgid: "akemi"
			}]
		},{
			imgcontainerdiv: "rotateimgblk",
			imagestoshow: [{
				imgclass: "orig_boat_class",
				imgid: "orig_boat"
			},{
				imgclass: "orig_butterfly_class",
				imgid: "orig_butterfly"
			},{
				imgclass: "orig_frog_class",
				imgid: "orig_frog"
			},{
				imgclass: "orig_swan_class",
				imgid: "orig_swan"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog3",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "origami_bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1_s6
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "purpleanimationsquare",
			textdata: "&nbsp;"
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "akemi2",
				imgid: "akemi"
			},{
				imgclass: "yadav",
				imgid: "yadav1"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog3",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "origami_bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1_s7
		},
		{
			dialogcontainer: "dialog4",
			dialogimageclass: "dialog_image2",
			dialogueimgid: "origami_bubble3",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1_s8
		}]
	},{
		// slide7
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "purpleanimationsquare expand",
			textdata: "&nbsp;"
		},{
			textclass: "description_light_purple",
			textdata: data.string.p1_s9
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "akemi3",
				imgid: "akemi2"
			},{
				imgclass: "yadav2",
				imgid: "yadav2"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog5",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "origami_bubble4",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1_s10
		},
		{
			dialogcontainer: "dialog6",
			dialogimageclass: "dialog_image2",
			dialogueimgid: "origami_bubble3",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1_s11
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "yadav1", src: imgpath+"yadav1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nepal_map", src: imgpath+"nepal_map.png", type: createjs.AbstractLoader.IMAGE},
			{id: "yadav2", src: imgpath+"yadav2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "akemi", src: imgpath+"akemi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "akemi2", src: imgpath+"akemi2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "living_room", src: imgpath+"living_room.png", type: createjs.AbstractLoader.IMAGE},
			{id: "origami_bubble1", src: imgpath+"origami_bubble1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "origami_bubble2", src: imgpath+"origami_bubble2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "origami_bubble3", src: imgpath+"origami_bubble3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "origami_bubble4", src: imgpath+"origami_bubble4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orig_boat", src: imgpath+"orig_boat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orig_butterfly", src: imgpath+"orig_butterfly.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orig_frog", src: imgpath+"orig_frog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orig_swan", src: imgpath+"orig_swan.png", type: createjs.AbstractLoader.IMAGE},
			{id: "abc", src: imgpath+"abc.jpg", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s0", src: soundAsset+"p1_s0.ogg"},
			{id: "s1", src: soundAsset+"p1_s1.ogg"},
			{id: "s2", src: soundAsset+"p1_s2.ogg"},
			{id: "s3", src: soundAsset+"p1_s3.ogg"},
			{id: "s4", src: soundAsset+"p1_s4.ogg"},
			{id: "s5", src: soundAsset+"p1_s5.ogg"},
			{id: "s6", src: soundAsset+"p1_s6.ogg"},
			{id: "s7_0", src: soundAsset+"p1_s7_0.ogg"},
			{id: "s7_1", src: soundAsset+"p1_s7_1.ogg"},
			{id: "s8_0", src: soundAsset+"p1_s8_0.ogg"},
			{id: "s8_1", src: soundAsset+"p1_s8_1.ogg"},
			{id: "s8_2", src: soundAsset+"p1_s8_2.ogg"},
			{id: "timetransit", src: soundAsset+"timetransit.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

		 function navigationcontroller(islastpageflag){
	 	 typeof islastpageflag === "undefined" ?
	 	 islastpageflag = false :
	 	 typeof islastpageflag != 'boolean'?
	 	 alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	 null;

	 	 if(countNext == 0 && $total_page!=1){
	 	 	$nextBtn.show(0);
	 	 	$prevBtn.css('display', 'none');
	 	 }
	 	 else if($total_page == 1){
	 	 	$prevBtn.css('display', 'none');
	 	 	$nextBtn.css('display', 'none');

	 	 	// if lastpageflag is true
	 	 	islastpageflag ?
	 	 	ole.footerNotificationHandler.lessonEndSetNotification() :
	 	 	ole.footerNotificationHandler.lessonEndSetNotification() ;
	 	 }
	 	 else if(countNext > 0 && countNext < $total_page-1){
	 	 	$nextBtn.show(0);
	 	 	$prevBtn.show(0);
	 	 }
	 	 else if(countNext == $total_page-1){
	 	 	$nextBtn.css('display', 'none');
	 	 	$prevBtn.show(0);

	 	 	// if lastpageflag is true
	 	 	islastpageflag ?
	 	 	ole.footerNotificationHandler.lessonEndSetNotification() :
	 	 	ole.footerNotificationHandler.pageEndSetNotification() ;
	 	 }
	 	 }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var timeoutcontroller;
  var intervalcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
				case 0:
					sound_player("s0");
	    	break;
				case 1:
					sound_player("s1");
	    	break;
				case 2:
					sound_player("s2");
	    	break;
				case 3:
					sound_player("s3");
	    	break;
	    	case 4:
	    		var audiochain = ["s4","timetransit"];
				chain_player(audiochain, null);
	    		var count = 0;
	    		var $description = $(".description");
	    		intervalcontroller = setInterval(function(){
	    			if(count < 3){
	    				count++;
	    				$description.append(".");
	    				if(count == 1){
		    				$(".purplebg2").addClass('blurout');
	    				}
	    			} else {
	    				clearInterval(intervalcontroller);
	    				$prevBtn.show(0);
	    				timeoutcontroller = setTimeout(function(){
		    				$nextBtn.trigger('click');
	    				}, 1000);
	    			}
	    		}, 800);
	    		break;
				case 5:
					$(".purplebg2").addClass('blurin');
					timeoutcontroller = setTimeout(function(){
						sound_player("s5");
					}, 1800);
	    	break;
	    	case 6:
				var $akemi2 = $(".akemi2").hide(0).delay(2200).fadeIn(1000);
				var $dialog3 = $(".dialog3").hide(0);
				timeoutcontroller = setTimeout(function(){
					$(".akemi2").fadeIn(1000);
					setTimeout(function(){
						sound_player("s6");
					},1000);
					$dialog3.delay(1000).fadeIn(400);
				}, 2200);
	    		break;
	    	case 7:
					var audiochain = ["s7_0","s7_1"];
					var dialogchain = ["dialog3","dialog4"];
					$(".dialog3").hide(0);
					$(".dialog4").hide(0);
					chain_player(audiochain, dialogchain);
	    		break;
	    	case 8:
	    		var $description_light_purple = $(".description_light_purple").hide(0);
	    		var $dialog5 = $(".dialog5").hide(0);
	    		var $dialog6 = $(".dialog6").hide(0);
	    		var $imageblock  = $(".imageblock ").hide(0);
					var audiochain = ["s8_0","s8_1","s8_2"];

					var dialogchain = ["description_light_purple","dialog5","dialog6"];
	    		timeoutcontroller = setTimeout(function(){
						chain_player(audiochain, dialogchain);
	    				$imageblock.show(0);
	    		}, 2200);
	    		break;
	   }
  }

/*=====  End of Templates Block  ======*/

function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		if(countNext != 4)
			navigationcontroller();
	});
}

function chain_player(sound_array, dialog_array){
	if(dialog_array != null)
		$("."+dialog_array[0]).fadeIn(1000);
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_array[0]);
	current_sound.on("complete", function(){
		sound_array.splice(0,1);
		if(dialog_array != null)
			dialog_array.splice(0,1);
		if(sound_array.length > 0){
			if(dialog_array != null)
				chain_player(sound_array, dialog_array);
			else 
				chain_player(sound_array, null);
		}
		else
		navigationcontroller();
	});
}

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    //navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			clearTimeout(timeoutcontroller);
			clearInterval(intervalcontroller);
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		clearInterval(intervalcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

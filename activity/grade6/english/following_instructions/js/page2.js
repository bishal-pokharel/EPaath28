var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentblockadditionalclass: "yellow_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description firsttext",
			textdata: data.string.p2_s0
		},{
			textclass: "footnote fadein3",
			textdata: data.string.p2_s4
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "lokharke",
				imgid: "lokharke02"
			}]
		}],
		lowertextblockadditionalclass: "ltb_addition",
		lowertextblock: [{
			textclass: "description firsttext",
			textdata: data.string.p2_s1
		},{
			textclass: "description lightgree_bg fadein1",
			textdata: data.string.p2_s2
		},{
			textclass: "description lightgree_bg fadein2",
			textdata: data.string.p2_s3
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_instruction",
			textdata: data.string.p2_s5
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image",
				imgid: "bg01"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1 mirror_image_X",
			dialogueimgid: "bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p2_s6
		},
		{
			dialogcontainer: "dialog2",
			dialogimageclass: "dialog_image2 mirror_image_X2",
			dialogueimgid: "bubble1",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p2_s7
		}],
		lowertextblockadditionalclass: "ltb_info orange_ltb",
		lowertextblock:[{
			textclass: "description",
			textdata: data.string.p2_s8
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_instruction",
			textdata: data.string.p2_s5
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image",
				imgid: "bg02"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog3",
			dialogimageclass: "dialog_image1 mirror_image_X",
			dialogueimgid: "bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p2_s9
		}],
		lowertextblockadditionalclass: "ltb_info grey_ltb",
		lowertextblock:[{
			textclass: "description",
			textdata: data.string.p2_s10
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_instruction newposition1",
			textdata: data.string.p2_s5
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image",
				imgid: "bg03"
			},{
				imgclass: "saahil_class",
				imgid: "saahil"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog4",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "bubble1",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p2_s11
		}],
		lowertextblockadditionalclass: "ltb_info grey_ltb",
		lowertextblock:[{
			textclass: "description",
			textdata: data.string.p2_s12
		}]
	},{
		// slide4
		contentblockadditionalclass: "peach_bg",
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_instruction",
			textdata: data.string.p2_s5
		},{
			textclass: "description2 a1",
			textdata: data.string.p2_s13
		},{
			textclass: "description2 a2",
			textdata: data.string.p2_s14
		},{
			textclass: "description2 a3",
			textdata: data.string.p2_s15
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "class_01",
				imgid: "01"
			},{
				imgclass: "class_02",
				imgid: "02"
			},{
				imgclass: "class_03",
				imgid: "03"
			}]
		}],
		lowertextblockadditionalclass: "ltb_info grey_ltb",
		lowertextblock:[{
			textclass: "description",
			textdata: data.string.p2_s16
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "lokharke02", src: imgpath+"lokharke02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubble1", src: imgpath+"bubble1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubble2", src: imgpath+"bubble2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "01", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "02", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "03", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg03", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "saahil", src: imgpath+"saahil.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s0_0", src: soundAsset+"p2_s0_0.ogg"},
			{id: "s0_1", src: soundAsset+"p2_s0_1.ogg"},
			{id: "s0_2", src: soundAsset+"p2_s0_2.ogg"},
			{id: "s0_3", src: soundAsset+"p2_s0_3.ogg"},
			{id: "s1_0", src: soundAsset+"p2_s1_0.ogg"},
			{id: "s1_1", src: soundAsset+"p2_s1_1.ogg"},
			{id: "s1_2", src: soundAsset+"p2_s1_2.ogg"},
			{id: "s1_3", src: soundAsset+"p2_s1_3.ogg"},
			{id: "s2_0", src: soundAsset+"p2_s2_0.ogg"},
			{id: "s2_1", src: soundAsset+"p2_s2_1.ogg"},
			{id: "s3_0", src: soundAsset+"p2_s3_0.ogg"},
			{id: "s3_1", src: soundAsset+"p2_s3_1.ogg"},
			{id: "s4_0_a", src: soundAsset+"p2_s4_0_a.ogg"},
			{id: "s4_0_b", src: soundAsset+"p2_s4_0_b.ogg"},
			{id: "s4_0_c", src: soundAsset+"p2_s4_0_c.ogg"},
			{id: "s4_1", src: soundAsset+"p2_s4_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

		 function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 	 $nextBtn.show(0);
	 	 $prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 	 $prevBtn.css('display', 'none');
	 	 $nextBtn.css('display', 'none');

		 // if lastpageflag is true
		 islastpageflag ?
		 ole.footerNotificationHandler.lessonEndSetNotification() :
		 ole.footerNotificationHandler.lessonEndSetNotification() ;
		 }

	 	else if(countNext > 0 && countNext < $total_page-1){
	 	 $nextBtn.show(0);
	 	 $prevBtn.show(0);
	 	}
	 	else if(countNext == $total_page-1){
	 	 $nextBtn.css('display', 'none');
	 	 $prevBtn.show(0);

	 	 // if lastpageflag is true
	 	 islastpageflag ?
	 	 ole.footerNotificationHandler.lessonEndSetNotification() :
	 	 ole.footerNotificationHandler.pageEndSetNotification() ;
	 	}
	 	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var timeoutcontroller;

  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
				case 0:
					var audiochain = ["s0_0","s0_1","s0_2","s0_3"];
					var dialogchain = ["firsttext","fadein1","fadein2","footnote"];
					$(".firsttext").hide(0);
					$(".fadein1").hide(0);
					$(".fadein2").hide(0);
					$(".footnote").hide(0);
					chain_player(audiochain, dialogchain);
    		break;
				case 1:
					var audiochain = ["s1_0","s1_1","s1_2","s1_3"];
					var dialogchain = ["description_instruction","dialog2","dialog1","orange_ltb"];
					$(".description_instruction").hide(0);
					$(".dialog2").hide(0);
					$(".dialog1").hide(0);
					$(".orange_ltb").hide(0);
					chain_player(audiochain, dialogchain);
    		break;
				case 2:
					var audiochain = ["s2_0","s2_1"];
					var dialogchain = ["dialog3","grey_ltb"];
					$(".dialog3").hide(0);
					$(".grey_ltb").hide(0);
					chain_player(audiochain, dialogchain);
    		break;
				case 3:
					var audiochain = ["s3_0","s3_1"];
					var dialogchain = ["dialog4","grey_ltb"];
					$(".dialog4").hide(0);
					$(".grey_ltb").hide(0);
					chain_player(audiochain, dialogchain);
    		break;
	    	case 4:
				var audiochain = ["s4_0_a","s4_0_b","s4_0_c","s4_1"];
				var dialogchain = ["a1, .class_01", "a2, .class_02", "a3, .class_03", "grey_ltb"];
				$(".a1, .a2, .a3, .class_01, .class_02, .class_03, .grey_ltb").hide(0);
				chain_player(audiochain, dialogchain);
	    		break;
	   }
  }

/*=====  End of Templates Block  ======*/

function sound_player(sound_id){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		navigationcontroller();
	});
}

function chain_player(sound_array, dialog_array){
	$("."+dialog_array[0]).fadeIn(1000);
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_array[0]);
	current_sound.on("complete", function(){
		sound_array.splice(0,1);
		dialog_array.splice(0,1);
		if(sound_array.length > 0){
			chain_player(sound_array, dialog_array);
		}
		else
		navigationcontroller();
	});
}

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    //navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			clearTimeout(timeoutcontroller);
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

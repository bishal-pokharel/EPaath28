var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblocknocenteradjust: true,
		uppertextblock: [{
			textclass: "description_instruction",
			textdata: data.string.p3_s0
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image",
				imgid: "a_25"
			}]
		}]
	},{
		// slide1
		contentblockadditionalclass: "purple_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblocknocenteradjust: true,
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p3_s1
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description2",
			textdata: data.string.p3_s2
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image zoom_in_anim",
				imgid: "table_top_view"
			}]
		},{
			imgcontainerdiv: "imagecontainer",
			imagestoshow: [{
				imgclass: "image2",
				imgid: "dog2"
			}],
			imagelabels: [{
				imagelabelclass: "image_label2",
				imagelabeldata: data.string.p3_s3
			},{
				imagelabelclass: "image_label2",
				imagelabeldata: data.string.p3_s4
			},{
				imagelabelclass: "image_label2",
				imagelabeldata: data.string.p3_s5
			}]
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description2",
			textdata: data.string.p3_s6
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image zoom_in",
				imgid: "table_top_view"
			}]
		},{
			imgcontainerdiv: "imagecontainer",
			imagestoshow: [{
				imgclass: "image2",
				imgid: "dog4"
			}]
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description2",
			textdata: data.string.p3_s7
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image zoom_in",
				imgid: "table_top_view"
			}]
		},{
			imgcontainerdiv: "imagecontainer",
			imagestoshow: [{
				imgclass: "rotate_img3 image3",
				imgid: "dog6"
			}]
		},{
			imgcontainerdiv: "imagecontainer2"
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description2",
			textdata: data.string.p3_s8
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image zoom_in",
				imgid: "table_top_view"
			}]
		},{
			imgcontainerdiv: "imagecontainer2"
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description2",
			textdata: data.string.p3_s9
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image zoom_in",
				imgid: "table_top_view"
			}]
		},{
			imgcontainerdiv: "imagecontainer2"
		}]
	},{
		// slide7
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description2",
			textdata: data.string.p3_s10
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image zoom_in",
				imgid: "table_top_view"
			}]
		},{
			imgcontainerdiv: "imagecontainer2"
		}]
	},{
		// slide8
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description2",
			textdata: data.string.p3_s11
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image zoom_in",
				imgid: "table_top_view"
			}]
		},{
			imgcontainerdiv: "imagecontainer2"
		}]
	},{
		// slide9
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "light_utb",
		uppertextblock: [{
			textclass: "description2",
			textdata: data.string.p3_s12
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "bg_image zoom_in",
				imgid: "table_top_view"
			}]
		}
		,{
					imgcontainerdiv: "imagecontainer",
					imagestoshow: [{
						imgclass: "image2",
						imgid: "dog"
					}]
				}
		,{
			imgcontainerdiv: "dog",
		}]
	},{
		// slide10
		contentblockadditionalclass: "purple_bg",
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "akemi",
				imgid: "akemi2"
			},{
				imgclass: "yadav",
				imgid: "yadav1"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "origami_bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p3_s13
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "a_25", src: "images/diy_bg/a_25.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table_top_view", src: imgpath+"table_top_view.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "dog1", src: imgpath+"dog1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog2", src: imgpath+"dog2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog3", src: imgpath+"dog3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog4", src: imgpath+"dog4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog5", src: imgpath+"dog5.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog6", src: imgpath+"dog6.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog6_fold", src: imgpath+"dog6_fold.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog7_fold", src: imgpath+"dog7_fold.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog8_fold", src: imgpath+"dog8_fold.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog9_fold", src: imgpath+"dog9_fold.png", type: createjs.AbstractLoader.IMAGE},
			{id: "final_dog", src: imgpath+"orig_frog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "yadav1", src: imgpath+"yadav1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "akemi2", src: imgpath+"akemi2_a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "origami_bubble2", src: imgpath+"origami_bubble2.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s0", src: soundAsset+"p3_s0.ogg"},
			{id: "s1", src: soundAsset+"p3_s1.ogg"},
			{id: "s2", src: soundAsset+"p3_s2.ogg"},
			{id: "s3", src: soundAsset+"p3_s3.ogg"},
			{id: "s4", src: soundAsset+"p3_s4.ogg"},
			{id: "s5", src: soundAsset+"p3_s5.ogg"},
			{id: "s6", src: soundAsset+"p3_s6.ogg"},
			{id: "s7", src: soundAsset+"p3_s7.ogg"},
			{id: "s8", src: soundAsset+"p3_s8.ogg"},
			{id: "s9", src: soundAsset+"p3_s9.ogg"},
			{id: "s10", src: soundAsset+"p3_s10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

		 function navigationcontroller(islastpageflag){
		 typeof islastpageflag === "undefined" ?
		 islastpageflag = false :
		 typeof islastpageflag != 'boolean'?
		 alert("NavigationController : Hi Master, please provide a boolean parameter") :
		 null;

		 if(countNext == 0 && $total_page!=1){
		 $nextBtn.show(0);
		 $prevBtn.css('display', 'none');
		 }
		 else if($total_page == 1){
		 $prevBtn.css('display', 'none');
		 $nextBtn.css('display', 'none');

		 // if lastpageflag is true
		 islastpageflag ?
		 ole.footerNotificationHandler.lessonEndSetNotification() :
		 ole.footerNotificationHandler.lessonEndSetNotification() ;
		 }

		 else if(countNext > 0 && countNext < $total_page-1){
		 $nextBtn.show(0);
		 $prevBtn.show(0);
		 }
		 else if(countNext == $total_page-1){
		 $nextBtn.css('display', 'none');
		 $prevBtn.show(0);

		 // if lastpageflag is true
		 islastpageflag ?
		 ole.footerNotificationHandler.lessonEndSetNotification() :
		 ole.footerNotificationHandler.pageEndSetNotification() ;
		 }
		 }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var timeoutcontroller;
  var intervalcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
				case 0:
					sound_player("s0");
	    	break;
				case 1:
					sound_player("s1");
	    	break;
	    	case 2:
					sound_player("s2");
	    		var count = 0;
	    		var $image2 = $(".image2");
	    		var $image_label2 = $(".image_label2").hide(0);
	    		$image2.attr("src", preload.getResult("dog1").src);
	    		intervalcontroller = setInterval(function(){
	    			$image_label2.hide(0);
	    			switch(count){
	    				case 0:
	    					$($image_label2[count]).show(0);
	    					$image2.attr("src", preload.getResult("dog1").src);
	    					break;
	    				case 1:
	    					$($image_label2[count]).show(0);
	    					$image2.attr("src", preload.getResult("dog2").src);
	    					break;
	    				case 2:
	    					$($image_label2[count]).show(0);
	    					$image2.attr("src", preload.getResult("dog3").src);
	    					break;
	    				default:
	    					break;
	    			}
	    			count++;
	    			count %= 3;
	    		}, 1200);
	    		break;
	    	case 3:
					sound_player("s3");
	    		var count = 0;
	    		var $image2 = $(".image2");
	    		$image2.attr("src", preload.getResult("dog3").src);
	    		intervalcontroller = setInterval(function(){
	    			switch(count){
	    				case 0:
	    					$image2.attr("src", preload.getResult("dog3").src);
	    					break;
	    				case 1:
	    					$image2.attr("src", preload.getResult("dog4").src);
	    					break;
	    				case 2:
	    					$image2.attr("src", preload.getResult("dog5").src);
	    					break;
	    				default:
	    					break;
	    			}
	    			count++;
	    			count %= 3;
	    		}, 1200);
	    		break;
	    	case 4:
					sound_player("s4");
	    		var $imagecontainer2 = $(".imagecontainer2").hide(0);
	    		$imagecontainer2.css("background-image", "url('"+preload.getResult("dog6_fold").src+"')").addClass("dog6_fold");
	    		setTimeout(function() {
	    			$(".imagecontainer").hide(0);
	    			$imagecontainer2.show(0);
	    		}, 1200);
	    		break;
	    	case 5:
					sound_player("s5");
	    		var $imagecontainer2 = $(".imagecontainer2");
	    		$imagecontainer2.css("background-image", "url('"+preload.getResult("dog6_fold").src+"')").addClass("dog6_fold_reverse");
	    		break;
	    	case 6:
					sound_player("s6");
	    		var $imagecontainer2 = $(".imagecontainer2");
	    		$imagecontainer2.css("background-image", "url('"+preload.getResult("dog7_fold").src+"')").addClass("dog7_fold");
	    		break;
	    	case 7:
					sound_player("s7");
	    		var $imagecontainer2 = $(".imagecontainer2").css({"height": "30%",
	    															"top": "54%"});
	    		$imagecontainer2.css("background-image", "url('"+preload.getResult("dog8_fold").src+"')").addClass("dog8_fold");
	    		break;
	    	case 8:
					sound_player("s8");
	    		var $imagecontainer2 = $(".imagecontainer2").css({"height": "30%",
	    															"top": "54%"});
	    		$imagecontainer2.css("background-image", "url('"+preload.getResult("dog9_fold").src+"')").addClass("dog9_fold");
	    		break;
	    	case 9:
				sound_player("s9");
	    		$(".image2").css({
	    			"left" : "30%",
	    			"width" : "40%",
	    			"bottom" : "10%"
	    		});
	    		
	    		var $dog = Snap(".dog");
	    		console.log("src", preload.getResult('dog').src);
	    		
	    		var svg = Snap.load(preload.getResult('dog').src, function(loadedFragment){
	    			console.log(loadedFragment);
	    			$dog.append(loadedFragment);
		    		$("#lefteye").fadeIn(1500);
		    		$("#righteye").delay(700).fadeIn(1500);
	    		});
	    		
	    		break;
	    	case 10:
					sound_player("s10");
	    		break;
	   }
  }

/*=====  End of Templates Block  ======*/

function sound_player(sound_id){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		navigationcontroller();
	});
}

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    //navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			clearTimeout(timeoutcontroller);
			clearInterval(intervalcontroller);
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		clearInterval(intervalcontroller);
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 0	
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.e1_q1,
				textclass : 'topbox',
				datahighlightflag:true,
	            datahighlightcustomclass:"color",
			}
		],
		extratextblock:[
			{
				textdata : data.string.exctext,
				textclass : 'passage',
			}
		],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
		    textdata : data.string.e1_q1_o3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.e1_q1_o2,
			optionclass : 'class2'
		},
		{
			textdata : data.string.e1_q1_o1,
			optionclass : 'class1'
			
		},
		{
			textdata : data.string.e1_q1_o4,
			optionclass : 'class4'
		}],
	},
	//slide 1
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q2,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q2_o1,
			optionclass : 'class1 optioncontainer3'
		},
		{
			textdata : data.string.e1_q2_o2,
			optionclass : 'class2 optioncontainer3'
		},
		{
			textdata : data.string.e1_q2_o3,
			optionclass : 'class3 optioncontainer3'
		},
		{
			textdata : data.string.e1_q2_o4,
			optionclass : 'class4 optioncontainer3'
		}],
	},
	//slide 2
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q3,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q3_o2,
			optionclass : 'class2'
		},
		{

			textdata : data.string.e1_q3_o1,
			optionclass : 'class1'			
		},
		{
			textdata : data.string.e1_q3_o3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.e1_q3_o4,
			optionclass : 'class4'
		}],
	},
	//slide 3
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q4,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q4_o3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.e1_q4_o2,
			optionclass : 'class2'
		},
		{

			textdata : data.string.e1_q4_o1,
			optionclass : 'class1'			
		},
		{
			textdata : data.string.e1_q4_o4,
			optionclass : 'class4'
		}],
	},
	//slide 4
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q5,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		},
			{
				textdata : data.string.exctext1,
				textclass : 'passage1',
			}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q5_o4,
			optionclass : 'class4 optioncontainer2'
		},
		{
			textdata : data.string.e1_q5_o3,
			optionclass : 'class3 optioncontainer2'			
		},
		{
			textdata : data.string.e1_q5_o2,
			optionclass : 'class2 optioncontainer2'
		},
		{
			textdata : data.string.e1_q5_o1,
			optionclass : 'class1 optioncontainer2'			
		}],
	},
		//slide 5
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q6,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q6_o4,
			optionclass : 'class4 optioncontainer2'
		},
		{
			textdata : data.string.e1_q6_o3,
			optionclass : 'class3 optioncontainer2'			
		},
		{
			textdata : data.string.e1_q6_o2,
			optionclass : 'class2 optioncontainer2'
		},
		{
			textdata : data.string.e1_q6_o1,
			optionclass : 'class1 optioncontainer2'		
		}],
	},
			//slide 6
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q7,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q7_o4,
			optionclass : 'class4 optioncontainer2'
		},
		{
			textdata : data.string.e1_q7_o3,
			optionclass : 'class3 optioncontainer2'			
		},
		{
			textdata : data.string.e1_q7_o2,
			optionclass : 'class2 optioncontainer2'
		},
		{
			textdata : data.string.e1_q7_o1,
			optionclass : 'class1 optioncontainer2'		
		}],
	},		//slide 4
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q8,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q8_o4,
			optionclass : 'class4 optioncontainer2'
		},
		{
			textdata : data.string.e1_q8_o3,
			optionclass : 'class3 optioncontainer2'			
		},
		{
			textdata : data.string.e1_q8_o2,
			optionclass : 'class2 optioncontainer2'
		},
		{
			textdata : data.string.e1_q8_o1,
			optionclass : 'class1 optioncontainer2'		
		}],
	},
			//slide 4
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q9,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q9_o4,
			optionclass : 'class4'
		},
		{
			textdata : data.string.e1_q9_o3,
			optionclass : 'class3'			
		},
		{
			textdata : data.string.e1_q9_o2,
			optionclass : 'class2'
		},
		{
			textdata : data.string.e1_q9_o1,
			optionclass : 'class1'		
		}],
	},
			//slide 4
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.e1_q10,
			textclass : 'topbox',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		extratextblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q10_o4,
			optionclass : 'class4 optioncontainer3'
		},
		{
			textdata : data.string.e1_q10_o3,
			optionclass : 'class3 optioncontainer3'			
		},
		{
			textdata : data.string.e1_q10_o2,
			optionclass : 'class2 optioncontainer3'
		},
		{
			textdata : data.string.e1_q10_o1,
			optionclass : 'class1 optioncontainer3'		
		}],
	}

	
];

//content.shufflearray();

$(function () 
{	
	var $board    = $('.board');
	var $nextBtn  = $("#activity-page-next-btn-enabled");
	var $prevBtn  = $("#activity-page-prev-btn-enabled");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new RhinoTemplate();
   
	rhino.init($total_page);

  //for shuffling two categories of questions
        var mainarr=[];
		var firstqns=[0,1,2,3,4];
		var secqns=[5,6,7,8,9]
		var fqn=firstqns.shufflearray();
		var sqn=secqns.shufflearray();
		var arr=mainarr.concat(fqn);
		var arr2=arr.concat(sqn);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[arr2[countNext]]);
		$board.html(html);
		
		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$("#box_icon_rhino").hide(0);
				//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	   } 
        
        
		$('.topbox').prepend(countNext+1+". ");
		var wrong_clicked = false;
		$(".optioncontainer").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(this).children('.correct-icon').show(0);
				$(".optioncontainer").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).children('.incorrect-icon').show(0);
				$(this).css({
					'background-color': '#FF0000',
					'border': '3px solid #980000'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		}); 
	}
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	
});
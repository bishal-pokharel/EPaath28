var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/p6/";

var sound_1 = "1";
var sound_2 = "2";
var sound_3 = "3";
var sound_4 = "4";
var sound_5 = "5";
var sound_6 = "6";
var sound_7 = "7";
var sound_8 = "8";
var sound_9 = "9";
var sound_10 = "10";
var sound_11 = "11";


createjs.Sound.registerSound((soundAsset + "0.ogg"), sound_1);
createjs.Sound.registerSound((soundAsset + "BUDIAMA1.ogg"), sound_2);
createjs.Sound.registerSound((soundAsset + "BUDIAMA2.ogg"), sound_3);
createjs.Sound.registerSound((soundAsset + "BUDIAMA3.ogg"), sound_4);
createjs.Sound.registerSound((soundAsset + "BUDIAMA4.ogg"), sound_5);
createjs.Sound.registerSound((soundAsset + "BUDIAMA5.ogg"), sound_6);
createjs.Sound.registerSound((soundAsset + "BUDIAMA6.ogg"), sound_7);
createjs.Sound.registerSound((soundAsset + "BUDIAMA7.ogg"), sound_8);
createjs.Sound.registerSound((soundAsset + "BUDIAMA8.ogg"), sound_9);
createjs.Sound.registerSound((soundAsset + "BUDIAMA9.ogg"), sound_10);
createjs.Sound.registerSound((soundAsset + "BUDIAMA10.ogg"), sound_11);


var sound_gr_1 = [sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-sky',
		
		extratextblock:[{
			textdata : data.string.p1text2,
			textclass: 'click-on-map'	
		}],
		
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "bg-map",
					imgsrc : imgpath + "park.svg",
				},
				{
					imgclass : "map-obj market",
					imgsrc : imgpath + "market.png",
				},
				{
					imgclass : "map-obj stupa",
					imgsrc : imgpath + "stupa.png",
				},
				{
					imgclass : "map-obj playground",
					imgsrc : imgpath + "playground.png",
				},
				{
					imgclass : "map-obj museum",
					imgsrc : imgpath + "museum.svg",
				},
				{
					imgclass : "map-obj hall hl-object",
					imgsrc : imgpath + "hall.svg",
				},
				{
					imgclass : "square",
					imgsrc : imgpath + "square.png",
				},{
					imgclass : "bahal",
					imgsrc : imgpath + "bahal.png",
				}
			],
		}],
		
		popupblock:[{
			popupclass: '',
			headerdata: data.string.p6text20,
			page:[
				//minislide 0
				{
					pageclass: '',
					text:[{
						textdata : data.string.p6text1,
						textclass: 'popup-desc-text'
					}],
					image : [
					{
						imgclass : "hall-p0",
						imgsrc : imgpath + "hall.svg",
					}]
				},
				//minislide 1
				{
					pageclass: '',
					museumblock:[{
						museumdiv: '',
						bgclass: 'hall-p0 hall-anim',
						bgsrc: imgpath + "hall.svg",
						imgclass: 'hall-room hall-room-anim',
						imgsrc: imgpath + "welcome.png",
						doorclass: ''
					}],
				},
				//minislide 2
				{
					containerclass: 'no-overflow',
					imageblock : [{
					imageblockclass: 'bg-hall room-lady-anim',
						image:[{
							imgclass : "lady-standing",
							imgsrc : imgpath + "lady-standing.png",
						}]
					}],
					image:[{
						imgclass : "lady-talking no_display",
						imgsrc : imgpath + "talking.gif",
					},{
						imgclass : "proj-image-2 fade_in-3",
						imgsrc : imgpath + "c1.png",
					}],
					speechbox:[{
						speechbox: 'speech-1',
						textdata : data.string.p6text2,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 3
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image",
						imgsrc : imgpath + "c1.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text3,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 4
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image fade_in",
						imgsrc : imgpath + "c2.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text4,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 5
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image ",
						imgsrc : imgpath + "c2.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text5,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 6
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image fade_in",
						imgsrc : imgpath + "c3.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text6,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 7
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image",
						imgsrc : imgpath + "c3.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text7,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 8
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image fade_in",
						imgsrc : imgpath + "c4.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text8,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 9
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image",
						imgsrc : imgpath + "c4.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text9,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 10
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image fade_in",
						imgsrc : imgpath + "c5.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text10,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				},
				//minislide 11
				{
					containerclass: 'bg-hall-2',
					image : [
					{
						imgclass : "lady-talking",
						imgsrc : imgpath + "lady-talking.png",
					},{
						imgclass : "proj-image",
						imgsrc : imgpath + "c5.png",
					}],
					speechbox:[{
						speechbox: 'speech-2',
						textdata : data.string.p6text11,
						textclass : '',
						imgsrc: imgpath + "bubble-06.png",
					}]
				}
			]
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = createjs.Sound.play(sound_1);
	var myTimeout =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	
	
	var current_div_x;
	var current_div_y;
	var popcount = 0;
	var popupcontent = '';
	var popuptotal = 0;
	var popup_is_close =  true;
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				if(countNext>0){ 
					$prevBtn.show(0);
				}
				popupcontent = content[countNext].popupblock[0].page;
				$('.hl-object').on("click", function(event){
					popup_window($(this), event);
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
	}
	
	function popupcreater(){
		var popsource = $("#popup-template").html();
		var poptemplate = Handlebars.compile(popsource);
		var pophtml = poptemplate(popupcontent[popcount]);
		$('.popuppage').html(pophtml);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($('.popuppage'));
		vocabcontroller.findwords(countNext);
		
		$('.popupnext').hide(0);
		$('.popupprev').hide(0);
		
		// if(popcount < popuptotal-1){
			// $('.popupnext').show(0);
		// } else{
			// $('.wrong_button').show(0);
		// }
		// if(popcount > 0){
			// $('.popupprev').show(0);
		// }
		
		$('.popupnext, .popupprev').unbind('click');
		$('.popupnext').click(function(){
			$('.popupnext').hide(0);
			$('.popupprev').hide(0);
			clearTimeout(myTimeout);
			current_sound.stop();
			popcount++;
			popupcreater();
		});
		$('.popupprev').click(function(){
			$('.popupnext').hide(0);
			$('.popupprev').hide(0);
			clearTimeout(myTimeout);
			current_sound.stop();
			popcount--;
			popupcreater();
		});
		$('.wrong_button').click(function(){
			close_popup_window();
		});
		
		if(countNext==0){
			switch(popcount){
				case 0:
					$('.popupnext').show(0);
					sound_player(sound_1);
					break;
				case 1:
					$('.popupprev').show(0);
					myTimeout = setTimeout(function(){
						$('.door-m').show(0);
						$('.door-m').addClass('door-m-anim');
						$('.door-n').show(0);
						$('.door-n').addClass('door-n-anim');
						$('.hall-p0').attr('src', imgpath + "hall-1.svg");
						$('.hall-p0').removeClass('hall-anim').addClass('hall-anim-1');
						$('.hall-room').show(0);
						$('.popupnext').show(0);
					}, 4000);
					break;
				case 2: 
					$('.popupprev').show(0);
					myTimeout = setTimeout(function(){
						$('.speech-1').show(0);
						$('.no_display').show(0);
						mini_sound_and_nav(sound_2, click_dg, '.speech-1');
					}, 3500);
					break;
				default:
					$('.popupprev').show(0);
					mini_sound_and_nav(sound_gr_1[popcount-2], click_dg, '.speechbox');
					break;
			}
		}
	}
	function popup_window(click_class, event){
		if(popup_is_close){
			// console.log('before started   '+ popup_is_close);
			
			$('.click-on-map').fadeOut(1000);
			$( ".map-obj" ).css('pointer-events', 'none');
			$prevBtn.hide(0);
			$nextBtn.hide(0);
			
			$('.wrong_button').hide(0);
			popuptotal = popupcontent.length;
			
			popupcreater();
			
			popup_is_close = false;
			popcount = 0;
			setTimeout(function(){
				console.log('started   '+ popup_is_close);
				
				current_div_x = click_class.position().left + click_class.width()/2;
				current_div_y = click_class.position().top + click_class.height()/2;
				
				$('.popupdiv').css({'display': 'block', 'width': "0%", 'height': '0%', 'left':current_div_x, 'top': current_div_y});
				$('.popuppage').hide(0);
				$('.popupdiv').animate({
					'width': "95%", 
					'height': '95%', 
					'left':'50%', 
					'top': '50%'
				}, 500, function(){
					$('.popuppage').fadeIn(300);
				});
			}, 100);
		}
	}
	function close_popup_window(){
		popcount = 0;
		current_sound.stop();
		$(".drop-box").css('background-color', 'rgb(120,121,196)');
		$('.popuppage, .wrong_button').fadeOut(300);
		$('.popupdiv').fadeOut(500, function(){
			popup_is_close = true;
			console.log('completed   ' + popup_is_close);
			$( ".map-obj" ).css('pointer-events', 'all');
			$('.click-on-map').fadeIn(1000);
		});
		$prevBtn.show(0);
		if( countNext == $total_page-1){
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$nextBtn.show(0);
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}
	function mini_sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		$('.lady-talking').attr('src', imgpath + "talking.gif");
		current_sound.on("complete", function(){
			$('.lady-talking').attr('src', imgpath + "lady-talking.png");
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if( popcount == popuptotal-1 ){
				$('.wrong_button').show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$('.popupnext').show(0);
			}
		});
	}
	
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			current_sound.stop();
			current_sound = createjs.Sound.play(audio);
			current_sound.play();
			$('.lady-talking').attr('src', imgpath + "talking.gif");
			current_sound.on("complete", function(){
				$('.lady-talking').attr('src', imgpath + "lady-talking.png");
			});
		});
	}

	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

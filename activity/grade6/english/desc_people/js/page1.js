var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/p1/";

var sound_9 = "9";
createjs.Sound.registerSound(soundAsset + "descpeople.ogg", sound_9);

var sound_1 = "1";
createjs.Sound.registerSound(soundAsset + "recording1.ogg", sound_1);

var sound_2 = "2";
createjs.Sound.registerSound(soundAsset + "recording2.ogg", sound_2);

var sound_3 = "3";
createjs.Sound.registerSound(soundAsset + "recording3.ogg", sound_3);

var sound_4 = "4";
createjs.Sound.registerSound(soundAsset + "recording4.ogg", sound_4);

var sound_5 = "5";
createjs.Sound.registerSound(soundAsset + "recording5.ogg", sound_5);

var sound_6 = "6";
createjs.Sound.registerSound(soundAsset + "recording6.ogg", sound_6);

var sound_7 = "7";
createjs.Sound.registerSound(soundAsset + "recording7.ogg", sound_7);

var sound_8 = "8";
createjs.Sound.registerSound(soundAsset + "recording8.ogg", sound_8);

var content = [
  //slide0
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "",

    extratextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: "lesson-title sniglet"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "cover",
            imgsrc: imgpath + "cover_page.png"
          }
        ]
      }
    ]
  },

  //slide1
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-sky",

    extratextblock: [
      {
        textdata: "",
        textclass: "overlay-darkness"
      },
      {
        textdata: data.string.p1text1,
        textclass: "welcome-text my_font_big"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-image",
            imgsrc: imgpath + "park.svg"
          }
        ]
      }
    ]
  },
  //slide2
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-sky",

    extratextblock: [
      {
        textdata: "",
        textclass: "overlay-darkness overlay-darkness-anim"
      },
      {
        // textdata : '',
        // textclass: 'sunrays sunrays-anim'
      },
      {
        textdata: data.string.p1text1,
        textclass: "welcome-text my_font_big fade-out"
      }
    ],
    bgblock: [
      {
        bgdiv: "bg-image bg-anim",
        bgclass: "full-bg",
        bgsrc: imgpath + "park.svg",
        imgclass: "sun sunrise",
        imgsrc: imgpath + "sun.png",
        image: [
          {
            imgclass: "map-obj2 market",
            imgsrc: imgpath + "market.png"
          },
          {
            imgclass: "map-obj2 stupa",
            imgsrc: imgpath + "stupa.png"
          },
          {
            imgclass: "map-obj2 playground",
            imgsrc: imgpath + "playground.png"
          },
          {
            imgclass: "map-obj2 museum",
            imgsrc: imgpath + "museum.svg"
          },
          {
            imgclass: "map-obj2 hall ",
            imgsrc: imgpath + "hall.svg"
          },
          {
            imgclass: "map-obj2 square ",
            imgsrc: imgpath + "square.png"
          },
          {
            imgclass: "map-obj2 bahal ",
            imgsrc: imgpath + "bahal.png"
          }
        ]
      }
    ]
  },
  //slide3
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-sky",

    extratextblock: [
      {
        textdata: data.string.p1text2,
        textclass: "click-on-map"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-map",
            imgsrc: imgpath + "park.svg"
          },
          {
            imgclass: "map-obj market hl-object",
            imgsrc: imgpath + "market.png"
          },
          {
            imgclass: "map-obj stupa",
            imgsrc: imgpath + "stupa.png"
          },
          {
            imgclass: "map-obj playground",
            imgsrc: imgpath + "playground.png"
          },
          {
            imgclass: "map-obj museum",
            imgsrc: imgpath + "museum.svg"
          },
          {
            imgclass: "map-obj hall ",
            imgsrc: imgpath + "hall.svg"
          },
          {
            imgclass: "map-obj square ",
            imgsrc: imgpath + "square.png"
          },
          {
            imgclass: "map-obj bahal ",
            imgsrc: imgpath + "bahal.png"
          }
        ]
      }
    ],
    popupblock: [
      {
        popupclass: "",
        headerdata: data.string.p1text3,
        page: [
          {
            pageclass: "page1",
            text: [
              {
                textdata: data.string.p1text4,
                textclass: "popup-desc-text-side"
              }
            ],
            image: [
              {
                imgclass: "market-2",
                imgsrc: imgpath + "market.png"
              }
            ]
          }
        ]
      }
    ]
  },
  //slide4
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-sky",

    extratextblock: [
      {
        textdata: data.string.p1text2,
        textclass: "click-on-map"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-map",
            imgsrc: imgpath + "park.svg"
          },
          {
            imgclass: "map-obj market",
            imgsrc: imgpath + "market.png"
          },
          {
            imgclass: "map-obj stupa",
            imgsrc: imgpath + "stupa.png"
          },
          {
            imgclass: "map-obj playground hl-object",
            imgsrc: imgpath + "playground.png"
          },
          {
            imgclass: "map-obj museum",
            imgsrc: imgpath + "museum.svg"
          },
          {
            imgclass: "map-obj hall ",
            imgsrc: imgpath + "hall.svg"
          },
          {
            imgclass: "map-obj square ",
            imgsrc: imgpath + "square.png"
          },
          {
            imgclass: "map-obj bahal ",
            imgsrc: imgpath + "bahal.png"
          }
        ]
      }
    ],
    popupblock: [
      {
        popupclass: "",
        headerdata: data.string.p1text5,
        page: [
          {
            pageclass: "page1",
            text: [
              {
                textdata: data.string.p1text6,
                textclass: "popup-desc-text"
              }
            ],
            image: [
              {
                imgclass: "popup-imageh",
                imgsrc: imgpath + "playground.png"
              }
            ]
          }
        ]
      }
    ]
  },
  //slide5
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-sky",

    extratextblock: [
      {
        textdata: data.string.p1text2,
        textclass: "click-on-map"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-map",
            imgsrc: imgpath + "park.svg"
          },
          {
            imgclass: "map-obj market",
            imgsrc: imgpath + "market.png"
          },
          {
            imgclass: "map-obj stupa hl-object",
            imgsrc: imgpath + "stupa.png"
          },
          {
            imgclass: "map-obj playground",
            imgsrc: imgpath + "playground.png"
          },
          {
            imgclass: "map-obj museum",
            imgsrc: imgpath + "museum.svg"
          },
          {
            imgclass: "map-obj hall ",
            imgsrc: imgpath + "hall.svg"
          },
          {
            imgclass: "map-obj square ",
            imgsrc: imgpath + "square.png"
          },
          {
            imgclass: "map-obj bahal ",
            imgsrc: imgpath + "bahal.png"
          }
        ]
      }
    ],
    popupblock: [
      {
        popupclass: "",
        headerdata: data.string.p1text7,
        page: [
          {
            pageclass: "",
            text: [
              {
                textdata: data.string.p1text8,
                textclass: "popup-desc-text"
              }
            ],
            image: [
              {
                imgclass: "stupa-p",
                imgsrc: imgpath + "stupa.png"
              }
            ]
          },
          {
            pageclass: "",
            textblockclass: "topblock-text my_font_medium",
            textblock: [
              {
                textdata: data.string.p1text9,
                textclass: ""
              }
            ],
            image: [
              {
                imgclass: "popup-imageh",
                imgsrc: imgpath + "swayambhu.jpg"
              }
            ]
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;
  var $total_page = content.length;

  var current_sound = createjs.Sound.play(sound_9);
  var myTimeout = null;
  var timeoutvar = null;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var current_div_x;
  var current_div_y;
  var popcount = 0;
  var popupcontent = "";
  var popuptotal = 0;
  var popup_is_close = true;

  /*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
  Handlebars.registerPartial(
    "definitioncontent",
    $("#definitioncontent-partial").html()
  );
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // controls the navigational state of the program
  // next btn is disabled for this page
  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
    // if lastpageflag is true
    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  templateCaller();
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    is_last_page = false;

    $board.html(html);
    loadTimelineProgress($total_page, countNext + 1);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch (countNext) {
      case 0:
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>" + sound_9);
        sound_player(sound_9);
        nav_button_controls(2000);
        break;
      case 1:
        sound_player(sound_1);
        $nextBtn.show(0);
        $prevBtn.show(0);
        break;
      case 2:
        $prevBtn.show(0);
        nav_button_controls(7000);
        break;
      case 3:
      case 4:
      case 5:
        sound_player(sound_2);
        $prevBtn.show(0);
        popupcontent = content[countNext].popupblock[0].page;
        $(".hl-object").on("click", function(event) {
          popup_window($(this), event);
        });
        break;
      default:
        $prevBtn.show(0);
        nav_button_controls(100);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_data) {
    console.log("I am here " + sound_data);
    current_sound.stop();
    current_sound = createjs.Sound.play(sound_data);
    current_sound.play();
  }

  function popupcreater() {
    var popsource = $("#popup-template").html();
    var poptemplate = Handlebars.compile(popsource);
    var pophtml = poptemplate(popupcontent[popcount]);
    $(".popuppage").html(pophtml);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($(".popuppage"));
    vocabcontroller.findwords(countNext);

    $(".popupnext").hide(0);
    $(".popupprev").hide(0);
    if (popcount < popuptotal - 1) {
      $(".popupnext").show(0);
    } else {
      $(".wrong_button").show(0);
    }
    if (popcount > 0) {
      $(".popupprev").show(0);
    }
    $(".popupnext, .popupprev").unbind("click");
    $(".popupnext").click(function() {
      $(".popupnext").hide(0);
      $(".popupprev").hide(0);
      current_sound.stop();
      popcount++;
      popupcreater();
    });
    $(".popupprev").click(function() {
      $(".popupnext").hide(0);
      $(".popupprev").hide(0);
      current_sound.stop();
      popcount--;
      popupcreater();
    });
    $(".wrong_button").click(function() {
      close_popup_window();
    });
    if (countNext == 3 && popcount == 0) {
      sound_player(sound_4);

      // createjs.Sound.play(sound_4);
    }
    if (countNext == 4 && popcount == 0) {
      sound_player(sound_5);

      // createjs.Sound.play(sound_5);
    }
    if (countNext == 5 && popcount == 0) {
      sound_player(sound_6);
      // createjs.Sound.play(sound_6);
    }
    if (countNext == 5 && popcount == 1) {
      sound_player(sound_7);
      // createjs.Sound.play(sound_7);
    }
  }
  function popup_window(click_class, event) {
    if (popup_is_close) {
      // console.log('before started   '+ popup_is_close);

      $(".click-on-map").fadeOut(1000);
      $(".map-obj").css("pointer-events", "none");
      $prevBtn.hide(0);
      $nextBtn.hide(0);

      $(".wrong_button").hide(0);
      popuptotal = popupcontent.length;

      popupcreater();

      popup_is_close = false;
      popcount = 0;
      setTimeout(function() {
        console.log("started   " + popup_is_close);

        current_div_x = click_class.position().left + click_class.width() / 2;
        current_div_y = click_class.position().top + click_class.height() / 2;

        $(".popupdiv").css({
          display: "block",
          width: "0%",
          height: "0%",
          left: current_div_x,
          top: current_div_y
        });
        $(".popuppage").hide(0);
        $(".popupdiv").animate(
          {
            width: "95%",
            height: "95%",
            left: "50%",
            top: "50%"
          },
          500,
          function() {
            $(".popuppage").fadeIn(300);
          }
        );
      }, 100);
    }
  }
  function close_popup_window() {
    popcount = 0;
    current_sound.stop();
    $(".drop-box").css("background-color", "rgb(120,121,196)");
    $(".popuppage, .wrong_button").fadeOut(300);
    $(".popupdiv").fadeOut(500, function() {
      popup_is_close = true;
      console.log("completed   " + popup_is_close);
      $(".map-obj").css("pointer-events", "all");
      $(".click-on-map").fadeIn(1000);
    });
    $prevBtn.show(0);
    if (countNext == $total_page - 1) {
      ole.footerNotificationHandler.pageEndSetNotification();
    } else {
      $nextBtn.show(0);
    }
  }

  function templateCaller() {
    //convention is to always hide the prev and next button and show them based
    //on the convention or page index
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();

    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        current_sound.stop();
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    clearTimeout(timeoutvar);
    // current_sound.stop();
    countNext--;
    templateCaller();
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object"
    ? alert(
        "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
      )
    : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
      $(this).attr(
        "data-highlightcustomclass"
      ) /*if there is data-highlightcustomclass defined it is true else it is not*/
        ? (stylerulename = $(this).attr("data-highlightcustomclass"))
        : (stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

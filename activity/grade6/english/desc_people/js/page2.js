var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/p2/";

var sound_1 = "1";
var sound_2 = "2";
var sound_3 = "3";
var sound_4 = "4";
var sound_5 = "5";
var sound_6 = "6";
var sound_7 = "7";
var sound_8 = "8";
var sound_9 = "9";

createjs.Sound.registerSound((soundAsset + "recording1.ogg"), sound_1);
createjs.Sound.registerSound((soundAsset + "recording2.ogg"), sound_2);
createjs.Sound.registerSound((soundAsset + "recording3.ogg"), sound_3);
createjs.Sound.registerSound((soundAsset + "recording4.ogg"), sound_4);
createjs.Sound.registerSound((soundAsset + "recording5.ogg"), sound_5);
createjs.Sound.registerSound((soundAsset + "recording6.ogg"), sound_6);
createjs.Sound.registerSound((soundAsset + "recording7.ogg"), sound_7);
createjs.Sound.registerSound((soundAsset + "recording8.ogg"), sound_8);
createjs.Sound.registerSound((soundAsset + "recording9.ogg"), sound_9);

var sound_gr_1 = [sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-sky',
		
		extratextblock:[{
			textdata : data.string.p1text2,
			textclass: 'click-on-map'	
		}],
		
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "bg-map",
					imgsrc : imgpath + "park.svg",
				},
				{
					imgclass : "map-obj market",
					imgsrc : imgpath + "market.png",
				},
				{
					imgclass : "map-obj stupa",
					imgsrc : imgpath + "stupa.png",
				},
				{
					imgclass : "map-obj playground",
					imgsrc : imgpath + "playground.png",
				},
				{
					imgclass : "map-obj museum hl-object",
					imgsrc : imgpath + "museum.svg",
				},
				{
					imgclass : "map-obj hall ",
					imgsrc : imgpath + "hall.svg",
				},
				{
					imgclass : "square",
					imgsrc : imgpath + "square.png",
				},{
					imgclass : "bahal ",
					imgsrc : imgpath + "bahal.png",
				}
			],
		}],
		popupblock:[{
			popupclass: '',
			headerdata: data.string.p2text1,
			page:[
			//s1
			{
				pageclass: '',
				text:[{
					textdata : data.string.p2text2,
					textclass: 'popup-desc-text'
				},{
					textdata : data.string.p2text3,
					textclass: 'popup-desc-text'
				}],
				image : [
				{
					imgclass : "museum-p",
					imgsrc : imgpath + "museum.svg",
				}]
			},
			//s2
			{
				pageclass: '',
				text:[{
					textdata : data.string.p2text4,
					textclass: 'popup-desc-text'
				},{
					textdata : data.string.p2text5,
					textclass: 'popup-desc-text'
				}],
				image : [
				{
					imgclass : "museum-p",
					imgsrc : imgpath + "museum.svg",
				}]
			},
			//s3
			{
				pageclass: '',
				museumblock:[{
					museumdiv: '',
					bgclass: 'museum-p0 museum-p0-anim-2',
					bgsrc: imgpath + "museum.svg",
					imgclass: 'mus-room mus-room-anim',
					imgsrc: imgpath + "inside_meuseum.png",
					doorclass: ''
				}],
			},
			//s4
			{
				pageclass: '',
				text:[{
					textdata : data.string.p2text6,
					textclass: 'popup-desc-text'
				},{
					textdata : data.string.p2text7,
					textclass: 'popup-desc-text'
				}],
				image : [
				{
					imgclass : "fullroom",
					imgsrc : imgpath + "inside_meuseum.png",
				}]
			},
			//s5
			{
				pageclass: '',
				text:[{
					textdata : data.string.p2text8,
					textclass: 'popup-desc-text'
				}],
				image : [
				{
					imgclass : "imgt-1",
					imgsrc : imgpath + "t1.jpg",
				}]
			},
			//s6
			{
				pageclass: '',
				text:[{
					textdata : data.string.p2text9,
					textclass: 'popup-desc-text'
				}],
				image : [
				{
					imgclass : "imgt-21",
					imgsrc : imgpath + "t2-1.jpg",
				},{
					imgclass : "imgt-22",
					imgsrc : imgpath + "t2-2.jpg",
				},{
					imgclass : "imgt-23",
					imgsrc : imgpath + "t2-3.jpg",
				}]
			},
			//s7
			{
				pageclass: '',
				text:[{
					textdata : data.string.p2text10,
					textclass: 'popup-desc-text'
				},{
					textdata : data.string.p2text12,
					textclass: 'text-d my_font_big'
				},{
					textdata : data.string.p2text13,
					textclass: 'text-g my_font_big'
				}],
				image : [
				{
					imgclass : "imgt-d",
					imgsrc : imgpath + "d.png",
				},{
					imgclass : "imgt-g",
					imgsrc : imgpath + "g.png",
				}]
			},
			//s8
			{
				pageclass: '',
				text:[{
					textdata : data.string.p2text11,
					textclass: 'popup-desc-text'
				}],
				image : [
				{
					imgclass : "imgt-31",
					imgsrc : imgpath + "t3-1.png",
				},{
					imgclass : "imgt-32",
					imgsrc : imgpath + "t3-2.png",
				}]
			}
			
			]
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = createjs.Sound.play(sound_1);
	var myTimeout =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	
	
	var current_div_x;
	var current_div_y;
	var popcount = 0;
	var popupcontent = '';
	var popuptotal = 0;
	var popup_is_close =  true;
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0: 
			case 1: 
			case 2:
				if(countNext>0){ 
					$prevBtn.show(0);
				}
				popupcontent = content[countNext].popupblock[0].page;
				$('.hl-object').on("click", function(event){
					popup_window($(this), event);
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
	}
	
	function popupcreater(){
		var popsource = $("#popup-template").html();
		var poptemplate = Handlebars.compile(popsource);
		var pophtml = poptemplate(popupcontent[popcount]);
		$('.popuppage').html(pophtml);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($('.popuppage'));
		vocabcontroller.findwords(countNext);
		
		$('.popupnext, .popupprev').unbind('click');
		$('.popupnext').click(function(){
			$('.popupnext').hide(0);
			$('.popupprev').hide(0);
			clearTimeout(myTimeout);
			current_sound.stop();
			popcount++;
			popupcreater();
		});
		$('.popupprev').click(function(){
			$('.popupnext').hide(0);
			$('.popupprev').hide(0);
			clearTimeout(myTimeout);
			current_sound.stop();
			popcount--;
			popupcreater();
		});
		$('.wrong_button').click(function(){
			close_popup_window();
		});
		if(countNext==0){
			switch(popcount){
				case 0:
					$('.popupnext').show(0);
					current_sound.stop();
					current_sound = createjs.Sound.play(sound_1);
					current_sound.play();
					current_sound.on("complete", function(){
						current_sound.stop();
						current_sound = createjs.Sound.play(sound_2);
						current_sound.play();
					});
					break;
				case 1:
					$('.popupprev').show(0);
					$('.popupnext').show(0);
					current_sound.stop();
					current_sound = createjs.Sound.play(sound_3);
					current_sound.play();
					current_sound.on("complete", function(){
						current_sound.stop();
						current_sound = createjs.Sound.play(sound_4);
						current_sound.play();
					});
					break;
				case 2:
					$('.popupprev').show(0);
					myTimeout = setTimeout(function(){
						$('.door-m').show(0);
						$('.door-m').addClass('door-m-anim');
						$('.museum-p0').attr('src', imgpath + "museum-1.svg");
						$('.museum-p0').removeClass('museum-p0-anim-2').addClass('museum-p0-anim-3');
						$('.mus-room').show(0);
						$('.popupnext').show(0);
					}, 4000);
					break;
				case 3:
					$('.popupprev').show(0);
					$('.popupnext').show(0);
					sound_player(sound_5);
					break;
				case 4:
					$('.popupprev').show(0);
					$('.popupnext').show(0);
					sound_player(sound_6);
					break;
				case 5:
					$('.popupprev').show(0);
					$('.popupnext').show(0);
					sound_player(sound_7);
					break;
				case 6:
					$('.popupprev').show(0);
					$('.popupnext').show(0);
					sound_player(sound_8);
					break;
				case 7:
					$('.popupprev').show(0);
					sound_player(sound_9);
					$('.wrong_button').show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
					break;
				default:
					$('.popupprev').show(0);
					$('.popupnext').show(0);
					break;
			}
		}
	}
	function popup_window(click_class, event){
		if(popup_is_close){
			// console.log('before started   '+ popup_is_close);
			
			$('.click-on-map').fadeOut(1000);
			$( ".map-obj" ).css('pointer-events', 'none');
			$prevBtn.hide(0);
			$nextBtn.hide(0);
			
			$('.wrong_button').hide(0);
			popuptotal = popupcontent.length;
			
			popupcreater();
			
			popup_is_close = false;
			popcount = 0;
			setTimeout(function(){
				console.log('started   '+ popup_is_close);
				
				current_div_x = click_class.position().left + click_class.width()/2;
				current_div_y = click_class.position().top + click_class.height()/2;
				
				$('.popupdiv').css({'display': 'block', 'width': "0%", 'height': '0%', 'left':current_div_x, 'top': current_div_y});
				$('.popuppage').hide(0);
				$('.popupdiv').animate({
					'width': "95%", 
					'height': '95%', 
					'left':'50%', 
					'top': '50%'
				}, 500, function(){
					$('.popuppage').fadeIn(300);
				});
			}, 100);
		}
	}
	function close_popup_window(){
		popcount = 0;
		current_sound.stop();
		$(".drop-box").css('background-color', 'rgb(120,121,196)');
		$('.popuppage, .wrong_button').fadeOut(300);
		$('.popupdiv').fadeOut(500, function(){
			popup_is_close = true;
			console.log('completed   ' + popup_is_close);
			$( ".map-obj" ).css('pointer-events', 'all');
			$('.click-on-map').fadeIn(1000);
		});
		$prevBtn.show(0);
		if( countNext == $total_page-1){
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$nextBtn.show(0);
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}

	

	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

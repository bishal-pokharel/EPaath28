var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/";

var content=[
	//coverpage
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdiv:"lesson_title",
			textclass: "centertext chapter",
			textdata: data.string.lesson_title
		}],

		imageblock:[{
			 imagestoshow:[{
				imgclass: "img_cover1",
				imgsrc: "relativecls",
				imgid: "cover02"
			},{
				imgclass: "img_cover3",
				imgsrc: "relativecls",
				imgid: "cover03"

			}]
		}],
		lowertextblockadditionalclass: "writerinfo",
		lowertextblock:[{
			textclass: "description centertext",
			datahighlightflag: true,
			datahighlightcustomclass: "bold_italic_hilight",
			textdata: data.string.cover_pg_2
		},
		{
			textclass: "description centertext",
			datahighlightflag: true,
			datahighlightcustomclass: "bold_italic_hilight",
			textdata: data.string.cover_pg_1
		}
		,{
			textclass: "description centertext",
			datahighlightflag: true,
			datahighlightcustomclass: "bold_italic_hilight",
			textdata: data.string.cover_pg_3
		 }]
	},
	{
		// slide1
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow:[{
				imgclass: "img1_slide1",
				imgsrc: "relativecls",
				imgid: "camel"
			},{
				imgclass: "img2_slide1",
				imgsrc: "relativecls",
				imgid: "map"
			}]
		}],
		extratextblock:[
		{

			datahighlightflag: true,
			datahighlightcustomclass: "egptImg",
			textclass: "centertext content2",
			textdata: data.string.p1_s0,
			textdiv:"text1"
		}]
	},
	{
		// slide2
		contentnocenteradjust: true,
		extratextblock:[
		{
			textclass: " centertext content2",
			textdata: data.string.p1_s1_1,
			textdiv:"p1_s1_1"
		},
		{
			textdiv:"p1_s1_2",
			textclass: " centertext content2",
			textdata: data.string.p1_s1_2
		},
        {
           textdiv:"p1_s1_3",
           textclass: " centertext content2",
           textdata: data.string.p1_s1_3
        }

		],

		imageblock:[{
				 imagestoshow:[{
					imgclass: "img_slide2",
					imgsrc: "",
					imgid: "hut1"
				}]
			}],
	},
	{
		// slide3
		contentnocenteradjust: true,
		extratextblock:[
		{
			textdiv: "p1_s2_1",
			textclass: "centertext content",
			textdata: data.string.p1_s2_1
		},
		{
			textdiv: "p1_s2_2",
			textclass: "centertext content2",
			textdata: data.string.p1_s2_2
		}
		],

		exerciseblock: [
			{
        		exeoptions: [
					{
                        forshuffle: "class1 content2",
                        forshufflecls: "centertext",
						optdata: data.string.p1_s2_4,
					},
					{
                        forshuffle: "class2 content2",
                        forshufflecls: "centertext",
						optdata: data.string.p1_s2_5,
					},
					{
                        forshuffle: "class3 content2",
                        forshufflecls: "centertext",
						optdata: data.string.p1_s2_6,
					},
					{
                        forshuffle: "class4 content2",
                        forshufflecls: "centertext",
						optdata: data.string.p1_s2_7,
					}]
				}]
		}
];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
	var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			{id: "hut1", src: imgpath+"hut.png", type: createjs.AbstractLoader.IMAGE},
			{id: "map", src: imgpath+"map.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover01", src: imgpath+"cover01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cover02", src: imgpath+"cover02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover03", src: imgpath+"cover03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "camel", src: imgpath+"camel.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "dummy", src: imgpath+"dummy.png", type: createjs.AbstractLoader.IMAGE},

			{id: "eye", src: imgpath+"eye.png", type: createjs.AbstractLoader.IMAGE},
			{id: "close", src: imgpath+"close.png", type: createjs.AbstractLoader.IMAGE},

			//sound
			{id: "p1_s0", src: soundAsset+"p1_s0.ogg"},
			{id: "p1_s1", src: soundAsset+"p1_s1.ogg"},
			{id: "p1_s2", src: soundAsset+"p1_s2.ogg"},
			{id: "p1_s3", src: soundAsset+"p1_s3.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
				// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				if(countNext == $total_page - 1)
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 2500);
		}
   }

	/*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var timeoutcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		switch(countNext){

			case 0:
				createjs.Sound.stop();
                sound_player("p1_s0");
                // call navigation controller
   				 navigationcontroller();
   			break;

			case 1:
				createjs.Sound.stop();
   				sound_player("p1_s1");
					setTimeout(function(){
						$(".egptImg")
						.mouseenter(function() {
							 $(".img2_slide1").show(0);
						 })
						 .mouseleave(function(){
 							 $(".img2_slide1").hide(100);
						 });
					},1000);
			break;

			case 2:
				createjs.Sound.stop();
				sound_player("p1_s2");
    			break;

			case 3:
				createjs.Sound.stop();
				sound_player("p1_s3");
				shufflehint(data.string.p1_s2_4);
				checkans();
				break;
		}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete',function(){
				navigationcontroller();
			});
		}
	}

/*=====  End of Templates Block  ======*/


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				// console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty('containsdialog')){
				var containsdialog = content[count].containsdialog;
				for(var i=0; i<containsdialog.length; i++){
					var image_src = preload.getResult(containsdialog[i].imgid).src;
					//get list of classes
					var classes_list = containsdialog[i].dialogimageclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');


    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/



    function shufflehint(correctans) {
        var optiondiv = $(".optionsdiv");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["optionscontainer class1","optionscontainer class2","optionscontainer class3","optionscontainer class4"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().trim()==correctans){
                $this.addClass("correct")
            }
        });

    }
    function checkans(){
        $(".optionscontainer").on("click",function () {
            if($(this).hasClass("correct") ) {
                $(this).addClass("correctans");
                $(".optionscontainer").addClass("avoid-clicks");
                // $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                $(this).find(".corctopt").show();
                play_correct_incorrect_sound(1);
                navigationcontroller();
            }
            else{
                $(this).addClass("wrongans");
                // $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                $(this).find(".wrngopt").show();
                play_correct_incorrect_sound(0);
            }
        });
    }
});

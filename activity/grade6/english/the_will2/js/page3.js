var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/";


var content=[
	{
		// slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: "background_yellow",
		imageblock:[{
			imagestoshow:[{
				imgclass: "img3_slide1",
				imgsrc: "",
				imgid: "cover02"
			}]
		}],

		extratextblock:[
		{
			textdiv: "p3_s0_1",
			textclass: "centertext title",
			textdata: data.string.p3_s0_1
		},
		{
			textdiv: "p3_s0_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s0_2
		}]
	},
	{
		// slide1
		contentnocenteradjust: true,
		extratextblock:[
		{
			textdiv: "p3_s1_1",
            textclass: "centertext content2",
            textdata: data.string.p3_s1_1
		},

		{
			textdiv: "p3_s1_2 question",
			textclass: "centertext content2",
			textdata: data.string.p3_s1_2,
            ans:data.string.p3_s1_1
		}],

		exerciseblock: [
			{
				exeoptions: [
					{
                        forshuffle: "class1",
                        forshufflecls: "centertext content2",
						optdata: data.string.p3_s1_4,

					},
					{
						forshuffle: "class2",
                        forshufflecls: "centertext content2",
                        optdata: data.string.p3_s1_5,
					},
					{
						forshuffle: "class3",
                        forshufflecls: "centertext content2",
                        optdata: data.string.p3_s1_6,
					},
					{
						forshuffle: "class4",
                        forshufflecls: "centertext content2",
                        optdata: data.string.p3_s1_7,
					}]
				}]
		},
	{
		// slide2
		contentnocenteradjust: true,
		extratextblock:[
		{
			textdiv:"p3_s2_1",
			textclass: "centertext content2",
			textdata: data.string.p3_s2_1
		},
		{
            textdiv:"p3_s2_2",
			textclass: "centertext content2",
			textdata: data.string.p3_s2_2
		}
		],

		imageblock:[{
			imagestoshow:[{
				imgclass: "img3_slide3",
				imgsrc: "",
				imgid: "quarelling"
			}]
		}]
	},
	{
		// slide3
		contentnocenteradjust: true,
        uppertextblockadditionalclass:"p3_s3_1",
		uppertextblock: [{
			textclass: "centertext content2",
			textdata: data.string.p3_s3_1
		}],

		 contentblockadditionalclass: 'background-yellow',

        lowertextblockadditionalclass:"loweroptionsdiv",
		lowertextblock: [
			{
				textdiv:"optionscontainer1 class2",
				textclass: "centertext content2",
				textdata: data.string.p3_s3_3
		    },
			{
                textdiv:"optionscontainer1 class1",
				textclass: "centertext content2",
				textdata: data.string.p3_s3_4
		    },
			{
                textdiv:"optionscontainer1 class3",
				textclass: "centertext content2",
				textdata: data.string.p3_s3_5
		    },
			{
                textdiv:"optionscontainer1 class4",
				textclass: "centertext content2",
				textdata: data.string.p3_s3_6
		    }
		],
	},
	{
		// slide4
		contentnocenteradjust: true,
		extratextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass:"bold",
			textdiv: "p3_s4_1",
			textclass: "centertext content2",
			textdata: data.string.p3_s4_1
		}]
	},
	{
		// slide5
		imageblock:[{
			imagestoshow:[{
				imgclass: "img3_slide5",
				imgsrc: "",
				imgid: "cover02"
			}]
		}],

		contentnocenteradjust: true,
			contentblockadditionalclass: "background_yellow",
		extratextblock:[
		{
			textdiv:"p3_s5_1",
			textclass: "centertext chapter",
			textdata: data.string.p3_s5_1
		},
		{
			textdiv: "p3_s5_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s5_2
		}]
	},
	{
		//slide6
		imageblock:[{
            imgcontainerdiv:"img1",
			imagestoshow:[{
				imgclass: "relativecls",
				imgsrc: "",
				imgid: "group_camel"
			}]
		}],

		contentnocenteradjust: true,
		extratextblock:[
		{
			textdiv:"p3_s6_1 slideL",
			textclass: "centertext content2",
			textdata: data.string.p3_s6_1
		}]
	},
	{
			// slide7
		contentnocenteradjust: true,
		imageblock:[{

			imagestoshow:[{
				imgclass: "img1_slide7",
				imgsrc: "",
				imgid: "camel"
			}, {

				imgclass: "img2_slide7",
				imgsrc: "",
				imgid: "band"
			},
			{
				imgclass: "img2_slide9a",
				imgsrc: "",
				imgid: "01"
			},{

				imgclass: "img2_slide10a",
				imgsrc: "",
				imgid: "cloud"
			},
			{

				imgclass: "img_scratch",
				imgsrc: "",
				imgid: "scratch"
			},
			{

				imgclass: "img_scratch1",
				imgsrc: "",
				imgid: "scratch"
			},
			{
				imgclass: "img3_slide9b",
				imgsrc: "",
				imgid: "09"
			}]
		}],

		extratextblock:[
		{
			textdiv:"p3_s7_1",
			textclass: "centertext content2",
			textdata: data.string.p3_s7_1
		},
		{
			textdiv: "p3_s7_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_2
		},

		{
			textdiv: "p3_s7_1_1",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_1
		},
		{
			textdiv: "p3_s7_1_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_2
		},
		{
			textdiv: "p3_s7_1_3",
			textclass: "centertext content2",
			textdata: data.string.p3_s7_1_3
		}]
	},
	{
		//slide 8
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow:[{
				imgclass: "img1_slide7",
				imgsrc: "",
				imgid: "camel"
			}, {

				imgclass: "img2_slide7",
				imgsrc: "",
				imgid: "band"
			},

		{
				imgclass: "img9_slide9",
				imgsrc: "",
				imgid: "09"
			},{
				imgclass: "img_cloud",
				imgsrc: "",
				imgid: "cloud"

			},{
				imgclass: "img04_slide8",
				imgsrc: "",
				imgid: "06"
			},{
				imgclass: "img_normal",
				imgsrc: "",
				imgid: "002"

			},
			{

				imgclass: "img_scratch_1",
				imgsrc: "",
				imgid: "scratch"
			},
			{
				imgclass: "img_scratch1_1",
				imgsrc: "",
				imgid: "scratch"
			}]
		}],
		extratextblock:[
		{
            textdiv:"p3_s7_1",
            textclass: "centertext content2",
			textdata: data.string.p3_s8
		},
		{
			textdiv: "p3_s7_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_2
		},

		{
			textdiv: "p3_s7_1_1",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_1
		},
		{
            textdiv: "p3_s7_1_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_2
		},
		{
            textdiv: "p3_s7_1_3",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_3
		}
	]
	}
	,{
		//slide 9

		contentnocenteradjust: true,

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "img1_slide7",
				imgsrc: "",
				imgid: "camel"
			}, {

				imgclass: "img2_slide7",
				imgsrc: "",
				imgid: "band"
			},
			{
				imgclass: "img2_slide10a",
				imgsrc: "",
				imgid: "cloud"
			},{
				imgclass: "img2_slide9a",
				imgsrc: "",
				imgid: "03"
			},
			{

				imgclass: "img_scratch",
				imgsrc: "",
				imgid: "scratch"
			},
			{

				imgclass: "img_scratch1",
				imgsrc: "",
				imgid: "scratch"
			},

			{
				imgclass: "img9_slide9",
				imgsrc: "",
				imgid: "09"
			}
			,{
				imgclass: "img3_slide9b",
				imgsrc: "",
				imgid: "02"
			}

			,{
				imgclass: "img1_slide12",
				imgsrc: "",
				imgid: "06"
			}]
		}],

		extratextblock:[
		{
			textdiv:"p3_s7_1",
			textclass: "centertext content2",
			textdata: data.string.p3_s9
		},
		{
            textdiv: "p3_s7_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_2
		},

		{
            textdiv: "p3_s7_1_1",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_1
		},
		{
            textdiv: "p3_s7_1_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_2
		},
		{
            textdiv: "p3_s7_1_3",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_3
		}]
	},
	{

	//slide 10
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow:[{
				imgclass: "img1_slide17",
				imgsrc: "",
				imgid: "09"
			}, {

				imgclass: "img4_slide17",
				imgsrc: "",
				imgid: "06"
			},
			{

				imgclass: "img3_slide17",
				imgsrc: "",
				imgid: "02"
			},{
				imgclass: "group_camel17",
				imgsrc: "",
				imgid: "group_camel"

			}]
		}],

		extratextblock:[{
			textdiv: "p3_s17",
			textclass: "centertext content2",
			textdata: data.string.p3_s17
		},{
            textdiv: "p3_s17_1",
            textclass: "centertext content2",
            textdata: data.string.p3_s17_1
		},{
            textdiv: "p3_s17_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s8
		}, {
            textdiv: "p3_s17_3",
            textclass: "centertext content2",
            textdata: data.string.p3_s9
		}]
	},
	{
		//slide11
	contentnocenteradjust: true,
	imageblock:[{
			imagestoshow:[

				{
				imgclass: "img1_slide7",
				imgsrc: "",
				imgid: "camel"
			},{
				imgclass: "img9_slide9a",
				imgsrc: "",
				imgid: "09"
			},{
				imgclass: "img1_slide12a",
				imgsrc: "",
				imgid: "06"
			},{

				imgclass: "img1_slide15a",
				imgsrc: "",
				imgid: "02"
			},{
				imgclass: "discuss-1",
				imgsrc: "",
				imgid: "discuss-1"

			}]
		}],
		extratextblock:[
		{
			textdiv: "p3_s18",
			textclass: "centertext content2",
			textdata: data.string.p3_s18
		},
		{
            textdiv: "p3_s7_1_1a",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_1
		},
		{
			textdiv: "p3_s7_1_2",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_2
		},
		{
			textdiv: "p3_s7_1_3",
            textclass: "centertext content2",
            textdata: data.string.p3_s7_1_3
		}
	  ]
	},
	{
		//slide 12
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow:[{
				imgclass: "discuss-1",
				imgsrc: "",
				imgid: "discuss-1"
			}]
		}],
		extratextblock:[
		{
			textdiv: "p3_s19",
			textclass: "centertext content2",
			textdata: data.string.p3_s19
		}]
	},
	{
		//slide 13
		contentnocenteradjust: true,
		contentblockadditionalclass: "background_yellow",
		extratextblock:[
		{
			textdiv: "p3_s13_1",
			textclass: "centertext chapter",
			textdata: data.string.p3_s13_1
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "italic",
			textdiv: "p3_s13",
			textclass: "centertext content2",
			textdata: data.string.p3_s13
		}],
        imageblockadditionalclass:"endimg",
        imageblock:[{
            imagestoshow:[{
            	imgclass: "discuss-1",
            	imgsrc: "",
            	imgid: "discuss-1"
            }
            ]
        }],
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var current_sound;
  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var classSelect="";
  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "1-by-9-23", src: imgpath+"1-by-9-23.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1-by-9", src: imgpath+"1-by-9.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1-by-9-cut", src: imgpath+"1-by-9-cut.png", type: createjs.AbstractLoader.IMAGE},
			{id: "01", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "002", src: imgpath+"002.png", type: createjs.AbstractLoader.IMAGE},
			{id: "03", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "01b", src: imgpath+"01b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "02b", src: imgpath+"02b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "03b", src: imgpath+"03b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "scratch", src: imgpath+"scratch.png", type: createjs.AbstractLoader.IMAGE},
			{id: "discuss-1", src: imgpath+"discuss-1.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud", src: imgpath+"cloud.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "02", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "06", src: imgpath+"06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "09", src: imgpath+"09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1-by-3", src: imgpath+"1-by-3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1-by-3-cut", src: imgpath+"1-by-3-cut.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover02", src: imgpath+"cover02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1-by-2-22", src: imgpath+"1-by-2-22.png", type: createjs.AbstractLoader.IMAGE},
			{id: "group_camel", src: imgpath+"group_camel.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "quarelling", src: imgpath+"quarelling.png", type: createjs.AbstractLoader.IMAGE},
			{id: "band", src: imgpath+"band.png", type: createjs.AbstractLoader.IMAGE},
			{id: "camel", src: imgpath+"camel.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "p3_s0", src: soundAsset+"p3_s0.ogg"},
			{id: "p3_s1", src: soundAsset+"p3_s1.ogg"},
			{id: "p3_s1_1", src: soundAsset+"s3_p2.ogg"},
			{id: "p3_s2", src: soundAsset+"p3_s2.ogg"},
			{id: "p3_s3", src: soundAsset+"p3_s3.ogg"},

			{id: "p3_s4", src: soundAsset+"p3_s4.ogg"},
			{id: "p3_s5", src: soundAsset+"p3_s5.ogg"},
			{id: "p3_s6", src: soundAsset+"p3_s6.ogg"},
			{id: "p3_s7", src: soundAsset+"p3_s7.ogg"},
			{id: "p3_s8", src: soundAsset+"p3_s8.ogg"},
			{id: "p3_s9", src: soundAsset+"p3_s9.ogg"},
			{id: "p3_s11", src: soundAsset+"p3_s11.ogg"},
			{id: "p3_s12", src: soundAsset+"p3_s12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
			// 	if(countNext == $total_page - 1)
			// 		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 2500);

					// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/



 var timeoutcontroller;
  var ansCheck = 0;
  function generalTemplate(slideleft) {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);

	    if(countNext > 0){
			var html_prev = template(content[countNext-1]);
		}
		// $board_prev.html(html_prev);

		var html_next = template(content[countNext+1]);;
		// $board_next.html(html_next);


		$board.html(html);
		put_image(content, countNext);


		switch(countNext){
			case 0:
					sound_player("p3_s0");
    		break;
			case 1:
				current_sound.stop();
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('p3_s1');
				current_sound.play();
                current_sound.on('complete',function(){
                    current_sound = createjs.Sound.play('p3_s1_1');
                    current_sound.play();
                });
	      shufflehint();
				$(".optionscontainer").click(function(){
								$('.optionscontainer').removeClass("selectedoption avoid-clicks").not($(this));
	            	$(this).addClass("selectedoption avoid-clicks");
	            	navigationcontroller();
	            	classSelect=$(this).find('p').text().trim();
								console.log(classSelect);
				});
    		break;
			case 2:
				sound_player("p3_s2");
    		break;
			case 3:
				console.log('your prediction is: '+classSelect);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('p3_s3');
					current_sound.play();

					current_sound.on('complete',function(){
						navigationcontroller();
									$(".class1").css("background-color", "green");
									var class1ptext =  $(".class1 p").text().toString().trim()
									$(".class1 p").append("<br>Correct Answer!");
									if (classSelect == class1ptext){
										$(".class1 p").css("background-color", "green");
										$(".class1 p").append("&nbsp;&nbsp;Your prediction was correct!")
									}
									else if (classSelect == $(".class2 p").text().toString().trim()){
										$(".class2 p").css("background-color", "red");
										$(".class2 p").append("<br>Your Prediction.")

									}
									else if (classSelect == $(".class3 p").text().toString().trim()){
										$(".class3 p").css("background-color", "red");
										$(".class3 p").append("<br>Your Prediction.")

									}
									else if (classSelect == $(".class4 p").text().toString().trim()){
										$(".class4 p").css("background-color", "red");
										$(".class4 p").append("<br>Your Prediction.")
										}
								});
								break;

			case 4:
					sound_player("p3_s4");
    		break;
			case 5:
					sound_player("p3_s5");
    		break;

			case 6:
				createjs.Sound.stop();
                sound_player("p3_s6");
				break;

			case 7:
				createjs.Sound.stop();
				sound_player("p3_s7");

				setTimeout(function(){
					$(".img2_slide10a").show(0);
				},1000);

				setTimeout(function(){
						$(".img2_slide9a").show(0);
				},2000);

				setTimeout(function(){

					$(".img2_slide9d").show(0);

				},2501);


				setTimeout(function(){

					$(".img_scratch").show(0);

				},3401);

				setTimeout(function(){

					$(".img_scratch1").show(0);

				},3401);

				setTimeout(function(){

					$(".img3_slide9b").fadeIn();
					$(".img3_slide9b").animate({
							left: '86%',
							top: '34%'
					},2600);

				},4100);
				break;

		case 8:
				createjs.Sound.stop();
					sound_player("p3_s8");
				setTimeout(function(){
					$(".img_cloud").show(0);
				},1000);

				setTimeout(function(){
						$(".img_normal").show(0);
				},2000);

				setTimeout(function(){

					$(".img2_slide9d").show(0);

				},2501);


				setTimeout(function(){

					$(".img_scratch_1").show(0);

				},3501);

				setTimeout(function(){

					$(".img_scratch1_1").show(0);

				},3501);

				setTimeout(function(){

					$(".img04_slide8").fadeIn();
					$(".img04_slide8").animate({
							left: '86%',
							top: '54%'

					},2600);


				},4100);
		break;

		case 9:
			createjs.Sound.stop();
				sound_player("p3_s9");
			setTimeout(function(){
					$(".img2_slide10a").show(0);
				},1000);

				setTimeout(function(){
						$(".img2_slide9a").show(0);
				},2000);

				setTimeout(function(){

					$(".img2_slide9d").show(0);

				},2501);


				setTimeout(function(){

					$(".img_scratch").show(0);

				},3501);

				setTimeout(function(){

					$(".img_scratch1").show(0);

				},3501);

					setTimeout(function(){

					$(".img3_slide9b").fadeIn();
					$(".img3_slide9b").animate({
							left: '86%',
							top: '80%'

						},2600);

				},4100);

		break;
		case 10:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("p3_s7");
			current_sound.play();
			setTimeout(function(){
				current_sound = createjs.Sound.play("p3_s8");
				current_sound.play();
			},10000);
			setTimeout(function(){

				sound_player("p3_s9");
			},20000);


			break;
		case 11:
				createjs.Sound.stop();
				sound_player("p3_s11");
			break;
		case 12:
				createjs.Sound.stop();
				sound_player("p3_s12");
			break;
		case 13:
		createjs.Sound.stop();
		navigationcontroller();
		break;
		}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete',function(){
				navigationcontroller();
			});
		}




 // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
  }




/*=====  End of Templates Block  ======*/

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}

		if(content[count].hasOwnProperty('containsdialog')){
				var containsdialog = content[count].containsdialog;
				for(var i=0; i<containsdialog.length; i++){
					var image_src = preload.getResult(containsdialog[i].imgid).src;
					//get list of classes
					var classes_list = containsdialog[i].dialogimageclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */




  function templateCaller(){
    /*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');


			// call the template

			generalTemplate();



			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */


	$nextBtn.on('click', function() {
				createjs.Sound.stop();
        countNext++;
        templateCaller();

	// if(countNext > 6 && countNext < 9){
		// put_image(content, countNext+1);
		// 	countNext++;
		// 	$(this).hide(0);
		// 	$prevBtn.hide(0);
		// 	$board_next.addClass("move_left_2");
		// 	$board.addClass("move_left_1");
		// 	setTimeout(function(){
		// 		$board_next.removeClass("move_left_2");
		// 		$board.removeClass("move_left_1");
		// 		templateCaller();
		// 	}, 4000);
		// }else {
		// 	countNext++;
		// 	 if(countNext == 17){
		// 	 	 $(".additional_ltb > p").show(0).addClass("expand");
		// 		 setTimeout(function(){
		// 			 templateCaller();
		// 		 }, 1550);
		// 	 }else{
		// 		templateCaller();
		// 	 }
		// }
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		// if(countNext > 7 && countNext < 10){
		// put_image(content, countNext-1);
		// 	countNext--;
		// 	$(this).hide(0);
		// 	$nextBtn.hide(0);
		// 	$board.addClass("move_right_2");
		// 	// $board_prev.addClass("move_right_1");
		// 	setTimeout(function(){
		// 		$board.removeClass("move_right_2");
		// 		// $board_prev.removeClass("move_right_1");
		// 		templateCaller();
		// 	}, 4000);
		// }else{
		// 	countNext--;
		// 	templateCaller();
		// }
        clearTimeout(timeoutcontroller);
        countNext--;
        templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


/*=====  End of Templates Controller Block  ======*/
    function shufflehint() {
        var correctans = $(".question").attr("data-answer");
        var optiondiv = $(".optionsdiv");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["optionscontainer class1","optionscontainer class2","optionscontainer class3","optionscontainer class4"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().trim()==correctans){
                $this.addClass("correct")
            }
        });
    }


});

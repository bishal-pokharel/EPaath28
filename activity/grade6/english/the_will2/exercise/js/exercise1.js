Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sound/";


var content=[

	//ex1
	{
		additionalclasscontentblock:"bg",
		// imageblock:[{
		// 	imagestoshow:[{
		// 		imgclass: "band",
		// 		imgsrc: "",
		// 		imgid: "band"
		// 	}]
		// }],
		exerciseblock: [
			{
      			textdata: data.string.exq1,

				exeoptions: [
					{
						forshuffle: "class3",
						optdata: data.string.exo1a,

					},
					{
						forshuffle: "class2",
						optdata: data.string.exo1b,
					},
					{
						forshuffle: "class1",
						optdata: data.string.exo1c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo1d,
					}]
			}]
	},
	//ex2
	{
		exerciseblock: [
			{
				textdata: data.string.exq2,

				exeoptions: [
					{
						forshuffle: "class2",
						optdata: data.string.exo2a,
					},
					{
						forshuffle: "class1",
						optdata: data.string.exo2b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.exo2c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo2d,
					}]

			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
				textdata: data.string.exq3,

				exeoptions: [
					{
						forshuffle: "class2",
						optdata: data.string.exo3a,
					},
					{
						forshuffle: "class1",
						optdata: data.string.exo3b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.exo3c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo3d,
					}]
			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
				textdata: data.string.exq4,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.exo4a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.exo4b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.exo4c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo4d,
					}]

			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
				textdata: data.string.exq5,

				exeoptions: [
					{
						forshuffle: "class3",
						optdata: data.string.exo5a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.exo5b,
					},
					{
						forshuffle: "class1",
						optdata: data.string.exo5c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo5d,
					}]
			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
				textdata: data.string.exq6,

				exeoptions: [
					{
						forshuffle: "class3",
						optdata: data.string.exo6a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.exo6b,
					},
					{
						forshuffle: "class1",
						optdata: data.string.exo6c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo6d,
					}]

			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{
       		textdata: data.string.exq7,
        
				exeoptions: [
					{
						forshuffle: "class4",
						optdata: data.string.exo7a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.exo7b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.exo7c,
					},
					{
						forshuffle: "class1",
						optdata: data.string.exo7d,
					}]

			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
       		textdata: data.string.exq8,
       
				exeoptions: [
					{
						forshuffle: "class2",
						optdata: data.string.exo8a,
					},
					{
						forshuffle: "class1",
						optdata: data.string.exo8b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.exo8c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo8d,
					}]

			}
		]
	},
	//ex9
	{
		exerciseblock: [
			{
        		textdata: data.string.exq9,
       		exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.exo9a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.exo9b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.exo9c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo9d,
					}]

			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
      		textdata: data.string.exq10,
      
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.exo10a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.exo10b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.exo10c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.exo10d,
					}]
			}
		]
	},
];

/*remove this for non random questions*/
//content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	loadTimelineProgress($total_page,countNext+1);
var preload;
	var timeoutvar = null;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "band", src: imgpath+"band.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex_ins", src: soundAsset+"ex_ins.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}

	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
				// call main function
		templateCaller();
	}
	//initialize
	init();


	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new LampTemplate();
	testin.init(10);

	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
    	texthighlight($board);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.titleno').append('<p>Click on the correct option.</p>').css({
			'position': 'absolute',
			'left': '4%',
			'top': '3%',
			'font-size':' 3.5vmin'
		});


		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		countNext==0 ? sound_player('ex_ins') : '';
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;

		if (countNext < 10){
			$(".contentblock").css({'background-image': 'url("'+preload.getResult('band').src+'")',
	    					'background-size': '100% 25%',
	    					'background-repeat': 'no-repeat',
	    					'background-position': '0% 100%'});
		}

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62fff");
						$(this).css("border","5px solid #deef3c");
         				  $(this).css("color","white");
						$(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		 loadTimelineProgress($total_page,countNext+1);
		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	// templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
}

  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

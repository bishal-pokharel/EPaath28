var imgpath = $ref + "/images/p2/";
var soundAsset = $ref+"/sounds/p2/";

var sound_1 = "1";
var sound_2 = "2";
var sound_3 = "3";
var sound_4 = "4";
var sound_5 = "5";
var sound_6 = "6";
var sound_7 = "7";
var sound_8 = "8";
var sound_9 = "9";
var sound_10 = "10";
var sound_11 = "11";
var sound_12 = "12";
var sound_13 = "13";
var sound_14 = "14";
var sound_15 = "15";

createjs.Sound.registerSound((soundAsset + "p2_s0_0.ogg"), sound_1);
createjs.Sound.registerSound((soundAsset + "p2_s0_paintings.ogg"), sound_2);
createjs.Sound.registerSound((soundAsset + "p2_s0_1.ogg"), sound_3);
createjs.Sound.registerSound((soundAsset + "p2_s1_1.ogg"), sound_4);
createjs.Sound.registerSound((soundAsset + "p2_s1_2.ogg"), sound_5);
createjs.Sound.registerSound((soundAsset + "p2_s1_spotlight.ogg"), sound_6);
createjs.Sound.registerSound((soundAsset + "p2_s2_0.ogg"), sound_7);
createjs.Sound.registerSound((soundAsset + "p2_s2_urn.ogg"), sound_8);
createjs.Sound.registerSound((soundAsset + "p2_s2_1.ogg"), sound_9);
createjs.Sound.registerSound((soundAsset + "p2_s3_0.ogg"), sound_10);
createjs.Sound.registerSound((soundAsset + "p2_s3_ornaments.ogg"), sound_11);
createjs.Sound.registerSound((soundAsset + "p2_s3_1.ogg"), sound_12);
createjs.Sound.registerSound((soundAsset + "p2_s4_0.ogg"), sound_13);
createjs.Sound.registerSound((soundAsset + "p2_s4_basket.ogg"), sound_14);
createjs.Sound.registerSound((soundAsset + "p2_s4_1.ogg"), sound_15);

// var sound_gr_1 = [sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-sky',

		extratextblock:[{
			textdata : data.string.p2text16,
			textclass: 'instruction-text'
		}],


		imageblock : [{
			imagestoshow : [
				{
					imgclass : "basket",
					imgsrc : imgpath + "basket.png",
				},
				{
					imgclass : "painting",
					imgsrc : imgpath + "painting.png",
				},
				{
					imgclass : "pot",
					imgsrc : imgpath + "pot.png",
				},
				{
					imgclass : "ornaments",
					imgsrc : imgpath + "jewellery.png",
				},
				{
					imgclass : "spotlight-2 spotlight",
					imgsrc : imgpath + "light03.png",
				},
				{
					imgclass : "spotlight-3 spotlight",
					imgsrc : imgpath + "light02.png",
				},
				{
					imgclass : "spotlight-1 spotlight",
					imgsrc : imgpath + "light01.png",
				},{
					imgclass : "bg_full ",
					imgsrc : imgpath + "bg.png",
				}
			],
		}],
		popupblock:[{
			popupclass: '',
			headerdata: data.string.p2text14,
			textdata : data.string.p2text15,
			textclass: 'popup-desc-text',
			imgclass : "popup-imageh",
			imgsrc : imgpath + "painting.png",
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-sky',

		extratextblock:[{
			textdata : data.string.p2text17,
			textclass: 'instruction-text'
		}],


		imageblock : [{
			imagestoshow : [
				{
					imgclass : "basket",
					imgsrc : imgpath + "basket.png",
				},
				{
					imgclass : "painting",
					imgsrc : imgpath + "painting.png",
				},
				{
					imgclass : "pot",
					imgsrc : imgpath + "pot.png",
				},
				{
					imgclass : "ornaments",
					imgsrc : imgpath + "jewellery.png",
				},
				{
					imgclass : "spotlight-2 spotlight",
					imgsrc : imgpath + "light03.png",
				},
				{
					imgclass : "spotlight-3 spotlight",
					imgsrc : imgpath + "light02.png",
				},
				{
					imgclass : "spotlight-1 spotlight",
					imgsrc : imgpath + "light01.png",
				},{
					imgclass : "bg_full ",
					imgsrc : imgpath + "bg.png",
				}
			],
		}],
		popupblock:[{
			popupclass: '',
			headerdata: data.string.p2text18,
			textdata : data.string.p2text19,
			textclass: 'popup-desc-text',
			imgclass : "popup-imageh",
			imgsrc : imgpath + "3_lights.png",
		}]
	},


		//slide2
		{
			hasheaderblock : false,
			contentblocknocenteradjust : true,
			contentblockadditionalclass : 'bg-sky',

			extratextblock:[{
				textdata : data.string.p2text20,
				textclass: 'instruction-text'
			}],


			imageblock : [{
				imagestoshow : [
					{
						imgclass : "basket",
						imgsrc : imgpath + "basket.png",
					},
					{
						imgclass : "painting",
						imgsrc : imgpath + "painting.png",
					},
					{
						imgclass : "pot",
						imgsrc : imgpath + "pot.png",
					},
					{
						imgclass : "ornaments",
						imgsrc : imgpath + "jewellery.png",
					},
					{
						imgclass : "spotlight-2 spotlight",
						imgsrc : imgpath + "light03.png",
					},
					{
						imgclass : "spotlight-3 spotlight",
						imgsrc : imgpath + "light02.png",
					},
					{
						imgclass : "spotlight-1 spotlight",
						imgsrc : imgpath + "light01.png",
					},{
						imgclass : "bg_full ",
						imgsrc : imgpath + "bg.png",
					}
				],
			}],
			popupblock:[{
				popupclass: '',
				headerdata: data.string.p2text21,
				textdata : data.string.p2text22,
				textclass: 'popup-desc-text',
				imgclass : "popup-imageh",
				imgsrc : imgpath + "pot.png",
			}]
		},

		//slide3
		{
			hasheaderblock : false,
			contentblocknocenteradjust : true,
			contentblockadditionalclass : 'bg-sky',

			extratextblock:[{
				textdata : data.string.p2text23,
				textclass: 'instruction-text'
			}],


			imageblock : [{
				imagestoshow : [
					{
						imgclass : "basket",
						imgsrc : imgpath + "basket.png",
					},
					{
						imgclass : "painting",
						imgsrc : imgpath + "painting.png",
					},
					{
						imgclass : "pot",
						imgsrc : imgpath + "pot.png",
					},
					{
						imgclass : "ornaments",
						imgsrc : imgpath + "jewellery.png",
					},
					{
						imgclass : "spotlight-2 spotlight",
						imgsrc : imgpath + "light03.png",
					},
					{
						imgclass : "spotlight-3 spotlight",
						imgsrc : imgpath + "light02.png",
					},
					{
						imgclass : "spotlight-1 spotlight",
						imgsrc : imgpath + "light01.png",
					},{
						imgclass : "bg_full ",
						imgsrc : imgpath + "bg.png",
					}
				],
			}],
			popupblock:[{
				popupclass: '',
				headerdata: data.string.p2text24,
				textdata : data.string.p2text25,
				textclass: 'popup-desc-text',
				imgclass : "popup-imageh",
				imgsrc : imgpath + "jewellery.png",
			}]
		},

		//slide4
		{
			hasheaderblock : false,
			contentblocknocenteradjust : true,
			contentblockadditionalclass : 'bg-sky',

			extratextblock:[{
				textdata : data.string.p2text26,
				textclass: 'instruction-text'
			}],


			imageblock : [{
				imagestoshow : [
					{
						imgclass : "basket",
						imgsrc : imgpath + "basket.png",
					},
					{
						imgclass : "painting",
						imgsrc : imgpath + "painting.png",
					},
					{
						imgclass : "pot",
						imgsrc : imgpath + "pot.png",
					},
					{
						imgclass : "ornaments",
						imgsrc : imgpath + "jewellery.png",
					},
					{
						imgclass : "spotlight-2 spotlight",
						imgsrc : imgpath + "light03.png",
					},
					{
						imgclass : "spotlight-3 spotlight",
						imgsrc : imgpath + "light02.png",
					},
					{
						imgclass : "spotlight-1 spotlight",
						imgsrc : imgpath + "light01.png",
					},{
						imgclass : "bg_full ",
						imgsrc : imgpath + "bg.png",
					}
				],
			}],
			popupblock:[{
				popupclass: '',
				headerdata: data.string.p2text27,
				textdata : data.string.p2text28,
				textclass: 'popup-desc-text',
				imgclass : "popup-imageh",
				imgsrc : imgpath + "basket.png",
			}]
		}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = createjs.Sound.play(sound_1);
	var myTimeout =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();



	var current_div_x;
	var current_div_y;
	var popcount = 0;
	var popupcontent = '';
	var popuptotal = 0;
	var popup_is_close =  true;
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
			setTimeout(function(){
				sound_player(sound_1);
			},500);
			show_div('painting', sound_2, sound_3);
			break;
			case 1:
			sound_player(sound_4);
			show_div('spotlight',sound_6, sound_5);
			break;
			case 2:
			sound_player(sound_7);
			show_div('pot', sound_8, sound_9);
			break;
			case 3:
			sound_player(sound_10);
			show_div('ornaments', sound_11, sound_12);
			break;
			case 4:
			sound_player(sound_13);
			show_div('basket',sound_14, sound_15);
			break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	function show_div(click_class, sound_first, sound_sec){
		$('.'+click_class).addClass('hl-object');
		// $('.wrong_button').show();
		$('.'+click_class).click(function(){
			$('.popupdiv ').show(500);
			sound_player_duo(sound_first, sound_sec);
			// nav_button_controls(100);
		});
		$('.wrong_button').click(function(){
			current_sound.stop();
			$('.popupdiv ').hide(500);
			nav_button_controls(500);
		})
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		// alert(sound_data);
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
	}

	function sound_player_duo(sound_data_1, sound_data_2){
		current_sound.stop();
		current_sound = createjs.Sound.play(sound_data_1);
		current_sound.play();
		current_sound.on("complete", function(){
			sound_player_1(sound_data_2);
		})
	}
	function sound_player_1(sound_data){
		current_sound.stop();
		// alert(sound_data);
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		current_sound.on("complete", function(){
			$(".wrong_button").show();
			// nav_button_controls();
		})
	}



	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}



	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

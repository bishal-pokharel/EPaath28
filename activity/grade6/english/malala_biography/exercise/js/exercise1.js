Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
};

var imgpath = $ref+"/exercise/images/";

var content=[

	//ex1
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.q1,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q1_o1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q1_o2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q1_o3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q1_o4,
					}]

			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.q2,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q2_o1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q2_o2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q2_o3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q2_o4,
					}]

			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.q3,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q3_o1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q3_o2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q3_o3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q3_o4,
					}]

			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.q4,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q4_o1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q4_o2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q4_o3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q4_o4,
					}]

			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.q5,
				sentdata: data.string.sentques5,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q5_o1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q5_o2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q5_o3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q5_o4,
					}]

			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.q6,
        sentdata: data.string.sentques6,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q6_o1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q6_o2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q6_o3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q6_o4,
					}]

			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.q7,
        sentdata: data.string.sentques7,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q7_o1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q7_o2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q7_o3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q7_o4,
					}]

			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.q8,
        sentdata: data.string.sentques8,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q8_o1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q8_o2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q8_o3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q8_o4,
					}]

			}
		]
	}
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = content.length;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new LampTemplate();

 	testin.init($total_page);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
    texthighlight($board);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

    var alphs = ["a) ", "b) ", "c) ", "d) "];
    var alcount = 0;
    $(".optionsdiv").children().children(".forhover").each(function(){
      $(this).prepend(alphs[alcount]);
      alcount++;
    });

		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css({ "background":"#bed62f", "border":"5px solid #deef3c", "color":"white"});
						$(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css({ "background":"#FF0000", "border":"5px solid #980000", "color":"white"});
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

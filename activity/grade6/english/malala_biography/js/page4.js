var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[
	{
		// slide 0
		contentnocenteradjust: true,
		imageblockadditionalclass: "additional_imgblk",
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "label_diy",
				imagelabeldata: data.string.p4_s0
			}],
			imagestoshow:[{
					imgclass: "image_diy",
					imgsrc: '',
					imgid : 'bg_image',
			}]
		}]
	},
	{
		//slide 1
		contentnocenteradjust: true,
		contentblockadditionalclass: "additional_content",
		uppertextblock:[{
			textclass: "question_context",
			textdata: data.string.p4_s1
		},{
			textclass: "question",
			textdata: data.string.p4_s1_q
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.p4_s2
			}],
			imagestoshow:[{
					imgclass: "image_correct",
					imgsrc: '',
					imgid : 'correct',
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.p4_s3
			}],
			imagestoshow:[{
					imgclass: "image_incorrect",
					imgsrc: '',
					imgid : 'incorrect',
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.p4_s4
			}],
			imagestoshow:[{
					imgclass: "image_incorrect",
					imgsrc: '',
					imgid : 'incorrect',
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.p4_s5
			}],
			imagestoshow:[{
					imgclass: "image_incorrect",
					imgsrc: '',
					imgid : 'incorrect',
			}]
		}]
	},
	{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: "additional_content",
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.p4_s6_q
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.p4_s7
			}],
			imagestoshow:[{
					imgclass: "image_correct",
					imgsrc: '',
					imgid : 'correct',
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.p4_s8
			}],
			imagestoshow:[{
					imgclass: "image_incorrect",
					imgsrc: '',
					imgid : 'incorrect',
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.p4_s9
			}],
			imagestoshow:[{
					imgclass: "image_incorrect",
					imgsrc: '',
					imgid : 'incorrect',
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.p4_s10
			}],
			imagestoshow:[{
					imgclass: "image_incorrect",
					imgsrc: '',
					imgid : 'incorrect',
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg_image", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: imgpath+"incorrect.png", type: createjs.AbstractLoader.IMAGE},

			//Sounds
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
		switch (countNext) {
			case 0:
			play_diy_audio();
			break;
			case 1:
			sound_player('sound_1');
			break;
			case 2:
			sound_player('sound_2');
			break;
			default:
		}
		if(countNext > 0){
		    var $imagediv = $(".imagediv");
		    var $imageblock = $(".imageblock");
		    $imageblock.html("");

		    var randomindex;
		    while($imagediv.length > 1){
		    	randomindex = Math.floor(Math.random() * ($imagediv.length + 1));
		    	$imageblock.append($($imagediv[randomindex]));
		    	$imagediv.splice(randomindex, 1);
		    }

		    $nextBtn.hide(0);
		    $imageblock.append($($imagediv[0]));

		    var answered = false;
		    $(".imagediv").click(function(){
		    	if(answered){
		    		return true;
		    	}
		    	var $this = $(this);
	    		$this.find("img").show(0);
	    		var $label = $this.find("label");
	    		$label.removeClass("onhover");
		    	if($label.hasClass("yes")){
		    		play_correct_incorrect_sound(true);
		    		$label.addClass("correct");
		    		answered = true;
		    		if((countNext+1) == ($total_page)){
						ole.footerNotificationHandler.pageEndSetNotification();
		    		}else{
			    		$nextBtn.show(0);
		    		}
		    		$(".imagediv> label").removeClass("onhover");
		    	} else {
		    		play_correct_incorrect_sound(false);
		    		$label.addClass("incorrect");
		    	}
		    });
		}
  }

/*=====  End of Templates Block  ======*/
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */
	 function sound_player(sound_id){
 			createjs.Sound.stop();
 			var current_sound = createjs.Sound.play(sound_id);
 			current_sound.play();
 		}

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sound/";


var content=[
	// slide0
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-1',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq1,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo1a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo1b,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo1c,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo1d,
				}],
			}
		]
	},
	// slide1
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-2',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq2,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo2a,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo2b,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo2c,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo2d,
				}],
			}
		]
	},
	// slide2
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-3',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq3,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo3a,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo3b,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo3c,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo3d,
				}],
			}
		]
	},
	// slide3
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-4',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq4,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo4a,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo4b,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo4c,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo4d,
				}],
			}
		]
	},
	// slide4
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-5',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq5,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo5a,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo5b,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo5c,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo5d,
				}],
			}
		]
	},
	// slide5
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-6',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq6,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo6a,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo6b,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo6c,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo6d,
				}],
			}
		]
	},
	// slide6
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-7',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq7,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo7a,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo7b,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo7c,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo7d,
				}],
			}
		]
	},
	// slide7
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-8',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq8,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo8a,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo8b,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo8c,
				},
				{
					option_class: "class2",
					optiondata: data.string.exo8d,
				}],
			}
		]
	}
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new NumberTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//sundar
			{id: "bg", src: imgpath+'bg.png', type: createjs.AbstractLoader.IMAGE},
			{id: "airplane", src: imgpath+'airplane.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-1", src: imgpath+'q01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+'q02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+'q03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-4", src: imgpath+'q04.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-5", src: imgpath+'q05.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-6", src: imgpath+'q06.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-7", src: imgpath+'q07.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-8", src: imgpath+'q08.png', type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"ex.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();


	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', ()=>{
			nav_button_controls(300);
		});
	}

	var wrongclick = false;

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		if(countNext==0) sound_player("sound_1");
		var ques_count = ['1. ','2. ','3. ','4. ','5. ','6. ','7. ','8. ','9. '];
		$('#ques-no').html(ques_count[countNext]);
		$('.center-image').attr('src', preload.getResult(content[countNext].ques_img).src);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		$('.default-bg').css({
			'background-image': 'url("'+ preload.getResult('bg').src +'")',
			'background-size': '100% 100%'
		});
		$('.aeroplane').attr('src', preload.getResult('airplane').src);
		$('.aeroplane').css({'left': (8*countNext)+'%'});

		var parent = $(".option-wrapper");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		for(var i=0; i<4; i++){
			var curStr = $('.option-label').eq(i).html();
			$('.option-label').eq(i).html(ques_count[i]+curStr);
		}

		var wrong_clicked = 0;
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				var answerData = $(this).find('p').html();
				if(wrong_clicked<1){
					scoring.update(true);
				}
				$(".option-container").css('pointer-events','none');
				createjs.Sound.stop();
	 			play_correct_incorrect_sound(1);
					$('.aeroplane').animate({'cleft': (8*(countNext+1))+'%'});
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				$('.fill-blank').html(answerData);
				$('.fill-blank').css('color', '#F63477');
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				scoring.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});

		$prevBtn.hide(0);
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

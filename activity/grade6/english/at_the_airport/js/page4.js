var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/";

var box_colors_1 = [['#20948B','#3FA39B'],
					['#CA466E','#D16082'],
					['#6A70A4','#7F84B0'],
					['#E67F5C','#E99173'],
					['#89A537','#99B153'],
					['#9566B1','#A47BBC'],
					['#A64D79','#B2668B'],
					['#5384C6','#6B95CE']];
var content=[
	//page 0
	{
		additionalgeneralTemplateblock: "purple_bg",
		additionalclasscontentblock: "light_purple_bg_2",
		contentnocenteradjust: true,
		title_class: "title_class",
		title_data: data.string.p4_s0_a,
		imgclass: "title_image",
		imgsrc: "",
		imgid: "title_image_id"
	},
	//ex1
	{
		exerciseblock: [
			{
				instructiondata: data.string.p4_s0,
				instructionhighlightflag: true,
				instructionhighlightcustomclass: 'instruction_highlight_class',
				welldone_class: "welldone_class",
				welldonedata: data.string.p4_s14,
				imgclass: "welldone_img",
				imgsrc: imgpath+ "welldone02.png",
				draggableblock:[{
					draggabletextadditionalclass: 'class_1',
					textdata: data.string.p4_s5
				},{
					draggabletextadditionalclass: 'class_2',
					textdata: data.string.p4_s7
				},{
					draggabletextadditionalclass: 'class_3',
					textdata: data.string.p4_s9
				},{
					draggabletextadditionalclass: 'class_4',
					textdata: data.string.p4_s11
				},{
					draggabletextadditionalclass: 'class_5',
					textdata: data.string.p4_s13
				}],
				droppableblock:[{
					droppabledivadditionalclass: 'drop_class_1',
					imgsrc: imgpath + '1.jpg',
					p1_data: data.string.p4_s4
				},{
					droppabledivadditionalclass: 'drop_class_2',
					imgsrc: imgpath + '2.jpg',
					p1_data: data.string.p4_s6
				},{
					droppabledivadditionalclass: 'drop_class_3',
					imgsrc: imgpath + '3.jpg',
					p1_data: data.string.p4_s8
				},{
					droppabledivadditionalclass: 'drop_class_4',
					imgsrc: imgpath + '4.jpg',
					p1_data: data.string.p4_s10
				},{
					droppabledivadditionalclass: 'drop_class_5',
					imgsrc: imgpath + '5.jpg',
					p1_data: data.string.p4_s12
				}]

			}
		]
	},
];

/*remove this for non random questions*/
//
content[1].exerciseblock[0].draggableblock.shufflearray();
content[1].exerciseblock[0].droppableblock.shufflearray();
box_colors_1.shufflearray();

// content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var is_dragging =  false;

	/*for limiting the questions to 10*/
	var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "title_image_id", src: imgpath+"domla02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "welldone02", src: imgpath+"welldone02.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			{id: "sound_1", src: soundAsset+"s4_p1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();


	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	/**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/

	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		switch(countNext){
			case 0:
				play_diy_audio();
				put_image(content, countNext);
				$nextBtn.show(0);
				break;
			case 1:
                createjs.Sound.stop();
                current_sound = createjs.Sound.play('sound_1');
                current_sound.play();
				give_colors_to_boxes();

				create_div_for_hover();

				var drop_count = 0;

				$(".nep_draggabletext").draggable({
		            containment: ".board",
		            revert: "invalid",
		            appendTo: "body",
		            zIndex: 100,
		            helper: "clone",
		            start: function(event, ui) {
		                $(this).css({
		                    "cursor": "grabbing"
		                });
		                is_dragging =  true;
		            },
		            stop: function(event, ui) {
		                $(this).css({
		                    "cursor": "grab"
		                });
		                is_dragging =  false;
		            }
		        });
		        $('.invisiblehover').hover(function(){
		        	if(is_dragging){
		        		var current_idx = $(this).attr('class');
		        		current_idx = parseInt(current_idx.replace(/[^\d]/g,''));
		        		$('.drop_class_'+current_idx).addClass('hoverd_match');
		        	}
		        }, function(){
		        	if(is_dragging){
			        	var current_idx = $(this).attr('class');
			        	current_idx = parseInt(current_idx.replace(/[^\d]/g,''));
			        	$('.drop_class_'+current_idx).removeClass('hoverd_match');
			        }
		        });
		        // $('.nep_droppablediv').hover(function(){
		        	// $(this).addClass('hoverd_match');
		        // },function(){
		        	// $(this).removeClass('hoverd_match');
		        // });
		        for(var index=1; index<=$('.nep_droppablediv').length; index++){
		        	$('.drop_class_'+index).droppable({
			            accept : ".class_"+index,
			            // hoverClass: "hoverd_match",
			            drop: function(event, ui) {
			            	play_correct_incorrect_sound(1);
			            	var current_idx = $(this).attr('class');
			            	current_idx = parseInt(current_idx.replace(/[^\d]/g,''));
			            	$('.nep_droppablediv').css('width', '100%');
			            	// $(this).find('.p1_class').css('width', '17.5%');
			            	$(this).find('.p2_class').html($(ui.draggable).html());
		  					var dt = new Date();
			            	$(this).find('.p2_class').css({
			            		'display': 'inline',
			            		// 'width': 'auto',
			            		'background-color': box_colors_1[current_idx-1][1],
			            		'background-image': 'url("images/star_1.gif?' + dt.getTime() + '")',
			            		'background-size': 'auto 100%',
			            		'background-repeat': 'no-repeat',
			            		'background-position': 'center center'
			            	});
			            	$('.invisiblehover').css('width', '97%');
			            	$(ui.draggable).hide(0);
			            	drop_count++;
			            	if(drop_count>=5){
			            		ole.footerNotificationHandler.pageEndSetNotification();
			            		$(".insturction").hide(0);
			            		$(".welldone_img").show(0);
			            		$(".welldone_class").show(0);
			            	}
			            }
			        });
		        }
				break;
			default:
				break;
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('contentnocenteradjust')){
			var imgid = content[count].imgid;
			var imageClass = content[count].imgclass;
			var image_src = preload.getResult(imgid).src;
			$("."+imageClass).attr('src', image_src);
		}
	}

	function give_colors_to_boxes(){
		for(var index=1; index<=$('.nep_droppablediv').length; index++){
			$('.drop_class_'+index).find('.p1_class').css('background-color', box_colors_1[index-1][0]);
		}
	}
	function create_div_for_hover(){
		for(var index=1; index<=$('.nep_droppablediv').length; index++){
			var x_position = $('.drop_class_'+index).position().left*100/$board.width()+'%';
			var y_position = ( $('.drop_class_'+index).position().top + $('.nep_droppableblock').position().top )*100/$board.height()+'%';
			var new_width = $('.drop_class_'+index).width()*100/$board.width()+'%';
			var new_height = $('.drop_class_'+index).height()*100/$board.height()+'%';
			console.log("new height", new_height);
			$('.contentblock').append('<div class="invisiblehover"></div>');
			$('.invisiblehover').eq(index-1).addClass('hover-class-'+index);
			$('.invisiblehover').eq(index-1).css({
				'width': new_width,
				'height': new_height,
				'left': x_position,
				'top':y_position
			});
		}
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/";

var content=[
	{
		// slide0
		contentblockadditionalclass: "background_navyblue contentblock_addition",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s0
		}],
		imageblockadditionalclass: "imageblock_addition",
		imageblock:[{
			imagestoshow:[{
				imgclass: "mrs_sharma_class",
				imgsrc: "",
				imgid: "mrs_sharma"
			}],
			imagelabels:[{
				imagelabelclass: "image_label1",
				imagelabeldata: data.string.p3_s1
			},{
				imagelabelclass: "image_label2",
				imagelabeldata: data.string.p3_s2+ "<br/><br/>"
			},{
				imagelabelclass: "image_label2",
				imagelabeldata: data.string.p3_s3+ "<br/><br/>"
			},{
				imagelabelclass: "image_label2",
				imagelabeldata: data.string.p3_s4+ "<br/><br/>"
			},{
				imagelabelclass: "image_label_note",
				imagelabeldata: data.string.p3_s5
			}]
		}]
	},{
		// slide1
		contentblockadditionalclass: "background_navyblue contentblock_addition",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s6
		}],
		imageblockadditionalclass: "imageblock_addition",
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "image_label1_b",
				imagelabeldata: data.string.p3_s7
			},{
				imagelabelclass: "image_label2_b",
				datahighlightflag: true,
				datahighlightcustomclass: "bluehighlight",
				imagelabeldata: data.string.p3_s8 + data.string.p3_s2+ "<br/> #" + data.string.p3_s9 + "@"
			},{
				imagelabelclass: "image_label2_b",
				datahighlightflag: true,
				datahighlightcustomclass: "bluehighlight",
				imagelabeldata: data.string.p3_s8 + data.string.p3_s3+ "<br/> #" + data.string.p3_s10 + "@"
			},{
				imagelabelclass: "image_label2_b",
				datahighlightflag: true,
				datahighlightcustomclass: "bluehighlight",
				imagelabeldata: data.string.p3_s8 + data.string.p3_s4+ "<br/> #" + data.string.p3_s11 + "@"
			},{
				imagelabelclass: "image_label_note",
				imagelabeldata: data.string.p3_s5
			}]
		}]
	},{
		// slide2
		contentblockadditionalclass: "background_navyblue contentblock_addition",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s12
		}],
		imageblockadditionalclass: "imageblock_addition",
		imageblock:[{
			imagestoshow:[{
				imgclass: "mrs_sharma_class2",
				imgsrc: "",
				imgid: "mrs_sharma"
			}],
			imagelabels:[{
				imagelabelclass: "image_label1_c",
				imagelabeldata: data.string.p3_s13
			}]
		}]
	},{
		// slide3
		contentblockadditionalclass: "background_lightblue",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition background_lightblue",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s14
		}],
		imageblockadditionalclass: "imageblock_addition2",
		imageblock:[{
			imagestoshow:[{
				imgclass: "image_bg",
				imgsrc: "",
				imgid: "temple_bg"
			}]
		}],
		containsdialog:[{
			dialogcontainer: "dialog2",
			dialogimageclass: "dialog_image",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content1",
			dialogcontent: data.string.p3_s15
		},{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image_flipY",
			dialogimagesrc: "",
			imgid: "bubblesc",
			dialogcontentclass: "dialog_content2",
			dialogcontent: data.string.p3_s16
		}]
	},{
		// slide4
		contentblockadditionalclass: "background_lightblue",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition background_lightblue",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s14
		}],
		imageblockadditionalclass: "imageblock_addition2",
		imageblock:[{
			imagestoshow:[{
				imgclass: "image_bg",
				imgsrc: "",
				imgid: "swimming_bg"
			}]
		}],
		containsdialog:[{
			dialogcontainer: "dialog2_b",
			dialogimageclass: "dialog_image_flipXY",
			dialogimagesrc: "",
			imgid: "bubblesc",
			dialogcontentclass: "dialog_content2",
			dialogcontent: data.string.p3_s18
		},{
			dialogcontainer: "dialog1_b",
			dialogimageclass: "dialog_image_flipX",
			dialogimagesrc: "dialog1_img",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content1",
			dialogcontent: data.string.p3_s17
		}]
	},{
		// slide5
		contentblockadditionalclass: "background_navyblue contentblock_addition",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s12
		}],
		imageblockadditionalclass: "imageblock_addition",
		imageblock:[{
			imagestoshow:[{
				imgclass: "mrs_sharma_class3",
				imgsrc: "",
				imgid: "mrs_sharma"
			}],
			imagelabels:[{
				imagelabelclass: "image_label1_d",
				imagelabeldata: data.string.p3_s19
			}]
		}]
	},{
		// slide6
		contentblockadditionalclass: "background_lightblue",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition background_lightblue",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s14
		}],
		imageblockadditionalclass: "imageblock_addition2",
		imageblock:[{
			imagestoshow:[{
				imgclass: "image_bg",
				imgsrc: "",
				imgid: "restaurant"
			}]
		}],
		containsdialog:[{
			dialogcontainer: "dialog2_c",
			dialogimageclass: "dialog_image",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content1",
			dialogcontent: data.string.p3_s21
		},{
			dialogcontainer: "dialog1_c",
			dialogimageclass: "dialog_image_flipX",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content1",
			dialogcontent: data.string.p3_s20
		}]
	},{
		// slide7
		contentblockadditionalclass: "background_lightblue",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition background_lightblue",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s14
		}],
		imageblockadditionalclass: "imageblock_addition2",
		imageblock:[{
			imagestoshow:[{
				imgclass: "image_bg",
				imgsrc: "",
				imgid: "elephant_bg"
			}]
		}],
		containsdialog:[{
			dialogcontainer: "dialog2_d",
			dialogimageclass: "dialog_image_flipXY",
			dialogimagesrc: "",
			imgid: "bubblesc",
			dialogcontentclass: "dialog_content2",
			dialogcontent: data.string.p3_s22
		},{
			dialogcontainer: "dialog1_d",
			dialogimageclass: "dialog_image_flipX",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content1",
			dialogcontent: data.string.p3_s23
		}]
	},{
		// slide8
		contentblockadditionalclass: "background_lightblue",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition background_lightblue",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p3_s14
		}],
		imageblockadditionalclass: "imageblock_addition2",
		imageblock:[{
			imagestoshow:[{
				imgclass: "image_bg",
				imgsrc: "",
				imgid: "school_bg"
			}]
		}],
		containsdialog:[{
			dialogcontainer: "dialog2_e",
			dialogimageclass: "dialog_image_flipXY",
			dialogimagesrc: "",
			imgid: "bubblesc",
			dialogcontentclass: "dialog_content2",
			dialogcontent: data.string.p3_s25
		},{
			dialogcontainer: "dialog1_e",
			dialogimageclass: "dialog_image_flipX",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content1",
			dialogcontent: data.string.p3_s24
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			{id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2ans1", src: soundAsset+"s3_p2_answer1.ogg"},
			{id: "sound_2ans2", src: soundAsset+"s3_p2_answer2.ogg"},
			{id: "sound_2ans3", src: soundAsset+"s3_p2_answer3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_4_1", src: soundAsset+"s3_p4_1.ogg"},
			{id: "sound_4_2", src: soundAsset+"s3_p4_2.ogg"},
			{id: "sound_5_1", src: soundAsset+"s3_p5_1.ogg"},
			{id: "sound_5_2", src: soundAsset+"s3_p5_2.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_7_1", src: soundAsset+"s3_p7_1.ogg"},
			{id: "sound_7_2", src: soundAsset+"s3_p7_2.ogg"},
			{id: "sound_8_1", src: soundAsset+"s3_p8_1.ogg"},
			{id: "sound_8_2", src: soundAsset+"s3_p8_2.ogg"},
			{id: "sound_9_1", src: soundAsset+"s3_p9_1.ogg"},
			{id: "sound_9_2", src: soundAsset+"s3_p9_2.ogg"},
			//textboxes
			{id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				if(countNext == $total_page - 1)
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 2500);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var timeoutcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
			put_image(content, countNext);

			$nextBtn.hide(0);
			if(countNext==1) sound_player4('sound_2','sound_2ans1','sound_2ans2','sound_2ans3');
			switch (countNext) {
				case 3:
				case 4:
				case 6:
				case 7:
				case 8:
				createjs.Sound.stop();
				current_sound1 = createjs.Sound.play('sound_'+(countNext+1)+'_1');
				current_sound1.play();
				current_sound1.on("complete", function(){
					current_sound2 = createjs.Sound.play('sound_'+(countNext+1)+'_2');
					current_sound2.play();
					current_sound2.on("complete", function(){
						nav_button_controls(100);
					});
				});
				break;
				default:
					if(countNext!=1) sound_player('sound_'+(countNext+1));
					break;

			}
		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/

function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}

function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	current_sound.on('complete', ()=>{
		nav_button_controls(300);
	});
}

function sound_player4(sound_id, sound_id2, sound_id3, sound_id4){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	current_sound.on('complete', ()=>{
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id2);
		current_sound.play();
		current_sound.on('complete', ()=>{
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id3);
			current_sound.play();
			current_sound.on('complete', ()=>{
				createjs.Sound.stop();
				current_sound = createjs.Sound.play(sound_id4);
				current_sound.play();
				current_sound.on('complete', ()=>{
					nav_button_controls(300);
					});
				});
			});
		});
}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}

		if(content[count].hasOwnProperty('containsdialog')){
				var containsdialog = content[count].containsdialog;
				for(var i=0; i<containsdialog.length; i++){
					var image_src = preload.getResult(containsdialog[i].imgid).src;
					//get list of classes
					var classes_list = containsdialog[i].dialogimageclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/airport_checking/";
var imgpath2 = $ref+"/images/";
var soundAsset = $ref+"/sound/";

var content=[
	{
		// slide0
		contentblockadditionalclass: "background_maroon contentblock_addition",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_addition",
		uppertextblock: [{
			textclass: "description_title",
			textdata: data.string.p5_s0
		}],
		imageblockadditionalclass: "imageblock_addition",
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "image_label1_b",
				imagelabeldata: data.string.p5_s1
			}]
		}]
	},{
		// slide1
		contentblockadditionalclass: "scale_focus1",
		contentnocenteradjust: true,
		imageblock:[{
			 imagestoshow:[{
				imgclass: "image_bg1",
				imgsrc: "",
				imgid: "bg_back"
			},{
				imgclass: "image_bg2",
				imgsrc: "",
				imgid: "machine_back"
			},{
				imgclass: "image_bg3",
				imgsrc: "",
				imgid: "machine_front"
			},{
				imgclass: "image_bg4",
				imgsrc: "",
				imgid: "machine_back01"
			},{
				imgclass: "ranjan1",
				imgsrc: "",
				imgid: "rajan01"
			},{
				imgclass: "ranjan2",
				imgsrc: "",
				imgid: "rajan02"
			},{
				imgclass: "deepak1",
				imgsrc: "",
				imgid: "deepak01"
			},{
				imgclass: "deepak2",
				imgsrc: "",
				imgid: "deepak02"
			},{
				imgclass: "kiran1",
				imgsrc: "",
				imgid: "kiran01"
			},{
				imgclass: "kiran2",
				imgsrc: "",
				imgid: "kiran02"
			},{
				imgclass: "tray1",
				imgsrc: "",
				imgid: "tray_empty"
			},{
				imgclass: "tray2 tray2_conveyerbelt_end",
				imgsrc: "",
				imgid: "tray_with_bag"
			},{
				imgclass: "dolma01_class",
				imgsrc: "",
				imgid: "dolma01"
			},{
				imgclass: "dolma03_class",
				imgsrc: "",
				imgid: "dolma03"
			}]
		}],
		lowertextblock:[{
			textclass: "dolma_walking_class dolma_walking_class2",
			textdata: "&nbsp;"
		}],
		containsdialog:[{
			dialogcontainer: "dialog2_b",
			dialogimageclass: "dialog_image",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content1",
			dialogcontent: data.string.p5_s2
		},{
			dialogcontainer: "dialog1_b",
			dialogimageclass: "dialog_image_flip",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content1",
			dialogcontent: data.string.p5_s3
		},{
			dialogcontainer: "dialog2_c",
			dialogimageclass: "dialog_image",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content",
			dialogcontent: data.string.p5_s4
		},{
			dialogcontainer: "dialog1_c",
			dialogimageclass: "dialog_image_flip2",
			dialogimagesrc: "",
			imgid: "dialog2_img",
			dialogcontentclass: "dialog_content3",
			dialogcontent: data.string.p5_s5
		},{
			dialogcontainer: "dialog2_d",
			dialogimageclass: "dialog_image",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content",
			dialogcontent: data.string.p5_s6
		},{
			dialogcontainer: "dialog2_e",
			dialogimageclass: "dialog_image2",
			dialogimagesrc: "",
			imgid: "dialog2_img",
			dialogcontentclass: "dialog_content3",
			dialogcontent: data.string.p5_s7
		},{
			dialogcontainer: "dialog2_f",
			dialogimageclass: "dialog_image",
			dialogimagesrc: "",
			imgid: "dialog1_img",
			dialogcontentclass: "dialog_content",
			dialogcontent: data.string.p5_s8
		},{
			dialogcontainer: "dialog2_g",
			dialogimageclass: "dialog_image2",
			dialogimagesrc: "",
			imgid: "dialog2_img",
			dialogcontentclass: "dialog_content3",
			dialogcontent: data.string.p5_s9
		}]
	},{},{},{},{},{
		// slide6
		contentnocenteradjust: true,
		imageblock:[{
			 imagestoshow:[{
				imgclass: "image_bg1",
				imgsrc: "",
				imgid: "waiting_room"
			}],
			imagelabels:[{
				imagelabelclass: "narration_label",
				imagelabeldata: data.string.p5_s10
			}]
		}]
	},{
		// slide7
		contentnocenteradjust: true,
		imageblock:[{
			 imagestoshow:[{
				imgclass: "image_bg1",
				imgsrc: "",
				imgid: "about_to_move"
			}],
			imagelabels:[{
				imagelabelclass: "narration_label",
				imagelabeldata: data.string.p5_s11
			}]
		}]
	},{
		// slide8
		contentblockadditionalclass: "scale_focus4",
		contentnocenteradjust: true,
		imageblock:[{
			 imagestoshow:[{
				imgclass: "plane_fly_away",
				imgsrc: "",
				imgid: "plane"
			},{
				imgclass: "dolma_class",
				imgsrc: "",
				imgid: "dolma"
			},{
				imgclass: "cloud01_cls_a",
				imgsrc: "",
				imgid: "cloud02"
			},{
				imgclass: "cloud01_cls_b",
				imgsrc: "",
				imgid: "cloud02"
			},{
				imgclass: "cloud02_cls_a",
				imgsrc: "",
				imgid: "cloud02"
			},{
				imgclass: "cloud02_cls_b",
				imgsrc: "",
				imgid: "cloud02"
			}]
		}],
		// containsdialog:[{
		// 	dialogcontainer: "farewell_dialog",
		// 	dialogimageclass: "dialog_image_flip",
		// 	dialogimagesrc: "",
		// 	imgid: "dialog1_img",
		// 	dialogcontentclass: "content_farewell",
		// 	dialogcontent: data.string.p5_s12
		// }]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	 var aeroplanesound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg_back", src: imgpath+"bg_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deepak01", src: imgpath+"deepak01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deepak02", src: imgpath+"deepak02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma01", src: imgpath+"dolma01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma02", src: imgpath+"dolma02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma03", src: imgpath+"dolma03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma_walking", src: imgpath+"dolma_walking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma_walking_with_bag", src: imgpath+"dolma_walking_with_bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kiran01", src: imgpath+"kiran01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kiran02", src: imgpath+"kiran02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "machine_back", src: imgpath+"machine_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "machine_back01", src: imgpath+"machine_back01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "machine_front", src: imgpath+"machine_front.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rajan01", src: imgpath+"rajan01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rajan02", src: imgpath+"rajan02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tray_empty", src: imgpath+"tray_empty.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tray_with_bag", src: imgpath+"tray_with_bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "waiting_room", src: imgpath+"waiting_room.png", type: createjs.AbstractLoader.IMAGE},
			{id: "about_to_move", src: imgpath+"about_to_move.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plane", src: imgpath+"plane.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma", src: imgpath2+"dolma.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud01", src: imgpath+"cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud02", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "dialog1_img", src: imgpath2+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dialog2_img", src: imgpath2+'bubleee-24.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s5_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s5_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s5_p3.ogg"},
			{id: "sound_4_1", src: soundAsset+"s5_p4_1.ogg"},
			{id: "sound_4_2", src: soundAsset+"s5_p4_2.ogg"},
			{id: "sound_5_1", src: soundAsset+"s5_p5_1.ogg"},
			{id: "sound_5_2", src: soundAsset+"s5_p5_2.ogg"},
			{id: "sound_6_1", src: soundAsset+"s5_p6_1.ogg"},
			{id: "sound_6_2", src: soundAsset+"s5_p6_2.ogg"},
			{id: "sound_7", src: soundAsset+"s5_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s5_p8.ogg"},
			// {id: "sound_9", src: soundAsset+"s5_p9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// aeroplanesound = createjs.Sound.play("aeroplane_sound", {loop: -1, volume:0.32});
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				if(countNext == $total_page - 1)
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 10000);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
	var timeoutcontroller;
	var timeoutcontroller2;
	var timeoutcontroller3;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	     texthighlight($board);
	    vocabcontroller.findwords(countNext);
			put_image(content, countNext);
			$nextBtn.hide(0);

		switch(countNext) {
			case 0:
				sound_player('sound_'+(countNext+1));
				break;
			case 1:
				createjs.Sound.stop();
				$nextBtn.hide(0);
				timeoutcontroller = setTimeout(function(){
					$(".dialog2_b").show(0);
					sound_player_nav('sound_'+(countNext+1));
				}, 2100);

				break;
				case 6:
				sound_player_nav('sound_7');
				break;
			case 7:
				$('.dialog2_f,.dialog2_g').hide(0);
				sound_player_nav('sound_8');
				break;

			case 8:
				$nextBtn.hide(0);
				$(".farewell_dialog").delay(1000).show(0);
				$(".farewell_dialog").delay(5000).hide(0);
				nav_button_controls(15000);
				// $(".dolma_class").delay(13400).show(0);
				break;

			default:
				break;
		}
		// splitintofractions($(".fractionblock"));

  }

/*=====  End of Templates Block  ======*/

function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}

function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	nav_button_controls(3000);
}
function sound_player_nav(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	current_sound.on("complete", function(){
		nav_button_controls(100);
	});
}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}

		if(content[count].hasOwnProperty('containsdialog')){
				var containsdialog = content[count].containsdialog;
				for(var i=0; i<containsdialog.length; i++){
					var image_src = preload.getResult(containsdialog[i].imgid).src;
					//get list of classes
					var classes_list = containsdialog[i].dialogimageclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		countNext++;
		switch(countNext){
			case 2:
				$prevBtn.hide(0);
				$(".dolma01_class").hide(0);
				$(".dolma03_class").show(0);
				$(".dialog2_b").hide(0);
				$(".rajan2").hide(0);
				$(".rajan1").show(0);
				$(".dialog1_b").show(0);
				sound_player('sound_3');
				$(".scale_focus1").addClass("focus1");
				$nextBtn.hide(0).delay(1500).show(0);
				loadTimelineProgress($total_page,countNext+1);
				break;
			case 3:
				$nextBtn.hide(0);
				$(".dialog1_b").hide(0);
				$(".focus1").addClass("scale_focus2");
				$(".dolma03_class").hide(0);
				$(".tray1").hide(0);
				$(".tray2").show(0);
				$(".dolma_walking_class").show(0);
				timeoutcontroller = setTimeout(function(){
					$(".dialog2_c").show(0);
					$(".deepak02").hide(0);
					$(".deepak01").show(0);
					current_sound1 = createjs.Sound.play('sound_'+(countNext+1)+'_2');
					 current_sound1.play();

					$(".dolma_walking_class").addClass("dolma_walking_class4").removeClass("dolma_walking_class2");
					timeoutcontroller = setTimeout(function(){
						current_sound1.on("complete", function(){
							$(".dialog1_c").show(0);
						 current_sound2 = createjs.Sound.play('sound_'+(countNext+1)+'_1');
						 current_sound2.play();
						 current_sound2.on("complete", function(){
							 nav_button_controls(100);
						 });
						});
					}, 2000);
				}, 6000);
				loadTimelineProgress($total_page,countNext+1);
				break;
			case 4:
				$nextBtn.hide(0);
				$(".dialog2_c").hide(0);
				$(".dialog1_c").hide(0);
				$(".dolma_walking_class").addClass("dolma_walking_class3");
				$(".focus1").addClass("scale_focus3");
				$(".tray2").addClass("tray3_conveyerbelt_end");
				timeoutcontroller3 = setTimeout(function(){
					$(".dolma_walking_class4").css("z-index", "29");
				}, 3200);
				timeoutcontroller2 = setTimeout(function(){
					$(".tray1").addClass("tray1_new_pos").show(0);
				}, 4300);
				timeoutcontroller = setTimeout(function(){
					$(".dialog2_d").show(0);
					$(".tray2").hide(0);
					$(".kiran1").hide(0);
					$(".kiran2").show(0);
					current_sound1 = createjs.Sound.play('sound_'+(countNext+1)+'_1');
				  current_sound1.play();
					timeoutcontroller = setTimeout(function(){
						$(".dolma_walking_class4").css("background-position", "-900% 0%");
							$(".dialog2_e").show(0);
						 current_sound2 = createjs.Sound.play('sound_'+(countNext+1)+'_2');
						 current_sound2.play();
						 current_sound2.on("complete", function(){
							 nav_button_controls(100);
						 });
					}, 2000);
				}, 6000);
				loadTimelineProgress($total_page,countNext+1);
				break;
			case 5:
				$nextBtn.hide(0);
				$(".dialog2_d").hide(0);
				$(".dialog2_e").hide(0);
				$(".dialog2_f").show(0);
				current_sound1 = createjs.Sound.play('sound_'+(countNext+1)+'_1');
				current_sound1.play();
				timeoutcontroller = setTimeout(function(){
					current_sound1.on("complete", function(){
						$(".dialog2_g").show(0);
					 current_sound2 = createjs.Sound.play('sound_'+(countNext+1)+'_2');
					 current_sound2.play();
					 current_sound2.on("complete", function(){
						 nav_button_controls(100);
					 });
					});
					$(".dolma_walking_class").addClass("dolma_walking_class5").removeClass("dolma_walking_class3");
				}, 2000);
				loadTimelineProgress($total_page,countNext+1);
				break;
			case 6:
				$nextBtn.hide(0);
				$(".dialog2_f").hide(0);
				$(".dialog2_g").hide(0);
				$(".dolma_walking_class").css("background-image", preload.getResult("dolma_walking_with_bag").src);
				$(".dolma_walking_class").addClass("dolma_walking_class6");
				$(".kiran1").show(0);
				$(".kiran2").hide(0);
				timeoutcontroller = setTimeout(function(){
					sound_player_nav('sound_7');
					templateCaller();
				}, 4000);
				loadTimelineProgress($total_page,countNext+1);
				break;

			default:
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		clearTimeout(timeoutcontroller2);
		clearTimeout(timeoutcontroller3);
		countNext--;
		if(countNext> 1 && countNext <= 5){
			countNext = 1;
		}
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

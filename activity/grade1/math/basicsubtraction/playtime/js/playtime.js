var imgpath = $ref+"/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";
var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"ptime",
                textclass: "chapter centertext",
                textdata: data.string.playtime
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: "coverpageImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "rhinodancingdiv",
                    imgclass: " relativecls rhinoimgdiv",
                    imgid: "rhinodance",
                    imgsrc: "",
                },
                {
                    imgdiv: "squirrelistening",
                    imgclass: " relativecls squirrelisteningimg",
                    imgid: "squirrel",
                    imgsrc: "",
                }]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"deductdiv",
                textclass: "",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic',
                textdiv:"text1",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic1',
                textdiv:"text2",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3",
                textclass: "content centertext",
                textdata: data.string.pt3
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.two,
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.four
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "frogfirst",
                    imgclass: "relativecls img1",
                    imgid: "f1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "whiteline",
                    imgclass: "relativecls img2",
                    imgid: "whitelineImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogsecond hide1",
                    imgclass: "relativecls img3",
                    imgid: "f2Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogthird correct",
                    imgclass: "relativecls img4",
                    imgid: "f3Img",
                    imgsrc: "",
                },
                // {
                //     imgdiv: "fruit",
                //     imgclass: "relativecls img5",
                //     imgid: "orangeImg",
                //     imgsrc: "",
                // },
             ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"deductdiv",
                textclass: "",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic',
                textdiv:"text1",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic1',
                textdiv:"text2",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3",
                textclass: "content centertext",
                textdata: data.string.pt3
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.two,
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.four
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "frogfirst",
                    imgclass: "relativecls img1",
                    imgid: "f1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "whiteline",
                    imgclass: "relativecls img2",
                    imgid: "whitelineImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogsecond hide1",
                    imgclass: "relativecls img3",
                    imgid: "f2Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogthird correct",
                    imgclass: "relativecls img4",
                    imgid: "f3Img",
                    imgsrc: "",
                },
                // {
                //     imgdiv: "fruit",
                //     imgclass: "relativecls img5",
                //     imgid: "orangeImg",
                //     imgsrc: "",
                // },
             ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"deductdiv",
                textclass: "",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic',
                textdiv:"text1",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic1',
                textdiv:"text2",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3",
                textclass: "content centertext",
                textdata: data.string.pt3
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.two,
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.four
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "frogfirst",
                    imgclass: "relativecls img1",
                    imgid: "f1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "whiteline",
                    imgclass: "relativecls img2",
                    imgid: "whitelineImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogsecond hide1",
                    imgclass: "relativecls img3",
                    imgid: "f2Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogthird correct",
                    imgclass: "relativecls img4",
                    imgid: "f3Img",
                    imgsrc: "",
                },
                // {
                //     imgdiv: "fruit",
                //     imgclass: "relativecls img5",
                //     imgid: "orangeImg",
                //     imgsrc: "",
                // },
             ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"deductdiv",
                textclass: "",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic',
                textdiv:"text1",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic1',
                textdiv:"text2",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3",
                textclass: "content centertext",
                textdata: data.string.pt3
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.two,
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.four
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "frogfirst",
                    imgclass: "relativecls img1",
                    imgid: "f1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "whiteline",
                    imgclass: "relativecls img2",
                    imgid: "whitelineImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogsecond hide1",
                    imgclass: "relativecls img3",
                    imgid: "f2Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogthird correct",
                    imgclass: "relativecls img4",
                    imgid: "f3Img",
                    imgsrc: "",
                },
                // {
                //     imgdiv: "fruit",
                //     imgclass: "relativecls img5",
                //     imgid: "orangeImg",
                //     imgsrc: "",
                // },
             ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"deductdiv",
                textclass: "",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic',
                textdiv:"text1",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamic1',
                textdiv:"text2",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3",
                textclass: "content centertext",
                textdata: data.string.pt3
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.two,
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.four
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "frogfirst",
                    imgclass: "relativecls img1",
                    imgid: "f1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "whiteline",
                    imgclass: "relativecls img2",
                    imgid: "whitelineImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogsecond hide1",
                    imgclass: "relativecls img3",
                    imgid: "f2Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "frogthird correct",
                    imgclass: "relativecls img4",
                    imgid: "f3Img",
                    imgsrc: "",
                },
                // {
                //     imgdiv: "fruit",
                //     imgclass: "relativecls img5",
                //     imgid: "orangeImg",
                //     imgsrc: "",
                // },
             ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;

    var preload;
    var timeoutvar = null;
    var current_sound="";
    var sound="";
    var tempfirstarray = new Array();
    var tempsecondarray = new Array();
    var palindromarray = new Array();

    var endpageex =  new EndPageofExercise();
    var message = data.string.lastmsg;
    endpageex.init(3);
    loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
             {id: "coverpageImg", src:imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "f1Img", src: imgpath+"frog01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "f2Img", src: imgpath+"frog03.png", type: createjs.AbstractLoader.IMAGE},
             {id: "f3Img", src: imgpath+"sitting_frog.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "whitelineImg", src: imgpath+"white_line.png", type: createjs.AbstractLoader.IMAGE},
             {id: "redbox", src: imgpath+"box_red.png", type: createjs.AbstractLoader.IMAGE},
             {id: "orangebox", src: imgpath+"box_orange.png", type: createjs.AbstractLoader.IMAGE},
             {id: "greenbox", src: imgpath+"box_green.png", type: createjs.AbstractLoader.IMAGE},
             {id: "bluebox", src: imgpath+"box_blue.png", type: createjs.AbstractLoader.IMAGE},
             {id: "orangeImg", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
             {id: "marbleImg", src: imgpath+"marble.png", type: createjs.AbstractLoader.IMAGE},


             //sounds
              {id: "yayy", src: soundAsset+"yayy.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    $refreshBtn.click(function(){
        templateCaller();
    });
    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
firstPagePlayTime(countNext);
        texthighlight($board);
        put_image(content, countNext);
        switch (countNext){
            case 0:

                navigationcontroller();
                break;
            case 1:
                var firstval = generaterandomnum([2,3,4,5,6,7,8,9,10],[0]);
                tempfirstarray.push(firstval);
                var arraysec = [1,2,3,4,5,6,7,8,9,10];
                arraysec.splice(arraysec.indexOf(firstval)+1,arraysec.length);
                var secondval = generaterandomnum(arraysec,[firstval]);
                tempsecondarray.push(secondval);
                createquesans(firstval,secondval);

                break;
            case 2:
            case 3:
            case 4:
            case 5:
                var firstval = generaterandomnum([2,3,4,5,6,7,8,9,10],[0]);
                tempfirstarray.push(firstval);
                var arraysec = [1,2,3,4,5,6,7,8,9,10];
                arraysec.splice(arraysec.indexOf(firstval)+1,arraysec.length);
                var secondval = generaterandomnum(arraysec,[firstval]);
                tempsecondarray.push(secondval);
                if($.inArray(firstval+"-"+secondval,palindromarray)>-1)
                    generaltemplate();
                else {
                    createquesans(firstval,secondval);
                }
                break;
                break;
            case 6:
                endpageex.endpage(message);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play('yayy');
                current_sound.play();
                 break;
            default:
                navigationcontroller();
                break;
        }
       $('.audiodiv').click(function(){
			sound_player(sound,false);
			});

    }


    function sound_player(sound_id,navigate,imgCls,imgSrc) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():"";
            if(imgCls && imgSrc){
                $("."+imgCls).attr("src",imgSrc);
            }
        });
    }



    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('imghintblock')) {
            var contentCount=content[count].imghintblock[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }
    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                // lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });

    function checkans(){
        var animate=0;
      $(".commonbtn ").on("click",function () {
          if($(this).hasClass("firstans")||$(this).hasClass("secondans") ) {
              $(this).attr("disabled", "disabled");
              $(this).addClass("correctans");
              if ($(this).hasClass("firstans")){
                  $(".anssecond").text($(this).text());
                  animate++;
              }
              else {
                $(".ansthird").text($(this).text());
                animate++;

              }
              $(this).prepend("<div class='correctWrongDiv'><img class='correctWrongImg' src='images/right.png'/></div>")
              current_sound = createjs.Sound.play("sound_17");
          }
          else{
              $(this).addClass("wrongans").attr("disabled","disabled");
              $(this).prepend("<div class='correctWrongDiv'><img class='correctWrongImg' src='images/wrong.png'/></div>")
              current_sound = createjs.Sound.play("sound_16");
          }
          if(animate==2){
              $(".commonbtn").attr("disabled","disabled");
              $(".commonbtn").addClass("disablePointer");
              animateImage();
              navigationcontroller();
          }
      });
    }



    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                 $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1","option opt2"];
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }
    function generaterandomnum(array,excludenum){
        for(var i = 0; i<excludenum.length;i++) {
            array.splice(array.indexOf(excludenum[i]), 1);
        }
        var random = array[Math.floor(Math.random() * array.length)];
        console.log("random val"+countNext+" coumtnext "+random);
        return random;
    }

    function createquesans(firstval,secondval){

        palindromarray.push(firstval + "-" + secondval);
        console.log(palindromarray)
        var option1 = firstval - secondval;
        var option2 = generaterandomnum([1,2,3,4,5,6,7,8,9,10],[option1]);

        //assign value and animate
        $(".dynamic").text(firstval);
        $(".dynamic1").text(secondval);
        $(".text3 p").text(firstval + " - " + secondval+" = ?");
        $(".opt1 p").text(option1);
        $(".opt2 p").text(option2);
        $(".text3,.option").hide();
        $(".text1,.fruit").delay(1000).animate({"top":"2%"},1000);
        $(".text2").hide().delay(2200).fadeIn(100).delay(2200).animate({"top":"15%","opacity":"1"},1000);
        $(".text3,.option").delay(5200).fadeIn(1000);
        shufflehint();
        for(var i=0;i<firstval;i++){
            $(".containercls").append("<img class='orangebox"+i+"' src='"+preload.getResult('orangebox').src+"'>");
        }
        var heightval = Math.round(
            ((($('.containercls').height()/firstval)*secondval) /
                $('.coverboardfull').height() * 100)
        );
        $(".deductdiv").delay(3500).animate({"height":heightval+"%"},1000);

        var containerfirstheight = Math.round(
            ($('.containercls').height()/
                $('.coverboardfull').height() * 100)
        );
        $(".frogfirst").css({"bottom":containerfirstheight+"%"});
        $(".whiteline").css({"bottom":containerfirstheight+"%"});
        $(".frogfirst,.whiteline").delay(3500).fadeOut(1000);
        $(".frogsecond").hide().css({"bottom":containerfirstheight+"%"}).delay(4200).fadeIn(1000);
        $(".frogthird").hide();
        $(".option").click(function(){
            $(".containercls1 img").remove();
            $(".containercls1").css({"bottom":heightval+"%"});
            if($(this).find("p").text()==option1){
                play_correct_incorrect_sound(1);
                $(this).addClass(" correctans");
                $(".option").addClass("avoid-clicks");
                for(var i=0;i<option1;i++){
                    $(".containercls1").append("<img class='greenbox"+i+"' src='"+preload.getResult('greenbox').src+"'>");
                }
                var containersecondheight = Math.round(
                    ($('.containercls1').height()/
                        $('.coverboardfull').height() * 100)
                );
                $(".frogsecond").fadeOut(1000);
                $(".frogthird").delay(1000).css({"bottom":(containersecondheight+heightval)+"%"}).fadeIn(1000);
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>");

                navigationcontroller();
            }
            else {
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                $(this).addClass("avoid-clicks wrongans");
                for(var i=0;i<option2;i++){
                    $(".containercls1").append("<img class='redbox"+i+"' src='"+preload.getResult('redbox').src+"'>");
                }
            }
        });
    }
});

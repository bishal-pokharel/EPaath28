var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "datafont centertext",
                textdata: data.string.chapter
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'covermainImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content centertext fadeInEffect",
                textdata: data.string.p1text1
            },
            {
                textdiv:"one number",
                textclass: "content centertext",
                textdata: data.string.one
            },
            {
                textdiv:"two number",
                textclass: "content centertext ",
                textdata: data.string.two
            },
            {
                textdiv:"three number",
                textclass: "content centertext ",
                textdata: data.string.three
            },
            {
                textdiv:"four number",
                textclass: "content centertext",
                textdata: data.string.four
            },
            {
                textdiv:"five  number",
                textclass: "content centertext",
                textdata: data.string.five
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b1 bird",
                    imgclass: "relativecls img2",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b2 bird",
                    imgclass: "relativecls img3",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b3 bird",
                    imgclass: "relativecls img4",
                    imgid: 'birdImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "b4 bird",
                    imgclass: "relativecls img5",
                    imgid: 'birdImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "b5 bird",
                    imgclass: "relativecls img6",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "timeline",
                    imgclass: "relativecls img7",
                    imgid: 'timelineImg',
                    imgsrc: ""
                },

            ]
        }],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content centertext fadeInEffect",
                textdata: data.string.p1text2
            },
            {
                textdiv:"one number",
                textclass: "content centertext",
                textdata: data.string.one
            },
            {
                textdiv:"two number",
                textclass: "content centertext ",
                textdata: data.string.two
            },
            {
                textdiv:"three number",
                textclass: "content centertext ",
                textdata: data.string.three
            },
            {
                textdiv:"four number",
                textclass: "content centertext",
                textdata: data.string.four
            },
            {
                textdiv:"five  number",
                textclass: "content centertext",
                textdata: data.string.five
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b1 bird",
                    imgclass: "relativecls img2",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b2 bird",
                    imgclass: "relativecls img3",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b3 bird",
                    imgclass: "relativecls img4",
                    imgid: 'birdImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "b4 bird flybird",
                    imgclass: "relativecls img5",
                    imgid: 'birdImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "b5 bird flybird",
                    imgclass: "relativecls img6",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "timeline",
                    imgclass: "relativecls img7",
                    imgid: 'timelineImg',
                    imgsrc: ""
                },

            ]
        }],
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content centertext fadeInEffect",
                textdata: data.string.p1text3
            },
            {
                textdiv:"one number",
                textclass: "content centertext",
                textdata: data.string.one
            },
            {
                textdiv:"two number",
                textclass: "content centertext ",
                textdata: data.string.two
            },
            {
                textdiv:"three number",
                textclass: "content centertext ",
                textdata: data.string.three
            },
            {
                textdiv:"four number",
                textclass: "content centertext",
                textdata: data.string.four
            },
            {
                textdiv:"five  number",
                textclass: "content centertext",
                textdata: data.string.five
            },
            {
                textdiv:"answerblock",
                textclass: "content1 centertext",
                textdata: data.string.p1text4
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b1 bird",
                    imgclass: "relativecls img2",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b2 bird",
                    imgclass: "relativecls img3",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b3 bird",
                    imgclass: "relativecls img4",
                    imgid: 'birdImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "timeline",
                    imgclass: "relativecls img7",
                    imgid: 'timelineImg',
                    imgsrc: ""
                },

            ]
        }],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content centertext fadeInEffect",
                textdata: data.string.p1text3
            },
            {
                textdiv:"one number",
                textclass: "content centertext",
                textdata: data.string.one
            },
            {
                textdiv:"two number",
                textclass: "content centertext ",
                textdata: data.string.two
            },
            {
                textdiv:"three number",
                textclass: "content centertext ",
                textdata: data.string.three
            },
            {
                textdiv:"four number",
                textclass: "content centertext",
                textdata: data.string.four
            },
            {
                textdiv:"five  number",
                textclass: "content centertext",
                textdata: data.string.five
            },
            {
                textdiv:"answerblock",
                textclass: "content1 centertext",
                textdata: data.string.p1text5
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b1 bird",
                    imgclass: "relativecls img2",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b2 bird",
                    imgclass: "relativecls img3",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b3 bird",
                    imgclass: "relativecls img4",
                    imgid: 'birdImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "timeline",
                    imgclass: "relativecls img7",
                    imgid: 'timelineImg',
                    imgsrc: ""
                },

            ]
        }],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [

            {
                textdiv:"text1 hide0",
                textclass: "content centertext",
                textdata: data.string.p1text7
            },
            {
                textdiv:"text1 hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text6
            },
            {
                textdiv:"text2 hide2",
                textclass: "content1 centertext",
                textdata: data.string.p1text8
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b1 bird",
                    imgclass: "relativecls img2",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b2 bird",
                    imgclass: "relativecls img3",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "b3 bird",
                    imgclass: "relativecls img4",
                    imgid: 'birdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow hide1",
                    imgclass: "relativecls img5",
                    imgid: "arrowImg"
                },

            ]
        }],
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [

            {
                textdiv:"text3",
                textclass: "content centertext",
                textdata: data.string.p1text9
            },

        ],
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [

            {
                textdiv:"dial1",
                textclass: "content1 centertext",
                textdata: data.string.p1text10
            },
            {
                textdiv:"text4 hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text11
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img1",
                    imgid: 'dialboxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "prem",
                    imgclass: "relativecls img2",
                    imgid: 'premImg',
                    imgsrc: ""
                },

            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            }
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [

            {
                textdiv:"text5",
                textclass: "content1 centertext",
                textdata: data.string.p1text12
            },
            {
                textdiv:"text4 hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text13
            },

        ],
        containerblock:[
            {
                divclass:"containercls1",
            }
        ]
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "covermainImg", src: imgpath + "cover_page.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coverpageImg", src: imgpath + "bg01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "birdImg", src: imgpath + "bird_pink.png", type: createjs.AbstractLoader.IMAGE},
            {id: "birdflyImg", src: imgpath + "bird_fly.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "timelineImg", src: imgpath + "timeline.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "premImg", src: imgpath + "prem.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg", src: imgpath + "text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "appleImg", src: imgpath + "apple.png", type: createjs.AbstractLoader.IMAGE},
            {id: "catImg", src: imgpath + "cat_01.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_1", src: soundAsset + "S1_P1.ogg"},
            {id: "sound_2", src: soundAsset + "S1_P2.ogg"},
            {id: "s1", src: soundAsset + "1.ogg"},
            {id: "s2", src: soundAsset + "2.ogg"},
            {id: "s3", src: soundAsset + "3.ogg"},
            {id: "sound_3", src: soundAsset + "S1_P3.ogg"},
            {id: "sound_4", src: soundAsset + "S1_P4_1.ogg"},
            {id: "sound_4_1", src: soundAsset + "S1_P4_2.ogg"},
            {id: "sound_5", src: soundAsset + "S1_P5.ogg"},
            {id: "sound_6", src: soundAsset + "S1_P6_2.ogg"},
            {id: "sound_7", src: soundAsset + "S1_P7.ogg"},
            {id: "sound_8", src: soundAsset + "S1_P8.ogg"},
            {id: "sound_8_1", src: soundAsset + "S1_P8_1.ogg"},
            {id: "sound_9", src: soundAsset + "S1_P9_1.ogg"},
            {id: "sound_9_1", src: soundAsset + "S1_P9_2.ogg"}
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                sound_player("sound_2",true);
                break;
            case 2:
                sound_player("sound_3",true);
                setTimeout(function(){
                    $(".b4 img").attr("src",preload.getResult("birdflyImg").src);
                    $(".b5 img").attr("src",preload.getResult("birdflyImg").src);
                },2000);
                break;
            case  3:
                $(".answerblock").hide();
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_4");
                current_sound.play();
                current_sound.on('complete', function () {
                    setTime = setTimeout(function(){
                        sound_player('s1',false);
                        $(".b1").addClass("blinkEffect");
                        setTimeout(function(){
                            sound_player('s2',false);
                            $(".b1").removeClass("blinkEffect");
                            $(".b2").addClass("blinkEffect");
                            setTimeout(function(){
                                sound_player('s3',false);
                                $(".b2").removeClass("blinkEffect");
                                $(".b3").addClass("blinkEffect");
                                setTimeout(function(){
                                    $(".b3").removeClass("blinkEffect");
                                    sound_player("sound_4_1",true);
                                    $(".answerblock").fadeIn(1000);
                                },2000);
                            },2000);
                        },2000);
                    },3000);
                });
                
                // $(".answerblock").hide().delay(4000).fadeIn(1000);
                break;
            case 4:
                clearTimeout(setTime);
                sound_player("sound_5",true);
                break;
            case 5:
                $(".hide1,.hide2").hide();
                $(".hide0").delay(2000).fadeOut(1000);
                $(".hide1,.hide2").delay(2500).fadeIn(1000);
                setTime = setTimeout(function(){
                    sound_player("sound_6",true);
                },3000);
                break;
            case 6:
                sound_player("sound_7",true);
                break;
            case 7:
                sound_player("sound_8",false);
                $(".hide1").hide();
                for(var i=0;i<8;i++){
                    $(".containercls").append("<img class='appimg"+i+"' src='"+preload.getResult('appleImg').src+"'>");
                    $(".containercls").append("<p class='content2 pcontent"+i+"'>"+(i+1)+"</p>");
                }
                 setTime = setTimeout(function(){
                    for(var i=7;i>4;i--){
                        $(".appimg"+i).fadeOut(100);
                        $(".pcontent"+i).fadeOut(100);
                    }
                    $(".hide1").delay(4000).fadeIn(1000);
                     setTimeout(function(){
                         sound_player("sound_8_1",true);
                     },5000);
                },3000);
                break;
            case 8:
                clearTimeout(setTime);
                sound_player("sound_9",false);
                $(".hide1").hide();
                for(var i=0;i<12;i++){
                    $(".containercls1").append("<img class='catimg"+i+"' src='"+preload.getResult('catImg').src+"'>");
                    $(".containercls1").append("<p class='content3 pcontent"+i+"'>"+(i+1)+"</p>");
                }
                setTime = setTimeout(function(){
                    for(var i=11;i>7;i--){
                        $(".catimg"+i).css("opacity","0");
                        $(".pcontent"+i).css("opacity","0");
                    }
                    $(".hide1").delay(4000).fadeIn(1000);
                    setTimeout(function(){
                        sound_player("sound_9_1",true);
                    },6000);
                },4500);
                break;
            default:
                clearTimeout(setTime);
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page,countNext==8?true:false) : "";
        });
    }

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

});

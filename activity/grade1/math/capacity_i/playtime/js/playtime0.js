var imgpath = $ref + "/images/playtime/";
var imgpath2 = $ref + "/images/playtime/forhover/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";
var soundAsset_playtime = $ref+"/sounds/"+ $lang + "/playtime/";
var content=[
	// slide 1
	{
			contentnocenteradjust: true,
			uppertextblock: [
					{
							textclass: "playtime",
							textdata: data.string.playtime
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							},
							{
									imgclass: "bg_full",
									imgid: "mainbg",
									imgsrc: "",
							}]
			}]
	},
	// slide 2
	{
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.guideline
		}],
		imageblock:[
			{
				imagestoshow:[{
						imgclass: "background",
						imgid: "kitchen",
						imgsrc: "",
				},{
						imgclass: "hand",
						imgid: "hand",
						imgsrc: "",
				}]
				},{
					imageblockclass:"group1 hg_lht_anim",
					imagestoshow:[{
						imgclass: "groupImg img1 pot1",
						imgid: "tea_pot",
						imgsrc: "",
				},{
						imgclass: "groupImg img2 cup1",
						imgid: "cup_without_water",
						imgsrc: "",
				}]
			},
			{
				imageblockclass:"group2",
				imagestoshow:[
					{
						imgclass: "groupImg img1 bucket1",
						imgid: "bucket",
						imgsrc: "",
					},
					{
						imgclass: "groupImg img2 jug1",
						imgid: "jug",
						imgsrc: "",
					}
			]
			},
			{
				imageblockclass:"group3",
				imagestoshow:[{
						imgclass: "groupImg img1 glass1",
						imgid: "glass",
						imgsrc: "",
				},{
						imgclass: "groupImg img2 bowl1",
						imgid: "bowl",
						imgsrc: "",
				}]
			}
		]
	},
	// slide 3--anim
	{
		clickblock:[{
			clickclass:"utensil cupClick"
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.question1,
			imgclass: 'flipped-h',
			textclass : 'text_inside',
			imgid : 'sp-dialogue1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow: [
				{
					imgclass: "correct",
					imgid: "correct",
					imgsrc: "",
				},
				{
					imgclass: "incorrect",
					imgid: "incorrect",
					imgsrc: "",
				},
				{
					imgclass: "bg_full",
					imgid: "kitchen",
					imgsrc: "",
				},
				{
					imgclass: "utensil cupGif",
					imgid: "cup_filling",
					imgsrc: "",
				},
				{
					imgclass: "cup cf",
					imgid: "teacup_front",
					imgsrc: "",
				},
				{
					imgclass: "cup cb",
					imgid: "teacup_back",
					imgsrc: "",
				},{
					imgclass: "utensil pot",
					imgid: "tea_pot",
					imgsrc: "",
				},
				{
					imgclass: "utensil pot pt",
					imgid: "tea_pot_front",
					imgsrc: "",
				},{
					imgclass: "utensil gf",
					imgid: "waterdrop_from_teapot",
					imgsrc: "",
				},{
					imgclass: "overflow",
					imgid: "overflow",
					imgsrc: "",
				},{
					imgclass: "utensil wtrDrpFrmCup",
					imgid: "waterdrop_from_teacup",
					imgsrc: "",
				},{
					imgclass: "utensil pot ptfull",
					imgid: "tea_pot_02",
					imgsrc: "",
				}
			]
		}]
	},
	// slide 4
	{
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.guideline
		}],
		imageblock:[
			{
				imagestoshow:[{
						imgclass: "background",
						imgid: "kitchen",
						imgsrc: "",
				},{
						imgclass: "hand s4_hand",
						imgid: "hand",
						imgsrc: "",
				}]
			},{
				imageblockclass:"group1",
				imagestoshow:[{
						imgclass: "groupImg img1 pot1",
						imgid: "tea_pot",
						imgsrc: "",
				},{
						imgclass: "groupImg img2 cup1",
						imgid: "cup_without_water",
						imgsrc: "",
				}]
			},
			{
				imageblockclass:"group2 hg_lht_anim",
				imagestoshow:[
					{
						imgclass: "groupImg img1 bucket1",
						imgid: "bucket",
						imgsrc: "",
					},
					{
						imgclass: "groupImg img2 jug1",
						imgid: "jug",
						imgsrc: "",
					}
			]
			},
			{
				imageblockclass:"group3",
				imagestoshow:[{
						imgclass: "groupImg img1 glass1",
						imgid: "glass",
						imgsrc: "",
				},{
						imgclass: "groupImg img2 bowl1",
						imgid: "bowl",
						imgsrc: "",
				}]
			}
		]
	},
	// slide 5--anim
	{
		contentnocenteradjust: true,
			clickblock:[{
				clickclass:"utensil jugClick"
			}],
		imageblock:[{
			imagestoshow: [
				{
					imgclass: "correct",
					imgid: "correct",
					imgsrc: "",
				},
				{
					imgclass: "incorrect",
					imgid: "incorrect",
					imgsrc: "",
				},
				{
					imgclass: "bg_full",
					imgid: "kitchen",
					imgsrc: "",
				},
				{
					imgclass: "jug jf",
					imgid: "jug_front",
					imgsrc: "",
				},
				{
					imgclass: "jug jb",
					imgid: "jug_back",
					imgsrc: "",
				},{
					imgclass: "utensil bucket",
					imgid: "bucket",
					imgsrc: "",
				},{
					imgclass: "wdFrmBkt",
					imgid: "waterdrop_from_bucket",
					imgsrc: "",
				},{
					imgclass: "utensil jug jfilGif",
					imgid: "jug_filling_overflow",
					imgsrc: "",
				},{
					imgclass: "wdFrmJug",
					imgid: "waterdrop_from_jug",
					imgsrc: "",
				},{
					imgclass: "utensil bktflGif",
					imgid: "bucket_fill",
					imgsrc: "",
				},{
					imgclass: "ovrFlwJug",
					imgid: "overflow_jug",
					imgsrc: "",
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.question1,
			imgclass: 'flipped-h',
			textclass : 'text_inside',
			imgid : 'sp-dialogue1',
			imgsrc: '',
		}],
	},
	// slide 6
	{
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.guideline
		}],
		imageblock:[
			{
				imagestoshow:[{
						imgclass: "background",
						imgid: "kitchen",
						imgsrc: "",
				},{
						imgclass: "hand s6_hand",
						imgid: "hand",
						imgsrc: "",
				}]
			},{
				imageblockclass:"group1",
				imagestoshow:[{
						imgclass: "groupImg img1 pot1",
						imgid: "tea_pot",
						imgsrc: "",
				},{
						imgclass: "groupImg img2 cup1",
						imgid: "cup_without_water",
						imgsrc: "",
				}]
			},
			{
				imageblockclass:"group2",
				imagestoshow:[
					{
						imgclass: "groupImg img1 bucket1",
						imgid: "bucket",
						imgsrc: "",
					},
					{
						imgclass: "groupImg img2 jug1",
						imgid: "jug",
						imgsrc: "",
					}
			]
			},
			{
				imageblockclass:"group3 hg_lht_anim",
				imagestoshow:[{
						imgclass: "groupImg img1 glass1",
						imgid: "glass",
						imgsrc: "",
				},{
						imgclass: "groupImg img2 bowl1",
						imgid: "bowl",
						imgsrc: "",
				}]
			}
		]
	},
	// slide 7--anim
	{
		contentnocenteradjust: true,
			clickblock:[{
				clickclass:"utensil bowlClick"
			}],
		imageblock:[{
			imagestoshow: [
				{
					imgclass: "correct",
					imgid: "correct",
					imgsrc: "",
				},
				{
					imgclass: "incorrect",
					imgid: "incorrect",
					imgsrc: "",
				},
				{
					imgclass: "bg_full",
					imgid: "kitchen",
					imgsrc: "",
				},
				{
					imgclass: "bowl bf",
					imgid: "bowl_front",
					imgsrc: "",
				},
				{
					imgclass: "bowl bb",
					imgid: "bowl_back",
					imgsrc: "",
				},{
					imgclass: "utensil glass",
					imgid: "glass",
					imgsrc: "",
				},{
					imgclass: "wdFrmGls",
					imgid: "waterdrop_from_glass",
					imgsrc: "",
				},{
					imgclass: "bwlFil",
					imgid: "bowl_filling",
					imgsrc: "",
				},{
					imgclass: "wdFrmBwl",
					imgid: "waterdrop_from_bowl",
					imgsrc: "",
				},{
					imgclass: "utensil glsFil",
					imgid: "glass_filling",
					imgsrc: "",
				},{
					imgclass: "uovrFlwBwl",
					imgid: "overflow_bwl",
					imgsrc: "",
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.question1,
			imgclass: 'flipped-h',
			textclass : 'text_inside',
			imgid : 'sp-dialogue1',
			imgsrc: '',
		}],
	},
	// slide 8
	{
		contentblockadditionalclass:'green',
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "background",
				imgid: "kitchen",
				imgsrc: "",
			},{
				imgclass: "lstPgImg lim-1",
				imgid: "bucket_fill_png",
				imgsrc: "",
			},{
				imgclass: "lstPgImg lim-2",
				imgid: "jug_fill",
				imgsrc: "",
			},{
				imgclass: "lstPgImg lim-3",
				imgid: "glass_fill",
				imgsrc: "",
			},{
				imgclass: "lstPgImg lim-4",
				imgid: "cup_full",
				imgsrc: "",
			}]
		}],
		uppertextblock:[{
			textclass:'playagain',
			textdata:data.string.playagain
		},{
			textclass:'mainmenu',
			textdata:data.string.mainmenu
		},{
			textclass:'learnagain',
			textdata:data.string.learnagain
		},{
			textclass:'lastMsg',
			textdata:data.string.lstmsg
		}]
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var selPgeArr=[];

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src:imgpath +"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mainbg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kitchen", src: imgpath+"kitchen01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug", src: imgpath+"jug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glass", src: imgpath+"glass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bucket", src: imgpath+"bucket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bowl", src: imgpath+"bowl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tea_pot", src: imgpath+"tea_pot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cup_without_water", src: imgpath+"cup_without_water.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			// cup and tea_pot
			{id: "tea_pot", src: imgpath+"tea_pot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tea_pot_front", src: imgpath+"tea_pot_front.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cup_filling", src: imgpath+"cup_filling.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "teacup_front", src: imgpath+"teacup_front.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teacup_back", src: imgpath+"teacup_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "waterdrop_from_teapot", src: imgpath+"waterdrop_from_teapot.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "waterdrop_from_teacup", src: imgpath+"waterdrop_from_teacup.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "overflow", src: imgpath+"overflow_teacup.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "tea_pot_02", src: imgpath+"tea_pot_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"speechbuuble1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pot_full", src: imgpath+"tea_pot_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cup_full", src: imgpath+"cup_with_water_new.png", type: createjs.AbstractLoader.IMAGE},
			// bucket and jug

			{id: "kitchen", src: imgpath+"kitchen01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bucket", src: imgpath+"bucket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug_filling_overflow", src: imgpath+"jug_filling_overflow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "jug_front", src: imgpath+"jug_front.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug_back", src: imgpath+"jug_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "waterdrop_from_bucket", src: imgpath+"waterdrop_from_bucket.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "waterdrop_from_jug", src: imgpath+"waterdrop_from_jug.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bucket_fill", src: imgpath+"bucket_fill.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "overflow_jug", src: imgpath+"overflow_from_jug.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "tea_pot_02", src: imgpath+"tea_pot_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug_fill", src: imgpath+"jug_fill.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bucket_fill_png", src: imgpath+"bucket_fill_sec.png", type: createjs.AbstractLoader.IMAGE},

			// glass and bowl

			{id: "glass", src: imgpath+"glass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bowl_filling", src: imgpath+"bowl_filling.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bowl_front", src: imgpath+"bowl_front.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bowl_back", src: imgpath+"bowl_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "waterdrop_from_glass", src: imgpath+"waterdrop_from_glass.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "waterdrop_from_bowl", src: imgpath+"waterdrop_from_bowl.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "glass_filling", src: imgpath+"glass_filling_half.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "overflow_bwl", src: imgpath+"overflow_from_glass.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "tea_pot_02", src: imgpath+"tea_pot_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glass_fill", src: imgpath+"glass_fill_water.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bowl_fill", src: imgpath+"bowl_water.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			//corect_incorrect_icons
			{id: "incorrect", src: 'images/correct_incorrect_icons/incorrect.png', type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: 'images/correct_incorrect_icons/correct.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "playtime", src: soundAsset+"playtime.ogg"},
			{id: "sound_2", src: soundAsset_playtime+"s1_p2.ogg"},
			{id: "sound_3", src: soundAsset_playtime+"s1_p3.ogg"},
			{id: "sound_4", src: soundAsset_playtime+"Yes_the_kettle_has.ogg"},
			{id: "sound_5", src: soundAsset_playtime+"No_the_cup_has.ogg"},
			{id: "sound_6", src: soundAsset_playtime+"Yes_the_bucket_has.ogg"},
			{id: "sound_7", src: soundAsset_playtime+"No_the_jug_has.ogg"},
			{id: "sound_8", src: soundAsset_playtime+"Yes_the_glass_has.ogg"},
			{id: "sound_9", src: soundAsset_playtime+"No_the_bowl_has.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
 // 	var pagenumbers  = new pagenameholder();
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var pagenumbers;

		$('.correct,.incorrect').hide(0);
		$('.playagain').click(function(){
			$('#activity-page-exercise-tab').find('button').trigger('click');
		});
		$('.learnagain').click(function(){
			$("#activity-page-lesson-tab").find('button').trigger('click');

		});
		$('.mainmenu').click(function(){
			$("#activity-page-menu-img").trigger("click");

		});
		$(".cupGif, .wtrDrpFrmCup, .ptfull, .gf, .overflow").hide(0);
		$(".wdFrmBkt, .jfilGif, .wdFrmJug, .bktflGif, .ovrFlwJug").hide(0);
		$(".wdFrmGls, .bwlFil, .glsFil, .wdFrmBwl, .uovrFlwBwl").hide(0);
		switch (countNext) {
			case 0:
					nav_button_controls(100);
			break;
			case 1:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_2");
				current_sound.play();
				current_sound.on('complete',function(){
					$(".group1").addClass("hoverClass");
					$('.group1, .hand').click(function(){
						createjs.Sound.stop();
						clearTimeout(timeoutvar);
						countNext++;
						templateCaller();
					});
				});
			break;
			case 2:
					sound_player("sound_3");
					$(".utensil, .hand, .cupClick").click(function(){
						if($(this).hasClass("pot")){
							$(".utensil").css("pointer-events","none");
							$(".pot").attr("src",preload.getResult("pot_full").src);
							setTimeout(function(){
							$(".pot").addClass("potToCupAnim");
								setTimeout(function(){
									$('.correct').show(100);
									sound_player("sound_4",1);
									$(".pot").hide(0);
									$(".gf").show(0);
									$(".cupGif").show(0);
									$(".cupGif").delay(100).animate({"z-index":"13"},100);
									$(".overflow").delay(1200).show(0);
									$(".text_inside").css("color","#13CE66");
									$(".text_inside").html(data.string.qn1_right);
									$(".of1").css("z-index","14").addClass("fade_in");
									// nav_button_controls(0);
								},3000);
							},200);
						}
						else{
							$(".utensil").css("pointer-events","none");
							$(".cup").attr('src',preload.getResult("cup_full").src);
							$(".cup").addClass("cupToPotAnim");
							setTimeout(function(){
								$('.incorrect').show(100);
								sound_player("sound_5",1);
								$(".cup").hide(0);
								$(".wtrDrpFrmCup").show(0);
								$(".wtrDrpFrmCup").delay(100).animate({"z-index":"22"});
								// $(".ptfull").delay(1000).show(0);
								$(".text_inside").css("color","#f00");
								$(".text_inside").html(data.string.qn1_wrong);
								// nav_button_controls(0);
							},3000);
						}
					});
			break;
			case 3:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_2");
				current_sound.play();
				current_sound.on('complete',function(){
					$(".group2").addClass("hoverClass");
					$('.group2, .hand').click(function(){
						createjs.Sound.stop();
						clearTimeout(timeoutvar);
						countNext++;
						templateCaller();
					});
				});
			break;
			case 4:
				sound_player("sound_3");
				$(".utensil, .jugClick").click(function(){
					if($(this).hasClass("bucket")){
						$(".utensil").css("pointer-events","none");
						$(".bucket").attr('src', preload.getResult("bucket_fill_png").src);
						$(this).addClass("bktToJugAnim");
						setTimeout(function(){
							$('.correct').show(100);
							sound_player("sound_6",1);
							$(".bucket").hide(0);
							$(".wdFrmBkt").show(0);
							$(".jfilGif").show(0);
							$(".jfilGif").delay(100).animate({"z-index":"15"},100);
							$(".ovrFlwJug").delay(1700).show(0);
							$(".text_inside").css("color","#13CE66");
							$(".text_inside").html(data.string.qn2_right);
							// nav_button_controls(0);
						},3000);
					}
					else{
						$(".utensil").css("pointer-events","none");
						$(".jug").attr('src',preload.getResult("jug_fill").src);
						setTimeout(function(){
							$(".jug").addClass("jugToBktAnim");
							setTimeout(function(){
								$('.incorrect').show(100);
								sound_player("sound_7",1);
								$(".wdFrmJug").show(0);
								$(".jug, .bucket").hide(0);
								$(".bktflGif").show(0);
								$(".bktflGif").delay(100).animate({"z-index":"15"},100);
								$(".text_inside").css("color","#f00");
								$(".text_inside").html(data.string.qn2_wrong);
								// nav_button_controls(0);
							},3000);
						},200);
					}
				});
			break;
			case 5:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_2");
				current_sound.play();
				current_sound.on('complete',function(){
					$(".group3").addClass("hoverClass");
					$('.group3, .hand').click(function(){
						createjs.Sound.stop();
						clearTimeout(timeoutvar);
						countNext++;
						templateCaller();
					});
				});
			break;
			case 6:
			sound_player("sound_3");
				$(".utensil, .bowlClick").click(function(){
					if($(this).hasClass("glass")){
						$(".utensil").css("pointer-events","none");
						$(".glass").attr('src', preload.getResult("glass_fill").src);
						$(this).addClass("glsToBwlAnim");
						setTimeout(function(){
							$('.correct').show(100);
							sound_player("sound_8",1);
							$(".glass").hide(0);
							$(".wdFrmGls").show(0);
							$(".bwlFil").show(0);
							$(".bwlFil").delay(100).animate({"z-index":"15"},100);
							$(".uovrFlwBwl").delay(1200).show(0);
							$(".text_inside").css("color","#13CE66");
							$(".text_inside").html(data.string.qn3_right);
							// nav_button_controls(0);
						},3000);
					}
					else{
						$(".utensil").css("pointer-events","none");
						$(".bowl").attr('src', preload.getResult("bowl_fill").src);
						$(".bowl").addClass("bwlToGlsAnim");
						setTimeout(function(){
							$('.incorrect').show(100);
							sound_player("sound_9",1);
						$(".bowl").hide(0);
						$(".wdFrmBwl").show(0);
						$(".glsFil").show(0);
						$(".glsFil").delay(100).animate({"z-index":"17"},100);
						$(".glass").hide(0);
							$(".text_inside").css("color","#f00");
							$(".text_inside").html(data.string.qn3_wrong);
							// nav_button_controls(0);
						},3000);
					}
				});
			break;
			case 7:
				highlightUtensils(1);
			break;
			default:
				nav_button_controls(100);

		}

	}


	function highlightUtensils(vslCount){
		$(".lim-"+vslCount).addClass("highlightAnm");
		setTimeout(function(){
			vslCount+=1;
			if(vslCount<=4){
				highlightUtensils(vslCount);
			}
		},2100);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			next?nav_button_controls(100):'';
		});
	}

	function put_image(content, count){
			if(content[count].hasOwnProperty('imageblock')){
				for(var j = 0; j < content[count].imageblock.length; j++){
					var imageblock = content[count].imageblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
		}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

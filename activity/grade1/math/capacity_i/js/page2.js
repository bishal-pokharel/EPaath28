var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg pot',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName",
			imagelabeldata:data.string.cup
		},{
			imagelabelclass:"imgName ptTxt",
			imagelabeldata:data.string.pot
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2s1txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 2
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg pot',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName",
			imagelabeldata:data.string.cup
		},{
			imagelabelclass:"imgName ptTxt",
			imagelabeldata:data.string.pot
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.cuptxt_1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 3
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg pot',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			},
			{
				imgclass:'tap cupTapS2',
				imgid:'tap',
			},
			{
				imgclass:'tap tapToCup',
				imgid:'tap_to_cup',
			},
			{
				imgclass:'hand hoverCls hand_s3',
				imgid:'hand',
			}
		],
		imagelabels:[{
				imagelabelclass:"imgName",
				imagelabeldata:data.string.cup
			},{
				imagelabelclass:"imgName ptTxt",
				imagelabeldata:data.string.pot
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.cuptxt_2,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 4
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup',
				imgid:'cup_with_water',
			},
			{
				imgclass:'rhsImg pot',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.cuptxt_3,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 5
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup',
				imgid:'cup_with_water',
			},
			{
				imgclass:'rhsImg pot',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			},
			{
				imgclass:'cup_to_teapot',
				imgid:'cup_to_teapot',
			},
			{
				imgclass:'hand hoverCls hand_s5',
				imgid:'hand',
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.cuptxt_4,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 6
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg pot',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.cuptxt_5,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 7
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg pot',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
				imagelabelclass:"imgName",
				imagelabeldata:data.string.cup
			},{
				imagelabelclass:"imgName ptTxt",
				imagelabeldata:data.string.pot
			},{
				imagelabelclass:"imgName lescap",
				imagelabeldata:data.string.lescap
			},{
				imagelabelclass:"imgName morecap",
				imagelabeldata:data.string.morecap
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.cuptxt_6,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 8
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cupSec',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg potSec',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName cupSecTxt",
			imagelabeldata:data.string.cup
		},{
			imagelabelclass:"imgName ptSecTxt",
			imagelabeldata:data.string.pot
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.pottxt_1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 9
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cupSec',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg potSec',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'tap potTap',
				imgid:'tap',
			},
			{
				imgclass:'tapToPot',
				imgid:'tap_to_pot',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			},
			{
				imgclass:'hand hoverCls hand_s9',
				imgid:'hand',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName cupSecTxt",
			imagelabeldata:data.string.cup
		},{
			imagelabelclass:"imgName ptSecTxt",
			imagelabeldata:data.string.pot
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.pottxt_2,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 10
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cupSec',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg potSec',
				imgid:'teapot_full',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName cupSecTxt",
			imagelabeldata:data.string.cup
		},{
			imagelabelclass:"imgName ptSecTxt",
			imagelabeldata:data.string.pot
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.pottxt_3,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 11
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cupSec rotatey_180deg',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg pots11',
				imgid:'teapot_lidless_sec',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			},
			{
				imgclass:'potToCup',
				imgid:'pot_to_cup',
			},
			{
				imgclass:'hand hoverCls hand_s11',
				imgid:'hand',
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.pottxt_4,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 12
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cupSec',
				imgid:'cup_with_water',
			},
			{
				imgclass:'rhsImg potSec',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.pottxt_5,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 13
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cupSec',
				imgid:'cup_with_water',
			},
			{
				imgclass:'rhsImg potSec',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
				imagelabelclass:"imgName cupSecTxt",
				imagelabeldata:data.string.cup
			},{
				imagelabelclass:"imgName ptSecTxt",
				imagelabeldata:data.string.pot
			},{
				imagelabelclass:"imgName lescap_sec",
				imagelabeldata:data.string.lescap
			},{
				imagelabelclass:"imgName morecap_sec",
				imagelabeldata:data.string.morecap
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.pottxt_6,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 14
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup cpImgs14',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg ptImgs14',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'rhsImg jug',
				imgid:'jug',
			},
			{
				imgclass:'rhsImg jar',
				imgid:'glass',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
				imagelabelclass:"imgName cptxt",
				imagelabeldata:data.string.cup
			},{
				imagelabelclass:"imgName potlasTxt",
				imagelabeldata:data.string.pot
			},
			{
				imagelabelclass:"imgName jugtxt",
				imagelabeldata:data.string.jug
			},{
				imagelabelclass:"imgName jartxt",
				imagelabeldata:data.string.glass
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2s3txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 15
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg cup cpImgs14',
				imgid:'cup_wo_water',
			},
			{
				imgclass:'rhsImg ptImgs14',
				imgid:'teapot_lidless',
			},
			{
				imgclass:'rhsImg jug',
				imgid:'jug',
			},
			{
				imgclass:'rhsImg jar',
				imgid:'glass',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
				imagelabelclass:"imgName cptxt",
				imagelabeldata:data.string.cup
			},{
				imagelabelclass:"imgName potlasTxt",
				imagelabeldata:data.string.pot
			},
			{
				imagelabelclass:"imgName jugtxt",
				imagelabeldata:data.string.jug
			},{
				imagelabelclass:"imgName jartxt",
				imagelabeldata:data.string.glass
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2s4txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
];



$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var cup = true;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"speechbuuble1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirell", src: imgpath+"squirrel01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teapot_sec", src: imgpath+"tea_pot_sec.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teapot_lidless", src: imgpath+"tea_pot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teapot_lidless_sec", src: imgpath+"tea_pot_02_sec.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teapot_full", src: imgpath+"tea_pot_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cup_wo_water", src: imgpath+"cup_without_water.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cup_with_water", src: imgpath+"cup_with_water.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tap", src: imgpath+"tap.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tap_to_cup", src: imgpath+"tap_cup_one_time.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cup_to_teapot", src: imgpath+"cup_tea_pot_one_time.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "tap_to_pot", src: imgpath+"tap_teapot_one_time.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pot_to_cup", src: imgpath+"tea_pot_cup_one_time.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "jug", src: imgpath+"jug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jar", src: imgpath+"jar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "glass", src: imgpath+"glass.png", type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "S2_P1", src: soundAsset+"S2_P1.ogg"},
			{id: "S2_P2", src: soundAsset+"S2_P2.ogg"},
			{id: "S2_P3", src: soundAsset+"S2_P3.ogg"},
			{id: "S2_P4", src: soundAsset+"S2_P4.ogg"},
			{id: "S2_P5", src: soundAsset+"S2_P5.ogg"},
			{id: "S2_P6", src: soundAsset+"S2_P6.ogg"},
			{id: "S2_P7", src: soundAsset+"S2_P7.ogg"},
			{id: "S2_P8", src: soundAsset+"S2_P8.ogg"},
			{id: "S2_P9", src: soundAsset+"S2_P9.ogg"},
			{id: "S2_P10", src: soundAsset+"S2_P10.ogg"},
			{id: "S2_P11", src: soundAsset+"S2_P11.ogg"},
			{id: "S2_P12", src: soundAsset+"S2_P12.ogg"},
			{id: "S2_P13", src: soundAsset+"S2_P13.ogg"},
			{id: "S2_P14", src: soundAsset+"S2_P14.ogg"},
			{id: "S2_P15", src: soundAsset+"S2_P15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// alert(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		// content1[countNext].secimage?put_image_sec(content1, countNext):"";
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			case 1:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			case 2:
				$(".cupTapS2").css("pointer-events","none");
				sound_player("S2_P"+(countNext+1), ".cupTapS2",".hand");
				$(".tapToCup, .hand").hide(0);
				$(".cupTapS2, .hand").click(function(){
					$(".cupTapS2, .hand").hide(0);
					$(".tapToCup").show(0);
						nav_button_controls(3000);
				});
			break;
			case 3:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			case 4:
				$(".cup").css("pointer-events","none");
				sound_player("S2_P"+(countNext+1), ".cup",".hand");
				$(".cup").addClass("hoverCls");
				$(".cup_to_teapot, .hand").hide(0);
				$(".cup, .hand").click(function(){
					$(".cup").addClass("cupToPotAnim");
					$(".hand").hide(0);
					setTimeout(function(){
						$(".pot, .cup").hide(0);
						$(".cup_to_teapot").show(0);
							nav_button_controls(1300);
					},3000);
				});
			break;
			case 5:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			case 6:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			case 7:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			case 8:
				$(".potTap").css("pointer-events","none");
				sound_player("S2_P"+(countNext+1), ".potTap",".hand");
				$(".tapToPot, .hand").hide(0);
				$(".potTap, .hand").click(function(){
					$(".potTap, .potSec, .hand").hide(0);
					$(".tapToPot").show(0);
						nav_button_controls(1000);
				});
			break;
			case 9:
			case 12:
			case 13:
			case 14:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			case 10:
				$(".pots11").css("pointer-events","none");
				sound_player("S2_P"+(countNext+1), ".pots11",".hand");
				$(".pots11").addClass("hoverCls");
				$(".potToCup, .hand").hide(0);
				$(".pots11, .hand").click(function(){
				$(".pots11").addClass("potToCupAnim");
				$(".hand").hide(0);
				setTimeout(function(){
				$(".potToCup").show(0);
						nav_button_controls(2000);
				},3000);
				});
			break;
			case 11:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			default:
				nav_button_controls(1000);
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,nopntrcls, hndcls){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			$(hndcls).show(0);
			$(nopntrcls).css("pointer-events","auto");
		});
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1--> coverpage
	{
		contentblockadditionalclass:'cream_bg',
		extratextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'coverpage',
				imgclass:'background'
			}]
		}]
	},
	// slide 2
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg jug blinkeffect',
				imgid:'jug',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName",
			imagelabeldata:data.string.jug
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1s1txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 3
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg jg',
				imgid:'jug_gif',
			},
			{
				imgclass:'rhsImg jug',
				imgid:'jug',
			},
			{
				imgclass:'rhsImg tap',
				imgid:'tap',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			},
			{
				imgclass:'hand hovercls',
				imgid:'hand',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName",
			imagelabeldata:data.string.jug
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1s2txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 4
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg jug',
				imgid:'jug_fill',
			},
			{
				imgclass:'midImg glass',
				imgid:'glass',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName",
			imagelabeldata:data.string.jug
		},{
			imagelabelclass:"imgName glsNam",
			imagelabeldata:data.string.glass
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1s3txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 5
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg jug',
				imgid:'jug_fill',
			},
			{
				imgclass:'midImg glass',
				imgid:'glass',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName",
			imagelabeldata:data.string.jug
		},{
			imagelabelclass:"imgName glsNam",
			imagelabeldata:data.string.glass
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1s4txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 6
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg jug jtp',
				imgid:'jug_fill',
			},
			{
				imgclass:'midImg glass',
				imgid:'glass',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			},
			{
				imgclass:'jugToGlass',
				imgid:'jug_glass',
			},
			{
				imgclass:'hand hovercls s6_hand',
				imgid:'hand',
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1s5txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 7
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg jug',
				imgid:'jug',
			},
			{
				imgclass:' glass glassOverflow',
				imgid:'glassOverflow',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1s7txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 8
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg jug',
				imgid:'jug',
			},
			{
				imgclass:'midImg glass',
				imgid:'glassFull',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName",
			imagelabeldata:data.string.jug
		},{
			imagelabelclass:"imgName glsNam",
			imagelabeldata:data.string.glass
		},{
			imagelabelclass:"lesMore les",
			imagelabeldata:data.string.lescap
		},{
			imagelabelclass:"lesMore more",
			imagelabeldata:data.string.morecap
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1s8txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 9
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'rhsImg jug',
				imgid:'jug',
			},
			{
				imgclass:'midImg glass',
				imgid:'glassFull',
			},
			{
				imgclass:'squirell s1',
				imgid:'squirell',
			}
		],
		imagelabels:[{
			imagelabelclass:"imgName",
			imagelabeldata:data.string.jug
		},{
			imagelabelclass:"imgName glsNam",
			imagelabeldata:data.string.glass
		},{
			imagelabelclass:"lesMore les",
			imagelabeldata:data.string.lescap
		},{
			imagelabelclass:"lesMore more",
			imagelabeldata:data.string.morecap
		}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1s9txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"speechbuuble1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirell", src: imgpath+"squirrel01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug", src: imgpath+"jug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug_gif", src: imgpath+"tap_jug_one_time.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "tap", src: imgpath+"tap.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glass", src: imgpath+"glass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glassFull", src: imgpath+"glass_fill_water.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug_glass", src: imgpath+"jug_glass_one_time.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "jug_fill", src: imgpath+"jug_fill.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glassOverflow", src: imgpath+"jug_glass_one_time.png", type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "S1_P1", src: soundAsset+"S1_P1.ogg"},
			{id: "S1_P2", src: soundAsset+"S1_P2.ogg"},
			{id: "S1_P3", src: soundAsset+"S1_P3.ogg"},
			{id: "S1_P4", src: soundAsset+"S1_P4.ogg"},
			{id: "S1_P5", src: soundAsset+"S1_P5.ogg"},
			{id: "S1_P6", src: soundAsset+"S1_P6.ogg"},
			{id: "S1_P7", src: soundAsset+"S1_P7.ogg"},
			{id: "S1_P8", src: soundAsset+"S1_P8.ogg"},
			{id: "S1_P9", src: soundAsset+"S1_P9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
			case 1:
				sound_player_nav("S1_P"+(countNext+1));
			break;
			case 2:
				$(".tap").css("pointer-events","none");
				sound_player("S1_P"+(countNext+1),".hand", ".tap");
				$(".jg, .hand").hide(0);
				$(".tap, .hand").click(function(){
					$(".jug, .tap, .hand").hide(0);
					$(".jg").show(0);
					setTimeout(function(){nav_button_controls(0);},3000);
				});
			break;
			case 3:
				sound_player_nav("S1_P"+(countNext+1));
			break;
			case 4:
				sound_player_nav("S1_P"+(countNext+1));
			break;
			case 5:
				$(".jug").css("pointer-events","none");
				sound_player("S1_P"+(countNext+1),".hand",".jug");
				$(".jugToGlass,  .hand").hide(0);
				$(".jug, .hand").click(function(){
				$(".jug").addClass("jugPour");
				$(".hand").hide(0);
					setTimeout(function(){
						$(".jug, .glass").hide(0);
						$(".jugToGlass").show(0);
						setTimeout(function(){nav_button_controls(0);},5500);
					},2950);
				});
			break;
			case 6:
				sound_player_nav("S1_P"+(countNext+1));
			break;
			case 7:
				sound_player_nav("S1_P"+(countNext+1));
			break;
			case 8:
				sound_player_nav("S1_P"+(countNext+1));
			break;
			default:
				nav_button_controls(0);
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,imgToShow, imgtoClk){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			$(imgToShow).show(0);
			$(imgtoClk).css("pointer-events","auto");
		});
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

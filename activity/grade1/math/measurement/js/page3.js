var soundAsset = $ref+"/sounds/"+$lang+"/";
var soundAsset_numbers = $ref+"/audio_numbers/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[

	// slide 0
	{
		contentblockadditionalclass:'cream_bg',
		imageblock:[{
			imagestoshow:[{
				imgclass:'boy',
				imgid:'mohan',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p3text1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 1
	{
    contentblockadditionalclass:'yellow_bg',
    lowertextblockadditionalclass: "lowertext",
    extratextblock:[{
      textclass:'chaptertitle',
      textdata: data.string.p3text2
    }],
    lowertextblock:[{
      textclass:'box1',
      textdata: data.string.thumb
    },{
      textclass:'box2',
      textdata: data.string.handspan
    }],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'topbox',
          imgid:'mohanhead',
        },
			{
				imgclass:'lbox',
				imgid:'thumb',
			},{
				imgclass:'rbox',
				imgid:'handspan',
			}
		]
		}]
	},
		// slide 2
		{
      contentblockadditionalclass:'yellow_bg',
      lowertextblockadditionalclass: "lowertext",
      extratextblock:[{
        textclass:'chaptertitle',
        textdata: data.string.p3text3
      },{
        			textclass:'animation'
      }],
      imageblock:[{
        imagestoshow:[
          {
            imgclass:'topbox',
            imgid:'mohanhead',
          },
        {
          imgclass:'tbox',
          imgid:'thumb',
        },{
          imgclass:'pbox',
          imgid:'lpencil',
        }
      ]
      }]
		},
	// slide 3
		{
      contentblockadditionalclass:'yellow_bg',
      lowertextblockadditionalclass: "lowertext",
      extratextblock:[{
        textclass:'chaptertitle',
        textdata: data.string.p3text4
      },{
        			textclass:'animation1'
      },{
        textclass: 'animation2'
      }],
      imageblock:[{
        imagestoshow:[
          {
            imgclass:'topbox',
            imgid:'mohanhead',
          },
        {
          imgclass:'handbox',
          imgid:'handspan',
        },{
          imgclass:'pbox1',
          imgid:'lpencil',
        }
      ]
      }]
		},

	// slide 4
	{
		contentblockadditionalclass:'yellow_bg',

		extratextblock:[{
			textclass:'title',
			textdata: data.string.p3text5
		},{
			textclass:'num hide1',
			textdata: data.string.one
		},{
			textclass:'num hide2',
			textdata: data.string.two
		},{
			textclass:'xbox box3',
			textdata: data.string.p3text7
		},{
			textclass:'num hide3',
			textdata: data.string.one
		},{
			textclass:'num hide4',
			textdata: data.string.two
		},{
			textclass:'num hide5',
			textdata: data.string.three
		},{
			textclass:'xbox box4',
			textdata: data.string.p3text8
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:'topbox',
					imgid:'mohanhead',
				},{
          imgclass:'book1',
					imgid:'book1',
        },{
          imgclass:'book2',
					imgid:'book2',
        },{
          imgclass:'hand hand1',
					imgid:'hand',
        },{
          imgclass:'hand hand2',
					imgid:'hand',
        },{
          imgclass:'hand hand3',
					imgid:'hand',
        },{
          imgclass:'hand hand4',
					imgid:'hand',
        },{
          imgclass:'hand hand5',
					imgid:'hand',
        }]
			}]
	},
	{
		//slide 5
		contentblockadditionalclass:'yellow_bg',

		extratextblock:[{
			textclass:'title title-1',
			textdata: data.string.p3text6
		},{
			textclass:'title tex',
			textdata: data.string.p3text9
		},{
			textclass:'num hide1',
			textdata: data.string.one
		},{
			textclass:'num hide2',
			textdata: data.string.two
		},{
			textclass:'num hide3',
			textdata: data.string.one
		},{
			textclass:'num hide4',
			textdata: data.string.two
		},{
			textclass:'num hide5',
			textdata: data.string.three
		},
			{
                textclass:'emptydiv '
			},
            {
                textclass:'emptydiv1 class1 '
            }
		],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"box5",
				optaddclass:"",
				optdata:data.string.p3text7
			},{
					optcontainerextra:"box6",
				optaddclass:"class1",
				optdata:data.string.p3text8
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:'topbox',
					imgid:'mohanhead',
				},{
					imgclass:'book1',
					imgid:'book1',
				},{
					imgclass:'book2',
					imgid:'book2',
				},{
					imgclass:'hand hand1',
					imgid:'hand',
				},{
					imgclass:'hand hand2',
					imgid:'hand',
				},{
					imgclass:'hand hand3',
					imgid:'hand',
				},{
					imgclass:'hand hand4',
					imgid:'hand',
				},{
					imgclass:'hand hand5',
					imgid:'hand',
				}]
			}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"bubble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mohan", src: imgpath+"mohan01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mohanhead", src: imgpath+"mohan02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thumb", src: imgpath+"thumb.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handspan", src: imgpath+"hand01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lpencil", src: imgpath+"long_pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book1", src: imgpath+"books01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book2", src: imgpath+"books03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand.png", type: createjs.AbstractLoader.IMAGE},



		//sound
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p6_1", src: soundAsset+"s3_p6_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		// content[countNext].imageload?put_image_sec(content, countNext):'';

		put_speechbox_image(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		switch(countNext) {
			case 2:
			sound_player("s3_p"+(countNext+1),1);
			$(".animation").delay(1000).show(0);
			nav_button_controls(4000);
			break;
			case 3:
			sound_player("s3_p"+(countNext+1),0);
			$(".animation2").delay(1000).show(0);
			$(".animation1").delay(5000).show(0);
			nav_button_controls(9000);
			break;
			case 4:
			sound_player("s3_p"+(countNext+1),0);
			$(".hide1,.hide2,.hide3,.hide4,.hide5,.hand1, .hand2,.hand3,.hand4,.hand5,.box3, .box4").hide();
			$(".hand1, .hide1").delay(7000).fadeIn(1000);
			$(".hand2, .hide2").delay(7500).fadeIn(1000);
			$(".box3").delay(8000).fadeIn(1000);
			$(".hand3, .hide3").delay(11000).fadeIn(1000);
			$(".hand4, .hide4").delay(11500).fadeIn(1000);
			$(".hand5, .hide5").delay(11600).fadeIn(1000);
			$(".box4").delay(12000).fadeIn(1000);
			nav_button_controls(18000);
			break;
			case 5:
			sound_player("s3_p"+(countNext+1),0);
			$(".tex").hide(0);
			$(".box5,.box6").hide(0);
			$(".box5").delay(1000).fadeIn(1000);
			$(".box6").delay(3000).fadeIn(1000);
			$(".optionsdiv p").css({
				"background-color":"#fff2cc",
				"color":"#000",
				"pointer-events":"none",
				"border":"5px solid #fff2cc"
			})
			break;
 		default:
			sound_player("s3_p"+(countNext+1),1);
		break;
		}
		$(".emptydiv,.emptydiv1").click(function(){
			$(this).removeClass('forhover');
					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						play_correct_incorrect_sound(1);
						$(this).css({
							"border":"7px solid #98c02e",
							"color":"#fff",
							'pointer-events': 'none',
							'z-index':'2'
						});
						$(this).siblings(".corctopt").show(0);
							$(".tex").show(0);
						$(".buttonsel").css("pointer-events","none")
						nav_button_controls(0);
						countNext==5?sound_player("s3_p6_1"):"";
					}
					else{
						play_correct_incorrect_sound(0);
						$(this).css({
							"border":"7px solid #FF0000",
							"color":"#000",
							'pointer-events': 'none',
                            'z-index':'2'

                        });
						$(this).siblings(".wrngopt").show(0);
					}
			});

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():"";
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
	if(content[count].hasOwnProperty('sideboxes')){
		for(var i=0; i<content[count].sideboxes.length;i++){
			if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
			{
				var imageblock = content[count].sideboxes[i].imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var j=0; j<imageClass.length; j++){
						var image_src = preload.getResult(imageClass[j].imgid).src;
						//get list of classes
						var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
							// alert(i);
					}
				}
			}
		}
	}
}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

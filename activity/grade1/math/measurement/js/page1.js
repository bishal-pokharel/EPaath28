var soundAsset = $ref+"/sounds/"+$lang+"/";
var imgpath = $ref+"/images/";


var content=[
	// slide 0--> coverpage
	{
		contentblockadditionalclass:'cream_bg',
		extratextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'coverpage',
				imgclass:'background'
			}]
		}]
	},
	// slide 1
	{
		contentblockadditionalclass:'cream_bg',
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'girl',
				imgid:'girl01',
			},
			{
				imgclass:'boy',
				imgid:'boy',
			}
		]

		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1text1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 2
	{
		contentblockadditionalclass:'cream_bg',
		extratextblock:[{
			textclass:'animation'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'girl',
				imgid:'girl01',
			},
			{
				imgclass:'boy',
				imgid:'boy',
			},{
				imgclass:'scale',
				imgid:'scale',
			}
		]

		}],
		speechbox:[{
			datahighlightflag: true,
			datahighlightcustomclass: "textcolor",
			speechbox: 'sp-1',
			textdata : data.string.p1text2,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
		// slide 3
		{
			contentblockadditionalclass:'cream_bg',
			extratextblock:[{
				textclass:'animation1'
			},{
				textclass: "dotline"
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass:'background',
					imgid:'bg01',
				},
				{
					imgclass:'girl-1',
					imgid:'girl03',
				},
				{
					imgclass:'boy',
					imgid:'boy',
				},{
					imgclass:'scale',
					imgid:'scale',
				}
			]

			}],
			speechbox:[{
				datahighlightflag: true,
				datahighlightcustomclass: "textcolor",
				speechbox: 'sp-1',
				textdata : data.string.p1text3,
				textclass : 'txt1',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}]
		},
	// slide 4
		{
			contentblockadditionalclass:'cream_bg',
			lowertextblockadditionalclass: "lowertext",
			lowertextblock:[{
				textclass:'box1',
				textdata: data.string.tall
			},{
				textclass:'box2',
				textdata: data.string.short
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass:'background',
					imgid:'bg01',
				},
				{
					imgclass:'girl',
					imgid:'girl03',
				},
				{
					imgclass:'boy',
					imgid:'boy',
				}
			]
		}]
	},
	// slide 5
	{
		contentblockadditionalclass:'cream_bg',

		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg02',
			},
			{
				imgclass:'girl',
				imgid:'girl',
			},
			{
				imgclass:'boy girl1',
				imgid:'girl01',
			}
		]

		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p1text4,
			textclass : 'txt1',
			imgclass: 'box box3',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 6
	{
		contentblockadditionalclass:'cream_bg',

		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg02',
			},
			{
				imgclass:'girl',
				imgid:'girl',
			},
			{
				imgclass:'boy girl1',
				imgid:'girl01',
			}
		]

		}],
		speechbox:[{
			datahighlightflag: true,
			datahighlightcustomclass: "textcolor",
			speechbox: 'sp-2',
			textdata : data.string.p1text5,
			textclass : 'txt1',
			imgclass: 'box box3',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 7
	{
		contentblockadditionalclass:'cream_bg',
		lowertextblockadditionalclass: "lowertext",
		lowertextblock:[{
			textclass:'box1',
			textdata: data.string.tall
		},{
			textclass:'box2',
			textdata: data.string.short
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg01',
			},
			{
				imgclass:'girl',
				imgid:'girl',
			},
			{
				imgclass:'boy',
				imgid:'girl02',
			}
		]
	}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"bubble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl01", src: imgpath+"geeta01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl03", src: imgpath+"geeta03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"mohan.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"mita.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl02", src: imgpath+"geeta02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "scale", src: imgpath+"scale01.png", type: createjs.AbstractLoader.IMAGE},

		//sound
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		switch(countNext) {
			case 1:
				sound_player("s1_p"+(countNext+1),1);	
			$(".sp-1").fadeIn(1000);
			break;
			case 2:
			case 3:
				sound_player("s1_p"+(countNext+1),1);	
			$(".sp-1").fadeIn(1000);
			$(".animation,.animation1").delay(1000).show(0);
			break;
			case 5:
			case 6:
				sound_player("s1_p"+(countNext+1),1);	
			$(".sp-2").fadeIn(1000);
			break;

			default:
				sound_player("s1_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():"";
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

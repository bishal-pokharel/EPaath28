var soundAsset = $ref+"/sounds/"+$lang+"/";
var imgpath = $ref+"/images/";


var content=[

	// slide 0
	{
		contentblockadditionalclass:'cream_bg',
		imageblock:[{
			imagestoshow:[{
				imgclass:'girl',
				imgid:'girl02',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2text1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},
	// slide 1
	{
		contentblockadditionalclass:'yellow_bg',
		extratextblock:[{
			datahighlightflag: true,
			datahighlightcustomclass: "textcolor",
			textclass:'box1',
			textdata: data.string.p2text2
		},{
			datahighlightflag: true,
			datahighlightcustomclass: "textcolor",
			textclass:'box2',
			textdata: data.string.p2text3
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass:'lpencil',
				imgid:'longpencil',
			},{
				imgclass:'spencil',
				imgid:'shortpencil',
			},{
				imgclass:'speaker speaker-1',
				imgid:'speaker',
			},{
				imgclass:'speaker speaker-2',
				imgid:'speaker',
			}
		]
		}]
	},
		// slide 2
		{
			contentblockadditionalclass:'yellow_bg',
			extratextblock:[{
				datahighlightflag: true,
				datahighlightcustomclass: "textcolor",
				textclass:'box1',
				textdata: data.string.p2text4
			},{
				datahighlightflag: true,
				datahighlightcustomclass: "textcolor",
				textclass:'box2',
				textdata: data.string.p2text5
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'lscale',
					imgid:'longscale',
				},{
					imgclass:'sscale',
					imgid:'shortscale',
				},{
					imgclass:'speaker speaker-1',
					imgid:'speaker',
				},{
					imgclass:'speaker speaker-2',
					imgid:'speaker',
				}
			]
			}]
		},
	// slide 3
		{
			contentblockadditionalclass:'yellow_bg',
			imageload:true,
			extratextblock:[{
				textclass:'chaptertitle',
				textdata: data.string.p2text6
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass:'topbox',
						imgid:'geeta',
					}]
				}],
			sideboxes:[{
			sideboxclass:"cbox leftBoxSec",
			imageblock:[{
				imagestoshow:[
				{
						imgclass:'long',
						imgid:'lsock',
				 	}]
			}]
		},
		{
			sideboxclass:"cbox class1 rightboxSec",
			imageblock:[{
				imagestoshow:[{
					imgclass:'short',
	 				imgid:'ssock',
				}]
			}]
		}]
	},
	// slide 4
	{
		contentblockadditionalclass:'yellow_bg',
		imageload:true,
		extratextblock:[{
			textclass:'chaptertitle',
			textdata: data.string.p2text7
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:'topbox biggerTpBx',
					imgid:'mohan',
				}]
			}],
		sideboxes:[{
		sideboxclass:"cbox class1 leftBoxSec",
		imageblock:[{
			imagestoshow:[
			{
					imgclass:'long',
					imgid:'lpant',
				}]
		}]
	},
	{
		sideboxclass:"cbox rightboxSec",
		imageblock:[{
			imagestoshow:[{
				imgclass:'short',
				imgid:'spant',
			}]
		}]
	}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var timeoutvar_1 = null;
	var current_sound;
	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"bubble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "longpencil", src: imgpath+"long_pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shortpencil", src: imgpath+"short_pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl03", src: imgpath+"geeta03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"mohan.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"mita.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shortscale", src: imgpath+"short_scale.png", type: createjs.AbstractLoader.IMAGE},
			{id: "longscale", src: imgpath+"long_scale.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl02", src: imgpath+"geeta02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lsock", src: imgpath+"long_sock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ssock", src: imgpath+"short_sock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "geeta", src: imgpath+"geeta05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lpant", src: imgpath+"long_pant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spant", src: imgpath+"half_pant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mohan", src: imgpath+"mohan02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speaker", src: imgpath+"sound-icon-2.png", type: createjs.AbstractLoader.IMAGE},



		//sound
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2-01", src: soundAsset+"s2_p2-01.ogg"},
			{id: "s2_p2-02", src: soundAsset+"s2_p2-02.ogg"},
			{id: "s2_p3-01", src: soundAsset+"s2_p3-01.ogg"},
			{id: "s2_p3-02", src: soundAsset+"s2_p3-02.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):'';
		put_speechbox_image(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		switch(countNext) {
			case 1:
			$(".speaker,.box1,.box2").css("pointer-events","none");
			timeoutvar = setTimeout(function(){
				sound_player("s2_p2-01",0);
				$(".box1,.speaker-1").show(0);
				timeoutvar_1 = setTimeout(function(){
					sound_player("s2_p2-02",1);
					$(".box2, .speaker-2").show(0);
				}, 3000);
			},8000);
			$(".speaker-1,.box1").click(function(){
				sound_player("s2_p2-01",1);
			});
			$(".speaker-2,.box2").click(function(){
				sound_player("s2_p2-02",1);
			});
			break;
			case 2:
			$(".speaker,.box1,.box2").css("pointer-events","none");
			timeoutvar = setTimeout(function(){
				sound_player("s2_p3-01",0);
				$(".box1,.speaker-1").show(0);
				timeoutvar_1 = setTimeout(function(){
					sound_player("s2_p3-02",1);
					$(".box2, .speaker-2").show(0);
				}, 1500);
			},5000);

			// setTimeout(function(){
			// 	sound_player("s2_p3-01",0);
			// },3200);
			// setTimeout(function(){
			// 	sound_player("s2_p3-02",1);
			// },9000);
			// $(".box1, .speaker-1").delay(3000).show(0);
			// $(".box2, .speaker-2").delay(6000).show(0);
			$(".speaker-1,.box1").click(function(){
				sound_player("s2_p3-01",1);
			});
			$(".speaker-2,.box2").click(function(){
				sound_player("s2_p3-02",1);
			});
			break;
			case 3:
			sound_player("s2_p"+(countNext+1),1);
			break;
			case 4:
			sound_player("s2_p"+(countNext+1),1);
			break;
 		default:
			sound_player("s2_p"+(countNext+1),1);
		break;
		}
		$(".cbox").click(function(){
			createjs.Sound.stop();
			if($(this).hasClass("class1")){
				$(this).children(".corctopt").show(0);
				play_correct_incorrect_sound(1);
				$(".cbox").css("pointer-events","none");
				nav_button_controls(0);
			}else{
				$(this).children(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				$(this).css("pointer-events","none");
			}
		});

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():"";
			next?$(".speaker,.box1,.box2").css("pointer-events","auto"):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
	if(content[count].hasOwnProperty('sideboxes')){
		for(var i=0; i<content[count].sideboxes.length;i++){
			if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
			{
				var imageblock = content[count].sideboxes[i].imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var j=0; j<imageClass.length; j++){
						var image_src = preload.getResult(imageClass[j].imgid).src;
						//get list of classes
						var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
							// alert(i);
					}
				}
			}
		}
	}
}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar_1);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

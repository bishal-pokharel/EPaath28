var imgpath = $ref + "/images/playtime/";
var soundAsset = $ref + "/sounds/" + $lang + "/";
var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    textblock: [
      {
        textdiv: "ptime",
        textclass: "chapter centertext",
        textdata: data.string.playtime
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgdiv: "coverpage",
            imgclass: "relativecls img1",
            imgid: "coverpageImg",
            imgsrc: ""
          },
          {
            imgdiv: "rhinodancingdiv",
            imgclass: " relativecls rhinoimgdiv",
            imgid: "rhinodance",
            imgsrc: ""
          },
          {
            imgdiv: "squirrelistening",
            imgclass: " relativecls squirrelisteningimg",
            imgid: "squirrel",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide 1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    extratextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "text",
        textclass: "box",
        textdata: data.string.playtext1
      }
    ],
    imgexc: [
      {
        imgexcontainerclass: "firstContainer",
        imgexcoptions: [
          {
            imgoptionscontainerclass: "opnContainer option1",
            imgclass: "boximg bim1",
            imgid: "prem"
          },
          {
            imgoptionscontainerclass: "opnContainer option2",
            imgclass: "boximg bim2",
            imgid: "premuncle"
          }
        ]
      }
    ],
    bulbcontainer: [
      {
        blbcontainerclass: " blbcontainer"
      }
    ]
  },
  //slide 2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    extratextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "text",
        textclass: "box",
        textdata: data.string.playtext2
      }
    ],
    imgexc: [
      {
        imgexcontainerclass: "firstContainer",
        imgexcoptions: [
          {
            imgoptionscontainerclass: "opnContainer contain option1",
            imgclass: "boximg rope bim1",
            imgid: "shortrope"
          },
          {
            imgoptionscontainerclass: "opnContainer contain option2",
            imgclass: "boximg rope bim2",
            imgid: "longrope"
          }
        ]
      }
    ],
    bulbcontainer: [
      {
        blbcontainerclass: " blbcontainer"
      }
    ]
  },
  {
    //slide 3
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    extratextblock: [
      {
        textclass: "box",
        textdata: data.string.playtext3
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "pic",
            imgid: "book",
            imgsrc: ""
          }
        ]
      }
    ],
    exetype1: [
      {
        optionsdivclass: "optionsdiv",
        exeoptions: [
          {
            optcontainerextra: "opti1 correct",
            optaddclass: "",
            optdata: data.string.playtext4
          },
          {
            optcontainerextra: "opti2",
            optaddclass: "",
            optdata: data.string.playtext5
          },
          {
            optcontainerextra: "opti3",
            optaddclass: "",
            optdata: data.string.playtext6
          },
          {
            optcontainerextra: "opti4",
            optaddclass: "",
            optdata: data.string.playtext7
          }
        ]
      }
    ],
    bulbcontainer: [
      {
        blbcontainerclass: " blbcontainer"
      }
    ]
  },
  {
    //slide 4
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    extratextblock: [
      {
        textclass: "box",
        textdata: data.string.playtext8
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "pic",
            imgid: "table",
            imgsrc: ""
          }
        ]
      }
    ],
    exetype1: [
      {
        optionsdivclass: "optionsdiv",
        exeoptions: [
          {
            optcontainerextra: "opti1",
            optaddclass: "",
            optdata: data.string.playtext9
          },
          {
            optcontainerextra: "opti2",
            optaddclass: "",
            optdata: data.string.playtext10
          },
          {
            optcontainerextra: "opti3 correct",
            optaddclass: "",
            optdata: data.string.playtext11
          },
          {
            optcontainerextra: "opti4",
            optaddclass: "",
            optdata: data.string.playtext12
          }
        ]
      }
    ],
    bulbcontainer: [
      {
        blbcontainerclass: " blbcontainer"
      }
    ]
  },
  {
    //slide 5
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    extratextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "text",
        textclass: "box",
        textdata: data.string.playtext13
      }
    ],
    imgexc: [
      {
        imgexcontainerclass: "firstContainer",
        imgexcoptions: [
          {
            imgoptionscontainerclass: "opnContainer contain option1",
            imgclass: "boximg hand bim1",
            imgid: "shortbottle"
          },
          {
            imgoptionscontainerclass: "opnContainer contain option2",
            imgclass: "boximg hand bim2",
            imgid: "longbottle"
          }
        ]
      }
    ],
    bulbcontainer: [
      {
        blbcontainerclass: " blbcontainer"
      }
    ]
  },
  //lastslide
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    bulbcontainer: [
      {
        blbcontainerclass: "blbcontain"
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var preload;
  var timeoutvar = null;
  var current_sound = "";
  var sound = "";
  var tempfirstarray = new Array();
  var tempsecondarray = new Array();
  var palindromarray = new Array();
  var endpageex = new EndPageofExercise();
  var message = data.string.lastmsg;
  endpageex.init(3);
  loadTimelineProgress($total_page, countNext + 1);

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {
        id: "coverpageImg",
        src: imgpath + "bg_play-time.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rhinodance",
        src: imgpath + "rhino_dancing.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "squirrel",
        src: imgpath + "squirrel-listening.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "prem",
        src: imgpath + "pream.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "premuncle",
        src: imgpath + "pream_uncle.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bulb1",
        src: imgpath + "bulb1.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bulb2",
        src: imgpath + "bulb2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "longrope",
        src: imgpath + "long_rope.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "shortrope",
        src: imgpath + "short_rope.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "book",
        src: imgpath + "measuringbook.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "shortrope",
        src: imgpath + "short_rope.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "table",
        src: imgpath + "measuring_table.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "longbottle",
        src: imgpath + "long_bottle.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "shortbottle",
        src: imgpath + "short_bottle.png",
        type: createjs.AbstractLoader.IMAGE
      },

      //sounds
      { id: "goodjob", src: soundAsset + "goodjob.ogg" },
      { id: "exe_ins_1_short", src: soundAsset + "exe_ins_1_short.ogg" },
      { id: "exe_ins_1_tall", src: soundAsset + "exe_ins_1_tall.ogg" },
      { id: "exe_ins_2_long", src: soundAsset + "exe_ins_2_long.ogg" },
      { id: "exe_ins_2_short", src: soundAsset + "exe_ins_2_short.ogg" },
      { id: "exe_ins_3", src: soundAsset + "exe_ins_3.ogg" },
      { id: "exe_ins_4", src: soundAsset + "exe_ins_4.ogg" },
      { id: "exe_ins_5_long", src: soundAsset + "exe_ins_5_long.ogg" },
      { id: "exe_ins_5_short", src: soundAsset + "exe_ins_5_short.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }

  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();
  $refreshBtn.click(function() {
    templateCaller();
  });
  /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
    } else if ($total_page == 1) {
      $nextBtn.css("display", "none");

      ole.footerNotificationHandler.lessonEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
    } else if (countNext == $total_page - 2) {
      $nextBtn.css("display", "none");
      // if lastpageflag is true
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*=====  End of user navigation controller function  ======*/

  /*=================================================
     =            general template function            =
     =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    firstPagePlayTime(countNext);
    texthighlight($board);
    put_image(content, countNext);
    put_image_sec(content, countNext);
    var number = ole.nepaliNumber(countNext, $lang);
    $(".box").prepend(number + ". ");

    // random number generator
    function rand_generator(limit) {
      var randNum = Math.floor(Math.random() * (limit - 1 + 1)) + 1;
      return randNum;
    }

    /*for randomizing the options*/
    function randomize(parent) {
      var parent = $(parent);
      var divs = parent.children();
      while (divs.length) {
        parent.append(
          divs.splice(Math.floor(Math.random() * divs.length), 1)[0]
        );
      }
    }

    for (var i = 0; i <= 4; i++) {
      $(".blbcontainer").append(
        "<img class='bulb" +
          (i + 1) +
          "' src='" +
          preload.getResult("bulb1").src +
          "'/>"
      );
    }
    for (var i = 0; i <= 4; i++) {
      $(".blbcontain").append(
        "<img class='bulbs" +
          (i + 1) +
          "' src='" +
          preload.getResult("bulb1").src +
          "'/>"
      );
    }

    switch (countNext) {
      case 1:
        var qnNum = rand_generator(2);
        if (qnNum == 1) {
          $(".text").html(eval("data.string.short1"));
          $(".option1").addClass("correct");
          sound_player("exe_ins_1_short");
        } else {
          $(".text").html(eval("data.string.tall1"));
          $(".option2").addClass("correct");
          sound_player("exe_ins_1_tall");
        }
        randomize(".firstContainer");
        break;
      case 2:
      var qnNum = rand_generator(2);
        if (qnNum == 1) {
          $(".text").html(eval("data.string.short2"));
          $(".option1").addClass("correct");
          sound_player("exe_ins_2_short");
        } else {
          $(".text").html(eval("data.string.long"));
          $(".option2").addClass("correct");
          sound_player("exe_ins_2_long");
        }
        $(".bulb1").attr("src", preload.getResult("bulb2").src);
        randomize(".firstContainer");
        break;
      case 3:
        $(".bulb1, .bulb2").attr("src", preload.getResult("bulb2").src);
        randomize(".optionsdiv");
        sound_player("exe_ins_3");
        break;
      case 4:
        $(".bulb1,.bulb2, .bulb3").attr("src", preload.getResult("bulb2").src);
        randomize(".optionsdiv");
        sound_player("exe_ins_4");
        break;
      case 5:
        var qnNum = rand_generator(2);
        if (qnNum == 1) {
          $(".text").html(eval("data.string.short2"));
          $(".option1").addClass("correct");
          sound_player("exe_ins_5_short");
        } else {
          $(".text").html(eval("data.string.long"));
          $(".option2").addClass("correct");
          sound_player("exe_ins_5_long");
        }
        $(".bulb1,.bulb2, .bulb3, .bulb4").attr(
          "src",
          preload.getResult("bulb2").src
        );
        randomize(".firstContainer");
        break;
      case 6:
        $(".bulbs1,.bulbs2, .bulbs3,.bulbs4,.bulbs5")
          .attr("src", preload.getResult("bulb1").src)
          .css("opacity", "1");
        bulbsappear(0, 6);
        endpageex.endpage(message);
        createjs.Sound.stop();
        current_sound = createjs.Sound.play("goodjob");
        current_sound.play();
      default:
        navigationcontroller();
        break;
    }
    $(".audiodiv").click(function() {
      sound_player(sound, false);
    });

    $(".opnContainer, .optionscontainer").click(function() {
      if ($(this).hasClass("correct")) {
        $(this)
          .children(".corctopt")
          .show(0);
        play_correct_incorrect_sound(1);
        $(".opnContainer, .optionscontainer").css("pointer-events", "none");
        navigationcontroller();
        $nextBtn.show(0);
        $(".bulb" + countNext).attr("src", preload.getResult("bulb2").src);
      } else {
        $(this)
          .children(".wrngopt")
          .show(0);
        play_correct_incorrect_sound(0);
        $(this).css("pointer-events", "none");
      }
    });
  }

  function sound_player(sound_id, navigate, imgCls, imgSrc) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      navigate ? navigationcontroller() : "";
      if (imgCls && imgSrc) {
        $("." + imgCls).attr("src", imgSrc);
      }
    });
  }

  // function for bulbapper
  function bulbsappear(bulbnum, bulblimit) {
    $(".bulbs" + bulbnum).attr("src", preload.getResult("bulb2").src);
    // $(".bulbs1"+countNext).attr("src", preload.getResult("bulb2").src);
    setTimeout(function() {
      bulbnum += 1;
      if (bulbnum < bulblimit) {
        bulbsappear(bulbnum, bulblimit);
        play_correct_incorrect_sound(1);
      }
    }, 1000);
  }

  function put_image(content, count) {
    var contentCount = content[count];
    var imageblockcontent = contentCount.hasOwnProperty("imageblock");
    dynamicimageload(imageblockcontent, contentCount);
  }

  function put_image2(content, count) {
    if (content[count].hasOwnProperty("imghintblock")) {
      var contentCount = content[count].imghintblock[0];
      var imageblockcontent = contentCount.hasOwnProperty("imageblock");
      dynamicimageload(imageblockcontent, contentCount);
    }
  }

  function dynamicimageload(imageblockcontent, contentCount) {
    if (imageblockcontent) {
      var imageblock = contentCount.imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  function put_image_sec(content, count) {
    if (content[count].hasOwnProperty("imgexc")) {
      var imageblock = content[count].imgexc[0];
      if (imageblock.hasOwnProperty("imgexcoptions")) {
        var imageClass = imageblock.imgexcoptions;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          // console.log(selector);
          $(selector).attr("src", image_src);
        }
      }
    }
  }
  function templateCaller() {
    $nextBtn.css("display", "none");
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        // lampTemplate.gotoNext();
        templateCaller();
        break;
    }
  });

  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
});

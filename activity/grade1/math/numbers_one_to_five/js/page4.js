var imgpath = $ref + "/images/SVG/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
// slide1
	{
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p4s0,
		imgclass: '',
		imgid : 'tb-2',
		imgsrc: '',
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg2',
				imgsrc: ""
			}
		]
	}]
	},
	// A slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-a',
		extratextblock:[{
			textdata: '',
			textclass: "divider divider-a",
			},{
			textdata: data.string.let1,
			textclass: "btmletter",
			}],
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "letter-left en",
					imgsrc : '',
					imgid : 'letter-a'
				},
				{
					imgclass : "letter-left np",
					imgsrc : '',
					imgid : 'letter-a-np'
				}
			]
		}],
		svgblock: [{
			svgblock: 'letter-svg',
		}],
	},
	
	// B slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-b',
		extratextblock:[{
			textdata: '',
			textclass: "divider divider-b",
		},{
			textdata: data.string.let2,
			textclass: "btmletter",
			}],
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "en letter-left",
					imgsrc : '',
					imgid : 'letter-b'
				},
				{
					imgclass : "letter-left np",
					imgsrc : '',
					imgid : 'letter-b-np'
				}
			]
		}],
		svgblock: [{
			svgblock: 'letter-svg',
		}],
	},
	//C slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-c',
		extratextblock:[{
			textdata: '',
			textclass: "divider divider-c",
		},{
			textdata: data.string.let3,
			textclass: "btmletter",
			}],
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "en letter-left",
					imgsrc : '',
					imgid : 'letter-c'
				},
				{
					imgclass : "letter-left np",
					imgsrc : '',
					imgid : 'letter-c-np'
				}
			]
		}],
		svgblock: [{
			svgblock: 'letter-svg',
		}],
	},
	
	//D slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-d',
		extratextblock:[{
			textdata: '',
			textclass: "divider divider-d",
		},{
			textdata: data.string.let4,
			textclass: "btmletter",
			}],
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "en letter-left",
					imgsrc : '',
					imgid : 'letter-d'
				},
				{
					imgclass : "letter-left np",
					imgsrc : '',
					imgid : 'letter-d-np'
				}
			]
		}],
		svgblock: [{
			svgblock: 'letter-svg',
		}],
	},
	//E slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-e',
		extratextblock:[{
			textdata: '',
			textclass: "divider divider-e",
		},{
			textdata: data.string.let5,
			textclass: "btmletter",
			}],
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "en letter-left",
					imgsrc : '',
					imgid : 'letter-e'
				},
				{
					imgclass : "letter-left np",
					imgsrc : '',
					imgid : 'letter-e-np'
				}
			]
		}],
		svgblock: [{
			svgblock: 'letter-svg',
		}],
	}
];
$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	var my_interval = null;
	var my_interval2 = null;
	//for preload
	var preload;
	var timeoutvar = null;
	var timeoutArr = [];
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "apple", src: imgpath+"p2/apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src: imgpath+"p2/ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cat", src: imgpath+"p2/cat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"p2/dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "egg", src: imgpath+"p2/egg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"p2/fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "goat", src: imgpath+"p2/goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"p2/hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boygirl", src: imgpath+"boyandgirl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"../bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: imgpath+"tl-1.png", type: createjs.AbstractLoader.IMAGE},
			
			{id: "letter-a", src: imgpath+"one_1.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "letter-b", src: imgpath+"two_2.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "letter-c", src: imgpath+"three_3.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "letter-d", src: imgpath+"four_4.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "letter-e", src: imgpath+"five_5.svg", type: createjs.AbstractLoader.IMAGE},
			
			{id: "letter-a-np", src: imgpath+"ek.svg", type: createjs.AbstractLoader.IMAGE},	
			{id: "letter-b-np", src: imgpath+"due_2.svg", type: createjs.AbstractLoader.IMAGE},	
			{id: "letter-c-np", src: imgpath+"teen_3.svg", type: createjs.AbstractLoader.IMAGE},	
			{id: "letter-d-np", src: imgpath+"char_4.svg", type: createjs.AbstractLoader.IMAGE},	
			{id: "letter-e-np", src: imgpath+"paach_5.svg", type: createjs.AbstractLoader.IMAGE},
			
			//sounds
			{id: "sound_letter_1", src: soundAsset+"1.ogg"},
			{id: "sound_letter_2", src: soundAsset+"2.ogg"},
			{id: "sound_letter_3", src: soundAsset+"3.ogg"},
			{id: "sound_letter_4", src: soundAsset+"4.ogg"},
			{id: "sound_letter_5", src: soundAsset+"5.ogg"},
			{id: "sound_16", src: soundAsset+"p4_s0.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		
		if($lang=='np'){
			$(".en").hide(0);
		}
		else if($lang == 'en'){
			$(".np").hide(0);
		}

		switch(countNext) {
			case 0:		
				sound_nav("sound_16");	     
				// $nextBtn.show(0);
			 break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			
				if(countNext>0) $prevBtn.show(0);
				var letterSoundArray = ['sound_letter_1', 'sound_letter_2', 'sound_letter_3', 'sound_letter_4', 'sound_letter_5'];
				
				var soundArray = ['sound_0', 'sound_2', 'sound_4', 'sound_6', 'sound_8'];
				
				var letterSVG = ['letter-a', 'letter-b', 'letter-c', 'letter-d', 'letter-e'];
				if($lang=='np'){
					var letterSVG = ['letter-a-np', 'letter-b-np', 'letter-c-np', 'letter-d-np', 'letter-e-np'];
				}
				var letterArr = ['a', 'b', 'c', 'd','e'];
				
				var s = Snap('#letter-svg');
				var newCount = countNext-1;
				//alert(newCount);
				var path = [], group, animate_letter = null;
				var svg = Snap.load(preload.getResult(letterSVG[newCount]).src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					for(var i=0; i<5; i++){
						if(Snap.select('#path'+i)!=null)
						path.push(Snap.select('#path'+i));
					}
					group = Snap.select('#gr');
					path[0].addClass('opacity_0');
					var animatePath = function(pathName, index, callback){					
						var curIndex = index;
						var soundName = ('step_'+(pathName.length-1))+(index+1);
						sound_player(soundName);
						console.log(pathName);
						if(index == 0){
							pathName[index+1].addClass('line-anim-1');
						} else{
							pathName[index+1].addClass('line-anim-css');
						}
						timeoutvar = setTimeout(function(){
							curIndex = index+1;
							if(index<pathName.length-2){
								callback(pathName, curIndex, callback);
							}else{
								//$nextBtn.show(0);
								// sound_player(letterSoundArray[newCount]);
								sound_nav(letterSoundArray[newCount]);
							}
						}, 2000);
					};
					animate_letter = function(){
						group.attr('display', 'block');
						if($lang=="np"){							
							path[0].addClass('letter-fade-in-'+letterArr[newCount]+'-np');
							group.addClass('letter-fade-out-'+letterArr[newCount]+'-np');
						}
						else if ($lang=="en"){							
							path[0].addClass('letter-fade-in-'+letterArr[newCount]);
							group.addClass('letter-fade-out-'+letterArr[newCount]);
						}
						for(var i=1; i<path.length; i++){
							path[i].addClass('no-dash-'+letterArr[newCount]);
						}
						animatePath(path, 0, animatePath);
					};
					animate_letter();
				} );
				// setTimeout(function(){
					// animate_letter();
				// },2000);
				// current_sound = createjs.Sound.play(soundArray[newCount]);
				// current_sound.play();
				// current_sound.on("complete", function(){
					// animate_letter();
				// });
				break;
			default:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_nav('sound_'+countNext);
				}, 300);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		clearTimeout(timeoutvar);
		createjs.Sound.stop();
		for(var i=0; i<timeoutArr.length; i++){
			clearTimeout(timeoutArr[i]);
		}
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		for(var i=0; i<timeoutArr.length; i++){
			clearTimeout(timeoutArr[i]);
		}
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

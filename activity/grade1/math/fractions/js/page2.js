var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	// slide0
  {
    contentblockadditionalclass:'plainpurplebg',
    extratextblock:[
    {
      textclass:'texttop',
      textdata:data.string.p2s5
    },
    {
      textclass:'suritext',
      textdata:data.string.p2s1
    },
    {
      textclass:'pematext',
      textdata:data.string.p2s2
    },
    {
      textclass:'devtext',
      textdata:data.string.p2s3
    },
    {
      textclass:'mayatext',
      textdata:data.string.p2s4
    },
    {
      textclass:'texttop after',
      textdata:data.string.p2s0
    },
    {
      textclass:'textbot',
      textdata:data.string.p2s6
    },
    {
      textclass:'texttop1',
      textdata:data.string.p2s8
    }
    ],
    speechbox:[{
  		speechbox: 'sp-1',
  		textclass: "answerl",
  		textdata: data.string.p2s9,
      datahighlightflag : true,
      datahighlightcustomclass : 'purple',
  		imgclass: '',
  		imgid : 'textbox',
  		imgsrc: '',
  	},
    {
      speechbox: 'sp-2',
      textclass: "answerl",
      textdata: data.string.p2s10,
      datahighlightflag : true,
      datahighlightcustomclass : 'purple',
      imgclass: '',
      imgid : 'textbox',
      imgsrc: '',
    },
    {
      speechbox: 'sp-3',
      textclass: "answerr",
      textdata: data.string.p2s10,
      datahighlightflag : true,
      datahighlightcustomclass : 'purple',
      imgclass: '',
      imgid : 'textboxf',
      imgsrc: '',
    },
    {
      speechbox: 'sp-4',
      textclass: "answerr",
      textdata: data.string.p2s10,
      datahighlightflag : true,
      datahighlightcustomclass : 'purple',
      imgclass: '',
      imgid : 'textboxf',
      imgsrc: '',
    }],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'suri',
          imgclass:'suri',
        },
        {
          imgid:'pema',
          imgclass:'pema'
        },
        {
          imgid:'dev',
          imgclass:'dev'
        },
        {
          imgid:'maya',
          imgclass:'maya'
        },
        {
          imgid:'dotline',
          imgclass:'dotted'
        },
        {
          imgid:'dotline1',
          imgclass:'dotted1'
        },
        {
          imgid:'roti1',
          imgclass:'roti1'
        },
        {
          imgid:'roti2',
          imgclass:'roti2'
        },
        {
          imgid:'roti3',
          imgclass:'roti3'
        },
        {
          imgid:'roti4',
          imgclass:'roti4'
        },
    ]
    }]
  },
  //slide1
  {
		contentblockadditionalclass:'plainpurplebg',
		extratextblock:[{
			textclass:'one',
			textdata:data.string.one
		},
    {
      textclass:'two',
      textdata:data.string.two
    },
    {
      textclass:'three',
      textdata:data.string.three
    },
    {
      textclass:'four',
      textdata:data.string.four
    },
    {
      textclass:'textbot1',
      textdata:data.string.p2s11,
      datahighlightflag : true,
      datahighlightcustomclass : 'purple',
    }],
		imageblock:[{
			imagestoshow:[{
        imgid:'roti1',
        imgclass:'roti1'
      },
      {
        imgid:'roti2',
        imgclass:'roti2'
      },
      {
        imgid:'roti3',
        imgclass:'roti3'
      },
      {
        imgid:'roti4',
        imgclass:'roti4'
      }
    ]
		}]
	},
  //slide2
  {
		contentblockadditionalclass:'plainpurplebg',
		extratextblock:[{
            splitintofractionsflag: true,
            textclass:'onehalf',
			textdata:data.string.p2s13
		},
    {
        splitintofractionsflag: true,
        textclass:'twohalf',
      textdata:data.string.p2s13
    },
    {
        splitintofractionsflag: true,
        textclass:'threehalf',
      textdata:data.string.p2s13
    },
    {
        splitintofractionsflag: true,
        textclass:'fourhalf',
      textdata:data.string.p2s13
    },
    {
      textclass:'textbot1',
      textdata:data.string.p2s11,
      datahighlightflag : true,
      datahighlightcustomclass : 'purple',
    }],
		imageblock:[{
			imagestoshow:[{
        imgid:'roti1',
        imgclass:'roti1'
      },
      {
        imgid:'roti2',
        imgclass:'roti2'
      },
      {
        imgid:'roti3',
        imgclass:'roti3'
      },
      {
        imgid:'roti4',
        imgclass:'roti4'
      },
      {
        imgid:'dotline',
        imgclass:'dotted'
      },
      {
        imgid:'dotline1',
        imgclass:'dotted1'
      },
    ]
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	$('.mainBox > div').prepend('<p class="replay_button"></p>');

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
      {id: "suri", src: imgpath+"suri.png", type: createjs.AbstractLoader.IMAGE},
      {id: "pema", src: imgpath+"pema.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dev", src: imgpath+"dev.png", type: createjs.AbstractLoader.IMAGE},
      {id: "maya", src: imgpath+"maya.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rotifull", src: imgpath+"roti01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "roti1", src: imgpath+"roti25-01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "roti2", src: imgpath+"roti25-02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "roti3", src: imgpath+"roti25-03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "roti4", src: imgpath+"roti25-04.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dotline", src: imgpath+"black_doted_line01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dotline1", src: imgpath+"black_doted_line02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "textbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
  		{id: "textboxf", src: imgpath+"text_box_flip.png", type: createjs.AbstractLoader.IMAGE},
			//images

  		// sounds
      {id: "S2_P1_1", src: soundAsset+"s2_p1-01.ogg"},
      {id: "S2_P1_2", src: soundAsset+"s2_p1-02.ogg"},
      {id: "S2_P1_3", src: soundAsset+"s2_p1-03.ogg"},
      {id: "S2_P1_4", src: soundAsset+"s2_p1-04.ogg"},
      {id: "S2_P1_5", src: soundAsset+"s2_p1-05.ogg"},
      {id: "S2_P1_6", src: soundAsset+"s2_p1-06.ogg"},
      {id: "S2_P2", src: soundAsset+"s2_p2.ogg"},
      {id: "S2_P3", src: soundAsset+"s2_p3.ogg"},
  		{id: "s2_p1_Sec", src: soundAsset+"s2_p1_1.ogg"},
  		{id: "sa", src: soundAsset+"s2_p1a.ogg"},
  		{id: "sb", src: soundAsset+"s2_p1b.ogg"},
  		{id: "sc", src: soundAsset+"s2_p1c.ogg"},
  		{id: "sd", src: soundAsset+"s2_p1d.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound); //for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

    firstPagePlayTime(countNext);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        splitintofractions($board);
        vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

/*	var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +6.5+"%";
	$('.top_below_text').css({
		'top':top_val
		});
*/
		switch(countNext) {
			case 0:
      $('.roti1,.roti2,.roti3,.roti4,.dev,.maya,.pema,.suri,.devtext,.mayatext,.suritext,.pematext,.after,.textbot,.dotted,.dotted1,.texttop1').css({opacity: "0"});
      $('.speechbox').hide(0);
      $('.after').css({top: "12%"});
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play("S2_P1_1");
  		current_sound.play();
      current_sound.on('complete', function(){
        $('.dev,.devtext').animate({opacity : "1"},1000,"linear");
    		createjs.Sound.stop();
    		current_sound = createjs.Sound.play("S2_P1_2");
    		current_sound.play();
        current_sound.on('complete',function(){
          $('.maya,.mayatext').animate({opacity : "1"},1000,"linear");
      		createjs.Sound.stop();
      		current_sound = createjs.Sound.play("S2_P1_3");
      		current_sound.play();
          current_sound.on('complete',function(){
            $('.suri,.suritext').animate({opacity : "1"},1000,"linear");
        		createjs.Sound.stop();
        		current_sound = createjs.Sound.play("S2_P1_4");
        		current_sound.play();
            current_sound.on('complete', function(){
              $('.pema,.pematext').animate({opacity : "1"},1000,"linear");
          		createjs.Sound.stop();
          		current_sound = createjs.Sound.play("S2_P1_5");
          		current_sound.play();
              current_sound.on('complete', function(){
              $('.roti1,.roti2,.roti3,.roti4,.after').animate({opacity : "1"},1000,"linear");
              $('.dotted,.dotted1,.textbot').animate({opacity : "1"},1000,"linear");
          		createjs.Sound.stop();
          		current_sound = createjs.Sound.play("S2_P1_6");
          		current_sound.play();
              });
            });

          });

        });
      });
      $('.roti4').click(function(){
        $('.dotted,.dotted1,.textbot,.texttop').hide(0);
        $('.roti4').animate({left:"26%", top: "20%"},2000,"linear");
        $('.roti3').animate({left:"27%", top: "55%"},2000,"linear");
        $('.roti2').animate({left:"54%", top: "55%"},2000,"linear");
        $('.roti1').animate({left:"54%", top: "20%"},2000,"linear");
          setTimeout(function(){
              sound_player_seq(["sa","sb","sc","sd","s2_p1_Sec"],['.speechbox.sp-1','.speechbox.sp-4','.speechbox.sp-2','.speechbox.sp-3','.texttop1'],true);

          },2000);
      });

      break;
			case 1:
      sound_player("S2_P"+(countNext+1),1);
      $(".one,.two,.three,.four").css({opacity: "0"});
      $(".roti4").delay(500).animate({left: "39%", top: "31%"}, 1200, "linear");
      $(".roti1").delay(500).animate({left: "41%", top: "31%"}, 1200, "linear");
      $(".roti2").delay(500).animate({left: "41%", top: "33%"}, 1200, "linear");
  		$(".roti3").delay(500).animate({left: "39%", top: "33%"}, 1200, "linear");
      $(".one,.two,.three,.four").delay(1700).animate({opacity: "1"}, 1000, "linear");
			break;
      case 2:
          setTimeout(function(){
          sound_player("S2_P3",true);

      },2000);
          $(".roti4").css({left: "39%", top: "31%"});
      $(".roti1").css({left: "41%", top: "31%"});
      $(".roti2").css({left: "41%", top: "33%"});
  		$(".roti3").css({left: "39%", top: "33%"});
      $('.dotted,.dotted1').css({opacity:"0"});
      $(".roti4,.roti1,.roti3,.roti2").delay(500).animate({left: "40%", top: "32%"}, 1200, "linear");
      $('.dotted,.dotted1').delay(1700).animate({opacity: "1"}, 1000, "linear");
      $(".onehalf").delay(1700).animate({opacity: "1"}, 1000, "linear");
      $(".twohalf").delay(2700).animate({opacity: "1"}, 1000, "linear");
      $(".threehalf").delay(4700).animate({opacity: "1"}, 1000, "linear");
      $(".fourhalf").delay(3700).animate({opacity: "1"}, 1000, "linear");
      break;
      default:
      nav_button_controls(200);
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

  function sound_player(sound_id, nextBtnFlg){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nextBtnFlg?nav_button_controls():'';
		});
	}
    function sound_player_seq(soundarray, textarray,navflag){
        $(textarray[0]).show(0).css("opacity","1");
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        textarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                sound_player_seq(soundarray,textarray, navflag);
            }else{
                if(navflag)
                    nav_button_controls(0);
            }

        });
    }


    function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
});

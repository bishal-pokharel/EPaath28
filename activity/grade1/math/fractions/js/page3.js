var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	// slide0
  {
    contentblockadditionalclass:'diybg',
    extratextblock:[
    {
      textclass:'diytitle',
      textdata:data.string.diy
    }],
      imageblock:[{
          imagestoshow: [
              {
                  imgid: 'diyimg',
                  imgclass: 'diyimage'
              },
          ]
      }]
  },
  //slide1
  {
    contentblockadditionalclass:'workbg',
    extratextblock:[
    {
      splitintofractionsflag: true,
      textclass:'texttop1',
      textdata:data.string.p3s1
    },
    {
        splitintofractionsflag: true,
      textclass:'option1',
      textdata:data.string.p3s2
    },
    {
        splitintofractionsflag: true,
      textclass:'option2',
      textdata:data.string.p3s3
    },
    {
        splitintofractionsflag: true,
      textclass:'onep',
      textdata:data.string.p3s4
    },
    {
        splitintofractionsflag: true,
      textclass:'twop',
      textdata:data.string.p3s4
    },
    {
        splitintofractionsflag: true,
      textclass:'onehalf',
      textdata:data.string.p3s5
    },
    {
        splitintofractionsflag: true,
      textclass:'twohalf',
      textdata:data.string.p3s5
    },
    {
        splitintofractionsflag: true,
      textclass:'threehalf',
      textdata:data.string.p3s5
    },
    {
        splitintofractionsflag: true,
      textclass:'fourhalf',
      textdata:data.string.p3s5
    }],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'fullcircle',
          imgclass:'circleclass'
        },
        {
          imgid:'circle1',
          imgclass:'circleclass1'
        },
        {
          imgid:'circle2',
          imgclass:'circleclass2'
        },
        {
          imgid:'circle3',
          imgclass:'circleclass3'
        },
        {
          imgid:'circle4',
          imgclass:'circleclass4'
        },
        {
          imgid:'halfcircle1',
          imgclass:'halfcircle1class'
        },
        {
          imgid:'halfcircle2',
          imgclass:'halfcircle2class'
        },
        {
          imgid:'dotline',
          imgclass:'dotted'
        },
        {
          imgid:'dotline1',
          imgclass:'dotted1'
        }
    ]
    }]
  },
  //slide2
  {
    contentblockadditionalclass:'workbg',
    extratextblock:[
    {
        splitintofractionsflag: true,
        textclass:'texttop1',
      textdata:data.string.p3s1
    },
    {
        splitintofractionsflag: true,
        textclass:'option1',
      textdata:data.string.p3s2
    },
    {
        splitintofractionsflag: true,
        textclass:'option2',
      textdata:data.string.p3s3
    },
    {
        splitintofractionsflag: true,
        textclass:'onep',
      textdata:data.string.p3s4
    },
    {
        splitintofractionsflag: true,
        textclass:'twop',
      textdata:data.string.p3s4
    },
    {
        splitintofractionsflag: true,
        textclass:'onehalf',
      textdata:data.string.p3s5
    },
    {
        splitintofractionsflag: true,
        textclass:'twohalf',
      textdata:data.string.p3s5
    },
    {
        splitintofractionsflag: true,
        textclass:'threehalf',
      textdata:data.string.p3s5
    },
    {
        splitintofractionsflag: true,
        textclass:'fourhalf',
      textdata:data.string.p3s5
    }],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'fullrect',
          imgclass:'rectclass'
        },
        {
          imgid:'rect1',
          imgclass:'rectclass1'
        },
        {
          imgid:'rect2',
          imgclass:'rectclass2'
        },
        {
          imgid:'rect3',
          imgclass:'rectclass3'
        },
        {
          imgid:'rect4',
          imgclass:'rectclass4'
        },
        {
          imgid:'halfrect1',
          imgclass:'halfrect1class'
        },
        {
          imgid:'halfrect2',
          imgclass:'halfrect2class'
        },
        {
          imgid:'dotline',
          imgclass:'dotted'
        },
        {
          imgid:'dotline1',
          imgclass:'dotted1'
        }
    ]
    }]
  },
  {
    contentblockadditionalclass:'workbg',
    extratextblock:[
    {
        splitintofractionsflag: true,
        textclass:'texttop1',
      textdata:data.string.p3s1
    },
    {
        splitintofractionsflag: true,
        textclass:'option1', //correct
      textdata:data.string.p3s2
    },
    {
        splitintofractionsflag: true,
        textclass:'option2',
      textdata:data.string.p3s3
    }],
    imageblock:[{
      imagestoshow:[{
        imgid:'apple',
        imgclass:'appleclass'
      }]
    }]
  }
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	$('.mainBox > div').prepend('<p class="replay_button"></p>');

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
      {id: "fullcircle", src: imgpath+"full_circle.png", type: createjs.AbstractLoader.IMAGE},
      {id: "fullrect", src: imgpath+"full_rectangle.png", type: createjs.AbstractLoader.IMAGE},
      {id: "circle1", src: imgpath+"circle01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "circle2", src: imgpath+"circle02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "circle3", src: imgpath+"circle03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "circle4", src: imgpath+"circle04.png", type: createjs.AbstractLoader.IMAGE},
      {id: "halfcircle1", src: imgpath+"half_circle01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "halfcircle2", src: imgpath+"half_circle02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rect1", src: imgpath+"rectangle01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rect2", src: imgpath+"rectangle02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rect3", src: imgpath+"rectangle03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rect4", src: imgpath+"rectangle04.png", type: createjs.AbstractLoader.IMAGE},
      {id: "halfrect1", src: imgpath+"half_rectangle01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "halfrect2", src: imgpath+"half_rectangle02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "suri", src: imgpath+"suri.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dotline", src: imgpath+"black_doted_line01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dotline1", src: imgpath+"black_doted_line02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
      {id: "twoapple", src: imgpath+"apple_2_parts.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "fourapple", src: imgpath+"apple_4_parts.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "reverse2", src: imgpath+"reverse2.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "reverse4", src: imgpath+"reverse4.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "diyimg", src: "images/diy_bg/a_07.png", type: createjs.AbstractLoader.IMAGE},


			//sounds
      {id: "S3_P1", src: soundAsset+"s3_p1.ogg"},
      {id: "S3_P2", src: soundAsset+"s3_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound); //for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

      firstPagePlayTime(countNext);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        splitintofractions($board);
        vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

/*	var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +6.5+"%";
	$('.top_below_text').css({
		'top':top_val
		});
*/
		switch(countNext) {
      case 0:
          play_diy_audio();
         nav_button_controls(100);
      break;
			case 1:
      sound_player("S3_P2",0);
      $('.circleclass1,.circleclass2,.circleclass3,.circleclass4,.halfcircle1class,.halfcircle2class,.dotted,.dotted1,.onep,.twop,.onehalf,.twohalf,.threehalf,.fourhalf').css({opacity:"0"});
      $('.option1').click(function(){
    		createjs.Sound.stop();
        $('.option2,.option1').addClass("avoid_clicks");
        $('.circleclass').show(0);
        $('.circleclass1,.circleclass2,.circleclass3,.circleclass4,.halfcircle1class,.halfcircle2class,.dotted,.dotted1,.onep,.twop,.onehalf,.twohalf,.threehalf,.fourhalf').css({opacity:"0"});
        $('.circleclass').hide(0);

        $('.halfcircle1class').animate({opacity:"1", left: "16%"},1500,"linear");
        $('.halfcircle1class').animate({left: "17%"},1500,"linear");
        $('.halfcircle2class').animate({opacity:"1", left: "18%"},1500,"linear");
        $('.halfcircle2class').animate({left: "17%"},1500,"linear");

        $('.dotted').css({opacity: "1", height: "0%"},2000,"linear");
        $('.dotted').animate({height: "53%"},2000,"linear");

        $('.onep').delay(3800).animate({opacity:"1"},1000,"linear");
        $('.twop').delay(4800).animate({opacity:"1"},1000,"linear");
        setTimeout(function(){
            $('.option2,.option1').removeClass("avoid_clicks");
        },6000);
        nav_button_controls(3000);

      });

      $('.option2').click(function(){
    		createjs.Sound.stop();
        $('.option1,.option2').addClass("avoid_clicks");
        $('.circleclass').show(0);
        $('.circleclass1,.circleclass2,.circleclass3,.circleclass4,.halfcircle1class,.halfcircle2class,.dotted,.dotted1,.onep,.twop,.onehalf,.twohalf,.threehalf,.fourhalf').css({opacity:"0"});
        $('.circleclass').hide(0);

        $('.circleclass1').animate({opacity:"1", left: "18%", top:"22%"},1500,"linear");
        $('.circleclass1').animate({left: "17%", top:"23%"},1500,"linear");
        $('.circleclass2').animate({opacity:"1", left: "18%", top:"24%"},1500,"linear");
        $('.circleclass2').animate({left: "17%", top:"23%"},1500,"linear");
        $('.circleclass3').animate({opacity:"1", left: "16%", top:"24%"},1500,"linear");
        $('.circleclass3').animate({left: "17%", top:"23%"},1500,"linear");
        $('.circleclass4').animate({opacity:"1", left: "16%", top:"22%"},1500,"linear");
        $('.circleclass4').animate({left: "17%", top:"23%"},1500,"linear");

        $('.dotted').css({opacity: "1", height: "0%"});
        $('.dotted1').css({opacity: "1", width: "0%"});
        $('.dotted').animate({height: "53%"},2000,"linear");
        $('.dotted1').animate({width: "31%"},2000,"linear");

        $('.onehalf').delay(3800).animate({opacity:"1"},1000,"linear");
        $('.twohalf').delay(4800).animate({opacity:"1"},1000,"linear");
        $('.threehalf').delay(6800).animate({opacity:"1"},1000,"linear");
        $('.fourhalf').delay(5800).animate({opacity:"1"},1000,"linear");
          setTimeout(function(){
              $('.option2,.option1').removeClass("avoid_clicks");
          },8500);
          nav_button_controls(3000);
      });
      break;
      case 2:
          $(".onep").css({"left":"0%","top":"39%"});
          $(".twop").css({"left":"26%","top":"39%"});
      $('.rectclass1,.rectclass2,.rectclass3,.rectclass4,.halfrect1class,.halfrect2class,.dotted,.dotted1,.onep,.twop,.onehalf,.twohalf,.threehalf,.fourhalf').css({opacity:"0"});
      $('.option1').click(function(){
    		createjs.Sound.stop();
          $('.option1,.option2').addClass("avoid_clicks");
        $('.rectclass').show(0);
        $('.rectclass1,.rectclass2,.rectclass3,.rectclass4,.halfrect1class,.halfrect2class,.dotted,.dotted1,.onep,.twop,.onehalf,.twohalf,.threehalf,.fourhalf').css({opacity:"0"});
        $('.rectclass').hide(0);

        $('.halfrect1class').animate({opacity:"1", left: "7%"},1500,"linear");
        $('.halfrect1class').animate({left: "7.75%"},1500,"linear");
        $('.halfrect2class').animate({opacity:"1", left: "9%"},1500,"linear");
        $('.halfrect2class').animate({left: "7.75%"},1500,"linear");

        $('.dotted').css({opacity: "1", height: "0%"},2000,"linear");
        $('.dotted').animate({height: "53%"},2000,"linear");

        $('.onep').delay(3800).animate({opacity:"1"},1000,"linear");
        $('.twop').delay(4800).animate({opacity:"1"},1000,"linear");
          setTimeout(function(){
              $('.option2,.option1').removeClass("avoid_clicks");
          },6000);
          nav_button_controls(3000);
      });

      $('.option2').click(function(){
    		createjs.Sound.stop();
          $('.option1,.option2').addClass("avoid_clicks");
        $('.rectclass').show(0);
        $('.rectclass1,.rectclass2,.rectclass3,.rectclass4,.halfrect1class,.halfrect2class,.dotted,.dotted1,.onep,.twop,.onehalf,.twohalf,.threehalf,.fourhalf').css({opacity:"0"});
        $('.rectclass').hide(0);
        $('.rectclass1').animate({opacity:"1", left: "8.75%", top:"22%"},1500,"linear");
        $('.rectclass1').animate({left: "7.75%", top:"23%"},1500,"linear");
        $('.rectclass2').animate({opacity:"1", left: "8.75%", top:"24%"},1500,"linear");
        $('.rectclass2').animate({left: "7.75%", top:"23%"},1500,"linear");
        $('.rectclass3').animate({opacity:"1", left: "6.75%", top:"24%"},1500,"linear");
        $('.rectclass3').animate({left: "7.75%", top:"23%"},1500,"linear");
        $('.rectclass4').animate({opacity:"1", left: "6.75%", top:"22%"},1500,"linear");
        $('.rectclass4').animate({left: "7.75%", top:"23%"},1500,"linear");

        $('.dotted').css({opacity: "1", height: "0%"});
        $('.dotted1').css({opacity: "1", width: "0%", left: "7%"});
        $('.dotted').animate({height: "53%"},2000,"linear");
        $('.dotted1').animate({width: "51%"},2000,"linear");

        $('.onehalf').delay(3800).animate({opacity:"1"},1000,"linear");
        $('.twohalf').delay(4800).animate({opacity:"1"},1000,"linear");
        $('.threehalf').delay(6800).animate({opacity:"1"},1000,"linear");
        $('.fourhalf').delay(5800).animate({opacity:"1"},1000,"linear");
          setTimeout(function(){
              $('.option2,.option1').removeClass("avoid_clicks");
          },8500);
          nav_button_controls(3000);
      });
      break;
      case 3:
      $('.option1').click(function(){
    		createjs.Sound.stop();
          $('.option1,.option2').addClass("avoid_clicks");
        $('.appleclass').attr('src',preload.getResult('twoapple').src);
          setTimeout(function(){
              $('.option2,.option1').removeClass("avoid_clicks");
          },3000);
          nav_button_controls(3000);

      //  $('.appleclass').delay(3000).attr('src',preload.getResult('reverse2').src);
      });
      $('.option2').click(function(){
    		createjs.Sound.stop();
          $('.option1,.option2').addClass("avoid_clicks");
        $('.appleclass').attr('src',preload.getResult('fourapple').src);
          setTimeout(function(){
              $('.option2,.option1').removeClass("avoid_clicks");
          },3000);
          nav_button_controls(3000);

    //    $('.appleclass').dealy(3000).attr('src',preload.getResult('reverse4').src);
      });
      break;
      default:
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

  function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
    });
  }

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
});

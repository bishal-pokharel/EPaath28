var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	// slide0
	{
		contentblockadditionalclass:'coverbg',
    extratextblock:[{
			textclass:'chapter_title',
			textdata:data.lesson.chapter
		}]
	},
	// slide1
	{
		contentblockadditionalclass:'grassbg',
    speechbox:[{
  		speechbox: 'sp-1',
  		textclass: "answer",
  		textdata: data.string.p1s1,
  		imgclass: '',
  		imgid : 'textbox',
  		imgsrc: '',
  	}],
		extratextblock:[{
			textclass:'answer1',
			textdata:data.string.p1s2
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
      {
				imgid:'gopal1',
				imgclass:'gopalclass'
			}]
		}]
	},
	// slide2
	{
		contentblockadditionalclass:'grassbg',
		speechbox:[{
			speechbox: 'sp-2',
			textclass: "answer2",
			textdata: data.string.p1s3,
		//	datahighlightflag : true,
		//	datahighlightcustomclass : 'fillimage',
			imgclass: '',
			imgid : 'textbox',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri2',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'rotifull',
				imgclass:'roticlass'
			}]
		}],
        extratextblock:[
        	{
				textclass:'name suriname',
				textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }
        ],
	},
	//slide3
	{
		contentblockadditionalclass:'grassbg',
		// speechbox:[{
		// 	speechbox: 'sp-2',
		// 	textclass: "answer2",
		// 	textdata: data.string.p1s3,
		// //	datahighlightflag : true,
		// //	datahighlightcustomclass : 'fillimage',
		// 	imgclass: '',
		// 	imgid : 'textbox',
		// 	imgsrc: '',
		// }],
		extratextblock:[{
			textclass:'textbot textbotanim',
			textdata:data.string.p1s4
		},
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }
		],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'roti40',
				imgclass:'roti40class'
			},
			{
				imgid:'roti60',
				imgclass:'roti60class'
			}]
		}]
	},
	//slide4
	{
		contentblockadditionalclass:'grassbg',
		extratextblock:[{
			textclass:'texttop',
			textdata:data.string.p1s5
		},
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }
		],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'gopalsad',
				imgclass:'gopalclass'
			},
			{
				imgid:'roti40',
				imgclass:'roti40class'
			},
			{
				imgid:'roti60',
				imgclass:'roti60class'
			}]
		}]
	},
	//slide5
	{
		contentblockadditionalclass:'grassbg',
		speechbox:[{
			speechbox: 'sp-2',
			textclass: "answer2",
			textdata: data.string.p1s6,
		//	datahighlightflag : true,
		//	datahighlightcustomclass : 'fillimage',
			imgclass: '',
			imgid : 'textbox',
			imgsrc: '',
		}],
		extratextblock:[{
			textclass:'textbot textbotanim',
			textdata:data.string.p1s4
		},
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'roti40',
				imgclass:'roti40class'
			},
			{
				imgid:'roti60',
				imgclass:'roti60class'
			}]
		}]
	},
	//slide6
	{
		contentblockadditionalclass:'grassbg',
		extratextblock:[{
			textclass:'texttop',
			textdata:data.string.p1s7
		},
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }],
		imageblock:[{
			imagestoshow:[{
				imgid:'surisad',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'roti40',
				imgclass:'roti40class'
			},
			{
				imgid:'roti60',
				imgclass:'roti60class'
			}]
		}]
	},
	//slide7
	{
		contentblockadditionalclass:'grassbg',
		speechbox:[{
			speechbox: 'sp-2',
			textclass: "answer2",
			textdata: data.string.p1s8,
			datahighlightflag : true,
			datahighlightcustomclass : 'purple',
			imgclass: '',
			imgid : 'textbox',
			imgsrc: '',
		}],
		extratextblock:[{
			textclass:'textbot textbotanim',
			textdata:data.string.p1s4
		},
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			},
			{
				imgid:'rotihalf1',
				imgclass:'rotihalfr'
			}]
		}]
	},
	//slide8
	{
		contentblockadditionalclass:'grassbg',
		extratextblock:[{
			textclass:'texttop',
			textdata:data.string.p1s9,
			datahighlightflag : true,
			datahighlightcustomclass : 'purple',
		},
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			},
			{
				imgid:'rotihalf1',
				imgclass:'rotihalfr'
			}]
		}]
	},
	//slide9
	{
		contentblockadditionalclass:'grassbg',
		extratextblock:[{
			textclass:'texttop',
			textdata:data.string.p1s10,
		},
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			},
			{
				imgid:'rotihalf1',
				imgclass:'rotihalfr'
			}]
		}]
	},
	//slide10
	{
		contentblockadditionalclass:'grassbg',
		extratextblock:[
            {
                textclass:'name suriname',
                textdata:data.string.suri
            }
		],
		speechbox:[{
  		speechbox: 'sp-3',
  		textclass: "answer",
  		textdata: data.string.p1s12,
			datahighlightflag : true,
			datahighlightcustomclass : 'purple',
  		imgclass: '',
  		imgid : 'textbox',
  		imgsrc: '',
  	}],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			}]
		}]
	},
	//slide11
	{
		contentblockadditionalclass:'grassbg',
		extratextblock:[
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }],
		speechbox:[{
  		speechbox: 'sp-3',
  		textclass: "answer",
  		textdata: data.string.p1s12,
			datahighlightflag : true,
			datahighlightcustomclass : 'purple',
  		imgclass: '',
  		imgid : 'textbox',
  		imgsrc: '',
  	},
		{
			speechbox: 'sp-4',
			textclass: "answerr",
			textdata: data.string.p1s13,
			datahighlightflag : true,
			datahighlightcustomclass : 'purple',
			imgclass: '',
			imgid : 'textboxf',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'rotihalf1',
				imgclass:'rotihalfr'
			},
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			}]
		}]
	},
	//slide12
	{
		contentblockadditionalclass:'grassbg',
		extratextblock:[
            {
                textclass:'name suriname',
                textdata:data.string.suri
            }
            ],
		speechbox:[{
			speechbox: 'sp-3',
			textclass: "answer",
			textdata: data.string.p1s14,
            splitintofractionsflag: true,
            datahighlightflag : true,
			datahighlightcustomclass : 'purple',
			imgclass: '',
			imgid : 'textbox',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			}]
		}]
	},
	//slide13
	{
		contentblockadditionalclass:'grassbg',
		extratextblock:[,
            {
                textclass:'name suriname',
                textdata:data.string.suri
            },
            {
                textclass:'name gopalname',
                textdata:data.string.gopal
            }],
		speechbox:[{
            splitintofractionsflag: true,
            speechbox: 'sp-3',
			textclass: "answer",
			textdata: data.string.p1s16,
			datahighlightflag : true,
			datahighlightcustomclass : 'purple',
			imgclass: '',
			imgid : 'textbox',
			imgsrc: '',
		},
		{
            splitintofractionsflag: true,
            speechbox: 'sp-4',
			textclass: "answerr",
			textdata: data.string.p1s15,
			datahighlightflag : true,
			datahighlightcustomclass : 'purple',
			imgclass: '',
			imgid : 'textboxf',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgid:'suri1',
				imgclass:'suriclass'
			},
			{
				imgid:'gopal1',
				imgclass:'gopalclass'
			},
			{
				imgid:'rotihalf1',
				imgclass:'rotihalfr'
			},
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			}]
		}]
	},
	//slide14
	{
		contentblockadditionalclass:'plainbluebg',
		extratextblock:[{
			textclass:'infobot',
			textdata:data.string.p1s17,
			datahighlightflag : true,
			datahighlightcustomclass : 'purple',
		},
		{
			textclass:'one',
			textdata:data.string.one,
		},
		{
			textclass:'two',
			textdata:data.string.two,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			},
			{
				imgid:'rotihalf1',
				imgclass:'rotihalfr'
			}]
		}]
	},
	//slide15
	{
		contentblockadditionalclass:'plainbluebg',
		extratextblock:[
		{
            splitintofractionsflag: true,
            textclass:'onehalf',
			textdata:data.string.p1s18,
		},
		{
            splitintofractionsflag: true,
            textclass:'twohalf',
			textdata:data.string.p1s18,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			},
			{
				imgid:'dotline',
				imgclass:'dotted'
			},
			{
				imgid:'rotihalf1',
				imgclass:'rotihalfr'
			}]
		}]
	},
	//slide16
	{
		contentblockadditionalclass:'plainbluebg',
		extratextblock:[
		{
			textclass:'label1',
			textdata:data.string.p1s19,
		},
		{
            splitintofractionsflag: true,
            textclass:'label2',
			textdata:data.string.p1s20,
		},
		{
            splitintofractionsflag: true,
            textclass:'label3',
			textdata:data.string.p1s20,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgid:'rotihalf',
				imgclass:'rotihalfl'
			},
			{
				imgid:'rotihalf1',
				imgclass:'rotihalfr'
			},
			{
				imgid:'rotifull',
				imgclass:'roticlass'
			}]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	$('.mainBox > div').prepend('<p class="replay_button"></p>');

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
		{id: "textbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
		{id: "textboxf", src: imgpath+"text_box_flip.png", type: createjs.AbstractLoader.IMAGE},
		{id: "suri1", src: imgpath+"suri01.png", type: createjs.AbstractLoader.IMAGE},
		{id: "suri2", src: imgpath+"suri03.png", type: createjs.AbstractLoader.IMAGE},
		{id: "gopal1", src: imgpath+"gopal.png", type: createjs.AbstractLoader.IMAGE},
		{id: "surisad", src: imgpath+"surisad.png", type: createjs.AbstractLoader.IMAGE},
		{id: "gopalsad", src: imgpath+"gopalsad.png", type: createjs.AbstractLoader.IMAGE},
		{id: "rotifull", src: imgpath+"roti01.png", type: createjs.AbstractLoader.IMAGE},
		{id: "rotihalf", src: imgpath+"rotihalf.png", type: createjs.AbstractLoader.IMAGE},
		{id: "rotihalf1", src: imgpath+"rotihalf01.png", type: createjs.AbstractLoader.IMAGE},
		{id: "roti40", src: imgpath+"roti40.png", type: createjs.AbstractLoader.IMAGE},
		{id: "roti60", src: imgpath+"roti60.png", type: createjs.AbstractLoader.IMAGE},
		{id: "dotline", src: imgpath+"black_doted_line01.png", type: createjs.AbstractLoader.IMAGE},
		//{id: "textbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE}

		// sounds
		{id: "S1_P1", src: soundAsset+"s1_p1.ogg"},
		{id: "S1_P2", src: soundAsset+"s1_p2.ogg"},
		{id: "S1_P3", src: soundAsset+"s1_p3.ogg"},
		{id: "S1_P4", src: soundAsset+"s1_p4.ogg"},
		{id: "S1_P5", src: soundAsset+"s1_p5.ogg"},
		{id: "S1_P6", src: soundAsset+"s1_p6.ogg"},
		{id: "S1_P7", src: soundAsset+"s1_p7.ogg"},
		{id: "S1_P8", src: soundAsset+"s1_p8.ogg"},
		{id: "S1_P9", src: soundAsset+"s1_p9.ogg"},
		{id: "S1_P10", src: soundAsset+"s1_p10.ogg"},
		{id: "S1_P11", src: soundAsset+"s1_p11.ogg"},
		{id: "S1_P12", src: soundAsset+"s1_p12.ogg"},
		{id: "S1_P12_1", src: soundAsset+"s1_p12_1.ogg"},
		{id: "S1_P13", src: soundAsset+"s1_p13.ogg"},
		{id: "S1_P14", src: soundAsset+"s1_p14.ogg"},
		{id: "S1_P14_1", src: soundAsset+"s1_p14_1.ogg"},
		{id: "S1_P15", src: soundAsset+"s1_p15.ogg"},
		{id: "S1_P16", src: soundAsset+"s1_p16.ogg"},
		{id: "S1_P17", src: soundAsset+"s1_p17.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

    firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        splitintofractions($board);
        vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

/*	var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +6.5+"%";
	$('.top_below_text').css({
		'top':top_val
		});
*/
		switch(countNext) {
			case 0:
				sound_player("S1_P"+(countNext+1),1);
			break;
			case 1:
			$('.gopalclass').hide(0);
			$('.answer1').hide(0);
				sound_player("S1_P"+(countNext+1),1);
			//after audio
			$('.gopalclass').delay(3200).fadeIn(1300);
			$('.answer1').delay(4000).fadeIn(1000);
			break;
			case 2:
			$('.suriname').css({"left":"16%"});
				sound_player("S1_P"+(countNext+1),1);
			break;
			case 3:
			sound_player("S1_P"+(countNext+1),0);
			$('.roti40class,.roti60class').click(function(){
                $(".textbot").hide();
				$('.roti40class').css('animation','none');
				$('.roti60class').css('animation','none');
    		    $(".roti40class").animate({left: "70%", top:"20%"}, 3000, "swing");
				$(".roti60class").animate({left: "7%", top:"13%"}, 3000, "swing");
				nav_button_controls(3500);
			});
			break;
			case 4:
			sound_player("S1_P"+(countNext+1),1);
			$(".roti40class").css({left: "70%", top:"20%"});
			$(".roti60class").css({left: "7%", top:"13%"});
			$(".roti60class").css('animation','none');
			break;
			case 5:
			sound_player("S1_P"+(countNext+1),0);
			$('.roti40class,.roti60class').click(function(){
                $(".textbot").hide();
                $('.roti40class').css('animation','none');
				$('.roti60class').css('animation','none');
    		    $(".roti60class").animate({left: "75%", top:"14%"}, 3000, "swing");
				$(".roti40class").animate({left: "3%", top:"15%"}, 3000, "swing");
				nav_button_controls(3500);
			});
			break;
			case 6:
			sound_player("S1_P"+(countNext+1),1);
			$(".roti60class").css({left: "75%", top:"14%"});
			$(".roti40class").css({left: "3%", top:"15%"});
			$(".roti60class").css('animation','none');
			$('.suriname').css({"left":"16%"});
			break;
			case 7:
			sound_player("S1_P"+(countNext+1),0);
			$('.rotihalfl,.rotihalfr').click(function(){
                $(".textbot").hide();
                $('.rotihalfl').css('animation','none');
				$('.rotihalfr').css('animation','none');
    		    $(".rotihalfr").animate({left: "73%", top:"14%"}, 3000, "swing");
				$(".rotihalfl").animate({left: "8%", top:"14%"}, 3000, "swing");
				nav_button_controls(3500);
			});
			break;
			case 8:
			sound_player("S1_P"+(countNext+1),1);
			$(".rotihalfr").css({left: "73%", top:"14%"});
			$(".rotihalfl").css({left: "8%", top:"14%"});
			break;
			case 9:
			sound_player("S1_P"+(countNext+1),1);
			$(".rotihalfr").css({left: "73%", top:"14%"});
			$(".rotihalfl").css({left: "8%", top:"14%"});
			break;
			case 10:
			sound_player("S1_P"+(countNext+1),1);
			$(".rotihalfl").css({left: "16%", top:"14%"});
			$(".suriclass").css({left: "15%"});
			$(".speechbox.sp-3").css({left: "33%","top":"34%"});
			$('.suriname').css({"left":"19%"});

			break;
			case 11:
			sound_player("S1_P"+(countNext+1)+"_1",1);
			$(".rotihalfr").css({left: "73%", top:"14%"});
			$(".rotihalfl").css({left: "8%", top:"14%"});
			$(".gopalclass").css({opacity: "0", animation: "fadein 1s forwards"});
			$(".suriclass").css({opacity: "0", animation: "fadein 1s forwards"});
			break;
			case 12:
			sound_player("S1_P"+(countNext+1),1);
			$(".rotihalfl").css({left: "16%", top:"14%"});
			$(".suriclass").css({left: "15%"});
			$(".speechbox.sp-3").css({left: "33%","height":"24%","width":"45%"});
			$('.suriname').css({"left":"19%"});

			break;
			case 13:
			sound_player_seq("S1_P"+(countNext+1),"S1_P"+(countNext+1)+"_1",1);
			$(".rotihalfr").css({left: "73%", top:"14%"});
			$(".rotihalfl").css({left: "8%", top:"14%"});
			$(".gopalclass").css({opacity: "0", animation: "fadein 1s forwards"});
			$(".suriclass").css({opacity: "0", animation: "fadein 1s forwards"});
            $(".answerr").css("top","13%");
            $(".answer").css("top","38%");
			break;
			case 14:
			sound_player("S1_P"+(countNext+1),1);
			$(".rotihalfr,.rotihalfl").css({animation:"none"});
			$(".rotihalfr").delay(500).animate({left: "43%"}, 1200, "linear");
			$(".rotihalfl").delay(500).animate({left: "39%"}, 1200, "linear");
			$(".one,.two").delay(2000).animate({opacity: "1"}, 1000, "linear");
			break;
			case 15:
			sound_player("S1_P"+(countNext+1),1);
			$(".rotihalfr,.rotihalfl").css({animation:"none"});
			$(".rotihalfr").css({left: "43%"});
			$(".rotihalfl").css({left: "39%"});
			$(".rotihalfr").animate({left: "41%"}, 1200, "linear");
			$(".rotihalfl").animate({left: "41%"}, 1200, "linear");
			$(".onehalf").delay(1200).animate({opacity: "1"}, 1000, "linear");
			$(".twohalf").delay(2200).animate({opacity: "1"}, 1000, "linear");
			break;
			case 16:
			sound_player("S1_P"+(countNext+1),1);
			$(".rotihalfr,.rotihalfl").css({left: "10%", top: "40%", animation: "none", opacity: "0"});
			$(".roticlass").animate({left: "10%", top: "40%"},1200,"linear");
			$(".label1").delay(1000).animate({opacity: "1"}, 1000, "linear");
			$(".rotihalfl").delay(2000).animate({left: "46%",opacity: "1"},1200,"linear");
			$(".label2").delay(3000).animate({opacity: "1"}, 1000, "linear");
			$(".rotihalfr").delay(4000).animate({left: "60%",opacity: "1"},1200,"linear");
			$(".label3").delay(5000).animate({opacity: "1"}, 1000, "linear");
			default:
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

  function sound_player(sound_id, nextbtnFlg){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nextbtnFlg?nav_button_controls():'';
		});
	}
    function sound_player_seq(sound1,sound2, nextbtnFlg){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound1);
        current_sound.play();
        current_sound.on('complete', function(){
            current_sound = createjs.Sound.play(sound2);
            current_sound.play();
            current_sound.on('complete', function() {
                nextbtnFlg ? nav_button_controls() : '';
            });
        });
    }

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }

});

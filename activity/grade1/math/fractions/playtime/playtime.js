var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
			contentnocenteradjust: true,
			extratextblock: [
					{
							textclass: "playtime-text",
							textdata: data.string.playtime
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "bg1",
									imgid: "bg1",
									imgsrc: "",
							},
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							}]
			}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
                    splitintofractionsflag: true,
                    textclass: "texttop",
						textdata: data.string.e1s1
				}
		],
		imageblock:[{
				imagestoshow: [
						{
								imgclass: "suriclass",
								imgid: "suri",
								imgsrc: "",
						}
					]
		}],
		fillblock:[
			{
                splitintofractionsflag: true,
                fillclass:'onehalf',
				textdata: data.string.p1s18
			},
			{
                splitintofractionsflag: true,
                fillclass:'twohalf',
				textdata: data.string.p1s18
			}
		]
	},
	//slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
						textclass: "texttop",
						textdata: data.string.e1s2
				},
				{
						textclass: "bottext",
						textdata: data.string.e1s5,
						datahighlightflag : true,
						datahighlightcustomclass : 'colors',
				}
		],
		imageblock:[{
				imagestoshow: [
						{
								imgclass: "suriclass",
								imgid: "suri1",
								imgsrc: "",
						}
					]
		}],
		colorblock:[
			{
			colorclass:'yellow'
			},
			{
			colorclass:'green'
			},
			{
			colorclass:'purple'
			},
			{
			colorclass:'brown'
			},
			{
			colorclass:'red'
			},
			{
			colorclass:'blue'
			},
		],
		fillblock:[
			{
                splitintofractionsflag: true,
                fillclass:'onehalf',
				textdata: data.string.p1s18
			},
			{
                splitintofractionsflag: true,
                fillclass:'twohalf',
				textdata: data.string.p1s18
			}
		]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
						textclass: "texttop",
						textdata: data.string.e1s4
				}
		],
		imageblock:[{
				imagestoshow: [
						{
								imgclass: "suriclass",
								imgid: "suri",
								imgsrc: "",
						}
					]
		}],
		fillblock:[
			{
                splitintofractionsflag: true,

                fillclass:'onequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'twoquat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'threequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'fourquat',
				textdata: data.string.p3s5
			}
		]
	},
	//slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
						textclass: "texttop",
						textdata: data.string.e1s2
				},
				{
						textclass: "bottext",
						textdata: data.string.e1s6,
						datahighlightflag : true,
						datahighlightcustomclass : 'colors',
				}
		],
		imageblock:[{
				imagestoshow: [
						{
								imgclass: "suriclass",
								imgid: "suri1",
								imgsrc: "",
						}
					]
		}],
		colorblock:[
			{
			colorclass:'yellow'
			},
			{
			colorclass:'green'
			},
			{
			colorclass:'purple'
			},
			{
			colorclass:'brown'
			},
			{
			colorclass:'red'
			},
			{
			colorclass:'blue'
			},
		],
		fillblock:[
			{
                splitintofractionsflag: true,

                fillclass:'onequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'twoquat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'threequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'fourquat',
				textdata: data.string.p3s5
			}
		]
	},
	//slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
                    splitintofractionsflag: true,

                    textclass: "texttop texttopp",
						textdata: data.string.e1s10
				},
				{
                    splitintofractionsflag: true,

                    textclass:'option1',
					textdata:data.string.p3s2
				},
				{
                    splitintofractionsflag: true,

                    textclass:'option2',
					textdata:data.string.p3s3
				},
			],
		fillblock:[
				{
                    splitintofractionsflag: true,

                    fillclass:'onehalf',
					textdata: data.string.p1s18
				},
				{
                    splitintofractionsflag: true,

                    fillclass:'twohalf',
					textdata: data.string.p1s18
				}
		],
	},
	//slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
						textclass: "texttop",
						textdata: data.string.e1s2
				},
				{
						textclass: "bottext",
						textdata: data.string.e1s5,
						datahighlightflag : true,
						datahighlightcustomclass : 'colors',
				}
		],
		colorblock:[
			{
			colorclass:'yellow'
			},
			{
			colorclass:'green'
			},
			{
			colorclass:'purple'
			},
			{
			colorclass:'brown'
			},
			{
			colorclass:'red'
			},
			{
			colorclass:'blue'
			},
		],
		fillblock:[
			{
                splitintofractionsflag: true,

                fillclass:'onehalf',
				textdata: data.string.p1s18
			},
			{
                splitintofractionsflag: true,

                fillclass:'twohalf',
				textdata: data.string.p1s18
			}
		]
	},
	//slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
                    splitintofractionsflag: true,
                    textclass: "texttop texttopp",
						textdata: data.string.e1s11
				}
		],
		fillblock:[
			{
                splitintofractionsflag: true,

                fillclass:'onehalf',
				textdata: data.string.p1s18
			},
			{
                splitintofractionsflag: true,

                fillclass:'twohalf',
				textdata: data.string.p1s18
			},
			{
                splitintofractionsflag: true,

                fillclass:'onequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'twoquat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'threequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'fourquat',
				textdata: data.string.p3s5
			}
		]
	},
	//slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
						textclass: "texttop",
						textdata: data.string.e1s2
				},
				{
						textclass: "bottext",
						textdata: data.string.e1s6,
						datahighlightflag : true,
						datahighlightcustomclass : 'colors',
				}
		],
		colorblock:[
			{
			colorclass:'yellow'
			},
			{
			colorclass:'green'
			},
			{
			colorclass:'purple'
			},
			{
			colorclass:'brown'
			},
			{
			colorclass:'red'
			},
			{
			colorclass:'blue'
			},
		],
		fillblock:[
			{
                splitintofractionsflag: true,

                fillclass:'onequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'twoquat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'threequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'fourquat',
				textdata: data.string.p3s5
			},
			// {
             //    splitintofractionsflag: true,
            //
             //    fillclass:'onehalf',
			// 	textdata: data.string.p1s18
			// },
			// {
             //    splitintofractionsflag: true,
            //
             //    fillclass:'twohalf',
			// 	textdata: data.string.p1s18
			// }
		]
	},
	//slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
                    splitintofractionsflag: true,
                    textclass: "texttop texttopp",
						textdata: data.string.e1s12
				}
		],
		fillblock:[
			{
                splitintofractionsflag: true,

                fillclass:'onehalf',
				textdata: data.string.p1s18
			},
			{
                splitintofractionsflag: true,

                fillclass:'twohalf',
				textdata: data.string.p1s18
			},
			{
                splitintofractionsflag: true,

                fillclass:'onequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'twoquat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'threequat',
				textdata: data.string.p3s5
			},
			{
                splitintofractionsflag: true,

                fillclass:'fourquat',
				textdata: data.string.p3s5
			}
		]
	},
	//slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock: [
				{
                    splitintofractionsflag: true,
                    textclass: "texttop",
						textdata: data.string.e1s2
				},
				{
						textclass: "bottext",
						textdata: data.string.e1s5,
						datahighlightflag : true,
						datahighlightcustomclass : 'colors',
				}
		],
		colorblock:[
			{
			colorclass:'yellow'
			},
			{
			colorclass:'green'
			},
			{
			colorclass:'purple'
			},
			{
			colorclass:'brown'
			},
			{
			colorclass:'red'
			},
			{
			colorclass:'blue'
			},
		],
		fillblock:[
			// {
             //    splitintofractionsflag: true,
            //
             //    fillclass:'onequat',
			// 	textdata: data.string.p3s5
			// },
			// {
             //    splitintofractionsflag: true,
            //
             //    fillclass:'twoquat',
			// 	textdata: data.string.p3s5
			// },
			// {
             //    splitintofractionsflag: true,
            //
             //    fillclass:'threequat',
			// 	textdata: data.string.p3s5
			// },
			// {
             //    splitintofractionsflag: true,
            //
             //    fillclass:'fourquat',
			// 	textdata: data.string.p3s5
			// },
			{
                splitintofractionsflag: true,

                fillclass:'onehalf',
				textdata: data.string.p1s18
			},
			{
                splitintofractionsflag: true,

                fillclass:'twohalf',
				textdata: data.string.p1s18
			}
		]
	},
	//slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "plainpurplebg",
		extratextblock: [
				{
						textclass: "texttop2",
						textdata: data.string.e1s13
				},
				{
						textclass: "lastbut1",
						textdata: data.string.playagain,
				},
				{
						textclass: "lastbut2",
						textdata: data.string.mainmenu,
				},
				{
						textclass: "lastbut3",
						textdata: data.string.learnagain,
				}
		],
		imageblock:[{
				imagestoshow: [
					{
						imgclass: "suriclass3",
						imgid: "surilast",
						imgsrc: "",
					},
					{
						imgclass: "suriclass4",
						imgid: "surilast2",
						imgsrc: "",
					}
				]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "suri", src: imgpath+"gopal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suri1", src: imgpath+"gopalgrt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "surilast", src: imgpath+"surilast.png", type: createjs.AbstractLoader.IMAGE},
			{id: "surilast2", src: imgpath+"surilast2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: imgpath+"wrong.png", type: createjs.AbstractLoader.IMAGE},

  		// sounds
			{id: "ex1_2", src: soundAsset+"ex1_2.ogg"},
			{id: "ex1_3", src: soundAsset+"ex1_3.ogg"},
			{id: "ex1_4", src: soundAsset+"ex1_4.ogg"},
			{id: "ex1_5", src: soundAsset+"ex1_5.ogg"},
			{id: "ex1_6", src: soundAsset+"ex1_6.ogg"},
			{id: "ex1_7", src: soundAsset+"ex1_7.ogg"},
			{id: "ex1_8", src: soundAsset+"ex1_8.ogg"},
			{id: "ex1_9", src: soundAsset+"ex1_9.ogg"},
			{id: "ex1_10", src: soundAsset+"ex1_10.ogg"},
			{id: "ex1_11", src: soundAsset+"ex1_11.ogg"},
			{id: "ex1_12", src: soundAsset+"ex1_12.ogg"},

			{id: "blue", src: soundAsset+"ex1(blue).ogg"},
			{id: "brown", src: soundAsset+"ex1(brown).ogg"},
			{id: "green", src: soundAsset+"ex1(green).ogg"},
			{id: "orange", src: soundAsset+"ex1(orange).ogg"},
			{id: "purple", src: soundAsset+"ex1(purple).ogg"},
      		{id: "red", src: soundAsset+"ex1(red).ogg"},

            {id: "sound_ting", src: "sounds/common/grade1/correct.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	fillChooser=0;

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        splitintofractions($board);
        put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			nav_button_controls(100);
			break;
			case 1:
				sound_player("ex1_"+(countNext+1));
			$('.onehalf').click(function(){
				current_sound.stop();
				sound_player("sound_ting");
				$(this).css({"background-color" : "#002c6a"});
				$('.twohalf').off('click').css({"user-select":"none"});
				fillChooser = 1;
				nav_button_controls(200);
			});
			$('.twohalf').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				$(this).css({"background-color" : "#002c6a"});
				$('.onehalf').off('click').css({"user-select":"none"});
				fillChooser = 2;
				nav_button_controls(200);
			});
			break;
			case 2:
				sound_player("ex1_"+(countNext+1));
				sound_player("ex1_"+(countNext+1));
                $('.onehalf,.twohalf').css({"cursor":"default"});
                $('.suriclass').css({"width":"17%"});
			$('.bottext').css({opacity:"0"});
			$('.colorblock').addClass('slidefromtop');
			if(fillChooser==1){
				$('.onehalf').css({"background-color" : "#002c6a"});
				$('.twohalf').off('click').css({"user-select":"none"});
			}
			else if (fillChooser==2) {
				$('.twohalf').css({"background-color" : "#002c6a"});
				$('.onehalf').off('click').css({"user-select":"none"});
			}
			$('.yellow').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('yellow','#fa8231', "ex1_3_1", "orange");
			});
			$('.green').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('green','green', "ex1_3_1", "green");
			});
			$('.purple').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('purple','purple', "ex1_3_1", "purple");
			});
			$('.brown').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('brown','brown', "ex1_3_1", "brown");
			});
			$('.red').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('red','red', "ex1_3_1", "red");
			});
			$('.blue').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('blue','blue', "ex1_3_1", "blue");
			});
			break;
			case 3:
				sound_player("ex1_"+(countNext+1));
				$(".onequat,.twoquat").css({"height":"16.5%","top":"29%"});
				$(".threequat,.fourquat").css({"height":"16.5%"});

				$('.onequat').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					$(this).css({"background-color" : "#002c6a"});
					$('.twoquat,.threequat,.fourquat').off('click').css({"user-select":"none"});
					fillChooser = 1;
					nav_button_controls(200);
				});
				$('.twoquat').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					$(this).css({"background-color" : "#002c6a"});
					$('.onequat,.threequat,.fourquat').off('click').css({"user-select":"none"});
					fillChooser = 2;
					nav_button_controls(200);
				});
				$('.threequat').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					$(this).css({"background-color" : "#002c6a"});
					$('.onequat,.twoquat,.fourquat').off('click').css({"user-select":"none"});
					fillChooser = 3;
					nav_button_controls(200);
				});
				$('.fourquat').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					$(this).css({"background-color" : "#002c6a"});
					$('.onequat,.threequat,.twoquat').off('click').css({"user-select":"none"});
					fillChooser = 4;
					nav_button_controls(200);
				});
			break;
			case 4:
				sound_player("ex1_"+(countNext+1));
				$(".onequat,.twoquat").css({"height":"16.5%","top":"29%","cursor":"default"});
				$(".threequat,.fourquat").css({"height":"16.5%","cursor":"default"});
				$('.suriclass').css({"width":"17%"});
				$('.bottext').css({opacity:"0"});
				$('.colorblock').addClass('slidefromtop');
				if(fillChooser==1){
					$('.onequat').css({"background-color" : "#002c6a"});
					$('.twoquat').off('click').css({"user-select":"none"});
					$('.threequat').off('click').css({"user-select":"none"});
					$('.fourquat').off('click').css({"user-select":"none"});
				}
				else if (fillChooser==2) {
					$('.twoquat').css({"background-color" : "#002c6a"});
					$('.onequat').off('click').css({"user-select":"none"});
					$('.threequat').off('click').css({"user-select":"none"});
					$('.fourquat').off('click').css({"user-select":"none"});
				}
				else if (fillChooser==3) {
					$('.threequat').css({"background-color" : "#002c6a"});
					$('.onequat').off('click').css({"user-select":"none"});
					$('.twoquat').off('click').css({"user-select":"none"});
					$('.fourquat').off('click').css({"user-select":"none"});
				}
				else if (fillChooser==4) {
					$('.fourquat').css({"background-color" : "#002c6a"});
					$('.onequat').off('click').css({"user-select":"none"});
					$('.threequat').off('click').css({"user-select":"none"});
					$('.twoquat').off('click').css({"user-select":"none"});
				}
			// $('.yellow').click(function(){
			// 	colorFillerq('yellow','#fa8231');
			// });
			// $('.green').click(function(){
			// 	colorFillerq('green','green');
			// });
			// $('.purple').click(function(){
			// 	colorFillerq('purple','purple');
			// });
			// $('.brown').click(function(){
			// 	colorFillerq('brown','brown');
			// });
			// $('.red').click(function(){
			// 	colorFillerq('red','red');
			// });
			// $('.blue').click(function(){
			// 	colorFillerq('blue','blue');
			// });
			$('.yellow').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFillerq('yellow','#fa8231', "ex1_5_1", "orange");
			});
			$('.green').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFillerq('green','green', "ex1_5_1", "green");
			});
			$('.purple').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFillerq('purple','purple', "ex1_5_1", "purple");
			});
			$('.brown').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFillerq('brown','brown', "ex1_5_1", "brown");
			});
			$('.red').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFillerq('red','red', "ex1_5_1", "red");
			});
			$('.blue').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFillerq('blue','blue', "ex1_5_1", "blue");
			});
			break;
			case 5:
				sound_player("ex1_"+(countNext+1));
				$(".onehalf,.twohalf").css({"cursor":"default","padding-top":"4%"});
				$(".onehalf").find(".fraction2").hide();
				$('.onehalf').css({"background-color":"#002c6a", "color" : "#002c6a","user-select":"none"});
				$('.twohalf').css({"user-select":"none"});
			  $('.option1').click(function(){
                  current_sound.stop();
                  play_correct_incorrect_sound(1);
	              $(".onehalf").find(".fraction2").show();
	              $(this).css({"background-color":"#98c02e","border-color":"#e4e954",'pointer-events':'none'});
					$('.option2').css('pointer-events','none');
					$('<img id="correct" style="left:70%;top:30%;position:absolute;width:5%;transform:translate(250%,50%)" src="'+imgpath +'correct.png" />').insertAfter(this);
					$('.onehalf').css({"color" : "white","user-select":"none"});
					nav_button_controls(100);
				});
				$('.option2').click(function(){
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
	                $(this).css({"background-color":"#ff0000","border-color":"#980000"});
					$('<img id="incorrect" style="left:70%;top:43%;position:absolute;width:5%;transform:translate(250%,320%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
					$(this).css('pointer-events','none');
				});
				break;
				case 6:
					sound_player("ex1_"+(countNext+1));
	                $(".onehalf,.twohalf").css({"cursor":"default","padding-top":"4%"});
	                fillChooser=1;
				$('.bottext').css({opacity:"0"});
				$('.colorblock').addClass('slidefromtop');
				if(fillChooser==1){
					$('.onehalf').css({"background-color" : "#002c6a"});
					$('.twohalf').off('click').css({"user-select":"none"});
				}
				else if (fillChooser==2) {
					$('.twohalf').css({"background-color" : "#002c6a"});
					$('.onehalf').off('click').css({"user-select":"none"});
				}
			// $('.yellow').click(function(){
			// 	colorFiller('yellow','#fa8231');
			// });
			// $('.green').click(function(){
			// 	colorFiller('green','green');
			// });
			// $('.purple').click(function(){
			// 	colorFiller('purple','purple');
			// });
			// $('.brown').click(function(){
			// 	colorFiller('brown','brown');
			// });
			// $('.red').click(function(){
			// 	colorFiller('red','red');
			// });
			// $('.blue').click(function(){
			// 	colorFiller('blue','blue');
			// });

			$('.yellow').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('yellow','#fa8231', "ex1_7_1", "orange");
			});
			$('.green').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('green','green', "ex1_7_1", "green");
			});
			$('.purple').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('purple','purple', "ex1_7_1", "purple");
			});
			$('.brown').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('brown','brown', "ex1_7_1", "brown");
			});
			$('.red').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('red','red', "ex1_7_1", "red");
			});
			$('.blue').click(function(){
                current_sound.stop();
                sound_player("sound_ting");
				colorFiller('blue','blue', "ex1_7_1", "blue");
			});
			break;
			case 7:
				sound_player("ex1_"+(countNext+1));
								$(".onequat,.twoquat").css({"height":"16.5%","top":"29%"});
								$(".threequat,.fourquat").css({"height":"16.5%"});
                $(".onequat,.onehalf").find(".fraction2").hide();
                $('.onequat,.threequat').css({left:"3%"});
				$('.twoquat,.fourquat').css({left:"16%"});

				$('.onehalf').css({"background-color" : "#002c6a", "color" : "#002c6a"});
				$('.onehalf,.twohalf,.onequat,.twoquat,.threequat,.fourquat').css({"user-select":"none"});
				$('.onequat').css({"background-color" : "#002c6a", "color" : "#002c6a"});

				$('.onequat,.twoquat,.threequat,.fourquat').click(function(){
                    current_sound.stop();
                    play_correct_incorrect_sound(1);
	                $(".onequat").find(".fraction2").show();

	                $('<img id="correct" style="left:-4%;top:61%;position:absolute;width:5%;transform:translate(250%,50%)" src="'+imgpath +'correct.png" />').insertAfter(this);
					$('.onehalf,.twohalf').css('pointer-events','none');
					$('.onequat,.twoquat,.threequat,.fourquat').css({'pointer-events':'none'});
					$('.onequat').css({"color" : "white","user-select":"none"});
					nav_button_controls(100);
				});

				$('.onehalf,.twohalf').click(function(){
                    current_sound.stop();
                    play_correct_incorrect_sound(0)
					$('<img id="incorrect" style="left:32%;top:41%;position:absolute;width:5%;transform:translate(250%,320%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
					$(this).css('pointer-events','none');
				});
			break;
			case 8:
				sound_player("ex1_"+(countNext+1));
				$(".onequat,.twoquat").css({"height":"16.5%","top":"29%"});
				$(".threequat,.fourquat").css({"height":"16.5%"});
	                $(".onehalf").find(".fraction2").hide();
	                $('.onequat,.threequat').css({left:"3%"});
				$('.twoquat,.fourquat').css({left:"16%"});
				$('.onehalf').css({"background-color":"#002c6a", "color":"#002c6a"});
				fillChooser==1
				$('.bottext').css({opacity:"0"});
				$('.colorblock').addClass('slidefromtop');
				if(fillChooser==1){
					$('.onequat').css({"background-color" : "#002c6a"});
					$('.twoquat').off('click').css({"user-select":"none"});
					$('.threequat').off('click').css({"user-select":"none"});
					$('.fourquat').off('click').css({"user-select":"none"});
				}
				else if (fillChooser==2) {
					$('.twoquat').css({"background-color" : "#002c6a"});
					$('.onequat').off('click').css({"user-select":"none"});
					$('.threequat').off('click').css({"user-select":"none"});
					$('.fourquat').off('click').css({"user-select":"none"});
				}
				else if (fillChooser==3) {
					$('.threequat').css({"background-color" : "#002c6a"});
					$('.onequat').off('click').css({"user-select":"none"});
					$('.twoquat').off('click').css({"user-select":"none"});
					$('.fourquat').off('click').css({"user-select":"none"});
				}
				else if (fillChooser==4) {
					$('.fourquat').css({"background-color" : "#002c6a"});
					$('.onequat').off('click').css({"user-select":"none"});
					$('.threequat').off('click').css({"user-select":"none"});
					$('.twoquat').off('click').css({"user-select":"none"});
				}
				$('.yellow').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFillerq('yellow','#fa8231', "ex1_9_1", "orange");
				});
				$('.green').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFillerq('green','green', "ex1_9_1", "green");
				});
				$('.purple').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFillerq('purple','purple', "ex1_9_1", "purple");
				});
				$('.brown').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFillerq('brown','brown', "ex1_9_1", "brown");
				});
				$('.red').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFillerq('red','red', "ex1_9_1", "red");
				});
				$('.blue').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFillerq('blue','blue', "ex1_9_1", "blue");
				});
			break;
			case 9:
				sound_player("ex1_"+(countNext+1));
                $(".onehalf").css({"padding-top":"3%"});

                $(".onequat,.twoquat").css({"height":"16.5%","top":"29%"});
				$(".threequat,.fourquat").css({"height":"16.5%"});
	                $(".onehalf,.onequat").find(".fraction2").hide();

	                $('.onequat,.threequat').css({left:"3%"});
				$('.twoquat,.fourquat').css({left:"16%"});

				$('.onehalf').css({"background-color" : "#002c6a", "color" : "#002c6a"});
				$('.onehalf,.twohalf,.onequat,.twoquat,.threequat,.fourquat').css({"user-select":"none"});
				$('.onequat').css({"background-color" : "#002c6a", "color" : "#002c6a"});

				$('.onehalf,.twohalf').click(function(){
                    current_sound.stop();
                    play_correct_incorrect_sound(1);
	                $(".onehalf").find(".fraction2").show();
	                $('<img id="correct" style="left:32%;top:41%;position:absolute;width:5%;transform:translate(250%,320%)" src="'+imgpath +'correct.png" />').insertAfter(this);
					$('.onehalf,.twohalf').css('pointer-events','none');
					$('.onequat,.twoquat,.threequat,.fourquat').css({'pointer-events':'none'});
					$('.onehalf').css({"color" : "white","user-select":"none"});
					nav_button_controls(100);
				});

				$('.onequat,.twoquat,.threequat,.fourquat').click(function(){
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
					$('<img id="incorrect" style="left:-4%;top:61%;position:absolute;width:5%;transform:translate(250%,50%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
					$(this).css('pointer-events','none');
				});
			break;
			case 10:
				sound_player("ex1_"+(countNext+1));
                $(".onehalf").css({"padding-top":"3%"});

                $(".onequat").find(".fraction2").hide();

                $('.onequat,.threequat').css({left:"3%"});
				$('.twoquat,.fourquat').css({left:"16%"});
				$('.onequat').css({"background-color":"#002c6a", "color":"#002c6a"});
				fillChooser=1;
				$('.bottext').css({opacity:"0"});
				$('.colorblock').addClass('slidefromtop');
				if(fillChooser==1){
					$('.onehalf').css({"background-color" : "#002c6a"});
					$('.twohalf').off('click').css({"user-select":"none"});
				}
				else if (fillChooser==2) {
					$('.twohalf').css({"background-color" : "#002c6a"});
					$('.onehalf').off('click').css({"user-select":"none"});
				}
				$('.yellow').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFiller('yellow','#fa8231', "ex1_11_1", "orange");
				});
				$('.green').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFiller('green','green', "ex1_11_1", "green");
				});
				$('.purple').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFiller('purple','purple', "ex1_11_1", "purple");
				});
				$('.brown').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFiller('brown','brown', "ex1_11_1", "brown");
				});
				$('.red').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFiller('red','red', "ex1_11_1", "red");
				});
				$('.blue').click(function(){
                    current_sound.stop();
                    sound_player("sound_ting");
					colorFiller('blue','blue', "ex1_11_1", "blue");
				});
			break;
			case 11:
				sound_player("ex1_"+(countNext+1));
			$('.lastbut1').click(function(){
				$('#activity-page-exercise-tab').find('button').trigger('click');
			});
			$('.lastbut3').click(function(){
				$("#activity-page-lesson-tab").find('button').trigger('click');
			});
			$('.lastbut2').click(function(){
				$("#activity-page-menu-img").trigger("click");
			});
			break;
			default:

	}
}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			}
		else {
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function colorFiller(colorName, colorCode, sound1, sound2){
			$('.colors').html(eval("data.string.text_"+colorName));
			$('.bottext').animate({opacity:"1"},800,"linear");
			if (fillChooser==1){
				$('.onehalf').css({"background-color" : colorCode});
			}
			else if (fillChooser==2) {
				$('.twohalf').css({"background-color" : colorCode});
			}

			if($lang == "en"){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play(sound1);
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play(sound2);
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(100);
					});
				});
			}else{
				createjs.Sound.stop();
				current_sound = createjs.Sound.play(sound2);
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play(sound1);
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(100);
					});
				});
			}
	}

	function colorFillerq(colorName, colorCode, sound1, sound2){
			$('.colors').html(eval("data.string.text_"+colorName));
			$('.bottext').animate({opacity:"1"},800,"linear");
			if (fillChooser==1){
				$('.onequat').css({"background-color" : colorCode});
			}
			else if (fillChooser==2) {
				$('.twoquat').css({"background-color" : colorCode});
			}
			else if (fillChooser==3) {
				$('.threequat').css({"background-color" : colorCode});
			}
			else if (fillChooser==4) {
				$('.fourquat').css({"background-color" : colorCode});
			}

			if($lang == "en"){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play(sound1);
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play(sound2);
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(100);
					});
				});
			}else{
				createjs.Sound.stop();
				current_sound = createjs.Sound.play(sound2);
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play(sound1);
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(100);
					});
				});
			}
			// nav_button_controls(100);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
			if(content[count].hasOwnProperty('imageblock')){
				for(var j = 0; j < content[count].imageblock.length; j++){
					var imageblock = content[count].imageblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
		}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}


	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
});

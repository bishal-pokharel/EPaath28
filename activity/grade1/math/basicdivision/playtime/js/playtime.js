var imgpath = $ref+"/images/Playtime/";
var soundAsset = $ref + "/sounds/";
var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"ptime",
                textclass: "chapter centertext",
                textdata: data.string.playtime
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: "coverpageImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "rhinodancingdiv",
                    imgclass: " relativecls rhinoimgdiv",
                    imgid: "rhinodance",
                    imgsrc: "",
                },
                {
                    imgdiv: "squirrelistening",
                    imgclass: " relativecls squirrelisteningimg",
                    imgid: "squirrel",
                    imgsrc: "",
                }]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic',
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                textdiv:"optiondiv"
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic1',
                textdiv:"question1",
                textclass: "content1 centertext",
                textdata: data.string.q1
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt3",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt4",
                textclass: "content1 centertext",
                textdata:""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: "bgImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p1",
                    imgclass: "relativecls img2",
                    imgid: "paraImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p2",
                    imgclass: "relativecls img3",
                    imgid: "paraImg1",
                    imgsrc: "",
                },
             ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic',
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                textdiv:"optiondiv"
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic1',
                textdiv:"question1",
                textclass: "content1 centertext",
                textdata: data.string.q1
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt3",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt4",
                textclass: "content1 centertext",
                textdata:""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: "bgImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p1",
                    imgclass: "relativecls img2",
                    imgid: "paraImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p2",
                    imgclass: "relativecls img3",
                    imgid: "paraImg1",
                    imgsrc: "",
                },
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic',
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.pt2
            },
            {
                textdiv:"optiondiv"
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic1',
                textdiv:"question1",
                textclass: "content1 centertext",
                textdata: data.string.q1
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt3",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt4",
                textclass: "content1 centertext",
                textdata:""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: "bgImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p1",
                    imgclass: "relativecls img2",
                    imgid: "paraImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p2",
                    imgclass: "relativecls img3",
                    imgid: "paraImg1",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p3",
                    imgclass: "relativecls img4",
                    imgid: "paraImg2",
                    imgsrc: "",
                },
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic',
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.pt2
            },
            {
                textdiv:"optiondiv"
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic1',
                textdiv:"question1",
                textclass: "content1 centertext",
                textdata: data.string.q1
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt3",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt4",
                textclass: "content1 centertext",
                textdata:""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: "bgImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p1",
                    imgclass: "relativecls img2",
                    imgid: "paraImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p2",
                    imgclass: "relativecls img3",
                    imgid: "paraImg1",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p3",
                    imgclass: "relativecls img4",
                    imgid: "paraImg2",
                    imgsrc: "",
                },
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic',
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.pt3
            },
            {
                textdiv:"optiondiv"
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic1',
                textdiv:"question1",
                textclass: "content1 centertext",
                textdata: data.string.q1
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt3",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt4",
                textclass: "content1 centertext",
                textdata:""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: "bgImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p1",
                    imgclass: "relativecls img2",
                    imgid: "paraImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p2",
                    imgclass: "relativecls img3",
                    imgid: "paraImg1",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p3",
                    imgclass: "relativecls img4",
                    imgid: "paraImg2",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p4",
                    imgclass: "relativecls img5",
                    imgid: "paraImg3",
                    imgsrc: "",
                },
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
    //SLIDE 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic',
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.pt3
            },
            {
                textdiv:"optiondiv"
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'questiondynamic1',
                textdiv:"question1",
                textclass: "content1 centertext",
                textdata: data.string.q1
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt3",
                textclass: "content1 centertext",
                textdata:""
            },
            {
                textdiv:"option opt4",
                textclass: "content1 centertext",
                textdata:""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: "bgImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p1",
                    imgclass: "relativecls img2",
                    imgid: "paraImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p2",
                    imgclass: "relativecls img3",
                    imgid: "paraImg1",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p3",
                    imgclass: "relativecls img4",
                    imgid: "paraImg2",
                    imgsrc: "",
                },
                {
                    imgdiv: "para p4",
                    imgclass: "relativecls img5",
                    imgid: "paraImg3",
                    imgsrc: "",
                },
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
//lastslide
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",

        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "parachutesimg pa1",
                    imgclass: "relativecls img1",
                    imgid: "sheep1_2",
                    imgsrc: "",
                },
                {
                    imgdiv: "parachutesimg pa2",
                    imgclass: "relativecls img2",
                    imgid: "dog11_3",
                    imgsrc: "",
                },
                {
                    imgdiv: "parachutesimg pa3",
                    imgclass: "relativecls img3",
                    imgid: "rabbit111_5",
                    imgsrc: "",
                },
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    var preload;
    var timeoutvar = null;
    var current_sound="";
    var sound="";
    var tempfirstarray = new Array();
    var tempsecondarray = new Array();
    var palindromarray = new Array();
    var endpageex =  new EndPageofExercise();
    var message = data.string.lastmsg;
    endpageex.init(3);
    loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
             {id: "coverpageImg", src:imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "bgImg", src: imgpath+"bg_playtime.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheepImg", src: imgpath+"sheep.png", type: createjs.AbstractLoader.IMAGE},
             {id: "dogImg", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rabbitImg", src: imgpath+"rabbit.png", type: createjs.AbstractLoader.IMAGE},
             {id: "paraImg", src: imgpath+"parachute.png", type: createjs.AbstractLoader.IMAGE},
             {id: "paraImg1", src: imgpath+"parachute2.png", type: createjs.AbstractLoader.IMAGE},
             {id: "paraImg2", src: imgpath+"parachute3.png", type: createjs.AbstractLoader.IMAGE},
             {id: "paraImg3", src: imgpath+"parachute4.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep1", src: imgpath+"one_sheep.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep1_1", src: imgpath+"one_sheep01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep2", src: imgpath+"two_sheep01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep1_2", src: imgpath+"two_sheep02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep3", src: imgpath+"three_sheep01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep1_3", src: imgpath+"three_sheep02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep4", src: imgpath+"four_sheep01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep1_4", src: imgpath+"four_sheep02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep5", src: imgpath+"five_sheep01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sheep1_5", src: imgpath+"five_sheep02.png", type: createjs.AbstractLoader.IMAGE},

            {id: "dog1", src: imgpath+"one_dog.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog1_1", src: imgpath+"one_dog01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog11_1", src: imgpath+"one_dog02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog2", src: imgpath+"two_dog01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog1_2", src: imgpath+"two_dog02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog11_2", src: imgpath+"two_dog03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog3", src: imgpath+"three_dog01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog1_3", src: imgpath+"three_dog02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog11_3", src: imgpath+"three_dog03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog4", src: imgpath+"four_dog01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog1_4", src: imgpath+"four_dog02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog11_4", src: imgpath+"four_dog03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog5", src: imgpath+"five_dog01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog1_5", src: imgpath+"five_dog02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog11_5", src: imgpath+"five_dog03.png", type: createjs.AbstractLoader.IMAGE},

            {id: "rabbit1", src: imgpath+"one_rabbit.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit1_1", src: imgpath+"one_rabbit01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit11_1", src: imgpath+"one_rabbit02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit111_1", src: imgpath+"one_rabbit03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit2", src: imgpath+"two_rabbit01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit1_2", src: imgpath+"two_rabbit02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit11_2", src: imgpath+"two_rabbit03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit111_2", src: imgpath+"two_rabbit04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit3", src: imgpath+"three_rabbit01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit1_3", src: imgpath+"three_rabbit02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit11_3", src: imgpath+"three_rabbit03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit111_3", src: imgpath+"three_rabbit04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit4", src: imgpath+"four_rabbit01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit1_4", src: imgpath+"four_rabbit02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit11_4", src: imgpath+"four_rabbit03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit111_4", src: imgpath+"four_rabbit04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit5", src: imgpath+"five_rabbit01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit1_5", src: imgpath+"five_rabbit02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit11_5", src: imgpath+"five_rabbit03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit111_5", src: imgpath+"five_rabbit04.png", type: createjs.AbstractLoader.IMAGE},

            //sounds
            {id: "sound_1", src:soundAsset+"fly_sound.ogg"},
            {id: "sound_2", src:soundAsset+"ukulele.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    // $refreshBtn.click(function(){
    //     templateCaller();
    // });
    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
firstPagePlayTime(countNext);
        texthighlight($board);
        put_image(content, countNext);
        switch (countNext){
            case 1:
            case 2:
                var firstval = countNext==2?generaterandomnum([2,4,6,8,10],tempfirstarray):generaterandomnum([2,4,6,8,10],[0]);
                tempfirstarray.push(firstval);
                //load images
                loadimages("sheep","sheepImg",firstval);

                //load question val
                createques(firstval,2);

                //check ans
                $(".option").click(function(){
                    var generatesheepval =firstval/2;
                    if($(this).find("p").text()==generatesheepval){
                        setTimeout(function(){
                          sound_player("sound_1") ;
                        },2000);
                        play_correct_incorrect_sound(1);
                        $(this).addClass("correctans");
                        $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                        $(".option").addClass("avoid-clicks");
                        for(var i=0;i<generatesheepval;i++){
                            $(".sheep"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"25%",
                                },1000
                            );
                        }
                        for(var i=generatesheepval;i<firstval;i++){
                            $(".sheep"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"65%",
                                },1000
                            );
                        }
                        setTimeout(function(){
                            $(".animals").animate({"opacity":"0"},100);
                            $(".p1 img").attr("src", preload.getResult("sheep"+generatesheepval).src);
                            $(".p2 img").attr("src", preload.getResult("sheep1_"+generatesheepval).src) ;
                        },1100);
                        setTimeout(function () {
                            $(".para").animate({"top":"-22%"},1200);
                            navigationcontroller();
                        },2200);
                    }
                    else{
                        $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                        $(this).addClass("wrongans avoid-clicks");
                        play_correct_incorrect_sound(0);
                    }
                });

                break;
            case 3:
            case 4:
                countNext==3?tempfirstarray = []:'';
                var firstval = countNext==4?generaterandomnum([3,6,9,12,15],tempfirstarray):generaterandomnum([3,6,9,12,15],[0]);
                tempfirstarray.push(firstval);
                $(".p1").css("left","9%");
                $(".p2").css("left","36%");
                $(".p3").css("left","63%");
                //load images
                loadimages("dog","dogImg",firstval);

                //load question val
                createques(firstval,3);

                //check ans
                $(".option").click(function(){
                    var generatesheepval =firstval/3;
                    if($(this).find("p").text()==generatesheepval){
                        setTimeout(function(){
                            sound_player("sound_1") ;
                        },2000);
                        play_correct_incorrect_sound(1);
                        $(this).addClass("correctans");
                        $(".option").addClass("avoid-clicks");
                        $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                        for(var i=0;i<generatesheepval;i++){
                            $(".dog"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"17%",
                                },1000
                            );
                        }
                        for(var i=generatesheepval;i<generatesheepval*2;i++){
                            $(".dog"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"49%",
                                },1000
                            );
                        }
                        for(var i=(generatesheepval*2);i<generatesheepval*3;i++){
                            $(".dog"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"80%",
                                },1000
                            );
                        }
                        setTimeout(function(){
                            $(".animals").animate({"opacity":"0"},100);
                            $(".p1 img").attr("src", preload.getResult("dog"+generatesheepval).src);
                            $(".p2 img").attr("src", preload.getResult("dog1_"+generatesheepval).src) ;
                            $(".p3 img").attr("src", preload.getResult("dog11_"+generatesheepval).src) ;
                        },1100);
                        setTimeout(function () {
                            $(".para").animate({"top":"-22%"},1200);
                            navigationcontroller();
                        },2200);
                    }
                    else{
                        $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                        play_correct_incorrect_sound(0);
                        $(this).addClass("wrongans avoid-clicks");
                    }
                });

                break;
            case 5:
            case 6:
                countNext==5?tempfirstarray = []:'';
                var firstval = countNext==6?generaterandomnum([4,8,12,16,20],tempfirstarray):generaterandomnum([4,8,12,16,20,0],[0]);
                tempfirstarray.push(firstval);
                $(".p1").css("left","2%");
                $(".p2").css("left","24%");
                $(".p3").css("left","45%");
                $(".p4").css("left","66%");
                //load images
                loadimages("rabbit","rabbitImg",firstval);

                //load question val
                createques(firstval,4);

                //check ans
                $(".option").click(function(){
                    var generatesheepval =firstval/4;
                    if($(this).find("p").text()==generatesheepval){
                        setTimeout(function(){
                            sound_player("sound_1") ;
                        },2000);
                        play_correct_incorrect_sound(1);
                        $(this).addClass("correctans");
                        $(".option").addClass("avoid-clicks");
                        $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                        for(var i=0;i<generatesheepval;i++){
                            $(".rabbit"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"8%",
                                },1000
                            );
                        }
                        for(var i=generatesheepval;i<generatesheepval*2;i++){
                            $(".rabbit"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"34%",
                                },1000
                            );
                        }
                        for(var i=(generatesheepval*2);i<generatesheepval*3;i++){
                            $(".rabbit"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"58%",
                                },1000
                            );
                        }
                        for(var i=(generatesheepval*3);i<generatesheepval*4;i++){
                            $(".rabbit"+i).animate(
                                {
                                    "top": "-78%",
                                    "left":"84%",
                                },1000
                            );
                        }
                        setTimeout(function(){
                            $(".animals").animate({"opacity":"0"},100);
                            $(".p1 img").attr("src", preload.getResult("rabbit"+generatesheepval).src);
                            $(".p2 img").attr("src", preload.getResult("rabbit1_"+generatesheepval).src) ;
                            $(".p3 img").attr("src", preload.getResult("rabbit11_"+generatesheepval).src) ;
                            $(".p4 img").attr("src", preload.getResult("rabbit111_"+generatesheepval).src) ;
                        },1100);
                        setTimeout(function () {
                            $(".para").animate({"top":"-22%"},1200);
                            navigationcontroller();
                        },2200);
                    }
                    else{
                        $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                        play_correct_incorrect_sound(0);
                        $(this).addClass("wrongans avoid-clicks");
                    }
                });
                break;
            case 7:
                var intervalid=setInterval(function() {
                    sound_player("sound_2") ;
                },33000);
                sound_player("sound_2") ;
                endpageex.endpage(message);
                navigationcontroller();
                break;
            default:
                navigationcontroller();
                break;
        }
       $('.audiodiv').click(function(){
			sound_player(sound,false);
			});

    }


    function sound_player(sound_id,navigate,imgCls,imgSrc) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():"";
        });
    }



    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('imghintblock')) {
            var contentCount=content[count].imghintblock[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }
    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                // lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });




    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                 $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1","option opt2","option opt3","option opt4"];
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }
    function generaterandomnum(array,excludenum){
        for(var i = 0; i<excludenum.length;i++) {
            array.splice(array.indexOf(excludenum[i]), 1);
        }
        var random = array[Math.floor(Math.random() * array.length)];
        return random;
    }

    //sheep,sheepImg
    function loadimages(classname,imageid,firstval){
        var leftv = 0;
        var leftv1 = 0;
        for(var i=0;i<firstval;i++) {
            $(".containercls").append("<img class='animals " + classname+i + "' src='" + preload.getResult(imageid).src + "'>");
            if(i>9){
                $("."+classname+i).css({"left":leftv1+"%","top":"50%","width":100 / 10 + "%"});
                leftv1 = leftv1+10;
            }
            else {
                $("."+classname+i).css({"left":leftv + "%","top":"0%","width":100 / 10 + "%"});
                leftv = leftv + 10;
            }
        }
    }
    function createques(firstval,numofpara){
        $(".questiondynamic").eq(0).text(firstval);
        $(".questiondynamic1").eq(0).text(firstval);
        $(".questiondynamic").eq(1).text(numofpara);
        $(".questiondynamic1").eq(1).text(numofpara);
        $(".opt1 p").text(firstval/numofpara);
        var arr = [1,2,3,4,5,6,7,8,9,10];
        var remarr = [firstval/numofpara];
        for(var i=2;i<5;i++){
            var curoption = generaterandomnum([1,2,3,4,5,6,7,8,9,10],remarr);
            $(".opt"+i).find("p").text(curoption);
            remarr.push(curoption);
        }
        shufflehint();
    }
});



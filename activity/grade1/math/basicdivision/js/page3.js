var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img2",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p3text1,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img2",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p3text2,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
         ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img2",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p3text3,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            },
            {
                divclass:"containercls2",
            },
        ]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.ten,
                textclass:"content1 relativecls"
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.ten,
                textclass:"content1 relativecls"
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img2",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p3text4,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            },
            {
                divclass:"containercls2",
            },
        ]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.ten,
                textclass:"content1 relativecls"
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.ten,
                textclass:"content1 relativecls"
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img2",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p3text5,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            },
            {
                divclass:"containercls2",
            },
        ]
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.ten,
                textclass:"content1 relativecls"
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.ten,
                textclass:"content1 relativecls"
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'fillimage',
                textdiv:"text2",
                textdata:data.string.p3text7,
                textclass:"content centertext",
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img2",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p3text6,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            },
            {
                divclass:"containercls2",
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "sagarImg", src: imgpath + "sagar04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "anjanaImg", src: imgpath + "anjana02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "anjanaImg1", src: imgpath + "anjana03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src: imgpath + "bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg", src: imgpath + "text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg1", src: imgpath + "text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg2", src: imgpath + "text_box03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "orangesackImg", src: imgpath + "orange_sack.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "basketImg", src: imgpath + "fruitbasket.png", type: createjs.AbstractLoader.IMAGE},
            {id: "orangeImg", src: imgpath + "orange.png", type: createjs.AbstractLoader.IMAGE},
            {id: "divisionImg", src: imgpath + "divide.png", type: createjs.AbstractLoader.IMAGE},
            {id: "equaltoImg", src: imgpath + "equal.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s3_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s3_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s3_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s3_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s3_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s3_p6.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
            case 1:
                sound_player("sound_"+(countNext+1),true);
                loadsvg('#orangesacksvg',"orangesackImg");
                for(var i=0;i<2;i++){
                    $(".containercls").append("<img class='basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                }
                break;
            case 2:
                sound_player("sound_"+(countNext+1),false);
                loadsvg('#orangesacksvg',"orangesackImg",20,true);
                for(var i=0;i<2;i++){
                    $(".containercls").append("<img class='basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                }
                for (var i = 1; i < 11; i++) {
                    $(".containercls1").append("<img class='hideorange orange" + i + "' src='" + preload.getResult('orangeImg').src + "'>");
                    $(".containercls2").append("<img class='hideorange orange" + i + "' src='" + preload.getResult('orangeImg').src + "'>");
                }
                //show below oranges
                var delaytime=1000;
                for(var i=6;i<11;i++){
                    for(var j=1;j<=2;j++) {
                        $(".containercls"+j).find(".orange" + i).delay(delaytime).animate({"opacity": "1"}, 100);
                        delaytime=delaytime+1000;
                    }
                }
                //show above oranges
                var delaytime1 =11000
                for(var i=1;i<=5;i++){
                    for(var j=1;j<=2;j++) {
                        $(".containercls"+j).find(".orange" + i).delay(delaytime1).animate({"opacity": "1"}, 100);
                        delaytime1=delaytime1+1000;
                    }
                }
                $(".n1").hide().delay(1000).fadeIn(100);
                $(".n2").hide().delay(2000).fadeIn(100);
                setTimeout(function(){
                    changetext(0,2);
                },2000)
                break;
            case 3:
            case 4:
            case 5:
                sound_player("sound_"+(countNext+1),true);
                loadsvg('#orangesacksvg',"orangesackImg",20,false);
                for(var i=0;i<2;i++){
                    $(".containercls").append("<img class='basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                }
                for (var i = 1; i < 11; i++) {
                    $(".containercls1").append("<img class=' orange" + i + "' src='" + preload.getResult('orangeImg').src + "'>");
                    $(".containercls2").append("<img class=' orange" + i + "' src='" + preload.getResult('orangeImg').src + "'>");
                }
                $(".fillimage").eq(1).append("<img class=' divided' src= '"+ preload.getResult('divisionImg').src +"'>");
                $(".fillimage").eq(3).append("<img class=' equalto' src= '"+ preload.getResult('equaltoImg').src +"'>");
                $(".fillimage").css("opacity","0");
                var delaytime=3000;
                for(var i=0;i<5;i++){
                    $(".fillimage").eq(i).delay(delaytime).animate({"opacity":"1"},100);
                    delaytime = delaytime+1000;
                }
                break;
            default:
                sound_player("sound_"+(countNext+1),true);
                clearTimeout(setTime);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page,countNext==5?true:false) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function loadsvg(svgid,imgid,removeorgnum,delay){
        var s = Snap(svgid);
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s.append(loadedFragment);
            clearTimeout(setTime);
            if(removeorgnum && delay){
                var delaytime2=1000;
                for(var i=1;i<=20;i++){
                    $("#orange"+i).delay(delaytime2).animate({"opacity":"0"},100);
                    delaytime2=delaytime2+1000
                }
                setTimeout(function(){
                    navigationcontroller(countNext,$total_page);
                },20000);
            }
            else if(removeorgnum && !delay){
                for(var i=1;i<=20;i++){
                  // $("#orange"+i).animate({"opacity":"0"},100);
                    $("#orange"+i).css("opacity","0");
                }
            }
        });

    }
    function loadorange(){
        for(var j=0;j<3;j++) {
            for (var i = 2; i < 6; i++) {
                $(".containercls" + i).append("<img class='hideorange orange" + i + "' src='" + preload.getResult('orangeImg').src + "'>");
            }
        }
        for(var j=0;j<2;j++) {
            for (var i = 6; i < 10; i++) {
                $(".containercls" +i).append("<img class=' hideorange orange" + i + "' src='" + preload.getResult('orangeImg').src + "'>");
            }
        }
    }
    function changetext(i,j){
        setTimeout(function(){
            $(".commonnumber").eq(i).find("p").text(j);
            i++;
            if(i>1){
                i=0;
                j++;
            }
            if(j<=10){
                changetext(i,j);
            }
        },1000)
    }
});

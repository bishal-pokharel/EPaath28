var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"text1",
                textclass: "content4 centertext",
                textdata: data.string.p2text2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar",
                    imgclass: "relativecls img2",
                    imgid: 'sagarImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text1,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar",
                    imgclass: "relativecls img2",
                    imgid: 'sagarImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text3,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
//slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar",
                    imgclass: "relativecls img2",
                    imgid: 'sagarImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text4,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls",
            },
        ]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana1",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox1",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text5,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls1",
            },
        ]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n3",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n4",
                textdata:data.string.one,
                textclass:"content1 centertext",
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana1",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox1",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text6,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls1",
            },
            {
                divclass:"container containercls2",
            },
            {
                divclass:"container containercls3",
            },
            {
                divclass:"container containercls4",
            },
            {
                divclass:"container containercls5",
            },
            {
                divclass:"container1 containercls6",
            },
            {
                divclass:"container1 containercls7",
            },
            {
                divclass:"container1 containercls8",
            },
            {
                divclass:"container1 containercls9",
            },
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n3",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n4",
                textdata:data.string.one,
                textclass:"content1 centertext",
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana1",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox1",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text7,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls1",
            },
            {
                divclass:"container containercls2",
            },
            {
                divclass:"container containercls3",
            },
            {
                divclass:"container containercls4",
            },
            {
                divclass:"container containercls5",
            },
            {
                divclass:"container1 containercls6",
            },
            {
                divclass:"container1 containercls7",
            },
            {
                divclass:"container1 containercls8",
            },
            {
                divclass:"container1 containercls9",
            },
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n3",
                textdata:data.string.one,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n4",
                textdata:data.string.one,
                textclass:"content1 centertext",
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana1",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox1",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text8,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls1",
            },
            {
                divclass:"container containercls2",
            },
            {
                divclass:"container containercls3",
            },
            {
                divclass:"container containercls4",
            },
            {
                divclass:"container containercls5",
            },
            {
                divclass:"container1 containercls6",
            },
            {
                divclass:"container1 containercls7",
            },
            {
                divclass:"container1 containercls8",
            },
            {
                divclass:"container1 containercls9",
            },
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n3",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n4",
                textdata:data.string.five,
                textclass:"content1 centertext",
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana1",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox1",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text9,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls1",
            },
            {
                divclass:"container containercls2",
            },
            {
                divclass:"container containercls3",
            },
            {
                divclass:"container containercls4",
            },
            {
                divclass:"container containercls5",
            },
            {
                divclass:"container1 containercls6",
            },
            {
                divclass:"container1 containercls7",
            },
            {
                divclass:"container1 containercls8",
            },
            {
                divclass:"container1 containercls9",
            },
        ]
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n3",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n4",
                textdata:data.string.five,
                textclass:"content1 centertext",
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana1",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox1",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text10,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls1",
            },
            {
                divclass:"container containercls2",
            },
            {
                divclass:"container containercls3",
            },
            {
                divclass:"container containercls4",
            },
            {
                divclass:"container containercls5",
            },
            {
                divclass:"container1 containercls6",
            },
            {
                divclass:"container1 containercls7",
            },
            {
                divclass:"container1 containercls8",
            },
            {
                divclass:"container1 containercls9",
            },
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n3",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n4",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana1",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox1",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text11,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls1",
            },
            {
                divclass:"container containercls2",
            },
            {
                divclass:"container containercls3",
            },
            {
                divclass:"container containercls4",
            },
            {
                divclass:"container containercls5",
            },
            {
                divclass:"container1 containercls6",
            },
            {
                divclass:"container1 containercls7",
            },
            {
                divclass:"container1 containercls8",
            },
            {
                divclass:"container1 containercls9",
            },
        ]
    },
//    slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock:[
            {
                textdiv:"commonnumber n1",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n2",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n3",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                textdiv:"commonnumber n4",
                textdata:data.string.five,
                textclass:"content1 centertext",
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'fillimage',
                textdiv:"text2",
                textdata:data.string.p2text13,
                textclass:"content centertext",
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "anjana1",
                    imgclass: "relativecls img3",
                    imgid: 'anjanaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox1",
                    imgclass: "relativecls img4",
                    imgid: 'dialboxImg2',
                    imgsrc: "" ,
                    textblock:[{
                        textdata: data.string.p2text12,
                        textclass:"content4"
                    }

                    ]
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'orangesacksvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ],
        containerblock:[
            {
                divclass:"containercls1",
            },
            {
                divclass:"container containercls2",
            },
            {
                divclass:"container containercls3",
            },
            {
                divclass:"container containercls4",
            },
            {
                divclass:"container containercls5",
            },
            {
                divclass:"container1 containercls6",
            },
            {
                divclass:"container1 containercls7",
            },
            {
                divclass:"container1 containercls8",
            },
            {
                divclass:"container1 containercls9",
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "sagarImg", src: imgpath + "sagar04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "anjanaImg", src: imgpath + "anjana02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "anjanaImg1", src: imgpath + "anjana03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src: imgpath + "bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg", src: imgpath + "text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg1", src: imgpath + "text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg2", src: imgpath + "text_box03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "orangesackImg", src: imgpath + "orange_sack.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "basketImg", src: imgpath + "fruitbasket.png", type: createjs.AbstractLoader.IMAGE},
            {id: "orangeImg", src: imgpath + "orange.png", type: createjs.AbstractLoader.IMAGE},
            {id: "divisionImg", src: imgpath + "divide.png", type: createjs.AbstractLoader.IMAGE},
            {id: "equaltoImg", src: imgpath + "equal.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s2_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s2_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s2_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s2_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s2_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s2_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s2_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s2_p10.ogg"},
            {id: "sound_11", src: soundAsset + "s2_p11.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_"+(countNext+1),true);
                $(".text1,#orangesacksvg").hide().delay(5000).fadeIn(1000);
                loadsvg('#orangesacksvg',"orangesackImg");
                break;
            case 1:
                sound_player("sound_"+(countNext+1),true);
                $(".dialbox p").css("padding","13%");
                loadsvg('#orangesacksvg',"orangesackImg");
                var delaytime = 7000;
                for(var i=0;i<4;i++){
                    $(".containercls").append("<img class='hide1 basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                    $(".containercls").append("<p class='hide1 basket"+i+"' >"+(i+1)+"</p>");
                    $(".containercls img").hide().delay(delaytime).fadeIn(1000);
                    $(".containercls p").hide().delay(delaytime).fadeIn(1000);
                    delaytime = delaytime+1000;
                }
                break;
            case 2:
                sound_player("sound_"+(countNext+1),true);
                $(".dialbox p").css({"padding":"4%"});
                loadsvg('#orangesacksvg',"orangesackImg");
                for(var i=0;i<4;i++){
                    $(".containercls").append("<img class='hide1 basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                    $(".containercls").append("<p class='hide1 basket"+i+"' >"+(i+1)+"</p>");
                }
                $(".containercls p").delay(800).animate({"opacity":"0"},1000);
                break;
            case 3:
                sound_player("sound_"+(countNext+1),true);
                loadsvg('#orangesacksvg',"orangesackImg");
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='hide1 basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                }
                break;
            case 4:
                sound_player("sound_"+(countNext+1),true);
                callnumber();
                $(".dialbox1 p").css({"padding":"5%"});
                loadsvg('#orangesacksvg',"orangesackImg",4,true);
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                }
                loadorange();
                var delaytime = 1000;
                for(var i=2;i<6;i++){
                    $(".containercls" + i).find(".hideorange").eq(0).delay(delaytime).animate({"opacity":"1"},100);
                    delaytime = delaytime+1000;
                }
                break;
            case 5:
                sound_player("sound_"+(countNext+1),true);
                $(".dialbox1 p").css({"padding":"7%"});
                loadsvg('#orangesacksvg',"orangesackImg",4,false);
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                }
                loadorange();
                for(var i=2;i<6;i++){
                    $(".containercls" + i).find(".hideorange").eq(0).css({"opacity":"1"},100);
                }
                break;
            case 6:
                sound_player("sound_"+(countNext+1),false);
                $(".dialbox1 p").css({"padding":"4%"});
                loadsvg('#orangesacksvg',"orangesackImg",20,true);
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                }
                loadorange();
                for(var i=2;i<6;i++){
                    $(".containercls" + i).find(".hideorange").eq(0).css({"opacity":"1"},100);
                }
                //load below threeoranges
                var delaytime =1000;
                for(var j=0;j<3;j++) {
                        for(var k=1;k<3;k++){
                            for (var i = 2; i < 6; i++) {
                             $(".containercls" + i).find(".hideorange").eq(k).delay(delaytime).animate({"opacity":"1"},100);

                          delaytime = delaytime +1000;
                        }
                    }
                }
                //load above two oranges
                var delaytime1 = 9000;
                for(var j=0;j<2;j++) {
                    for(var k=0;k<2;k++){
                        for (var i = 6; i < 10; i++) {
                            $(".containercls" + i).find(".hideorange").eq(k).delay(delaytime1).animate({"opacity":"1"},100);
                            delaytime1 = delaytime1 +1000;
                        }
                    }
                }
                changetext(0,2);
                break;
            case 7:
            case 8:
            case 9:
            case 10:
                sound_player("sound_"+(countNext+1),true);
                clearInterval(setTime);
                $(".dialbox1 p").css({"padding":"4%"});
                loadsvg('#orangesacksvg',"orangesackImg",20,false);
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='basket"+i+"' src='"+preload.getResult('basketImg').src+"'>");
                }
                loadorange();
                $(".hideorange").css("opacity","1");
                $(".fillimage").eq(1).append("<img class=' divided' src= '"+ preload.getResult('divisionImg').src +"'>");
                $(".fillimage").eq(3).append("<img class=' equalto' src= '"+ preload.getResult('equaltoImg').src +"'>");
                $(".fillimage").css("opacity","0");
                var delaytime=1000;
                for(var i=0;i<5;i++){
                    $(".fillimage").eq(i).delay(delaytime).animate({"opacity":"1"},100);
                    delaytime = delaytime+1000;
                }
                break;
            default:
                clearTimeout(setTime);
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function loadsvg(svgid,imgid,removeorgnum,delay){
        var s = Snap(svgid);
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s.append(loadedFragment);
            clearTimeout(setTime);
            if(countNext == 6){
                for (var i = 1; i <= 4; i++) {
                    $("#orange" + i).animate({"opacity": "0"}, 100);
                }
                var delaytime = 1000;
                for (var i = 5; i <= removeorgnum; i++) {

                    $("#orange" + i).delay(delaytime).animate({"opacity": "0"}, 100);
                    delaytime = delaytime + 1000;
                }
                setTimeout(function(){
                    navigationcontroller(countNext,$total_page);
                },16000);
            }
            else if(countNext>=7){
                for (var i = 1; i <= 20; i++) {
                    $("#orange" + i).css({"opacity": "0"});
                }
            }
            else if(removeorgnum){
                if(delay) {
                    var delaytime = 1000;
                    for (var i = 1; i <= removeorgnum; i++) {

                        $("#orange" + i).delay(delaytime).animate({"opacity": "0"}, 100);
                        delaytime = delaytime + 1000;
                    }
                }
                else{
                    for (var i = 1; i <= removeorgnum; i++) {
                        $("#orange" + i).animate({"opacity": "0"}, 100);
                    }
                }
            }

        });

    }
    function loadorange(){
        for(var j=0;j<3;j++) {
            for (var i = 2; i < 6; i++) {
                $(".containercls" + i).append("<img class='hideorange orange" + i + "' src='" + preload.getResult('orangeImg').src + "'>");
            }
        }
        for(var j=0;j<2;j++) {
            for (var i = 6; i < 10; i++) {
                $(".containercls" +i).append("<img class=' hideorange orange" + i + "' src='" + preload.getResult('orangeImg').src + "'>");
            }
        }
    }

    function callnumber(){
        $(".commonnumber").hide();
        var delaytime=1000;
        for(var i=0;i<4;i++){
            $(".commonnumber").eq(i).delay(delaytime).fadeIn(1000);
            delaytime = delaytime+1000;
        }
    }

    function changetext(i,j){
     setTimeout(function(){
                $(".commonnumber").eq(i).find("p").text(j);
                i++;
                if(i>3){
                    i=0;
                    j++;
                }
                if(j<6){
                    changetext(i,j);
                }
        },1000)
    }
});

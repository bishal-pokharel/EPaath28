var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "datafont centertext",
                textdata: data.string.chapter
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'covermainImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'fillimage',
                textdiv:"text1",
                textclass: "content centertext",
                textdata: data.string.p1text2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar",
                    imgclass: "relativecls img2",
                    imgid: 'sagarImg',
                    imgsrc: ""
                },
                {
                  imgdiv: "dialbox",
                  imgclass: "relativecls img3",
                  imgid: 'dialboxImg',
                  imgsrc: "",
                    textblock:[
                        {
                            textdata:data.string.p1text1,
                            textclass:"content4",
                        }
                    ]
                },
            ]
        }],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"text2",
                textclass: "title centertext",
                textdata: data.string.p1text3
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"text2",
                textclass: "title centertext",
                textdata: data.string.p1text4
            },
            {
                textdiv:"numleft",
                textclass: "title centertext",
                textdata: data.string.one
            },
            {
                textdiv:"numright",
                textclass: "title centertext",
                textdata: data.string.two
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                svgclass:"zoomInEffect",
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"text2",
                textclass: "title centertext",
                textdata: data.string.p1text5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"text2",
                textclass: "title centertext",
                textdata: data.string.p1text6
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"text2",
                textclass: "title centertext",
                textdata: data.string.p1text7
            },
            {
                textdiv:"numleft",
                textclass: "title centertext",
                textdata: data.string.five
            },
            {
                textdiv:"numright",
                textclass: "title centertext",
                textdata: data.string.five
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"text2",
                textclass: "title centertext",
                textdata: data.string.p1text8
            },
            {
                textdiv:"numleft",
                textclass: "title centertext",
                textdata: data.string.five
            },
            {
                textdiv:"numright",
                textclass: "title centertext",
                textdata: data.string.five
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"text3",
                textclass: "title centertext fadeInEffect",
                textdata: data.string.p1text9
            },
            {
                textdiv:"numleft",
                textclass: "title centertext",
                textdata: data.string.five
            },
            {
                textdiv:"numright",
                textclass: "title centertext",
                textdata: data.string.five
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'fillimage1',
                textdiv:"text4 hide2",
                textclass: "title centertext",
                textdata: data.string.p1text10
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'fillimage2',
                textdiv:"text3 hide3",
                textclass: "content centertext",
                textdata: data.string.p1text11
            },
            {
                textdiv:"numleft hide1",
                textclass: "title centertext",
                textdata: data.string.five
            },
            {
                textdiv:"numright hide1",
                textclass: "title centertext",
                textdata: data.string.five
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                svgclass:"hide1",
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                svgclass:"hide1",
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'fillimage3',
                textdiv:"text4",
                textclass: "content centertext",
                textdata: data.string.p1text12
            },
            {

                textdiv:"text2",
                textclass: "title centertext",
                textdata: data.string.p1text13
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar1",
                    imgclass: "relativecls img2",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "covermainImg", src: imgpath + "cover_page.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagarImg", src: imgpath + "sagar01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagar1Img", src: imgpath + "sagar03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src: imgpath + "bg01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg", src: imgpath + "text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "divisionImg", src: imgpath + "divide.png", type: createjs.AbstractLoader.IMAGE},
            {id: "divisionImg1", src: imgpath + "divide_w.png", type: createjs.AbstractLoader.IMAGE},
            {id: "equaltoImg", src: imgpath + "equal.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lefthandImg", src: imgpath + "left_hand.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "righthandImg", src: imgpath + "right_hand.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "righthandImg1", src: imgpath + "right_hand1.svg", type: createjs.AbstractLoader.IMAGE},

            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_6_1", src: soundAsset + "s1_p6_1.ogg"},
            {id: "sound_6_2", src: soundAsset + "s1_p6_2.ogg"},
            {id: "sound_6_3", src: soundAsset + "s1_p6_3.ogg"},
            {id: "sound_6_4", src: soundAsset + "s1_p6_4.ogg"},
            {id: "sound_6_5", src: soundAsset + "s1_p6_5.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p10.ogg"},
            {id: "sound_11", src: soundAsset + "s1_p11.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_1",true);
                 break;
            case 1:
                sound_player("sound_2",true);
                $(".fillimage").eq(0).append("<img class=' divided' src= '"+ preload.getResult('divisionImg').src +"'>");
                $(".fillimage").eq(1).append("<img class=' equalto' src= '"+ preload.getResult('equaltoImg').src +"'>");
                break;
            case 2:
                var sound_id = "sound_"+(countNext+1);
                sound_player(sound_id,true);
                var righthand = countNext==5?"righthandImg1":"righthandImg";
                  loadsvg('#lefthandsvg',"lefthandImg",true);
                  loadsvg('#righthandsvg',righthand,true);
                 break;
            case 5:
                  var righthand = countNext==5?"righthandImg1":"righthandImg";
                  loadsvg('#lefthandsvg',"lefthandImg",false);
                  loadsvg('#righthandsvg',righthand,false);
                  function countFingers(soundCount,fingerCount){
                    createjs.Sound.stop();
                    var current_sound = createjs.Sound.play("sound_6_"+soundCount);
                    current_sound.play();
                    $(".num"+fingerCount).fadeIn(100);
                    current_sound.on('complete', function(){
                      soundCount+=1;
                      fingerCount+=1;
                      soundCount>5?soundCount=1:soundCount=soundCount;
                      fingerCount<=10?countFingers(soundCount,fingerCount):navigationcontroller(countNext, $total_page);
                    });
                  }
                  createjs.Sound.stop();
                  var current_sound = createjs.Sound.play("sound_"+(countNext+1));
                  current_sound.play();
                  current_sound.on('complete', function () {
                    countFingers(1,1);
                  });
                 break;
            case 3:
                sound_player("sound_"+(countNext+1),true);
                $("#righthandsvg,.numleft,.numright").hide();
                $(".numleft").delay(1000).fadeIn(1000);
                $(".numright").delay(3000).fadeIn(1000);
                loadsvg('#lefthandsvg',"lefthandImg");
                setTimeout(function(){
                    $("#righthandsvg").show();
                    loadsvg('#righthandsvg',"righthandImg");
                },3000)
                break;
            case 4:
            case 8:
                sound_player("sound_"+(countNext+1),true);
                loadsvg('#lefthandsvg',"lefthandImg");
                loadsvg('#righthandsvg',"righthandImg1");
                break;
            case 6:
                sound_player("sound_"+(countNext+1),true);
                $(".numleft,.numright").hide();
                $(".numleft").css("left","20%").delay(4000).fadeIn(100);
                $(".numright").css("right","17%").delay(5000).fadeIn(100);
                loadsvg('#lefthandsvg',"lefthandImg");
                loadsvg('#righthandsvg',"righthandImg1");
                break;
            case 7:
                sound_player("sound_"+(countNext+1),true);
                $(".numleft").css("left","20%");
                $(".numright").css("right","17%");
                $(".numleft").delay(100).animate({"left":"26%"},1000);
                $(".numright").delay(100).animate({"right":"22%"},1000);
                loadsvg('#lefthandsvg',"lefthandImg");
                loadsvg('#righthandsvg',"righthandImg");
                break;
            case 8:
                sound_player("sound_"+(countNext+1),true);
                loadsvg('#lefthandsvg',"lefthandImg");
                loadsvg('#righthandsvg',"righthandImg");
                break;
            case 9:
                sound_player("sound_"+(countNext+1),true);
                loadsvg('#lefthandsvg',"lefthandImg");
                loadsvg('#righthandsvg',"righthandImg");
                $(".hide1").delay(1000).fadeOut(1000);
                $(".hide2").hide().delay(1000).fadeIn(1000);
                $(".hide3").hide().delay(8000).fadeIn(1000);
                $(".fillimage1").eq(0).append("<img class=' divided' src= '"+ preload.getResult('divisionImg').src +"'>");
                $(".fillimage1").eq(1).append("<img class=' equalto' src= '"+ preload.getResult('equaltoImg').src +"'>");
                $(".fillimage2").eq(0).append("<img class=' divided' src= '"+ preload.getResult('divisionImg1').src +"'>");
                break;
            case 10:
                sound_player("sound_"+(countNext+1),true);
                $(".text4 .fillimage3").css("opacity","0");
                $(".fillimage3").eq(1).append("<img class=' divided' src= '"+ preload.getResult('divisionImg').src +"'>");
                $(".fillimage3").eq(3).append("<img class=' equalto' src= '"+ preload.getResult('equaltoImg').src +"'>");
                var delaytime = 2000;
                for(var i=0;i<5;i++){
                    $(".text4 .fillimage3").eq(i).delay(delaytime).animate({"opacity":"1"},100);
                    delaytime = delaytime+1000;
                }
                break;
            default:
                sound_player("sound_"+(countNext+1),true);
                clearTimeout(setTime);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function loadsvg(svgid,imgid,shownum){
        var s = Snap(svgid);
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s.append(loadedFragment);
            clearTimeout(setTime);
            countNext==3?$("#righthandsvg").addClass("zoomInEffect"):"";
            $(".number").hide().css("font-family","abeezee");
            if(shownum) {
                var delaytime = 4000;
                for (var i = 1; i < 11; i++) {
                    $(".num" + i).delay(delaytime).fadeIn(100);
                    delaytime = delaytime + 1000;
                }
            }
          if(countNext==5){
                $("#lefthandsvg").animate({"left":"13%"},1000);
                $("#righthandsvg").animate({"right":"7%"},1000);
          }
          if(countNext == 6 || countNext == 7){
              $("#lefthandsvg").css({"left":"13%"});
              $("#righthandsvg").css({"right":"7%"});
          }
            if(countNext==7){
                $("#lefthandsvg").animate({"left":"18%"},1000);
                $("#righthandsvg").animate({"right":"13%"},1000);
            }
        });
    }
});

var soundAsset = $ref+"/audio_"+$lang + "/";
var soundAsset_common = $ref+"/audio_en/";
var soundAsset_numbers = $ref+"/audio_numbers/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide0
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg1",
			uppertextblock: [
					{
							textclass: "playtime",
							textdata: data.string.play1text15
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							}]
			}]
	},

	// slide1
	{
		uppertextblock:[{
			textdata:data.string.play1text1,
			textclass:'top_text'
		}],
		imageblock:[{
			imagestoshow:[{
					imgid:'bg',
					imgclass:'bg_full'
			}]
		}]
	},

	// slide2
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata:data.string.play1text2,
			textclass:'top_instruction'
		}],
		imageblock:[{
			imagestoshow:[{
					imgid:'aasha',
					imgclass:'asha'
			},
			{
					imgid:'goat',
					imgclass:'goat1'
			},
			{
					imgid:'goat',
					imgclass:'goat2'
			},
			{
					imgid:'goat',
					imgclass:'goat3'
			},
			{
					imgid:'hen',
					imgclass:'hen1'
			},
			{
					imgid:'hen',
					imgclass:'hen2'
			},
			{
					imgid:'hen',
					imgclass:'hen3'
			},
			{
					imgid:'hen',
					imgclass:'hen4'
			},
			{
					imgid:'cow',
					imgclass:'cow1'
			},
			{
					imgid:'cow',
					imgclass:'cow2'
			},
			{
					imgid:'dog',
					imgclass:'dog'
			}]
		}]
	},

	// slide3
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata:data.string.play1text16,
			textclass:'top_instruction',
			datahighlightflag:true,
			datahighlightcustomclass:'dogcolor'
		}],
		imageblock:[{
			imagestoshow:[
			{
					imgid:'goat',
					imgclass:'goat1a goaty'
			},
			{
					imgid:'goat',
					imgclass:'goat2a goaty'
			},
			{
					imgid:'goat',
					imgclass:'goat3a goaty'
			},
			{
					imgid:'hen',
					imgclass:'hen1a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen2a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen3a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen4a henny'
			},
			{
					imgid:'cow',
					imgclass:'cow1a cowy'
			},
			{
					imgid:'cow',
					imgclass:'cow2a cowy'
			},
			{
					imgid:'dog',
					imgclass:'doga doggy'
			}]
		}],
		dog:imgpath+'dog.png',
		hen:imgpath+'hen.png',
		goat:imgpath+'goat03.png',
		cow:imgpath+'cow.png',
		table:[
		{
				textclass:'number2',
				textdata:data.string.p1text6
		},
		{
				textclass:'number3',
				textdata:data.string.p1text5
		},
		{
				textclass:'number4',
				textdata:data.string.p1text4
		},
		{
				textclass:'number5',
				textdata:data.string.p1text3
		},
		{
				textclass:'animalnumber',
				textdata:data.string.play1text4
		},
		{
				textclass:'dogtag',
				textdata:data.string.play1text6
		},
		{
				textclass:'goattag',
				textdata:data.string.play1text7
		},
		{
				textclass:'hentag',
				textdata:data.string.play1text8
		},
		{
				textclass:'cowtag',
				textdata:data.string.play1text9
		},],
		question_div:[{
					textclass:'question',
					textdata:data.string.play1text10,
					datahighlightflag:true,
					datahighlightcustomclass:'instruction_highlight'
			},
			{
					textclass:'option correct',
					textdata:data.string.p1text6
			},
			{
					textclass:'option',
					textdata:data.string.p1text5
			},
			{
					textclass:'option',
					textdata:data.string.p1text4
			},
			{
					textclass:'option',
					textdata:data.string.p1text3
			},
		]
	},

	// slide4
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata:data.string.play1text17,
			textclass:'top_instruction',
			datahighlightflag:true,
			datahighlightcustomclass:'goatcolor'
		}],
		imageblock:[{
			imagestoshow:[
			{
					imgid:'goat',
					imgclass:'goat1a goaty'
			},
			{
					imgid:'goat',
					imgclass:'goat2a goaty'
			},
			{
					imgid:'goat',
					imgclass:'goat3a goaty'
			},
			{
					imgid:'hen',
					imgclass:'hen1a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen2a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen3a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen4a henny'
			},
			{
					imgid:'cow',
					imgclass:'cow1a cowy'
			},
			{
					imgid:'cow',
					imgclass:'cow2a cowy'
			},
			{
					imgid:'dog',
					imgclass:'doga doggy'
			}]
		}],
		dog:imgpath+'dog.png',
		hen:imgpath+'hen.png',
		goat:imgpath+'goat03.png',
		cow:imgpath+'cow.png',
		table:[
		{
				textclass:'number2',
				textdata:data.string.p1text6
		},
		{
				textclass:'number3',
				textdata:data.string.p1text5
		},
		{
				textclass:'number4',
				textdata:data.string.p1text4
		},
		{
				textclass:'number5',
				textdata:data.string.p1text3
		},
		{
				textclass:'animalnumber',
				textdata:data.string.play1text4
		},
		{
				textclass:'dogtag',
				textdata:data.string.play1text6
		},
		{
				textclass:'goattag',
				textdata:data.string.play1text7
		},
		{
				textclass:'hentag',
				textdata:data.string.play1text8
		},
		{
				textclass:'cowtag',
				textdata:data.string.play1text9
		},],
		question_div:[{
					textclass:'question',
					textdata:data.string.play1text11,
					datahighlightflag:true,
					datahighlightcustomclass:'instruction_highlight'
			},
			{
					textclass:'option ',
					textdata:data.string.p1text6
			},
			{
					textclass:'option ',
					textdata:data.string.p1text5
			},
			{
					textclass:'option correct',
					textdata:data.string.p1text4
			},
			{
					textclass:'option',
					textdata:data.string.p1text3
			},
		]
	},

	// slide5
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata:data.string.play1text18,
			textclass:'top_instruction',
			datahighlightflag:true,
			datahighlightcustomclass:'hencolor'
		}],
		imageblock:[{
			imagestoshow:[
			{
					imgid:'goat',
					imgclass:'goat1a goaty'
			},
			{
					imgid:'goat',
					imgclass:'goat2a goaty'
			},
			{
					imgid:'goat',
					imgclass:'goat3a goaty'
			},
			{
					imgid:'hen',
					imgclass:'hen1a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen2a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen3a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen4a henny'
			},
			{
					imgid:'cow',
					imgclass:'cow1a cowy'
			},
			{
					imgid:'cow',
					imgclass:'cow2a cowy'
			},
			{
					imgid:'dog',
					imgclass:'doga doggy'
			}]
		}],
		dog:imgpath+'dog.png',
		hen:imgpath+'hen.png',
		goat:imgpath+'goat03.png',
		cow:imgpath+'cow.png',
		table:[
		{
				textclass:'number2',
				textdata:data.string.p1text6
		},
		{
				textclass:'number3',
				textdata:data.string.p1text5
		},
		{
				textclass:'number4',
				textdata:data.string.p1text4
		},
		{
				textclass:'number5',
				textdata:data.string.p1text3
		},
		{
				textclass:'animalnumber',
				textdata:data.string.play1text4
		},
		{
				textclass:'dogtag',
				textdata:data.string.play1text6
		},
		{
				textclass:'goattag',
				textdata:data.string.play1text7
		},
		{
				textclass:'hentag',
				textdata:data.string.play1text8
		},
		{
				textclass:'cowtag',
				textdata:data.string.play1text9
		},],
		question_div:[{
					textclass:'question',
					textdata:data.string.play1text12,
					datahighlightflag:true,
					datahighlightcustomclass:'instruction_highlight'
			},
			{
					textclass:'option ',
					textdata:data.string.p1text6
			},
			{
					textclass:'option ',
					textdata:data.string.p1text5
			},
			{
					textclass:'option ',
					textdata:data.string.p1text4
			},
			{
					textclass:'option correct',
					textdata:data.string.p1text3
			},
		]
	},

	// slide6
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata:data.string.play1text19,
			textclass:'top_instruction',
			datahighlightflag:true,
			datahighlightcustomclass:'cowcolor'
		}],
		imageblock:[{
			imagestoshow:[
			{
					imgid:'goat',
					imgclass:'goat1a goaty'
			},
			{
					imgid:'goat',
					imgclass:'goat2a goaty'
			},
			{
					imgid:'goat',
					imgclass:'goat3a goaty'
			},
			{
					imgid:'hen',
					imgclass:'hen1a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen2a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen3a henny'
			},
			{
					imgid:'hen',
					imgclass:'hen4a henny'
			},
			{
					imgid:'cow',
					imgclass:'cow1a cowy'
			},
			{
					imgid:'cow',
					imgclass:'cow2a cowy'
			},
			{
					imgid:'dog',
					imgclass:'doga doggy'
			}]
		}],
		dog:imgpath+'dog.png',
		hen:imgpath+'hen.png',
		goat:imgpath+'goat03.png',
		cow:imgpath+'cow.png',
		table:[
		{
				textclass:'number2',
				textdata:data.string.p1text6
		},
		{
				textclass:'number3',
				textdata:data.string.p1text5
		},
		{
				textclass:'number4',
				textdata:data.string.p1text4
		},
		{
				textclass:'number5',
				textdata:data.string.p1text3
		},
		{
				textclass:'animalnumber',
				textdata:data.string.play1text4
		},
		{
				textclass:'dogtag',
				textdata:data.string.play1text6
		},
		{
				textclass:'goattag',
				textdata:data.string.play1text7
		},
		{
				textclass:'hentag',
				textdata:data.string.play1text8
		},
		{
				textclass:'cowtag',
				textdata:data.string.play1text9
		},],
		question_div:[{
					textclass:'question',
					textdata:data.string.play1text13,
					datahighlightflag:true,
					datahighlightcustomclass:'instruction_highlight'
			},
			{
					textclass:'option ',
					textdata:data.string.p1text6
			},
			{
					textclass:'option correct',
					textdata:data.string.p1text5
			},
			{
					textclass:'option ',
					textdata:data.string.p1text4
			},
			{
					textclass:'option ',
					textdata:data.string.p1text3
			},
		]
	},


	// slide7
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata:data.string.play1text14,
			textclass:'top_instruction'
		}],
		imageblock:[{
			imagestoshow:[
			{
					imgid:'aasha',
					imgclass:'aasha'
			}]
		}],
		dog:imgpath+'dog.png',
		hen:imgpath+'hen.png',
		goat:imgpath+'goat03.png',
		cow:imgpath+'cow.png',
		table:[
		{
				textclass:'number2',
				textdata:data.string.p1text6
		},
		{
				textclass:'number3',
				textdata:data.string.p1text5
		},
		{
				textclass:'number4',
				textdata:data.string.p1text4
		},
		{
				textclass:'number5',
				textdata:data.string.p1text3
		},
		{
				textclass:'animalnumber',
				textdata:data.string.play1text4
		},
		{
				textclass:'dogtag',
				textdata:data.string.play1text6
		},
		{
				textclass:'goattag',
				textdata:data.string.play1text7
		},
		{
				textclass:'hentag',
				textdata:data.string.play1text8
		},
		{
				textclass:'cowtag',
				textdata:data.string.play1text9
		},]
	},




];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var endpageex =  new EndPageofExercise();
	endpageex.init(5);
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "bg", src: imgpath+'bg01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "goat", src: imgpath+'goat03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+'cow.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+'dog.png', type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+'hen.png', type: createjs.AbstractLoader.IMAGE},
			{id: "aasha", src: imgpath+'aasha.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: imgpath+'correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: imgpath+'wrongicon.png', type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "correct", src: soundAsset_common+"correct.ogg"},
			{id: "incorrect", src: soundAsset_common+"incorrect.ogg"},
			{id: "camera", src: soundAsset_common+"camera.ogg"},
			{id: "dog_audio", src: soundAsset_common+"dog.ogg"},
			{id: "cow_audio", src: soundAsset_common+"cow.ogg"},
			{id: "hen_audio", src: soundAsset_common+"hen.ogg"},
			{id: "goat_audio", src: soundAsset_common+"goat.ogg"},
			{id: "exp2", src: soundAsset+"ex_p2.ogg"},
			{id: "exp3", src: soundAsset+"ex_p3.ogg"},
			{id: "exp4", src: soundAsset+"ex_p4.ogg"},
			{id: "exp5", src: soundAsset+"ex_p5.ogg"},
			{id: "exp6", src: soundAsset+"ex_p6.ogg"},
			{id: "exp7", src: soundAsset+"ex_p7.ogg"},
			{id: "exp8", src: soundAsset+"ex_p8.ogg"},
			{id: "click_on_the_correct_answer", src: soundAsset+"click_on_the_correct_answer.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		// put_speechbox_image(content, countNext);
		$('.option').click(function(){
			if($(this).hasClass('correct')){
				$(this).css({"background": "#98C02E",
											"border-color": "#DEEF3C",
												"transition":'.1s'});
				$('.option').css('pointer-events','none');
				sound_player('correct');
				$(this).children(".right").show(0);
				nav_button_controls(100);
			}
			else{
				$(this).css({"background": "#FF0000",
											"border-color": "#980000",
												"transition":'.1s',
												"pointer-events":"none"});
				sound_player('incorrect');
				$(this).children(".wrong").show(0);
			}
		});
		$('.top_instruction').hide(0).fadeIn(600);
		switch(countNext) {
			case 0:
			// sound_player('playtime');
			nav_button_controls(1000);
			break;
			case 1:
				sound_player("exp2",true);
				break;
			case 2:
                sound_player("exp3",true);
				break;
			case 3:
        sound_player("exp4",false);
        pop_up_animals(1,'doggy','dog_audio');
				$(".option").append("<img class='corincor right' src='"+preload.getResult("right").src+"' />");
				$(".option").append("<img class='corincor wrong' src='"+preload.getResult("wrong").src+"' />");
			break;

			case 4:
                sound_player("exp5",false);
                pop_up_animals(3,'goaty','goat_audio');
								$(".option").append("<img class='corincor right' src='"+preload.getResult("right").src+"' />");
								$(".option").append("<img class='corincor wrong' src='"+preload.getResult("wrong").src+"' />");
			$('.doggy').hide(0);
			$('.doggya').show(0);
			break;
			case 5:
                sound_player("exp6",false);
                pop_up_animals(4,'henny','hen_audio');
								$(".option").append("<img class='corincor right' src='"+preload.getResult("right").src+"' />");
								$(".option").append("<img class='corincor wrong' src='"+preload.getResult("wrong").src+"' />");
			$('.doggy').hide(0);
			$('.doggya').show(0);
			$('.goaty').hide(0);
			$('.goatya').show(0);
			break;
			case 6:
                sound_player("exp7",false);
                pop_up_animals(2,'cowy','cow_audio');
								$(".option").append("<img class='corincor right' src='"+preload.getResult("right").src+"' />");
								$(".option").append("<img class='corincor wrong' src='"+preload.getResult("wrong").src+"' />");
			$('.doggy').hide(0);
			$('.doggya').show(0);
			$('.henny').hide(0);
			$('.hennya').show(0);
			$('.goaty').hide(0);
			$('.goatya').show(0);
			break;
			case 7:
				$(".top_instruction").css({"left":"-100%"});
				$(".top_instruction").animate({"left":"0%"},800,"linear");
                sound_player("exp8",false);
                $('.table_div').css({"width": "61%",
														"top": "17%",
														"left": "28%",
														"height": "68%"});
	    ole.footerNotificationHandler.hideNotification();
			endpageex.endpage("");
			$('.dogbg').css({'background':'#54CDF7','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.goatbg').css({'background':'#FFC06E','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.henbg').css({'background':'#6CD877','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.cowbg').css({'background':'#F192F4','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			break;
		}
	}

// animals fade in and question fade in function
	function pop_up_animals(animal_numbers,animal_class,audio_animal){
		$(".top_instruction").css({"left":"-100%"});
		$(".top_instruction").animate({"left":"0%"},800,"linear");

		$nextBtn.hide(0);
		var pos_array=['pos1','pos2','pos3','pos4'];
		pos_array.shufflearray();
		for (var i = 0; i < 4; i++) {
			$('.option').eq(i).addClass(pos_array[i]);
		}
		$('.insideimage').hide(0);
		var regulator = animal_numbers;
		$('.goat1a,.goat2a,.goat3a,.cow1a,.cow2a,.hen1a,.hen2a,.hen3a,.hen4a,.doga').click(function(){

				if($(this).hasClass(animal_class)){
					regulator--;
					sound_player('camera');
					$(this).css('pointer-events','none');
					$(this).hide(500);
					$('.'+animal_class+'a').eq(regulator).show(500);
				}
				else{
					sound_player('incorrect');
				}
				if(regulator==0 && $(this).hasClass(animal_class)){
					sound_player(audio_animal);
					$(".top_instruction").css({"left":"-100%"});
					$(".top_instruction").html(data.string.commonquestion);
					$(".top_instruction").animate({"left":"0%"},800,"linear");
          $('.question_div').animate({'right':'0'},1000,function(){
						sound_player('click_on_the_correct_answer');
					});
					$('.top_instruction').text(data.string.play1text20);
				}
		});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(100) : "";
        });
    }
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

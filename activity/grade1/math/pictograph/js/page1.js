var soundAsset = $ref+"/audio_"+$lang + "/";
var soundAsset_numbers = $ref+"/audio_numbers/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'panda',
				imgclass:'panda'
			}]
		}]
	},

	// slide1
	{

		contentblockadditionalclass:'cream_bg',
		uppertextblock:[
			{
				textdata:data.string.p1text1,
				textclass:'top_instruction'
			},
			{
			textdata:data.string.p1text2,
			textclass:'number1'
		},
		{
			textdata:data.string.p1text3,
			textclass:'number2'
		},
		{
			textdata:data.string.p1text4,
			textclass:'number3'
		},
		{
			textdata:data.string.p1text5,
			textclass:'number4'
		},
		{
			textdata:data.string.p1text6,
			textclass:'number5'
		}],
		imgsrc_apple:imgpath+'apple.png',
		text1:data.string.p1text7,
		text2:data.string.p1text8,
		text3:data.string.p1text9,
		table:[{
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'tree_apple',
				imgclass:'tree_top'
			},
			{
				imgid:'trunk_apple',
				imgclass:'tree_tail'
			},
			{
				imgid:'basket',
				imgclass:'bucket'
			},
			{
				imgid:'apple',
				imgclass:'apple1'
			},
			{
				imgid:'apple',
				imgclass:'apple2'
			},
			{
				imgid:'apple',
				imgclass:'apple3'
			}]
		}]
	},

	// slide2
	{

		contentblockadditionalclass:'cream_bg',
		uppertextblock:[
			{
				textdata:data.string.p1text1a,
				textclass:'top_instruction'
			},
			{
			textdata:data.string.p1text2,
			textclass:'number1'
		},
		{
			textdata:data.string.p1text3,
			textclass:'number2'
		},
		{
			textdata:data.string.p1text4,
			textclass:'number3'
		},
		{
			textdata:data.string.p1text5,
			textclass:'number4'
		},
		{
			textdata:data.string.p1text6,
			textclass:'number5'
		}],
		imgsrc_apple:imgpath+'apple.png',
		imgsrc_orange:imgpath+'orange.png',
		text1:data.string.p1text7,
		text2:data.string.p1text8,
		text3:data.string.p1text9,
		table:[{
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'orangetree',
				imgclass:'tree_top'
			},
			{
				imgid:'trunk_apple',
				imgclass:'tree_tail'
			},
			{
				imgid:'basket',
				imgclass:'bucket'
			},
			{
				imgid:'orange',
				imgclass:'apple1'
			},
			{
				imgid:'orange',
				imgclass:'apple2'
			}]
		}]
	},

	// slide3
	{

		contentblockadditionalclass:'cream_bg',
		uppertextblock:[
			{
				textdata:data.string.p1text1b,
				textclass:'top_instruction'
			},
			{
			textdata:data.string.p1text2,
			textclass:'number1'
		},
		{
			textdata:data.string.p1text3,
			textclass:'number2'
		},
		{
			textdata:data.string.p1text4,
			textclass:'number3'
		},
		{
			textdata:data.string.p1text5,
			textclass:'number4'
		},
		{
			textdata:data.string.p1text6,
			textclass:'number5'
		}],
		imgsrc_apple:imgpath+'apple.png',
		imgsrc_orange:imgpath+'orange.png',
		imgsrc_mango:imgpath+'mango.png',
		text1:data.string.p1text7,
		text2:data.string.p1text8,
		text3:data.string.p1text9,
		table:[{
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'orangetree',
				imgclass:'tree_top'
			},
			{
				imgid:'trunk_apple',
				imgclass:'tree_tail'
			},
			{
				imgid:'basket',
				imgclass:'bucket'
			},
			{
				imgid:'mango',
				imgclass:'mango1'
			},
			{
				imgid:'mango',
				imgclass:'mango2'
			},
			{
				imgid:'mango',
				imgclass:'mango3'
			},
			{
				imgid:'mango',
				imgclass:'mango4'
			},
			{
				imgid:'mango',
				imgclass:'mango5'
			}]
		}]
	},


		// slide4
		{

			contentblockadditionalclass:'cream_bg',
			uppertextblock:[
				{
					textdata:data.string.p1text10,
					textclass:'top_instruction'
				},
				{
				textdata:data.string.p1text2,
				textclass:'number1'
			},
			{
				textdata:data.string.p1text3,
				textclass:'number2'
			},
			{
				textdata:data.string.p1text4,
				textclass:'number3'
			},
			{
				textdata:data.string.p1text5,
				textclass:'number4'
			},
			{
				textdata:data.string.p1text6,
				textclass:'number5'
			}],
			imgsrc_apple:imgpath+'apple.png',
			imgsrc_orange:imgpath+'orange.png',
			imgsrc_mango:imgpath+'mango.png',
			text1:data.string.p1text7,
			text2:data.string.p1text8,
			text3:data.string.p1text9,
			table:[{
			}]
		},

		// slide5
		{

			contentblockadditionalclass:'cream_bg',
			uppertextblock:[
				{
					textdata:data.string.p1text11,
					textclass:'top_instruction'
				},
				{
				textdata:data.string.p1text2,
				textclass:'number1'
			},
			{
				textdata:data.string.p1text3,
				textclass:'number2'
			},
			{
				textdata:data.string.p1text4,
				textclass:'number3'
			},
			{
				textdata:data.string.p1text5,
				textclass:'number4'
			},
			{
				textdata:data.string.p1text6,
				textclass:'number5'
			}],
			imgsrc_apple:imgpath+'apple.png',
			imgsrc_orange:imgpath+'orange.png',
			imgsrc_mango:imgpath+'mango.png',
			text1:data.string.p1text7,
			text2:data.string.p1text8,
			text3:data.string.p1text9,
			table:[{
			}]
		},


		// slide6
		{

			contentblockadditionalclass:'cream_bg',
			uppertextblock:[
				{
					textdata:data.string.p1text12,
					textclass:'top_instruction'
				},
				{
				textdata:data.string.p1text2,
				textclass:'number1'
			},
			{
				textdata:data.string.p1text3,
				textclass:'number2'
			},
			{
				textdata:data.string.p1text4,
				textclass:'number3'
			},
			{
				textdata:data.string.p1text5,
				textclass:'number4'
			},
			{
				textdata:data.string.p1text6,
				textclass:'number5'
			}],
			imgsrc_apple:imgpath+'apple.png',
			imgsrc_orange:imgpath+'orange.png',
			imgsrc_mango:imgpath+'mango.png',
			text1:data.string.p1text7,
			text2:data.string.p1text8,
			text3:data.string.p1text9,
			table:[{
			}]
		},

		// slide7
		{

			contentblockadditionalclass:'cream_bg',
			uppertextblock:[
				{
					textdata:data.string.p1text13,
					textclass:'top_instruction'
				},
				{
				textdata:data.string.p1text2,
				textclass:'number1'
			},
			{
				textdata:data.string.p1text3,
				textclass:'number2'
			},
			{
				textdata:data.string.p1text4,
				textclass:'number3'
			},
			{
				textdata:data.string.p1text5,
				textclass:'number4'
			},
			{
				textdata:data.string.p1text6,
				textclass:'number5'
			}],
			imgsrc_apple:imgpath+'apple.png',
			imgsrc_orange:imgpath+'orange.png',
			imgsrc_mango:imgpath+'mango.png',
			text1:data.string.p1text7,
			text2:data.string.p1text8,
			text3:data.string.p1text9,
			table:[{
			}]
		},

		// slide8
		{

			contentblockadditionalclass:'cream_bg',
			uppertextblock:[
				{
					textdata:data.string.p1text14,
					textclass:'top_instruction'
				},
				{
				textdata:data.string.p1text2,
				textclass:'number1'
			},
			{
				textdata:data.string.p1text3,
				textclass:'number2'
			},
			{
				textdata:data.string.p1text4,
				textclass:'number3'
			},
			{
				textdata:data.string.p1text5,
				textclass:'number4'
			},
			{
				textdata:data.string.p1text6,
				textclass:'number5'
			}],
			imgsrc_apple:imgpath+'apple.png',
			imgsrc_orange:imgpath+'orange.png',
			text1:data.string.p1text7,
			text2:data.string.p1text8,
			tablecompare1:[{
			}]
		},


		// slide9
		{

			contentblockadditionalclass:'cream_bg',
			uppertextblock:[
				{
					textdata:data.string.p1text15,
					textclass:'top_instruction'
				},
				{
				textdata:data.string.p1text2,
				textclass:'number1'
			},
			{
				textdata:data.string.p1text3,
				textclass:'number2'
			},
			{
				textdata:data.string.p1text4,
				textclass:'number3'
			},
			{
				textdata:data.string.p1text5,
				textclass:'number4'
			},
			{
				textdata:data.string.p1text6,
				textclass:'number5'
			}],
			imgsrc_apple:imgpath+'apple.png',
			imgsrc_mango:imgpath+'mango.png',
			text1:data.string.p1text7,
			text3:data.string.p1text9,
			tablecompare2:[{
			}]
		},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tree_apple", src: imgpath+'appletree_top.png', type: createjs.AbstractLoader.IMAGE},
			{id: "orangetree", src: imgpath+'orangetree_top.png', type: createjs.AbstractLoader.IMAGE},
			{id: "trunk_apple", src: imgpath+'appletree_trunk.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bananatrunk", src: imgpath+'bananatrunk.png', type: createjs.AbstractLoader.IMAGE},
			{id: "basket", src: imgpath+'basket.png', type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+'apple.png', type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+'orange.png', type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+'mango.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bananaleaf", src: imgpath+'bananaleaf.png', type: createjs.AbstractLoader.IMAGE},
			{id: "panda", src: imgpath+'panda.png', type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "tree_sound", src: soundAsset+"tree_rustle.ogg"},
			// {id: "one", src: soundAsset_numbers+"one.ogg"},
			// {id: "two", src: soundAsset_numbers+"two.ogg"},
			// {id: "three", src: soundAsset_numbers+"three.ogg"},
			// {id: "four", src: soundAsset_numbers+"four.ogg"},
			// {id: "five", src: soundAsset_numbers+"five.ogg"},
			{id: "s1p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1p10", src: soundAsset+"s1_p10.ogg"},
			{id: "sn1", src: soundAsset+"1.ogg"},
			{id: "sn2", src: soundAsset+"2.ogg"},
			{id: "sn3", src: soundAsset+"3.ogg"},
			{id: "sn4", src: soundAsset+"4.ogg"},
			{id: "sn5", src: soundAsset+"5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		// put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_player("s1p1",true);
				break;
			case 1:
				    sound_player("s1p2",false);
					var flag = 0;
					var show_fruite_flag=false;
					$('.insideimage').hide(0);
					var class_value = ['.apple1', '.apple2', '.apple3'];
					$('.tree_top,.tree_tail,.apple1, .applfalsee2, .apple3').click(function(){
					    if(flag <= 2)
					    {
									 sound_player('tree_sound');
									 $('.tree_top').removeClass('zoom_in').addClass('shaking');
									 $('.tree_top,.tree_tail,.apple1, .apple2, .apple3').css('pointer-events','none');
									 setTimeout(function(){
										 $('.tree_top').removeClass('shaking').addClass('zoom_in');
										 $(class_value[flag]).addClass('top_animate');
										 $('.tree_top,.tree_tail,.apple1, .apple2, .apple3').css('pointer-events','auto');
										 flag++;
									 },1000);
						 }
						 if(flag==2){
							 $('.tree_top,.tree_tail,.apple1, .apple2').css('pointer-events','none');
							 setTimeout(function(){
								 sound_player('one');
								 $('.apple1').hide(500);
		 						$('.one').show(500);
		 						sound_player("sn1",false);
		 					},2000);
							setTimeout(function(){
								sound_player('two');
								$('.apple2').hide(500);
								$('.two').show(500);
                                sound_player("sn2",false);
                            },3000);
							setTimeout(function(){
								sound_player('three');
								$('.apple3').hide(500);
								$('.three').show(500);
                                sound_player("sn3",true);
							},4000);
						}
						 });
			break;
			case 2:
                sound_player("s1p3",false);
                var flag = 0;
					var show_fruite_flag=false;
					$('.m1,.m2,.m3,.m4,.m5').hide(0);
					$('.o1,.o2').hide(0);
					var class_value = ['.apple1', '.apple2'];
					$('.tree_top,.tree_tail,.apple1, .apple2').click(function(){
							if(flag <= 1)
							{
									sound_player('tree_sound');
									 $('.tree_top').addClass('shaking').removeClass('zoom_in');
									 $('.tree_top,.tree_tail,.apple1, .apple2, .apple3').css('pointer-events','none');
									 setTimeout(function(){
										 $('.tree_top').removeClass('shaking').addClass('zoom_in');
										 $(class_value[flag]).addClass('top_animate');
										 $('.tree_top,.tree_tail,.apple1, .apple2, .apple3').css('pointer-events','auto');
										 flag++;
									 },1000);
						 }
						 if(flag==1){
							 $('.tree_top,.tree_tail,.apple1, .apple2').css('pointer-events','none');
							 setTimeout(function(){
								 sound_player('one');
							  $('.apple1').hide(500);
								$('.o1').show(500);
                                 sound_player("sn1",false);
                             },2000);
							setTimeout(function(){
								sound_player('two');
								$('.apple2').hide(500);
								$('.o2').show(500);
                                sound_player("sn2",true);
							},3000);
						}
						 });
				break;
			case 3:
				sound_player("s1p4",false);
						$('.bucket').css({"width": "20%",
												"top": "78%",
												"left": "71%",
												"z-index":"9999"});
						var flag = 0;
						var show_fruite_flag=false;
						$('.m1,.m2,.m3,.m4,.m5').hide(0);
						var class_value = ['.mango1', '.mango2','.mango3','.mango4','.mango5'];
						$('.tree_top,.tree_tail,.apple1, .apple2').click(function(){
								if(flag <= 4)
								{
										sound_player('tree_sound');
										 $('.tree_top').addClass('shaking').removeClass('zoom_in');
										 $('.tree_top,.tree_tail,.apple1, .apple2, .apple3').css('pointer-events','none');
										 setTimeout(function(){
											 $('.tree_top').removeClass('shaking').addClass('zoom_in');
											 $(class_value[flag]).addClass('top_animate');
											 $('.tree_top,.tree_tail,.apple1, .apple2, .apple3').css('pointer-events','auto');
											 flag++;
										 },1000);
							 }
							 if(flag==4){
								 $('.tree_top,.tree_tail,.apple1, .apple2').css('pointer-events','none');
								 setTimeout(function(){
									 sound_player('one');
								  $('.mango1').hide(500);
									$('.m1').show(500);
                                     sound_player("sn1",false);
                                 },2000);
								setTimeout(function(){
									sound_player('two');
									$('.mango2').hide(500);
									$('.m2').show(500);
                                    sound_player("sn2",false);
                                },3000);
								setTimeout(function(){
									sound_player('three');
									$('.mango3').hide(500);
									$('.m3').show(500);
                                    sound_player("sn3",false);
                                },4000);
								setTimeout(function(){
									sound_player('four');
									$('.mango4').hide(500);
									$('.m4').show(500);
                                    sound_player("sn4",false);
                                },5000);
								setTimeout(function(){
									sound_player('five');
									$('.mango5').hide(500);
									$('.m5').show(500);
                                    sound_player("sn5",true);
                                },6000);
							}
							 });
					break;
				break;
				case 4:
					sound_player("s1p5",true);
				$('.main_table').css({"width": "49%",
														"left": "27%"});
				$('.number1, .number2, .number3, .number4, .number5').css({"left":"23%"});
				$('.number3').css({"background": "#D72B27",
													"color": "white",
													"width": "4%",
													"border-radius": "50%","left":"22%"});
					$('.applerow').css({"background": "#FFE599",
			    "width": "100%",
			    "height": "100%",
			    "position": "absolute",
			    "left": "0",
			    "top": "0"});
					break;
				case 5:
                    sound_player("s1p6",true);
                    $('.main_table').css({"width": "49%",
														"left": "27%"});
				$('.number1, .number2, .number3, .number4, .number5').css({"left":"23%"});
				$('.number4').css({"background": "#FCB514",
													"color": "white",
													"width": "4%",
													"border-radius": "50%","left":"22%"});
					$('.orangerow').css({"background": "#FFE599",
					"width": "100%",
					"height": "100%",
					"position": "absolute",
					"left": "0",
					"top": "0"});
					nav_button_controls(1000);
					break;
				case 6:
                    sound_player("s1p7",true);
                    $('.main_table').css({"width": "49%",
														"left": "27%"});
				$('.number1, .number2, .number3, .number4, .number5').css({"left":"23%"});
				$('.number1').css({"background": "#F68D1E",
													"color": "white",
													"width": "4%",
													"border-radius": "50%","left":"22%"});
					$('.mangorow').css({"background": "#FFE599",
					"width": "100%",
					"height": "100%",
					"position": "absolute",
					"left": "0",
					"top": "0"});
				break;
				case 7:
                    sound_player("s1p8",true);

                    $('.main_table').css({"width": "49%",
														"left": "27%"});
				$('.number1, .number2, .number3, .number4, .number5').css({"left":"23%"});
				break;
				case 8:
                    sound_player("s1p9",true);
                    $('.main_table').css({"width": "49%",
														"left": "27%"});
				$('.number1, .number2, .number3, .number4, .number5').css({"left":"23%"});
				$('.insideimage').css("width", "22%");
				$('.applerow').css({"background": "#FFE599",
				"width": "100%",
				"height": "100%",
				"position": "absolute",
				"left": "0",
				"top": "0"});
				break;
				case 9:
                    sound_player("s1p10",true);
                    $('.main_table').css({"width": "49%",
														"left": "27%"});
				$('.number1, .number2, .number3, .number4, .number5').css({"left":"23%"});
				$('.insideimage').css("width", "22%");
				$('.mangorow').css({"background": "#FFE599",
				"width": "100%",
				"height": "100%",
				"position": "absolute",
				"left": "0",
				"top": "0"});
				break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(100) : "";
        });
    }
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

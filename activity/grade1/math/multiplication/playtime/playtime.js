var soundAsset_numbers = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide0
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg1",
			uppertextblock: [
					{
							textclass: "playtime",
							textdata: data.string.playtime
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							}]
			}]
	},

	// slide1
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "blue_bg",
			uppertextblock: [
					{
							textclass: "top_text",
							textdata: data.string.play1text1
					},
					{
							textclass: "mid_text",
							textdata: data.string.play1text2
					},
					{
							textclass: "options1 correct",
							textdata: data.string.play1text3
					},
					{
							textclass: "options2 incorrect",
							textdata: data.string.play1text4
					},
					{
							textclass: "right_bg",
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "pencil pencil1",
									imgid: "pencil",
									imgsrc: "",
							},
							{
									imgclass: "pencil pencil2",
									imgid: "pencil",
									imgsrc: "",
							},
							{
									imgclass: "threebox1",
									imgid: "threebox",
									imgsrc: "",
							},
							{
									imgclass: "threebox2",
									imgid: "threebox",
									imgsrc: "",
							}]
			}]
	},


	// slide2
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "blue_bg",
			uppertextblock: [
					{
							textclass: "top_text",
							textdata: data.string.play1text1
					},
					{
							textclass: "mid_text",
							textdata: data.string.play1text2
					},
					{
							textclass: "options2 correct",
							textdata: data.string.play1text8
					},
					{
							textclass: "options1 incorrect",
							textdata: data.string.play1text9
					},
					{
							textclass: "right_bg",
					}
			],
			imageblock:[{
					imagestoshow: [
						{
								imgclass: "pencil pencil1",
								imgid: "pencil",
								imgsrc: "",
						},
						{
								imgclass: "pencil pencil2",
								imgid: "pencil",
								imgsrc: "",
						},
						{
								imgclass: "threebox1",
								imgid: "threebox",
								imgsrc: "",
						},
						{
								imgclass: "threebox2",
								imgid: "threebox",
								imgsrc: "",
						}]
			}]
	},


		// slide3
		{
				contentnocenteradjust: true,
				contentblockadditionalclass: "blue_bg",
				uppertextblock: [
						{
								textclass: "top_text",
								textdata: data.string.play1text1
						},
						{
								textclass: "mid_text",
								textdata: data.string.play1text2
						},
						{
								textclass: "options2 correct",
								textdata: data.string.play1text10
						},
						{
								textclass: "options1 incorrect",
								textdata: data.string.play1text11
						},
						{
								textclass: "right_bg",
						}
				],
				imageblock:[{
						imagestoshow: [
							{
									imgclass: "pencil pencil1",
									imgid: "pencil",
									imgsrc: "",
							},
							{
									imgclass: "pencil pencil2",
									imgid: "pencil",
									imgsrc: "",
							},
							{
									imgclass: "threebox1",
									imgid: "threebox",
									imgsrc: "",
							},
							{
									imgclass: "threebox2",
									imgid: "threebox",
									imgsrc: "",
							}]
				}]
		},

		// slide4
		{
				contentnocenteradjust: true,
				contentblockadditionalclass: "blue_bg",
				uppertextblock: [
						{
								textclass: "top_text",
								textdata: data.string.play1text1
						},
						{
								textclass: "mid_text",
								textdata: data.string.play1text5
						},
						{
								textclass: "options1 correct",
								textdata: data.string.play1text6
						},
						{
								textclass: "options2 incorrect",
								textdata: data.string.play1text7
						},
						{
								textclass: "right_bg",
						}
				],
				imageblock:[{
						imagestoshow: [
							{
									imgclass: "marble1",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "marble2",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "marble3",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "fivebox1",
									imgid: "fivebox",
									imgsrc: "",
							},
							{
									imgclass: "fivebox2",
									imgid: "fivebox",
									imgsrc: "",
							},
							{
									imgclass: "fivebox3",
									imgid: "fivebox",
									imgsrc: "",
							}]
				}]
		},

		// slide5
		{
				contentnocenteradjust: true,
				contentblockadditionalclass: "blue_bg",
				uppertextblock: [
						{
								textclass: "top_text",
								textdata: data.string.play1text1
						},
						{
								textclass: "mid_text",
								textdata: data.string.play1text5
						},
						{
								textclass: "options2 correct",
								textdata: data.string.play1text12
						},
						{
								textclass: "options1 incorrect",
								textdata: data.string.play1text13
						},
						{
								textclass: "right_bg",
						}
				],
				imageblock:[{
						imagestoshow: [
							{
									imgclass: "marble1",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "marble2",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "marble3",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "fivebox1",
									imgid: "fivebox",
									imgsrc: "",
							},
							{
									imgclass: "fivebox2",
									imgid: "fivebox",
									imgsrc: "",
							},
							{
									imgclass: "fivebox3",
									imgid: "fivebox",
									imgsrc: "",
							}]
				}]
		},

		// slide6
		{
				contentnocenteradjust: true,
				contentblockadditionalclass: "blue_bg",
				uppertextblock: [
						{
								textclass: "top_text",
								textdata: data.string.play1text1
						},
						{
								textclass: "mid_text",
								textdata: data.string.play1text5
						},
						{
								textclass: "options2 correct",
								textdata: data.string.play1text14
						},
						{
								textclass: "options1 incorrect",
								textdata: data.string.play1text15
						},
						{
								textclass: "right_bg",
						}
				],
				imageblock:[{
						imagestoshow: [
							{
									imgclass: "marble1",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "marble2",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "marble3",
									imgid: "marble",
									imgsrc: "",
							},
							{
									imgclass: "fivebox1",
									imgid: "fivebox",
									imgsrc: "",
							},
							{
									imgclass: "fivebox2",
									imgid: "fivebox",
									imgsrc: "",
							},
							{
									imgclass: "fivebox3",
									imgid: "fivebox",
									imgsrc: "",
							}]
				}]
		},



		// slide7
		{
				contentnocenteradjust: true,
				contentblockadditionalclass: "blue_bg",
				uppertextblock: [
						{
								textclass: "top_text",
								textdata: data.string.play1text1
						},
						{
								textclass: "mid_text",
								textdata: data.string.play1text16
						},
						{
								textclass: "options1 correct",
								textdata: data.string.play1text17
						},
						{
								textclass: "options2 incorrect",
								textdata: data.string.play1text18
						},
						{
								textclass: "right_bg",
						}
				],
				imageblock:[{
						imagestoshow: [
							{
									imgclass: "pencil pencil1",
									imgid: "dog01",
									imgsrc: "",
							},
							{
									imgclass: "pencil pencil2",
									imgid: "dog02",
									imgsrc: "",
							},
							{
									imgclass: "threebox1",
									imgid: "fourbox",
									imgsrc: "",
							},
							{
									imgclass: "threebox2",
									imgid: "fourbox",
									imgsrc: "",
							}]
				}]
		},


		// slide8
		{
				contentnocenteradjust: true,
				contentblockadditionalclass: "blue_bg",
				uppertextblock: [
						{
								textclass: "top_text",
								textdata: data.string.play1text1
						},
						{
								textclass: "mid_text",
								textdata: data.string.play1text16
						},
						{
								textclass: "options1 incorrect",
								textdata: data.string.play1text20
						},
						{
								textclass: "options2 correct",
								textdata: data.string.play1text19
						},
						{
								textclass: "right_bg",
						}
				],
				imageblock:[{
						imagestoshow: [
							{
									imgclass: "pencil pencil1",
									imgid: "dog01",
									imgsrc: "",
							},
							{
									imgclass: "pencil pencil2",
									imgid: "dog02",
									imgsrc: "",
							},
							{
									imgclass: "threebox1",
									imgid: "fourbox",
									imgsrc: "",
							},
							{
									imgclass: "threebox2",
									imgid: "fourbox",
									imgsrc: "",
							}]
				}]
		},


			// slide9
			{
					contentnocenteradjust: true,
					contentblockadditionalclass: "blue_bg",
					uppertextblock: [
							{
									textclass: "top_text",
									textdata: data.string.play1text1
							},
							{
									textclass: "mid_text",
									textdata: data.string.play1text16
							},
							{
									textclass: "options2 correct",
									textdata: data.string.play1text21
							},
							{
									textclass: "options1 incorrect",
									textdata: data.string.play1text22
							},
							{
									textclass: "right_bg",
							}
					],
					imageblock:[{
							imagestoshow: [
								{
										imgclass: "pencil pencil1",
										imgid: "dog01",
										imgsrc: "",
								},
								{
										imgclass: "pencil pencil2",
										imgid: "dog02",
										imgsrc: "",
								},
								{
										imgclass: "threebox1",
										imgid: "fourbox",
										imgsrc: "",
								},
								{
										imgclass: "threebox2",
										imgid: "fourbox",
										imgsrc: "",
								}]
					}]
			},

			//slide10
			{
				contentblockadditionalclass:'bg_black',
				uppertextblock: [
						{
								textclass: "text-1",
								textdata: data.string.play1text23
						},
						{
								textclass: "text-2",
								textdata: data.string.play1text24
						},
						{
								textclass: "text-3",
								textdata: data.string.play1text25
						}
				],
				imageblock:[{
						imagestoshow: [
							{
									imgclass: "sixbox",
									imgid: "sixbox",
									imgsrc: "",
							},
							{
									imgclass: "fifteenbox",
									imgid: "fifteenbox",
									imgsrc: "",
							},
							{
									imgclass: "eightbox",
									imgid: "eightbox",
									imgsrc: "",
							},
							{
									imgclass: "char",
									imgid: "icon02",
									imgsrc: "",
							}]
				}]
			},


			//slide11
			{
				contentblockadditionalclass:'bg_black',
				imageblock:[{
						imagestoshow: [
							{
									imgclass: "sixbox",
									imgid: "sixbox",
									imgsrc: "",
							},
							{
									imgclass: "fifteenbox",
									imgid: "fifteenbox",
									imgsrc: "",
							},
							{
									imgclass: "eightbox",
									imgid: "eightbox",
									imgsrc: "",
							},
							{
									imgclass: "char",
									imgid: "icon02",
									imgsrc: "",
							}]
				}]
			}





];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var endpageex =  new EndPageofExercise();
	var message =  data.string.play1text26;
	endpageex.init(5);
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble", src: imgpath+"marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "threebox", src: imgpath+"threebox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fivebox", src: imgpath+"fivebox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fourbox", src: imgpath+"fourbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog01", src: imgpath+"dog01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "icon02", src: imgpath+"icon02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog02", src: imgpath+"dog02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eightbox", src: imgpath+"eightbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fifteenbox", src: imgpath+"fifteenbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sixbox", src: imgpath+"xisbox.png", type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "yayy", src: soundAsset_numbers+"yayy.ogg"},
			{id: "sound_1", src: soundAsset_numbers+"ex.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		countNext==1?sound_player("sound_1"):"";
		// correct and incorrect actions for all
		$('.options1,.options2 ').click(function(){
			if($(this).hasClass('correct'))
			{
				switch (countNext) {
					case 1:
					$('.threebox1').addClass('movbox1_noadd');
					$('.threebox2').addClass('movbox2_noadd');
						break;
					case 2:
					$('.threebox1').addClass('movbox1');
					$('.threebox2').addClass('movbox2');
						break;
					case 3:
						$('.threebox1').css('background','#ff5244');
						setTimeout(function(){
							$('.threebox2').css('background','#ff5244');
						},500);
						break;
					case 4:
						$('.fivebox1').addClass('fivebox1_animation');
						$('.fivebox2').addClass('fivebox2_animation');
						$('.fivebox3').addClass('fivebox3_animation');
						break;
					case 5:
						$('.fivebox1').animate({'top':'38%'},1000);
						$('.fivebox2').animate({'top':'54%'},1000);
						break;
					case 6:
						$('.fivebox1').css({'background':'#00ff89'});
						setTimeout(function(){
							$('.fivebox3').css({'background':'#00ff89'});
						},500);
						setTimeout(function(){
							$('.fivebox2').css({'background':'#00ff89'});
						},1000);
						break;

						case 7:
						$('.threebox1').addClass('movbox1_noadd');
						$('.threebox2').addClass('movbox2_noadd');
							break;
						case 8:
						$('.threebox1').addClass('movbox1');
						$('.threebox2').addClass('movbox2');
							break;
						case 9:
							$('.threebox1').css('background','#ffe700');
							setTimeout(function(){
								$('.threebox2').css('background','#ffe700');
							},500);
							break;
					default:

				}
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-24%,66%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$('.options1,.options2 ').css({"pointer-events":"none"});
				$(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else {
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-24%,66%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				play_correct_incorrect_sound(0);
				$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
			}
		});
		switch(countNext) {
			case 0:
			sound_player('playtime');
			nav_button_controls(2000);
			break;

			case 2:
			case 8:
			$('.threebox1').css('top','40%');
			$('.threebox2').css('top','52%');
			// function moveboxes(){
			// 	$('.threebox1').addClass('movmarble1');
			// 	$('.threebox2').addClass('movmarble2');
			// 	$('.threebox2').addClass('movmarble3');
			// 	setTimeout(function(){
			// 		$('.threebox1').css('background','#ff5244');
			// 	},2000);
			// 	setTimeout(function(){
			// 		$('.threebox2').css('background','#ff5244');
			// 		$('.correct').html(6);
			// 		$('.incorrect').html(4);
			// 	},3000);
			//
			// }
			break;

			case 3:
			case 9:
			$('.threebox1').css('top','42%');
			$('.threebox2').css('top','50%');
			break;
			case 5:
			$('.fivebox1').css('top','34%');
			$('.fivebox2').css('top','58%');
			$('.fivebox3').css('left','80%');
			break;
			case 6:
			$('.fivebox1').css({'top':'38%'});
			$('.fivebox2').css({'top':'54%'});
			$('.fivebox3').css('left','80%');
			break;
			case 10:
				$('.text-1,.text-2,.text-3,.sixbox,.fifteenbox,.eightbox').hide(0);
				$('.char').css('left','-10%').animate({'left':'41%'},4000,function(){
					$('.sixbox').fadeIn(1500,function(){
							$('.text-3').fadeIn(1500,function(){
								$('.fifteenbox').fadeIn(1500,function(){
									$('.text-2').fadeIn(1500,function(){
										$('.eightbox').fadeIn(1500,function(){
											$('.text-1').fadeIn(1500,function(){
												nav_button_controls(100);
											});
										});
									});
								});
							});
					});
				});
				break;
				case 11:
				endpageex.endpage(message);
				$('.messagediv,.btnNavigationSong').hide(0);
				$('.char').animate({'left':'33%'},500,function(){
					$('.char').addClass('rotate1');
				});
				setTimeout(function(){
					$('.char').animate({'top':'81%'},500,function(){
						$('.char').css({'transform': 'rotate(90deg)'}).addClass('rotate2');
					});
				},1000);
				setTimeout(function(){
					$('.messagediv,.btnNavigationSong').fadeIn(500);
					sound_player('yayy');
				},6600);

				break;

		}
	}

// animals fade in and question fade in function
	function pop_up_animals(animal_numbers,animal_class,audio_animal){
		$nextBtn.hide(0);
		var pos_array=['pos1','pos2','pos3','pos4'];
		pos_array.shufflearray();
		for (var i = 0; i < 4; i++) {
			$('.option').eq(i).addClass(pos_array[i]);
		}
		$('.insideimage').hide(0);
		var regulator = animal_numbers;
		$('.goat1a,.goat2a,.goat3a,.cow1a,.cow2a,.hen1a,.hen2a,.hen3a,.hen4a,.doga').click(function(){

				if($(this).hasClass(animal_class)){
					regulator--;
					sound_player('camera');
					$(this).css('pointer-events','none');
					$(this).hide(500);
					$('.'+animal_class+'a').eq(regulator).show(500);
				}
				else{
					sound_player('incorrect');
				}
				if(regulator==0 && $(this).hasClass(animal_class)){
					sound_player(audio_animal);
					$('.question_div').animate({'right':'0'},500);
					$('.top_instruction').hide(500);
				}
		});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p3text1,
			textclass:'top_text'
		}],
		extratextblock:[{
			textclass:'tableCss'
		}],
	},
	// slide 2
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p3text2,
			textclass:'top_text'
		}],
		extratextblock:[{
			textclass:'tableCss'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bowl',
				imgclass:'basket1'
			},{
				imgid:'bowl',
				imgclass:'basket2'
			},{
				imgid:'bowl',
				imgclass:'basket3'
			}]
		}]
	},
	// slide 3
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p3text3,
			textclass:'top_text'
		}],
		extratextblock:[{
			textclass:'tableCss'
		}],
		flowerpot :[{
				potclass: "pot p_1",
			},{
				potclass: "pot p_2",
			},{
				potclass: "pot p_3",
			}],
	},
	// slide 4
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p3text4,
			textclass:'top_text'
		}],
		extratextblock:[{
			textclass:'tableCss'
		}],
		flowerpot :[{
				potclass: "pot p_1",
			},{
				potclass: "pot p_2",
			},{
				potclass: "pot p_3",
			}],
	},
	// slide 5
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p3text5,
			textclass:'top_text'
		}],
		extratextblock:[{
			textclass:'tableCss'
		}],
		flowerpot :[{
				potclass: "pot p_1",
			},{
				potclass: "pot p_2",
			},{
				potclass: "pot p_3",
			}],
		numbers:[{
			number_container:"numberContainer"
		}]
	},
	// slide 6
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p3text9,
			textclass:'top_text'
		},{
			textdata:data.string.p3text10,
			textclass:'two'
		},{
			textdata:data.string.p3text11,
			textclass:'four'
		},{
			textdata:data.string.p3text12,
			textclass:'six'
		}],
		flowerpot :[{
				potclass: "pot p_1",
			},{
				potclass: "pot p_2",
			},{
				potclass: "pot p_3",
			}],
		extratextblock:[{
				textclass:'tableCss'
			}],
		imageblock:[{
			imagestoshow:[{
				imgid:'line',
				imgclass:'line1'
			},{
				imgid:'line',
				imgclass:'line2'
			},{
				imgid:'line',
				imgclass:'line3'
			}]
		}]
	},
	// slide 7
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p3text13,
			textclass:'top_text'
		}],
		flowerpot :[{
				potclass: "pot p_1",
			},{
				potclass: "pot p_2",
			},{
				potclass: "pot p_3",
			}],
		extratextblock:[
		{
			datahighlightflag:true,
			datahighlightcustomclass:"onbTxt",
			textdata:data.string.p3text14_1,
			textclass:'eqntxt fadeIn0'
		}],
	},
	// slide 8
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p3text17,
			textclass:'top_text'
		}],
		extratextblock:[{
			textclass:'tableCss'
		}],
		flowerpot :[{
				potclass: "pot p_1",
			},{
				potclass: "pot p_2",
			},{
				potclass: "pot p_3",
			}],
		midtxt_box:[
		{
			boxtxt:[{
				textdata:data.string.p3text16,
				textclass:'boxtxt_2',
				datahighlightflag: true,
				datahighlightcustomclass:'green',
			}]
			}]
	},
	// slide 9
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.string.p2text18,
			textclass:'top_text'
		}],
		extratextblock:[{
			textclass:'tableCss'
		}],
		flowerpot :[{
				potclass: "pot p_1",
			},{
				potclass: "pot p_2",
			},{
				potclass: "pot p_3",
			}],
		midtxt_box:[
		{
			boxtxt:[{
				textdata:data.string.p3text18,
				textclass:'boxtxt_2_sec',
			}]
			}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	// var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bowl", src: imgpath+'bowl.png', type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+'apple.png', type: createjs.AbstractLoader.IMAGE},
			{id: "line", src: imgpath+'line-png-28.png', type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"new/s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "5", src: soundAsset+"5.ogg"},
			{id: "10", src: soundAsset+"10.ogg"},
			{id: "15", src: soundAsset+"15.ogg"},
			{id: "s3_p7_1", src: soundAsset+"s3_p7_1.ogg"},
			{id: "s3_p7_2", src: soundAsset+"s3_p7_2.ogg"},
			{id: "s3_p8", src: soundAsset+"s3_p8.ogg"},
			{id: "s3_p9", src: soundAsset+"s3_p9.ogg"},


			{id: "sound_0", src: soundAsset+"new/s3_p5-01.ogg"},
			{id: "sound_1", src: soundAsset+"new/s3_p5-02.ogg"},
			{id: "sound_2", src: soundAsset+"new/s3_p5-03.ogg"},
			{id: "sound_3", src: soundAsset+"new/s3_p5-04.ogg"},
			{id: "sound_4", src: soundAsset+"new/s3_p5-05.ogg"},
			{id: "sound_5", src: soundAsset+"new/s3_p5-06.ogg"},
			{id: "sound_6", src: soundAsset+"new/s3_p5-07.ogg"},
			{id: "sound_7", src: soundAsset+"new/s3_p5-08.ogg"},
			{id: "sound_8", src: soundAsset+"new/s3_p5-09.ogg"},
			{id: "sound_9", src: soundAsset+"new/s3_p5-10.ogg"},
			{id: "sound_10", src: soundAsset+"new/s3_p5-11.ogg"},
			{id: "sound_11", src: soundAsset+"new/s3_p5-12.ogg"},
			{id: "sound_12", src: soundAsset+"new/s3_p5-13.ogg"},
			{id: "sound_13", src: soundAsset+"new/s3_p5-14.ogg"},
			{id: "sound_14", src: soundAsset+"new/s3_p5-15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
        vocabcontroller.findwords(countNext);

        // put_speechbox_image(content, countNext);
		var pot_1 = $(".p_1");
		var pot_2 = $(".p_2");
		var pot_3 = $(".p_3");
		var numbers = $(".numberContainer");
		var flwrCLs = "flowers_in_pot_1";
		var flwrCLsSec = "flowers_in_pot_2";
		var flwrCLsThrd = "flowers_in_pot_3";

		function highlight_number(count){
			$(".number"+(count-1)).addClass("zoomin");
			$(".flower_"+count).addClass("zoomin");
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_"+(count-1));
			current_sound.play();
			current_sound.on("complete",function(){
				count++;
				if(count < 16)
					highlight_number(count);
					else {
						nav_button_controls(0);
					}
			});
		}
		var number = 1;
		function show_image(img_no){
			$(img_no[number]).removeClass("hidden");
			number++;
			if(number <= 5){
			setTimeout(function(){
				$(img_no[number]).removeClass("hidden");
				show_image(img_no);
			},500);
			}else
			number =1;
		}

		function potAndFlowerManager(potName,potClass,flwrcontName, flowerClasses, leftFst, leftSec, btm, width){
			potName.append("<img class = '"+potClass+"' src = '"+preload.getResult('bowl').src+"'>");
			for(var i = 1; i<= 5; i++){
				$(potName).append("<img class = '"+flwrcontName+"' src='"+preload.getResult('apple').src+"'/>");
			}
			var flwrNUm = $(flowerClasses);
			// alert(flwrNUm);
			var bottom = btm;
			var left = leftFst;
			var width = width;
			for(var i= 0;i<$(flwrNUm).length; i++){
				if(i == 3){
					bottom = 47;
					left = 15;
				}
				$($(flwrNUm)[i]).css({
					"position" : "absolute",
					"left" : left+"%",
					"bottom" : bottom+"%",
					"width" : width+"%"
				});
				left+=31;
			}
		}
		switch(countNext) {
			case 0:
			case 1:
				sound_player_nav("s3_p"+(countNext+1));
			break;
			case 2:
				sound_player("s3_p"+(countNext+1));
				potAndFlowerManager(pot_1, "pot1",flwrCLs,".flowers_in_pot_1", 0, 0,25, 30);
				potAndFlowerManager(pot_2, "pot1",flwrCLsSec,".flowers_in_pot_2", 0, 0,25, 30);
				potAndFlowerManager(pot_3, "pot1",flwrCLsThrd,".flowers_in_pot_3", 0, 0,25, 30);
				var img_no_arr= [];
				var img_no_arr_2= [];
				var img_no_arr_3= [];
				for(var i = 1; i<= 5; i++){
					$(".p_1 img:eq("+i+")").addClass("hidden");
					$(".p_2 img:eq("+i+")").addClass("hidden");
					$(".p_3 img:eq("+i+")").addClass("hidden");
					img_no_arr[i] = $(".p_1 img:eq("+i+")");
					img_no_arr_2[i] = $(".p_2 img:eq("+i+")");
					img_no_arr_3[i] = $(".p_3 img:eq("+i+")");
				}
				show_image(img_no_arr);
				setTimeout(function(){
					show_image(img_no_arr_2);
				},2300);
				setTimeout(function(){
					show_image(img_no_arr_3);
					nav_button_controls(2200);
				},4500);
			break;
			case 3:
				sound_player_nav("s3_p"+(countNext+1));
				potAndFlowerManager(pot_1, "pot1",flwrCLs,".flowers_in_pot_1", 0, 0,25, 30);
				potAndFlowerManager(pot_2, "pot1",flwrCLsSec,".flowers_in_pot_2", 0, 0,25, 30);
				potAndFlowerManager(pot_3, "pot1",flwrCLsThrd,".flowers_in_pot_3", 0, 0,25, 30);
			break;
			case 4:
				potAndFlowerManager(pot_1, "pot1",flwrCLs,".flowers_in_pot_1", 0, 0,25, 30);
				potAndFlowerManager(pot_2, "pot1",flwrCLsSec,".flowers_in_pot_2", 0, 0,25, 30);
				potAndFlowerManager(pot_3, "pot1",flwrCLsThrd,".flowers_in_pot_3", 0, 0,25, 30);
				for(var i = 1; i<= 5; i++){
					$(".p_1 img:eq("+i+")").addClass("flower_"+i);
					$(".p_2 img:eq("+i+")").addClass("flower_"+(i+5));
					$(".p_3 img:eq("+i+")").addClass("flower_"+(i+10));
				}
				// for numbers
				for(var i=15; i>=1; i--){
					numbers.prepend("<p class = 'numbers'>" +i+ "</p>");
				}
				for(var i = 0; i<= 15; i++){
					$(".numberContainer p:eq("+i+")").addClass("number"+i);
				}
				var number = $(".numbers");
				var top = 10;
				var left_num = 4;
				for(var j = 0; j<$(number).length;j++){
					if(j == 5){
						left_num+=5;
						}
						else if (j == 10) {
						left_num+=10;
						}
						$($(number)[j]).css({
							"position" : "absolute",
							"left" : left_num+"%",
							"top" : top+"%",
						});
						j>9?left_num+=7:left_num+=5;
				}
				createjs.Sound.stop();
				current_sound1 = createjs.Sound.play('s3_p5');
				current_sound1.play();
				current_sound1.on('complete',function(){
					highlight_number(1);
				});
			break;current_sound
			case 5:
				potAndFlowerManager(pot_1, "pot1",flwrCLs,".flowers_in_pot_1", 0, 0,25, 30);
				potAndFlowerManager(pot_2, "pot1",flwrCLsSec,".flowers_in_pot_2", 0, 0,25, 30);
				potAndFlowerManager(pot_3, "pot1",flwrCLsThrd,".flowers_in_pot_3", 0, 0,25, 30);$('.two,.four,.six').hide(0);
				createjs.Sound.stop();
				current_sound0 = createjs.Sound.play('s3_p6');
				current_sound0.play();
				current_sound0.on('complete', function(){
					createjs.Sound.stop();
					current_sound1 = createjs.Sound.play('5');
					current_sound1.play();
						$('.line3').animate({'width':'20%'},1000);
						$('.two').fadeIn(500);
						current_sound1.on("complete", function(){
							$('.line2').animate({'width':'50%'},1000);
							createjs.Sound.stop();
							current_sound2 = createjs.Sound.play('10');
							current_sound2.play();
							$('.four').fadeIn(500);
							current_sound2.on("complete", function(){
								$('.line1').animate({'width':'80%'},1000);
								createjs.Sound.stop();
								current_sound3 = createjs.Sound.play('15');
								current_sound3.play();
								$('.six').fadeIn(500);
								current_sound3.on('complete', function(){
									nav_button_controls(500);
								});
							});
						});
				});


			break;
			case 6:
				potAndFlowerManager(pot_1, "pot1",flwrCLs,".flowers_in_pot_1", 0, 0,25, 30);
				potAndFlowerManager(pot_2, "pot1",flwrCLsSec,".flowers_in_pot_2", 0, 0,25, 30);
				potAndFlowerManager(pot_3, "pot1",flwrCLsThrd,".flowers_in_pot_3", 0, 0,25, 30);
				var numArray = ['5', '+5', '5+5+5', '5+5+5=', '5+5+5=15'];
				var numCount = 0;
				$(".onbTxt").hide(0);
				createjs.Sound.stop();
				current_sound0 = createjs.Sound.play('s3_p7_1');
				current_sound0.play();
				current_sound0.on('complete',function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s3_p7_2');
					current_sound.play();
					$(".onbTxt:eq(0)").delay(400).fadeIn(500);
					$(".onbTxt:eq(1)").delay(1500).fadeIn(500);
					$(".onbTxt:eq(2)").delay(2500).fadeIn(500);
					$(".onbTxt:eq(3)").delay(3500).fadeIn(500);
					current_sound.on('complete',function(){
						nav_button_controls(100);
					});
				});
			break;
			case 7:
				sound_player_nav("s3_p"+(countNext+1));
				potAndFlowerManager(pot_1, "pot1",flwrCLs,".flowers_in_pot_1", 0, 0,25, 30);
				potAndFlowerManager(pot_2, "pot1",flwrCLsSec,".flowers_in_pot_2", 0, 0,25, 30);
				potAndFlowerManager(pot_3, "pot1",flwrCLsThrd,".flowers_in_pot_3", 0, 0,25, 30);
			break;
			case 8:
				sound_player_nav("s3_p"+(countNext+1));
				potAndFlowerManager(pot_1, "pot1",flwrCLs,".flowers_in_pot_1", 0, 0,25, 30);
				potAndFlowerManager(pot_2, "pot1",flwrCLsSec,".flowers_in_pot_2", 0, 0,25, 30);
				potAndFlowerManager(pot_3, "pot1",flwrCLsThrd,".flowers_in_pot_3", 0, 0,25, 30);
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

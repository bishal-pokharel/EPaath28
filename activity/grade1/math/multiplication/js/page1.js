var soundAsset = $ref+"/sounds/"+$lang+"/";
var soundAsset_numbers = $ref+"/audio_numbers/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'cover_page',
				imgclass:'bg_full'
			}]
		}]
	},

	// slide1
	{
		uppertextblock:[{
			textdata:data.string.p1text1,
			textclass:'top_text'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'bg_full'
			},{
				imgid:'basket01',
				imgclass:'basket1'
			},{
				imgid:'basket01',
				imgclass:'basket2'
			},{
				imgid:'basket01',
				imgclass:'basket3'
			}]
		}]
	},
	// slide2
	{
		uppertextblock:[{
			textdata:data.string.p1text2,
			textclass:'top_text'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'bg_full'
			},{
				imgid:'basket01',
				imgclass:'basket1'
			},{
				imgid:'basket01',
				imgclass:'basket2'
			},{
				imgid:'basket01',
				imgclass:'basket3'
			},{
				imgid:'puppy01',
				imgclass:'puppy01'
			},{
				imgid:'puppy02',
				imgclass:'puppy02'
			},{
				imgid:'puppy03',
				imgclass:'puppy03'
			},{
				imgid:'puppy04',
				imgclass:'puppy04'
			},{
				imgid:'puppy05',
				imgclass:'puppy05'
			},{
				imgid:'puppy06',
				imgclass:'puppy06'
			}]
		}]
	},
	// slide3
	{
		uppertextblock:[{
			textdata:data.string.p1text3,
			textclass:'top_text'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'bg_full'
			},{
				imgid:'basket01',
				imgclass:'basket1'
			},{
				imgid:'basket01',
				imgclass:'basket2'
			},{
				imgid:'basket01',
				imgclass:'basket3'
			},{
				imgid:'puppy01',
				imgclass:'puppy01'
			},{
				imgid:'puppy02',
				imgclass:'puppy02'
			},{
				imgid:'puppy03',
				imgclass:'puppy03'
			},{
				imgid:'puppy04',
				imgclass:'puppy04'
			},{
				imgid:'puppy05',
				imgclass:'puppy05'
			},{
				imgid:'puppy06',
				imgclass:'puppy06'
			}]
		}]
	},
	// slide4
	{
		uppertextblock:[{
			textdata:data.string.p1text4,
			textclass:'top_text'
		},{
			textdata:data.string.p1text5,
			textclass:'count_text1'
		},{
			textdata:data.string.p1text6,
			textclass:'count_text2'
		},{
			textdata:data.string.p1text7,
			textclass:'count_text3'
		},{
			textdata:data.string.p1text8,
			textclass:'count_text4'
		},{
			textdata:data.string.p1text9,
			textclass:'count_text5'
		},{
			textdata:data.string.p1text10,
			textclass:'count_text6'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'bg_full'
			},{
				imgid:'basket01',
				imgclass:'basket1'
			},{
				imgid:'basket01',
				imgclass:'basket2'
			},{
				imgid:'basket01',
				imgclass:'basket3'
			},{
				imgid:'puppy01',
				imgclass:'puppy01'
			},{
				imgid:'puppy02',
				imgclass:'puppy02'
			},{
				imgid:'puppy03',
				imgclass:'puppy03'
			},{
				imgid:'puppy04',
				imgclass:'puppy04'
			},{
				imgid:'puppy05',
				imgclass:'puppy05'
			},{
				imgid:'puppy06',
				imgclass:'puppy06'
			}]
		}]
	},

	// slide5
	{
		uppertextblock:[{
			textdata:data.string.p1text4_1,
			textclass:'top_text'
		},{
			textdata:data.string.p1text6,
			textclass:'two'
		},{
			textdata:data.string.p1text8,
			textclass:'four'
		},{
			textdata:data.string.p1text10,
			textclass:'six'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'bg_full'
			},{
				imgid:'basket01',
				imgclass:'basket1'
			},{
				imgid:'basket01',
				imgclass:'basket2'
			},{
				imgid:'basket01',
				imgclass:'basket3'
			},{
				imgid:'puppy01',
				imgclass:'puppy01'
			},{
				imgid:'puppy02',
				imgclass:'puppy02'
			},{
				imgid:'puppy03',
				imgclass:'puppy03'
			},{
				imgid:'puppy04',
				imgclass:'puppy04'
			},{
				imgid:'puppy05',
				imgclass:'puppy05'
			},{
				imgid:'puppy06',
				imgclass:'puppy06'
			},{
				imgid:'line',
				imgclass:'line1'
			},{
				imgid:'line',
				imgclass:'line2'
			},{
				imgid:'line',
				imgclass:'line3'
			}]
		}]
	}



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	// var shuffledcolors=colors.shufflearray();
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "basket01", src: imgpath+'basket02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "puppy01", src: imgpath+'puppy01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "puppy02", src: imgpath+'puppy02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "puppy03", src: imgpath+'puppy03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "puppy04", src: imgpath+'puppy04.png', type: createjs.AbstractLoader.IMAGE},
			{id: "puppy05", src: imgpath+'puppy04.png', type: createjs.AbstractLoader.IMAGE},
			{id: "puppy06", src: imgpath+'puppy02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "line", src: imgpath+'line-png-28.png', type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+'cover_page.png', type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "dog", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"new/s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p6_1", src: soundAsset+"new/s1_p6.ogg"},
			{id: "s1_p5-01", src: soundAsset+"new/s1_p5-01.ogg"},
			{id: "s1_p5-02", src: soundAsset+"new/s1_p5-02.ogg"},
			{id: "s1_p5-03", src: soundAsset+"new/s1_p5-03.ogg"},
			{id: "s1_p5-04", src: soundAsset+"new/s1_p5-04.ogg"},
			{id: "s1_p5-05", src: soundAsset+"new/s1_p5-05.ogg"},
			{id: "s1_p5-06", src: soundAsset+"new/s1_p5-06.ogg"},
			{id: "s1_p6-01", src: soundAsset+"new/s1_p6-01.ogg"},
			{id: "s1_p6-02", src: soundAsset+"new/s1_p6-02.ogg"},
			{id: "s1_p6-03", src: soundAsset+"new/s1_p6-03.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		// put_speechbox_image(content, countNext);


        vocabcontroller.findwords(countNext);

		switch(countNext) {
			case 0:
				sound_player_nav("s1_p"+(countNext+1));
			break;
			case 1:
				sound_player_nav("s1_p"+(countNext+1));
			break;
			case 2:
				sound_player("s1_p"+(countNext+1));
				$('.puppy01,.puppy02,.puppy03,.puppy04,.puppy05,.puppy06').hide(0);
				$('.puppy01').fadeIn(500,function(){
					$('.puppy02').fadeIn(500,function(){
						$('.puppy03').fadeIn(500,function(){
							$('.puppy04').fadeIn(500,function(){
								$('.puppy05').fadeIn(500,function(){
									$('.puppy06').fadeIn(500,function(){
										nav_button_controls(500);
									});
								});
							});
						});
					});
				});
			break;
			case 3:
				sound_player_nav("s1_p"+(countNext+1));
			break;
				case 4:
				createjs.Sound.stop();
				current_sound1 = createjs.Sound.play('s1_p5');
				current_sound1.play();
				// $('.count_text1,.puppy01').addClass('zoomin');
				current_sound1.on("complete", function(){
					// $('.count_text1,.puppy01').removeClass('zoomin');
					$('.count_text1,.puppy01').addClass('zoomin');
					createjs.Sound.stop();
					current_sound2 = createjs.Sound.play('s1_p5-01');
					current_sound2.play();
					current_sound2.on("complete", function(){
						$('.count_text1,.puppy01').removeClass('zoomin');
						$('.count_text2,.puppy02').addClass('zoomin');
						createjs.Sound.stop();
						current_sound3 = createjs.Sound.play('s1_p5-02');
						current_sound3.play();
						current_sound3.on("complete", function(){
							$('.count_text2,.puppy02').removeClass('zoomin');
							$('.count_text3,.puppy03').addClass('zoomin');
							createjs.Sound.stop();
							current_sound4 = createjs.Sound.play('s1_p5-03');
							current_sound4.play();
							current_sound4.on("complete", function(){
								$('.count_text3,.puppy03').removeClass('zoomin');
								$('.count_text4,.puppy04').addClass('zoomin');
								createjs.Sound.stop();
								current_sound5 = createjs.Sound.play('s1_p5-04');
								current_sound5.play();
								current_sound5.on("complete", function(){
									$('.count_text4,.puppy04').removeClass('zoomin');
									$('.count_text5,.puppy05').addClass('zoomin');
									createjs.Sound.stop();
									current_sound6 = createjs.Sound.play('s1_p5-05');
									current_sound6.play();
									current_sound6.on("complete", function(){
										$('.count_text5,.puppy05').removeClass('zoomin');
										$('.count_text6,.puppy06').addClass('zoomin');
										createjs.Sound.stop();
										current_sound7 = createjs.Sound.play('s1_p5-06');
										current_sound7.play();
										current_sound7.on("complete",function(){
											$('.count_text6,.puppy06').removeClass('zoomin');
											nav_button_controls(100);
										});
									});
								});
							});
						});
					});
				});
				break;
				case 5:
							$('.two,.four,.six').hide(0);
							createjs.Sound.stop();
							current_sound0 = createjs.Sound.play('s1_p6_1');
							current_sound0.play();
							current_sound0.on('complete',function(){
								createjs.Sound.stop();
								current_sound1 = createjs.Sound.play('s1_p6-01');
								current_sound1.play();
								$('.line3').animate({'width':'20%'},1000);
								$('.two').fadeIn(500);
								current_sound1.on("complete", function(){
									$('.line2').animate({'width':'50%'},1000);
									createjs.Sound.stop();
									current_sound2 = createjs.Sound.play('s1_p6-02');
									current_sound2.play();
									$('.four').fadeIn(500);
									current_sound2.on("complete", function(){
										$('.line1').animate({'width':'80%'},1000);
										createjs.Sound.stop();
										current_sound3 = createjs.Sound.play('s1_p6-03');
										current_sound3.play();
										$('.six').fadeIn(500);
										current_sound3.on("complete", function(){
										nav_button_controls(100);
										});
									});
								});
							});
							break;
					default:
					nav_button_controls(100);
					break;
		}
	}
	function nav_button_controls(delay_ms)
{
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

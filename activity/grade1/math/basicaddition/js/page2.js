var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p2text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rumi",
                    imgclass: "relativecls img1",
                    imgid: 'rumiImg',
                    imgsrc: ""
                },

            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                svgclass:"zoomInEffect",
                handclass:"relativecls centertext"
            },
        ]
    },
//    SLIDE 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p2text2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rumi",
                    imgclass: "relativecls img1",
                    imgid: 'rumiImg',
                    imgsrc: ""
                },

            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p2text3
            },
            {
                textdiv:"countmsg",
                textclass: "content0 centertext",
                textdata: data.string.countmsg
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rumi",
                    imgclass: "relativecls img1",
                    imgid: 'rumiImg',
                    imgsrc: ""
                },
                {
                    imgdiv:"handicon",
                    imgclass: "relativecls img2",
                    imgid: 'handImg',
                    imgsrc: ""
                }

            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p2text3
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rumi",
                    imgclass: "relativecls img1",
                    imgid: 'rumiImg',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p2text4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rumi",
                    imgclass: "relativecls img1",
                    imgid: 'rumiImg',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"centertext relativecls "
            }
        ]
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p2text5
            },
            {
                textdiv:"numfive numeric hide1",
                textclass: "chapter centertext",
                textdata: data.string.five
            },
            {
                textdiv:"numfive1 numeric hide1",
                textclass: "chapter centertext",
                textdata: data.string.five
            },
            {
                textdiv:"numten numeric hide1",
                textclass: "chapter centertext",
                textdata: data.string.ten
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rumi",
                    imgclass: "relativecls img1",
                    imgid: 'rumiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "plus hide1",
                    imgclass: "relativecls img2",
                    imgid: 'plusImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "equalto hide1",
                    imgclass: "relativecls img3",
                    imgid: 'equaltoImg',
                    imgsrc: ""
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'lefthandsvg',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'righthandsvg',
                handclass:"relativecls centertext"
            },
        ]
    },
//                    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [

            {
                textdiv:"text3",
                textclass: "content centertext",
                textdata: data.string.p2text6
            },

        ],
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "rumiImg", src: imgpath + "rumi.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lefthandImg", src: imgpath + "left_hand.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "righthandImg", src: imgpath + "right_hand.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "righthandImg1", src: imgpath + "right_hand1.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "handImg", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "plusImg", src:  imgpath + "plush.png", type: createjs.AbstractLoader.IMAGE},
            {id: "equaltoImg", src: imgpath + "equal.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_1", src: soundAsset + "S2_P1.ogg"},
            {id: "sound_2", src: soundAsset + "S2_P2.ogg"},
            {id: "sound_3_1", src: soundAsset + "S2_P3_1.ogg"},
            {id: "sound_3_2", src: soundAsset + "S2_P3_2.ogg"},
            {id: "sound_5", src: soundAsset + "S2_P5.ogg"},
            {id: "sound_6", src: soundAsset + "S2_P6.ogg"},
            {id: "sound_7", src: soundAsset + "S2_P7.ogg"},
            {id: "sound_n1", src: soundAsset + "1.ogg"},
            {id: "sound_n2", src: soundAsset + "2.ogg"},
            {id: "sound_n3", src: soundAsset + "3.ogg"},
            {id: "sound_n4", src: soundAsset + "4.ogg"},
            {id: "sound_n5", src: soundAsset + "5.ogg"},
            {id: "sound_n6", src: soundAsset + "6.ogg"},
            {id: "sound_n7", src: soundAsset + "7.ogg"},
            {id: "sound_n8", src: soundAsset + "8.ogg"},
            {id: "sound_n9", src: soundAsset + "9.ogg"},
            {id: "sound_n10", src: soundAsset + "10.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
       
        
        switch (countNext) {
            case 0:
                sound_player("sound_1",true);
                loadsvg('#lefthandsvg',"lefthandImg");
                break;
            case 1:
                sound_player("sound_2",true);
                loadsvg('#lefthandsvg',"lefthandImg");
                setTimeout(function(){
                    loadsvg('#righthandsvg',"righthandImg1",true);
                },1500);
                break;
            case 2:
                clearTimeout(setTime);
                sound_player("sound_3_1",false)
                loadsvg('#lefthandsvg',"lefthandImg");
                loadsvg('#righthandsvg',"righthandImg1");
                $(".countmsg,.handicon").click(function(){
                    createjs.Sound.stop();
                    countNext++;
                    generaltemplate();
                });
                break;
            case 3:
                loadsvg('#lefthandsvg',"lefthandImg");
                loadsvg('#righthandsvg',"righthandImg");
                break;
            case 4:
                sound_player("sound_5",true);
                loadsvg('#righthandsvg',"righthandImg");
                loadsvg('#lefthandsvg',"lefthandImg");
                break;
            case 5:
                sound_player("sound_6",true);
                loadsvg('#lefthandsvg',"lefthandImg");
                loadsvg('#righthandsvg',"righthandImg");
                $(".hide1").hide()
                setTimeout(function () {
                    $(".numfive").delay(1000).fadeIn();
                    $(".plus").delay(2000).fadeIn();
                    $(".numfive1").delay(3000).fadeIn();
                    $(".equalto").delay(4000).fadeIn();
                    $(".numten").delay(5000).fadeIn();
                },1000);
                break;
            case 6:
                clearTimeout(setTime);
                sound_player("sound_7",true);
                break;
            default:
                clearTimeout(setTime);
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function loadsvg(svgid,imgid,effect){
        var s = Snap(svgid);
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s.append(loadedFragment);
            clearTimeout(setTime);
            countNext==1? $("#lefthandsvg").addClass("reducewidth"):'';
            effect?$("#righthandsvg").addClass("zoomInEffect"):"";
            countNext>=2?$("#lefthandsvg").css({"left":"0%"}):"";
            if(countNext==3){
                $(".number").hide();
                var delaytime = 1000;
                for(var i=1;i<11;i++){
                    $(".num"+i).delay(delaytime).fadeIn(100);
                    delaytime = delaytime+1000;
                }
                var j = 1;
                setTime = setInterval(function(){
                    var soundid = "sound_n"+j;
                    console.log("soundid is"+soundid)
                    sound_player(soundid,j==10?true:false);
                    if(j==10){
                        clearInterval(setTime);
                    }
                    j++;
                },1000)

            }
            if(countNext == 5){
                $(".number").hide();
                $("#Layer_1,#Layer_2").animate({"width":"400px","height":"360px"},1000);
                $("#lefthandsvg,#righthandsvg").animate({"top":"40%"},1000);
                $("#righthandsvg").animate({"right":"9%"},1000);
            }

        });

    }
});

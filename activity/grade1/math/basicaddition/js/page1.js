var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "datafont centertext",
                textdata: data.string.chapter
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'covermainImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content centertext fadeInEffect",
                textdata: data.string.p1text1
            },
             {
                textdiv:"sagarone number hide1",
                textclass: "content centertext",
                textdata: data.string.one
            },
             {
                textdiv:"sagartwo number hide2",
                textclass: "content centertext",
                textdata: data.string.two
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "sagar",
                    imgclass: "relativecls img1",
                    imgid: 'sagarImg',
                    imgsrc: ""
                },
                {
                  imgdiv: "ball b1 hide1",
                  imgclass: "relativecls img2",
                  imgid: 'ballImg',
                  imgsrc: ""
                },
                {
                  imgdiv: "ball b2 hide2",
                  imgclass: "relativecls img3",
                  imgid: 'ballImg',
                  imgsrc: ""
                },

            ]
        }],
    },
    //slide 2
    {
            contentnocenteradjust: true,
            contentblockadditionalclass: "bg",
            textblock: [
                {
                    textdiv:"maintitle",
                    textclass: "content centertext fadeInEffect",
                    textdata: data.string.p1text2
                },
                 {
                    textdiv:"sagarone number",
                    textclass: "content centertext",
                    textdata: data.string.one
                },
                 {
                    textdiv:"sagartwo number",
                    textclass: "content centertext",
                    textdata: data.string.two
                },
                {
                    textdiv:"dadone number hide1",
                    textclass: "content centertext",
                    textdata: data.string.one
                },
                 {
                    textdiv:"dadtwo number hide2",
                    textclass: "content centertext",
                    textdata: data.string.two
                },


            ],
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "sagar",
                        imgclass: "relativecls img1",
                        imgid: 'sagarImg',
                        imgsrc: ""
                    },
                    {
                      imgdiv: "ball b1",
                      imgclass: "relativecls img2",
                      imgid: 'ballImg',
                      imgsrc: ""
                    },
                    {
                      imgdiv: "ball b2",
                      imgclass: "relativecls img3",
                      imgid: 'ballImg',
                      imgsrc: ""
                    },
                    {
                      imgdiv: "dad slideR",
                      imgclass: "relativecls img4",
                      imgid: 'dadImg',
                      imgsrc: ""
                    },
                    {
                      imgdiv: "ball b3 hide1",
                      imgclass: "relativecls img5",
                      imgid: 'ballImg',
                      imgsrc: ""
                    },
                    {
                      imgdiv: "ball b4 hide2",
                      imgclass: "relativecls img5",
                      imgid: 'ballImg',
                      imgsrc: ""
                    },



                ]
            }],
        },
    //slide 3
    {
                contentnocenteradjust: true,
                contentblockadditionalclass: "bg",
                textblock: [
                    {
                        textdiv:"maintitle",
                        textclass: "content centertext ",
                        textdata: data.string.p1text3
                    },
                     {
                        textdiv:"dialtxt",
                        textclass: "content centertext fadeInEffect",
                        textdata: data.string.p1text4
                    },


                ],
                 imageblock: [{
                        imagestoshow: [
                            {
                                imgdiv: "sagar1",
                                imgclass: "relativecls img1",
                                imgid: 'sagar1Img',
                                imgsrc: ""
                            },
                            {
                                imgdiv: "dialbox",
                                imgclass: "relativecls img2",
                                imgid: 'dialboxImg',
                                imgsrc: ""
                            },
                            {
                                imgdiv: "ball b5",
                                imgclass: "relativecls img3",
                                imgid: 'ballImg',
                                imgsrc: "",
                                textblock:[
                                    {
                                      imgcls:"content num1",
                                      imgtxt:data.string.one
                                    }
                                ]
                            },
                            {
                                imgdiv: "ball b6",
                                imgclass: "relativecls img4",
                                imgid: 'ballImg',
                                imgsrc: "",
                                textblock:[
                                    {
                                       imgcls:"content num2",
                                       imgtxt:data.string.two
                                     }
                                ]
                            },
                            {
                                imgdiv: "ball b7",
                                imgclass: "relativecls img5",
                                imgid: 'ballImg',
                                imgsrc: "",
                                textblock:[
                                    {
                                       imgcls:" content num3",
                                       imgtxt:data.string.three
                                    }
                                ]
                            },
                            {
                                imgdiv: "ball b8",
                                imgclass: "relativecls img6",
                                imgid: 'ballImg',
                                imgsrc: "",
                                textblock:[
                                   {
                                      imgcls:" content num4",
                                      imgtxt:data.string.four
                                   }
                                ]
                            },
                        ]
                    }],
            },
    //slide 4
     {
                    contentnocenteradjust: true,
                    contentblockadditionalclass: "bg",
                    textblock: [
                        {
                            textdiv:"maintitle",
                            textclass: "content centertext ",
                            textdata: data.string.p1text3
                        },
                         {
                            textdiv:"dialtxt",
                            textclass: "content centertext fadeInEffect",
                            textdata: data.string.p1text5
                        },


                    ],
                    imageblock: [{
                        imagestoshow: [
                            {
                                imgdiv: "sagar1",
                                imgclass: "relativecls img1",
                                imgid: 'sagar1Img',
                                imgsrc: ""
                            },
                            {
                                imgdiv: "dialbox",
                                imgclass: "relativecls img2",
                                imgid: 'dialboxImg',
                                imgsrc: ""
                            },
                            {
                                imgdiv: "ball b5",
                                imgclass: "relativecls img3",
                                imgid: 'ballImg',
                                imgsrc: "",
                                textblock:[
                                    {
                                      imgcls:"content num1",
                                      imgtxt:data.string.one
                                    }
                                ]
                            },
                            {
                                imgdiv: "ball b6",
                                imgclass: "relativecls img4",
                                imgid: 'ballImg',
                                imgsrc: "",
                                textblock:[
                                    {
                                       imgcls:"content num2",
                                       imgtxt:data.string.two
                                     }
                                ]
                            },
                            {
                                imgdiv: "ball b7",
                                imgclass: "relativecls img5",
                                imgid: 'ballImg',
                                imgsrc: "",
                                textblock:[
                                    {
                                       imgcls:" content num3",
                                       imgtxt:data.string.three
                                    }
                                ]
                            },
                            {
                                imgdiv: "ball b8",
                                imgclass: "relativecls img6",
                                imgid: 'ballImg',
                                imgsrc: "",
                                textblock:[
                                   {
                                      imgcls:" content num4",
                                      imgtxt:data.string.four
                                   }
                                ]
                            },
                        ]
                    }],
                },
    //slide 5
    {
                        contentnocenteradjust: true,
                        contentblockadditionalclass: "bg",
                        textblock: [
                            {
                                textdiv:"maintitle",
                                textclass: "content centertext ",
                                textdata: data.string.p1text3
                            },
                             {
                                textdiv:"dialtxt",
                                textclass: "content centertext fadeInEffect",
                                textdata: data.string.p1text6
                            },


                        ],
                        imageblock: [{
                            imagestoshow: [
                                {
                                    imgdiv: "sagar1",
                                    imgclass: "relativecls img1",
                                    imgid: 'sagar1Img',
                                    imgsrc: ""
                                },
                                {
                                    imgdiv: "dialbox",
                                    imgclass: "relativecls img2",
                                    imgid: 'dialboxImg',
                                    imgsrc: ""
                                },
                                {
                                    imgdiv: "twoball tb1 image1",
                                    imgclass: "relativecls img3",
                                    imgid: 'twoballImg',
                                    imgsrc: "",
                                    textblock:[
                                        {
                                          imgcls:"content num1",
                                          imgtxt:data.string.two
                                        }
                                    ]
                                },
                                {
                                    imgdiv: "plus image2",
                                    imgclass: "relativecls img4",
                                    imgid: 'plusImg',
                                    imgsrc: "",
                                },
                                {
                                    imgdiv: "twoball tb2 image3",
                                    imgclass: "relativecls img5",
                                    imgid: 'twoballImg',
                                    imgsrc: "",
                                    textblock:[
                                        {
                                          imgcls:"content num1",
                                          imgtxt:data.string.two
                                        }
                                    ]
                                },
                                {
                                    imgdiv: "equalto image4",
                                    imgclass: "relativecls img6",
                                    imgid: 'equalImg',
                                    imgsrc: "",
                                },
                                {
                                    imgdiv: "fourball image5",
                                    imgclass: "relativecls img7",
                                    imgid: 'fourballImg',
                                    imgsrc: "",
                                    textblock:[
                                       {
                                          imgcls:" content num4",
                                          imgtxt:data.string.four
                                       }
                                    ]
                                },
                            ]
                        }],
                    },
//                    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [

            {
                textdiv:"text3",
                textclass: "content centertext",
                textdata: data.string.p1text7
            },

        ],
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "covermainImg", src: imgpath + "cover_page.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagarImg", src: imgpath + "sagar01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ballImg", src: imgpath + "ball.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dadImg", src: imgpath + "dad.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg", src: imgpath + "text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagar1Img", src: imgpath + "sagar02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "twoballImg", src: imgpath + "twoball.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fourballImg", src: imgpath + "fourball.png", type: createjs.AbstractLoader.IMAGE},
            {id: "plusImg", src: imgpath + "plush.png", type: createjs.AbstractLoader.IMAGE},
            {id: "equalImg", src: imgpath + "equal.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_1", src: soundAsset + "S1_P1.ogg"},
            {id: "sound_2", src: soundAsset + "S1_P2.ogg"},
            {id: "sound_3", src: soundAsset + "S1_P3.ogg"},
            {id: "sound_4_1", src: soundAsset + "S1_P4_1.ogg"},
            {id: "sound_4_2", src: soundAsset + "S1_P4_2.ogg"},
            {id: "sound_5_1", src: soundAsset + "s1_p5_a.ogg"},
            {id: "sound_5_2", src: soundAsset + "s1_p5_b.ogg"},
            {id: "sound_5_3", src: soundAsset + "s1_p5_c.ogg"},
            {id: "sound_5_4", src: soundAsset + "s1_p5_d.ogg"},
            {id: "sound_5_5", src: soundAsset + "s1_p5_e.ogg"},
            {id: "sound_6", src: soundAsset + "S1_P6.ogg"},
            {id: "sound_6_1", src: soundAsset + "S1_P6_1.ogg"},
            {id: "sound_7", src: soundAsset + "S2_P7.ogg"},
            {id: "sound_8", src: soundAsset + "S1_P8.ogg"},
            {id: "sound_9", src: soundAsset + "S1_P9_1.ogg"},
            {id: "sound_9_1", src: soundAsset + "S1_P9_2.ogg"},
            {id: "sound_n1", src: soundAsset + "1.ogg"},
            {id: "sound_n2", src: soundAsset + "2.ogg"},
            {id: "sound_n3", src: soundAsset + "3.ogg"},
            {id: "sound_n4", src: soundAsset + "4.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                count = 0;
                $(".hide1,.hide2").hide();
                var array = ["sound_2","sound_n1","sound_n2"];
                playsoundinarray(array);
                 $(".hide1,.hide2").hide();
                 break;
              case 2:
                  $(".hide1,.hide2").hide();
                  count = 0;
                  var array = ["sound_3","sound_n1","sound_n2"];
                  playsoundinarray(array);
                  $(".hide1,.hide2").hide();
                 break;
            case 3:
                sound_player("sound_4_1",false);
                setTime = setTimeout(function(){
                    sound_player("sound_4_2",true)
                },4000);
                break;
            case 4:
                  $(".num1,.num2,.num3,.num4").hide();
                  createjs.Sound.stop();
                  current_sound1 = createjs.Sound.play('sound_5_1');
                  current_sound1.play();
                  current_sound1.on('complete', function () {
                    $(".num1").fadeIn(500);
                    current_sound2 = createjs.Sound.play('sound_5_2');
                    current_sound2.play();
                    current_sound2.on('complete', function () {
                      $(".num2").fadeIn(500);
                      current_sound3 = createjs.Sound.play('sound_5_3');
                      current_sound3.play();
                      current_sound3.on('complete', function () {
                        $(".num3").fadeIn(500);
                        current_sound4 = createjs.Sound.play('sound_5_4');
                        current_sound4.play();
                        current_sound4.on('complete', function () {
                          $(".num4").fadeIn(500);
                          current_sound5 = createjs.Sound.play('sound_5_5');
                          current_sound5.play();
                          current_sound5.on('complete', function () {
                            navigationcontroller(countNext, $total_page);
                          });
                        });
                      });
                    });
                  });
                   break;
              case 5:
                  sound_player("sound_6",true);
                  $(".image1,.image2,.image3,.image4,.image5").hide();
                  $(".image1").delay(1000).fadeIn(1000);
                  $(".image2").delay(2000).fadeIn(1000);
                  $(".image3").delay(3000).fadeIn(1000);
                  $(".image4").delay(4000).fadeIn(1000);
                  $(".image5").delay(5000).fadeIn(1000);
                  navigationcontroller(countNext, $total_page);
                  break;
            case 6:
                clearTimeout(setTime);
                sound_player("sound_7",true);
                break;
            default:
                clearTimeout(setTime);
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }

    function playsoundinarray(array){
        if(count>=array.length){
            navigationcontroller(countNext, $total_page);
        }
        else {
            createjs.Sound.stop();
            current_sound = createjs.Sound.play(array[count]);
            current_sound.play();
            current_sound.on('complete', function () {
                if (array.length >= count) {
                    count++;
                    $(".hide" + count).show();
                    playsoundinarray(array);
                }
            });
        }
    }
    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p3text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img1",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },

            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            }
        ]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p3text2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img1",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "didi",
                    imgclass: "relativecls img2",
                    imgid: 'didiImg',
                    imgsrc: ""
                },

            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p3text3
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img1",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "didi",
                    imgclass: "relativecls img2",
                    imgid: 'didiImg',
                    imgsrc: ""
                },

            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p3text3
            },
            {
                textdiv:"dial1",
                textclass: "content2 centertext",
                textdata: data.string.p3text4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img1",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "didi",
                    imgclass: "relativecls img2",
                    imgid: 'didiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg',
                    imgsrc: ""
                },

            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    //slide 4
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p3text3
            },
            {
                textdiv:"dial1",
                textclass: "content2 centertext",
                textdata: data.string.p3text5
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img1",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "didi",
                    imgclass: "relativecls img2",
                    imgid: 'didiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg',
                    imgsrc: ""
                },

            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content0 centertext",
                textdata: data.string.p3text3
            },
            {
                textdiv:"dial1",
                textclass: "content2 centertext",
                textdata: data.string.p3text6
            },
            {
                textdiv:"num9 number hide1",
                textclass: "chapter centertext",
                textdata: data.string.nine
            },
            {
                textdiv:"num4 number hide1",
                textclass: "chapter centertext",
                textdata: data.string.four
            },
            {
                textdiv:"num13 number hide1",
                textclass: "chapter centertext",
                textdata: data.string.thirteen
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img1",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img3",
                    imgid: 'dialboxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "plus hide1",
                    imgclass: "relativecls img4",
                    imgid: 'plusImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "equalto hide1",
                    imgclass: "relativecls img5",
                    imgid: 'equaltoImg',
                    imgsrc: ""
                },
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1",
            }
        ]
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "nitiImg", src: imgpath + "niti.png", type: createjs.AbstractLoader.IMAGE},
            {id: "didiImg", src: imgpath + "didi.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pencilImg", src: imgpath + "pencil.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialboxImg", src: imgpath + "text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "plusImg", src:  imgpath + "plush.png", type: createjs.AbstractLoader.IMAGE},
            {id: "equaltoImg", src: imgpath + "equal.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "S3_P1.ogg"},
            {id: "sound_2", src: soundAsset + "S3_P2.ogg"},
            {id: "sound_3", src: soundAsset + "S3_P3.ogg"},
            {id: "sound_4", src: soundAsset + "S3_P4_1.ogg"},
            {id: "sound_5", src: soundAsset + "S3_P5.ogg"},
            {id: "sound_6", src: soundAsset + "S3_P6.ogg"},
            {id: "sound_n1", src: soundAsset + "1.ogg"},
            {id: "sound_n2", src: soundAsset + "2.ogg"},
            {id: "sound_n3", src: soundAsset + "3.ogg"},
            {id: "sound_n4", src: soundAsset + "4.ogg"},
            {id: "sound_n5", src: soundAsset + "5.ogg"},
            {id: "sound_n6", src: soundAsset + "6.ogg"},
            {id: "sound_n7", src: soundAsset + "7.ogg"},
            {id: "sound_n8", src: soundAsset + "8.ogg"},
            {id: "sound_n9", src: soundAsset + "9.ogg"},
            {id: "sound_n10", src: soundAsset + "10.ogg"},
            {id: "sound_n11", src: soundAsset + "11.ogg"},
            {id: "sound_n12", src: soundAsset + "12.ogg"},
            {id: "sound_n13", src: soundAsset + "13.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
 
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_1",true);
                for(var i=0;i<9;i++){
                    $(".containercls").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls").append("<p class='penciltext"+i+"'>"+(i+1)+"</p>");
                }
                break;
            case 1:
                sound_player("sound_2",true);
                for(var i=0;i<9;i++){
                    $(".containercls").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls").append("<p class='penciltext"+i+"'>"+(i+1)+"</p>");
                }
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls1").append("<p class='penciltext"+i+"'>"+(i+1)+"</p>");
                }
                break;
            case 2:
                sound_player("sound_3",true);
                for(var i=0;i<9;i++){
                    $(".containercls").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls").append("<p class='penciltext"+i+"'>"+(i+1)+"</p>");
                }
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls1").append("<p class='penciltext"+i+"'>"+(i+1)+"</p>");
                }
                break;
            case 3:
                sound_player("sound_4",false);
                for(var i=0;i<9;i++){
                    $(".containercls").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls").append("<p class=' hidetext penciltext"+i+"'>"+(i+1)+"</p>");
                }
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls1").append("<p class='hidetext penciltext"+i+"'>"+(i+10)+"</p>");
                }
                setTimeout(function(){
                    var i=12;
                    (function shownum (i) {
                       setTime = setTimeout(function () {
                            $(".hidetext").eq(i).animate({"opacity":"1"},100);
                            if (++i) {
                                shownum(i);
                                sound_player("sound_n"+i,i==13?true:false);
                                if(i==13){
                                    clearTimeout(setTime);
                                }
                            }
                        }, 1000);
                    })(0);
                },2000);
                break;
            case 4:
                clearInterval(setTime);
                sound_player("sound_5",true);
                for(var i=0;i<9;i++){
                    $(".containercls").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls").append("<p class='  penciltext"+i+"'>"+(i+1)+"</p>");
                }
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls1").append("<p class=' penciltext"+i+"'>"+(i+10)+"</p>");
                }
                break;
            case 5:
                sound_player("sound_6",true);
                for(var i=0;i<9;i++){
                    $(".containercls").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls").append("<p class=' penciltext"+i+"'>"+(i+1)+"</p>");
                }
                for(var i=0;i<4;i++){
                    $(".containercls1").append("<img class='pencil"+i+"' src='"+preload.getResult('pencilImg').src+"'>");
                    $(".containercls1").append("<p class=' penciltext"+i+"'>"+(i+10)+"</p>");
                }
                $(".hide1").hide();
                $(".num9").delay(2000).fadeIn(100);
                $(".plus").delay(3000).fadeIn(100);
                $(".num4").delay(3500).fadeIn(100);
                $(".equalto").delay(4000).fadeIn(100);
                $(".num13").delay(4500).fadeIn(100);
                break;
            default:
                clearTimeout(setTime);
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page,countNext==5?true:false) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

});

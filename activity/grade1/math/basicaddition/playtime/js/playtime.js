var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"ptime",
                textclass: "chapter centertext",
                textdata: data.string.playtime
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: "coverpageImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "rhinodancingdiv",
                    imgclass: " relativecls rhinoimgdiv",
                    imgid: "rhinodance",
                    imgsrc: "",
                },
                {
                    imgdiv: "squirrelistening",
                    imgclass: " relativecls squirrelisteningimg",
                    imgid: "squirrel",
                    imgsrc: "",
                }]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"thread",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdivbottom",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"sundarihas fadeInEffect",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"sundarigot fadeInEffect show1",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"answer show3",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval',
                textdiv:"text1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval1',
                textdiv:"text2 show1",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3 show2",
                textclass: "content centertext",
                textdata: ""
            },
            {
                textdiv:"option opt1 show2",
                textclass: "content centertext",
                textdata: "",
            },
            {
                textdiv:"option opt2 show2",
                textclass: "content centertext",
                textdata: ""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "pencil",
                    imgclass: "relativecls img5",
                    imgid: "pencilImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "fruit",
                    imgclass: "relativecls img1",
                    imgid: "bananaImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "plus show1",
                    imgclass: "relativecls img2",
                    imgid: "plusImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "equalto show3",
                    imgclass: "relativecls img3",
                    imgid: "equaltoImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "notequalto show3",
                    imgclass: "relativecls img4",
                    imgid: "notequaltoImg",
                    imgsrc: "",
                }
             ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1 show1",
            },
            {
                divclass:"containercls2 show3",
            }
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"thread",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdivbottom",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"sundarihas fadeInEffect",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"sundarigot fadeInEffect show1",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"answer show3",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval',
                textdiv:"text1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval1',
                textdiv:"text2 show1",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3 show2",
                textclass: "content centertext",
                textdata: ""
            },
            {
                textdiv:"option opt1 show2",
                textclass: "content centertext",
                textdata: "",
            },
            {
                textdiv:"option opt2 show2",
                textclass: "content centertext",
                textdata: ""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "pencil",
                    imgclass: "relativecls img5",
                    imgid: "pencilImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "fruit",
                    imgclass: "relativecls img1",
                    imgid: "bananaImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "plus show1",
                    imgclass: "relativecls img2",
                    imgid: "plusImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "equalto show3",
                    imgclass: "relativecls img3",
                    imgid: "equaltoImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "notequalto show3",
                    imgclass: "relativecls img4",
                    imgid: "notequaltoImg",
                    imgsrc: "",
                }
             ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1 show1",
            },
            {
                divclass:"containercls2 show3",
            }
        ]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"thread",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdivbottom",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"sundarihas fadeInEffect",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"sundarigot fadeInEffect show1",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"answer show3",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval',
                textdiv:"text1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval1',
                textdiv:"text2 show1",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3 show2",
                textclass: "content centertext",
                textdata: ""
            },
            {
                textdiv:"option opt1 show2",
                textclass: "content centertext",
                textdata: "",
            },
            {
                textdiv:"option opt2 show2",
                textclass: "content centertext",
                textdata: ""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "pencil",
                    imgclass: "relativecls img5",
                    imgid: "pencilImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "fruit",
                    imgclass: "relativecls img1",
                    imgid: "bananaImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "plus show1",
                    imgclass: "relativecls img2",
                    imgid: "plusImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "equalto show3",
                    imgclass: "relativecls img3",
                    imgid: "equaltoImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "notequalto show3",
                    imgclass: "relativecls img4",
                    imgid: "notequaltoImg",
                    imgsrc: "",
                }
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1 show1",
            },
            {
                divclass:"containercls2 show3",
            }
        ]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"thread",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdivbottom",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"sundarihas fadeInEffect",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"sundarigot fadeInEffect show1",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"answer show3",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval',
                textdiv:"text1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval1',
                textdiv:"text2 show1",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3 show2",
                textclass: "content centertext",
                textdata: ""
            },
            {
                textdiv:"option opt1 show2",
                textclass: "content centertext",
                textdata: "",
            },
            {
                textdiv:"option opt2 show2",
                textclass: "content centertext",
                textdata: ""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "pencil",
                    imgclass: "relativecls img5",
                    imgid: "pencilImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "fruit",
                    imgclass: "relativecls img1",
                    imgid: "bananaImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "plus show1",
                    imgclass: "relativecls img2",
                    imgid: "plusImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "equalto show3",
                    imgclass: "relativecls img3",
                    imgid: "equaltoImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "notequalto show3",
                    imgclass: "relativecls img4",
                    imgid: "notequaltoImg",
                    imgsrc: "",
                }
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1 show1",
            },
            {
                divclass:"containercls2 show3",
            }
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"thread",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"leftdivbottom",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"sundarihas fadeInEffect",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"sundarigot fadeInEffect show1",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                textdiv:"answer show3",
                textclass: "content1 centertext",
                textdata: ""
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval',
                textdiv:"text1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.pt1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'dynamicval1',
                textdiv:"text2 show1",
                textclass: "content1 centertext",
                textdata: data.string.pt1_1
            },
            {
                textdiv:"text3 show2",
                textclass: "content centertext",
                textdata: ""
            },
            {
                textdiv:"option opt1 show2",
                textclass: "content centertext",
                textdata: "",
            },
            {
                textdiv:"option opt2 show2",
                textclass: "content centertext",
                textdata: ""
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "pencil",
                    imgclass: "relativecls img5",
                    imgid: "pencilImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "fruit",
                    imgclass: "relativecls img1",
                    imgid: "bananaImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "plus show1",
                    imgclass: "relativecls img2",
                    imgid: "plusImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "equalto show3",
                    imgclass: "relativecls img3",
                    imgid: "equaltoImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "notequalto show3",
                    imgclass: "relativecls img4",
                    imgid: "notequaltoImg",
                    imgsrc: "",
                }
            ]
        }],
        containerblock:[
            {
                divclass:"containercls",
            },
            {
                divclass:"containercls1 show1",
            },
            {
                divclass:"containercls2 show3",
            }
        ]
    },
//lastslide
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "monkeydancing",
                    imgclass: "relativecls img5",
                    imgid: "monkeydancingImg",
                    imgsrc: "",
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    var preload;
    var timeoutvar = null;
    var current_sound="";
    var sound="";
    var tempfirstarray = new Array();
    var tempsecondarray = new Array();
    var palindromarray = new Array();
    var endpageex =  new EndPageofExercise();
    var message = data.string.lastmsg;
    endpageex.init(3);
    loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
             {id: "coverpageImg", src:imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "sundariImg", src: imgpath+"sundari.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sundari1Img", src: imgpath+"sundari01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "redbox", src: imgpath+"box_red.png", type: createjs.AbstractLoader.IMAGE},
             {id: "greenbox", src: imgpath+"box_green.png", type: createjs.AbstractLoader.IMAGE},
             {id: "whitebox", src: imgpath+"box_white.png", type: createjs.AbstractLoader.IMAGE},
             {id: "bananaImg", src: imgpath+"banana.png", type: createjs.AbstractLoader.IMAGE},
             {id: "plusImg", src: imgpath+"pluswhite.png", type: createjs.AbstractLoader.IMAGE},
             {id: "equaltoImg", src: imgpath+"equalwhite.png", type: createjs.AbstractLoader.IMAGE},
             {id: "notequaltoImg", src: imgpath+"not_equal.png", type: createjs.AbstractLoader.IMAGE},
             {id: "pencilImg", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
             {id: "monkeydancingImg", src: imgpath+"monkeydancing.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "correct", src:"images/correct.png", type: createjs.AbstractLoader.IMAGE},
             {id: "incorrect", src:"images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},

             //sounds
              {id: "sound_1", src: soundAsset+"1.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    $refreshBtn.click(function(){
        templateCaller();
    });
    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        firstPagePlayTime(countNext);
        texthighlight($board);
        put_image(content, countNext);
        
        $(".option").append("<img class='corincor right' src='"+preload.getResult("correct").src+"'/>");
        $(".option").append("<img class='corincor wrong' src='"+preload.getResult("incorrect").src+"'/>");
        switch (countNext){
            case 0:

                navigationcontroller();
                break;
            case 1:
                var firstval = generaterandomnum([1,2,3,4,5],[0]);
                tempfirstarray.push(firstval);
                var secondval = generaterandomnum([1,2,3,4,5],[0]);
                tempsecondarray.push(secondval);
                createquesans(firstval,secondval);
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                var firstval = generaterandomnum([1,2,3,4,5],[0]);
                tempfirstarray.push(firstval);
                var secondval = generaterandomnum([1,2,3,4,5],[0]);
                tempsecondarray.push(secondval);
                if($.inArray(firstval+"+"+secondval,palindromarray)>-1 || $.inArray(secondval+"+"+firstval,palindromarray)>-1)
                        generaltemplate();
                else {
                    createquesans(firstval,secondval);
                }
                break;
            case 6:
                endpageex.endpage(message);
                 break;
            default:
                navigationcontroller();
                break;
        }
       $('.audiodiv').click(function(){
			sound_player(sound,false);
			});

    }


    function sound_player(sound_id,navigate,imgCls,imgSrc) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():"";
            if(imgCls && imgSrc){
                $("."+imgCls).attr("src",imgSrc);
            }
        });
    }



    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('imghintblock')) {
            var contentCount=content[count].imghintblock[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }
    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                // lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });



    function shufflehint(firstans,secondans) {
        var btndiv = $(".selectdiv");

        for (var i = btndiv.children().length; i >= 0; i--) {
             btndiv.append(btndiv.children().eq(Math.random() * i | 0));
        }
        btndiv.children().removeClass();
        var a = ["commonbtn firstdiv","commonbtn seconddiv","commonbtn thirddiv","commonbtn fourthdiv","commonbtn fifthdiv"]
        btndiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.text().trim()==firstans){
                $this.addClass("firstans")
            }
            if($this.text().trim()==secondans){
                $this.addClass("secondans")
            }
        });

    }

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                 $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1 show2","option opt2 show2"];
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }
    function generaterandomnum(array,excludenum){
        for(var i = 0; i<excludenum.length;i++) {
            array.splice(array.indexOf(excludenum[i]), 1);
        }
        var random = array[Math.floor(Math.random() * array.length)];
        console.log("random val"+countNext+" coumtnext "+random);
        return random;
    }

    function createquesans(firstval,secondval){
        $(".show1,.show2,.show3").hide();
        palindromarray.push(firstval + "+" + secondval);
        palindromarray.push(secondval + "+" + firstval);
        console.log(palindromarray)
        var option1 = firstval + secondval;
        var option2 = generaterandomnum([1,2,3,4,5,6,7,8],[option1]);
        //assign value in question and option
        for(var i=0;i<firstval;i++){
            $(".containercls").append("<img class='whitebox"+i+"' src='"+preload.getResult('whitebox').src+"'>");
        }
        $(".containercls1").prepend("<img class='sundari s2' src='"+preload.getResult('sundariImg').src+"'>");
        for(var i=0;i<secondval;i++){
            $(".containercls1").append("<img class='whitebox"+i+"' src='"+preload.getResult('whitebox').src+"'>");
        }
        $(".text3 p").text(firstval +" + "+secondval+" = ?");
        $(".sundarihas p").text(firstval);

        if(firstval==1){
            firstval = firstval+" "+data.string.pencil;
        }
        else{
            firstval = firstval+" "+data.string.pencils;
        }
        $(".dynamicval").text(firstval);
        $(".dynamicval1,.sundarigot p").text(secondval);
        $(".opt1 p").text(option1);
        $(".opt2 p").text(option2);
        shufflehint();
        $(".containercls").prepend("<img class='sundari s1' src='"+preload.getResult('sundariImg').src+"'>");
        $(".show1").delay(1000).fadeIn(1000);
        $(".s1").delay(1000).fadeOut(1000);
        $(".show2").delay(2000).fadeIn(1000);


        $(".option").click(function(){
            $(".show3").show();
            $(".containercls2 img").remove();
            if($(this).find("p").text()==option1) {
                play_correct_incorrect_sound(1);
                $(this).children(".right").show(0);
                $(this).addClass(" correctans");
                $(".option").addClass("avoid-clicks");
                $(".containercls2").prepend("<img class='sundari s3' src='"+preload.getResult('sundari1Img').src+"'>");
                for(var i=0;i<option1;i++){
                    $(".containercls2").append("<img class='greenbox"+i+"' src='"+preload.getResult('greenbox').src+"'>");
                }
                $(".s2").fadeOut(1000);
                $(".answer p").text(option1);
                $(".notequalto").hide();
                var heightval = Math.round(
                    $('.containercls2').height() /
                    $('.coverboardfull').height() * 100
                );
                var heightsundari = Math.round(
                    $('.s3').height() /
                    $('.coverboardfull').height() * 100);
                $(".fruit").animate({"top":(Math.abs(100 - (heightval+heightsundari))-3)+"%"},1000);
                $(".thread").animate({"height":(Math.abs(100 - (heightval+heightsundari))-2)+"%"},1000);
                navigationcontroller();
            }
            else{
                play_correct_incorrect_sound(0);
                $(this).children(".wrong").show(0);
                $(this).addClass("avoid-clicks wrongans");
                for(var i=0;i<option2;i++){
                    $(".containercls2").append("<img class='redbox"+i+"' src='"+preload.getResult('redbox').src+"'>");
                }
                $(".equalto").hide();
            }
        });
    }
});

var imgpath = $ref+"/playtime/images/";
var soundAsset = $ref + "/sounds/"+$lang+"/";
var content = [
  // slide1
  {
    contentblockadditionalclass: "bg",
    extratextblock: [{
        	textclass: "playtime",
            textdata: data.string.pt
    }],
    imageblock:[{
      imagestoshow: [
        {
            imgclass: "rhinodance",
            imgid: "rhinodance",
            imgsrc: "",
        },{
            imgclass: "squirreldance",
            imgid: "squirreldance",
            imgsrc: "",
        },{
            imgclass: "ptbg",
            imgid: "ptbg",
            imgsrc: "",
      }]
    }]
  },
  // slide 2--rndObj
  {
    imgload:true,
    extratextblock:[{
      textclass:"tpQn",
      textdata:data.string.tshop
    },{
      textclass:"btmGrn",
    },{
      datahighlightflag:true,
      datahighlightcustomclass:"redTxt",
      textclass:"tpQn tpQn1",
      textdata:data.string.q1
    }],
    imageblock:[{
      imgblkclass:"basketAndTys",
      imagestoshow:[
        {
          imgclass:"basket",
          imgid:"basket",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-3 bsktPutali",
          imgid:"putali",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-2 bsktTrain",
          imgid:"train",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-1 bsktfBall",
          imgid:"fBall",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-4 bsktcube",
          imgid:"cube",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-5 bsktPncl",
          imgid:"pencil01",
          imgsrc:''
        }
      ]
    }],
    exerciseblk:[{
      excblkclass:"mainContainer",
      optionscontainer:[
        {
          optionscontainerdivclass:"optContainer flxCont optCont1",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali smlPutali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont2",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil pncl",
                imgid:"pencil01",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil smlPutali pncl1",
                imgid:"pencil02",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont3",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali smlPutali trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont4",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali bll",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali cub",
                imgid:"cube",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali dck",
                imgid:"duck",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxContWrpd optCont5",
          optdiv:[{
            optdivclass:"options clkbl rbtOpt flxCont opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options clkbl carOpt opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"car",
                imgid:"car",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt clkbl class1 flxCont opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"thr_ball img-1",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt clkbl flxCont opt4",
            imageblock:[{
              imagestoshow:[{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              }]
            }]
          }]
        }
      ]
    }]
  },
  // slide 3--train
  {
    imgload:true,
    extratextblock:[{
      textclass:"tpQn",
      textdata:data.string.tshop
    },{
      textclass:"btmGrn",
    },{
      datahighlightflag:true,
      datahighlightcustomclass:"redTxt",
      textclass:"tpQn tpQn1",
      textdata:data.string.q2
    }],
    imageblock:[{
      imgblkclass:"basketAndTys",
      imagestoshow:[
        {
          imgclass:"basket",
          imgid:"basket",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-3 bsktPutali",
          imgid:"putali",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-2 bsktTrain",
          imgid:"train",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-1 bsktfBall",
          imgid:"fBall",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-4 bsktcube",
          imgid:"cube",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-5 bsktPncl",
          imgid:"pencil01",
          imgsrc:''
        }
      ]
    }],
    exerciseblk:[{
      excblkclass:"mainContainer",
      optionscontainer:[
        {
          optionscontainerdivclass:"optContainer flxCont optCont1",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali smlPutali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont2",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil pncl",
                imgid:"pencil01",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil smlPutali pncl1",
                imgid:"pencil02",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont3",
          optdiv:[{
            optdivclass:"options clkbl opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"train trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options clkbl class1 opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"train smlTrain img-2 trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont4",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali bll",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali cub",
                imgid:"cube",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali dck",
                imgid:"duck",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxContWrpd optCont5",
          optdiv:[{
            optdivclass:"options  rbtOpt flxCont opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options  carOpt opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"car",
                imgid:"car",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt clkbl class1 flxCont opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt  flxCont opt4",
            imageblock:[{
              imagestoshow:[{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              }]
            }]
          }]
        }
      ]
    }]
  },
  // slide 4--doll
  {
    imgload:true,
    extratextblock:[{
      textclass:"tpQn",
      textdata:data.string.tshop
    },{
      textclass:"btmGrn",
    },{
      datahighlightflag:true,
      datahighlightcustomclass:"redTxt",
      textclass:"tpQn tpQn1",
      textdata:data.string.q3
    }],
    imageblock:[{
      imgblkclass:"basketAndTys",
      imagestoshow:[
        {
          imgclass:"basket",
          imgid:"basket",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-3 bsktPutali",
          imgid:"putali",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-2 bsktTrain",
          imgid:"train",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-1 bsktfBall",
          imgid:"fBall",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-4 bsktcube",
          imgid:"cube",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-5 bsktPncl",
          imgid:"pencil01",
          imgsrc:''
        }
      ]
    }],
    exerciseblk:[{
      excblkclass:"mainContainer",
      optionscontainer:[
        {
          optionscontainerdivclass:"optContainer flxCont optCont1",
          optdiv:[{
            optdivclass:"options clkbl class1 opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali img-3",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options clkbl opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali smlPutali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont2",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil pncl",
                imgid:"pencil01",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil smlPutali pncl1",
                imgid:"pencil02",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont3",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"train trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"train smlTrain trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont4",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali bll",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali cub",
                imgid:"cube",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali dck",
                imgid:"duck",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxContWrpd optCont5",
          optdiv:[{
            optdivclass:"options  rbtOpt flxCont opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options  carOpt opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"car",
                imgid:"car",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt clkbl class1 flxCont opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt  flxCont opt4",
            imageblock:[{
              imagestoshow:[{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              }]
            }]
          }]
        }
      ]
    }]
  },
  // slide 5--3imgs
  {
    imgload:true,
    extratextblock:[{
      textclass:"tpQn",
      textdata:data.string.tshop
    },{
      textclass:"btmGrn",
    },{
      datahighlightflag:true,
      datahighlightcustomclass:"redTxt",
      textclass:"tpQn tpQn1",
      textdata:data.string.q4
    }],
    imageblock:[{
      imgblkclass:"basketAndTys",
      imagestoshow:[
        {
          imgclass:"basket",
          imgid:"basket",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-3 bsktPutali",
          imgid:"putali",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-2 bsktTrain",
          imgid:"train",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-1 bsktfBall",
          imgid:"fBall",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-4 bsktcube",
          imgid:"cube",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-5 bsktPncl",
          imgid:"pencil01",
          imgsrc:''
        }
      ]
    }],
    exerciseblk:[{
      excblkclass:"mainContainer",
      optionscontainer:[
        {
          optionscontainerdivclass:"optContainer flxCont optCont1",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali smlPutali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont2",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil pncl",
                imgid:"pencil01",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil smlPutali pncl1",
                imgid:"pencil02",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont3",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"train trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"train smlTrain trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont4",
          optdiv:[{
            optdivclass:"options clkbl opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali bll",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options clkbl class1 opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali img-4 cub",
                imgid:"cube",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options clkbl opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali dck",
                imgid:"duck",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxContWrpd optCont5",
          optdiv:[{
            optdivclass:"options  rbtOpt flxCont opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options  carOpt opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"car",
                imgid:"car",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt clkbl class1 flxCont opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt  flxCont opt4",
            imageblock:[{
              imagestoshow:[{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              }]
            }]
          }]
        }
      ]
    }]
  },
  // slide 6--pencil
  {
    extratextblock:[{
      datahighlightflag:true,
      datahighlightcustomclass:"redTxt",
      textclass:"tpQn",
      textdata:data.string.tshop
    },{
      textclass:"btmGrn",
    },{
      datahighlightflag:true,
      datahighlightcustomclass:"redTxt",
      textclass:"tpQn tpQn1",
      textdata:data.string.q5
    }],
    imageblock:[{
      imgblkclass:"basketAndTys",
      imagestoshow:[
        {
          imgclass:"basket",
          imgid:"basket",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-3 bsktPutali",
          imgid:"putali",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-2 bsktTrain",
          imgid:"train",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-1 bsktfBall",
          imgid:"fBall",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-4 bsktcube",
          imgid:"cube",
          imgsrc:''
        },{
          imgclass:"bsktImg bim-5 bsktPncl",
          imgid:"pencil02",
          imgsrc:''
        }
      ]
    }],
    imgload:true,
    exerciseblk:[{
      excblkclass:"mainContainer",
      optionscontainer:[
        {
          optionscontainerdivclass:"optContainer flxCont optCont1",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali smlPutali",
                imgid:"putali",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont2",
          optdiv:[{
            optdivclass:"options clkbl opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil pncl",
                imgid:"pencil01",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options  clkbl class1 opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"pencil smlPutali img-5 pncl1",
                imgid:"pencil02",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont3",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"train trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"train smlTrain trn",
                imgid:"train",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxCont optCont4",
          optdiv:[{
            optdivclass:"options opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali bll",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali cub",
                imgid:"cube",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options clkbl opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"putali dck",
                imgid:"duck",
                imgsrc:''
              }]
            }]
          }]
        },
        {
          optionscontainerdivclass:"optContainer flxContWrpd optCont5",
          optdiv:[{
            optdivclass:"options  rbtOpt flxCont opt1",
            imageblock:[{
              imagestoshow:[{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              },{
                imgclass:"robot rbt",
                imgid:"robot",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options  carOpt opt2",
            imageblock:[{
              imagestoshow:[{
                imgclass:"car",
                imgid:"car",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt clkbl class1 flxCont opt3",
            imageblock:[{
              imagestoshow:[{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              },{
                imgclass:"thr_ball bl",
                imgid:"ball",
                imgsrc:''
              }]
            }]
          },{
            optdivclass:"options shrtOpt  flxCont opt4",
            imageblock:[{
              imagestoshow:[{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              },{
                imgclass:"bear",
                imgid:"bear",
                imgsrc:''
              }]
            }]
          }]
        }
      ]
    }]
  },
  // slide 7
  {
    extratextblock:[{
      textclass:"tptxt",
      textdata:data.string.s6txt
    }],
    imageblock:[{
      imgblkclass:"basketAndTysSec",
      imagestoshow:[
        {
          imgclass:"basket",
          imgid:"basket",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-3 bsktPutali",
          imgid:"putali",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-2 bsktTrain",
          imgid:"train",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-1 bsktfBall",
          imgid:"fBall",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-4 bsktcube",
          imgid:"cube",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-5 bsktPncl",
          imgid:"pencil02",
          imgsrc:''
        }
      ]
    },{
      imgblkclass:"AshaDiv",
      imagestoshow:[{
          imgclass:"tlkMan",
          imgid:"talking-man",
          imgsrc:''
        }
      ]
    }],
  },
  // slide 8
  {
    imageblock:[{
      imgblkclass:"basketAndTysSec shakeAnim lstpgBskt",
      imagestoshow:[
        {
          imgclass:"basket",
          imgid:"basket",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-3 bsktPutali",
          imgid:"putali",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-2 bsktTrain",
          imgid:"train",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-1 bsktfBall",
          imgid:"fBall",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-4 bsktcube",
          imgid:"cube",
          imgsrc:''
        },{
          imgclass:"bsktImg bi-5 bsktPncl",
          imgid:"pencil02",
          imgsrc:''
        }
      ]
    },{
      imgblkclass:"AshaDiv",
      imagestoshow:[{
          imgclass:"asha",
          imgid:"asha",
          imgsrc:''
        }
      ]
    }],
  },
];

$(function () {
    var $board = $('.board');
  	var $nextBtn = $("#activity-page-next-btn-enabled");
  	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var current_sound="";
    var sound="";
    var stmout1,stmout2;

    var endpageex =  new EndPageofExercise();


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "squirreldance", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "ptbg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
            {id: "putali", src: imgpath+"putali.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pencil01", src: imgpath+"pencil01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pencil02", src: imgpath+"pencil02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "train", src: imgpath+"toy01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ball", src: imgpath+"toy03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cube", src: imgpath+"cube.png", type: createjs.AbstractLoader.IMAGE},
            {id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
            {id: "robot", src: imgpath+"toy02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "car", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bear", src: imgpath+"toy04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "brown_band", src: imgpath+"brown_band.png", type: createjs.AbstractLoader.IMAGE},
            {id: "basket", src: imgpath+"basket.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fBall", src: imgpath+"toy03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "asha", src: imgpath+"asha.png", type: createjs.AbstractLoader.IMAGE},
            {id: "talking-man", src: imgpath+"talking-man.gif", type: createjs.AbstractLoader.IMAGE},


           //sounds
           {id: "tada", src: soundAsset+"tadasoundeffect.ogg"},
            {id: "pop", src: soundAsset+"Popsoundeffect.ogg"},
           {id: "ex1_2", src: soundAsset+"ex1_2.ogg"},
           {id: "ex1_3", src: soundAsset+"ex1_3.ogg"},
           {id: "ex1_4", src: soundAsset+"ex1_4.ogg"},
           {id: "ex1_5", src: soundAsset+"ex1_5.ogg"},
           {id: "ex1_6", src: soundAsset+"ex1_6.ogg"},
           {id: "ex1_7", src: soundAsset+"ex1_7.ogg"},
           {id: "ex1_8", src: soundAsset+"ex1_8.ogg"},
     ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/




    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    // function navigationcontroller(islastpageflag) {
    //     typeof islastpageflag === "undefined" ?
    //         islastpageflag = false :
    //         typeof islastpageflag != 'boolean' ?
    //             alert("NavigationController : Hi Master, please provide a boolean parameter") :
    //             null;
    //     if (countNext == 0 && $total_page != 1) {
    //         $nextBtn.show(0);
    //     }
    //     else if ($total_page == 1) {
    //         $nextBtn.css('display', 'none');
    //
    //         ole.footerNotificationHandler.lessonEndSetNotification();
    //     }
    //     else if (countNext > 0 && countNext < $total_page - 1) {
    //
    //         $nextBtn.show(0);
    //     }
    //     else if (countNext == $total_page - 2) {
    //
    //         $nextBtn.css('display', 'none');
    //         // if lastpageflag is true
    //         ole.footerNotificationHandler.pageEndSetNotification();
    //     }
    //
    // }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
      var source = $("#general-template").html();
      var template = Handlebars.compile(source);
      var html = template(content[countNext]);
      $board.html(html);
      firstPagePlayTime(countNext);
      texthighlight($board);
      put_image(content, countNext);
      content[countNext].imgload?put_imageSec(content, countNext):"";
		      //tick qns start

  		var wrngClicked = false;
  		var corrCounter=0;
      function shwImg(count){
        for(var i=1;i<=count;i++){
          $(".bim-"+i).show(0);
        }
      }
      $(".mainContainer").append("<img class='background' src='"+preload.getResult("brown_band").src+"'/>");
				$prevBtn.hide(0);
				$nextBtn.hide(0);
      switch (countNext){
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
  				$prevBtn.hide(0);
          $nextBtn.hide(0);
          shwImg(countNext-1);
          stmout1 = setTimeout(function(){
            $(".mainContainer").addClass("zoom-"+countNext);
          },2000);
          stmout2 = setTimeout(function(){
            $(".tpQn").hide(0);
            $(".tpQn1").show(0);
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("ex1_"+(countNext+1));
            current_sound.play();
            current_sound.on('complete',function(){
              $(".clkbl").css("pointer-events","auto");
            });
          },5500);
        break;
        case 6:
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("ex1_"+(countNext+1));
          current_sound.play();
          current_sound.on('complete',function(){
            sound_player("tada");
              nav_button_controls(1000);
          });
        break;
        case 7:
          setTimeout(function(){
            sound_player("pop");
            $(".bi-1").animate({
              bottom: "117%",
              right: "128%"
            },500,function(){
              sound_player("pop");
              $(".bi-2").animate({
                bottom: "231%",
                right: "56%",
                transform: "rotate(0deg)"
              },500,function(){
                sound_player("pop");
                  $(".bi-3").animate({
                    bottom: "176%",
                    right: "11%",
                  },500,function(){
                    sound_player("pop");
                    $(".bi-4").animate({
                      bottom: "228%",
                      right: "113%",
                    },500,function(){
                      sound_player("pop");
                      $(".bi-5").animate({
                        bottom: "166%",
                        right: "50%",
                      },500,function(){
                        sound_player("ex1_"+(countNext+1));
                      });
                    });
                  });
              });
            });
          },950);
         endpageex.endpage(data.string.yaytxt);
        break;
      default:
        // nav_button_controls(2000);
      break;
      }
      $(".clkbl").click(function(){
        if($(this).hasClass("class1")){
          $(this).children(".corctopt").show(0);
          play_correct_incorrect_sound(1);
          $(".img-"+countNext).addClass("anim-"+countNext);
          $(".clkbl").css("pointer-events","none");
          itemAnimate();
          nav_button_controls(2900);
        }else{
          $(this).children(".wrngopt").show(0);
          $(this).css("pointer-events","none");
          play_correct_incorrect_sound(0);
        }
      });
    }

    function itemAnimate(){
      // to animate objects to baskets according to countNext
      switch (countNext) {
        case 1:
        case 3:
          $(".img-"+countNext).animate({
            top: "40%",
            left: "227%"
          },2000,function(){
            $(".img-"+countNext).hide(0);
            $(".bim-"+countNext).show(0);
          });
        break;
        case 2:
          $(".img-"+countNext).animate({
            top: "136%",
            left: "89%"
          },2000,function(){
            $(".img-"+countNext).hide(0);
            $(".bim-"+countNext).show(0);
          });
        break;
        case 4:
          $(".img-"+countNext).animate({
            top: "177%",
            left: "179%"
          },2000,function(){
            $(".img-"+countNext).hide(0);
            $(".bim-"+countNext).show(0);
          });
        break;
        case 5:
          $(".img-"+countNext).animate({
            top: "221%",
            left: "89%"
          },2000,function(){
            $(".img-"+countNext).hide(0);
            $(".bim-"+countNext).show(0);
          });
        break;

      }
    }


    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          // $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.lessonEndSetNotification();
        } else{
          // $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }
    function sound_player_nav(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on('complete',function(){
        nav_button_controls();
      });
    }
    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
    }

    function put_image(content, count){
    	if(content[count].hasOwnProperty('imageblock')){
    		var images = content[count].imageblock;
    		for(var j=0; j<images.length; j++){
    		var imageblock = content[count].imageblock[j];
    		if(imageblock.hasOwnProperty('imagestoshow')){
    			var imageClass = imageblock.imagestoshow;
    			for(var i=0; i<imageClass.length; i++){
    				var image_src = preload.getResult(imageClass[i].imgid).src;
    				//get list of classes
    				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
    				var selector = ('.'+classes_list[classes_list.length-1]);
    				$(selector).attr('src', image_src);
    			}
    		}
    	}
    	}
    }
    function put_imageSec(content, count){
      if(content[count].hasOwnProperty('exerciseblk')){
        if(content[count].exerciseblk[0].hasOwnProperty('optionscontainer')){
          for(var l=0;l<content[count].exerciseblk[0].optionscontainer.length;l++){
            var excimgblk= content[count].exerciseblk[0].optionscontainer[l];
            if(excimgblk.hasOwnProperty('optdiv')){
              for(var k=0;k<excimgblk.optdiv.length;k++){
                if(excimgblk.optdiv[k].hasOwnProperty('imageblock')){
              		var imageblock = excimgblk.optdiv[k].imageblock[0];
              		if(imageblock.hasOwnProperty('imagestoshow')){
              			var imageClass = imageblock.imagestoshow;
              			for(var i=0; i<imageClass.length; i++){
              				var image_src = preload.getResult(imageClass[i].imgid).src;
              				//get list of classes
              				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
              				var selector = ('.'+classes_list[classes_list.length-1]);
              				$(selector).attr('src', image_src);
              			}
              		}
                }
              }
            }
          }
        }
      }
    }


    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
    		loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(stmout1);
        clearTimeout(stmout2);
        switch (countNext) {
            default:
                countNext++;
                // lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });
  $refreshBtn.on('click', function(){
    createjs.Sound.stop();
    clearTimeout(stmout1);
    clearTimeout(stmout2);
    templateCaller();
  });
});

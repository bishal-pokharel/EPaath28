var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// coverpage
	{
		contentblockadditionalclass: 'bg-green',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "malaFst",
					imgsrc : '',
					imgid : 'mala'
				}
			]
		}],
		speechbox:[{
			speechbox:"sp-3",
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp svnVh",
			textdata:data.string.p2s1txt
		}]
	},
	// slide 2
	{
		contentblockadditionalclass: 'bg-pink',
		extratextblock:[{
			textclass: "midTxt",
			textdata: data.string.p2s2txt,
		}]
	},
	// slide 3
	{
		contentblockadditionalclass: 'bg-green',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chickLeft hidn chk thnBk ",
					imgid : 'thinBook',
					imgsrc : '',
				},{
					imgclass : "mala",
					imgid : 'mala',
					imgsrc : '',
				},{
					imgclass : "henRight hidn hn thkBk ",
					imgid : 'thickBook',
					imgsrc : '',
				}]
		}],
		speechbox:[{
			speechbox:"sp-2 hn hidn",
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.p2s3txt_1
		},{
			speechbox:"sp-1 chk hidn",
			imgclass:'flipped',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.p2s3txt
		}]
	},
	// slide 4
	{
		contentblockadditionalclass: 'bg-green',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chickLeft hidn chk ",
					imgid : 'thin_brush',
					imgsrc : '',
				},{
					imgclass : "mala",
					imgid : 'mala',
					imgsrc : '',
				},{
					imgclass : "henRight hidn hn ",
					imgid : 'thick_brush',
					imgsrc : '',
				}]
		}],
		speechbox:[{
			speechbox:"sp-2 hn hidn",
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.p2s4txt_1
		},{
			speechbox:"sp-1 chk hidn",
			imgclass:'flipped',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.p2s4txt
		}]
	},
	// slide 5
	{
		contentblockadditionalclass: 'bg-green',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chickLeft hidn chk thnRp ",
					imgid : 'thin_rope',
					imgsrc : '',
				},{
					imgclass : "mala",
					imgid : 'mala',
					imgsrc : '',
				},{
					imgclass : "henRight hidn hn thkRp",
					imgid : 'thick_rope',
					imgsrc : '',
				}]
		}],
		speechbox:[{
			speechbox:"sp-2 hn hidn",
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.p2s5txt_1
		},{
			speechbox:"sp-1 chk hidn",
			imgclass:'flipped',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.p2s5txt
		}]
	},
	// slide 6
	{
		contentblockadditionalclass: 'bg-pink',
		extratextblock:[{
			textclass: "midTxt",
			textdata: data.string.p2s6txt,
		}]
	},
	// slide 7
	{
		contentblockadditionalclass: 'bg-green',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s3txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "talTree-lhs",
					imgid : 'big_tree',
					imgsrc : '',
				}]
		}]
	},
	// slide 8
	{
		contentblockadditionalclass: 'bg-green',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s3txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "talTree-lhs",
					imgid : 'big_tree',
					imgsrc : '',
				}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"oboTxt hidn",
			textclass:"sideTxt ",
			textdata:data.string.p2s7txt
		}]
	},
	// slide 9
	{
		contentblockadditionalclass: 'bg-green',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chickLeft hidn smlTr chk ",
					imgid : 'small_tree',
					imgsrc : '',
				},{
					imgclass : "mala",
					imgid : 'mala',
					imgsrc : '',
				},{
					imgclass : "henRight hidn tlTrRght hn ",
					imgid : 'big_tree',
					imgsrc : '',
				}]
		}],
		speechbox:[{
			speechbox:"sp-2 hn hidn",
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.p2s8txt_1
		},{
			speechbox:"sp-1 chk hidn",
			imgclass:'flipped',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.p2s8txt
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grlImg", src: imgpath+"mala01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"mala.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},

			{id: "thick_brush", src: imgpath+"thick_brush.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thin_brush", src: imgpath+"thin_brush.png", type: createjs.AbstractLoader.IMAGE},

			{id: "thickBook", src: imgpath+"thick_book.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thinBook", src: imgpath+"thin_book.png", type: createjs.AbstractLoader.IMAGE},

			{id: "thin_rope", src: imgpath+"thin_rope.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thick_rope", src: imgpath+"thick_rope.png", type: createjs.AbstractLoader.IMAGE},

			{id: "small_tree", src: imgpath+"small_tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "big_tree", src: imgpath+"big_tree.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p3_1", src: soundAsset+"s2_p3_1.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p4_1", src: soundAsset+"s2_p4_1.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p5_1", src: soundAsset+"s2_p5_1.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s2_p7", src: soundAsset+"s2_p7.ogg"},
			{id: "s2_p8", src: soundAsset+"s2_p8.ogg"},
			{id: "s2_p9", src: soundAsset+"s2_p9.ogg"},
			{id: "s2_p9_1", src: soundAsset+"s2_p9_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		// $(".topTxtImgDiv").append("<img src='"+preload.getResult("grlImg").src+"'/>");
		$(".topsideimg").attr("src",preload.getResult("grlImg").src);
		switch(countNext) {
			case 0:
				// $nextBtn.show(0);
				sound_nav('s2_p'+(countNext+1));
			break;
			case 2:
			case 3:
			case 4:
			case 8:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('s2_p'+(countNext+1));
				current_sound.play();
				$(".chk").fadeIn(500);
				current_sound.on('complete',function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s2_p'+(countNext+1)+"_1");
					current_sound.play();
					$(".hn").fadeIn(500);
					current_sound.on('complete',function(){
						nav_button_controls(100);
					});
				});
			break;
			case 7:
				sound_nav('s2_p'+(countNext+1));
				$(".oboTxt:eq(0)").fadeIn(500);
				$(".oboTxt:eq(1)").delay(3500).fadeIn(500);
			break;
			default:
				sound_nav('s2_p'+(countNext+1));
			break;
		}
		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				$(this).css({
					"background-color":"#98c02e",
					"border":"5px solid #deef3c"
				});
				play_correct_incorrect_sound(1);
				$(this).siblings(".corctopt").show(0);
			}else{
				$(this).css({
					"background-color":"#ff0000",
					"border":"5px solid #980000"
				});
				play_correct_incorrect_sound(0);
				$(this).siblings(".wrngopt").show(0);
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

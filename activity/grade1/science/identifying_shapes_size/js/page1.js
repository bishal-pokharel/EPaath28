var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// coverpage
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',

		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "midTxt cvpName",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "background",
					imgsrc : '',
					imgid : 'im-1'
				}
			]
		}],
	},
	// slide 2
	{
		contentblockadditionalclass: 'bg-pink',
		extratextblock:[{
			textclass: "midTxt",
			textdata: data.string.p1s2txt,
		}]
	},
	// slide 3
	{
		contentblockadditionalclass: 'bg-green',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s3txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chick",
					imgid : 'chick',
					imgsrc : '',
				}]
		}],
	},
	// slide 4
	{
		contentblockadditionalclass: 'bg-green',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s3txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chick",
					imgid : 'chick',
					imgsrc : '',
				}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"oboTxt hidn",
			textclass:"sideTxt ",
			textdata:data.string.p1s4txt
		}]
	},
	// slide 5-->gifAnim left
	{
		contentblockadditionalclass: 'bg-green',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			datahighlightflag:true,
			datahighlightcustomclass:"ylwTxt",
			textdata:data.string.p1s5txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chick chk",
					imgid : 'chick',
					imgsrc : '',
				},{
					imgclass : "chick grwHn",
					imgid : 'growing_hen',
					imgsrc : '',
				}]
		}]
	},
	// slide 6
	{
		contentblockadditionalclass: 'bg-green',
		// txtimg:[{
		// 	txtimgclass:"topTxtImgDiv",
		// 	textclass:"topTXt",
		// 	datahighlightflag:true,
		// 	datahighlightcustomclass:"ylwTxt",
		// 	textdata:data.string.p1s5txt
		// }],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chickLeft hidn chk ",
					imgid : 'chick',
					imgsrc : '',
				},{
					imgclass : "mala",
					imgid : 'mala',
					imgsrc : '',
				},{
					imgclass : "henRight hidn hn ",
					imgid : 'hen',
					imgsrc : '',
				}]
		}],
		speechbox:[{
			speechbox:"sp-2 hn hidn",
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.bghen
		},{
			speechbox:"sp-1 chk hidn",
			imgclass:'flipped',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.smlchk
		}]
	},
	// slide 7
	{
		contentblockadditionalclass: 'bg-green',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s3txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chick",
					imgid : 'tennisBall',
					imgsrc : '',
				}]
		}],
	},
	// slide 8
	{
		contentblockadditionalclass: 'bg-green',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s3txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chick",
					imgid : 'tennisBall',
					imgsrc : '',
				}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"oboTxt ",
			textclass:"sideTxt hidn",
			textdata:data.string.p1s8txt
		}]
	},
	// slide 9
	{
		contentblockadditionalclass: 'bg-green',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chickLeft hidn chk ",
					imgid : 'tennisBall',
					imgsrc : '',
				},{
					imgclass : "mala",
					imgid : 'mala',
					imgsrc : '',
				},{
					imgclass : "ftbl hidn hn ",
					imgid : 'football',
					imgsrc : '',
				}]
		}],
		speechbox:[{
			speechbox:"sp-2 hn hidn",
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.bgbl
		},{
			speechbox:"sp-1 chk hidn",
			imgclass:'flipped',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"grnTxt",
			textclass:"textInSp svnVh",
			textdata:data.string.smlbl
		}]
	},
	// slide 10
	{
		contentblockadditionalclass: 'bg-green',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s10txt_1
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chick",
					imgid : 'tennisBall',
					imgsrc : '',
				}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"blTxt",
			textclass:"sideTxt hidn",
			textdata:data.string.p1s10txt
		}]
	},
	// slide 11
	{
		contentblockadditionalclass: 'bg-green',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "chickLeft hidn chk ",
					imgid : 'tenish_ball_rotation',
					imgsrc : '',
				},{
					imgclass : "mala",
					imgid : 'mala',
					imgsrc : '',
				},{
					imgclass : "ftbl hidn hn ",
					imgid : 'football',
					imgsrc : '',
				}]
		}],
		speechbox:[{
			speechbox:"sp-1 chk hidn",
			imgclass:'flipped',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"clrd",
			textclass:"textInSp",
			textdata:data.string.p1s11txt
		},{
			speechbox:"sp-2 hn hidn",
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			datahighlightflag:true,
			datahighlightcustomclass:"clrdS",
			textclass:"textInSp",
			textdata:data.string.p1s11txt_1
		},]
	},


	// slide 12
	{
		contentblockadditionalclass: 'bg-pink',
		extratextblock:[{
			textclass: "midTxt",
			textdata: data.string.p1s12txt,
		}]
	},
	// slide 13
	{
		contentblockadditionalclass: 'bg-cream',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s3txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bokLft",
					imgid : 'book',
					imgsrc : '',
				}]
		}],
	},
	// slide 14
	{
		contentblockadditionalclass: 'bg-cream',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s3txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bokLft",
					imgid : 'book',
					imgsrc : '',
				}]
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"oboTxt",
			textclass:"sideTxt hidn",
			textdata:data.string.p1s14txt
		}]
	},
	// slide 15
	{
		contentblockadditionalclass: 'bg-cream',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s15txt_1
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"bltxt",
			textclass:"sideTxt hidn",
			textdata:data.string.p1s15txt
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bookarrow1",
					imgid : 'arrow02',
					imgsrc : '',
				},
				{
					imgclass : "bookarrow2",
					imgid : 'arrow02',
					imgsrc : '',
				},
				{
					imgclass : "bookarrow3",
					imgid : 'arrow02',
					imgsrc : '',
				},
				{
					imgclass : "bookarrow4",
					imgid : 'arrow02',
					imgsrc : '',
				},
			]
		}],
		svgblock:[{
			svgid:"bokSvg"
		}]
	},


	// slide 16
	{
		contentblockadditionalclass: 'bg-cream',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s16txt
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.one
			},{
				optdata:data.string.five
			},{
				optaddclass:"class1",
				optdata:data.string.four
			},{
				optdata:data.string.seven
			}]
		}],
		svgblock:[{
			svgid:"bokSvg"
		}]
	},
	// slide 17
	{
		contentblockadditionalclass: 'bg-cream',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s15txt_1
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"bltxt",
			textclass:"sideTxt hidn",
			textdata:data.string.p1s17txt
		}],
		svgblock:[{
			svgid:"bokSvg"
		}]
	},
	// slide 18
	{
		contentblockadditionalclass: 'bg-cream',
		txtimg:[{
			txtimgclass:"topTxtImgDiv",
			textclass:"topTXt",
			textdata:data.string.p1s18txt_1
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"clrd",
			textclass:"sideTxt hidn",
			textdata:data.string.p1s18txt
		}],
		svgblock:[{
			svgid:"bokSvg"
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chick", src: imgpath+"chick.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grlImg", src: imgpath+"mala01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"mala.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tenish_ball_rotation", src: imgpath+"tenish_ball_rotation.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "growing_hen", src: imgpath+"growing_hen.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "tennisBall", src: imgpath+"tenish-ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "football", src: imgpath+"football.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow02", src: imgpath+"arrow02.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "book", src: imgpath+"thin_brush.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book", src: imgpath+"book_01.svg", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p6_1", src: soundAsset+"s1_p6_1.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p9_1", src: soundAsset+"s1_p9_1.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "s1_p11_1", src: soundAsset+"s1_p11_1.ogg"},
			{id: "s1_p12", src: soundAsset+"s1_p12.ogg"},
			{id: "s1_p13", src: soundAsset+"s1_p13.ogg"},
			{id: "s1_p14", src: soundAsset+"s1_p14.ogg"},
			{id: "s1_p15", src: soundAsset+"s1_p15.ogg"},
			{id: "s1_p16", src: soundAsset+"s1_p16.ogg"},
			{id: "s1_p17", src: soundAsset+"s1_p17.ogg"},
			{id: "s1_p18", src: soundAsset+"s1_p18.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		// $(".topTxtImgDiv").append("<img src='"+preload.getResult("grlImg").src+"'/>");
		$(".topsideimg").attr("src",preload.getResult("grlImg").src);
		if (countNext!=4){
			$(".chick").css({"cursor":"auto","transform":"scale(1)"});
		}
		switch(countNext) {
			case 0:
				// $nextBtn.show(0);
				sound_player('s1_p'+(countNext+1),1);
			break;
			case 1:
				$(".midTxt").css({"left":"50%","top":"50%"});
				sound_player('s1_p'+(countNext+1),1);
				break;
			case 3:
				sound_player('s1_p'+(countNext+1),1);
				$(".oboTxt:eq(0)").show();
				$(".oboTxt:eq(1)").show();
			break;
			case 4:
				sound_player('s1_p'+(countNext+1),0);
				$(".chick").css({"left":"43%"});
				$('.chk').on('click',function(){
					$(".chk").hide(0);
					$(".grwHn").show(0);
					nav_button_controls(1000);
				});
			break;
			case 5:
			case 8:
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s1_p'+(countNext+1));
					current_sound.play();
					$(".chk").fadeIn(500);
					current_sound.on('complete',function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play('s1_p'+(countNext+1)+"_1");
						current_sound.play();
						$(".hn").fadeIn(500);
						current_sound.on('complete',function(){
							nav_button_controls(100);
						});
					});
			break;
			case 7:
			case 9:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('s1_p'+(countNext+1));
				current_sound.play();
				if(countNext==9){
				$('.topTXt').css("opacity","0").delay(500).css("opacity","1");
				$(".sideTxt").delay(4000).fadeIn(500);
			}
			else{
				$(".sideTxt").fadeIn(500);
			}
				current_sound.on('complete',function(){
					nav_button_controls(100);
				});
			break;
			case 10:
				$(".clrd:eq(0), .clrdS:eq(0)").addClass("grnTxt");
				$(".clrd:eq(1), .clrdS:eq(1)").addClass("blTxt");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('s1_p'+(countNext+1));
				current_sound.play();
				$(".chk").fadeIn(500);
				current_sound.on('complete',function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s1_p'+(countNext+1)+"_1");
					current_sound.play();
					$(".hn").fadeIn(500);
					current_sound.on('complete',function(){
						nav_button_controls(100);
					});
				});
			break;
			case 13:
				$(".sideTxt").delay(200).fadeIn(200);
				sound_player('s1_p'+(countNext+1),1);
			break;
			case 14:
				var s = Snap('#bokSvg');
				var svg = Snap.load(preload.getResult("book").src,function(loadedFragment){
					s.append(loadedFragment);
					// $("#corner04,#corner03,#corner02,#corner01").css("display","none");
					// $("#path01,#path02,#path03,#path04").css("display","none");
					$("#path04").delay(100).show(0);
					$("#path01").delay(500).show(0);
					$("#path02").delay(1000).show(0);
					$("#path03").delay(1500).show(0);

				});
				setTimeout(function(){
					$("#path01,#path02,#path03,#path04").attr("class","blink");
				},1700);
				$(".sideTxt").delay(2000).fadeIn(200);

				$('.bookarrow3').animate({top:"29%", "opacity":"1"},900,"linear");
				$('.bookarrow1').delay(900).animate({left:"29.7%", "opacity":"1"},900,"linear");
				$('.bookarrow2').delay(1800).animate({top:"82%", "opacity":"1"},900,"linear");
				$('.bookarrow4').delay(2700).animate({left:"4.2%", "opacity":"1"},900,"linear");
				sound_player('s1_p'+(countNext+1),1);
			break;
			case 15:
				var s = Snap('#bokSvg');
				var svg = Snap.load(preload.getResult("book").src,function(loadedFragment){
					s.append(loadedFragment);
						$("#path01,#path02,#path03,#path04").attr("display","block");

						$("#path01").attr("class","blink1");
						$("#path02").attr("class","blink2");
						$("#path03").attr("class","blink3");
						$("#path04").attr("class","blink4");
				});
				$(".sideTxt").delay(500).fadeIn(200);
				sound_player('s1_p'+(countNext+1),0);
			break;
			case 16:
				var s = Snap('#bokSvg');
				var svg = Snap.load(preload.getResult("book").src,function(loadedFragment){
					s.append(loadedFragment);
					$("#outline_1_").css("display","none");
						// $("#path01,#path02,#path03,#path04").css("display","none");
						// $("#corner04,#corner03,#corner02,#corner01").css("display","none");
						$("#corner04").show(0);
						$("#corner03").delay(1500).show(0);
						$("#corner02").delay(1000).show(0);
						$("#corner01").delay(500).show(0);
				});
				setTimeout(function(){
					$("#corner01,#corner02,#corner03,#corner04").attr("class","blink");
				},1700);
				$(".sideTxt").delay(2800).fadeIn(200);
				sound_player('s1_p'+(countNext+1),1);
			break;
			case 17:
				$(".clrd:eq(0)").addClass("prplTxt");
				$(".clrd:eq(1)").addClass("blTxt");
				$(".sideTxt").delay(5200).fadeIn(200);
				sound_player('s1_p'+(countNext+1),1);
					var s = Snap('#bokSvg');
					var svg = Snap.load(preload.getResult("book").src,function(loadedFragment){
						s.append(loadedFragment);
						$("#outline_1_").css("display","none");
							$("#path01,#path02,#path03,#path04").css("display","none");
							$("#corner04,#corner03,#corner02,#corner01").css("display","none");
							$("#path04").show(0);
							$("#path03").delay(1500).show(0);
							$("#path02").delay(1000).show(0);
							$("#path01").delay(500).show(0);
							setTimeout(function(){
								$("#path01,#path02,#path03,#path04").css("display","none");
								$("#corner04").show(0);
								$("#corner03").delay(1500).show(0);
								$("#corner02").delay(1000).show(0);
								$("#corner01").delay(500).show(0);
							},1700);
					});
			break;
			default:
				sound_player('s1_p'+(countNext+1),1);
			break;
		}
		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				$(this).css({
					"background-color":"#98c02e",
					"border":"5px solid #deef3c"
				});
				$(".buttonsel").css("pointer-events","none");
					createjs.Sound.stop();
				play_correct_incorrect_sound(1);
				$(this).siblings(".corctopt").show(0);
				nav_button_controls(100);
			}else{
				$(this).css({
					"background-color":"#ff0000",
					"border":"5px solid #980000",
					"pointer-events":"none"
				});
					createjs.Sound.stop();
				play_correct_incorrect_sound(0);
				$(this).siblings(".wrngopt").show(0);
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "chapter_title",
			textdata: data.string.title
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg',
				imgclass:'bg_full'
			}]
		}]
	},

	{
		// slide1
		contentnocenteradjust: true,
		contains_dialog_or_optiondiv:[{
			dialogcontainer:'dialogue1 ',
			dialogueimgid:'dialogue',
			dialogcontent:data.string.p1_s1,
			dialogimageclass:'dialoguebox1 flip',
			dialogcontentclass:'text_inside'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg',
				imgclass:'bg_full'
			},
			{
				imgid:'girl',
				imgclass:'girl'
			},
			{
				imgid:'boy',
				imgclass:'boy'
			}]

		}]
	},

	{
		// slide2
		contentnocenteradjust: true,
		contentblockadditionalclass: 'yellow_bg',
		uppertextblock:[{
			textclass: "main_question",
			textdata: data.string.p1_s2
		},
		{
			textclass: "instruction",
			textdata: data.string.p1_s3
		}],
		contains_dialog_or_optiondiv:[{
			dialogcontainer:'option_box1 correct',
			dialogueimgid:'love_animal',
			dialogcontent:data.string.p1_s4,
			dialogimageclass:'option_image1',
			dialogcontentclass:'title_class'
		},
		{
			dialogcontainer:'option_box2',
			dialogueimgid:'chase_animal',
			dialogcontent:data.string.p1_s5,
			dialogimageclass:'option_image2',
			dialogcontentclass:'title_class'
		}]
	},

	{
		// slide3
		contentnocenteradjust: true,
		contentblockadditionalclass: 'yellow_bg',
		uppertextblock:[{
			textclass: "main_question",
			textdata: data.string.p1_s2
		},
		{
			textclass: "instruction",
			textdata: data.string.p1_s3
		}],
		contains_dialog_or_optiondiv:[{
			dialogcontainer:'option_box1 ',
			dialogueimgid:'hurt_animals',
			dialogcontent:data.string.p1_s7,
			dialogimageclass:'option_image1',
			dialogcontentclass:'title_class'
		},
		{
			dialogcontainer:'option_box2 correct',
			dialogueimgid:'feed_animal',
			dialogcontent:data.string.p1_s6,
			dialogimageclass:'option_image2',
			dialogcontentclass:'title_class'
		}]
	},

	{
		// slide4
		contentnocenteradjust: true,
		contentblockadditionalclass: 'yellow_bg',
		uppertextblock:[{
			textclass: "main_question",
			textdata: data.string.p1_s2
		},
		{
			textclass: "instruction",
			textdata: data.string.p1_s3
		}],
		contains_dialog_or_optiondiv:[{
			dialogcontainer:'option_box1 correct',
			dialogueimgid:'clean_place',
			dialogcontent:data.string.p1_s8,
			dialogimageclass:'option_image1',
			dialogcontentclass:'title_class'
		},
		{
			dialogcontainer:'option_box2 ',
			dialogueimgid:'dirty_place',
			dialogcontent:data.string.p1_s9,
			dialogimageclass:'option_image2',
			dialogcontentclass:'title_class'
		}]
	},

];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"niti.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialogue", src: imgpath+"dialogue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "love_animal", src: imgpath+"love_animal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chase_animal", src: imgpath+"chase_animal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "feed_animal", src: imgpath+"feed_animal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hurt_animals", src: imgpath+"hurt_animals.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clean_place", src: imgpath+"clean_place.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dirty_place", src: imgpath+"dirty_place.png", type: createjs.AbstractLoader.IMAGE},

			//sounds
			{id: "instruction", src: soundAsset+"instruction.ogg"},
			{id: "loveanimals", src: soundAsset+"loveanimals.ogg"},
			{id: "chaseanimals", src: soundAsset+"chaseanimals.ogg"},
			{id: "hurtanimals", src: soundAsset+"hurtanimals.ogg"},
			{id: "feedanimals", src: soundAsset+"feedanimals.ogg"},
			{id: "cleantheshed", src: soundAsset+"cleantheshed.ogg"},
			{id: "makedirty", src: soundAsset+"makedirty.ogg"},
			{id: "p1_s1", src: soundAsset+"p1_s1.ogg"},
			{id: "p1_s0", src: soundAsset+"p1_s0.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var intervalcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
 

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
		// sound_player("ding");
		// $('.option_box1,.option_box2').mouseenter(function(){
		// 	$(this).find('.title_class').css('background','#666666');
		// });
		// $('.option_box1,.option_box2').mouseleave(function(){
		// 	$(this).find('.title_class').css('background','#D3759E');
		// });
		var top_val = (($('.main_question').height()/$('.coverboardfull').height() )* 100) +1+"%";
		console.log(top_val);
		$('.instruction').css({
			'top':top_val
		});
			$('.option_box1,.option_box2').click(function(){
				createjs.Sound.stop();
				if($(this).hasClass('correct')){
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$('.care_of_animals_and_plants').width()+'%';
					var centerY = (100-((position.top + height)*100)/$('.care_of_animals_and_plants').height())+'%';
					$(this).css('background','#6AA84F');
					$('.option_box1,.option_box2').css('pointer-events','none');
					$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(470%,-972%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
					play_correct_incorrect_sound(1);
					nav_button_controls(100);
				}
				else{
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$('.care_of_animals_and_plants').width()+'%';
					var centerY = (100-((position.top + height)*100)/$('.care_of_animals_and_plants').height())+'%';
					$(this).css('background','#A30B21');
					$(this).css('pointer-events','none');
					$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(470%,-972%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
					play_correct_incorrect_sound(0);
				}
			});

	    switch(countNext){
				case 0:
				sound_player("p1_s0",1);
				// nav_button_controls(100);
				break;
				case 1:
				sound_player("p1_s1",1);
				break;
				case 2:
				sound_player("instruction",0);
				($lang=="en")?setTimeout(()=>{sound_on_hover('loveanimals','chaseanimals')},4000):setTimeout(()=>{sound_on_hover('loveanimals','chaseanimals')},5000);
				break;
				case 3:
				sound_on_hover('hurtanimals','feedanimals');
				break;
				case 4:
				sound_on_hover('cleantheshed','makedirty');
				break;
	   }
		 function sound_on_hover(soundid1,soundid2){
			 $('.option_box1').mouseenter(function(){
				 sound_player(soundid1,0);
			 });
			 $('.option_box2').mouseenter(function(){
				 sound_player(soundid2,0);
			 });
		 }
  }

/*=====  End of Templates Block  ======*/
function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}
function sound_player(sound_id, next){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		if(next){
		navigationcontroller();
	}
	});
}
function sound_player_trio(sound_id_1, sound_id_2, sound_id_3){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id_1);
	current_sound.play();
	current_sound.on('complete', function(){
			current_sound_1 = createjs.Sound.play(sound_id_2);
			current_sound_1.on('complete',function(){
				sound_player_1(sound_id_3);
			});
	});
}
function sound_player_1(sound_id){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
}

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty("contains_dialog_or_optiondiv")){
			for(var j = 0; j < content[count].contains_dialog_or_optiondiv.length; j++){
				var contains_dialog_or_optiondiv = content[count].contains_dialog_or_optiondiv[j];
				console.log("imageblock", imageblock);
				if(contains_dialog_or_optiondiv.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(contains_dialog_or_optiondiv.dialogueimgid).src;
						//get list of classes
						var classes_list = contains_dialog_or_optiondiv.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			clearInterval(intervalcontroller);
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearInterval(intervalcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[

	// slide0
	{
		contentblockadditionalclass:'light_bg',
		uppertextblock:[{
			textclass:'chapter_title',
			textdata:data.lesson.chapter
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'deer-gif',
				imgclass:'img-l'
			},{
				imgid:'elephant-gif',
				imgclass:'img-r'
			}]
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[{
			textclass:'chapter_title',
			textdata:data.string.p1text1
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'dog-gif',
				imgclass:'img-l-2'
			},{
				imgid:'elephant-gif',
				imgclass:'img-r'
			}]
		}]
	},

	// slide2
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[{
			textclass:'top_text',
			textdata:data.string.p1text2a
		},{
			textclass:'top_below_text',
			textdata:data.string.p1text13
		},{
			textclass:'click_gif_change dog_button',
			textdata:data.string.p1text2
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'dog',
				imgclass:'animal_image '
			},{
				imgid:'hand-icon',
				imgclass:'handicon dog_handicon'
			}]
		}]
	},

	// slide3
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[{
			textclass:'top_text',
			textdata:data.string.p1text3
		},{
			textclass:'top_below_text',
			textdata:data.string.p1text13
		},{
			textclass:'click_gif_change pig_button',
			textdata:data.string.p1text4
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'pig',
				imgclass:'animal_image horizontal_flip'
			},{
				imgid:'hand-icon',
				imgclass:'handicon pig_handicon'
			}]
		}]
	},

	// slide4
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[{
			textclass:'top_text',
			textdata:data.string.p1text5
		},{
			textclass:'top_below_text',
			textdata:data.string.p1text13
		},{
			textclass:'click_gif_change elephant_button',
			textdata:data.string.p1text6
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'elephant',
				imgclass:'animal_image'
			},{
				imgid:'hand-icon',
				imgclass:'handicon elephant_handicon'
			}]
		}]
	},

	// slide5
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[{
			textclass:'top_text',
			textdata:data.string.p1text7
		},{
			textclass:'top_below_text',
			textdata:data.string.p1text13
		},{
			textclass:'click_gif_change monkey_button',
			textdata:data.string.p1text8
		}],
		imageblock:[{
			imagestoshow:[
				{
                    imgid:'monkey_bg',
                    imgclass:'monkeybg'
				},
				{
				imgid:'monkey',
				imgclass:'animal_image'
			},{
				imgid:'hand-icon',
				imgclass:'handicon monkey_handicon'
			}]
		}]
	},

	// slide6
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[{
			textclass:'top_text',
			textdata:data.string.p1text9
		},{
			textclass:'top_below_text',
			textdata:data.string.p1text13
		},{
			textclass:'click_gif_change mouse_button',
			textdata:data.string.p1text10
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mouse',
				imgclass:'animal_image'
			},{
				imgid:'hand-icon',
				imgclass:'handicon mouse_handicon'
			}]
		}]
	},

	// slide7
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[{
			textclass:'top_text',
			textdata:data.string.p1text11
		},{
			textclass:'top_below_text',
			textdata:data.string.p1text13
		},{
			textclass:'click_gif_change horse_button',
			textdata:data.string.p1text12
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'horse',
				imgclass:'animal_image'
			},{
				imgid:'hand-icon',
				imgclass:'handicon horse_handicon'
			}]
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	$('.mainBox > div').prepend('<p class="replay_button"></p>');

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "dog", src: imgpath+"dogtail_00.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cat", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"pigtail_00.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig-gif", src: imgpath+"pigtail_00.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "horse", src: imgpath+"horsetail_00.png", type: createjs.AbstractLoader.IMAGE},
			{id: "horse-gif", src: imgpath+"horsetail_00.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mouse", src: imgpath+"mousetail_00.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouse-gif", src: imgpath+"mousetail_00.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"elephanttail_00.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: imgpath+"monkeytail_00.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey-gif", src: imgpath+"monkeytail_00.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant-gif", src: imgpath+"elephanttail_00.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dog-gif", src: imgpath+"dogtail_00.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hand-icon", src: imgpath+"hand-icon-1.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "deer-gif", src: imgpath+"deer.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey_bg", src: imgpath+"bg_monkey01.png", type: createjs.AbstractLoader.IMAGE},
			//sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3_1.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p3_2.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p4_1.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p4_2.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p5_1.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p5_2.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p6_1.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p6_2.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p7_1.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p7_2.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p8_1.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p9_2.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +6.5+"%";
		$('.top_below_text').css({
			'top':top_val
		});


		switch(countNext) {
			case 0:
			sound_nav("sound_0");
			break;
			case 1:
			sound_nav("sound_1");
			break;
			case 2:
			sound_player("sound_2");
			setTimeout(function(){
				sound_player("sound_14");
			},3000);
			onclick_gif_changer('dog-gif',"sound_3");
			break;
			case 3:
			sound_player("sound_4");
			setTimeout(function(){
				sound_player("sound_14");
			},3000);
			onclick_gif_changer('pig-gif','sound_5');
			break;
			case 4:
			sound_player("sound_6");
			setTimeout(function(){
				sound_player("sound_14");
			},3000);
			onclick_gif_changer('elephant-gif','sound_7');
			break;
			case 5:
			sound_player("sound_8");
			setTimeout(function(){
				sound_player("sound_14");
			},3000);
				$(".animal_image").css({"top":"50%","z-index":"2"});
			onclick_gif_changer('monkey-gif','sound_9');
			break;
			case 6:
			sound_player("sound_10");
			setTimeout(function(){
				sound_player("sound_14");
			},3000);
			onclick_gif_changer('mouse-gif','sound_11');
			break;
			case 7:
			sound_player("sound_12");
			setTimeout(function(){
				sound_player("sound_14");
			},3000);
			onclick_gif_changer('horse-gif','sound_13');
			break;
		}

		function onclick_gif_changer(imgid,sound_id){
			setTimeout(function(){
				$('.click_gif_change,.handicon').click(function(){
					$('.animal_image').attr('src',preload.getResult(imgid).src);
					$(this).css('pointer-events','none');
					$('.top_below_text,.handicon').hide(0);
					sound_nav(sound_id);
				});
			},6000);

		}

	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

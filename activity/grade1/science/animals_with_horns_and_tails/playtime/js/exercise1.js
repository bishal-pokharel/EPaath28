var imgpath = $ref+"/playtime/images/";
var imgpath2 = $ref+"/playtime/images/option/";
var sound_path = $ref+"/audio_"+$lang+"/";
var inst1 = new buzz.sound(sound_path + "inst-01.ogg");
var inst2 = new buzz.sound(sound_path + "inst-02.ogg");
var inst3 = new buzz.sound(sound_path + "inst-03.ogg");
var inst4 = new buzz.sound(sound_path + "inst-04.ogg");
var inst5 = new buzz.sound(sound_path + "inst-05.ogg");
var inst6 = new buzz.sound(sound_path + "inst-06.ogg");
var inst7 = new buzz.sound(sound_path + "inst-07.ogg");
var inst8 = new buzz.sound(sound_path + "inst-08.ogg");
var inst9 = new buzz.sound(sound_path + "inst-09.ogg");

var content=[
	//slide 0
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay1,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'monkey01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'monkey02.png'
		},
		{
			imgclass:'option1 correct',
			imgsrc:imgpath2 + 'monkey02.png'
		},
		{
			imgclass:'option2',
			imgsrc:imgpath2 + 'buffalo02.png'
		},
		{
			imgclass:'option3',
			imgsrc:imgpath2 + 'rhyno02.png'
		},]
	},

	//slide 1
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay2,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'pig01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'pig02.png'
		},
		{
			imgclass:'option1 ',
			imgsrc:imgpath2 + 'monkey02.png'
		},
		{
			imgclass:'option2 correct',
			imgsrc:imgpath2 + 'pig02.png'
		},
		{
			imgclass:'option3',
			imgsrc:imgpath2 + 'buffalo02.png'
		},]
	},

	//slide 2
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay4,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'deer01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'deer02.png'
		},
		{
			imgclass:'option1 ',
			imgsrc:imgpath2 + 'buffalo02.png'
		},
		{
			imgclass:'option2 ',
			imgsrc:imgpath2 + 'monkey02.png'
		},
		{
			imgclass:'option3 correct',
			imgsrc:imgpath2 + 'deer02.png'
		},]
	},

	//slide 3
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay5,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'rat02.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'rat01.png'
		},
		{
			imgclass:'option1 ',
			imgsrc:imgpath2 + 'horse02.png'
		},
		{
			imgclass:'option2 ',
			imgsrc:imgpath2 + 'dog02.png'
		},
		{
			imgclass:'option3 correct',
			imgsrc:imgpath2 + 'rat01.png'
		},]
	},

	//slide 4
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay6,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'rhyno01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'rhyno02.png'
		},
		{
			imgclass:'option1 ',
			imgsrc:imgpath2 + 'buffalo02.png'
		},
		{
			imgclass:'option2 correct',
			imgsrc:imgpath2 + 'rhyno02.png'
		},
		{
			imgclass:'option3 ',
			imgsrc:imgpath2 + 'monkey02.png'
		},]
	},

	//slide 5
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay3,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'buffalo01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'buffalo02.png'
		},
		{
			imgclass:'option1 correct',
			imgsrc:imgpath2 + 'buffalo02.png'
		},
		{
			imgclass:'option2 ',
			imgsrc:imgpath2 + 'monkey02.png'
		},
		{
			imgclass:'option3 ',
			imgsrc:imgpath2 + 'rhyno02.png'
		},]
	},

	//slide 6
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay7,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'dog01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'dog02.png'
		},
		{
			imgclass:'option1',
			imgsrc:imgpath2 + 'horse02.png'
		},
		{
			imgclass:'option2 ',
			imgsrc:imgpath2 + 'monkey02.png'
		},
		{
			imgclass:'option3 correct',
			imgsrc:imgpath2 + 'dog02.png'
		},]
	},

	//slide 7
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay8,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'elephant01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'elephant02.png'
		},
		{
			imgclass:'option1',
			imgsrc:imgpath2 + 'goat02.png'
		},
		{
			imgclass:'option2 ',
			imgsrc:imgpath2 + 'monkey02.png'
		},
		{
			imgclass:'option3 correct',
			imgsrc:imgpath2 + 'elephant02.png'
		},]
	},

	//slide 8
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay9,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'horse01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'horse02.png'
		},
		{
			imgclass:'option1 correct',
			imgsrc:imgpath2 + 'horse02.png'
		},
		{
			imgclass:'option3 ',
			imgsrc:imgpath2 + 'rat01.png'
		},
		{
			imgclass:'option2 ',
			imgsrc:imgpath2 + 'monkey02.png'
		},]
	},

	//slide 9
	{
		contentblockadditionalclass:'greybg',
		extratextblock : [
		{
			textdata : data.string.eplay10,
			textclass : 'instruction',
		}
		],
		imageblock:[
		{
			imgclass:'main_question_image',
			imgsrc:imgpath + 'cow01.png'
		},
		{
			imgclass:'questionimage_overlap',
			imgsrc:imgpath + 'cow02.png'
		},
		{
			imgclass:'option2 correct',
			imgsrc:imgpath2 + 'cow02.png'
		},
		{
			imgclass:'option1 ',
			imgsrc:imgpath2 + 'dog02.png'
		},
		{
			imgclass:'option3 ',
			imgsrc:imgpath2 + 'pig02.png'
		},]
	},
	{
		contentblockadditionalclass:'green',
		imageslider:[{}],
		uppertextblock:[{
			textclass:'playagain',
			textdata:data.string.playagain
		},{
			textclass:'mainmenu',
			textdata:data.string.mainmenu
		},{
			textclass:'learnagain',
			textdata:data.string.learnagain
		}]

	}


];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	// var rhino = new NumberTemplate();

	// rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);


		$('.questionimage_overlap').hide(0);
		$('.option1,.option2,.option3').click(function(){
			if($(this).hasClass('correct')){
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
				var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
				$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(229%,-150%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);

				$(this).css('background','#92AF3B');
				$('.option1,.option2,.option3').css({'pointer-events':'none'})
				$('.questionimage_overlap').fadeIn(500);
				play_correct_incorrect_sound(1);

				$nextBtn.show(0);
			}
			else{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
				var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
				$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(229%,-150%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				$(this).css({'background':'#980000','pointer-events':'none'});
				play_correct_incorrect_sound(0);

			}
		});

		$('.slide-image1').attr('src',imgpath+"exe_1_1.png");
		$('.slide-image2').attr('src',imgpath+"exe_1_2.png");
		$('.slide-image3').attr('src',imgpath+"exe_1_3.png");
		$('.next-button').attr('src','images/next.png');
		$('.prev-button').attr('src','images/previous.png');

		$('.next-button').click(function(){

			$('.next-button,.prev-button').css('pointer-events','none');
				$('.left').animate({
					'left':'33.33333%',
					'opacity':'1'
				},1000,function(){
					$(this).addClass('mid').removeClass('left');
				});
				$('.mid').animate({
					'left':'66.66666%',
					'opacity':'.3'
				},1000,function(){
					$(this).addClass('right').removeClass('mid');
				});
				$('.right').animate({
					'left':'83.332665%'
				},500,function(){
					$('.right').css('left','-16.666665%');
					$('.right').animate({
						'left':'0%',
						'opacity':'.3'
					},500,function(){
						$('.prev-button,.next-button').css('pointer-events','auto')
						$(this).addClass('left').removeClass('right');
					});
				});
		});
		$('.prev-button').click(function(){
			$('.next-button,.prev-button').css('pointer-events','none');
				$('.mid').animate({
					'left':'0%',
					'opacity':'.3'
					},1000,function(){
					$(this).addClass('left').removeClass('mid');
				});
				$('.right').animate({
					'left':'33.333333%',
					'opacity':'1'
				},1000,function(){
					$(this).addClass('mid').removeClass('right');
				});
				$('.left').animate({
					'left':'-16.666665%'
				},500,function(){
					$('.left').css('left','100%');
					$('.left').animate({
						'left':'66.66666%',
						'opacity':'.3'
					},500,function(){
						$('.prev-button,.next-button').css('pointer-events','auto')
						$(this).addClass('right').removeClass('left');
					});
				});
		});

		$('.playagain').click(function(){
			$('#activity-page-exercise-tab').find('button').trigger('click');
		});
		$('.learnagain').click(function(){
			$("#activity-page-lesson-tab").find('button').trigger('click');

		});
		$('.mainmenu').click(function(){
			$("#activity-page-menu-img").trigger("click");

		});


		switch(countNext){
			case 1:
			soundplayer(inst1);
			break;

			case 2:
			soundplayer(inst2);
			break;

			case 3:
			soundplayer(inst3);
			break;

			case 4:
			soundplayer(inst4);
			break;

			case 5:
			soundplayer(inst5);
			break;

			case 6:
			soundplayer(inst6);
			break;

			case 7:
			soundplayer(inst7);
			break;

			case 8:
			soundplayer(inst8);
			break;

			case 9:
			soundplayer(inst9);
			break;
		}

	}
	function soundplayer(i){
		buzz.all().stop();
		i.play();
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();
		loadTimelineProgress($total_page, countNext + 1);

		// call the template
		generalTemplate();
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

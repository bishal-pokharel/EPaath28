var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-ex',

		uppertextblockadditionalclass: 'playtime-text',
		uppertextblock:[{
			textdata: data.string.eplay,
			textclass: "",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-playtime",
					imgsrc : '',
					imgid : 'bg'
				},{
					imgclass : "rhino",
					imgsrc : '',
					imgid : 'rhino'
				},{
					imgclass : "squirrel",
					imgsrc : '',
					imgid : 'squirrel'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins1,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'lamb',
			textdata:  data.string.e1text1a,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'kitten',
			textdata:  data.string.e1text1b,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.e1text1,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'sheep',
			childid: 'lamb'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide2 pig
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins2,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'piglet',
			textdata:  data.string.e1text2a,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'calf',
			textdata:  data.string.e1text2b,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.e1text2,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'pig',
			motherclass: 'pigmother',
			childid: 'piglet'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide3 dog
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins3,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'puppy',
			textdata:  data.string.e1text3a,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'kitten',
			textdata:  data.string.e1text3b,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.e1text3,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'dog',
			childid: 'puppy'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide4 cow
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins4,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'calf',
			textdata:  data.string.e1text4a,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'lamb',
			textdata:  data.string.e1text4b,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.e1text4,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'cow',
			motherclass: 'cowmother',
			childid: 'calf'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide5 duck
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins5,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'duckling',
			textdata:  data.string.e1text5a,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'chick',
			textdata:  data.string.e1text5b,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.e1text5,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'duck',
			childclass: 'duckchild',
			childid: 'duckling'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},



	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		extratextblock:[{
			textdata: data.string.emsg,
			textclass: "msg-last",
		}],

		optionblockadditionalclass: 'end-option-block',
		optionblock:[{
			optionclass: 'end-options eopt-1',
			imgclass: 'cloud-ex',
			imgid : 'cloud-ex',
			textdata:  data.string.end1,
		},{
			optionclass: 'end-options eopt-2',
			imgclass: 'cloud-ex',
			imgid : 'cloud-ex',
			textdata:  data.string.end2,
		},{
			optionclass: 'end-options eopt-3',
			imgclass: 'cloud-ex',
			imgid : 'cloud-ex',
			textdata:  data.string.end3,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				}
			]
		}],

		boatblock: [{
			boatclass: 'boat-2 boat-last-1',
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'sheep',
			childclass: 'not-hidden',
			childid: 'lamb'
		},{
			boatclass: 'boat-2 boat-last-2',
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'pig',
			motherclass: 'pigmother',
			childclass: 'not-hidden',
			childid: 'piglet'
		},{
			boatclass: 'boat-2 boat-last-3',
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'dog',
			childclass: 'not-hidden',
			childid: 'puppy'
		},{
			boatclass: 'boat-2 boat-last-4',
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'cow',
			motherclass: 'cowmother',
			childclass: 'not-hidden',
			childid: 'calf'
		},{
			boatclass: 'boat-2 boat-last-5',
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'duck',
			childclass: 'duckchild not-hidden',
			childid: 'duckling'
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg", src: "images/grade1_playtime/bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: "images/grade1_playtime/squirrel.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: "images/grade1_playtime/rhino.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "flag", src: imgpath+"ex/flag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boat", src: imgpath+"ex/boat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-1", src: imgpath+"ex/wave01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-2", src: imgpath+"ex/wave02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-3", src: imgpath+"ex/wave03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cloud-1", src: imgpath+"cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-3", src: imgpath+"cloud03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-ex", src: imgpath+"ex/cloud_ex.png", type: createjs.AbstractLoader.IMAGE},



			{id: "sheep", src: imgpath+"sheep.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lamb", src: imgpath+"lamb.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kitten", src: imgpath+"kitten.png", type: createjs.AbstractLoader.IMAGE},

			{id: "pig", src: imgpath+"pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "piglet", src: imgpath+"piglet.png", type: createjs.AbstractLoader.IMAGE},
			{id: "calf", src: imgpath+"calf.png", type: createjs.AbstractLoader.IMAGE},

			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "puppy", src: imgpath+"puppy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duckling", src: imgpath+"duckling.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cow", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chick", src: imgpath+"chick.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_play", src: soundAsset+"playtime.ogg"},
			{id: "sound_1", src: soundAsset+"ex1.ogg"},
			{id: "sound_2", src: soundAsset+"ex2.ogg"},
			{id: "sound_3", src: soundAsset+"ex3.ogg"},
			{id: "sound_4", src: soundAsset+"ex4.ogg"},
			{id: "sound_5", src: soundAsset+"ex5.ogg"},
			{id: "sound_6", src: soundAsset+"ex6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_image3(content, countNext);

		switch(countNext) {
			case 6:
				// var props = new createjs.PlayPropsConfig().set({interrupt: createjs.Sound.INTERRUPT_ANY, loop: -1});
				// createjs.Sound.play("sound_jump2", props);
				$('.eopt-1').click(playExAgain);
				$('.eopt-2').click(gotoLesson);
				$('.eopt-3').click(go_to_menu_page);
				sound_player('sound_6');
				break;
			default:
				sound_player('sound_'+countNext);
				$('.boatdiv').animate({'left': '10%'}, 3000, "swing");
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				//to randomize option
				if(Math.random()>0.5) $('.optiondiv').eq(0).css('order', '2');
				$('.optiondiv').click(function(){
					if($(this).hasClass('class-1')){
						var answerText = $(this).children('p').html();
						play_correct_incorrect_sound(1);
						$(this).children('.correct-icon').show(0);
						$(this).addClass('correct-answer');
						$('.childtext').html(answerText);
						$('.childtext').css('background-color', 'transparent');
						$('.boat-child').show(0);
						$('.optiondiv').css('pointer-events', 'none');
						$nextBtn.show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
						$(this).children('.incorrect-icon').show(0);
						$(this).addClass('incorrect-answer');
					}
				});
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('optionblock')){
			var imageClass = content[count].optionblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_image3(content, count){
		if(content[count].hasOwnProperty('boatblock')){
			var imageClass = content[count].boatblock;
			for(var i=0; i<imageClass.length; i++){
				var mSrc = preload.getResult(imageClass[i].motherid).src;
				var cSrc = preload.getResult(imageClass[i].childid).src;
				var bSrc = preload.getResult(imageClass[i].boatid).src;
				var boatClass = imageClass[i].boatclass.match(/\S+/g) || [];
				var mSel = ('.'+boatClass[boatClass.length-1]+'>.boat-mother');
				var cSel = ('.'+boatClass[boatClass.length-1]+'>.boat-child');
				var bSel = ('.'+boatClass[boatClass.length-1]+'>.boatbody');
				if(imageClass[i].hasOwnProperty('flagid')){
					var fSrc = preload.getResult(imageClass[i].flagid).src;
					var fSel = ('.'+boatClass[boatClass.length-1]+'>.boat-flag');
					$(fSel).attr('src', fSrc);
				}
				$(mSel).attr('src', mSrc);
				$(cSel).attr('src', cSrc);
				$(bSel).attr('src', bSrc);
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			case 0:
				countNext++;
				templateCaller();
				break;
			default:
				$nextBtn.hide(0);
				$('.optionsblock').removeClass('slideRightIn').addClass('slideRightOut');
				timeoutvar = setTimeout(function(){
					$('.boatdiv').animate({'left': '110%'}, 4000, "swing", function(){
						countNext++;
						templateCaller();
					});
				}, 1000);
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

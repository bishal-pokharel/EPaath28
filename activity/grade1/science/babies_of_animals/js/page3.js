var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';;

var content=[
	// cow
	// slide20
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "what-text what-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cowmother1 center-animal fadeInfast",
					imgsrc : '',
					imgid : 'cow'
				}
			]
		}],
	},
	// slide21
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text9,
			textclass: "this-text this-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cowmother1 center-animal",
					imgsrc : '',
					imgid : 'cow'
				}
			]
		}],
	},
	// slide22
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text9c,
			textclass: "this-text this-anim",
		}],

		nametagblock:[
		{
			textdata: data.string.p1text9a,
			tagclass: "name-tag mother-tag",
		},
		{
			textdata: data.string.p1text9b,
			tagclass: "name-tag child-tag",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cowmother mother-animal",
					imgsrc : '',
					imgid : 'cow'
				},
				{
					imgclass : "child-animal",
					imgsrc : '',
					imgid : 'calf'
				}
			]
		}],
	},
	// horse
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "what-text what-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal motherhorse fadeInfast",
					imgsrc : '',
					imgid : 'horse'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text10,
			textclass: "this-text this-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal motherhorse",
					imgsrc : '',
					imgid : 'horse'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text10c,
			textclass: "this-text this-anim",
		}],

		nametagblock:[
		{
			textdata: data.string.p1text10a,
			tagclass: "name-tag mother-tag",
		},
		{
			textdata: data.string.p1text10b,
			tagclass: "name-tag child-tag",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mother-animal motherhorse",
					imgsrc : '',
					imgid : 'horse'
				},
				{
					imgclass : "child-animal childhorse",
					imgsrc : '',
					imgid : 'foal'
				}
			]
		}],
	},
	// lion
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "what-text what-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal motherlion fadeInfast",
					imgsrc : '',
					imgid : 'lion'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text11,
			textclass: "this-text this-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal motherlion",
					imgsrc : '',
					imgid : 'lion'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text11c,
			textclass: "this-text this-anim",
		}],

		nametagblock:[
		{
			textdata: data.string.p1text11a,
			tagclass: "name-tag mother-tag",
		},
		{
			textdata: data.string.p1text11b,
			tagclass: "name-tag child-tag",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mother-animal motherlion",
					imgsrc : '',
					imgid : 'lion'
				},
				{
					imgclass : "child-animal childtiger",
					imgsrc : '',
					imgid : 'cub'
				}
			]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cow-gif", src: imgpath+"gif/cow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "calf-gif", src: imgpath+"gif/calf.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"first-frames/cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "calf", src: imgpath+"first-frames/calf.png", type: createjs.AbstractLoader.IMAGE},

			{id: "lion-gif", src: imgpath+"tiger.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cub-gif", src: imgpath+"cubby.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "lion", src: imgpath+"tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cub", src: imgpath+"cubby.png", type: createjs.AbstractLoader.IMAGE},

			{id: "horse-gif", src: imgpath+"gif/horse.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "foal-gif", src: imgpath+"gif/foal.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "horse", src: imgpath+"first-frames/horse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "foal", src: imgpath+"first-frames/foal.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sound-icon", src: imgpath+"sound-icon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-1", src: imgpath+"cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-3", src: imgpath+"cloud03.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p3_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p3_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p3_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p3_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p3_s12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		var motherGif = ['cow-gif', 'horse-gif', 'lion-gif'];
		var childGif = ['calf-gif', 'foal-gif', 'cub-gif'];
		var motherSound = ['sound_3', 'sound_7', 'sound_11'];
		var childSound = ['sound_4', 'sound_8', 'sound_12'];
		var thisChildSound = ['sound_2', 'sound_6', 'sound_10'];
		$('.motherlion').css('height','42%');
		$('.childtiger').css('bottom','24%');
		switch(countNext) {
			case 0:
			case 3:
			case 6:
				if(countNext>0) $prevBtn.show(0);
				setTimeout(function(){
				sound_nav('sound_0');
			},1000);

				break;
			case 1:
				$prevBtn.show(0);
				sound_nav('sound_1');
				break;
			case 4:
				$prevBtn.show(0);
				sound_nav('sound_5');
				break;
			case 7:
				$prevBtn.show(0);
				sound_nav('sound_9');
				break;
			case 2:
			case 5:
			case 8:
				$prevBtn.show(0);
				var curIndex = parseInt((countNext-2)/3);
				$('.tag-audio-icon').attr('src', preload.getResult('sound-icon').src);
				$('.mother-animal, .child-animal, .name-tag, .this-text').hide(0);
				$('.name-tag').css('pointer-events', 'none');
				$('.mother-animal').fadeIn(500, function(){
					$('.mother-tag').fadeIn(500, function(){
						current_sound = createjs.Sound.play(motherSound[curIndex]);
						current_sound.play();
						current_sound.on("complete", function(){
							$('.child-animal').fadeIn(500, function(){
								$('.child-tag').fadeIn(500, function(){
									current_sound = createjs.Sound.play(childSound[curIndex]);
									current_sound.play();
									current_sound.on("complete", function(){
										$('.this-text').show(0);
										current_sound = createjs.Sound.play(thisChildSound[curIndex]);
										current_sound.play();
										current_sound.on("complete", function(){
											$('.mother-animal').attr('src', preload.getResult(motherGif[curIndex]).src);
											$('.child-animal').attr('src', preload.getResult(childGif[curIndex]).src);
											nav_button_controls(0);
											$('.name-tag').css('pointer-events', 'all');
										});
									});
								});
							});
						});
					});
				});
				$('.mother-tag').click(function(){
					sound_player(motherSound[curIndex]);
				});
				$('.child-tag').click(function(){
					sound_player(childSound[curIndex]);
				});
				break;
			default:
				if(countNext>0) $prevBtn.show(0);
				sound_nav('sound_1');
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-front',

		extratextblock:[{
			textdata: data.string.title,
			textclass: "lesson-title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},
				{
					imgclass : "cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				},
				{
					imgclass : "sheep-1",
					imgsrc : '',
					imgid : 'sheep-gif'
				},
				{
					imgclass : "lamb-1",
					imgsrc : '',
					imgid : 'lamb-gif'
				},
				{
					imgclass : "pig-1",
					imgsrc : '',
					imgid : 'pig-gif'
				},
				{
					imgclass : "piglet-1",
					imgsrc : '',
					imgid : 'piglet-gif'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-front',

		extratextblock:[{
			textdata: data.string.p1text1,
			textclass: "second-title",
		}]
	},

	// sheep
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "what-text what-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal fadeInfast",
					imgsrc : '',
					imgid : 'sheep'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text3,
			textclass: "this-text this-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal",
					imgsrc : '',
					imgid : 'sheep'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text3c,
			textclass: "this-text this-anim",
		}],

		nametagblock:[
		{
			textdata: data.string.p1text3a,
			tagclass: "name-tag mother-tag",
		},
		{
			textdata: data.string.p1text3b,
			tagclass: "name-tag child-tag",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mother-animal",
					imgsrc : '',
					imgid : 'sheep'
				},
				{
					imgclass : "child-animal",
					imgsrc : '',
					imgid : 'lamb'
				}
			]
		}],
	},


	// hen
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "what-text what-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal fadeInfast",
					imgsrc : '',
					imgid : 'hen'
				}
			]
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text4,
			textclass: "this-text this-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal",
					imgsrc : '',
					imgid : 'hen'
				}
			]
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text4c,
			textclass: "this-text this-anim",
		}],

		nametagblock:[
		{
			textdata: data.string.p1text4a,
			tagclass: "name-tag mother-tag",
		},
		{
			textdata: data.string.p1text4b,
			tagclass: "name-tag child-tag",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mother-animal",
					imgsrc : '',
					imgid : 'hen'
				},
				{
					imgclass : "child-animal",
					imgsrc : '',
					imgid : 'chick'
				}
			]
		}],
	},


	// dog
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "what-text what-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal fadeInfast",
					imgsrc : '',
					imgid : 'dog'
				}
			]
		}],
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text5,
			textclass: "this-text this-anim",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-animal",
					imgsrc : '',
					imgid : 'dog'
				}
			]
		}],
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata: data.string.p1text5c,
			textclass: "this-text this-anim",
		}],

		nametagblock:[
		{
			textdata: data.string.p1text5a,
			tagclass: "name-tag mother-tag",
		},
		{
			textdata: data.string.p1text5b,
			tagclass: "name-tag child-tag",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mother-animal",
					imgsrc : '',
					imgid : 'dog'
				},
				{
					imgclass : "child-animal",
					imgsrc : '',
					imgid : 'puppy'
				}
			]
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "sheep-gif", src: imgpath+"gif/sheep.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "lamb-gif", src: imgpath+"gif/lamb.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sheep", src: imgpath+"first-frames/sheep.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lamb", src: imgpath+"first-frames/lamb.png", type: createjs.AbstractLoader.IMAGE},

			{id: "dog-gif", src: imgpath+"gif/dog.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "puppy-gif", src: imgpath+"gif/puppy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "puppy", src: imgpath+"puppy.png", type: createjs.AbstractLoader.IMAGE},

			{id: "pig-gif", src: imgpath+"gif/pig.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "piglet-gif", src: imgpath+"gif/piglet.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "hen-gif", src: imgpath+"gif/hen.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "chick-gif", src: imgpath+"gif/chick.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chick", src: imgpath+"chick.png", type: createjs.AbstractLoader.IMAGE},


			{id: "sound-icon", src: imgpath+"sound-icon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-1", src: imgpath+"cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-3", src: imgpath+"cloud03.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p1_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p1_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p1_s14.ogg"},
			{id: "sound_15", src: soundAsset+"p1_s15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		var motherGif = ['sheep-gif', 'hen-gif', 'dog-gif'];
		var childGif = ['lamb-gif', 'chick-gif', 'puppy-gif'];
		var motherSound = ['sound_6', 'sound_10', 'sound_14'];
		var childSound = ['sound_7', 'sound_11', 'sound_15'];
		var thisChildSound = ['sound_5', 'sound_9', 'sound_13'];

		switch(countNext) {
			case 0:
				sound_nav('sound_1');
				break;
			case 1:
				$prevBtn.show(0);
				sound_nav('sound_2');
				break;
			//what animal part
			case 2:
			case 5:
			case 8:
				$prevBtn.show(0);
				setTimeout(function(){
						sound_nav('sound_3');
				},1000);

				break;
			//this is an animal part
			case 3:
				$prevBtn.show(0);
				sound_nav('sound_4');
				break;
			case 6:
				$prevBtn.show(0);
				sound_nav('sound_8');
				break;
			case 9:
				$prevBtn.show(0);
				sound_nav('sound_12');
				break;
			//final animation parts
			case 4:
			case 7:
			case 10:
				$prevBtn.show(0);
				var curIndex = parseInt((countNext-1)/3) - 1;
				$('.tag-audio-icon').attr('src', preload.getResult('sound-icon').src);
				$('.mother-animal, .child-animal, .name-tag, .this-text').hide(0);
				$('.name-tag').css('pointer-events', 'none');
				$('.mother-animal').fadeIn(500, function(){
					$('.mother-tag').fadeIn(500, function(){
						current_sound = createjs.Sound.play(motherSound[curIndex]);
						current_sound.play();
						current_sound.on("complete", function(){
							$('.child-animal').fadeIn(500, function(){
								$('.child-tag').fadeIn(500, function(){
									current_sound = createjs.Sound.play(childSound[curIndex]);
									current_sound.play();
									current_sound.on("complete", function(){
										$('.this-text').show(0);
										current_sound = createjs.Sound.play(thisChildSound[curIndex]);
										current_sound.play();
										current_sound.on("complete", function(){
											$('.mother-animal').attr('src', preload.getResult(motherGif[curIndex]).src);
											$('.child-animal').attr('src', preload.getResult(childGif[curIndex]).src);
											nav_button_controls(0);
											$('.name-tag').css('pointer-events', 'all');
										});
									});
								});
							});
						});
					});
				});
				$('.mother-tag').click(function(){
					sound_player(motherSound[curIndex]);
				});
				$('.child-tag').click(function(){
					sound_player(childSound[curIndex]);
				});
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_'+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/playtime/";
var imgpath2 = $ref + "/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[
	// slide0
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg1",
			extratextblock: [
					{
							textclass: "playtime-text",
							textdata: data.string.playtime
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							}]
			}]
	},
	// slide1
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textdata: data.string.e1s1,
			textclass: "top-text",
		},],
		imageblock:[{
			imagestoshow:[
				{
					imgid:'mainbg',
					imgclass:'bg_full'
				},
				{
				imgid:'monkey1',
				imgclass:'bg_full one'
			},
			{
				imgid:'monkey2',
				imgclass:'bg_full two'
			},
			{
				imgclass: "banana",
				imgid: "five_banana"
			}]
		}],
		optionblock:[{
			additionaloptionblockclass:'option1',
			textdata:data.string.e1s3,
			textclass:'option_inside_text',
			imgclass: "option_center_image deer",
			imgid: "deer"
		},{
			additionaloptionblockclass:'option2 correct',
			textdata:data.string.e1s2,
			textclass:'option_inside_text',
			imgclass: "option_center_image duck",
			imgid: "duck"
		}]
	},
	//slide2
	{
		contentblockadditionalclass:'bluebg',
		imageblock:[{
				imagestoshow: [
						{
							imgid:'nextbg',
							imgclass:'bg_full'
						},
						{
								imgclass: "deergif",
								imgid: "deer-gif",
								imgsrc: "",
						},
						{
								imgclass: "duckgif",
								imgid: "duck-gif",
								imgsrc: "",
						}]
		}],
		uppertextblock:[{
			textdata: data.string.playtime1,
			textclass: "middle_text",
		}]
	},
	//slide3
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textdata: data.string.e1s1,
			textclass: "top-text",
		},],
		imageblock:[{
			imagestoshow:[
				{
					imgid:'mainbg',
					imgclass:'bg_full'
				},
				{
				imgid:'monkey2',
				imgclass:'bg_full one'
			},
			{
				imgid:'monkey3',
				imgclass:'bg_full two'
			},
			{
				imgclass: "banana",
				imgid: "five_banana"
			}]
		}],
		optionblock:[{
			additionaloptionblockclass:'option1',
			textdata:data.string.e1s5,
			textclass:'option_inside_text',
			imgclass: "option_center_image pig",
			imgid: "pig"
		},{
			additionaloptionblockclass:'option2 correct',
			textdata:data.string.e1s4,
			textclass:'option_inside_text',
			imgclass: "option_center_image parrot",
			imgid: "parrot"
		}]
	},
	//slide4
	{
		contentblockadditionalclass:'bluebg',
		imageblock:[{
				imagestoshow: [
						{
							imgid:'nextbg',
							imgclass:'bg_full'
						},
						{
								imgclass: "piggif",
								imgid: "pig-gif",
								imgsrc: "",
						},
						{
								imgclass: "parrotgif",
								imgid: "parrot-gif",
								imgsrc: "",
						}]
		}],
		uppertextblock:[{
			textdata: data.string.playtime2,
			textclass: "middle_text",
		}]
	},
	//slide5
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textdata: data.string.e1s1,
			textclass: "top-text",
		},],
		imageblock:[{
			imagestoshow:[
				{
					imgid:'mainbg',
					imgclass:'bg_full'
				},
				{
				imgid:'monkey3',
				imgclass:'bg_full one'
			},
			{
				imgid:'monkey4',
				imgclass:'bg_full two'
			},
			{
				imgclass: "banana",
				imgid: "five_banana"
			}]
		}],
		optionblock:[{
			additionaloptionblockclass:'option1',
			textdata:data.string.e1s6,
			textclass:'option_inside_text',
			imgclass: "option_center_image lamb",
			imgid: "lamb"
		},{
			additionaloptionblockclass:'option2 correct',
			textdata:data.string.e1s7,
			textclass:'option_inside_text',
			imgclass: "option_center_image bee",
			imgid: "bee"
		}]
	},
	//slide6
	{
		contentblockadditionalclass:'bluebg',
		imageblock:[{
				imagestoshow: [
						{
							imgid:'nextbg',
							imgclass:'bg_full'
						},
						{
								imgclass: "lambgif",
								imgid: "lamb-gif",
								imgsrc: "",
						},
						{
								imgclass: "beegif",
								imgid: "bee-gif",
								imgsrc: "",
						}]
		}],
		uppertextblock:[{
			textdata: data.string.playtime3,
			textclass: "middle_text",
		}]
	},
	//slide7
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textdata: data.string.e1s1,
			textclass: "top-text",
		},],
		imageblock:[{
			imagestoshow:[
				{
					imgid:'mainbg',
					imgclass:'bg_full'
				},
				{
				imgid:'monkey4',
				imgclass:'bg_full one'
			},
			{
				imgid:'monkey5',
				imgclass:'bg_full two'
			},
			{
				imgclass: "banana",
				imgid: "five_banana"
			}]
		}],
		optionblock:[{
			additionaloptionblockclass:'option1 correct',
			textdata:data.string.e1s8,
			textclass:'option_inside_text',
			imgclass: "option_center_image ladybug",
			imgid: "ladybug"
		},{
			additionaloptionblockclass:'option2',
			textdata:data.string.e1s9,
			textclass:'option_inside_text',
			imgclass: "option_center_image human",
			imgid: "human"
		}]
	},
	//slide8
	{
		contentblockadditionalclass:'bluebg',
		imageblock:[{
				imagestoshow: [
						{
							imgid:'nextbg',
							imgclass:'bg_full'
						},
						{
								imgclass: "ladybuggif",
								imgid: "ladybug-gif",
								imgsrc: "",
						},
						{
								imgclass: "humangif",
								imgid: "human-gif",
								imgsrc: "",
						}]
		}],
		uppertextblock:[{
			textdata: data.string.playtime4,
			textclass: "middle_text",
		}]
	},
	//slide9
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textdata: data.string.e1s1,
			textclass: "top-text",
		},],
		imageblock:[{
			imagestoshow:[
				{
					imgid:'mainbg',
					imgclass:'bg_full'
				},
				{
				imgid:'monkey5',
				imgclass:'bg_full one'
			},
			{
				imgid:'monkey6',
				imgclass:'bg_full two'
			},
			{
				imgclass: "banana",
				imgid: "five_banana"
			}]
		}],
		optionblock:[{
			additionaloptionblockclass:'option1 correct',
			textdata:data.string.e1s10,
			textclass:'option_inside_text',
			imgclass: "option_center_image crow",
			imgid: "crow"
		},{
			additionaloptionblockclass:'option2',
			textdata:data.string.e1s11,
			textclass:'option_inside_text',
			imgclass: "option_center_image snake",
			imgid: "snake"
		}]
	},
	//slide10
	{
		contentblockadditionalclass:'bluebg',
		imageblock:[{
				imagestoshow: [
						{
							imgid:'nextbg',
							imgclass:'bg_full'
						},
						{
								imgclass: "crowgif",
								imgid: "crow-gif",
								imgsrc: "",
						},
						{
								imgclass: "snakegif",
								imgid: "snake-gif",
								imgsrc: "",
						}]
		}],
		uppertextblock:[{
			textdata: data.string.playtime5,
			textclass: "middle_text",
		}]
	},
	//slide 11
	{
		contentblockadditionalclass:'bluebg',
		imageblock:[{
			imagestoshow:[
				{
					imgid:'mainbg',
					imgclass:'bg_full'
				},
			{
				imgid:'monkey6',
				imgclass:'bg_full two'
			},
			{
				imgclass: "banana",
				imgid: "five_banana"
			}]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);


	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "mainbg", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nextbg", src: imgpath+"bg_landscape.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mainbg2", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
		//	{id: "pen", src: imgpath2+"pen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey1", src: imgpath+"monkey01.png", type: createjs.AbstractLoader.IMAGE},
		//	{id: "bun", src: imgpath2+"bun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey2", src: imgpath+"monkey02.png", type: createjs.AbstractLoader.IMAGE},
	//		{id: "cat", src: imgpath2+"cat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey3", src: imgpath+"monkey03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey4", src: imgpath+"monkey04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey5", src: imgpath+"monkey05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey6", src: imgpath+"monkey06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deer", src: imgpath+"deer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
			{id: "parrot", src: imgpath+"parrot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lamb", src: imgpath+"lamb.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bee", src: imgpath+"bee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "human", src: imgpath+"human.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ladybug", src: imgpath+"ladybug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crow", src: imgpath+"crow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "snake", src: imgpath+"snake.png", type: createjs.AbstractLoader.IMAGE},

			{id: "deer-gif", src: imgpath+"deer.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-gif", src: imgpath+"duck.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "parrot-gif", src: imgpath+"parrot_new.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pig-gif", src: imgpath+"pig.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "lamb-gif", src: imgpath+"lamb.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bee-gif", src: imgpath+"bee.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "human-gif", src: imgpath+"human.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "ladybug-gif", src: imgpath+"ladybug.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "crow-gif", src: imgpath+"crow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "snake-gif", src: imgpath+"snake.gif", type: createjs.AbstractLoader.IMAGE},
	//		{id: "pot", src: imgpath2+"pot.png", type: createjs.AbstractLoader.IMAGE},
		//	{id: "pin", src: imgpath2+"pin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinodance", src: imgpath2+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath2+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "five_banana", src: imgpath+"five_banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "four_banana", src: imgpath+"four_banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: imgpath+"l-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: imgpath+"lb-1.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"correct.ogg"},
			{id: "sound_2", src: soundAsset+"incorrect.ogg"},
			{id: "sound_3", src: soundAsset+"monkeysound.mp3"},
			{id: "sound_4", src: soundAsset+"ex_s1_p12_end.ogg"},
			{id: "sound_0", src: soundAsset+"ex_s1_p2_Which animal has wings.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		preload_option_img('deer');
		preload_option_img('duck');
		preload_option_img('parrot');
		preload_option_img('pig');
		preload_option_img('lamb');
		preload_option_img('bee');
		preload_option_img('human');
		preload_option_img('ladybug');
		preload_option_img('crow');
		preload_option_img('snake');

		switch(countNext){
			case 2:case 4:case 6:case 8:case 10:
			nav_button_controls(4000);
			break;

		}
		if(countNext==1){
					$(".textblock  >p").prepend(countNext+". ");
				setTimeout(function(){
					sound_player("sound_0");
				},1000);
		}else if(countNext==3){
			$(".textblock  >p").prepend(countNext - "1" +". ");
			setTimeout(function(){
				sound_player("sound_0");
			},1000);
		}else if(countNext==5){
				$(".textblock  >p").prepend(countNext - "2" +". ");
				setTimeout(function(){
					sound_player("sound_0");
				},1000);
		}else if(countNext==7){
				$(".textblock  >p").prepend(countNext - "3" +". ");
				setTimeout(function(){
					sound_player("sound_0");
				},1000);
		}else if(countNext==9){
				$(".textblock  >p").prepend(countNext - "4" +". ");
				setTimeout(function(){
					sound_player("sound_0");
				},1000);
		}else if(countNext==11){
			setTimeout(function(){
				sound_player("sound_4");
			},3500);
		}
		if( countNext>0 && countNext == $total_page-1){

			// console.log('this is ');
			$('.coverboardfull').append(
				"<p class='.template-dialougebox2-right-white monkeydialogue'>"+data.string.e1s12+"</p>" +
				"<div class='btnNavigationSong'>" +
					"<div class='playAgainDiv'><p name='playbtn' class='playagaintext' onclick='playExAgain()'>"+data.string.playagain+"</p><img class='clouds' ></div>" +
					"<div class='mainMenuDiv'><p name='playbtn'  class='playagaintext' onclick='go_to_menu_page(true)'>"+data.string.mainmenu+"</p><img class='clouds' ></div>" +
		"<div class='learnAgainDiv'><p name='learnbtn'  class='playagaintext' onclick='gotoLesson()'>"+data.string.learnagain+"</p><img class='clouds' ></div></div>");
		$('.monkeydialogue').hide(0);
		$('.btnNavigationSong').css({'left':'-50%','opacity':'0'}).delay(2900).animate({'left':'0%','opacity':'1'},1000);
		$('.clouds').attr('src',preload.getResult('clouds').src);
		setTimeout(function(){
			$('.playagaintext').addClass('colorchange');
		},3500);
		setTimeout(function(){
			$('.monkeydialogue').fadeIn(500);
		},2900);
			function playExAgain() {
					$('.active').find('.footerClick').trigger('click');
			}

			function gotoLesson() {
					$(".btn-default").not(".active").find('.footerClick').trigger('click');
			}
			$('.banana').css('animation','none').attr('src',preload.getResult('four_banana').src);
			$('.textblock,.uppertextblock,.image,.glyphicon').not('.top-text').css('display','none');
			$('.two').addClass('front').attr('src',imgpath + 'monkeydancing.png').addClass('positioned');
			$('.main').attr('src',preload.getResult('mainbg2').src);
			setTimeout(function(){
				$('.two').addClass('gotback');
			},1800);
			setTimeout(function(){
				$('.two').attr('src',imgpath +'monkeydancing.gif');
				$('#corr').css("display","none");
				$('#incorr').css("display","none");
			},2800);
		}

		$('.banana').addClass('zoominout');

		$(".two").css('opacity','0');
		$(".imageblock").append('&nbsp;&nbsp; <span class="speaker_icon glyphicon glyphicon-volume-up"></span>');
		if(countNext==0){
			$nextBtn.show(0);
			// sound_player('sound_4');
			$('.speaker_icon').css('display','none');
		}
		if(countNext>0){
			var id = 'a'+countNext;
			sound_player(id);
			$('.glyphicon').css({'cursor':'pointer','z-index':'999999'});
			$('.image,.glyphicon').click(function(){
				sound_player(id);
			});
		}
		$('.optionblock').click(function(){
			if($(this).hasClass('correct'))
			{
				var $this = $(this);
				var position = $this.position();
				$(".one").addClass('blurout');
				$(".two").addClass('blurin');
				$('.optionblock').css('pointer-events','none');
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img id="corr" style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-46%,0%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$(this).css({"background":"rgb(190,214,47)","color":"white","border":"1vmin solid #DEEF3C"});
					play_correct_incorrect_sound(1);
				setTimeout(function(){
					sound_player('sound_3');
				},400);


				if( countNext>0 && countNext == $total_page-1){
				 $prevBtn.show(0);
				 setTimeout(function(){
					 sound_player('sound_3');
				 },2800);
				 setTimeout(function(){
					 sound_player('sound_3');
				 },3200);
				 setTimeout(function(){
					 sound_player('sound_3');
				 },3600);
				 setTimeout(function(){
					 sound_player('sound_3');
				 },4000);
				 $('.banana').css('animation','none').attr('src',preload.getResult('four_banana').src);
				 $('.textblock,.uppertextblock,.image,.glyphicon').not('.top-text').css('display','none');
				 $('.two').addClass('front').attr('src',imgpath + 'monkeydancing.png').addClass('positioned');
				 $('.main').attr('src',preload.getResult('mainbg2').src);
				 setTimeout(function(){
					 $('.two').addClass('gotback');
				 },1800);
				 setTimeout(function(){
					 $('.two').attr('src',imgpath +'monkeydancing.gif');
					 $('#corr').css("display","none");
					 $('#incorr').css("display","none");
				 },2800);

			 }
				nav_button_controls(100);
			}
			else {
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img id="incorr" style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-46%,0%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
							play_correct_incorrect_sound(0);
				$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border":"1vmin solid #980000"});
			}
		});
	}

	function preload_option_img(preload_image_id){
		$('.'+preload_image_id).attr('src',preload.getResult(preload_image_id).src);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			}
		else {
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
	}

	function put_image(content, count){
			if(content[count].hasOwnProperty('imageblock')){
				for(var j = 0; j < content[count].imageblock.length; j++){
					var imageblock = content[count].imageblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
		}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}


	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[

	// slide0
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead',
      textdata:data.string.p2s1
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgbutterfly',
          imgclass:'bgparrotclass bigowl zoomout'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead'
        },
        {
          imgid:'butterfly-gif',
          imgclass:'butterflygifclass smallone'
        }
    ]
    }]
  },
  // slide1
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead short',
      textdata:data.string.p2s2
    },
    {
      textclass:'instructionhead ins',
      textdata:data.string.p2s3
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgmain',
          imgclass:'bgparrotclass zoombutterfly'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead short'
        },
        {
          imgid:'handImg',
          imgclass:'handclass butterflyclass'
        },
        {
          imgid:'butterfly-gif',
          imgclass:'butterflygifclass'
        }
    ]
    }]
  },
  // slide2
  // {
  //   //contentblockadditionalclass:'forestbg zoomparrot',
  //   extratextblock:[
  //   {
  //     textclass:'instructionhead',
  //     textdata:data.string.p2s1
  //   }
  //   ],
  //   imageblock:[{
  //     imagestoshow:[
  //       {
  //         imgid:'bgmain',
  //         imgclass:'bgparrotclass bigbutterfly zoomout'
  //       },
  //       {
  //         imgid:'asha4',
  //         imgclass:'ashahead'
  //       }
  //   ]
  //   }]
  // },
  // slide3
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead short',
      textdata:data.string.p2s4
    },
    {
      textclass:'instructionhead ins',
      textdata:data.string.p2s3
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgmain',
          imgclass:'bgparrotclass zoomdragonfly'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead short'
        },
        {
          imgid:'handImg',
          imgclass:'handclass dragonflyclass'
        },
        {
          imgid:'dragonfly-gif',
          imgclass:'dragonflygifclass'
        }
    ]
    }]
  },
  // slide4
  // {
  //   //contentblockadditionalclass:'forestbg zoomparrot',
  //   extratextblock:[
  //   {
  //     textclass:'instructionhead',
  //     textdata:data.string.p2s1
  //   }
  //   ],
  //   imageblock:[{
  //     imagestoshow:[
  //       {
  //         imgid:'bgmain',
  //         imgclass:'bgparrotclass bigdragonfly zoomout'
  //       },
  //       {
  //         imgid:'asha4',
  //         imgclass:'ashahead'
  //       }
  //   ]
  //   }]
  // },
  // slide5
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead short',
      textdata:data.string.p2s5
    },
    {
      textclass:'instructionhead ins',
      textdata:data.string.p2s3
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgmain',
          imgclass:'bgparrotclass zoomladybug'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead short'
        },
        {
          imgid:'handImg',
          imgclass:'handclass ladybugclass'
        },
        {
          imgid:'ladybug-gif',
          imgclass:'ladybuggifclass'
        }
    ]
    }]
  },
  // slide6
  // {
  //   //contentblockadditionalclass:'forestbg zoomparrot',
  //   extratextblock:[
  //   {
  //     textclass:'instructionhead',
  //     textdata:data.string.p2s1
  //   }
  //   ],
  //   imageblock:[{
  //     imagestoshow:[
  //       {
  //         imgid:'bgmain',
  //         imgclass:'bgparrotclass bigladybug zoomout'
  //       },
  //       {
  //         imgid:'asha4',
  //         imgclass:'ashahead'
  //       }
  //   ]
  //   }]
  // },
  //slide7
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead short',
      textdata:data.string.p2s6
    },
    {
      textclass:'instructionhead ins',
      textdata:data.string.p2s3
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgmain',
          imgclass:'bgparrotclass zoombee'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead short'
        },
        {
          imgid:'handImg',
          imgclass:'handclass beeclass'
        },
        {
          imgid:'bee-gif',
          imgclass:'beegifclass'
        }
    ]
    }]
  },
  //slide8
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionlast',
      textdata:data.string.p2s7
    }],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bg3',
          imgclass:'bgparrotclass opacity'
        },
        {
          imgid:'img1',
          imgclass:'lastimage image1'
        },
        {
          imgid:'img2',
          imgclass:'lastimage image2'
        },
        {
          imgid:'img3',
          imgclass:'lastimage image3'
        },
        {
          imgid:'img4',
          imgclass:'lastimage image4'
        },
        {
          imgid:'img5',
          imgclass:'lastimage1 image5'
        },
        {
          imgid:'img6',
          imgclass:'lastimage1 image6'
        },
        {
          imgid:'img7',
          imgclass:'lastimage1 image7'
        },
        {
          imgid:'img8',
          imgclass:'lastimage1 image8'
        }
    ]
    }]
  },
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	$('.mainBox > div').prepend('<p class="replay_button"></p>');

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
      {id: "handImg", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "asha4", src: imgpath+"asha04.png", type: createjs.AbstractLoader.IMAGE},
    	{id: "butterfly-gif", src: imgpath+"butterfly.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "dragonfly-gif", src: imgpath+"dragonfly.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "ladybug-gif", src: imgpath+"ladybug.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "bee-gif", src: imgpath+"bee.gif", type: createjs.AbstractLoader.IMAGE},
  //  {id: "bgparrot", src: imgpath+"bg03_new01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bgbutterfly", src: imgpath+"bg_butterfly.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bgdragonfly", src: imgpath+"bg_dragonfly.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bgladybug", src: imgpath+"bg_ladybug.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bgbee", src: imgpath+"bg_honeybee.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bgmain", src: imgpath+"bg_main.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "dragonfly", src: imgpath+"dragonfly.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ladybug", src: imgpath+"ladybug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "img1", src: imgpath+"owl.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "img2", src: imgpath+"dragonfly.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "img3", src: imgpath+"hen.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "img4", src: imgpath+"duck.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "img5", src: imgpath+"ladybug.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "img6", src: imgpath+"butterfly.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "img7", src: imgpath+"bee.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "img8", src: imgpath+"parrot_wings.gif", type: createjs.AbstractLoader.IMAGE},
      //sounds
				{id: "sound_0", src: soundAsset+"s2_p1_Insects have wings too.ogg"},
				{id: "sound_1", src: soundAsset+"s2_p2.ogg"},
				{id: "sound_2", src: soundAsset+"s2_p4.ogg"},
				{id: "sound_3", src: soundAsset+"s2_p6.ogg"},
				{id: "sound_4", src: soundAsset+"s2_p8.ogg"},
				{id: "sound_5", src: soundAsset+"s2_p9.ogg"},
      	{id: "sound_6", src: soundAsset+"s1_p8_2_These are its wings.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

/*	var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +6.5+"%";
	$('.top_below_text').css({
		'top':top_val
		});
*/
		switch(countNext) {
			case 0:
      sound_player("sound_0");
      break;
			case 1:
      sound_player1("sound_1");
      onclick_changer('butterflygifclass','bgbutterfly');
      break;
      // case 2:
      // nav_button_controls(1200);
      // break;
      case 2:
      sound_player1("sound_2");
      onclick_changer('dragonflygifclass','bgdragonfly');
      break;
      // case 4:
      // nav_button_controls(1200);
      // break;
      case 3:
      sound_player1("sound_3");
      onclick_changer('ladybuggifclass','bgladybug');
      break;
      // case 6:
      // nav_button_controls(1200);
      // break;
      case 4:
      sound_player1("sound_4");
      onclick_changer('beegifclass','bgbee');
      break;
      case 5:
      sound_player("sound_5");
      break;
      default:
      nav_button_controls(200);
		}

    function onclick_changer(imageclass, imageid){
      $('.' + imageclass).hide(0);
      $('.instructionhead.ins').hide(0);
      $('.handclass').click(function(){
        $('.bgparrotclass').attr('src',preload.getResult(imageid).src);
        $('.'+imageclass).show(0);
        $('.handclass').hide(0);
        $('.instructionhead.ins').show(0);
        sound_player("sound_6");
      });
    }

	/*	function onclick_gif_changer(imgid){
			$('.click_gif_change,.handicon').click(function(){
				$('.animal_image').attr('src',preload.getResult(imgid).src);
				$(this).css('pointer-events','none');
				$('.top_below_text,.handicon').hide(0);
				nav_button_controls(100);
			});
		}
*/
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

  function sound_player(sound_id){
    setTimeout(function(){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on('complete', function(){
        nav_button_controls(0);
      });
    },2000);

	}
  function sound_player1(sound_id){
    setTimeout(function(){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
      },2000);
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[

	// slide0
	{
		imageblock:[{
			imagestoshow:[
				{
					imgid:'bg03',
					imgclass:'bgparrotclass'
				},
				{
          imgid:'owl-gif',
          imgclass:'image1'
        },
        {
          imgid:'parrotwings-gif',
          imgclass:'image2'
        },
        {
          imgid:'hen-gif',
          imgclass:'image3'
        },
        {
          imgid:'duck-gif',
          imgclass:'image4'
        },
        {
          imgid:'bee-gif',
          imgclass:'image5'
        },
        {
          imgid:'butterfly-gif',
          imgclass:'image6'
        },
        {
          imgid:'dragonfly-gif',
          imgclass:'image7'
        },
        {
          imgid:'ladybug-gif',
          imgclass:'image8'
        }

		]
	}],
		contentblockadditionalclass:'coverbg',
    extratextblock:[{
			textclass:'chapter_title',
			textdata:data.lesson.chapter
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'treebg',
    speechbox:[{
  		speechbox: 'sp-1',
  		textclass: "answer",
  		textdata: data.string.p1s1,
  		imgclass: '',
  		imgid : 'textbox',
  		imgsrc: '',
  	}],
		imageblock:[{
			imagestoshow:[{
				imgid:'birdsit',
				imgclass:'birdtree'
			},
      {
				imgid:'asha1',
				imgclass:'ashaclass point'
			}]
		}]
	},

	// slide2
  {
    contentblockadditionalclass:'treebg',
    speechbox:[{
      speechbox: 'sp-1',
      textclass: "answer",
      textdata: data.string.p1s3,
      imgclass: '',
      imgid : 'textbox',
      imgsrc: '',
    }],
    extratextblock:[
    {
      textclass:'instruction',
      textdata:data.string.p1s2
    },
		{
			textclass:'wings',
			textdata:data.string.wings
		}],
    imageblock:[{
      imagestoshow:[{
        imgid:'birdsit',
        imgclass:'birdtree'
      },
      {
        imgid:'asha3',
        imgclass:'ashaclass point'
      },
			{
				imgid:'handImg',
				imgclass:'handclass firstbird'
			},
      {
        imgid:'birdfly',
        imgclass:'birdtree fly'
      },
      {
        imgid:'arrow',
        imgclass:'wing_arrow1'
      },
      {
        imgid:'arrow',
        imgclass:'wing_arrow2'
      }
    ]
    }]
  },

	// slide3
  {
    contentblockadditionalclass:'treebg',
    speechbox:[{
      speechbox: 'sp-1',
      textclass: "answer",
      textdata: data.string.p1s4,
      imgclass: '',
      imgid : 'textbox',
      imgsrc: '',
    }],
    imageblock:[{
      imagestoshow:[
      {
        imgid:'asha2',
        imgclass:'ashaclass'
      },
      {
        imgid:'birdfly',
        imgclass:'birdtree fly'
      }
    ]
    }]
  },

	// slide4
  {
    contentblockadditionalclass:'treebg',
    imageblock:[{
      imagestoshow:[
      {
        imgid:'asha2',
        imgclass:'ashaclass'
      },
      {
        imgid:'birdfly-gif',
        imgclass:'birdflyclass'
      }
    ]
    }]
  },
	// slide5
  {
    contentblockadditionalclass:'treebg',
    speechbox:[{
      speechbox: 'sp-1',
      textclass: "answer",
      textdata: data.string.p1s5,
      imgclass: '',
      imgid : 'textbox',
      imgsrc: '',
    }],
    imageblock:[{
      imagestoshow:[
      {
        imgid:'asha3',
        imgclass:'ashaclass point'
      }
    ]
    }]
  },
	// slide6
  {
  //  contentblockadditionalclass:'forestbg',
    extratextblock:[
    {
      textclass:'instructionhead',
      textdata:data.string.p1s6
    }],
    imageblock:[{
      imagestoshow:[
      {
        imgid:'bgparrot',
        imgclass:'bgparrotclass'
      },
      {
        imgid:'asha4',
        imgclass:'ashahead'
      },
      {
        imgid:'parrotfly-gif',
        imgclass:'parrotclass'
      }
    ]
    }]
  },

	// slide7
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead short',
      textdata:data.string.p1s7
    },
    {
      textclass:'instructionhead ins',
      textdata:data.string.p1s8
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgmain',
          imgclass:'bgparrotclass zoomparrot'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead short'
        },
        {
          imgid:'handImg',
          imgclass:'handclass'
        },
        {
          imgid:'parrotwings-gif',
          imgclass:'parrotgifclass'
        }
    ]
    }]
  },
  // slide8 'zoomout only slide'
  // {
  //   //contentblockadditionalclass:'forestbg zoomparrot',
  //   extratextblock:[
  //   {
  //     textclass:'instructionhead',
  //     textdata:data.string.p1s6
  //   }
  //   ],
  //   imageblock:[{
  //     imagestoshow:[
  //       {
  //         imgid:'bgmain',
  //         imgclass:'bgparrotclass bigparrot zoomout'
  //       },
  //       {
  //         imgid:'asha4',
  //         imgclass:'ashahead'
  //       }
  //   ]
  //   }]
  // },
  // slide9
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead short',
      textdata:data.string.p1s9
    },
    {
      textclass:'instructionhead ins',
      textdata:data.string.p1s8
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgmain',
          imgclass:'bgparrotclass zoomhen'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead short'
        },
        {
          imgid:'handImg',
          imgclass:'handclass henclass'
        },
        {
          imgid:'hen-gif',
          imgclass:'hengifclass'
        }
    ]
    }]
  },
  // slide10
  // {
  //   //contentblockadditionalclass:'forestbg zoomparrot',
  //   extratextblock:[
  //   {
  //     textclass:'instructionhead',
  //     textdata:data.string.p1s6
  //   }
  //   ],
  //   imageblock:[{
  //     imagestoshow:[
  //       {
  //         imgid:'bgmain',
  //         imgclass:'bgparrotclass bighen zoomout'
  //       },
  //       {
  //         imgid:'asha4',
  //         imgclass:'ashahead'
  //       }
  //   ]
  //   }]
  // },
  // slide11
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead short',
      textdata:data.string.p1s10
    },
    {
      textclass:'instructionhead ins',
      textdata:data.string.p1s8
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgmain',
          imgclass:'bgparrotclass zoomduck'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead short'
        },
        {
          imgid:'handImg',
          imgclass:'handclass duckclass'
        },
        {
          imgid:'duck-gif',
          imgclass:'duckgifclass'
        }
    ]
    }]
  },
  // slide12
  // {
  //   //contentblockadditionalclass:'forestbg zoomparrot',
  //   extratextblock:[
  //   {
  //     textclass:'instructionhead',
  //     textdata:data.string.p1s6
  //   }
  //   ],
  //   imageblock:[{
  //     imagestoshow:[
  //       {
  //         imgid:'bgmain',
  //         imgclass:'bgparrotclass bigduck zoomout'
  //       },
  //       {
  //         imgid:'asha4',
  //         imgclass:'ashahead'
  //       }
  //   ]
  //   }]
  // },
  // slide13
  {
    //contentblockadditionalclass:'forestbg zoomparrot',
    extratextblock:[
    {
      textclass:'instructionhead short',
      textdata:data.string.p1s11
    },
    {
      textclass:'instructionhead ins',
      textdata:data.string.p1s8
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgid:'bgmain',
          imgclass:'bgparrotclass zoomowl'
        },
        {
          imgid:'asha4',
          imgclass:'ashahead short'
        },
        {
          imgid:'handImg',
          imgclass:'handclass owlclass'
        },
        {
          imgid:'owl-gif',
          imgclass:'owlgifclass'
        }
    ]
    }]
  }

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	$('.mainBox > div').prepend('<p class="replay_button"></p>');

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
      {id: "handImg", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "asha1", src: imgpath+"ashapoint.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha2", src: imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha3", src: imgpath+"asha03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha4", src: imgpath+"asha04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
      {id: "owl", src: imgpath+"owl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
    	{id: "hen-gif", src: imgpath+"hen.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "duck-gif", src: imgpath+"duck.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "owl-gif", src: imgpath+"owl.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "butterfly-gif", src: imgpath+"butterfly.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dragonfly-gif", src: imgpath+"dragonfly.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "ladybug-gif", src: imgpath+"ladybug.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bee-gif", src: imgpath+"bee.gif", type: createjs.AbstractLoader.IMAGE},
    //  {id: "bgparrot", src: imgpath+"bg03_new01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg03", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},

      {id: "bgparrot", src: imgpath+"bg_parrot.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bghen", src: imgpath+"bg_hen.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bgduck", src: imgpath+"bg_duck.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bgowl", src: imgpath+"bg_owl.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bgmain", src: imgpath+"bg_main.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "dragonfly", src: imgpath+"dragonfly.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ladybug", src: imgpath+"ladybug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "birdfly", src: imgpath+"bird_fly.png", type: createjs.AbstractLoader.IMAGE},
			{id: "birdfly-gif", src: imgpath+"bird_fly.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "birdfly1-gif", src: imgpath+"bird_fly01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "birdfly1", src: imgpath+"bird_fly01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "parrotfly", src: imgpath+"parrot.png", type: createjs.AbstractLoader.IMAGE},
      {id: "parrotfly-gif", src: imgpath+"parrot_fly.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "parrotwings-gif", src: imgpath+"parrot_wings.gif", type: createjs.AbstractLoader.IMAGE},

      {id: "beefly-gif", src: imgpath+"bee.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "beefly", src: imgpath+"bee.png", type: createjs.AbstractLoader.IMAGE},
      {id: "birdsit", src: imgpath+"bird_sitting.png", type: createjs.AbstractLoader.IMAGE},
			{id: "butterflyorange", src: imgpath+"butterfly_orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "butterflyorange-gif", src: imgpath+"butterfly_orange.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mouserunning", src: imgpath+"mouse_running.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			//sounds
				{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
				{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
				{id: "sound_2", src: soundAsset+"s1_p3_1.ogg"},
				{id: "sound_3", src: soundAsset+"s1_p3_2.ogg"},
				{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
				{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
				{id: "sound_6", src: soundAsset+"s1_p7_let us loo at other animals with wings.ogg"},
				{id: "sound_7", src: soundAsset+"s1_p8_1.ogg"},
				{id: "sound_8", src: soundAsset+"s1_p8_2_These are its wings.ogg"},
				{id: "sound_9", src: soundAsset+"s1_p10.ogg"},
				{id: "sound_10", src: soundAsset+"s1_p12.ogg"},
				{id: "sound_11", src: soundAsset+"s1_p14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

/*	var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +6.5+"%";
	$('.top_below_text').css({
		'top':top_val
		});
*/
		switch(countNext) {
			case 0:
			sound_player("sound_0");
			break;
			case 1:
			sound_player("sound_1");
			break;
			case 2:
			$('.wing_arrow1,.wing_arrow2,.wings').hide(0);
			sound_player1("sound_2");
        $('.birdtree.fly').hide(0);
        $('.speechbox.sp-1').hide(0);
        $(".birdtree,.handclass").click(function(){
					$('.wing_arrow1,.wing_arrow2,.wings').fadeIn(500);
          $('.birdtree').hide(0);
					$('.handclass').hide(0);
          $('.birdtree.fly').show(0);
          $('.instruction').hide(0);
          $('.speechbox.sp-1').show(0);
          sound_player("sound_3");
        });
      break;
			case 3:
      sound_player("sound_4");
      break;
			case 4:
			nav_button_controls(2000);
			break;
			case 5:
      sound_player("sound_5");
			break;
			case 6:
			setTimeout(function(){
					sound_player1("sound_6");
			},2000);

      setTimeout(function () {
        $('.parrotclass').hide();
        $('.bgparrotclass').attr('src',preload.getResult('bgmain').src);
        nav_button_controls(1000);
      }, 6050);
			break;
			case 7:
			setTimeout(function(){
					sound_player1("sound_7");
			},2000);
      onclick_changer('parrotgifclass', 'bgparrot','sound_8');
			break;
      // case 8:
      // nav_button_controls(1200);
		// 	break;
      case 8:
			setTimeout(function(){
					sound_player1("sound_9");
			},2000);
      onclick_changer('hengifclass', 'bghen',"sound_8");
      break;
      // case 10:
      // nav_button_controls(1200);
      // break;
      case 9:
			setTimeout(function(){
					sound_player1("sound_10");
			},2000);
      onclick_changer('duckgifclass', 'bgduck','sound_8');
      break;
      // case 12:
      // nav_button_controls(1200);
      // break;
      case 10:
			setTimeout(function(){
					sound_player1("sound_11");
			},2000);
      onclick_changer('owlgifclass', 'bgowl','sound_8');
      break;
      case 11:
      nav_button_controls(200);
      break;
      default:

		}
    function onclick_changer(imageclass, imageid,sound_id){
      $('.' + imageclass).hide(0);
      $('.instructionhead.ins').hide(0);
      $('.handclass').click(function(){
        $('.bgparrotclass').attr('src',preload.getResult(imageid).src);
        $('.'+imageclass).show(0);
        $('.handclass').hide(0);
        $('.instructionhead.ins').show(0);
				setTimeout(function(){
					sound_player(sound_id);
				},2000);
      });
    }

	/*	function onclick_gif_changer(imgid){
			$('.click_gif_change,.handicon').click(function(){
				$('.animal_image').attr('src',preload.getResult(imgid).src);
				$(this).css('pointer-events','none');
				$('.top_below_text,.handicon').hide(0);
				nav_button_controls(100);
			});
		}
*/
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

  function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_player1(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

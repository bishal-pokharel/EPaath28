var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide1
	{
		extratextblock:[
		{
			textclass: "playtime",
			textdata: data.string.playtime
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'cover',
					imgsrc: ""
				},
				{
					imgclass: "rhino",
					imgid : 'rhino',
					imgsrc: ""
				},
				{
					imgclass: "squirrel",
					imgid : 'squirrel',
					imgsrc: ""
				}
			]
		}]
	},
	//slide2
	{
		extratextblock:[
		{
			textclass: "topQn-redbg",
			textdata: data.string.exc_q1
		}],
		imageblock:[{
			imgsincont:true,
			imgscontainerdivclass:"leftImgCont blbg",
			imagestoshow:[
				{
					imgclass: "lftImg",
					imgid : 'sunny_day',
					imgsrc: ""
				},{
					imgclass: "sun1 snpng",
					imgid : 'sun',
					imgsrc: ""
				},{
					imgclass: "sun1 sngif",
					imgid : 'sun-dance',
					imgsrc: ""
				}
			]
		}],
		exetype1:[{
			optionsdivclass:"optionsdivtxt",
			exeoptions:[{
				optaddclass:"class1",
				optdata:data.string.exc_op1
			},{
				optdata:data.string.exc_op2
			},{
				optdata:data.string.exc_op3
			},{
				optdata:data.string.exc_op4
			}]
		}]
	},
	//slide3
	{
		extratextblock:[
		{
			textclass: "topQn-redbg",
			textdata: data.string.exc_q1
		}],
		imageblock:[{
			imgsincont:true,
			imgscontainerdivclass:"leftImgCont",
			imagestoshow:[
				{
					imgclass: "lftImg",
					imgid : 'cloudy_day',
					imgsrc: ""
				},{
					imgclass: "clouds cld1",
					imgid : 'cloud01',
					imgsrc: ""
				},{
					imgclass: "clouds cld2",
					imgid : 'cloud01',
					imgsrc: ""
				}
			]
		}],
		exetype1:[{
			optionsdivclass:"optionsdivtxt",
			exeoptions:[{
				optdata:data.string.exc_op1
			},{
				optdata:data.string.exc_op2
			},{
				optaddclass:"class1",
				optdata:data.string.exc_op3
			},{
				optdata:data.string.exc_op4
			}]
		}]
	},
	//slide4
	{
		extratextblock:[
		{
			textclass: "topQn-redbg",
			textdata: data.string.exc_q1
		}],
		imageblock:[{
			imgsincont:true,
			imgscontainerdivclass:"leftImgCont",
			imagestoshow:[
				{
					imgclass: "lftImg",
					imgid : 'rainy_day',
					imgsrc: ""
				},{
					imgclass: "rng rng1",
					imgid : 'raining',
					imgsrc: ""
				},{
					imgclass: "rng rng2",
					imgid : 'rainingGif',
					imgsrc: ""
				}
			]
		}],
		exetype1:[{
			optionsdivclass:"optionsdivtxt",
			exeoptions:[{
				optdata:data.string.exc_op1
			},{
				optdata:data.string.exc_op2
			},{
				optdata:data.string.exc_op3
			},{
				optaddclass:"class1",
				optdata:data.string.exc_op4
			}]
		}]
	},
	//slide5
	{
		extratextblock:[
		{
			textclass: "topQn-redbg",
			textdata: data.string.exc_q1
		}],
		imageblock:[{
			imgsincont:true,
			imgscontainerdivclass:"leftImgCont",
			imagestoshow:[
				{
					imgclass: "lftImg",
					imgid : 'windy_day',
					imgsrc: ""
				},{
					imgclass: "windBlow",
					imgid : 'wind_blow',
					imgsrc: ""
				}
			]
		}],
		exetype1:[{
			optionsdivclass:"optionsdivtxt",
			exeoptions:[{
				optdata:data.string.exc_op1
			},{
				optaddclass:"class1",
				optdata:data.string.exc_op2
			},{
				optdata:data.string.exc_op3
			},{
				optdata:data.string.exc_op4
			}]
		}]
	},
	//slide6
	{
		extratextblock:[
		{
			textclass: "topQn-grnbg",
			textdata: data.string.exc_q2
		}],
		imageblock:[{
			imgsincont:true,
			imgscontainerdivclass:"leftImgCont",
			imagestoshow:[
				{
					imgindiv:true,
					imgdivclass:"frImgDiv",
					imgclass: "imgoptn class1 sny",
					imgid : 'sunny_day_original',
					imgsrc: ""
				},{
					imgindiv:true,
					imgdivclass:"frImgDiv",
					imgclass: "imgoptn cldy",
					imgid : 'cloudy_day_original',
					imgsrc: ""
				},{
					imgindiv:true,
					imgdivclass:"frImgDiv",
					imgclass: "imgoptn wndy",
					imgid : 'windy_day_original',
					imgsrc: ""
				},{
					imgindiv:true,
					imgdivclass:"frImgDiv",
					imgclass: "imgoptn rny",
					imgid : 'rainy_day_original',
					imgsrc: ""
				}
			]
		},{
			imgsincont:true,
			imgscontainerdivclass:"rightImgCont",
			imagestoshow:[{
				imgclass: "rghtImg",
				imgid : 't_shirt_pant',
				imgsrc: ""
			}]
		}]
	},
	//slide7
	{
		extratextblock:[
		{
			textclass: "topQn-grnbg",
			textdata: data.string.exc_q2
		}],
		imageblock:[{
			imgsincont:true,
			imgscontainerdivclass:"leftImgCont",
			imagestoshow:[
				{
					imgindiv:true,
					imgdivclass:"frImgDiv",
					imgclass: "imgoptn sny",
					imgid : 'sunny_day_original',
					imgsrc: ""
				},{
					imgindiv:true,
					imgdivclass:"frImgDiv",
					imgclass: "imgoptn wndy",
					imgid : 'windy_day_original',
					imgsrc: ""
				},{
					imgindiv:true,
					imgdivclass:"frImgDiv",
					imgclass: "imgoptn cldy",
					imgid : 'cloudy_day_original',
					imgsrc: ""
				},{
					imgindiv:true,
					imgdivclass:"frImgDiv",
					imgclass: "imgoptn rny class1",
					imgid : 'rainy_day_original',
					imgsrc: ""
				}
			]
		},{
			imgsincont:true,
			imgscontainerdivclass:"rightImgCont",
			imagestoshow:[{
				imgclass: "rghtImg",
				imgid : 'rain_coat_boot',
				imgsrc: ""
			}]
		}]
	},
	// //slide8
	// {
	// 	extratextblock:[
	// 	{
	// 		textclass: "topQn-grnbg",
	// 		textdata: data.string.exc_q2
	// 	}],
	// 	imageblock:[{
	// 		imgsincont:true,
	// 		imgscontainerdivclass:"leftImgCont",
	// 		imagestoshow:[
	// 			{
	// 				imgindiv:true,
	// 				imgdivclass:"frImgDiv",
	// 				imgclass: "imgoptn sny",
	// 				imgid : 'sunny_day',
	// 				imgsrc: ""
	// 			},{
	// 				imgindiv:true,
	// 				imgdivclass:"frImgDiv",
	// 				imgclass: "imgoptn class1 wndy",
	// 				imgid : 'windy_day',
	// 				imgsrc: ""
	// 			},{
	// 				imgindiv:true,
	// 				imgdivclass:"frImgDiv",
	// 				imgclass: "imgoptn cldy",
	// 				imgid : 'cloudy_day',
	// 				imgsrc: ""
	// 			},{
	// 				imgindiv:true,
	// 				imgdivclass:"frImgDiv",
	// 				imgclass: "imgoptn rny",
	// 				imgid : 'rainy_day',
	// 				imgsrc: ""
	// 			}
	// 		]
	// 	},{
	// 		imgsincont:true,
	// 		imgscontainerdivclass:"rightImgCont",
	// 		imagestoshow:[{
	// 			imgclass: "rghtImg",
	// 			imgid : 'woolen_jacket_pant',
	// 			imgsrc: ""
	// 		}]
	// 	}]
	// },
	// slide 9
	{
		sliderload:true,
		contentblockadditionalclass:'green',
		slider:[{
			imagestoshow:[{
				imgclass : "slideimg si1",
				imgsrc : '',
				imgid : 'sunny_day_original',
				textclass:"weatherNam",
				textdata:data.string.sunny
			},{
				imgclass : "slideimg si2",
				imgsrc : '',
				imgid : 'windy_day_original',
				textclass:"weatherNam",
				textdata:data.string.windy
			},{
				imgclass : "slideimg si3",
				imgsrc : '',
				imgid : 'cloudy_day_original',
				textclass:"weatherNam",
				textdata:data.string.cloudy
			},{
				imgclass : "slideimg si4",
				imgsrc : '',
				imgid : 'rainy_day_original',
				textclass:"weatherNam",
				textdata:data.string.rainy
			}]
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var video_object;


	var endpageex =  new EndPageofExercise();
	var message =  "Yay! we just learned making some lines and curves.";

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//
			//images
			{id: "cover", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sun-dance", src: imgpath+"sun-dance.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/right.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloudy_day", src: imgpath+"cloudy_day.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rainy_day", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunny_day", src: imgpath+"bg03a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "windy_day", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cloudy_day_original", src: imgpath+"cloudy_day.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rainy_day_original", src: imgpath+"rainy_day.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunny_day_original", src: imgpath+"sunny_day.png", type: createjs.AbstractLoader.IMAGE},
			{id: "windy_day_original", src: imgpath+"windy_day.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud01", src: imgpath+"../../images/weather/cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "raining", src: imgpath+"../../images/weather/raining.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rain_drop01", src: imgpath+"../../images/weather/rain_drop01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun-dance.png", type: createjs.AbstractLoader.IMAGE},

			{id: "rain_coat_boot", src: imgpath+"rain_coat_boot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "t_shirt_pant", src: imgpath+"t_shirt_pant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "woolen_jacket_pant", src: imgpath+"woolen_jacket_pant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "raining", src: imgpath+"raining.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rainingGif", src: imgpath+"raining.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "wind_blow", src: imgpath+"wind_blow.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			//video_src
			// {id: "tutorial", src: imgpath+"tutorial.ogg", type: createjs.AbstractLoader.VIDEO},
			// sounds
			// {id: "sound_1", src: soundAsset+"playtime.ogg"},
			{id: "exe_ins_1", src: soundAsset+"exe_ins_1.ogg"},
			{id: "exe_ins_2", src: soundAsset+"exe_ins_2.ogg"},

			// {id: "s1", src: soundAsset+"1.ogg"},
			// {id: "s2", src: soundAsset+"2.ogg"},
			// {id: "s3", src: soundAsset+"3.ogg"},
			// {id: "s4", src: soundAsset+"4.ogg"},
			// {id: "s5", src: soundAsset+"5.ogg"},
			// {id: "s6", src: soundAsset+"../6.ogg"},
			// {id: "s7", src: soundAsset+"../7.ogg"},
			// {id: "s8", src: soundAsset+"../8.ogg"},
			// {id: "s9", src: soundAsset+"../9.ogg"},
			// {id: "s10", src: soundAsset+"../10.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
		if(event.item.id === "tutorial"){
			video_object = event.item;
		}
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image3(content, countNext);
		put_image2(content, countNext);
		content[countNext].sliderload?put_image3(content, countNext):"";
		put_speechbox_image(content, countNext);

		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
							// 	alert("here");
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		switch(countNext){
			case 0:
				nav_button_controls(2000);
			break;
			case 1:
				sound_player("exe_ins_1");
				$prevBtn.hide(0);
				$nextBtn.hide(0);
			break;
			case 5:
				sound_player("exe_ins_2");
			break;
			case 7:
				sound_player("rewards_page");
				$(".next-button").on("click", function(){
				$(".next-button, .prev-button ").css('pointer-events', 'none');
					var selectImg = $(".slider:eq(0)");
					$(".innerwrapper").append(selectImg.clone());
					selectImg.animate({
						width:"0%",
						padding:"0%"
					},1000, function(){
						selectImg.remove();
						$(".next-button, .prev-button").css('pointer-events', 'auto');
					});
				});

				$(".prev-button").on("click", function(){
					$(".next-button, .prev-button").css('pointer-events', 'none');
					var selectImg = $(".slider").last();
					$(".innerwrapper").prepend(selectImg.clone());
					selectImg.remove();
					var selectFirstImg = $(".slider:eq(0)");
					selectFirstImg.animate({
						width:"0%",
					},0,function(){
						selectFirstImg.animate({
							width:"20%"
						},1000);
						setTimeout(function(){
							$(".next-button, .prev-button").css('pointer-events', 'auto');
						},900);
					});
				});
        endpageex.endpage(" ");
			break;
				default:
				// nav_button_controls(1000);
		}
		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).addClass("corClass");
				$(this).siblings(".corectopt").show();
				$(".buttonsel").css("pointer-events","none");
				nav_button_controls(100);
				switch (countNext) {
					case 1:
						$(".snpng").hide(0);
						$(".sngif").show(0);
					break;
					case 2:
						$(".cld1").addClass("cld_1_anim");
						$(".cld2").addClass("cld_2_anim");
					break;
					case 3:
						$(".rng1").hide(0);
						$(".rng2").show(0);
					break;
					case 4:
						$(".windBlow").addClass("windBlowAnim");
					break;
					default:

				}
			}else{
				$(this).addClass("incorClass");
				$(this).siblings(".wrongopt").show();
				play_correct_incorrect_sound(0);
			}
		});


		$(".imgoptn").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).addClass("corClass");
				$(this).siblings(".cropt").show();
				$(".imgoptn").css("pointer-events","none");
				nav_button_controls(100);
			}else{
				$(this).addClass("incorClass");
				$(this).siblings(".wrngopt").show();
				play_correct_incorrect_sound(0);
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		// current_sound.on('complete', function(){
		// 	navigationcontroller();
		// });
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j=0;j<content[count].imageblock.length;j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
function put_image2(content, count){
		if(content[count].hasOwnProperty('optionsblock')){
			var imageClass = content[count].optionsblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}


			function put_image3(content, count){
				if(content[count].hasOwnProperty('slider')){
					var imageblock = content[count].slider[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
	// function put_image3(content, count){
	// 	if(content[count].hasOwnProperty('exerciseblock')){
	// 		var lncontent = content[count].exerciseblock[0];
	// 			if(lncontent.hasOwnProperty('imageoptions')){
	// 				var imageClass = lncontent.imageoptions;
	// 				for(var i=0; i<imageClass.length; i++){
	// 					var image_src = preload.getResult(imageClass[i].imgid).src;
	// 					//get list of classes
	// 					// console.log(image_src)
	// 					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
	// 					var selector = ('.'+classes_list[classes_list.length-1]);
	// 					// console.log(selector)
	// 					$(selector).attr('src', image_src);
	// 				}
	// 			}
	// 		}
	// }
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var selector = ('.'+speechbox[i].speechbox+' > .speechbg');
				console.log("imgsrc---"+image_src, $(selector));
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		loadTimelineProgress($total_page, countNext + 1);

		if(countNext == 0 || countNext == 1)
		navigationcontroller();

		generaltemplate();
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var maincontent = [
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg-crm",
        imageblock: [{
            imagestoshow: [
                {
                  imgnotindiv:true,
                  imgclass: "cvPage",
                  imgid: 'weather',
                  imgsrc: ""
                }
            ]
        }],
        extratextblock:[{
          textclass:"cvTxt",
          textdata:data.string.weather
        }]
    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg-prpl",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrel",
                    imgclass: "relativecls img1",
                    imgid: 'rbt_1',
                    imgsrc: ""
                },
                {
                    imgdiv: "sp-1-fntsz4",
                    imgclass: "spimg",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            txtcls:"sptxt",
                            imgtxt:data.string.p1s1txt
                        }
                    ]
                }
            ]
        }],
    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgGrn",
        extratextblock:[{
          textclass:"topInstrn",
          textdata:data.string.p1s2txt
        }],
        imageblock: [{
            imagestoshow: [
              {
                imgdiv: "weatherContainer-t5-l5 options sunny",
                imgclass: "imgInDiv ",
                imgid: 'sunny',
                imgsrc: "",
                textblock:[
                    {
                        txtcls:"wthrNm",
                        imgtxt:data.string.sunny
                    }
                ]
            },{
              imgdiv: "weatherContainer-t5-r5 options cloudy",
              imgclass: "imgInDiv cldImg",
              imgid: 'clouds',
              imgsrc: "",
              textblock:[
                  {
                      txtcls:"wthrNm",
                      imgtxt:data.string.cloudy
                  }
              ]
          },{
            imgdiv: "weatherContainer-b5-l5 options windy",
            imgclass: "imgInDiv wndImg",
            imgid: 'wind',
            imgsrc: "",
            textblock:[
                {
                    txtcls:"wthrNm",
                    imgtxt:data.string.windy
                }
            ]
        },{
          imgdiv: "weatherContainer-b5-r5 options rainy",
          imgclass: "imgInDiv rnImg",
          imgid: 'rain',
          imgsrc: "",
          textblock:[
              {
                  txtcls:"wthrNm",
                  imgtxt:data.string.rainy
              }
          ]
      }]
        }],
    },
];

var cntntSuny = [
  //slide 4
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg1 sny",
          imageblock: [{
              imgdivclass:"imgdv1",
              imagestoshow: [{
                  imgnotindiv:true,
                  imgclass: "background",
                  imgid: 'bg_sunny01',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "rabit",
                  imgid: 'rbt_1',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "wave waveAnim",
                  imgid: 'wave',
                  imgsrc: ""
              },{
                  imgdiv:"sp-2-fntsz4",
                  imgclass: "spimg",
                  imgid: 'dialImg',
                  imgsrc: "",
                  textblock:[{
                    txtcls:"sptxt",
                    imgtxt:data.string.sunytxt_1
                  }]
              }]
          }]
      },
  //slide 5
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg1 sny",
          pgtozoom:true,
          imageblock: [{
            imgdivclass:"imgdv1",
              imagestoshow: [{
                  imgnotindiv:true,
                  imgclass: "background",
                  imgid: 'bg_sunny01',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "rabit",
                  imgid: 'rabbit03',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "wave waveAnim",
                  imgid: 'wave',
                  imgsrc: ""
              },]
          },
          {
            imgdivclass:"imgdv2",
            imagestoshow:[{
                imgdiv:"sp-2-fntsz3hlf",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.sunytxt_2
                }]
            }]
          }]
      },
  //slide 6
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg1 sny",
          panpage:[{
            panclass:"panPage-1",
            imageblock: [{
                imagestoshow: [{
                    imgnotindiv:true,
                    imgclass: "background p1",
                    imgid: 'bg_sunny01',
                    imgsrc: ""
                },{
                    imgnotindiv:true,
                    imgclass: "wave waveAnim",
                    imgid: 'wave',
                    imgsrc: ""
                },{
                    imgnotindiv:true,
                    imgclass: "wave waveAnim",
                    imgid: 'wave',
                    imgsrc: ""
                }]
            }]
          },{
            panclass:"panPage-2",
            imageblock: [{
                imagestoshow: [{
                    imgnotindiv:true,
                    imgclass: "background p2",
                    imgid: 'bg_sunny02',
                    imgsrc: ""
                },{
                    imgnotindiv:true,
                    imgclass: "rabit",
                    imgid: 'rabbit03',
                    imgsrc: ""
                },{
                    imgnotindiv:true,
                    imgclass: "fBall",
                    imgid: 'ball',
                    imgsrc: ""
                },{
                    imgnotindiv:true,
                    imgclass: "boykickBall bkb_gif",
                    imgid: 'boy_kick_ball',
                    imgsrc: ""
                },{
                    imgnotindiv:true,
                    imgclass: "boykickBall bkb_png",
                    imgid: 'boy_kick_ball_png',
                    imgsrc: ""
                },{
                    imgdiv:"sp-2-fntsz3hlf",
                    imgclass: "spimg",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[{
                      txtcls:"sptxt",
                      imgtxt:data.string.p1s3txt
                    }]
                }]
            }]
          }]
      },
  //slide 7
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg1 sny",
          pgtozoom:true,
          imageblock: [{
            imgdivclass:"imgdv1",
              imagestoshow: [{
                  imgnotindiv:true,
                  imgclass: "background",//zoomAnim
                  imgid: 'bg_02',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "rabit inverted-l80",
                  imgid: 'rbt_2',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "wave-l60 waveAnimSec",
                  imgid: 'wave',
                  imgsrc: ""
              },]
          },{
            imgdivclass:"imgdv2",
            imagestoshow:[{
                imgdiv:"sp-2-fntsz3hlf l47",
                imgclass: "spimg inverted",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.p1s4txt
                }]
            }]
          }]
      },
  //slide 8
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg1 sny",
          imageblock: [{
              imagestoshow: [{
                  imgnotindiv:true,
                  imgclass: "background",
                  imgid: 'bg_sunny02a',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "fBall",
                  imgid: 'ball',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "boykickBall",
                  imgid: 'boy_kick_ball_png',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "rabit l2-b2",
                  imgid: 'rabbit03',
                  imgsrc: ""
              },{
                  imgdiv:"sp-2-fntsz3 spbig",
                  imgclass: "spimg",
                  imgid: 'dialImg',
                  imgsrc: "",
                  textblock:[{
                    txtcls:"sptxt",
                    imgtxt:data.string.p1s5txt
                  }]
              }]
          }]
      },
]

var contentWndy = [
  //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 wndy",
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background",
                imgid: 'bg_windy',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabit",
                imgid: 'rbt_1',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_1_1",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_2_3",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgdiv:"sp-2",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.wndy_txt_1
                }]
            }]
        }]
    },
  //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 wndy",
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background",
                imgid: 'bg_windy01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabit",
                imgid: 'rabbit03',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_1_1",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_2_2",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_2_3",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "clths clthAnim",
                imgid: 'clothes',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "treeGif",
                imgid: 'tree',
                imgsrc: ""
            },{
                imgdiv:"sp-2",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.wndy_txt_2
                }]
            }]
        }]
    },
  //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 wndy",
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background",
                imgid: 'bg_windy01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabit",
                imgid: 'rbt_1',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_1_1",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_2_3",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_2_4",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "leavesFly",
                imgid: 'leaf_fly',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "leavesFlySec",
                imgid: 'leaf_fly',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "leavesFlyThrd",
                imgid: 'leaf_fly',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "lvsOnGrnd",
                imgid: 'leaf_on_ground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "lvsOnGrnd lvsSec",
                imgid: 'leaf_on_ground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "lvsOnGrnd lvsthrd",
                imgid: 'leaf_on_ground01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "clths clthAnim",
                imgid: 'clothes',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "treeGif",
                imgid: 'tree',
                imgsrc: ""
            },{
                imgdiv:"sp-2-fntsz4",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.wndy_txt_3
                }]
            }]
        }]
    },
  //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 wndy",
        // panpage:[{
          // panclass:"panPage-1",
          pgtozoom:true,
          imageblock: [{
              imagestoshow: [{
                  imgnotindiv:true,
                  imgclass: "background",
                  imgid: 'bg_windy01',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "wind wnd_1_1",
                  imgid: 'wind01',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "wind wnd_2_3",
                  imgid: 'wind01',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "wind wnd_2_4",
                  imgid: 'wind01',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "clths clthAnim",
                  imgid: 'clothes',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "shirtGif shirtAnin",
                  imgid: 'shirt',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "tshirtGif t_shirtAnin",
                  imgid: 't_shirt',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "treeGif",
                  imgid: 'tree',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "rabit",
                  imgid: 'rabbit03',
                  imgsrc: ""
              }]
          },
          {
            imagestoshow:[{
                imgdiv:"sp-2 spbig-l17-t72",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.wndy_txt_4
                }]
            }]
          }]
        // }]
    },
  //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 wndy",
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background",
                imgid: 'bg_sunny02',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabit",
                imgid: 'rbt_1',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "dust dustAnim",
                imgid: 'dust',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_1_1 show wnd_1_1_anim",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_2_3 show wnd_2_3_anim",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "wind wnd_2_4 show wnd_2_4_anim",
                imgid: 'wind01',
                imgsrc: ""
            },{
                imgdiv:"sp-2",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.wndy_txt_5
                }]
            }]
        }]
    },
  ];

var contentCldy=[
    //slide 4
      {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 cldy",
        pgtozoom:true,
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background bgDrk",
                imgid: 'bg06a',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "background bgLight",
                imgid: 'bg_sunny01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabit",
                imgid: 'rbt_1',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "clouds cld_1 cld_1_Anim",
                imgid: 'cloud01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "clouds cld_3 cld_3_Anim",
                imgid: 'cloud01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "clouds cld_2 cld_2_Anim",
                imgid: 'cloud02',
                imgsrc: ""
            },{
                imgdiv:"sp-2-fntsz3hlf",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.cldy_txt_1
                }]
            }]
        }]
      },
    //slide 5
      {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 cldy",
        panpage:[{
          panclass:"panPage-1",
          imageblock: [{
              imagestoshow: [{
                  imgnotindiv:true,
                  imgclass: "background p1",
                  imgid: 'bg_sunny02',
                  imgsrc: ""
              }]
          }]
        },{
          panclass:"panPage-2 bgsky",
          imageblock: [{
              imagestoshow: [{
                  imgnotindiv:true,
                  imgclass: "panSun",
                  imgid: 'sun',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "clouds cld_1 cld_1_Anim",
                  imgid: 'cloud01',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "clouds-t9 cld_3 cld_3_Anim",
                  imgid: 'cloud01',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "clouds cld_2 t11 cld_2_Anim",
                  imgid: 'cloud02',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "clouds-t20-inverted cld_4 cld_4_Anim",
                  imgid: 'cloud02',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "rabit",
                  imgid: 'rbt_2',
                  imgsrc: ""
              },{
                  imgdiv:"sp-2",
                  imgclass: "spimg",
                  imgid: 'dialImg',
                  imgsrc: "",
                  textblock:[{
                    txtcls:"sptxt",
                    imgtxt:data.string.cldy_txt_2
                  }]
              }]
          }]
        }]
      },
    //slide 6
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg1 cldy",
          imageblock: [{
              imagestoshow: [{
                  imgnotindiv:true,
                  imgclass: "background",
                  imgid: 'bg_sunny02',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "rabit",
                  imgid: 'rabbit03',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "foggycld",
                  imgid: 'cloud03',
                  imgsrc: ""
              },{
                  imgnotindiv:true,
                  imgclass: "foggycld-l41-b75",
                  imgid: 'cloud03',
                  imgsrc: ""
              },{
                  imgdiv:"sp-2",
                  imgclass: "spimg",
                  imgid: 'dialImg',
                  imgsrc: "",
                  textblock:[{
                    txtcls:"sptxt",
                    imgtxt:data.string.cldy_txt_3
                  }]
              }]
          }]
        },
  ];

var contentrny=[
  // slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 rny",
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background",
                imgid: 'bg_sunny01_1',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "raining",
                imgid: 'raining',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnyClds",
                imgid: 'rainy_clouds',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnyClds-l53",
                imgid: 'rainy_clouds',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnOngrnd",
                imgid: 'rain_on_ground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabitUmbrla",
                imgid: 'rabbit_with_umbrella',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rainDrop",
                imgid: 'rain_drop01',
                imgsrc: ""
            },{
                imgdiv:"sp-2-fntsz4",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.rainy_txt_1
                }]
            }]
        }]
    },
  // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 rny",
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background",
                imgid: 'bg_sunny-playground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabitUmbrla",
                imgid: "rabbit_with_umbrella",
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "raining",
                imgid: 'raining',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnyClds",
                imgid: 'rainy_clouds',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnyClds-l53",
                imgid: 'rainy_clouds',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnOngrnd-t69",
                imgid: 'rain_on_ground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rainDrop",
                imgid: 'rain_drop01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rainDrop-l26",
                imgid: 'rain_drop01',
                imgsrc: ""
            },{
                imgdiv:"sp-2-fntsz3hlf",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.rainy_txt_2
                }]
            }]
        }]
    },
  // slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 rny",
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background",
                imgid: 'bg_sunny-playground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabitUmbrla",
                imgid: 'rabbit_with_umbrella',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "raining",
                imgid: 'raining',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnyClds",
                imgid: 'rainy_clouds',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnyClds-l53",
                imgid: 'rainy_clouds',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnOngrnd-t69",
                imgid: 'rain_on_ground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rainDrop",
                imgid: 'rain_drop01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rainDrop-l26",
                imgid: 'rain_drop01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "boyUmbrela byUmbPng",
                imgid: 'a_boy_with_umbrella',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "boyUmbrela boyUmbAnim",
                imgid: 'boy_with_umbrella',
                imgsrc: ""
            },{
                imgdiv:"sp-2",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.rainy_txt_3
                }]
            }]
        }]
    },
  // slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 rny",
        imageblock: [{
            imagestoshow: [{
                imgnotindiv:true,
                imgclass: "background",
                imgid: 'bg_sunny-playground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rabitUmbrla",
                imgid: 'rabbit_with_umbrella',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "raining",
                imgid: 'raining',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnyClds",
                imgid: 'rainy_clouds',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnyClds-l53",
                imgid: 'rainy_clouds',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rnOngrnd-t69",
                imgid: 'rain_on_ground',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rainDrop",
                imgid: 'rain_drop01',
                imgsrc: ""
            },{
                imgnotindiv:true,
                imgclass: "rainDrop-l26",
                imgid: 'rain_drop01',
                imgsrc: ""
            },{
                imgdiv:"sp-2",
                imgclass: "spimg",
                imgid: 'dialImg',
                imgsrc: "",
                textblock:[{
                  txtcls:"sptxt",
                  imgtxt:data.string.rainy_txt_4
                }]
            }]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = maincontent.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();
    var mergecontent = [];
    var visitedcontent =
        {"sunny":false,
        "windy":false,
        "cloudy":false,
        "rainy":false};

    var sunnyContent = $.merge(mergecontent,maincontent);
        sunnyContent = $.merge(mergecontent,cntntSuny);
        mergecontent = [];
    var windyContent = $.merge(mergecontent,maincontent)
        windyContent = $.merge(mergecontent,contentWndy);
        mergecontent = [];
    var cloudyContent = $.merge(mergecontent,maincontent)
        cloudyContent = $.merge(mergecontent,contentCldy);
        mergecontent = [];
    var rainyContent = $.merge(mergecontent,maincontent)
        rainyContent = $.merge(mergecontent,contentrny);
        // mergecontent = [];

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "rbt_1", src: imgpath + "weather/rabbit01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rbt_2", src: imgpath + "weather/rabbit.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit03", src: imgpath + "weather/rabbit03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "weather/text01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg_1", src: imgpath + "weather/tex02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "clouds", src: imgpath + "weather/clouds.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "sunny", src: imgpath + "weather/sunny.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rain", src: imgpath + "weather/rain.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "wind", src: imgpath + "weather/wind.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_02", src: imgpath + "weather/bg_02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_sunny01_1", src: imgpath + "weather/bg_sunny01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_sunny01", src: imgpath + "weather/bg_with_sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_sunny02", src: imgpath + "weather/bg_sunny02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_sunny02a", src: imgpath + "weather/bg_sunny02a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_sunny-playground", src: imgpath + "weather/bg_sunny-playground.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_sunny-drying-clothes", src: imgpath + "weather/bg_sunny-drying-clothes.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ball", src: imgpath + "weather/ball.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg06a", src: imgpath + "weather/bg06a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wave", src: imgpath + "weather/wave.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunDnce", src: imgpath + "weather/sun-dance.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "weather", src: imgpath + "weather/weather.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pant", src: imgpath + "weather/pant.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "shirt", src: imgpath + "weather/shirt.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "t_shirt", src: imgpath + "weather/t_shirt.gif", type: createjs.AbstractLoader.IMAGE},

            {id: "boy_kick_ball", src: imgpath + "weather/boy_kick_ball.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "boy_kick_ball_png", src: imgpath + "weather/boy_kick_ball.png", type: createjs.AbstractLoader.IMAGE},
            {id: "t_shirt", src: imgpath + "weather/t_shirt.gif", type: createjs.AbstractLoader.IMAGE},

            {id: "wind01", src: imgpath + "weather/wind01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_windy", src: imgpath + "weather/bg_windy.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_windy01", src: imgpath + "weather/bg_windy01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "clothes", src: imgpath + "weather/clothes.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "leaf_fly", src: imgpath + "weather/leaf_fly01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "leaf_on_ground", src: imgpath + "weather/leaf_on_ground.png", type: createjs.AbstractLoader.IMAGE},
            {id: "leaf_on_ground01", src: imgpath + "weather/leaf_on_ground01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dust", src: imgpath + "weather/dust.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree", src: imgpath + "weather/tree.gif", type: createjs.AbstractLoader.IMAGE},

            {id: "sun", src: imgpath + "weather/sun.png", type: createjs.AbstractLoader.IMAGE},

            {id: "cloud01", src: imgpath + "weather/cloud01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cloud02", src: imgpath + "weather/cloud02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cloud03", src: imgpath + "weather/cloud03.png", type: createjs.AbstractLoader.IMAGE},

            {id: "rainy_clouds", src: imgpath + "weather/rainy_clouds.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rain_on_ground", src: imgpath + "weather/rain_on_ground.png", type: createjs.AbstractLoader.IMAGE},
            {id: "raining", src: imgpath + "weather/raining.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "rain_drop", src: imgpath + "weather/rain_drop.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "rain_drop01", src: imgpath + "weather/rain_drop01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "boy_with_umbrella", src: imgpath + "weather/boy_with_umbrella.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "a_boy_with_umbrella", src: imgpath + "weather/a_boy_with_umbrella.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbit_with_umbrella", src: imgpath + "weather/rabbit_with_umbrella.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "s1_p1", src: soundAsset + "s1_p1.ogg"},
            {id: "s1_p2", src: soundAsset + "s1_p2.ogg"},
            {id: "s1_p3", src: soundAsset + "s1_p3.ogg"},

            {id: "s1_p3_sunny_1", src: soundAsset + "s1_p3_sunny_1.ogg"},
            {id: "s1_p3_sunny_2", src: soundAsset + "s1_p3_sunny_2.ogg"},
            {id: "s1_p3_sunny_3", src: soundAsset + "s1_p3_sunny_3.ogg"},
            {id: "s1_p3_sunny_4", src: soundAsset + "s1_p3_sunny_4.ogg"},
            {id: "s1_p3_sunny_5", src: soundAsset + "s1_p3_sunny_5.ogg"},

            {id: "s1_p3_cloudy_1", src: soundAsset + "s1_p3_cloudy_1.ogg"},
            {id: "s1_p3_cloudy_2", src: soundAsset + "s1_p3_cloudy_2.ogg"},
            {id: "s1_p3_cloudy_3", src: soundAsset + "s1_p3_cloudy_3.ogg"},

            {id: "s1_p3_windy_1", src: soundAsset + "s1_p3_windy_1.ogg"},
            {id: "s1_p3_windy_2", src: soundAsset + "s1_p3_windy_2.ogg"},
            {id: "s1_p3_windy_3", src: soundAsset + "s1_p3_windy_3.ogg"},
            {id: "s1_p3_windy_4", src: soundAsset + "s1_p3_windy_4.ogg"},
            {id: "s1_p3_windy_5", src: soundAsset + "s1_p3_windy_5.ogg"},

            {id: "s1_p3_rainy_1", src: soundAsset + "s1_p3_rainy_1.ogg"},
            {id: "s1_p3_rainy_2", src: soundAsset + "s1_p3_rainy_2.ogg"},
            {id: "s1_p3_rainy_3", src: soundAsset + "s1_p3_rainy_3.ogg"},
            {id: "s1_p3_rainy_4", src: soundAsset + "s1_p3_rainy_4.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*=
    =            Handlers and helpers Block            =
    =*/
    /*===  register the handlebar partials first  ===*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*
     =            general template function            =
     */
    function generaltemplate() {
        loadpage(maincontent);
        switch (countNext) {
          case 0:
          case 1:
            sound_player("s1_p"+(countNext+1),1);
          break;
          case 2:
            sound_player("s1_p"+(countNext+1),0);
            $(".sunny").append("<img class='sunDance' src='"+preload.getResult("sunDnce").src+"'/>");
            $(".options").click(function () {
                countNext = 3;
                if($(this).hasClass("sunny")){
                    visitedcontent["sunny"]=true;
                    maincontent = sunnyContent;
                    sunnygeneraltemp();
                }
                else if($(this).hasClass("windy")){
                    visitedcontent["windy"]=true;
                    maincontent = windyContent;
                    wndygeneraltemp();
                }
                else if($(this).hasClass("cloudy")){
                    visitedcontent["cloudy"]=true;
                    maincontent = cloudyContent;
                    cloudygeneraltemp();
                }
                else if($(this).hasClass("rainy")){
                    visitedcontent["rainy"]=true;
                    maincontent = rainyContent;
                    rainygeneraltemp();
                }
            });
          break;
          default:

        }
    }
    function loadpage(contenttobeload){
        $total_page = contenttobeload.length;
        loadTimelineProgress($total_page, countNext + 1);
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(contenttobeload[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(contenttobeload, countNext, preload);
    }
    function sunnygeneraltemp(){
        loadpage(maincontent);
        console.log("I am in sunny tab"+countNext);
        switch(countNext){
          case 3:
            // navigationcontroller(countNext,$total_page);
            sound_player("s1_p3_sunny_1",1);
            $prevBtn.css("display","none");
          break;
          case 4:
          //zoomAnimSun
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("s1_p3_sunny_2");
          current_sound.play();
          current_sound.on('complete',function(){
            $(".wave").hide(0);
            $(".imgdv1").addClass("zoomAnimSun");
            navigationcontroller(countNext,$total_page);
          });
          break;
          case 5:
            $(".p1").attr('src',preload.getResult('bg_sunny01').src);
            $(".rabit").attr('src',preload.getResult('rbt_2').src);
            $(".spimg").attr('src',preload.getResult('dialImg').src);
            $(".p2").attr('src',preload.getResult('bg_sunny02a').src);
            $(".wave").attr('src',preload.getResult('wave').src);

            $(".fBall").attr('src',preload.getResult('ball').src);
            $(".bkb_png").attr('src',preload.getResult('boy_kick_ball_png').src);
            $(".bkb_gif").attr('src',preload.getResult('boy_kick_ball').src);

            $(".panPage-1").animate({top:"100%"},3000);
            $(".panPage-2").animate({top:"0%"},3000);
            setTimeout(function(){
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p3_sunny_3");
              current_sound.play();
              current_sound.on('complete',function(){
                  $(".bkb_gif").fadeIn(10);
                  $(".bkb_png").delay(10).fadeOut(1);
                  $(".fBall").addClass("ballAnim");
                  setTimeout(function(){
                    navigationcontroller(countNext,$total_page);
                  },3000);
              });
            },3000);
          break;
          case 6:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p3_sunny_4");
            current_sound.play();
            current_sound.on('complete',function(){
              $(".imgdv1").addClass("zoomAnimclthsDry");
                navigationcontroller(countNext,$total_page);
            });
          break;
          case 7:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p3_sunny_5");
            current_sound.play();
            current_sound.on('complete',function(){
                navigationcontroller(countNext,$total_page,false,checkforlastpage("sunny"));
            });
          break;
          default:
            navigationcontroller(countNext,$total_page);
          break;

        }

    }


    function wndygeneraltemp(){
        loadpage(maincontent);
        console.log("I am in windy tab"+countNext);
        switch(countNext){
          case 3:
            $prevBtn.css("display","none");
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p3_windy_1");
            current_sound.play();
            current_sound.on('complete',function(){
              $(".wnd_1_1").fadeIn(100).addClass("wnd_1_1_anim");
              $(".wnd_2_3").fadeIn(300).addClass("wnd_2_3_anim");
              navigationcontroller(countNext,$total_page);
            })
          break;
          case 4:
            $(".wnd_1_1").fadeIn(100).addClass("wnd_1_1_anim");
            $(".wnd_2_2").delay(2000).fadeIn(500).addClass("wnd_2_2_anim");
            $(".wnd_2_3").fadeIn(1000).addClass("wnd_2_3_anim");
            sound_player("s1_p3_windy_2",1);
            // navigationcontroller(countNext,$total_page);
          break;
          case 5:

            $prevBtn.css("display","none");
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p3_windy_3");
            current_sound.play();
            $(".leavesFly").addClass("lvsFlyAnim");
            $(".leavesFlySec").addClass("lvsFlyAnimSec");
            current_sound.on('complete',function(){
              $(".wnd_1_1").fadeIn(100).addClass("wnd_1_1_anim");
              $(".wnd_2_2").delay(2000).fadeIn(500).addClass("wnd_2_2_anim");
              $(".wnd_2_3").fadeIn(1000).addClass("wnd_2_3_anim");
              $(".wnd_2_4").fadeIn(1000).addClass("wnd_2_4_anim");
              navigationcontroller(countNext,$total_page);
            })
          break;
          case 6:
            sound_player("s1_p3_windy_4",1);
              $(".background").attr('src',preload.getResult('bg_windy01').src);
              $(".rabit").attr('src',preload.getResult('rbt_2').src);
              $(".spimg").attr('src',preload.getResult('dialImg').src);
              $(".wind").attr('src',preload.getResult('wind01').src);
              $(".clths").attr('src',preload.getResult('pant').src);
              $(".treeGif").attr('src',preload.getResult('tree').src);
              $(".shirtGif").attr('src',preload.getResult('shirt').src);
              $(".tshirtGif").attr('src',preload.getResult('t_shirt').src);

                $(".wnd_1_1").fadeIn(100).addClass("wnd_1_1_anim");
                $(".wnd_2_2").delay(2000).fadeIn(500).addClass("wnd_2_2_anim");
                $(".wnd_2_3").fadeIn(1000).addClass("wnd_2_3_anim");
              $(".imgdivcontainer:eq(0)").addClass("zoomAnimclthsblwn");
              // navigationcontroller(countNext,$total_page);
          break;
          case 7:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p3_windy_5");
            current_sound.play();
            current_sound.on('complete',function(){
                navigationcontroller(countNext,$total_page,false,checkforlastpage("windy"));
            });
          break;
          default:
              navigationcontroller(countNext,$total_page);
          break;
        }
    }

    function cloudygeneraltemp(){
        loadpage(maincontent);
        console.log("I am in internal transport tab"+countNext);
        switch(countNext){
          case 3:
            // navigationcontroller(countNext,$total_page);
            sound_player("s1_p3_cloudy_1",0);
            $prevBtn.css("display","none");
            $(".pgtozoomdiv").css("opacity","0.9");
            setTimeout(function(){
              $(".bgLight").fadeOut(1000);
            },8000);
            nav_button_controls(10000);
          break;
          case 4:
            $(".p1").attr('src',preload.getResult('bg_sunny02').src);
            $(".rabit").attr('src',preload.getResult('rabbit03').src);
            $(".spimg").attr('src',preload.getResult('dialImg').src);
            $(".panSun").attr('src',preload.getResult('sun').src);
            $(".cld_1, .cld_3").attr('src',preload.getResult('cloud01').src);
            $(".cld_2, .cld_4").attr('src',preload.getResult('cloud02').src);

            setTimeout(function(){
              $(".panPage-1").animate({top:"100%"},3000);
              $(".panPage-2").animate({top:"0%"},3000);
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p3_cloudy_2");
              current_sound.play();
              current_sound.on('complete',function(){
                  navigationcontroller(countNext,$total_page);
              });
            },1000);
          break;
          case 5:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p3_cloudy_3");
            current_sound.play();
            current_sound.on('complete',function(){
                navigationcontroller(countNext,$total_page,false,checkforlastpage("cloudy"));
            });
          break;
            default:
                navigationcontroller(countNext,$total_page);
        }
    }

    function rainygeneraltemp(){
        loadpage(maincontent);
        console.log("I am in rainy tab"+countNext);
        switch(countNext){
          case 3:
            // navigationcontroller(countNext,$total_page);
            sound_player("s1_p3_rainy_1",1);
            $prevBtn.css("display","none");
          break;
          case 4:
            sound_player("s1_p3_rainy_2",1);
          break;
          case 5:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p3_rainy_3");
            current_sound.play();
            current_sound.on('complete',function(){
                navigationcontroller(countNext,$total_page,false,checkforlastpage("rainy"));
            });
          break;
          case 6:
            sound_player("s1_p3_rainy_4",1);
          break;
          default:
            navigationcontroller(countNext,$total_page);
        }
    }

    function  checkforlastpage(updatekey){
        var test = 0;
        visitedcontent[updatekey] = true;
        if( visitedcontent["sunny"] == true &&
            visitedcontent["windy"] == true &&
            visitedcontent["cloudy"] == true &&
            visitedcontent["rainy"] == true
        )
            return false;
        else
            return true;
    }

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }

    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on("click", function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        if (countNext == $total_page - 1) {
            countNext = 2;
            templateCaller();
        }
        else {
            countNext++;
            switchtemplate();
        }
    });

    function switchtemplate(){
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        $(".contentblock").hasClass("sny")?sunnygeneraltemp():
            $(".contentblock").hasClass("wndy")?wndygeneraltemp():
                $(".contentblock").hasClass("cldy")?cloudygeneraltemp():
                    $(".contentblock").hasClass("rny")?rainygeneraltemp():generaltemplate();
    }

    $refreshBtn.click(function(){
        countNext==0?templateCaller():switchtemplate();
    });


    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        countNext==0?templateCaller():switchtemplate();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

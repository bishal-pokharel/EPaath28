var imgpath = $ref + "/images/";
var soundAsset = $ref + "/audio_" + $lang + "/";

var content = [
  {
    // slide0
    contentblockadditionalclass: "bluebg",
    contentnocenteradjust: true,
    // uppertextblocknocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description1",
        textdata: data.string.title
      }
    ]
  },
  {
    // slide1
    contentblockadditionalclass: "bluebg",
    contentnocenteradjust: true,
    imageblock: [
      {
        imgcontainerdiv: "svgcontainer1 svgcontainer"
      }
    ],
    uppertextblock: [
      {
        textclass: "description grey_bg",
        textdata: data.string.p1_s0
      }
    ]
  },
  {
    // slide2
    contentblockadditionalclass: "bluebg",
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description grey_bg",
        textdata: data.string.p1_s1
      }
    ],
    imageblock: [
      {
        imgcontainerdiv: "svgcontainer1",
        imagelabels: [
          {
            imagelabelclass: "imagelabel1",
            imagelabeldata: data.string.p1_s2
          }
        ]
      },
      {
        imgcontainerdiv: "svgcontainer2",
        imagelabels: [
          {
            imagelabelclass: "imagelabel1",
            imagelabeldata: data.string.p1_s3
          }
        ]
      },
      {
        imgcontainerdiv: "svgcontainer3",
        imagelabels: [
          {
            imagelabelclass: "imagelabel1",
            imagelabeldata: data.string.p1_s4
          }
        ]
      }
    ]
  },
  {
    // slide3
    contentblockadditionalclass: "bluebg",
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description grey_bg",
        textdata: data.string.p1_s1
      }
    ],
    imageblock: [
      {
        imgcontainerdiv: "svgcontainer1",
        imagelabels: [
          {
            imagelabelclass: "imagelabel1",
            imagelabeldata: data.string.p1_s2
          }
        ]
      },
      {
        imgcontainerdiv: "svgcontainer2",
        imagelabels: [
          {
            imagelabelclass: "imagelabel1",
            imagelabeldata: data.string.p1_s3
          }
        ]
      },
      {
        imgcontainerdiv: "svgcontainer3",
        imagelabels: [
          {
            imagelabelclass: "imagelabel1",
            imagelabeldata: data.string.p1_s4
          }
        ]
      }
    ]
  },
  {
    // slide4
    contentblockadditionalclass: "bluebg",
    contentnocenteradjust: true,
    imageblock: [
      {
        imgcontainerdiv: "svgboy"
      },
      {
        imagelabels: [
          {
            imagelabelclass: "imagelabel2_a ",
            imagelabeldata: data.string.p1_s2
          },
          {
            imagelabelclass: "imagelabel2_b ",
            imagelabeldata: data.string.p1_s3
          },
          {
            imagelabelclass: "imagelabel2_c ",
            imagelabeldata: data.string.p1_s4
          }
        ]
      }
    ]
  },
  {
    // slide5
    contentblockadditionalclass: "bluebg",
    contentnocenteradjust: true,
    imageblock: [
      {
        imgcontainerdiv: "svgboy boy_left"
      },
      {
        imgcontainerdiv: "svgplant fadein1"
      },
      {
        imagelabels: [
          {
            imagelabelclass: "imagelabel2_a",
            imagelabeldata: data.string.p1_s2
          },
          {
            imagelabelclass: "imagelabel2_b",
            imagelabeldata: data.string.p1_s3
          },
          {
            imagelabelclass: "imagelabel2_c",
            imagelabeldata: data.string.p1_s4
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "ltb_addn",
    lowertextblock: [
      {
        textclass: "description_label fadein1",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_label fadein5",
        textdata: data.string.p1_s6
      }
    ]
  },
  {
    // slide6
    contentblockadditionalclass: "bluebg",
    contentnocenteradjust: true,
    imageblock: [
      {
        imgcontainerdiv: "svgplant2"
      },
      {
        imagelabels: [
          {
            imagelabelclass: "imagelabel3_d",
            imagelabeldata: data.string.p1_s10
          },
          {
            imagelabelclass: "imagelabel3_c",
            imagelabeldata: data.string.p1_s9
          },
          {
            imagelabelclass: "imagelabel3_b",
            imagelabeldata: data.string.p1_s8
          },
          {
            imagelabelclass: "imagelabel3_a",
            imagelabeldata: data.string.p1_s7
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  loadTimelineProgress($total_page, countNext + 1);

  //for preload
  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      {
        id: "boy",
        src: imgpath + "boy.svg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bubble2",
        src: imgpath + "bubble2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "coverpage",
        src: imgpath + "coverpage.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "plant",
        src: imgpath + "plant.svg",
        type: createjs.AbstractLoader.IMAGE
      },

      //sounds
      { id: "ding", src: soundAsset + "ding.ogg" },
      { id: "p1_s0", src: soundAsset + "p1_s0.ogg" },
      { id: "p1_s1", src: soundAsset + "p1_s1.ogg" },
      { id: "p1_s2_0", src: soundAsset + "p1_s2_0.ogg" },
      { id: "p1_s2_1", src: soundAsset + "p1_s2_1.ogg" },
      { id: "p1_s2_2", src: soundAsset + "p1_s2_2.ogg" },
      { id: "p1_s2_3", src: soundAsset + "p1_s2_3.ogg" },
      { id: "p1_s5", src: soundAsset + "p1_s5.ogg" },
      { id: "p1_s6", src: soundAsset + "p1_s6.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
=            Handlers and helpers Block            =
==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
     =            data highlight function            =
     ===============================================*/
  /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }

  /*=====  End of data highlight function  ======*/

  /*===============================================
     =            user notification function        =
     ===============================================*/
  /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

  /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }
  /*=====  End of user notification function  ======*/

  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
  /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }
  /*=====  End of user navigation controller function  ======*/

  /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
  /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

  /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }
  /*=====  End of InstructionBlockController  ======*/

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var intervalcontroller;
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    put_image(content, countNext);
    vocabcontroller.findwords(countNext);
    // splitintofractions($(".fractionblock"));
    // sound_player("ding");
    switch (countNext) {
      case 0:
        sound_player("p1_s0");
        $(".bluebg").css(
          "background-image",
          "url('" + preload.getResult("coverpage").src + "')"
        );
        break;
      case 1:
        var $svg1 = Snap(".svgcontainer1");

        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg1.append(loadedFragment);
          sound_player("p1_s1");
        });
        break;
      case 2:
        var $svg1 = Snap(".svgcontainer1");
        var $svg2 = Snap(".svgcontainer2");
        var $svg3 = Snap(".svgcontainer3");
        var audiochain = ["p1_s2_0", "p1_s2_1", "p1_s2_2", "p1_s2_3"];
        $(".svgcontainer1, .svgcontainer2, .svgcontainer3").hide(0);
        var displaycontainers = [
          "sdsadfa",
          "svgcontainer1",
          "svgcontainer2",
          "svgcontainer3"
        ];
        chain_player(audiochain, displaycontainers);

        // var svg = Snap.load(preload.getResult('boy').src, function ( loadedFragment ) {
        // $svg1.append(loadedFragment);
        // $svg2.append(loadedFragment);
        // $svg3.append(loadedFragment);
        // });
        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg1.append(loadedFragment);
          // $(".svgcontainer1 #body, .svgcontainer1 #head, .svgcontainer1 #leg").css("opacity", "0.5");
          $(
            ".svgcontainer1 #body, .svgcontainer1 #leg, .svgcontainer1 #hands"
          ).css("opacity", "0.1");
          $(".svgcontainer1 #head").css({
            filter: "drop-shadow(0 0 0.2em #000)"
          });
        });
        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg2.append(loadedFragment);
          $(
            ".svgcontainer2 #head, .svgcontainer2 #leg,.svgcontainer2 #body"
          ).css("opacity", "0.1");
          $(".svgcontainer2 #hands").css({
            filter: "drop-shadow(0 0 0.2em #000)"
          });
        });
        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg3.append(loadedFragment);
          $(
            ".svgcontainer3 #body, .svgcontainer3 #head,.svgcontainer3 #hands"
          ).css("opacity", "0.1");
          $(".svgcontainer3 #leg").css({
            filter: "drop-shadow(0 0 0.2em #000)"
          });
        });

        break;
      case 3:
        navigationcontroller();
        var $svg1 = Snap(".svgcontainer1");
        var $svg2 = Snap(".svgcontainer2");
        var $svg3 = Snap(".svgcontainer3");
        // var svg = Snap.load(preload.getResult('boy').src, function ( loadedFragment ) {
        // $svg1.append(loadedFragment);
        // $svg2.append(loadedFragment);
        // $svg3.append(loadedFragment);
        // });
        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg1.append(loadedFragment);
          // $(".svgcontainer1 #body, .svgcontainer1 #head, .svgcontainer1 #leg").css("opacity", "0.5");
          $(
            ".svgcontainer1 #body, .svgcontainer1 #leg,.svgcontainer1 #hands"
          ).css("opacity", "0.1");
          $(".svgcontainer1 #head").css({
            filter: "drop-shadow(0 0 0.2em #000)"
          });
        });
        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg2.append(loadedFragment);
          $(
            ".svgcontainer2 #head, .svgcontainer2 #leg, .svgcontainer2 #body"
          ).css("opacity", "0.1");
          $(".svgcontainer2 #hands").css({
            filter: "drop-shadow(0 0 0.2em #000)"
          });
        });
        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg3.append(loadedFragment);
          $(
            ".svgcontainer3 #body, .svgcontainer3 #head,.svgcontainer3 #hands"
          ).css("opacity", "0.1");
          $(".svgcontainer3 #leg").css({
            filter: "drop-shadow(0 0 0.2em #000)"
          });
        });
        timeoutcontroller = setTimeout(function() {
          $(".svgcontainer1, .svgcontainer3").addClass("animate_to_center");
          $(".svgcontainer3 #body").css("opacity", "1");
          $(".imagelabel1").fadeOut(600);
        }, 800);
        break;
      case 4:
        $("label").hide(0);
        var displaycontainers = [
          "imagelabel2_a",
          "imagelabel2_b",
          "imagelabel2_c"
        ];
        var audiochain = ["p1_s2_1", "p1_s2_2", "p1_s2_3"];
        chain_player(audiochain, displaycontainers);
        var $svg1 = Snap(".svgboy");
        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg1.append(loadedFragment);
          $(".svgboy").addClass("slideboy_to_left");
        });
        break;
      case 5:
        var $svg1 = Snap(".svgboy");
        var $svg2 = Snap(".svgplant");
        $(".ltb_addn, .svgplant").hide(0);
        var svg = Snap.load(preload.getResult("boy").src, function(
          loadedFragment
        ) {
          $svg1.append(loadedFragment);
        });

        var svg2 = Snap.load(preload.getResult("plant").src, function(
          loadedFragment
        ) {
          $svg2.append(loadedFragment);
          timeoutcontroller = setTimeout(function() {
            $(".ltb_addn").fadeIn(700);
            sound_player("p1_s5");
          }, 1200);
          $(".svgplant")
            .delay(400)
            .show(0);
        });
        break;
      case 6:
        var $svg1 = Snap(".svgplant2");
        var $label = $("[class^='imagelabel3_']").hide(0);
        var svg = Snap.load(preload.getResult("plant").src, function(
          loadedFragment
        ) {
          $svg1.append(loadedFragment);
          var $leaf = $("#leaf").hide(0);
          var $root = $("#root").hide(0);
          var $steam = $("#stem").hide(0);
          var $flower = $("#flower").hide(0);
          var count = 0;
          timeoutcontroller = setTimeout(function() {
            sound_player("p1_s6");
          }, 2050);
          intervalcontroller = setInterval(function() {
            switch (count) {
              case 0:
                $root.fadeIn("1600");
                $($label[count]).fadeIn("1600");
                break;
              case 1:
                $steam.fadeIn("1600");
                $label.hide(0);
                $($label[count]).fadeIn("1600");
                break;
              case 2:
                $leaf.fadeIn("1600");
                $label.hide(0);
                $($label[count]).fadeIn("1600");
                break;
              case 3:
                $flower.fadeIn("1600");
                $label.hide(0);
                $($label[count]).fadeIn("1600");
                break;
              default:
                $label.hide(0);
                $label.fadeIn(500);
                clearInterval(intervalcontroller);
                ole.footerNotificationHandler.pageEndSetNotification();
                break;
            }
            count++;
          }, 1850);
        });
        break;
      default:
        break;
    }
  }

  /*=====  End of Templates Block  ======*/

  function sound_player(sound_id) {
    createjs.Sound.stop();
    var current_sound = createjs.Sound.play(sound_id);
    current_sound.on("complete", function() {
      navigationcontroller();
    });
  }

  function chain_player(sound_array, dialog_array) {
    if (dialog_array != null) $("." + dialog_array[0]).fadeIn(200);
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_array[0]);
    current_sound.on("complete", function() {
      sound_array.splice(0, 1);
      if (dialog_array != null) dialog_array.splice(0, 1);
      if (sound_array.length > 0) {
        if (dialog_array != null) chain_player(sound_array, dialog_array);
        else chain_player(sound_array, null);
      } else navigationcontroller();
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      for (var j = 0; j < content[count].imageblock.length; j++) {
        var imageblock = content[count].imageblock[j];
        console.log("imageblock", imageblock);
        if (imageblock.hasOwnProperty("imagestoshow")) {
          var imageClass = imageblock.imagestoshow;
          for (var i = 0; i < imageClass.length; i++) {
            var image_src = preload.getResult(imageClass[i].imgid).src;
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = "." + classes_list[classes_list.length - 1];
            $(selector).attr("src", image_src);
          }
        }
      }
    }

    if (content[count].hasOwnProperty("containsdialog")) {
      for (var j = 0; j < content[count].containsdialog.length; j++) {
        var containsdialog = content[count].containsdialog[j];
        console.log("imageblock", imageblock);
        if (containsdialog.hasOwnProperty("dialogueimgid")) {
          var image_src = preload.getResult(containsdialog.dialogueimgid).src;
          //get list of classes
          var classes_list =
            containsdialog.dialogimageclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  /*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller() {
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page, countNext + 1);
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    clearInterval(intervalcontroller);
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    clearInterval(intervalcontroller);
    countNext--;
    templateCaller();

    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
  /*=====  End of Templates Controller Block  ======*/
});

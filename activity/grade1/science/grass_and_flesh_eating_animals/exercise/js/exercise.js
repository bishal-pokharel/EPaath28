var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.playtime
		}],
				imageblock:[{
						imagestoshow: [
								{
										imgclass: "rhinoimgdiv",
										imgid: "rhinodance",
								},
								{
										imgclass: "squirrelisteningimg",
										imgid: "squirrel",
								},
								{
										imgclass: "bg_full",
										imgid: "bg",
								}]
				}]
	},
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins1,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'meat',
			textdata:  data.string.p1text16,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'grass',
			textdata:  data.string.p1text17,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.p1text12,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'tiger',
			childid: 'meat'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide2 buffalo
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins2,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal',
			imgid : 'meat',
			textdata:  data.string.p1text16,
		},{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal anim-2',
			imgid : 'grass',
			textdata:  data.string.p1text17,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.p1text19,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'buffalo',
			// motherclass: 'buffalo',
			childid: 'grass'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide3 deer
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins3,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal',
			imgid : 'meat',
			textdata:  data.string.p1text16,
		},{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal anim-2',
			imgid : 'grass',
			textdata:  data.string.p1text17,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.p1text31,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'deer',
			childid: 'grass'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide4 elephant
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins4,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal',
			imgid : 'meat',
			textdata:  data.string.p1text16,
		},{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal anim-2',
			imgid : 'grass',
			textdata:  data.string.p1text17,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.p1text27,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'elephant',
			childid: 'grass'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide5 fox
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins5,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'meat',
			textdata:  data.string.p1text16,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'grass',
			textdata:  data.string.p1text17,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.p1text34,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'fox',
			childid: 'meat'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide5 leopard
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins6,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'meat',
			textdata:  data.string.p1text16,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'grass',
			textdata:  data.string.p1text17,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.p1text30,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'leopard',
			childid: 'meat'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide5 rhinoceros
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins7,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal',
			imgid : 'meat',
			textdata:  data.string.p1text16,
		},{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal anim-2',
			imgid : 'grass',
			textdata:  data.string.p1text17,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.p1text24,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'rhinoceros',
			childid: 'grass'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	// slide5 crock
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata:  data.string.e1ins8,
			textclass: "",
		}],

		optionblockadditionalclass: 'optionsblock slideRightIn',
		optionblock:[{
			optionclass: 'optiondiv class-1',
			imgclass: 'opt-animal',
			imgid : 'meat',
			textdata:  data.string.p1text16,
		},{
			optionclass: 'optiondiv class-2',
			imgclass: 'opt-animal anim-2',
			imgid : 'grass',
			textdata:  data.string.p1text17,
		}],

		boatblock: [{
			boatclass: 'boat-1 boat-anim',
			motherdata: data.string.p1text40,
			childdata: '',
			hasflag: true,
			flagid: 'flag',
			boatid: 'boat',
			motherid: 'crock',
			childid: 'meat'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				},{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-3'
				}
			]
		}],
	},
	//lasts
	{
		extratextblock:[
		{
			textclass:'hurray',
			textdata:data.string.hurray
		},
		{
			textclass:'playagain',
			textdata:data.string.playagain
		},
		{
			textclass:'mainmenu',
			textdata:data.string.mainmenu
		},
		{
			textclass:'learnagain',
			textdata:data.string.learnagain
		},
		{
			textclass:'grassbarm',
			textdata:data.string.p2text22
		},
		{
			textclass:'fleshbarm',
			textdata:data.string.p2text32
		}
	],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'lastpage',
					imgsrc: ""
				},
				{
					imgclass: "lastani-1",
					imgid : 'elephant',
					imgsrc: ""
				},
				{
					imgclass: "lastani-2",
					imgid : 'horse',
					imgsrc: ""
				},
				{
					imgclass: "lastani-3",
					imgid : 'deer',
					imgsrc: ""
				},
				{
					imgclass: "lastani-4",
					imgid : 'sheep',
					imgsrc: ""
				},
				{
					imgclass: "lastani-5",
					imgid : 'tiger',
					imgsrc: ""
				},
				{
					imgclass: "lastani-6",
					imgid : 'lion',
					imgsrc: ""
				},
				{
					imgclass: "lastani-7",
					imgid : 'fox',
					imgsrc: ""
				}
			]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: "images/grade1_playtime/squirrel.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: "images/grade1_playtime/rhino.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "flag", src: imgpath+"ex/flag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boat", src: imgpath+"ex/boat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-1", src: imgpath+"ex/wave01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-2", src: imgpath+"ex/wave02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-3", src: imgpath+"ex/wave03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cloud-1", src: imgpath+"cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-3", src: imgpath+"cloud03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-ex", src: imgpath+"ex/cloud_ex.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tiger", src: imgpath+"playtime/tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tigereat", src: imgpath+"playtime/tiger01.gif", type: createjs.AbstractLoader.IMAGE},


			{id: "rhino", src: imgpath+"playtime/rhino.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinoeat", src: imgpath+"playtime/rhino.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "elephant", src: imgpath+"playtime/elephant_eating.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephanteat", src: imgpath+"playtime/elephant_eating.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "lastpage", src: imgpath+"last_page.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fox", src: imgpath+"playtime/fox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "foxeat", src: imgpath+"playtime/fox_eating.gif", type: createjs.AbstractLoader.IMAGE},



			{id: "horse", src: imgpath+"playtime/horse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "horseeat", src: imgpath+"playtime/horse01.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "lion", src: imgpath+"playtime/lion.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lioneat", src: imgpath+"playtime/lion01.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "sheep", src: imgpath+"playtime/sheep.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sheepeat", src: imgpath+"playtime/sheep01.gif", type: createjs.AbstractLoader.IMAGE},


			{id: "tiger", src: imgpath+"tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "buffalo", src: imgpath+"buffalo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deer", src: imgpath+"deer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fox", src: imgpath+"fox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "leopard", src: imgpath+"lepord.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinoceros", src: imgpath+"rhino.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crock", src: imgpath+"crock.png", type: createjs.AbstractLoader.IMAGE},

			{id: "meat", src: imgpath+"meat_for_play_time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grass", src: imgpath+"grass_for_play_time.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"inst-01.ogg"},
			{id: "sound_2", src: soundAsset+"inst-02.ogg"},
			{id: "sound_3", src: soundAsset+"inst-03.ogg"},
			{id: "sound_4", src: soundAsset+"inst-04.ogg"},
			{id: "sound_5", src: soundAsset+"inst-05.ogg"},
			{id: "sound_6", src: soundAsset+"inst-06.ogg"},
			{id: "sound_7", src: soundAsset+"inst-07.ogg"},
			{id: "sound_8", src: soundAsset+"inst-08.ogg"},
			{id: "sound_9", src: soundAsset+"inst-09.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	 function navigationcontroller(islastpageflag){
 	 typeof islastpageflag === "undefined" ?
 	 islastpageflag = false :
 	 typeof islastpageflag != 'boolean'?
 	 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 	 null;

 	 if(countNext == 0 && $total_page!=1){
 	 	$nextBtn.show(0);
 	 	$prevBtn.css('display', 'none');
 	 }
 	 else if($total_page == 1){
 	 	$prevBtn.css('display', 'none');
 	 	$nextBtn.css('display', 'none');

 	 	// if lastpageflag is true
 	 	islastpageflag ?
 	 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 	 }
 	 else if(countNext > 0 && countNext < $total_page-1){
 	 	$nextBtn.show(0);
 	 	$prevBtn.show(0);
 	 }
 	 // else if(countNext == $total_page-1){
 	 // 	$nextBtn.css('display', 'none');
 	 // 	$prevBtn.show(0);
	 //
 	 // 	// if lastpageflag is true
 	 // 	islastpageflag ?
 	 // 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	 // 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 	 // }
 	 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		firstPagePlayTime(countNext);



		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_image3(content, countNext);
			if(countNext==7){
				$(".boat-child").css({"left":"65%"});
			}
			if(countNext==8){
				$(".boat-child").css({"left":"74%","bottom":"21%"});
				$(".boat-mother").css({
															"left":"8%",
															"bottom":"18%",
															"transform": "scaleX(-1) rotate(5deg)"
				});
			}
			if(countNext==3){
				$(".boat-mother").css({"left":"14%"});
			}
			if(countNext>0){
				sound_player('sound_'+countNext);
			}
		switch(countNext) {
			case 0:
				// sound_player('playtime1');
				$nextBtn.delay(1000).show(0);
				break;
			default:
				$('.boatdiv').animate({'left': '10%'}, 3000, "swing");
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				//to randomize option
				if(Math.random()>0.5) $('.optiondiv').eq(0).css('order', '2');
				$('.optiondiv').click(function(){
					if($(this).hasClass('class-1')){
						var answerText = $(this).children('p').html();
						play_correct_incorrect_sound(1);
						$(this).children('.correct-icon').show(0);
						$(this).addClass('correct-answer');
						$('.childtext').html(answerText);
						$('.childtext').css('background-color', 'transparent');
						$('.boat-child').show(0);
						$('.optiondiv').css('pointer-events', 'none');
						navigationcontroller();
						// $nextBtn.show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
						$(this).children('.incorrect-icon').show(0);
						$(this).addClass('incorrect-answer');
					}
				});
				break;
		}

				$('.playagain').click(function(){
					$('#activity-page-exercise-tab').find('button').trigger('click');
				});
				$('.learnagain').click(function(){
					$("#activity-page-lesson-tab").find('button').trigger('click');

				});
				$('.mainmenu').click(function(){
					$("#activity-page-menu-img").trigger("click");
				});

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('optionblock')){
			var imageClass = content[count].optionblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_image3(content, count){
		if(content[count].hasOwnProperty('boatblock')){
			var imageClass = content[count].boatblock;
			for(var i=0; i<imageClass.length; i++){
				var mSrc = preload.getResult(imageClass[i].motherid).src;
				var cSrc = preload.getResult(imageClass[i].childid).src;
				var bSrc = preload.getResult(imageClass[i].boatid).src;
				var boatClass = imageClass[i].boatclass.match(/\S+/g) || [];
				var mSel = ('.'+boatClass[boatClass.length-1]+'>.boat-mother');
				var cSel = ('.'+boatClass[boatClass.length-1]+'>.boat-child');
				var bSel = ('.'+boatClass[boatClass.length-1]+'>.boatbody');
				if(imageClass[i].hasOwnProperty('flagid')){
					var fSrc = preload.getResult(imageClass[i].flagid).src;
					var fSel = ('.'+boatClass[boatClass.length-1]+'>.boat-flag');
					$(fSel).attr('src', fSrc);
				}
				$(mSel).attr('src', mSrc);
				$(cSel).attr('src', cSrc);
				$(bSel).attr('src', bSrc);
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			case 0:
				countNext++;
				templateCaller();
				break;
			default:
				$nextBtn.hide(0);
				$('.optionsblock').removeClass('slideRightIn').addClass('slideRightOut');
				timeoutvar = setTimeout(function(){
					$('.boatdiv').animate({'left': '110%'}, 4000, "swing", function(){
						countNext++;
						templateCaller();
					});
				}, 1000);
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

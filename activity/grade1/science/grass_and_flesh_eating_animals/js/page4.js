var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0

			contentnocenteradjust: true,
			contentblockadditionalclass: 'bg-ex',

			uppertextblockadditionalclass: 'playtime-text',
			uppertextblock:[{
				textdata: data.string.diy,
				textclass: "",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-playtime",
						imgsrc : '',
						imgid : 'bg'
					}
				]
			}],
		},
	// 	contentnocenteradjust: true,
	// 	uppertextblock:[{
	// 		textclass: "description1",
	// 		textdata: data.string.playtime
	// 	}],
  //       imageblock:[{
  //           imagestoshow: [
  //               {
  //                   imgclass: "rhinoimgdiv",
  //                   imgid: "rhinodance",
  //               },
  //               {
  //                   imgclass: "squirrelisteningimg",
  //                   imgid: "squirrel",
  //               },
  //               {
  //                   imgclass: "bg_full",
  //                   imgid: "bg",
  //               }]
  //       }]
	// },
// slide1
{
	singletext:[
		{
			textclass: "toptext",
			textdata: data.string.pt1
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'zookeeper01',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-1 thisflip',
	textclass: "answer",
	textdata: data.string.pt2,
	imgclass: '',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide2
{
	singletext:[
		{
			textclass: "toptext",
			textdata: data.string.pt5
		},
		{
			textclass: "anitext-1 buttonsel correct meat forhover",
			textdata: data.string.p1text12
		},
		{
			textclass: "anitext-2 buttonsel forhover",
			textdata: data.string.p1text31
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'zookeeper04',
			imgsrc: ""
		},
		{
			imgclass: "animal-1 correctimg",
			imgid : 'tiger',
			imgsrc: ""
		},
		{
			imgclass: "animal-2",
			imgid : 'deer',
			imgsrc: ""
		},
		{
			imgclass: "cage-1",
			imgid : 'cage',
			imgsrc: ""
		},
		{
			imgclass: "cage-2",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	singletext:[
		{
			textclass: "toptext",
			textdata: data.string.pt4
		},
		{
			textclass: "anitext-1 buttonsel correct grass forhover",
			textdata: data.string.p1text27
		},
		{
			textclass: "anitext-2 buttonsel forhover",
			textdata: data.string.p1text12
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'zookeeper03',
			imgsrc: ""
		},
		{
			imgclass: "animal-1 correctimg",
			imgid : 'elephant',
			imgsrc: ""
		},
		{
			imgclass: "animal-2",
			imgid : 'tiger',
			imgsrc: ""
		},
		{
			imgclass: "cage-1",
			imgid : 'cage',
			imgsrc: ""
		},
		{
			imgclass: "cage-2",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	singletext:[
		{
			textclass: "toptext",
			textdata: data.string.pt5
		},
		{
			textclass: "anitext-1 buttonsel forhover",
			textdata: data.string.p1text31
		},
		{
			textclass: "anitext-2 buttonsel correct meat forhover",
			textdata: data.string.p1text34
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'zookeeper04',
			imgsrc: ""
		},
		{
			imgclass: "animal-1",
			imgid : 'deer',
			imgsrc: ""
		},
		{
			imgclass: "animal-2 correctimg",
			imgid : 'fox',
			imgsrc: ""
		},
		{
			imgclass: "cage-1",
			imgid : 'cage',
			imgsrc: ""
		},
		{
			imgclass: "cage-2",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}]
},
// slide5
{
	singletext:[
		{
			textclass: "toptext",
			textdata: data.string.pt4
		},
		{
			textclass: "anitext-1 buttonsel correct grass forhover",
			textdata: data.string.p1text56
		},
		{
			textclass: "anitext-2 buttonsel forhover",
			textdata: data.string.p1text55
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'zookeeper03',
			imgsrc: ""
		},
		{
			imgclass: "animal-1 correctimg",
			imgid : 'horse',
			imgsrc: ""
		},
		{
			imgclass: "animal-2",
			imgid : 'lion',
			imgsrc: ""
		},
		{
			imgclass: "cage-1",
			imgid : 'cage',
			imgsrc: ""
		},
		{
			imgclass: "cage-2",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}]
},
// slide6
{
	singletext:[
		{
			textclass: "toptext",
			textdata: data.string.pt4
		},
		{
			textclass: "anitext-1 buttonsel forhover",
			textdata: data.string.p1text12
		},
		{
			textclass: "anitext-2 buttonsel correct grass forhover",
			textdata: data.string.p1text57
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'zookeeper03',
			imgsrc: ""
		},
		{
			imgclass: "animal-1",
			imgid : 'tiger',
			imgsrc: ""
		},
		{
			imgclass: "animal-2 correctimg",
			imgid : 'sheep',
			imgsrc: ""
		},
		{
			imgclass: "cage-1",
			imgid : 'cage',
			imgsrc: ""
		},
		{
			imgclass: "cage-2",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}]
},
// slide7
{
	singletext:[
		{
			textclass: "toptext",
			textdata: data.string.pt4
		},
		{
			textclass: "anitext-1 buttonsel correct grass forhover",
			textdata: data.string.p1text31
		},
		{
			textclass: "anitext-2 buttonsel forhover",
			textdata: data.string.p1text55
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'zookeeper03',
			imgsrc: ""
		},
		{
			imgclass: "animal-1 correctimg",
			imgid : 'deer',
			imgsrc: ""
		},
		{
			imgclass: "animal-2",
			imgid : 'lion',
			imgsrc: ""
		},
		{
			imgclass: "cage-1",
			imgid : 'cage',
			imgsrc: ""
		},
		{
			imgclass: "cage-2",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg", src: "images/diy_bg/a_24.png", type: createjs.AbstractLoader.IMAGE},


			{id: "cover_page", src: imgpath+"zoo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lastpage", src: imgpath+"last_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zoo", src: imgpath+"zoo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cage", src: imgpath+"cage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zookeeper01", src: imgpath+"zookeeper01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zookeeper02", src: imgpath+"zookeeper02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zookeeper03", src: imgpath+"zookeeper_with_grass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zookeeper04", src: imgpath+"zookeeper_with_meat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tiger", src: imgpath+"playtime/tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tigereat", src: imgpath+"playtime/tiger01.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "buffalo", src: imgpath+"playtime/buffalo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "buffaloeat", src: imgpath+"playtime/buffalo.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "rhino", src: imgpath+"playtime/rhino.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinoeat", src: imgpath+"playtime/rhino.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "elephant", src: imgpath+"playtime/elephant_eating.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephanteat", src: imgpath+"playtime/elephant_eating.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "deer", src: imgpath+"playtime/deer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deereat", src: imgpath+"playtime/deer.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "fox", src: imgpath+"playtime/fox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "foxeat", src: imgpath+"playtime/fox_eating.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "cow", src: imgpath+"playtime/cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coweat", src: imgpath+"playtime/cow01.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "horse", src: imgpath+"playtime/horse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "horseeat", src: imgpath+"playtime/horse01.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "lion", src: imgpath+"playtime/lion.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lioneat", src: imgpath+"playtime/lion01.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "sheep", src: imgpath+"playtime/sheep.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sheepeat", src: imgpath+"playtime/sheep01.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "pig", src: imgpath+"playtime/pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pigeat", src: imgpath+"playtime/pig01.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "meat", src: imgpath+"meat01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grass", src: imgpath+"grass01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_1_1", src: soundAsset+"s4_p1_1.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s4_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
  $nextBtn.css('display', 'none');
  $prevBtn.show(0);

  // if lastpageflag is true
  islastpageflag ?
  ole.footerNotificationHandler.lessonEndSetNotification() :
  ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
// firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		$(".anitext-1").hover(function(){
			$(".asha-2").addClass("daiflip");
		});

		$(".anitext-2").hover(function(){
			$(".asha-2").removeClass("daiflip");
		});

		switch(countNext){
			case 0:
			play_diy_audio();
			break;
			case 1:
			sound_player("sound_1", 0);

                current_sound.on('complete', function(){
                    sound_player("sound_1_1", 1);

                });
			break;
			case 2:
			sound_player("sound_2", 0);
			break;
			case 3:
			sound_player("sound_3", 0);

			$('.animal-2').css({
													"width":"26%",
													"top":"44%"
			});
			break;
			case 4:
			sound_player("sound_2", 0);
			break;
			case 5:
			sound_player("sound_3", 0);

			break;

			case 6:
			sound_player("sound_3", 0);

			$('.animal-2').css({
													"width":"24%",
													"top":"44%"
			});
			break;
			case 7:
			sound_player("sound_3", 0);

			break;
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("forhover")){
				$(this).removeClass('forhover');
					if($(this).hasClass("correct")){
						createjs.Sound.stop();

						play_correct_incorrect_sound(1);
						$(".anitext-1,.anitext-2").css({"pointer-events":"none"});
						$(this).css("background","#bed62f");
						$(this).css("border","0.3em solid #deef3c");
						$(this).css("color","white");
						// $(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						$(".asha-2").attr('src',preload.getResult("zookeeper02").src);
						appender($(this),'corrimg');
						$('.buttonsel').removeClass('forhover forhoverimg');

						switch (countNext) {
							case 2:

								$(".correctimg").attr('src',preload.getResult("tigereat").src);
							break;
							case 3:
							console.log("ehy");

								$(".correctimg").attr('src',preload.getResult("elephanteat").src);
							break;
							case 4:

								$(".correctimg").attr('src',preload.getResult("foxeat").src);
							break;
							case 5:

								$(".correctimg").attr('src',preload.getResult("horseeat").src);
							break;
							case 6:

								$(".correctimg").attr('src',preload.getResult("sheepeat").src);
							break;
							case 7:

								$(".correctimg").attr('src',preload.getResult("deereat").src);
								$(".food-one").css({"left":"11%"});
							break;
						}
						navigationcontroller();
					}
					else{
						createjs.Sound.stop();

						play_correct_incorrect_sound(0);
						appender($(this),'incorrimg');
						$(this).css("background","#FF0000");
						$(this).css("border","0.3em solid #980000");
						$(this).css("color","white");
						// $(this).siblings(".wrngopt").show(0);
					}
			}

			function appender($this, icon){

				if($this.hasClass("anitext-1")){
					$(".coverboardfull").append("<img class='icon-one' src= '"+ preload.getResult(icon).src +"'>");
					if(icon == "corrimg"){
						if($this.hasClass("meat")){
							$(".coverboardfull").append("<img class='food-one' src= '"+ preload.getResult("meat").src +"'>");
						}
						else{
							$(".coverboardfull").append("<img class='food-one' src= '"+ preload.getResult("grass").src +"'>");
						}
					}
				}
				else{
					$(".coverboardfull").append("<img class='icon-two' src= '"+ preload.getResult(icon).src +"'>");
					if(icon == "corrimg"){
						if($this.hasClass("meat")){
							$(".coverboardfull").append("<img class='food-two' src= '"+ preload.getResult("meat").src +"'>");
						}
						else{
							$(".coverboardfull").append("<img class='food-two' src= '"+ preload.getResult("grass").src +"'>");
						}
					}
				}
				}
			});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	var Scount = 0;
	function chain_fade(sound_id, lastcount){
		console.log(Scount);
		$(".fademe"+Scount).fadeIn();
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			// countNums(lastcount);
			if(Scount < lastcount){
				Scount++;
				chain_fade("sound_1_"+Scount, lastcount);
			}
			else{
				navigationcontroller();
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	$refreshBtn.click(function(){
		templatecaller();
	});
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//leopard start
	// slide0
	{
	singletext:[
		{
			textclass: "buttonsel forhover correct diybutton-1",
			textdata: data.string.p1text23
		},
		{
			textclass: "buttonsel forhover diybutton-2",
			textdata: data.string.p1text31
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "background",
				imgid : 'zoo',
				imgsrc: ""
			},
			{
				imgclass: "asha-1",
				imgid : 'asha01',
				imgsrc: ""
			},
			{
				imgclass: "mala-1",
				imgid : 'mala01',
				imgsrc: ""
			},
			{
				imgclass: "animal leopard",
				imgid : 'leopard',
				imgsrc: ""
			},
			{
				imgclass: "background cage",
				imgid : 'cage',
				imgsrc: ""
			}
		]
	}],
	speechbox:[
		{
			speechbox: 'sp-1 thisflip',
			textclass: "answer",
			textdata: data.string.p1text10,
			imgclass: '',
			imgid : 'tb-1',
			imgsrc: '',
		},
		{
			speechbox: 'sp-2 thisflip',
			textclass: "answer1",
			textdata: data.string.p1text11,
			imgclass: 'flipped',
			imgid : 'tb-1',
			imgsrc: '',
		}
	]
},
// slide1
{
singletext:[
	{
		textclass: "buttonsel forhover correct diybutton-1",
		textdata: data.string.p1text16
	},
	{
		textclass: "buttonsel forhover diybutton-2",
		textdata: data.string.p1text17
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha02',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala02',
			imgsrc: ""
		},
		{
			imgclass: "animal leopard",
			imgid : 'leopard',
			imgsrc: ""
		},
		{
			imgclass: "background cage",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}],
speechbox:[
	{
		speechbox: 'sp-1 thisflip',
		textclass: "answer",
		textdata: data.string.p1text32,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	},
	{
		speechbox: 'sp-2 thisflip',
		textclass: "answer1",
		textdata: data.string.p1text15,
		imgclass: 'flipped',
		imgid : 'tb-1',
		imgsrc: '',
	}
]
},
// slide2
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha03',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala03',
			imgsrc: ""
		},
		{
			imgclass: "food",
			imgid : 'meat',
			imgsrc: ""
		},
		{
			imgclass: "animal leopard",
			imgid : 'leopard',
			imgsrc: ""
		},
		{
			imgclass: "background cage",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}],
speechbox:[
	{
		speechbox: 'sp-1 thisflip',
		textclass: "answer",
		textdata: data.string.p1text33,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}
]
},
//leopard ends
	//fox start
	// slide0
	{
	singletext:[
		{
			textclass: "buttonsel forhover diybutton-1",
			textdata: data.string.p1text20
		},
		{
			textclass: "buttonsel forhover correct diybutton-2",
			textdata: data.string.p1text34
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "background",
				imgid : 'zoo',
				imgsrc: ""
			},
			{
				imgclass: "asha-1",
				imgid : 'asha04',
				imgsrc: ""
			},
			{
				imgclass: "mala-1",
				imgid : 'mala04',
				imgsrc: ""
			},
			{
				imgclass: "animal fox",
				imgid : 'fox',
				imgsrc: ""
			},
			{
				imgclass: "background cage",
				imgid : 'cage',
				imgsrc: ""
			}
		]
	}],
	speechbox:[
		{
			speechbox: 'sp-1 thisflip',
			textclass: "answer",
			textdata: data.string.p1text10,
			imgclass: '',
			imgid : 'tb-1',
			imgsrc: '',
		},
		{
			speechbox: 'sp-2 thisflip',
			textclass: "answer1",
			textdata: data.string.p1text11,
			imgclass: 'flipped',
			imgid : 'tb-1',
			imgsrc: '',
		}
	]
},
// slide1
{
singletext:[
	{
		textclass: "buttonsel forhover correct diybutton-1",
		textdata: data.string.p1text16
	},
	{
		textclass: "buttonsel forhover diybutton-2",
		textdata: data.string.p1text17
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha05',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala05',
			imgsrc: ""
		},
		{
			imgclass: "animal fox",
			imgid : 'fox',
			imgsrc: ""
		},
		{
			imgclass: "background cage",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}],
speechbox:[
	{
		speechbox: 'sp-1 thisflip',
		textclass: "answer",
		textdata: data.string.p1text35,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	},
	{
		speechbox: 'sp-2 thisflip',
		textclass: "answer1",
		textdata: data.string.p1text15,
		imgclass: 'flipped',
		imgid : 'tb-1',
		imgsrc: '',
	}
]
},
// slide2
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha02',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala02',
			imgsrc: ""
		},
		{
			imgclass: "food",
			imgid : 'meat',
			imgsrc: ""
		},
		{
			imgclass: "animal fox",
			imgid : 'fox',
			imgsrc: ""
		},
		{
			imgclass: "background cage",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}],
speechbox:[
	{
		speechbox: 'sp-1 thisflip',
		textclass: "answer",
		textdata: data.string.p1text36,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}
]
},
//fox ends
	//deer start
	// slide0
	{
	singletext:[
		{
			textclass: "buttonsel forhover diybutton-1",
			textdata: data.string.p1text37
		},
		{
			textclass: "buttonsel forhover correct diybutton-2",
			textdata: data.string.p1text31
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "background",
				imgid : 'zoo',
				imgsrc: ""
			},
			{
				imgclass: "asha-1",
				imgid : 'asha01',
				imgsrc: ""
			},
			{
				imgclass: "mala-1",
				imgid : 'mala01',
				imgsrc: ""
			},
			{
				imgclass: "animal deer",
				imgid : 'deer',
				imgsrc: ""
			},
			{
				imgclass: "background cage",
				imgid : 'cage',
				imgsrc: ""
			}
		]
	}],
	speechbox:[
		{
			speechbox: 'sp-1 thisflip',
			textclass: "answer",
			textdata: data.string.p1text10,
			imgclass: '',
			imgid : 'tb-1',
			imgsrc: '',
		},
		{
			speechbox: 'sp-2 thisflip',
			textclass: "answer1",
			textdata: data.string.p1text11,
			imgclass: 'flipped',
			imgid : 'tb-1',
			imgsrc: '',
		}
	]
},
// slide1
{
singletext:[
	{
		textclass: "buttonsel forhover diybutton-1",
		textdata: data.string.p1text16
	},
	{
		textclass: "buttonsel forhover correct diybutton-2",
		textdata: data.string.p1text17
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha05',
			imgsrc: "p1text29"
		},
		{
			imgclass: "mala-1",
			imgid : 'mala05',
			imgsrc: ""
		},
		{
			imgclass: "animal deer",
			imgid : 'deer',
			imgsrc: ""
		},
		{
			imgclass: "background cage",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}],
speechbox:[
	{
		speechbox: 'sp-1 thisflip',
		textclass: "answer",
		textdata: data.string.p1text38,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	},
	{
		speechbox: 'sp-2 thisflip',
		textclass: "answer1",
		textdata: data.string.p1text15,
		imgclass: 'flipped',
		imgid : 'tb-1',
		imgsrc: '',
	}
]
},
// slide2
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha03',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala03',
			imgsrc: ""
		},
		{
			imgclass: "food",
			imgid : 'grass',
			imgsrc: ""
		},
		{
			imgclass: "animal deer",
			imgid : 'deer',
			imgsrc: ""
		},
		{
			imgclass: "background cage",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}],
speechbox:[
	{
		speechbox: 'sp-1 thisflip',
		textclass: "answer",
		textdata: data.string.p1text39,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}
]
},
//deer ends
	//crocodile start
	// slide0
	{
	singletext:[
		{
			textclass: "buttonsel forhover correct diybutton-1",
			textdata: data.string.p1text40
		},
		{
			textclass: "buttonsel forhover diybutton-2",
			textdata: data.string.p1text41
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "background",
				imgid : 'zoo',
				imgsrc: ""
			},
			{
				imgclass: "asha-1",
				imgid : 'asha04',
				imgsrc: ""
			},
			{
				imgclass: "mala-1",
				imgid : 'mala04',
				imgsrc: ""
			},
			{
				imgclass: "animal crocodile",
				imgid : 'crocodile',
				imgsrc: ""
			},
			{
				imgclass: "background cage",
				imgid : 'cage',
				imgsrc: ""
			}
		]
	}],
	speechbox:[
		{
			speechbox: 'sp-1 thisflip',
			textclass: "answer",
			textdata: data.string.p1text10,
			imgclass: '',
			imgid : 'tb-1',
			imgsrc: '',
		},
		{
			speechbox: 'sp-2 thisflip',
			textclass: "answer1",
			textdata: data.string.p1text11,
			imgclass: 'flipped',
			imgid : 'tb-1',
			imgsrc: '',
		}
	]
},
// slide1
{
singletext:[
	{
		textclass: "buttonsel forhover correct diybutton-1",
		textdata: data.string.p1text16
	},
	{
		textclass: "buttonsel forhover diybutton-2",
		textdata: data.string.p1text17
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha01',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala01',
			imgsrc: ""
		},
		{
			imgclass: "animal crocodile",
			imgid : 'crocodile',
			imgsrc: ""
		},
		{
			imgclass: "background cage",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}],
speechbox:[
	{
		speechbox: 'sp-1 thisflip',
		textclass: "answer",
		textdata: data.string.p1text42,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	},
	{
		speechbox: 'sp-2 thisflip',
		textclass: "answer1",
		textdata: data.string.p1text15,
		imgclass: 'flipped',
		imgid : 'tb-1',
		imgsrc: '',
	}
]
},
// slide2
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha05',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala05',
			imgsrc: ""
		},
		{
			imgclass: "food",
			imgid : 'meat',
			imgsrc: ""
		},
		{
			imgclass: "animal crocodile",
			imgid : 'crocodile',
			imgsrc: ""
		},
		{
			imgclass: "background cage",
			imgid : 'cage',
			imgsrc: ""
		}
	]
}],
speechbox:[
	{
		speechbox: 'sp-1 thisflip',
		textclass: "answer",
		textdata: data.string.p1text43,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}
]
},
//crocodile ends
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "zoo", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cage", src: imgpath+"iron_bar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha01", src: imgpath+"asha04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha02", src: imgpath+"asha05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha03", src: imgpath+"asha08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha04", src: imgpath+"asha09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha05", src: imgpath+"asha10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala01", src: imgpath+"mala03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala02", src: imgpath+"mala04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala03", src: imgpath+"mala08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala04", src: imgpath+"mala07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala05", src: imgpath+"mala10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deer", src: imgpath+"deer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "leopard", src: imgpath+"lepord.png", type: createjs.AbstractLoader.IMAGE},
			{id: "leopardeat", src: imgpath+"lepord_eating.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fox", src: imgpath+"fox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "foxeat", src: imgpath+"fox_eating.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "meat", src: imgpath+"meat01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grass", src: imgpath+"grass01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deereat", src: imgpath+"deer_eating.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "crocodile", src: imgpath+"crock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crocodileeat", src: imgpath+"crock_eating.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1_1", src: soundAsset+"s3_p1_1.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2_1", src: soundAsset+"s3_p2_1.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4_1", src: soundAsset+"s3_p4_1.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5_1", src: soundAsset+"s3_p5_1.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_7_1", src: soundAsset+"s3_p7_1.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_8_1", src: soundAsset+"s3_p8_1.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p10.ogg"},
			{id: "sound_10_1", src: soundAsset+"s3_p10_1.ogg"},
			{id: "sound_11", src: soundAsset+"s3_p11.ogg"},
			{id: "sound_11_1", src: soundAsset+"s3_p11_1.ogg"},
			{id: "sound_12", src: soundAsset+"s3_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		var replacer;

		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		itsa = `sound_${countNext+1}_1`;
		if(countNext==2||countNext==5||countNext==8||countNext==11){
			sound_player12(`sound_${countNext+1}`, 1);
		}
		else{
		sound_player12(`sound_${countNext+1}`, 0);
	}
		switch(countNext){

			//leopard start
			case 0:
				replacer = data.string.p1text51;

			break;
			case 1:
				replacer = data.string.p1text46;

			break;
			case 2:
				setTimeout(function(){
					$(".leopard").attr("src", preload.getResult("leopardeat").src);
				}, 2000);
			break;
			//leopard end
			//fox start
			case 3:
				replacer = data.string.p1text52;

			break;
			case 4:
				replacer = data.string.p1text46;

			break;
			case 5:
				setTimeout(function(){
					$(".fox").attr("src", preload.getResult("foxeat").src);
				}, 2000);
			break;
			//fox end
			//deer start
			case 6:
				replacer = data.string.p1text53;

			break;
			case 7:
				replacer = data.string.p1text48;

			break;
			case 8:
				$(".food").css({"left":"65.4%"});
				setTimeout(function(){
					$(".deer").attr("src", preload.getResult("deereat").src);
				}, 2000);
			break;
			//deer end
			//deer start
			case 9:
				replacer = data.string.p1text54;

			break;
			case 10:
				replacer = data.string.p1text46;

			break;
			case 11:
				setTimeout(function(){
					$(".crocodile").attr("src", preload.getResult("crocodileeat").src);
				}, 2000);
			break;
			//deer end
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("forhover")){
				$(this).removeClass('forhover');
					if($(this).hasClass("correct")){
						play_correct_incorrect_sound(1);
						$('.speechbg.flipped,.answer1').animate({"opacity":"1"},600,"linear");
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
						$(this).css("color","white");
						$(".sp-2 > p").html(replacer);
						appender($(this),'corrimg');
						$('.buttonsel').removeClass('forhover forhoverimg');
						// navigationcontroller();
						sound_player(itsa, 1);
					}
					else{
						play_correct_incorrect_sound(0);
						appender($(this),'incorrimg');
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						// $(this).siblings(".wrngopt").show(0);
					}
			}

			function appender($this, icon){
				if($this.hasClass("diybutton-1"))
					$(".coverboardfull").append("<img class='icon-one' src= '"+ preload.getResult(icon).src +"'>");
				else
					$(".coverboardfull").append("<img class='icon-two' src= '"+ preload.getResult(icon).src +"'>");
				}
			});
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function sound_player12(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".sp-2").show(0);

			$("[class*='diybutton']").show(0);

			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	$refreshBtn.click(function(){
		templatecaller();
	});
});

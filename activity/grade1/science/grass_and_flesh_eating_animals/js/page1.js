var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		singletext:[
			{
				textclass: "cvTxt",
				textdata: data.lesson.chapter
			}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "background",
				imgid : 'cover_page',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'zoo',
			imgsrc: ""
		},
		{
			imgclass: "asha-1",
			imgid : 'asha06',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-1 thisflip',
	textclass: "answer",
	textdata: data.string.p1text1,
	imgclass: '',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide2
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'asha09',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala03',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer1",
	textdata: data.string.p1text2,
	imgclass: 'flipped',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide3
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'asha12',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala07',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-3 thisflip',
	textclass: "answer",
	textdata: data.string.p1text3,
	imgclass: '',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide4
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'asha09',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala08',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer1",
	textdata: data.string.p1text4,
	imgclass: 'flipped',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide5
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'asha06',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala02',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer",
	textdata: data.string.p1text5,
	imgclass: 'flipped',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide6
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'asha12',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala07',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer1",
	textdata: data.string.p1text6,
	imgclass: 'flipped',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide7
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'asha06',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala07',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-3 thisflip',
	textclass: "answer",
	textdata: data.string.p1text7,
	imgclass: '',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide8
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'asha09',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala03',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer1",
	textdata: data.string.p1text8,
	imgclass: 'flipped',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
},
// slide9
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "background",
			imgid : 'bg02',
			imgsrc: ""
		},
		{
			imgclass: "asha-2",
			imgid : 'asha06',
			imgsrc: ""
		},
		{
			imgclass: "mala-1",
			imgid : 'mala02',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer",
	textdata: data.string.p1text9,
	imgclass: 'flipped',
	imgid : 'tb-1',
	imgsrc: '',
	// audioicon: true,
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover_page", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zoo", src: imgpath+"zoo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha01", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha02", src: imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha03", src: imgpath+"asha03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha04", src: imgpath+"asha04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha05", src: imgpath+"asha05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha06", src: imgpath+"asha06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha07", src: imgpath+"asha07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha08", src: imgpath+"asha08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha09", src: imgpath+"asha09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha10", src: imgpath+"asha10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha11", src: imgpath+"asha11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha12", src: imgpath+"asha12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala01", src: imgpath+"mala01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala02", src: imgpath+"mala02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala03", src: imgpath+"mala03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala04", src: imgpath+"mala04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala05", src: imgpath+"mala05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala06", src: imgpath+"mala06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala07", src: imgpath+"mala07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala08", src: imgpath+"mala08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala09", src: imgpath+"mala09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala10", src: imgpath+"mala10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1_2", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_1_3", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_1_4", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_1_5", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_1_6", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_1_7", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_1_8", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_1_9", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_1_10", src: soundAsset+"s1_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		sound_player("sound_1_"+(countNext+1), 1);
		switch(countNext){
			case 3:
				$(".mala-1").css({"left":"57%"});
			break;
			case 6:
				$(".mala-1").css({"left":"57%"});
				break;
			case 7:
				$(".sp-3").css({"left":"30%"});
				$(".mala-1").css({"left":"57%"});
				break;
			case 20:
			case 22:
			break;
			default:
			break;
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("forhover")){
				$(this).removeClass('forhover');
					if($(this).hasClass("correct")){
						play_correct_incorrect_sound(1);
						$('.answer1').css({"opacity":"1"});
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
						$(this).css("color","white");
						// $(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						appender($(this),'corrimg');
						$('.buttonsel').removeClass('forhover forhoverimg');
						navigationcontroller();
					}
					else{
						play_correct_incorrect_sound(0);
						appender($(this),'incorrimg');
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						// $(this).siblings(".wrngopt").show(0);
					}
			}

			function appender($this, icon){
				if($this.hasClass("diybutton-1"))
					$(".coverboardfull").append("<img class='icon-one' src= '"+ preload.getResult(icon).src +"'>");
				else
					$(".coverboardfull").append("<img class='icon-two' src= '"+ preload.getResult(icon).src +"'>");
				}
			});

			$(".buttonsel2").click(function(){
				console.log("lksjfl");
				if($(this).hasClass("forhover2")){
					$(this).removeClass('forhover2');
						if($(this).hasClass("correct")){
							play_correct_incorrect_sound(1);
							$('.answer1').css({"opacity":"1"});

							$(this).css("background","#bed62f");
							$(this).css("border","5px solid #deef3c");
							$(this).css("color","white");
							// $(this).siblings(".corctopt").show(0);
							//$('.hint_image').show(0);
							appender($(this),'corrimg');
							$('.buttonsel2').removeClass('forhover2 forhoverimg');
							navigationcontroller();
						}
						else{
							play_correct_incorrect_sound(0);
							appender($(this),'incorrimg');
							$(this).css("background","#FF0000");
							$(this).css("border","5px solid #980000");
							$(this).css("color","white");
							// $(this).siblings(".wrngopt").show(0);
						}
				}

				function appender($this, icon){
					if($this.hasClass("newdiybtn-1"))
						$(".coverboardfull").append("<img class='icon2-one' src= '"+ preload.getResult(icon).src +"'>");
					else
						$(".coverboardfull").append("<img class='icon2-two' src= '"+ preload.getResult(icon).src +"'>");
					}
				});
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			nav_button_controls(300);
		});
	}

	var Scount = 0;
	function chain_fade(sound_id, lastcount){
		console.log(Scount);
		$(".fademe"+Scount).fadeIn();
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			// countNums(lastcount);
			if(Scount < lastcount){
				Scount++;
				chain_fade("sound_1_"+Scount, lastcount);
			}
			else{
				navigationcontroller();
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

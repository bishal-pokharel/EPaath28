var imgpath = $ref + "/images/";
var soundAsset = $ref + "/audio_" + $lang + "/";

var content = [
  {
    // slide0
    contentblockadditionalclass: "bg_blue",
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "chapter_title",
        textdata: data.string.title
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "coverpage",
            imgclass: "bg_full"
          }
        ]
      }
    ]
  },

  {
    // slide1
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text brown_bg",
        textdata: data.string.title
      },
      {
        textclass: "bigplant",
        textdata: data.string.p1_s4
      },
      {
        textclass: "smallplant",
        textdata: data.string.p1_s5
      },
      {
        textclass: "verysmallplant",
        textdata: data.string.p1_s22
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "bg",
            imgclass: "bg_full"
          },
          {
            imgclass: "left_plant",
            imgid: "mango"
          },
          {
            imgclass: "mid_plant",
            imgid: "rose_plant"
          },
          {
            imgclass: "right_plant",
            imgid: "small_plant"
          }
        ]
      }
    ]
  },

  {
    // slide2
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text brown_bg",
        textdata: data.string.p1_s0,
        datahighlightflag: true,
        datahighlightcustomclass: "blue"
      },
      {
        textclass: "bigplant",
        textdata: data.string.p1_s4
      },
      {
        textclass: "smallplant",
        textdata: data.string.p1_s5
      },
      {
        textclass: "verysmallplant",
        textdata: data.string.p1_s22
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "bg",
            imgclass: "bg_full"
          },
          {
            imgclass: "left_plant",
            imgid: "mango"
          },
          {
            imgclass: "mid_plant",
            imgid: "rose_plant"
          },
          {
            imgclass: "right_plant",
            imgid: "small_plant"
          }
        ]
      }
    ]
  },

  {
    // slide3
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text brown_bg",
        textdata: data.string.p1_s2,
        datahighlightflag: true,
        datahighlightcustomclass: "blue"
      },
      {
        textclass: "first_type_text",
        textdata: data.string.p1_s10
      },
      {
        textclass: "second_type_text",
        textdata: data.string.p1_s11
      },
      {
        textclass: "third_type_text",
        textdata: data.string.p1_s12
      },
      {
        textclass: "fourth_type_text",
        textdata: data.string.p1_s13
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "bg01",
            imgclass: "bg_full"
          },
          {
            imgclass: "first_type",
            imgid: "mango"
          },
          {
            imgclass: "second_type",
            imgid: "peepal_tree"
          },
          {
            imgclass: "third_type",
            imgid: "litchi"
          },
          {
            imgclass: "fourth_type",
            imgid: "pine-tree"
          }
        ]
      }
    ]
  },

  {
    // slide4
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text brown_bg",
        textdata: data.string.p1_s23,
        datahighlightflag: true,
        datahighlightcustomclass: "blue"
      },
      {
        textclass: "bigplant",
        textdata: data.string.p1_s4
      },
      {
        textclass: "smallplant",
        textdata: data.string.p1_s5
      },
      {
        textclass: "verysmallplant",
        textdata: data.string.p1_s22
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "bg",
            imgclass: "bg_full"
          },
          {
            imgclass: "left_plant",
            imgid: "mango"
          },
          {
            imgclass: "mid_plant",
            imgid: "rose_plant"
          },
          {
            imgclass: "right_plant",
            imgid: "small_plant"
          }
        ]
      }
    ]
  },

  {
    // slide5
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text brown_bg",
        textdata: data.string.p1_s6,
        datahighlightflag: true,
        datahighlightcustomclass: "blue"
      },
      {
        textclass: "first_type_text",
        textdata: data.string.p1_s14
      },
      {
        textclass: "second_type_text",
        textdata: data.string.p1_s15
      },
      {
        textclass: "third_type_text",
        textdata: data.string.p1_s16
      },
      {
        textclass: "fourth_type_text",
        textdata: data.string.p1_s17
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "bg01",
            imgclass: "bg_full"
          },
          {
            imgclass: "first_type",
            imgid: "rose_plant"
          },
          {
            imgclass: "second_type",
            imgid: "tulip02"
          },
          {
            imgclass: "third_type",
            imgid: "sugarcane01"
          },
          {
            imgclass: "fourth_type lemon",
            imgid: "lemon"
          }
        ]
      }
    ]
  },

  {
    // slide6
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text brown_bg",
        textdata: data.string.p1_s24,
        datahighlightflag: true,
        datahighlightcustomclass: "blue"
      },
      {
        textclass: "bigplant",
        textdata: data.string.p1_s4
      },
      {
        textclass: "smallplant",
        textdata: data.string.p1_s5
      },
      {
        textclass: "verysmallplant",
        textdata: data.string.p1_s22
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "bg",
            imgclass: "bg_full"
          },
          {
            imgclass: "left_plant",
            imgid: "mango"
          },
          {
            imgclass: "mid_plant",
            imgid: "rose_plant"
          },
          {
            imgclass: "right_plant",
            imgid: "small_plant"
          }
        ]
      }
    ]
  },

  {
    // slide7
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text brown_bg",
        textdata: data.string.p1_s9,
        datahighlightflag: true,
        datahighlightcustomclass: "blue"
      },
      {
        textclass: "first_type_text",
        textdata: data.string.p1_s18
      },
      {
        textclass: "second_type_text",
        textdata: data.string.p1_s19
      },
      {
        textclass: "third_type_text",
        textdata: data.string.p1_s20
      },
      {
        textclass: "fourth_type_text",
        textdata: data.string.p1_s21
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "bg01",
            imgclass: "bg_full"
          },
          {
            imgclass: "first_type",
            imgid: "pea_plant"
          },
          {
            imgclass: "second_type",
            imgid: "mustard_plant"
          },
          {
            imgclass: "third_type",
            imgid: "wheat"
          },
          {
            imgclass: "fourth_type",
            imgid: "mint"
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  loadTimelineProgress($total_page, countNext + 1);

  //for preload
  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      {
        id: "bg",
        src: imgpath + "bg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg01",
        src: imgpath + "bg01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "coverpage",
        src: imgpath + "coverpage.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "mango",
        src: imgpath + "mango.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "peepal_tree",
        src: imgpath + "peepal_tree.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "litchi",
        src: imgpath + "litchi.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "pine-tree",
        src: imgpath + "pine-tree.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rose_plant",
        src: imgpath + "rose_plant.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "sugarcane01",
        src: imgpath + "sugarcane01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "tulip02",
        src: imgpath + "jasmine.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "lemon",
        src: imgpath + "lemon_tree.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "small_plant",
        src: imgpath + "small_plant.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "pea_plant",
        src: imgpath + "pea_plant.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "mustard_plant",
        src: imgpath + "mustard_plant.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "wheat",
        src: imgpath + "wheat.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "mint",
        src: imgpath + "mint.png",
        type: createjs.AbstractLoader.IMAGE
      },

      //sounds
      { id: "ding", src: soundAsset + "ding.ogg" },
      { id: "herb", src: soundAsset + "Click on the herb.ogg" },
      { id: "shrub", src: soundAsset + "Click on the shrub.ogg" },
      { id: "tree", src: soundAsset + "Click on the tree.ogg" },
      { id: "p1_s0", src: soundAsset + "p1_s0.ogg" },
      { id: "p1_s1_1", src: soundAsset + "p1_s1_1.ogg" },
      { id: "p1_s1_2", src: soundAsset + "p1_s1_2.ogg" },
      { id: "p1_s1_3", src: soundAsset + "p1_s1_3.ogg" },
      { id: "p1_s2_1", src: soundAsset + "p1_s2_1.ogg" },
      { id: "p1_s2_2", src: soundAsset + "p1_s2_2.ogg" },
      { id: "p1_s3_1", src: soundAsset + "p1_s3_1.ogg" },
      { id: "p1_s3_2", src: soundAsset + "p1_s3_2.ogg" },
      { id: "p1_s3_3", src: soundAsset + "p1_s3_3.ogg" },
      { id: "p1_s3_4", src: soundAsset + "p1_s3_4.ogg" },
      { id: "p1_s3_5", src: soundAsset + "p1_s3_5.ogg" },
      { id: "p1_s4_1", src: soundAsset + "p1_s4_1.ogg" },
      { id: "p1_s4_2", src: soundAsset + "p1_s4_2.ogg" },
      { id: "p1_s5_1", src: soundAsset + "p1_s5_1.ogg" },
      { id: "p1_s5_2", src: soundAsset + "p1_s5_2.ogg" },
      { id: "p1_s5_3", src: soundAsset + "p1_s5_3.ogg" },
      { id: "p1_s5_4", src: soundAsset + "p1_s5_4.ogg" },
      { id: "p1_s5_5", src: soundAsset + "p1_s5_5.ogg" },
      { id: "p1_s6_1", src: soundAsset + "p1_s6_1.ogg" },
      { id: "p1_s6_2", src: soundAsset + "p1_s6_2.ogg" },
      { id: "p1_s7_1", src: soundAsset + "p1_s7_1.ogg" },
      { id: "p1_s7_2", src: soundAsset + "p1_s7_2.ogg" },
      { id: "p1_s7_3", src: soundAsset + "p1_s7_3.ogg" },
      { id: "p1_s7_4", src: soundAsset + "p1_s7_4.ogg" },
      { id: "p1_s7_5", src: soundAsset + "p1_s7_5.ogg" }

      // {id: "p1_s1", src: soundAsset+"p1_s1.ogg"},
      // {id: "p1_s2_0", src: soundAsset+"p1_s2_0.ogg"},
      // {id: "p1_s2_1", src: soundAsset+"p1_s2_1.ogg"},
      // {id: "p1_s2_2", src: soundAsset+"p1_s2_2.ogg"},
      // {id: "p1_s2_3", src: soundAsset+"p1_s2_3.ogg"},
      // {id: "p1_s5", src: soundAsset+"p1_s5.ogg"},
      // {id: "p1_s6", src: soundAsset+"p1_s6.ogg"},
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
=            Handlers and helpers Block            =
==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
     =            data highlight function            =
     ===============================================*/
  /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }

  /*=====  End of data highlight function  ======*/

  /*===============================================
     =            user notification function        =
     ===============================================*/
  /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

  /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }
  /*=====  End of user notification function  ======*/

  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
  /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }
  /*=====  End of user navigation controller function  ======*/

  /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
  /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

  /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }
  /*=====  End of InstructionBlockController  ======*/

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var intervalcontroller;
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    put_image(content, countNext);
    vocabcontroller.findwords(countNext);
    // splitintofractions($(".fractionblock"));
    // sound_player("ding");
    switch (countNext) {
      case 0:
        sound_player("p1_s0");
        break;
      case 1:
        $(".bigplant,.smallplant,.verysmallplant").hide(0);
        $(".bigplant").fadeIn(500);
        createjs.Sound.stop();
        var current_sound1 = createjs.Sound.play("p1_s1_1");
        $(".left_plant").addClass("pop_up");
        current_sound1.on("complete", function() {
          setTimeout(function() {
            $(".smallplant").fadeIn(500);
            var current_sound2 = createjs.Sound.play("p1_s1_2");
            $(".mid_plant").addClass("pop_up");
            current_sound2.on("complete", function() {
              setTimeout(function() {
                $(".verysmallplant").fadeIn(500);
                var current_sound3 = createjs.Sound.play("p1_s1_3");
                $(".right_plant").addClass("pop_up");
                current_sound3.on("complete", function() {
                  nav_button_controls(100);
                });
              }, 500);
            });
          }, 500);
        });

        break;

      case 2:
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play("p1_s2_1");
        current_sound.on("complete", function() {
          click_function(
            "left_plant",
            "bigplant",
            data.string.p1_s1,
            ".mid_plant,.smallplant,.right_plant,.verysmallplant"
          );
        });
        break;
      case 3:
        $(
          ".first_type_text, .second_type_text, .third_type_text, .fourth_type_text,.first_type, .second_type, .third_type, .fourth_type"
        ).hide(0);
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play("p1_s3_1");
        current_sound.on("complete", function() {
          $(".first_type_text,.first_type").fadeIn(500);
          var current_sound1 = createjs.Sound.play("p1_s3_2");
          current_sound1.on("complete", function() {
            $(".second_type_text,.second_type").fadeIn(500);
            var current_sound2 = createjs.Sound.play("p1_s3_3");
            current_sound2.on("complete", function() {
              $(".third_type_text,.third_type").fadeIn(500);
              var current_sound3 = createjs.Sound.play("p1_s3_4");
              current_sound3.on("complete", function() {
                var current_sound4 = createjs.Sound.play("p1_s3_5");
                $(".fourth_type_text,.fourth_type").fadeIn(500);
                nav_button_controls(100);
              });
            });
          });
        });
        break;
      case 5:
      case 7:
        $(
          ".first_type_text, .second_type_text, .third_type_text, .fourth_type_text,.first_type, .second_type, .third_type, .fourth_type"
        ).hide(0);
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play("p1_s" + countNext + "_1");
        $(".first_type, .second_type, .third_type, .fourth_type").css(
          "width",
          "38%"
        );
        current_sound.on("complete", function() {
          setTimeout(function() {
            $(".first_type_text,.first_type").fadeIn(500);
            var current_sound1 = createjs.Sound.play("p1_s" + countNext + "_2");
            current_sound1.on("complete", function() {
              setTimeout(function() {
                $(".second_type_text,.second_type").fadeIn(500);
                var current_sound2 = createjs.Sound.play(
                  "p1_s" + countNext + "_3"
                );
                current_sound2.on("complete", function() {
                  setTimeout(function() {
                    $(".third_type_text,.third_type").fadeIn(500);
                    var current_sound3 = createjs.Sound.play(
                      "p1_s" + countNext + "_4"
                    );
                    current_sound3.on("complete", function() {
                      setTimeout(function() {
                        var current_sound4 = createjs.Sound.play(
                          "p1_s" + countNext + "_5"
                        );
                        $(".fourth_type_text,.fourth_type").fadeIn(500);
                        nav_button_controls(100);
                      }, 2000);
                    });
                  }, 2000);
                });
              }, 2000);
            });
          }, 2000);
        });
        break;
      case 4:
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play("p1_s4_1");
        current_sound.on("complete", function() {
          click_function(
            "mid_plant",
            "smallplant",
            data.string.p1_s3,
            ".left_plant,.bigplant,.right_plant,.verysmallplant"
          );
        });
        break;
      case 6:
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play("p1_s6_1");
        current_sound.on("complete", function() {
          click_function(
            "right_plant",
            "verysmallplant",
            data.string.p1_s7,
            ".mid_plant,.smallplant,.left_plant,.bigplant"
          );
        });
        break;
      default:
        nav_button_controls(100);
        break;
    }

    function click_function(tree_class, clickclass, text, classes_to_hide) {
      // $(classes_to_hide).animate({'opacity':'.4'},500);
      $("." + clickclass + ",." + tree_class)
        .css("cursor", "pointer")
        .addClass("pop_up");
      $("." + tree_class).mouseenter(function() {
        $(this)
          .removeClass("pop_up")
          .css({
            transform: "translate(-50%,-50%) scale(1.1)",
            transition: ".2s"
          });
      });
      $("." + tree_class).mouseleave(function() {
        $(this).css({
          transform: "translate(-50%,-50%) scale(1)",
          transition: ".2s"
        });
      });
      //  $('.top_text').addClass('rolldown');
      $("." + clickclass + ",." + tree_class).click(function() {
        $("." + clickclass + ",." + tree_class).css("pointer-events", "none");
        $(".top_text")
          .removeClass("rolldown pop_up")
          .addClass("rollup");
        setTimeout(function() {
          $(".top_text").html(text);
          $(".top_text")
            .removeClass("rollup")
            .addClass("rolldown");
        }, 500);
        createjs.Sound.stop();
        var current_sound1 = createjs.Sound.play("p1_s" + countNext + "_2");
        current_sound1.on("complete", function() {
          nav_button_controls();
        });
        return 1;
      });
    }
  }

  /*=====  End of Templates Block  ======*/
  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.lessonEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id, next) {
    createjs.Sound.stop();
    var current_sound = createjs.Sound.play(sound_id);
    current_sound.on("complete", function() {
      if (next == null) navigationcontroller();
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      for (var j = 0; j < content[count].imageblock.length; j++) {
        var imageblock = content[count].imageblock[j];
        console.log("imageblock", imageblock);
        if (imageblock.hasOwnProperty("imagestoshow")) {
          var imageClass = imageblock.imagestoshow;
          for (var i = 0; i < imageClass.length; i++) {
            var image_src = preload.getResult(imageClass[i].imgid).src;
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = "." + classes_list[classes_list.length - 1];
            $(selector).attr("src", image_src);
          }
        }
      }
    }

    if (content[count].hasOwnProperty("containsdialog")) {
      for (var j = 0; j < content[count].containsdialog.length; j++) {
        var containsdialog = content[count].containsdialog[j];
        console.log("imageblock", imageblock);
        if (containsdialog.hasOwnProperty("dialogueimgid")) {
          var image_src = preload.getResult(containsdialog.dialogueimgid).src;
          //get list of classes
          var classes_list =
            containsdialog.dialogimageclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  /*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller() {
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page, countNext + 1);
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    clearInterval(intervalcontroller);
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    clearInterval(intervalcontroller);
    countNext--;
    templateCaller();

    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
  /*=====  End of Templates Controller Block  ======*/
});

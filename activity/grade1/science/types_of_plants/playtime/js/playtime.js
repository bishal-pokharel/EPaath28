var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var soundAsset1 = $ref+"/audio_"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.playtime
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "rhinoimgdiv",
                    imgid: "rhinodance",
                },
                {
                    imgclass: "squirrelisteningimg",
                    imgid: "squirrel",
                },
                {
                    imgclass: "bg_full",
                    imgid: "bg_playtime",
                }]
        }]
	},

	{
		// slide1
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "center_text",
			textdata: data.string.pt_s1
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: "bg",
                }]
        }]
	},

	{
		// slide2
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "top_text",
			textdata: data.string.pt_s2
		},{
			textclass: "option_text_1",
			textdata: data.string.pt_s19
		},{
			textclass: "option_text_2",
			textdata: data.string.pt_s18
		},{
			textclass: "option_text_3",
			textdata: data.string.pt_s11
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: "plain_bg",
                },{
                    imgclass: "option_1 correct",
                    imgid: "mango",
                },{
                    imgclass: "option_2",
                    imgid: "pea_plant",
                },{
                    imgclass: "option_3",
                    imgid: "tulip02",
                },{
                    imgclass: "left_girl",
                    imgid: "girltalkingg",
                }]
        }]
	},

	{
		// slide3
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "top_text",
			textdata: data.string.pt_s20
		},{
			textclass: "option_text_1",
			textdata: data.string.pt_s3
		},{
			textclass: "option_text_2",
			textdata: data.string.pt_s14
		},{
			textclass: "option_text_3",
			textdata: data.string.pt_s12
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: "plain_bg",
                },{
                    imgclass: "option_1 ",
                    imgid: "apple_tree",
                },{
                    imgclass: "option_2 ",
                    imgid: "wheat",
                },{
                    imgclass: "option_3 correct",
                    imgid: "rose_plant",
                },{
                    imgclass: "left_girl",
                    imgid: "girl",
                }]
        }]
	},

	{
		// slide4
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "top_text",
			textdata: data.string.pt_s21
		},{
			textclass: "option_text_1",
			textdata: data.string.pt_s3
		},{
			textclass: "option_text_2",
			textdata: data.string.pt_s14
		},{
			textclass: "option_text_3",
			textdata: data.string.pt_s22
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: "plain_bg",
                },{
                    imgclass: "option_1 ",
                    imgid: "apple_tree",
                },{
                    imgclass: "option_2 correct",
                    imgid: "wheat",
                },{
                    imgclass: "option_3 ",
                    imgid: "lemon_tree",
                },{
                    imgclass: "left_girl",
                    imgid: "girl",
                }]
        }]
	},

	{
		// slide5
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "top_text",
			textdata: data.string.pt_s2
		},{
			textclass: "option_text_1",
			textdata: data.string.pt_s23
		},{
			textclass: "option_text_2",
			textdata: data.string.pt_s24
		},{
			textclass: "option_text_3",
			textdata: data.string.pt_s12
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: "plain_bg",
                },{
                    imgclass: "option_1 correct",
                    imgid: "litchi",
                },{
                    imgclass: "option_2 ",
                    imgid: "mint",
                },{
                    imgclass: "option_3 ",
                    imgid: "rose_plant",
                },{
                    imgclass: "left_girl",
                    imgid: "girl",
                }]
        }]
	},

	{
		// slide6
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "top_text",
			textdata: data.string.pt_s21
		},{
			textclass: "option_text_1",
			textdata: data.string.pt_s19
		},{
			textclass: "option_text_2",
			textdata: data.string.pt_s18
		},{
			textclass: "option_text_3",
			textdata: data.string.pt_s12
		}],
				imageblock:[{
						imagestoshow: [
								{
										imgclass: "bg_full",
										imgid: "plain_bg",
								},{
										imgclass: "option_1 ",
										imgid: "mango",
								},{
										imgclass: "option_2 correct",
										imgid: "pea_plant",
								},{
										imgclass: "option_3 ",
										imgid: "rose_plant",
								},{
										imgclass: "left_girl",
										imgid: "girl",
								}]
				}]
	},

	{
		// slide7
			contentnocenteradjust: true,
			containsdialog:[{
				dialogcontainer:'dialogue1',
				dialogueimgid:'dialoguebox',
				dialogcontent:data.string.pt_s15,
				dialogimageclass:'dialoguebox1 flip',
				dialogcontentclass:'text_inside'
			},
			{
				dialogcontainer:'dialogue2',
				dialogueimgid:'dialoguebox',
				dialogcontent:data.string.pt_s16,
				dialogimageclass:'dialoguebox2 ',
				dialogcontentclass:'text_inside'
			}],
				imageblock:[{
						imagestoshow: [
								{
										imgclass: "bg_full",
										imgid: "plain_bg",
								},{
										imgclass: "char_1 ",
										imgid: "girl1",
								},{
										imgclass: "char_2",
										imgid: "anjana",
								}]
				}]
	},

	{
		// slide6
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "tree_text",
			textdata: data.string.pt_s5
		},{
			textclass: "shurb_text",
			textdata: data.string.pt_s6
		},{
			textclass: "herb_text",
			textdata: data.string.pt_s7
		}],
				imageblock:[{
						imagestoshow: [
								{
										imgclass: "bg_full",
										imgid: "plain_bg",
								},{
										imgclass: "tree ",
										imgid: "mango",
								},{
										imgclass: "shurb",
										imgid: "rose_plant",
								},{
										imgclass: "herb ",
										imgid: "wheat",
								},{
										imgclass: "left_girl1",
										imgid: "girl1",
								}]
				}]
	},
	{
		contentnocenteradjust: true,
		imageblock:[{
				imagestoshow: [
						{
								imgclass: "bg_full",
								imgid: "plain_bg",
						},{
								imgclass: "tree ",
								imgid: "mango",
						},{
								imgclass: "shurb",
								imgid: "rose_plant",
						},{
								imgclass: "herb ",
								imgid: "wheat",
						}]
		}]
	}

];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;



	var message =  data.string.pt_s17;
	var endpageex =  new EndPageofExercise();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			 {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
       {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			 {id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "incorrect", src: imgpath+"wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "bg_playtime", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "plain_bg", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "mango", src: imgpath+"mango.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "apple_tree", src: imgpath+"apple_tree.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "rose_plant", src: imgpath+"rose_plant.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "pea_plant", src: imgpath+"pea_plant.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "wheat", src: imgpath+"wheat.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "tulip02", src: imgpath+"jasmine.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "girl", src: imgpath+"girlwalking.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "walking_girl", src: imgpath+"walking_girl.gif", type: createjs.AbstractLoader.IMAGE},
			 {id: "lemon_tree", src: imgpath+"lemon_tree.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "litchi", src: imgpath+"litchi.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "mint", src: imgpath+"mint.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "girl1", src: imgpath+"girl1.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "anjana", src: imgpath+"anjana.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "dialoguebox", src: imgpath+"dialoguebox.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "girltalkingg", src: imgpath+"girltalkingg.gif", type: createjs.AbstractLoader.IMAGE},
			 {id: "mothertalking", src: imgpath+"mothertalking.gif", type: createjs.AbstractLoader.IMAGE},
			 {id: "gilrtalking", src: imgpath+"gilrtalking.gif", type: createjs.AbstractLoader.IMAGE},

         //sounds
				 	{id: "correct", src: soundAsset+"correct.ogg"},
          {id: "incorrect", src: soundAsset+"incorrect.ogg"},
          {id: "p1_s1", src: soundAsset+"play_1.ogg"},
          {id: "p8_1", src: soundAsset+"exe_8.ogg"},
          {id: "p8_2", src: soundAsset+"play_8_2.ogg"},
          {id: "tree_click", src: soundAsset+"play_clcik_tree.ogg"},
          {id: "herb_click", src: soundAsset+"play_click_herb.ogg"},
          {id: "shrub_click", src: soundAsset+"play_click_shrub.ogg"},
          {id: "playtime", src: soundAsset+"playtime.ogg"},
			{id: "tree", src: soundAsset+"tree.ogg"},
			{id: "shurb", src: soundAsset+"shurb.ogg"},
			{id: "herb", src: soundAsset+"herb.ogg"},
			{id: "hurray", src: soundAsset+"hurray.ogg"},
			// {id: "slide2-2", src: soundAsset+"p2_s0.ogg"},
			// {id: "slide2-3", src: soundAsset+"p2_s1.ogg"},

					// {id: "slide4-1", src: soundAsset+"correct.ogg"},
					// {id: "slide4-2", src: soundAsset+"p2_s0.ogg"},
					// {id: "slide4-3", src: soundAsset+"ding.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(endLesson){

      if (countNext == 0 && $total_page != 1) {
          console.log("Hello this is");
          $nextBtn.show(0);
          $prevBtn.css('display', 'none');
      }
      else if ($total_page == 1) {
          $prevBtn.css('display', 'none');
          $nextBtn.css('display', 'none');

          ole.footerNotificationHandler.lessonEndSetNotification();
      }
      else if (countNext > 0 && countNext < $total_page - 1) {
          $nextBtn.show(0);
          $prevBtn.show(0);
      }
      else if (countNext == $total_page - 1) {
          $nextBtn.css('display', 'none');
          $prevBtn.show(0);

          // if lastpageflag is true
          ole.footerNotificationHandler.pageEndSetNotification();
          endLesson?ole.footerNotificationHandler.lessonEndSetNotification():'';

      }
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var intervalcontroller;

 function shuffeloptions($img){
	 	$prevBtn.hide(0);
		var randidx = 0;
		var count = 1;
		while($img.length > 0){
			randidx = Math.floor(Math.random() * $img.length);
			$($img[randidx]).addClass("pos"+ count);
			$img.splice(randidx, 1);
			count++;
		}
 }
 var answered = false;
 function clicklistener($img, correct){
 	answered = false;
 	var $this = null;
 	var arraylist;
 	var $optionscontainer = $(".optionscontainer");
 	for (var i = 0; i < $img.length; i++){
 		$this = $($img[i]);
 		arraylist = $this.attr('class').split(/\s+/);
 		if ($this.is('#'+correct)){
	 		$optionscontainer.append("<img class='"+ arraylist[1]+"_sign' src= '"+ preload.getResult('correct').src +"'/>");
 		} else {
	 		$optionscontainer.append("<img class='"+ arraylist[1]+"_sign' src= '"+ preload.getResult('incorrect').src +"'/>");
 		}
 	}

 	$img.click(function(){
 		if(answered)
 			return answered;
 		var $this = $(this);
 		arraylist = $this.attr('class').split(/\s+/);
 		$("."+arraylist[1]+"_sign").show(0);
 		if($this.is('#'+correct)){
 			$img.addClass('disable');
 			$this.addClass("correct");
 			play_correct_incorrect_sound(true);
 			$("#Layer_1 > #"+correct).fadeIn(600);
 			answered = true;
 			$nextBtn.show(0);
 		}else{
 			$this.addClass("incorrect");
 			play_correct_incorrect_sound(false);
 		}

 	});
 }
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
firstPagePlayTime(countNext);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);

			var pos_array=['pos1','pos2','pos3'];
			pos_array.shufflearray();
			for (var i = 1; i < 4; i++) {
				$('.option_'+i).addClass(pos_array[i-1]);
				$('.option_text_'+i).addClass(pos_array[i-1]);
				// hover_sound('slide'+countNext+'-',i);
			}
			function hover_sound(audio_letter,option_number){
		 	 $('.option_'+option_number).mouseenter(function(){
		 		 createjs.Sound.play(audio_letter+option_number);
				 $(this).mouseleave(function(){
					 createjs.Sound.stop();
				 });
		 	 });
		  }
			$('.option_1, .option_2, .option_3').click(function(){
				if($(this).hasClass('correct')){
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$('.types_of_plants').width()+'%';
					var centerY = (100-((position.top + height)*100)/$('.types_of_plants').height())+'%';
					$('.option_1, .option_2, .option_3').css('pointer-events','none');
					if($(this).hasClass('option_2')||$(this).hasClass('option_3')){
						$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;width:4%;transform:translate(13%,-568%)" src="'+imgpath +'correct.png" />').insertAfter(this);
					}
					else{
						$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;width:4%;transform:translate(13%,-848%)" src="'+imgpath +'correct.png" />').insertAfter(this);
					}
					girl_walks();
					play_correct_incorrect_sound(1);
					// nav_button_controls(100);
					function girl_walks(){
						$('.left_girl').attr('src',preload.getResult('walking_girl').src);
						$('.left_girl').animate({"left":"115%"},8000,function(){
							$nextBtn.show(0);
						});
					}
				}
				else{
					$(this).css({"pointer-events":"none"});
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$('.types_of_plants').width()+'%';
					var centerY = (100-((position.sound_playertop + height)*100)/$('.types_of_plants').height())+'%';
					if($(this).hasClass('option_2')||$(this).hasClass('option_3')){
						$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;width:4%;transform:translate(13%,-568%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
					}
					else{
						$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;width:4%;transform:translate(13%,-848%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
					}
					play_correct_incorrect_sound(0);
				}
			});

		// splitintofractions($(".fractionblock"));
		switch (countNext) {
			// case 0:
			// sound_player("playtime",true);
			// break;
			case 1:
			sound_player("p1_s1",true);
			break;
			case 2:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play('tree_click');
			current_sound.play();
			current_sound.on('complete', function () {
				$('.left_girl').attr('src',preload.getResult('girl').src);
			});
			break;
			case 3:
			$('.left_girl').attr('src',preload.getResult('girltalkingg').src);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play('shrub_click');
			current_sound.play();
			current_sound.on('complete', function () {
				$('.left_girl').attr('src',preload.getResult('girl').src);
			});
			break;
			case 4:
			$('.left_girl').attr('src',preload.getResult('girltalkingg').src);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play('herb_click');
			current_sound.play();
			current_sound.on('complete', function () {
				$('.left_girl').attr('src',preload.getResult('girl').src);
			});
			break;
			case 5:
			$('.left_girl').attr('src',preload.getResult('girltalkingg').src);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play('tree_click');
			current_sound.play();
			current_sound.on('complete', function () {
				$('.left_girl').attr('src',preload.getResult('girl').src);
			});
			break;
			case 6:
			$('.left_girl').attr('src',preload.getResult('girltalkingg').src);
			createjs.Sound.stop();
				current_sound = createjs.Sound.play('herb_click');
				current_sound.play();
				current_sound.on('complete', function () {
					$('.left_girl').attr('src',preload.getResult('girl').src);
				});
			break;
			case 6:
			$('.left_girl').attr('src',preload.getResult('girltalkingg').src);
			createjs.Sound.stop();
				current_sound = createjs.Sound.play('herb_click');
				current_sound.play();
				current_sound.on('complete', function () {
					$('.left_girl').attr('src',preload.getResult('girl').src);
				});
			break;
			case 7:
			$('.char_1').attr('src',preload.getResult('gilrtalking').src);
				createjs.Sound.stop();
				var current_sound1 = createjs.Sound.play('p8_1');
				current_sound1.play();
				current_sound1.on('complete', function () {
					$('.char_1').attr('src',preload.getResult('girl1').src);
					$('.char_2').attr('src',preload.getResult('mothertalking').src);
					var current_sound2 = createjs.Sound.play('p8_2');
					setTimeout(function(){
					current_sound2.play();},700);
					current_sound2.on('complete', function () {
						console.log("ehy");
						$('.char_2').attr('src',preload.getResult('anjana').src);
					});
				});
				setTimeout(()=>navigationcontroller(),5800);
			break;
			case 8:
				$('.tree,.tree_text,.shurb_text,.shurb,.herb,.herb_text').hide(0);
				createjs.Sound.stop();
					$('.tree_text').fadeIn(200,function(){
						var current_sound1 = createjs.Sound.play('tree');
						current_sound1.on("complete", function(){
						$('.tree').fadeIn(500,function(){
							$('.shurb_text').fadeIn(200,function(){
								var current_sound2 = createjs.Sound.play('shurb');
								current_sound2.on("complete", function(){
									$('.shurb').fadeIn(500,function(){
										$('.herb_text').fadeIn(200,function(){
											var current_sound3 = createjs.Sound.play('herb');
											current_sound3.on("complete", function(){
												$('.herb').fadeIn(500);
												$nextBtn.show(0);
											});
										});
									});
								});
							});
						});
					});
				});
				break;
				case 9:
				// ole.footerNotificationHandler.hideNotification();
				createjs.Sound.play('hurray');
				endpageex.endpage(message);
				$nextBtn.hide(0);
				$prevBtn.hide(0);
				break;


		}

  }

/*=====  End of Templates Block  ======*/

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():"";
        });
    }

    function play_sound_sequence(soundarray, navflag){
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                setTimeout(function(){
                    play_sound_sequence(soundarray, navflag);
                },1000)
            }else{
                if(navflag)
                    navigationcontroller(countNext,$total_page);
            }

        });
    }
function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        // templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			clearInterval(intervalcontroller);
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearInterval(intervalcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/playtime/";
var imgpath1 = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[
	{
		// slide0
		contentblockadditionalclass: "bluebg",
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "playtime_text",
			textdata: data.string.eplay
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: "bgimage",
                },
                {
                    imgclass: "elephant",
                    imgid: "witht_kids",
                }]
        }]
	},

	{
		// slide1
		contentblockadditionalclass: "bluebg",
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "bottom_text",
			textdata: data.string.eplay1
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: "jungle",
                }]
        }]
	},

	{
		// slide2
			contentnocenteradjust: true,
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: "jungle",
                },
								{
										imgclass: "kid1",
										imgid: "kid1",
								},
								{
										imgclass: "elephant_without_kid",
										imgid: "without_kid",
								}]
        }],
				containsdialog: [{
					dialogcontainer: "dialog2 fadein_dialogue",
					dialogimageclass: "dialog_image1 ",
					dialogueimgid: "funky_dialog",
					dialogcontentclass: "textfill",
					dialogcontent: data.string.eplay11
				}],
	},

	{
		// slide3
			contentnocenteradjust: true,
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full blur",
                    imgid: "jungle",
                },
								{
										imgclass: "model_image",
										imgid: "without_kid",
								},
								{
										imgclass: "arrow1_elephant",
										imgid: "arrow",
								}]
        }],
				uppertextblock:[{
					textclass: "top_question",
					textdata: data.string.eplay2
				}],
				question_slide_div:[{
					textclass: "option correct",
					textdata: data.string.eplay3
				},
				{
					textclass: "option",
					textdata: data.string.eplay4
				},
				{
					textclass: "option",
					textdata: data.string.eplay5
				},
				{
					textclass: "option",
					textdata: data.string.eplay6
				}]
	},

	{
		// slide4
			contentnocenteradjust: true,
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "bg_full blur",
                    imgid: "jungle",
                },
								{
										imgclass: "model_image",
										imgid: "without_kid",
								},
								{
										imgclass: "arrow2_elephant",
										imgid: "arrow",
								}]
        }],
				uppertextblock:[{
					textclass: "top_question",
					textdata: data.string.eplay2
				}],
				question_slide_div:[{
					textclass: "option correct",
					textdata: data.string.eplay6
				},
				{
					textclass: "option",
					textdata: data.string.eplay4
				},
				{
					textclass: "option",
					textdata: data.string.eplay5
				},
				{
					textclass: "option",
					textdata: data.string.eplay3
				}]
	},


		{
			// slide5
				contentnocenteradjust: true,
	        imageblock:[{
	            imagestoshow: [
	                {
	                    imgclass: "bg_full blur",
	                    imgid: "jungle",
	                },
									{
											imgclass: "model_image",
											imgid: "without_kid",
									},
									{
											imgclass: "arrow3_elephant",
											imgid: "arrow",
									}]
	        }],
					uppertextblock:[{
						textclass: "top_question",
						textdata: data.string.eplay2
					}],
					question_slide_div:[{
						textclass: "option correct",
						textdata: data.string.eplay4
					},
					{
						textclass: "option",
						textdata: data.string.eplay6
					},
					{
						textclass: "option",
						textdata: data.string.eplay5
					},
					{
						textclass: "option",
						textdata: data.string.eplay3
					}]
		},

		{
			// slide6
				contentnocenteradjust: true,
				contentblockadditionalclass:'moving_bg',
	        imageblock:[{
	            imagestoshow: [

									{
											imgclass: "animate_elephant",
											imgid: "elephnatwalk",
									},{
											imgclass: "animate_tiger",
											imgid: "bush_tiger",
									}]
	        }],
					containsdialog: [{
						dialogcontainer: "dialog3",
						dialogimageclass: "dialog_image1 ",
						dialogueimgid: "funky_dialog",
						dialogcontentclass: "textfill",
						dialogcontent: data.string.eplay12
					}],
			},



				{
					// slide7
						contentnocenteradjust: true,
			        imageblock:[{
			            imagestoshow: [
			                {
			                    imgclass: "bg_full blur",
			                    imgid: "jungle",
			                },
											{
													imgclass: "tiger_images",
													imgid: "tiger",
											},
											{
													imgclass: "arrow_tiger",
													imgid: "arrow",
											}]
			        }],
							uppertextblock:[{
								textclass: "top_question",
								textdata: data.string.eplay2
							}],
							question_slide_div:[{
								textclass: "option correct",
								textdata: data.string.eplay5
							},
							{
								textclass: "option",
								textdata: data.string.eplay6
							},
							{
								textclass: "option",
								textdata: data.string.eplay4
							},
							{
								textclass: "option",
								textdata: data.string.eplay3
							}]
				},

				{
					// slide8
						contentnocenteradjust: true,
						contentblockadditionalclass:'moving_bg2',
			        imageblock:[{
			            imagestoshow: [

											{
													imgclass: "animate_elephant",
													imgid: "elephnatwalk",
											},
											{
													imgclass: "eagle_fly",
													imgid: "anim_eagle",
											}]
			        }],
							containsdialog: [{
								dialogcontainer: "dialog4",
								dialogimageclass: "dialog_image1 ",
								dialogueimgid: "funky_dialog",
								dialogcontentclass: "textfill",
								dialogcontent: data.string.eplay13
							}],
				},

				{
					// slide9
						contentnocenteradjust: true,
						contentblockadditionalclass:'bg_blue',
			        imageblock:[{
			            imagestoshow: [
											{
													imgclass: "model_image",
													imgid: "eagle",
											},
											{
													imgclass: "arrow1_eagle",
													imgid: "arrow",
											}]
			        }],
							uppertextblock:[{
								textclass: "top_question",
								textdata: data.string.eplay2
							}],
							question_slide_div:[{
								textclass: "option correct",
								textdata: data.string.eplay7
							},
							{
								textclass: "option",
								textdata: data.string.eplay8
							},
							{
								textclass: "option",
								textdata: data.string.eplay9
							},
							{
								textclass: "option",
								textdata: data.string.eplay3
							}]
				},

				{// slide10
					contentnocenteradjust: true,
					contentblockadditionalclass:'bg_blue',
						imageblock:[{
								imagestoshow: [
										{
												imgclass: "model_image",
												imgid: "eagle",
										},
										{
												imgclass: "arrow2_eagle",
												imgid: "arrow",
										}]
						}],
						uppertextblock:[{
							textclass: "top_question",
							textdata: data.string.eplay2
						}],
						question_slide_div:[{
							textclass: "option correct",
							textdata: data.string.eplay8
						},
						{
							textclass: "option",
							textdata: data.string.eplay7
						},
						{
							textclass: "option",
							textdata: data.string.eplay9
						},
						{
							textclass: "option",
							textdata: data.string.eplay3
						}]
			},

			{// slide11
				contentnocenteradjust: true,
				contentblockadditionalclass:'bg_blue',
					imageblock:[{
							imagestoshow: [
									{
											imgclass: "model_image",
											imgid: "eagle",
									},
									{
											imgclass: "arrow3_eagle",
											imgid: "arrow",
									}]
					}],
					uppertextblock:[{
						textclass: "top_question",
						textdata: data.string.eplay2
					}],
					question_slide_div:[{
						textclass: "option correct",
						textdata: data.string.eplay9
					},
					{
						textclass: "option",
						textdata: data.string.eplay7
					},
					{
						textclass: "option",
						textdata: data.string.eplay8
					},
					{
						textclass: "option",
						textdata: data.string.eplay3
					}]
		},

		{// slide12
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "bg_full blur",
									imgid: "jungle",
							},
							]
			}],
	},

];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;
	var message =  data.string.eplay10;
	var endpageex =  new EndPageofExercise();
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bgimage", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "jungle", src: imgpath+"jungle.png", type: createjs.AbstractLoader.IMAGE},
       {id: "elephnatwalk", src: imgpath+"elephnatwalk.gif", type: createjs.AbstractLoader.IMAGE},
			 {id: "tiger", src: imgpath1+"tiger.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "bush_tiger", src: imgpath+"tiger.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "eagle", src: imgpath1+"eagle.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "kid1", src: imgpath+"wow-04.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "without_kid", src: imgpath+"without_kid.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "arrow", src: imgpath1+"arrow2.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "witht_kids", src: imgpath+"witht_kids.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "witht_kids_point", src: imgpath+"boy_pointing_bird.png", type: createjs.AbstractLoader.IMAGE},
			 {id: "anim_eagle", src: imgpath+"eagle01.gif", type: createjs.AbstractLoader.IMAGE},
			 {id: "funky_dialog", src: imgpath+"funky_dialog.png", type: createjs.AbstractLoader.IMAGE},

         //sounds
          {id: "playtime", src: soundAsset+"playtime.ogg"},
					{id: "correct", src: soundAsset+"correct.ogg"},
					{id: "incorrect", src: soundAsset+"incorrect.ogg"},
					{id: "sound_1", src: soundAsset+"playtime.ogg"},
					{id: "sound_2", src: soundAsset+"pt2.ogg"},
					{id: "sound_3", src: soundAsset+"pt3.ogg"},
					{id: "sound_4", src: soundAsset+"pt4.ogg"},
					{id: "sound_5", src: soundAsset+"pt5.ogg"},
					{id: "sound_6", src: soundAsset+"pt6.ogg"},
					{id: "whatthis", src: soundAsset+"whatthis.ogg"},
					{id: "junglesound", src: soundAsset+"junglesound.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var intervalcontroller;

 function shuffeloptions($img){
	 	$prevBtn.hide(0);
		var randidx = 0;
		var count = 1;
		while($img.length > 0){
			randidx = Math.floor(Math.random() * $img.length);
			$($img[randidx]).addClass("pos"+ count);
			$img.splice(randidx, 1);
			count++;
		}
 }
 var answered = false;
 function clicklistener($img, correct){
 	answered = false;
 	$img.click(function(){
 		if(answered)
 			return answered;
 		var $this = $(this);
 		if($this.is('#'+correct)){
 			$img.addClass('disable');
 			$this.addClass("correct");
 			play_correct_incorrect_sound(true);
 			$("#Layer_1 > #"+correct).fadeIn(600);
 			answered = true;
 			$nextBtn.show(0);
 		}else{
 			$this.addClass("incorrect");
 			play_correct_incorrect_sound(false);
 		}

 	});
 }
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
firstPagePlayTime(countNext);
			vocabcontroller.findwords(countNext);
	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
		// splitintofractions($(".fractionblock"));
		var pos_array=['pos1','pos2','pos3','pos4'];
		pos_array.shufflearray();
		for (var i = 0; i < 4; i++) {
			$('.option').eq(i).removeClass('pos4 pos1 pos2 pos3').addClass(pos_array[i]);
		}

		$('.option').click(function(){
			if($(this).hasClass('correct')){
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.slide_div ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.slide_div ').height()+'%';
				$('<img class="right_icon" style="left:'+centerX+';top:'+centerY+';position:absolute;width:15%;transform:translate(246%,-120%)" src="'+imgpath1 +'correct.png" />').insertAfter(this);
				$(this).addClass('right');
				$('.option').css('pointer-events','none');
				createjs.Sound.play('correct');
				$('.hint1').fadeIn(1000);
				$('.cross_button').show(200);
				nav_button_controls(100);
			}
			else{
				$(this).css('pointer-events','none');
				$(this).addClass('wrong');
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.slide_div ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.slide_div ').height()+'%';
				$('<img class="wrong_icon" style="left:'+centerX+';top:'+centerY+';position:absolute;width:15%;transform:translate(246%,-120%)" src="'+imgpath1 +'incorrect.png" />').insertAfter(this);
				createjs.Sound.play('incorrect');
				$('.hint1').fadeIn(1000);
			}
	});
	    switch(countNext){
				case 0:
					playtime_audio();
                    $nextBtn.show();
				break;
				case 1:
				createjs.Sound.stop();
			 	var current_sound = createjs.Sound.play('sound_2');
				current_sound.on("complete", function(){
					createjs.Sound.play('junglesound');
					$nextBtn.show();
				});
				break;

				case 2:
				$('.dialog2').hide(0);
				$('.dialog2').show(300);
				sound_player('sound_3');
				createjs.Sound.play('junglesound');
				break;
				case 3:
				slide_animator('arrow1_elephant','animate_arrow_elephant');
				break;
				case 4:
				slide_animator('arrow2_elephant','animate_arrow_elephant');
				break;
				case 5:
				slide_animator('arrow3_elephant','animate_arrow2_elephant');
				break;
				case 6:
				createjs.Sound.stop();
				$('.dialog3').hide(0);
				createjs.Sound.play('junglesound');
				setTimeout(function(){
					$('.animate_elephant').attr('src',preload.getResult('witht_kids').src);
					$('.dialog3').show(300);
					createjs.Sound.play('sound_4');
					$nextBtn.show(0);
				},15000);
				break;
				case 7:
				slide_animator('arrow_tiger','animate_arrow2_elephant');
				break;
				case 8:
				createjs.Sound.play('junglesound');
				setTimeout(function(){
					$('.eagle_fly').addClass('eagle_move');
				},9500);
				$('.dialog4').hide(0);
				setTimeout(function(){
					$('.dialog4').show(350);
					$('.animate_elephant').attr('src',preload.getResult('witht_kids_point').src);
					createjs.Sound.play('sound_5');
					$nextBtn.show(0);
				},15000);
				break;
				case 9:
				slide_animator('arrow1_eagle','animate_arrow1_eagle');
				break;
				case 10:
				slide_animator('arrow2_eagle','animate_arrow2_elephant');
				break;
				case 11:
				slide_animator('arrow3_eagle','animate_arrow2_elephant');
				break;
				case 12:
				ole.footerNotificationHandler.hideNotification();
				sound_player('sound_6');
				createjs.Sound.play('junglesound');
				endpageex.endpage(message);
				$nextBtn.hide(0);
				$prevBtn.hide(0);
				break;
	    	default:
				$nextBtn.show();
	    		break;
	   }

		 function slide_animator(arrowclass,animatorclass){
			 createjs.Sound.stop();
			 createjs.Sound.play('junglesound');
	 		var current_sound3 = createjs.Sound.play('whatthis');
	 		current_sound3.play();
	 		current_sound3.on("complete", function(){
	 			$('.slide_div').addClass('animateslide');
				$('.'+arrowclass).addClass(animatorclass);
	 		});
		 }
  }

/*=====  End of Templates Block  ======*/

function sound_player(sound_id){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		if($total_page > (countNext+1)){
			$nextBtn.show(0);
		}else{
			// ole.footerNotificationHandler.pageEndSetNotification();
		}
	});
}

function sound_player1(sound_id){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
}
function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}
function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			clearInterval(intervalcontroller);
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearInterval(intervalcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

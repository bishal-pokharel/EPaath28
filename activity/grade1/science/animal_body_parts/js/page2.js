var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[

	// slide0
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textclass:'middle_text',
			textdata:data.string.p2text1
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textclass:'name_tag',
			textdata:data.string.p2text2
		},
		{
			textclass:'tag_1',
			textdata:data.string.p2text3
		},{
			textclass:'tag_2',
			textdata:data.string.p2text4
		},{
			textclass:'tag_3',
			textdata:data.string.p2text5
		},{
			textclass:'tag_4',
			textdata:data.string.p2text6
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'cat',
				imgclass:'middle_image'
			},{
				imgid:'arrow',
				imgclass:'arrow1'
			},{
				imgid:'arrow',
				imgclass:'arrow2'
			},{
				imgid:'arrow',
				imgclass:'arrow3'
			},{
				imgid:'arrow',
				imgclass:'arrow4'
			},
		]
		}]
	},


	// slide2
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textclass:'name_tag',
			textdata:data.string.p2text9
		},
		{
			textclass:'tag_1_rabbit',
			textdata:data.string.p2text7
		},{
			textclass:'tag_2_rabbit',
			textdata:data.string.p2text8
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'rabbit',
				imgclass:'middle_image'
			},{
				imgid:'arrow',
				imgclass:'arrow1_rabbit'
			},{
				imgid:'arrow',
				imgclass:'arrow2_rabbit'
			}
		]
		}]
	},

	// slide3
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textclass:'name_tag',
			textdata:data.string.p2text10
		},
		{
			textclass:'tag_1_elephant',
			textdata:data.string.p2text11
		},{
			textclass:'tag_2_elephant',
			textdata:data.string.p2text12
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'elephant',
				imgclass:'middle_image'
			},{
				imgid:'arrow',
				imgclass:'arrow1_elephant'
			},{
				imgid:'arrow',
				imgclass:'arrow2_elephant'
			},{
				imgid:'arrow',
				imgclass:'arrow3_elephant'
			}
		]
		}]
	},

	// slide4
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textclass:'name_tag',
			textdata:data.string.p2text15
		},
		{
			textclass:'tag_1_giraffe',
			textdata:data.string.p2text13
		},{
			textclass:'tag_2_giraffe',
			textdata:data.string.p2text14
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'giraffe',
				imgclass:'middle_image'
			},{
				imgid:'arrow',
				imgclass:'arrow1_giraffe'
			},{
				imgid:'arrow',
				imgclass:'arrow2_giraffe'
			},
			{
				imgid:'arrow',
				imgclass:'arrow3_giraffe'
			}
		]
		}]
	},

	// slide5
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textclass:'name_tag',
			textdata:data.string.p2text18
		},
		{
			textclass:'tag_1_buffalo',
			textdata:data.string.p2text16
		},{
			textclass:'tag_2_buffalo',
			textdata:data.string.p2text17
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'buffalo',
				imgclass:'middle_image'
			},{
				imgid:'arrow',
				imgclass:'arrow1_buffalo'
			},{
				imgid:'arrow',
				imgclass:'arrow2_buffalo'
			},{
				imgid:'arrow',
				imgclass:'arrow3_buffalo'
			},{
				imgid:'arrow',
				imgclass:'arrow4_buffalo'
			}
		]
		}]
	},


	// slide6
	{
	  contentblockadditionalclass:'blue_bg',
	  uppertextblock:[{
	    textclass:'name_tag',
	    textdata:data.string.p2text20
	  },
	  {
	    textclass:'tag_1_fish',
	    textdata:data.string.p2text19
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'fish',
	      imgclass:'middle_image'
	    },{
	      imgid:'arrow',
	      imgclass:'arrow1_fish'
	    },{
	      imgid:'arrow',
	      imgclass:'arrow2_fish'
	    }
	  ]
	  }]
	},


	// slide7
	{
	  contentblockadditionalclass:'blue_bg',
	  uppertextblock:[{
	    textclass:'name_tag',
	    textdata:data.string.p2text22
	  },
	  {
	    textclass:'tag_1_pig',
	    textdata:data.string.p2text21
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'pig',
	      imgclass:'middle_image'
	    },{
	      imgid:'arrow',
	      imgclass:'arrow1_pig'
	    }
	  ]
	  }]
	},

	// slide8
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textclass:'name_tag',
			textdata:data.string.p2text26
		},
		{
			textclass:'tag_1_eagle',
			textdata:data.string.p2text24
		},{
			textclass:'tag_2_eagle',
			textdata:data.string.p2text23
		},{
			textclass:'tag_3_eagle',
			textdata:data.string.p2text25
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'eagle',
				imgclass:'middle_image'
			},{
				imgid:'arrow',
				imgclass:'arrow1_eagle'
			},{
				imgid:'arrow',
				imgclass:'arrow2_eagle'
			},{
				imgid:'arrow',
				imgclass:'arrow3_eagle'
			},{
				imgid:'arrow',
				imgclass:'arrow4_eagle'
			}
		]
		}]
	},

	// slide9
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textclass:'name_tag',
			textdata:data.string.p2text29
		},
		{
			textclass:'tag_1_tiger',
			textdata:data.string.p2text27
		},{
			textclass:'tag_2_tiger',
			textdata:data.string.p2text28
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'tiger',
				imgclass:'middle_image'
			},{
				imgid:'arrow',
				imgclass:'arrow1_tiger'
			},{
				imgid:'arrow',
				imgclass:'arrow2_tiger'
			},{
				imgid:'arrow',
				imgclass:'arrow3_tiger'
			}
		]
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cat", src: imgpath+"cat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "buffalo", src: imgpath+"buffalo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rabbit", src: imgpath+"rabbit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "giraffe", src: imgpath+"giraffe.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger", src: imgpath+"tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eagle", src: imgpath+"eagle.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "eye", src: soundAsset+"eye.ogg"},
			{id: "head", src: soundAsset+"head.ogg"},
			{id: "body", src: soundAsset+"body.ogg"},
			{id: "tail", src: soundAsset+"tail.ogg"},
			{id: "fur", src: soundAsset+"fur.ogg"},
			{id: "ear", src: soundAsset+"ear.ogg"},
			{id: "tusk", src: soundAsset+"tusk.ogg"},
			{id: "trunk", src: soundAsset+"trunk.ogg"},
			{id: "neck", src: soundAsset+"neck.ogg"},
			{id: "leg", src: soundAsset+"leg.ogg"},
			{id: "horns", src: soundAsset+"horns.ogg"},
			{id: "hooves", src: soundAsset+"hooves.ogg"},
			{id: "fins", src: soundAsset+"fins.ogg"},
			{id: "snout", src: soundAsset+"snout.ogg"},
			{id: "claw", src: soundAsset+"claw.ogg"},
			{id: "beak", src: soundAsset+"beak.ogg"},
			{id: "wings", src: soundAsset+"wings.ogg"},
			{id: "teeth", src: soundAsset+"teeth.ogg"},
			{id: "paws", src: soundAsset+"paws.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		if(countNext==9){
			$('.middle_image').css('left', '60%');
		}
		switch(countNext) {
			case 0:
				sound_nav('sound_1');
				break;
			case 1:
			$('.arrow1,.arrow2,.arrow3,.arrow4').css('opacity','0');
			$('.arrow1').addClass('animationdown1');
			$('.tag_1').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('eye');
			current_sound1.play();
			setTimeout(function(){

				// current_sound1.on("complete", function(){
					$('.tag_2').addClass('bg_orange');
					$('.arrow2').addClass('animationdown2');
					var current_sound2 = createjs.Sound.play('head');
					current_sound2.play();
					setTimeout(function(){

						// current_sound2.on("complete", function(){
							$('.tag_3').addClass('bg_orange');
							$('.arrow3').addClass('animated fadeInDown');
							var current_sound3 = createjs.Sound.play('body');
							current_sound3.play();
							setTimeout(function(){

										// current_sound3.on("complete", function(){
											$('.tag_4').addClass('bg_orange');
											$('.arrow4').addClass('animationdown4');
											var current_sound4 = createjs.Sound.play('tail');
											current_sound4.play();
											// setTimeout(function(){
														$nextBtn.show(0);
														$prevBtn.show(0);
													// },700);
										// });
									},2000);
						// });
					},1800);
				// });
			},1500);

			break;
			case 2:
			$('.arrow1_rabbit,.arrow2_rabbit').css('opacity','0');
			$('.arrow1_rabbit').addClass('animationdown_1_rabbit');
			$('.tag_1_rabbit').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('fur');
			current_sound1.play();
			setTimeout(function(){

				// current_sound1.on("complete", function(){
					$('.tag_2_rabbit').addClass('bg_orange');
					$('.arrow2_rabbit').addClass('animationdown_2_rabbit');
					var current_sound2 = createjs.Sound.play('ear');
					current_sound2.play();
					// setTimeout(function(){
						current_sound2.on("complete", function(){
														$prevBtn.show(0);
														$nextBtn.show(0);
				});
			},1800);

			break;
			case 3:
			$('.arrow1_elephant,.arrow2_elephant,.arrow3_elephant').css('opacity','0');
			$('.arrow3_elephant').addClass('animationdown_1a_elephant');
			$('.arrow2_elephant').addClass('animationdown_1b_elephant');
			$('.tag_1_elephant').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('tusk');
			current_sound1.play();
			setTimeout(function(){

			  // current_sound1.on("complete", function(){
			    $('.tag_2_elephant').addClass('bg_orange');
			    $('.arrow1_elephant').addClass('animationdown_2_elephant');
			    var current_sound2 = createjs.Sound.play('trunk');
			    current_sound2.play();
			    // setTimeout(function(){
			      current_sound2.on("complete", function(){
														$prevBtn.show(0);
			                      $nextBtn.show(0);
			  });
			},1800);

			break;

			case 4:
			$('.arrow1_giraffe,.arrow2_giraffe,.arrow3_giraffe').css('opacity','0');
			$('.arrow1_giraffe').addClass('animationdown_1_giraffe');
			$('.tag_1_giraffe').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('neck');
			current_sound1.play();
			setTimeout(function(){

			  // current_sound1.on("complete", function(){
			    $('.tag_2_giraffe').addClass('bg_orange');
			    $('.arrow2_giraffe').addClass('animationdown_1_giraffe');
					$('.arrow3_giraffe').addClass('animationdown_51_giraffe');
			    var current_sound2 = createjs.Sound.play('leg');
			    current_sound2.play();
			    // setTimeout(function(){
			      current_sound2.on("complete", function(){
			                      $prevBtn.show(0);
			                      $nextBtn.show(0);
			  });
			},1800);

			break;

			case 5:
			$('.arrow1_buffalo,.arrow2_buffalo,.arrow3_buffalo,.arrow4_buffalo').css('opacity','0');
			$('.arrow1_buffalo').addClass('animationdown_1_buffalo');
			$('.arrow2_buffalo').addClass('animationdown_2_buffalo');
			$('.tag_1_buffalo').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('horns');
			current_sound1.play();
			setTimeout(function(){

			  // current_sound1.on("complete", function(){
			    $('.tag_2_buffalo').addClass('bg_orange');
					$('.arrow3_buffalo').addClass('animationdown_3_buffalo');
			    $('.arrow4_buffalo').addClass('animationdown_4_buffalo');
			    var current_sound2 = createjs.Sound.play('hooves');
			    current_sound2.play();
			    // setTimeout(function(){
			      current_sound2.on("complete", function(){
			                      $prevBtn.show(0);
			                      $nextBtn.show(0);
			  });
			},1800);
			break;

			case 6:
			// $('.arrow1_fish,.arrow2_fish').css('opacity','0');
			$('.arrow1_fish').addClass('animationdown_1_fish');
			$('.arrow2_fish').addClass('animationdown_2_fish');
			$('.tag_1_fish').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('fins');
			current_sound1.play();
			setTimeout(function(){

			  // current_sound1.on("complete", function(){
					$prevBtn.show(0);
					$nextBtn.show(0);
			},1800);
			break;

			case 7:
			// $('.arrow1_pig,.arrow2_pig').css('opacity','0');
			$('.arrow1_pig').addClass('animationdown_1_pig');
			// $('.arrow2_pig').addClass('animationdown_2_pig');
			$('.tag_1_pig').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('snout');
			current_sound1.play();
			setTimeout(function(){

			  // current_sound1.on("complete", function(){
			    $prevBtn.show(0);
			    $nextBtn.show(0);
			},1800);
			break;

			case 8:
			$('.arrow1_eagle,.arrow2_eagle,.arrow3_eagle,.arrow4_eagle').css('opacity','0');
			$('.arrow1_eagle').addClass('animationdown_1_eagle');
			$('.tag_1_eagle').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('claw');
			current_sound1.play();
			setTimeout(function(){

			  // current_sound1.on("complete", function(){
			    $('.tag_2_eagle').addClass('bg_orange');
			    $('.arrow2_eagle').addClass('animationdown_1_eagle');
			    var current_sound2 = createjs.Sound.play('beak');
			    current_sound2.play();
			    setTimeout(function(){
			      // current_sound2.on("complete", function(){
							$('.tag_3_eagle').addClass('bg_orange');
							$('.arrow4_eagle').addClass('animationdown_3_eagle');
						 $('.arrow3_eagle').addClass('animationdown_2_eagle');
						 var current_sound3 = createjs.Sound.play('wings');
						 	current_sound3.play();
						 	current_sound3.on("complete", function(){
			                      $prevBtn.show(0);
			                      $nextBtn.show(0);
													});
			  },2200);
			},1800);
			break;

			case 9:
			$('.arrow1_tiger,.arrow2_tiger,.arrow3_tiger').css('opacity','0');
			$('.arrow1_tiger').addClass('animationdown_1a_tiger');
			$('.arrow2_tiger').addClass('animationdown_1b_tiger');
			$('.tag_1_tiger').addClass('bg_orange');
			var current_sound1 = createjs.Sound.play('paws');
			current_sound1.play();
			setTimeout(function(){

			  // current_sound1.on("complete", function(){
			    $('.tag_2_tiger').addClass('bg_orange');
			    $('.arrow3_tiger').addClass('animationdown_2_tiger');
			    var current_sound2 = createjs.Sound.play('teeth');
			    current_sound2.play();
			    // setTimeout(function(){
			      current_sound2.on("complete", function(){
														nav_button_controls(100);
			  });
			},1800);
			break;
			default:
			nav_button_controls(100);
			break;

	}
}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

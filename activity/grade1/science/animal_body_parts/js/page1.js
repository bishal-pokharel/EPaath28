var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/bodyparts/";
var soundAsset = $ref + "/audio_" + $lang + "/";

var content = [
  // slide0
  {
    uppertextblock: [
      {
        textclass: "cover_text",
        textdata: data.string.p1text16
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "coverpage",
            imgclass: "full_bg"
          }
        ]
      }
    ]
  },

  // slide1
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text1
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "head",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide2
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text2
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "head",
            imgclass: "head_move zoom_in_girl1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "headonly",
            imgclass: "bodyparts"
          }
        ]
      }
    ]
  },

  // slide3
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text3
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "eyes",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide4
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text3a
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "eyes",
            imgclass: "head_move zoom_in_girl1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "eyesonly",
            imgclass: "bodyparts"
          }
        ]
      }
    ]
  },

  // slide5
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text3b
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "teeths",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide6
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text3c
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "teeths",
            imgclass: "head_move zoom_in_girl1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "teethonly",
            imgclass: "bodyparts"
          }
        ]
      }
    ]
  },

  // slide7
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text4
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "ears",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide8
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text5
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "ears",
            imgclass: "head_move zoom_in_girl1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "earsonly",
            imgclass: "bodyparts"
          }
        ]
      }
    ]
  },

  // slide9
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text6
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "hair",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide10
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text7
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "hair",
            imgclass: "head_move zoom_in_girl1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "haironly",
            imgclass: "hairparts"
          }
        ]
      }
    ]
  },

  // slide11
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text8
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "shoulders",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide12
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text9
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "shoulders",
            imgclass: "head_move zoom_in_girl1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "shouldersonly",
            imgclass: "bodyparts"
          }
        ]
      }
    ]
  },

  // slide13
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text10
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "hands",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide14
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text11
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "hands",
            imgclass: "head_move zoom_in_girl1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "handsonly",
            imgclass: "bodyparts"
          }
        ]
      }
    ]
  },

  // slide15
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text12
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "stomach",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide16
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text13
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "stomach",
            imgclass: "head_move zoom_in_girl1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "stomachonly",
            imgclass: "bodyparts"
          }
        ]
      }
    ]
  },

  // slide17
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.p1text14
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "feet",
            imgclass: "head_move"
          },
          {
            imgid: "girl",
            imgclass: "still_girl"
          }
        ]
      }
    ]
  },
  // slide18
  {
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text15
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgid: "feet",
            imgclass: "head_move zoom_in_girl_feet1"
          },
          {
            imgid: "girl",
            imgclass: "still_girl zoom_in_girl_feet"
          },
          {
            imgid: "sound_icon",
            imgclass: "sound_icon"
          },
          {
            imgid: "feetonly",
            imgclass: "bodyparts"
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);
  // readCSV();
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  $(".mainBox > div").prepend('<p class="replay_button"></p>');

  //for preload
  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      {
        id: "girl",
        src: imgpath + "girl.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "coverpage",
        src: imgpath + "coverpage.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "head",
        src: imgpath + "head.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "sound_icon",
        src: imgpath + "sound_icon.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "ears",
        src: imgpath + "ears.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "teeths",
        src: imgpath + "teeths.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "eyes",
        src: imgpath + "eyes.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "hair",
        src: imgpath + "hair.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "shoulders",
        src: imgpath + "shoulders.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "hands",
        src: imgpath + "hands.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "stomach",
        src: imgpath + "stomach.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "feet",
        src: imgpath + "feet.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "headonly",
        src: imgpath1 + "head.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "earsonly",
        src: imgpath1 + "ears.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "eyesonly",
        src: imgpath1 + "eyes.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "feetonly",
        src: imgpath1 + "feet.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "haironly",
        src: imgpath1 + "hair.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "handsonly",
        src: imgpath1 + "hands.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "shouldersonly",
        src: imgpath1 + "shoulders.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "stomachonly",
        src: imgpath1 + "stomach.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "teethonly",
        src: imgpath1 + "teeth.png",
        type: createjs.AbstractLoader.IMAGE
      },
      // sounds
      { id: "sound_1", src: soundAsset + "p1_s1.ogg" },
      { id: "sound_2", src: soundAsset + "p1_s2.ogg" },
      { id: "sound_3", src: soundAsset + "p1_s3.ogg" },
      { id: "sound_4", src: soundAsset + "p1_s4.ogg" },
      { id: "sound_5", src: soundAsset + "p1_s5.ogg" },
      { id: "sound_6", src: soundAsset + "p1_s6.ogg" },
      { id: "sound_7", src: soundAsset + "p1_s7.ogg" },
      { id: "sound_8", src: soundAsset + "p1_s8.ogg" },
      { id: "sound_9", src: soundAsset + "p1_s9.ogg" },
      { id: "sound_10", src: soundAsset + "p1_s10.ogg" },
      { id: "sound_11", src: soundAsset + "p1_s11.ogg" },
      { id: "sound_12", src: soundAsset + "p1_s12.ogg" },
      { id: "sound_13", src: soundAsset + "shoulder.ogg" },
      { id: "sound_14", src: soundAsset + "p1_s13.ogg" },
      { id: "sound_15", src: soundAsset + "p1_s14.ogg" },
      { id: "sound_16", src: soundAsset + "p1_s15.ogg" },
      { id: "sound_17", src: soundAsset + "p1_s16.ogg" },
      { id: "sound_18", src: soundAsset + "p1_s17.ogg" },
      { id: "sound_19", src: soundAsset + "p1_s18.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_speechbox_image(content, countNext);
    $(".replay_button").html(data.string.replay);
    $(".replay_button").hide(0);
    if (countNext % 2 != 0) {
      $(".replay_button").show(0);
    }
    if (countNext == 0) {
      $(".replay_button").hide(0);
    }
    switch (countNext) {
      case 0:
        sound_nav("sound_1");
        break;
      case 1:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_2");
        break;

      case 3:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_4");
        break;

      case 5:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_6");
        break;

      case 7:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_8");
        break;

      case 9:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_10");
        break;

      case 11:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_12");
        break;

      case 13:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_14");
        break;

      case 15:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_16");
        break;

      case 17:
        $(".head_move, .still_girl").css({ left: "50%", top: "50%" });
        move_parts("replay_button", "sound_18");
        break;

      case 2:
        move_parts("sound_icon", "sound_3");
        break;

      case 4:
        move_parts("sound_icon", "sound_5");
        break;

      case 6:
        move_parts("sound_icon", "sound_7");
        break;

      case 8:
        move_parts("sound_icon", "sound_9");
        break;

      case 10:
        move_parts("sound_icon", "sound_11");
        break;

      case 12:
        move_parts("sound_icon", "sound_13");
        break;

      case 14:
        move_parts("sound_icon", "sound_15");
        break;

      case 16:
        move_parts("sound_icon", "sound_17");
        break;

      case 18:
        move_parts("sound_icon", "sound_19");
        break;
    }
    function move_parts(sound_icon_class, sound_class) {
      inside_function();
      $("." + sound_icon_class).click(function() {
        inside_function();
      });
      function inside_function() {
        $("." + sound_icon_class).css("pointer-events", "none");
        $(".head_move").hide(0);
        sound_player(sound_class);
        current_sound.on("complete", function() {
          $(".head_move").show(0);
          $(".still_girl").hide(0);
          if (sound_icon_class == "sound_icon") {
            $("." + sound_icon_class).addClass("zoom_in_out");
          }
          if (sound_icon_class == "replay_button") {
            $("." + sound_icon_class).addClass("focus_button");
          }
          setTimeout(function() {
            $("." + sound_icon_class).css("pointer-events", "auto");
            $(".still_girl").show(0);
            $(".head_move").hide(0);
            nav_button_controls(100);
          }, 2000);
        });
      }
    }
  }
  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }
  function sound_nav(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      nav_button_controls(0);
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }
  function put_speechbox_image(content, count) {
    if (content[count].hasOwnProperty("speechbox")) {
      var speechbox = content[count].speechbox;
      for (var i = 0; i < speechbox.length; i++) {
        var image_src = preload.getResult(speechbox[i].imgid).src;
        //get list of classes
        var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
        var selector =
          "." + classes_list[classes_list.length - 1] + ">.speechbg";
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
    /*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

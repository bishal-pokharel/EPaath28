var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/playtime/";

var content=[
	//slide0
	{
		singletext:[
		{
			textclass: "playtime",
			textdata: data.string.playtime
		}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover',
				imgsrc: ""
			},
			{
				imgclass: "rhino",
				imgid : 'rhino',
				imgsrc: ""
			},
			{
				imgclass: "squirrel",
				imgid : 'squirrel',
				imgsrc: ""
			}
		]
	}]
},
//slide1
{
	contentnocenteradjust : true,
	contentblockadditionalclass : "bg",
	uppertextblock : [{
		textclass : "toptxt",
		textdata : data.string.atop
	}],
	videoblock:[{
		videoid: "tutorial",
		videoclass: "video_player",
		// video_src: imgpath+"tutorial.mp4"
	}]
	// exerciseblock : [{
	// 	imageoptions : [{
	// 		imgclass : " option pc1 path1",
	// 		imgid : 'line3',
	// 		imgsrc : ""
	// 	},{
	// 		imgclass : " option pc2 path2",
	// 		imgid : 'line4',
	// 		imgsrc : ""
	// 	},{
	// 		imgclass : " option pc3 path3",
	// 		imgid : 'line1',
	// 		imgsrc : ""
	// 	},{
	// 		imgclass : " option pc4",
	// 		imgid : 'circle',
	// 		imgsrc : ""
	// 	},{
	// 		imgclass : " option pc5",
	// 		imgid : 'leftcurve',
	// 		imgsrc : ""
	// 	},{
	// 		imgclass : " option pc6",
	// 		imgid : 'rightcurve',
	// 		imgsrc : ""
	// 	},{
	// 		imgclass : " option pc7",
	// 		imgid : 'leftcurve',
	// 		imgsrc : ""
	// 	}]
	// }],
	// svgblock: [{
	// 	svgblock: 'qn-svg',
	// }]
},
//extra
{
	contentnocenteradjust : true,
	contentblockadditionalclass : "bg",
	speechbox:[{
		speechbox: 'sp-1',
		textdata : data.string.box_text,
		textclass : 'txt1',
		imgclass: 'box',
		imgid : 'text_box',
		imgsrc: '',
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: 'asha',
			imgid : 'aasha',
			imgsrc: '',
		}]
	}]
},
//slide2
{
	contentnocenteradjust : true,
	contentblockadditionalclass : "bg",
	uppertextblock : [{
		textclass : "toptxt",
		textdata : data.string.exctxt
	}],
	exerciseblock : [{
		imageoptions : [{
			imgclass : "option pc1 path1",
			imgid : 'line2',
			imgsrc : ""
		},{
			imgclass : "option pc2 path03 path2",
			imgid : 'rightcurve',
			imgsrc : ""
		},{
			imgclass : "option pc3 path02 path3",
			imgid : 'rightcurve',
			imgsrc : ""
		},{
			imgclass : "option pc4",
			imgid : 'circle',
			imgsrc : ""
		},{
			imgclass : "option pc5",
			imgid : 'line1',
			imgsrc : ""
		},{
			imgclass : "option pc6",
			imgid : 'line4',
			imgsrc : ""
		},{
			imgclass : "option pc7",
			imgid : 'line3',
			imgsrc : ""
		}]
	}],
	svgblock: [{
		svgblock: 'qn-svg',
	}]
},
//slide3
{
	contentnocenteradjust : true,
	contentblockadditionalclass : "bg",
	uppertextblock : [{
		textclass : "toptxt",
		textdata : data.string.exctxt
	}],
	exerciseblock : [{
		imageoptions : [{
			imgclass : "option pc1 path1",
			imgid : 'leftcurve',
			imgsrc : ""
		},{
			imgclass : "option pc2 path2",
			imgid : 'line1',
			imgsrc : ""
		},{
			imgclass : "option pc3 path3",
			imgid : 'line2',
			imgsrc : ""
		},{
			imgclass : "option pc4",
			imgid : 'circle',
			imgsrc : ""
		},{
			imgclass : "option pc5",
			imgid : 'line3',
			imgsrc : ""
		},{
			imgclass : "option pc6",
			imgid : 'rightcurve',
			imgsrc : ""
		},{
			imgclass : "option pc7",
			imgid : 'line4',
			imgsrc : ""
		}]
	}],
	svgblock: [{
		svgblock: 'qn-svg',
	}]
},
//slide4
{
	contentnocenteradjust : true,
	contentblockadditionalclass : "bg",
	uppertextblock : [{
		textclass : "toptxt",
		textdata : data.string.exctxt
	}],
	exerciseblock : [{
		imageoptions : [{
			imgclass : "option pc1 path1",
			imgid : 'line2',
			imgsrc : ""
		},{
			imgclass : "option pc2 path2",
			imgid : 'rightcurve',
			imgsrc : ""
		},{
			imgclass : "option pc3 path3",
			imgid : 'leftcurve',
			imgsrc : ""
		},{
			imgclass : "option pc4",
			imgid : 'circle',
			imgsrc : ""
		},{
			imgclass : "option pc5",
			imgid : 'line3',
			imgsrc : ""
		},{
			imgclass : "option pc6",
			imgid : 'line4',
			imgsrc : ""
		},{
			imgclass : "option pc7",
			imgid : 'line1',
			imgsrc : ""
		}]
	}],
	svgblock: [{
		svgblock: 'qn-svg',
	}]
},
//slide5
{
	contentnocenteradjust : true,
	contentblockadditionalclass : "bg",
	uppertextblock : [{
		textclass : "toptxt",
		textdata : data.string.exctxt
	}],
	exerciseblock : [{
		imageoptions : [{
			imgclass : "option pc1 path1",
			imgid : 'line2',
			imgsrc : ""
		},{
			imgclass : "option pc2 path03  path2",
			imgid : 'line1',
			imgsrc : ""
		},{
			imgclass : "option pc3 path02  path3",
			imgid : 'line1',
			imgsrc : ""
		},{
			imgclass : "option pc4 path01  path4",
			imgid : 'line1',
			imgsrc : ""
		},{
			imgclass : "option pc5",
			imgid : 'line3',
			imgsrc : ""
		},{
			imgclass : "option pc6",
			imgid : 'line4',
			imgsrc : ""
		},{
			imgclass : "option pc6",
			imgid : 'rightcurve',
			imgsrc : ""
		}]
	}],
	svgblock: [{
		svgblock: 'qn-svg',
	}]
},
//slide6
{
	contentnocenteradjust : true,
	contentblockadditionalclass : "bg",
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "image l_1",
				imgid : 'line1',
				imgsrc: ""
			},
			{
				imgclass: "image l_2",
				imgid : 'line2',
				imgsrc: ""
			},
			{
				imgclass: "image l_3",
				imgid : 'line3',
				imgsrc: ""
			},{
				imgclass: "image l_4",
				imgid : 'line4',
				imgsrc: ""
			},
			{
				imgclass: "image l_5",
				imgid : 'leftcurve',
				imgsrc: ""
			},
			{
				imgclass: "image l_6",
				imgid : 'rightcurve',
				imgsrc: ""
			},
			{
				imgclass: "image l_7",
				imgid : 'circle',
				imgsrc: ""
			}
		]
	}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var video_object;


	var endpageex =  new EndPageofExercise();
	var message =  "Yay! we just learned making some lines and curves.";

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/right.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asvg", src: imgpath+"letters/A.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "bsvg", src: imgpath+"letters/B.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "csvg", src: imgpath+"C_new.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "dsvg", src: imgpath+"D_01.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "esvg", src: imgpath+"E_01.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "line1", src: imgpath+"line01_n.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line2", src: imgpath+"line02_n.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line3", src: imgpath+"line03_n.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line4", src: imgpath+"line04_n.png", type: createjs.AbstractLoader.IMAGE},
			{id: "leftcurve", src: imgpath+"left-curve_n.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rightcurve", src: imgpath+"right-curve_n.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circle", src: imgpath+"circle_n.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "aasha", src: imgpath+"aasha-talking.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			//video_src
			{id: "tutorial", src: imgpath+"tutorial.ogg", type: createjs.AbstractLoader.VIDEO},
			// sounds
			{id: "sound_1", src: soundAsset+"playtime.ogg"},
			{id: "sound_2", src: soundAsset+"watch_the.ogg"},
			{id: "sound_3", src: soundAsset+"now_it_is.ogg"},
			{id: "sound_4", src: soundAsset+"yay_we_just_learnt.ogg"},
			{id: "sound_5", src: soundAsset+"drag_and_drop_matching_shape.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
		if(event.item.id === "tutorial"){
			video_object = event.item;
		}
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image3(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
							// 	alert("here");
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

    // drag and drop start
		var wrong_dropped = false;
		var correct_count=0;
		var counterfornext=0;
		var svgtoload;

		var svg1;
		var svg2;
		var svg3;
		var svg4;
		var svg5;
		var svg6;
		var svgmain;
		var showanm;
		var count;
		var someflag = -1;
		var pts;

		function svgblock(id){
          var s = Snap('#qn-svg');
          var svg = Snap.load(preload.getResult(id).src, function ( loadedFragment ) {
					s.append(loadedFragment);
					svg1 = $('#path01');
					svg2 = $('#path02');
					svg3 = $('#path03');
					svg8 = $('#path04');
					svg4 = $('#fill01');
					svg5 = $('#fill02');
					svg6 = $('#fill03');
					svg7 = $('#fill04');
					svgmain=$('#fullimg');
					svgmain.css({
						display:'none'
					});
					svg4.on('mouseover', function(){
						someflag = 1;
						$('.drop-hover-1 #path01').css({
							opacity: 0.7,
							fill: 'rgba(255,255,255,0.5)'
						});
					});
					svg4.on('mouseleave', function(){
						someflag = -1;
						$('.drop-hover-1 #path01').css({
							opacity: 1,
							fill: 'none'
						});
					});
					svg5.on('mouseover',function(){
						someflag = 2;
						$('.drop-hover-1 #path02').css({
							opacity:0.7,
							fill: 'rgba(255,255,255,0.5)'
						});
					});
					svg5.on('mouseleave', function(){
						someflag = -2;
						$('.drop-hover-1 #path02').css({
							opacity: 1,
							fill: 'none'
						});
					});
					svg6.on('mouseover',function(){
						someflag = 3;
						$('.drop-hover-1 #path03').css({
							opacity:0.7,
							fill: 'rgba(255,255,255,0.5)'
						});
					});
					svg6.on('mouseleave', function(){
						someflag = -3;
						$('.drop-hover-1 #path03').css({
							opacity: 1,
							fill: 'none'
						});
					});
					svg7.on('mouseover',function(){
						someflag = 4;
						$('.drop-hover-1 #path04').css({
							opacity:0.7,
							fill: 'rgba(255,255,255,0.5)'
						});
					});
					svg7.on('mouseleave', function(){
						someflag = -4;
						$('.drop-hover-1 #path04').css({
							opacity: 1,
							fill: 'none'
						});
					});
				 	showanm= function (path_to_show){
						$('#'+path_to_show).attr('class',"dash-"+countNext);
					}
					});

		}
		$('.option').draggable({
								containment : ".generalTemplateblock",
								revert : true,
								cursor : "move",
								zIndex: 100,
						});
						$("#qn-svg").droppable({
					    hoverClass: "drop-hover-1",
							drop: function (event, ui){
								switch (someflag) {
									case 1:
									if(ui.draggable.hasClass('path1')){
											var pth="fill01";
											showanm(pth);
											play_correct_incorrect_sound(1);
											$(".path1").removeClass("forhoverimg");
											$('.path1').detach();
											count--;
											if(count === 0){
												navcntrl();
											}
										} else{
											play_correct_incorrect_sound(0);
										}
										break;
										case 2:
										if(ui.draggable.hasClass('path2')||ui.draggable.hasClass('path02')||ui.draggable.hasClass('path01')){
												var pth="fill02";
												showanm(pth);
												play_correct_incorrect_sound(1);
												if(ui.draggable.hasClass('path2')){
													$(".path2").removeClass("forhoverimg");
													$('.path2').detach();
												} else if ( ui.draggable.hasClass('path02') ){
													$(".path02").removeClass("forhoverimg");
													$('.path02').detach();
												} else {
													$(".path01").removeClass("forhoverimg");
													$('.path01').detach();
												}
												count--;
												if(count === 0){
													navcntrl();
												}
											} else{
												play_correct_incorrect_sound(0);
											}
										break;
										case 3:
										if(ui.draggable.hasClass('path3')||ui.draggable.hasClass('path03')||ui.draggable.hasClass('path01')){
												var pth="fill03";
												showanm(pth);
												play_correct_incorrect_sound(1);
												if(ui.draggable.hasClass('path3')){
													$(".path3").removeClass("forhoverimg");
													$('.path3').detach();
												} else if ( ui.draggable.hasClass('path01') ){
													$(".path01").removeClass("forhoverimg");
													$('.path01').detach();
												} else {
													$(".path03").removeClass("forhoverimg");
													$('.path03').detach();
												}
												count--;
												if(count === 0){
													navcntrl();
												}
											} else{
												play_correct_incorrect_sound(0);
											}
										break;
										case 4:
										if(ui.draggable.hasClass('path4')||ui.draggable.hasClass('path02')||ui.draggable.hasClass('path03')){
												var pth="fill04";
												showanm(pth);
												play_correct_incorrect_sound(1);
												if(ui.draggable.hasClass('path4')){
													$(".path4").removeClass("forhoverimg");
													$('.path4').detach();
												} else if ( ui.draggable.hasClass('path02') ){
													$(".path02").removeClass("forhoverimg");
													$('.path02').detach();
												} else {
													$(".path03").removeClass("forhoverimg");
													$('.path03').detach();
												}
												count--;
												if(count === 0){
													navcntrl();
												}
											}
										 else{
												play_correct_incorrect_sound(0);
											}
									break;
									default:
										 play_correct_incorrect_sound(0);
								}
							}
						});
						function navcntrl(){
							$('.option').draggable('disable');
							$(".option").removeClass("forhoverimg");
							// svgmain.addClass("fade_in");
							$.each([svg1, svg2, svg3, svg4, svg5, svg6, svg7, svg8],function(){
								$(this).fadeOut(5000);
							});
							 svgmain.fadeIn(5000);
							setTimeout(function () {
								navigationcontroller();
							},4000);
						}
						$nextBtn.hide(0);
						$prevBtn.hide(0);
		switch(countNext){
			case 0:
			$nextBtn.show(0);
				break;
			case 1:
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_2");
					current_sound.on("complete",function(){
							document.getElementById("tutorial").controls = true;
							$('#tutorial').attr({'autoplay':'true'});
							//for autoplay in chrome
							$('#tutorial')[0].play()
												current_sound.on("complete",function(){
													nav_button_controls(0);
												});

					});
					var svgtoload="asvg";
					svgblock(svgtoload);
					count=3;
					setTimeout(function(){
						$nextBtn.show(0);
						$prevBtn.show(0);
					},19500);
			break;
			case 2:
					sound_player("sound_3");
		setTimeout(function(){
			$nextBtn.show(0);
			$prevBtn.show(0);
		},3000);
			break;
			case 3:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_5");
					var svgtoload="bsvg";
					svgblock(svgtoload);
					count=3;
			break;
			case 4:
					var svgtoload="csvg";
					svgblock(svgtoload);
					count=1;
			break;
			case 5:
					var svgtoload="dsvg";
					svgblock(svgtoload);
					count=2;
			break;
			case 6:
					var svgtoload="esvg";
					svgblock(svgtoload);
					count=4;
			break;
			case 7:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_4");
				endpageex.endpage(message);
			break;
		}

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
						nav_button_controls(0);

		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		if(content[count].hasOwnProperty('videoblock')){
					var videoblock = content[count].videoblock;
					var id = videoblock[0].videoid;
					// var video_src = preload.getItem(id).src;
					//get list of classes

					//  $("#"+id+" > source ").attr('src', video_src);
				    if(video_object != null){
						$("#"+id+" > source ").attr('src', video_object.src);
				    } else {
						$("#"+id).html(' <source src="activity/grade1/english/lines_and_curves/images/tutorial.mp4" type="video/ogg">Your browser does not support the video tag.');
				    }
				}
	}
function put_image2(content, count){
		if(content[count].hasOwnProperty('optionsblock')){
			var imageClass = content[count].optionsblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_image3(content, count){
		if(content[count].hasOwnProperty('exerciseblock')){
			var lncontent = content[count].exerciseblock[0];
				if(lncontent.hasOwnProperty('imageoptions')){
					var imageClass = lncontent.imageoptions;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						// console.log(image_src)
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						// console.log(selector)
						$(selector).attr('src', image_src);
					}
				}
			}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var selector = ('.'+speechbox[i].speechbox+' > .speechbg');
				console.log("imgsrc---"+image_src, $(selector));
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		loadTimelineProgress($total_page, countNext + 1);

		if(countNext == 0 || countNext == 1)
		navigationcontroller();

		generaltemplate();
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

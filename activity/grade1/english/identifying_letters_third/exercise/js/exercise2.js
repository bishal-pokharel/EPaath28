var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref+"/exercise/sounds/";

var content=[
	// slide 0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',

		leaftextblockadditionalclass: 'leaf-text-block',
		leaftextblock:[{
			textdata: data.string.textq,
			leafclass: "leaf-text lt-q",
		},{
			textdata: '',
			leafclass: "leaf-text empty-leaf leaf-press blink-anim lt-r",
		},{
			textdata: data.string.texts,
			leafclass: "leaf-text lt-s",
		},{
			leafclass: "leaf-text empty-leaf leaf-press lt-t",
		},{
			textdata: data.string.textu,
			leafclass: "leaf-text  lt-u",
		}
	],

		optiontextblockadditionalclass: 'float-option-block',
		optiontextblock:[{
			textdata: data.string.textq,
			optionclass: "float-text ft-q",
		},{
			textdata: data.string.textr,
			optionclass: "float-text ft-r",
		},{
			textdata: data.string.texts,
			optionclass: "float-text ft-s",
		},{
			textdata: data.string.textt,
			optionclass: "float-text ft-t",
		},{
			textdata: data.string.textu,
			optionclass: "float-text ft-u",
		},{
			textdata: data.string.textv,
			optionclass: "float-text ft-v",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "pond",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "frog",
					imgsrc : '',
					imgid : 'frog'
				},
				{
					imgclass : "crown",
					imgsrc : '',
					imgid : 'crown'
				},
				{
					imgclass : "arrows",
					imgsrc : '',
					imgid : 'arrow'
				}
			]
		}],
	},
	// slide 1
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: 'bg',

			leaftextblockadditionalclass: 'leaf-text-block',
			leaftextblock:[{
				textdata: data.string.textq,
				leafclass: "leaf-text lt-q",
			},{
				textdata: data.string.textr,
				leafclass: "leaf-text empty-leaf leaf-press lt-r",
			},{
				textdata: data.string.texts,
				leafclass: "leaf-text lt-s",
			},{
				leafclass: "leaf-text empty-leaf leaf-press blink-anim lt-t",
			},{
				textdata: data.string.textu,
				leafclass: "leaf-text  lt-u",
			}
		],

			optiontextblockadditionalclass: 'float-option-block',
			optiontextblock:[{
				textdata: data.string.textq,
				optionclass: "float-text ft-q",
			},{
				textdata: data.string.textr,
				optionclass: "float-text ft-r",
			},{
				textdata: data.string.texts,
				optionclass: "float-text ft-s",
			},{
				textdata: data.string.textt,
				optionclass: "float-text ft-t",
			},{
				textdata: data.string.textu,
				optionclass: "float-text ft-u",
			},{
				textdata: data.string.textv,
				optionclass: "float-text ft-v",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "pond",
						imgsrc : '',
						imgid : 'bg'
					},
					{
						imgclass : "frog",
						imgsrc : '',
						imgid : 'frog'
					},
					{
						imgclass : "crown",
						imgsrc : '',
						imgid : 'crown'
					},
					{
						imgclass : "arrows",
						imgsrc : '',
						imgid : 'arrow'
					}
				]
			}],
		},
// slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',

		leaftextblockadditionalclass: 'leaf-text-block',
		leaftextblock:[{
			textdata: data.string.textv,
			leafclass: "leaf-text lt-v",
		},{
			leafclass: "leaf-text empty-leaf leaf-press  blink-anim lt-w",
		},{
			textdata: data.string.textx,
			leafclass: "leaf-text lt-x",
		},{
			leafclass: "leaf-text empty-leaf leaf-press lt-y",
		},{
			textdata: data.string.textz,
			leafclass: "leaf-text  lt-z",
		}
	],

		optiontextblockadditionalclass: 'float-option-block',
		optiontextblock:[{
			textdata: data.string.textu,
			optionclass: "float-text ft-u",
		},{
			textdata: data.string.textv,
			optionclass: "float-text ft-v",
		},{
			textdata: data.string.textw,
			optionclass: "float-text ft-w",
		},{
			textdata: data.string.textx,
			optionclass: "float-text ft-x",
		},{
			textdata: data.string.texty,
			optionclass: "float-text ft-y",
		},{
			textdata: data.string.textz,
			optionclass: "float-text ft-z",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "pond",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "frog",
					imgsrc : '',
					imgid : 'frog'
				},
				{
					imgclass : "crown",
					imgsrc : '',
					imgid : 'crown'
				},
				{
					imgclass : "arrows",
					imgsrc : '',
					imgid : 'arrow'
				}
			]
		}],
	},
// slide 3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg',

	leaftextblockadditionalclass: 'leaf-text-block',
	leaftextblock:[{
		textdata: data.string.textv,
		leafclass: "leaf-text lt-v",
	},{
		textdata: data.string.textw,
		leafclass: "leaf-text empty-leaf lt-w",
	},{
		textdata: data.string.textx,
		leafclass: "leaf-text lt-x",
	},{
		leafclass: "leaf-text empty-leaf leaf-press blink-anim lt-y",
	},{
		textdata: data.string.textz,
		leafclass: "leaf-text  lt-z",
	}
],

	optiontextblockadditionalclass: 'float-option-block',
	optiontextblock:[{
		textdata: data.string.textu,
		optionclass: "float-text ft-u",
	},{
		textdata: data.string.textv,
		optionclass: "float-text ft-v",
	},{
		textdata: data.string.textw,
		optionclass: "float-text ft-w",
	},{
		textdata: data.string.textx,
		optionclass: "float-text ft-x",
	},{
		textdata: data.string.texty,
		optionclass: "float-text ft-y",
	},{
		textdata: data.string.textz,
		optionclass: "float-text ft-z",
	}],

	imageblock:[{
		imagestoshow : [
			{
				imgclass : "pond",
				imgsrc : '',
				imgid : 'bg'
			},
			{
				imgclass : "frog",
				imgsrc : '',
				imgid : 'frog'
			},
			{
				imgclass : "crown",
				imgsrc : '',
				imgid : 'crown'
			},
			{
				imgclass : "arrows",
				imgsrc : '',
				imgid : 'arrow'
			}
		]
	}],
},

	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg',

		leaftextblockadditionalclass: 'leaf-text-block',
		leaftextblock:[{
			textdata: data.string.textq,
			leafclass: "leaf-text lt-q",
		},{
			textdata: data.string.textr,
			leafclass: "leaf-text lt-r",
		},{
			textdata: data.string.texts,
			leafclass: "leaf-text lt-s",
		},{
			textdata: data.string.textt,
			leafclass: "leaf-text lt-t",
		},{
			textdata: data.string.textu,
			leafclass: "leaf-text lt-u",
		},{
			textdata: data.string.textv,
			leafclass: "leaf-text  lt-v",
		},{
			textdata: data.string.textw,
			leafclass: "leaf-text lt-w",
		},{
			textdata: data.string.textx,
			leafclass: "leaf-text  lt-x",
		},{
			textdata: data.string.texty,
			leafclass: "leaf-text  lt-y",
		},{
			textdata: data.string.textz,
			leafclass: "leaf-text  lt-z",
		}],

		optiontextblockadditionalclass: 'float-option-block',
		optiontextblock:[{
			textdata: data.string.textb,
			optionclass: "float-text ft-b",
		},{
			textdata: data.string.textc,
			optionclass: "float-text ft-c",
		},{
			textdata: data.string.textd,
			optionclass: "float-text ft-d",
		},{
			textdata: data.string.texte,
			optionclass: "float-text ft-e",
		},{
			textdata: data.string.textf,
			optionclass: "float-text ft-f",
		},{
			textdata: data.string.texth,
			optionclass: "float-text ft-h",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "pond",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "frog",
					imgsrc : '',
					imgid : 'frog'
				},
				{
					imgclass : "crown",
					imgsrc : '',
					imgid : 'crown'
				},
				{
					imgclass : "arrows arrSec",
					imgsrc : '',
					imgid : 'arrow01'
				},
				{
					imgclass : "frog2",
					imgsrc : '',
					imgid : 'frog2'
				}
			]
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-last',

		extratextblock:[{
			textdata: data.string.emsg,
			textclass: "msg-last",
		}],

		optiontextblockadditionalclass: 'end-option-block',
		optiontextblock:[{
			textdata: data.string.end1,
			optionclass: "end-options eopt-1",
		},{
			textdata: data.string.end2,
			optionclass: "end-options eopt-2",
		},{
			textdata: data.string.end3,
			optionclass: "end-options eopt-3",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "frog-last",
					imgsrc : '',
					imgid : 'frog2'
				}
			]
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg", src: imgpath+"ex2/pond.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow01", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frog", src: imgpath+"frog/thinking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frog-gif", src: imgpath+"frog/jumping.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "crown", src: imgpath+"ex2/crown.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thinking", src: imgpath+"frog/thinking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frog2", src: imgpath+"frog/withcrown.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_jump", src: soundAsset+"jump.ogg"},
			{id: "sound_jump2", src: soundAsset+"jump2.ogg"},
			{id: "sound_tada", src: soundAsset+"tada.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);

		switch(countNext) {
			// case 4:
			// 	$nextBtn.show(0);
			// 	break;
			case 4:
				var xPos = [13, 21, 26, 34, 41, 48, 56,  60, 66, 70, 84];
				var yPos = [32, 47, 32, 47, 32, 47, 32,  47, 32, 47, 40];
				function moveFrog(index){
					$('.leaf-text').eq(index).addClass('press-anim');
					sound_player('sound_jump');
					var randStr = new Date().getTime();
					$('.frog').attr('src', preload.getResult('frog-gif').src+'?'+randStr);
					$('.frog').animate({
						'top': yPos[index]+'%',
						'left': xPos[index] +'%'
					}, 1000, function(){
						if(index<10){
							var newIndex = index+1;
							moveFrog(newIndex);
						} else{
							$('.crown').fadeOut(500);
							$('.frog').fadeOut(500, function(){
								sound_player('sound_tada');
								$('.frog2').fadeIn(500, function(){
									$nextBtn.show(0);
								});
							});
						}
					});
				}
				var indexNum = 0;
				moveFrog(0);
				break;
			case 5:
				var props = new createjs.PlayPropsConfig().set({interrupt: createjs.Sound.INTERRUPT_ANY, loop: -1});
				createjs.Sound.play("sound_jump2", props);
				$('.eopt-1').click(playExAgain);
				$('.eopt-2').click(gotoLesson);
				$('.eopt-3').click(go_to_menu_page);
				break;
			default:
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				$('.float-text').click(function(){
					var correctAns = 'ft-b';
					var leaf = '.lt-b';
					switch(countNext){
						case 0:
							correctAns = 'ft-r';
							leaf = '.lt-r';
							break;
						case 1:
							correctAns = 'ft-t';
							leaf = '.lt-t';
							break;
						case 2:
							correctAns = 'ft-w';
							leaf = '.lt-w';
							break;
						case 3:
							correctAns = 'ft-y';
							leaf = '.lt-y';
							break;
					}
					if($(this).hasClass(correctAns)){
						var textData = $(this).children('p').html();
						play_correct_incorrect_sound(1);
						$(this).children('.correct-icon').show(0);
						$(this).addClass('correct-answer');
						$(leaf).removeClass('leaf-press');
						$(leaf).removeClass('blink-anim');
						$(leaf+' p').html(textData);
						$('.float-text').css('pointer-events', 'none');
						$nextBtn.show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
						$(this).children('.incorrect-icon').show(0);
						$(this).addClass('incorrect-answer');
					}
				});
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		 /*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/images/Playtime/";
var soundAsset = $ref + "/sounds/";
var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"ptime",
        uppertextblock: [
            {
                textclass: "playtime",
                textdata: data.string.playtime
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "rhinodancingdiv",
                    imgclass: "relativecls rhinoimgdiv",
                    imgid: "rhinodance",
                    imgsrc: "",
                },
                {
                    imgdiv: "squirrelistening",
                    imgclass: "relativecls squirrelisteningimg",
                    imgid: "squirrel",
                    imgsrc: "",
                }]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"question",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "green",
                textclass: "title",
                textdata: data.string.pt1text1
            }
        ],
        imageblock:[{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "commonbtn div1",
                    imgclass: "relativecls greenballoonimg",
                    imgid: "greenballoonimg",
                    imgsrc: "",
                    textclass:"hidetext color",
                    textdata:data.string.green
                },
                {
                    imgdiv: "commonbtn div2",
                    imgclass: "relativecls pinkballoonimg",
                    imgid: "pinkballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div3",
                    imgclass: "relativecls yellowballoonimg",
                    imgid: "yellowballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div4",
                    imgclass: "relativecls orangeballoonimg",
                    imgid: "orangeballoonimg",
                    imgsrc: "",
                },
                ]
        }]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"question",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "pink",
                textclass: "title",
                textdata: data.string.pt1text2
            }
        ],
        imageblock:[{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "commonbtn div1",
                    imgclass: "relativecls img1",
                    imgid: "greenballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div2",
                    imgclass: "relativecls smallimage img2",
                    imgid: "greenballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div3",
                    imgclass: "relativecls img3",
                    imgid: "pinkballoonimg",
                    imgsrc: "",
                    textclass:"hidetext color",
                    textdata:data.string.pink
                },
                {
                    imgdiv: "commonbtn div4",
                    imgclass: "relativecls smallimage img4",
                    imgid: "pinkballoonimg",
                    imgsrc: "",
                },
            ]
        }]
    },
//slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"question",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blue",
                textclass: "title",
                textdata: data.string.pt1text3
            }
        ],
        imageblock:[{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "commonbtn div1",
                    imgclass: "relativecls img1",
                    imgid: "blueballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div2",
                    imgclass: "relativecls smallimage img2",
                    imgid: "blueballoonimg",
                    imgsrc: "",
                    textclass:"hidetext color",
                    textdata:data.string.blue
                },
                {
                    imgdiv: "commonbtn div3",
                    imgclass: "relativecls img3",
                    imgid: "pinkballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div4",
                    imgclass: "relativecls smallimage img4",
                    imgid: "pinkballoonimg",
                    imgsrc: "",
                },
            ]
        }]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"question",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "yellow",
                textclass: "title",
                textdata: data.string.pt1text4
            }
        ],
        imageblock:[{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "commonbtn div1",
                    imgclass: "relativecls img1",
                    imgid: "greenballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div2",
                    imgclass: "relativecls img2",
                    imgid: "redballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div3",
                    imgclass: "relativecls smallimage img3",
                    imgid: "yellowballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div4",
                    imgclass: "relativecls  img4",
                    imgid: "yellowballoonimg",
                    imgsrc: "",
                    textclass:"hidetext color",
                    textdata:data.string.yellow
                },
            ]
        }]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"question",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "red",
                textclass: "title",
                textdata: data.string.pt1text5
            }
        ],
        imageblock:[{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "commonbtn div1",
                    imgclass: "relativecls img1",
                    imgid: "greenballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div2",
                    imgclass: "relativecls img2",
                    imgid: "redballoonimg",
                    imgsrc: "",
                    textclass:"hidetext color",
                    textdata:data.string.red
                },
                {
                    imgdiv: "commonbtn div3",
                    imgclass: "relativecls img3",
                    imgid: "pinkballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div4",
                    imgclass: "relativecls img4",
                    imgid: "blueballoonimg",
                    imgsrc: "",
                },
            ]
        }]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"question",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "orange",
                textclass: "title",
                textdata: data.string.pt1text6
            }
        ],
        imageblock:[{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "commonbtn div1",
                    imgclass: "relativecls img1",
                    imgid: "greenballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div2",
                    imgclass: "relativecls img2",
                    imgid: "yellowballoonimg",
                    imgsrc: "",

                },
                {
                    imgdiv: "commonbtn div3",
                    imgclass: "relativecls img3",
                    imgid: "pinkballoonimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonbtn div4",
                    imgclass: "relativecls img4",
                    imgid: "orangeballoonimg",
                    imgsrc: "",
                    textclass:"hidetext color",
                    textdata:data.string.orange
                },
            ]
        }]
    },
    // slidelast
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgendpage",
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv: "commonimg enddiv1",
                    imgclass: "relativecls img1",
                    imgid: "greensplashimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv2",
                    imgclass: "relativecls img2",
                    imgid: "redsplashimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv3",
                    imgclass: "relativecls img3",
                    imgid: "bluesplashimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv4",
                    imgclass: "relativecls img5",
                    imgid: "pinksplashimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv5",
                    imgclass: "relativecls img6",
                    imgid: "orangesplashimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv6",
                    imgclass: "relativecls img7",
                    imgid: "yellowsplashimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv7",
                    imgclass: "relativecls img8",
                    imgid: "bluedropimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv8",
                    imgclass: "relativecls img9",
                    imgid: "reddropimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv9",
                    imgclass: "relativecls img10",
                    imgid: "greendropimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv10",
                    imgclass: "relativecls img11",
                    imgid: "pinkdropimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv11",
                    imgclass: "relativecls img12",
                    imgid: "orangedropimg",
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg enddiv12",
                    imgclass: "relativecls img13",
                    imgid: "yellowdropimg",
                    imgsrc: "",
                },
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");

    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound="";
    var sound="";

    var endpageex =  new EndPageofExercise();
    var message =  data.string.messageplaytime;
    endpageex.init(6);
    // var lampTemplate = new LampTemplate();
    // lampTemplate.init($total_page-1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "ex", src: $ref + "css/playtimetemplate.css", type: createjs.AbstractLoader.CSS},
            {id: "ex1", src: $ref + "css/playtime.css", type: createjs.AbstractLoader.CSS},
            {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "greenballoonimg", src: imgpath+"green_balloon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pinkballoonimg", src: imgpath+"pink_balloon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowballoonimg", src: imgpath+"yellow_balloon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "orangeballoonimg", src: imgpath+"orange_balloon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "redballoonimg", src: imgpath+"red_balloon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "blueballoonimg", src: imgpath+"blue_balloon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "greensplashimg", src: imgpath+"green_splash.png", type: createjs.AbstractLoader.IMAGE},
            {id: "redsplashimg", src: imgpath+"red_splash.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bluesplashimg", src: imgpath+"blue_splash.png", type: createjs.AbstractLoader.IMAGE},
            {id: "orangesplashimg", src: imgpath+"orange_splash.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pinksplashimg", src: imgpath+"pink_splash.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowsplashimg", src: imgpath+"yellow_splash.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bluedropimg", src: imgpath+"colordrop_blue.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "reddropimg", src: imgpath+"colordrop_red.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "greendropimg", src: imgpath+"colordrop_green.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "orangedropimg", src: imgpath+"colordrop_orange.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "pinkdropimg", src: imgpath+"colordrop_pink.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowdropimg", src: imgpath+"colordrop_yellow.gif", type: createjs.AbstractLoader.IMAGE},


            //sounds
            {id: "sound_1", src: soundAsset+"Playtime-1.ogg"},
            {id: "sound_2", src: soundAsset+"Playtime-2.ogg"},
            {id: "sound_3", src: soundAsset+"Playtime-3.ogg"},
            {id: "sound_4", src: soundAsset+"Playtime-4.ogg"},
            {id: "sound_5", src: soundAsset+"Playtime-5.ogg"},
            {id: "sound_6", src: soundAsset+"Playtime-6.ogg"},
            {id: "sound_7", src: soundAsset+"excompleted.ogg"},
            {id: "sound_balbrust", src: 'sounds/common/balloon-pop.ogg'},
            {id: "sound_balfly", src:soundAsset+"fly_sound.ogg"},
            {id: "sound_clrdrop", src:soundAsset+"water_drop_sound.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
firstPagePlayTime(countNext);
        $(".hidetext").hide(0);
        put_image(content, countNext);
        texthighlight($board);
        var typeShow;
        clearInterval(typeShow);
        switch (countNext){
            case 0:
            firstPagePlayTime();
            break;
            case 1:
                sound_player("sound_1");
                shufflehint(data.string.green);
                checkans(imgpath+"green_splash.png",imgpath+"brokengreen.png","#5ECD42");
                break;
            case 2:
                sound_player("sound_2");
                shufflehint(data.string.pink);
                checkans(imgpath+"pink_splash.png",imgpath+"pink_broken.png","#FF51A3");
                break;
            case 3:
                sound_player("sound_3");
                shufflehint(data.string.blue);
                checkans(imgpath+"blue_splash.png",imgpath+"bluebroken.png","#0067FF");
                break;
            case 4:
                sound_player("sound_4");
                shufflehint(data.string.yellow);
                checkans(imgpath+"yellow_splash.png",imgpath+"yellow_broken.png","#FBFF00");
                break;
            case 5:
                sound_player("sound_5");
                shufflehint(data.string.red);
                checkans(imgpath+"red_splash.png",imgpath+"red_broken.png","#EE0100");
                break;
            case 6:
                sound_player("sound_6");
                shufflehint(data.string.orange);
                checkans(imgpath+"orange_splash.png",imgpath+"orange_broken.png","#FF7800");
                break;
            case 7:
                sound_player("sound_7",false);
                setTimeout(function(){
                    sound_player("sound_clrdrop",false);
                    typeShow = setInterval(function(){
                        sound_player("sound_clrdrop",false);
                    },2000);
                },5000);
                endpageex.endpage(message);
                break;
            default:
                navigationcontroller();
                break;
        }
    }


    function sound_player(sound_id,navigate,imgCls,imgSrc) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():"";
            if(imgCls && imgSrc){
                $("."+imgCls).attr("src",imgSrc);
            }
        });
    }



    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('imghintblock')) {
            var contentCount=content[count].imghintblock[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }
    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                // lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });

    $refreshBtn.on('click', ()=>{
        // countNext==0;
        templateCaller();
    });

    function checkans(srcofsplash,srcofbroken,bgcolor){
        $(".commonbtn ").on("click",function () {
            if($(this).hasClass("correctans")) {
                play_correct_incorrect_sound(1);
                $(".commonbtn").addClass("avoid-clicks");
                var correctdiv = $(this).clone();
                $(this).prepend("<div class='correctWrongDiv'><img class='correctWrongImg' src='images/right.png'/></div>")
                setTimeout(function(){
                correctdiv.appendTo($(".coverboardfull")).addClass("moveUp");
                sound_player("sound_balfly",false);
                correctdiv.prepend("<img class='scaleup splashimage relativecls' src='"+srcofsplash+"'>");
                $(".splashimage").hide(0);
                setTimeout(function(){
                    sound_player("sound_balbrust",false);
                    correctdiv.find("img").next().attr("src",srcofbroken).addClass("fadeOutEffect");
                    setTimeout(function(){
                        correctdiv.find("img:nth-child(2)").remove();
                        $(".splashimage").show(0);
                        correctdiv.find("p").show(0).removeClass("fadeOutEffect").css("background-color",bgcolor);
                        navigationcontroller();
                    },500);
                },2000);
                },500);
            }
            else{
                $(this).addClass("wrongans").attr("disabled","disabled");
                $(this).prepend("<div class='correctWrongDiv'><img class='correctWrongImg' src='images/wrong.png'/></div>")
                play_correct_incorrect_sound(0);
            }
        });
    }


    function shufflehint(correctans) {
        var imgdiv = $(".commonImgDiv");
        for (var i = imgdiv.children().length; i >= 0; i--) {
            imgdiv.append(imgdiv.children().eq(Math.random() * i | 0));
        }
        imgdiv.children().removeClass();
        var a = ["commonbtn div1","commonbtn div2","commonbtn div3","commonbtn div4"]
        imgdiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().trim()==correctans){
                $this.addClass("correctans")
            }
        });

    }



    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

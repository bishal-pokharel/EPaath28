var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        imageblock: [{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "bigballdiv imgdiv1 zoomInEffect",
                    imgclass: "relativecls imgcls bigballimg",
                    imgid: 'bigballImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "smallballdiv imgdiv2 zoomInEffect",
                    imgclass: "relativecls imgcls smallballimg",
                    imgid: 'smallballImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "textdiv1 zoomInEffect a1",
                    imgclass: "audioicondiv a1 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content centertext zoomInEffectText",
                        textdata: data.string.p2text1
                    }]
                },
                {
                    imgdiv: "textdiv2 zoomInEffect a2",
                    imgclass: "audioicondiv a2 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content  centertext zoomInEffectText",
                        textdata: data.string.p2text2
                    }]
                }
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        imageblock: [{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "bigballdiv imgdiv1 slideL",
                    imgclass: "relativecls imgcls bigballimg",
                    imgid: 'bigballImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "smallballdiv imgdiv2 slideR",
                    imgclass: "relativecls imgcls smallballimg",
                    imgid: 'smallballImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "bigdiv zoomInEffect a1",
                    imgclass: "audioicondiv a1 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content zoomInEffect",
                        textdata: data.string.p2text3
                    }]
                },
                {
                    imgdiv: "smalldiv zoomInEffect a2",
                    imgclass: "audioicondiv a2 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content  zoomInEffect",
                        textdata: data.string.p2text4
                    }]
                }
            ]
        }]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        imageblock: [{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "bigbirddiv imgdiv1 zoomInEffect",
                    imgclass: "relativecls imgcls bigbirdimg",
                    imgid: 'bigbirdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "smallbirddiv imgdiv2 zoomInEffect",
                    imgclass: "relativecls imgcls smallbirdimg",
                    imgid: 'smallbirdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "textdiv1 zoomInEffect a1",
                    imgclass: "audioicondiv a1 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content centertext zoomInEffect",
                        textdata: data.string.p2text5
                    }]
                },
                {
                    imgdiv: "textdiv2 zoomInEffect a2",
                    imgclass: "audioicondiv a2 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content centertext zoomInEffect",
                        textdata: data.string.p2text6
                    }]
                }
            ]
        }]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        imageblock: [{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "bigbirddiv imgdiv1 slideL",
                    imgclass: "relativecls imgcls bigbirdimg",
                    imgid: 'bigbirdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "smallbirddiv imgdiv2 slideR",
                    imgclass: "relativecls imgcls smallbirdimg",
                    imgid: 'smallbirdImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "bigdiv zoomInEffect a1",
                    imgclass: "audioicondiv a1 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content  zoomInEffect",
                        textdata: data.string.p2text7
                    }]
                },
                {
                    imgdiv: "smalldiv zoomInEffect a2",
                    imgclass: "audioicondiv a2 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content  zoomInEffect",
                        textdata: data.string.p2text8
                    }]
                }
            ]
        }]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        imageblock: [{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "bigbusdiv imgdiv3 zoomInEffect",
                    imgclass: "relativecls imgcls bigbusimg",
                    imgid: 'bigbusImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "smallbusdiv imgdiv4 zoomInEffect",
                    imgclass: "relativecls imgcls smallbusimg",
                    imgid: 'smallbusImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "textdiv1 zoomInEffect a1",
                    imgclass: "audioicondiv a1 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content centertext zoomInEffect",
                        textdata: data.string.p2text9
                    }]
                },
                {
                    imgdiv: "textdiv2 zoomInEffect a2",
                    imgclass: "audioicondiv a2 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content centertext zoomInEffect",
                        textdata: data.string.p2text10
                    }]
                }
            ]
        }]
    },
    // slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        imageblock: [{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "bigbusdiv imgdiv3 slideL",
                    imgclass: "relativecls imgcls bigbusimg",
                    imgid: 'bigbusImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "smallbusdiv imgdiv4 slideR",
                    imgclass: "relativecls imgcls smallbusimg",
                    imgid: 'smallbusImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "bigdiv1 zoomInEffect a1",
                    imgclass: "audioicondiv1 a1 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content  zoomInEffect",
                        textdata: data.string.p2text11
                    }]
                },
                {
                    imgdiv: "smalldiv1 zoomInEffect a2",
                    imgclass: "audioicondiv1 a2 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content  zoomInEffect",
                        textdata: data.string.p2text12
                    }]
                }
            ]
        }]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        imageblock: [{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "bigtreediv imgdiv3 zoomInEffect",
                    imgclass: "relativecls imgcls bigtreeimg",
                    imgid: 'bigtreeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "smalltreediv imgdiv4 zoomInEffect",
                    imgclass: "relativecls imgcls smalltreeimg",
                    imgid: 'smalltreeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "textdiv1 zoomInEffect a1",
                    imgclass: "audioicondiv a1 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content centertext zoomInEffect",
                        textdata: data.string.p2text13
                    }]
                },
                {
                    imgdiv: "textdiv2 zoomInEffect a2",
                    imgclass: "audioicondiv a2 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content centertext zoomInEffect",
                        textdata: data.string.p2text14
                    }]
                }
            ]
        }]
    },
    // slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        imageblock: [{
            commonImgDiv:"commonImgDiv",
            imagestoshow: [
                {
                    imgdiv: "bigtreediv imgdiv3 slideL",
                    imgclass: "relativecls imgcls bigtreeimg",
                    imgid: 'bigtreeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "smalltreediv imgdiv4 slideR",
                    imgclass: "relativecls imgcls smalltreeimg",
                    imgid: 'smalltreeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "bigdiv1 zoomInEffect a1",
                    imgclass: "audioicondiv1 a1 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content  zoomInEffect",
                        textdata: data.string.p2text15
                    }]
                },
                {
                    imgdiv: "smalldiv1 zoomInEffect a2",
                    imgclass: "audioicondiv1 a2 audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                    textdata:[{
                        textclass:"content  zoomInEffect",
                        textdata: data.string.p2text16
                    }]
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "bigballImg", src: imgpath+"bigball.png", type: createjs.AbstractLoader.IMAGE},
            {id: "smallballImg", src: imgpath+"smallball.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bigbirdImg", src: imgpath+"bigbird.png", type: createjs.AbstractLoader.IMAGE},
            {id: "smallbirdImg", src: imgpath+"smallbird.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bigbusImg", src: imgpath+"bigbus.png", type: createjs.AbstractLoader.IMAGE},
            {id: "smallbusImg", src: imgpath+"smallbus.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bigtreeImg", src: imgpath+"bigtree.png", type: createjs.AbstractLoader.IMAGE},
            {id: "smalltreeImg", src: imgpath+"smalltree.png", type: createjs.AbstractLoader.IMAGE},
            {id: "audioicon", src: "images/speaker.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"bigball.ogg"},
            {id: "sound_0_1", src: soundAsset+"smallball.ogg"},
            {id: "sound_1", src: soundAsset+"bigball1.ogg"},
            {id: "sound_1_1", src: soundAsset+"smallball1.ogg"},
            {id: "sound_2", src: soundAsset+"bigbird.ogg"},
            {id: "sound_2_1", src: soundAsset+"smallbird.ogg"},
            {id: "sound_3", src: soundAsset+"bigbird1.ogg"},
            {id: "sound_3_1", src: soundAsset+"smallbird1.ogg"},
            {id: "sound_4", src: soundAsset+"bigbus.ogg"},
            {id: "sound_4_1", src: soundAsset+"smallbus.ogg"},
            {id: "sound_5", src: soundAsset+"bigbus1.ogg"},
            {id: "sound_5_1", src: soundAsset+"smallbus1.ogg"},
            {id: "sound_6", src: soundAsset+"bigtree.ogg"},
            {id: "sound_6_1", src: soundAsset+"smalltree.ogg"},
            {id: "sound_7", src: soundAsset+"bigtree1.ogg"},
            {id: "sound_7_1", src: soundAsset+"smalltree1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
 
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        $(".audioicondiv,.audioicondiv1,.textdiv1,.textdiv2,.bigdiv,.smalldiv,.bigdiv1,.smalldiv1").click(function(){
            if($(this).hasClass("a1"))
              sound_player("sound_"+countNext);
            if($(this).hasClass("a2"))
                sound_player("sound_"+countNext+"_1");
        });

        switch(countNext){
            case 0:
                animateImage(2000);
                syncsound("sound_0","sound_0_1",4500);
                break;
            case 1:
                animateImage(5000);
                syncsound("sound_1","sound_1_1",7000);
                break;
            case 2:
                animateImage(2000);
                syncsound("sound_2","sound_2_1",4500);
                break;
            case 3:
                animateImage(5000);
                syncsound("sound_3","sound_3_1",7000);
                break;
            case 4:
                animateImage(2000);
                syncsound("sound_4","sound_4_1",4500);
                break;
            case 5:
                animateImage(7500);
                syncsound("sound_5","sound_5_1",9500);
                break;
            case 6:
                animateImage(2000);
                syncsound("sound_6","sound_6_1",4500);
                break;
            case 7:
                animateImage(6500);
                syncsound("sound_7","sound_7_1",8000);
                break;
        }
    }



    function sound_player(sound_id,navigate,lastpage) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page,lastpage):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateImage(timeinterval){
        $(".textdiv1,.textdiv2,.imgdiv2,.bigdiv,.smalldiv,.imgdiv4,.bigdiv1,.smalldiv1").hide(0);
        setTimeout(function () {
         $(".textdiv1,.bigdiv,.bigdiv1").show(0);
         setTimeout(function () {
             $(".imgdiv2,.imgdiv4").show(0);
             setTimeout(function () {
                 $(".textdiv2,.smalldiv,.smalldiv1").show(0);
             },2000)
         },timeinterval)
        },2000)
    }
    function syncsound(sound1,sound2,timeinterval){
        setTimeout(function(){
            sound_player(sound1,false);
            setTimeout(function(){
                sound_player(sound2,true,countNext==7?true:false);
            },timeinterval);
        },2500);
    }
});


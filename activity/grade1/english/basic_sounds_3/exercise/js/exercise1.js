var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref+"/exercise/sounds/";


var instruction = new buzz.sound((soundAsset + "BS_ins.ogg"));
var sound_v = new buzz.sound((soundAsset + "v.mp3"));
var sound_w = new buzz.sound((soundAsset + "w.mp3"));
var sound_y = new buzz.sound((soundAsset + "y.mp3"));
var sound_z = new buzz.sound((soundAsset + "z.mp3"));
var sound_wh = new buzz.sound((soundAsset + "wh.mp3"));
var sound_ch = new buzz.sound((soundAsset + "ch.mp3"));
var sound_sh = new buzz.sound((soundAsset + "sh.mp3"));

var sound_sheep = new buzz.sound((soundAsset + "sheep.mp3"));
var sound_wheel = new buzz.sound((soundAsset + "wheel.mp3"));
var sound_chair = new buzz.sound((soundAsset + "chair.mp3"));
var sound_shop = new buzz.sound((soundAsset + "shop.mp3"));
var sound_zipper = new buzz.sound((soundAsset + "zipper.mp3"));
var sound_web = new buzz.sound((soundAsset + "web.mp3"));
var sound_video = new buzz.sound((soundAsset + "video.mp3"));
var sound_vase = new buzz.sound((soundAsset + "vase.mp3"));
var sound_zebra = new buzz.sound((soundAsset + "zebra.mp3"));
var sound_child = new buzz.sound((soundAsset + "child.mp3"));
var sound_wall = new buzz.sound((soundAsset + "wall.mp3"));
var sound_yellow = new buzz.sound((soundAsset + "yellow.mp3"));
var sound_zoo = new buzz.sound((soundAsset + "zoo.mp3"));
var sound_ship = new buzz.sound((soundAsset + "ship.mp3"));
var sound_yak = new buzz.sound((soundAsset + "yak.mp3"));
var sound_vest = new buzz.sound((soundAsset + "vest.mp3"));
var sound_van = new buzz.sound((soundAsset + "van.mp3"));
var sound_wave = new buzz.sound((soundAsset + "wave.mp3"));
var sound_chess = new buzz.sound((soundAsset + "chess.mp3"));
var sound_zero = new buzz.sound((soundAsset + "zero.mp3"));
var sound_whip = new buzz.sound((soundAsset + "whip.mp3"));
var sound_way = new buzz.sound((soundAsset + "way.mp3"));

var content=[
  {
  //slide 0
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "first"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "way.png",
        labeltext: data.string.ex_way,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "sheep.png",
        labeltext: data.string.ex_sheep,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "chair.png",
        labeltext: data.string.ex_chair,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "wheel.png",
        labeltext: data.string.ex_wheel,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 1
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "second"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "shop.png",
        labeltext: data.string.ex_shop,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "zipper.png",
        labeltext: data.string.ex_zipper,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "web.png",
        labeltext: data.string.ex_web,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "video.png",
        labeltext: data.string.ex_video,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 2
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "third"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "vase.png",
        labeltext: data.string.ex_vase,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "zebra.png",
        labeltext: data.string.ex_zebra,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "child.png",
        labeltext: data.string.ex_child,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "web.png",
        labeltext: data.string.ex_web,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 3
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "fourth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "sheep.png",
        labeltext: data.string.ex_sheep,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "wheel.png",
        labeltext: data.string.ex_wheel,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "chair.png",
        labeltext: data.string.ex_chair,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "way.png",
        labeltext: data.string.ex_way,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 4
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "fifth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "wall.png",
        labeltext: data.string.ex_wall,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "zoo.png",
        labeltext: data.string.ex_zoo,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "ship.png",
        labeltext: data.string.ex_ship,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "yellow.png",
        labeltext: data.string.ex_yellow,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 5
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "sixth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "yak.png",
        labeltext: data.string.ex_yak,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "shop.png",
        labeltext: data.string.ex_shop,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "vest.png",
        labeltext: data.string.ex_vest,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "zoo.png",
        labeltext: data.string.ex_zoo,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 6
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "seventh"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "van.png",
        labeltext: data.string.ex_van,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "wave.png",
        labeltext: data.string.ex_wave,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "chair.png",
        labeltext: data.string.ex_chair,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "sheep.png",
        labeltext: data.string.ex_sheep,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
{
  //slide 7
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
      imgsrc : imgpath + "speaker.png",
      imgclass : "speakerimageclass",
      speakerlash : "eighth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "chess.png",
        labeltext: data.string.ex_chess,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "wheel.png",
        labeltext: data.string.ex_wheel,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "zero.png",
        labeltext: data.string.ex_zero,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "shop.png",
        labeltext: data.string.ex_shop,
        correctoption :[{}],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]},

{
  //slide 8
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
      imgsrc : imgpath + "speaker.png",
      imgclass : "speakerimageclass",
      speakerlash : "ninth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "whip.png",
        labeltext: data.string.ex_whip,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "sheep.png",
        labeltext: data.string.ex_sheep,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "yak.png",
        labeltext: data.string.ex_yak,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "chair.png",
        labeltext: data.string.ex_chair,
        correctoption :[{}],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]},

{
  //slide 9
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
      imgsrc : imgpath + "speaker.png",
      imgclass : "speakerimageclass",
      speakerlash : "tenth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "van.png",
        labeltext: data.string.ex_van,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "wave.png",
        labeltext: data.string.ex_wave,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "ship.png",
        labeltext: data.string.ex_ship,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "child.png",
        labeltext: data.string.ex_child,
        correctoption :[{}],
        speaker:[{
          speakersrc : imgpath + "speaker.png",
          speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]},
];

/*remove this for non random questions*/
content.shufflearray();

$(function (){
  var $board    = $('.board');
  var $nextBtn  = $('#activity-page-next-btn-enabled');
  var $prevBtn  = $('#activity-page-prev-btn-enabled');
  var countNext = 0;

  var $total_page = content.length;
  var currentSound = instruction;

  function navigationcontroller(islastpageflag){
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined" ?
    islastpageflag = false :
    typeof islastpageflag != 'boolean'?
    alert("NavigationController : Hi Master, please provide a boolean parameter") :
    null;
  }

  var score = 0;

  /*values in this array is same as the name of images of eggs in image folder*/
  var testin = new EggTemplate();

  testin.init(10);
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $('.rightic').hide(0);
    $('.wrongic1').hide(0);
    $('.wrongic2').hide(0);
    $('.wrongic3').hide(0);
    $('.optionscontainer').hide(0);
    countNext==0?instruction.play():'';
		if($('.speakerimageclass').attr('id') == 'first')
			{
			soundplay(sound_wh);
    		soundplay1(sound_way);
   			soundplay2(sound_sheep);
 		    soundplay3(sound_chair);
   			soundplay4(sound_wheel);
   		    }
		else if($('.speakerimageclass').attr('id') ==  'second')
		{
			soundplay(sound_v);
        	soundplay1(sound_shop);
        	soundplay2(sound_zipper);
        	soundplay3(sound_web);
        	soundplay4(sound_video);
        }
		else if($('.speakerimageclass').attr('id') == 'third')
		{
			soundplay(sound_w);
       		soundplay1(sound_vase);
        	soundplay2(sound_zebra);
        	soundplay3(sound_child);
        	soundplay4(sound_web);
        	}
		else if($('.speakerimageclass').attr('id') == 'fourth')
		{
			soundplay(sound_w);
        	soundplay1(sound_sheep);
        	soundplay2(sound_wheel);
        	soundplay3(sound_chair);
        	soundplay4(sound_way);
        }
    else if($('.speakerimageclass').attr('id') == 'fifth')
    {
      soundplay(sound_y);
          soundplay1(sound_wall);
          soundplay2(sound_zoo);
          soundplay3(sound_ship);
          soundplay4(sound_yellow);
          }

		else if($('.speakerimageclass').attr('id') == 'sixth')
		{
			soundplay(sound_z);
       		soundplay1(sound_yak);
       		soundplay2(sound_shop);
        	soundplay3(sound_vest);
        	soundplay4(sound_zoo);
        }
		else if($('.speakerimageclass').attr('id') == 'seventh')
		{
			soundplay(sound_sh);
        	soundplay1(sound_van);
        	soundplay2(sound_wave);
        	soundplay3(sound_chair);
        	soundplay4(sound_sheep);
        	}
    else if($('.speakerimageclass').attr('id') == 'eighth')
    {
      soundplay(sound_sh);
          soundplay1(sound_chess);
          soundplay2(sound_wheel);
          soundplay3(sound_zero);
          soundplay4(sound_shop);
          }
    else if($('.speakerimageclass').attr('id') == 'ninth')
    {
      soundplay(sound_ch);
          soundplay1(sound_whip);
          soundplay2(sound_sheep);
          soundplay3(sound_yak);
          soundplay4(sound_chair);
          }
    else if($('.speakerimageclass').attr('id') == 'tenth')
    {
      soundplay(sound_ch);
          soundplay1(sound_van);
          soundplay2(sound_wave);
          soundplay3(sound_child);
          soundplay4(sound_ship);
          }
		else{
				$nextBtn.hide(0);
			}

    	function soundplay(sound) {
		$(".speakerimageclass").click(function() {
      currentSound.stop();
			sound.play().bind("playing", function() {
				$(".speakerimageclass").css({animation: "none"});
				$(".optionscontainer").delay(1000).fadeIn(500);
			})
		})
		}
         function soundplay1(sound) {
   		 $(".option1speaker").click(function() {
   	    sound.play().bind("playing", function() {
     	 })
  		  })
  		}
  function soundplay2(sound) {
    $(".option2speaker").click(function() {
      sound.play().bind("playing", function() {
      })
    })
  	}
  function soundplay3(sound) {
    $(".option3speaker").click(function() {
      sound.play().bind("playing", function() {
      })
    })
 	 }
  function soundplay4(sound) {
    $(".option4speaker").click(function() {
      sound.play().bind("playing", function() {
      })
    })
	  }
    /*generate question no at the beginning of question*/
    testin.numberOfQuestions();

    var parent = $(".optionsdiv");
    var divs = parent.children();
    while (divs.length) {
      parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    }

        var frac_1_n = 0;
        var frac_1_d = 0;
        var frac_2_n = 0;
        var frac_2_d = 0;
        var new_question1 = 0;
        var new_question2 = 0;
        var rand_multiplier_1 = 1;
        var rand_multiplier_2 = 1;
        var incorrect = false;

        function correct_btn(current_btn){
          $('.options_sign').addClass('disabled');
          $('.hidden_sign').html($(current_btn).html());
          $('.hidden_sign').addClass('fade_in');
          $('.rightic').show(0);
          $('.optionscontainer').removeClass('forHover');
          $(current_btn).addClass('option_true');
          $nextBtn.show(0);
          if(!incorrect){
          testin.update(true);
          }
        }
        function incorrect_btn1(current_btn){
          $(current_btn).addClass('disabled');
          $(current_btn).addClass('option_false');
          testin.update(false);
          incorrect = true;

        }
        function incorrect_btn2(current_btn){
          $(current_btn).addClass('disabled');
          $(current_btn).addClass('option_false');
          testin.update(false);
          $('.wrongic').show(0);
          incorrect = true;
        }
        function incorrect_btn(current_btn){
          $(current_btn).addClass('disabled');
          $(current_btn).addClass('option_false');
          $('.wrongic').show(0);
          testin.update(false);
          incorrect = true;
        }
    $('.correctOne').click(function(){
      correct_btn(this);
      var texto = $(this).find(".quesLabel");
      var textoo = $(texto).text();
      $('.dashedLine').html(textoo);
      $('.dashedLine').html(textoo).addClass('dsLine');
      play_correct_incorrect_sound(1);
    });

    $('.incorrectOne').click(function(){
      incorrect_btn(this);
      $('.wrongic1').show(0);
      play_correct_incorrect_sound(0);
    });
    $('.incorrectTwo').click(function(){
      incorrect_btn(this);
      $('.wrongic2').show(0);
      play_correct_incorrect_sound(0);
    });
    $('.incorrectThree').click(function(){
      incorrect_btn1(this);
      $('.wrongic3').show(0);
      play_correct_incorrect_sound(0);
    });
  }

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


  }

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function(){
  	countNext++;
  	 templateCaller();
    testin.gotoNext();
  });

  $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
    countNext--;
    templateCaller();
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });
/*=====  End of Templates Controller Block  ======*/
});

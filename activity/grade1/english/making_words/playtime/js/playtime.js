var imgpath = $ref+"/images/";
var soundAsset = $ref + "/sounds/";
var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblock: [
            {
                divcls:"ptime",
                textclass: "playtime",
                textdata: data.string.playtime
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "rhinodancingdiv",
                    imgclass: "rhinoimgdiv",
                    imgid: "rhinodance",
                    imgsrc: "",
                },
                {
                    imgdiv: "squirrelistening",
                    imgclass: "squirrelisteningimg",
                    imgid: "squirrel",
                    imgsrc: "",
                }]
        }]
    },
    //slide1

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        lefttextblock:[
            {
                divcls:"commontxt idiv",
                textclass: "itxt",
                textdata: data.string.i
            },
            {
                divcls:"commontxt zdiv",
                textclass: "ztxt",
                textdata: data.string.z
            },
            {
                divcls:"commontxt adiv",
                textclass: "atxt",
                textdata: data.string.a
            },
            {
                divcls:"commontxt rdiv",
                textclass: "rtxt",
                textdata: data.string.r
            },
            {
                divcls:"commontxt cdiv",
                textclass: "ctxt",
                textdata: data.string.c
            }
        ],
        righttextblock:[

            {
                divcls:"commontxt ediv",
                textclass: "etxt",
                textdata: data.string.e
            },
            {
                divcls:"commontxt mdiv",
                texvextclass: "mtxt",
                textdata: data.string.m
            },
            {
                divcls:"commontxt jdiv",
                textclass: "jtxt",
                textdata: data.string.j
            },
            {
                divcls:"commontxt sdiv",
                textclass: "stxt",
                textdata: data.string.s
            },
            {
                divcls:"commontxt pdiv",
                textclass: "ptxt",
                textdata: data.string.p
            },
            {
                divcls:"commontxt wdiv",
                textclass: "wtxt",
                textdata: data.string.w
            }

        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "speechdiv1",
                    imgclass: "speechimg",
                    imgid: "speech",
                    imgsrc: "",
                    imgcls:"saying1",
                    imgtxt:data.string.pt1text1
                },
                {
                    imgdiv: "ashadiv",
                    imgclass: "moveyes ashaimg",
                    imgid: "asha",
                    imgsrc: "",
                },
                {
                    imgdiv: "premdiv",
                    imgclass: "moveyesprem premimg",
                    imgid: "premtalking",
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg3",
        quesansblock: [{
            qadiv: "quesdiv",
            qacls: "ques",
            qatxt: data.string.clickplay,
            selectbox: [
                {
                    selectcls: "commonbtn firstdiv",
                    selecttxt: data.string.b
                },
                {
                    selectcls: "commonbtn seconddiv firstans",
                    selecttxt: data.string.i
                },
                {
                    selectcls: "commonbtn thirddiv",
                    selecttxt: data.string.k
                },
                {
                    selectcls: "commonbtn fourthdiv",
                    selecttxt: data.string.j
                },
                {
                    selectcls: "commonbtn fifthdiv secondans",
                    selecttxt: data.string.n
                }
            ],
            ansbox:[
                {
                    selectcls: "commonans ansfirst",
                    selecttxt: data.string.b
                },
                {
                    selectcls: "commonans anssecond",
                    selecttxt: ""//data.string.i
                },
                {
                    selectcls: "commonans ansthird",
                    selecttxt: ""//data.string.n
                }
            ]
        }],
        imghintblock:[{
            imghintdiv:"hintmaindiv zoomin",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "hintdiv",
                        imgclass: "hintimg",
                        imgid: "binImg",
                        imgsrc: ""
                    },
                    {
                        imgdiv: "div1",
                        imgclass: "audiodiv",
                        imgid: "audioicon",
                        imgsrc: "",
                    }
                ]
            }]
        }],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"rhinodiv",
                    imgclass: "rhinoimg",
                    imgid: "rhino",
                    imgsrc: "",
                },
                {
                    imgdiv:"leoparddiv",
                    imgclass: "leopardimg",
                    imgid: "leopard",
                    imgsrc: "",
                },
                {
                    imgdiv:"redpandadiv",
                    imgclass: "redpandaimg",
                    imgid: "redpanda",
                    imgsrc: "",
                },
                {
                    imgdiv:"monkeydiv",
                    imgclass: "monkeyimg",
                    imgid: "monkey",
                    imgsrc: "",
                },
                {
                    imgdiv:"crocodilediv",
                    imgclass: "crocodileimg",
                    imgid: "crocodile",
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg4",
        quesansblock: [{
            qadiv: "quesdiv",
            qacls: "ques",
            qatxt: data.string.clickplay,
            selectbox: [
                {
                    selectcls: "commonbtn firstdiv",
                    selecttxt: data.string.z
                },
                {
                    selectcls: "commonbtn seconddiv",
                    selecttxt: data.string.o
                },
                {
                    selectcls: "commonbtn thirddiv secondans",
                    selecttxt: data.string.t
                },
                {
                    selectcls: "commonbtn fourthdiv",
                    selecttxt: data.string.l
                },
                {
                    selectcls: "commonbtn fifthdiv firstans",
                    selecttxt: data.string.a
                }
            ],
            ansbox:[
                {
                    selectcls: "commonans ansfirst",
                    selecttxt: data.string.c
                },
                {
                    selectcls: "commonans anssecond",
                    selecttxt: ""//data.string.i
                },
                {
                    selectcls: "commonans ansthird",
                    selecttxt: ""//data.string.n
                }
            ]
        }],
        imghintblock:[{
            imghintdiv:"hintmaindiv zoomin",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "hintdiv",
                        imgclass: "hintimgcat",
                        imgid: "catImg",
                        imgsrc: ""
                    },
                    {
                        imgdiv: "div2",
                        imgclass: "audiodiv",
                        imgid: "audioicon",
                        imgsrc: "",
                    }
                ]
            }]
        }],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"rhinodiv",
                    imgclass: "rhinoimg",
                    imgid: "rhino",
                    imgsrc: "",
                },
                {
                    imgdiv:"leoparddiv",
                    imgclass: "leopardimg",
                    imgid: "leopard",
                    imgsrc: "",
                },
                {
                    imgdiv:"redpandadiv",
                    imgclass: "redpandaimg",
                    imgid: "redpanda",
                    imgsrc: "",
                },
                {
                    imgdiv:"monkeydiv",
                    imgclass: "monkeyimg",
                    imgid: "monkey",
                    imgsrc: "",
                },
                {
                    imgdiv:"crocodilediv",
                    imgclass: "crocodileimg",
                    imgid: "crocodile",
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg5",
        quesansblock: [{
            qadiv: "quesdiv",
            qacls: "ques",
            qatxt: data.string.clickplay,
            selectbox: [
                {
                    selectcls: "commonbtn firstdiv",
                    selecttxt: data.string.b
                },
                {
                    selectcls: "commonbtn seconddiv firstans",
                    selecttxt: data.string.u
                },
                {
                    selectcls: "commonbtn thirddiv",
                    selecttxt: data.string.k
                },
                {
                    selectcls: "commonbtn fourthdiv",
                    selecttxt: data.string.e
                },
                {
                    selectcls: "commonbtn fifthdiv secondans",
                    selecttxt: data.string.g
                }
            ],
            ansbox:[
                {
                    selectcls: "commonans ansfirst",
                    selecttxt: data.string.j
                },
                {
                    selectcls: "commonans anssecond",
                    selecttxt: ""//data.string.i
                },
                {
                    selectcls: "commonans ansthird",
                    selecttxt: ""//data.string.n
                }
            ]
        }],
        imghintblock:[{
            imghintdiv:"hintmaindiv zoomin",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "hintdiv",
                        imgclass: "hintimg",
                        imgid: "jugImg",
                        imgsrc: ""
                    },
                    {
                        imgdiv: "div1",
                        imgclass: "audiodiv",
                        imgid: "audioicon",
                        imgsrc: "",
                    }
                ]
            }]
        }],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"rhinodiv",
                    imgclass: "rhinoimg",
                    imgid: "rhino",
                    imgsrc: "",
                },
                {
                    imgdiv:"leoparddiv",
                    imgclass: "leopardimg",
                    imgid: "leopard",
                    imgsrc: "",
                },
                {
                    imgdiv:"redpandadiv",
                    imgclass: "redpandaimg",
                    imgid: "redpanda",
                    imgsrc: "",
                },
                {
                    imgdiv:"monkeydiv",
                    imgclass: "monkeyimg",
                    imgid: "monkey",
                    imgsrc: "",
                },
                {
                    imgdiv:"crocodilediv",
                    imgclass: "crocodileimg",
                    imgid: "crocodile",
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg6",
        imageblock:[{
        imagestoshow: [
            {
                imgdiv: "speechdiv",
                imgclass: "speechimg",
                imgid: "speech",
                imgsrc: "",
                imgcls:"saying",
                imgtxt:data.string.pt1text2
            },
            {
                imgdiv: "premdivcls",
                imgclass: "premimgcls",
                imgid: "premtalkinghand",
                imgsrc: "",
            }

            ]
        }]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg7",
        quesansblock: [{
            qadiv: "quesdiv",
            qacls: "ques",
            qatxt: data.string.clickplay,
            selectbox: [
                {
                    selectcls: "commonbtn firstdiv",
                    selecttxt: data.string.r
                },
                {
                    selectcls: "commonbtn seconddiv secondans",
                    selecttxt: data.string.n
                },
                {
                    selectcls: "commonbtn thirddiv",
                    selecttxt: data.string.g
                },
                {
                    selectcls: "commonbtn fourthdiv",
                    selecttxt: data.string.d
                },
                {
                    selectcls: "commonbtn fifthdiv firstans",
                    selecttxt: data.string.e
                }
            ],
            ansbox:[
                {
                    selectcls: "commonans ansfirst",
                    selecttxt: data.string.p
                },
                {
                    selectcls: "commonans anssecond",
                    selecttxt: ""//data.string.i
                },
                {
                    selectcls: "commonans ansthird",
                    selecttxt: ""//data.string.n
                }
            ]
        }],
        imghintblock:[{
            imghintdiv:"hintmaindiv zoomin",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "hintdiv",
                        imgclass: "hintimgpen",
                        imgid: "penImg",
                        imgsrc: ""
                    },
                    {
                        imgdiv: "div3",
                        imgclass: "audiodiv",
                        imgid: "audioicon",
                        imgsrc: "",
                    }
                ]
            }]
        }],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"rhinodiv",
                    imgclass: "rhinoimg",
                    imgid: "rhino",
                    imgsrc: "",
                },
                {
                    imgdiv:"leoparddiv",
                    imgclass: "leopardimg",
                    imgid: "leopard",
                    imgsrc: "",
                },
                {
                    imgdiv:"redpandadiv",
                    imgclass: "redpandaimg",
                    imgid: "redpanda",
                    imgsrc: "",
                },
                {
                    imgdiv:"monkeydiv",
                    imgclass: "monkeyimg",
                    imgid: "monkey",
                    imgsrc: "",
                },
                {
                    imgdiv:"crocodilediv",
                    imgclass: "crocodileimg",
                    imgid: "crocodile",
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg6",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "speechdiv",
                    imgclass: "speechimg",
                    imgid: "speech",
                    imgsrc: "",
                    imgcls:"saying",
                    imgtxt:data.string.pt1text3
                },
                {
                    imgdiv: "premdivcls",
                    imgclass: "premimgcls",
                    imgid: "asha",
                    imgsrc: "",
                }

            ]
        }]
    },
    //slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg9",
        quesansblock: [{
            qadiv: "quesdiv",
            qacls: "ques",
            qatxt: data.string.clickplay,
            selectbox: [
                {
                    selectcls: "commonbtn firstdiv firstans",
                    selecttxt: data.string.o
                },
                {
                    selectcls: "commonbtn seconddiv",
                    selecttxt: data.string.s
                },
                {
                    selectcls: "commonbtn thirddiv",
                    selecttxt: data.string.n
                },
                {
                    selectcls: "commonbtn fourthdiv secondans",
                    selecttxt: data.string.t
                },
                {
                    selectcls: "commonbtn fifthdiv",
                    selecttxt: data.string.k
                }
            ],
            ansbox:[
                {
                    selectcls: "commonans ansfirst",
                    selecttxt: data.string.p
                },
                {
                    selectcls: "commonans anssecond",
                    selecttxt: ""//data.string.i
                },
                {
                    selectcls: "commonans ansthird",
                    selecttxt: ""//data.string.n
                }
            ]
        }],
        imghintblock:[{
            imghintdiv:"hintmaindiv zoomin",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "hintdiv",
                        imgclass: "hintimg",
                        imgid: "potImg",
                        imgsrc: ""
                    },
                    {
                        imgdiv: "div1",
                        imgclass: "audiodiv",
                        imgid: "audioicon",
                        imgsrc: "",
                    }
                ]
            }]
        }],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"rhinodiv",
                    imgclass: "rhinoimg",
                    imgid: "rhino",
                    imgsrc: "",
                },
                {
                    imgdiv:"leoparddiv",
                    imgclass: "leopardimg",
                    imgid: "leopard",
                    imgsrc: "",
                },
                {
                    imgdiv:"redpandadiv",
                    imgclass: "redpandaimg",
                    imgid: "redpanda",
                    imgsrc: "",
                },
                {
                    imgdiv:"monkeydiv",
                    imgclass: "monkeyimg",
                    imgid: "monkey",
                    imgsrc: "",
                },
                {
                    imgdiv:"crocodilediv",
                    imgclass: "crocodileimg",
                    imgid: "crocodile",
                    imgsrc: "",
                }
            ]
        }]
    },
    // slidelast
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg10",
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"rhinolastdiv",
                    imgclass: "rhinogifimg",
                    imgid: "rhinogifimg",
                    imgsrc: "",
                },
                {
                    imgdiv:"leopardlastdiv",
                    imgclass: "leopardgifimg",
                    imgid: "leopardgifimg",
                    imgsrc: "",
                },
                {
                    imgdiv:"redpandalastdiv",
                    imgclass: "redpandagifimg",
                    imgid: "redpandagifimg",
                    imgsrc: "",
                },
                {
                    imgdiv:"monkeylastdiv",
                    imgclass: "monkeygifimg",
                    imgid: "monkeygifimg",
                    imgsrc: "",
                },
                {
                    imgdiv:"crocodilelastdiv",
                    imgclass: "crocodilegifimg",
                    imgid: "crocodilegifimg",
                    imgsrc: "",
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $('#activity-page-prev-btn-enabled');
    var $refreshBtn= $("#activity-page-refresh-btn");

    var countNext = 0;

    var $total_page = content.length;
    // loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound="";
    var sound="";

    var endpageex =  new EndPageofExercise();
    var message =  "Yay!!! We just learned how to make words.";
    endpageex.init(5);
    // var lampTemplate = new LampTemplate();
    // lampTemplate.init($total_page-1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "ex", src: $ref + "css/playtimetemplate.css", type: createjs.AbstractLoader.CSS},
            {id: "ex1", src: $ref + "css/playtime.css", type: createjs.AbstractLoader.CSS},
             {id: "audioicon", src: "images/speaker.png", type: createjs.AbstractLoader.IMAGE},
             {id: "binImg", src:imgpath+"bin.png", type: createjs.AbstractLoader.IMAGE},
             {id: "catImg", src:imgpath+"cat.png", type: createjs.AbstractLoader.IMAGE},
             {id: "jugImg", src:imgpath+"jug.png", type: createjs.AbstractLoader.IMAGE},
             {id: "penImg", src:imgpath+"pen.png", type: createjs.AbstractLoader.IMAGE},
             {id: "potImg", src:imgpath+"pot.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rhino", src:imgpath+"rh01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "leopard", src:imgpath+"l01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "redpanda", src:imgpath+"r01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "monkey", src:imgpath+"monkey.png", type: createjs.AbstractLoader.IMAGE},
             {id: "crocodile", src:imgpath+"cr01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "speech", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
             {id: "asha", src: imgpath+"sarala01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "premtalking", src: imgpath+"prem01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "premtalkinghand", src: imgpath+"prem-talking.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},

             {id: "rhinogifimg", src: imgpath+"rhino-on-keyboard.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "leopardgifimg", src: imgpath+"snow-leopardess-on-sax01.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "redpandagifimg", src: imgpath+"red-panda-on-recorder.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "monkeygifimg", src: imgpath+"sundari-maracas.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "crocodilegifimg", src: imgpath+"croc-and-congas.gif", type: createjs.AbstractLoader.IMAGE},

             //sounds
              {id: "sound_1", src: soundAsset+"1.ogg"},
              {id: "sound_2", src: soundAsset+"2.ogg"},
              {id: "sound_3", src: soundAsset+"BIN.ogg"},
              {id: "sound_4", src: soundAsset+"CAT.ogg"},
              {id: "sound_5", src: soundAsset+"JUG.ogg"},
              {id: "sound_6", src: soundAsset+"3.ogg"},
              {id: "sound_7", src: soundAsset+"PEN.ogg"},
              {id: "sound_8", src: soundAsset+"POT.ogg"},
              {id:"sound_11", src: soundAsset+"4.ogg"},
              {id: "sound_9", src: soundAsset+"drum.ogg"},
              {id: "sound_10", src: soundAsset+"flute.ogg"},
              {id: "sound_12", src: soundAsset+"sax.ogg"},
              {id: "sound_13", src: soundAsset+"piano.ogg"},
              {id: "sound_14", src: soundAsset+"maracas.ogg"},
              {id: "sound_15", src: soundAsset+"songLastPage.ogg"},
              {id: "sound_16", src: soundAsset+"incorrect.ogg"},
              {id: "sound_17", src: soundAsset+"correct.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ?
                ole.footerNotificationHandler.lessonEndSetNotification() :
                ole.footerNotificationHandler.pageEndSetNotification();
        }
    }


    // function navigationcontroller(islastpageflag) {
    //     typeof islastpageflag === "undefined" ?
    //         islastpageflag = false :
    //         typeof islastpageflag != 'boolean' ?
    //             alert("NavigationController : Hi Master, please provide a boolean parameter") :
    //             null;
    //
    //     if (countNext == 0 && $total_page != 1) {
    //         $nextBtn.show(0);
    //     }
    //     else if ($total_page == 1) {
    //         $nextBtn.css('display', 'none');
    //
    //         ole.footerNotificationHandler.lessonEndSetNotification();
    //     }
    //     else if (countNext > 0 && countNext < $total_page - 1) {
    //
    //         $nextBtn.show(0);
    //     }
    //     else if (countNext == $total_page - 2) {
    //
    //         $nextBtn.css('display', 'none');
    //         // if lastpageflag is true
    //         ole.footerNotificationHandler.pageEndSetNotification();
    //     }
    //
    // }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
firstPagePlayTime(countNext);
        put_image(content, countNext);
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');
        switch (countNext){
            case 0:
                // sound="sound_1";
                // sound_player("sound_1",true);
                break;
            case 1:
                sound="sound_2";
                sound_player("sound_2",true);
                break;
            case 2:
                sound="sound_3";
                sound_player("sound_3",false);
                put_image2(content, countNext);
                shufflehint("I", "N");
                checkans();
                break;
            case 3:
                sound="sound_4";
                sound_player("sound_4",false);
                put_image2(content, countNext);
                shufflehint("A", "T");
                checkans();
                break;
            case 4:
                sound="sound_5";
                sound_player("sound_5",false);
                put_image2(content, countNext);
                shufflehint("U", "G");
                checkans();
                break;
            case 5:
                sound="sound_6";
                sound_player("sound_6",true);
                break;
            case 6:
                sound="sound_7";
                sound_player("sound_7",false);
                put_image2(content, countNext);
                shufflehint("E", "N");
                checkans();
                break;
            case 7:
                sound="sound_11";
                sound_player("sound_11",true);
                break;
            case 8:
                sound="sound_8";
                sound_player("sound_8",false);
                put_image2(content, countNext);
                shufflehint("O", "T");
                checkans();
                break;
            case 9:
                sound="sound_15";
                sound_player("sound_15", true);
                var intervalid=setInterval(function() {
                    sound_player("sound_15", true);
                },4000);
                endpageex.endpage(message);
                break;
            default:
                navigationcontroller();
                break;
        }
       $('.audiodiv').click(function(){
			sound_player(sound,false);
			});

    }


    function sound_player(sound_id,navigate,imgCls,imgSrc) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():"";
            if(imgCls && imgSrc){
                $("."+imgCls).attr("src",imgSrc);
            }
        });
    }



    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('imghintblock')) {
            var contentCount=content[count].imghintblock[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }
    function templateCaller() {

        // $nextBtn.css('display', 'none');

         //$prevBtn.css('display','none');
        // generaltemplate();


       // navigationcontroller();

        loadTimelineProgress($total_page, countNext + 1);

        generaltemplate();
         /*
        // for (var i = 0; i < content.length; i++) {
        //     slides(i);
        //     $($('.totalsequence')[i]).html(i);
        //     $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
        //         "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
        // }
        // function slides(i){
        //     $($('.totalsequence')[i]).click(function(){
        //         countNext = i;
        //         templateCaller();
        //     });
        //

      }*/


    }
// );




$prevBtn.on('click', function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
     previous slide button hide the footernotification */
//     countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
});

$refreshBtn.click(function(){
    templateCaller();
});
$nextBtn.on('click', function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch(countNext) {
        default:
            countNext++;
            templateCaller();
            break;
    }
});
function checkans(){
    var animate=0;
  $(".commonbtn ").on("click",function () {
      if($(this).hasClass("firstans")||$(this).hasClass("secondans") ) {
          $(this).attr("disabled", "disabled");
          $(this).addClass("correctans");
          if ($(this).hasClass("firstans")){
              $(".anssecond").text($(this).text());
              animate++;
          }
          else {
            $(".ansthird").text($(this).text());
            animate++;

          }
          $(this).prepend("<div class='correctWrongDiv'><img class='correctWrongImg' src='images/right.png'/></div>")
          current_sound = createjs.Sound.play("sound_17");
      }
      else{
          $(this).addClass("wrongans").attr("disabled","disabled");
          $(this).prepend("<div class='correctWrongDiv'><img class='correctWrongImg' src='images/wrong.png'/></div>")
          current_sound = createjs.Sound.play("sound_16");
      }
      if(animate==2){
          $(".commonbtn").attr("disabled","disabled");
          $(".commonbtn").addClass("disablePointer");
          animateImage();
          navigationcontroller();
      }
  });
}
//2 -> ,3,4,6,8
function animateImage(){
    if(countNext=="2"){
      $(".rhinoimg").attr("src",imgpath+"rhino-on-keyboard.gif");
      $(".rhinoimg").addClass("animal");
      sound_player("sound_13",true,"rhinoimg",imgpath+"rh01.png");
    }
    if(countNext=="3"){
        $(".leopardimg").attr("src",imgpath+"snow-leopardess-on-sax01.gif");
        $(".leopardimg").addClass("animal");
        sound_player("sound_12",true,"leopardimg",imgpath+"l01.png");
    }
    if(countNext=="4"){
        $(".redpandaimg").attr("src",imgpath+"red-panda-on-recorder.gif");
        $(".redpandaimg").addClass("animal");
        sound_player("sound_10",true,"redpandaimg",imgpath+"r01.png");

    }
    if(countNext=="6"){
        $(".monkeyimg").attr("src",imgpath+"sundari-maracas.gif");
        $(".monkeyimg").addClass("animal");
        sound_player("sound_14",true,"monkeyimg",imgpath+"monkey.png");

    }
    if(countNext=="8"){
        $(".crocodileimg").attr("src",imgpath+"croc-and-congas.gif");
        $(".crocodileimg").addClass("animal");
        sound_player("sound_9",true,"crocodileimg",imgpath+"cr01.png");

    }
}


function shufflehint(firstans,secondans) {
    var btndiv = $(".selectdiv");

    for (var i = btndiv.children().length; i >= 0; i--) {
         btndiv.append(btndiv.children().eq(Math.random() * i | 0));
    }
    btndiv.children().removeClass();
    var a = ["commonbtn firstdiv","commonbtn seconddiv","commonbtn thirddiv","commonbtn fourthdiv","commonbtn fifthdiv"]
    btndiv.children().each(function (index) {
        var $this = $(this)
        $this.addClass(a[index]);
        if($this.text().trim()==firstans){
            $this.addClass("firstans")
        }
        if($this.text().trim()==secondans){
            $this.addClass("secondans")
        }
    });

}
function movepremashaImg() {
    setTimeout(function () {
        $(".moveyes").attr("src", imgpath + "sarala01.png");
        $(".moveyesprem").attr("src", imgpath + "prem01.png");
        setTimeout(function () {
            $(".moveyes").attr("src", imgpath + "sarala02.png");
            $(".moveyesprem").attr("src", imgpath + "prem02.png");
            setTimeout(function () {
                $(".moveyes").attr("src", imgpath + "sarala03.png");
                $(".moveyesprem").attr("src", imgpath + "prem03.png");
                setTimeout(function () {
                    $(".moveyes").attr("src", imgpath + "sarala01.png");
                    $(".moveyesprem").attr("src", imgpath + "prem01.png");
                }, 500);
            }, 500);
        }, 500);
    }, 500);
}
});

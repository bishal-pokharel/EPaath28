var imgpath = $ref+"/images/";
var soundAsset = $ref + "/sounds/";
var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        extratextblock: [
            {
            	textclass: "playtime",
                textdata: data.string.pt
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "rhinodance",
                    imgid: "rhinodance",
                    imgsrc: "",
                },
                {
                    imgclass: "squirreldance",
                    imgid: "squirreldance",
                    imgsrc: "",
                },
                {
                    imgclass: "ptbg",
                    imgid: "ptbg",
                    imgsrc: "",
                }]
        }]
    },
    // slide1
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg1",
		uppertextblock : [{
			textclass : "pttxt",
			textdata : data.string.pttxt
		}],
		imageblock:[{
            imagestoshow: [{
	                imgclass: "housebg",
	                imgid: "housebg",
	                imgsrc: "",
               },
                {
                    imgclass: "img grayscld wall",
                    imgid: "wall",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld window01",
                    imgid: "window01",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld window02",
                    imgid: "window02",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld doorpt",
                    imgid: "doorpt",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld pole",
                    imgid: "pole",
                    imgsrc: "",
                }
                ]
        }],
		exerciseblock : [{

			exeoptions : [{
				forshuffle: "class2 cir",
				optdata : data.string.pttxt1
			}, {
				forshuffle: "class1 rct",
				optdata : data.string.pttxt2
			}, {
				forshuffle: "class3 tri",
				optdata : data.string.pttxt3
			}]
		}]
	},
// slide2
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg1",
		uppertextblock : [{
			textclass : "pttxt",
			textdata : data.string.pttxt
		}],
		imageblock:[{
            imagestoshow: [{
	                imgclass: "housebg",
	                imgid: "housebg",
	                imgsrc: "",
               },
                {
                    imgclass: "wall",
                    imgid: "wall",
                    imgsrc: "",
                },
                {
                    imgclass: "window01",
                    imgid: "window01",
                    imgsrc: "",
                },
                {
                    imgclass: "window02",
                    imgid: "window02",
                    imgsrc: "",
                },
                {
                    imgclass: "doorpt",
                    imgid: "doorpt",
                    imgsrc: "",
                },
                {
                    imgclass: "pole",
                    imgid: "pole",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld rooftop",
                    imgid: "rooftop",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld star",
                    imgid: "star",
                    imgsrc: "",
                }
                ]
        }],
		exerciseblock : [{

			exeoptions : [{
				forshuffle: "class1 tri",
				optdata : data.string.pttxt3
			}, {
				forshuffle: "class3 rct",
				optdata : data.string.pttxt2
			}, {
				forshuffle: "class2 cir",
				optdata : data.string.pttxt1
			}]
		}]
	},
// slide3
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg1",
		uppertextblock : [{
			textclass : "pttxt",
			textdata : data.string.pttxt
		}],
		imageblock:[{
            imagestoshow: [{
	                imgclass: "housebg",
	                imgid: "housebg",
	                imgsrc: "",
               },
                {
                    imgclass: "wall",
                    imgid: "wall",
                    imgsrc: "",
                },
                {
                    imgclass: "window01",
                    imgid: "window01",
                    imgsrc: "",
                },
                {
                    imgclass: "window02",
                    imgid: "window02",
                    imgsrc: "",
                },
                {
                    imgclass: "doorpt",
                    imgid: "doorpt",
                    imgsrc: "",
                },
                {
                    imgclass: "pole",
                    imgid: "pole",
                    imgsrc: "",
                },
                {
                    imgclass: "rooftop",
                    imgid: "rooftop",
                    imgsrc: "",
                },
                {
                    imgclass: "star",
                    imgid: "star",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld sun",
                    imgid: "sun",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld circlept",
                    imgid: "circlept",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld orange_circle",
                    imgid: "orange_circle",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld orangecircle oc1",
                    imgid: "orange_circle",
                    imgsrc: "",
                },
                 {
                    imgclass: "img grayscld orangecircle oc2",
                    imgid: "orange_circle",
                    imgsrc: "",
                },
                 {
                    imgclass: "img grayscld orangecircle oc3",
                    imgid: "orange_circle",
                    imgsrc: "",
                },
                 {
                    imgclass: "img grayscld orangecircle oc4",
                    imgid: "orange_circle",
                    imgsrc: "",
                },
                {
                    imgclass: "img grayscld orangecircle oc5",
                    imgid: "orange_circle",
                    imgsrc: "",
                },
                 {
                    imgclass: "img grayscld orangecircle oc6",
                    imgid: "orange_circle",
                    imgsrc: "",
                },{
                  imgclass: "grass",
                  imgid: "grass",
                  imgsrc: "",
                }

                ]
        }],
		exerciseblock : [{

			exeoptions : [{
				forshuffle: "class1 cir",
				optdata : data.string.pttxt1
			}, {
				forshuffle: "class2 rct",
				optdata : data.string.pttxt2
			}, {
				forshuffle: "class3 tri",
				optdata : data.string.pttxt3
			}]
		}]
},
	// slide4
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg2",
		imageblock:[{
           imagestoshow: [{
	                imgclass: "ptbg1",
	                imgid: "housebg",
	                imgsrc: "",
               },
                {
                    imgclass: "wallpt",
                    imgid: "wall",
                    imgsrc: "",
                },
                {
                    imgclass: "window01pt",
                    imgid: "window01",
                    imgsrc: "",
                },
                {
                    imgclass: "window02pt",
                    imgid: "window02",
                    imgsrc: "",
                },
                {
                    imgclass: "doorptpt",
                    imgid: "doorpt",
                    imgsrc: "",
                },
                {
                    imgclass: "polept",
                    imgid: "pole",
                    imgsrc: "",
                },
                {
                    imgclass: "rooftoppt",
                    imgid: "rooftop",
                    imgsrc: "",
                },
                {
                    imgclass: "starpt",
                    imgid: "star",
                    imgsrc: "",
                },
                {
                    imgclass: "sunpt",
                    imgid: "sun",
                    imgsrc: "",
                },
                {
                    imgclass: "circleptpt",
                    imgid: "circlept",
                    imgsrc: "",
                },
                {
                    imgclass: "orange_circlept",
                    imgid: "orange_circle",
                    imgsrc: "",
                },
                 {
                    imgclass: "flying-birds",
                    imgid: "flying-birds",
                    imgsrc: "",
                },{
                  imgclass: "grass1",
                  imgid: "grass",
                  imgsrc: "",
                },{
                  imgclass: "grass2",
                  imgid: "grass1",
                  imgsrc: "",
                }
                ]
        }]
	}
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound="";
    var sound="";

    var endpageex =  new EndPageofExercise();
    var message =  "Yay!!! We just built a house!";
    //endpageex.init(5);
    // var lampTemplate = new LampTemplate();
    // lampTemplate.init($total_page-1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "ex", src: $ref + "css/playtimetemplate.css", type: createjs.AbstractLoader.CSS},
            {id: "ex1", src: $ref + "css/playtime.css", type: createjs.AbstractLoader.CSS},
	         {id: "audioicon", src: "images/speaker.png", type: createjs.AbstractLoader.IMAGE},
	         {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
	         {id: "squirreldance", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
	         {id: "ptbg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
	         {id: "circle", src: imgpath+"circle.png", type: createjs.AbstractLoader.IMAGE},
             {id: "star", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "circlept", src: imgpath+"circlept.png", type: createjs.AbstractLoader.IMAGE},
             {id: "doorpt", src: imgpath+"doorpt.png", type: createjs.AbstractLoader.IMAGE},
             {id: "housebg", src: imgpath+"hosuebg.png", type: createjs.AbstractLoader.IMAGE},
             {id: "orange_circle", src: imgpath+"orange_circle.png", type: createjs.AbstractLoader.IMAGE},
             {id: "pole", src: imgpath+"pole.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rooftop", src: imgpath+"rooftop.png", type: createjs.AbstractLoader.IMAGE},
             {id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
             {id: "wall", src: imgpath+"wall.png", type: createjs.AbstractLoader.IMAGE},
             {id: "window01", src: imgpath+"window01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "window02", src: imgpath+"window02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "wall", src: imgpath+"wall.png", type: createjs.AbstractLoader.IMAGE},
             {id: "book-cover", src: imgpath+"book_cover.png", type: createjs.AbstractLoader.IMAGE},
             {id: "flying-birds", src: imgpath+"flying-birds.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "grass", src: imgpath+"grass.png", type: createjs.AbstractLoader.IMAGE},
             {id: "grass1", src: imgpath+"stones.png", type: createjs.AbstractLoader.IMAGE},


             //sounds
              {id: "circlesnd", src: soundAsset+"Correct These are  circles.ogg"},
              {id: "rectanglesnd", src: soundAsset+"Correct These are  rectangles.ogg"},
              {id: "trainglesnd", src: soundAsset+"Correct These are  traingles.ogg"},
              {id: "tryagain", src: soundAsset+"Oh oh! Try again.ogg"},
              {id: "lastpage", src: soundAsset+"Last.ogg"},
              {id: "sound_2", src: soundAsset+"pt_s1.ogg"},
              {id: "sound_3", src: soundAsset+"songLastPage.ogg"},
     ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
firstPagePlayTime(countNext);
        put_image(content, countNext);
		//tick qns start

		var wrngClicked = false;
		var corrCounter=0;

$(".buttonsel").click(function(){

			/*class 1 is always for the right answer. updates scoreboard and disables other click if
			right answer is clicked*/

			if($(this).hasClass("class1") && $(this).hasClass("forhover")){

				$(".buttonsel").removeClass('forhover');
                corrCounter++;
				checkCrCount();
				//corr_action();
				//play_correct_incorrect_sound(1);
				if($(this).hasClass("cir")){
			    current_sound = createjs.Sound.play("circlesnd");
                current_sound.play();
				}
				else if($(this).hasClass("rct")){
			    current_sound = createjs.Sound.play("rectanglesnd");
                current_sound.play();
				}
				else if($(this).hasClass("tri")){
			    current_sound = createjs.Sound.play("trainglesnd");
                current_sound.play();
				}
				($('.img').removeClass("grayscld").addClass("slwanm"));
				$(this).siblings(".corctopt").show(0);
				$nextBtn.show(0);
			  }
		else if(($(this).hasClass("class2")||$(this).hasClass("class3")) && $(this).hasClass("forhover")){
				//testin.update(false);
				$(this).removeClass('forhover');
					if($(this).hasClass("cir")){
					    current_sound = createjs.Sound.play("tryagain");
			            adoTckCntrl();
					}
					else if($(this).hasClass("rct")){
					    current_sound = createjs.Sound.play("tryagain");
			            adoTckCntrl();
					}
					else if($(this).hasClass("tri")){
					    current_sound = createjs.Sound.play("tryagain");
			            adoTckCntrl();
					}
				$(this).siblings(".wrngopt").show(0);
				wrngClicked = true;

			}
	function adoTckCntrl(){
        $(".buttonsel").removeClass('forhover');
        current_sound.on('complete', function(){
		 $(".buttonsel").addClass('forhover');
			});
		}

	function checkCrCount(){
		if(corrCounter==3){
				$('.buttonsel').removeClass('forhover forhoverimg');
                    console.log(corrCounter);
				navigationcontroller();
			}
		}
});

		//tick qns end
        switch (countNext){
            case 0:
           		current_sound.on('complete', function(){
				 $nextBtn.show(0);
				});
                break;
            case 1:
        		$(".buttonsel").removeClass('forhover');
                sound_player("sound_2");
                current_sound.on('complete', function(){
                	$(".buttonsel").addClass('forhover');
				});
                break;
            case 2:
        		$(".buttonsel").removeClass('forhover');
                sound_player("sound_2");
                current_sound.on('complete', function(){
                	$(".buttonsel").addClass('forhover');
				});
                break;
            case 3:
        		$(".buttonsel").removeClass('forhover');
                sound_player("sound_2");
                current_sound.on('complete', function(){
                	$(".buttonsel").addClass('forhover');
				});
                break;
            case 4:
            sound_player("lastpage");
           		current_sound.on('complete', function(){
	              	sound="sound_3";
	                sound_player("sound_3", true);
	                var intervalid=setInterval(function() {
	                    sound_player("sound_3", true);
	                },4000);
				});

                 endpageex.endpage(message);
                break;
            default:
                navigationcontroller();
                break;
        }
    }



    function sound_player(sound_id,navigate,imgCls,imgSrc) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();


    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('imghintblock')) {
            var contentCount=content[count].imghintblock[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }


    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
        		loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                // lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });

});

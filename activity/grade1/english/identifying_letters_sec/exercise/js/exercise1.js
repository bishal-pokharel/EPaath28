var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref+"/exercise/sounds/";

var content1=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-ex',

		uppertextblockadditionalclass: 'playtime-text',
		uppertextblock:[{
			textdata: data.string.e1text1,
			textclass: "",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-playtime",
					imgsrc : '',
					imgid : 'bg'
				},{
					imgclass : "rhino",
					imgsrc : '',
					imgid : 'rhino'
				},{
					imgclass : "squirrel",
					imgsrc : '',
					imgid : 'squirrel'
				}
			]
		}],
	}];

var content2=[
	// slide1
	{
		sound: '1',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'ins-container',
		uppertextblock:[{
			textdata: data.string.e1ins,
			textclass: "e1-ins",
			datahighlightflag : true,
			datahighlightcustomclass : 'yellow-text',
		}],
		extratextblock:[{
			textdata: '',
			textclass: "bg-grass",
		}],
		imagetextblockadditionalclass:'questionblock',
		imagetextblock:[{
			textdata: data.string.e1ins2,
			textclass: "",
			imgclass : "speaker",
			imgsrc : '',
			imgid : 'sun'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sun sun-1",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "sun sunring-1",
					imgsrc : '',
					imgid : 'sun-2'
				},{
					imgclass : "sun sunring-2",
					imgsrc : '',
					imgid : 'sun-3'
				},{
					imgclass : "bird",
					imgsrc : '',
					imgid : 'insect'
				},{
					imgclass : "butterfly",
					imgsrc : '',
					imgid : 'butterfly'
				}
			]
		}],

		imagedivblock:[{
			imagedivclass: 'shirtdiv shirt-1',
			imagestoshow : [
				{
					imgclass : "shirt-image si-1",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-1 correct-ans',
			textdata: data.string.texti
		},{
			imagedivclass: 'shirtdiv shirt-2',
			imagestoshow : [
				{
					imgclass : "shirt-image si-2",
					imgsrc : '',
					imgid : 'shirt-2'
				}
			],
			textdivclass: 'shirttext shirt-text-2',
			textdata: data.string.textj
		},{
			imagedivclass: 'shirtdiv shirt-3',
			imagestoshow : [
				{
					imgclass : "shirt-image si-3",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-3',
			textdata: data.string.textk
		},{
			imagedivclass: 'shirtdiv shirt-4',
			imagestoshow : [
				{
					imgclass : "shirt-image si-4",
					imgsrc : '',
					imgid : 'shirt-3'
				}
			],
			textdivclass: 'shirttext shirt-text-4',
			textdata: data.string.textl
		}],
		svgblock: [{
			svgblock: 'rope-svg',
		}],
	},
	// slide2
	{
		sound: '2',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'ins-container',
		uppertextblock:[{
			textdata: data.string.e1ins,
			textclass: "e1-ins",
			datahighlightflag : true,
			datahighlightcustomclass : 'yellow-text',
		}],
		extratextblock:[{
			textdata: '',
			textclass: "bg-grass",
		}],
		imagetextblockadditionalclass:'questionblock',
		imagetextblock:[{
			textdata: data.string.e1ins2,
			textclass: "",
			imgclass : "speaker",
			imgsrc : '',
			imgid : 'sun'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sun sun-1",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "sun sunring-1",
					imgsrc : '',
					imgid : 'sun-2'
				},{
					imgclass : "sun sunring-2",
					imgsrc : '',
					imgid : 'sun-3'
				},{
					imgclass : "bird",
					imgsrc : '',
					imgid : 'jug'
				},{
					imgclass : "butterfly",
					imgsrc : '',
					imgid : 'butterfly'
				}
			]
		}],

		imagedivblock:[{
			imagedivclass: 'shirtdiv shirt-1',
			imagestoshow : [
				{
					imgclass : "shirt-image si-1",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-1',
			textdata: data.string.texti
		},{
			imagedivclass: 'shirtdiv shirt-2',
			imagestoshow : [
				{
					imgclass : "shirt-image si-2",
					imgsrc : '',
					imgid : 'shirt-2'
				}
			],
			textdivclass: 'shirttext shirt-text-2 correct-ans',
			textdata: data.string.textj
		},{
			imagedivclass: 'shirtdiv shirt-3',
			imagestoshow : [
				{
					imgclass : "shirt-image si-3",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-3',
			textdata: data.string.textk
		},{
			imagedivclass: 'shirtdiv shirt-4',
			imagestoshow : [
				{
					imgclass : "shirt-image si-4",
					imgsrc : '',
					imgid : 'shirt-3'
				}
			],
			textdivclass: 'shirttext shirt-text-4',
			textdata: data.string.textl
		}],
		svgblock: [{
			svgblock: 'rope-svg',
		}],
	},
	// slide3
	{
		sound: '3',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'ins-container',
		uppertextblock:[{
			textdata: data.string.e1ins,
			textclass: "e1-ins",
			datahighlightflag : true,
			datahighlightcustomclass : 'yellow-text',
		}],
		extratextblock:[{
			textdata: '',
			textclass: "bg-grass",
		}],
		imagetextblockadditionalclass:'questionblock',
		imagetextblock:[{
			textdata: data.string.e1ins2,
			textclass: "",
			imgclass : "speaker",
			imgsrc : '',
			imgid : 'sun'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sun sun-1",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "sun sunring-1",
					imgsrc : '',
					imgid : 'sun-2'
				},{
					imgclass : "sun sunring-2",
					imgsrc : '',
					imgid : 'sun-3'
				},{
					imgclass : "bird",
					imgsrc : '',
					imgid : 'kite'
				},{
					imgclass : "butterfly",
					imgsrc : '',
					imgid : 'butterfly'
				}
			]
		}],

		imagedivblock:[{
			imagedivclass: 'shirtdiv shirt-1',
			imagestoshow : [
				{
					imgclass : "shirt-image si-1",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-1',
			textdata: data.string.texti
		},{
			imagedivclass: 'shirtdiv shirt-2',
			imagestoshow : [
				{
					imgclass : "shirt-image si-2",
					imgsrc : '',
					imgid : 'shirt-2'
				}
			],
			textdivclass: 'shirttext shirt-text-2',
			textdata: data.string.textj
		},{
			imagedivclass: 'shirtdiv shirt-3',
			imagestoshow : [
				{
					imgclass : "shirt-image si-3",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-3  correct-ans',
			textdata: data.string.textk
		},{
			imagedivclass: 'shirtdiv shirt-4',
			imagestoshow : [
				{
					imgclass : "shirt-image si-4",
					imgsrc : '',
					imgid : 'shirt-3'
				}
			],
			textdivclass: 'shirttext shirt-text-4',
			textdata: data.string.textl
		}],
		svgblock: [{
			svgblock: 'rope-svg',
		}],
	},
	// slide4
	{
		sound: '4',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'ins-container',
		uppertextblock:[{
			textdata: data.string.e1ins,
			textclass: "e1-ins",
			datahighlightflag : true,
			datahighlightcustomclass : 'yellow-text',
		}],
		extratextblock:[{
			textdata: '',
			textclass: "bg-grass",
		}],
		imagetextblockadditionalclass:'questionblock',
		imagetextblock:[{
			textdata: data.string.e1ins2,
			textclass: "",
			imgclass : "speaker",
			imgsrc : '',
			imgid : 'sun'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sun sun-1",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "sun sunring-1",
					imgsrc : '',
					imgid : 'sun-2'
				},{
					imgclass : "sun sunring-2",
					imgsrc : '',
					imgid : 'sun-3'
				},{
					imgclass : "bird",
					imgsrc : '',
					imgid : 'lion'
				},{
					imgclass : "butterfly",
					imgsrc : '',
					imgid : 'butterfly'
				}
			]
		}],

		imagedivblock:[{
			imagedivclass: 'shirtdiv shirt-1',
			imagestoshow : [
				{
					imgclass : "shirt-image si-1",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-1',
			textdata: data.string.texti
		},{
			imagedivclass: 'shirtdiv shirt-2',
			imagestoshow : [
				{
					imgclass : "shirt-image si-2",
					imgsrc : '',
					imgid : 'shirt-2'
				}
			],
			textdivclass: 'shirttext shirt-text-2',
			textdata: data.string.textj
		},{
			imagedivclass: 'shirtdiv shirt-3',
			imagestoshow : [
				{
					imgclass : "shirt-image si-3",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-3 ',
			textdata: data.string.textk
		},{
			imagedivclass: 'shirtdiv shirt-4',
			imagestoshow : [
				{
					imgclass : "shirt-image si-4",
					imgsrc : '',
					imgid : 'shirt-3'
				}
			],
			textdivclass: 'shirttext shirt-text-4  correct-ans',
			textdata: data.string.textl
		}],
		svgblock: [{
			svgblock: 'rope-svg',
		}],
	},
	// slide5
	{
		sound: '5',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'ins-container',
		uppertextblock:[{
			textdata: data.string.e1ins,
			textclass: "e1-ins",
			datahighlightflag : true,
			datahighlightcustomclass : 'yellow-text',
		}],
		extratextblock:[{
			textdata: '',
			textclass: "bg-grass",
		}],
		imagetextblockadditionalclass:'questionblock',
		imagetextblock:[{
			textdata: data.string.e1ins2,
			textclass: "",
			imgclass : "speaker",
			imgsrc : '',
			imgid : 'sun'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sun sun-1",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "sun sunring-1",
					imgsrc : '',
					imgid : 'sun-2'
				},{
					imgclass : "sun sunring-2",
					imgsrc : '',
					imgid : 'sun-3'
				},{
					imgclass : "bird",
					imgsrc : '',
					imgid : 'mango'
				},{
					imgclass : "butterfly",
					imgsrc : '',
					imgid : 'butterfly'
				}
			]
		}],

		imagedivblock:[{
			imagedivclass: 'shirtdiv shirt-1 ',
			imagestoshow : [
				{
					imgclass : "shirt-image si-1",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-1',
			textdata: data.string.textn
		},{
			imagedivclass: 'shirtdiv shirt-2',
			imagestoshow : [
				{
					imgclass : "shirt-image si-2",
					imgsrc : '',
					imgid : 'shirt-2'
				}
			],
			textdivclass: 'shirttext shirt-text-2 correct-ans',
			textdata: data.string.textm
		},{
			imagedivclass: 'shirtdiv shirt-3',
			imagestoshow : [
				{
					imgclass : "shirt-image si-3",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-3',
			textdata: data.string.texto
		},{
			imagedivclass: 'shirtdiv shirt-4',
			imagestoshow : [
				{
					imgclass : "shirt-image si-4",
					imgsrc : '',
					imgid : 'shirt-3'
				}
			],
			textdivclass: 'shirttext shirt-text-4',
			textdata: data.string.textk
		}],
		svgblock: [{
			svgblock: 'rope-svg',
		}],
	},
	// slide6
	{
		sound: '6',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'ins-container',
		uppertextblock:[{
			textdata: data.string.e1ins,
			textclass: "e1-ins",
			datahighlightflag : true,
			datahighlightcustomclass : 'yellow-text',
		}],
		extratextblock:[{
			textdata: '',
			textclass: "bg-grass",
		}],
		imagetextblockadditionalclass:'questionblock',
		imagetextblock:[{
			textdata: data.string.e1ins2,
			textclass: "",
			imgclass : "speaker",
			imgsrc : '',
			imgid : 'sun'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sun sun-1",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "sun sunring-1",
					imgsrc : '',
					imgid : 'sun-2'
				},{
					imgclass : "sun sunring-2",
					imgsrc : '',
					imgid : 'sun-3'
				},{
					imgclass : "bird",
					imgsrc : '',
					imgid : 'nest'
				},{
					imgclass : "butterfly",
					imgsrc : '',
					imgid : 'butterfly'
				}
			]
		}],

		imagedivblock:[{
			imagedivclass: 'shirtdiv shirt-1',
			imagestoshow : [
				{
					imgclass : "shirt-image si-1",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-1',
			textdata: data.string.textk
		},{
			imagedivclass: 'shirtdiv shirt-2',
			imagestoshow : [
				{
					imgclass : "shirt-image si-2",
					imgsrc : '',
					imgid : 'shirt-2'
				}
			],
			textdivclass: 'shirttext shirt-text-2',
			textdata: data.string.textl
		},{
			imagedivclass: 'shirtdiv shirt-3',
			imagestoshow : [
				{
					imgclass : "shirt-image si-3",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-3 correct-ans',
			textdata: data.string.textn
		},{
			imagedivclass: 'shirtdiv shirt-4',
			imagestoshow : [
				{
					imgclass : "shirt-image si-4",
					imgsrc : '',
					imgid : 'shirt-3'
				}
			],
			textdivclass: 'shirttext shirt-text-4',
			textdata: data.string.textm
		}],
		svgblock: [{
			svgblock: 'rope-svg',
		}],
	},
	// slide7
	{
		sound: '7',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'ins-container',
		uppertextblock:[{
			textdata: data.string.e1ins,
			textclass: "e1-ins",
			datahighlightflag : true,
			datahighlightcustomclass : 'yellow-text',
		}],
		extratextblock:[{
			textdata: '',
			textclass: "bg-grass",
		}],
		imagetextblockadditionalclass:'questionblock',
		imagetextblock:[{
			textdata: data.string.e1ins2,
			textclass: "",
			imgclass : "speaker",
			imgsrc : '',
			imgid : 'sun'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sun sun-1",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "sun sunring-1",
					imgsrc : '',
					imgid : 'sun-2'
				},{
					imgclass : "sun sunring-2",
					imgsrc : '',
					imgid : 'sun-3'
				},{
					imgclass : "bird",
					imgsrc : '',
					imgid : 'orange'
				},{
					imgclass : "butterfly",
					imgsrc : '',
					imgid : 'butterfly'
				}
			]
		}],

		imagedivblock:[{
			imagedivclass: 'shirtdiv shirt-1',
			imagestoshow : [
				{
					imgclass : "shirt-image si-1",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-1',
			textdata: data.string.textk
		},{
			imagedivclass: 'shirtdiv shirt-2',
			imagestoshow : [
				{
					imgclass : "shirt-image si-2",
					imgsrc : '',
					imgid : 'shirt-2'
				}
			],
			textdivclass: 'shirttext shirt-text-2',
			textdata: data.string.textl
		},{
			imagedivclass: 'shirtdiv shirt-3',
			imagestoshow : [
				{
					imgclass : "shirt-image si-3",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-3 correct-ans',
			textdata: data.string.texto
		},{
			imagedivclass: 'shirtdiv shirt-4',
			imagestoshow : [
				{
					imgclass : "shirt-image si-4",
					imgsrc : '',
					imgid : 'shirt-3'
				}
			],
			textdivclass: 'shirttext shirt-text-4',
			textdata: data.string.textm
		}],
		svgblock: [{
			svgblock: 'rope-svg',
		}],
	},
	// slide8
	{
		sound: '8',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'ins-container',
		uppertextblock:[{
			textdata: data.string.e1ins,
			textclass: "e1-ins",
			datahighlightflag : true,
			datahighlightcustomclass : 'yellow-text',
		}],
		extratextblock:[{
			textdata: '',
			textclass: "bg-grass",
		}],
		imagetextblockadditionalclass:'questionblock',
		imagetextblock:[{
			textdata: data.string.e1ins2,
			textclass: "",
			imgclass : "speaker",
			imgsrc : '',
			imgid : 'sun'
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sun sun-1",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "sun sunring-1",
					imgsrc : '',
					imgid : 'sun-2'
				},{
					imgclass : "sun sunring-2",
					imgsrc : '',
					imgid : 'sun-3'
				},{
					imgclass : "bird",
					imgsrc : '',
					imgid : 'pig'
				},{
					imgclass : "butterfly",
					imgsrc : '',
					imgid : 'butterfly'
				}
			]
		}],

		imagedivblock:[{
			imagedivclass: 'shirtdiv shirt-1',
			imagestoshow : [
				{
					imgclass : "shirt-image si-1",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-1',
			textdata: data.string.textn
		},{
			imagedivclass: 'shirtdiv shirt-2',
			imagestoshow : [
				{
					imgclass : "shirt-image si-2",
					imgsrc : '',
					imgid : 'shirt-2'
				}
			],
			textdivclass: 'shirttext shirt-text-2',
			textdata: data.string.textm
		},{
			imagedivclass: 'shirtdiv shirt-3',
			imagestoshow : [
				{
					imgclass : "shirt-image si-3",
					imgsrc : '',
					imgid : 'shirt-1'
				}
			],
			textdivclass: 'shirttext shirt-text-3',
			textdata: data.string.textl
		},{
			imagedivclass: 'shirtdiv shirt-4',
			imagestoshow : [
				{
					imgclass : "shirt-image si-4",
					imgsrc : '',
					imgid : 'shirt-3'
				}
			],
			textdivclass: 'shirttext shirt-text-4 correct-ans',
			textdata: data.string.textp
		}],
		svgblock: [{
			svgblock: 'rope-svg',
		}],
	}
];

// content2.shufflearray();
// var content = content2;
var content = content1.concat(content2);


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	newCount = 1;
	var wind_sound = null;
	var sound_letter_arr = ['sound_letter_1', 'sound_letter_2', 'sound_letter_3', 'sound_letter_4', 'sound_letter_5', 'sound_letter_6', 'sound_letter_7', 'sound_letter_8'];

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+"rhino.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rope", src: imgpath+"rope.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "butterfly", src: imgpath+"flyingbutterfly.png", type: createjs.AbstractLoader.IMAGE},
			{id: "butterfly-2", src: imgpath+"flyingbutterfly.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "shirt-1", src: imgpath+"shirt-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shirt-2", src: imgpath+"shirt-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shirt-3", src: imgpath+"shirt-3.png", type: createjs.AbstractLoader.IMAGE},

			{id: "grass", src: imgpath+"grass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speaker", src: imgpath+"speaker.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun-2", src: imgpath+"sunout01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun-3", src: imgpath+"sunout02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "insect", src: imgpath+"../../images/p2/insect.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug", src: imgpath+"../../images/p2/jug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kite", src: imgpath+"../../images/p2/kite.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lion", src: imgpath+"../../images/p2/lion.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+"../../images/p2/mango.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nest", src: imgpath+"../../images/p2/nest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"../../images/p2/orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"../../images/p2/pig.png", type: createjs.AbstractLoader.IMAGE},


			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"playtime_1.ogg"},
			{id: "sound_chime", src: soundAsset+"windchime.ogg"},
			{id: "sound_letter_1", src: soundAsset+"../../sounds/page2/i.ogg"},
			{id: "sound_letter_2", src: soundAsset+"../../sounds/page2/j.ogg"},
			{id: "sound_letter_3", src: soundAsset+"../../sounds/page2/k.ogg"},
			{id: "sound_letter_4", src: soundAsset+"../../sounds/page2/l.ogg"},
			{id: "sound_letter_5", src: soundAsset+"../../sounds/page2/m.ogg"},
			{id: "sound_letter_6", src: soundAsset+"../../sounds/page2/n.ogg"},
			{id: "sound_letter_7", src: soundAsset+"../../sounds/page2/o.ogg"},
			{id: "sound_letter_8", src: soundAsset+"../../sounds/page2/p.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				$nextBtn.show(0);
				 
				// sound_player('sound_1');
				// $('.playtime-button').hover(function(){
					// $('.playtime-text, .playtime-button').css({'transform': 'scale(1.05)'});
				// }, function(){
					// $('.playtime-text, .playtime-button').css({'transform': 'scale(1)'});
				// });
				// $('.playtime-button').click(function(){
					// $nextBtn.trigger('click');
				// });
				break;
			default:
				$('.speaker').attr('src', preload.getResult('speaker').src);
				var letter_arr = [data.string.texti,data.string.textj,data.string.textk,data.string.textl,data.string.textm,data.string.textn,data.string.texto,data.string.textp];
				var temp_arr = letter_arr;
				var ans_idx = letter_arr.indexOf($('.correct-ans').text().trim());
				// console.log(ans_idx);
				temp_arr.splice(ans_idx, 1);
				temp_arr.shufflearray();
				// console.log(temp_arr);
				for(var i=0; i<4; i++){
					var $tempClass = $('.shirttext').eq(i);
					if(!$tempClass.hasClass('correct-ans')){
						$tempClass.children('p').html(temp_arr[i]);
					}
				}
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

				var s = Snap('#rope-svg');
				var path, path_1, path_2, move_to_2, move_to_1;
				var svg = Snap.load(preload.getResult('rope').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					path			= Snap.select('#path01');
					var path2		= Snap.select("#path02");
					path_1		= path.attr('path');
					path_2		= path2.attr('path');
					move_to_2 = function(){
						$('.shirt-1').addClass('shirt-anim-1');
						$('.shirt-2').addClass('shirt-anim-2');
						$('.shirt-3').addClass('shirt-anim-3');
						$('.shirt-4').addClass('shirt-anim-4');
						$('.butterfly').addClass('butterfly-2');
						path.animate({ d: path_2 }, 1000, mina.linear, move_to_1);
					};
					move_to_1 = function(){
						$('.shirt-1').removeClass('shirt-anim-1');
						$('.shirt-2').removeClass('shirt-anim-2');
						$('.shirt-3').removeClass('shirt-anim-3');
						$('.shirt-4').removeClass('shirt-anim-4');
						$('.butterfly').removeClass('butterfly-2');
						path.animate({ d: path_1}, 1000, mina.linear, function(){
							if(newCount != countNext)
								move_to_2();
							console.log(newCount);
						});
					};
					// move_to_2();
				} );
				current_sound = createjs.Sound.play('sound_1');
				current_sound.play();
				current_sound.on("complete", function(){
					sound_player('sound_letter_'+content[countNext].sound);
					$('.speaker').click(function(){
						sound_player('sound_letter_'+content[countNext].sound);
					});
					$('.shirtdiv').click(function(){
						if($(this).children('.shirttext').hasClass('correct-ans')){
							var textData = $(this).children('p').html();
							play_correct_incorrect_sound(1);
							$(this).children('.correct-icon').show(0);
							$(this).children('.shirttext').addClass('correct-answer');
							$('.shirtdiv').css('pointer-events', 'none');
							$('.butterfly').attr('src',  preload.getResult('butterfly-2').src);
							newCount++;
							move_to_2();

							wind_sound = createjs.Sound.play('sound_chime');
							wind_sound.loop= -1;

							if(countNext < $total_page-1){
								$nextBtn.show(0);
							} else{
								ole.footerNotificationHandler.pageEndSetNotification();
							}
						} else{
							play_correct_incorrect_sound(0);
							$(this).css('pointer-events', 'none');
							$(this).children('.incorrect-icon').show(0);
							$(this).children('.shirttext').addClass('incorrect-answer');
						}
					});
				});
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			for(var a=0; a<content[count].imagedivblock.length; a++){
				var imageblock = content[count].imagedivblock[a];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		 /*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/playtime/";

var content=[
	// slide0
	{
		singletext:[
		{
			textclass: "playtime",
			textdata: data.string.playtime
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover',
				imgsrc: ""
			},
			{
				imgclass: "rhino",
				imgid : 'rhino',
				imgsrc: ""
			},
			{
				imgclass: "squirrel",
				imgid : 'squirrel',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover2',
			imgsrc: ""
		},
		{
			imgclass: "stand",
			imgid : 'stand',
			imgsrc: ""
		},
		{
			imgclass: "counter",
			imgid : 'counter',
			imgsrc: ""
		},
		{
			imgclass: "circle",
			imgid : 'circle',
			imgsrc: ""
		},
		{
			imgclass: "box1",
			imgid : 'box1',
			imgsrc: ""
		},
		{
			imgclass: "box2",
			imgid : 'box2',
			imgsrc: ""
		},
		{
			imgclass: "box3",
			imgid : 'box3',
			imgsrc: ""
		},
		{
			imgclass: "box4",
			imgid : 'box4',
			imgsrc: ""
		},
		{
			imgclass: "box5",
			imgid : 'box5',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
headerblock:[
	{
		textdata: data.string.pt1,
	}
],
op3: "corr",
num1: "5",
num2: "6",
num3: "4",
num4: "2",
optflex:[{}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover3',
			imgsrc: ""
		},
		{
			imgclass: "bigblock",
			imgid : 'box2',
			imgsrc: ""
		},
		{
			imgclass: "animani-goat1",
			imgid : 'goat',
			imgsrc: ""
		},
		{
			imgclass: "animani-goat2",
			imgid : 'goat',
			imgsrc: ""
		},
		{
			imgclass: "animani-goat3",
			imgid : 'goat',
			imgsrc: ""
		},
		{
			imgclass: "animani-goat4",
			imgid : 'goat',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
headerblock:[
	{
		textdata: data.string.pt2,
	}
],
op2: "corr",
num1: "6",
num2: "2",
num3: "10",
num4: "8",
optflex:[{}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover3',
			imgsrc: ""
		},
		{
			imgclass: "bigblock",
			imgid : 'box5',
			imgsrc: ""
		},
		{
			imgclass: "animani-turt1",
			imgid : 'turt1',
			imgsrc: ""
		},
		{
			imgclass: "animani-turt2",
			imgid : 'turt2',
			imgsrc: ""
		},
	]
}]
},// slide4
{
headerblock:[
	{
		textdata: data.string.pt3,
	}
],
op1: "corr",
num1: "5",
num2: "4",
num3: "7",
num4: "3",
optflex:[{}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover3',
			imgsrc: ""
		},
		{
			imgclass: "bigblock",
			imgid : 'box4',
			imgsrc: ""
		},
		{
			imgclass: "animani-she1",
			imgid : 'sheep',
			imgsrc: ""
		},
		{
			imgclass: "animani-she2",
			imgid : 'sheep',
			imgsrc: ""
		},
		{
			imgclass: "animani-she3",
			imgid : 'sheep',
			imgsrc: ""
		},
		{
			imgclass: "animani-she4",
			imgid : 'sheep',
			imgsrc: ""
		},
		{
			imgclass: "animani-she5",
			imgid : 'sheep',
			imgsrc: ""
		}
	]
}]
},// slide5
{
headerblock:[
	{
		textdata: data.string.pt4,
	}
],
op4: "corr",
num1: "9",
num2: "5",
num3: "3",
num4: "7",
optflex:[{}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover3',
			imgsrc: ""
		},
		{
			imgclass: "bigblock",
			imgid : 'box1',
			imgsrc: ""
		},
		{
			imgclass: "animani-dog1",
			imgid : 'dognew',
			imgsrc: ""
		},
		{
			imgclass: "animani-dog2",
			imgid : 'dognew',
			imgsrc: ""
		},
		{
			imgclass: "animani-dog3",
			imgid : 'dognew',
			imgsrc: ""
		},
		{
			imgclass: "animani-dog4",
			imgid : 'dognew',
			imgsrc: ""
		},
		{
			imgclass: "animani-dog5",
			imgid : 'dognew',
			imgsrc: ""
		},
		{
			imgclass: "animani-dog6",
			imgid : 'dognew',
			imgsrc: ""
		},
		{
			imgclass: "animani-dog7",
			imgid : 'dognew',
			imgsrc: ""
		}
	]
}]
},// slide6
{
headerblock:[
	{
		textdata: data.string.pt5,
	}
],
op3: "corr",
num1: "3",
num2: "6",
num3: "10",
num4: "9",
optflex:[{}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover3',
			imgsrc: ""
		},
		{
			imgclass: "bigblock",
			imgid : 'box3',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb1",
			imgid : 'rab1',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb2",
			imgid : 'rab2',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb3",
			imgid : 'rab1',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb4",
			imgid : 'rab2',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb5",
			imgid : 'rab1',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb6",
			imgid : 'rab2',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb7",
			imgid : 'rab1',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb8",
			imgid : 'rab1',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb9",
			imgid : 'rab2',
			imgsrc: ""
		},
		{
			imgclass: "animani-rabb10",
			imgid : 'rab1',
			imgsrc: ""
		}
	]
}]
},
// slide7
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover2',
			imgsrc: ""
		},
		{
			imgclass: "stand",
			imgid : 'stand',
			imgsrc: ""
		},
		{
			imgclass: "circle",
			imgid : 'circle',
			imgsrc: ""
		},
		{
			imgclass: "box1",
			imgid : 'box6f',
			imgsrc: ""
		},
		{
			imgclass: "box2",
			imgid : 'box2f',
			imgsrc: ""
		},
		{
			imgclass: "box3",
			imgid : 'box3f',
			imgsrc: ""
		},
		{
			imgclass: "box4",
			imgid : 'box4f',
			imgsrc: ""
		},
		{
			imgclass: "box5",
			imgid : 'box5f',
			imgsrc: ""
		}
	]
}]
},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
		loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover2", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover3", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "box1", src: imgpath+"dog01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box2", src: imgpath+"goat01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box3", src: imgpath+"rabit01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box4", src: imgpath+"ship01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box5", src: imgpath+"tortle01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box5f", src: imgpath+"dog02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box2f", src: imgpath+"goat02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box6f", src: imgpath+"rabit02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box4f", src: imgpath+"ship02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box3f", src: imgpath+"tortle02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circle", src: imgpath+"circles.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stand", src: imgpath+"stand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "test", src: imgpath+"circle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"../images/dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "goat", src: imgpath+"goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "turt1", src: imgpath+"turt01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "turt2", src: imgpath+"turt02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sheep", src: imgpath+"ship.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dognew", src: imgpath+"god.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rab1", src: imgpath+"rab01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rab2", src: imgpath+"rab02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "counter", src: imgpath+"ticketcounter.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/right.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"playtime.ogg"},
			{id: "sound_2", src: soundAsset+"1.ogg"},
			{id: "sound_3", src: soundAsset+"2.ogg"},
			{id: "sound_4", src: soundAsset+"3.ogg"},
			{id: "sound_5", src: soundAsset+"4.ogg"},
			{id: "sound_6", src: soundAsset+"5.ogg"},
			{id: "bg", src: soundAsset+"../bg.ogg"},


			{id: "s1", src: soundAsset+"../1.ogg"},
			{id: "s2", src: soundAsset+"../2.ogg"},
			{id: "s3", src: soundAsset+"../3.ogg"},
			{id: "s4", src: soundAsset+"../4.ogg"},
			{id: "s5", src: soundAsset+"../5.ogg"},
			{id: "s6", src: soundAsset+"../6.ogg"},
			{id: "s7", src: soundAsset+"../7.ogg"},
			{id: "s8", src: soundAsset+"../8.ogg"},
			{id: "s9", src: soundAsset+"../9.ogg"},
			{id: "s10", src: soundAsset+"../10.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		firstPagePlayTime(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		$(".optns").click(function(){
			if($(this).hasClass("forhover")){
				if($(this).hasClass("corr")){
					$(this).addClass("corans");
					$(".forhover").removeClass("forhover");
					play_correct_incorrect_sound(1);
					$( "img[class*='animani']" ).animate({
						"left":"42%",
						"top":"42%",
					},2000,function(){
						$( "img[class*='animani']" ).remove();
						$(".bigblock").attr("src", preload.getResult("box"+countNext+"f").src);
						navigationcontroller();
					});
					$(this).append("<img class='correct_sign' src= '"+ preload.getResult('correct').src +"'>");
				}
				else{
					$(this).addClass("incans");
					$(this).removeClass("forhover");
					play_correct_incorrect_sound(0);
					$(this).append("<img class='correct_sign' src= '"+ preload.getResult('wrong').src +"'>");
				}
			}
		});

		$('.optns').mouseenter(function(){
			if($(this).hasClass("forhover")){
				var hovernum;
				hovernum = $(this).text();
				sound_player("s"+hovernum, 0);
			}
		});

		switch(countNext){
			case 1:
			sound_player("bg");
			break;
			case 2:
			exe_sound("sound_2");
			break;
			case 3:
			exe_sound("sound_3");
			break;
			case 4:
			exe_sound("sound_4");
			break;
			case 5:
			exe_sound("sound_5");
			break;
			case 6:
			exe_sound("sound_6");
			break;
			case 7:
			sound_player("bg");
			var endpageex =  new EndPageofExercise();
			endpageex.endpage(data.string.finmsg);
			break;
		}

		var parent = $("#flexcontainer");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function exe_sound(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
				showopts();
		});
	}

	function showopts(){
		$("#flexcontainer").animate({
			right:"0%"
		},1000);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0 || countNext == 1)
		navigationcontroller();

		generaltemplate();
				loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

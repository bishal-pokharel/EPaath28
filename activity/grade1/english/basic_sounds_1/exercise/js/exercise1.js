var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref+"/exercise/sounds/";

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var instrnSound = new buzz.sound((soundAsset + "BS_ins.ogg"));
var sound_b = new buzz.sound((soundAsset + "b.mp3"));
var sound_c = new buzz.sound((soundAsset + "c.mp3"));
var sound_d = new buzz.sound((soundAsset + "d.mp3"));
var sound_f = new buzz.sound((soundAsset + "f.mp3"));
var sound_g = new buzz.sound((soundAsset + "g.mp3"));
var sound_h = new buzz.sound((soundAsset + "h.mp3"));
var sound_j = new buzz.sound((soundAsset + "j.mp3"));

var sound_tree = new buzz.sound((soundAsset + "tree.mp3"));
var sound_bed = new buzz.sound((soundAsset + "bed.mp3"));
var sound_computer = new buzz.sound((soundAsset + "computer.mp3"));
var sound_flower = new buzz.sound((soundAsset + "flower.mp3"));
var sound_pencil = new buzz.sound((soundAsset + "pencil.mp3"));
var sound_hen = new buzz.sound((soundAsset + "hen.mp3"));
var sound_cat = new buzz.sound((soundAsset + "cat.mp3"));
var sound_frog = new buzz.sound((soundAsset + "frog.mp3"));
var sound_door = new buzz.sound((soundAsset + "door.mp3"));
var sound_book = new buzz.sound((soundAsset + "book.mp3"));
var sound_guitar = new buzz.sound((soundAsset + "guitar.mp3"));
var sound_jug = new buzz.sound((soundAsset + "jug.mp3"));

var content=[
  {
  //slide 0
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "first"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "tree.png",
        labeltext: data.string.ex_img_name12,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "flow.png",
        labeltext: data.string.ex_img_name7,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "comp.png",
        labeltext: data.string.ex_img_name5,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "bed.png",
        labeltext: data.string.ex_img_name2,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 1
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "second"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "flow.png",
        labeltext: data.string.ex_img_name7,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "bed.png",
        labeltext: data.string.ex_img_name2,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "hen.png",
        labeltext: data.string.ex_img_name9,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "cat.png",
        labeltext: data.string.ex_img_name3,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 2
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "third"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "frog.png",
        labeltext: data.string.ex_img_name13,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "book.png",
        labeltext: data.string.ex_img_name15,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "comp.png",
        labeltext: data.string.ex_img_name5,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "door.png",
        labeltext: data.string.ex_img_name14,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 3
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "fourth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "bed.png",
        labeltext: data.string.ex_img_name2,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "tree.png",
        labeltext: data.string.ex_img_name12,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "comp.png",
        labeltext: data.string.ex_img_name5,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "flow.png",
        labeltext: data.string.ex_img_name7,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 4
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "fifth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "bed.png",
        labeltext: data.string.ex_img_name2,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "comp.png",
        labeltext: data.string.ex_img_name5,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "tree.png",
        labeltext: data.string.ex_img_name12,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "guitar.png",
        labeltext: data.string.ex_img_name16,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 5
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "sixth"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "bed.png",
        labeltext: data.string.ex_img_name2,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "flow.png",
        labeltext: data.string.ex_img_name7,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "cat.png",
        labeltext: data.string.ex_img_name3,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "hen.png",
        labeltext: data.string.ex_img_name9,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
   {
  //slide 6
    uppertextblock : [

      {
        textclass : "upper",
        textdata : data.string.exhead
      }
      ],
    speakerimg : [{
    	imgsrc : imgpath + "speaker.png",
    	imgclass : "speakerimageclass",
    	speakerlash : "seventh"
    }],


    exerciseblock: [
    {
      imageoptions: [
      {
        forshuffle: "options_sign incorrectOne",
        imgsrc: imgpath + "bed.png",
        labeltext: data.string.ex_img_name2,
        wrongoption :[{
          wrongs : "wrongic1",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option1speaker"
        }]

      },
      {
        forshuffle: "options_sign incorrectTwo",
        imgsrc: imgpath + "comp.png",
        labeltext: data.string.ex_img_name5,
        wrongoption :[{
          wrongs : "wrongic2",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option2speaker"
        }]
      },
      {
        forshuffle: "options_sign incorrectThree",
        imgsrc: imgpath + "frog.png",
        labeltext: data.string.ex_img_name13,
        wrongoption :[{
          wrongs : "wrongic3",
        }],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option3speaker"
        }]

      },
      {
        forshuffle: "options_sign correctOne",
        imgsrc: imgpath + "jug.png",
        labeltext: data.string.ex_img_name17,
        correctoption :[{}],
        speaker:[{
        	speakersrc : imgpath + "speaker.png",
        	speakerclass: "option4speaker"
        }]
      },
      ]
    }
    ]
  },
  ];

/*remove this for non random questions*/
content.shufflearray();

$(function (){
  var $board    = $('.board');
  var $nextBtn  = $('#activity-page-next-btn-enabled');
  var $prevBtn  = $('#activity-page-prev-btn-enabled');
  var countNext = 0;
  var currentSound = instrnSound;

  var $total_page = content.length;

  function navigationcontroller(islastpageflag){
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined" ?
    islastpageflag = false :
    typeof islastpageflag != 'boolean'?
    alert("NavigationController : Hi Master, please provide a boolean parameter") :
    null;
  }

  var score = 0;

  /*values in this array is same as the name of images of eggs in image folder*/
  var testin = new EggTemplate();

  testin.init(7);
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $('.rightic').hide(0);
    $('.wrongic1').hide(0);
    $('.wrongic2').hide(0);
    $('.wrongic3').hide(0);
    $('.optionscontainer').hide(0);
    countNext==0?instrnSound.play():'';
		if($('.speakerimageclass').attr('id') == 'first')
			{
			soundplay(sound_b);
    		soundplay1(sound_tree);
   			soundplay2(sound_flower);
 		    soundplay3(sound_computer);
   			soundplay4(sound_bed);
   		    }
		else if($('.speakerimageclass').attr('id') ==  'second')
		{
			soundplay(sound_c);
        	soundplay1(sound_flower);
        	soundplay2(sound_bed);
        	soundplay3(sound_hen);
        	soundplay4(sound_cat);
        }
		else if($('.speakerimageclass').attr('id') == 'third')
		{
			soundplay(sound_d);
       		soundplay1(sound_frog);
        	soundplay2(sound_book);
        	soundplay3(sound_computer);
        	soundplay4(sound_door);
        	}
		else if($('.speakerimageclass').attr('id') == 'fourth')
		{
			soundplay(sound_f);
        	soundplay1(sound_bed);
        	soundplay2(sound_tree);
        	soundplay3(sound_door);
        	soundplay4(sound_flower);
        }
		else if($('.speakerimageclass').attr('id') == 'fifth')
		{
			soundplay(sound_g);
        	soundplay1(sound_bed);
        	soundplay2(sound_computer);
        	soundplay3(sound_tree);
        	soundplay4(sound_guitar);
        	}
		else if($('.speakerimageclass').attr('id') == 'sixth')
		{
			soundplay(sound_h);
       		soundplay1(sound_bed);
       		soundplay2(sound_flower);
        	soundplay3(sound_cat);
        	soundplay4(sound_hen);
        }
		else if($('.speakerimageclass').attr('id') == 'seventh')
		{
			soundplay(sound_j);
        	soundplay1(sound_bed);
        	soundplay2(sound_computer);
        	soundplay3(sound_frog);
        	soundplay4(sound_jug);
        	}
		else{
				$nextBtn.hide(0);
			}

    	function soundplay(sound) {
		$(".speakerimageclass").click(function() {
      currentSound.stop();
			sound.play().bind("playing", function() {
				$(".speakerimageclass").css({animation: "none"});
				$(".optionscontainer").delay(1000).fadeIn(500);
			})
		})
		}
         function soundplay1(sound) {
   		 $(".option1speaker").click(function() {
   	    sound.play().bind("playing", function() {
     	 })
  		  })
  		}
  function soundplay2(sound) {
    $(".option2speaker").click(function() {
      sound.play().bind("playing", function() {
      })
    })
  	}
  function soundplay3(sound) {
    $(".option3speaker").click(function() {
      sound.play().bind("playing", function() {
      })
    })
 	 }
  function soundplay4(sound) {
    $(".option4speaker").click(function() {
      sound.play().bind("playing", function() {
      })
    })
	  }
    /*generate question no at the beginning of question*/
    testin.numberOfQuestions();

    var parent = $(".optionsdiv");
    var divs = parent.children();
    while (divs.length) {
      parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    }

        var frac_1_n = 0;
        var frac_1_d = 0;
        var frac_2_n = 0;
        var frac_2_d = 0;
        var new_question1 = 0;
        var new_question2 = 0;
        var rand_multiplier_1 = 1;
        var rand_multiplier_2 = 1;
        var incorrect = false;

        function correct_btn(current_btn){
          $('.options_sign').addClass('disabled');
          $('.hidden_sign').html($(current_btn).html());
          $('.hidden_sign').addClass('fade_in');
          $('.rightic').show(0);
          $('.optionscontainer').removeClass('forHover');
          $(current_btn).addClass('option_true');
          $nextBtn.show(0);
          if(!incorrect){
          testin.update(true);
          }
        }
        function incorrect_btn1(current_btn){
          $(current_btn).addClass('disabled');
          $(current_btn).addClass('option_false');
          testin.update(false);
          incorrect = true;

        }
        function incorrect_btn2(current_btn){
          $(current_btn).addClass('disabled');
          $(current_btn).addClass('option_false');
          testin.update(false);
          $('.wrongic').show(0);
          incorrect = true;
        }
        function incorrect_btn(current_btn){
          $(current_btn).addClass('disabled');
          $(current_btn).addClass('option_false');
          $('.wrongic').show(0);
          testin.update(false);
          incorrect = true;
        }
    $('.correctOne').click(function(){
      correct_btn(this);
      var texto = $(this).find(".quesLabel");
      var textoo = $(texto).text();
      $('.dashedLine').html(textoo);
      $('.dashedLine').html(textoo).addClass('dsLine');
      play_correct_incorrect_sound(1);
    });

    $('.incorrectOne').click(function(){
      incorrect_btn(this);
      $('.wrongic1').show(0);
      play_correct_incorrect_sound(0);
    });
    $('.incorrectTwo').click(function(){
      incorrect_btn(this);
      $('.wrongic2').show(0);
      play_correct_incorrect_sound(0);
    });
    $('.incorrectThree').click(function(){
      incorrect_btn1(this);
      $('.wrongic3').show(0);
      play_correct_incorrect_sound(0);
    });
  }

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


  }

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function(){
  	countNext++;
  	 templateCaller();
    testin.gotoNext();
  });

  $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
    countNext--;
    templateCaller();
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });
/*=====  End of Templates Controller Block  ======*/
});

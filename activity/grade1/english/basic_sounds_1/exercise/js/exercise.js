var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref + "/exercise/sounds/";

/*Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};*/

var content = [
  {
    //slide 0
    uppertextblock: [
      {
        textclass: "upper",
        textdata: data.string.top
      }
    ],
    exerciseblock: [
      {
        imageoptions: [
          {
            forshuffle: "options_sign incorrectOne",
            imgsrc: imgpath + "tree.png",
            labeltext: data.string.q1opt1,
            wrongoption: [
              {
                wrongs: "wrongic1"
              }
            ]
          },
          {
            forshuffle: "options_sign incorrectTwo",
            imgsrc: imgpath + "flow.png",
            labeltext: data.string.q1opt2,
            wrongoption: [
              {
                wrongs: "wrongic2"
              }
            ]
          },
          {
            forshuffle: "options_sign incorrectThree",
            imgsrc: imgpath + "comp.png",
            labeltext: data.string.q1opt3,
            wrongoption: [
              {
                wrongs: "wrongic3"
              }
            ]
          },
          {
            forshuffle: "options_sign correctOne",
            imgsrc: imgpath + "bed.png",
            labeltext: data.string.q1opt4,
            correctoption: [{}]
          }
        ]
      }
    ]
  }
];

/*remove this for non random questions*/
//content.shufflearray();

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var countNext = 0;

  var $total_page = content.length;

  function navigationcontroller(islastpageflag) {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
  }

  var score = 0;

  /*values in this array is same as the name of images of eggs in image folder*/
  var testin = new EggTemplate();

  testin.init(10);
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $(".rightic").hide(0);
    $(".wrongic1").hide(0);
    $(".wrongic2").hide(0);
    $(".wrongic3").hide(0);
    /*generate question no at the beginning of question*/
    testin.numberOfQuestions();

    var parent = $(".optionsdiv");
    var divs = parent.children();
    while (divs.length) {
      parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    }

    switch (countNext) {
      default:
        var frac_1_n = 0;
        var frac_1_d = 0;
        var frac_2_n = 0;
        var frac_2_d = 0;
        var new_question1 = 0;
        var new_question2 = 0;
        var rand_multiplier_1 = 1;
        var rand_multiplier_2 = 1;
        var incorrect = false;

        function correct_btn(current_btn) {
          $(".options_sign").addClass("disabled");
          $(".hidden_sign").html($(current_btn).html());
          $(".hidden_sign").addClass("fade_in");
          $(".rightic").show(0);
          $(".optionscontainer").removeClass("forHover");
          $(current_btn).addClass("option_true");
          $nextBtn.show(0);
          if (!incorrect) {
            testin.update(true);
          }
        }
        function incorrect_btn1(current_btn) {
          $(current_btn).addClass("disabled");
          $(current_btn).addClass("option_false");
          testin.update(false);
          incorrect = true;
        }
        function incorrect_btn2(current_btn) {
          $(current_btn).addClass("disabled");
          $(current_btn).addClass("option_false");
          testin.update(false);
          $(".wrongic").show(0);
          incorrect = true;
        }
        function incorrect_btn(current_btn) {
          $(current_btn).addClass("disabled");
          $(current_btn).addClass("option_false");
          $(".wrongic").show(0);
          testin.update(false);
          incorrect = true;
        }
        break;
    }

    $(".correctOne").click(function() {
      correct_btn(this);
      var texto = $(this).find(".quesLabel");
      var textoo = $(texto).text();
      $(".dashedLine").html(textoo);
      $(".dashedLine")
        .html(textoo)
        .addClass("dsLine");
      play_correct_incorrect_sound(1);
    });

    $(".incorrectOne").click(function() {
      incorrect_btn(this);
      $(".wrongic1").show(0);
      play_correct_incorrect_sound(0);
    });
    $(".incorrectTwo").click(function() {
      incorrect_btn(this);
      $(".wrongic2").show(0);
      play_correct_incorrect_sound(0);
    });
    $(".incorrectThree").click(function() {
      incorrect_btn1(this);
      $(".wrongic3").show(0);
      play_correct_incorrect_sound(0);
    });
  }

  function templateCaller() {
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  }

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    testin.gotoNext();
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    countNext--;
    templateCaller();
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
  /*=====  End of Templates Controller Block  ======*/
});

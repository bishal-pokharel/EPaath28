var soundAsset = $ref+"/audio_en/";
var imgpath = $ref+"/images/";


var content=[
	// slide0
	{
		// contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "chaptertitle",
		}],
		lowertextblockadditionalclass:'allletters',
		lowertextblock:[{
			textdata: 'A ',
			textclass: "bot-text pink"
		},
		{
			textdata: 'E ',
			textclass: "bot-text orange"
		},
		{
			textdata: 'I ',
			textclass: "bot-text green"
		},
		{
			textdata: 'O ',
			textclass: "bot-text yellow"
		},
		{
			textdata: 'U ',
			textclass: "bot-text purple"
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'coverpage'
			},
			{
				imgclass:'icon',
				imgid:'icon-orange'
			}]
		}]
	},
	// slide1
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text1,
			textclass: "top-text",
		}],
		lowertextblockadditionalclass:'allletters',
		lowertextblock:[{
			textdata: 'A',
			textclass: "bot-text a"
		},
		{
			textdata: 'B',
			textclass: "bot-text"
		},
		{
			textdata: 'C',
			textclass: "bot-text"
		},
		{
			textdata: 'D',
			textclass: "bot-text"
		},
		{
			textdata: 'E',
			textclass: "bot-text e"
		},
		{
			textdata: 'F',
			textclass: "bot-text"
		},
		{
			textdata: 'G',
			textclass: "bot-text"
		},
		{
			textdata: 'H',
			textclass: "bot-text"
		},
		{
			textdata: 'I',
			textclass: "bot-text i"
		},
		{
			textdata: 'J',
			textclass: "bot-text"
		},
		{
			textdata: 'K',
			textclass: "bot-text"
		},
		{
			textdata: 'L',
			textclass: "bot-text"
		},
		{
			textdata: 'M',
			textclass: "bot-text"
		},
		{
			textdata: 'N',
			textclass: "bot-text"
		},
		{
			textdata: 'O',
			textclass: "bot-text o"
		},
		{
			textdata: 'P',
			textclass: "bot-text"
		},
		{
			textdata: 'Q',
			textclass: "bot-text"
		},
		{
			textdata: 'R',
			textclass: "bot-text"
		},
		{
			textdata: 'S',
			textclass: "bot-text"
		},
		{
			textdata: 'T',
			textclass: "bot-text"
		},
		{
			textdata: 'U',
			textclass: "bot-text u"
		},
		{
			textdata: 'V',
			textclass: "bot-text"
		},
		{
			textdata: 'W',
			textclass: "bot-text"
		},
		{
			textdata: 'X',
			textclass: "bot-text"
		},
		{
			textdata: 'Y',
			textclass: "bot-text"
		},
		{
			textdata: 'Z',
			textclass: "bot-text"
		}]
	},

	// slide2
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "top-text",
		},{
			textdata: data.string.p1text3,
			textclass: "bottom-text",
			datahighlightflag:true,
			datahighlightcustomclass:'blue'
		}],
		lowertextblockadditionalclass:'allletters',
		lowertextblock:[{
			textdata: 'A ',
			textclass: "bot-text border_changed a"
		},
		{
			textdata: 'B',
			textclass: "bot-text"
		},
		{
			textdata: 'C',
			textclass: "bot-text"
		},
		{
			textdata: 'D',
			textclass: "bot-text"
		},
		{
			textdata: 'E',
			textclass: "bot-text border_changed e"
		},
		{
			textdata: 'F',
			textclass: "bot-text"
		},
		{
			textdata: 'G',
			textclass: "bot-text"
		},
		{
			textdata: 'H',
			textclass: "bot-text"
		},
		{
			textdata: 'I',
			textclass: "bot-text border_changed i"
		},
		{
			textdata: 'J',
			textclass: "bot-text"
		},
		{
			textdata: 'K',
			textclass: "bot-text"
		},
		{
			textdata: 'L',
			textclass: "bot-text"
		},
		{
			textdata: 'M',
			textclass: "bot-text"
		},
		{
			textdata: 'N',
			textclass: "bot-text"
		},
		{
			textdata: 'O',
			textclass: "bot-text border_changed o"
		},
		{
			textdata: 'P',
			textclass: "bot-text"
		},
		{
			textdata: 'Q',
			textclass: "bot-text"
		},
		{
			textdata: 'R',
			textclass: "bot-text"
		},
		{
			textdata: 'S',
			textclass: "bot-text"
		},
		{
			textdata: 'T',
			textclass: "bot-text"
		},
		{
			textdata: 'U',
			textclass: "bot-text border_changed u"
		},
		{
			textdata: 'V',
			textclass: "bot-text"
		},
		{
			textdata: 'W',
			textclass: "bot-text"
		},
		{
			textdata: 'X',
			textclass: "bot-text"
		},
		{
			textdata: 'Y',
			textclass: "bot-text"
		},
		{
			textdata: 'Z',
			textclass: "bot-text"
		}]
	},


	// slide4
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text5,
			textclass: "desc_text",
			datahighlightflag:true,
			datahighlightcustomclass:'blue'
		}],
		lowertextblockadditionalclass:'allletters',
		lowertextblock:[{
			textdata: 'A ',
			textclass: "bot-text"
		},
		{
			textdata: 'E',
			textclass: "bot-text"
		},
		{
			textdata: 'I',
			textclass: "bot-text"
		},
		{
			textdata: 'O',
			textclass: "bot-text"
		},
		{
			textdata: 'U',
			textclass: "bot-text"
		}]
	},

	// slide5
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text6,
			textclass: "desc_text",
			datahighlightflag:true,
			datahighlightcustomclass:'blue'
		}],
		lowertextblockadditionalclass:'allletters',
		lowertextblock:[{
			textdata: 'A ',
			textclass: "bot-text"
		},
		{
			textdata: 'E',
			textclass: "bot-text"
		},
		{
			textdata: 'I',
			textclass: "bot-text"
		},
		{
			textdata: 'O',
			textclass: "bot-text"
		},
		{
			textdata: 'U',
			textclass: "bot-text"
		}]
	},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "coverpage", src: imgpath+'coverpage.png', type: createjs.AbstractLoader.IMAGE},
			{id: "icon-orange", src: imgpath+'icon-orange.png', type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		// put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				// $nextBtn.show(0);
				// $prevBtn.hide(0);
				sound_player('sound_1');
				break;
			case 1:
			sound_player('sound_2');
			$( ".bot-text").css('opacity','0');
				for (var i = 0; i < 26; i++) {
					var a = 6500+(i*1450);
					fadein_letters(i,a);
				}
				function fadein_letters(i,a){
					setTimeout(function(){
						$( ".bot-text").eq(i).animate({'opacity':'1'},500);
					},a);
				}
			// setTimeout(function(){
			// 	for (var i = 0; i < 26; i++) {
			// 							$( ".bot-text").eq(i).animate({'opacity':'1'},500).delay(i*500);
			// 	}
			// },8000);

			var count=0;
			for (var i = 0; i < 26; i++) {
					if(count>5){
						count=0;
					}
					else{
						$( ".bot-text").eq(i).addClass(shuffledcolors[count]);
					}
					count++;
			}
			break;
			case 2:
			sound_player('sound_3');
			$('.bottom-text').hide(0);
				var count=0;
				for (var i = 0; i < 26; i++) {
						if(count>5){
							count=0;
						}
						else{
							$( ".bot-text").eq(i).addClass(shuffledcolors[count]);
						}
						count++;
				}
				$('.allletters *').not('.border_changed').animate({'opacity':'0'},2500);
				setTimeout(function(){
					$('.allletters *').not('.border_changed').css('display','none');
					$('.bottom-text').css('top','48%').fadeIn(500);
				},2500);
				$('.a').css({'background': '#EA9999',
											'color': '#38761D'});
				$('.e').css({'background': '#F6B26B',
											'color': '#FF00FF'});
				$('.i').css({'background': '#93C47D',
											'color': '#0000FF'});
				$('.o').css({'background': '#FFD966',
												'color': '#9900FF'});
				$('.u').css({'background': '#8E7CC3',
											'color': '#FFD966'});

				break;
			case 3:
				sound_player('sound_4');
				var count=0;
				$('.top-text').css({"padding-top": "12%"});
				$('.bottom-text').hide(0);
				$('.bottom-text').fadeIn(500);
				for (var i = 0; i < 5; i++) {
						if(count>5){
							count=0;
						}
						else{
							$( ".bot-text").eq(i).addClass(shuffledcolors[count]);
						}
						count++;
				}
				break;
				case 4:
				sound_player('sound_5');
					var count=0;
					$('.desc_text').hide(0);
					setTimeout(function(){
						$('.desc_text').fadeIn(200);
					},500);
					$('.desc_text').fadeIn(500);
					for (var i = 0; i < 5; i++) {
							if(count>5){
								count=0;
							}
							else{
								$( ".bot-text").eq(i).addClass(shuffledcolors[count]);
							}
							count++;
					}
					break;
				case 5:
				case 6:
				var count=0;
				$('.desc_text').hide(0);
				$('.desc_text').fadeIn(200);
				for (var i = 0; i < 5; i++) {
						if(count>5){
							count=0;
						}
						else{
							$( ".bot-text").eq(i).addClass(shuffledcolors[count]);
						}
						count++;
				}
				nav_button_controls(100);
				break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		 /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

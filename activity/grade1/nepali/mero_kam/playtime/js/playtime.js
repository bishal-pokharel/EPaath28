var imgpath = $ref+"/images/playtime/";
var soundAsset = $ref + "/sounds/";
var maincontent = [

  //slide0
  {
    textblock: [
      {
        option: 'playtime',
        textclass: "",
        datahighlightflag: false,
        datahighlightcustomclass: "",
        textdata: data.string.p2text1
      },
    ],
    imageblock:[{
      imagestoshow: [

        {
          imgdiv: "bgDiv",
          imgclass: "mainbg",
          imgid: "playtime",
          imgsrc: "",
        },
        {
          imgdiv: "rhinoDiv",
          imgclass: "rhinoimgdiv",
          imgid: "rhino",
          imgsrc: "",
        },
        {
          imgdiv: "squirrelDiv",
          imgclass: "squirrelisteningimg",
          imgid: "squirrel",
          imgsrc: "",
        }]
      }],
    },

    //slide1
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text2,
        }
      ],
      // textblock: [
      //     {
      //         textclass: "instruction",
      //         datahighlightflag: false,
      //         datahighlightcustomclass: "",
      //         textdata: data.string.p2text2
      //     },
      // ],
      imageblock:[{
        imagestoshow: [
          {
            imgdiv: "img imgDiv1",
            imgclass: "img1",
            imgid: "eating",
            imgsrc: "",
          },
          {
            imgdiv: "img imgDiv2",
            imgclass: "img2",
            imgid: "hospital",
            imgsrc: "",
          },
          {
            imgdiv: "img imgDiv3",
            imgclass: "img3",
            imgid: "nest",
            imgsrc: "",
          },
        ]
      }],

    },
    // slide 2

  ];

  var contentnutri = [
    //slide1
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg nutr",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text3,
        }
      ],

      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "sitaDiv",
            imgclass: "sita1",
            imgid: 'sita01',
            imgsrc: ""
          },
        ]
      }]
    },
    //slide 2
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg nutr",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text4,
        }
      ],

      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "sitaDiv",
            imgclass: "sita1",
            imgid: 'sita02',
            imgsrc: ""
          },
        ]
      }]
    },
    //slide 3
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg nutr",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text5,
        }
      ],

      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "correct optionDiv1",
            imgclass: "option1",
            imgid: 'fruits',
            imgsrc: ""
          },
          {
            imgdiv: "incorrect optionDiv2",
            imgclass: "option2",
            imgid: 'junk_food',
            imgsrc: ""
          },
        ]
      }]
    },
    //slide 4
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg nutr",
      textblock: [
        {
          textclass: "finalWord",
          datahighlightflag: false,
          datahighlightcustomclass: "",
          textdata: data.string.p2text6
        },
      ],
      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "girlcycling",
            imgclass: "girlcycling",
            imgid: 'girl_cycling',
            imgsrc: ""
          },
          {
            imgdiv: "bgcycling",
            imgclass: "bgcycling",
            imgid: 'bgcycling',
            imgsrc: ""
          }
        ]
      }],
    },

  ];

  var contentresp = [
    //slide1
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg resp",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text14,
        }
      ],

      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "sitaDiv",
            imgclass: "sita1",
            imgid: 'doctor01',
            imgsrc: ""
          },
        ]
      }]
    },
    //slide 2
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg resp",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text15,
        }
      ],

      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "sitaDiv",
            imgclass: "sita1",
            imgid: 'doctor01',
            imgsrc: ""
          },
        ]
      }]
    },
    //slide 3
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg1 resp",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text16,
        }
      ],

      imageblock: [{
        imagestoshow: [

          {
            imgdiv: "correct hospitalDiv",
            imgclass: "correct option1",
            imgid: 'hospital01',
          },
          {
            imgdiv: "incorrect bankDiv",
            imgclass: "incorrect2 option2",
            imgid: 'bank',
          },
          {
            imgdiv: "incorrect policeDiv",
            imgclass: "incorrect1 option3",
            imgid: 'policestation',
          },
          {
            imgdiv: "street",
            imgclass: "street",
            imgid: 'street',
          },
          {
            imgdiv: "cyclingDiv",
            imgclass: "cyclingtohosp",
            imgid: 'girl_cycling',
          },

        ]
      }]
    },

    //slide 4
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg resp",
      textblock: [
        {
          textclass: "finalWord",
          datahighlightflag: false,
          datahighlightcustomclass: "",
          textdata: data.string.p2text17
        },
      ],
      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "sitaDiv",
            imgclass: "sita1",
            imgid: 'doctor02',
            imgsrc: ""
          },
        ]
      }],
    },


  ];

  var contentexcre = [
    //slide1
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg excre",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text8,
        }
      ],

      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "sitaDiv",
            imgclass: "sita1",
            imgid: 'treewithoutnest01',
            imgsrc: ""
          },
          {
            imgdiv: "eggnest",
            imgclass: "egg",
            imgid: 'eggcrack',
            imgsrc: ""
          },
          {
            imgdiv: "eggnestgif",
            imgclass: "egggif",
            imgid: 'eggcrackgif',
            imgsrc: ""
          },
        ]
      }]
    },
    //slide 2
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg excre",
      uppertextblockadditionalclass:"instruction",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.p2text9,
        }
      ],

      textblockadditionalclass: 'slideOption',
      textblock: [
        {
          option: 'incorrect div1',
          textclass: "textalign",
          textdata: data.string.p2text10,
        },
        {
          option:'correct div2',
          textclass: "textalign",
          textdata: data.string.p2text11,
        },
        {
          option:'incorrect div3',
          textclass: "textalign",
          textdata: data.string.p2text12,
        },
        {
          option:'incorrect div4',
          textclass: "textalign",
          textdata: data.string.p2text13,
        }
      ],

      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "sitaDiv",
            imgclass: "sita1",
            imgid: 'treewithoutnest02',
            imgsrc: ""
          },
          {
            imgdiv: "eggnest",
            imgclass: "egg",
            imgid: 'eggcrack',
            imgsrc: ""
          }
        ]
      }]
    },

    //slide 3
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg nutr",
      textblock: [
        {
          textclass: "finalWord",
          datahighlightflag: false,
          datahighlightcustomclass: "",
          textdata: data.string.p2text6
        },
      ],
      imageblock: [{
        imagestoshow: [
          {
            imgdiv: "chickmoving",
            imgclass: "chick",
            imgid: 'chick_moving_head_gif',
            imgsrc: ""
          }
        ]
      }],
    }

    // //slide 4
    // {
    //     contentnocenteradjust: true,
    //     contentblockadditionalclass: "bg nutr",
    //     textblock: [
    //       {
    //           textclass: "finalWord",
    //           datahighlightflag: false,
    //           datahighlightcustomclass: "",
    //           textdata: data.string.p2text6
    //       },
    //     ],
    //     imageblock: [{
    //         imagestoshow: [
    //             {
    //                 imgdiv: "chickmoving",
    //                 imgclass: "chick",
    //                 imgid: 'chick_moving_head_gif',
    //                 imgsrc: ""
    //             }
    //         ]
    //     }],
    // },

  ];

  var contentfinal = [
    //slide1
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "purplebg",
      uppertextblockadditionalclass:"",
      uppertextblock: [
        {
          textclass: "test",
          textdata: data.string.finale,
        }
      ],
      sliderload:true,
      slider:[{
        imagestoshow:[{
          imgclass : "slideimg si1",
          imgsrc : '',
          imgid : 'eating',
        },{
          imgclass : "slideimg si2",
          imgsrc : '',
          imgid : 'hospital',
        },{
          imgclass : "slideimg si3",
          imgsrc : '',
          imgid : 'nest',
        }]
      }]
    }
  ];

  $(function () {

    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = maincontent.length;
    loadTimelineProgress($total_page, countNext + 1);
    var lastpagefinal = false;

    var message =  data.string.message;

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    var endpageex =  new EndPageofExercise();
    endpageex.init(4);

    var mergecontent = [];
    var visitedcontent =
    {"nutrition":false,
    "respiration":false,
    "excretion":false};

    var nutritionContent = $.merge(mergecontent,maincontent);
    nutritionContent = $.merge(mergecontent,contentnutri);
    mergecontent = [];

    var respirationContent = $.merge(mergecontent,maincontent);
    respirationContent = $.merge(mergecontent,contentresp);
    mergecontent = [];

    var excretionContent = $.merge(mergecontent,maincontent);
    excretionContent = $.merge(mergecontent,contentexcre);
    mergecontent = [];

    function init() {
      //specify type otherwise it will load assests as XHR
      manifest = [

        //images

        {id: "playtime", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
        {id: "rhino", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
        {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},

        {id: "eating", src:imgpath+"eating.png", type: createjs.AbstractLoader.IMAGE},
        {id: "hospital", src: imgpath+"hospital01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "nest", src: imgpath+"nest.png", type: createjs.AbstractLoader.IMAGE},
        {id: "sita01", src: imgpath+"sita01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "sita02", src: imgpath+"sita02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "fruits", src: imgpath+"fruits.png", type: createjs.AbstractLoader.IMAGE},
        {id: "junk_food", src: imgpath+"junk_food.png", type: createjs.AbstractLoader.IMAGE},
        {id: "bgcycling", src: imgpath+"bg_cycling.png", type: createjs.AbstractLoader.IMAGE},
        {id: "girl_cycling", src: imgpath+"girl-in-cycle.gif", type: createjs.AbstractLoader.IMAGE},


        {id: "doctor01", src: imgpath+"bg_hospital.png", type: createjs.AbstractLoader.IMAGE},
        {id: "doctor02", src: imgpath+"bg_hospital01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "street", src: imgpath+"bg_street01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "hospital01", src: imgpath+"hospital.png", type: createjs.AbstractLoader.IMAGE},
        {id: "bank", src: imgpath+"bank.png", type: createjs.AbstractLoader.IMAGE},
        {id: "policestation", src: imgpath+"policestation.png", type: createjs.AbstractLoader.IMAGE},
        {id: "doctorsita", src: imgpath+"doctor_sita.png", type: createjs.AbstractLoader.IMAGE},

        {id: "treewithoutnest01", src: imgpath+"tree_without_nest01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "treewithoutnest02", src: imgpath+"tree_without_nest02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "treewithnest01", src: imgpath+"tree_with_nest01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "treewithnest02", src: imgpath+"tree_with_nest02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "chick_moving_head_gif", src: imgpath+"chick_moving_head.gif", type: createjs.AbstractLoader.IMAGE},
        {id: "chick_moving_head", src: imgpath+"chick_moving_head.png", type: createjs.AbstractLoader.IMAGE},
        {id: "eggcrack", src: imgpath+"egg_crack.png", type: createjs.AbstractLoader.IMAGE},
        {id: "eggcrackgif", src: imgpath+"egg_crack.gif", type: createjs.AbstractLoader.IMAGE},
        {id: "policestation", src: imgpath+"policestation.png", type: createjs.AbstractLoader.IMAGE},


        //sounds
        {id: "endo", src: soundAsset+"s1_end.ogg"},
        {id: "ex1_2", src: soundAsset+"ex1_2.ogg"},
        {id: "ex1_3", src: soundAsset+"ex1_3(bird).ogg"},
        {id: "ex1_4", src: soundAsset+"ex1_3(doctor).ogg"},
        {id: "ex1_5", src: soundAsset+"ex1_3(rita).ogg"},
        {id: "ex1_6", src: soundAsset+"ex1_4(bird).ogg"},
        {id: "ex1_7", src: soundAsset+"ex1_4(doctor).ogg"},
        {id: "ex1_8", src: soundAsset+"ex1_4(rita).ogg"},
        {id: "ex1_9", src: soundAsset+"ex1_5(bird).ogg"},
        {id: "ex1_10", src: soundAsset+"ex1_5(doctor).ogg"},
        {id: "ex1_11", src: soundAsset+"ex1_5(rita).ogg"},
        {id: "ex1_12", src: soundAsset+"ex1_6(doctor).ogg"},
        {id: "ex1_13", src: soundAsset+"ex1_6(rita).ogg"}


      ];
      preload = new createjs.LoadQueue(false);
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.on("progress", handleProgress);
      preload.on("complete", handleComplete);
      preload.on("fileload", handleFileLoad);
      preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
      // console.log(event.item);
    }

    function handleProgress(event) {
      $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
      $('#loading-wrapper').hide(0);
      //initialize varibales
      current_sound = createjs.Sound.play('sound_1');
      current_sound.stop();
      // call main function
      templateCaller();
    }

    //initialize
    init();
    // $refreshBtn.click(function(){
    //     templateCaller();
    // });
    /*=
    =            Handlers and helpers Block            =
    =*/
    /*===  register the handlebar partials first  ===*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    function loadpage(contenttobeload){

      $total_page = contenttobeload.length;
      loadTimelineProgress($total_page, countNext + 1);
      var source = $("#general-template").html();
      var template = Handlebars.compile(source);
      var html = template(contenttobeload[countNext]);
      $board.html(html);
      if(!lastpagefinal){
        firstPagePlayTime(countNext);
      }
      texthighlight($board);
      vocabcontroller.findwords(countNext);
      put_image(contenttobeload, countNext, preload);
      switch (countNext) {
          case 1:
            sound_player("ex1_2");
          break;
        default:

      }
    }


    function nutritiongeneraltemp(){
      loadpage(maincontent);
      console.log("I am in nutrition tab"+countNext);

      switch(countNext){
        case 2:
        console.log("wy");
        sound_player_nav('ex1_5');
        break;
        case 3:
        sound_player_nav('ex1_8');
        break;

        case 4:

        sound_player('ex1_11');
        checkCorrectAns();
        break;

        case 5:
        console.log('checkresult'+checkforlastpage("nutrition"));
        //  navigationcontroller(countNext,$total_page,false,checkforlastpage("nutrition"));
        addLastPage("nutrition");
        break;

        default:
        // navigationcontroller(countNext,$total_page);
        break;

      }

    }

    function respirationgeneraltemp(){
      loadpage(maincontent);
      console.log("I am in respirationgeneraltemp"+countNext);
      switch(countNext){

        case 2:
        createjs.Sound.stop();
        sound_player_nav('ex1_4');
        break;
        case 3:
        sound_player_nav('ex1_7');
        break;

        case 4:
        console.log('case 4 in respiration');
        sound_player('ex1_10');
        checkCorrectAnsResp();
        break;

        case 5:
        sound_player('ex1_12');
        console.log('case 5');
        // navigationcontroller(countNext,$total_page,false,checkforlastpage("respiration"));
        addLastPage("respiration");
        break;

        default:
        navigationcontroller(countNext,$total_page);
        break;

      }
    }

    function nutritiongeneraltemp(){
      loadpage(maincontent);
      console.log("I am in nutrition tab"+countNext);
      switch(countNext){
          case 2:
          console.log("wy");
          sound_player_nav('ex1_5');
          break;
          case 3:
          sound_player_nav('ex1_8');
          break;

          case 4:

          sound_player('ex1_11');
        checkCorrectAns();
        break;

        case 5:
        console.log('checkresult'+checkforlastpage("nutrition"));
        sound_player('ex1_13');

        //  navigationcontroller(countNext,$total_page,false,checkforlastpage("nutrition"));
        addLastPage("nutrition");
        break;

        default:
        navigationcontroller(countNext,$total_page);
        break;

      }

    }

    function excretiongeneraltemp(){
      loadpage(maincontent);
      console.log("I am in excretin tab"+countNext);

      $('.egggif').hide();
      $('.slideOption').hide();

      switch(countNext){

        case 2:
        createjs.Sound.stop();
        $nextBtn.hide();
        setTimeout(function(){
          $('.egggif').show();
          sound_player_nav('ex1_3');
          // navigationcontroller(countNext,$total_page);
        },4000);
        break;
        case 3:
        setTimeout(function(){
          sound_player('ex1_6');
          $('.slideOption').show();
        },3000);
        checkCorrectAnsExc();
        break;

        case 4:
        // navigationcontroller(countNext,$total_page,false,checkforlastpage("excretion"));
        sound_player('ex1_9');
        addLastPage("excretion");
        break;

        default:
        navigationcontroller(countNext,$total_page);
        break;


      }

    }

    function addLastPage(value){
      $nextBtn.show();
      console.log("Hello " +value);
      var test = checkforlastpage(value);
      if(!test){
        lastpagefinal = true;
        countNext = 0;
      }
      else{
      }
    }

    function checkCorrectAnsExc(){
      $('.div1,.div2,.div3,.div4').on('click',function(){
        console.log('checkCorrectAnsResp clicked');
        if($(this).hasClass('correct'))
        {
          var $this = $(this);
          var position = $this.position();
          var width = $this.width();
          var height = $this.height();
          var centerX = ((position.right + width / 2)*100)/$('.coverboardfull').width()+'%';
          var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
          $('<img style="right:10%;top:'+centerY+';position:absolute;width:15%;z-index:1" src="'+imgpath +'correct.png" />').insertAfter(this);
          $('.div1,.div2,.div3,.div4').css({"pointer-events":"none"});
          //  $(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
          play_correct_incorrect_sound(1);
          navigationcontroller(countNext,$total_page);
        }else {
          var $this = $(this);
          var position = $this.position();
          var width = $this.width();
          var height = $this.height();
          var centerX = ((position.right + width / 2)*100)/$('.coverboardfull').width()+'%';
          var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
          $('<img style="right:10%;top:'+centerY+';position:absolute;width:15%;z-index:1" src="'+imgpath +'incorrect.png" />').insertAfter(this);
          play_correct_incorrect_sound(0);
          //  $(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
        }
      });
    }

    function checkCorrectAns(){
      $('.optionDiv1,.optionDiv2 ').on('click',function(){
        if($(this).hasClass('correct'))
        {
          var $this = $(this);
          var position = $this.position();
          var width = $this.width();
          var height = $this.height();
          var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
          var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
          $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-24%,66%)" src="'+imgpath +'correct.png" />').insertAfter(this);
          $('.optionDiv1,.optionDiv2 ').css({"pointer-events":"none"});
          $(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
          play_correct_incorrect_sound(1);
          navigationcontroller(countNext,$total_page);
        }
        else {
          var $this = $(this);
          var position = $this.position();
          var width = $this.width();
          var height = $this.height();
          var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
          var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
          $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-24%,66%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
          play_correct_incorrect_sound(0);
          $(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
        }
      });
    }

    function checkCorrectAnsResp(){
      console.log('checkCorrectAnsResp');
      $('.option1,.option2,.option3').on('click',function(){
        console.log('checkCorrectAnsResp clicked');
        if($(this).hasClass('correct'))
        {

          $('<img style="right:-20%;bottom:38%;position:absolute;width:20%;z-index:1" src="'+imgpath +'correct.png" />').insertAfter(this);
          $('.bankDiv,.policeDiv,.hospitalDiv').css({"pointer-events":"none"});
          //  $(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
          play_correct_incorrect_sound(1);
          animateToHospital();

        }else if($(this).hasClass('incorrect1')){
          $('<img style="right:-10%;bottom:38%;position:absolute;width:20%;z-index:1" src="'+imgpath +'incorrect.png" />').insertAfter(this);
          play_correct_incorrect_sound(0);
          //    $(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});

        }else {
          $('<img style="right:40%;top:93%;position:absolute;width:20%;z-index:1" src="'+imgpath +'incorrect.png" />').insertAfter(this);
          play_correct_incorrect_sound(0);
          //  $(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
        }
      });
    }

    function animateToHospital(){

      $('.cyclingDiv').css({'animation': 'cycleToHospital 5s 1'});
      setTimeout(function(){
        $('.cyclingDiv').css({'animation': 'cycleToHospital1 10s 1'});
      },5000)

      setTimeout(function(){
        $('.cyclingDiv').hide();
        navigationcontroller(countNext,$total_page);
      },9500)

    }

    function  checkforlastpage(updatekey){
      var test = 0;
      visitedcontent[updatekey] = true;
      if( visitedcontent["nutrition"] == true &&
      visitedcontent["respiration"] == true &&
      visitedcontent["excretion"] == true
    )
    return false;
    else
    return true;
  }

  function sound_player(sound_id){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }

  function sound_player_nav(sound_id){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function () {
      navigationcontroller(countNext,$total_page);
    });
  }


  /*=====  End of user navigation controller function  ======*/

  //
  // function sound_player(sound_id,navigate,imgCls,imgSrc) {
  //   createjs.Sound.stop();
  //   current_sound = createjs.Sound.play(sound_id);
  //   current_sound.play();
  //   current_sound.on('complete', function () {
  //     navigate?navigationcontroller():"";
  //     if(imgCls && imgSrc){
  //       $("."+imgCls).attr("src",imgSrc);
  //     }
  //   });
  // }

  function templateCaller() {
    console.log("I am here");
    $nextBtn.css('display', 'none');
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
    $('#activity-page-finish-btn').hide();
  }


  /*=====  End of user navigation controller function  ======*/


  /*
  =            general template function            =
  */

  function generaltemplate() {
    console.log("This is general template "+maincontent.length);
    loadpage(maincontent);
    maincontent[countNext].sliderload?put_image3(maincontent, countNext):"";


    $('.girlcycle,.chickmove,.doctorsita').show();

    switch (countNext) {
      case 0:

      if(maincontent.length == 1){
        // $('.board').find('*').css({"display":"block"
        //                                 });
        console.log('m here in');
        sound_player("endo");
        endpageex.endpage();
        $('.btnNavigationSong').css({"top":"80%"});
        $('.messagediv').hide();

        // sound_player("rewards_page");
        $(".next-button").on("click", function(){
          $(".next-button, .prev-button ").css('pointer-events', 'none');
          var selectImg = $(".slider:eq(0)");
          $(".innerwrapper").append(selectImg.clone());
          selectImg.animate({
            width:"0%",
            padding:"0%"
          },1000, function(){
            selectImg.remove();
            $(".next-button, .prev-button").css('pointer-events', 'auto');
          });
        });

        $(".prev-button").on("click", function(){
          $(".next-button, .prev-button").css('pointer-events', 'none');
          var selectImg = $(".slider").last();
          $(".innerwrapper").prepend(selectImg.clone());
          selectImg.remove();
          var selectFirstImg = $(".slider:eq(0)");
          selectFirstImg.animate({
            width:"0%",
          },0,function(){
            selectFirstImg.animate({
              width:"20%"
            },1000);
            setTimeout(function(){
              $(".next-button, .prev-button").css('pointer-events', 'auto');
            },900);
          });
        });
        break;
        $('.girlcycle').fadeIn();
        setTimeout(function(){
          $('.chickmove').fadeIn();
        },1000);
        setTimeout(function(){
          $('.doctorsita').fadeIn();
        },2000);
      }
      break;

      case 1:
      console.log('in case 1');
      $(".img").click(function () {
        console.log('clicked case 1');
        countNext = 2;
        if($(this).hasClass("imgDiv1")){
          visitedcontent["nutrition"]=true;
          maincontent = nutritionContent;
          nutritiongeneraltemp();
        }else if($(this).hasClass("imgDiv2")){
          visitedcontent["respiration"]=true;
          maincontent = respirationContent;
          respirationgeneraltemp();
        }else if($(this).hasClass("imgDiv3")){
          visitedcontent["excretion"]=true;
          maincontent = excretionContent;
          excretiongeneraltemp();
        }
      });
      break;

      case 2:
      //animate pictures

      default:
      break;
    }
  }


  function sound_player(sound_id,navigate,imgCls,imgSrc) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function () {
      navigate?navigationcontroller():"";
      if(imgCls && imgSrc){
        $("."+imgCls).attr("src",imgSrc);
      }
    });
  }

  function switchtemplate(){
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    $(".contentblock").hasClass("nutr")?nutritiongeneraltemp():
    $(".contentblock").hasClass("resp")?respirationgeneraltemp():
    $(".contentblock").hasClass("excre")?excretiongeneraltemp():generaltemplate();
  }

  function put_image3(content, count){
    if(content[count].hasOwnProperty('slider')){
      var imageblock = content[count].slider[0];
      if(imageblock.hasOwnProperty('imagestoshow')){
        var imageClass = imageblock.imagestoshow;
        for(var i=0; i<imageClass.length; i++){
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = ('.'+classes_list[classes_list.length-1]);
          $(selector).attr('src', image_src);
        }
      }
    }
  }

  $nextBtn.on("click", function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    if(lastpagefinal){
      console.log("I am in last page next btn");
      maincontent = contentfinal;
      templateCaller();
      // endpageex.endpage(message);
    }else{
      if (countNext == $total_page - 1) {
        countNext = 1;
        templateCaller();
      }
      else {
        countNext++;
        switchtemplate();
      }
    }
  });

  $refreshBtn.click(function(){
    countNext==0?templateCaller():switchtemplate();
  });

  $prevBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    countNext==0?templateCaller():switchtemplate();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });


  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
});

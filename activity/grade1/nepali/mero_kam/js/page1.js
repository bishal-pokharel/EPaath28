var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {

        textblock: [
            {
                textclass: "title",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.title
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "coverpage",
                    imgid: 'coverpage',
                    // imgsrc: ""
                },
            ]
        }]
    },

    //slide 1
    {
        textblock: [
            {
                textclass: "line1",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text1_0
            },
            {
                textclass: "line2",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text1_1
            },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "student",
                    imgid: 'student01',
                    // imgsrc: ""
                },
                {
                    imgclass: "speechbox",
                    imgid: 'speechbox',
                },
                {
                    imgclass: "bgstudent",
                    imgid: 'bgstudent',
                }
            ]
        }]
    },

    //slide 2
    {

        textblock: [
            {
                textclass: "lineFarmer1",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text2_0
            },
            {
                textclass: "lineFarmer2",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text2_1
            },
        ],

        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "farmer",
                    imgid: 'farmer01',
                    // imgsrc: ""
                },
                {
                    imgclass: "speechbox1",
                    imgid: 'speechbox',
                },
                {
                    imgclass: "bgstudent",
                    imgid: 'bgfarmer',
                }
            ]
        }]
    },
    //slide 3
    {
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "coverpage",
                    imgid: 'coverpage',
                },
                {
                    imgclass: "coverpage",
                    imgid: 'coverpage',
                },
            ]
        }],

        divname: 'quesDiv',
        imgid: 'wrong',
        imgsrc: imgpath + 'wrong03.png',
        imgclass: 'button',
        popupques:[
                {
                    textclass: 'question',
                    datahighlightflag: false,
                    datahighlightcustomclass: '',
                    textdata: data.string.p1text3

                },
                {
                    textclass: 'ans1 correct',
                    datahighlightflag: false,
                    datahighlightcustomclass: '',
                    textdata: data.string.p1text4

                },
                {
                    textclass: 'ans2',
                    datahighlightflag: false,
                    datahighlightcustomclass: '',
                    textdata: data.string.p1text5

                },
        ]

    },

    //slide 4
    {

        textblock: [
            {
                textclass: "lineShopkeeper1",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text6_0
            },
            {
                textclass: "lineShopkeeper2",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text6_1
            },
        ],

        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "shopkeeper",
                    imgid: 'shopkeeper01',
                    // imgsrc: ""
                },
                {
                    imgclass: "speechbox2",
                    imgid: 'speechbox',
                },
                {
                    imgclass: "bgstudent",
                    imgid: 'bgshopkeeper',
                }
            ]
        }]
    },

    //slide 5
    {

        textblock: [
            {
                textclass: "lineDoctor1",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text7_0
            },
            {
                textclass: "lineDoctor2",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text7_1
            },
        ],

        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "doctor",
                    imgid: 'doctor01',
                    // imgsrc: ""
                },
                {
                    imgclass: "speechbox3",
                    imgid: 'speechbox',
                },
                {
                    imgclass: "bgstudent",
                    imgid: 'bgdoctor',
                }
            ]
        }]
    },

    //slide 6
    {
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "coverpage",
                    imgid: 'doctor',
                },
            ]
        }],

        divname: 'quesDiv',
        imgid: 'wrong',
        imgsrc: imgpath + 'wrong03.png',
        imgclass: 'button',
        popupques:[
            {
                textclass: 'question',
                datahighlightflag: false,
                datahighlightcustomclass: '',
                textdata: data.string.p1text8

            },
            {
                textclass: 'ans1 correct',
                datahighlightflag: false,
                datahighlightcustomclass: '',
                textdata: data.string.p1text9

            },
            {
                textclass: 'ans2',
                datahighlightflag: false,
                datahighlightcustomclass: '',
                textdata: data.string.p1text10

            },
        ]

    },

    //slide 7
    {

        textblock: [
            {
                textclass: "lineDriver1",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text11_0
            },
            {
                textclass: "lineDriver2",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text11_1
            },
        ],

        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "driver",
                    imgid: 'driver01',
                    // imgsrc: ""
                },
                {
                    imgclass: "speechbox4",
                    imgid: 'speechbox',
                },
                {
                    imgclass: "bgstudent",
                    imgid: 'bgdriver',
                }
            ]
        }]
    },

    //slide 8
    {

        textblock: [
            {
                textclass: "lineTailor1",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text12_0
            },
            {
                textclass: "lineTailor2",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text12_1
            },
        ],

        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "tailor",
                    imgid: 'tailor01',
                    // imgsrc: ""
                },
                {
                    imgclass: "speechbox5",
                    imgid: 'speechbox',
                },
                {
                    imgclass: "bgstudent",
                    imgid: 'bgtailor',
                }
            ]
        }]
    },

    //slide 9
    {
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "coverpage",
                    imgid: 'tailor',
                },
            ]
        }],

        divname: 'quesDiv',
        imgid: 'wrong',
        imgsrc: imgpath + 'wrong03.png',
        imgclass: 'button',
        popupques:[
            {
                textclass: 'question',
                datahighlightflag: false,
                datahighlightcustomclass: '',
                textdata: data.string.p1text13

            },
            {
                textclass: 'ans1 correct',
                datahighlightflag: false,
                datahighlightcustomclass: '',
                textdata: data.string.p1text14

            },
            {
                textclass: 'ans2',
                datahighlightflag: false,
                datahighlightcustomclass: '',
                textdata: data.string.p1text15

            },
        ]

    },

    //slide 10
    {

        textblock: [
            {
                textclass: "lineArtist1",
                datahighlightflput_imageag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text16_0
            },
            {
                textclass: "lineArtist2",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text16_1
            },
        ],

        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "tailor",
                    imgid: 'artist01',
                    // imgsrc: ""
                },
                {
                    imgclass: "speechbox6",
                    imgid: 'speechbox',
                },
                {
                    imgclass: "bgstudent",
                    imgid: 'bgartist',
                }
            ]
        }]
    },

    //slide 11
    {

        textblock: [
            {
                textclass: "lineTeacher1",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text17_0
            },
            {
                textclass: "lineTeacher2",
                datahighlightflag: false,
                datahighlightcustomclass: "",
                textdata: data.string.p1text17_1
            },
        ],

        imageblock: [{
            imagestoshow: [

                {
                    imgclass: "teacher1",
                    imgid: 'teacher01',
                    // imgsrc: ""
                },
                {
                    imgclass: "speechbox7",
                    imgid: 'speechbox',
                },
                {
                    imgclass: "bgstudent",
                    imgid: 'bgteacher',
                }
            ]
        }]
    },


];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            //images
            {id: "coverpage", src: imgpath + "farmer.png", type: createjs.AbstractLoader.IMAGE},
            {id: "artist", src: imgpath + "artist.png", type: createjs.AbstractLoader.IMAGE},
            {id: "beeGif", src: imgpath + "bee.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly_orange", src: imgpath + "butterfly_orange.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "doctor", src: imgpath + "doctor.png", type: createjs.AbstractLoader.IMAGE},
            {id: "driver", src: "driver.png", type: createjs.AbstractLoader.IMAGE},
            {id: "shopkeeper", src: imgpath + "shopkeeper.png", type: createjs.AbstractLoader.IMAGE},
            {id: "student", src: imgpath + "student.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tailor", src: imgpath + "tailor.png", type: createjs.AbstractLoader.IMAGE},
            {id: "teacher", src: imgpath + "teacjher.png", type: createjs.AbstractLoader.IMAGE},
            {id: "speechbox", src: imgpath + "text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wrong", src: imgpath + "wrong.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wrongtest", src: imgpath + "wrong03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "student01", src: imgpath + "student01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgstudent", src: imgpath + "bg_student.png", type: createjs.AbstractLoader.IMAGE},
            {id: "farmer01", src: imgpath + "farmer01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgfarmer", src: imgpath + "bg_farmer.png", type: createjs.AbstractLoader.IMAGE},
            {id: "shopkeeper01", src: imgpath + "shopkeeper01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgshopkeeper", src: imgpath + "bg_shopkeeper.png", type: createjs.AbstractLoader.IMAGE},
            {id: "doctor01", src: imgpath + "doctor01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgdoctor", src: imgpath + "bg_doctor.png", type: createjs.AbstractLoader.IMAGE},
            {id: "driver01", src: imgpath + "driver01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgdriver", src: imgpath + "bg_driver.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tailor01", src: imgpath + "tailor01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgtailor", src: imgpath + "bg_tailor.png", type: createjs.AbstractLoader.IMAGE},
            {id: "artist01", src: imgpath + "artist01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgartist", src: imgpath + "bg_artist.png", type: createjs.AbstractLoader.IMAGE},
            {id: "teacher01", src: imgpath + "teacher01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgteacher", src: imgpath + "bg_teacher.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p10.ogg"},
            {id: "sound_11", src: soundAsset + "s1_p11.ogg"},
            {id: "sound_12", src: soundAsset + "s1_p12.ogg"},
            {id: "sound_13", src: soundAsset + "s1_end.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        put_image(content, countNext, preload);
        vocabcontroller.findwords(countNext);
        switch (countNext) {
            case 0:
            sound_player('sound_1');
            break;
            case 1:
                $('.speechbox').hide();
                $('.line1').hide();
                $('.line2').hide();

                setTimeout(function () {
                    $('.speechbox').fadeIn();
                    $('.line1').fadeIn();
                    $('.line2').fadeIn();
                    sound_player('sound_2');


                },1000);

                break;

            case 2:

                $('.speechbox1').hide();
                $('.lineFarmer1').hide();
                $('.lineFarmer2').hide();
                $('.farmer').css({"width":"23%",
                                  "left":"42%",
                                  "top":"17%"});
                setTimeout(function () {
                    $('.speechbox1').fadeIn();
                    $('.lineFarmer1').fadeIn();
                    $('.lineFarmer2').fadeIn();

                    sound_player('sound_3');

                },1000);


                break;

            case 3:

            case 6:

            case 9:
                createjs.Sound.stop();
                var  current_sound = createjs.Sound.play('sound_'+(countNext+1));
                current_sound.play();
                // correct and incorrect actions for all
                $('.ans1,.ans2').click(function(){
                    if($(this).hasClass('correct'))
                    {
                        var $this = $(this);
                        var position = $this.position();
                        var width = $this.width();
                        var height = $this.height();
                        // var centerX = ((position.left + width / 2)*100)+'%';
                        // var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
                        $('<img style="left:25%;top:80%;position:absolute;width:5%;transform:translate(-34%,26%)" src="'+imgpath +'correct.png" />').insertAfter(this);
                        $('.ans1,.ans2').css({"pointer-events":"none"});
                        $(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
                        play_correct_incorrect_sound(1);
                        // nav_button_controls(100);
                        // $(".crossbutton").removeClass("avoid-clicks");
                        navigationcontroller(countNext, $total_page);

                    }
                    else {
                        var $this = $(this);
                        var position = $this.position();
                        var width = $this.width();
                        var height = $this.height();
                        // var centerX = ((position.left + width / 2)*100)+'%';
                        // var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
                        $('<img style="right:20%;top:80%;position:absolute;width:5%;transform:translate(-34%,26%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
                        play_correct_incorrect_sound(0);
                        $(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
                    }
                });
                break;

            case 4:

                $('.speechbox2').hide();
                $('.lineShopkeeper1').hide();
                $('.lineShopkeeper2').hide();

                setTimeout(function () {
                    $('.speechbox2').fadeIn();
                    $('.lineShopkeeper1').fadeIn();
                    $('.lineShopkeeper2').fadeIn();
                    sound_player('sound_5');

                },1000);


                break;

            case 5:

                $('.speechbox3').hide();
                $('.lineDoctor1').hide();
                $('.lineDoctor2').hide();

                setTimeout(function () {
                    $('.speechbox3').fadeIn();
                    $('.lineDoctor1').fadeIn();
                    $('.lineDoctor2').fadeIn();

                    sound_player('sound_6');

                },1000);

                break;

            case 7:

                $('.speechbox4').hide();
                $('.lineDriver1').hide();
                $('.lineDriver2').hide();

                setTimeout(function () {
                    $('.speechbox4').fadeIn();
                    $('.lineDriver1').fadeIn();
                    $('.lineDriver2').fadeIn();

                    sound_player('sound_8');

                },1000);

                break;

            case 8:

                $('.speechbox5').hide();
                $('.lineTailor1').hide();
                $('.lineTailor2').hide();

                setTimeout(function () {
                    $('.speechbox5').fadeIn();
                    $('.lineTailor1').fadeIn();
                    $('.lineTailor2').fadeIn();

                    sound_player('sound_9');

                },1000);

                break;

            case 10:

                $('.speechbox6').hide();
                $('.lineArtist1').hide();
                $('.lineArtist2').hide();
                $('.tailor').css({
                "width":" 41%",
                "height": "77%",
                "left": "19%",
                "top": "10%"});
                setTimeout(function () {
                    $('.speechbox6').fadeIn();
                    $('.lineArtist1').fadeIn();
                    $('.lineArtist2').fadeIn();

                    sound_player('sound_11');

                },1000);

                break;

            case 11:

                $('.speechbox7').hide();
                $('.lineTeacher1').hide();
                $('.lineTeacher2').hide();
                setTimeout(function () {
                    $('.speechbox7').fadeIn();
                    $('.lineTeacher1').fadeIn();
                    $('.lineTeacher2').fadeIn();

                    sound_player('sound_12');

                },1000);

                break;


            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
          navigationcontroller(countNext, $total_page);
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

var imgpath = $ref + "/images/playtime/";
var soundAsset = $ref+"/sounds/playtime_sounds/";

var content=[

	// slide0
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [

							{
									imgclass: "bg_full",
									imgid: "p04-2",
									imgsrc: "",
							}]
			}],
				speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p7text1,
					imgclass: 'flipped-h',
					textclass : 'text_inside',
					imgid : 'sp-dialogue1',
					imgsrc: '',
					// audioicon: true,
				}],
	},
	// slide1
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [

							{
									imgclass: "bg_full",
									imgid: "p04-2",
									imgsrc: "",
								}]
			}],
				speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p7text1,
					imgclass: 'flipped-h',
					textclass : 'text_inside',
					imgid : 'sp-dialogue1',
					imgsrc: '',
					// audioicon: true,
				}],
				td1:data.string.p7text2,
				td2:data.string.p7text3,
				td3:data.string.p7text4,
				class1:'correct',
				question_div:[{}]
	},

	// slide2
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "bg_full",
									imgid: "p04-1",
									imgsrc: "",
							},
                        {
                            imgclass: "footballgif",
                            imgid: "football",
                            imgsrc: "",
                        }
					]
			}],
				speechbox:[{
					speechbox: 'sp-2',
					textdata : data.string.p7text3,
					imgclass: 'flippedyes',
					textclass : 'text_inside',
					imgid : 'sp-dialogue2',
					imgsrc: '',
					// audioicon: true,
				}],
	},
	// slide3
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [

							{
									imgclass: "bg_full",
									imgid: "p04-2",
									imgsrc: "",
							}]
			}],
				speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p7text5,
					imgclass: 'flipped-h',
					textclass : 'text_inside',
					imgid : 'sp-dialogue1',
					imgsrc: '',
					// audioicon: true,
				}],
	},

	// slide5
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [

							{
									imgclass: "bg_full",
									imgid: "p04-2",
									imgsrc: "",
							}]
			}],
				speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p7text5,
					imgclass: 'flipped-h',
					textclass : 'text_inside',
					imgid : 'sp-dialogue1',
					imgsrc: '',
					// audioicon: true,
				}],
				td1:data.string.p7text2,
				td2:data.string.p7text6,
				td3:data.string.p7text7,
				class2:'correct',
				question_div:[{}]
	},

	// slide6
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [

							{
									imgclass: "bg_full",
									imgid: "p06-1",
									imgsrc: "",
							}]
			}],
				speechbox:[{
					speechbox: 'sp-2',
					textdata : data.string.p7text7,
					imgclass: 'flippedyes',
					textclass : 'text_inside',
					imgid : 'sp-dialogue2',
					imgsrc: '',
					// audioicon: true,
				}],
	},

	// slide7
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [

							{
									imgclass: "bg_full",
									imgid: "p04-2",
									imgsrc: "",
							}]
			}],
				speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p7text8,
					textclass : 'text_inside',
					imgid : 'sp-dialogue1',
					imgsrc: '',
					// audioicon: true,
				}],
	},


	// slide8
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [

							{
									imgclass: "bg_full",
									imgid: "p04-2",
									imgsrc: "",
							}]
			}],
				speechbox:[{
					speechbox: 'sp-1',
					textdata : data.string.p7text8,
					textclass : 'text_inside',
					imgid : 'sp-dialogue1',
					imgsrc: '',
					// audioicon: true,
				}],
				td1:data.string.p7text2,
				td2:data.string.p7text11,
				td3:data.string.p7text10,
				class1:'correct',
				question_div:[{}]
	},
	// slide9
	{
			contentnocenteradjust: true,
			imageblock:[{
					imagestoshow: [

							{
									imgclass: "bg_full",
									imgid: "p06-1",
									imgsrc: "",
							},]
			}],
				speechbox:[{
					speechbox: 'sp-2',
					textdata : data.string.p7text11,
					imgclass: 'flippedyes',
					textclass : 'text_inside',
					imgid : 'sp-dialogue2',
					imgsrc: '',
					// audioicon: true,
				}],
	},
	{
		contentblockadditionalclass:'green',
		imageslider:[{}],
		uppertextblock:[{
			textclass:'playagain',
			textdata:data.string.playagain
		},{
			textclass:'mainmenu',
			textdata:data.string.mainmenu
		},{
			textclass:'learnagain',
			textdata:data.string.learnagain
		}]
}



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);


	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src:imgpath +"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mainbg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mina", src: imgpath+"mina.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "p04-2", src: imgpath+"bg_park.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p04-1", src: imgpath+"p6-02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "football", src: imgpath+"football.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"chairbb-14.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue2", src: imgpath+"speechbubble-03-05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p06-1", src: imgpath+"bg_park01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "png1", src: imgpath+"exe_3_1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png2", src: imgpath+"exe_3_2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png3", src: imgpath+"exe_3_3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png4", src: imgpath+"p07-03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "next", src: "images/next.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prev", src: "images/previous.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"correct.ogg"},
			{id: "sound_2", src: soundAsset+"incorrect.ogg"},
			{id: "sound_3", src: soundAsset+"monkeysound.mp3"},
			{id: "ex1_ground_1", src: soundAsset+"ex1_ground_1.ogg"},
			{id: "ex1_ground_3", src: soundAsset+"ex1_ground_3.ogg"},
			{id: "ex1_ground_4", src: soundAsset+"ex1_ground_4.ogg"},
			{id: "ex1_ground_6", src: soundAsset+"ex1_ground_6.ogg"},
			{id: "ex1_ground_7", src: soundAsset+"ex1_ground_7.ogg"},
			{id: "ex1_ground_9", src: soundAsset+"ex1_ground_9.ogg"},
			{id: "ex1(football)_p2(ins)", src: soundAsset+"ex1(football)_p2(ins).ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(2)').css('display','none');
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(3)').css('display','none');
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(4)').css('display','none');
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(5)').css('display','none');
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(6)').html('1').css({
			"border": "3px solid #F99774",
		"background": "#F99774",
		"pointer-events":"none",
		'color':'white'
		});
		$('.arrowLine').hide(0);

		$('.slide-image1').attr('src',preload.getResult('png1').src);
		$('.slide-image2').attr('src',preload.getResult('png2').src);
		$('.slide-image3').attr('src',preload.getResult('png3').src);
		$('.next-button').attr('src',preload.getResult('next').src);
		$('.prev-button').attr('src',preload.getResult('prev').src);

		$('.prev-button').click(function(){

			$('.next-button,.prev-button').css('pointer-events','none')
				$('.left').animate({
					'left':'33.33333%',
					'opacity':'1'
				},1000,function(){
					$(this).addClass('mid').removeClass('left');
				});
				$('.mid').animate({
					'left':'66.66666%',
					'opacity':'.3'
				},1000,function(){
					$(this).addClass('right').removeClass('mid');
				});
				$('.right').animate({
					'left':'83.332665%'
				},500,function(){
					$('.right').css('left','-16.666665%');
					$('.right').animate({
						'left':'0%',
						'opacity':'.3'
					},500,function(){
						$('.next-button,.prev-button').css('pointer-events','auto')
						$(this).addClass('left').removeClass('right');
					});
				});
		});
		$('.next-button').click(function(){
			$('.next-button,.prev-button').css('pointer-events','none')
				$('.mid').animate({
					'left':'0%',
					'opacity':'.3'
					},1000,function(){
					$(this).addClass('left').removeClass('mid');
				});
				$('.right').animate({
					'left':'33.333333%',
					'opacity':'1'
				},1000,function(){
					$(this).addClass('mid').removeClass('right');
				});
				$('.left').animate({
					'left':'-16.666665%'
				},500,function(){
					$('.left').css('left','100%');
					$('.left').animate({
						'left':'66.66666%',
						'opacity':'.3'
					},500,function(){
						$('.next-button,.prev-button').css('pointer-events','auto')
						$(this).addClass('right').removeClass('left');
					});
				});
		});

		$('.playagain').click(function(){
            window.open('activity.html?id=nepkur01&lang='+$lang+'&grade=1&exercise=yes&filter=page2', '_self');
		});
		$('.learnagain').click(function(){
			$("#activity-page-lesson-tab").find('button').trigger('click');

		});
		$('.mainmenu').click(function(){
			$("#activity-page-menu-img").trigger("click");

		});
		$('.option1,.option2').click(function(){
			createjs.Sound.stop();
			if($(this).hasClass('correct')){
				$(this).css({'border-color':'#D4EF34','background':'#92AF3B'});
				$('.option1,.option2').css('pointer-events','none');
                var classname=$(this).hasClass("option1")?"firstcorrwrongimg":"secondcorrwrongimg";
                $('<img class="'+classname+'" src="'+imgpath +'correct.png" />').insertAfter(this);
                play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else{
				$(this).css({'border-color':'#980000','background':'#FF0000'});
				$(this).css('pointer-events','none');
                var classname=$(this).hasClass("option1")?"firstcorrwrongimg":"secondcorrwrongimg";
                $('<img class="'+classname+'" src="'+imgpath +'incorrect.png" />').insertAfter(this);
                play_correct_incorrect_sound(0);
			}
		});
		switch (countNext) {
      case 1:
      case 4:
      case 7:
          $(".sp-1").addClass("moveleft");
					countNext==1?sound_player("ex1(football)_p2(ins)",1):'';
      break;
			case 0:
			case 2:
			case 3:
			case 5:
			case 6:
			case 8:
				sound_player("ex1_ground_"+(countNext+1),1);
			break;
			case 9:
				$('.girl').css({'width':'10%','left':'52%'});
				break;
			default:

		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():"";
		});
	}

	function put_image(content, count){
			if(content[count].hasOwnProperty('imageblock')){
				for(var j = 0; j < content[count].imageblock.length; j++){
					var imageblock = content[count].imageblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
		}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var cloth_store = ['','','',''];


var content=[
	// slide0
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg1",
			uppertextblock: [
					{
							textclass: "playtime",
							textdata: data.string.play_text
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							}]
			}]
	},

	// slide1
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "pink_bg",
			uppertextblock: [
					{
							textclass: "question_top",
							textdata: data.string.playtext1
					},
					{
							textclass: "option1 correct",
							textdata: data.string.playtext2
					},{
							textclass: "option2",
							textdata: data.string.playtext3
					},{
							textclass: "left_bg",
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "right_image",
									imgid: "train_track",
									imgsrc: "",
							}]
			}]

	},
	// slide2
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "pink_bg",
			uppertextblock: [
					{
							textclass: "question_top",
							textdata: data.string.playtext1
					},
					{
							textclass: "option1 ",
							textdata: data.string.playtext4
					},{
							textclass: "option2 correct",
							textdata: data.string.playtext5
					},{
							textclass: "left_bg",
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "right_image",
									imgid: "train_track",
									imgsrc: "",
							}]
			}]
	},
	// slide3
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "pink_bg",
			uppertextblock: [
					{
							textclass: "question_top",
							textdata: data.string.playtext1
					},
					{
							textclass: "option1 correct",
							textdata: data.string.playtext6
					},{
							textclass: "option2",
							textdata: data.string.playtext7
					},{
							textclass: "left_bg",
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "right_image",
									imgid: "train_track",
									imgsrc: "",
							}]
			}]
	},

	// slide4
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "pink_bg",
			uppertextblock: [
					{
							textclass: "question_top",
							textdata: data.string.playtext1
					},
					{
							textclass: "option1 ",
							textdata: data.string.playtext9
					},{
							textclass: "option2 correct",
							textdata: data.string.playtext8
					},{
							textclass: "left_bg",
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "right_image",
									imgid: "train_track",
									imgsrc: "",
							}]
			}]
	},

	// slide5
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "pink_bg",
			uppertextblock: [
					{
							textclass: "question_top",
							textdata: data.string.playtext1
					},
					{
							textclass: "option1 correct",
							textdata: data.string.playtext10
					},{
							textclass: "option2 ",
							textdata: data.string.playtext11
					},{
							textclass: "left_bg",
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "right_image",
									imgid: "train_track",
									imgsrc: "",
							}]
			}]
	},

	//last page
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "pink_bg",
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train",
					imgid : 'train',
					imgsrc: ""
				},
				{
					imgclass: "track",
					imgid : 'track',
					imgsrc: ""
				}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "right_image",
									imgid: "train_track",
									imgsrc: "",
							}]
			}]
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var endpageex =  new EndPageofExercise();
	var message =  "हामीले पूर्णविरामको प्रयोगको बारेमा सिक्यौँ।";
	endpageex.init(7);

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "btn01", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "train", src: imgpath+"train.png", type: createjs.AbstractLoader.IMAGE},
			{id: "track", src: imgpath+"track.png", type: createjs.AbstractLoader.IMAGE},
			{id: "train_track", src: imgpath+"train_track.png", type: createjs.AbstractLoader.IMAGE},
			{id: "train_track_gif", src: imgpath+"train_track.gif", type: createjs.AbstractLoader.IMAGE},



			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "ex1_2", src: soundAsset+"ex1_2.ogg"},
			{id: "ex1_7", src: soundAsset+"ex1_7.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	 var audiocontroller = new AudioController($total_page, true,  160, 800, vocabcontroller);
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);
		var pbox1, pbox2,pbox2, ptext1, ptext2, ptext3;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var button_counter = 0;
		$nextBtn.hide(0);
		ole.footerNotificationHandler.hideNotification();
		$description = $(".description");
		vocabcontroller.findwords(countNext);
		// correct and incorrect actions for all
		countNext==1?sound_player("ex1_2",0):'';
		$('.option1,.option2 ').click(function(){
			if($(this).hasClass('correct'))
			{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull  ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.coverboardfull  ').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-1%,-199%);z-index:10" src="'+imgpath +'correct.png" />').insertAfter(this);
				$('.option1,.option2 ').css({"pointer-events":"none"});
				$(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
				play_correct_incorrect_sound(1);
				$('.right_image').attr('src',preload.getResult('train_track_gif').src);
				nav_button_controls(7000);
			}
			else {
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull  ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.coverboardfull  ').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-1%,-199%);z-index:10" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				play_correct_incorrect_sound(0);
				$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
			}
		});
		if(countNext==6){
			sound_player("ex1_7",0);
			endpageex.endpage(message);
		}
		if(countNext==0){
			nav_button_controls(100);
		}

}

  function sound_player(sound_id,next){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function(){
      next?nav_button_controls(1000):'';
    });
  }
function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.lessonEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
function templateCaller() {
	$prevBtn.css('display', 'none');
	$nextBtn.css('display', 'none');

	navigationcontroller();

	loadTimelineProgress($total_page, countNext + 1);
	generaltemplate();
	/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					templateCaller();
				});
			}
	*/
}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

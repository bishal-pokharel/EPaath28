var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	//slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		uppertextblock:[
			{
				textclass: "covertext",
				textdata: data.lesson.chapter
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train",
					imgid : 'train',
					imgsrc: ""
				},
				{
					imgclass: "track",
					imgid : 'track',
					imgsrc: ""
				}
			]
		}]
	},

	//slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		speechbox:[
			{
				textclass: "insidetext",
				textdata: data.string.p1text1,
				imgid:'tb-2',
				speechbox:'train_speech'
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train_unmoved",
					imgid : 'train',
					imgsrc: ""
				},
				{
					imgclass: "track",
					imgid : 'track',
					imgsrc: ""
				}
			]
		}]
	},

	//slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		speechbox:[
			{
				textclass: "insidetext",
				textdata: data.string.p1text2,
				imgid:'tb-2',
				speechbox:'train_speech'
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train_unmoved",
					imgid : 'train',
					imgsrc: ""
				},
				{
					imgclass: "track",
					imgid : 'track',
					imgsrc: ""
				}
			]
		}]
	},

	//slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		speechbox:[
			{
				textclass: "insidetext",
				textdata: data.string.p1text3,
				imgid:'tb-2',
				speechbox:'train_speech'
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train_unmoved",
					imgid : 'train',
					imgsrc: ""
				},
				{
					imgclass: "track",
					imgid : 'track',
					imgsrc: ""
				}
			]
		}]
	},

	//slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		uppertextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text4
			},
			{
				textclass: "right_blank",
			},
			{
				textclass: "drag_top yes",
			},
			{
				textclass: "drag_bot",
			},
			{
				textclass: "text_1 ",
				textdata: data.string.p1text5
			},
			{
				textclass: "text_2",
				textdata: data.string.p1text6
			},
			{
				textclass: "drag_able",
				textdata: data.string.p1text7
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train1",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "train2",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "track1",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "track2",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "top_sign",
					imgid : 'correct'
				},
				{
					imgclass: "bot_sign",
					imgid : 'incorrect'
				}
			]
		}]
	},


	//slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		uppertextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text4
			},
			{
				textclass: "right_blank",
			},
			{
				textclass: "drag_top ",
			},
			{
				textclass: "drag_bot yes",
			},
			{
				textclass: "text_1 ",
				textdata: data.string.p1text8
			},
			{
				textclass: "text_2",
				textdata: data.string.p1text9
			},
			{
				textclass: "drag_able",
				textdata: data.string.p1text7
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train1",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "train2",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "track1",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "track2",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "top_sign",
					imgid : 'incorrect'
				},
				{
					imgclass: "bot_sign",
					imgid : 'correct'
				}
			]
		}]
	},

	//slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		uppertextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text4
			},
			{
				textclass: "right_blank",
			},
			{
				textclass: "drag_top ",
			},
			{
				textclass: "drag_bot yes",
			},
			{
				textclass: "text_1 ",
				textdata: data.string.p1text10
			},
			{
				textclass: "text_2",
				textdata: data.string.p1text11
			},
			{
				textclass: "drag_able",
				textdata: data.string.p1text7
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train1",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "train2",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "track1",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "track2",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "top_sign",
					imgid : 'incorrect'
				},
				{
					imgclass: "bot_sign",
					imgid : 'correct'
				}
			]
		}]
	},

	//slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		uppertextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text4
			},
			{
				textclass: "right_blank",
			},
			{
				textclass: "drag_top yes",
			},
			{
				textclass: "drag_bot ",
			},
			{
				textclass: "text_1 ",
				textdata: data.string.p1text13
			},
			{
				textclass: "text_2",
				textdata: data.string.p1text12
			},
			{
				textclass: "drag_able",
				textdata: data.string.p1text7
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train1",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "train2",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "track1",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "track2",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "top_sign",
					imgid : 'correct'
				},
				{
					imgclass: "bot_sign",
					imgid : 'incorrect'
				}
			]
		}]
	},


	//slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "orange_bg",
		uppertextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text4
			},
			{
				textclass: "right_blank",
			},
			{
				textclass: "drag_top ",
			},
			{
				textclass: "drag_bot yes",
			},
			{
				textclass: "text_1 ",
				textdata: data.string.p1text14
			},
			{
				textclass: "text_2",
				textdata: data.string.p1text15
			},
			{
				textclass: "drag_able",
				textdata: data.string.p1text7
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "train1",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "train2",
					imgid : 'train_only',
					imgsrc: ""
				},
				{
					imgclass: "track1",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "track2",
					imgid : 'track',
					imgsrc: ""
				},
				{
					imgclass: "top_sign",
					imgid : 'incorrect'
				},
				{
					imgclass: "bot_sign",
					imgid : 'correct'
				}
			]
		}]
	},

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "train", src: imgpath+"train.png", type: createjs.AbstractLoader.IMAGE},
			{id: "track", src: imgpath+"track.png", type: createjs.AbstractLoader.IMAGE},
			{id: "train_only", src: imgpath+"train01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: imgpath+"incorrect.png", type: createjs.AbstractLoader.IMAGE},


			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset + "s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset + "s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset + "s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset + "s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset + "s1_p5.ogg"},
			{id: "rail", src: soundAsset + "rail.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		$nextBtn.hide(0);
		ole.footerNotificationHandler.hideNotification();
		switch(countNext){
			default:
				sound_player("s1_p"+(countNext+1),1);
			break;
			case 0:
			createjs.Sound.stop();
			var	current_sound = createjs.Sound.play("rail");
			current_sound.play();
			current_sound.on("complete", function(){
					sound_player("s1_p"+(countNext+1),1);
			});
			break;
			case 4:
			case 7:
			countNext==4?sound_player("s1_p"+(countNext+1),0):'';

			$('.top_sign, .bot_sign').hide(0);

					$('.drag_top,.drag_bot').click(function(){
						//dropped at right place
						if($(this).hasClass("yes")){
							 $(this).html(data.string.p1text7).removeClass("incorrect").addClass("correct");
							 $('.top_sign').fadeIn(500);
							 $('.train1').addClass('animate_train_correct');
							 play_correct_incorrect_sound(true);
							 $('.drag_top,.drag_bot').css({"background": "#e0e0e0", "border": "0.1em solid #e0e0e0"});
							 $('.train2,.track2,.text_2,.right_blank,.drag_able,.drag_bot,.bot_sign').fadeOut(500);
							 $('.text_1,.top_sign,.drag_top').addClass('text_anim_correct');
							 $('.top_sign').addClass('sign_anim');
							 $('.drag_top').addClass('biram_anim');
								nav_button_controls(100);
							 }

								//dropped at wrong place
								else {
									$('.bot_sign').fadeIn(500);
									$('.train2').addClass('animate_train_incorrect');
									$(this).addClass("incorrect");
									 // dropped.draggable('option', 'revert', true);
									 play_correct_incorrect_sound(false);
								}
					});


			break;


			case 5:
			case 6:
			case 8:

			$('.top_sign, .bot_sign').hide(0);

			$('.top_sign, .bot_sign').hide(0);

					$('.drag_top,.drag_bot').click(function(){
						//dropped at right place
						if($(this).hasClass("yes")){
							 $(this).html(data.string.p1text7).removeClass("incorrect").addClass("correct");
							 $('.bot_sign').fadeIn(500);
							 $('.train2').addClass('animate_train_correct');
							 play_correct_incorrect_sound(true);
							 $('.train1,.track1,.text_1,.right_blank,.drag_able,.drag_top,.top_sign').fadeOut(500);
							 $('.text_2,.bot_sign,.drag_bot').addClass('text_anim_correct');
							 $('.bot_sign').addClass('sign_anim');
							 $('.drag_bot').addClass('biram_anim');
								nav_button_controls(100);
							 }

								//dropped at wrong place
								else {
									$('.top_sign').fadeIn(500);
			 						$('.train1').addClass('animate_train_incorrect');
			 						 play_correct_incorrect_sound(false);
								}
					});

			function handleCardDrop1(event, ui){
					var dropped = ui.draggable;
					var droppedOn = $(this);

					//dropped at right place
					if(droppedOn.hasClass("yes")){
						 $('.bot_sign').fadeIn(500);
						 droppedOn.html(dropped.html());
						 $('.train2').addClass('animate_train_correct');
						 droppedOn.removeClass("incorrect").addClass("correct");
						 play_correct_incorrect_sound(true);
						 draggables.draggable('disable');
						 draggables.css({"background": "#e0e0e0", "border": "0.1em solid #e0e0e0"});
						 $('.train1,.track1,.text_1,.right_blank,.drag_able,.drag_top,.top_sign').fadeOut(500);
						 $('.text_2,.bot_sign,.drag_bot').addClass('text_anim_correct');
						 $('.bot_sign').addClass('sign_anim');
						 $('.drag_bot').addClass('biram_anim');
						 	nav_button_controls(100);
						 }

						 	//dropped at wrong place
		 					else {
		 						$('.top_sign').fadeIn(500);
		 						$('.train1').addClass('animate_train_incorrect');
		 						droppedOn.addClass("incorrect");
		 						 // dropped.draggable('option', 'revert', true);
		 						 play_correct_incorrect_sound(false);
		 					}

					}

			break;
		}

	}


	//function's definition starts

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

  function sound_player(sound_id,next){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function(){
      next?nav_button_controls(1000):'';
    });
  }
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
function templateCaller() {
	$prevBtn.css('display', 'none');
	$nextBtn.css('display', 'none');
ole.footerNotificationHandler.hideNotification()
	navigationcontroller();

	loadTimelineProgress($total_page, countNext + 1);
	generaltemplate();
}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

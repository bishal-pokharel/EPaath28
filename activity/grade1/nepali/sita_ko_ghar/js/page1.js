var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[

	//coverpage
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "mainbg",
			uppertextblockadditionalclass: 'cover',
			uppertextblock: [
			{
					textclass: "",
					textdata: data.lesson.chapter
			}],
			imageblock:[{
			imagestoshow: [
					{
							imgclass: "mainbg",
							imgid: "coverpage",
							imgsrc: "",
					}]
			}]
	},

	// slide 0

	{

        extratextblock:[{
            textclass:"chapterName",
            textdata:data.string.sita_intro,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background",
                imgsrc : '',
                imgid : 'sita_intro',
            }]
        }]
    },


    // slide 1

    {

        extratextblock:[{
            textclass:"chapterName",
            textdata:data.string.sita_family,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background",
                imgsrc : '',
                imgid : 'sita_family',
            }]
        }]
    },

    // slide 2

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_shoes,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background",
                imgsrc : '',
                imgid : 'sita_shoes',
            }]
        }]
    },

    // slide 3

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_handwash,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background",
                imgsrc : '',
                imgid : 'sita_handwash',
            }]
        }]
    },

    // slide 4

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_clothes,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background",
                imgsrc : '',
                imgid : 'sita_clothes',
            }]
        }]
    },

    // slide 5

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_carpet,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background",
                imgsrc : '',
                imgid : 'sita_carpet'
            },
				{
                    imgclass : "carpetBlue",
                    imgsrc : '',
                    imgid : 'carpet_blue',
                },
                {
                    imgclass : "carpetRed",
                    imgsrc : '',
                    imgid : 'carpet_red'
                }
			]
        }]
    },

    // slide 6

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_bike,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background",
                imgsrc : '',
                imgid : 'sita_bike'
            }]
        }]
    },

    // slide 7

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_jewels,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background zomed_at_fst",
                imgsrc : '',
                imgid : 'sita_jewels'
            }]
        }]
    },

    // slide 8

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_kitchen,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "backgroundZoom",
                imgsrc : '',
                imgid : 'sita_kitchen'
            }]
        }]
    },


    // slide 9

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_lunch,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "backgroundZoom",
                imgsrc : '',
                imgid : 'sita_lunch'
            }]
        }]
    },

    // slide 10

    {

        extratextblock:[{
            textclass:"sitaShoe",
            textdata:data.string.sita_playground,
            datahighlightflag: true,
            datahighlightcustomclass:'grenLtr',
        }],
        imageblock:[{
            imagestoshow:[{
                imgclass : "background",
                imgsrc : '',
                imgid : 'sita_playground'
            }]
        }]
    },

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var my_interval = null;
	var my_interval2 = null;
	//for preload
	var preload;
	var timeoutvar = null;
	var timeoutArr = [];
	var current_sound;
	var arrayCountToShow = 0;
	var arrClkToShow = 0;
	var arrayNumber = 0;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
  		{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sita_intro", src: imgpath+"02.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sita_family", src: imgpath+"03.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "sita_shoes", src: imgpath+"04.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sita_handwash", src: imgpath+"05.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "sita_clothes", src: imgpath+"06.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "sita_stick", src: imgpath+"07_01.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "grandFatherStick", src: imgpath+"stick.png", type: createjs.AbstractLoader.IMAGE},
      {id: "sita_carpet", src: imgpath+"08_01.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "carpet_red", src: imgpath+"carpet_red.png", type: createjs.AbstractLoader.IMAGE},
      {id: "carpet_blue", src: imgpath+"carpet_blue.png", type: createjs.AbstractLoader.IMAGE},
      {id: "sita_bike", src: imgpath+"09.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "sita_jewels", src: imgpath+"11.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "sita_kitchen", src: imgpath+"12.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "sita_lunch", src: imgpath+"15.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "sita_playground", src: imgpath+"16.gif", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p0", src: soundAsset+"chapter_start.ogg"},
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image2(content, countNext):"";
		content[countNext].sliderload?put_image3(content, countNext):"";
	 	var count  = 0;

		var soundCount = 0;
		function btnClk(sounds, arrayNumber){
			var count = 1;
			sound_player(sounds[arrayNumber][count]);
			$(".arrow").click(function(){
				count+=1;
				soundCount+=1;
				count>2?count = 0:count;
			$(".arrow").removeClass("scld");
			$(".arrow").css('pointer-events', 'none');
				var selectImg = $(".slider:eq(0)");
				$(".innerwrapper").append(selectImg.clone());
				selectImg.animate({
					height: "0%",
					padding: "0%"
				},1000,function(){
					selectImg.remove();
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play(sounds[arrayNumber][count]);
					current_sound_1.play();
					current_sound_1.on('complete', function(){
						$(".arrow").css('pointer-events', 'auto');
					$(".arrow").addClass("scld");
						soundCount==2?nav_button_controls(0):"";
					});
				});
			});
		}
		function clickAndPlay(clickCount, arrayCountToShow, soundArr){
			$(".div1, .div2, .div3").find("img:eq(0)").on("click", function(){
				$(this).parent().children().show(0);
				var parentCLs = $(this).parent().attr('class');
				if($(this).parent().hasClass("div1")){
					$(this).css("pointer-events","none");
					sound_player(soundArr[arrayCountToShow][0]);
					clickCount+=1;
				}
				else if ($(this).parent().hasClass("div2")) {
					$(this).css("pointer-events","none");
					sound_player(soundArr[arrayCountToShow][1]);
					clickCount+=1;
				}
				else {
					$(this).css("pointer-events","none");
					sound_player(soundArr[arrayCountToShow][2]);
					clickCount+=1;
				}
				if(clickCount == 3){
					$(".speakerimg").css("pointer-events","auto");
					nav_button_controls(100);
				}
			});
			$(".spkr1").on('click', function(){
				sound_player(soundArr[arrayCountToShow][0]);
			});
			$(".spkr2").on('click', function(){
				sound_player(soundArr[arrayCountToShow][1]);
			});
			$(".spkr3").on('click', function(){
				sound_player(soundArr[arrayCountToShow][2]);
			});
		}
		switch(countNext) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
      case 6:
      case 7:
      case 8:
      case 9:
	// TODO: Sanish commented this portion. Seems un-necessary
      	// $('.background').animate();
      case 10:
			case 11:
				sound_player("s1_p"+(countNext),1);
			break;
      // case 5:
			// 	sound_player("s1_p"+(countNext+1),1);
	// TODO: Sanish comment or delete console logs after the taks have been completed
	// console.log('m here case 5');
	// TODO: always add 0 to show and hide as default vale is 400
          $('.carpetBlue').hide(0);

          setTimeout(function(){
		// TODO: always add 0 to show and hide as default vale is 400
              $('.carpetBlue').show(0);
          }, 2000);

        nav_button_controls(2000);
			break;
			default:
				// TODO: do we need this?
				nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].imgspkrcontainer[0].hasOwnProperty('imagedivblock')){
			var imagblockVal = content[count].imgspkrcontainer[0];
			for(var j=0; j<imagblockVal.imagedivblock.length; j++){
				var imageblock = imagblockVal.imagedivblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

	function put_image3(content, count){
		if(content[count].hasOwnProperty('slider')){
			var imageblock = content[count].slider[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		clearTimeout(timeoutvar);
		createjs.Sound.stop();
		for(var i=0; i<timeoutArr.length; i++){
			clearTimeout(timeoutArr[i]);
		}
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		for(var i=0; i<timeoutArr.length; i++){
			clearTimeout(timeoutArr[i]);
		}
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

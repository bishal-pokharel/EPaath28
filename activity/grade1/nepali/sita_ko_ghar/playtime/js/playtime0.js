var imgpath = $ref + "/playtime/images/";
var imgpath2 = $ref + "/images/playtime/forhover/";
var soundAsset = $ref+"/sounds/";

var sound="";

var content=[

    // slide 0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "mainbg",
        uppertextblock: [
                {
                    textclass: "playtime",
                    textdata: data.string.diytext7
                }],
        imageblock:[{
        imagestoshow: [
            {
                imgclass: "mainbg",
                imgid: "mainbg",
                imgsrc: "",
            },
            {
                imgclass: "rhinoimgdiv",
                imgid: "rhinodance",
                imgsrc: "",
            },
            {
                imgclass: "squirrelisteningimg",
                imgid: "squirrel",
                imgsrc: "",
            }]
        }]
    },

	// slide 1

    {
        extratextblock:[{
            textclass:"playtime_info1",
            textdata:data.string.playtime_info1,
            datahighlightflag: false,
            datahighlightcustomclass:'grenLtr',
        },
		{
		    textclass:"bottomBg",
			textdata: '',
			datahighlightflag: false,
			datahighlightcustomclass:'grenLtr',
		},
		{

            textclass:"process_word",
            textdata: data.string.baas_rng,
            datahighlightflag: false,
            datahighlightcustomclass:'grenLtr',
		},
		// {
		// 	textclass:"chandrabindu",
		// 	textdata: data.string.chandrabindu,
		// 	datahighlightflag: false,
		// 	datahighlightcustomclass:'grenLtr',
		// },
		{
			textclass:"rightChoiceDisplay",
			textdata: data.string.baas_rt,
			datahighlightflag: false,
			datahighlightcustomclass:'grenLtr',
		},
		],

        imageblock:[{
            imagestoshow:[{
                imgclass : "container_close",
                imgsrc : '',
                imgid : 'container_close',
            },
			{
				imgclass : "container_hidden",
				imgsrc : '',
				imgid : 'container_open',
			},
			{
				imgclass : "bambooHidden",
				imgsrc : '',
				imgid : 'baas',
			},
            {
                imgclass : "chandrabinduImg",
                imgsrc : '',
                imgid : 'chandrabindu',
            },
			]
        }]
    },

	//slide 2

    {
        extratextblock:[
        	{
            textclass:"playtime_info1",
            textdata:data.string.playtime_info2,
            datahighlightflag: false,
            datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.baas_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"rightChoice",
                textdata: data.string.baas_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"wrongChoice",
                textdata: data.string.baas_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            }
        ],

        imageblock:[{
            imagestoshow:[
            	{
                imgclass : "container_close",
                imgsrc : '',
                imgid : 'container_open',
                },
                {
                    imgclass : "bamboo",
                    imgsrc : '',
                    imgid : 'baas',
                },
                {
                    imgclass : "rightSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "wrongSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "tickMark",
                    imgsrc : '',
                    imgid : 'tickMark',
                },
                {
                    imgclass : "crossMark",
                    imgsrc : '',
                    imgid : 'crossMark',
                },

            ]
        }]
    },

    // slide 3

    {
        extratextblock:[{
            textclass:"playtime_info1",
            textdata:data.string.playtime_info1,
            datahighlightflag: false,
            datahighlightcustomclass:'grenLtr',
        },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"process_word",
                textdata: data.string.bagaicha_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },

            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.bagaicha_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
        ],

        imageblock:[{
            imagestoshow:[{
                imgclass : "container_close",
                imgsrc : '',
                imgid : 'container_close',
            },
                {
                    imgclass : "container_hidden",
                    imgsrc : '',
                    imgid : 'container_open',
                },
                {
                    imgclass : "bambooHidden",
                    imgsrc : '',
                    imgid : 'bagaicha',
                },
                {
                    imgclass : "chandrabinduImg",
                    imgsrc : '',
                    imgid : 'chandrabindu',
                },
            ]
        }]
    },

    //slide 4

    {
        extratextblock:[
            {
                textclass:"playtime_info1",
                textdata:data.string.playtime_info2,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.bagaicha_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"rightChoice",
                textdata: data.string.bagaicha_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"wrongChoice",
                textdata: data.string.bagaicha_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            }
        ],

        imageblock:[{
            imagestoshow:[
                {
                    imgclass : "container_close",
                    imgsrc : '',
                    imgid : 'container_open',
                },
                {
                    imgclass : "bamboo",
                    imgsrc : '',
                    imgid : 'bagaicha',
                },
                {
                    imgclass : "rightSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "wrongSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "tickMark",
                    imgsrc : '',
                    imgid : 'tickMark',
                },
                {
                    imgclass : "crossMark",
                    imgsrc : '',
                    imgid : 'crossMark',
                },

            ]
        }]
    },

    // slide 5

    {
        extratextblock:[{
            textclass:"playtime_info1",
            textdata:data.string.playtime_info1,
            datahighlightflag: false,
            datahighlightcustomclass:'grenLtr',
        },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"process_word",
                textdata: data.string.galaicha_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.galaicha_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
        ],

        imageblock:[{
            imagestoshow:[{
                imgclass : "container_close",
                imgsrc : '',
                imgid : 'container_close',
            },
                {
                    imgclass : "container_hidden",
                    imgsrc : '',
                    imgid : 'container_open',
                },
                {
                    imgclass : "bambooHidden",
                    imgsrc : '',
                    imgid : 'galaicha',
                },
                {
                    imgclass : "chandrabinduImg",
                    imgsrc : '',
                    imgid : 'chandrabindu',
                },
            ]
        }]
    },

    //slide 6

    {
        extratextblock:[
            {
                textclass:"playtime_info1",
                textdata:data.string.playtime_info2,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.galaicha_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"rightChoice",
                textdata: data.string.galaicha_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"wrongChoice",
                textdata: data.string.galaicha_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            }
        ],

        imageblock:[{
            imagestoshow:[
                {
                    imgclass : "container_close",
                    imgsrc : '',
                    imgid : 'container_open',
                },
                {
                    imgclass : "bamboo",
                    imgsrc : '',
                    imgid : 'galaicha',
                },
                {
                    imgclass : "rightSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "wrongSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "tickMark",
                    imgsrc : '',
                    imgid : 'tickMark',
                },
                {
                    imgclass : "crossMark",
                    imgsrc : '',
                    imgid : 'crossMark',
                },

            ]
        }]
    },

    // slide 7

    {
        extratextblock:[{
            textclass:"playtime_info1",
            textdata:data.string.playtime_info1,
            datahighlightflag: false,
            datahighlightcustomclass:'grenLtr',
        },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"process_word",
                textdata: data.string.chandi_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.chandi_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
        ],

        imageblock:[{
            imagestoshow:[{
                imgclass : "container_close",
                imgsrc : '',
                imgid : 'container_close',
            },
                {
                    imgclass : "container_hidden",
                    imgsrc : '',
                    imgid : 'container_open',
                },
                {
                    imgclass : "bambooHidden",
                    imgsrc : '',
                    imgid : 'chandi',
                },
                {
                    imgclass : "chandrabinduImg",
                    imgsrc : '',
                    imgid : 'chandrabindu',
                },
            ]
        }]
    },

    //slide 8

    {
        extratextblock:[
            {
                textclass:"playtime_info1",
                textdata:data.string.playtime_info2,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.chandi_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"rightChoice",
                textdata: data.string.chandi_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"wrongChoice",
                textdata: data.string.chandi_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            }
        ],

        imageblock:[{
            imagestoshow:[
                {
                    imgclass : "container_close",
                    imgsrc : '',
                    imgid : 'container_open',
                },
                {
                    imgclass : "bamboo",
                    imgsrc : '',
                    imgid : 'chandi',
                },
                {
                    imgclass : "rightSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "wrongSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "tickMark",
                    imgsrc : '',
                    imgid : 'tickMark',
                },
                {
                    imgclass : "crossMark",
                    imgsrc : '',
                    imgid : 'crossMark',
                },

            ]
        }]
    },

    // slide 9

    {
        extratextblock:[
            {
                textclass:"playtime_info1",
                textdata:data.string.playtime_info1,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"process_word",
                textdata: data.string.pedhi_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.pedhi_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            }
        ],

        imageblock:[{
            imagestoshow:[{
                imgclass : "container_close",
                imgsrc : '',
                imgid : 'container_close',
            },
                {
                    imgclass : "container_hidden",
                    imgsrc : '',
                    imgid : 'container_open',
                },
                {
                    imgclass : "bambooHidden",
                    imgsrc : '',
                    imgid : 'pedhi',
                },
                {
                    imgclass : "chandrabinduImg",
                    imgsrc : '',
                    imgid : 'chandrabindu',
                },
            ]
        }]
    },

    //slide 10

    {
        extratextblock:[
            {
                textclass:"playtime_info1",
                textdata:data.string.playtime_info2,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"bottomBg",
                textdata: '',
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"rightChoiceDisplay",
                textdata: data.string.pedhi_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {

                textclass:"rightChoice",
                textdata: data.string.pedhi_rt,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            },
            {
                textclass:"wrongChoice",
                textdata: data.string.pedhi_rng,
                datahighlightflag: false,
                datahighlightcustomclass:'grenLtr',
            }
        ],

        imageblock:[{
            imagestoshow:[
                {
                    imgclass : "container_close",
                    imgsrc : '',
                    imgid : 'container_open',
                },
                {
                    imgclass : "bamboo",
                    imgsrc : '',
                    imgid : 'pedhi',
                },
                {
                    imgclass : "rightSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "wrongSound",
                    imgsrc : '',
                    imgid : 'audio',
                },
                {
                    imgclass : "tickMark",
                    imgsrc : '',
                    imgid : 'tickMark',
                },
                {
                    imgclass : "crossMark",
                    imgsrc : '',
                    imgid : 'crossMark',
                },

            ]
        }]
    },

    //slide 11

    {
        sliderload:true,
        contentblockadditionalclass:'green',

        // uppertextblock:[{
        //     textclass:'playagain',
        //     textdata:data.string.playagain
        // },{
        //     textclass:'mainmenu',
        //     textdata:data.string.mainmenu
        // },{
        //     textclass:'learnagain',
        //     textdata:data.string.learnagain
        // }],

        slider:[{
            imagestoshow:[
                {
                    imgclass : "slideimg si5",
                    imgsrc : '',
                    imgid : 'pedhi',
                    textclass:'rightWordDisplay',
                    textdata:data.string.pedhi_rt
                },{
                imgclass : "slideimg si1",
                imgsrc : '',
                imgid : 'baas',
                textclass:'rightWordDisplay',
                textdata:data.string.baas_rt

            },{
                imgclass : "slideimg si2",
                imgsrc : '',
                imgid : 'bagaicha',
                textclass:'rightWordDisplay',
                textdata:data.string.bagaicha_rt
            },{
                imgclass : "slideimg si3",
                imgsrc : '',
                imgid : 'galaicha',
                textclass:'rightWordDisplay',
                textdata:data.string.galaicha_rt
            },{
                imgclass : "slideimg si4",
                imgsrc : '',
                imgid : 'chandi',
                textclass:'rightWordDisplay',
                textdata:data.string.chandi_rt
            }]
        }]
    }

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var selPgeArr=[];

	var preload;
	var timeoutvar = null;
	var current_sound;

  // var endpageex =  new EndPageofExercise();
  // endpageex.init(7);
  // var message =  data.string.lasttext;

    var endpageex =  new EndPageofExercise();
    var message =  data.string.lasttext;
    endpageex.init(5);

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

    
      {id: "mainbg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
      {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},


      {id: "chandrabindu", src: imgpath+"chandra_bindu.png", type: createjs.AbstractLoader.IMAGE},
			{id: "baas", src: imgpath+"bamboo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bgplaytime", src:imgpath +"bgplaytime.png", type: createjs.AbstractLoader.IMAGE},
			{id: "galaicha", src: imgpath+"carpet.png", type: createjs.AbstractLoader.IMAGE},
			{id: "container_close", src: imgpath+"cointainer-close.png", type: createjs.AbstractLoader.IMAGE},
			{id: "container_open", src: imgpath+"cointainer-open.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bagaicha", src: imgpath+"garden.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pedhi", src: imgpath+"pedhi01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chandi", src: imgpath+"silver01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "silver01", src: imgpath+"silver.png", type: createjs.AbstractLoader.IMAGE},
      {id: "audio", src: imgpath+"speaker.png", type: createjs.AbstractLoader.IMAGE},
      {id: "tickMark", src: imgpath+"right.png", type: createjs.AbstractLoader.IMAGE},
      {id: "crossMark", src: imgpath+"wrong.png", type: createjs.AbstractLoader.IMAGE},

      // Last Page Playtime

      {id: "png1", src: imgpath+"exe_4_1.png", type: createjs.AbstractLoader.IMAGE},
      {id: "png2", src: imgpath+"exe_4_2.png", type: createjs.AbstractLoader.IMAGE},
      {id: "png3", src: imgpath+"exe_4_3.png", type: createjs.AbstractLoader.IMAGE},
      {id: "png4", src: imgpath+"p07-03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "next", src: imgpath+"next.png", type: createjs.AbstractLoader.IMAGE},
      {id: "prev", src: imgpath+"previous.png", type: createjs.AbstractLoader.IMAGE},

      //sounds

      {id: "ex_ins1", src: soundAsset+"exe_ins_1.ogg"},
      {id: "ex_ins2", src: soundAsset+"exe_ins_2.ogg"},

      {id: "ex1_p1", src: soundAsset+"exe1_p3_1.ogg"},
      {id: "ex1_p3", src: soundAsset+"exe1_p5_1.ogg"},
      {id: "ex1_p5", src: soundAsset+"exe1_p7_1.ogg"},
      {id: "ex1_p7", src: soundAsset+"exe1_p9_1.ogg"},
      {id: "ex1_p9", src: soundAsset+"exe1_p11_1.ogg"},

      {id: "ex2_p1", src: soundAsset+"exe1_p3_1.ogg"},
      {id: "ex2_p3", src: soundAsset+"exe1_p5_1.ogg"},
      {id: "ex2_p5", src: soundAsset+"exe1_p7_1.ogg"},
      {id: "ex2_p7", src: soundAsset+"exe1_p9_1.ogg"},
      {id: "ex2_p9", src: soundAsset+"exe1_p11_1.ogg"},

      {id: "ex2_p2", src: soundAsset+"exe1_p3_2.ogg"},
      {id: "ex2_p4", src: soundAsset+"exe1_p5_2.ogg"},
      {id: "ex2_p6", src: soundAsset+"exe1_p7_2.ogg"},
      {id: "ex2_p8", src: soundAsset+"exe1_p9_2.ogg"},
      {id: "ex2_p10", src: soundAsset+"exe1_p11_2.ogg"},

      // {id: "ex1_p3", src: soundAsset+"ex1_p3.ogg"},
      // {id: "ex1_p5", src: soundAsset+"ex1_p5.ogg"},
      // {id: "ex1_p7", src: soundAsset+"ex1_p7.ogg"},
      // {id: "ex1_p9", src: soundAsset+"ex1_p9.ogg"},
      // {id: "ex1_p11", src: soundAsset+"ex1_p11.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any nostifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
 // 	var pagenumbers  = new pagenameholder();
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		content[countNext].sliderload ? put_image3(content, countNext):true;
		var pagenumbers;


 		/*for randomizing the options*/
		var parent = $(".opnBlk");
		var divs = parent.children();
		while (divs.length) {
 			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		$(".hideIt").hide(0);
		$(".option").on("click", function(){
			if($(this).hasClass("correct")){
				$(this).siblings(".corctopt").show(0);
				$(".hideIt").show(0);
				play_correct_incorrect_sound(1);
				$(".option").css("pointer-events","none");
				nav_button_controls(100);
			}
			else{
				$(this).siblings(".wrngopt").show(0);
				$(this).css("pointer-events","none");
				play_correct_incorrect_sound(0);
			}
		});


		switch (countNext) {
        case 0:
        break;
				case 1:
				case 3:
				case 5:
				case 7:
				case 9:
          console.log('countNext '+countNext);
          sound_player_nav("ex_ins1",0);
	            // $('rightChoiceDisplay').css('z-index',function () {
	                //     return 0
	                // });
          $('.process_word').css('pointer-events','none');

          $('.process_word').click(function () {
          	// console.log('clicked');
              $('.process_word').css({"animation": "move1 5s 1"});
             setTimeout(function () {
                 readyChandraBindu();
             },3000);
          });
					// TODO: let's discuss these
          readyChandraBindu = function(){
              $('.process_word').css({"visibility": "hidden"});
              $('.playtime_info1').css('color','#3c78d8');
              $('.chandrabinduImg').css({"animation": "pulse_animation 500ms infinite"});
              $('.chandrabinduImg').click(function () {
              	// TODO: this one as well
                  $('.chandrabinduImg').css({"animation": "move2 5s 1"});
                  setTimeout(function () {
                      $('.chandrabinduImg').css('z-index','0');
                      $('.chandrabinduImg').hide();
                      shakeWordMachine();
                  },2000);
              });
	        };

	        shakeWordMachine = function () {
					$('.container_close').css({"animation": "shake 500ms 5"});
              setTimeout(function () {
                  showBaas();
                  sound_player_nav("ex1_p"+(countNext),0);
              },2000);
          };

          showBaas = function () {
  						$('.container_close').css({"visibility":"hidden"});
              $('.container_hidden').css({"visibility":"visible"});
              $('.bambooHidden').css({'z-index':'3'});
              $('.bambooHidden').animate({
                  'position': 'absolute',
                  'left':'30%',
                  'width': '37%',
              },4000,function () {
                  // sound_player("sound_test1",false);
              });
  						$('.rightChoiceDisplay').css({"z-index":"4"});
                  //    $('bambooHidden').css({"z-index":"1"});
              nav_button_controls(5000);
            };

					break;
				case 2:
				case 4:
				case 6:
				case 8:
				case 10:
          console.log('countNext '+countNext);
          sound_player_nav("ex_ins2",0);
          $(".wrongChoice,.rightChoice").css('pointer-events','none');
          $('.rightChoiceDisplay').css({"z-index":"2"});

					$('.rightChoice').click(function () {
              // $('.rightChoice').css('background','green');
              play_correct_incorrect_sound(1);
              $('.tickMark').css('visibility', 'visible');
              $('.crossMark').css('visibility', 'hidden');
              $('.wrongChoice').css('pointer-events', 'none');
             // $('.wrongChoice').disable();
              nav_button_controls(100);
          });

          $(".wrongChoice").hover(function(){
              sound_player_nav("ex2_p"+(countNext),0);
          });

          $(".rightChoice").hover(function(){
              sound_player_nav("ex2_p"+(countNext-1),0);
          });

          $('.wrongChoice').click(function () {
              // $('.wrongChoice').css('background','red');
              play_correct_incorrect_sound(0);

              $('.crossMark').css('visibility', 'visible');
              $('.tickMark').css('visibility', 'hidden');
          });

          $('.rightSound').click(function () {
               // sound_player("sound_test1",false);
          });

          $('.wrongSound').click(function () {
               // sound_player("sound_test2",false);
          });
					break;
        case 11:
          $(".next-button").on("click", function(){
              $(".next-button, .prev-button ").css('pointer-events', 'none');
              var selectImg = $(".slider:eq(0)");
              $(".innerwrapper").append(selectImg.clone());
              selectImg.animate({
                  width:"0%",
                  padding:"0%",
              },1000, function(){
                  selectImg.remove();
                  $(".next-button, .prev-button").css('pointer-events', 'auto');
              });
          });
          $(".prev-button").on("click", function(){
              $(".next-button, .prev-button").css('pointer-events', 'none');
              var selectImg = $(".slider").last();
              $(".innerwrapper").prepend(selectImg.clone());
              selectImg.remove();
              var selectFirstImg = $(".slider:eq(0)");
              selectFirstImg.animate({
                  width:"0%",
              },0,function(){
                  selectFirstImg.animate({
                      width:"20%"
                  },1000);
                  setTimeout(function(){
                      $(".next-button, .prev-button").css('pointer-events', 'auto');
                  },900);
              });
          });
          endpageex.endpage(message);
          break;
				default:
					nav_button_controls(100);
					break;
			}

	}

    function sound_player_nav(sound_id, next,id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on("complete", function(){
          console.log('m here complete');
          $(".wrongChoice,.rightChoice,.process_word").css('pointer-events','auto');
          next?nav_button_controls(100):'';
        });
    }

    function sound_player(sound_id,navigate,imgCls,imgSrc) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            nav_button_controls(100);
            if(imgCls && imgSrc){
                $("."+imgCls).attr("src",imgSrc);
            }
        });
    }

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				//ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}


	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_image3(content, count){
		if(content[count].hasOwnProperty('slider')){
			var imageblock = content[count].slider[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

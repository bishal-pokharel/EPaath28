var imgpath = $ref+"/images/playtime/";
var soundAsset = $ref + "/sounds/";
var content = [
      // slide0
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"ptime",
                  textclass: "chapter centertext",
                  textdata: data.string.playtime
              }
          ],
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: "coverpageImg",
                      imgsrc: "",
                  },
                  {
                      imgdiv: "rhinodancingdiv",
                      imgclass: " relativecls rhinoimgdiv",
                      imgid: "rhinodance",
                      imgsrc: "",
                  },
                  {
                      imgdiv: "squirrelistening",
                      imgclass: " relativecls squirrelisteningimg",
                      imgid: "squirrel",
                      imgsrc: "",
                  }]
          }]
      },
      //slide1
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "chibi",
                      imgclass: "relativecls img1",
                      imgid: "flyingchibiImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox",
                      imgclass: "relativecls img2",
                      imgid: "dialboxImg",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial content3",
                              imgtxt:data.string.pt1
                          }
                      ]
                  },
               ]
          }],
      },
  // slide 2
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "chibi",
                      imgclass: "relativecls img1",
                      imgid: "flyingchibiImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox1 shakingEffect",
                      imgclass: "relativecls img2",
                      imgid: "dialboxImg",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial1 content3",
                              imgtxt:data.string.pt2
                          }
                      ]
                  },
              ]
          }],
      },
  //    slide 3
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "zebracrossing",
                      imgclass: "relativecls img1",
                      imgid: "zebracrossingImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "car",
                      imgclass: "relativecls img2",
                      imgid: "carImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "police",
                      imgclass: "relativecls img3",
                      imgid: "policeImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "girl",
                      imgclass: "relativecls img4",
                      imgid: "girlImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox2",
                      imgclass: "relativecls img5",
                      imgid: "dialbox1Img",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial2 content4",
                              imgtxt:data.string.pt3
                          }
                      ]
                  },
              ]
          }],
      },
  //    slide 4
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle",
                  textclass: "title centertext",
                  textdata: data.string.pt4
              },
              {
                  textdiv:"rightleft blinkEffect",
                  textclass: "chapter centertext",
                  textdata: data.string.right
              }
          ],
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "zebracrossing",
                      imgclass: "relativecls img1",
                      imgid: "zebracrossingImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "girl",
                      imgclass: "relativecls img4",
                      imgid: "girlImg",
                      imgsrc: ""
                  },
              ]
          }],
      },
      //slide 5
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle",
                  textclass: "title centertext",
                  textdata: data.string.pt4
              },
              {
                  textdiv:"leftright blinkEffect",
                  textclass: "chapter centertext",
                  textdata: data.string.left
              }
          ],
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "zebracrossing",
                      imgclass: "relativecls img1",
                      imgid: "zebracrossingImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "girl",
                      imgclass: "relativecls img4",
                      imgid: "rightgirlImg",
                      imgsrc: ""
                  },
              ]
          }],
      },
  //slide 6
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "zebracrossing show1",
                      imgclass: "relativecls img1",
                      imgid: "zebracrossingImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "girl crossing show1",
                      imgclass: "relativecls img2",
                      imgid: "girl1Img",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "chibi1 show2",
                      imgclass: "relativecls img3",
                      imgid: "flyingchibiImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox3 show2",
                      imgclass: "relativecls img4",
                      imgid: "dialboxImg",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial3 content3",
                              imgtxt:data.string.pt5
                          }
                      ]
                  },
                  {
                      imgdiv: "option opt1",
                      imgclass: "relativecls img5",
                      imgid: "tedyImg",
                      imgsrc: "",
                      ans:"tedy"
                  },
                  {
                      imgdiv: "option opt2 ",
                      imgclass: "relativecls img6",
                      imgid: "ballImg",
                      imgsrc: "",
                      ans:"ball"
                  },
                  {
                      imgdiv: "option opt3",
                      imgclass: "relativecls img7",
                      imgid: "icecreamImg",
                      imgsrc: "",
                      ans:"icecream"
                  },
                  {
                      imgdiv: "option opt4",
                      imgclass: "relativecls img8",
                      imgid: "chocolateImg",
                      imgsrc: "",
                      ans:"chocolate"
                  },
                  {
                      imgdiv: "option opt5",
                      imgclass: "relativecls img9",
                      imgid: "toycarImg",
                      imgsrc: "",
                      ans:"toycar"
                  }
              ]
          }],
          svgblock:[
              {
                  svgblock: 'mainbag',
                  svgclass:"",
                  handclass:"relativecls centertext"
              },
          ]
      },
  //    slide 7
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg1",
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "uma",
                      imgclass: "relativecls img1",
                      imgid: "umaImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox4",
                      imgclass: "relativecls img2",
                      imgid: "dialbox1Img",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial2 content3",
                              imgtxt:data.string.pt6
                          }
                      ]
                  },
                  {
                      imgdiv: "dustbin",
                      imgclass: "relativecls img3",
                      imgid: "dustbinImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "garbage",
                      imgclass: "relativecls img4",
                      imgid: "garbageImg",
                      imgsrc: ""
                  },
              ]
          }],
      },
  //    slide 8
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "chibi1",
                      imgclass: "relativecls img3",
                      imgid: "flyingchibiImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox3",
                      imgclass: "relativecls img4",
                      imgid: "dialboxImg",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial3 content0",
                              imgtxt:data.string.pt5
                          }
                      ]
                  },
                  {
                      imgdiv: "option opt1",
                      imgclass: "relativecls img5",
                      imgid: "tedyImg",
                      imgsrc: "",
                      ans:"tedy"
                  },
                  {
                      imgdiv: "option opt2 ",
                      imgclass: "relativecls img6",
                      imgid: "ballImg",
                      imgsrc: "",
                      ans:"ball"
                  },
                  {
                      imgdiv: "option opt3",
                      imgclass: "relativecls img7",
                      imgid: "icecreamImg",
                      imgsrc: "",
                      ans:"icecream"
                  },
                  {
                      imgdiv: "option opt4",
                      imgclass: "relativecls img8",
                      imgid: "chocolateImg",
                      imgsrc: "",
                      ans:"chocolate"
                  },
                  {
                      imgdiv: "option opt5",
                      imgclass: "relativecls img9",
                      imgid: "toycarImg",
                      imgsrc: "",
                      ans:"toycar"
                  }
              ]
          }],
          svgblock:[
              {
                  svgblock: 'mainbag',
                  svgclass:"",
                  handclass:"relativecls centertext"
              },
          ]
      },
  //    slide 9
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg1",
          textblock: [
              {
                  textdiv:"maintitle",
                  textclass: "title centertext",
                  textdata: data.string.pt8
              }
          ],
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "uma",
                      imgclass: "relativecls img1",
                      imgid: "umeshImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox4",
                      imgclass: "relativecls img2",
                      imgid: "dialbox1Img",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial2 content3",
                              imgtxt:data.string.pt7
                          }
                      ]
                  },
                  {
                      imgdiv: "tap show1",
                      imgclass: "relativecls img3",
                      imgid: "tap1Img",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "tap show2",
                      imgclass: "relativecls img4",
                      imgid: "tap2Img",
                      imgsrc: ""
                  },
              ]
          }],
      },
  //    slide 10
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "chibi1",
                      imgclass: "relativecls img3",
                      imgid: "flyingchibiImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox3",
                      imgclass: "relativecls img4",
                      imgid: "dialboxImg",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial3 content3",
                              imgtxt:data.string.pt5
                          }
                      ]
                  },
                  {
                      imgdiv: "option opt1",
                      imgclass: "relativecls img5",
                      imgid: "tedyImg",
                      imgsrc: "",
                      ans:"tedy"
                  },
                  {
                      imgdiv: "option opt2 ",
                      imgclass: "relativecls img6",
                      imgid: "ballImg",
                      imgsrc: "",
                      ans:"ball"
                  },
                  {
                      imgdiv: "option opt3",
                      imgclass: "relativecls img7",
                      imgid: "icecreamImg",
                      imgsrc: "",
                      ans:"icecream"
                  },
                  {
                      imgdiv: "option opt4",
                      imgclass: "relativecls img8",
                      imgid: "chocolateImg",
                      imgsrc: "",
                      ans:"chocolate"
                  },
                  {
                      imgdiv: "option opt5",
                      imgclass: "relativecls img9",
                      imgid: "toycarImg",
                      imgsrc: "",
                      ans:"toycar"
                  }
              ]
          }],
          svgblock:[
              {
                  svgblock: 'mainbag',
                  svgclass:"",
                  handclass:"relativecls centertext"
              },
          ]
      },
  //    slide 11
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          imageblock:[{
              imagestoshow: [
                  {
                      imgdiv: "chibi2",
                      imgclass: "relativecls img3",
                      imgid: "flyingchibiImg",
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dialbox5",
                      imgclass: "relativecls img4",
                      imgid: "dialboxImg",
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dial5 content4",
                              imgtxt:data.string.pt9
                          }
                      ]
                  }
              ]
          }],
          svgblock:[
              {
                  svgblock: 'mainbag1',
                  svgclass:"shakingEffect1",
                  handclass:"relativecls centertext"
              },
          ]
      },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    var preload;
    var timeoutvar = null;
    var current_sound="";
    var sound="";
    var tempfirstarray = new Array();
    var endpageex =  new EndPageofExercise();
    var message = "";
    endpageex.init(3);
    loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
             {id: "coverpageImg", src:imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "bgImg", src: imgpath+"bg_playtime.png", type: createjs.AbstractLoader.IMAGE},
             {id: "flyingchibiImg", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "dialboxImg", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "dialbox1Img", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
             {id: "zebracrossingImg", src: imgpath+"zebra_crossing01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "policeImg", src: imgpath+"policeman01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "girlImg", src: imgpath+"sanbina03.png", type: createjs.AbstractLoader.IMAGE},
             {id: "carImg", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rightgirlImg", src: imgpath+"sanbina01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "leftgirlImg", src: imgpath+"sanbina02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "tedyImg", src: imgpath+"teddy.png", type: createjs.AbstractLoader.IMAGE},
             {id: "ballImg", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
             {id: "icecreamImg", src: imgpath+"ice_cream.png", type: createjs.AbstractLoader.IMAGE},
             {id: "chocolateImg", src: imgpath+"chocolate.png", type: createjs.AbstractLoader.IMAGE},
             {id: "toycarImg", src: imgpath+"toycar.png", type: createjs.AbstractLoader.IMAGE},
             {id: "bagImg", src: imgpath+"shopping_bag.svg", type: createjs.AbstractLoader.IMAGE},
             {id: "umaImg", src: imgpath+"uma01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "dustbinImg", src: imgpath+"dustbin.png", type: createjs.AbstractLoader.IMAGE},
             {id: "garbageImg", src: imgpath+"garbage.png", type: createjs.AbstractLoader.IMAGE},
             {id: "umeshImg", src: imgpath+"umesh01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "tap1Img", src: imgpath+"washing_hand.png", type: createjs.AbstractLoader.IMAGE},
             {id: "tap2Img", src: imgpath+"washing_hand01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "girl1Img", src: imgpath+"sabina_walking.gif", type: createjs.AbstractLoader.IMAGE},
            //sounds
            {id: "ex1_p2", src: soundAsset+"ex1_p2.ogg"},
            {id: "ex1_p3", src: soundAsset+"ex1_p3.ogg"},
            {id: "ex1_p4", src: soundAsset+"ex1_p4.ogg"},
            {id: "ex1_p5_1", src: soundAsset+"ex1_p5_1.ogg"},
            {id: "ex1_p5_2", src: soundAsset+"ex1_p5_2.ogg"},
            {id: "ex1_p6", src: soundAsset+"ex1_p6.ogg"},
            {id: "ex1_p7", src: soundAsset+"ex1_p7.ogg"},
            {id: "ex1_p8", src: soundAsset+"ex1_p8.ogg"},
            {id: "ex1_p9", src: soundAsset+"ex1_p9.ogg"},
            {id: "ex1_p10_1", src: soundAsset+"ex1_p10_1.ogg"},
            {id: "ex1_p10_2", src: soundAsset+"ex1_p10_2.ogg"},
            {id: "ex1_p11", src: soundAsset+"ex1_p11.ogg"},
            {id: "ex1_p12", src: soundAsset+"ex1_p12.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    // $refreshBtn.click(function(){
    //     templateCaller();
    // });
    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        firstPagePlayTime(countNext);
        texthighlight($board);
        put_image(content, countNext);
        switch (countNext){
            case 4:
                sound_player("ex1_p5_1",0);
                $(".rightleft").click(function(){
                    sound_player("ex1_p5_2",1);
                    $(this).removeClass("blinkEffect");
                    $(".girl img").attr("src",preload.getResult("rightgirlImg").src);
                });
                break;
            case 5:
                $(".leftright").click(function(){
                  sound_player("ex1_p"+(countNext+1),1);
                    $(this).removeClass("blinkEffect");
                    $(".girl img").attr("src",preload.getResult("leftgirlImg").src);
                });
                break;
            case 6:
                setTimeout(function(){
                    sound_player("ex1_p"+(countNext+1),0);
                },4000);
                $(".show2,.option").hide();
                $(".show1").delay(2800).fadeOut(1000);
                $(".show2,.option").delay(3300).fadeIn(1000);
                loadsvg('#mainbag',"bagImg");
                break;
            case 7:
              sound_player("ex1_p"+(countNext+1),0);
                $(".garbage").click(function(){
                    $(this).addClass("dirtinbin");
                    navigationcontroller();
                });
                break;
            case 8:
            case 10:
              sound_player("ex1_p"+(countNext+1),0);
                removegifts();
                loadsvg('#mainbag',"bagImg");
                break;
            case 9:
                $(".show2,.dialbox4, .maintitle").hide();
                $(".dialbox4").fadeIn(500);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("ex1_p10_1");
                current_sound.play();
                current_sound.on("complete", function(){
                  $(".maintitle").fadeIn(500);
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("ex1_p10_2");
                  current_sound.play();
                  current_sound.on("complete", function(){
                    $(".tap").click(function(){
                        $(".show1").fadeOut(1000);
                        $(".show2").fadeIn(500);
                        $(".show2,.dialbox4").addClass("avoid-clicks");
                        nav_button_controls(300);
                    });
                  });
                });
                break;
            case 11:
              sound_player("ex1_p"+(countNext+1),0);
                $(".dialbox5").css({"top":"8%","width":"40%"});
                $(".dialbox5 p").css({"top":"17%","padding":"0%"});
                endpageex.endpage(message);
                loadsvg('#mainbag1',"bagImg");
                break;
            default:
              sound_player("ex1_p"+(countNext+1),1);
                // navigationcontroller();
              break;
        }
    }



  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.lessonEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }


  function sound_player(sound_id,next){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function(){
      next?nav_button_controls(1000):'';
    });
  }



    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('imghintblock')) {
            var contentCount=content[count].imghintblock[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }
    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                // lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });




    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                 $(this).html(replaceinstring);
            });
        }
    }

    function loadsvg(svgid,imgid){
        var s = Snap(svgid);
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s.append(loadedFragment);
            var delaytime = (countNext==6)?3300:0;
            $(svgid).delay(delaytime).animate({"opacity":"1"},1000);
            $(".giftitems").hide();
            for(var i=0;i<tempfirstarray.length;i++){
                $("."+tempfirstarray[i]).show();
            }
            choosegift();
        });
    }

    //select gift and store the selected gift in temporary array
    function choosegift(){
        $(".option").click(function(){
            var itemselected = $(this).attr("data-answer");
            $("."+itemselected).show();
            tempfirstarray.push(itemselected);
            $(this).remove();
            arrangepos(true);
            navigationcontroller();
        });
    }
    //arrange position of option after removing selected gift
    function arrangepos(diableoption){
        $(".option").each(function(index){
            $(this).removeClass().addClass("option opt"+(index+1));
            diableoption==true?$(this).addClass("avoid-clicks"):'';
        });
    }
    //intially remove all gifts that were previously selected and stored in temp array
    function removegifts(){
        $(".option").each(function(index){
            if(tempfirstarray.indexOf($(this).attr("data-answer"))>-1){
                $(this).remove();
                arrangepos(false);
            }
        });
    }
});

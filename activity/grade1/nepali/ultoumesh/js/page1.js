var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
      //slide0
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "coverback",
          textblock: [
              {
                  textdiv:"chtitle",
                  textclass: "datafont centertext",
                  textdata: data.string.chapter
              },
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'covermainImg',
                      imgsrc: ""
                  },

              ]
          }]
      },
      // slide1
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle show1",
                  textclass: "content centertext",
                  textdata: data.string.p1text1
              },
              {
                  textdiv:"maintitle1 show2",
                  textclass: "content centertext",
                  textdata: data.string.p1text2
              },
              {
                  textdiv:"line1",
              },
              {
                  textdiv:"line2",
              }
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'one',
                      imgsrc: ""
                  },
              ]
          }],
      },
      //slide 2
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle show1",
                  textclass: "content centertext",
                  textdata: data.string.p1text3
              },
              {
                  textdiv:"maintitle1 show2",
                  textclass: "content centertext",
                  textdata: data.string.p1text4
              },
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'two_a',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "coverpage show2",
                      imgclass: "relativecls img2",
                      imgid: 'two_b',
                      imgsrc: ""
                  },
              ]
          }],
      },
      //slide 3
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle show1",
                  textclass: "content centertext",
                  textdata: data.string.p1text5
              },
              {
                  textdiv:"maintitle1 show2",
                  textclass: "content centertext",
                  textdata: data.string.p1text6
              },
              {
                  textdiv:"emptyrightdiv"
              }
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'three',
                      imgsrc: ""
                  },
              ]
          }],
      },
      //slide 4
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle show1",
                  textclass: "content centertext",
                  textdata: data.string.p1text7
              },
              {
                  textdiv:"maintitle1 show2",
                  textclass: "content centertext",
                  textdata: data.string.p1text8
              },
              {
                  textdiv:"emptyrightdiv"
              }
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'four',
                      imgsrc: ""
                  },
              ]
          }],
      },
      //slide 5
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle show1",
                  textclass: "content centertext",
                  textdata: data.string.p1text9
              },
              {
                  textdiv:"maintitle1 show2",
                  textclass: "content centertext",
                  textdata: data.string.p1text10
              },
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'five',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "cat1 show1",
                      imgclass: "relativecls img2",
                      imgid: 'cat',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "cat2 show2",
                      imgclass: "relativecls img3",
                      imgid: 'cat',
                      imgsrc: ""
                  },
              ]
          }],
      },
  // slide 6
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle show1",
                  textclass: "content centertext",
                  textdata: data.string.p1text11
              },
              {
                  textdiv:"maintitle1 show2",
                  textclass: "content centertext",
                  textdata: data.string.p1text12
              },
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'six',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "umesh show2",
                      imgclass: "relativecls img2",
                      imgid: 'umesh',
                      imgsrc: ""
                  },
              ]
          }],
      },
  //    slide 7
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle2 show1",
                  textclass: "content centertext",
                  textdata: data.string.p1text13
              },
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'seven',
                      imgsrc: ""
                  }
              ]
          }],
      },
  //    slide 8
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle2 show1",
                  textclass: "content centertext",
                  textdata: data.string.p1text14
              },
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'eight',
                      imgsrc: ""
                  }
              ]
          }],
      },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "covermainImg", src: imgpath + "cover.png", type: createjs.AbstractLoader.IMAGE},
            {id: "one", src: imgpath + "one.png", type: createjs.AbstractLoader.IMAGE},
            {id: "two", src: imgpath + "one.png", type: createjs.AbstractLoader.IMAGE},
            {id: "two_a", src: imgpath + "two_a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "two_b", src: imgpath + "two_c.png", type: createjs.AbstractLoader.IMAGE},
            {id: "three", src: imgpath + "two.png", type: createjs.AbstractLoader.IMAGE},
            {id: "four", src: imgpath + "three_a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "five", src: imgpath + "four.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cat", src: imgpath + "cat_big.png", type: createjs.AbstractLoader.IMAGE},
            {id: "six", src: imgpath + "five.png", type: createjs.AbstractLoader.IMAGE},
            {id: "umesh", src: imgpath + "fiveb.png", type: createjs.AbstractLoader.IMAGE},
            {id: "seven", src: imgpath + "six.png", type: createjs.AbstractLoader.IMAGE},
            {id: "eight", src: imgpath + "seven.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "s1_p1", src: soundAsset + "s1_p1.ogg"},
            {id: "s1_p2", src: soundAsset + "s1_p2.ogg"},
            {id: "s1_p2_1", src: soundAsset + "s1_p2_1.ogg"},
            {id: "s1_p3", src: soundAsset + "s1_p3.ogg"},
            {id: "s1_p3_1", src: soundAsset + "s1_p3_1.ogg"},
            {id: "s1_p4", src: soundAsset + "s1_p4.ogg"},
            {id: "s1_p4_1", src: soundAsset + "s1_p4_1.ogg"},
            {id: "s1_p5", src: soundAsset + "s1_p5.ogg"},
            {id: "s1_p5_1", src: soundAsset + "s1_p5_1.ogg"},
            {id: "s1_p6", src: soundAsset + "s1_p6.ogg"},
            {id: "s1_p6_1", src: soundAsset + "s1_p6_1.ogg"},
            {id: "s1_p7", src: soundAsset + "s1_p7.ogg"},
            {id: "s1_p7_1", src: soundAsset + "s1_p7_1.ogg"},
            {id: "s1_p8", src: soundAsset + "s1_p8.ogg"},
            {id: "s1_p8_1", src: soundAsset + "s1_p8_1.ogg"},
            {id: "s1_p9", src: soundAsset + "s1_p9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        put_image(content, countNext, preload);
        vocabcontroller.findwords(countNext);
        showtextonebyone();
        switch (countNext) {
            case 1:
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p2");
              current_sound.play();
              current_sound.on('complete', function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p2_1");
                current_sound.play();
                $(".line2").delay(2500).animate({"opacity":"1","width":"3.3%"},2000);
                current_sound.on('complete', function(){
                  nav_button_controls(100);
                });
              });
                $(".line1").delay(1000).animate({"opacity":"1","width":"10.2%"},2000);
                break;
            case 2:
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p3");
              current_sound.play();
              current_sound.on('complete', function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p3_1");
                current_sound.play();
                // $(".coverpage").delay(3500).animate({"width":"93%","right":"7%"},2000);
                current_sound.on('complete', function(){
                  nav_button_controls(100);
                });
              });
                break;
            case 3:
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p4");
              current_sound.play();
              current_sound.on('complete', function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p4_1");
                current_sound.play();
                $(".coverpage").delay(3500).animate({"width":"93%","right":"7%"},2000);
                current_sound.on('complete', function(){
                  nav_button_controls(100);
                });
              });
                break;
            case 4:
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p5");
              current_sound.play();
              current_sound.on('complete', function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p5_1");
                current_sound.play();
                $(".coverpage").delay(3500).animate({"width":"70%"},2000);
                current_sound.on('complete', function(){
                  nav_button_controls(100);
                });
              });
              break;
              case 5:
              case 6:
              case 7:
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p"+(countNext+1));
                current_sound.play();
                current_sound.on('complete', function(){
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
                  current_sound.play();
                  current_sound.on('complete', function(){
                    nav_button_controls(100);
                  });
                });
              break;
            case 8:
                sound_player("s1_p"+(countNext+1),1);
                break;

            default:
                sound_player("s1_p"+(countNext+1),1);
                // navigationcontroller(countNext, $total_page);
                break;
        }
    }

    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.lessonEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }


    function sound_player(sound_id,next){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on("complete", function(){
        next?nav_button_controls(1000):'';
      });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function showtextonebyone(){
        $(".show1").delay(500).animate({"opacity":"1"},1000);
        $(".show2").delay(6000).animate({"opacity":"1"},1000);
        navigationcontroller(countNext,$total_page);
    }
});

var imgpath = $ref + "/playtime/images/";
var imgpath2 = $ref + "/images/playtime/forhover/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide 1
	{
		//playtime first page
	},
	// slide 2---first exc start
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "picClk",
			textdata: data.string.exc_txt_1
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_clouds",
				imgsrc: "",
			},{
				imgclass: "clickables sclAnim clk_1",
				imgid: "circle02",
				imgsrc: "",
			},{
				imgclass: "clickables gryScld clk_2",
				imgid: "circle01",
				imgsrc: "",
			},{
				imgclass: "clickables gryScld clk_3 ",
				imgid: "circle03",
				imgsrc: "",
			}]
		}]
	},
	// slide 3
	{
		contentblockadditionalclass:"creamBg",
		extratextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_2
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_mouse01",
				imgsrc: "",
			},{
				imgopn:true,
				imgopnclass:"ansOpn tigerClass class1",
				imgclass: "tigerFst",
				imgid: "tiger01",
				imgsrc: "",
			},{
				imgopn:true,
				imgopnclass:"ansOpn mouseClass",
				imgclass: "mouseFst",
				imgid: "mouse",
				imgsrc: "",
			}]
		}]
	},
	// slide 4
	{
		contentblockadditionalclass:"zmAnimFst",
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_mouse01",
				imgsrc: "",
			},{
				imgclass: "tigerwlk",
				imgid: "tiger02",
				imgsrc: "",
			},{
				imgclass: "msSec",
				imgid: "mouse",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'inverted',
			imgid:"text_box06",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.exc_txt_3
		}]
	},
	// slide 5
	{
		contentblockadditionalclass:"fstZmd zmOut",
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_mouse01",
				imgsrc: "",
			},{
				imgclass: "tigerwlk",
				imgid: "tiger02",
				imgsrc: "",
			},{
				imgclass: "msSec",
				imgid: "mouse",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"text_box03",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.exc_txt_4
		}]
	},
	// slide 6
	{
		extratextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_5_sec
		},{
			textclass: "vayoTxt",
			textdata: data.string.vayo
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_mouse01",
				imgsrc: "",
			},{
				imgclass: "tigerwlk tgrSml",
				imgid: "tiger02",
				imgsrc: "",
			},{
				imgclass: "msSec mmousSml",
				imgid: "mouse",
				imgsrc: "",
			},{
				imgclass: "lion",
				imgid: "lion02",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:"sp-3",
			imgclass:'',
			imgid:"text_box03",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_5
		}]
	},
	// slide 7
	{
		extratextblock:[{
			textclass: "vayoTxt",
			textdata: data.string.vayo
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_mouse01",
				imgsrc: "",
			},{
				imgclass: "tigerwlk tgrSml inverted",
				imgid: "tiger02",
				imgsrc: "",
			},{
				imgclass: "msSec mmousSml",
				imgid: "mouse",
				imgsrc: "",
			},{
				imgclass: "lion",
				imgid: "lion01",
				imgsrc: "",
			}]
		}],
		// speechbox:[{
		// 	speechbox:"sp-3",
		// 	imgclass:'',
		// 	imgid:"text_box03",
		// 	imgsrc:'',
		// 	textclass:"textInSp spTxt4",
		// 	textdata:data.string.exc_txt_5
		// }]
	},
	// slide 8
	{
		extratextblock:[{
			textclass: "congratTxt",
			textdata: data.string.exc_txt_8
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_mouse_dance",
				imgsrc: "",
			},{
				imgclass: "dancing_mouse",
				imgid: "dancing_mouse",
				imgsrc: "",
			}]
		}]
	},
	// slide 9---sec exc start
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "picClk",
			textdata: data.string.exc_txt_1
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_clouds",
				imgsrc: "",
			},{
				imgclass: "clickables gryScld clk_1",
				imgid: "circle02",
				imgsrc: "",
			},{
				imgclass: "clickables sclAnim clk_2",
				imgid: "circle01",
				imgsrc: "",
			},{
				imgclass: "clickables gryScld clk_3 ",
				imgid: "circle03",
				imgsrc: "",
			}]
		}]
	},
	// slide 10
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "river01",
				imgsrc: "",
			},{
				imgclass: "boy_in_river",
				imgid: "boy_in_river",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp-4',
			imgclass:'',
			imgid:"text_box03",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_10
		}]
	},
	// slide 11
	{
		extratextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_11
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "river01",
				imgsrc: "",
			},{
				imgclass: "boy_in_river",
				imgid: "boy_in_river",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp-4',
			imgclass:'',
			imgid:"text_box03",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_10
		}]
	},
	// slide 12
	{
		extratextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_12
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "river02",
				imgsrc: "",
			}]
		}],
		// speechbox:[{
		// 	speechbox:'sp-4',
		// 	imgclass:'',
		// 	imgid:"text_box03",
		// 	imgsrc:'',
		// 	textclass:"textInSp spTxt4",
		// 	textdata:data.string.exc_txt_10
		// }]
	},
	// slide 13
	{
		contentblockadditionalclass:"ZmToFace",
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "river03",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp_5-s13spb',
			imgclass:'inverted',
			imgid:"text_box01",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_13
		}]
	},
	// slide 14
	{
		extratextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_14
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bgGrn",
				imgsrc: "",
			},{
				imgopn:true,
				startposextra:"bkTubeCorinCor",
				imgopnclass:"ansOpn bucketClas",
				imgclass: "tigerFst smlbucket",
				imgid: "bucket",
				imgsrc: "",
			},{
				imgopn:true,
				startposextra:"bkTubeCorinCor",
				imgopnclass:"ansOpn tubeClas class1",
				imgclass: "mouseFst",
				imgid: "tube",
				imgsrc: "",
			}]
		}]
	},
	// slide 15--animations left
	{
		extratextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_5_sec
		},{
			textclass: "vayoTxt",
			textdata: data.string.vayo
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "river_man01",
				imgsrc: "",
			},{
				imgclass: "bir_sec",
				imgid: "boy_in_river",
				imgsrc: "",
			},{
				imgclass: "tubetoBoy",
				imgid: "tube",
				imgsrc: "",
			},{
				imgclass: "boy_with_tube",
				imgid: "boy_with_tube",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp_5-tp30',
			imgclass:'',
			imgid:"text_box02",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_15
		}]
	},
	// // slide 16
	// {
	// 	imageblock:[{
	// 		imagestoshow:[
	// 		{
	// 			imgclass: "bg_full",
	// 			imgid: "river05",
	// 			imgsrc: "",
	// 		}]
	// 	}]
	// },
	// slide 17
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "river07",
				imgsrc: "",
			}]
		}]
	},
	// slide 18
	{
		extratextblock:[{
			textclass:"ThnkYouTxt",
			textdata:data.string.exc_txt_18
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "river08",
				imgsrc: "",
			}]
		}]
	},
	// slide 19---third exc start
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "picClk",
			textdata: data.string.exc_txt_1
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bg_clouds",
				imgsrc: "",
			},{
				imgclass: "clickables gryScld clk_1",
				imgid: "circle02",
				imgsrc: "",
			},{
				imgclass: "clickables gryScld clk_2",
				imgid: "circle01",
				imgsrc: "",
			},{
				imgclass: "clickables sclAnim clk_3 ",
				imgid: "circle03",
				imgsrc: "",
			}]
		}]
	},
	// slide 20
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "school01",
				imgsrc: "",
			},{
				imgclass: "grlCry",
				imgid: "girl_crying_01_gif",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp_6',
			imgclass:'',
			imgid:"text_box06",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_20_fst
		}]
	},
	// slide 21
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "school01",
				imgsrc: "",
			},{
				imgclass: "grlCry",
				imgid: "girl_crying_01_gif",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp_5-grlsp',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_20
		}]
	},
	// slide 22
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "school01",
				imgsrc: "",
			},{
				imgclass: "grlCry",
				imgid: "girl_crying_01",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp_6',
			imgclass:'',
			imgid:"text_box06",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_21
		}]
	},
	// slide 23
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_22
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bgGrn",
				imgsrc: "",
			},{
				imgopn:true,
				imgopnclass:"mobHolding",
				imgclass: "mobile",
				imgid: "mobile01",
				imgsrc: "",
			}]
		}]
	},
	// slide 24
	{
		contentblockadditionalclass:"creamBg",
		// uppertextblock:[{
		// 	textclass: "insTrn",
		// 	textdata: data.string.exc_txt_22
		// }],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "bgGrn",
				imgsrc: "",
			},{
				imgopn:true,
				imgopnclass:"mobHolding",
				imgclass: "mobile",
				imgid: "mobile02",
				imgsrc: "",
			}]
		}]
	},
	// slide 25
	{
		contentblockadditionalclass:"creamBg",
		// uppertextblock:[{
		// 	textclass: "insTrn",
		// 	textdata: data.string.exc_txt_22
		// }],
		imgwithspchtozoom:true,
		imgwithspch:"ZmToFace",
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "talking01",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp_5-grlspSec',
			imgclass:'',
			imgid:"text_box05",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_24
		}]
	},
	// slide 26
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_5_sec
		},{
			textclass: "vayoTxt",
			textdata: data.string.vayo
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bg_full",
				imgid: "talking02",
				imgsrc: "",
			}]
		}],
		speechbox:[{
			speechbox:'sp_5-grldad',
			imgclass:'',
			imgid:"text_box07",
			imgsrc:'',
			textclass:"textInSp spTxt4",
			textdata:data.string.exc_txt_25
		}]
	},
	// slide 27
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "insTrn",
			textdata: data.string.exc_txt_26
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "imgwithspch bgc2",
				imgid: "bg_car02",
				imgsrc: "",
			},{
				imgclass: "bg_full bgc1",
				imgid: "bg_car01",
				imgsrc: "",
			},{
				imgclass: "car crCmng",
				imgid: "car_coming",
				imgsrc: "",
			},{
				imgclass: "car crGoing",
				imgid: "car_going",
				imgsrc: "",
			}]
		}]
	},
	// slide 28
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "byetxt",
			textdata: data.string.exc_txt_28
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "imgwithspch bgc2",
				imgid: "bye_bye",
				imgsrc: "",
			}]
		}]
	},
	// slide 29
	{
		contentblockadditionalclass:"prplbg",
		sliderload:true,
		slider:[{
			imagestoshow:[{
				imgclass : "slideimg si1",
				imgsrc : '',
				imgid : 'main_menu01',
			},{
				imgclass : "slideimg si2",
				imgsrc : '',
				imgid : 'main_menu02',
			},{
				imgclass : "slideimg si3",
				imgsrc : '',
				imgid : 'main_menu03',
			}]
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var selPgeArr=[];

	var preload;
	var timeoutvar = null;
	var current_sound;

  var endpageex =  new EndPageofExercise();
  endpageex.init(7);
  var message =  " तिमीले विद्यालयको पहिलो दिन शीर्षकको पाठ पढ्‍यौ।"
	var tmout;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg_clouds", src: imgpath+"bg_clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river02", src: imgpath+"river02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school02", src: imgpath+"school02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "main_menu01", src: imgpath+"main_menu01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "main_menu02", src: imgpath+"main_menu02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "main_menu03", src: imgpath+"main_menu03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg_mouse01", src: imgpath+"bg_mouse01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger01", src: imgpath+"tiger01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouse", src: imgpath+"mouse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger02", src: imgpath+"tiger02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box06", src: imgpath+"text_box06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box03", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lion02", src: imgpath+"lion02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lion01", src: imgpath+"lion01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_mouse_dance", src: imgpath+"bg_mouse_dance.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dancing_mouse", src: imgpath+"dancing_mouse.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "circle01", src: imgpath+"circle01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circle02", src: imgpath+"circle02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circle03", src: imgpath+"circle03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "river01", src: imgpath+"imgs_drwn/river_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_in_river", src: imgpath+"imgs_drwn/boy_in_river.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "river_man01", src: imgpath+"imgs_drwn/river_man01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_with_tube", src: imgpath+"imgs_drwn/boy_with_tube.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river02", src: imgpath+"river02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river03", src: imgpath+"river03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river04", src: imgpath+"river04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river05", src: imgpath+"river05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river06", src: imgpath+"river06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river07", src: imgpath+"river07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "river08", src: imgpath+"river08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bucket", src: imgpath+"bucket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tube", src: imgpath+"tube.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bgGrn", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},

			{id: "school01", src: imgpath+"school01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school02", src: imgpath+"school02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "talking01", src: imgpath+"talking01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "talking02", src: imgpath+"talking02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mobile01", src: imgpath+"mobile01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mobile02", src: imgpath+"mobile02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_car01", src: imgpath+"bg_car01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_car02", src: imgpath+"bg_car02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "car_coming", src: imgpath+"car_coming.png", type: createjs.AbstractLoader.IMAGE},
			{id: "car_going", src: imgpath+"car_going.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bye_bye", src: imgpath+"bye_bye.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_crying_01", src: imgpath+"girl_crying_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_crying_01_gif", src: imgpath+"girl_crying_01.gif", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "text_box07", src: imgpath+"text_box07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box06", src: imgpath+"text_box06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box05", src: imgpath+"text_box05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box03", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box01", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "rewards_page", src: soundAsset+"rewards_page.ogg"},
			{id: "ex_2", src: soundAsset+"ex_2.ogg"},
			{id: "ex_3", src: soundAsset+"ex_3.ogg"},
			{id: "ex_4", src: soundAsset+"ex_4.ogg"},
			{id: "ex_5", src: soundAsset+"ex_5.ogg"},
			{id: "ex_6", src: soundAsset+"ex_6.ogg"},
			{id: "ex_8", src: soundAsset+"ex_8.ogg"},
			{id: "ex_10", src: soundAsset+"ex_10.ogg"},
			{id: "ex_11", src: soundAsset+"ex_11.ogg"},
			{id: "ex_12", src: soundAsset+"ex_12.ogg"},
			{id: "ex_13", src: soundAsset+"ex_13.ogg"},
			{id: "ex_14", src: soundAsset+"ex_14.ogg"},
			{id: "ex_15", src: soundAsset+"ex_15.ogg"},
			{id: "ex_17", src: soundAsset+"ex_17.ogg"},
			{id: "ex_19", src: soundAsset+"ex_19.ogg"},
			{id: "ex_20", src: soundAsset+"ex_20.ogg"},
			{id: "ex_21", src: soundAsset+"ex_21.ogg"},
			{id: "ex_22", src: soundAsset+"ex_22.ogg"},
			{id: "ex_23", src: soundAsset+"ex_23.ogg"},
			{id: "ex_24", src: soundAsset+"ex_24.ogg"},
			{id: "ex_25", src: soundAsset+"ex_25.ogg"},
			{id: "ex_26", src: soundAsset+"ex_26.ogg"},
			{id: "ex_27", src: soundAsset+"ex_27.ogg"},
			{id: "tiger", src: soundAsset+"tiger.mp3"},
			{id: "tiger1", src: soundAsset+"playtime1-6.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
 // 	var pagenumbers  = new pagenameholder();
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		content[countNext].sliderload?put_image3(content, countNext):"";
		var pagenumbers;


 	/*for randomizing the options*/
		var parent = $(".opnBlk");
		var divs = parent.children();
		while (divs.length) {
 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

	$('.playagain').click(function(){
		$('#activity-page-exercise-tab').find('button').trigger('click');
	});
	$('.learnagain').click(function(){
		$("#activity-page-lesson-tab").find('button').trigger('click');

	});
	$('.mainmenu').click(function(){
		$("#activity-page-menu-img").trigger("click");

	});


		switch (countNext) {
			case 0:
				firstPagePlayTime();
				nav_button_controls();
			break;
			case 2:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("ex_"+(countNext+1));
			current_sound.play();
			current_sound.on("complete", function(){
				var current_sound1 = createjs.Sound.play("tiger");
				current_sound1.play();
			});
			break;
			case 13:
				sound_player("ex_"+(countNext+1),0);
			break;
			case 5:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("ex_"+(countNext+1));
			current_sound.play();
			current_sound.on("complete", function(){
				var current_sound1 = createjs.Sound.play("tiger1");
				current_sound1.play();
				current_sound1.on("complete", function(){
				next?nav_button_controls(0):'';
				});
			});
			$(".vayoTxt").delay(3000).fadeIn(100);
			$(".vayoTxt").click(function(){
				countNext++;
				templateCaller();
			});
		break;
			case 22:
				sound_player("ex_23",true);
				break;
			case 24:
				sound_player("ex_6",0);
				// sound_player("ex_"+(countNext+1),0);
				$(".vayoTxt").delay(3000).fadeIn(100);
				$(".vayoTxt").click(function(){
					countNext++;
					templateCaller();
				});
			break;
			case 6:
			case 15:
				nav_button_controls(1000);
			break;
			case 8:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("ex_2");
			current_sound.play();
			$(".clickables").click(function(){
				countNext++;
				templateCaller();
			});
			break;
			case 1:
			case 17:
				sound_player("ex_"+(countNext+1),0);
				$(".clickables").click(function(){
					countNext++;
					templateCaller();
				});
			break;
			case 14:
				$(".vayoTxt").delay(3000).fadeIn(100);
				$(".vayoTxt").click(function(){
					countNext++;
					templateCaller();
				});
				setTimeout(function(){
						$(".bir_sec, .tubetoBoy").delay(50).hide(100);
						$(".boy_with_tube").fadeIn(100);
						$(".boy_with_tube").addClass("tubeToMan");
				},2500);
			break;
			case 21:
				sound_player("ex_22",0);
				$(".mobHolding").append("<div class='btnDiv'></div>");
				$(".btnDiv").click(function(){
					setTimeout(function(){
						countNext++;
						templateCaller();
					},500);
				});
			break;
			break;
			case 25:
				sound_player("ex_"+(countNext+1),0);
				$(".crCmng").animate({
					left: "10%"
				},2000,function(){
					$(".bgc1").fadeOut(100);
					$(".insTrn").html(data.string.exc_txt_27);
					$(".crGoing").fadeIn(10);
					$(".crCmng").delay(50).fadeOut(10);
					tmout = setTimeout(function(){
						$(".crGoing").animate({
							left:'100%'
						},3000,function(){
							nav_button_controls(1000);
						});
					},1000);
				});
			break;
			case 27:
				sound_player("rewards_page");
				$(".next-button").on("click", function(){
				$(".next-button, .prev-button ").css('pointer-events', 'none');
					var selectImg = $(".slider:eq(0)");
					$(".innerwrapper").append(selectImg.clone());
					selectImg.animate({
						width:"0%",
						padding:"0%"
					},1000, function(){	createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(0):'';
		});
						selectImg.remove();
						$(".next-button, .prev-button").css('pointer-events', 'auto');
					});
				});

				$(".prev-button").on("click", function(){
					$(".next-button, .prev-button").css('pointer-events', 'none');
					var selectImg = $(".slider").last();
					$(".innerwrapper").prepend(selectImg.clone());
					selectImg.remove();
					var selectFirstImg = $(".slider:eq(0)");
					selectFirstImg.animate({
						width:"0%",
					},0,function(){
						selectFirstImg.animate({
							width:"20%"
						},1000);
						setTimeout(function(){
							$(".next-button, .prev-button").css('pointer-events', 'auto');
						},900);
					});
				});
				endpageex.endpage(message);
			break;
			default:
				sound_player("ex_"+(countNext+1),1);
		}
		// for clicks to change the page
		// $(".vayoTxt, .clickables").click(function(){
		// 	countNext++;
		// 	templateCaller();
		// });

		// for click of answer options
		$(".ansOpn").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).children(".corctopt").show(0);
				$(".ansOpn").css("pointer-events","none");
				nav_button_controls(0);
			}
			else{
				play_correct_incorrect_sound(0);
				$(this).children(".wrngopt").show(0);
				$(this).css("pointer-events","none");
			}
		});
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(0):'';
		});
	}

	function put_image(content, count){
			if(content[count].hasOwnProperty('imageblock')){
				for(var j = 0; j < content[count].imageblock.length; j++){
					var imageblock = content[count].imageblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
		}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

		function put_image3(content, count){
			if(content[count].hasOwnProperty('slider')){
				var imageblock = content[count].slider[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

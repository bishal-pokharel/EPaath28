var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// coverpage
	{
		extratextblock:[{
			textclass:"chapterName",
			textdata:data.lesson.chapter,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'cvpg',
			}]
		}]
	},
	// slide 2
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s2_txt,
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"fullPgImg-zoom1",//
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'bg_01',
			},{
				imgclass : "eyeBlinkGirl",
				imgsrc : '',
				imgid : 'eye_blink',
			}]
		}]
	},
	// slide 3
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s3_txt,
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"fullPgImg-zoom1 zoomOutAnim",//
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'bg_01',
			},{
				imgclass : "eyeBlinkGirl",
				imgsrc : '',
				imgid : 'eye_blink',
			}]
		}]
	},
	// slide 4
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s4_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'school',
			}]
		}]
	},
	// slide 5
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s5_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background scooterZoomIn",
				imgsrc : '',
				imgid : 'school',
			}]
		}]
	},
	// slide 6
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s6_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background sctrZm zomToBus",
				imgsrc : '',
				imgid : 'school',
			}]
		}]
	},
	// slide 7
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s7_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background busZm zmToCycle",
				imgsrc : '',
				imgid : 'school',
			}]
		}]
	},
	// slide 8
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s8_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background cyclZm zmToWalk",
				imgsrc : '',
				imgid : 'school',
			}]
		}]
	},
	// slide 9
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s9_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'with_mum_bg01',
			},{
				imgclass : "blinkEyeGrl",
				imgsrc : '',
				imgid : 'girl_blinking_eye',
			}]
		}]
	},
	// slide 10
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s10_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'mum_nodding_bg01',
			},{
				imgclass : "grlBlnkSml",
				imgsrc : '',
				imgid : 'girl_blinking_eye',
			},{
				imgclass : "mum_nodding",
				imgsrc : '',
				imgid : 'mum_nodding',
			}]
		}]
	},
	// slide 11
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s11_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'with_maam_bg',
			},{
				imgclass : "teacherGif",
				imgsrc : '',
				imgid : 'teacher',
			},{
				imgclass : "glBlnkVrySml",
				imgsrc : '',
				imgid : 'girl_blinking_eye_png',
			}]
		}]
	},
	// slide 12
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s12_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'mum_waving_hand_bg01',
			},{
				imgclass : "mum_waving_hand",
				imgsrc : '',
				imgid : 'mum_waving_hand',
			},{
				imgclass : "grlBlnkSml",
				imgsrc : '',
				imgid : 'girl_blinking_eye',
			}]
		}]
	},
	// slide 13
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s13_txt,
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"fullPgImg zmGrlCry zmGrlCryAnim",
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'bg_girl_cry',
			},{
				imgclass : "grlCryingWthMom",
				imgsrc : '',
				imgid : 'girl_crying',
			}]
		}]
	},
	// slide 14
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s14_txt,
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"fullPgImg mamZmIn",
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'bg_20',
			},{
				imgclass : "maam_blinking_eye",
				imgsrc : '',
				imgid : 'maam_blinking_eye',
			},{
				imgclass : "girl_blinking_eye_with_mum",
				imgsrc : '',
				imgid : 'girl_blinking_eye_with_mum',
			}]
		}]
	},
	// slide 15
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5",
			textdata:data.string.p1s15_txt,
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"fullPgImg mamZm mamInGrlOut",
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'bg_20',
			},{
				imgclass : "maam_blinking_eye",
				imgsrc : '',
				imgid : 'maam_blinking_eye',
			},{
				imgclass : "girl_blinking_eye_with_mum",
				imgsrc : '',
				imgid : 'girl_blinking_eye_with_mum_Png',
			}]
		}]
	},
	// slide 16
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5 bttxt",
			textdata:data.string.p1s16_txt,
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"fullPgImg zmImMomTlk sclZmOut",
			imagestoshow:[{
				imgclass : "background bgfst",
				imgsrc : '',
				imgid : 'bg_25',
			}]
		}]
	},
	// slide 17
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5 bttxt",
			textdata:data.string.p1s17_txt,
		}],
		imageblock:[{
			imgindiv:true,
			imgdivclass:"fullPgImg",
			imagestoshow:[{
				imgclass : "background bgSec",
				imgsrc : '',
				imgid : 'bg_26',
			}]
		}]
	},
	// slide 18
	{
		extratextblock:[{
			textclass:"btmTxt-ylwLow-pad1-fnt5 bttxt",
			textdata:data.string.p1s18_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'bg_29',
			},{
				imgclass : "girl_blinking_eye",
				imgsrc : '',
				imgid : 'girl_blinking_eye01',
			}]
		}]
	},
	// slide 19
	{
		extratextblock:[{
			textclass:"sidetxt",
			textdata:data.string.p1s19_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass : "background",
				imgsrc : '',
				imgid : 'bg_29',
			},{
				imgclass : "girl_blinking_eye",
				imgsrc : '',
				imgid : 'girl_blinking_eye01',
			}]
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var my_interval = null;
	var my_interval2 = null;
	//for preload
	var preload;
	var timeoutvar = null;
	var timeoutArr = [];
	var current_sound;
	var arrayCountToShow = 0;
	var arrClkToShow = 0;
	var arrayNumber = 0;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cvpg", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg_29", src: imgpath+"bg_29.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_20", src: imgpath+"bg_20.png", type: createjs.AbstractLoader.IMAGE},
			{id: "with_mum", src: imgpath+"with_mum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_01", src: imgpath+"bg_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eye_blink", src: imgpath+"eye_blink.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "school", src: imgpath+"school.png", type: createjs.AbstractLoader.IMAGE},
			{id: "with_mum_bg01", src: imgpath+"with_mum_bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_blinking_eye", src: imgpath+"girl_blinking_eye.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_blinking_eye_png", src: imgpath+"girl_blinking_eye.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mum_nodding_bg01", src: imgpath+"mum_nodding_bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mum_nodding", src: imgpath+"mum_nodding.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "with_maam_bg", src: imgpath+"with_maam_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teacher", src: imgpath+"teacher.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mum_waving_hand_bg01", src: imgpath+"mum_waving_hand_bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mum_waving_hand", src: imgpath+"mum_waving_hand.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_crying", src: imgpath+"girl_crying.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_girl_cry", src: imgpath+"bg_girl_cry.png", type: createjs.AbstractLoader.IMAGE},
			{id: "maam_blinking_eye", src: imgpath+"maam_blinking_eye.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_blinking_eye_with_mum", src: imgpath+"girl_blinking_eye_with_mum.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_20", src: imgpath+"bg_20.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_blinking_eye_with_mum_Png", src: imgpath+"girl_blinking_eye_with_mum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_25", src: imgpath+"bg-25.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_26", src: imgpath+"bg-26.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_29", src: imgpath+"bg_29.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_blinking_eye01", src: imgpath+"girl_blinking_eye01.gif", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "s1_p12", src: soundAsset+"s1_p12.ogg"},
			{id: "s1_p13", src: soundAsset+"s1_p13.ogg"},
			{id: "s1_p14", src: soundAsset+"s1_p14.ogg"},
			{id: "s1_p15", src: soundAsset+"s1_p15.ogg"},
			{id: "s1_p16", src: soundAsset+"s1_p16.ogg"},
			{id: "s1_p17", src: soundAsset+"s1_p17.ogg"},
			{id: "s1_p18", src: soundAsset+"s1_p18.ogg"},
			{id: "s1_p19", src: soundAsset+"s1_p19.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image2(content, countNext):"";
		content[countNext].sliderload?put_image3(content, countNext):"";
		var count  = 0;
		switch(countNext) {
			case 4:
			case 5:
			case 6:
			case 7:
				$("[class*='btmTxt']").css("bottom","-30%");
				setTimeout(function(){
					$("[class*='btmTxt']").animate({"bottom":"0%"},1000);
						sound_player("s1_p"+(countNext+1),1);
				},3100);
			break;
			case 14:
				sound_player("s1_p"+(countNext+1),0);
				nav_button_controls(8100);
			break;
			default:
				sound_player("s1_p"+(countNext+1),1);
			// nav_button_controls(1000);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nxtBtnFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		if(nxtBtnFlag){
			current_sound.on('complete',function(){
				nav_button_controls(0);
			});
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].imgspkrcontainer[0].hasOwnProperty('imagedivblock')){
			var imagblockVal = content[count].imgspkrcontainer[0];
			for(var j=0; j<imagblockVal.imagedivblock.length; j++){
				var imageblock = imagblockVal.imagedivblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

	function put_image3(content, count){
		if(content[count].hasOwnProperty('slider')){
			var imageblock = content[count].slider[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
		slides(i);
		$($('.totalsequence')[i]).html(i);
		$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
	});
}
function slides(i){
$($('.totalsequence')[i]).click(function(){
countNext = i;
createjs.Sound.stop();
templateCaller();
});
}
*/
}

$nextBtn.on('click', function() {
	clearTimeout(timeoutvar);
	createjs.Sound.stop();
	for(var i=0; i<timeoutArr.length; i++){
		clearTimeout(timeoutArr[i]);
	}
	switch(countNext) {
		default:
		countNext++;
		templateCaller();
		break;
	}
});

$refreshBtn.click(function(){
	templateCaller();
});

$prevBtn.on('click', function() {
	createjs.Sound.stop();
	clearTimeout(timeoutvar);
	for(var i=0; i<timeoutArr.length; i++){
		clearTimeout(timeoutArr[i]);
	}
	countNext--;
	templateCaller();
	/* if footerNotificationHandler pageEndSetNotification was called then on click of
	previous slide button hide the footernotification */
	countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
});

});

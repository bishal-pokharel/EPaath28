var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/orgsounds/";

var content=[


 // kha
// slide 5 -->3 boxes
{
	contentblockadditionalclass:"khaBg",
	imageload:true,
	extratextblock:[{
		textclass:"boxes_top_txt",
		textdata:data.string.boxes_top_txt,
	}],
	imgspkrcontainer:[{
		imagedivblock:[{
			imagescontainerclass:"btmContainer",
			imagestoshow:[{
				imagediv:"imgdiv div1",
					speaker:true,
					imgclass : "imgbox ib1",
					imgsrc : '',
					imgid : 'kharayo',
					datahighlightflag:'true',
					textclass:"boxTxt",
					textdata:data.string.kharayo,
					speakerimgclass:"speakerimg spkr1"
				},{
				imagediv:"imgdiv div2",
					speaker:true,
					imgclass : "imgbox ib2",
					imgsrc : '',
					imgid : 'khal',
					datahighlightflag:'true',
					textclass:"boxTxt",
					textdata:data.string.khal,
					speakerimgclass:"speakerimg spkr2"
				},{
				imagediv:"imgdiv div3",
					speaker:true,
					imgclass : "imgbox ib3",
					imgsrc : '',
					imgid : 'khasi',
					datahighlightflag:'true',
					textclass:"boxTxt",
					textdata:data.string.khasi,
					speakerimgclass:"speakerimg spkr3"
			}]
		}]
	}]
},
// slide 6-->3 boxes
{
	contentblockadditionalclass:"khaBg",
	imageload:true,
	imgspkrcontainer:[{
		imagedivblock:[{
			imagescontainerclass:"btmContainer",
			imagestoshow:[{
				ltranim:true,
				imagediv:"imgdiv div1",
					imgclass : "imgbox ib1",
					imgsrc : '',
					imgid : 'kharayo',
					datahighlightflag:'true',
					datahighlightcustomclass:'blueLtr',
					textclass:"boxTxt",
					textdata:data.string.kharayo,
					speakerimgclass:"speakerimg spkr1",
					ltr:data.string.kha
				},{
					ltranim:true,
				imagediv:"imgdiv div2",
					imgclass : "imgbox ib2",
					imgsrc : '',
					imgid : 'khal',
					datahighlightflag:'true',
					datahighlightcustomclass:'blueLtr',
					textclass:"boxTxt",
					textdata:data.string.khal,
					speakerimgclass:"speakerimg spkr2",
					ltr:data.string.kha
				},{
					ltranim:true,
				imagediv:"imgdiv div3",
					imgclass : "imgbox ib3",
					imgsrc : '',
					imgid : 'khasi',
					datahighlightflag:'true',
					datahighlightcustomclass:'blueLtr',
					textclass:"boxTxt",
					textdata:data.string.khasi,
					speakerimgclass:"speakerimg spkr3",
					ltr:data.string.kha
			}]
		}]
	}]
},
// slide 7
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-b border',
	extratextblock:[{
		textdata: '',
		textclass: "divider divider-b",
	}],

	imageblock:[{
		imagestoshow : [
			{
				imgclass : "letter-left",
				imgsrc : '',
				imgid : 'letter-kha'
			}
		]
	}],
	svgblock: [{
		svgblock: 'letter-svg',
	}],
},
// slide 8 ->slider
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-b',
	sliderload:true,
	extratextblock:[
	{
		textdata: '',
		textclass: "rightDiv",
	}],
	imageblock:[{
		imagestoshow : [
			{
				imgclass : "letter-left",
				imgsrc : '',
				imgid : 'letter-kha'
			}
		]
	}],
	slider:[{
		sliderbgclass:"khaBg",
		imagestoshow:[{
			imgclass : "slideimg si1",
			imgsrc : '',
			imgid : 'kharayo',
			textdata: data.string.kharayo,
			textclass: "slideTxt",
			datahighlightflag:'true',
			datahighlightcustomclass:'redLtr',
		},{
			imgclass : "slideimg si2",
			imgsrc : '',
			imgid : 'khal',
			textdata: data.string.khal,
			textclass: "slideTxt",
			datahighlightflag:'true',
			datahighlightcustomclass:'redLtr',
		},{
			imgclass : "slideimg si3",
			imgsrc : '',
			imgid : 'khasi',
			textdata: data.string.khasi,
			textclass: "slideTxt",
			datahighlightflag:'true',
			datahighlightcustomclass:'redLtr',
		}]
	}]
},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var my_interval = null;
	var my_interval2 = null;
	//for preload
	var preload;
	var timeoutvar = null;
	var timeoutArr = [];
	var current_sound;
	var arrayCountToShow = 0;
	var arrClkToShow = 0;
	var arrayNumber = 0;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cvpg", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},


			{id: "kharayo", src: imgpath+"p2/rabbit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "khal", src: imgpath+"p2/khal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "khasi", src: imgpath+"p2/goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "letter-kha", src: imgpath+"letters/kha_path.svg", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_letter_2", src: soundAsset+"7.ogg"},

			{id: "coverSound", src: soundAsset+"1.ogg"},
			{id: "sound_2", src: soundAsset+"7.ogg"},
			{id: "sound_10", src: soundAsset+"s1p2.ogg"},


			{id: "6_kharayo", src: soundAsset+"6_kharayo.ogg"},
			{id: "6_khal", src: soundAsset+"6_khal.ogg"},
			{id: "6_khasi", src: soundAsset+"6_khasi.ogg"},

			{id: "9_kharayo", src: soundAsset+"9_kharayo.ogg"},
			{id: "9_khal", src: soundAsset+"9_khal.ogg"},
			{id: "9_khasi", src: soundAsset+"9_khasi.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image2(content, countNext):"";
		content[countNext].sliderload?put_image3(content, countNext):"";
	 	var count  = 0;

		var soundCount = 0;
		function btnClk(sounds, arrayNumber){
			var count = 1;
			sound_player(sounds[arrayNumber][count]);
			$(".arrow").click(function(){
				count+=1;
				soundCount+=1;
				count>2?count = 0:count;
			$(".arrow").removeClass("scld");
			$(".arrow").css('pointer-events', 'none');
				var selectImg = $(".slider:eq(0)");
				$(".innerwrapper").append(selectImg.clone());
				selectImg.animate({
					height: "0%",
					padding: "0%"
				},1000,function(){
					selectImg.remove();
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play(sounds[arrayNumber][count]);
					current_sound_1.play();
					current_sound_1.on('complete', function(){
						$(".arrow").css('pointer-events', 'auto');
					$(".arrow").addClass("scld");
						soundCount==2?nav_button_controls(0):"";
					});
				});
			});
		}
		function clickAndPlay(clickCount, arrayCountToShow, soundArr){
			$(".div1, .div2, .div3").find("img:eq(0)").on("click", function(){
				$(this).parent().children(".boxTxt").show(0);
				var parentCLs = $(this).parent().attr('class');
				if($(this).parent().hasClass("div1")){
					$(this).css("pointer-events","none");
					sound_player(soundArr[arrayCountToShow][0]);
					clickCount+=1;
				}
				else if ($(this).parent().hasClass("div2")) {
					$(this).css("pointer-events","none");
					sound_player(soundArr[arrayCountToShow][1]);
					clickCount+=1;
				}
				else {
					$(this).css("pointer-events","none");
					sound_player(soundArr[arrayCountToShow][2]);
					clickCount+=1;
				}
				if(clickCount == 3){
					// $(".speakerimg").css("pointer-events","auto");
					$(".speakerimg").css("display","block");
					nav_button_controls(100);
				}
			});
			$(".spkr1").on('click', function(){
				sound_player(soundArr[arrayCountToShow][0]);
			});
			$(".spkr2").on('click', function(){
				sound_player(soundArr[arrayCountToShow][1]);
			});
			$(".spkr3").on('click', function(){
				sound_player(soundArr[arrayCountToShow][2]);
			});
		}
		switch(countNext) {
			case 0:
				sound_player("sound_10");
				var clickCount=0;
				$(".boxTxt").hide(0);
				$(".div1, .div2, .div3").find("img:eq(0)").mouseover(function(){
					$(this).addClass("hoveredImg");
				})
				.mouseleave(function(){
					$(this).removeClass("hoveredImg");
				});
				var soundArr = [
												["6_kharayo", "6_khal", "6_khasi"],
											];

				countNext==0?arrayCountToShow=0:"";
				clickAndPlay(clickCount, arrayCountToShow, soundArr);
			break;
			case 1:
			$(".xtratxt").hide(0);
				var spanTxt = $(".boxTxt:eq(0)").find('span').text();
				$(".boxTxt").delay(500).fadeOut(1500);
				$(".xtratxt").delay(1300).fadeIn(1500);
				setTimeout(function(){
					$(".xtratxt").addClass("animLtr");
					countNext==1?$(".khaBg").animate({"background-color":"#EAD1DC"},3000):"";
					// countNext==6?$(".khaBg").animate({"background-color":"#EAD1DC"},3000):
					$(".div1, .div2, .div3").delay(3200).animate({
						left:"10%"
					},1000, function(){
						nav_button_controls(0);
					});
				},3000);
			break;
			case 3:
				var soundArray = [
													["9_kharayo", "9_khal", "9_khasi"],
												];
				 arrayNumber = countNext==3?0:"";
				 $(".arrow").addClass("scld");
				btnClk(soundArray, arrayNumber);
		break;
		case 2:
			if(countNext>0) $prevBtn.show(0);
			var letterSoundArray = ['sound_letter_2'];
			var soundArray = ['sound_2'];
			var letterSVG = ['letter-kha'];
			var letterArr = ['kha'];
			var s = Snap('#letter-svg');
			// var newCount = countNext/3;
			countNext==2?newCount=0:"";
			var path = [], group, animate_letter = null;
			var svg = Snap.load(preload.getResult(letterSVG[newCount]).src, function ( loadedFragment ) {
				s.append(loadedFragment);
				//to resive whole svg
				for(var i=0; i<5; i++){
					if(Snap.select('#path'+i)!=null)
					path.push(Snap.select('#path'+i));
				}
				group = Snap.select('#gr');
				path[0].addClass('opacity_0');
				// countNext==18?
				// var dot = Snap.select("#dot").addClass('opacity_0');
				var animatePath = function(pathName, index, callback){
					var curIndex = index;
					// var soundName = ('step_'+(pathName.length-1))+(index+1);
					// audio for 1, 2, 3....
					// sound_player(soundName);
					console.log(pathName);
					if(index == 0){
						pathName[index+1].addClass('line-anim-1');
					} else{
						pathName[index+1].addClass('line-anim-css');
					}
					timeoutvar = setTimeout(function(){
						curIndex = index+1;
						if(index<pathName.length-2){
							callback(pathName, curIndex, callback);
						}else{
									sound_player(letterSoundArray[newCount]);
									$nextBtn.show(0);
						}
					}, 2000);
				};
				animate_letter = function(){
						group.attr('display', 'block');
						path[0].addClass('letter-fade-in-'+letterArr[newCount]);
						group.addClass('letter-fade-out-'+letterArr[newCount]);
						for(var i=1; i<path.length; i++){
							path[i].addClass('no-dash-'+letterArr[newCount]);
						}
						animatePath(path, 0, animatePath);
				};
			} );
			current_sound = createjs.Sound.play(soundArray[newCount]);
			current_sound.play();
			current_sound.on("complete", function(){
				animate_letter();
			});
		break;
		default:
			nav_button_controls(1000);
		break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		$(".imgdiv").css("pointer-events","none");
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			$(".imgdiv").css("pointer-events","auto");
		});
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

		function put_image2(content, count){
			if(content[count].imgspkrcontainer[0].hasOwnProperty('imagedivblock')){
				var imagblockVal = content[count].imgspkrcontainer[0];
				for(var j=0; j<imagblockVal.imagedivblock.length; j++){
					var imageblock = imagblockVal.imagedivblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
		}

		function put_image3(content, count){
			if(content[count].hasOwnProperty('slider')){
				var imageblock = content[count].slider[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		clearTimeout(timeoutvar);
		createjs.Sound.stop();
		for(var i=0; i<timeoutArr.length; i++){
			clearTimeout(timeoutArr[i]);
		}
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		for(var i=0; i<timeoutArr.length; i++){
			clearTimeout(timeoutArr[i]);
		}
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

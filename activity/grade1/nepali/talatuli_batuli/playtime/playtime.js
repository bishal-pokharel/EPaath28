var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/poem/";
var cloth_store = ['','','',''];


var sound_l_1 = new buzz.sound((soundAsset1 + "1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset1 + "2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset1 + "3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset1 + "4.ogg"));
var sound_l_5 = new buzz.sound((soundAsset1 + "5.ogg"));
var sound_l_6 = new buzz.sound((soundAsset1 + "6.ogg"));
var sound_l_7 = new buzz.sound((soundAsset1 + "7.ogg"));
var sound_l_8 = new buzz.sound((soundAsset1 + "8.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8];

var content=[
	// slide0
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg1",
			uppertextblock: [
					{
							textclass: "playtime",
							textdata: data.string.diytext7
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							}]
			}]
	},
	// slide1
	{
		contentblockadditionalclass:'brown_bg',
			uppertextblock:[{
					textdata:data.string.diytext1,
					textclass:'top_instruction'
			},
			{
					textdata:data.string.diytext6,
					textclass:'don_button'
			}],
			selectorbox:[{
						selectorboxclass:'jama-box',
						define_next_catagory: 'jama-next_button',
						define_prev_catagory: 'jama-prev_button',
						textdata:data.string.diytext2,
						imageinsideclass:'jama-image',
			},
			{
						selectorboxclass:'ribon-box',
						define_next_catagory: 'ribon-next_button',
						define_prev_catagory: 'ribon-prev_button',
						textdata:data.string.diytext3,
						imageinsideclass:'ribon-image',
			},
			{
						selectorboxclass:'patuki-box',
						define_next_catagory: 'patuki-next_button',
						define_prev_catagory: 'patuki-prev_button',
						textdata:data.string.diytext4,
						imageinsideclass:'patuki-image',
			},
			{
						selectorboxclass:'choli-box',
						define_next_catagory: 'choli-next_button',
						define_prev_catagory: 'choli-prev_button',
						textdata:data.string.diytext5,
						imageinsideclass:'choli-image',
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass:'middoll',
					imgid:'maindoll'
				},
				{
					imgclass:'middoll jama-doll displaynone',
					imgid:'maindoll'
				},
				{
					imgclass:'middoll ribon-doll displaynone',
					imgid:'maindoll'
				},
				{
					imgclass:'middoll patuki-doll displaynone',
					imgid:'maindoll'
				},
				{
					imgclass:'middoll choli-doll displaynone',
					imgid:'maindoll'
				}]
			}]
	},
	//slide2
	{
		contentblockadditionalclass:'brown_bg' ,
		imageblock:[{
			imagestoshow:[{
				imgclass:'middoll1',
				imgid:'maindoll'
			},
			{
				imgclass:'middoll1 jama-doll displaynone',
				imgid:'maindoll'
			},
			{
				imgclass:'middoll1 ribon-doll displaynone',
				imgid:'maindoll'
			},
			{
				imgclass:'middoll1 patuki-doll displaynone',
				imgid:'maindoll'
			},
			{
				imgclass:'middoll1 choli-doll displaynone',
				imgid:'maindoll'
			},

			{
				imgclass:'jama-dolla displaynone',
				imgid:'ribon2a'
			},
			{
				imgclass:'ribon-dolla displaynone',
				imgid:'ribon2a'
			},
			{
				imgclass:'patuki-dolla displaynone',
				imgid:'ribon2a'
			},
			{
				imgclass:'choli-dolla displaynone',
				imgid:'ribon2a'
			}]
		}],
		headerblock:[{
			textclass: "description_title",
			textdata: data.lesson.chapter
		},{
			textclass: "writer",
			textdata: data.string.writer
		}],
		uppertextblockadditionalclass: "left_50",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.para1_stanza1
		},{
			textclass: "description",
			textdata: data.string.para1_stanza2,
			breakafterp: true
		},{
			textclass: "description",
			textdata: data.string.para2_stanza1
		}],
		lowertextblockadditionalclass: "right_50",
		lowertextblock:[,{
			textclass: "description",
			textdata: data.string.para2_stanza2
		},{
			textclass: "description",
			textdata: data.string.para3_stanza1
		},{
			textclass: "description",
			textdata: data.string.para3_stanza2,
			breakafterp: true
		}]
	},
	//slide3
	{
			contentblockadditionalclass:'brown_bg',
        imageblock:[{
            imagestoshow:[
            	{
					imgclass:'middoll2 ',
					imgid:'maindoll'
                },
                {
                    imgclass:'middoll2 jama-doll displaynone',
                    imgid:'maindoll'
                },
                {
                    imgclass:'middoll2 ribon-doll displaynone',
                    imgid:'maindoll'
                },
                {
                    imgclass:'middoll2 patuki-doll displaynone',
                    imgid:'maindoll'
                },
                {
                    imgclass:'middoll2 choli-doll displaynone',
                    imgid:'maindoll'
                }
                ]
        }],
			// uppertextblock:[{
			// 		textdata:data.string.diytext8,
			// 		textclass:'middel_text'
			// }],
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "btn01", src: imgpath+"btn01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "btn02", src: imgpath+"btn02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "jama1", src: imgpath+"diy/jama1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jama2", src: imgpath+"diy/jama2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jama3", src: imgpath+"diy/jama3.png", type: createjs.AbstractLoader.IMAGE},

			{id: "patuki1", src: imgpath+"diy/patuki1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "patuki2", src: imgpath+"diy/patuki2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "patuki3", src: imgpath+"diy/patuki3.png", type: createjs.AbstractLoader.IMAGE},

			{id: "ribon1", src: imgpath+"diy/ribon1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ribon2", src: imgpath+"diy/ribon2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ribon3", src: imgpath+"diy/ribon3.png", type: createjs.AbstractLoader.IMAGE},

			{id: "choli1", src: imgpath+"diy/shirt1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "choli2", src: imgpath+"diy/shirt2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "choli3", src: imgpath+"diy/shirt3.png", type: createjs.AbstractLoader.IMAGE},

			{id: "jama1a", src: imgpath+"diy/materials/dhoti02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jama2a", src: imgpath+"diy/materials/dhoti01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jama3a", src: imgpath+"diy/materials/dhoti03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "patuki1a", src: imgpath+"diy/materials/patuki02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "patuki2a", src: imgpath+"diy/materials/patuki01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "patuki3a", src: imgpath+"diy/materials/patuki03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "ribon1a", src: imgpath+"diy/materials/ribon02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ribon2a", src: imgpath+"diy/materials/ribon03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ribon3a", src: imgpath+"diy/materials/ribon01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "choli1a", src: imgpath+"diy/materials/cholo02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "choli2a", src: imgpath+"diy/materials/cholo03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "choli3a", src: imgpath+"diy/materials/cholo01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "maindoll", src: imgpath+"/diy/doll.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},



			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"Ex_ins_1.ogg"},
			{id: "sound_2", src: soundAsset+"Ex_ins_2.ogg"},
			{id: "sound_3", src: soundAsset+"Ex_end.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	 var audiocontroller = new AudioController($total_page, true,  160, 800, vocabcontroller);
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
         firstPagePlayTime(countNext);
		var pbox1, pbox2,pbox2, ptext1, ptext2, ptext3;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var button_counter = 0;
		$nextBtn.hide(0);
		$('.prev_button').attr("src",preload.getResult('btn01').src);
		$('.next_button').attr("src",preload.getResult('btn02').src);
		ole.footerNotificationHandler.hideNotification();
		$description = $(".description");
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
                $nextBtn.show(0);
				break;
			case 1:
                sound_player("sound_1");
				$('.don_button').hide(0);
				$('.next_button').addClass('zoom_in_out');
				$('.choli-image').css('width','65%');
				functioning_selector('ribon','.ribon-image','.ribon-next_button','.ribon-prev_button');
				functioning_selector('patuki','.patuki-image','.patuki-next_button','.patuki-prev_button');
				functioning_selector('choli','.choli-image','.choli-next_button','.choli-prev_button');
				functioning_selector('jama','.jama-image','.jama-next_button','.jama-prev_button');
				$('.don_button').click(function(){
						$('.box,.next_button,.prev_button').addClass('blackandwhite').css({'pointer-events':'none'});
						$(this).css({'pointer-events':'none','opacity':'.5'});
						$('.top_instruction').text(data.string.diytext9).animate({'top':'2%','border-radius':'1vmin','width':'50%'},500);
						$nextBtn.show(0);
                    sound_player("sound_2");
                    console.log('the selected are :' +cloth_store[0]+','+cloth_store[1]+','+cloth_store[2]+','+cloth_store[3]);
				});
				break;
			case 2:
				console.log('the selected are :' +cloth_store[0]+','+cloth_store[1]+','+cloth_store[2]+','+cloth_store[3]);
				$('.jama-doll').attr("src",preload.getResult(cloth_store[0]).src).css('display','block');
				$('.ribon-doll').attr("src",preload.getResult(cloth_store[1]).src).css('display','block');
				$('.patuki-doll').attr("src",preload.getResult(cloth_store[2]).src).css('display','block');
				$('.choli-doll').attr("src",preload.getResult(cloth_store[3]).src).css('display','block');
				$('.jama-dolla').attr("src",preload.getResult(cloth_store[0]+'a').src).css('display','block');
				$('.ribon-dolla').attr("src",preload.getResult(cloth_store[1]+'a').src).css('display','block');
				$('.patuki-dolla').attr("src",preload.getResult(cloth_store[2]+'a').src).css('display','block');
				$('.choli-dolla').attr("src",preload.getResult(cloth_store[3]+'a').src).css('display','block');
				audiocontroller.init($description, sound_group_p1, countNext);
				break;

			case 3:
                $('.jama-doll').attr("src",preload.getResult(cloth_store[0]).src).css('display','block');
                $('.ribon-doll').attr("src",preload.getResult(cloth_store[1]).src).css('display','block');
                $('.patuki-doll').attr("src",preload.getResult(cloth_store[2]).src).css('display','block');
                $('.choli-doll').attr("src",preload.getResult(cloth_store[3]).src).css('display','block');
                $('.jama-dolla').attr("src",preload.getResult(cloth_store[0]+'a').src).css('display','block');
                $('.ribon-dolla').attr("src",preload.getResult(cloth_store[1]+'a').src).css('display','block');
                $('.patuki-dolla').attr("src",preload.getResult(cloth_store[2]+'a').src).css('display','block');
                $('.choli-dolla').attr("src",preload.getResult(cloth_store[3]+'a').src).css('display','block');

                // ole.footerNotificationHandler.hideNotification();
                var endpageex =  new EndPageofExercise();
                var message =  data.string.diytext8;
                endpageex.init(3);
                endpageex.endpage(message);
                sound_player("sound_3");
				break;

		}

		function functioning_selector(cloth_type,cloth_image_selector,cloth_nextbutton_class,cloth_prevbutton_class){
			var array_image_id=[cloth_type+1,cloth_type+2,cloth_type+3];
			// array_image_id.shufflearray();
			// $(cloth_image_selector).click(function(){
				// $('.'+cloth_type+'-doll').attr("src",preload.getResult(array_image_id[cloth_counter]).src).css('display','block');
			// });
			var cloth_counter=0
			$(cloth_image_selector).attr("src",preload.getResult(array_image_id[cloth_counter]+'a').src);
			$(cloth_prevbutton_class).css({'pointer-events':'none','opacity':'.5'});
			$(cloth_nextbutton_class).click(function(){
				if(cloth_counter==0){
					button_counter++;
					console.log(button_counter);
					if(button_counter>3){
						$('.don_button').show(500);
					}
				}
				$(this).removeClass('zoom_in_out');
				cloth_counter++;
				$('.'+cloth_type+'-doll').attr("src",preload.getResult(array_image_id[cloth_counter]).src).css('display','block');
				// $(cloth_image_selector).trigger('click');
				if(cloth_counter==2){
					$(this).css({'pointer-events':'none','opacity':'.5'});
				}
				if(cloth_counter<2){
					$(this).css({'pointer-events':'auto','opacity':'1'});
				}
				if(cloth_counter>0){
					$(cloth_prevbutton_class).css({'pointer-events':'auto','opacity':'1'});
				}
				$(cloth_image_selector).attr("src",preload.getResult(array_image_id[cloth_counter]+'a').src);
				if(cloth_type=='jama')
				{
					cloth_store[0]=array_image_id[cloth_counter];
				}
				if(cloth_type=='ribon')
				{
					cloth_store[1]=array_image_id[cloth_counter];
				}
				if(cloth_type=='patuki')
				{
					cloth_store[2]=array_image_id[cloth_counter];
				}
				if(cloth_type=='choli')
				{
					cloth_store[3]=array_image_id[cloth_counter];
				}
			});
			$(cloth_prevbutton_class).click(function(){
				cloth_counter--;
				$('.'+cloth_type+'-doll').attr("src",preload.getResult(array_image_id[cloth_counter]).src).css('display','block');
				// $(cloth_image_selector).trigger('click');
				if(cloth_counter==0){
					$(this).css({'pointer-events':'none','opacity':'.5'});
				}
				if(cloth_counter>0){
					$(this).css({'pointer-events':'auto','opacity':'1'});
				}
				if(cloth_counter<2){
					$(cloth_nextbutton_class).css({'pointer-events':'auto','opacity':'1'});
				}
				$(cloth_image_selector).attr("src",preload.getResult(array_image_id[cloth_counter]+'a').src);
				if(cloth_type=='jama')
				{
					cloth_store[0]=array_image_id[cloth_counter];
				}
				if(cloth_type=='ribon')
				{
					cloth_store[1]=array_image_id[cloth_counter];
				}
				if(cloth_type=='patuki')
				{
					cloth_store[2]=array_image_id[cloth_counter];
				}
				if(cloth_type=='choli')
				{
					cloth_store[3]=array_image_id[cloth_counter];
				}
			});

		}
}
    function sound_player(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }
function sound_player_nav(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	current_sound.on("complete", function(){
		if(typeof click_class != 'undefined'){
			$(click_class).click(function(){
				current_sound.play();
			});
		}
		nav_button_controls(1000);
	});
}
function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.lessonEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
function templateCaller() {
	$prevBtn.css('display', 'none');
	$nextBtn.css('display', 'none');

	navigationcontroller();

	loadTimelineProgress($total_page, countNext + 1);
	generaltemplate();
	/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					templateCaller();
				});
			}
	*/
}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

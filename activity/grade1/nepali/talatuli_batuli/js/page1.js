var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "green_bg",
		singletext:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		},
		{
			textclass: "writername",
			textdata: data.string.writername
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "ground_object1",
				imgid : 'talatule',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "green_bg",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p1part1,
		textdata2: data.string.p1part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg1',
			imgsrc: ""
		},
		{
			imgclass: "ground_object",
			imgid : 'talatule',
			imgsrc: ""
		},
		{
			imgclass: "main_char",
			imgid : 'girlwith_putali',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "green_bg",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p2part1,
		textdata2: data.string.p2part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "putali",
			imgid : 'putali',
			imgsrc: ""
		},
		{
			imgclass: "car",
			imgid : 'car',
			imgsrc: ""
		},
		{
			imgclass: "toy",
			imgid : 'toy',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "green_bg",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p3part1,
		textdata2: data.string.p3part2,
		textdata3: data.string.p3part3,

	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "putali_big",
			imgid : 'putali',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "green_bg",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p4part1,
		textdata2: data.string.p4part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "putali_slide4",
			imgid : 'putali',
			imgsrc: ""
		},
		{
			imgclass: "putali_pink_tiki",
			imgid : 'putali_pink_tiki',
			imgsrc: ""
		},
		{
			imgclass: "bulaki",
			imgid : 'bulaki',
			imgsrc: ""
		}
	]
}]
},
// slide5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "green_bg",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p5part1,
		textdata2: data.string.p5part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "putali_gif",
			imgid : 'chulthi_moving',
			imgsrc: ""
		},

	]
}]
},
// slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "green_bg",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p6part1,
		textdata2: data.string.p6part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg1',
			imgsrc: ""
		},
		{
			imgclass: "ground_object",
			imgid : 'talatule',
			imgsrc: ""
		},
		{
			imgclass: "main_char",
			imgid : 'girlwith_putal02',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "talatule", src: imgpath+"talatule.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwith_putali", src: imgpath+"putali.png", type: createjs.AbstractLoader.IMAGE},
			{id: "putali", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "car", src: imgpath+"doll02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "toy", src: imgpath+"doll03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "putali_pink_tiki", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bulaki", src: imgpath+"bulaki.png", type: createjs.AbstractLoader.IMAGE},
			{id: "putali_pink_withbulaki", src: imgpath+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chulthi_moving", src: imgpath+"movinghair.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "putali_pink_gajalu", src: imgpath+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwith_putal02", src: imgpath+"girlwith_putal02.png", type: createjs.AbstractLoader.IMAGE},


			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "para11", src: soundAsset+"p1_s1_1.ogg"},
			{id: "para12", src: soundAsset+"p1_s1_2.ogg"},

			{id: "para21", src: soundAsset+"p1_s2_1.ogg"},
			{id: "para22", src: soundAsset+"p1_s2_2.ogg"},

			{id: "para31", src: soundAsset+"p1_s3_1.ogg"},
			{id: "para32", src: soundAsset+"p1_s3_2.ogg"},
			{id: "para33", src: soundAsset+"p1_s3_3.ogg"},

			{id: "para41", src: soundAsset+"p1_s4_1.ogg"},
			{id: "para42", src: soundAsset+"p1_s4_2.ogg"},

			{id: "para51", src: soundAsset+"p1_s5_1.ogg"},
			{id: "para52", src: soundAsset+"p1_s5_2.ogg"},

			{id: "para61", src: soundAsset+"p1_s6_1.ogg"},
			{id: "para62", src: soundAsset+"p1_s6_2.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
     ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
		function generaltemplate() {
			current_sound.stop();
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
 
		var pbox1, pbox2,pbox2, ptext1, ptext2, ptext3;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var line_count = 0 ;
		pbox1 = "poembox1";
		pbox2 = "poembox2";
		pbox3 = "poembox3";
		$nextBtn.hide(0);
		ole.footerNotificationHandler.hideNotification();
		switch(countNext){
			case 0:
				sound_player_nav('sound_1');
				break;
			case 1:
				ptext1 = "para11";
				ptext2 = "para12";
				third_line_flag = false;
			break;
			case 2:
				ptext1 = "para21";
				ptext2 = "para22";
				third_line_flag = false;
			break;
			case 3:
				ptext1 = "para31";
				ptext2 = "para32";
				ptext3 = "para33";
				$('.putali_big').addClass('putali_big_anim1');
				third_line_flag = true;
			break;
			case 4:
			$('.bulaki').css('display','none');
			setTimeout(function(){
				$('.bulaki').css('display','block').addClass('bulaki_anim');
			},1000);
				ptext1 = "para41";
				ptext2 = "para42";
				third_line_flag = false;
			break;
			case 5:
			$('.putali_gif').attr("src" ,preload.getResult('putali_pink_tiki').src).css({'width':'19%','left':'65%'});
			$('.putali_gif').addClass('putali_gif_anim');
			setTimeout(function(){
				$('.putali_gif').addClass('putali_gif_anim1');
			},2000);
			setTimeout(function(){
				$('.putali_gif').addClass('putali_gif_anim2');
			},3000);
			setTimeout(function(){
				$('.putali_gif').attr("src" ,preload.getResult('putali_pink_gajalu').src);
			},4000);
			setTimeout(function(){
				$('.putali_gif').attr("src" ,preload.getResult('putali_pink_tiki').src);
				$('.putali_gif').addClass('putali_gif_anim3');
			},4500);
			setTimeout(function(){
				$('.putali_gif').addClass('putali_gif_anim4');
			},5500);
				ptext1 = "para51";
				ptext2 = "para52";
				third_line_flag = false;
			break;
			case 6:
				ptext1 = "para61";
				ptext2 = "para62";
				third_line_flag = false;
			break;
		}
		if(countNext>0){
			for_all_slides(pbox1, ptext1,third_line_flag);
		}

		function firstline_fin(){
			$("#span_speec_text").append("<br>");
			for_all_slides(pbox2, ptext2,third_line_flag);
			$nextBtn.hide(0);
			switch (countNext) {
				case 1:
					$('.main_char').addClass('main_char_anim');
					$('.ground_object').addClass('ground_object_anim');
					break;
				case 2:
					$('.putali').addClass('putali_anim');
					$('.car').addClass('car_anim');
					$('.toy').addClass('toy_anim');
					break;
				case 3:
					// $('.putali_big').addClass('putali_big_anim2');
					break;
				case 4:
					$('.bulaki').hide(0);
					$('.putali_slide4').attr("src" ,preload.getResult('putali_pink_withbulaki').src);
					$('.putali_slide4').addClass('putali_slide4_anim');
					$('.putali_pink_tiki').addClass('fadein');
					break;

			}
		}

		function secondline_fin(){
			$("#span_speec_text").append("<br>");
			for_all_slides(pbox3, ptext3,third_line_flag);
			switch (countNext) {
				case 3:
					break;

			}
		}

		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.lessonEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}
		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}
		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				if(typeof click_class != 'undefined'){
					$(click_class).click(function(){
						current_sound.play();
					});
				}
				nav_button_controls(1000);
			});
		}

		function for_all_slides(text_class, my_sound_data){
				var $textblack = $("."+text_class);
				var current_text = $textblack.html();
				// current_text.replace(/<.*>/, '');
				play_text($textblack, current_text);
					current_sound = createjs.Sound.play(my_sound_data);
					current_sound.play();
					current_sound.on('complete', function(){
						textanimatecomplete = false;
						if(line_count==0)
								{
									setTimeout(function(){
										console.log(line_count);
										firstline_fin();
										line_count++;
									},500);						}
						else if(line_count==1&&third_line_flag){
							console.log("Hello 2")
							setTimeout(function(){
								console.log(line_count);
									secondline_fin();
									third_line_flag = false;
									line_count++;
							},500);

						}else if(line_count==2){
							console.log(line_count);
							vocabcontroller.findwords(countNext);
							navigationcontroller();
						}else if(!third_line_flag){
							console.log("This is sthird");
							console.log(line_count);
							vocabcontroller.findwords(countNext);
							navigationcontroller();
						}
					});
		}

		var textanimatecomplete = false;

		function play_text($this, text){
			original_text =  text;
			if(isFirefox){
				$this.html("<span id='span_speec_text'></span>"+text);
				$prevBtn.hide(0);
				var $span_speec_text = $("#span_speec_text");
				// $this.css("background-color", "#faf");
				show_text($this, $span_speec_text,text, 110);	// 65 ms is the interval found out by hit and trial
			} else {
				$this.html("<span id='span_speec_text'>"+original_text+"</span>");
			}
		}

		function show_text($this,  $span_speec_text, message, interval) {
			if (0 < message.length && !textanimatecomplete) {
				var nextText = message.substring(0, 1);
				var additionalinterval = 0;
				if (nextText == "<") {
					additionalinterval = 800;
					message = message.substring(4, message.length);
				} else {
					$span_speec_text.append(nextText);
					message = message.substring(1, message.length);
				}
				$this.html($span_speec_text);
				$this.append(message);
				setTimeout(function() {
					show_text($this, $span_speec_text, message, interval);
				}, (interval + additionalinterval));
			} else{
		  		textanimatecomplete = true;
		  	}
		}
	}

		function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

		function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
	}

		function templateCaller() {

	$prevBtn.css('display', 'none');
	$nextBtn.css('display', 'none');

	navigationcontroller();

	loadTimelineProgress($total_page, countNext + 1);
	generaltemplate();
	/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					templateCaller();
				});
			}
	*/
}

		$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

		$refreshBtn.click(function(){
		templateCaller();
	});

		$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

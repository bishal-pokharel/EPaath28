var imgpath = $ref+"/images/playtime/";
var soundAsset = $ref + "/sounds/new/";
var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"ptime",
                textclass: "chapter centertext",
                textdata: data.string.playtime
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: "coverpageImg",
                    imgsrc: "",
                },
                {
                    imgdiv: "rhinodancingdiv",
                    imgclass: " relativecls rhinoimgdiv",
                    imgid: "rhinodance",
                    imgsrc: "",
                },
                {
                    imgdiv: "squirrelistening",
                    imgclass: " relativecls squirrelisteningimg",
                    imgid: "squirrel",
                    imgsrc: "",
                }]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.playtimetitle
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.apple,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.sagh
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "shop s1",
                    imgclass: "relativecls img1",
                    imgid: "apple1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "shop s2",
                    imgclass: "relativecls img2",
                    imgid: "appleImg",
                    imgsrc: "",
                },
             ]
        }],
        svgblock:[
            {
                svgblock: 'bag',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ]
    },
// slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.playtimetitle
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.amala,
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.amba,
                ans:"correct"
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "shop s1",
                    imgclass: "relativecls img1",
                    imgid: "guava1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "shop s2",
                    imgclass: "relativecls img2",
                    imgid: "guavaImg",
                    imgsrc: "",
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'bag',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.playtimetitle
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.jhanda,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.jhadi
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "shop s1",
                    imgclass: "relativecls img1",
                    imgid: "flag1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "shop s2",
                    imgclass: "relativecls img2",
                    imgid: "flagImg",
                    imgsrc: "",
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'bag',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.playtimetitle
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.slippers,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.chamal
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "shop s1",
                    imgclass: "relativecls img1",
                    imgid: "flipflop1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "shop s2",
                    imgclass: "relativecls img2",
                    imgid: "flipflopImg",
                    imgsrc: "",
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'bag',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"question",
                textclass: "content1 centertext",
                textdata: data.string.playtimetitle
            },
            {
                textdiv:"option opt1",
                textclass: "content centertext",
                textdata: data.string.soyabean
            },
            {
                textdiv:"option opt2",
                textclass: "content centertext",
                textdata: data.string.football,
                ans:"correct"
            },
        ],
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "shop s1",
                    imgclass: "relativecls img1",
                    imgid: "ball1Img",
                    imgsrc: "",
                },
                {
                    imgdiv: "shop s2",
                    imgclass: "relativecls img2",
                    imgid: "ballImg",
                    imgsrc: "",
                },
            ]
        }],
        svgblock:[
            {
                svgblock: 'bag',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ]
    },
//lastslide
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        svgblock:[
            {
                svgblock: 'bag1',
                svgclass:"",
                handclass:"relativecls centertext"
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    var preload;
    var timeoutvar = null;
    var current_sound="";
    var sound="";
    var tempfirstarray = new Array();
    var tempsecondarray = new Array();
    var palindromarray = new Array();
    var endpageex =  new EndPageofExercise();
    var message = data.string.lastmsg;
    endpageex.init(3);
    loadTimelineProgress($total_page, countNext + 1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
             {id: "coverpageImg", src:imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
             {id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
             {id: "bgImg", src: imgpath+"bg_playtime.png", type: createjs.AbstractLoader.IMAGE},
             {id: "bagImg", src: imgpath+"bag.svg", type: createjs.AbstractLoader.IMAGE},
             {id: "appleImg", src: imgpath+"apple01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "apple1Img", src: imgpath+"apple02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "guavaImg", src: imgpath+"guava01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "guava1Img", src: imgpath+"guava02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "flagImg", src: imgpath+"flag01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "flag1Img", src: imgpath+"flag02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "flipflopImg", src: imgpath+"flip_flop01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "flipflop1Img", src: imgpath+"flip_flop02.png", type: createjs.AbstractLoader.IMAGE},
             {id: "ballImg", src: imgpath+"ball01.png", type: createjs.AbstractLoader.IMAGE},
             {id: "ball1Img", src: imgpath+"ball02.png", type: createjs.AbstractLoader.IMAGE},
            //sounds
              {id: "ex1_2", src: soundAsset+"ex1_2.ogg"},
              {id: "ex1_7", src: soundAsset+"ex1_7.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    // $refreshBtn.click(function(){
    //     templateCaller();
    // });
    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
firstPagePlayTime(countNext);
        texthighlight($board);
        put_image(content, countNext);
        switch (countNext){
            case 0:
                navigationcontroller();
                break;
            case 6:
                endpageex.endpage(message);
                sound_player("ex1_7");
                loadsvg('#bag1',"bagImg");
                 break;
            default:
                loadsvg('#bag',"bagImg");
                countNext==1?sound_player("ex1_2"):'';
                break;
        }
    }


    function sound_player(sound_id,navigate,imgCls,imgSrc) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():"";
            if(imgCls && imgSrc){
                $("."+imgCls).attr("src",imgSrc);
            }
        });
    }



    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('imghintblock')) {
            var contentCount=content[count].imghintblock[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }
    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                // lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });




    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                 $(this).html(replaceinstring);
            });
        }
    }

    function loadsvg(svgid,imgid){
        var s = Snap(svgid);
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s.append(loadedFragment);
            switch(countNext){
                case 2:
                    $("#apple").css("opacity","1");
                    break;
                case 3:
                    $("#apple,#guava").css("opacity","1");
                    break;
                case 4:
                    $("#apple,#guava,#flag").css("opacity","1");
                    break;
                case 5:
                    $("#apple,#flip-flop,#guava,#flag").css("opacity","1");
                    break;
                case 6:
                    $("#apple,#flip-flop,#guava,#flag,#ball").css("opacity","1");
                    break;
            }
            $(".option").click(function(){
                if($(this).attr("data-answer")=="correct"){
                    $(this).addClass("correctans");
                    $(".option").addClass("avoid-clicks");
                    current_sound.stop();
                    play_correct_incorrect_sound(1);
                    $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                    navigationcontroller();
                    $(".s2").css("opacity","0");
                    switch(countNext){
                        case 1:
                            $("#apple").css("opacity","1");
                            break;
                        case 2:
                            $("#guava").css("opacity","1");
                            break;
                        case 3:
                            $("#flag").css("opacity","1");
                            break;
                        case 4:
                            $("#flip-flop").css("opacity","1");
                            break;
                        case 5:
                            $("#ball").css("opacity","1");
                            break;
                    }

                }
                else{
                    $(this).addClass("wrongans avoid-clicks");
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
                    $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                }
            })
        });

    }
});



var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/new/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "datafont centertext",
                textdata: data.string.chapter
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'covermainImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.info
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gauri",
                    imgclass: "relativecls img1",
                    imgid: 'gauriImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon1",
                    imgclass: "relativecls img2",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon2 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "amala fruitdiv",
                    imgclass: "relativecls img4",
                    imgid: 'amalaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls: "title centertext",
                            imgtxt: data.string.amala
                        }
                    ]
                },
                {
                    imgdiv: "amba fruitdiv hide1",
                    imgclass: "relativecls img5",
                    imgid: 'ambaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"title centertext",
                            imgtxt:data.string.amba
                        }
                    ]
                },
            ]
        }],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.info
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gauri",
                    imgclass: "relativecls img1",
                    imgid: 'gauriImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon1",
                    imgclass: "relativecls img2",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon2",
                    imgclass: "relativecls img3",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "amala fruitdiv",
                    imgclass: "relativecls img4",
                    imgid: 'amalaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls: "title centertext",
                            imgtxt: data.string.amala
                        }
                    ]
                },
                {
                    imgdiv: "amba fruitdiv",
                    imgclass: "relativecls img5",
                    imgid: 'ambaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"title centertext",
                            imgtxt:data.string.amba
                        }
                    ]
                },
                {
                    imgdiv: "amala fruitdiv1",
                    imgclass: "relativecls img6",
                    imgid: 'amala1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "amba fruitdiv1",
                    imgclass: "relativecls img7",
                    imgid: 'amba1Img',
                    imgsrc: "",
                },
            ]
        }],
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.info
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gauri",
                    imgclass: "relativecls img1",
                    imgid: 'gauriImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon1",
                    imgclass: "relativecls img2",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon2",
                    imgclass: "relativecls img3",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "amala fruitdiv",
                    imgclass: "relativecls img4",
                    imgid: 'amalaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls: "chapter centertext text1",
                            imgtxt: data.string.ma
                        },
                        {
                            imgcls: "content centertext text2",
                            imgtxt: data.string.p1text3
                        }
                    ]
                },
                {
                    imgdiv: "amba fruitdiv",
                    imgclass: "relativecls img5",
                    imgid: 'ambaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"chapter centertext text1",
                            imgtxt:data.string.adhama
                        },
                        {
                            imgcls: "content centertext text2",
                            imgtxt: data.string.p1text4
                        }
                    ]
                },
            ]
        }],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.info
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gauri",
                    imgclass: "relativecls img1",
                    imgid: 'gauriImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon1",
                    imgclass: "relativecls img2",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon2",
                    imgclass: "relativecls img3",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "amala fruitdiv",
                    imgclass: "relativecls img4",
                    imgid: 'amalaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls: "chapter centertext text1",
                            imgtxt: data.string.ma
                        },
                        {
                            imgcls: "content centertext text2",
                            imgtxt: data.string.p1text3
                        }
                    ]
                },
                {
                    imgdiv: "amba fruitdiv",
                    imgclass: "relativecls img5",
                    imgid: 'ambaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"chapter centertext text1",
                            imgtxt:data.string.adhama
                        },
                        {
                            imgcls: "content centertext text2",
                            imgtxt: data.string.p1text4
                        }
                    ]
                },
                {
                    imgdiv: "dialbox fadeInEffect",
                    imgclass: "relativecls img6",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content centertext",
                            imgtxt:data.string.p1text5
                        }
                    ]
                }
            ]
        }],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.info
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gauri",
                    imgclass: "relativecls img1",
                    imgid: 'gauriImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon1",
                    imgclass: "relativecls img2",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon2",
                    imgclass: "relativecls img3",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "amala fruitdiv",
                    imgclass: "relativecls img4",
                    imgid: 'amalaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls: "chapter centertext text1",
                            imgtxt: data.string.ma
                        },
                        {
                            imgcls: "content centertext text2",
                            imgtxt: data.string.p1text6
                        }
                    ]
                },
                {
                    imgdiv: "amba fruitdiv",
                    imgclass: "relativecls img5",
                    imgid: 'ambaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"chapter centertext text1",
                            imgtxt:data.string.adhama
                        },
                        {
                            imgcls: "content centertext text2",
                            imgtxt: data.string.p1text7
                        }
                    ]
                }
            ]
        }],
    },
//                    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.info
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gauri",
                    imgclass: "relativecls img1",
                    imgid: 'gauriImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon1",
                    imgclass: "relativecls img2",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio icon3",
                    imgclass: "relativecls img3",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "amala fruitdiv2",
                    imgclass: "relativecls img4",
                    imgid: 'amalaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls: "content centertext",
                            imgtxt: data.string.amala
                        }
                    ]
                },
                {
                    imgdiv: "amba1 fruitdiv2 shakingEffect",
                    imgclass: "relativecls img5",
                    imgid: 'ambaImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content centertext",
                            imgtxt:data.string.amba
                        }
                    ]
                },
                {
                    imgdiv: "dialbox1 fadeInEffect",
                    imgclass: "relativecls img6",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content centertext",
                            imgtxt:data.string.p1text8
                        }
                    ]
                }
            ]
        }],
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gauri",
                    imgclass: "relativecls img1",
                    imgid: 'gauriImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things h1",
                    imgclass: "relativecls img2",
                    imgid: 'appleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things h2",
                    imgclass: "relativecls img3",
                    imgid: 'slipersImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things h3",
                    imgclass: "relativecls img4",
                    imgid: 'footballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "things h4",
                    imgclass: "relativecls img5",
                    imgid: 'brinjalImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "dialbox2 fadeInEffect",
                    imgclass: "relativecls img6",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content centertext",
                            imgtxt:data.string.p1text9
                        }
                    ]
                }
            ]
        }],
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gauri",
                    imgclass: "relativecls img1",
                    imgid: 'gauriImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things h1",
                    imgclass: "relativecls img2",
                    imgid: 'appleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things h2",
                    imgclass: "relativecls img3",
                    imgid: 'slipersImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things h3",
                    imgclass: "relativecls img4",
                    imgid: 'footballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "things h4",
                    imgclass: "relativecls img5",
                    imgid: 'brinjalImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "dialbox2 fadeInEffect",
                    imgclass: "relativecls img6",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content centertext",
                            imgtxt:data.string.p1text10
                        }
                    ]
                }
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "gauriImg", src: imgpath + "Gauri.png", type: createjs.AbstractLoader.IMAGE},
            {id: "amalaImg", src: imgpath + "amla02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "amala1Img", src: imgpath + "amla01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ambaImg", src: imgpath + "amba02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "amba1Img", src: imgpath + "amba01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "audioicon", src: "images/speaker.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src: imgpath + "bubblee-02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "appleImg", src: imgpath + "apple.png", type: createjs.AbstractLoader.IMAGE},
            {id: "slipersImg", src: imgpath + "flip_flop.png", type: createjs.AbstractLoader.IMAGE},
            {id: "footballImg", src: imgpath + "footbal.png", type: createjs.AbstractLoader.IMAGE},
            {id: "brinjalImg", src: imgpath + "brinjal.png", type: createjs.AbstractLoader.IMAGE},
            {id: "covermainImg", src: imgpath + "cover_page.jpg", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "s1_p2_title", src: soundAsset + "s1_p2_title.ogg"},
            {id: "s1_p1", src: soundAsset + "s1_p1.ogg"},
            {id: "s1_p5", src: soundAsset + "s1_p5.ogg"},
            {id: "s1_p7", src: soundAsset + "s1_p7.ogg"},
            {id: "s1_p8", src: soundAsset + "s1_p8.ogg"},
            {id: "s1_p9", src: soundAsset + "s1_p9.ogg"},
            {id: "s1_p2-01", src: soundAsset + "s1_p2-01.ogg"},
            {id: "s1_p2-02", src: soundAsset + "s1_p2-02.ogg"},
            {id: "s1_p3-01", src: soundAsset + "s1_p3-01.ogg"},
            {id: "s1_p3-02", src: soundAsset + "s1_p3-02.ogg"},
            {id: "s1_p4-01", src: soundAsset + "s1_p4-01.ogg"},
            {id: "s1_p4-02", src: soundAsset + "s1_p4-02.ogg"},
            {id: "s1_p6-01", src: soundAsset + "s1_p6-01.ogg"},
            {id: "s1_p6-02", src: soundAsset + "s1_p6-02.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        put_image(content, countNext, preload);
        vocabcontroller.findwords(countNext);
        switch (countNext) {
            case 0:
                sound_player("s1_p1",1);
            break;
            case 1:
                console.log("Hello ")
              $(".hide1").hide(0);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p2_title");
                current_sound.play();
                current_sound.on('complete', function () {
                    current_sound = createjs.Sound.play("s1_p" + (countNext + 1) + "-01");
                    current_sound.play();
                    current_sound.on('complete', function () {
                        $(".hide1").fadeIn(1000);
                        createjs.Sound.stop();
                        setTimeout(function(){
                         current_sound = createjs.Sound.play("s1_p" + (countNext + 1) + "-02");
                          current_sound.play();
                        current_sound.on('complete', function () {
                            $(".audio").click(function () {
                                if ($(this).hasClass("icon1")) {
                                    sound_player("s1_p" + (countNext + 1) + "-01");
                                }
                                else {
                                    sound_player("s1_p" + (countNext + 1) + "-02");
                                }
                            });
                            nav_button_controls(300);
                        });
                        },2000);
                    });

                });
                break;
            case 2:
                $(".fruitdiv").delay(1000).fadeOut(1000);
                $(".fruitdiv1,.icon2").hide();
                setTimeout(function(){
                  $(".amala").fadeIn(1000);
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"-01");
                  current_sound.play();
                  current_sound.on('complete', function () {
                    $(".amba,.icon2").fadeIn(1000);
                        createjs.Sound.stop();
                      setTimeout(function(){
                        current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"-02");
                        current_sound.play();
                        current_sound.on('complete', function () {
                          $(".audio").click(function(){
                              if($(this).hasClass("icon1")){
                                  sound_player("s1_p"+(countNext+1)+"-01");
                              }
                              else{
                                  sound_player("s1_p"+(countNext+1)+"-02");
                              }
                          });
                          nav_button_controls(300);
                        });
                      },2000);
                  });
                },1000);
                break;
            case 3:
            $(".fruitdiv,.icon2").hide();
              $(".amala").fadeIn(1000);
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"-01");
              current_sound.play();
              current_sound.on('complete', function () {
                $(".amba,.icon2").fadeIn(1000);
                    createjs.Sound.stop();
                  setTimeout(function(){
                    current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"-02");
                    current_sound.play();
                    current_sound.on('complete', function () {
                      $(".audio").click(function(){
                          if($(this).hasClass("icon1")){
                              sound_player("s1_p"+(countNext+1)+"-01");
                          }
                          else{
                              sound_player("s1_p"+(countNext+1)+"-02");
                          }
                      });
                      nav_button_controls(300);
                    });
                  },2000);
              });
            break;
            case 4:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p5");
            current_sound.play();
            current_sound.on('complete', function () {
            $(".amba").fadeIn(1000);
                  $(".audio").click(function(){
                      if($(this).hasClass("icon1")){
                          sound_player("s1_p4-01");
                      }
                      else{
                          sound_player("s1_p4-02");
                      }
                  });
                  nav_button_controls(300);
            });
            break;
            case 5:
            $(".fruitdiv,.icon2").hide();
              $(".amala").fadeIn(1000);
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"-01");
              current_sound.play();
              current_sound.on('complete', function () {
                $(".amba,.icon2").fadeIn(1000);
                    createjs.Sound.stop();
                  setTimeout(function() {
                      current_sound = createjs.Sound.play("s1_p" + (countNext + 1) + "-02");
                      current_sound.play();
                      current_sound.on('complete', function () {
                          $(".audio").click(function () {
                              if ($(this).hasClass("icon1")) {
                                  sound_player("s1_p" + (countNext + 1) + "-01");
                              }
                              else {
                                  sound_player("s1_p" + (countNext + 1) + "-02");
                              }
                          });
                          nav_button_controls(300);
                      });
                  },2000);
              });
            break;
            case 6:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s1_p7");
            current_sound.play();
            current_sound.on('complete', function () {
            $(".amba").fadeIn(1000);
              $(".audio").click(function(){
                  if($(this).hasClass("icon1")){
                      sound_player("s1_p2-01");
                  }
                  else{
                      sound_player("s1_p2-02");
                  }
              });
                  nav_button_controls(300);
            });
                $(".audio").css("width","3%");
                break;
            case 7:
                sound_player("s1_p"+(countNext+1),1);
                $(".things").hide();
                $(".h1").delay(2000).fadeIn(500);
                $(".h2").delay(3000).fadeIn(500);
                $(".h3").delay(4000).fadeIn(500);
                $(".h4").delay(5000).fadeIn(500);
                break;
            case 8:
                sound_player("s1_p"+(countNext+1),1);
                $(".dialbox2 p").css("padding","17%");
                break;
            default:
                sound_player("s1_p"+(countNext+1),1);
                break;
        }
    }

    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/new/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.info1
            },
            {
                textdiv:"naming slideimg1",
                textclass: "sign centertext",
                textdata: data.string.chamcha
            },
            {
                textdiv:"naming slideimg2",
                textclass: "sign centertext",
                textdata: data.string.brinjal
            },
            {
                textdiv:"naming slideimg3",
                textclass: "sign centertext",
                textdata: data.string.apple
            },
            {
                textdiv:"naming slideimg4",
                textclass: "sign centertext",
                textdata: data.string.knife
            },
            {
                textdiv:"naming slideimg5",
                textclass: "sign centertext",
                textdata: data.string.jhanda
            },
            {
                textdiv:"naming slideimg6",
                textclass: "sign centertext",
                textdata: data.string.slippers
            },
            {
                textdiv:"naming slideimg7",
                textclass: "sign centertext",
                textdata: data.string.football
            },
            {
                textdiv:"naming slideimg8",
                textclass: "sign centertext",
                textdata: data.string.orange
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img0",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "things slideimg1",
                    imgclass: "relativecls img1",
                    imgid: 'spoonImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things slideimg2",
                    imgclass: "relativecls img2",
                    imgid: 'brinjalImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things slideimg3",
                    imgclass: "relativecls img3",
                    imgid: 'appleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "things slideimg4",
                    imgclass: "relativecls img4",
                    imgid: 'knifeImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "things slideimg5",
                    imgclass: "relativecls img5",
                    imgid: 'jhandaImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "things slideimg6",
                    imgclass: "relativecls img6",
                    imgid: 'slippersImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "things slideimg7",
                    imgclass: "relativecls img7",
                    imgid: 'footballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "things slideimg8",
                    imgclass: "relativecls img8",
                    imgid: 'orangeImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "uparrow arrow blinkEffect",
                    imgclass: "relativecls img9",
                    imgid: 'upImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "downarrow arrow blinkEffect",
                    imgclass: "relativecls img10",
                    imgid: 'downImg',
                    imgsrc: "",
                },

            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "audioicon", src: "images/speaker.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src: imgpath + "bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "spoonImg", src: imgpath + "spoon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "brinjalImg", src: imgpath + "brinjal.png", type: createjs.AbstractLoader.IMAGE},
            {id: "appleImg", src: imgpath + "apple.png", type: createjs.AbstractLoader.IMAGE},
            {id: "knifeImg", src: imgpath + "knife.png", type: createjs.AbstractLoader.IMAGE},
            {id: "jhandaImg", src: imgpath + "flag.png", type: createjs.AbstractLoader.IMAGE},
            {id: "slippersImg", src: imgpath + "flip_flop.png",  type: createjs.AbstractLoader.IMAGE},
            {id: "footballImg", src: imgpath + "footbal.png", type: createjs.AbstractLoader.IMAGE},
            {id: "orangeImg", src: imgpath + "orange_01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "upImg", src: imgpath + "up.png", type: createjs.AbstractLoader.IMAGE},
            {id: "downImg", src: imgpath + "down.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_4", src: soundAsset + "s2_p1_knife.ogg"},
            {id: "sound_6", src: soundAsset + "s2_p1_slipper.ogg"},
            {id: "sound_1", src: soundAsset + "s2_p1_spoon.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p1_flag.ogg"},
            {id: "sound_7", src: soundAsset + "s2_p1_ball.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p1_brinjal.ogg"},
            {id: "sound_8", src: soundAsset + "s2_p1_orange.ogg"},
            {id: "sound_3", src: soundAsset + "s2_p1_apple.ogg"},
            // {id: "sound_1", src: soundAsset + "s3_p1(च्याउ).ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
 
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            default:
                var count = 1;
                sound_player("sound_1",0); 
                $(".slideimg1").css("opacity","1");
                $(".things").find("img").css("top","-100%");
                $(".slideimg1").find("img").css("top","0%");
                $(".audio").click(function(){
                    sound_player("sound_1",0);                        
                }); 
                $(".arrow").click(function(){
                    $(".arrow").removeClass("blinkEffect");
                    if($(this).hasClass("uparrow")){
                        count = count==1?8:count-1;
                        if(count<=1){
                            count=1;
                        }
                    }
                    else if($(this).hasClass("downarrow")){
                        count =  count+1;
                        if(count>8) {
                            count = 1;
                        }
                    }
                    sound_player("sound_"+count,0);
                    $(".naming,.things img").removeClass("slider").css("opacity","0");
                    $(".slideimg"+count).css("opacity","1").removeClass("shownextbtn");
                    $(".slideimg"+count).find("img").addClass("slider shownextbtn").animate({"opacity":"1"},100);
                    $(".audio").click(function(){
                        sound_player("sound_"+count,0);                        
                    });
                    if($(".shownextbtn").length>=7 ){
                       navigationcontroller(countNext, $total_page);
                    }
                });
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

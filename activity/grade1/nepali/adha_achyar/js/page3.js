var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/new/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p3text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "audio",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "girl",
                    imgclass: "relativecls img1",
                    imgid: 'girlImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"title centertext",
                            imgtxt:data.string.mushroom
                        }
                    ]
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img3",
                    imgid: 'pyajImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img4",
                    imgid: 'mushroomImg',
                    imgsrc: "",
                    ans:"correct"
                },

            ]
        }],
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p3text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "audio",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "girl",
                    imgclass: "relativecls img1",
                    imgid: 'girlImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"title centertext",
                            imgtxt:data.string.pyaj
                        }
                    ]
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img3",
                    imgid: 'pyajImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img4",
                    imgid: 'mushroomImg',
                    imgsrc: ""
                },

            ]
        }],
    },
    // slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p3text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "audio",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "girl",
                    imgclass: "relativecls img1",
                    imgid: 'girlImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"title centertext",
                            imgtxt:data.string.orange
                        }
                    ]
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img3",
                    imgid: 'orangeImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img4",
                    imgid: 'mangoImg',
                    imgsrc: ""
                },

            ]
        }],
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p3text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "audio",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "girl",
                    imgclass: "relativecls img1",
                    imgid: 'girlImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"title centertext",
                            imgtxt:data.string.cabbage
                        }
                    ]
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img3",
                    imgid: 'cabbageImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img4",
                    imgid: 'pyajImg',
                    imgsrc: ""
                },

            ]
        }],
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p3text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "audio",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "girl",
                    imgclass: "relativecls img1",
                    imgid: 'girlImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialbox",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"title centertext",
                            imgtxt:data.string.sisno
                        }
                    ]
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img3",
                    imgid: 'sisnoImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img4",
                    imgid: 'saghImg',
                    imgsrc: ""
                },

            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "audioicon", src: "images/speaker.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlImg", src: imgpath + "talking-girl01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "bubblee-04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pyajImg", src: imgpath + "onion.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mushroomImg", src: imgpath + "musroon.png", type: createjs.AbstractLoader.IMAGE},
            {id: "orangeImg", src: imgpath + "orange_01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mangoImg", src: imgpath + "mango.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cabbageImg", src: imgpath + "cuabage.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sisnoImg", src: imgpath + "sishnu.png", type: createjs.AbstractLoader.IMAGE},
            {id: "saghImg", src: imgpath + "saag.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "s3_p1_title", src: soundAsset + "s3_p1.ogg"},
            {id: "s3_p1", src: soundAsset + "s3_p1_mushroom.ogg"},
            {id: "s3_p2", src: soundAsset + "s3_p2_onion.ogg"},
            {id: "s3_p3", src: soundAsset + "s3_p3_orange.ogg"},
            {id: "s3_p4", src: soundAsset + "s3_p4_cabbage.ogg"},
            {id: "s3_p5", src: soundAsset + "s3_p5_sisno.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
 
        texthighlight($board);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s3_p1_title");
                current_sound.play();
                current_sound.on('complete', function () {
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play("s3_p1");
                    current_sound.play();
                    current_sound.on('complete', function () {
                    });
                });
                shufflehint();
                checkans();
                $(".audio").on('click', function(){
                    sound_player("s3_p"+(countNext+1),0);                    
                });
            break;
            default:
                sound_player("s3_p"+(countNext+1),0);
                $(".audio").on('click', function(){
                    sound_player("s3_p"+(countNext+1),0);                    
                });
                shufflehint();
                checkans();

        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1","option opt2"];
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }
    function checkans(correctmsg){
        $(".option").click(function(){
            if($(this).attr("data-answer")=="correct"){
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                $(".text1").hide();
                $(".showcorrect").show().css("color","#98c02e");
                navigationcontroller(countNext, $total_page);
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
            }
        })
    }
});

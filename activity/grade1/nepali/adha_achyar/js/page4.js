var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/new/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p4text1
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.ladder
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.pumpkin,
                ans:"correct"
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bg",
                    imgclass: "relativecls img0",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a1",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a2",
                    imgclass: "relativecls img02",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "image",
                    imgclass: "relativecls img1",
                    imgid: 'pumpkinImg',
                    imgsrc: ""
                },

            ]
        }],
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p4text1
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.shoes
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.jhanda,
                ans:"correct"
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bg",
                    imgclass: "relativecls img0",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a1",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a2",
                    imgclass: "relativecls img02",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "image",
                    imgclass: "relativecls img1",
                    imgid: 'flagImg',
                    imgsrc: ""
                },

            ]
        }],
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p4text1
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.legs
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.kites,
                ans:"correct"
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bg",
                    imgclass: "relativecls img0",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a1",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a2",
                    imgclass: "relativecls img02",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "image",
                    imgclass: "relativecls img1",
                    imgid: 'kiteImg',
                    imgsrc: ""
                },

            ]
        }],
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p4text1
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.slippers
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.bell,
                ans:"correct"
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bg",
                    imgclass: "relativecls img0",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a1",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a2",
                    imgclass: "relativecls img02",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "image",
                    imgclass: "relativecls img1",
                    imgid: 'bellImg',
                    imgsrc: ""
                },

            ]
        }],
    },
    //    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p4text1
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.apple
            },
            {
                textdiv:"option opt1",
                textclass: "title centertext",
                textdata: data.string.gloves,
                ans:"correct"
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bg",
                    imgclass: "relativecls img0",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a1",
                    imgclass: "relativecls img01",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "audio a2",
                    imgclass: "relativecls img02",
                    imgid: 'audioicon',
                    imgsrc: ""
                },
                {
                    imgdiv: "image",
                    imgclass: "relativecls img1",
                    imgid: 'glovesImg',
                    imgsrc: ""
                },

            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "bgImg", src: imgpath + "bg01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "audioicon", src: "images/speaker.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pumpkinImg", src: imgpath + "pumkin.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flagImg", src: imgpath + "flag.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "kiteImg", src: imgpath + "kite.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bellImg", src: imgpath + "bell.png", type: createjs.AbstractLoader.IMAGE},
            {id: "glovesImg", src: imgpath + "gloves_new.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "s4_p1_title", src: soundAsset + "s4_p1.ogg"},
            {id: "s4_p1_1", src: soundAsset + "s4_p1_ladder.ogg"},
            {id: "s4_p1", src: soundAsset + "s4_p1_pumpkin.ogg"},
            {id: "s4_p2", src: soundAsset + "s4_p2_flag.ogg"},
            {id: "s4_p2_1", src: soundAsset + "s4_p2_shoe.ogg"},
            {id: "s4_p3", src: soundAsset + "s4_p3_kite.ogg"},
            {id: "s4_p3_1", src: soundAsset + "s4_p3_leg.ogg"},
            {id: "s4_p4", src: soundAsset + "s4_p4_bell.ogg"},
            {id: "s4_p4_1", src: soundAsset + "s4_p4_chappal.ogg"},
            {id: "s4_p5", src: soundAsset + "s4_p5_glove.ogg"},
            {id: "s4_p5_1", src: soundAsset + "s4_p5_syau.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
 
        texthighlight($board);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s4_p1_title");
                current_sound.play();
                shufflehint();
                checkans();
                    $(".a1").on('click',function(){
                        if($(".opt1").attr("data-answer")=="correct"){
                            sound_player("s4_p1");
                        }else{
                            sound_player("s4_p1_1");
                        }
                    });
                    $(".a2").on('click',function(){
                        if($(".opt2").attr("data-answer")=="correct"){
                            sound_player("s4_p1");
                        }else{
                            sound_player("s4_p1_1");
                        }                    
                    });

            break;
            default:
                    $(".a1").on('click',function(){
                        if($(".opt1").attr("data-answer")=="correct"){
                            sound_player("s4_p"+(countNext+1));
                        }else{
                            sound_player("s4_p"+(countNext+1)+"_1");
                        }
                    });
                    $(".a2").on('click',function(){
                        if($(".opt2").attr("data-answer")=="correct"){
                            sound_player("s4_p"+(countNext+1));
                        }else{
                            sound_player("s4_p"+(countNext+1)+"_1");
                        }                    
                    });
                shufflehint();
                checkans();
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1","option opt2"];
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }
    function checkans(correctmsg){
        $(".option").click(function(){
            if($(this).attr("data-answer")=="correct"){
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                navigationcontroller(countNext, $total_page,countNext == 4?true:false);
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
            }
        })
    }
});

var imgpath = $ref + "/playtime/images/";
var imgpath2 = $ref + "/images/playtime/forhover/";
var soundAsset = $ref+"/sounds/orgSnd1/";

var content=[
	// slide 1
	{
			contentnocenteradjust: true,
			uppertextblock: [
					{
							textclass: "playtime",
							textdata: data.string.playtime
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							},
							{
									imgclass: "bg_full",
									imgid: "mainbg",
									imgsrc: "",
							}]
			}]
	},
	// slide 2
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "insTrn",
			textdata: data.string.instrn
		}],
		imageblock:[
		{
			imagestoshow:[
				{
					imgclass: "qnImage qim1",
					imgid: "kachuwa_qn",
					imgsrc: "",
			},{
				imgclass: "qnImage qim2 hideIt",
				imgid: "kachuwa_ans",
				imgsrc: "",
		}]
		},
		{
			imageblockclass:"opnBlk",
			options:true,
			imagestoshow:[
				{
					imgclass: "image  option correct",
					imgid: "kachuwa_opn",
					imgsrc: "",
			},
			{
				imgclass: "image option inc",
				imgid: "ghar_opn",
				imgsrc: "",
		}]
		}]
	},
	// slide 3
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "insTrn",
			textdata: data.string.instrn
		}],
		imageblock:[
		{
			imagestoshow:[
				{
					imgclass: "qnImage qim1",
					imgid: "kharayo_qn",
					imgsrc: "",
			},{
				imgclass: "qnImage qim2 hideIt",
				imgid: "kharayo_ans",
				imgsrc: "",
		}]
		},
		{
			imageblockclass:"opnBlk",
			options:true,
			imagestoshow:[
				{
					imgclass: "image  option correct",
					imgid: "kharayo_opn",
					imgsrc: "",
			},
			{
				imgclass: "image option inc",
				imgid: "ghar_opn",
				imgsrc: "",
		}]
		}]
	},
	// slide 4
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "insTrn",
			textdata: data.string.instrn
		}],
		imageblock:[
		{
			imagestoshow:[
				{
					imgclass: "qnImage qim1",
					imgid: "gamala_qn",
					imgsrc: "",
			},{
				imgclass: "qnImage qim2 hideIt",
				imgid: "gamala_ans",
				imgsrc: "",
		}]
		},
		{
			imageblockclass:"opnBlk",
			options:true,
			imagestoshow:[
				{
					imgclass: "image  option correct",
					imgid: "gamala_opn",
					imgsrc: "",
			},
			{
				imgclass: "image option inc",
				imgid: "ghar_opn",
				imgsrc: "",
		}]
		}]
	},
	// slide 5
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "insTrn",
			textdata: data.string.instrn
		}],
		imageblock:[
		{
			imagestoshow:[
				{
					imgclass: "qnImage qim1",
					imgid: "ghar_qn",
					imgsrc: "",
			},{
				imgclass: "qnImage qim2 hideIt",
				imgid: "ghar_ans",
				imgsrc: "",
		}]
		},
		{
			imageblockclass:"opnBlk",
			options:true,
			imagestoshow:[
				{
					imgclass: "image  option correct",
					imgid: "ghar_opn",
					imgsrc: "",
			},
			{
				imgclass: "image option inc",
				imgid: "gamala_opn",
				imgsrc: "",
		}]
		}]
	},
	// slide 6
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass: "insTrn",
			textdata: data.string.instrn_nga
		}],
		imageblock:[
		{
			imagestoshow:[
				{
					imgclass: "qnImage qim1",
					imgid: "nang_qn",
					imgsrc: "",
			},{
				imgclass: "qnImage qim2 hideIt",
				imgid: "nang_ans",
				imgsrc: "",
		}]
		},
		{
			imageblockclass:"opnBlk",
			options:true,
			imagestoshow:[
				{
					imgclass: "image  option correct",
					imgid: "nang_opn",
					imgsrc: "",
			},
			{
				imgclass: "image option inc",
				imgid: "gamala_opn",
				imgsrc: "",
		}]
		}]
	},
	// slide 7
	{
		sliderload:true,
		contentblockadditionalclass:'green',
		slider:[{
			imagestoshow:[{
				imgclass : "slideimg si1",
				imgsrc : '',
				imgid : 'na_full',
			},{
				imgclass : "slideimg si2",
				imgsrc : '',
				imgid : 'ka_full',
			},{
				imgclass : "slideimg si3",
				imgsrc : '',
				imgid : 'kha_full',
			},{
				imgclass : "slideimg si4",
				imgsrc : '',
				imgid : 'ga_full',
			},{
				imgclass : "slideimg si5",
				imgsrc : '',
				imgid : 'gha_full',
			}]
		}]
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var selPgeArr=[];

	var preload;
	var timeoutvar = null;
	var current_sound;

  var endpageex =  new EndPageofExercise();
  endpageex.init(7);
  var message =  data.string.lasttext;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src:imgpath +"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mainbg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},

			{id: "kachuwa_qn", src: imgpath+"tapai01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kachuwa_ans", src: imgpath+"tapai02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kachuwa_opn", src: imgpath+"ta.png", type: createjs.AbstractLoader.IMAGE},

			{id: "kharayo_qn", src: imgpath+"lamp01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kharayo_ans", src: imgpath+"lamp02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kharayo_opn", src: imgpath+"tha.png", type: createjs.AbstractLoader.IMAGE},

			{id: "gamala_qn", src: imgpath+"dammaru01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gamala_ans", src: imgpath+"dammaru02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gamala_opn", src: imgpath+"da.png", type: createjs.AbstractLoader.IMAGE},

			{id: "ghar_qn", src: imgpath+"dhak01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ghar_ans", src: imgpath+"dhak02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ghar_opn", src: imgpath+"dha.png", type: createjs.AbstractLoader.IMAGE},

			{id: "nang_qn", src: imgpath+"bana01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nang_ans", src: imgpath+"bana02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nang_opn", src: imgpath+"na.png", type: createjs.AbstractLoader.IMAGE},

			{id: "ga_full", src: imgpath+"13.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gha_full", src: imgpath+"14.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kha_full", src: imgpath+"12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "na_full", src: imgpath+"15.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ka_full", src: imgpath+"11.png", type: createjs.AbstractLoader.IMAGE},

			{id: "next-button", src: 'images/next.png', type: createjs.AbstractLoader.IMAGE},
			{id: "prev-button", src: 'images/previous.png', type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "rewards_page", src: soundAsset+"rewards_page.ogg"},
			{id: "instruction_aud", src: soundAsset+"ex1s2.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
 // 	var pagenumbers  = new pagenameholder();
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		content[countNext].sliderload?put_image3(content, countNext):"";
		var pagenumbers;


 	/*for randomizing the options*/
		var parent = $(".opnBlk");
		var divs = parent.children();
		while (divs.length) {
 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		$(".hideIt").hide(0);
		$(".option").on("click", function(){
			if($(this).hasClass("correct")){
				$(this).siblings(".corctopt").show(0);
				$(".hideIt").show(0);
				play_correct_incorrect_sound(1);
				$(".option").css("pointer-events","none");
				nav_button_controls(100);
			}
			else{
				$(this).siblings(".wrngopt").show(0);
				$(this).css("pointer-events","none");
				play_correct_incorrect_sound(0);
			}
		});

	$('.playagain').click(function(){
		$('#activity-page-exercise-tab').find('button').trigger('click');
	});
	$('.learnagain').click(function(){
		$("#activity-page-lesson-tab").find('button').trigger('click');

	});
	$('.mainmenu').click(function(){
		$("#activity-page-menu-img").trigger("click");

	});


		switch (countNext) {
			case 0:
                nav_button_controls(0);
                break;
			case 1:
			sound_player('instruction_aud');
			break;
			case 6:
				sound_player("rewards_page");
				$(".next-button").on("click", function(){
				$(".next-button, .prev-button ").css('pointer-events', 'none');
					var selectImg = $(".slider:eq(0)");
					$(".innerwrapper").append(selectImg.clone());
					selectImg.animate({
						width:"0%",
						padding:"0%"
					},1000, function(){
						selectImg.remove();
						$(".next-button, .prev-button").css('pointer-events', 'auto');
					});
				});
				$(".prev-button").on("click", function(){
					$(".next-button, .prev-button").css('pointer-events', 'none');
					var selectImg = $(".slider").last();
					$(".innerwrapper").prepend(selectImg.clone());
					selectImg.remove();
					var selectFirstImg = $(".slider:eq(0)");
					selectFirstImg.animate({
						width:"0%",
					},0,function(){
						selectFirstImg.animate({
							width:"20%"
						},1000);
						setTimeout(function(){
							$(".next-button, .prev-button").css('pointer-events', 'auto');
						},900);
					});
				});
        endpageex.endpage(message);
			break;
			default:
				// nav_button_controls(100);

		}

	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
			if(content[count].hasOwnProperty('imageblock')){
				for(var j = 0; j < content[count].imageblock.length; j++){
					var imageblock = content[count].imageblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
		}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

		function put_image3(content, count){
			if(content[count].hasOwnProperty('slider')){
				var imageblock = content[count].slider[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/playtime/images/";
var soundAsset = $ref+"/sounds/np/";

var content=[
	// slide0
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg1",
			uppertextblock: [
					{
							textclass: "playtime",
							textdata: data.string.game
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							}]
			}]
	},	{
	//slide1
			contentnocenteradjust: true,
			contentblockadditionalclass: "pink_bg",
			uppertextblockadditionalclass:"covertext",
			uppertextblock:[
			{
				textclass: "text",
				textdata: data.string.diytext1
			}
	],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "bg",
					imgid : 'mainimg',
					imgsrc: ""
				}
			]
		}]
	},{
//slide2

		contentnocenteradjust: true,
		contentblockadditionalclass: "pink_bg",
		uppertextblockadditionalclass:"covertext2",
		uppertextblock:[
		{
			textclass: "box",
			textdata: data.string.diytext2
		},{
			textclass:"textbox",
			textdata: data.string.chautara
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "bg",
				imgid : 'mainimg',
				imgsrc: ""
			},{
				imgclass: "image img img1",
				imgid : "tree1",
				imgsrc: ""
			},{
				imgclass: "image img img2",
				imgid : "tree2",
				imgsrc: ""
			},{
				imgclass: "image img3",
				imgid : "tree3",
				imgsrc: ""
			},{
				imgclass: "image img4",
				imgid : "tree4",
				imgsrc: ""
			},{
					imgclass: "speaker",
					imgid : 'speaker',
					imgsrc: ""
				}
		]
	}]
},{
//slide3

	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass:"covertext2",
	uppertextblock:[
	{
		textclass: "box",
		textdata: data.string.diytext2
	},{
		textclass:"textbox",
		textdata: data.string.house
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "bg",
			imgid : 'mainimg',
			imgsrc: ""
		},{
			imgclass: "house house1",
			imgid : "house1",
			imgsrc: ""
		},{
			imgclass: "house house2",
			imgid : "house2",
			imgsrc: ""
		},{
			imgclass: "house house3",
			imgid : "house3",
			imgsrc: ""
		},{
			imgclass: "house house4",
			imgid : "house4",
			imgsrc: ""
		},{
			imgclass: "image treeNxtPage",
			imgid : "tree3",
			imgsrc: ""
		},{
				imgclass: "speaker",
				imgid : 'speaker',
				imgsrc: ""
			}
	]
}]
},{
//slide4
	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass:"covertext2",
	uppertextblock:[
	{
		textclass: "box",
		textdata: data.string.diytext2
	},{
		textclass:"textbox",
		textdata: data.string.well
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "bg",
			imgid : 'mainimg',
			imgsrc: ""
		},{
			imgclass: " dhara dhara1",
			imgid : "dhara4",
			imgsrc: ""
		},{
			imgclass: "dhara dhara2",
			imgid : "dhara3",
			imgsrc: ""
		},{
			imgclass: "dhara dhara3",
			imgid : "dhara2",
			imgsrc: ""
		},{
			imgclass: "dhara dhara4",
			imgid : "dhara1",
			imgsrc: ""
		},{
			imgclass: "image treeNxtPage",
			imgid : "tree3",
			imgsrc: ""
		},{
			imgclass: "house houseNxtPage",
			imgid : "house4",
			imgsrc: ""
		},{
				imgclass: "speaker",
				imgid : 'speaker',
				imgsrc: ""
			}
	]
}]
},{
//slide5
	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass:"covertext2",
	uppertextblock:[
	{
		textclass: "box",
		textdata: data.string.diytext2
	},{
		textclass:"textbox",
		textdata: data.string.animal
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "bg",
			imgid : 'mainimg',
			imgsrc: ""
		},{
			imgclass: "animal pig",
			imgid : "pig",
			imgsrc: ""
		},{
			imgclass: "animal hen",
			imgid : "hen",
			imgsrc: ""
		},{
			imgclass: "animal sheep",
			imgid : "sheep",
			imgsrc: ""
		},{
			imgclass: "animal calf",
			imgid : "calf",
			imgsrc: ""
		},{
			imgclass: "image treeNxtPage",
			imgid : "tree3",
			imgsrc: ""
		},{
			imgclass: "house houseNxtPage",
			imgid : "house4",
			imgsrc: ""
		},{
			imgclass: "dhara dharaNxtPage",
			imgid : "dhara1",
			imgsrc: ""
		},{
				imgclass: "speaker",
				imgid : 'speaker',
				imgsrc: ""
			}
	]
}]
},{
//slide6
	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass:"covertext2",
	uppertextblock:[
	{
		textclass: "box",
		textdata: data.string.diytext2
	},{
		textclass:"textbox",
		textdata: data.string.flower
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "bg",
			imgid : 'mainimg',
			imgsrc: ""
		},{
			imgclass: "flower flower1",
			imgid : "flower3",
			imgsrc: ""
		},{
			imgclass: "flower flower2",
			imgid : "flower2",
			imgsrc: ""
		},{
			imgclass: "flower flower3",
			imgid : "flower4",
			imgsrc: ""
		},{
			imgclass: "flower flower4",
			imgid : "flower1",
			imgsrc: ""
		},{
			imgclass: "image treeNxtPage",
			imgid : "tree3",
			imgsrc: ""
		},{
			imgclass: "house houseNxtPage",
			imgid : "house4",
			imgsrc: ""
		},{
			imgclass: "dhara dharaNxtPage",
			imgid : "dhara1",
			imgsrc: ""
		},{
			imgclass: "animal animalNxtPage",
			imgid : "calf",
			imgsrc: ""
		},{
				imgclass: "speaker",
				imgid : 'speaker',
				imgsrc: ""
			}
	]
}]
},{
//slide7
	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass:"covertext1",
	uppertextblock:[
	{
		textclass: "box1",
		textdata: data.string.diytext3
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "bg",
			imgid : 'mainimg',
			imgsrc: ""
		},{
			imgclass: "image treeNxtPage",
			imgid : "tree3",
			imgsrc: ""
		},{
			imgclass: "house houseNxtPage",
			imgid : "house4",
			imgsrc: ""
		},{
			imgclass: "dhara dharaNxtPage",
			imgid : "dhara1",
			imgsrc: ""
		},{
			imgclass: "animal animalNxtPage",
			imgid : "calf",
			imgsrc: ""
		},{
			imgclass: "flower flowerNxtPage",
			imgid : "flower1",
			imgsrc: ""
		}
	]
}]
},{
	//slide8
	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass:"covertext1",

imageblock:[{
	imagestoshow:[
		{
			imgclass: "bg",
			imgid : 'mainimg',
			imgsrc: ""
		},{
			imgclass: "image treeNxtPage",
			imgid : "tree3",
			imgsrc: ""
		},{
			imgclass: "house houseNxtPage",
			imgid : "house4",
			imgsrc: ""
		},{
			imgclass: "dhara dharaNxtPage",
			imgid : "dhara1",
			imgsrc: ""
		},{
			imgclass: "animal animalNxtPage",
			imgid : "calf",
			imgsrc: ""
		},{
			imgclass: "flower flowerNxtPage",
			imgid : "flower1",
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var endpageex =  new EndPageofExercise();
	var message =  "Yay!!! We just learned how to make words.";
	endpageex.init(5);

	var treeholder;
	var householder;
	var dharaholder;
	var animalholder;
	var flowerholder;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "mainimg", src: imgpath+ "bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree1", src: imgpath+"tree03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree2", src: imgpath+"tree02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree3", src: imgpath+"tree04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree4", src: imgpath+"tree01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house1", src: imgpath+"house01_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house2", src: imgpath+"house02_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house3", src: imgpath+"house03_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house4", src: imgpath+"house04_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dhara1", src: imgpath+"dhara01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dhara2", src: imgpath+"dhara02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dhara3", src: imgpath+"dhara03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dhara4", src: imgpath+"dhara04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sheep", src: imgpath+"sheep.png", type: createjs.AbstractLoader.IMAGE},
			{id: "calf", src: imgpath+"calf.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flower1", src: imgpath+"flower01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flower2", src: imgpath+"flower02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flower3", src: imgpath+"flower03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flower4", src: imgpath+"flower04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "playbg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},



			{id: "maindoll", src: imgpath+"/diy/doll.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "speaker", src: imgpath+"sound-icon-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox", src: imgpath+"/playtime/text_box.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"ex_s1_p2.ogg"},
			{id: "sound_1", src: soundAsset+"ex_s1_p3.ogg"},
			{id: "sound_2", src: soundAsset+"ex_s1_p3_chautara.ogg"},
			{id: "sound_3", src: soundAsset+"ex_s1_p4_ghar.ogg"},
			{id: "sound_4", src: soundAsset+"ex_s1_p5_dhara.ogg"},
			{id: "sound_5", src: soundAsset+"ex_s1_p6_janawar.ogg"},
			{id: "sound_6", src: soundAsset+"ex_s1_p7_phul.ogg"},
			{id: "hurray", src: soundAsset+"hurray.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
	 typeof islastpageflag === "undefined" ?
	 islastpageflag = false :
	 typeof islastpageflag != 'boolean'?
	 alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 null;

	 if(countNext == 0 && $total_page!=1){
	 	$nextBtn.show(0);
	 	$prevBtn.css('display', 'none');
	 }
	 else if($total_page == 1){
	 	$prevBtn.css('display', 'none');
	 	$nextBtn.css('display', 'none');

	 	// if lastpageflag is true
	 	islastpageflag ?
	 	ole.footerNotificationHandler.lessonEndSetNotification() :
	 	ole.footerNotificationHandler.lessonEndSetNotification() ;
	 }
	 else if(countNext > 0 && countNext < $total_page-1){
	 	$nextBtn.show(0);
	 	$prevBtn.show(0);
	 }
	 else if(countNext == $total_page-1){
	 	$nextBtn.css('display', 'none');
	 	$prevBtn.show(0);

	 	// if lastpageflag is true
	 	islastpageflag ?
	 	ole.footerNotificationHandler.lessonEndSetNotification() :
	 	ole.footerNotificationHandler.pageEndSetNotification() ;
	 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	 var audiocontroller = new AudioController($total_page, true,  160, 800, vocabcontroller);
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		firstPagePlayTime(countNext);
		var pbox1, pbox2,pbox2, ptext1, ptext2, ptext3;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var button_counter = 0;
		$nextBtn.hide(0);

		// $('.prev_button').attr("src",preload.getResult('btn01').src);
		// $('.next_button').attr("src",preload.getResult('btn02').src);
		ole.footerNotificationHandler.hideNotification();
		$description = $(".description");
		vocabcontroller.findwords(countNext);
		$(".houseNxtPage, .dharaNxtPage, .animalNxtPage, .flowerNxtPage").css("pointer-events","none");
		$(".houseNxtPage, .dharaNxtPage, .animalNxtPage").hide();
		switch(countNext){
			case 1:
			sound_player("sound_0");
			break;
			case 2:
						sound_player("sound_1");
			$(".image").click(function(){
				createjs.Sound.stop();
				$(".image , .box, .covertext2, .speaker").hide(0);
				$(this).show(0).addClass("tree_anim").css("pointer-events","none");
				treeholder= $(this).attr("src");
				$nextBtn.show(0);
			});
			$(".speaker, .textbox").click(function(){
				sound_player("sound_2");
			});
			break;
			case 3:
			sound_player("sound_1");
			$(".treeNxtPage").attr("src",treeholder);
			$(".house").click(function(){
				createjs.Sound.stop();
				$(".house , .box, .covertext2, .speaker").hide(0);
				$(this).show(0).addClass("house_anim").css("pointer-events","none");

				householder= $(this).attr("src");
				$nextBtn.show(0);
			});
		$(".speaker, .textbox").click(function(){
			sound_player("sound_3");
		});
			break;
 			case 4:
			sound_player("sound_1");
			$(".treeNxtPage").attr("src", treeholder);
			$(".houseNxtPage").attr("src", householder);

			$(".dhara").click(function(){
				createjs.Sound.stop();
			$(".dhara , .box, .covertext2, .speaker").hide(0);
				$(".house").show();
			$(this).show(0).addClass("dhara_anim").css("pointer-events","none");
			dharaholder=$(this).attr("src");
			$nextBtn.show(0);
		});
		$(".speaker, .textbox").click(function(){
			sound_player("sound_4");
		});

			break;
			case 5:
			sound_player("sound_1");
			$(".treeNxtPage").attr("src", treeholder);
			$(".houseNxtPage").attr("src", householder);
			$(".dharaNxtPage").attr("src", dharaholder);

			$(".animal").click(function(){
			createjs.Sound.stop();
			$(".animal , .box, .covertext2, .speaker").hide(0);
				$(".house, .dhara").show();
			$(this).show(0).addClass("animal_anim").css("pointer-events","none");
			animalholder=$(this).attr("src");
			$nextBtn.show(0);
		});
		$(".speaker, .textbox").click(function(){
			sound_player("sound_5");
		});
			break;
			case 6:
			sound_player("sound_1");
			$(".treeNxtPage").attr("src", treeholder);
			$(".houseNxtPage").attr("src", householder);
			$(".dharaNxtPage").attr("src", dharaholder);
			$(".animalNxtPage").attr("src", animalholder);

			$(".flower").click(function(){
			createjs.Sound.stop();
			$(".flower, .box, .covertext2, .speaker").hide(0);
			$(".house, .animal, .dhara").show();
			$(this).show(0).addClass("flower_anim").css("pointer-events","none");
			$nextBtn.show(0);
			flowerholder=$(this).attr("src");
			});
			$(".speaker, .textbox").click(function(){
				sound_player("sound_6");
			});
			break;
			case 7:
			sound_player_nav("hurray");
			$(".treeNxtPage").attr("src", treeholder);
			$(".houseNxtPage").attr("src", householder);
			$(".dharaNxtPage").attr("src", dharaholder);
			$(".animalNxtPage").attr("src", animalholder);
			$(".flowerNxtPage").attr("src", flowerholder);
			$(".house, .animal, .dhara").show();
			break;
			case 8:
			$prevBtn.hide(0);
			$(".treeNxtPage").attr("src", treeholder);
			$(".houseNxtPage").attr("src", householder);
			$(".dharaNxtPage").attr("src", dharaholder);
			$(".animalNxtPage").attr("src", animalholder);
			$(".flowerNxtPage").attr("src", flowerholder);
			$(".house, .animal, .dhara").show();
			 endpageex.endpage("");
			break;
			default:
				nav_button_controls(0);
			break;
		}
}
function sound_player_nav(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	current_sound.on("complete", function(){
		nav_button_controls(500);
	});
}
function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	current_sound.on("complete", function(){
		if(countNext==1){	nav_button_controls(0);}
		else{
			$nextBtn.hide(0);
		}
	});
}
function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.lessonEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
function templateCaller() {
	$prevBtn.css('display', 'none');
	$nextBtn.css('display', 'none');

	navigationcontroller();

	loadTimelineProgress($total_page, countNext + 1);
	generaltemplate();
	/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					templateCaller();
				});
			}
	*/
}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

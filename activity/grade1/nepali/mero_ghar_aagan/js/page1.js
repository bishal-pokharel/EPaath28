var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/np/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "pink_bg",
		uppertextblockadditionalclass:"coverpagetext",
		uppertextblock:[
		{
			textclass: "coverpage",
			textdata: data.lesson.chapter
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "ghar",
				imgid : 'cover',
				imgsrc: ""
			}
		]
	}]
},
{
	//slide1
	// contentnocenteradjust:true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass:"covertext",
	uppertextblock:[
		{
			textclass: " text-1",
			textdata: data.string.p1part1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "ghar",
				imgid: 'cover',
				imgsrc: ""
			},
			{
				imgclass: "girl",
				imgid: 'girl',
				imgsrc: ""
			}
		]
	}]
},
{
	//slide2
	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass: "covertext",
	uppertextblock:[
		{
			textclass: "text-1",
			textdata: data.string.p1part2
		}
	],
	imageblock:[{
		imagestoshow:[
			{
			imgclass: "ghar",
			imgid: 'cover',
			imgsrc: ""
		},	{
			imgclass: "girl",
			imgid: 'girl1',
			imgsrc: ""
		}
		]
	}]
},{
	//slide3
	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass: "covertext",
	uppertextblock:[
		{
			textclass: "text-1",
			textdata: data.string.p1part3
		}
	],
	imageblock:[{
		imagestoshow:[
			{
			imgclass: "ghar",
			imgid: 'cover',
			imgsrc: ""
		},	{
			imgclass: "girl2",
			imgid: 'girl2',
			imgsrc: ""
		}
		]
	}]
},{
	//slide4
	contentnocenteradjust: true,
	contentblockadditionalclass: "pink_bg",
	uppertextblockadditionalclass: "covertext1",
	uppertextblock:[
		{
			textclass: " text text1 cowshed",
			textdata: data.string.shed
		},{
			textclass: " text text2 flower",
			textdata: data.string.flower
		},{
			textclass: "text text3 dhara",
			textdata: data.string.tap
		},{
			textclass: "text text4 dog",
			textdata: data.string.dog
		},{
			textclass: "text text5 roof",
			textdata: data.string.chano
		},{
				textclass: "text-2 text6 doko",
				textdata: data.string.doko
		},{
				textclass: "text-2 text7 toilet",
				textdata: data.string.toilet
		},{
				textclass: "text-2 text8 cow",
				textdata: data.string.cow
		},{
				textclass: "text-2 text9 dhiki",
				textdata: data.string.dhiki
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "girl3",
				imgid: 'girl3',
				imgsrc: ""
			}
		]
	}],
	svgblock:[{
		svgblock:"gothsvg"
	}]
}


];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';
	var setTime;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"main_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"girl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl1", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl2", src: imgpath+"girl01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl3", src: imgpath+"girl05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mero_ghar", src: imgpath+"mero_ghar.svg", type: createjs.AbstractLoader.IMAGE},
			//sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_roof", src: soundAsset+"s2_p7_chano.ogg"},
			{id: "sound_cowshed", src: soundAsset+"s2_p5_goth.ogg"},
			{id: "sound_flower", src: soundAsset+"s2_p10_phul.ogg"},
			{id: "sound_dhara", src: soundAsset+"s2_p8_dharo.ogg"},
			{id: "sound_dog", src: soundAsset+"s2_p9_kukur.ogg"},
			{id: "sound_doko", src: soundAsset+"s1_p5_doko.ogg"},
			{id: "sound_toilet", src: soundAsset+"s1_p5_sauchalaya.ogg"},
			{id: "sound_cow", src: soundAsset+"s1_p5_gai.ogg"},
			{id: "sound_dhiki", src: soundAsset+"s1_p5_dhiki.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
	 // TODO: interesting use of conditional statement
	 typeof islastpageflag === "undefined" ?
	 islastpageflag = false :
	 typeof islastpageflag != 'boolean'?
	 alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 null;

	 if(countNext == 0 && $total_page!=1){
	 	$nextBtn.show(0);
	 	$prevBtn.css('display', 'none');
	 } else if($total_page == 1){
	 	$prevBtn.css('display', 'none');
	 	$nextBtn.css('display', 'none');

	 	// if lastpageflag is true
	 	islastpageflag ?
	 	ole.footerNotificationHandler.lessonEndSetNotification() :
	 	ole.footerNotificationHandler.lessonEndSetNotification() ;
	 } else if(countNext > 0 && countNext < $total_page-1){
	 	$nextBtn.show(0);
	 	$prevBtn.show(0);
	 } else if(countNext == $total_page-1){
	 	$nextBtn.css('display', 'none');
	 	$prevBtn.show(0);

	 	// if lastpageflag is true
	 	islastpageflag ?
	 	ole.footerNotificationHandler.lessonEndSetNotification() :
	 	ole.footerNotificationHandler.pageEndSetNotification() ;
	 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// var pbox1, pbox2,pbox2, ptext1, ptext2, ptext3;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var svgtoload;
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		// TODO: add mechanism for audio for other slides as well
		switch(countNext){
			case 0:
			sound_player("sound_0");
			break;
			case 1:
			sound_player("sound_1");
			break;
			case 2:
			sound_player("sound_2");
			break;
			case 3:
			sound_player("sound_3");
			break;
			case 4:
				var s= Snap('#gothsvg');
				var svg = Snap.load(preload.getResult("mero_ghar").src, function ( loadedFragment ) {
						s.append(loadedFragment);
					$(".option")
							.click(function(){
								clearTimeout(setTime);
								$("#Layer_1").find("g").not($(this)).children().attr("fill","#0001");
								var currentoptid = $(this).attr("id");
								$("."+currentoptid).addClass("highlights");
								sound_player("sound_"+currentoptid);
								setTime = setTimeout(function(){
									templateCaller();
								},2000);
						});
						nav_button_controls(0);
					});
					break;
			default:
			navigationcontroller(true);
			break;
		}


		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}
		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
				});
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		// /*
		// 	for (var i = 0; i < content.length; i++) {
		// 		slides(i);
		// 		$($('.totalsequence')[i]).html(i);
		// 		$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		// 	"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		// 	}
		// 	function slides(i){
		// 			$($('.totalsequence')[i]).click(function(){
		// 				countNext = i;
		// 				templateCaller();
		// 			});
		// 		}
		// */
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

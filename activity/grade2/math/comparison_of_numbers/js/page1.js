var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/" + $lang + "/";

var content = [
  //slide0

  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    uppertextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: "lesson-title vertical-horizontal-center"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "firstpageimagecss",
            imgsrc: imgpath + "comparison_of_numbers.gif"
          }
        ]
      }
    ]
  },

  //slide1
  {
    contentblockadditionalclass: "main_bg",
    contentblocknocenteradjust: true,

    uppertextblockadditionalclass: "introduction",
    uppertextblock: [
      {
        textdata: data.string.p1text1,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red"
      }
    ],

    numberlineadditionalclass: "ones_line",
    numberline: [
      {
        textdata: data.string.pnum9,
        textclass: "num_9",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/9.png"
      },
      {
        textdata: data.string.pnum8,
        textclass: "num_8",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/8.png"
      },
      {
        textdata: data.string.pnum7,
        textclass: "num_7",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/7.png"
      },
      {
        textdata: data.string.pnum6,
        textclass: "num_6",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/6.png"
      },
      {
        textdata: data.string.pnum5,
        textclass: "num_5",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/5.png"
      },
      {
        textdata: data.string.pnum4,
        textclass: "num_4",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/4.png"
      },
      {
        textdata: data.string.pnum3,
        textclass: "num_3",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/3.png"
      },
      {
        textdata: data.string.pnum2,
        textclass: "num_2",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/2.png"
      },
      {
        textdata: data.string.pnum1,
        textclass: "num_1",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/1.png"
      },
      {
        textdata: data.string.pnum0,
        textclass: "num_0  auto_cursor",
        imgclass: "stick_image",
        imgsrc: imgpath + "sticks/0.png"
      }
    ]
  },
  //slide2
  {
    contentblockadditionalclass: "main_bg",
    contentblocknocenteradjust: true,

    uppertextblockadditionalclass: "introduction",
    uppertextblock: [
      {
        textdata: data.string.p1text1,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red"
      },
      {
        textdata: data.string.p1text2,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_green"
      }
    ],

    numberlineadditionalclass: "ones_line",
    numberline: [
      {
        textdata: data.string.pnum9,
        textclass: "num_9",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/9.png"
      },
      {
        textdata: data.string.pnum8,
        textclass: "num_8",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/8.png"
      },
      {
        textdata: data.string.pnum7,
        textclass: "num_7",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/7.png"
      },
      {
        textdata: data.string.pnum6,
        textclass: "num_6",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/6.png"
      },
      {
        textdata: data.string.pnum5,
        textclass: "num_5",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/5.png"
      },
      {
        textdata: data.string.pnum4,
        textclass: "num_4",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/4.png"
      },
      {
        textdata: data.string.pnum3,
        textclass: "num_3",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/3.png"
      },
      {
        textdata: data.string.pnum2,
        textclass: "num_2",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/2.png"
      },
      {
        textdata: data.string.pnum1,
        textclass: "num_1 number_highlight auto_cursor",
        imgclass: "stick_image",
        imgsrc: imgpath + "sticks/1.png"
      },
      {
        textdata: data.string.pnum0,
        textclass: "num_0",
        imgclass: "stick_image",
        imgsrc: imgpath + "sticks/0.png"
      }
    ]
  },
  //slide3
  {
    contentblockadditionalclass: "main_bg",
    contentblocknocenteradjust: true,

    uppertextblockadditionalclass: "introduction",
    uppertextblock: [
      {
        textdata: data.string.p1text1,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red"
      },
      {
        textdata: data.string.p1text2,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_green"
      },
      {
        textdata: data.string.p1text3,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red"
      }
    ],

    numberlineadditionalclass: "ones_line",
    numberline: [
      {
        textdata: data.string.pnum9,
        textclass: "num_9",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/9.png"
      },
      {
        textdata: data.string.pnum8,
        textclass: "num_8",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/8.png"
      },
      {
        textdata: data.string.pnum7,
        textclass: "num_7",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/7.png"
      },
      {
        textdata: data.string.pnum6,
        textclass: "num_6",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/6.png"
      },
      {
        textdata: data.string.pnum5,
        textclass: "num_5",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/5.png"
      },
      {
        textdata: data.string.pnum4,
        textclass: "num_4",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/4.png"
      },
      {
        textdata: data.string.pnum3,
        textclass: "num_3",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/3.png"
      },
      {
        textdata: data.string.pnum2,
        textclass: "num_2 number_highlight auto_cursor",
        imgclass: "stick_image",
        imgsrc: imgpath + "sticks/2.png"
      },
      {
        textdata: data.string.pnum1,
        textclass: "num_1",
        imgclass: "stick_image",
        imgsrc: imgpath + "sticks/1.png"
      },
      {
        textdata: data.string.pnum0,
        textclass: "num_0",
        imgclass: "stick_image",
        imgsrc: imgpath + "sticks/0.png"
      }
    ]
  },
  //slide4
  {
    contentblockadditionalclass: "main_bg",
    contentblocknocenteradjust: true,

    uppertextblockadditionalclass: "introduction",
    uppertextblock: [
      {
        textdata: data.string.p1text1,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red"
      },
      {
        textdata: data.string.p1text2,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_green"
      },
      {
        textdata: data.string.p1text3,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red"
      },
      {
        textdata: data.string.p1text4,
        textclass: "intro_text",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red_dark"
      }
    ],

    numberlineadditionalclass: "ones_line",
    numberline: [
      {
        textdata: data.string.pnum9,
        textclass: "num_9",
        imgclass: "stick_image stick_9 its_hidden",
        imgsrc: imgpath + "sticks/9.png"
      },
      {
        textdata: data.string.pnum8,
        textclass: "num_8",
        imgclass: "stick_image stick_8 its_hidden",
        imgsrc: imgpath + "sticks/8.png"
      },
      {
        textdata: data.string.pnum7,
        textclass: "num_7",
        imgclass: "stick_image stick_7 its_hidden",
        imgsrc: imgpath + "sticks/7.png"
      },
      {
        textdata: data.string.pnum6,
        textclass: "num_6",
        imgclass: "stick_image stick_6 its_hidden",
        imgsrc: imgpath + "sticks/6.png"
      },
      {
        textdata: data.string.pnum5,
        textclass: "num_5",
        imgclass: "stick_image stick_5 its_hidden",
        imgsrc: imgpath + "sticks/5.png"
      },
      {
        textdata: data.string.pnum4,
        textclass: "num_4",
        imgclass: "stick_image stick_4 its_hidden",
        imgsrc: imgpath + "sticks/4.png"
      },
      {
        textdata: data.string.pnum3,
        textclass: "num_3",
        imgclass: "stick_image stick_3 its_hidden",
        imgsrc: imgpath + "sticks/3.png"
      },
      {
        textdata: data.string.pnum2,
        textclass: "num_2 number_highlight",
        imgclass: "stick_image stick_2",
        imgsrc: imgpath + "sticks/2.png"
      },
      {
        textdata: data.string.pnum1,
        textclass: "num_1",
        imgclass: "stick_image",
        imgsrc: imgpath + "sticks/1.png"
      },
      {
        textdata: data.string.pnum0,
        textclass: "num_0",
        imgclass: "stick_image",
        imgsrc: imgpath + "sticks/0.png"
      }
    ]
  },

  //slide5
  {
    contentblockadditionalclass: "main_bg",
    contentblocknocenteradjust: true,

    uppertextblockadditionalclass: "introduction_question",
    uppertextblock: [
      {
        textdata: data.string.p1text5,
        textclass: "biggestnumbertext ",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red"
      }
    ],
    lowertextblockadditionalclass: "feedback",
    lowertextblock: [
      {
        textdata: data.string.p1text6,
        textclass: "incorrect its_hidden",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red_dark"
      },
      {
        textdata: data.string.p1text7,
        textclass: "correct its_hidden",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red_dark"
      }
    ],

    numberlineadditionalclass: "ones_line1 nl_6",
    numberline: [
      {
        elementclass: "line_element1",
        textdata: data.string.pnum8,
        textclass: "num_8 number_highlight",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/8.png"
      },
      {
        elementclass: "line_element1",
        textdata: data.string.pnum7,
        textclass: "num_7 number_highlight",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/7.png"
      },
      {
        elementclass: "line_element1",
        textdata: data.string.pnum9,
        textclass: "num_9 number_highlight",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/9.png"
      }
    ]
  },
  //slide6
  {
    contentblockadditionalclass: "main_bg",
    contentblocknocenteradjust: true,

    uppertextblockadditionalclass: "introduction_question",
    uppertextblock: [
      {
        textdata: data.string.p1text5,
        textclass: "biggestnumbertext",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red"
      }
    ],
    lowertextblockadditionalclass: "feedback",
    lowertextblock: [
      {
        textdata: data.string.p1text6,
        textclass: "incorrect its_hidden",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red_dark"
      },
      {
        textdata: data.string.p1text7,
        textclass: "correct its_hidden",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_red_dark"
      }
    ],

    numberlineadditionalclass: "ones_line1 nl_6",
    numberline: [
      {
        elementclass: "line_element1",
        textdata: data.string.pnum5,
        textclass: "num_5 number_highlight",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/5.png"
      },
      {
        elementclass: "line_element1",
        textdata: data.string.pnum7,
        textclass: "num_7 number_highlight",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/7.png"
      },
      {
        elementclass: "line_element1",
        textdata: data.string.pnum1,
        textclass: "num_1 number_highlight",
        imgclass: "stick_image its_hidden",
        imgsrc: imgpath + "sticks/1.png"
      }
    ]
  }
];

$(function() {
  var $board = $(".board");

  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var countNext = 0;
  var $label = $(".label-box");
  var $total_page = content.length;
  var prev_large = 9;

  loadTimelineProgress($total_page, countNext + 1);

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
      // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
      //   ,
      //images
      // {id: "bg01", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
      // {id: "girl1", src: imgpath+"girl1.png", type: createjs.AbstractLoader.IMAGE},

      // soundsicon-orange
      { id: "s1_p1", src: soundAsset + "s1_p1.ogg" },
      { id: "s1_p2", src: soundAsset + "s1_p2.ogg" },
      { id: "s1_p3", src: soundAsset + "s1_p3.ogg" },
      { id: "s1_p4", src: soundAsset + "s1_p4.ogg" },
      { id: "s1_p5", src: soundAsset + "s1_p5.ogg" },
      { id: "s1_p6", src: soundAsset + "s1_p6.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
  Handlebars.registerPartial(
    "definitioncontent",
    $("#definitioncontent-partial").html()
  );
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // controls the navigational state of the program
  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if (countNext > 1) {
      $nextBtn.hide(0);
      $prevBtn.show(0);
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");
      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);
      // if lastpageflag is true
      // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);

    loadTimelineProgress($total_page, countNext + 1);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    $nextBtn.hide(0);
    $prevBtn.hide(0);
    switch (countNext) {
      case 0:
      case 1:
      case 2:
      case 3:
        sound_player("s1_p" + (countNext + 1), 1);
        break;
      case 4:
        sound_player("s1_p" + (countNext + 1), 1);
        var i = 2;
        $prevBtn.hide(0);
        var my_interval = setInterval(function() {
          var m = i + 1;
          $(".num_" + m).addClass("number_highlight auto_cursor");
          $(".num_" + i).removeClass("number_highlight auto_cursor");
          $(".stick_" + m).removeClass("its_hidden");
          i++;
          if (i > 8) {
            clearInterval(my_interval);
            // $nextBtn.show(0);
            // $prevBtn.show(0);
          }
        }, 500);
        break;
      case 5:
        sound_player("s1_p" + (countNext + 1), 0);
        checkans(9);
        break;
      case 6:
        sound_player("s1_p6", 0);
        sound_player("s1_p" + (countNext + 1), 0);
        checkans(7);
        break;
      default:
        $nextBtn.show(0);
        $prevBtn.show(0);
        break;
    }
  }

  function checkans(ans) {
    $(".numberline")
      .find("div")
      .click(function() {
        if (
          $(this)
            .find("p")
            .text() == ans
        ) {
          $(this)
            .find(".wrong")
            .hide(0);
          $(".correct").show(0);
          $(".incorrect").hide(0);
          $(this)
            .find(".right")
            .show(0);
          play_correct_incorrect_sound(1);
          nav_button_controls();
        } else {
          console.log("This is false");
          $(this)
            .find(".wrong")
            .show(0);
          $(this)
            .find(".right")
            .hide(0);
          $(".incorrect").show(0);
          play_correct_incorrect_sound(0);
        }
        $(".stick_image").show();
        $(".highlight_red_dark").text(ans);
      });
  }
  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id, nextBtn) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      if (nextBtn) {
        nav_button_controls(200);
        // $nextBtn.show(0);
        // $prevBtn.show(0);
      }
    });
  }
  function templateCaller() {
    //convention is to always hide the prev and next button and show them based
    //on the convention or page index
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();

    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    countNext--;
    templateCaller();
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });

  total_page = content.length;
  templateCaller();
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object"
    ? alert(
        "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
      )
    : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
      $(this).attr(
        "data-highlightcustomclass"
      ) /*if there is data-highlightcustomclass defined it is true else it is not*/
        ? (stylerulename = $(this).attr("data-highlightcustomclass"))
        : (stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

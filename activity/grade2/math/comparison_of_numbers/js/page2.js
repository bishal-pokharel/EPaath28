var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content = [

	//slide0
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "introduction",
		uppertextblock : [
		{
			textdata : data.string.p2text1,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		}],

		numberlineadditionalclass: 'ones_line',
		numberline : [
			{
				textdata : data.string.pnum9,
				textclass : 'num_gray ones_9',
				text2data : data.string.pnum9,
				text2class : 'num_9',
			},
			{
				textdata : data.string.pnum8,
				textclass : 'num_gray ones_8',
				text2data : data.string.pnum8,
				text2class : 'num_8',
			},
			{
				textdata : data.string.pnum7,
				textclass : 'num_gray ones_7',
				text2data : data.string.pnum7,
				text2class : 'num_7',
			},
			{
				textdata : data.string.pnum6,
				textclass : 'num_gray ones_6',
				text2data : data.string.pnum6,
				text2class : 'num_6',
			},
			{
				textdata : data.string.pnum5,
				textclass : 'num_gray ones_5',
				text2data : data.string.pnum5,
				text2class : 'num_5',
			},
			{
				textdata : data.string.pnum4,
				textclass : 'num_gray ones_4',
				text2data : data.string.pnum4,
				text2class : 'num_4',
			},
			{
				textdata : data.string.pnum3,
				textclass : 'num_gray ones_3',
				text2data : data.string.pnum3,
				text2class : 'num_3',
			},
			{
				textdata : data.string.pnum2,
				textclass : 'num_gray ones_2',
				text2data : data.string.pnum2,
				text2class : 'num_2',
			},
			{
				textdata : data.string.pnum1,
				textclass : 'num_gray ones_1',
				text2data : data.string.pnum1,
				text2class : 'num_1',
			},
			{
				textdata : data.string.pnum0,
				textclass : 'num_gray ones_0 number_highlight',
				text2data : data.string.pnum0,
				text2class : 'num_0 number_highlight',
			}
		],
		imageblock : [{
			imagelabels : [
			{
				imagelabelclass : "tens_box",
				imagelabeldata : data.string.ptens
			},
			{
				imagelabelclass : "ones_box",
				imagelabeldata : data.string.pones
			}]
		}],
	},
	//slide1
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "introduction",
		uppertextblock : [
		{
			textdata : data.string.p2text1,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text2,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		],

		numberlineadditionalclass: 'ones_line',
		numberline : [
			{
				textdata : data.string.pnum9,
				textclass : 'num_gray ones_9',
				text2data : data.string.pnum9,
				text2class : 'num_9',
			},
			{
				textdata : data.string.pnum8,
				textclass : 'num_gray ones_8',
				text2data : data.string.pnum8,
				text2class : 'num_8',
			},
			{
				textdata : data.string.pnum7,
				textclass : 'num_gray ones_7',
				text2data : data.string.pnum7,
				text2class : 'num_7',
			},
			{
				textdata : data.string.pnum6,
				textclass : 'num_gray ones_6',
				text2data : data.string.pnum6,
				text2class : 'num_6',
			},
			{
				textdata : data.string.pnum5,
				textclass : 'num_gray ones_5',
				text2data : data.string.pnum5,
				text2class : 'num_5',
			},
			{
				textdata : data.string.pnum4,
				textclass : 'num_gray ones_4',
				text2data : data.string.pnum4,
				text2class : 'num_4',
			},
			{
				textdata : data.string.pnum3,
				textclass : 'num_gray ones_3',
				text2data : data.string.pnum3,
				text2class : 'num_3',
			},
			{
				textdata : data.string.pnum2,
				textclass : 'num_gray ones_2',
				text2data : data.string.pnum2,
				text2class : 'num_2',
			},
			{
				textdata : data.string.pnum1,
				textclass : 'num_gray ones_1',
				text2data : data.string.pnum1,
				text2class : 'num_1',
			},
			{
				textdata : data.string.pnum0,
				textclass : 'num_gray ones_0 number_highlight',
				text2data : data.string.pnum0,
				text2class : 'num_0 number_highlight',
			}
		],
		imageblock : [{
			imagelabels : [
			{
				imagelabelclass : "tens_box",
				imagelabeldata : data.string.ptens
			}]
		}],
	},
	//slide2
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "introduction",
		uppertextblock : [
		{
			textdata : data.string.p2text1,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text2,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text3,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		}],

		numberlineadditionalclass: 'ones_line',
		numberline : [
			{
				textdata : data.string.pnum9,
				textclass : 'num_gray ones_9',
				text2data : data.string.pnum9,
				text2class : 'num_9',
			},
			{
				textdata : data.string.pnum8,
				textclass : 'num_gray ones_8',
				text2data : data.string.pnum8,
				text2class : 'num_8',
			},
			{
				textdata : data.string.pnum7,
				textclass : 'num_gray ones_7',
				text2data : data.string.pnum7,
				text2class : 'num_7',
			},
			{
				textdata : data.string.pnum6,
				textclass : 'num_gray ones_6',
				text2data : data.string.pnum6,
				text2class : 'num_6',
			},
			{
				textdata : data.string.pnum5,
				textclass : 'num_gray ones_5',
				text2data : data.string.pnum5,
				text2class : 'num_5',
			},
			{
				textdata : data.string.pnum4,
				textclass : 'num_gray ones_4',
				text2data : data.string.pnum4,
				text2class : 'num_4',
			},
			{
				textdata : data.string.pnum3,
				textclass : 'num_gray ones_3',
				text2data : data.string.pnum3,
				text2class : 'num_3',
			},
			{
				textdata : data.string.pnum2,
				textclass : 'num_gray ones_2',
				text2data : data.string.pnum2,
				text2class : 'num_2',
			},
			{
				textdata : data.string.pnum1,
				textclass : 'num_gray ones_1',
				text2data : data.string.pnum1,
				text2class : 'num_1',
			},
			{
				textdata : data.string.pnum0,
				textclass : 'num_gray ones_0 number_highlight',
				text2data : data.string.pnum0,
				text2class : 'num_0 number_highlight',
			}
		],
		imageblock : [{
			imagelabels : [
			{
				imagelabelclass : "tens_box",
				imagelabeldata : data.string.ptens
			}]
		}],
	},
	//slide3
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "introduction",
		uppertextblock : [
		{
			textdata : data.string.p2text1,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text2,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text3,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text5,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		}],

		numberlineadditionalclass: 'ones_line',
		numberline : [
			{
				textdata : data.string.pnum9,
				textclass : 'num_gray ones_9',
				text2data : data.string.pnum9,
				text2class : 'num_9 number_highlight',
			},
			{
				textdata : data.string.pnum8,
				textclass : 'num_gray ones_8',
				text2data : data.string.pnum8,
				text2class : 'num_8',
			},
			{
				textdata : data.string.pnum7,
				textclass : 'num_gray ones_7',
				text2data : data.string.pnum7,
				text2class : 'num_7',
			},
			{
				textdata : data.string.pnum6,
				textclass : 'num_gray ones_6',
				text2data : data.string.pnum6,
				text2class : 'num_6',
			},
			{
				textdata : data.string.pnum5,
				textclass : 'num_gray ones_5',
				text2data : data.string.pnum5,
				text2class : 'num_5',
			},
			{
				textdata : data.string.pnum4,
				textclass : 'num_gray ones_4',
				text2data : data.string.pnum4,
				text2class : 'num_4',
			},
			{
				textdata : data.string.pnum3,
				textclass : 'num_gray ones_3',
				text2data : data.string.pnum3,
				text2class : 'num_3',
			},
			{
				textdata : data.string.pnum2,
				textclass : 'num_gray ones_2',
				text2data : data.string.pnum2,
				text2class : 'num_2',
			},
			{
				textdata : data.string.pnum1,
				textclass : 'num_gray ones_1',
				text2data : data.string.pnum1,
				text2class : 'num_1',
			},
			{
				textdata : data.string.pnum0,
				textclass : 'num_gray ones_0 number_highlight',
				text2data : data.string.pnum0,
				text2class : 'num_0',
			}
		],
		optionboxadditionalclass:'optiondiv',
		optionbox : [
		{
			textdata : data.string.pnum0,
			optioncontainerclass : 'option_1 its_hidden',
		},
		{
			textdata : data.string.pnum0,
			optioncontainerclass : 'option_2 its_hidden',
		},
		{
			textdata : data.string.pnum0,
			optioncontainerclass : 'option_3 its_hidden',
		},
		{
			textdata : data.string.pnum0,
			optioncontainerclass : 'option_4 its_hidden',
		}],
	},
	//slide4
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "introduction",
		uppertextblock : [
		{
			textdata : data.string.p2text1,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text2,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text3,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p2text5,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		}],
		lowertextblockadditionalclass : "bottom_question",
		lowertextblock : [
		{
			textclass : "intro_text",
			textdata : data.string.p2text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red_dark'
		}],

		imageblock : [{
			imagelabels : [
			{
				imagelabeldata : data.string.p2text10,
				imagelabelclass : 'feedback_1 incorrect its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red_dark'
			},
			{
				imagelabeldata : data.string.p2text11,
				imagelabelclass : 'feedback_1 correct its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red_dark'
			}]
		}],


		numberlineadditionalclass: 'ones_line',
		numberline : [
			{
				textdata : data.string.pnum9,
				textclass : 'num_gray ones_9',
				text2data : data.string.pnum9,
				text2class : 'num_9',
			},
			{
				textdata : data.string.pnum8,
				textclass : 'num_gray ones_8',
				text2data : data.string.pnum8,
				text2class : 'num_8',
			},
			{
				textdata : data.string.pnum7,
				textclass : 'num_gray ones_7',
				text2data : data.string.pnum7,
				text2class : 'num_7',
			},
			{
				textdata : data.string.pnum6,
				textclass : 'num_gray ones_6',
				text2data : data.string.pnum6,
				text2class : 'num_6',
			},
			{
				textdata : data.string.pnum5,
				textclass : 'num_gray ones_5',
				text2data : data.string.pnum5,
				text2class : 'num_5',
			},
			{
				textdata : data.string.pnum4,
				textclass : 'num_gray ones_4',
				text2data : data.string.pnum4,
				text2class : 'num_4',
			},
			{
				textdata : data.string.pnum3,
				textclass : 'num_gray ones_3',
				text2data : data.string.pnum3,
				text2class : 'num_3',
			},
			{
				textdata : data.string.pnum2,
				textclass : 'num_gray ones_2',
				text2data : data.string.pnum2,
				text2class : 'num_2',
			},
			{
				textdata : data.string.pnum1,
				textclass : 'num_gray ones_1',
				text2data : data.string.pnum1,
				text2class : 'num_1',
			},
			{
				textdata : data.string.pnum0,
				textclass : 'num_gray ones_0',
				text2data : data.string.pnum0,
				text2class : 'num_0',
			}
		],

		optionboxadditionalclass:'optiondiv',
		optionbox : [
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_1',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_2',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_3',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_4',
		}],
	},

	//slide5
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "introduction",
		uppertextblock : [
		{
			textdata : data.string.newtext,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		}
		],
		lowertextblockadditionalclass : "introduction_1",
		lowertextblock : [
			{
				textclass : "intro_text",
				textdata : data.string.p2text4
			},
			{
				textdata : data.string.p2text6,
				textclass : 'intro_text its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_tens_1'
			},
			{
				textdata : data.string.p2text7,
				textclass : 'intro_text its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_tens_2 '
			},
			{
				textdata : data.string.p2text8,
				textclass : 'intro_text its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_number_correct'
			},
			{
				textdata : data.string.p2text9,
				textclass : 'intro_text its_hidden',
			},
		],

		imageblock : [{
			imagelabels : [
			{
				imagelabeldata : data.string.p2text10,
				imagelabelclass : 'feedback_1 incorrect its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red_dark'
			},
			{
				imagelabeldata : data.string.p2text11,
				imagelabelclass : 'feedback_1 correct its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red_dark'
			}]
		}],


		numberlineadditionalclass: 'ones_line',
		numberline : [
			{
				textdata : data.string.pnum9,
				textclass : 'num_gray ones_9',
				text2data : data.string.pnum9,
				text2class : 'num_9',
			},
			{
				textdata : data.string.pnum8,
				textclass : 'num_gray ones_8',
				text2data : data.string.pnum8,
				text2class : 'num_8',
			},
			{
				textdata : data.string.pnum7,
				textclass : 'num_gray ones_7',
				text2data : data.string.pnum7,
				text2class : 'num_7',
			},
			{
				textdata : data.string.pnum6,
				textclass : 'num_gray ones_6',
				text2data : data.string.pnum6,
				text2class : 'num_6',
			},
			{
				textdata : data.string.pnum5,
				textclass : 'num_gray ones_5',
				text2data : data.string.pnum5,
				text2class : 'num_5',
			},
			{
				textdata : data.string.pnum4,
				textclass : 'num_gray ones_4',
				text2data : data.string.pnum4,
				text2class : 'num_4',
			},
			{
				textdata : data.string.pnum3,
				textclass : 'num_gray ones_3',
				text2data : data.string.pnum3,
				text2class : 'num_3',
			},
			{
				textdata : data.string.pnum2,
				textclass : 'num_gray ones_2',
				text2data : data.string.pnum2,
				text2class : 'num_2',
			},
			{
				textdata : data.string.pnum1,
				textclass : 'num_gray ones_1',
				text2data : data.string.pnum1,
				text2class : 'num_1',
			},
			{
				textdata : data.string.pnum0,
				textclass : 'num_gray ones_0',
				text2data : data.string.pnum0,
				text2class : 'num_0',
			}
		],

		optionboxadditionalclass:'optiondiv_1',
		optionbox : [
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_1',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_2',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_3',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_4',
		}],
	},

	//slide6
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "introduction",
		uppertextblock : [
		{
			textdata : data.string.newtext,
			textclass : 'intro_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		}
		],
		lowertextblockadditionalclass : "introduction_1",
		lowertextblock : [
			{
				textclass : "intro_text",
				textdata : data.string.p2text4
			}
		],

		imageblock : [{
			imagelabels : [
			{
				imagelabeldata : data.string.p2text10,
				imagelabelclass : 'feedback_1 incorrect its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red_dark'
			},
			{
				imagelabeldata : data.string.p2text11,
				imagelabelclass : 'feedback_1 correct its_hidden',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red_dark'
			}]
		}],


		numberlineadditionalclass: 'ones_line',
		numberline : [
			{
				textdata : data.string.pnum9,
				textclass : 'num_gray ones_9',
				text2data : data.string.pnum9,
				text2class : 'num_9',
			},
			{
				textdata : data.string.pnum8,
				textclass : 'num_gray ones_8',
				text2data : data.string.pnum8,
				text2class : 'num_8',
			},
			{
				textdata : data.string.pnum7,
				textclass : 'num_gray ones_7',
				text2data : data.string.pnum7,
				text2class : 'num_7',
			},
			{
				textdata : data.string.pnum6,
				textclass : 'num_gray ones_6',
				text2data : data.string.pnum6,
				text2class : 'num_6',
			},
			{
				textdata : data.string.pnum5,
				textclass : 'num_gray ones_5',
				text2data : data.string.pnum5,
				text2class : 'num_5',
			},
			{
				textdata : data.string.pnum4,
				textclass : 'num_gray ones_4',
				text2data : data.string.pnum4,
				text2class : 'num_4',
			},
			{
				textdata : data.string.pnum3,
				textclass : 'num_gray ones_3',
				text2data : data.string.pnum3,
				text2class : 'num_3',
			},
			{
				textdata : data.string.pnum2,
				textclass : 'num_gray ones_2',
				text2data : data.string.pnum2,
				text2class : 'num_2',
			},
			{
				textdata : data.string.pnum1,
				textclass : 'num_gray ones_1',
				text2data : data.string.pnum1,
				text2class : 'num_1',
			},
			{
				textdata : data.string.pnum0,
				textclass : 'num_gray ones_0',
				text2data : data.string.pnum0,
				text2class : 'num_0',
			}
		],

		optionboxadditionalclass:'optiondiv_1',
		optionbox : [
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_1',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_2',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_3',
		},
		{
			textdata : data.string.pnum0,
			textclass : 'option_highlight',
			optioncontainerclass : 'option_4',
		}],
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var prev_large = 9;
	var not_go_next = 0;

	var num_1_tens = 0;
	var num_1_ones = 0;

	var num_2_tens = 0;
	var num_2_ones = 0;

	var num_3_tens = 0;
	var num_3_ones = 0;

	var num_4_tens = 0;
	var num_4_ones = 0;

	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "girl1", src: imgpath+"girl1.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if (countNext > 1) {
			$nextBtn.hide(0);
			$prevBtn.show(0);
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		switch (countNext) {
		case 0:
		case 1:
		sound_player("s2_p"+(countNext+1),1);
		break;
		case 2:
		sound_player("s2_p"+(countNext+1),1);
			var i = 0;
			$prevBtn.hide(0);
			var my_interval = setInterval(function(){
				var m = i+1;
				$('.num_'+m).addClass('number_highlight');
				$('.num_'+i).removeClass('number_highlight');
				$('.stick_'+m).removeClass('its_hidden');
				i++;
				if( i > 8){
					clearInterval(my_interval);
					// $nextBtn.show(0);
					// $prevBtn.show(0);
				}
			}, 500);
			break;
		case 3:
		sound_player("s2_p"+(countNext+1),1);
			not_go_next = 1;
			num_1_ones = parseInt(ole.getRandom(1,9,0));
			num_2_ones = num_1_ones;
			num_3_ones = num_1_ones;
			num_4_ones = num_1_ones;
			num_1_tens = parseInt(ole.getRandom(1,4,1));
			num_2_tens = parseInt(ole.getRandom(1,num_1_tens+2,num_1_tens+1));
			num_3_tens = parseInt(ole.getRandom(1,num_2_tens+2,num_2_tens+1));
			num_4_tens = parseInt(ole.getRandom(1,9,num_3_tens+1));
			$('.option_1>p').html(num_1_tens*10+num_1_ones);
			$('.option_2>p').html(num_2_tens*10+num_2_ones);
			$('.option_3>p').html(num_3_tens*10+num_3_ones);
			$('.option_4>p').html(num_4_tens*10+num_4_ones);
			// $nextBtn.show(0);
			// $prevBtn.show(0);
			break;
		case 4:
		sound_player("s2_p"+(countNext+1),0);
			$nextBtn.hide(0);
			$('.option_1>p').html(num_1_tens*10+num_1_ones);
			$('.option_2>p').html(num_2_tens*10+num_2_ones);
			$('.option_3>p').html(num_3_tens*10+num_3_ones);
			$('.option_4>p').html(num_4_tens*10+num_4_ones);
			$('.highlight_red_dark').html(num_4_tens*10+num_4_ones);
			line_on_hover('.option_1', num_1_ones, num_1_tens);
			line_on_hover('.option_2', num_2_ones, num_2_tens);
			line_on_hover('.option_3', num_3_ones, num_3_tens);
			line_on_hover('.option_4', num_4_ones, num_4_tens);
			option_on_click('.option_1', false);
			option_on_click('.option_2', false);
			option_on_click('.option_3', false);
			option_on_click('.option_4', true);
			break;
		case 5:
		sound_player("s2_p"+(countNext+1),1);
			not_go_next = 1;
			// $nextBtn.show(0);
			// $prevBtn.show(0);
			// $nextBtn.hide(0);
			// generate numbers
			num_4_ones = parseInt(ole.getRandom(1,9,1));
			num_1_ones = parseInt(ole.getRandom(1,num_4_ones-1,0));
			num_2_ones = parseInt(ole.getRandom(1,9,0));
			num_3_ones = parseInt(ole.getRandom(1,9,0));

			num_1_tens = parseInt(ole.getRandom(1,9,4));
			num_2_tens = parseInt(ole.getRandom(1,num_1_tens-1, 1));
			num_3_tens = parseInt(ole.getRandom(1,num_1_tens-1, 1));

			// check 2 numbers
			while(num_3_tens==num_2_tens){
				num_3_tens = parseInt(ole.getRandom(1,num_1_tens-1, 1));
			}
			num_4_tens = num_1_tens;

			// change text
			$('.highlight_tens_1').html(num_4_tens);
			$('.highlight_tens_2').html(num_4_ones);
			$('.highlight_tens_3').html(num_1_ones);
			$('.highlight_tens_4').html(num_4_tens*10+num_4_ones);
			$('.highlight_tens_5').html(num_1_tens*10+num_1_ones);
			$('.highlight_red_dark').html(num_4_tens*10+num_4_ones);
			$('.highlight_number_correct').html(num_4_tens*10+num_4_ones);

			// randomize options
			randomize_options_3();

			// change option text
			$('.option_1>p').html(num_1_tens*10+num_1_ones);
			$('.option_2>p').html(num_2_tens*10+num_2_ones);
			$('.option_3>p').html(num_3_tens*10+num_3_ones);
			$('.option_4>p').html(num_4_tens*10+num_4_ones);

			// hover and click functions
			line_on_hover('.option_1', num_1_ones, num_1_tens);
			line_on_hover('.option_2', num_2_ones, num_2_tens);
			line_on_hover('.option_3', num_3_ones, num_3_tens);
			line_on_hover('.option_4', num_4_ones, num_4_tens);
			option_on_click('.option_1', false);
			option_on_click('.option_2', false);
			option_on_click('.option_3', false);
			option_on_click('.option_4', true);
			break;
		case 6:
		sound_player("s2_p6",0);
			var bools = [false, false, false, false];
			// generate numbers
			num_4_ones = parseInt(ole.getRandom(1,9,1));
			num_1_ones = parseInt(ole.getRandom(1,num_4_ones-1,0));
			num_2_ones = parseInt(ole.getRandom(1,9,0));
			num_3_ones = parseInt(ole.getRandom(1,9,0));

			num_1_tens = parseInt(ole.getRandom(1,9,4));
			num_2_tens = parseInt(ole.getRandom(1,num_1_tens-1, 1));
			num_3_tens = parseInt(ole.getRandom(1,num_1_tens-1, 1));
			// check 2 numbers
			while(num_3_tens==num_2_tens){
				num_3_tens = parseInt(ole.getRandom(1,num_1_tens-1, 1));
			}
			num_4_tens = num_1_tens;
			// change text
			$('.highlight_red_dark').html(num_4_tens*10+num_4_ones);

			// randomize options
			var correct_index = randomize_options_4();
			bools[correct_index] = true;

			// change option text
			$('.option_1>p').html(num_1_tens*10+num_1_ones);
			$('.option_2>p').html(num_2_tens*10+num_2_ones);
			$('.option_3>p').html(num_3_tens*10+num_3_ones);
			$('.option_4>p').html(num_4_tens*10+num_4_ones);

			// hover and click functions
			line_on_hover('.option_1', num_1_ones, num_1_tens);
			line_on_hover('.option_2', num_2_ones, num_2_tens);
			line_on_hover('.option_3', num_3_ones, num_3_tens);
			line_on_hover('.option_4', num_4_ones, num_4_tens);

			option_on_click('.option_1', bools[0]);
			option_on_click('.option_2', bools[1]);
			option_on_click('.option_3', bools[2]);
			option_on_click('.option_4', bools[3]);
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(nextBtn){
				nav_button_controls(200);
				// $nextBtn.show(0);
				// $prevBtn.show(0);
			}
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			case 3:
				if(!not_go_next){
					countNext++;
					templateCaller();
				} else if(not_go_next == 1) {
					$('.option_1').removeClass('its_hidden');
					$('.ones_0').removeClass('number_highlight');
					$('.num_9').removeClass('number_highlight');
					$('.ones_'+num_1_ones).addClass('number_highlight');
					$('.num_'+num_1_tens).addClass('number_highlight');
					not_go_next = 2;
				} else if(not_go_next == 2) {
					$('.option_2').removeClass('its_hidden');
					$('.num_'+num_1_tens).removeClass('number_highlight');
					$('.num_'+num_2_tens).addClass('number_highlight');
					not_go_next = 3;
				} else if(not_go_next == 3) {
					$('.option_3').removeClass('its_hidden');
					$('.num_'+num_2_tens).removeClass('number_highlight');
					$('.num_'+num_3_tens).addClass('number_highlight');
					not_go_next = 4;
				} else if(not_go_next == 4) {
					$('.option_4').removeClass('its_hidden');
					$('.num_'+num_3_tens).removeClass('number_highlight');
					$('.num_'+num_4_tens).addClass('number_highlight');
					not_go_next = 0;
				}
				break;
			case 5:
				if(!not_go_next){
					countNext++;
					templateCaller();
				}
				var $newEntry = $(".introduction_1>.its_hidden").eq(0);
				$newEntry.removeClass('its_hidden');
				not_go_next++;
				if(not_go_next==5){
					not_go_next = 0;
				}

				break;
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	function line_on_hover(option_class, ones_number, tens_number){
		$(option_class+ '>p').hover(function(){
			$('.line_element>p').removeClass('number_highlight');
			$('.ones_'+ones_number).addClass('number_highlight');
			$('.num_'+tens_number).addClass('number_highlight');
		}, function(){
			$('.line_element>p').removeClass('number_highlight');
		});
	}
	function option_on_click(option_div_class, correct_bool){
		$(option_div_class+ '>p').click(function(){
			if(correct_bool){
				$('.incorrect').hide(0);
				$('.correct').removeClass('its_hidden');
				$('.incorrect').addClass('its_hidden');
				$('.optionbox').css('pointer-events', 'none');
				$(option_div_class+'>.right').show(0);
				$(option_div_class+ '>p').addClass('correct_option');
	 			play_correct_incorrect_sound(1);
				if(countNext == content.length-1)
				{
					ole.footerNotificationHandler.lessonEndSetNotification();
					$prevBtn.show(0);
				} else {
					$nextBtn.show(0);
					$prevBtn.show(0);
				}
			} else {
				$(option_div_class+'>.wrong').show(0);
				$(option_div_class).css('pointer-events', 'none');
				$('.incorrect').removeClass('its_hidden');
				$(option_div_class+ '>p').addClass('incorrect_option');
	 			play_correct_incorrect_sound(0);
			}
		});
	}
	function randomize_options_3(){
		var arr = [];
		var arr2 = [];
		var return_arr = [];
		var return_arr2 = [];
		arr.push(num_1_ones);
		arr.push(num_2_ones);
		arr.push(num_3_ones);
		arr2.push(num_1_tens);
		arr2.push(num_2_tens);
		arr2.push(num_3_tens);
		while(arr.length) {
			var index = ole.getRandom(1, arr.length-1, 0);
			return_arr.push(arr[index]);
			arr.splice((index),1);
			return_arr2.push(arr2[index]);
			arr2.splice((index),1);
		}
		num_1_ones = return_arr[0];
		num_2_ones = return_arr[1];
		num_3_ones = return_arr[2];
		num_1_tens = return_arr2[0];
		num_2_tens = return_arr2[1];
		num_3_tens = return_arr2[2];
	}
	function randomize_options_4(){
		var arr = [];
		var arr2 = [];
		var return_arr = [];
		var return_arr2 = [];
		arr.push(num_1_ones);
		arr.push(num_2_ones);
		arr.push(num_3_ones);
		arr.push(num_4_ones);
		arr2.push(num_1_tens);
		arr2.push(num_2_tens);
		arr2.push(num_3_tens);
		arr2.push(num_4_tens);
		var bool_flag;
		while(arr.length) {
			var index = ole.getRandom(1, arr.length-1, 0);
			return_arr.push(arr[index]);
			arr.splice((index),1);
			return_arr2.push(arr2[index]);
			arr2.splice((index),1);
		}
		for(var s=0; s<4; s++){
			if(return_arr2[s]==num_4_tens && return_arr[s]==num_4_ones){
				bool_flag = s;
			}
		}
		num_1_ones = return_arr[0];
		num_2_ones = return_arr[1];
		num_3_ones = return_arr[2];
		num_4_ones = return_arr[3];
		num_1_tens = return_arr2[0];
		num_2_tens = return_arr2[1];
		num_3_tens = return_arr2[2];
		num_4_tens = return_arr2[3];
		return bool_flag;
	}
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

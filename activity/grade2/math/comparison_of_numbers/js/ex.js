var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

if($lang=='np'){
	var sound_intro = new buzz.sound((soundAsset + "ex1_p1.ogg"));
	var sound_intro_sec = new buzz.sound((soundAsset + "ex1_p2.ogg"));
	var sound_correct = new buzz.sound((soundAsset + "correct.ogg"));
	var sound_incorrect = new buzz.sound((soundAsset + "incorrect.ogg"));
} else {
	var sound_intro = new buzz.sound((soundAsset + "ex1_p1-en.ogg"));
	var sound_intro_sec = new buzz.sound((soundAsset + "ex1_p2-en.ogg"));
	var sound_correct = new buzz.sound((soundAsset + "yummy.ogg"));
	var sound_incorrect = new buzz.sound((soundAsset + "yuk.ogg"));
}
var sound_bye = new buzz.sound((soundAsset + "byebye.ogg"));
var bgmusic = new buzz.sound((soundAsset + "ukulele.ogg"));

var croc_images = ["normal.png", "open.png", "full.png", "dislike.png"];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: "bg_intro",
		uppertextblockadditionalclass : 'instruction_text',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : ''
		}]
	},
	//slide1
	{
        contentblockadditionalclass: "bg_main",
        contentblocknocenteradjust: true,
        uppertextblockadditionalclass: 'score_box',
        uppertextblock: [{
            textdata: '',
            textclass: 'score_data'
        }],
        extratextblockadditionalclass: 'cloudy_text',
        extratextblock: [{
            textdata: data.string.playon,
            textclass: 'playon'
        },
        {
            textdata: data.string.gomenu,
            textclass: 'gomenu'
        }],
    //     imageblock : [{
		// 	// imagestoshow : [
		// 	// 	{
		// 	// 		imgclass:'water',
		// 	// 		imgsrc: imgpath + "water.png",
		// 	// 	},
		// 	// ]
		// 	},
		// ],
        // draggableblockadditionalclass: 'frac_ques',
        draggableblock: [{
            draggables: [{
                draggableclass: "moving_1 fish_1",
                labelclass: "label_1",
                labeldata: '1',
                imgclass: 'fish',
                imgsrc: imgpath + "fish.png"
            }, {
                draggableclass: "moving_2 fish_2",
                labelclass: "label_2",
                labeldata: '2',
                imgclass: 'fish',
                imgsrc: imgpath + "fish.png"
            }, {
                draggableclass: "moving_3 fish_3",
                labelclass: "label_3",
                labeldata: '3',
                imgclass: 'fish',
                imgsrc: imgpath + "fish.png"
            }, {
                draggableclass: "moving_4 fish_4",
                labelclass: "label_4",
                labeldata: '4',
                imgclass: 'fish',
                imgsrc: imgpath + "fish.png"
            }]
        }],
        droppableblock: [{
            droppables: [{
                headerdata: data.string.dropdomestic,
                droppablecontainerclass: "",
                droppableclass: "drop_class_1",
                imgsrc: imgpath + "normal.png",
                imgclass: 'fish_boat'

            }]
        }],
        lowertextblockadditionalclass : 'instruction_text_2',
		lowertextblock : [{
			textdata : data.string.p3text2,
			textclass : ''
		}]
    }
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;

	var fish_val_1;
	var fish_val_2;
	var fish_val_3;
	var fish_val_4;
	var score = 0;
	var croc = 0;
	loadTimelineProgress($total_page, countNext + 1);

	var excCount = 0;

	var current_sound = sound_intro;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
    Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
		case 0:
			sound_player(sound_intro);
			$lang=="en"?$nextBtn.delay(4000).show(0):$nextBtn.delay(8000).show(0);
			break;
		case 1:
				if(excCount==0){
					sound_intro_sec.play();
				$lang=="np"?setTimeout(function(){
											bgmusic.play().loop();},8100):
										setTimeout(function(){
											bgmusic.play().loop();},6100);
				excCount+=1;
			}else{
				bgmusic.play().loop();
			}

			$('.fish_boat').attr('src', imgpath + croc_images[croc]);
			$('.score_data').html(score);
			if(score == 10){
            	$('.cloudy_text').css('display','flex');
							$(".draggableblock").css("pointer-events","none");
            }
			fish_val_1 = parseInt(ole.getRandom(1, 99, 0));
			fish_val_2 = parseInt(ole.getRandom(1, 99, 0));
			fish_val_3 = parseInt(ole.getRandom(1, 99, 0));
			fish_val_4 = parseInt(ole.getRandom(1, 99, 0));
			var highest_index = 1;
			while(fish_val_1 == fish_val_2 || fish_val_1 == fish_val_3 || fish_val_1 == fish_val_4 ){
				fish_val_1 = parseInt(ole.getRandom(1, 99, 0));
			}
			while(fish_val_2 == fish_val_3 || fish_val_2 == fish_val_4 || fish_val_2 == fish_val_1 ){
				fish_val_2 = parseInt(ole.getRandom(1, 99, 0));
			}
			while(fish_val_3 == fish_val_2 || fish_val_3 == fish_val_4 || fish_val_3 == fish_val_1 ){
				fish_val_3 = parseInt(ole.getRandom(1, 99, 0));
			}
			var max = Math.max(fish_val_1, fish_val_2, fish_val_3, fish_val_4);
			if( max == fish_val_1){
				highest_index = 1;
			} else if(max == fish_val_2){
				highest_index = 2;
			} else if(max == fish_val_3){
				highest_index = 3;
			} else {
				highest_index = 4;
			}
			$('.label_1').html(fish_val_1);
			$('.label_2').html(fish_val_2);
			$('.label_3').html(fish_val_3);
			$('.label_4').html(fish_val_4);
			$('.fish_'+highest_index).addClass('class_1');

			$(".draggable").draggable({
                containment: "body",
                revert: "invalid",
                appendTo: "body",
                helper: "clone",
                zindex: 1000,
                start: function(event, ui) {
                	$(this).css({
                        "opacity": "0"
                    });
                    $(ui.helper).addClass("ui-draggable-helper");
                    $(ui.helper).addClass("no_animation");
                },
                stop: function(event, ui) {
                	$(this).css({
                        "opacity": "1"
                    });
                    $(ui.helper).removeClass("ui-draggable-helper");
 					$(ui.helper).removeClass("no_animation");
                }
            });

			$('.drop_class_1').droppable({
                // accept : ".class_1",
                over: function(event, ui) {
                	$('.fish_boat').attr('src', imgpath + croc_images[1]);
                },
                out: function(event, ui) {
                	(croc==10)?$('.fish_boat').attr('src', imgpath + croc_images[2]):$('.fish_boat').attr('src', imgpath + croc_images[0]);
                },
                drop: function(event, ui) {
                    if (ui.draggable.hasClass("class_1")) {
                    	score++;
                    	$('.score_data').html(score);
                    	sound_player(sound_correct);
                    	croc = 2;
                        templateCaller();
                    } else {
                    	croc = 3;
                        sound_player(sound_incorrect);
                       	templateCaller();
                    }
                }
            });
			$('.playon').click(function(){
				$("#activity-page-exercise-tab").find("button").trigger("click");
				$('.cloudy_text').animate({'left': '100%'}, 1000, function(){
					$('.cloudy_text').fadeOut(100, function(){
            			// ole.footerNotificationHandler.lessonEndSetNotification();
					});
				});
			});
			$('.gomenu').click(function(){
				$("#activity-page-menu-container").find("img").trigger("click");
			});
			break;

		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

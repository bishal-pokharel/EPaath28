var soundAsset = $ref+"/sound/"+$lang +"/";;
var soundAsset_numbers = $ref+"/audio_numbers/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[


	// slide1
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bluish_bg",
			speechbox:[{
				speechbox:'sp-1',
				imgid:'dialogue',
				textdata:data.string.play1text1,
				textclass:'text_inside_box'
			}],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "squirrel",
									imgid: "squirrelpng",
									imgsrc: "",
							}]
			}]
	},

	// slide2
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "cream_bg",
			uppertextblock:[{
				textdata:data.string.play1text2,
				textclass:'clickme'
			}],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "handicon",
									imgid: "handicon",
									imgsrc: "",
							}]
			}],
			textdata:data.string.p1text49,
			textclass:'top_text',
			left1:data.string.p1text29,
			left2:data.string.p1text30,
			left3:data.string.p1text31,
			left4:data.string.p1text32,
			left5:data.string.p1text33,
			left6:data.string.p1text34,
			left7:data.string.p1text35,
			left8:data.string.p1text36,
			left9:data.string.p1text37,
			left10:data.string.p1text38,
			right1:data.string.p1text39,
			right2:data.string.p1text40,
			right3:data.string.p1text41,
			right4:data.string.p1text42,
			right5:data.string.p1text43,
			right6:data.string.p1text44,
			right7:data.string.p1text45,
			right8:data.string.p1text46,
			right9:data.string.p1text47,
			right10:data.string.p1text48,
			table_div:[{
			}],

			question_div:[{
				textclass:'options1',
				textdata:data.string.play1text3
			},{
				textclass:'options2',
				textdata:data.string.play1text4
			},{
				textclass:'options3',
				textdata:data.string.play1text5
			},{
				textclass:'statement',
				textdata:data.string.play1text7
			},{
				textclass:'question',
				textdata:data.string.play1text6
			}]
	},


	// slide3
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "cream_bg",
			uppertextblock:[{
				textdata:data.string.play1text2,
				textclass:'clickme'
			}],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "handicon",
									imgid: "handicon",
									imgsrc: "",
							}]
			}],
			textdata:data.string.p1text49,
			textclass:'top_text',
			left1:data.string.p1text29,
			left2:data.string.p1text30,
			left3:data.string.p1text31,
			left4:data.string.p1text32,
			left5:data.string.p1text33,
			left6:data.string.p1text34,
			left7:data.string.p1text35,
			left8:data.string.p1text36,
			left9:data.string.p1text37,
			left10:data.string.p1text38,
			right1:data.string.p1text39,
			right2:data.string.p1text40,
			right3:data.string.p1text41,
			right4:data.string.p1text42,
			right5:data.string.p1text43,
			right6:data.string.p1text44,
			right7:data.string.p1text45,
			right8:data.string.p1text46,
			right9:data.string.p1text47,
			right10:data.string.p1text48,
			table_div:[{
			}],

			question_div:[{
				textclass:'options1',
				textdata:data.string.play1text3
			},{
				textclass:'options2',
				textdata:data.string.play1text4
			},{
				textclass:'options3',
				textdata:data.string.play1text5
			},{
				textclass:'statement',
				textdata:data.string.play1text7
			},{
				textclass:'question',
				textdata:data.string.play1text6
			}]
	},

	// slide4
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "cream_bg",
			uppertextblock:[{
				textdata:data.string.play1text2,
				textclass:'clickme'
			}],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "handicon",
									imgid: "handicon",
									imgsrc: "",
							}]
			}],
			textdata:data.string.p1text49,
			textclass:'top_text',
			left1:data.string.p1text29,
			left2:data.string.p1text30,
			left3:data.string.p1text31,
			left4:data.string.p1text32,
			left5:data.string.p1text33,
			left6:data.string.p1text34,
			left7:data.string.p1text35,
			left8:data.string.p1text36,
			left9:data.string.p1text37,
			left10:data.string.p1text38,
			right1:data.string.p1text39,
			right2:data.string.p1text40,
			right3:data.string.p1text41,
			right4:data.string.p1text42,
			right5:data.string.p1text43,
			right6:data.string.p1text44,
			right7:data.string.p1text45,
			right8:data.string.p1text46,
			right9:data.string.p1text47,
			right10:data.string.p1text48,
			table_div:[{
			}],

			question_div:[{
				textclass:'options1',
				textdata:data.string.play1text3
			},{
				textclass:'options2',
				textdata:data.string.play1text4
			},{
				textclass:'options3',
				textdata:data.string.play1text5
			},{
				textclass:'statement',
				textdata:data.string.play1text7
			},{
				textclass:'question',
				textdata:data.string.play1text6
			}]
	},

	// slide5
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "cream_bg",
			uppertextblock:[{
				textdata:data.string.play1text2,
				textclass:'clickme'
			}],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "handicon",
									imgid: "handicon",
									imgsrc: "",
							}]
			}],
			textdata:data.string.p1text49,
			textclass:'top_text',
			left1:data.string.p1text29,
			left2:data.string.p1text30,
			left3:data.string.p1text31,
			left4:data.string.p1text32,
			left5:data.string.p1text33,
			left6:data.string.p1text34,
			left7:data.string.p1text35,
			left8:data.string.p1text36,
			left9:data.string.p1text37,
			left10:data.string.p1text38,
			right1:data.string.p1text39,
			right2:data.string.p1text40,
			right3:data.string.p1text41,
			right4:data.string.p1text42,
			right5:data.string.p1text43,
			right6:data.string.p1text44,
			right7:data.string.p1text45,
			right8:data.string.p1text46,
			right9:data.string.p1text47,
			right10:data.string.p1text48,
			table_div:[{
			}],

			question_div:[{
				textclass:'options1',
				textdata:data.string.play1text3
			},{
				textclass:'options2',
				textdata:data.string.play1text4
			},{
				textclass:'options3',
				textdata:data.string.play1text5
			},{
				textclass:'statement',
				textdata:data.string.play1text7
			},{
				textclass:'question',
				textdata:data.string.play1text6
			}]
	},

	// slide6
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "cream_bg",
			uppertextblock:[{
				textdata:data.string.play1text2,
				textclass:'clickme'
			}],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "handicon",
									imgid: "handicon",
									imgsrc: "",
							}]
			}],
			textdata:data.string.p1text49,
			textclass:'top_text',
			left1:data.string.p1text29,
			left2:data.string.p1text30,
			left3:data.string.p1text31,
			left4:data.string.p1text32,
			left5:data.string.p1text33,
			left6:data.string.p1text34,
			left7:data.string.p1text35,
			left8:data.string.p1text36,
			left9:data.string.p1text37,
			left10:data.string.p1text38,
			right1:data.string.p1text39,
			right2:data.string.p1text40,
			right3:data.string.p1text41,
			right4:data.string.p1text42,
			right5:data.string.p1text43,
			right6:data.string.p1text44,
			right7:data.string.p1text45,
			right8:data.string.p1text46,
			right9:data.string.p1text47,
			right10:data.string.p1text48,
			table_div:[{
			}],

			question_div:[{
				textclass:'options1',
				textdata:data.string.play1text3
			},{
				textclass:'options2',
				textdata:data.string.play1text4
			},{
				textclass:'options3',
				textdata:data.string.play1text5
			},{
				textclass:'statement',
				textdata:data.string.play1text7
			},{
				textclass:'question',
				textdata:data.string.play1text6
			}]
	},




];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var numbertemplate = new NumberTemplate();
	numbertemplate.init($total_page-1);
	var clickcount = 0;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrelpng", src: imgpath+"squirrel-walk-new-.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialogue", src: imgpath+'dialogue.png', type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: imgpath+'hand-icon-1.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "croc", src: imgpath+'croc-and-congas.png', type: createjs.AbstractLoader.IMAGE},
			{id: "croc-gif", src: imgpath+'croc-and-congas01.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "leopard", src: imgpath+'snow-leopardess-on-sax01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "leopard-gif", src: imgpath+'snow-leopardess-on-sax01.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "panda", src: imgpath+'red-panda-on-recorder.png', type: createjs.AbstractLoader.IMAGE},
			{id: "panda-gif", src: imgpath+'red-panda-on-recorder.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+'rhino-on-keyboard.png', type: createjs.AbstractLoader.IMAGE},
			{id: "rhino-gif", src: imgpath+'rhino-on-keyboard.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "sundari", src: imgpath+'sundari-maracas01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "sundari-gif", src: imgpath+'sundari-maracas01.gif', type: createjs.AbstractLoader.IMAGE},
			//textboxes

			{id: "dics-1", src: imgpath+'dics01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-2", src: imgpath+'dics02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-3", src: imgpath+'dics03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-4", src: imgpath+'dics04.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-5", src: imgpath+'dics05.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-6", src: imgpath+'dics06.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-7", src: imgpath+'dics07.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-8", src: imgpath+'dics08.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-9", src: imgpath+'dics09.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-10", src: imgpath+'dics10.png', type: createjs.AbstractLoader.IMAGE},
			{id: "white_wrong", src: imgpath+'white_wrong.png', type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange

			{id: "areuready", src: soundAsset+"areuready.ogg"},
			{id: "click_on_the_correct_answer", src: soundAsset+"click_on_the_correct_answer.ogg"},
			// {id: "song_2", src: soundAsset_numbers+"drum.ogg"},
			// {id: "song_3", src: soundAsset_numbers+"flute.ogg"},
			// {id: "song_4", src: soundAsset_numbers+"sax.ogg"},
			// {id: "song_5", src: soundAsset_numbers+"piano.ogg"},
			// {id: "song_6", src: soundAsset_numbers+"maracas.ogg"},
			// {id: "song_7", src: soundAsset_numbers+"songLastPage.ogg"},
			// {id: "wood", src: soundAsset_numbers+"woodhoversound.wav"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		numbertemplate.numberOfQuestions($total_page);


		// inialization for animatetr()
		var current_number=0;
		var counter = 1;
		var wrongcount=0;
		// correct and incorrect actions for all
		$('.options1,.options2,.options3 ').click(function(){
			if($(this).hasClass('correct'))
			{
				var img_id = $('.musician').attr("id");
				console.log(img_id);
				$('.musician').attr('src',preload.getResult(img_id+'-gif').src);
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.question_div ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-34%,26%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$('.options1,.options2,.options3 ').css({"pointer-events":"none"});
				$(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
				createjs.Sound.stop();
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
				if(wrongcount==0){
					numbertemplate.update(true);
				}
			}
			else {
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.question_div ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-34%,26%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				createjs.Sound.stop();
				play_correct_incorrect_sound(0);
				$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
				numbertemplate.update(false);
				wrongcount++;
			}
		});
	// $refreshBtn.hide(0);

		switch(countNext) {
			case 0:
				$('.ex-number-template-score').hide(0);
				sound_player_nav('areuready');
				// nav_button_controls;
				break;
			case 1:
				// $nextBtn.show(0);
				// $prevBtn.hide(0);
				if(clickcount==0){
					sound_player('clickme');
				}
				clickcount++;
				$('.ex-number-template-score').show(0);
				multiplication_table_generator('6');

				$('.question_div').hide(0);
				$('.musician').attr('src',preload.getResult('croc').src).attr('id','croc');
				$('.crossbutton').attr('src',preload.getResult('white_wrong').src);
				$('.crossbutton').click(function(){
					// $('.clickme,.main_table_div').removeClass('blur_it').addClass('unblur_it');
					$('tr').removeClass('bluish_bg');
					$('.question_div').hide(500);
					// enable click me
					$('.clickme').css('pointer-events','auto');
				});

				// all for question div
				$('.gota-3, .gota-1, .gota-2, .gota-4,.gota-5, .gota-6, .gota-7, .gota-8 ,.gota-9, .gota-10').attr('src',preload.getResult('dics-6').src).hide(0);
				var randomnumber =[1100,1300,1500,1700,1900,2100,2300,2500,2700,2900];
				randomnumber.shufflearray();
				console.log(randomnumber[0]);
				var randomed_optionclass =['options1','options2','options3'];
				randomed_optionclass.shufflearray();
				// till here

				// inialization for animatetr()
				var current_number=0;
				var counter = 1;


				// rolling clickme
				$('.clickme,.handicon').click(function(){
					templateCaller();
					animatetr();
					setTimeout(function(){
						sound_player('click_on_the_correct_answer');
					},randomnumber[0]+200);
					// disable click me
					$('.clickme').css('pointer-events','none');
				});
				break;

				case 2:
					// $nextBtn.show(0);
					// $prevBtn.hide(0);
					multiplication_table_generator('7');

					$('.question_div').hide(0);
					$('.musician').attr('src',preload.getResult('panda').src).attr('id','panda');
					$('.crossbutton').attr('src',preload.getResult('white_wrong').src);
					$('.crossbutton').click(function(){
						// $('.clickme,.main_table_div').removeClass('blur_it').addClass('unblur_it');
						$('tr').removeClass('bluish_bg');
						$('.question_div').hide(500);
						// enable click me
						$('.clickme').css('pointer-events','auto');
					});

					// all for question div
					$('.gota-3, .gota-1, .gota-2, .gota-4,.gota-5, .gota-6, .gota-7, .gota-8 ,.gota-9, .gota-10').attr('src',preload.getResult('dics-7').src).hide(0);
					var randomnumber =[1100,1300,1500,1700,1900,2100,2300,2500,2700,2900];
					randomnumber.shufflearray();
					console.log(randomnumber[0]);
					var randomed_optionclass =['options1','options2','options3'];
					randomed_optionclass.shufflearray();
					// till here

					// rolling clickme
					$('.clickme,.handicon').click(function(){
						templateCaller();
						animatetr();
						// disable click me
						$('.clickme').css('pointer-events','none');
					});
					break;



					case 3:
						// $nextBtn.show(0);
						// $prevBtn.hide(0);
						multiplication_table_generator('8');

						$('.question_div').hide(0);
						$('.musician').attr('src',preload.getResult('leopard').src).attr('id','leopard');
						$('.crossbutton').attr('src',preload.getResult('white_wrong').src);
						$('.crossbutton').click(function(){
							// $('.clickme,.main_table_div').removeClass('blur_it').addClass('unblur_it');
							$('tr').removeClass('bluish_bg');
							$('.question_div').hide(500);
							// enable click me
							$('.clickme').css('pointer-events','auto');
						});

						// all for question div
						$('.gota-3, .gota-1, .gota-2, .gota-4,.gota-5, .gota-6, .gota-7, .gota-8 ,.gota-9, .gota-10').attr('src',preload.getResult('dics-8').src).hide(0);
						var randomnumber =[1100,1300,1500,1700,1900,2100,2300,2500,2700,2900];
						randomnumber.shufflearray();
						console.log(randomnumber[0]);
						var randomed_optionclass =['options1','options2','options3'];
						randomed_optionclass.shufflearray();
						// till here

						// rolling clickme
						$('.clickme,.handicon').click(function(){
							templateCaller();
							animatetr();
							// disable click me
							$('.clickme').css('pointer-events','none');
						});
						break;

						case 4:
							// $nextBtn.show(0);
							// $prevBtn.hide(0);
							multiplication_table_generator('9');

							$('.question_div').hide(0);
							$('.musician').attr('src',preload.getResult('rhino').src).attr('id','rhino');
							$('.crossbutton').attr('src',preload.getResult('white_wrong').src);
							$('.crossbutton').click(function(){
								// $('.clickme,.main_table_div').removeClass('blur_it').addClass('unblur_it');
								$('tr').removeClass('bluish_bg');
								$('.question_div').hide(500);
								// enable click me
								$('.clickme').css('pointer-events','auto');
							});

							// all for question div
							$('.gota-3, .gota-1, .gota-2, .gota-4,.gota-5, .gota-6, .gota-7, .gota-8 ,.gota-9, .gota-10').attr('src',preload.getResult('dics-9').src).hide(0);
							var randomnumber =[1100,1300,1500,1700,1900,2100,2300,2500,2700,2900];
							randomnumber.shufflearray();
							console.log(randomnumber[0]);
							var randomed_optionclass =['options1','options2','options3'];
							randomed_optionclass.shufflearray();
							// till here

							// rolling clickme
							$('.clickme,.handicon').click(function(){
								templateCaller();
								animatetr();
								// disable click me
								$('.clickme').css('pointer-events','none');
							});
							break;


							case 5:
								// $nextBtn.show(0);
								// $prevBtn.hide(0);
								multiplication_table_generator('10');

								$('.question_div').hide(0);
								$('.musician').attr('src',preload.getResult('sundari').src).attr('id','sundari');
								$('.crossbutton').attr('src',preload.getResult('white_wrong').src);
								$('.crossbutton').click(function(){
									$('tr').removeClass('bluish_bg');
									$('.question_div').hide(500);
									// enable click me
									$('.clickme').css('pointer-events','auto');
								});

								// all for question div
								$('.gota-3, .gota-1, .gota-2, .gota-4,.gota-5, .gota-6, .gota-7, .gota-8 ,.gota-9, .gota-10').attr('src',preload.getResult('dics-10').src).hide(0);
								var randomnumber =[1100,1300,1500,1700,1900,2100,2300,2500,2700,2900];
								randomnumber.shufflearray();
								console.log(randomnumber[0]);
								var randomed_optionclass =['options1','options2','options3'];
								randomed_optionclass.shufflearray();
								// till here

								// rolling clickme
								$('.clickme,.handicon').click(function(){
									templateCaller();
									animatetr();
									// disable click me
									$('.clickme').css('pointer-events','none');
								});
								break;




		}

		//animatetr() does series of functions during and after row color change animation

		function animatetr(){
				var multiplication_number = countNext+5;
				$('tr:nth-child('+counter+')').animate('bluish_bg',100,function(){
					$('tr:nth-child('+counter+')').addClass('bluish_bg');

					$('tr:nth-child('+counter+')').animate('bluish_bg',100,function(){
						$('tr:nth-child('+counter+')').removeClass('bluish_bg');
						counter++;
						if(counter<=10){
							animatetr();
						}
						if(counter==11){
							counter=1;
							animatetr();
						}

					});
				});

				setTimeout(function(){
					// $('.clickme,.main_table_div').removeClass('blur_it unblur_it').addClass('blur_it');
					// colors currently rolled row
					$('tr:nth-child('+counter+')').addClass('bluish_bg');
					// finds currently rolled row
					var current_number = $('.bluish_bg > td:nth-child(3) span').attr('data-answer');

					//shows gotas
					for (var i = 1; i <=current_number; i++) {
						$('.gota-'+i).show(100);
					}


					// creating options
					console.log(1);
					$('.'+randomed_optionclass[0]).html(multiplication_number*current_number).addClass('correct');
					$('.'+randomed_optionclass[1]).html(multiplication_number*current_number+2);
					$('.'+randomed_optionclass[2]).html(multiplication_number*current_number+4);

					$('.statement').html(eval('data.string.play1text7_'+current_number.toString()));

					// defines which multiplication table is this?
					$('.number').html(multiplication_number);

					$('.rownumber').html(current_number);

					//shows question div
					$('.question_div').show(500);
					//  loop breaker
					counter=12;

				},randomnumber[0]);
			}

		// multiplication table generator
		function multiplication_table_generator(numberselect){
				$('.insideimage').attr('src',preload.getResult('dics-'+numberselect).src);
				$('.number').html(numberselect);
				$('.productnumber1').html(parseInt(numberselect)*1);
				$('.productnumber2').html(parseInt(numberselect)*2);
				$('.productnumber3').html(parseInt(numberselect)*3);
				$('.productnumber4').html(parseInt(numberselect)*4);
				$('.productnumber5').html(parseInt(numberselect)*5);
				$('.productnumber6').html(parseInt(numberselect)*6);
				$('.productnumber7').html(parseInt(numberselect)*7);
				$('.productnumber8').html(parseInt(numberselect)*8);
				$('.productnumber9').html(parseInt(numberselect)*9);
				$('.productnumber10').html(parseInt(numberselect)*10);
		}





	}
	function rand_generator(limit){
		var randNum = Math.floor((Math.random() * limit) + 1);
		return randNum;
	}


	function nav_button_controls(delay_ms){
		$nextBtn.show(0);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		if(countNext==0){

		}
		else{
			numbertemplate.gotoNext();
		}
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

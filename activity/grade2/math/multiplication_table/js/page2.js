var soundAsset = $ref+"/sound/"+$lang+"/";
var imgpath = $ref+"/images/";


var content=[

	// slide0
	{
		speechbox:[{
			speechbox:'sp-2',
			imgid:'dialogue',
			textdata:data.string.p1text28,
			textclass:'text_inside_box1'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'ducklin-2',
				imgclass:'duck'
			},
			{
				imgid:'bg-1',
				imgclass:'bg_full'
			}]
		}],
		uppertextblock:[{
			textclass:'number-6 number-1 numbers',
			textdata:data.string.p1text23
		},{
			textclass:'number-7 number-2 numbers',
			textdata:data.string.p1text24
		},{
			textclass:'number-8 number-3 numbers',
			textdata:data.string.p1text25
		},{
			textclass:'number-9 number-4 numbers',
			textdata:data.string.p1text26
		},{
			textclass:'number-10 number-5 numbers',
			textdata:data.string.p1text27
		}],
		textdata:data.string.p1text49,
		textclass:'top_text',
		left1:data.string.p1text29,
		left2:data.string.p1text30,
		left3:data.string.p1text31,
		left4:data.string.p1text32,
		left5:data.string.p1text33,
		left6:data.string.p1text34,
		left7:data.string.p1text35,
		left8:data.string.p1text36,
		left9:data.string.p1text37,
		left10:data.string.p1text38,
		right1:data.string.p1text39,
		right2:data.string.p1text40,
		right3:data.string.p1text41,
		right4:data.string.p1text42,
		right5:data.string.p1text43,
		right6:data.string.p1text44,
		right7:data.string.p1text45,
		right8:data.string.p1text46,
		right9:data.string.p1text47,
		right10:data.string.p1text48,
		table_div:[{
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg01a.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "bg-2", src: imgpath+'bg02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-1", src: imgpath+'dics01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-2", src: imgpath+'dics02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-3", src: imgpath+'dics03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-4", src: imgpath+'dics04.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-5", src: imgpath+'dics05.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-6", src: imgpath+'dics06.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-7", src: imgpath+'dics07.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-8", src: imgpath+'dics08.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-9", src: imgpath+'dics09.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-10", src: imgpath+'dics10.png', type: createjs.AbstractLoader.IMAGE},
			{id: "ducklin-1", src: imgpath+'ducklin.png', type: createjs.AbstractLoader.IMAGE},
			{id: "ducklin-2", src: imgpath+'ducklin01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dialogue", src: imgpath+'dialogue.png', type: createjs.AbstractLoader.IMAGE},
			{id: "crossbutton", src: imgpath+'white_wrong.png', type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "instruction", src: soundAsset+"s2_p1.ogg"},
			{id: "table_of_6", src: soundAsset+"table_of_6.ogg"},
			{id: "table_of_7", src: soundAsset+"table_of_7.ogg"},
			{id: "table_of_8", src: soundAsset+"table_of_8.ogg"},
			{id: "table_of_9", src: soundAsset+"table_of_9.ogg"},
			{id: "table_of_10", src: soundAsset+"table_of_10.ogg"},

			{id: "6_1", src: soundAsset+"6_one_time.ogg"},
			{id: "6_2", src: soundAsset+"6_two_time.ogg"},
			{id: "6_3", src: soundAsset+"6_three_time.ogg"},
			{id: "6_4", src: soundAsset+"6_four_time.ogg"},
			{id: "6_5", src: soundAsset+"6_five_time.ogg"},
			{id: "6_6", src: soundAsset+"6_six_time.ogg"},
			{id: "6_7", src: soundAsset+"6_seven_time.ogg"},
			{id: "6_8", src: soundAsset+"6_eight_time.ogg"},
			{id: "6_9", src: soundAsset+"6_nine_time.ogg"},
			{id: "6_10", src: soundAsset+"6_ten_time.ogg"},

			{id: "7_1", src: soundAsset+"7_one_time.ogg"},
			{id: "7_2", src: soundAsset+"7_two_time.ogg"},
			{id: "7_3", src: soundAsset+"7_three_time.ogg"},
			{id: "7_4", src: soundAsset+"7_four_time.ogg"},
			{id: "7_5", src: soundAsset+"7_five_time.ogg"},
			{id: "7_6", src: soundAsset+"7_six_time.ogg"},
			{id: "7_7", src: soundAsset+"7_seven_time.ogg"},
			{id: "7_8", src: soundAsset+"7_eight_time.ogg"},
			{id: "7_9", src: soundAsset+"7_nine_time.ogg"},
			{id: "7_10", src: soundAsset+"7_ten_time.ogg"},

			{id: "8_1", src: soundAsset+"8_one_time.ogg"},
			{id: "8_2", src: soundAsset+"8_two_time.ogg"},
			{id: "8_3", src: soundAsset+"8_three_time.ogg"},
			{id: "8_4", src: soundAsset+"8_four_time.ogg"},
			{id: "8_5", src: soundAsset+"8_five_time.ogg"},
			{id: "8_6", src: soundAsset+"8_six_time.ogg"},
			{id: "8_7", src: soundAsset+"8_seven_time.ogg"},
			{id: "8_8", src: soundAsset+"8_eight_time.ogg"},
			{id: "8_9", src: soundAsset+"8_nine_time.ogg"},
			{id: "8_10", src: soundAsset+"8_ten_time.ogg"},

			{id: "9_1", src: soundAsset+"9_one_time.ogg"},
			{id: "9_2", src: soundAsset+"9_two_time.ogg"},
			{id: "9_3", src: soundAsset+"9_three_time.ogg"},
			{id: "9_4", src: soundAsset+"9_four_time.ogg"},
			{id: "9_5", src: soundAsset+"9_five_time.ogg"},
			{id: "9_6", src: soundAsset+"9_six_time.ogg"},
			{id: "9_7", src: soundAsset+"9_seven_time.ogg"},
			{id: "9_8", src: soundAsset+"9_eight_time.ogg"},
			{id: "9_9", src: soundAsset+"9_nine_time.ogg"},
			{id: "9_10", src: soundAsset+"9_ten_time.ogg"},

			{id: "10_1", src: soundAsset+"10_one_time.ogg"},
			{id: "10_2", src: soundAsset+"10_two_time.ogg"},
			{id: "10_3", src: soundAsset+"10_three_time.ogg"},
			{id: "10_4", src: soundAsset+"10_four_time.ogg"},
			{id: "10_5", src: soundAsset+"10_five_time.ogg"},
			{id: "10_6", src: soundAsset+"10_six_time.ogg"},
			{id: "10_7", src: soundAsset+"10_seven_time.ogg"},
			{id: "10_8", src: soundAsset+"10_eight_time.ogg"},
			{id: "10_9", src: soundAsset+"10_nine_time.ogg"},
			{id: "10_10", src: soundAsset+"10_ten_time.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				sound_player('instruction');
				$('.main_table_div').hide(0);
				var countclick=0;
				$('.crossbutton').attr('src',preload.getResult('crossbutton').src).click(function(){
					createjs.Sound.stop();
					$('.main_table_div').hide(500);
					$('tr').css('pointer-events','auto');
				});
				multiplication_table_generator('6');
				multiplication_table_generator('7');
				multiplication_table_generator('8');
				multiplication_table_generator('9');
				multiplication_table_generator('10');

				// multiplication table generator
				function multiplication_table_generator(numberselect){
					$('.number-'+numberselect).click(function(){
						sound_player('table_of_'+numberselect);
						countclick++;
						if(countclick==2){
							ole.footerNotificationHandler.lessonEndSetNotification();
						}
						$('.insideimage').attr('src',preload.getResult('dics-'+numberselect).src);
						$('.number').html(numberselect);
						$('.productnumber1').html(parseInt(numberselect)*1);
						$('.productnumber2').html(parseInt(numberselect)*2);
						$('.productnumber3').html(parseInt(numberselect)*3);
						$('.productnumber4').html(parseInt(numberselect)*4);
						$('.productnumber5').html(parseInt(numberselect)*5);
						$('.productnumber6').html(parseInt(numberselect)*6);
						$('.productnumber7').html(parseInt(numberselect)*7);
						$('.productnumber8').html(parseInt(numberselect)*8);
						$('.productnumber9').html(parseInt(numberselect)*9);
						$('.productnumber10').html(parseInt(numberselect)*10);
						$('.main_table_div').show(500);
						sound_click(1);
						sound_click(2);
						sound_click(3);
						sound_click(4);
						sound_click(5);
						sound_click(6);
						sound_click(7);
						sound_click(8);
						sound_click(9);
						sound_click(10);

						function sound_click(tablerownumber){
							$('tr:nth-child('+tablerownumber+')').click(function(){
								$('tr').css('pointer-events','none');
								console.log(numberselect.toString()+'_'+tablerownumber.toString());
								createjs.Sound.stop();


								// uncommend following line if audio is provided and change above oggs
								// current_sound = createjs.Sound.play('sound-'+numberselect.toString()+tablerownumber.toString());


								current_sound = createjs.Sound.play(numberselect.toString()+'_'+tablerownumber.toString());
								current_sound.play();
								current_sound.on("complete", function(){
										$('tr').css('pointer-events','auto');
									});
							});
						}

					});
				}


				break;
				default:
				nav_button_controls(100);
				break;


		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

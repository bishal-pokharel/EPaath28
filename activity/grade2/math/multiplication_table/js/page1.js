var soundAsset = $ref+"/sound/"+$lang+'/';
var imgpath = $ref+"/images/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'chaptertitle fadein'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'coverpage',
				imgclass:'bg_full'
			}]
		}]
	},

	// slide1
	{
		speechbox:[{
			speechbox:'sp-1',
			imgid:'dialogue',
			textdata:data.string.p1text1,
			textclass:'text_inside_box'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'ducklin-2',
				imgclass:'duck'
			},
			{
				imgid:'bg-1',
				imgclass:'bg_full'
			}]
		}]
	},

	// slide2
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text3,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text4,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide3
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text5,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text6,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide4
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text7,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text8,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-3 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide5
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text9,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text10,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-3'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-4 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide6
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text11,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text12,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-3'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-4'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-5 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide7
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text13,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text14,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-3'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-4'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-5'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-6 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide8
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text15,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text16,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-3'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-4'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-5'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-6'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-7 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide9
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text17,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text18,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-3'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-4'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-5'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-6'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-7'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-8 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide10
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text19,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text20,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-3'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-4'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-5'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-6'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-7'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-8'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-9 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},

	// slide11
	{
	  uppertextblock:[{
	    textdata:data.string.p1text2,
	    textclass:'top_text'
	  },{
	    textdata:data.string.p1text21,
	    textclass:'left_text'
	  },{
	    textdata:data.string.p1text22,
	    textclass:'right_text'
	  },{
	    textclass:'line'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'ducklin-1',
	      imgclass:'duck2'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-1'
	    },{
	      imgid:'dics-6',
	      imgclass:'disc-2'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-3'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-4'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-5'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-6'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-7'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-8'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-9'
	    },
	    {
	      imgid:'dics-6',
	      imgclass:'disc-10 fadein'
	    },
	    {
	      imgid:'bg-2',
	      imgclass:'bg_full'
	    }]
	  }]
	},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg01a.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "bg-2", src: imgpath+'bg02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-1", src: imgpath+'dics01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-2", src: imgpath+'dics02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-3", src: imgpath+'dics03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-4", src: imgpath+'dics04.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-5", src: imgpath+'dics05.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-6", src: imgpath+'dics06.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-7", src: imgpath+'dics07.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-8", src: imgpath+'dics08.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-9", src: imgpath+'dics09.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dics-10", src: imgpath+'dics10.png', type: createjs.AbstractLoader.IMAGE},
			{id: "ducklin-1", src: imgpath+'ducklin.png', type: createjs.AbstractLoader.IMAGE},
			{id: "ducklin-2", src: imgpath+'ducklin01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dialogue", src: imgpath+'dialogue.png', type: createjs.AbstractLoader.IMAGE},
			{id: "crossbutton", src: imgpath+'white_wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+'coverpage.png', type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_3_1", src: soundAsset+"s1_p3_1.ogg"},
			{id: "sound_3_2", src: soundAsset+"s1_p3_2.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		if(countNext==2){
			$('.left_text,.right_text,.line,.disc-1').hide(0);
			createjs.Sound.stop();
			current_sound1 = createjs.Sound.play('sound_3_1');
			current_sound1.play();
			current_sound1.on("complete", function(){
				$('.left_text,.right_text,.line,.disc-1').fadeIn(1000);
				sound_player_nav('sound_3_2');
			});
		}
		else{
			sound_player_nav('sound_'+(countNext+1));
		}

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content;
content = [
  //1st page
  {
    contentblockadditionalclass : 'background_image',
        uppertextblockadditionalclass: "upperblock animated fadeInUp zindex",
        uppertextblock: [{
            textclass: "introText",
            textdata: data.string.introtitle
        }],

    },

    //2nd page
    {
      contentblockadditionalclass : 'bgbg',
      uppertextblockadditionalclass: "upperblock2 animated fadeInUp ",
      uppertextblock: [{
          textclass: "puzzleHelp",
          textdata: data.string.puzzle1help
      }],
        inputfields: true,
        inputbox: [{
            inputtypeclass: "hintButton",
            inputtype: "button",
            inputtypevalue: data.string.hint,
            inputid: "hint"
        }],
        lowertextblockadditionalclass: "lowerblock",
        lowertextblock: [{
            textclass: "afterWin",
            textdata: data.string.success,
            datahighlightflag:true,
            datahighlightcustomclass :'congrats'
             }],
        hascanvasblock: true,
        onLoad: "init();",
        canvasid: "canvas",
        imageblock: [{
            imagestoshow: [{
                imgclass: "yakimg",
                imgsrc: imgpath + "yakbg.jpg"
            },
            {
                imgclass: "afterWin posi",
                imgsrc: imgpath + "Squirrel-Keep-it-up.png"
            }]
        }],


    },

    //3rdpage
    {
      contentblockadditionalclass : 'bgbg',
      uppertextblockadditionalclass: "upperblock2 animated fadeInUp",
      uppertextblock: [{
          textclass: "puzzleHelp",
          textdata: data.string.puzzle2help
      }],
        inputfields: true,
        inputbox: [{
            inputtypeclass: "hintButton",
            inputtype: "button",
            inputtypevalue: data.string.hint,
            inputid: "hint"
        }],
        lowertextblockadditionalclass: "lowerblock",
        lowertextblock: [{
            textclass: "afterWin",
            datahighlightflag:true,
            datahighlightcustomclass :'congrats',
            textdata: data.string.success
        }],
        hascanvasblock: true,
        onLoad: "init();",
        canvasid: "canvas",
        imageblock: [{
            imagestoshow: [{
                imgclass: "yakimg",
                imgsrc: imgpath + "danfe_with_number1.jpg"
            },
            {
                imgclass: "afterWin posi",
                imgsrc: imgpath + "Squirrel-Keep-it-up.png"
            }]
        }]
    },

    {
      contentblockadditionalclass : 'bgbg',
      uppertextblockadditionalclass: "upperblock2 animated fadeInUp",
      uppertextblock: [{
          textclass: "puzzleHelp",
          textdata: data.string.puzzle3help
      }],
        inputfields: true,
        inputbox: [{
            inputtypeclass: "hintButton",
            inputtype: "button",
            inputtypevalue: data.string.hint,
            inputid: "hint"
        }],
        lowertextblockadditionalclass: "lowerblock",
        lowertextblock: [{
            textclass: "afterWin",
            datahighlightflag:true,
            datahighlightcustomclass :'congrats',
            textdata: data.string.success
        }],
        hascanvasblock: true,
        onLoad: "init();",
        canvasid: "canvas",
        imageblock: [{
            imagestoshow: [{
                imgclass: "yakimg",
                imgsrc: imgpath + "butterfly_with_number1.jpg"
            },
            {
                imgclass: "afterWin posi",
                imgsrc: imgpath + "Squirrel-Keep-it-up.png"
            }]
        }]
    }
];


// }

var puzzleWin;

$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");
    var preload;
    $(window).resize(function() {
        recalculateHeightWidth();
    });

    function recalculateHeightWidth() {

    	// added by sandy
        var heightresized = $('.canvasblock').height();
        var widthresized = $('.canvasblock').width();
        $("#canvas").css({
            "width": widthresized,
            "height": heightresized,
            "left": 0,
            "top": 0,
        });
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");

    var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
	var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);
     init1();
    function init1() {
        //specify type otherwise it will load assests as XHR
        manifest = [

          // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    //controls the navigational state of the program
    function navigationController() {
        if (countNext == 0 && total_page != 1) {
            $nextBtn.show(0);
        } else if (countNext > 0 && countNext < (total_page - 1)) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == total_page - 1) {
            $prevBtn.show(0);
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
    	recalculateHeightWidth();
        var count;
        switch (countNext) {
            case 0:
            	sound_player("sound_1",true);
                $nextBtn.hide(0);

                break;
            case 1:
                sound_player("sound_2",false);
                count = countNext;
                $nextBtn.hide(0);
                var canvasimg = imgpath + "yakbg.jpg";
                CanvasPuzzle(canvasimg, 5, 3, count);
                $("#hint").bind("mouseover", function() {
                    $(".yakImg").css({
                        'display': 'block',
                        'z-index': '9999'
                    });
                });

                $("#hint").bind("mouseout", function() {
                    $(".yakImg").css({
                        'display': 'none',
                        'z-index': '9999'
                    });
                });

                if (puzzleWin == true) {
                    $nextBtn.show(0);
                }
                break;

            case 2:
                sound_player("sound_3",false);
                count = countNext;
                $nextBtn.hide(0);
                var canvasimg = imgpath + "danfe_with_number1.jpg";
                CanvasPuzzle(canvasimg, 5, 3, count) ;
                $("#hint").bind("mouseover", function() {
                    $(".yakImg").css({
                        'display': 'block',
                        'z-index': '9999'
                    });
                });

                $("#hint").bind("mouseout", function() {
                    $(".yakImg").css({
                        'display': 'none',
                        'z-index': '9999'
                    });
                });
                break;
            case 3:
                sound_player("sound_4",false);
                count = countNext;
                var canvasimg = imgpath + "butterfly_with_number1.jpg";
                CanvasPuzzle(canvasimg, 5, 4, count);
                $("#hint").bind("mouseover", function() {
                    $(".yakImg").css({
                        'display': 'block',
                        'z-index': '9999'
                    });
                });

                $("#hint").bind("mouseout", function() {
                    $(".yakImg").css({
                        'display': 'none',
                        'z-index': '9999'
                    });
                });
                break;
            default:

        }

    }
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });
    // setTimeout(function(){
    total_page = content.length;
    // }, 250);





    // canvas

    function CanvasPuzzle(imgFor, width, height, counter) {
	    var pageCount = counter;
	    var puzzle_diff_width = width;
	    var puzzle_diff_height = height;
	    var PUZZLE_HOVER_TINT = "#009900";
	    var puzzleWin;
	    var nextBtn = $("#activity-page-next-btn-enabled");
	    var startAfter;
	    var canvas;
	    var stage;
	    var cImg;
	    var pieces;
	    var puzzleWidth;
	    var puzzleHeight;
	    var pieceWidth;
	    var pieceHeight;
	    var currentPiece;
	    var currentDropPiece;
	    var mouse;


		// added by sandy
	    var new_factor;


	    console.log("pageCount" + pageCount);
	    console.log(imgFor);
	    init();
	    //fires the onImage method after the image is loaded
	    function init() {
	        console.log("iniside init function");
	        cImg = new Image();
	        cImg.addEventListener('load', onImage, false);
	        cImg.src = imgFor;
	    }

	    function onImage(e) {
	        pieceWidth = Math.floor(cImg.width / puzzle_diff_width);
	        pieceHeight = Math.floor(cImg.height / puzzle_diff_height);
	        puzzleWidth = pieceWidth * puzzle_diff_width;
	        puzzleHeight = pieceHeight * puzzle_diff_height;


	    	// added by sandy
	        new_factor = $('.canvasblock').width()/puzzleWidth;


	        console.log(pieceWidth);
	        console.log(pieceHeight);
	        setCanvas();
	        initPuzzle();
	    }

	    function setCanvas() {
	        canvas = document.getElementById('canvas');
	        stage = canvas.getContext('2d');
	        canvas.width = puzzleWidth;
	        canvas.height = puzzleHeight;
	        console.log(puzzleWidth);
	        console.log(puzzleHeight);
	        canvas.style.border = "1px solid black";
	    }

	    function initPuzzle() {
	        pieces = [];
	        mouse = {
	            x: 0,
	            y: 0
	        };
	        currentPiece = null;
	        currentDropPiece = null;
	        buildPieces();
	        setTimeout(function() {
	            shufflePuzzle();
	        }, 10);
	    };

	    function initPuzzleAfterWin() {
	    	$('#canvas').click(function(){
	        	templateCaller();
	    	});
	    }

	    // function createTitle(msg){
	    //     stage.fillStyle = "#000000";
	    //     stage.globalAlpha = .4;
	    //     stage.fillRect(100,puzzleHeight - 40, puzzleWidth - 200,40);
	    //     stage.fillStyle = "#FFFFFF";
	    //     stage.globalAlpha = 1;
	    //     stage.textAlign = "center";
	    //     stage.textBaseline = "middle";
	    //     stage.font = "20px Arial";
	    //     stage.fillText(msg,puzzleWidth / 2,puzzleHeight - 20);
	    // }
	    function buildPieces() {
	        var i;
	        var piece;
	        var xPos = 0;
	        var yPos = 0;

	        for (i = 0; i < puzzle_diff_width * puzzle_diff_height; i++) {
	            piece = {};
	            piece.sx = xPos;
	            piece.sy = yPos;
	            pieces.push(piece);
	            xPos += pieceWidth;
	            /**
	            **
	            this is for writing in canvas
	            */
	            if (xPos >= puzzleWidth) {
	                xPos = 0;
	                yPos += pieceHeight;
	            }
	        }

	        if (startAfter) {
	            document.getElementById('canvas').addEventListener("mousedown",shufflePuzzle);
	        }
	    }

	    //this function writes on canvas element
	    // function writeOn(){
	    //   for (var w = 1; w <= 15; w++){
	    //     console.log(w);
	    //   }
	    // }

	    function shufflePuzzle() {
	        pieces = shuffleArray(pieces);
	        stage.clearRect(0, 0, puzzleWidth, puzzleHeight);
	        var i;
	        var piece;
	        var xPos = 0;
	        var yPos = 0;

	        for (i = 0; i < pieces.length; i++) {
	            piece = pieces[i];
	            piece.xPos = xPos;
	            piece.yPos = yPos;
	            stage.drawImage(cImg, piece.sx, piece.sy, pieceWidth, pieceHeight, xPos, yPos, pieceWidth, pieceHeight);
	            stage.strokeRect(xPos, yPos, pieceWidth, pieceHeight);
	            xPos += pieceWidth;
	            if (xPos >= puzzleWidth) {
	                xPos = 0;
	                yPos += pieceHeight;
	            }
	        }
	        document.onmousedown = onPuzzleClick;
	    }

	    function shuffleArray(o) {
	        for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	        return o;
	    }

	    function onPuzzleClick(e) {

	    	// added by sandy
	        mouse.x = e.pageX - $('#canvas').offset().left;
	        mouse.y = e.pageY - $('#canvas').offset().top;

	        // if(e.layerX  >= 480){
	        //   currentPiece.style.left = "480px";
	        // }
	        //
	        // if(e.layerY  >= 800){
	        //   currentPiece.style.left = "802px";
	        // }

	        currentPiece = checkPieceClicked();
	        if (currentPiece != null) {
	            stage.clearRect(currentPiece.xPos, currentPiece.yPos, pieceWidth, pieceHeight);
	            stage.save();
	            stage.globalAlpha = .9;
	            stage.drawImage(cImg, currentPiece.sx, currentPiece.sy, pieceWidth, pieceHeight, mouse.x/new_factor - (pieceWidth / 2), mouse.y/new_factor - (pieceHeight / 2), pieceWidth, pieceHeight);
	            stage.restore();
	            document.onmousemove = updatePuzzle;
	            document.onmouseup = pieceDropped;
	        }
	    }

	    function checkPieceClicked() {
	        var i;
	        var piece;
	        for (i = 0; i < pieces.length; i++) {
	            piece = pieces[i];
	            if (mouse.x < piece.xPos*new_factor || mouse.x > (piece.xPos*new_factor + pieceWidth*new_factor) || mouse.y < piece.yPos*new_factor || mouse.y > (piece.yPos*new_factor + pieceHeight*new_factor)) {
	                //PIECE NOT HIT
	            } else {
	                return piece;
	            }
	        }
	        return null;
	    }

	    function updatePuzzle(e) {
	        currentDropPiece = null;

	    	// added by sandy
	        mouse.x = (e.pageX - $('#canvas').offset().left)/new_factor;
	        mouse.y = (e.pageY - $('#canvas').offset().top)/new_factor;

	        stage.clearRect(0, 0, puzzleWidth, puzzleHeight);
	        var i;
	        var piece;
	        for (i = 0; i < pieces.length; i++) {
	            piece = pieces[i];
	            if (piece == currentPiece) {
	                continue;
	            }
	            stage.drawImage(cImg, piece.sx, piece.sy, pieceWidth, pieceHeight, piece.xPos, piece.yPos, pieceWidth, pieceHeight);
	            stage.strokeRect(piece.xPos, piece.yPos, pieceWidth, pieceHeight);
	            if (currentDropPiece == null) {
	                if (mouse.x < piece.xPos || mouse.x > (piece.xPos + pieceWidth) || mouse.y < piece.yPos || mouse.y > (piece.yPos + pieceHeight)) {
	                    //NOT OVER
	                } else {
	                    currentDropPiece = piece;
	                    stage.save();
	                    stage.globalAlpha = .4;
	                    stage.fillStyle = PUZZLE_HOVER_TINT;
	                    stage.fillRect(currentDropPiece.xPos, currentDropPiece.yPos, pieceWidth, pieceHeight);
	                    stage.restore();
	                }
	            }
	        }
	        stage.save();
	        stage.globalAlpha = .6;
	        stage.drawImage(cImg, currentPiece.sx, currentPiece.sy, pieceWidth, pieceHeight, mouse.x - (pieceWidth / 2), mouse.y - (pieceHeight / 2), pieceWidth, pieceHeight);
	        stage.restore();
	        stage.strokeRect(mouse.x - (pieceWidth / 2), mouse.y - (pieceHeight / 2), pieceWidth, pieceHeight);
	    }

	    function pieceDropped(e) {
	        document.onmousemove = null;
	        document.onmouseup = null;
	        if (currentDropPiece != null) {
	            var tmp = {
	                xPos: currentPiece.xPos,
	                yPos: currentPiece.yPos
	            };
	            currentPiece.xPos = currentDropPiece.xPos;
	            currentPiece.yPos = currentDropPiece.yPos;
	            currentDropPiece.xPos = tmp.xPos;
	            currentDropPiece.yPos = tmp.yPos;
	        }
	        resetPuzzleAndCheckWin();
	    }


	    function resetPuzzleAndCheckWin() {
	    	// added by sandy
	    	currentDropPiece = null;

	        stage.clearRect(0, 0, puzzleWidth, puzzleHeight);
	        var gameWin = true;
	        var i;
	        var piece;
	        for (i = 0; i < pieces.length; i++) {
	            piece = pieces[i];
	            stage.drawImage(cImg, piece.sx, piece.sy, pieceWidth, pieceHeight, piece.xPos, piece.yPos, pieceWidth, pieceHeight);
	            stage.strokeRect(piece.xPos, piece.yPos, pieceWidth, pieceHeight);
	            if (piece.xPos != piece.sx || piece.yPos != piece.sy) {
	                gameWin = false;
	            }
	        }
	        if (gameWin) {
	            setTimeout(gameOver, 500);
	        }
	    }

	    function gameOver() {
	    	sound_player("sound_5",false);
	        document.onmousedown = null;
	        document.onmousemove = null;
	        document.onmouseup = null;
	        stage.drawImage(cImg, 0, 0, puzzleWidth, puzzleHeight, 0, 0, puzzleWidth, puzzleHeight);
	        // createTitle(" Congrats ! Click again to Start the puzzle");
	        $('.afterWin').css({
	            'display': 'block'
	        });

	        startAfter = true;
	        initPuzzleAfterWin();
	        $(".hintButton").hide(0);
	        console.log(pageCount);

	        if(pageCount < 3){
	          nextBtn.show(0);
	        }
	        else if (pageCount == 3) {
	          	ole.footerNotificationHandler.lessonEndSetNotification();
	        }
	    }

	}
    function sound_player(sound_id, navigate) {
    	console.log("sound id is"+sound_id);
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ?  $nextBtn.show(0): "";
        });
    }


	//page 1


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");


            texthighlightstarttag = "<span class = " + stylerulename + " >";

            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content;
content = [{
    uppertextblockadditionalclass: "instructText",
    exercisetxt: data.string.exetxt,
    flexbox: true,
    maxlength: 2,
    uppertextblock: [{
            textclass: "headerText",
            textdata: data.string.p1text1
        }]
}];

$(function() {

    $(window).resize(function() {
        recalculateHeightWidth();
    });

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
    }
    var $board = $(".board");

    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    recalculateHeightWidth();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "ex1_1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    init();
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ?  $nextBtn.show(0): "";
        });
    }


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    //controls the navigational state of the program
    function navigationController() {
        if (countNext == 0 && total_page != 1) {
            $nextBtn.show(0);
        } else if (countNext > 0 && countNext < (total_page - 1)) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == total_page - 1) {
            $prevBtn.show(0);
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);
       $('.exenextbtn').hide(0);
        countNext==0?sound_player("sound_1",false):"";
        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        var $displayRow = $(".row1");
        var ind;
        switch (countNext) {
            case 0:
                duplicateRow(7, 9);



                break;
            default:

        }
        $('.exenextbtn').click(function(){
          $("#titleToolbar").trigger("click");
        });
    }

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });
    // setTimeout(function(){
    total_page = content.length;
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

function duplicateRow(row, col) {
    for (var i = 1; i < row; i++) {
        var div = document.createElement('div');
        document.querySelector('.hello').appendChild(div);
        div.className += "allrows ";
        div.className += "row" + i;

        for (var j = 1; j < col; j++) {
            var insidediv = document.createElement('div');
            document.querySelector('.row' + i).appendChild(insidediv);
            insidediv.className += "col" + j;
            insidediv.className += " allcolls";
        }

    }
    creatingNumbers();
}

function creatingNumbers() {
    var ans;
    var score = 0;
    var arr1 = randomArray(0, 6);
    console.log(arr1);

    for (var l = 0; l <=8; l++) {
      $('.row1 .col2').empty().append("<input id ='value1' type='text' class ='firstInput input1'  maxlength= '3'> </input>");
      $('.row1 .col7').empty().append("<input id ='value2' type='text' class ='firstInput input1'  maxlength= '3'> </input>");
      $(".row1 .col" + [l]).append(arr1[l]);
      allowNumbs();

    }
    var arr2 = randomArray(10, 94);
    console.log(arr2);
    for (var l = 0; l <=8; l++) {
      $('.row2 .col1').empty().append("<input id ='value3' type='text' class ='firstInput input2' maxlength= '3'> </input>");
      $('.row2 .col6').empty().append("<input id ='value4' type='text' class ='firstInput input2' maxlength= '3'> </input>");
      $(".row2 .col" + [l]).append(arr2[l]);
      allowNumbs();
    }
    var arr3 = randomArray(10,94);
    console.log(arr3);
    for (var l = 0; l <=8; l++) {
      $('.row3 .col4').empty().append("<input id ='value5' type='text' class ='firstInput input3' maxlength= '3'> </input>");
      $('.row3 .col7').empty().append("<input id ='value6' type='text' class ='firstInput input3' maxlength= '3'> </input>");
      $(".row3 .col" + [l]).append(arr3[l]);
      allowNumbs();
    }
    var arr4 = randomArray(10,94);
    console.log(arr4);
    for (var l = 0; l <=8; l++) {
      $('.row4 .col1').empty().append("<input id ='value7' type='text' class ='firstInput input4' maxlength= '3'> </input>");
      $('.row4 .col6').empty().append("<input id ='value8' type='text' class ='firstInput input4' maxlength= '3'> </input>");
      $(".row4 .col" + [l]).append(arr4[l]);
      allowNumbs();
    }
    var arr5 = randomArray(10, 94);
    console.log(arr5);
    for (var l = 0; l <=8; l++) {
      $('.row5 .col2').empty().append("<input id ='value9' type='text' class ='firstInput input5' maxlength= '3'> </input>");
      $('.row5 .col5').empty().append("<input id ='value10' type='text' class ='firstInput input5' maxlength= '3'> </input>");
      $(".row5 .col" + [l]).append(arr5[l]);
      allowNumbs();
    }

    var arr6 = randomArray(10,94);
    console.log(arr6);
    for (var l = 0; l <= 8; l++) {
      $('.row6 .col3').empty().append("<input id ='value11' type='text' class ='firstInput input6' maxlength= '3'> </input>");
      $('.row6 .col7').empty().append("<input id ='value12' type='text' class ='firstInput input6'> </input>");
      $(".row6 .col" + [l]).append(arr6[l]);
      allowNumbs();
    }

    function allowNumbs() {
        $(".firstInput").keyup(function(e) {
            var regex = /^[0-9]+$/;
            // This is will test the value against the regex
            // Will return True if regex satisfied
            if (regex.test(this.value) !== true)
            //alert if not true
            //alert("Invalid Input");
            // You can replace the invalid characters by:
                this.value = this.value.replace(/[^0-9]+/, '');
        });
    }

    function check_fin() {
      score++;
      if(score == 6){
          var endpageex =  new EndPageofExercise();
          endpageex.endpage("");
          $(".instructText").hide(0)
      }
    }


    $('.input1').change(function(){
      var row1Value = document.getElementById('value1').value;
      var row1Value2 = document.getElementById('value2').value;
      console.log(row1Value);
      console.log(row1Value2);
      if(arr1[2] == row1Value && arr1[7] == row1Value2){
        check_fin();
        console.log("rowss1 correct");
        $(".row1").addClass("animated fadeOut");
      }
    });

    $('.input2').change(function(){
       var row2Value = document.getElementById('value3').value;
       var row2Value2 = document.getElementById('value4').value;
      console.log(row2Value);
      console.log(row2Value2);
      if(arr2[1] == row2Value && arr2[6] == row2Value2){
        check_fin();
        console.log("row2 correct");
        $(".row2").addClass("animated fadeOut");
      }
    });
    $('.input3').change(function(){
       var row3Value = document.getElementById('value5').value;
       var row3Value2 = document.getElementById('value6').value;
      console.log(row3Value);
      console.log(row3Value2);
      if(arr3[4] == row3Value && arr3[7] == row3Value2){
        check_fin();
        console.log(" row3 correct");
        $(".row3").addClass("animated fadeOut");
      }
    });
    $('.input4').change(function(){
      var row4Value = document.getElementById('value7').value;
      var row4Value2 = document.getElementById('value8').value;
      console.log(row4Value);
      console.log(row4Value2);
      if(arr4[1] == row4Value && arr4[6] == row4Value2){
        check_fin();
        console.log(" row3 correct");
        $(".row4").addClass("animated fadeOut");
      }
    });
    $('.input5').change(function(){
      var row5Value = document.getElementById('value9').value;
      var row5Value2 = document.getElementById('value10').value;
      console.log(row5Value);
      console.log(row5Value2);
      if(arr5[2] == row5Value && arr5[5] == row5Value2){
        check_fin();
        console.log(" row3 correct");
        $(".row5").addClass("animated fadeOut");
      }
    });
    $('.input6').change(function(){
      var row6Value = document.getElementById('value11').value;
      var row6Value2 = document.getElementById('value12').value;
      console.log(row6Value);
      console.log(row6Value2);
      if(arr6[3] == row6Value && arr6[7] == row6Value2){
        check_fin();
        console.log(" row6 correct");
        $(".row6").addClass("animated fadeOut");
      }
    });

    function randomArray(minVal, maxVal) {

        var min = minVal;
        var max = maxVal;
        var randomNumber = Math.floor(Math.random() * (max-min)) + min;
        console.log("random number: " + randomNumber);
        var stopHere = randomNumber + 8;
        console.log("stoping: " + stopHere);

        var arr = [];
        for (var k = randomNumber; k <= stopHere; k++) {
            arr.push(k);
        }

        return (arr);
    }
}
//page 1

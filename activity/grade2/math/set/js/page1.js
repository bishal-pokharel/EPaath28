var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/p1/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title chelseamarket",
		}],
        imagedivblock:[{
            imagestoshow: [
                {
                    imgclass: "bg",
                    imgsrc: '',
                    imgid: 'coverpage'
                },
            ]
        }]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.string.p1text1,
			textclass: "set-title",
		}],
		uppertextblockadditionalclass: 'bottom-set-text',
		uppertextblock:[{
			textdata: data.string.p1text1a,
			textclass: "text-1",
		},{
			textdata: data.string.p1text1b,
			textclass: "text-2",
		},{
			textdata: data.string.p1text1c,
			textclass: "text-3",
		},{
			textdata: data.string.p1text1d,
			textclass: "text-4",
		},{
			textdata: data.string.p1text1e,
			textclass: "text-5",
		}],

		imagedivblock:[{
			imagedivblockadditionalclass: 'in-cloud',
			imagestoshow : [
				{
					imgclass : "cloud-main",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "item item-2 apple",
					imgsrc : '',
					imgid : 'apple'
				},
				{
					imgclass : "item item-1 mango",
					imgsrc : '',
					imgid : 'mango'
				},
				{
					imgclass : "item item-3 banana",
					imgsrc : '',
					imgid : 'banana'
				},
				{
					imgclass : "item item-4 papaya",
					imgsrc : '',
					imgid : 'papaya'
				},
				{
					imgclass : "item item-5 pomegranate",
					imgsrc : '',
					imgid : 'pomegranate'
				},
			]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "set-title",
		}],
		uppertextblockadditionalclass: 'bottom-set-text',
		uppertextblock:[{
			textdata: data.string.p1text2a,
			textclass: "text-1",
		},{
			textdata: data.string.p1text2b,
			textclass: "text-2",
		},{
			textdata: data.string.p1text2c,
			textclass: "text-3",
		},{
			textdata: data.string.p1text2d,
			textclass: "text-4",
		},{
			textdata: data.string.p1text2e,
			textclass: "text-5",
		}],

		imagedivblock:[{
			imagedivblockadditionalclass: 'in-cloud',
			imagestoshow : [
				{
					imgclass : "cloud-main",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "item item-1 veg",
					imgsrc : '',
					imgid : 'veg'
				},
				{
					imgclass : "item item-2 eggplant",
					imgsrc : '',
					imgid : 'eggplant'
				},
				{
					imgclass : "item item-3 cauliflower",
					imgsrc : '',
					imgid : 'cauliflower'
				},
				{
					imgclass : "item item-4 pea",
					imgsrc : '',
					imgid : 'pea'
				},
				{
					imgclass : "item item-5 tomato",
					imgsrc : '',
					imgid : 'tomato'
				},
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.string.p1text3,
			textclass: "set-title",
		}],
		uppertextblockadditionalclass: 'bottom-set-text',
		uppertextblock:[{
			textdata: data.string.p1text3a,
			textclass: "text-1",
		},{
			textdata: data.string.p1text3b,
			textclass: "text-2",
		},{
			textdata: data.string.p1text3c,
			textclass: "text-3",
		},{
			textdata: data.string.p1text3d,
			textclass: "text-4",
		}],

		imagedivblock:[{
			imagedivblockadditionalclass: 'in-cloud',
			imagestoshow : [
				{
					imgclass : "cloud-main",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "item item-1 bucket",
					imgsrc : '',
					imgid : 'bucket'
				},
				{
					imgclass : "item item-2 pot",
					imgsrc : '',
					imgid : 'pot'
				},
				{
					imgclass : "item item-3 jug",
					imgsrc : '',
					imgid : 'jug'
				},
				{
					imgclass : "item item-4 glass",
					imgsrc : '',
					imgid : 'glass'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.string.p1text1,
			textclass: "cloud-title ct-1 slide-right",
		},
		{
			textdata: data.string.p1text2,
			textclass: "cloud-title ct-2 slide-left",
		},
		{
			textdata: data.string.p1text3,
			textclass: "cloud-title ct-3 scale-out",
		},
		{
			textdata: data.string.p1text4,
			textclass: "bottom-def",
		}],

		imagedivblock:[{
			imagedivblockadditionalclass: 'mini-cloud slide-right cloud-1',
			imagestoshow : [
				{
					imgclass : "cloud-main",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "item item-2 apple",
					imgsrc : '',
					imgid : 'apple'
				},
				{
					imgclass : "item item-1 mango",
					imgsrc : '',
					imgid : 'mango'
				},
				{
					imgclass : "item item-3 banana",
					imgsrc : '',
					imgid : 'banana'
				},
				{
					imgclass : "item item-4 papaya",
					imgsrc : '',
					imgid : 'papaya'
				},
				{
					imgclass : "item item-5 pomegranate",
					imgsrc : '',
					imgid : 'pomegranate'
				},
			]
		},
		{
			imagedivblockadditionalclass: 'mini-cloud slide-left cloud-2',
			imagestoshow : [
				{
					imgclass : "cloud-main",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "item item-1 veg",
					imgsrc : '',
					imgid : 'veg'
				},
				{
					imgclass : "item item-2 eggplant",
					imgsrc : '',
					imgid : 'eggplant'
				},
				{
					imgclass : "item item-3 cauliflower",
					imgsrc : '',
					imgid : 'cauliflower'
				},
				{
					imgclass : "item item-4 pea",
					imgsrc : '',
					imgid : 'pea'
				},
				{
					imgclass : "item item-5 tomato",
					imgsrc : '',
					imgid : 'tomato'
				},
			]
		},
		{
			imagedivblockadditionalclass: 'mini-cloud scale-out cloud-3',
			imagestoshow : [
				{
					imgclass : "cloud-main",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "item item-1 bucket",
					imgsrc : '',
					imgid : 'bucket'
				},
				{
					imgclass : "item item-2 pot",
					imgsrc : '',
					imgid : 'pot'
				},
				{
					imgclass : "item item-3 jug",
					imgsrc : '',
					imgid : 'jug'
				},
				{
					imgclass : "item item-4 glass",
					imgsrc : '',
					imgid : 'glass'
				}
			]
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue-2',

		extratextblock:[{
			textdata: data.string.p1text5,
			textclass: "set-title-2 text-0",
		},{
			textdata: data.string.p1text10,
			textclass: "bt-text-2 text-5",
		}],
		uppertextblockadditionalclass: 'list-block',
		uppertextblock:[{
			textdata: data.string.p1text6,
			textclass: "text-1",
		},{
			textdata: data.string.p1text7,
			textclass: "text-2",
		},{
			textdata: data.string.p1text8,
			textclass: "text-3",
		},{
			textdata: data.string.p1text9,
			textclass: "text-4",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "kid",
					imgsrc : '',
					imgid : 'kid'
				},
				{
					imgclass : "kid-1",
					imgsrc : '',
					imgid : 'kid-1'
				},
				{
					imgclass : "plate",
					imgsrc : '',
					imgid : 'plate'
				},
				{
					imgclass : "teddy",
					imgsrc : '',
					imgid : 'teddy'
				}
			]
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue-3',

		extratextblock:[{
			textdata: data.string.p1text11,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "grass",
		},{
			textdata: data.string.p1text12,
			textclass: "fb-text fb-text-1",
		},{
			textdata: data.string.p1text13,
			textclass: "fb-text fb-text-2",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cloud-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "click-item correct-item tree",
					imgsrc : '',
					imgid : 'tree'
				},
				{
					imgclass : "click-item cow",
					imgsrc : '',
					imgid : 'cow'
				},
				{
					imgclass : "click-item goat",
					imgsrc : '',
					imgid : 'goat'
				},
				{
					imgclass : "click-item cat",
					imgsrc : '',
					imgid : 'cat'
				},
				{
					imgclass : "click-item dog",
					imgsrc : '',
					imgid : 'dog'
				}
			]
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-yellow',

		extratextblock:[{
			textdata: data.string.p1text14,
			textclass: "instruction-2",
		},{
			textdata: data.string.p1text15,
			textclass: "fb-text fb-text-1",
		},{
			textdata: data.string.p1text16,
			textclass: "fb-text fb-text-2",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cloud-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "click-item correct-item chicken",
					imgsrc : '',
					imgid : 'chicken'
				},
				{
					imgclass : "click-item copy",
					imgsrc : '',
					imgid : 'copy'
				},
				{
					imgclass : "click-item book",
					imgsrc : '',
					imgid : 'book'
				},
				{
					imgclass : "click-item pencil",
					imgsrc : '',
					imgid : 'pencil'
				},
				{
					imgclass : "click-item eraser",
					imgsrc : '',
					imgid : 'eraser'
				}
			]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var index = 0;
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},

			{id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "banana", src: imgpath+"banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+"mango.png", type: createjs.AbstractLoader.IMAGE},
			{id: "papaya", src: imgpath+"papaya.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pomegranate", src: imgpath+"pomegranate.png", type: createjs.AbstractLoader.IMAGE},

			{id: "veg", src: imgpath+"veg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cauliflower", src: imgpath+"cauliflower.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pea", src: imgpath+"pea.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tomato", src: imgpath+"tomato.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eggplant", src: imgpath+"eggplant.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bucket", src: imgpath+"bucket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jug", src: imgpath+"jug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glass", src: imgpath+"glass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pot", src: imgpath+"pot.png", type: createjs.AbstractLoader.IMAGE},

			{id: "kid", src: imgpath+"kids.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plate", src: imgpath+"plate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kid-1", src: imgpath+"kid-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teddy", src: imgpath+"teddy.png", type: createjs.AbstractLoader.IMAGE},

			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "goat", src: imgpath+"goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cat", src: imgpath+"cat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree", src: imgpath+"tree.png", type: createjs.AbstractLoader.IMAGE},

			{id: "chicken", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "copy", src: imgpath+"notebook.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book", src: imgpath+"book.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eraser", src: imgpath+"eraser.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2_1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2_mango.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p2_apple.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p2_banana.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p2_pear.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p2_pomeogranate.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p3_1.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p3_raddish.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p3_brinjal.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p3_cauliflower.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p3_peapod.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p3_tomato.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p4_1.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p4_bucket.ogg"},
			{id: "sound_15", src: soundAsset+"s1_p4_pot.ogg"},
			{id: "sound_16", src: soundAsset+"s1_p4_jug.ogg"},
			{id: "sound_17", src: soundAsset+"s1_p4_glass.ogg"},
			{id: "sound_18", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_19", src: soundAsset+"s1_p6_1.ogg"},
			{id: "sound_20", src: soundAsset+"s1_p6_2.ogg"},
			{id: "sound_21", src: soundAsset+"s1_p6_3.ogg"},
			{id: "sound_22", src: soundAsset+"s1_p6_4.ogg"},
			{id: "sound_23", src: soundAsset+"s1_p6_5.ogg"},
			{id: "sound_24", src: soundAsset+"s1_p6_6.ogg"},
			{id: "sound_25", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_26", src: soundAsset+"s1_p7_animals.ogg"},
			{id: "sound_27", src: soundAsset+"s1_p7_trees.ogg"},
			{id: "sound_28", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_29", src: soundAsset+"s1_p8_chicken.ogg"},
			{id: "sound_30", src: soundAsset+"s1_p8_stationery.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		vocabcontroller.findwords(countNext);

		switch(countNext) {
			case 0:
			sound_player("sound_0");
				$nextBtn.show(0);
				break;
			case 1:
			sound_player("sound_1");
			setTimeout(function(){
				var sound_arr = ['sound_2', 'sound_3', 'sound_4', 'sound_5', 'sound_6'];
				index = 1;
				item_caller(sound_arr);
			},3300);

			break;
			case 2:
				sound_player("sound_7");
				setTimeout(function(){
					var sound_arr = ['sound_8', 'sound_9', 'sound_10', 'sound_11', 'sound_12'];
					index = 1;
					item_caller(sound_arr);
				},3300);

				break;
			case 3:
			sound_player("sound_13");
			setTimeout(function(){
				var sound_arr = ['sound_14', 'sound_15', 'sound_16', 'sound_17'];
				index = 1;
				item_caller(sound_arr);
				},3300);

				break;
			case 4:
				$(".cloud-2").delay(1000).animate({"opacity":"1"},1000);
				sound_player("sound_18");
				break;
			case 5:
				index = 0;
				var sound_arr = ['sound_19', 'sound_20', 'sound_21', 'sound_22', 'sound_23', 'sound_24'];
				list_caller(sound_arr);
				function list_caller(sound_array){
					$('.text-' + index).css('visibility', 'visible');
					createjs.Sound.stop();
					current_sound = createjs.Sound.play(sound_array[index]);
					current_sound.play();
					index++;
					current_sound.on("complete", function(){
						if(index<sound_array.length){
							list_caller(sound_array);
						} else{
                            nav_button_controls(0);
						}
					});
				}
				break;
			case 6:
				sound_player("sound_25");
				$('.click-item').click(function(){
					if($(this).hasClass('correct-item')){
						current_sound.stop();
						play_correct_incorrect_sound(1);
						$('.click-item').css('pointer-events', 'none');
						$('.instruction').addClass('ins-anim');
						$('.grass').animate({
							'height': '0%'
						}, 500, function(){
							$('.cow').animate({
								'bottom':'40%',
								'left':'10%',
								'height':'20%',
							}, 500, function(){
								$('.dog').animate({
									'bottom':'28%',
									'left':'28%',
									'height':'22%',
								}, 800, function(){
									$('.goat').animate({
										'bottom':'55%',
										'right':'58%',
										'height':'22%',
									},  800, function(){
										$('.tree').animate({
											'bottom':'39%',
											'left':'68%',
											'width':'30%',
										},  800, function(){
											$('.cat').animate({
												'bottom':'41%',
												'right':'41%',
												'height':'20%',
											},  800, function(){
												$('.cloud-2').fadeIn(500, function(){
													$('.fb-text-2').fadeIn(500);
												});
										createjs.Sound.stop();
										current_sound = createjs.Sound.play("sound_27");
										current_sound.play();
												current_sound.on("complete", function(){
													createjs.Sound.stop();
													$('.fb-text-1').fadeIn(500);
													current_sound = createjs.Sound.play("sound_26");
													current_sound.play();
															current_sound.on("complete", function(){
																nav_button_controls(0);
													});
												});
											});
										});
									});
								});
							});
						});
					} else{
                        current_sound.stop();
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
					}
				})
				break;
			case 7:
				sound_player("sound_28");
				$('.click-item').click(function(){
					if($(this).hasClass('correct-item')){
                        current_sound.stop();
						play_correct_incorrect_sound(1);
						$('.click-item').css('pointer-events', 'none');
						$('.bg-yellow').addClass('bg-anim');
						timeoutvar= setTimeout(function(){
							$('.eraser').animate({
								'bottom':'30%',
								'left':'30%',
								'height':'15%',
							}, 500, function(){
								$('.book').animate({
									'bottom':'45%',
									'right':'71%',
									'width':'18%',
								}, 800);
								$('.chicken').animate({
									'left':'75%',
								},  800, function(){
									$('.copy').animate({
										'bottom':'55%',
										'left':'30%',
										'height':'20%',
									},  800, function(){
										$('.pencil').animate({
											'bottom':'45%',
											'right':'41%',
											'height':'15%',
										},  800, function(){
											$('.cloud-2').fadeIn(500, function(){
												$('.fb-text-2').fadeIn(500);
											});
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("sound_29");
									current_sound.play();
											current_sound.on("complete", function(){
												createjs.Sound.stop();
															$('.fb-text-1').fadeIn(500);
												current_sound = createjs.Sound.play("sound_30");
												current_sound.play();
														current_sound.on("complete", function(){
															nav_button_controls(0);
												});
											});
										});
									});
								});
							});
						}, 500);
					} else{
                        current_sound.stop();
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
					}
				})
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function item_caller(sound_array){
		$('.text-' + index).css('opacity', '1');
		$('.item-'+index).addClass('current-item');
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_array[index-1]);
		current_sound.play();
		index++;
		current_sound.on("complete", function(){
			$('.item').removeClass('current-item');
			if(index<=sound_array.length){
				item_caller(sound_array);
			} else{
				nav_button_controls(0);
			}
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(countNext==4){
					nav_button_controls(0);
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			for(var j=0; j<content[count].imagedivblock.length; j++){
				var imageblock = content[count].imagedivblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

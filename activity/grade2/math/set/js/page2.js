var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content = [
	//start
	{
		contentblockadditionalclass : "simplebg",
		contentnocenteradjust : true,
		uppertextblockadditionalclass : "firsttitle",
		uppertextblock : [{
			textdata : data.string.diytext
		}],
        imageblock : [{
            imagestoshow: [{
                imgclass: "bgimg",
                imgsrc: '',
                imgid: 'diyimg'
            },
            ]
        }]
	},
	//page 1
	{
		contentblockadditionalclass : "green-bg",
		contentnocenteradjust : true,
		imageblockadditionalclass : "upperimageblockholder",
		imageblock : [{
			imagestoshow : [{
				//1
				imgclass : "draggable chew  sliding hide im-1",
				imgsrc : '',
				imgid : 'duck'
			}, {
				//2
				imgclass : "draggable chew  sliding hide im-2",
				imgsrc : '',
				imgid : 'eagle'
			}, {
				//3
				imgclass : "draggable chew  sliding hide im-3",
				imgsrc : '',
				imgid : 'hen'
			}, {
				//4
				imgclass : "draggable chew sliding hide im-4",
				imgsrc : '',
				imgid : 'parrot'
			}, {
				//2_1
				imgclass : "draggable lick sliding hide im-5",
				imgsrc : '',
				imgid : 'pant'
			}, {
				//2_2
				imgclass : "draggable lick sliding hide im-6",
				imgsrc : '',
				imgid : 'shoes'
			}, {
				//2_3
				imgclass : "draggable lick sliding hide im-7",
				imgsrc : '',
				imgid : 'shirt'
			}, {
				//2_4
				imgclass : "draggable lick sliding hide im-8",
				imgsrc : '',
				imgid : 'shirt-1'
			}, {
				//3_1
				imgclass : "draggable swallow sliding hide im-9",
				imgsrc : '',
				imgid : 'shape01'
			}, {
				//3_2
				imgclass : "draggable swallow sliding hide im-10",
				imgsrc : '',
				imgid : 'shape02'
			}, {
				//3_3
				imgclass : "draggable swallow sliding hide im-11",
				imgsrc : '',
				imgid : 'shape03'
			}, {
				//3_4
				imgclass : "draggable swallow sliding hide im-12",
				imgsrc : '',
				imgid : 'shape04'
			}],
			imagelabels : [{
				imagelabelclass : "draganddropinst",
				imagelabeldata : data.string.p2text1
			}]
		}],
		specialdropdiv : [{
			droplabelclass : "birdlabel",
			droplabeldata : data.string.p2text2,
			dropdiv : "birddrop"
		}, {
			droplabelclass : "clothlabel",
			droplabeldata : data.string.p2text3,
			dropdiv : "clothdrop"
		}, {
			droplabelclass : "shapelabel",
			droplabeldata : data.string.p2text4,
			dropdiv : "shapedrop"
		}]
	}
];

content[1].imageblock[0].imagestoshow.shufflearray();
for(var i=0; i< 4; i++){
	var classname = content[1].imageblock[0].imagestoshow[i].imgclass;
	classname = classname.replace(/hide/,'');
	content[1].imageblock[0].imagestoshow[i].imgclass = classname + ' draggableposition' + (i+1);
	console.log(content[1].imageblock[0].imagestoshow[i].imgclass);
}
$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "diyimg", src: "images/diy_bg/a_08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shape01", src: imgpath+"diy/shape01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shape02", src: imgpath+"diy/shape02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shape03", src: imgpath+"diy/shape03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shape04", src: imgpath+"diy/shape04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "duck", src: imgpath+"diy/duck.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"diy/hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "parrot", src: imgpath+"diy/parrot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eagle", src: imgpath+"diy/eagle.png", type: createjs.AbstractLoader.IMAGE},

			{id: "pant", src: imgpath+"diy/pant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shoes", src: imgpath+"diy/shoes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shirt", src: imgpath+"diy/t-shirt01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shirt-1", src: imgpath+"diy/t-shirt02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s2_p2.ogg"},
			// {id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			//textboxes
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	function navigationcontroller(islastpageflag) {
		// typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		vocabcontroller.findwords(countNext);

		//call notifyuser
		// notifyuser($anydiv);

		switch(countNext){
			case 0:
				play_diy_audio();
				$nextBtn.show(0);
				break;
			case 1:
				sound_player('sound_0');
				$(".draggable").draggable({
					containment : "body",
					cursor : "grab",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 1000,
					start: function(event, ui){
						$(ui.helper).addClass("disableanimation");
						$(ui.helper).css({"max-width": "18%",
										  "max-height": "15%"});

					},
					stop: function(event, ui){
						$(ui.helper).removeClass("disableanimation");
						$(ui.helper).css("max-width", "");
					}
				});

				$('.birddrop').droppable({
					// accept : ".chew",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "chew" , "birddrop", ui.draggable);
					}
				});

				$('.clothdrop').droppable({
					// accept : ".lick",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "lick" , "clothdrop", ui.draggable);
					}
				});

				$('.shapedrop').droppable({
					// accept : ".swallow",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "swallow" , "shapedrop", ui.draggable);
					}
				});
				var divsdroppedcount = 0;
				function handleCardDrop(event, ui, classname, droppedon, dropped) {
					if(dropped.hasClass(classname)){
                        current_sound.stop();
						play_correct_incorrect_sound(1);
						ui.draggable.draggable('disable');
						var dropped = ui.draggable;
						var droppedOn = $("."+droppedon);
						var count = 0;
						var top = 0;

							count = $("."+droppedon+"> ."+classname).length;
							top = (count < 2)? 10: 50;
							left = (count%2==0)? 0: 50;
							// $(dropped.context.children[0]).css({
								// "width": "40%",
								// "height": "auto",
								// "max-height": "70%"
							// });
							$(dropped).detach().css({
								"position" : "absolute",
								"top" : top+"%",
								"left": left + "%",
								"width": "50%",
								"height": "auto",
								"max-height": "70%"
							}).appendTo(droppedOn);

						// var $classtoshow;
						var $newEntry = $(".upperimageblockholder> .hide").eq(0);
						var $draggable3;
						var $draggable2;
						var $draggable1;
						if(dropped.hasClass("draggableposition4")){
							dropped.toggleClass("draggableposition4");
							$draggable3 = $(".draggableposition3");
							$draggable2 = $(".draggableposition2");
							$draggable1 = $(".draggableposition1");
							// $classtoshow = $(".draggableposition4").eq(0);

						}else if(dropped.hasClass("draggableposition3")){
							dropped.toggleClass("draggableposition3");
							$draggable2 = $(".draggableposition2");
							$draggable1 = $(".draggableposition1");
							// $classtoshow = $(".draggableposition3").eq(0);
						}else if(dropped.hasClass("draggableposition2")){
							dropped.toggleClass("draggableposition2");
							$draggable1 = $(".draggableposition1");
							// $classtoshow = $(".draggableposition2").eq(0);
						}else if(dropped.hasClass("draggableposition1")){
							dropped.toggleClass("draggableposition1");
							// $classtoshow = $(".draggableposition1").eq(0);
						}

						if($draggable3 != null){
							 $draggable3.removeClass("draggableposition3").addClass("draggableposition4");
						}
						if($draggable2 != null){
							 $draggable2.removeClass("draggableposition2").addClass("draggableposition3");
						}
						if($draggable1 != null){
							 $draggable1.removeClass("draggableposition1").addClass("draggableposition2");
						}
						if($newEntry != null){
							 $newEntry.removeClass("hide").addClass("draggableposition1");
						}
						divsdroppedcount++;
						if(divsdroppedcount == 12){
							$prevBtn.show(0);
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					} else {
                        current_sound.stop();
						play_correct_incorrect_sound(0);
					}
				}
				break;
			default:
				break;
		}

	}

	/*=====  End of Templates Block  ======*/
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		switch(countNext) {
		case 1:
			$nextBtn.hide(0);
			$(".container2, .container3").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant1").toggleClass("plant1animate");
			// $(".plant2").toggleClass("plant2animate");
			// $(".plant3").toggleClass("plant3animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 2:
			$nextBtn.hide(0);
			// $(".container2, .container3").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant1").toggleClass("plant1animate");
			$(".plant2").toggleClass("plant2animate");
			$(".plant3").toggleClass("plant3animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		case 4:
			$nextBtn.hide(0);
			$(".container3, .container1").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant2_1").toggleClass("plant2_1animate");
			// $(".plant2_2").toggleClass("plant2_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 5:
			$nextBtn.hide(0);
			// $(".container3, .container1").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant2_1").toggleClass("plant2_1animate");
			$(".plant2_2").toggleClass("plant2_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		case 7:
			$nextBtn.hide(0);
			$(".container2, .container1").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant3_1").toggleClass("plant3_1animate");
			// $(".plant3_2").toggleClass("plant3_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 8:
			$nextBtn.hide(0);
			// $(".container2, .container1").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant3_1").toggleClass("plant3_1animate");
			$(".plant3_2").toggleClass("plant3_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

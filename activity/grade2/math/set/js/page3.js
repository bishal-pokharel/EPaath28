var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-main',

		extratextblock:[{
			textdata: data.string.p3text1,
			textclass: "head-text fade-in-0",
		},
		{
			textdata: data.string.p3text2,
			textclass: "cloud-text fade-in-1",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cloud-center fade-in-1",
					imgsrc : '',
					imgid : 'cloud'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-main',

		extratextblock:[{
			textdata: data.string.p3text3,
			textclass: "head-text fade-in-0",
		},
		{
			textdata: data.string.p3text4,
			textclass: "cloud-text fade-in-1",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cloud-center fade-in-1",
					imgsrc : '',
					imgid : 'cloud'
				}
			]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-main',

		extratextblock:[{
			textdata: data.string.p3text5,
			textclass: "head-text fade-in-0",
		},
		{
			textdata: data.string.p3text6,
			textclass: "cloud-text fade-in-1",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cloud-center fade-in-1",
					imgsrc : '',
					imgid : 'cloud'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-main-2',

		extratextblock:[
		{
			textdata: '',
			textclass: "text-bg",
		},
		{
			textdata: data.string.p3text1,
			textclass: "head-text2 ht-1 fade-in-2",
		},
		{
			textdata: data.string.p3text2,
			textclass: "cloud-text2 ct-1 scale-out",
		},{
			textdata: data.string.p3text3,
			textclass: "head-text2 ht-2 fade-in-2",
		},
		{
			textdata: data.string.p3text4,
			textclass: "cloud-text2 ct-2 scale-out",
		},{
			textdata: data.string.p3text5,
			textclass: "head-text2 ht-3 fade-in-2",
		},
		{
			textdata: data.string.p3text6,
			textclass: "cloud-text2 ct-3 scale-out",
		},
		{
			textdata: data.string.p3text7,
			textclass: "box-text fade-in-4",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cloud-mini cloud-1 fade-in-1",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-mini cloud-2 fade-in-1",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-mini cloud-3 fade-in-1",
					imgsrc : '',
					imgid : 'cloud'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.string.p3text8,
			textclass: "instruction",
		},
		{
			textdata: data.string.p3text9a,
			textclass: "num-text c-1",
		},
		{
			textdata: data.string.p3text9b,
			textclass: "num-text c-2",
		},
		{
			textdata: data.string.p3text9c,
			textclass: "num-text c-3",
		},
		{
			textdata: data.string.p3text9e,
			textclass: "num-text c-4 correct-item",
		},
		{
			textdata: data.string.p3text9d,
			textclass: "num-text c-5",
		},
		{
			textdata: data.string.p3text9g,
			textclass: "f-text f-1",
		},
		{
			textdata: data.string.p3text9f,
			textclass: "f-text f-2",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "hill",
					imgsrc : '',
					imgid : 'grass'
				},
				{
					imgclass : "cloud-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-num c-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-num c-2",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-num c-3",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-num c-4 correct-item correct-image",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-num c-5",
					imgsrc : '',
					imgid : 'cloud-1'
				}
			]
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.string.p3text10,
			textclass: "instruction",
		},
		{
			textdata: data.string.p3text11a,
			textclass: "num-text c-1",
		},
		{
			textdata: data.string.p3text11b,
			textclass: "num-text c-2",
		},
		{
			textdata: data.string.p3text11e,
			textclass: "num-text c-3  correct-item",
		},
		{
			textdata: data.string.p3text11c,
			textclass: "num-text c-4",
		},
		{
			textdata: data.string.p3text11d,
			textclass: "num-text c-5",
		},
		{
			textdata: data.string.p3text11g,
			textclass: "f-text f-1",
		},
		{
			textdata: data.string.p3text11f,
			textclass: "f-text f-2",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "hill",
					imgsrc : '',
					imgid : 'grass'
				},
				{
					imgclass : "cloud-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-num c-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-num c-2",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-num c-3 correct-item correct-image",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-num c-4",
					imgsrc : '',
					imgid : 'cloud-1'
				},
				{
					imgclass : "cloud-num c-5",
					imgsrc : '',
					imgid : 'cloud-1'
				}
			]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grass", src: imgpath+"grass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-1", src: imgpath+"cloud_2.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p5_20.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p5_set_of_single.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p6_75.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p6_set_of_multiple.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		switch(countNext) {
			case 0:
					sound_player("sound_1");
				break;
			case 1:
					sound_player("sound_2");
					break;
			case 2:
					sound_player("sound_3");
				break;
			case 3:
				setTimeout(function(){
					sound_player("sound_4");
				},4000);
				break;
			case 4:
				sound_player("sound_5");
				hover_func('.c-1');
				hover_func('.c-2');
				hover_func('.c-3');
				hover_func('.c-4');
				hover_func('.c-5');
                $('.cloud-2').hide().css({"width":"29%","top":"26%"});
				$('.num-text, .cloud-num').click(function(){
					if($(this).hasClass('correct-item')){
                        current_sound.stop();
						play_correct_incorrect_sound(1);
						$('.num-text, .cloud-num').css('pointer-events', 'none');
						$('.num-text, .cloud-num').removeClass('disselect');
						$('.c-1').animate({
							'left': '10%',
							'top': '34%'
						}, 1000);
						$('.c-2').animate({
							'left': '22%',
							'top': '22%'
						}, 1000);
						$('.c-3').animate({
							'left': '37%',
							'top': '34%'
						}, 1000);
						$('.c-5').animate({
							'left': '22%',
							'top': '46%'
						}, 1000, function(){
							$('.cloud-num').fadeOut(500);
							$('.cloud-2').fadeIn(500, function(){
								$('.f-text').fadeIn(500, function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_6");
						current_sound.play();
								current_sound.on("complete", function(){
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("sound_7");
									current_sound.play();
											current_sound.on("complete", function(){
												nav_button_controls(0);
									});
								});
								});
							});
						});


          $('.c-4').addClass('odd-anim');
					} else {
						var cname = $(this).attr('class').replace(/\D+/g, '');
						$('.c-'+cname).css({
							'pointer-events': 'none',
						});
						$('.c-'+cname).addClass('disselect');
                        current_sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
				break;
			case 5:
        $('.cloud-2').hide().css({"width":"29%","top":"26%"});
      	sound_player("sound_8");
				hover_func('.c-1');
				hover_func('.c-2');
				hover_func('.c-3');
				hover_func('.c-4');
				hover_func('.c-5');

				$('.num-text, .cloud-num').click(function(){
					if($(this).hasClass('correct-item')){
						console.log("Correct clicked");
                        current_sound.stop();
						play_correct_incorrect_sound(1);
						$('.num-text, .cloud-num').css('pointer-events', 'none');
						$('.num-text, .cloud-num').removeClass('disselect');
						$('.c-1').animate({
							'left': '10%',
							'top': '34%'
						}, 1000);
						$('.c-2').animate({
							'left': '22%',
							'top': '22%'
						}, 1000);
						$('.c-5').animate({
							'left': '37%',
							'top': '34%'
						}, 1000);
						$('.c-4').animate({
							'left': '22%',
							'top': '46%'
						}, 1000, function(){
							$('.cloud-num').fadeOut(500);
							$('.cloud-2').fadeIn(500, function(){
								$('.f-text').fadeIn(500, function(){
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("sound_9");
							current_sound.play();
									current_sound.on("complete", function(){
										createjs.Sound.stop();
										current_sound = createjs.Sound.play("sound_10");
										current_sound.play();
												current_sound.on("complete", function(){
													nav_button_controls(0);
										});
									});
								});
							});
						});
						$('.c-3').addClass('odd-anim');
                    } else {
						var cname = $(this).attr('class').replace(/\D+/g, '');
						$('.c-'+cname).css({
							'pointer-events': 'none',
						});
						$('.c-'+cname).addClass('disselect');
                        current_sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		console.log("Here I am");
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
				current_sound.on("complete", function(){
					if(countNext==4 || countNext==5){
						$prevBtn.hide(0);
						$nextBtn.hide(0);
					}
					else{
						nav_button_controls(0);
					}
	});
}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function hover_func(class_name){
		$(class_name).hover(function(){
			$(class_name).css({
				'transform': 'scale(1.1)',
				'filter': 'drop-shadow(0px 0px 2px black)',
				'-webkit-filter': 'drop-shadow(0px 0px 2px black)'
			});
		}, function(){
			$(class_name).css({
				'transform': '',
				'filter': '',
				'-webkit-filter': ''
			});
		});
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

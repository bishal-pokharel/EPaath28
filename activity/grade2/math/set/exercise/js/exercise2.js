var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 1
	{
		imgtype: true,
		centerimgsrc: 'cloud-1',
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text1,
				option: [
					{
						option_class: "class1",
						optionsrc: 'im-1',
					},
					{
						option_class: "class2",
						optionsrc: 'im-2',
					},
					{
						option_class: "class3",
						optionsrc: 'im-3',
					},
					{
						option_class: "class4",
						optionsrc: 'im-4',
					},
				],
			}
		]
	},
	//slide 2
	{
		imgtype: true,
		centerimgsrc: 'cloud-2',
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text2,
				option: [
					{
						option_class: "class1",
						optionsrc: 'im-5',
					},
					{
						option_class: "class2",
						optionsrc: 'im-2',
					},
					{
						option_class: "class3",
						optionsrc: 'im-4',
					},
					{
						option_class: "class4",
						optionsrc: 'im-6',
					},
				],
			}
		]
	},
	//slide 3
	{
		imgtype: true,
		centerimgsrc: 'cloud-3',
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text3,
				option: [
					{
						option_class: "class1",
						optionsrc: 'im-2',
					},
					{
						option_class: "class2",
						optionsrc: 'im-6',
					},
					{
						option_class: "class3",
						optionsrc: 'im-4',
					},
					{
						option_class: "class4",
						optionsrc: 'im-1',
					},
				],
			}
		]
	},
	//slide 4
	{
		imgtype: true,
		centerimgsrc: 'cloud-4',
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text4,
				option: [
					{
						option_class: "class1",
						optionsrc: 'im-4',
					},
					{
						option_class: "class2",
						optionsrc: 'im-2',
					},
					{
						option_class: "class3",
						optionsrc: 'im-1',
					},
					{
						option_class: "class4",
						optionsrc: 'im-6',
					},
				],
			}
		]
	},
	//slide 5
	{
		imgtype: true,
		centerimgsrc: 'cloud-5',
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text1,
				option: [
					{
						option_class: "class1",
						optionsrc: 'im-1',
					},
					{
						option_class: "class2",
						optionsrc: 'im-2',
					},
					{
						option_class: "class3",
						optionsrc: 'im-5',
					},
					{
						option_class: "class4",
						optionsrc: 'im-4',
					},
				],
			}
		]
	},
	//slide 6
	{
		imgtype: false,
		centerimgsrc: 'cloud',
		contentblockadditionalclass:'',
		extratextblock:[{
			textclass: 'ques-img-text qit-1',
			textdata: data.string.e2q6a
		},{
			textclass: 'ques-img-text qit-2',
			textdata: data.string.e2q6b
		},{
			textclass: 'ques-img-text qit-3',
			textdata: data.string.e2q6c
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text6,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e2text6a,
					},
					{
						option_class: "class2",
						optiondata: data.string.e2text6b,
					},
					{
						option_class: "class3",
						optiondata: data.string.e2text6c,
					},
					{
						option_class: "class4",
						optiondata: data.string.e2text6d,
					}
				],
			}
		]
	},
	//slide7
	{
		imgtype: false,
		centerimgsrc: 'cloud',
		contentblockadditionalclass:'',
		extratextblock:[{
			textclass: 'ques-img-text qit-1',
			textdata: data.string.e2q7a
		},{
			textclass: 'ques-img-text qit-2',
			textdata: data.string.e2q7b
		},{
			textclass: 'ques-img-text qit-3',
			textdata: data.string.e2q7c
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text7,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e2text7a,
					},
					{
						option_class: "class2",
						optiondata: data.string.e2text7b,
					},
					{
						option_class: "class3",
						optiondata: data.string.e2text7c,
					},
					{
						option_class: "class4",
						optiondata: data.string.e2text7d,
					}
				],
			}
		]
	},
	//slide 8
	{
		imgtype: false,
		centerimgsrc: 'cloud',
		contentblockadditionalclass:'',
		extratextblock:[{
			textclass: 'ques-img-text qit-1',
			textdata: data.string.e2q8a
		},{
			textclass: 'ques-img-text qit-2',
			textdata: data.string.e2q8b
		},{
			textclass: 'ques-img-text qit-3',
			textdata: data.string.e2q8c
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text8,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e2text8a,
					},
					{
						option_class: "class2",
						optiondata: data.string.e2text8b,
					},
					{
						option_class: "class3",
						optiondata: data.string.e2text8c,
					},
					{
						option_class: "class4",
						optiondata: data.string.e2text8d,
					}
				],
			}
		]
	},
	//slide 9
	{
		imgtype: false,
		centerimgsrc: 'cloud',
		contentblockadditionalclass:'',
		extratextblock:[{
			textclass: 'ques-img-text qit-1',
			textdata: data.string.e2q9a
		},{
			textclass: 'ques-img-text qit-2',
			textdata: data.string.e2q9b
		},{
			textclass: 'ques-img-text qit-3',
			textdata: data.string.e2q9c
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text9,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e2text9a,
					},
					{
						option_class: "class2",
						optiondata: data.string.e2text9b,
					},
					{
						option_class: "class3",
						optiondata: data.string.e2text9c,
					},
					{
						option_class: "class4",
						optiondata: data.string.e2text9d,
					}
				],
			}
		]
	},
	//slide 10
	{
		imgtype: false,
		centerimgsrc: 'cloud',
		contentblockadditionalclass:'',
		extratextblock:[{
			textclass: 'ques-img-text qit-1',
			textdata: data.string.e2q10a
		},{
			textclass: 'ques-img-text qit-2',
			textdata: data.string.e2q10b
		},{
			textclass: 'ques-img-text qit-3',
			textdata: data.string.e2q10c
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e2ins,
				questiondata: data.string.e2text10,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e2text10a,
					},
					{
						option_class: "class2",
						optiondata: data.string.e2text10b,
					},
					{
						option_class: "class3",
						optiondata: data.string.e2text10c,
					},
					{
						option_class: "class4",
						optiondata: data.string.e2text10d,
					}
				],
			}
		]
	},
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new LampTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-1", src: imgpath+"set-of-stationary.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"set-of-bug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-3", src: imgpath+"set-of-bird.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-4", src: imgpath+"set-of-transport.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-5", src: imgpath+"set-of-book.png", type: createjs.AbstractLoader.IMAGE},

			{id: "im-1", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+"bee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-4", src: imgpath+"cycle.png", type: createjs.AbstractLoader.IMAGE},

			{id: "im-5", src: imgpath+"bee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-6", src: imgpath+"book.png", type: createjs.AbstractLoader.IMAGE},

			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"ex_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrong_clicked 	= false;

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		scoring.numberOfQuestions();

		$('.center-img').attr('src', preload.getResult(content[countNext].centerimgsrc).src);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		/*for randomizing the options*/
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}
		if(content[countNext].imgtype){
			put_image(content, countNext);
		}

		if(countNext==0){
			sound_player("sound_0");
		}
		var wrong_clicked = 0;
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				$(".option-container").css('pointer-events','none');
				current_sound.stop();
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				scoring.update(false);
                current_sound.stop();
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	};

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('exerciseblock')){
			var imageblock = content[count].exerciseblock[0];
			if(imageblock.hasOwnProperty('option')){
				var imageClass = imageblock.option;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].optionsrc).src;
					//get list of classes
					var classes_list = imageClass[i].option_class.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]+'>img');
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generalTemplate();
		// for scoringg purpose
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

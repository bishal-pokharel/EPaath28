var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 1
	{
		imgtype: true,
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1q1,
				questiondata: '',
				option: [
					{
						option_class: "class1",
						optionsrc: 'im-5',
					},
					{
						option_class: "class2",
						optionsrc: 'im-2',
					},
					{
						option_class: "class3",
						optionsrc: 'im-3',
					},
					{
						option_class: "class4",
						optionsrc: 'im-4',
					},
					{
						option_class: "class5",
						optionsrc: 'im-1',
					}],
			}
		]
	},
	//slide 2
	{
		imgtype: true,
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1q2,
				option: [
					{
						option_class: "class3",
						optionsrc: 'im-5',
					},
					{
						option_class: "class2",
						optionsrc: 'im-6',
					},
					{
						option_class: "class1",
						optionsrc: 'im-1',
					},
					{
						option_class: "class4",
						optionsrc: 'im-7',
					},
					{
						option_class: "class5",
						optionsrc: 'im-8',
					}],
			}
		]
	},
	//slide 3
	{
		imgtype: true,
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1q3,
				option: [
					{
						option_class: "class3",
						optionsrc: 'im-9',
					},
					{
						option_class: "class1",
						optionsrc: 'im-2',
					},
					{
						option_class: "class2",
						optionsrc: 'im-10',
					},
					{
						option_class: "class4",
						optionsrc: 'im-11',
					},
					{
						option_class: "class5",
						optionsrc: 'im-12',
					}],
			}
		]
	},
	//slide 4
	{
		imgtype: false,
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1q4,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e1o1,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o2,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o3,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o4,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o5,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o6,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o7,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o8,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o9,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1o10,
					}
				],
			}
		]
	},
	//slide 5
	{
		imgtype: false,
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1q5,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e1oa1,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa2,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa3,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa4,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa5,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa6,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa7,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa8,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa9,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1oa10,
					}
				],
			}
		]
	}
];
// content.shufflearray();
$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new LampTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"eagle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-4", src: imgpath+"parrot.png", type: createjs.AbstractLoader.IMAGE},

			{id: "im-5", src: imgpath+"shape01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-6", src: imgpath+"shape02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-7", src: imgpath+"shape03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-8", src: imgpath+"shape04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "im-9", src: imgpath+"shirt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-10", src: imgpath+"shoes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-11", src: imgpath+"t_shirt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-12", src: imgpath+"pant.png", type: createjs.AbstractLoader.IMAGE},

			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"ex_1_birds.ogg"},
			{id: "sound_2", src: soundAsset+"ex_1_clothes.ogg"},
			{id: "sound_1", src: soundAsset+"ex_1_geometric_shapes.ogg"},
			{id: "sound_4", src: soundAsset+"ex_1_multiples_of_2.ogg"},
			{id: "sound_3", src: soundAsset+"ex_1_numbers_less_than_10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrong_clicked 	= false;
	var contArray = [0,1,2,3,4];
	var shufledArray = contArray.shufflearray();
	// alert(shufledArray);

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[shufledArray[countNext]]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		scoring.numberOfQuestions();

		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		sound_player("sound_"+shufledArray[countNext]);

		/*for randomizing the options*/
		if(content[shufledArray[countNext]].imgtype){
			put_image(content, shufledArray[countNext]);
			var option_position = [1,2,3,4,5];
			option_position.shufflearray();
			for(var op=0; op<5; op++){
				$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
			}
		} else{
			var option_position = [1,2,3,4,5,6,7,8,9,10];
			option_position.shufflearray();
			for(var op=0; op<10; op++){
				$('.main-container-2').eq(op).addClass('option-pos-l-'+option_position[op]);
			}
		}
		if(countNext==0){
		}

		var wrong_clicked = 0;
		$(".option").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				$(".option").css('pointer-events','none');
				current_sound.stop();
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				scoring.update(false);
                current_sound.stop();
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	};
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('exerciseblock')){
			var imageblock = content[count].exerciseblock[0];
			if(imageblock.hasOwnProperty('option')){
				var imageClass = imageblock.option;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].optionsrc).src;
					//get list of classes
					var classes_list = imageClass[i].option_class.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generalTemplate();
		// for scoringg purpose
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

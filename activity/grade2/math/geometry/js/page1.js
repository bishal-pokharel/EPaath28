var images = $ref +"/images/" ;
var soundAsset = $ref+"/sounds/"+ $lang + "/";


var content=[

	//1st slide
	{
		additionalclasscontentblock: "contentwithbg",
		uppertextblock:[
			{
				textdata : data.string.heading1,
				textclass : "mainheading"
			}
		],

	},
	//2nd slide
	{
		hasheaderblock : true,
		headerblock: [
			{
				textclass: "headertextstyle",
				textdata: data.string.heading1
			}
		],

		additionalclasscontentblock : "singleshapeintro",

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "individualshapes individualshapecircle",
						imgsrc: images + "individual_images/circle.png"
					}
				],

				imagelabels:[
					{
						imagelabelclass: "individualsingleshapename shapenamecircle",
						imagelabeldata: data.string.circle
					}
				]

			}
		]
	},
	//3rd slide
	{
		hasheaderblock : true,
		headerblock: [
			{
				textclass: "headertextstyle",
				textdata: data.string.heading1
			}
		],

		additionalclasscontentblock : "singleshapeintro",

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "individualshapes individualshaperectangle",
						imgsrc: images + "individual_images/rectangle.png"
					}
				],

				imagelabels:[
					{
						imagelabelclass: "individualsingleshapename shapenamerectangle",
						imagelabeldata: data.string.rectangle
					}
				]

			}
		]
	},

	//4th slide
	{
		hasheaderblock : true,
		headerblock: [
			{
				textclass: "headertextstyle",
				textdata: data.string.heading1
			}
		],

		additionalclasscontentblock : "singleshapeintro",

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "individualshapes",
						imgsrc: images + "individual_images/triangle.png"
					}
				],

				imagelabels:[
					{
						imagelabelclass: "individualsingleshapename shapenametriangle",
						imagelabeldata: data.string.triangle
					}
				]

			}
		]
	},
	//5th slide
	{hasheaderblock : true,
		headerblock: [
			{
				textclass: "headertextstyle",
				textdata: data.string.heading1
			}
		],

		additionalclasscontentblock : "singleshapeintro",

		definitionblock: [
			{
				definitiondata: data.string.popcircle,
				difinitionclass: "C-PopUp"
			},
			{
				definitiondata: data.string.poprect,
				difinitionclass: "R-PopUp"
			},
			{
				definitiondata: data.string.poptri,
				difinitionclass: "T-PopUp"
			}
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "individualshape1 isshowing",
						imgsrc: images + "individual_images/circle.png"
					},
					{
						imgclass: "individualshape2 isshowing",
						imgsrc: images + "individual_images/rectangle.png"
					},
					{
						imgclass: "individualshape3 isshowing",
						imgsrc: images + "individual_images/triangle.png"
					},
					{
						imgclass: "individualshape1 ishidden",
						imgsrc: images + "dropable_images/basket1.png"
					},
					{
						imgclass: "individualshape2 ishidden rectmod",
						imgsrc: images + "dropable_images/basket2.png"
					},
					{
						imgclass: "individualshape3 ishidden",
						imgsrc: images + "dropable_images/basket3.png"
					}
					],

				imagelabels:[
					{
						imagelabelclass: "individualshapename1 gnlabel ",
						imagelabeldata: data.string.circle
					},
					{
						imagelabelclass: "individualshapename2 gnlabel",
						imagelabeldata: data.string.rectangle
					},
					{
						imagelabelclass: "individualshapename3 gnlabel",
						imagelabeldata: data.string.triangle
					}
				]

			}
		]

	},
	//6th slide
	{
		hasheaderblock : true,
		headerblock: [
			{
				textclass: "headerdescription",
				textdata: data.string.heading3
			}
		],

		additionalclasscontentblock : "singleshapeintro",

		draggableimageblock: [
			{
				imagetoshow:[
					{
						imgclass: "draggablecircle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/circle.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/circle01.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/square.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/square01.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/rectangle.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/rectangle01.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/triangle.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/triangle01.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn",
						imgsrc: images + "draggable_shapes/geometrical_shapes/triangle02.png"
					}
				]
			}
		],

		definitionblock: [
			{
				definitiondata: data.string.popcircle,
				definitionclass: "C-PopUp"
			},
			{
				definitiondata: data.string.poprect,
				definitionclass: "R-PopUp"
			},
			{
				definitiondata: data.string.poptri,
				definitionclass: "T-PopUp"
			}
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "dropablecircle1 dropable movedown1",
						dragclass: "isCircle",
						imgsrc: images + "dropable_images/basket1.png"
					},
					{
						imgclass: "dropablerectangle2 dropable movedown2",
						dragclass: "isRectangle",
						imgsrc: images + "dropable_images/basket2.png"
					},
					{
						imgclass: "dropabletriangle3 dropable movedown3",
						dragclass: "isTriangle",
						imgsrc: images + "dropable_images/basket3.png"
					}
				],

				imagelabels:[
					{
						imagelabelclass: "dropableshapename01 dropableshapename",
						imagelabeldata: data.string.circle
					},
					{
						imagelabelclass: "dropableshapename02 dropableshapename",
						imagelabeldata: data.string.rectangle
					},
					{
						imagelabelclass: "dropableshapename03 dropableshapename",
						imagelabeldata: data.string.triangle
					}
				]

			}
		]

	},

];


$(function(){
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var current_sound;

	var total_page = content.length;
  	loadTimelineProgress(total_page,countNext+1);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
            // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
            //   ,
            //images

            // sounds
            {id: "S1_P1", src: soundAsset+"s1_p1.ogg"},
            {id: "S1_P2", src: soundAsset+"s1_p2.ogg"},
            {id: "S1_P3", src: soundAsset+"s1_p3.ogg"},
            {id: "S1_P4", src: soundAsset+"s1_p4.ogg"},
            {id: "S1_P5", src: soundAsset+"s1_p7.ogg"},
            {id: "S1_P6", src: soundAsset+"s2_p1.ogg"},
            {id: "S1_P7", src: soundAsset+"s3_p1.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();



	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

        // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
    }


    //controls the navigational state of the program
    function nav_button_controls(delay_ms){
        timeoutvar = setTimeout(function(){
            if(countNext==0){
                $nextBtn.show(0);
            } else if( countNext>0 && countNext == total_page-1){
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else{
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        },delay_ms);
    }

    function sound_player(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }

    function sound_player_nav(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on("complete", function(){
        	console.log('now here end',sound_id);
            nav_button_controls(1000);
        });
    }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		var countCir = 0;
		var countRec = 0;
		var countTri = 0;

		// Random Image
		function randomImage() {
			var imgArr = $("#draggable > .area");
			var imgArrSize = imgArr.length;
			var ranImgArr = [];

			for(var i=0; i<imgArrSize; i++) {
				ranImgArr.push(imgArr.eq(i));
			}

			var $draggable = $("#draggable");
			$draggable.html('');
			var randomGenerator;
			while(ranImgArr.length > 0) {
				randomGenerator = Math.floor(Math.random()*(ranImgArr.length-1));
				$draggable.append(ranImgArr[randomGenerator]);
				//Splice - makes generated no starting point
				//and deletes
				//                start          value you want to delete
				ranImgArr.splice(randomGenerator, 1);
			}
		}

		randomImage();

		function checkCount() {
			if(countCir == 2 && countRec == 4 && countTri == 3) {
					ole.footerNotificationHandler.pageEndSetNotification();
				}
		}

		switch(countNext){

			case 0:
				sound_player_nav('S1_P1');
				break;

			case 1:
			setTimeout(function(){sound_player_nav('S1_P2');},1200);


                break;

			case 2:
			setTimeout(function(){sound_player_nav('S1_P3');},1200);


                break;
			case 3:
				$(".imageblock").css({
					marginTop: "5%",
					padding : "3% 0px 0px",
					height: "60%"
				});

				$(".droparea").css({
					height: "100%"
				});

				setTimeout(function(){sound_player_nav('S1_P4');},1200);

//				setTimeout(function(){$nextBtn.show(0);}, 2000);


			break;
			case 4:
				$(".isshowing").toggleClass("ishidden", function(){
					$.when(setTimeout(function(){
						$(".isshowing").hide(0);
					}, 2000)).done(1000,function(){
						$(".ishidden").show(0);
					});
				});

				$(".imageblock").css({
					marginTop : "5%",
					height : "60%"
				});

				$(".gnlabel").css({
					marginTop : "5%"
				});

				setTimeout(function(){$nextBtn.show(0);}, 3000);
				console.log(countNext);



			break;

			case 5:

				setTimeout(
				function(){
					$(".fadeIn").removeClass("fadeIn");

							createjs.Sound.stop();
							current_sound = createjs.Sound.play("S1_P7");
							current_sound.play();
				}, 5000);

				$(".imageblock").css({
					marginTop : "5%",
					height : "35%"
				});

				$("#popup").css({
					bottom: "44%"
				});

				$(".draggablecircle, .draggablerectangle, .draggabletriangle").draggable({
					containment : "body",
					cursor : "pointer",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 10000
				});

				$('.isCircle').droppable({
					accept : ".draggablecircle",
					hoverClass : "hovered",
					drop : handleCardDrop
				});

				$('.isRectangle').droppable({
					accept : ".draggablerectangle",
					hoverClass : "hovered",
					drop : handleCardDrop
				});

				$('.isTriangle').droppable({
					accept : ".draggabletriangle",
					hoverClass : "hovered",
					drop : handleCardDrop
				});

				function topLeftCalculator(count){
					var top = count % 3;
					var factor = Math.floor(count / 3);
					var height = 0;
					var left = 0;
					switch(top){
						case 0:
						  height = 20;
						  left = (factor > 0)? ((factor*5)+ 20): 30;
						  break;
						case 1:
						  height = 30;
						  left = (factor > 0)? ((factor*7)+ 30): 35;
						  break;
						case 2:
						  height = 40;
						  left = (factor > 0)? ((factor*9)+ 40): 40;
						  break;
					}

					var returnNumber = [height, left];
					return returnNumber;

				}

				function handleCardDrop(event, ui){
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					// $(dropped.context).toggleClass('fadeIn');
					var droppedOn = $(this);
					var count = 0;

					if(dropped.hasClass("draggablecircle")) {
						count = $(".isCircle> .draggablecircle").length;
						var topLeft = topLeftCalculator(count);

						$(dropped.context).css({
							"width": "15%",
							"height": "auto",
							"max-height": "50%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : (topLeft[1]-5) + "%",
							"top" : topLeft[0] + "%",
						}).appendTo(droppedOn);

						countCir++;
						checkCount();
					}
					else if (dropped.hasClass("draggablerectangle")) {
						count = $(".isRectangle> .draggablerectangle").length;
						var topLeft = topLeftCalculator(count);

						$(dropped.context).css({
							"width": "14%",
							"height": "auto",
							"max-height": "50%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : topLeft[1] + "%",
							"top" : topLeft[0] + "%",
						}).appendTo(droppedOn);

						countRec++;
						checkCount();
					}
					else {
						count = $(".isTriangle> .draggabletriangle").length;
						var topLeft = topLeftCalculator(count);

						$(dropped.context).css({
							"width": "20%",
							"height": "auto",
							"max-height": "50%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : (topLeft[1]+ 20) + "%",
							"top" : (topLeft[0] + 7) + "%",
						}).appendTo(droppedOn);

						countTri++;
						checkCount();
					}
				};
			break;

			default:
			break;
		}

		/*
			shows the info of dropable circle on mouse interaction
		*/

		 $(".dropablecircle1")
			 .mouseover(function(){
				 $(".C-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".C-PopUp ").hide(0);
			 });

		 $(".dropablerectangle2")
			 .mouseover(function(){
				 $(".R-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".R-PopUp ").hide(0);
			 });

		 $(".dropabletriangle3")
			 .mouseover(function(){
				 $(".T-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".T-PopUp ").hide(0);
			 });
	};




	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);


        navigationcontroller();

  		loadTimelineProgress(total_page,countNext+1);
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	templateCaller();
	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
	});

});

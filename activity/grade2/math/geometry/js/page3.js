var images = $ref +"/images/" ;
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content=[

	//1st slide
	{hasuppertextblock : true,
		uppertextblock : [
			{
				textclass: "lastheaderdescription1",
				textdata: data.string.heading3
			}
		],

		additionalclasscontentblock : "contentwithplaybg",

		definitionblock: [
			{
				definitiondata: data.string.popcircle,
				definitionclass: "C-PopUp"
			},
			{
				definitiondata: data.string.poprect,
				definitionclass: "R-PopUp"
			},
			{
				definitiondata: data.string.poptri,
				definitionclass: "T-PopUp"
			}
		],

		draggableimageblock: [
			{
				imagetoshow:[
					{
						imgclass: "draggablecircle draggable fadeIn balloon01 balloon",
						imgsrc: images + "Geometrical shapes/BG and Shapes/balloon01.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn balloon02 balloon",
						imgsrc: images + "Geometrical shapes/BG and Shapes/balloon02.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn balloon03 balloon",
						imgsrc: images + "Geometrical shapes/BG and Shapes/balloon03.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn cap",
						imgsrc: images + "Geometrical shapes/BG and Shapes/cap.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn carwindow carwindow01",
						imgsrc: images + "Geometrical shapes/BG and Shapes/carwindow01.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn carwindow carwindow02",
						imgsrc: images + "Geometrical shapes/BG and Shapes/carwindow02.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn carwindow carwindow03",
						imgsrc: images + "Geometrical shapes/BG and Shapes/carwindow03.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn carwindow carwindow04",
						imgsrc: images + "Geometrical shapes/BG and Shapes/carwindow04.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn door",
						imgsrc: images + "Geometrical shapes/BG and Shapes/door.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn flag flag01",
						imgsrc: images + "Geometrical shapes/BG and Shapes/flag02.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn flag flag02",
						imgsrc: images + "Geometrical shapes/BG and Shapes/flag01.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn orange01 orange",
						imgsrc: images + "Geometrical shapes/BG and Shapes/orange01.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn orange02 orange",
						imgsrc: images + "Geometrical shapes/BG and Shapes/orange02.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn orange03 orange",
						imgsrc: images + "Geometrical shapes/BG and Shapes/orange03.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn orange04 orange",
						imgsrc: images + "Geometrical shapes/BG and Shapes/orange04.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn rooftop",
						imgsrc: images + "Geometrical shapes/BG and Shapes/rooftop.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn sun",
						imgsrc: images + "Geometrical shapes/BG and Shapes/sun.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn wheel wheelfront",
						imgsrc: images + "Geometrical shapes/BG and Shapes/wheel01.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn wheel",
						imgsrc: images + "Geometrical shapes/BG and Shapes/wheel02.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn window window01",
						imgsrc: images + "Geometrical shapes/BG and Shapes/window01.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn window",
						imgsrc: images + "Geometrical shapes/BG and Shapes/window02.png"
					}
				]
			}
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "dropablecircle1   dropable",
						dragclass: "drpCont isCircle",
						dropCont:true,
						dropContClass:"imgCont circleCont",
						imgsrc: images + "dropable_images/basket1.png"
					},
					{
						imgclass: "dropablerectangle2   dropable",
						dragclass: "drpCont isRectangle",
						dropCont:true,
						dropContClass:"imgCont rectCont",
						imgsrc: images + "dropable_images/basket2.png"
					},
					{
						imgclass: "dropabletriangle3   dropable",
						dragclass: "drpCont isTriangle",
						dropCont:true,
						dropContClass:"imgCont triangleCont",
						imgsrc: images + "dropable_images/basket3.png"
					}
				]
			}
		]
	}
];


$(function(){
	window.mobileAndTabletcheck();
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var current_sound;
	var total_page = content.length;
  	loadTimelineProgress(total_page,countNext+1);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
            // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
            //   ,
            //images

            // sounds
            // {id: "S1_P1", src: soundAsset+"s1_p1.ogg"},
            // {id: "S1_P2", src: soundAsset+"s1_p2.ogg"},
            // {id: "S1_P3", src: soundAsset+"s1_p3.ogg"},
            // {id: "S1_P4", src: soundAsset+"s1_p4.ogg"},
            // {id: "S1_P5", src: soundAsset+"s1_p7.ogg"},
            // {id: "S1_P6", src: soundAsset+"s2_p1.ogg"},
            {id: "S1_P7", src: soundAsset+"s3_p1.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();



    /*
        inorder to use the handlebar partials we need to register them
        to their respective handlebar partial pointer first
    */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
    function sound_player(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }

    function navigationController(){
        if(countNext == 0 && total_page != 1){
            setTimeout(function(){$nextBtn.show(0);}, 1000);
        }else if(countNext > 0 && countNext < (total_page-1)){
            // $prevBtn.show(0);
        }else if(countNext == total_page-1){
            // $prevBtn.show(0);
        }
    }


    function sound_player_nav(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on("complete", function(){
            console.log('now here end',sound_id);
            navigationController();
        });
    }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		var countCir = 0;
		var countRec = 0;
		var countTri = 0;

		function checkCount() {
			if(countCir == 10 && countRec == 5 && countTri == 6) {
	        ole.footerNotificationHandler.lessonEndSetNotification();
				}
		}

		switch(countNext){

			case 0:
			$('.draggable').addClass('shake');
                sound_player_nav('S1_P7');
				setTimeout(
					function(){
						$(".fadeIn").removeClass("fadeIn");

				}, 5000);

				$(".draggablecircle, .draggablerectangle, .draggabletriangle").draggable({
					containment : "body",
					cursor : "pointer",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 10000
				});

				$('.isCircle').droppable({
					accept : ".draggablecircle",
					hoverClass : "hovered",
					drop : handleCardDrop
				});

				$('.isRectangle').droppable({
					accept : ".draggablerectangle",
					hoverClass : "hovered",
					drop : handleCardDrop
				});

				$('.isTriangle').droppable({
					accept : ".draggabletriangle",
					hoverClass : "hovered",
					drop : handleCardDrop
				});

				// function topLeftCalculator(count){
				// 	var top = count % 3;
				// 	var factor = Math.floor(count / 3);
				// 	var height = 0;
				// 	var left = 0;
				// 	switch(top){
				// 			case 0:
				// 			  height = 20;
				// 			  left = (factor > 0)? ((factor*15)+ 15): 15;
				// 			  break;
				// 			case 1:
				// 			  height = 35;
				// 			  left = (factor > 0)? ((factor*15)+ 15): 15;
				// 			  break;
				// 			case 2:
				// 			  height = 50;
				// 			  left = (factor > 0)? ((factor*15)+ 15): 15;
				// 			  break;
				// 	}
				//
				// 	var returnNumber = [height, left];
				// 	return returnNumber;
				//
				// }

				function handleCardDrop(event, ui){
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					var droppedOn = $(this);
					var count = 0;

					if(dropped.hasClass("draggablecircle")) {
						// count = $(".isCircle> .draggablecircle").length;
						// var topLeft = topLeftCalculator(count);
						//
						// $(dropped.context).css({
						// 	"width": "15%",
						// 	"height": "auto",
						// 	"max-height": "50%"
						// });

						// $(dropped).detach().css({
						// 	"position" : "absolute",
						// 	"left" : (topLeft[1]-5) + "%",
						// 	"top" : topLeft[0] + "%",
						// }).appendTo(droppedOn);
						console.log($(this).find("div").attr("class"))
						$(dropped).detach().appendTo($(this).find("div"));

						countCir++;
						checkCount();
					}
					else if (dropped.hasClass("draggablerectangle")) {
						count = $(".isRectangle> .draggablerectangle").length;
						// var topLeft = topLeftCalculator(count);
						//
						// $(dropped.context).css({
						// 	"width": "14%",
						// 	"height": "auto",
						// 	"max-height": "50%"
						// });
						//
						// $(dropped).detach().css({
						// 	"position" : "absolute",
						// 	"left" : topLeft[1] + "%",
						// 	"top" : topLeft[0] + "%",
						// }).appendTo(droppedOn);
						$(dropped).detach().appendTo($(this).find("div"));

						countRec++;
						checkCount();
					}
					else {
						count = $(".isTriangle> .draggabletriangle").length;
						// var topLeft = topLeftCalculator(count);
						//
						// $(dropped.context).css({
						// 	"width": "20%",
						// 	"height": "auto",
						// 	"max-height": "50%"
						// });
						//
						// $(dropped).detach().css({
						// 	"position" : "absolute",
						// 	"left" : (topLeft[1]+ 20) + "%",
						// 	"top" : (topLeft[0] + 7) + "%",
						// }).appendTo(droppedOn);
						$(dropped).detach().appendTo($(this).find("div"));

						countTri++;
						checkCount();
					}
				}
				break;

		};

		/*
			shows the info of dropable circle on mouse interaction
		*/

		 $(".dropablecircle1")
			 .mouseover(function(){
				 $(".C-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".C-PopUp ").hide(0);
			 });

		 $(".dropablerectangle2")
			 .mouseover(function(){
				 $(".R-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".R-PopUp ").hide(0);
			 });

		 $(".dropabletriangle3")
			 .mouseover(function(){
				 $(".T-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".T-PopUp ").hide(0);
			 });
	};




	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);


		navigationController();

  		loadTimelineProgress(total_page,countNext+1);
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	templateCaller();
});

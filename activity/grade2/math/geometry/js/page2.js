var images = $ref +"/images/" ;
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content=[

	//1st slide
	{hasheaderblock : true,
		headerblock: [
			{
				textclass: "headerdescription-clone",
				textdata: data.string.heading3
			}
		],

		additionalclasscontentblock : "singleshapeintro",

		definitionblock: [
			{
				definitiondata: data.string.popcircle,
				definitionclass: "C-PopUp"
			},
			{
				definitiondata: data.string.poprect,
				definitionclass: "R-PopUp"
			},
			{
				definitiondata: data.string.poptri,
				definitionclass: "T-PopUp"
			}
		],

		draggableimageblock: [
			{
				imagetoshow:[
					{
						imgclass: "draggablecircle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/ball.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/book.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/bread.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/carpet.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/coin.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/earring.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/hanger.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/note.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/pictureframe.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/plate.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/roti.png"
					},
					{
						imgclass: "draggablecircle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/saleroti.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/signboard.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/triangle.png"
					},
					{
						imgclass: "draggabletriangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/tripod.png"
					},
					{
						imgclass: "draggablerectangle draggable fadeIn",
						imgsrc: images + "Geometrical shapes/Geometrical shapes real/tv.png"
					}
				]
			}
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "dropablecircle1 dropable",
						dragclass: "isCircle",
						imgsrc: images + "dropable_images/basket1.png"
					},
					{
						imgclass: "dropablerectangle2 dropable",
						dragclass: "isRectangle",
						imgsrc: images + "dropable_images/basket2.png"
					},
					{
						imgclass: "dropabletriangle3 dropable",
						dragclass: "isTriangle",
						imgsrc: images + "dropable_images/basket3.png"
					}
				],

				imagelabels:[
					{
						imagelabelclass: "dropableshapename01 dropableshapename",
						imagelabeldata: data.string.circle
					},
					{
						imagelabelclass: "dropableshapename02 dropableshapename",
						imagelabeldata: data.string.rectangle
					},
					{
						imagelabelclass: "dropableshapename03 dropableshapename",
						imagelabeldata: data.string.triangle
					}
				]

			}
		]



	},

];


$(function(){
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var total_page = content.length;
  	loadTimelineProgress(total_page,countNext+1);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
            // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
            //   ,
            //images

            // sounds
            {id: "S1_P1", src: soundAsset+"s1_p1.ogg"},
            {id: "S1_P2", src: soundAsset+"s1_p2.ogg"},
            {id: "S1_P3", src: soundAsset+"s1_p3.ogg"},
            {id: "S1_P4", src: soundAsset+"s1_p4.ogg"},
            {id: "S1_P5", src: soundAsset+"s1_p7.ogg"},
            {id: "S1_P6", src: soundAsset+"s2_p1.ogg"},
            {id: "S1_P7", src: soundAsset+"s3_p1.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();


    /*
        inorder to use the handlebar partials we need to register them
        to their respective handlebar partial pointer first
    */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	function navigationController(){
		if(countNext == 0 && total_page != 1){
			setTimeout(function(){$nextBtn.show(0);}, 1000);
		}else if(countNext > 0 && countNext < (total_page-1)){
			// $prevBtn.show(0);
		}else if(countNext == total_page-1){
			// $prevBtn.show(0);
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		var current_sound;

		$board.html(html);

		var countCir = 0;
		var countRec = 0;
		var countTri = 0;

		// Random Image
		function randomImage() {
			var imgArr = $("#draggable > .area");
			var imgArrSize = imgArr.length;
			var ranImgArr = [];

			for(var i=0; i<imgArrSize; i++) {
				ranImgArr.push(imgArr.eq(i));
			}

			var $draggable = $("#draggable");
			$draggable.html('');
			var randomGenerator;
			while(ranImgArr.length > 0) {
				randomGenerator = Math.floor(Math.random()*(ranImgArr.length-1));
				$draggable.append(ranImgArr[randomGenerator]);
				//Splice - makes generated no starting point
				//and deletes
				//                start          value you want to delete
				ranImgArr.splice(randomGenerator, 1);
			}
		}

		randomImage();

		function checkCount() {
			if(countCir == 5 && countRec == 6 && countTri == 5) {
					ole.footerNotificationHandler.pageEndSetNotification();
				}
		}

        function sound_player(sound_id){
            createjs.Sound.stop();
            current_sound = createjs.Sound.play(sound_id);
            current_sound.play();
        }

        function sound_player_nav(sound_id){
            createjs.Sound.stop();
            current_sound = createjs.Sound.play(sound_id);
            current_sound.play();
            current_sound.on("complete", function(){
                console.log('now here end',sound_id);
                navigationController();
            });
        }

		switch(countNext){

			case 0:
			$('.headerdescription-clone').hide(0).delay(500).fadeIn(500,function(){
				sound_player('S1_P7');
			});

				 $(".dropablecircle1")
					 .mouseover(function(){
						 $(".C-PopUp ").show(0);
					 })
					 .mouseout(function(){
						 $(".C-PopUp ").hide(0);
					 });

				 $(".dropablerectangle2")
					 .mouseover(function(){
						 $(".R-PopUp ").show(0);
					 })
					 .mouseout(function(){
						 $(".R-PopUp ").hide(0);
					 });

				 $(".dropabletriangle3")
					 .mouseover(function(){
						 $(".T-PopUp ").show(0);
					 })
					 .mouseout(function(){
						 $(".T-PopUp ").hide(0);
					 });

						$(".fadeIn").removeClass("fadeIn");


				$(".imageblock").css({
					marginTop : "5%",
					height : "35%"
				});

				$("#popup").css({
					bottom: "44%"
				});



				$(".draggablecircle, .draggablerectangle, .draggabletriangle").draggable({
					containment : "body",
					cursor : "pointer",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 10000,
					start: function(event, ui){
						$(this).css({"opacity": "0.5"});
					},
					stop: function(event, ui){
						$(this).css({"opacity": "1"});
					}
				});

				$('.isCircle').droppable({
					accept : ".draggablecircle",
					hoverClass : "hovered",
					drop : handleCardDrop
				});

				$('.isRectangle').droppable({
					accept : ".draggablerectangle",
					hoverClass : "hovered",
					drop : handleCardDrop
				});

				$('.isTriangle').droppable({
					accept : ".draggabletriangle",
					hoverClass : "hovered",
					// over: function(){
						// $(".fadeIn").remove();
					// },
					drop : handleCardDrop
				});

				function topLeftCalculator(count){
					var top = count % 3;
					var factor = Math.floor(count / 3);
					var height = 0;
					var left = 0;
					switch(top){
							case 0:
							  height = 20;
							  left = (factor > 0)? ((factor*15)+ 15): 15;
							  break;
							case 1:
							  height = 35;
							  left = (factor > 0)? ((factor*15)+ 15): 15;
							  break;
							case 2:
							  height = 50;
							  left = (factor > 0)? ((factor*15)+ 15): 15;
							  break;
					}

					var returnNumber = [height, left];
					return returnNumber;

				}

				function handleCardDrop(event, ui){
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					var droppedOn = $(this);
					var count = 0;

					if(dropped.hasClass("draggablecircle")) {
						count = $(".isCircle> .draggablecircle").length;
						var topLeft = topLeftCalculator(count);

						$(dropped.context).css({
							"width": "15%",
							"height": "auto",
							"max-height": "50%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : (topLeft[1]-5) + "%",
							"top" : topLeft[0] + "%",
						}).appendTo(droppedOn);

						countCir++;
						checkCount();
					}
					else if (dropped.hasClass("draggablerectangle")) {
						count = $(".isRectangle> .draggablerectangle").length;
						var topLeft = topLeftCalculator(count);

						$(dropped.context).css({
							"width": "14%",
							"height": "auto",
							"max-height": "50%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : topLeft[1] + "%",
							"top" : topLeft[0] + "%",
						}).appendTo(droppedOn);

						countRec++;
						checkCount();
					}
					else {
						count = $(".isTriangle> .draggabletriangle").length;
						var topLeft = topLeftCalculator(count);

						$(dropped.context).css({
							"width": "20%",
							"height": "auto",
							"max-height": "50%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : (topLeft[1]+ 20) + "%",
							"top" : (topLeft[0] + 7) + "%",
						}).appendTo(droppedOn);

						countTri++;
						checkCount();
					}
				}


			break;


		};

		/*
			shows the info of dropable circle on mouse interaction
		*/

		 $(".dropablecircle1")
			 .mouseover(function(){
				 $(".C-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".C-PopUp ").hide(0);
			 });

		 $(".dropablerectangle2")
			 .mouseover(function(){
				 $(".R-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".R-PopUp ").hide(0);
			 });

		 $(".dropabletriangle3")
			 .mouseover(function(){
				 $(".T-PopUp ").show(0);
			 })
			 .mouseout(function(){
				 $(".T-PopUp ").hide(0);
			 });
	};




	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);


		navigationController();

  		loadTimelineProgress(total_page,countNext+1);
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	templateCaller();
});

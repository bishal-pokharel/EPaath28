Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var instruction = new buzz.sound((soundAsset + "instruction.ogg"));

var content=[
		//slide 0
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "01.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class3",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class2",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class1",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 2
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "02.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class1",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class2",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class3",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 3
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "03.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class3",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class2",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class1",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 4
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "04.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class3",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class2",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class1",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 5
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "05.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class1",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class2",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class3",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 6
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "06.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class3",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class2",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class1",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 7
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "07.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class2",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class1",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class3",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 8
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "08.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class2",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class1",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class3",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 9
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "09.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class2",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class1",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class3",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},
		//slide 10
		{
			imgexerciseblock: [
				{
					ques_img: imgpath + "10.jpg",
					textdata: data.string.ques1,

					exeoptions: [
						{
							forshuffle: "class1",
							forbackground: "circleback",
							optdata: data.string.circle,
						},
						{
							forshuffle: "class2",
							forbackground: "triangleback",
							optdata: data.string.triangle,
						},
						{
							forshuffle: "class3",
							forbackground: "rectangleback",
							optdata: data.string.rectangle,
						},
					]
				}
			]

		},


];


/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;

	var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 	testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		/*var parent = $(".optionsdiv_img");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }*/


		/*======= SCOREBOARD SECTION ==============*/

		var ansClicked = false;
		var wrngClicked = false;

			instruction.play();
			$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
						$(this).css("background-position","100% 0%");
						$(this).siblings(".corctopt").css("visibility","visible");
            play_correct_incorrect_sound(1);
            
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background-position","200% 0%");
						$(this).siblings(".wrngopt").css("visibility","visible");
						wrngClicked = true;
					}

				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

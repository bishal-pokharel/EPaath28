var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainerSimplEq",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname rghtTxt fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname lftTxt fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname rghtTxt secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname lftTxt secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname fourNum",
											textclass:"qnMrkBox",
											textdata:data.string.qnmrk
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[
				{
					optaddclass:"class1"
				},{
				},{
				}]
		}],
		extratextblock:[{
				textclass:"tpQn",
				textdata:data.string.qn1
			}
		]
	},
	// slide 2
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainerSimplEq",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname rghtTxt fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname lftTxt fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname rghtTxt secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname lftTxt secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname fourNum",
											textclass:"qnMrkBox",
											textdata:data.string.qnmrk
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[
				{
					optaddclass:"class1"
				},{
				},{
				}]
		}],
		extratextblock:[{
				textclass:"tpQn",
				textdata:data.string.qn1
			}
		]
	},
	// slide 3
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainerSimplEq",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname rghtTxt fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname lftTxt fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname rghtTxt secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname lftTxt secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname fourNum",
											textclass:"qnMrkBox",
											textdata:data.string.qnmrk
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[
				{
					optaddclass:"class1"
				},{
				},{
				}]
		}],
		extratextblock:[{
				textclass:"tpQn",
				textdata:data.string.qn1
			}
		]
	},
	// slide 4
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainerSimplEq",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname rghtTxt fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname lftTxt fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname rghtTxt secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname lftTxt secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname fourNum",
											textclass:"qnMrkBox",
											textdata:data.string.qnmrk
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[
				{
					optaddclass:"class1"
				},{
				},{
				}]
		}],
		extratextblock:[{
				textclass:"tpQn",
				textdata:data.string.qn1
			}
		]
	},
	// slide 5
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv shortMidDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer hidn flexDiv-tenMid",
						}]
					},{
						subdivclass:"subDiv flexDiv-mid hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname brdrright_Sec secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm hidn thirdSubDiv farther",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip1"
										},{
											tabledataclass:"tcname brdrLft_third fvNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip2"
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		extratextblock:[{
			textclass:"submit",
			textdata:data.string.check
		},
		// {
		// 	textclass:"hint",
		// 	textdata:data.string.tryagain
		// },
		{
				textclass:"tpQn",
				textdata:data.string.qn2
		}]
	},
	// slide 6
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv shortMidDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer hidn flexDiv-tenMid",
						}]
					},{
						subdivclass:"subDiv flexDiv-mid hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname brdrright_Sec secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm hidn thirdSubDiv farther",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip1"
										},{
											tabledataclass:"tcname brdrLft_third fvNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip2"
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		extratextblock:[{
			textclass:"submit",
			textdata:data.string.check
		},
		// {
		// 	textclass:"hint",
		// 	textdata:data.string.tryagain
		// },
		{
				textclass:"tpQn",
				textdata:data.string.qn2
		}]
	},
	// slide 7
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv shortMidDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer hidn flexDiv-tenMid",
						}]
					},{
						subdivclass:"subDiv flexDiv-mid hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname brdrright_Sec secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm hidn thirdSubDiv farther",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip1"
										},{
											tabledataclass:"tcname brdrLft_third fvNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip2"
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		extratextblock:[{
			textclass:"submit",
			textdata:data.string.check
		},
		// {
		// 	textclass:"hint",
		// 	textdata:data.string.tryagain
		// },
		{
				textclass:"tpQn",
				textdata:data.string.qn2
		}]
	},
	// slide 8
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv shortMidDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer hidn flexDiv-tenMid",
						}]
					},{
						subdivclass:"subDiv flexDiv-mid hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname brdrright_Sec secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm hidn thirdSubDiv farther",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip1"
										},{
											tabledataclass:"tcname brdrLft_third fvNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip2"
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		extratextblock:[{
			textclass:"submit",
			textdata:data.string.check
		},
		// {
		// 	textclass:"hint",
		// 	textdata:data.string.tryagain
		// },
		{
				textclass:"tpQn",
				textdata:data.string.qn2
		}]
	},
	// slide 9
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv shortMidDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer hidn flexDiv-tenMid",
						}]
					},{
						subdivclass:"subDiv flexDiv-mid hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname brdrright_Sec secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm hidn thirdSubDiv farther",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip1"
										},{
											tabledataclass:"tcname brdrLft_third fvNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip2"
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		extratextblock:[{
			textclass:"submit",
			textdata:data.string.check
		},
		// {
		// 	textclass:"hint",
		// 	textdata:data.string.tryagain
		// },
		{
				textclass:"tpQn",
				textdata:data.string.qn2
		}]
	},
	// slide 10
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv shortMidDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer hidn flexDiv-tenMid",
						}]
					},{
						subdivclass:"subDiv flexDiv-mid hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname brdrright_Sec secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm hidn thirdSubDiv farther",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip1"
										},{
											tabledataclass:"tcname brdrLft_third fvNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip2"
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		extratextblock:[{
			textclass:"submit",
			textdata:data.string.check
		},
		// {
		// 	textclass:"hint",
		// 	textdata:data.string.tryagain
		// },
		{
				textclass:"tpQn",
				textdata:data.string.qn2
		}]
	},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var qnArray1 = [
		[25,12,13],[37,15,22],[98,45,53],[74,33,41],[22,10,12],
		[65,20,45],[81,51,30],[43,31,12],[68,33,35],[76,42,34]
	];
	var ansArray1 = [
		[13,23,15],[22,20,18],[53,33,50],[41,23,16],[12,2,15],
		[45,20,25],[30,21,12],[12,16,20],[35,40,90],[34,20,15]
	];
	var qnArray2 = [
		[98,69,29],[46,29,17],[34,18,16],[51,37,14],[76,27,49],
		[43,16,27],[72,46,26],[51,13,38],[62,35,27],[80,23,57]
	];
	var ansArray2 = [
		[29,18,39],[17,27,15],[16,26,20],[14,25,30],[49,59,32],
		[27,16,40],[26,32,41],[38,23,41],[27,32,25],[57,60,43]
	];

		// var scoring = new monkeyscoreTemplate(10);
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//  
			//images
			{id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ten_blocks", src: imgpath+"ten_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blue_block", src: imgpath+"blue_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pink_block", src: imgpath+"pink_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundari01", src: imgpath+"sundari01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thirty_block", src: imgpath+"thirty_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "seven_block", src: imgpath+"seven_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "twenty_blocks", src: imgpath+"twenty_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "four_block", src: imgpath+"four_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minus", src: imgpath+"minus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"1.ogg"},
			{id: "sound_1", src: soundAsset+"2.ogg"},
			{id: "sound_2", src: soundAsset+"3.ogg"},
			{id: "sound_3", src: soundAsset+"4.ogg"},
			{id: "sound_4", src: soundAsset+"5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		// scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	var score = 0;
	var testin = new EggTemplate();
	// var testin = new NumberTemplate();

  // testin.eggMove(countNext);
	testin.init(10);

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);

		// scoring.numberOfQuestions(10);
		var count=0;
		var incorClk = false;


		$(".h60").append("<img class='corincor cor' src='"+preload.getResult("correct").src+"'/>");
		$(".h60").append("<img class='corincor incor' src='"+preload.getResult("wrong").src+"'/>");

		function qnMaker(qnArray,qnprtsArr){
			var fstNum = qnArray[0];
			var secNum = qnArray[1];
			var ans = qnArray[2];

			var fstNumOnes = Math.floor(fstNum % 10);
			var fstNumTens = Math.floor(fstNum / 10);

			var secNumTens = Math.floor(secNum / 10);
			var secNumOnes = Math.floor(secNum % 10);

			var tensAns = Math.floor(ans / 10);
			var onesAns = Math.floor(ans % 10);
			qnprtsArr.push(fstNumTens,fstNumOnes,secNumTens,secNumOnes,tensAns,onesAns);
			return(qnprtsArr);
		}
		function ansAppender(AnsArray){
			$(".buttonsel:eq(0)").html(AnsArray[0]);
			$(".buttonsel:eq(1)").html(AnsArray[1]);
			$(".buttonsel:eq(2)").html(AnsArray[2]);
		}

		// random numberGenerator
		function randomGenerator(maxVal){
			var randNUm = Math.floor(Math.random() * (maxVal - 0 +1)) + 0;
			return randNUm;
		}

	 	/*for randomizing the options*/
		function randomize(parent){
			// alert(parent);
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
		switch(countNext) {
			case 0:
			case 1:
			case 2:
			case 3:
				var qnArray;
				var AnsArray;

				var qnNum = randomGenerator((qnArray1.length-1));
				(countNext==0||countNext==1)?qnArray=qnArray1[qnNum]:qnArray=qnArray2[qnNum];
				(countNext==0||countNext==1)?AnsArray=ansArray1[qnNum]:AnsArray=ansArray2[qnNum];
				// AnsArray=ansArray1[qnNum];
				ansAppender(AnsArray);
				var qnprtsArr = [];
				var qnReqs = qnMaker(qnArray,qnprtsArr);
				// qnReqs content--->(fstNumTens,fstNumOnes,secNumTens,secNumOnes,tensAns,onesAns)
				var tensAns = qnReqs[4];
				var onesAns = qnReqs[5];

				$(".fstTn").html(qnReqs[0]);
				$(".fstOn").html(qnReqs[1]);

				$(".secTn").html(qnReqs[2]);
				$(".secOns").html(qnReqs[3]);
				$(".minap").append("<img class='minus' src='"+preload.getResult("minus").src+"'/>");
				randomize(".optionsdiv");
				(countNext==0||countNext==1)?qnArray1.splice(qnNum, 1):qnArray2.splice(qnNum, 1);
				(countNext==0||countNext==1)?ansArray1.splice(qnNum, 1):ansArray2.splice(qnNum, 1);
			break;
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				input_box('.inputAns', 1, null);
				var qnArray;
				var AnsArray;
				var qnNum = randomGenerator((qnArray1.length-1));
				(countNext==4||countNext==5||countNext==6)?qnArray=qnArray1[qnNum]:qnArray=qnArray2[qnNum];
				(countNext==7||countNext==8||countNext==9)?AnsArray=ansArray1[qnNum]:AnsArray=ansArray2[qnNum];

				ansAppender(AnsArray);
				var qnprtsArr = [];
				var qnReqs = qnMaker(qnArray,qnprtsArr);
				// qnReqs content--->(fstNumTens,fstNumOnes,secNumTens,secNumOnes,tensAns,onesAns)
				var tensAns = qnReqs[4];
				var onesAns = qnReqs[5];

				$(".fstTn").html(qnReqs[0]);
				$(".fstOn").html(qnReqs[1]);

				$(".secTn").html(qnReqs[2]);
				$(".secOns").html(qnReqs[3]);
				$(".minap").append("<img class='minus' src='"+preload.getResult("minus").src+"'/>");

				$(".submit").click(function(){
					var input1 = $(".ip1").val();
					var input2 = $(".ip2").val();
					chkAns(tensAns, onesAns, input1, input2);
				});
				(countNext==4||countNext==5||countNext==6)?qnArray1.splice(qnNum, 1):qnArray2.splice(qnNum, 1);
				(countNext==7||countNext==8||countNext==9)?ansArray1.splice(qnNum, 1):ansArray2.splice(qnNum, 1);
			break;
			default:
				nav_button_controls(100);
		}

		$(".inputAns").keyup(function(){
			$(this).removeClass("iphgh");
		});
		$(".buttonsel").click(function () {
			if($(this).hasClass("class1")){
				$(this).css({
					"background-color":"#0f0",
					"border":"5px solid #0f0"
				});
				$(".buttonsel").css("pointer-events","none");
				$(this).siblings(".corctopt").show(0);

				play_correct_incorrect_sound(1);
				nav_button_controls(100);
				!incorClk?testin.update(true):"";
			}else{
				$(this).css({
					"background-color":"#f00",
					"border":"5px solid #f00"
				});
				$(this).siblings(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				testin.update(false);
				incorClk = true;
			}
		});
		function chkAns(tensAns, onesAns, input1, input2){
			if(input1 == tensAns){
				$(".ip1").removeClass("incorectClass");
				$(".ip1").addClass("corectClass");
				$(".ip1").attr("disabled","true");
				if( input2 == onesAns){
					$(".ip2").removeClass("incorectClass");
					$(".ip2").addClass("corectClass");
					$(".incor").hide(0);
					$(".cor").show(0);
					$(".hidn").removeClass("hidn");
					$(".inputAns").attr("disabled","true");
					play_correct_incorrect_sound(1);
						// nav_button_controls(100);
	    				if($total_page >= (countNext+1)){
							$nextBtn.show(0);
						}
						!incorClk?testin.update(true):"";
						incorClk = false;
				}else{
					$(".ip2").addClass("incorectClass");
					$(".thirdSubDiv").removeClass("hidn");
					$(".incor").show(0);
					play_correct_incorrect_sound(0);
					testin.update(false);
					incorClk = true;
				}
			}
			else if (input2 == onesAns) {
				$(".ip2").removeClass("incorectClass");
				testin.update(false);
				$(".ip2").addClass("corectClass");
				play_correct_incorrect_sound(0);
					$(".ip1").addClass("incorectClass");
					$(".incor").show(0);
				incorClk = true;
			}else{
				$(".ip1, .ip2").addClass("incorectClass");
				$(".incor").show(0);
				testin.update(false);
				$(".thirdSubDiv").removeClass("hidn");
				play_correct_incorrect_sound(0);
				incorClk = true;
			}
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
	 	dropedImgCount = 0;
			countNext++;
			testin.gotoNext();
			if(countNext < 10){
				templateCaller();
			} else {
				$(".scoreboard").hide(0);
				$nextBtn.hide(0);
			}
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	//  	dropedImgCount = 0;
	// });

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

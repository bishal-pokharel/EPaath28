var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		contentblockadditionalclass:"bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p3s1txt
		}]
	},
	// slide 2
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainerPrev",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minusRght',
									imgid:'minus',
									imgsrc:""
							}
						]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p3s2txt
		}]
	},
	// slide 3
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv hidn fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'thirty_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv hidn thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background fvBlks',
									imgid:'seven_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textclass:"tcol tns",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minusRght',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk tntyBlk hidn',
									imgid:'twenty_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv ",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer hidn",
							imagestoshow:[{
									imgclass:'background threeBlks hidn',
									imgid:'four_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
											// textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"sundari01",
			imgsrc:'',
			textclass:"textInSp_2",
			textdata:data.string.p3s3txt
		}]
	},
	// slide 4
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'thirty_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background fvBlks',
									imgid:'seven_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textclass:"tcol tns",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv hidn fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk tntyBlk',
									imgid:'twenty_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv hidn thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background threeBlks',
									imgid:'four_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
											// textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"sundari01",
			imgsrc:'',
			textclass:"textInSp_2",
			textdata:data.string.p3s4txt
		}]
	},
	// slide 5
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'thirty_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background fvBlks',
									imgid:'seven_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textclass:"tcol tns",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk tntyBlk',
									imgid:'twenty_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background threeBlks',
									imgid:'four_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						textindiv:true,
						mcq:true,
						subdivclass:"subDiv fstSubDiv",
						texttoshow:[{
							textclass:"qnPtag leftQn",
							textdata:data.string.p3s5txt_1
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
							textindiv:true,
							mcq:true,
							texttoshow:[{
								textclass:"qnPtag rightQn class1",
								textdata:data.string.p3s5txt_2
							}]
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.p3s5txt_2
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
											// textdata:data.string.p3s5txt_1
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"sundari01",
			imgsrc:'',
			textclass:"textInSp_2",
			textdata:data.string.p3s5txt
		}]
	},
	// slide 6
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'thirty_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
						// imageblock:[{
						// 	imageblockaddnalclass:"tenBlkContainer",
						// 	imagestoshow:[{
						// 			imgclass:'background fvBlks',
						// 			imgid:'five_block',
						// 			imgsrc:""
						// 	}]
						// }]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textclass:"tcol tns",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk tntyBlk',
									imgid:'twenty_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											inputdata:true,
											inputclass:"iput iphgh",
											tabledataclass:"tcname brdrright_third  fvNum",
											// textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"sundari01",
			imgsrc:'',
			textclass:"textInSp_2",
			textdata:data.string.p3s6txt
		}],
		extratextblock:[{
			textclass:"submit",
			textdata:data.string.submit
		},{
			textclass:"hint hidn",
			textdata:data.string.hint
		}]
	},
	// slide 7
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'thirty_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
						// imageblock:[{
						// 	imageblockaddnalclass:"tenBlkContainer",
						// 	imagestoshow:[{
						// 			imgclass:'background fvBlks',
						// 			imgid:'five_block',
						// 			imgsrc:""
						// 	}]
						// }]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textclass:"tcol tns",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk tntyBlk',
									imgid:'twenty_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"sundari01",
			imgsrc:'',
			textclass:"textInSp_2",
			textdata:data.string.p3s7txt
		}],
	},
	// slide 8
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer flexDiv-tenMid",
							imgwithtext:true,
							imagestoshow:[{
									imgTxtContainerclass:"minBlckDiv",
									imgclass:'blkMinINDiv mns',
									imgid:'minus',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont-1",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											inputdata:true,
											inputclass:"iput iphgh",
											tabledataclass:"tcname brdrLft_third fourNum",
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"sundari01",
			imgsrc:'',
			textclass:"textInSp_2",
			textdata:data.string.p3s8txt
		}],
		extratextblock:[{
			textclass:"submit sh_2",
			textdata:data.string.submit
		},{
			textclass:"hint hidn sh_2",
			textdata:data.string.hint
		}]
	},
	// slide 9
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer flexDiv-tenMid",
							imgwithtext:true,
							imagestoshow:[{
									imgTxtContainerclass:"minBlckDiv",
									imgclass:'blkMinINDiv mns',
									imgid:'minus',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont-1",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textclass:"threRem hidn",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"sundari01",
			imgsrc:'',
			textclass:"textInSp_2",
			textdata:data.string.p3s9txt
		}]
	},
	// slide 10
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer flexDiv-tenMid",
							imgwithtext:true,
							imagestoshow:[{
									imgTxtContainerclass:"minBlckDiv",
									imgclass:'blkMinINDiv mns',
									imgid:'minus',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont-1",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.two
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.four
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:'',
			imgid:"sundari01",
			imgsrc:'',
			textclass:"textInSp_2",
			textdata:data.string.p3s10txt
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var setout1;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "text_box", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ten_blocks", src: imgpath+"ten_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blue_block", src: imgpath+"blue_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pink_block", src: imgpath+"pink_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundari01", src: imgpath+"sundari01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundari02", src: imgpath+"sundari02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thirty_block", src: imgpath+"thirty_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "seven_block", src: imgpath+"seven_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "twenty_blocks", src: imgpath+"twenty_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "four_block", src: imgpath+"four_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minus", src: imgpath+"minus.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p5_ones", src: soundAsset+"s3_p5_ones.ogg"},
			{id: "s3_p5_ones_1", src: soundAsset+"s3_p5_ones_1.ogg"},
			{id: "s3_p5_tens", src: soundAsset+"s3_p5_tens.ogg"},
			{id: "s3_p5_tens_1", src: soundAsset+"s3_p5_tens_1.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p6_1", src: soundAsset+"s3_p6_1.ogg"},
			{id: "s3_p7", src: soundAsset+"s3_p7.ogg"},
			{id: "s3_p8", src: soundAsset+"s3_p8.ogg"},
			{id: "s3_p8_1", src: soundAsset+"s3_p8_1.ogg"},
			{id: "s3_p9", src: soundAsset+"s3_p9.ogg"},
			{id: "s3_p10", src: soundAsset+"s3_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);
		var count=0;
        vocabcontroller.findwords(countNext);

		// function to append images and cross line
		function imgAppender(contClass,divClass1,divClass2,imClass1,imClass2, imageId,lineClass1,lineClass2,iteration){
			for(var i=0;i<=iteration;i++){
				$(contClass).append("<div class='"+divClass1+" "+divClass2+""+i+"'></div>");
				$("."+divClass2+i).append("<img class='"+imClass1+" "+imClass2+""+i+"' src='"+preload.getResult(imageId).src+"'/>");
				$("."+divClass2+i).append("<p  class='"+lineClass1+" "+lineClass2+""+i+"'/>");
			}
		}

		// function to show cross on boxes and animating remaining ones
		function showCross(crossClass,count,iterationCount,imageClass, totalBoxes,animtopPosn){
			$("."+crossClass+count).show(100);
			setout1 = setTimeout(function(){
				count+=1;
				if(count<=iterationCount){
					showCross(crossClass,count,iterationCount,imageClass, totalBoxes,animtopPosn)
				}else{
					boxAnim(count,imageClass,totalBoxes, animtopPosn);
				}
			},1000);
		}
		function boxAnim(count,imageClass, totalBoxes,animtopPosn){
			for(var i=count;i<=totalBoxes;i++){
				$("."+imageClass+i).animate({
					top:animtopPosn+"%"
				},2000);
			};
		}
		switch(countNext) {
			case 1:
				sound_player("s3_p"+(countNext+1),1);
			break;
			case 2:
				sound_player("s3_p"+(countNext+1),1);
				$(".fstSubDiv").delay(1000).fadeIn(1000);
				$(".thirdSubDiv").delay(1500).fadeIn(1000);
				// $(".secSubDiv").hide(0);
			break;
			case 3:
				sound_player("s3_p"+(countNext+1),1);
				$(".fstSubDiv").delay(1000).fadeIn(1000);
				$(".thirdSubDiv").delay(1500).fadeIn(1000);
			break;
			case 4:
				sound_player("s3_p"+(countNext+1),0);
				$(".qnPtag").addClass("plsanim");
				$(".qnPtag").mouseenter(function(){
					$(this).css("animation","none");
				});
				$(".qnPtag").click(function(){
					if($(this).hasClass("class1")){
						$(this).siblings(".corctopt").show(0);
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						$(this).css("border","3px solid #0f0");
						$(".qnPtag").css("pointer-events","none");
						$(".textInSp_2").empty().html(eval("data.string.p3s5txt_3"));
						nav_button_controls(100);
					}
					else{
						createjs.Sound.stop();
						play_correct_incorrect_sound(0);
						$(this).siblings(".wrngopt").show(0);
						$(this).css("border","3px solid #f00");
						$(".textInSp_2").empty().html(eval("data.string.p3s5txt_4"));
					}
				});
			break;
			case 5:
				sound_player("s3_p"+(countNext+1),0);
				var hintclk = false;
				// (contClass,divClass1,divClass2,imClass, imageId,lineClass1,lineClass2,iteration)
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 3);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 6);
				input_box('.iput', 1, null);
				$(".crossLine").hide(0);

				for(var i=1;i<=3;i++){
					$(".flexDiv-top").append("<img class='prpl-"+i+"' src='"+preload.getResult("pink_block").src+"'/>");
				}
				$(".hint").click(function(){
					hintclk = true;
					clearTimeout(setout1);
					// showCross(crossClass,count,iterationCount,imageClass, totalBoxes,animtopPosn)
					showCross("crsln",0,3,"im",6,650);
				});
				$(".submit").click(function(){
					if($(".iput").val() == 3){
						$(".textInSp_2").empty().html(eval("data.string.p3s6txt_2"));
						$(".iput, .submit, .hint").css("pointer-events","none");
						clearTimeout(setout1);
						showCross("crsln",0,3,"im",6,650);
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						$(".iput").css("border","4px solid #0f0");
						!hintclk?nav_button_controls(6200):nav_button_controls(500);
					}else{
						$(".hint").fadeIn(500);
						$(".iput").css("border","4px solid #f00");
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
			break;
			case 6:
				sound_player("s3_p"+(countNext+1),1);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 3);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 6);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crsln', 2);
				$(".crsln4,.crsln5,.crsln6").hide(0);
			break;
			case 7:
				sound_player("s3_p"+(countNext+1),0);
				var hintclk = false;
				input_box('.iput', 1, null);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 3);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 6);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crsln', 2);
				$(".crsln4,.crsln5,.crsln6").hide(0);

				imgAppender(".flexDiv-tenTop",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTn', 2);

				$(".flexDiv-tenTop").append("<img class='tnB-1' src='"+preload.getResult("ten_blocks").src+"'/>");

				$(".blk10Cont").append("<p class='crossLine crslnTn0'/>");
				$(".blk10Cont-1").append("<p class='crossLine crslnTn1'/>");
				$(".crossLine").hide(0);
				$(".hint").click(function(){
					hintclk = true;
					clearTimeout(setout1);
					// showCross(crossClass,count,iterationCount,imageClass, totalBoxes,animtopPosn)
					showCross("crslnTn",0,1,"imTn",6,250);
				});
				$(".submit").click(function(){
					if($(".iput").val() == 1){
						$(".textInSp_2").empty().html(eval("data.string.p3s9txt"));
						$(".iput, .submit, .hint").css("pointer-events","none");
						$(".iput").css("border","3px solid #0f0");
						clearTimeout(setout1);
						showCross("crslnTn",0,1,"imTn",6,250);
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						!hintclk?nav_button_controls(6200):nav_button_controls(500);
					}else{
						$(".hint").fadeIn(500);
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});

			break;
			case 8:
			case 9:
				sound_player("s3_p"+(countNext+1),1);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 3);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 6);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crsln', 2);

				imgAppender(".flexDiv-tenTop",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTn', 2);
				imgAppender(".flexDivTen-btm",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLineHdn','crslnTn', 0);
				$(".crslnTn2,.crsln4,.crsln5,.crsln6").hide(0);
				$(".blk10Cont").append("<p class='crossLine crslnTn0'/>");
				$(".blk10Cont-1").append("<p class='crossLine crslnTn1'/>");
			break;
			default:
				sound_player("s3_p"+(countNext+1),1);
			break;
		}
		$(".iput").keyup(function(){
			$(this).removeClass("iphgh");
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(setout1);
	 	dropedImgCount = 0;
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	 	dropedImgCount = 0;
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}
});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		contentblockadditionalclass:"bg",
		extratextblock:[{
			textclass:"diytxt",
			textdata:data.string.diy
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'background',
				imgid:'bg_diy',
				imgsrc:""
			}]
		}]
	},
	// slide 2
	{
		contentblockadditionalclass:"bg",
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fstTn",
											textclass:"tens four",
											// textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright fstOn",
											textclass:"ones onsFv",
											// textdata:data.string.seven
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv shortMidDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer hidn flexDiv-tenMid",
						}]
					},{
						subdivclass:"subDiv flexDiv-mid hidn thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv minap",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec secTn",
											textclass:"tens one",
										},{
											tabledataclass:"tcname brdrright_Sec secOns",
											textclass:"ones onsthree",
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDivTen-btm hidn fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm hidn thirdSubDiv farther",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip1"
										},{
											tabledataclass:"tcname brdrLft_third fvNum",
											inputdata:true,
											inputclass:"inputAns iphgh ip2"
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		extratextblock:[{
			textclass:"submit",
			textdata:data.string.check
		},{
			textclass:"hint",
			textdata:data.string.pracmore
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var setout1;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ten_blocks", src: imgpath+"ten_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blue_block", src: imgpath+"blue_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pink_block", src: imgpath+"pink_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundari01", src: imgpath+"sundari01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thirty_block", src: imgpath+"thirty_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "seven_block", src: imgpath+"seven_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "twenty_blocks", src: imgpath+"twenty_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "four_block", src: imgpath+"four_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_diy", src: imgpath+"bg_diy(1).png", type: createjs.AbstractLoader.IMAGE},
			{id: "minus", src: imgpath+"minus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"1.ogg"},
			{id: "sound_1", src: soundAsset+"2.ogg"},
			{id: "sound_2", src: soundAsset+"3.ogg"},
			{id: "sound_3", src: soundAsset+"4.ogg"},
			{id: "sound_4", src: soundAsset+"5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);
		var count=0;
        vocabcontroller.findwords(countNext);

        // function to append images and cross line
		function imgAppender(contClass,divClass1,divClass2,imClass1,imClass2, imageId,lineClass1,lineClass2,iteration){
			for(var i=0;i<=iteration;i++){
				$(contClass).append("<div class='"+divClass1+" "+divClass2+""+i+"'></div>");
				$("."+divClass2+i).append("<img class='"+imClass1+" "+imClass2+""+i+"' src='"+preload.getResult(imageId).src+"'/>");
				$("."+divClass2+i).append("<p  class='"+lineClass1+" "+lineClass2+""+i+"'/>");
			}
		}

		// function to show cross on boxes and animating remaining ones
		function showCross(crossClass,count,iterationCount,imageClass, totalBoxes,animtopPosn){
			$("."+crossClass+count).show(100);
			setout1 = setTimeout(function(){
				count+=1;
				if(count<=iterationCount){
					showCross(crossClass,count,iterationCount,imageClass, totalBoxes,animtopPosn)
				}else{
					boxAnim(count,imageClass,totalBoxes, animtopPosn);
				}
			},1000);
		}
		function boxAnim(count,imageClass, totalBoxes,animtopPosn){
			for(var i=count;i<=totalBoxes;i++){
				$("."+imageClass+i).animate({
					top:animtopPosn+"%"
				},2000);
			};
		}
		// random numberGenerator
		function randomGenerator(){
			var randNUm = Math.floor(Math.random() * (4 - 0 +1)) + 0;
			return randNUm;
		}
		switch(countNext) {
			case 0:
			play_diy_audio();
				nav_button_controls(100);
			break;
			case 1:
			 	var qno =  randomGenerator();
				var qnArray=[[67,24,43],
										[78,52,26],
										[35,14,21],
										[99,11,88],
										[28,13,15]];
				var fstNum = qnArray[qno][0];
				var secNum = qnArray[qno][1];
				var ans = qnArray[qno][2];

				var fstNumOnes = Math.floor(fstNum % 10);
				var fstNumTens = Math.floor(fstNum / 10);

				var secNumTens = Math.floor(secNum / 10);
				var secNumOnes = Math.floor(secNum % 10);

				var onesAns = Math.floor(ans / 10);
				var tensAns = Math.floor(ans % 10);

				var fstAnsBlk = fstNumTens-secNumTens;
				var secAnsBlk = fstNumOnes-secNumOnes;
				input_box('.inputAns', 1, null);

				$(".fstTn").html(fstNumTens);
				$(".fstOn").html(fstNumOnes);

				$(".secTn").html(secNumTens);
				$(".secOns").html(secNumOnes);
				$(".minap").append("<img class='minus' src='"+preload.getResult("minus").src+"'/>");

				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', (fstNumOnes-1));
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', (secNumOnes-1));
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crsln',( secAnsBlk-1));

				imgAppender(".flexDiv-tenTop",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTn', (fstNumTens-1));
				imgAppender(".flexDiv-tenMid",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTn', (secNumTens-1));
				imgAppender(".flexDivTen-btm",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLineHdn','crslnTn',( fstAnsBlk-1));
				$(".crossLine").hide(0);
				for(var i=0; i<=(secNumTens-1) ;i++ ){
					$(".crslnTn"+i).show(0);
				}
				for(var i=0; i<=(secNumOnes-1) ;i++ ){
					$(".crsln"+i).show(0);
				}

				$(".h60").append("<img class='corincor cor' src='"+preload.getResult("correct").src+"'/>");
				$(".h60").append("<img class='corincor incor' src='"+preload.getResult("wrong").src+"'/>");

				$(".submit").click(function(){
				$(".hidn").removeClass("hidn");
					var input1 = $(".ip1").val();
					var input2 = $(".ip2").val();
					if(input1 == fstAnsBlk){
						$(".ip1").removeClass("incorectClass");
						$(".ip1").addClass("corectClass");
						$(".ip1").attr("disabled",'true');
						if( input2 == secAnsBlk){
							$(".ip2").removeClass("incorectClass");
							$(".ip2").addClass("corectClass");
							$(".incor").hide(0);
							$(".cor").show(0);
							play_correct_incorrect_sound(1);
							$(".hint").show();
							// $(".hidn").removeClass("hidn");
							nav_button_controls(100);
							$(".inputAns").attr('disabled','true');
							$(".ip1,.ip2").attr("disabled",'true');
						}else{
							play_correct_incorrect_sound(0);
							$(".ip2").addClass("incorectClass");
							$(".thirdSubDiv").removeClass("hidn");
							$(".incor").show(0);
						}
					}
					else if (input2 == secAnsBlk) {
						$(".ip2").removeClass("incorectClass");
						play_correct_incorrect_sound(0);
						$(".ip2").addClass("corectClass");
						$(".ip2").attr('disabled','true');
						$(".ip2").attr("disabled",'true');
						$(".ip1").addClass("incorectClass");
						$(".incor").show(0);
					}else{
						play_correct_incorrect_sound(0);
						$(".ip1, .ip2").addClass("incorectClass");
						$(".incor").show(0);
						$(".thirdSubDiv").removeClass("hidn");
					}
				});
				$(".hint").click(function(){
					templateCaller();
				});
			break;
			default:
				nav_button_controls(100);
		}
		$(".inputAns").keyup(function(){
			$(this).removeClass("iphgh");
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
	 	dropedImgCount = 0;
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	 	dropedImgCount = 0;
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

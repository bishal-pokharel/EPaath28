var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		extratextblock:[{
			textclass:"midTxt oneAndTen",
			textdata:data.string.p2s1txt
		}]
	},
	// slide 2
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s2txt
		}]
	},
	// slide 3
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s3txt
		}]
	},
	// slide 4
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'foutry_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background fvBlks',
									imgid:'five_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textClass:"tcol tns",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											// textClass:"tcol ons",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus hidn',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk oneBlk',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background threeBlks',
									imgid:'three_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum hidn",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
											// textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s3txt
		}]
	},
	// slide 5
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'foutry_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background fvBlks',
									imgid:'five_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textClass:"tcol tns",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											// textClass:"tcol ons",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk oneBlk',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background threeBlks',
									imgid:'three_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
											// textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s5txt
		}]
	},
	// slide 6
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'foutry_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background fvBlks',
									imgid:'five_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textclass:"tcol tns",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk oneBlk',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv thirdSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background threeBlks',
									imgid:'three_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
											// textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s6txt
		}]
	},
	// slide 7
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'foutry_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",

					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textclass:"tcol tns",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk oneBlk',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",

					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third  fvNum",
											// textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s7txt
		}]
	},
	// slide 8
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'foutry_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											// textclass:"tcol tns",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk oneBlk',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem hidn",
											textdata:data.string.two
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s8txt
		}]
	},
	// slide 9
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'foutry_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk oneBlk',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.two
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s9txt
		}]
	},
	// slide 10
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'background frty',
									imgid:'foutry_block',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer",
							imagestoshow:[{
									imgclass:'tnBlk minus',
									imgid:'minus',
									imgsrc:""
							},{
									imgclass:'tnBlk oneBlk',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											// textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.two
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s10txt
		}]
	},
	// slide 11
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer flexDiv-tenMid",
							imgwithtext:true,
							imagestoshow:[{
									imgTxtContainerclass:"minBlckDiv",
									imgclass:'blkMinINDiv mns',
									imgid:'minus',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											textclass:"threRem hidn",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.two
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s11txt
		}]
	},
	// slide 12
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer flexDiv-tenMid",
							imgwithtext:true,
							imagestoshow:[{
									imgTxtContainerclass:"minBlckDiv",
									imgclass:'blkMinINDiv mns',
									imgid:'minus',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
						// imageblock:[{
						// 	imageblockaddnalclass:"tenBlkContainer",
						// 	imagestoshow:[{
						// 			imgclass:'background threeBlks',
						// 			imgid:'three_block',
						// 			imgsrc:""
						// 	}]
						// }]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											textclass:"threRem",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.two
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s12txt
		}]
	},
	// slide 13
	{
		contentblockadditionalclass:"blue-bg",
		imageblock:[{
			imagestoshow:[{
				imgclass:'sundari02',
				imgid:'sundari02',
				imgsrc:""
			}]
		}],
		masterdivcontainerclass:"masterDivContainer",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
						// }]
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer flexDiv-tenMid",
							imgwithtext:true,
							imagestoshow:[{
									imgTxtContainerclass:"minBlckDiv",
									imgclass:'blkMinINDiv mns',
									imgid:'minus',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-btmTen fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											textclass:"threRem",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.two
										}
									]
								}
							]
						}]
					}
				]
			}
		],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s13txt
		}]
	},
	// slide 14
	{
		contentblockadditionalclass:"blue-bg",
		masterdivcontainerclass:"masterDivContainer lstPg",
		imageload:true,
		divscontainer:[
			{
				divscontainerclass:"divsContainer topContainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-tenTop fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-top thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"topName  tn",
									tabledata:[{
											tabledataclass:"tcname brdrLft tendata",
											textclass:"tcol tns",
											textdata:data.string.tens
										},{
											tabledataclass:"tcname brdrright onedata",
											textclass:"tcol ons",
											textdata:data.string.ones
										}
									]
								},{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft fourNum",
											textclass:"tens four",
											textdata:data.string.four
										},{
											tabledataclass:"tcname brdrright fvNum",
											textclass:"ones onsFv",
											textdata:data.string.five
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer midcontainer",
				subdivs:[
					{
						subdivclass:"subDiv  fstSubDiv",
						imageblock:[{
							imageblockaddnalclass:"tenBlkContainer flexDiv-tenMid",
							imgwithtext:true,
							imagestoshow:[{
									imgTxtContainerclass:"minBlckDiv",
									imgclass:'blkMinINDiv mns',
									imgid:'minus',
									imgsrc:""
							},{
									imgTxtContainerclass:"minBlckDiv blk10Cont",
									imgclass:'blkMinINDiv blk10',
									imgid:'ten_blocks',
									imgsrc:""
							}]
						}]
					},{
						subdivclass:"subDiv flexDiv-mid thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv",
						table:[{
							tablerow:[
								{
									tablerowclass:"numbers",
									tabledata:[{
											tabledataclass:"tcname brdrLft_Sec fourNum",
											textclass:"tens one",
											textdata:data.string.one
										},{
											tabledataclass:"tcname brdrright_Sec fvNum",
											textclass:"ones onsthree",
											textdata:data.string.three
										}
									]
								}
							]
						}]
					}
				]
			},
			{
				divscontainerclass:"divsContainer btmcontainer",
				subdivs:[
					{
						subdivclass:"subDiv flexDiv-btmTen fstSubDiv",
					},{
						subdivclass:"subDiv flexDiv-btm thirdSubDiv",
					},{
						subdivclass:"subDiv secSubDiv tpBlkBrdr h60",
						table:[{
							tblextraclass:"top9",
							tablerow:[
								{
									tablerowclass:"numbers btmNum",
									tabledata:[{
											tabledataclass:"tcname brdrLft_third fourNum",
											textclass:"threRem",
											textdata:data.string.three
										},{
											tabledataclass:"tcname brdrright_third fvNum",
											textclass:"twRem",
											textdata:data.string.two
										}
									]
								}
							]
						}]
					}
				]
			}
		],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "text_box", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box03", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ten_blocks", src: imgpath+"ten_blocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blue_block", src: imgpath+"blue_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pink_block", src: imgpath+"pink_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundari01", src: imgpath+"sundari01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundari02", src: imgpath+"sundari02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "foutry_block", src: imgpath+"foutry_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "five_block", src: imgpath+"five_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "three_block", src: imgpath+"three_block.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minus", src: imgpath+"minus.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s2_p7", src: soundAsset+"s2_p7.ogg"},
			{id: "s2_p8", src: soundAsset+"s2_p8.ogg"},
			{id: "s2_p9", src: soundAsset+"s2_p9.ogg"},
			{id: "s2_p10", src: soundAsset+"s2_p10.ogg"},
			{id: "s2_p11", src: soundAsset+"s2_p11.ogg"},
			{id: "s2_p12", src: soundAsset+"s2_p12.ogg"},
			{id: "s2_p13", src: soundAsset+"s2_p13.ogg"},
			{id: "s2_p14_1", src: soundAsset+"s2_p14_1.ogg"},
			{id: "s2_p14_2", src: soundAsset+"s2_p14_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		content[countNext].imageload?put_image_third(content, countNext):'';
		put_speechbox_image(content, countNext);
		var count=0;
        vocabcontroller.findwords(countNext);

		// function to appear box number
		function showBoxNum(boxno,soundno,count,iteration){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundno+count);
			$(boxno+(count+1)).show(0);
			current_sound.play();
			current_sound.on('complete', function(){
				if(count<4){
					count+=1;
					showBoxNum(boxno,soundno,count);
				}else{
					nav_button_controls(100);
				}
			});
		}
		// function to append images and cross line
		function imgAppender(contClass,divClass1,divClass2,imClass1,imClass2, imageId,lineClass1,lineClass2,iteration){
			for(var i=0;i<=iteration;i++){
				$(contClass).append("<div class='"+divClass1+" "+divClass2+""+i+"'></div>");
				$("."+divClass2+i).append("<img class='"+imClass1+" "+imClass2+""+i+"' src='"+preload.getResult(imageId).src+"'/>");
				$("."+divClass2+i).append("<p  class='"+lineClass1+" "+lineClass2+""+i+"'/>");
			}
		}
		function showCross(crossClass,count,iterationCount){
			// function to show cross on blocks
			$("."+crossClass+count).show(100);
			setTimeout(function(){
				count+=1;
				if(count<=iterationCount){
					showCross(crossClass,count,iterationCount)
				}else{
					// nav_button_controls(100);
				}
			},1000);
		}
		switch(countNext) {
			case 0:
				sound_player("s2_p1",1);
			break;
			case 1:
				sound_player("s2_p2",1);
			break;
			case 3:
				$(".h60").addClass("tpBlkBrdr");
				$(".minus").delay(1000).show(0);
				$(".btmNum").delay(2000).show(0);
				nav_button_controls(2000);
			break;
			case 5:
				sound_player("s2_p"+(countNext+1),1);
				$(".ones").addClass("pulseHghlght");
			break;
			case 6:
				sound_player("s2_p"+(countNext+1),1);
				$(".ones").addClass("pulseHghlght");
				// (contClass,divClass1,divClass2,imClass, imageId,lineClass1,lineClass2,iteration)
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 2);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 4);
				$(".crossLine").hide(0);
				setTimeout(function(){
					showCross('crsln',0,2);
				},1000);
			break;
			case 7:
				sound_player("s2_p"+(countNext+1),1);
				$(".ones").addClass("pulseHghlght");
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 2);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 4);
				for(var i=1;i<=2;i++){
					$(".flexDiv-top").append("<img class='xtrablk-"+i+"' src='"+preload.getResult("pink_block").src+"'/>");
				}
				$(".crossLine").hide(0);
				$(".crsln0,.crsln1,.crsln2").show(0);
				$(".im3,.im4").animate({
					top:"620%"
				},2000,function(){
					$(".twRem").fadeIn(500);
					// nav_button_controls(1500);
				});
			break;
			case 8:
				sound_player("s2_p"+(countNext+1),1);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 4);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 2);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crslnShow', 1);
				$(".crossLine").hide(0);
				$(".crsln0,.crsln1,.crsln2").show(0);
				$(".tens").addClass("pulseHghlght");
					// nav_button_controls(100);
			break;
			case 9:
				sound_player("s2_p"+(countNext+1),1);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 4);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 2);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crslnShow', 1);
				$(".crossLine").hide(0);
				$(".crsln0,.crsln1,.crsln2").show(0);
				$(".tens").addClass("pulseHghlght");
					// nav_button_controls(100);
			break;
			case 10:
				sound_player("s2_p"+(countNext+1),1);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 4);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 2);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crslnShow', 1);

				imgAppender(".flexDiv-tenTop",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTn', 3);
				$(".blk10Cont").append("<p class='crossLine crslnTn0'/>");

				$(".crossLine").hide(0);
				$(".crsln0,.crsln1,.crsln2").show(0);
				$(".tens").addClass("pulseHghlght");
				setTimeout(function(){
					showCross('crslnTn',0,0);
					$(".threRem").delay(1500).show(0);
				},1000);
			break;
			case 11:
				sound_player("s2_p"+(countNext+1),1);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 4);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 2);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crslnShow', 1);

				imgAppender(".flexDiv-tenTop",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTn', 3);
				$(".blk10Cont").append("<p class='crossLine crslnTn0'/>");

				$(".crossLine").hide(0);
				$(".crsln0,.crsln1,.crsln2, .crslnTn0").show(0);
				$(".tens").addClass("pulseHghlght");
				for(var i=1;i<=3;i++){
					$(".flexDiv-tenTop").append("<img class='tblk-"+i+"' src='"+preload.getResult("ten_blocks").src+"'/>");
				}
				$(".imTn1,.imTn2,.imTn3").animate({
					top:"250%"
				},2000,function(){
				// nav_button_controls(100);
				});
			break;
			case 12:
				sound_player("s2_p"+(countNext+1),1);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 4);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 2);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crslnShow', 1);

				imgAppender(".flexDiv-tenTop",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTn', 3);
				imgAppender(".flexDiv-btmTen",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTnSh', 2);
				$(".blk10Cont").append("<p class='crossLine crslnTn0'/>");
				$(".crossLine").hide(0);
				$(".crsln0,.crsln1,.crsln2, .crslnTn0").show(0);
				$(".tens").addClass("pulseHghlght");
					// nav_button_controls(100);
			break;
			case 13:
				// sound_player("s2_p"+(countNext+1)+"_1",1);
				imgAppender(".flexDiv-top",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 4);
				imgAppender(".flexDiv-mid",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLine','crsln', 2);
				imgAppender(".flexDiv-btm",'tnblkContDiv_pink','snglBlk','tnBlkInCont_pink','im',"pink_block",'crossLineHdn','crslnShow', 1);

				imgAppender(".flexDiv-tenTop",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTn', 3);
				imgAppender(".flexDiv-btmTen",'tnblkContDiv','tenBlk','tnBlkInCont','imTn',"ten_blocks",'crossLine','crslnTnSh', 2);
				$(".blk10Cont").append("<p class='crossLine crslnTn0'/>");

				$(".crossLine").hide(0);
				$(".crsln0,.crsln1,.crsln2, .crslnTn0").show(0);
				nav_button_controls(1000);
			break;
			default:
				sound_player("s2_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}

	function put_image_third(content, count){
		if(content[count].hasOwnProperty('divscontainer')){
			for(var k=0;k<content[count].divscontainer.length;k++){
				if(content[count].divscontainer[k].hasOwnProperty('subdivs')){
					for(var i=0; i<content[count].divscontainer[k].subdivs.length;i++){
						if(content[count].divscontainer[k].subdivs[i].hasOwnProperty('imageblock'))
						{
							var imageblock = content[count].divscontainer[k].subdivs[i].imageblock[0];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var j=0; j<imageClass.length; j++){
									var image_src = preload.getResult(imageClass[j].imgid).src;
									//get list of classes
									var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
										// alert(i);
								}
							}
						}
					}
				}
			}
		}
	}




	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
	 	dropedImgCount = 0;
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	 	dropedImgCount = 0;
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
	 	dropedImgCount = 0;
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

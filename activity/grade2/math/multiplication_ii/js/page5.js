var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 0
	{
		contentblockadditionalclass:"whitebg",
		speechbox:[{
			speechbox:'speechbox_1',
			imgid:'dialoguebox',
			textclass:'insidetext',
			textdata:data.string.p5text1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "fairy",
				imgid: 'fairy',
				imgsrc: '',
			}]
		}]
	},
	//slide 1
	{
		contentblockadditionalclass:"whitebg",
		imageblock:[{
			imagestoshow:[{
				imgclass: "ten_balls",
				imgid: 'ten_balls',
			}]
		}],
		uppertextblock:[{
			textdata:data.string.p5text2,
			textclass:'top_text'
		},{
			textclass:'mid_box'
		},{
			textdata:data.string.p4text7,
			textclass:'option1'
		},{
			textdata:data.string.p5text3,
			textclass:'option2'
		},{
			textdata:data.string.p5text4,
			textclass:'option3'
		},{
			textdata:data.string.p5text5,
			textclass:'option4 correct'
		}]
	},
	//slide 2
	{
		contentblockadditionalclass:"whitebg",
		imageblock:[{
			imagestoshow:[{
				imgclass: "ten_balls1",
				imgid: 'ten_balls',
			},{
				imgclass: "ten_balls2",
				imgid: 'ten_balls',
			}]
		}],
		uppertextblock:[{
			textdata:data.string.p5text2,
			textclass:'top_text'
		},{
			textclass:'mid_box'
		},{
			textdata:data.string.p5text5,
			textclass:'option1'
		},{
			textdata:data.string.p5text3,
			textclass:'option2'
		},{
			textdata:data.string.p5text4,
			textclass:'option3'
		},{
			textdata:data.string.p5text11,
			textclass:'option4 correct'
		}]
	},

	//slide 3
	{
		contentblockadditionalclass:"whitebg",
		speechbox:[{
			speechbox:'speechbox_2',
			imgid:'dialoguebox',
			textclass:'insidetext',
			textdata:data.string.p5text12
		}],
		uppertextblock:[{
			textclass:'mid_box'
		},{
			textclass:'top_text',
			textdata:data.string.p5text2
		},{
			textclass:'label_group',
			textdata:data.string.p5text13
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "fairy1",
				imgid: 'fairy',
				imgsrc: '',
			},{
				imgclass: "ten_balls1",
				imgid: 'ten_balls',
			},{
				imgclass: "ten_balls2",
				imgid: 'ten_balls',
			}]
		}]
	},

	//slide 4
	{
		contentblockadditionalclass:"whitebg",
		uppertextblock:[{
			textclass:'mid_box'
		},{
			textclass:'right_box'
		},{
			textclass:'right_texts',
			textdata:data.string.p5text14
		},{
			textclass:'top_text',
			textdata:data.string.p5text2
		},{
			textclass:'label_group',
			textdata:data.string.p5text13
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "ten_balls1",
				imgid: 'ten_balls',
			},{
				imgclass: "ten_balls2",
				imgid: 'ten_balls',
			}]
		}]
	},

	//slide 5
	{
		contentblockadditionalclass:"whitebg",
		uppertextblock:[{
			textclass:'mid_box'
		},{
			textclass:'right_box'
		},{
			textclass:'right_texts',
			textdata:data.string.p5text17
		},{
			textclass:'top_text',
			textdata:data.string.p5text15
		},{
			textclass:'label_group',
			textdata:data.string.p5text16
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "ten_balls1",
				imgid: 'ten_balls',
			},{
				imgclass: "ten_balls2",
				imgid: 'ten_balls',
			},{
				imgclass: "ten_balls3",
				imgid: 'ten_balls',
			}]
		}]
	},

	//slide 6
	{
		contentblockadditionalclass:"whitebg",
		uppertextblock:[{
			textclass:'mid_box'
		},{
			textclass:'right_box'
		},{
			textclass:'right_texts',
			textdata:data.string.p5text17
		},{
			textclass:'top_text',
			textdata:data.string.p5text19
		},{
			textclass:'label_group',
			textdata:data.string.p5text16
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "fairyNEW",
				imgid: 'fairy',
				imgsrc: '',
			},{
				imgclass: "ten_balls1",
				imgid: 'ten_balls',
			},{
				imgclass: "ten_balls2",
				imgid: 'ten_balls',
			},{
				imgclass: "ten_balls3",
				imgid: 'ten_balls',
			}]
		}],
		speechbox:[{
			speechbox:'speechboxNEW',
			imgid:'dialoguebox',
			textclass:'insidetext',
			textdata:data.string.p5text18
		}]
	},

	//slide 0
	{
		contentblockadditionalclass:"whitebg",
		speechbox:[{
			speechbox:'speechbox_1',
			imgid:'dialoguebox',
			textclass:'insidetext',
			textdata:data.string.p5text20
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "fairy",
				imgid: 'fairy',
				imgsrc: '',
			}]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var basket_count = 0;
	var apple_count = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src:imgpath+'flying-chibi-fairy.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "dialoguebox", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ten_balls", src: imgpath+"ten_balls.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p6", src: soundAsset+"s5_p6.ogg"},
			{id: "s5_p7", src: soundAsset+"s5_p7.ogg"},
			{id: "s5_p8", src: soundAsset+"s5_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_speechbox_image(content, countNext);
		put_image(content, countNext);
		$('.option1,.option2,.option3,.option4').click(function(){
			if($(this).hasClass('correct'))
			{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(-14%,-245%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$('.option1,.option2,.option3,.option4').css({"pointer-events":"none"});
				$(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else {
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(-14%,-245%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				play_correct_incorrect_sound(0);
				$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
			}
		});

		switch (countNext) {
			case 0:
			case 3:
			case 7:
				sound_player("s5_p"+(countNext+1),1);
			break;
			case 4:
				sound_player("s5_p"+(countNext+1),1);
			$('.mid_box').animate({'left': '5%','top': '66%'},1000);
			$('.fairy1').animate({'left': '51%','top': '19%'},1000);
			$('.label_group').animate({'left': '35%','top': '66%'},1000);
			$('.ten_balls1').animate({'left': '11.6667%','top': '76%'},1000);
			$('.firstone,.secondone,.thirdone,.right_box').css('opacity','0');
			$('.ten_balls2').animate({'left': '38.3334%','top': '76%'},1000,function(){

				$('.right_box').animate({'opacity':'1'},500);
				$('.firstone').delay(500).animate({'opacity':'1'},1000);
				$('.secondone').delay(4000).animate({'opacity':'1'},1000);
				$('.thirdone').delay(10000).animate({'opacity':'1'},1000,function(){
					// nav_button_controls(100);
				});

			});
				break;

			case 5:
				sound_player("s5_p"+(countNext+1),1);
			$('.mid_box').css({'left': '5%','top': '47%','height':'50%'});
			$('.fairy1').css({'left': '51%','top': '19%'});
			$('.label_group').css({'left': '35%','top': '47%'});
			$('.ten_balls1').css({'left': '11.6667%','top': '76%'});
			$('.ten_balls2').css({'left': '38.3334%','top': '76%'});
			$('.firstone,.secondone,.thirdone,.right_box').css('opacity','0');
			$('.right_box').animate({'opacity':'1'},500);
			$('.firstone').delay(4000).animate({'opacity':'1'},1000);
			$('.secondone').delay(7500).animate({'opacity':'1'},1000);
			$('.thirdone').delay(17000).animate({'opacity':'1'},1000,function(){
				// nav_button_controls(100);
			});
			break;
			case 6:
				sound_player("s5_p"+(countNext+1),1);
			$('.mid_box').css({'left': '5%','top': '47%','height':'50%'});
			$('.fairy1').css({'left': '51%','top': '19%'});
			$('.label_group').css({'left': '35%','top': '47%'});
			$('.ten_balls1').css({'left': '11.6667%','top': '76%'});
			$('.ten_balls2').css({'left': '38.3334%','top': '76%'});
			$('.snell_text').hide(0).fadeIn(500);
			break;
			default:
				sound_player("s5_p"+(countNext+1),0);
			break;
		}



	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].arrow_nav[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].arrow_nav[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
    }

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

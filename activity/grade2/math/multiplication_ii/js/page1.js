var soundAsset = $ref+"/sounds/"+$lang+"/";
var imgpath = $ref+"/images/";


var content=[
	//slide0
	{
		imageblock:[{
			imagestoshow:[{
				imgclass: "bg_full",
				imgid : 'cover',
				imgsrc: ""
			}]
		}],
		contentblockadditionalclass:'bg_color',
		uppertextblock:[{
			textdata:data.string.p1text,
			textclass:'middle_text'
		}]

	},
	//slide1
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text1,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textdata:data.string.p1text2,
			textclass:'option_1 correct',
		},{
			textdata:data.string.p1text3,
			textclass:'option_2',
		},{
			textdata:data.string.p1text4,
			textclass:'option_3',
		},{
			textdata:data.string.p1text5,
			textclass:'option_4',
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'t_ball',
				imgclass:'animated bounce t_ball'
			},{
				imgid:'box',
				imgclass:'box'
			}]
		}]
	},
	//slide2
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text1,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textdata:data.string.p1text2,
			textclass:'option_1 ',
		},{
			textdata:data.string.p1text3,
			textclass:'option_2 correct',
		},{
			textdata:data.string.p1text4,
			textclass:'option_3',
		},{
			textdata:data.string.p1text5,
			textclass:'option_4',
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'t_ball',
				imgclass:'animated bounce t_ball'
			},{
				imgid:'box',
				imgclass:'box'
			},{
				imgid:'t_ball',
				imgclass:'animated bounce t_ball2'
			},{
				imgid:'box',
				imgclass:'box2'
			}]
		}]
	},

	//slide3
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text7,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textdata:data.string.p1text9,
			textclass:'label'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'t_ball',
				imgclass:'animated bounce t_ball'
			},{
				imgid:'box',
				imgclass:'box'
			},{
				imgid:'t_ball',
				imgclass:'animated bounce t_ball2'
			},{
				imgid:'box',
				imgclass:'box2'
			},{
				imgid:'fairy',
				imgclass:'fairy1'
			}]
		}],
		speechbox:[{
			speechbox:'speechbox_3',
			imgid:'dialoguebox',
			textclass:'insidetext',
			textdata:data.string.p1text8
		}]
	},

	//slide4
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text7,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textclass:'right_box'
		},{
			textdata:data.string.p1text9,
			textclass:'label'
		},{
			textdata:data.string.p1text10,
			textclass:'right_texts',
			datahighlightflag:true,
			datahighlightcustomclass:'first_text',
			datahighlightcustomclass2:'second_text',
			datahighlightcustomclass3:'third_text'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'t_ball',
				imgclass:'animated bounce t_ball'
			},{
				imgid:'box',
				imgclass:'box'
			},{
				imgid:'t_ball',
				imgclass:'animated bounce t_ball2'
			},{
				imgid:'box',
				imgclass:'box2'
			}]
		}]
	},

	//slide5
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text11,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textclass:'right_box'
		},{
			textdata:data.string.p1text16,
			textclass:'label'
		},{
			textdata:data.string.p1text12,
			textclass:'right_question',
		},{
			textdata:data.string.p1text2,
			textclass:'option1',
		},{
			textdata:data.string.p1text4,
			textclass:'option2',
		},{
			textdata:data.string.p1text31,
			textclass:'option3 correct',
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'t_ball',
				imgclass:'animated roll1'
			},{
				imgid:'box',
				imgclass:'box'
			},{
				imgid:'t_ball',
				imgclass:'animated roll2'
			},{
				imgid:'box',
				imgclass:'box2'
			}]
		}]
	},

	//slide6
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text14,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textdata:data.string.p1text16,
			textclass:'label'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'box',
				imgclass:'box'
			},{
				imgid:'box',
				imgclass:'box2'
			},{
				imgid:'fairy',
				imgclass:'fairy1'
			}]
		}],
		speechbox:[{
			speechbox:'speechbox_3',
			imgid:'dialoguebox',
			textclass:'insidetext',
			textdata:data.string.p1text15
		}]
	},

	//slide7
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text14,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textclass:'right_box'
		},{
			textdata:data.string.p1text16,
			textclass:'label'
		},{
			textdata:data.string.p1text17,
			textclass:'right_texts',
			datahighlightflag:true,
			datahighlightcustomclass:'first_text',
			datahighlightcustomclass2:'second_text',
			datahighlightcustomclass3:'third_text'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'box',
				imgclass:'box'
			},{
				imgid:'box',
				imgclass:'box2'
			}]
		}]
	},

	//slide8
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text18,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textdata:data.string.p1text19,
			textclass:'label'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'box',
				imgclass:'box'
			},{
				imgid:'box',
				imgclass:'box2'
			},{
				imgid:'box',
				imgclass:'box3'
			},{
				imgid:'fairy',
				imgclass:'fairy'
			}]
		}],
		speechbox:[{
			speechbox:'speechbox_1',
			imgid:'dialoguebox',
			textclass:'insidetext',
			textdata:data.string.p1text20
		}]
	},

	//slide9
	{
		contentblockadditionalclass:'bg_color_pink',
		uppertextblock:[{
			textdata:data.string.p1text18,
			textclass:'top_text'
		},{
			textclass:'ball_box'
		},{
			textclass:'		animation-delay: 2s;right_box'
		},
		{
			textclass:'right_box'
		},{
			textdata:data.string.p1text19,
			textclass:'label'
		},{
			textdata:data.string.p1text23,
			textclass:'right_texts',
			datahighlightflag:true,
			datahighlightcustomclass:'first_text',
			datahighlightcustomclass2:'second_text',
			datahighlightcustomclass3:'third_text'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'box',
				imgclass:'box'
			},{
				imgid:'box',
				imgclass:'box2'
			},{
				imgid:'box',
				imgclass:'box3'
			}]
		}]
	},

	//slide10
	{
		contentblockadditionalclass:'bg_color_pink',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy_center'
			}]
		}],
		speechbox:[{
			speechbox:'speechbox_2',
			imgid:'dialoguebox',
			textclass:'insidetext',
			textdata:data.string.p1text22
		}]
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "box", src: imgpath+"box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "t_ball", src: imgpath+"t_ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialoguebox", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy", src: imgpath+"flying-chibi-fairy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "snell", src: imgpath+"mr.know-it-all.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cover", src: imgpath+"cover_page.png",  type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);



	$('.option_1,.option_2,.option_3,.option_4').click(function(){
		if($(this).hasClass('correct'))
		{
			var $this = $(this);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
			var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
			$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(-14%,-245%)" src="'+imgpath +'correct.png" />').insertAfter(this);
			$('.option_1,.option_2,.option_3,.option_4').css({"pointer-events":"none"});
			$(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
			play_correct_incorrect_sound(1);
			nav_button_controls(100);
		}
		else {
			var $this = $(this);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
			var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
			$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(-14%,-245%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
			play_correct_incorrect_sound(0);
			$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
		}
	});

	$('.option1,.option2,.option3, .option4').click(function(){
		if($(this).hasClass('correct'))
		{
			var $this = $(this);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
			var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
			$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(166%,-75%)" src="'+imgpath +'correct.png" />').insertAfter(this);
			$('.option1,.option2,.option3,.option4').css({"pointer-events":"none"});
			$(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
			play_correct_incorrect_sound(1);
			nav_button_controls(100);
		}
		else {
			var $this = $(this);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
			var centerY = ((position.top + height)*100)/$('.coverboardfull').height()+'%';
			$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(166%,-75%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
			play_correct_incorrect_sound(0);
			$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
		}
	});
		switch(countNext) {
			case 1:
				sound_player("s1_p"+(countNext+1),0);
			break;
			case 2:
				sound_player("s1_p"+(countNext+1),1);
			$('.box').css('left','14%');
			$('.t_ball').css('left','22%');
			break;
			case 3:
			case 6:
				countNext==3?sound_player("s1_p"+(countNext+1),1):sound_player("s1_p"+(countNext+1),1);
			$('.box').css('left','14%');
			$('.t_ball').css('left','22%');
			$('.ball_box').css('top','46%');
			break;
			case 4:
			case 7:
				sound_player("s1_p"+(countNext+1),1);
			$('.box').css('left','14%');
			$('.t_ball').css('left','22%');
			$('.ball_box').css('top','46%');
			$('.first_text,.second_text,.third_text').css('opacity','0');
			$('.first_text').animate({'opacity':'1'},1000);
			$('.second_text').delay(4000).animate({'opacity':'1'},1000);
			$('.third_text').delay(11700).animate({'opacity':'1'},1000);
			// nav_button_controls(3000);
			break;
			case 5:
				sound_player("s1_p"+(countNext+1),1);
			$('.box').css('left','14%');
			$('.t_ball').css('left','22%');
			$('.ball_box').css('top','46%');
			break;

			case 8:
				sound_player("s1_p"+(countNext+1),1);
			$('.box').css({'left':'11.5%','width':'15%'});
			$('.box2').css({'left':'29%','width':'15%'});
			$('.box3').css({'left':'46.5%','width':'15%'});
			$('.ball_box').css('top','46%');
			break;

			case 9:
				sound_player("s1_p"+(countNext+1),1);
			$('.box').css({'left':'11.5%','width':'15%'});
			$('.box2').css({'left':'29%','width':'15%'});
			$('.box3').css({'left':'46.5%','width':'15%'});
			$('.ball_box').css('top','46%');
			$('.first_text,.second_text,.third_text').css('opacity','0');
			$('.first_text').animate({'opacity':'1'},1000);
			$('.second_text').delay(4000).animate({'opacity':'1'},1000);
			$('.third_text').delay(13000).animate({'opacity':'1'},1000);
			break;
			default:
			// nav_button_controls(1000);
				sound_player("s1_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
			// $nextBtn.trigger('click');
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

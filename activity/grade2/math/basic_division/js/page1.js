var soundAsset = $ref+"/sounds/"+$lang+"/";
var soundAsset_numbers = $ref+"/audio_numbers/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide1
	{
		extratextblock:[{
			textdata:data.lesson.chapter,
			textclass:'covertxt'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'cover_page',
				imgclass:'background'
			}]
		}]
	},
	// slide2
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1txt1,
		}]
	},
	// slide3
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s2txt,
		}]
	},
	// slide4
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s3txt,
		}]
	},
	// slide5
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s4txt,
		}],
		imgcontainer:[{
			img_containerclass:"cupContainer"
		}]
	},
	// slide6
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s5txt,
		}],
		imgcontainer:[{
			img_containerclass:"cupContainer"
		}],
		traycupcontainer:[
			{
				trayclasses:"trays tray0",
				cupintray:[{
					cupintrayclass:"cupsInTray ct0"
				}]
			},{
			trayclasses:"trays tray1",
			cupintray:[{
				cupintrayclass:"cupsInTray ct1"
			}]
			},{
			trayclasses:"trays tray2",
			cupintray:[{
				cupintrayclass:"cupsInTray ct2"
			}]
			},{
			trayclasses:"trays tray3",
			cupintray:[{
				cupintrayclass:"cupsInTray ct3"
			}]
			}
		]
	},
	// slide7
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s6txt,
		}],
		imgcontainer:[{
			img_containerclass:"cupContainer"
		}],
		traycupcontainer:[
			{
				trayclasses:"trays tray0",
				cupintray:[{
					cupintrayclass:"cupsInTray ct0"
				}]
			},{
			trayclasses:"trays tray1",
			cupintray:[{
				cupintrayclass:"cupsInTray ct1"
			}]
			},{
			trayclasses:"trays tray2",
			cupintray:[{
				cupintrayclass:"cupsInTray ct2"
			}]
			},{
			trayclasses:"trays tray3",
			cupintray:[{
				cupintrayclass:"cupsInTray ct3"
			}]
			}
		]
	},
	// slide8
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s7txt,
		}],
		traycupcontainer:[
			{
				trayclasses:"trays tray0",
				cupintray:[{
					cupintrayclass:"cupsInTray ct0"
				}]
			},{
			trayclasses:"trays tray1",
			cupintray:[{
				cupintrayclass:"cupsInTray ct1"
			}]
			},{
			trayclasses:"trays tray2",
			cupintray:[{
				cupintrayclass:"cupsInTray ct2"
			}]
			},{
			trayclasses:"trays tray3",
			cupintray:[{
				cupintrayclass:"cupsInTray ct3"
			}]
			}
		]
	},
	// slide9
	{
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.p1s9toptxt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s9txt,
		}],
		imgcontainer:[{
			img_containerclass:"cupContainer"
		}],
		traycupcontainer:[
			{
				trayclasses:"trays tray0",
				cupintray:[{
					cupintrayclass:"cupsInTray ct0"
				}]
			},{
			trayclasses:"trays tray1",
			cupintray:[{
				cupintrayclass:"cupsInTray ct1"
			}]
			},{
			trayclasses:"trays tray2",
			cupintray:[{
				cupintrayclass:"cupsInTray ct2"
			}]
			},{
			trayclasses:"trays tray3",
			cupintray:[{
				cupintrayclass:"cupsInTray ct3"
			}]
			}
		]
	},
	// slide10
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'background'
			}]
		}],
		speechbox:[{
			speechbox:'sp-3',
			imgclass:"spImage",
			imgid:'sp_bubble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s10txt,
		}],
		equation:true,
		eqncontainerclass:"btmEqnConatiner",
		equation:[
			{
				eqncontentclass:"eqnContent",
				eqncontent:[{
					textclass:"topEqnTxt",
					textdata:data.string.twl,
				},{
					textclass:"btmEqnTxt",
					textdata:data.string.dividend,
				}]
		},{
			eqncontentclass:"eqnContent",
			eqncontent:[{
				textclass:"topEqnTxt",
				textdata:data.string.div,
			},{
				textclass:"btmEqnTxt",
				textdata:data.string.divsymb,
			}]
		},{
			eqncontentclass:"eqnContent",
			eqncontent:[{
				textclass:"topEqnTxt",
				textdata:data.string.four,
			},{
				textclass:"btmEqnTxt",
				textdata:data.string.divisor,
			}]
		},{
			eqncontentclass:"eqnContent",
			eqncontent:[{
				textclass:"topEqnTxt",
				textdata:data.string.equals,
			}]
		},{
			eqncontentclass:"eqnContent",
			eqncontent:[{
				textclass:"topEqnTxt",
				textdata:data.string.thre,
			},{
				textclass:"btmEqnTxt",
				textdata:data.string.quotient,
			}]
		}]
	},
	// slide11
	{
		contentblockadditionalclass:'creamBg',
		imageblock:[{
			imagestoshow:[{
				imgid:'board',
				imgclass:'textBoard'
			}],
			imagelabels:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"brdEqn",
				imagelabelclass:"brdTxt",
				imagelabeldata:data.string.boardTxt
			}]
		}],
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"eqTxt",
			textclass:"btmeqn1 top2",
			textdata:data.string.p1s10btmtxt,
		},
		// {
		// 	datahighlightflag:'true',
		// 	datahighlightcustomclass:"eqTxt1",
		// 	textclass:"btmeqn1 top12",
		// 	textdata:data.string.p1s11btmtxt_sec,
		// }
	]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+'new/bg01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "sp_bubble1", src: imgpath+'new/bubble01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "cup", src: imgpath+'new/cup.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tray", src: imgpath+'new/tray.png', type: createjs.AbstractLoader.IMAGE},
			{id: "board", src: imgpath+'new/board.png', type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+'new/cover_page.png', type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "Divident", src: soundAsset+"Divident.ogg"},
			{id: "Divisor", src: soundAsset+"Divisor.ogg"},
			{id: "Quotient", src: soundAsset+"Quotient.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
        vocabcontroller.findwords(countNext);
        var imageNum = 0;
		var cpInccCount = 0;
		var trayCount = 0;
		var cInTrCount = 0;
		var itnCount = 0;
		function showOneByONe(picClass, count){
			$(picClass+imageNum).css("opacity","1");
			imageNum+=1;
			setTimeout(function(){
				if(imageNum<= 11){
					showOneByONe(picClass, imageNum);
				}else{
					nav_button_controls();
				}
			},500);
		}

		switch(countNext) {
			case 1:
				sound_player("s1_p"+(countNext+1),1);
				// nav_button_controls(100);
			break;
			case 4:
				sound_player("s1_p"+(countNext+1),0);
				// nav_button_controls(1000);
				var container = $(".cupContainer");
				var img_arr = [];
				for(i=0; i<=11; i++){
					container.append("<img class = 'cups cup"+i+"' src ='"+preload.getResult('cup').src+"'/>");
				}
				$(".cups").css("opacity","0");
        showOneByONe(".cup", 11);
				// current_sound = createjs.Sound.play("sound_0");
				// current_sound.play();
				// current_sound.on('complete', function(){
				// 	showOneByONe(".cup", 11);
				// });
			break;
			case 5:
				sound_player("s1_p"+(countNext+1),1);
				// nav_button_controls(1000);
				var container = $(".cupContainer");
				for(i=0; i<=11; i++){
					container.append("<img class = 'cups cup"+i+"' src ='"+preload.getResult('cup').src+"'/>");
				}
				var containerSec = $(".traycupcontainer");
				for(var i=0; i<=3; i++){
					$(".tray"+i).append("<img class='trayImg trayImg"+i+"' src='"+preload.getResult('tray').src+"'/>");
				}
				$(".trayImg").css("opacity","0");
					showOneByONe(".trayImg", 3);
			break;
			case 6:
				// sound_player("s1_p"+(countNext+1),1);
				// nav_button_controls(1000);
				var cupscount = 0;
				var container = $(".cupContainer");
				for(i=0; i<=11; i++){
					container.append("<img class = 'cups cup"+i+"' src ='"+preload.getResult('cup').src+"'/>");
				}
				var containerSec = $(".traycupcontainer");
				for(var i=0; i<=3; i++){
					$(".tray"+i).append("<img class='trayImg trayImg"+i+"' src='"+preload.getResult('tray').src+"'/>");
				}
				for(var i=0; i<=3;i++){
					for(var j=0; j<=2; j++){
						$(".ct"+i).append("<img class = 'cIt cInT"+j+"' src ='"+preload.getResult('cup').src+"'/>");
					}
				}
				$(".cIt").css("opacity","0");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
					// showOneByONe(".cup", 11);
					showHIde(cpInccCount,trayCount, cInTrCount, itnCount);
				});
				// setTimeout(function(){
				// 	showHIde(cpInccCount,trayCount, cInTrCount, itnCount);
				// },500);
			break;
			case 7:
				sound_player("s1_p"+(countNext+1),1);
				// nav_button_controls(1000);
				var containerSec = $(".traycupcontainer");
				for(var i=0; i<=3; i++){
					$(".tray"+i).append("<img class='trayImg trayImg"+i+"' src='"+preload.getResult('tray').src+"'/>");
				}
				for(var i=0; i<=3;i++){
					for(var j=0; j<=2; j++){
						$(".ct"+i).append("<img class = 'cIt cInT"+j+"' src ='"+preload.getResult('cup').src+"'/>");
					}
				}
			break;
			case 8:
				// sound_player("s1_p"+(countNext+1),1);
				// nav_button_controls(1000);
				var cupscount = 0;
				$(".cupContainer").css("left","100%");
				$(".traycupcontainer").css("left","-51%");
				var container = $(".cupContainer");
				for(i=0; i<=11; i++){
					container.append("<img class = 'cups cup"+i+"' src ='"+preload.getResult('cup').src+"'/>");
				}
				var containerSec = $(".traycupcontainer");
				for(var i=0; i<=3; i++){
					$(".tray"+i).append("<img class='trayImg trayImg"+i+"' src='"+preload.getResult('tray').src+"'/>");
				}
				for(var i=0; i<=3;i++){
					for(var j=0; j<=2; j++){
						$(".ct"+i).append("<img class = 'cIt cInT"+j+"' src ='"+preload.getResult('cup').src+"'/>");
					}
				}
				$(".cIt").css("opacity","0");

				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
					showHIde(cpInccCount,trayCount, cInTrCount, itnCount);
				});

				$(".cupContainer").delay(1000).animate({
					left:"53%"
				},1000,function(){
					$(".cupContainer").delay(500).append("<p class='textInContainer'>"+data.string.twl_cups+"</p>");
					$(".traycupcontainer").delay(1000).animate({
						left:"1%"
					},1000,function(){
						$(".traycupcontainer").delay(500).append("<p class='textInContainer'>"+data.string.fr_trays+"</p>");
					});
				});
			break;
			case 9:
				sound_player("s1_p"+(countNext+1),1);
				$(".eqnContent:eq(3)").css("pointer-events","none");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
				$(".eqnContent").mouseenter(function(){
					$(this).addClass("borderclass");
					$(this).children(".btmEqnTxt").css("opacity","1").addClass("result");
					if($(".result").length==4){
            nav_button_controls(100);
          }
				})
				.mouseleave(function(){
					$(this).removeClass("borderclass");
					$(this).children(".btmEqnTxt").css("opacity","0");
					});
				});
			break;
			case 10:
				sound_player("s1_p"+(countNext+1),1);
				$(".eqTxt:eq(0)").css("left","0%");
				$(".eqTxt:eq(1)").css("left","25%");
				$(".eqTxt:eq(2)").css("left","49%");
				$(".eqTxt:eq(3)").css("left","60%");
				$(".eqTxt:eq(4)").css("left","60%");
					$(".eqTxt:eq(0)").append("<span class='belwTxt'>"+data.string.dividend+"</span>");
					$(".eqTxt:eq(1)").append("<span class='belwTxt'>"+data.string.divsymb+"</span>");
					$(".eqTxt:eq(2)").append("<span class='belwTxt'>"+data.string.divisor+"</span>");
					$(".eqTxt:eq(4)").append("<span class='belwTxt'>"+data.string.quotient+"</span>");
					// $(".eqTxt:eq(4)").children(".belwTxt").css("left","50%");

					$(".eqTxt").children().hide();
            $(".eqTxt:eq(0), .eqTxt:eq(1)").children().fadeIn(1000);
            $(".eqTxt:eq(4)").children().fadeIn(1000);
            $(".eqTxt:eq(2)").children().fadeIn(1000);
						// createjs.Sound.stop();
						// current_sound = createjs.Sound.play("sound_0");
						// current_sound.play();
						// current_sound.on('complete', function(){
						// 	$(".eqTxt:eq(0), .eqTxt:eq(1)").children().fadeIn(1000);
						// 		createjs.Sound.stop();
						// 		current_sound_1 = createjs.Sound.play("sound_0");
						// 		current_sound_1.play();
						// 		current_sound_1.on('complete', function(){
						// 			$(".eqTxt:eq(2)").children().fadeIn(1000);
						// 				createjs.Sound.stop();
						// 				current_sound_2 = createjs.Sound.play("sound_0");
						// 				current_sound_2.play();
						// 				current_sound_2.on('complete', function(){
						// 					$(".eqTxt:eq(4)").children().fadeIn(1000);
						// 					nav_button_controls(1000);
						// 				});
						// 		});
						// });
			break;
			default:
				sound_player("s1_p"+(countNext+1),1);
		}
	}

	function showHIde(cpInccCount,trayCount, cInTrCount, itnCount){
		trayCount==4?trayCount=0:'';
		$(".cup"+cpInccCount).css("opacity","0");
		$(".ct"+trayCount).find("img:eq("+cInTrCount+")").delay(1000).css("opacity","1");
		cpInccCount+=1;
		trayCount+=1;
		setTimeout(function(){
			if(cpInccCount<=11){
				itnCount++;
				itnCount<=3?cInTrCount=0:itnCount<=7?cInTrCount=1:itnCount<=11?cInTrCount=2:'';
				showHIde(cpInccCount,trayCount, cInTrCount, itnCount);
			}else{
				nav_button_controls();
			}
		},500);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//cover
	{
		imageblock:[{
			imagestoshow:[{
				imgclass: "bg",
				imgid : 'bg_diy01',
				imgsrc: ""
			}]
		}],
		extratextblock:[{
			textclass: "diytxt",
			textdata: data.string.diytxt
		}]
	},
	// slide1
	{
		uppertextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"qntxt",
			textclass:"instruction",
			textdata:data.string.p2s0q1
		}],
		imagecontainer:[{
			imagecontainerclass:"friendsContainer"
		},{
			imagecontainerclass:"pencilContainer"
		}],
		extratextblock:[{
			textclass:"clickBtn",
			textdata:data.string.clk
		}]
	},
	// slide2
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"qntxt",
			textclass:"instruction",
			textdata:data.string.p2s3top
		}],
		dragdiv:[
			{
				dragCLass:"dragable drgb1",
				textclass:"drgbOpn",
				dataanswer:"dividend"
			},{
				dragCLass:"dragable drgb2",
				textclass:"drgbOpn",
				dataanswer:"divisor"
			},{
				dragCLass:"dragable drgb3",
				textclass:"drgbOpn",
				dataanswer:"quotient"
			}
		],
		dropdiv:[
			{
				dropCLass:"dropable drop1",
				textclass:"dropName",
				textdata:data.string.Dividend,
				dataanswer:"dividend"
			},{
				dropCLass:"signs sgn1",
				textclass:"dropName",
				textdata:data.string.divide
			},{
				dropCLass:"dropable drop2",
				textclass:"dropName",
				dataanswer:"divisor",
				textdata:data.string.Divisor,
			},{
				dropCLass:"signs sgn2",
				textclass:"dropName",
				textdata:data.string.equal
			},{
				dropCLass:"dropable drop3",
				textclass:"dropName",
				dataanswer:"quotient",
				textdata:data.string.Quotient,
			}
		]
	},
	// slide3
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"qntxt",
			textclass:"instruction",
			textdata:data.string.p2s3top
		}],
		midtxt:[
			{
				textclass:"mdTxt mT1",
			},{
				textclass:"sgn1sec",
				textdata:data.string.divide
			},{
				textclass:"mdTxt mT2",
			},{
				textclass:"sgn2sec",
				textdata:data.string.equal
			},{
				textclass:"mdTxt mT3",
			}
		],
		extratextblock:[{
			textclass:"correctTxt",
			textdata:data.string.correct
		}]
	},
	// slide4
	{
		contentblockadditionalclass:"creamBg",
		extratextblock:[{
			textclass:"pracMore",
			textdata:data.string.practise
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"grilLast",
				imgsrc:'',
				imgid:"niti05"
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'spbox',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2spbox,
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var drgDrpCount = 0;



	var dividend = 0;
	var divisor = 0;
	var imageName;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "frnd_1", src:imgpath+'new/pream.png', type: createjs.AbstractLoader.IMAGE},
			{id: "frnd_2", src:imgpath+'new/rumi.png', type: createjs.AbstractLoader.IMAGE},
			{id: "frnd_3", src:imgpath+'new/sagar.png', type: createjs.AbstractLoader.IMAGE},
			{id: "frnd_4", src:imgpath+'new/suraj.png', type: createjs.AbstractLoader.IMAGE},
			{id: "frnd_5", src:imgpath+'new/asha.png', type: createjs.AbstractLoader.IMAGE},
			{id: "frnd_0", src:imgpath+'new/niti01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "pencil", src:imgpath+'new/pencil.png', type: createjs.AbstractLoader.IMAGE},
			{id: "book", src:imgpath+'new/book.png', type: createjs.AbstractLoader.IMAGE},
			{id: "eraser", src:imgpath+'new/eraser.png', type: createjs.AbstractLoader.IMAGE},
			{id: "niti05", src:imgpath+'new/niti05.png', type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src:imgpath+'new/bubble01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "a_01", src:imgpath+'new/a_01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bg_diy01", src:imgpath+'new/bg_diy01.png', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			// {id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			// {id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			// {id: "sound_5", src: soundAsset+"p1_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		var cpInccCount = 0;
		var trayCount = 0;
		var cInTrCount = 0;
		var itnCount = 0;

	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 0 +1)) + 0;
	    return randNum;
	  }

		/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
		switch (countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
			var randNum = rand_generator(2);
			var randItemNum  = Math.floor(Math.random() * (2 - 0 +1)) + 0;
			var imgArray = [[[18,6],[12,4],[15,5]],
											[[12,4],[10,5],[8,4]],
											[[16,4],[18,6],[15,5]]
										];
			divisor = imgArray[randItemNum][randNum][1];
			dividend = imgArray[randItemNum][randNum][0];
			switch (randItemNum) {
				case 0:
					imageGenerators(randItemNum, randNum, imgArray, "pencil", data.string.pencils);
					$(".pencilsAns").css("opacity","0");
					imageName = data.string.pencils;
				break;
				case 1:
						imageGenerators(randItemNum, randNum, imgArray, "book", data.string.books);
						$(".pencils").css("width","30%");
						$(".pencilsAns").css("width","80%");
						$(".pencilsAns").css("opacity","0");
						imageName = data.string.books;
				break;
				case 2:
					imageGenerators(randItemNum, randNum, imgArray, "eraser", data.string.erasers);
						$(".pencils").css("width","15%");
						$(".pencilsAns").css("width","60%");
						$(".pencilsAns").css("opacity","0");
						imageName = data.string.erasers;
				break;
				default:
			}
			$(".clickBtn").on('click', function(){
				showHIde(cpInccCount,trayCount, cInTrCount, itnCount, imgArray,randItemNum,randNum);
			});
			break;
			case 2:
				if(dividend==16 && divisor==4){
					$(".drgb3, .drop3").find("p").attr("data-answer","divisor")
				}
				$(".qntxt:eq(0)").html(dividend+" "+imageName);
				$(".qntxt:eq(1)").html(divisor);
				$(".qntxt:eq(2)").html(imageName);

				$(".drgb1").find('p').html(dividend);
				$(".drgb2").find('p').html(divisor);
				$(".drgb3").find('p').html(dividend/divisor);
				randomize(".dragdiv");
				$(".dragable").draggable({
		      revert : true,
					zIndex:"100",
					start:function(event,ui){
						$(this).css("cursor","move");
					}
				});
				$(".dropable").droppable({
					accept:".dragable",
					hoverClass:"hovered",
					drop: function(event, ui){
						dropFunc(event, ui, $(this));
					}
				});
			break;
			case 3:
				$(".qntxt:eq(0)").html(dividend+" "+imageName);
				$(".qntxt:eq(1)").html(divisor);
				$(".qntxt:eq(2)").html(dividend/divisor+" "+imageName);
					$(".mT1").html(dividend);
					$(".mT2").html(divisor);
					$(".mT3").html(dividend/divisor);
					play_correct_incorrect_sound(1);
					nav_button_controls(1000);
			break;
			case 4:
				sound_player("s2_p5",1);
				$(".pracMore").on('click', function(){
						countNext = 1;
						templatecaller();
						createjs.Sound.stop();
				});
        break;
			default:
			nav_button_controls(500);

		}

		function dropFunc(event, ui, thisClass){
			var dragClass = ui.draggable.children().attr("data-answer").toString().trim();
			var dropClass = thisClass.children().attr("data-answer").toString().trim();
			if(dragClass==dropClass){
				var text = ui.draggable.children().text();
				ui.draggable.detach();
				thisClass.append("<p class='drpdtxt'>"+text+"</p>");
				drgDrpCount+=1;

				play_correct_incorrect_sound(1);
				thisClass.css({
					"background":"#98c02e",
					"border":"4px solid #ff0"
				});
				if(drgDrpCount==3){
					drgDrpCount = 0;
					nav_button_controls(100);}
			}else{
			play_correct_incorrect_sound(0);
				thisClass.css({
					"background":"#f00",
					"border":"4px solid #980000"
				});
			}
		}
	}

	function showHIde(cpInccCount,trayCount, cInTrCount, itnCount ,imgArray, randItemNum, randNum){
		trayCount==(imgArray[randItemNum][randNum][1])?trayCount=0:'';
		$(".pncl"+cpInccCount).css("opacity","0");
		$(".anc"+trayCount).find("img:eq("+cInTrCount+")").delay(1000).css("opacity","1");
		cpInccCount+=1;
		trayCount+=1;
		setTimeout(function(){
			if(cpInccCount<=imgArray[randItemNum][randNum][0]){
				itnCount++;
				if(itnCount==imgArray[randItemNum][randNum][1]){
					itnCount = 0;
					cInTrCount+=1;
				}
				showHIde(cpInccCount,trayCount, cInTrCount, itnCount,imgArray, randItemNum, randNum);
			}
			else{
				nav_button_controls(100);
			}
		},500);
	}

	function imageGenerators(randItemNum, randNum, imgArray, imageId, imgNames){
		$(".qntxt:eq(0)").html((imgArray[randItemNum][randNum][0])+" "+imgNames);
		$(".qntxt:eq(1)").html(imgArray[randItemNum][randNum][1]);
		$(".qntxt:eq(2)").html(imgNames);
			for(var i=0; i<imgArray[randItemNum][randNum][1]; i++){
				$(".friendsContainer").append("<div class='frndDivContainer fdc"+i+"' />");
				$(".fdc"+i).append("<div class='manyImageContainer mic"+i+"' />");
				$(".fdc"+i).append("<div class='answerContainer anc"+i+"' />");
				$(".mic"+i).append("<img class='friends frnd"+i+"' src='"+preload.getResult("frnd_"+i).src+"' />");
			}

			for(var i=0; i<imgArray[randItemNum][randNum][0]; i++){
				$(".pencilContainer").append("<img class='pencils pncl"+i+"' src='"+preload.getResult(imageId).src+"' />");
			}

			for(var i=0; i<imgArray[randItemNum][randNum][1]; i++){
				for(var j=0;j<(imgArray[randItemNum][randNum][0]/imgArray[randItemNum][randNum][1]);j++){
					$(".anc"+i).append("<img class='pencilsAns pnclans"+j+"' src='"+preload.getResult(imageId).src+"' />");
				}
			}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].arrow_nav[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].arrow_nav[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
    }

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

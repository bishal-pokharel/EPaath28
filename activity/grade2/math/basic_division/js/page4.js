var soundAsset = $ref+"/audio_en/";
var soundAsset_numbers = $ref+"/audio_numbers/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	//cover
	{
		imageblock:[{
			imagestoshow:[{
				imgclass: "bg",
				imgid : 'bg_diy',
				imgsrc: ""
			}]
		}],
		extratextblock:[{
			textclass: "diytxt",
			textdata: data.string.diytxt
		}]
	},
	//slide 1
	{
		contentblockadditionalclass:'creamBg',
		extratextblock:[{
			textclass:"qntxt",
			textdata:data.string.p4s1qn
		}],
		imagecontainer:[{
			imagecontainerclass:"container leftCont"
		},{
			imagecontainerclass:"container rightCont"
		}],
		btmeqncontainer:[{
			textclass:"eqn",
			textdata:data.string.s4fsteqn
		},{
			textclass:"numBox",
			// textdata:data.string.p4s1qn
		},{
			textclass:"check",
			textdata:data.string.check
		}]
	},
	//slide 2
	{
		contentblockadditionalclass:'creamBg',
		extratextblock:[{
			textclass:"qntxt",
			textdata:data.string.p4s3qn
		}],
		imagecontainer:[{
			imagecontainerclass:"container leftCont"
		},{
			imagecontainerclass:"container rightCont"
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.s4seceqn
	      },
	      {
					optdata:data.string.s4thirdeqn
	    	}
			]
		}],
	},
	//slide 3
	{
		contentblockadditionalclass:'creamBg',
		extratextblock:[{
			textclass:"qntxt",
			textdata:data.string.p4s3qn
		}],
		imagecontainer:[{
			imagecontainerclass:"container leftCont"
		},{
			imagecontainerclass:"container rightCont"
		}],
		btmeqncontainer:[{
			textclass:"eqn",
			textdata:data.string.p4s3_eqn
		},{
			textclass:"numBox",
			// textdata:data.string.p4s1qn
		},{
			textclass:"check",
			textdata:data.string.check
		}]
	},
	//slide 4
	{
		contentblockadditionalclass:'creamBg',
		extratextblock:[{
			textclass:"qntxt",
			textdata:data.string.p4s4qn
		}],
		imagecontainer:[{
			imagecontainerclass:"container leftCont h34"
		},{
			imagecontainerclass:"container rightCont h34"
		}],
		btmeqncontainer:[{
			textclass:"check",
			textdata:data.string.check
		},{
			textclass:"instrn",
			textdata:data.string.p4s4instrn
		},{
			textclass:"numBox nb1",
		},{
			textclass:"numBox nb2",
		}]
	},
	//slide 5
	{
		contentblockadditionalclass:'creamBg',
		extratextblock:[{
			textclass:"qntxt",
			textdata:data.string.p4s4qn
		}],
		imagecontainer:[{
			imagecontainerclass:"container leftCont h34"
		},{
			imagecontainerclass:"container rightCont h34"
		}],
		btmeqncontainer:[{
			textclass:"check rght2",
			textdata:data.string.check
		},{
			textclass:"instrn",
			textdata:data.string.p4s4instrn
		},{
			textclass:"numBox nb1",
			textdata:data.string.seven
		},{
			textclass:"numBox nb2",
			textdata:data.string.twntyone
		},{
			textclass:"numBox nb3",
		}]
	},
	//slide 6
	{
		contentblockadditionalclass:'creamBg',
		extratextblock:[{
			textclass:"qntxt",
			textdata:data.string.p4s6qn
		}],
		imagecontainer:[{
			imagecontainerclass:"container leftCont h34"
		},{
			imagecontainerclass:"container rightCont h34"
		}],
		btmeqncontainer:[{
			textclass:"instrn",
			textdata:data.string.p4s6instrn
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    exeoptions:[
	      {
					optcontainerextra:"smallerCont",
					optdata:data.string.three
	      },
	      {
					optcontainerextra:"smallerCont",
	        optaddclass: "correct",
					optdata:data.string.two
	    	},
	      {
					optcontainerextra:"smallerCont",
					optdata:data.string.one
	    	}
			]
		}],
	},
	//slide 7
	{
		contentblockadditionalclass:'creamBg',
		extratextblock:[{
			textclass:"qntxt",
			textdata:data.string.p4s7qn
		}],
		imagecontainer:[{
			imagecontainerclass:"container leftCont h34"
		},{
			imagecontainerclass:"container rightCont h34"
		}],
		btmeqncontainer:[{
			textclass:"instrn instSec",
			textdata:data.string.p4s7instrn
		}],
		dragdiv:[
			{
				dragCLass:"dragable drgb1",
				textclass:"drgbOpn",
				dataanswer:"dividend",
				textdata:data.string.frtn
			},{
				dragCLass:"dragable drgb2",
				textclass:"drgbOpn",
				dataanswer:"divisor",
				textdata:data.string.seven
			},{
				dragCLass:"dragable drgb3",
				textclass:"drgbOpn",
				dataanswer:"quotient",
				textdata:data.string.two
			}
		],
		dropdiv:[
			{
				dropCLass:"dropable drop1",
				textclass:"dropName",
				textdata:data.string.Dividend,
				dataanswer:"dividend"
			},{
				dropCLass:"signs sgn1",
				textclass:"dropName",
				textdata:data.string.divide
			},{
				dropCLass:"dropable drop2",
				textclass:"dropName",
				dataanswer:"divisor",
				textdata:data.string.Divisor,
			},{
				dropCLass:"signs sgn2",
				textclass:"dropName",
				textdata:data.string.equal
			},{
				dropCLass:"dropable drop3",
				textclass:"dropName",
				dataanswer:"quotient",
				textdata:data.string.Quotient,
			}
		]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var drgDrpCount = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "pencil", src:imgpath+'new/pencil.png', type: createjs.AbstractLoader.IMAGE},
			{id: "five-benches", src:imgpath+'new/five-benches.png', type: createjs.AbstractLoader.IMAGE},
			{id: "four-benches", src:imgpath+'new/four-benches.png', type: createjs.AbstractLoader.IMAGE},
			{id: "kids", src:imgpath+'new/kids.png', type: createjs.AbstractLoader.IMAGE},
			{id: "kids-8", src:imgpath+'new/kids-8.png', type: createjs.AbstractLoader.IMAGE},
			{id: "kids-sisters", src:imgpath+'new/kids-sisters.png', type: createjs.AbstractLoader.IMAGE},
			{id: "kids12", src:imgpath+'new/kids12.png', type: createjs.AbstractLoader.IMAGE},
			{id: "seventree", src:imgpath+'new/seventree.png', type: createjs.AbstractLoader.IMAGE},
			{id: "up", src:imgpath+'new/up.png', type: createjs.AbstractLoader.IMAGE},
			{id: "down", src:imgpath+'new/down.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bench", src:imgpath+'new/bench.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bird", src:imgpath+'new/bird.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bg_diy", src:imgpath+'new/bg_diy.png', type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src:imgpath+'new/t_ball.png', type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
					navigationcontroller();
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
        vocabcontroller.findwords(countNext);
        var bxCountFst=4;
		var bxCountSec=19;
		var corCount = 0;
		var arCount=0;
		function arwClick(upLimit, lowLimit, arCount, boxClass){
			$(".uparw").click(function() {
				arCount==upLimit?arCount=0:"";
				$(boxClass).html(opnArray[arCount]);
				arCount+=1;
			});
			$(".downarw").click(function() {
				arCount-=1;
				arCount<lowLimit?arCount=(upLimit-1):"";
				$(boxClass).html(opnArray[arCount]);
			});
		}

			/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
		function submitCheck(corAns, boxName){
			$(".check").click(function(){
				if($(boxName).text()==3){
					$(boxName).css({
						"background-color":"#6eb260",
						"border":"4px solid #ff0"
					});
					play_correct_incorrect_sound(1);
					$(".check, .arrow").hide(0);
					nav_button_controls(0);
				}else{
					$(boxName).css({
						"background-color":"#FF0000",
						"border":"4px solid #980000"
					});
					play_correct_incorrect_sound(0);
				}
			});
		}
		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
				var arCount = 0;
				$(".rightCont").append("<img class='rightFstImg' src='"+preload.getResult("kids-8").src+"' />");
				for(var i=0; i<=23; i++){
					$(".leftCont").append("<img class='leftFstImg' src='"+preload.getResult("pencil").src+"' />");
				}
				$(".btmeqncontainerclass").append("<img class='arrow uparw' src='"+preload.getResult("up").src+"' />");
				$(".btmeqncontainerclass").append("<img class='arrow downarw' src='"+preload.getResult("down").src+"' />");
				var opnArray = [1,2,3,4,5];
				$(".uparw").click(function() {
					arCount<5?arCount++:"";
					$(".numBox").html(arCount);
				});
				$(".downarw").click(function() {
					arCount>0?arCount--:"";
					$(".numBox").html(arCount);
				});
			 	submitCheck(3, ".numBox");
			// nav_button_controls(1000);
			break;
			case 2:
				$(".rightCont").append("<img class='rightFstImg secRgtImg' src='"+preload.getResult("kids12").src+"' />");
				for(var i=0; i<=3; i++){
					$(".leftCont").append("<img class='leftFstImg ben' src='"+preload.getResult("bench").src+"' />");
				}
				btnSlclk();
			break;
			case 3:
				$(".rightCont").append("<img class='rightFstImg secRgtImg' src='"+preload.getResult("kids12").src+"' />");
				for(var i=0; i<=3; i++){
					$(".leftCont").append("<img class='leftFstImg ben' src='"+preload.getResult("bench").src+"' />");
				}
				$(".btmeqncontainerclass").append("<img class='arrow uparw' src='"+preload.getResult("up").src+"' />");
				$(".btmeqncontainerclass").append("<img class='arrow downarw' src='"+preload.getResult("down").src+"' />");
				var opnArray = [1,2,3,4,5];
				$(".uparw").click(function() {
					arCount<5?arCount++:"";
					$(".numBox").html(arCount);
				});
				$(".downarw").click(function() {
					arCount>0?arCount--:"";
					$(".numBox").html(arCount);
				});
			 	submitCheck(3, ".numBox");
			break;
			case 4:
				var fstcor=false;
				var seccor=false;

				$(".rightCont").append("<img class='leftImg' src='"+preload.getResult("seventree").src+"' />");
				for(var i=0; i<=20; i++){
					$(".leftCont").append("<img class='brds' src='"+preload.getResult("bird").src+"' />");
				}
				$(".btmeqncontainerclass").append("<img class='arrow uparw arfst' src='"+preload.getResult("up").src+"' />");
				$(".btmeqncontainerclass").append("<img class='arrow uparw arsec' src='"+preload.getResult("up").src+"' />");
				$(".btmeqncontainerclass").append("<p class='btmSgn dvSgn'>&#247;</p>");
				$(".btmeqncontainerclass").append("<p class='btmSgn eqSgn'> = </p>");
				$(".arsec").click(function(){
					bxCountFst+=1;
					$(".nb1").html(bxCountFst);
					bxCountFst==10?bxCountFst=4:'';
				});
				$(".arfst").click(function(){
					bxCountSec+=1;
					$(".nb2").html(bxCountSec);
					bxCountSec==25?bxCountSec=19:'';
				});
					$(".check").click(function(){
							if($(".nb1").text()==7){
								fstcor = true;
							$(".nb1").css({
								"background-color":"#98C02E",
								"border":"2px solid #EEFF41"
							});
							$(".arsec").hide(0);
						}else{
							$(".nb1").css({
								"background-color":"#FF0000",
								"border":"2px solid #980000"
							});
						}
						if($(".nb2").text()==21){
							seccor = true;
							$(".nb2").css({
								"background-color":"#98C02E",
								"border":"2px solid #EEFF41"
							});
							$(".arfst").hide(0);
						}else{
							$(".nb2").css({
								"background-color":"#FF0000",
								"border":"2px solid #980000"
							});
						}
						if(seccor==true&&fstcor==true){
							nav_button_controls(100);
							play_correct_incorrect_sound(1);
						}else{
							play_correct_incorrect_sound(0);
						}
					});
			break;
			case 5:
				$(".rightCont").append("<img class='leftImg' src='"+preload.getResult("seventree").src+"' />");
				for(var i=0; i<=20; i++){
					$(".leftCont").append("<img class='brds' src='"+preload.getResult("bird").src+"' />");
				}
				$(".btmeqncontainerclass").append("<p class='btmSgn dvSgn'>&#247;</p>");
				$(".btmeqncontainerclass").append("<p class='btmSgn eqSgn'> = </p>");
				$(".btmeqncontainerclass").append("<img class='arrow rghtArw uparw ' src='"+preload.getResult("up").src+"' />");
				$(".btmeqncontainerclass").append("<img class='arrow rghtArw downarw ' src='"+preload.getResult("down").src+"' />");
				$(".nb2").append("<span class='boxnames'>"+data.string.birds+"</span>");
				$(".nb1").append("<span class='boxnames'>"+data.string.trees+"</span>");
				var opnArray = [1,2,3,4,5];
				$(".uparw").click(function() {
					arCount<5?arCount++:"";
					$(".nb3").html(arCount);
				});
				$(".downarw").click(function() {
					arCount>0?arCount--:"";
					$(".nb3").html(arCount);
				});
				submitCheck(3, ".nb3");
			break;
			case 6:
				$(".leftCont").append("<img class='leftImg1' src='"+preload.getResult("kids-sisters").src+"' />");
				for(var i=0; i<=13; i++){
					$(".rightCont").append("<img class='balls' src='"+preload.getResult("ball").src+"' />");
				}
				btnSlclk();
			break;
			case 7:
				$(".leftCont").append("<img class='leftImg1' src='"+preload.getResult("kids-sisters").src+"' />");
				for(var i=0; i<=13; i++){
					$(".rightCont").append("<img class='balls' src='"+preload.getResult("ball").src+"' />");
				}
				randomize(".dragdiv");
				$(".dragable").draggable({
		      revert : true,
		      cursor : "move",
					zIndex:"100",
				});
				$(".dropable").droppable({
					accept:".dragable",
					hoverClass:"hovered",
					drop: function(event, ui){
						dropFunc(event, ui, $(this));
					}
				});
			break;
		}
	}
	function dropFunc(event, ui, thisClass){
		var dragClass = ui.draggable.children().attr("data-answer").toString().trim();
		var dropClass = thisClass.children().attr("data-answer").toString().trim();
		if(dragClass==dropClass){
			var text = ui.draggable.children().text();
			ui.draggable.detach();
			thisClass.append("<p class='drgans'>"+text+"</p>");
			drgDrpCount+=1;
			drgDrpCount==3?nav_button_controls(100):"";
			thisClass.css({
				"background":"#6eb260",
				"border":"4px solid #ff0"
			});
			play_correct_incorrect_sound(1);
		}
		else{
			thisClass.css({
				"background":"#f00",
				"border":"4px solid #980000"
			});
			play_correct_incorrect_sound(0);
		}
	}
	function btnSlclk(){
		$(".buttonsel").click(function(){
			if($(this).hasClass("correct")){
				$(this).css({"background":"#98C02E","border":"4px solid #EEFF41"});
				$(".buttonsel").css("pointer-events","none");
				$(this).siblings(".corctopt").show(0);
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else{
				$(this).css({"background":"#FF0000","border":"4px solid #980000"});
				$(this).css("pointer-events","none");
				$(this).siblings(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
		/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

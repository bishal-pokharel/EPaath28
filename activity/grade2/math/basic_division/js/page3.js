var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
//slide 1
{
	contentblockadditionalclass:"creambg",
		extratextblock:[
		{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnTxt",
			textclass: "toptxt",
			textdata: data.string.p3s1qn
		},{
			textclass: "submit",
			textdata: data.string.clk
		}
	],
	imagecontainer:[{
		imagecontainerclass:"fruitsHolder"
	},{
		imagecontainerclass:"basketsHOlder"
	}]
},
//slide 2
{
	contentblockadditionalclass:"creambg",
		extratextblock:[
			{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnTxt",
				textclass: "toptxt",
				textdata: data.string.p3s2qn
			}
	],
	exetype1: [{
		optionsdivclass:"optionsdiv",
    exeoptions:[
      {
        optaddclass: "correct",
      },
      {
    	}
		]
	}],
	imagecontainer:[{
		imagecontainerclass:"basketsHOlder"
	}]
},
//slide 3
{
	contentblockadditionalclass:"creambg",
		extratextblock:[
		{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnTxt",
			textclass: "toptxt",
			textdata: data.string.p3s1qn
		}
	],
	imagecontainer:[{
		imagecontainerclass:"fruitsHolder t14"
	},{
		imagecontainerclass:"basketsHOlder t39"
	}],
	midtxt:[
		{
			textclass:"mdTxt mT1",
		},{
			textclass:"sgn1sec",
			// textdata:data.string.divide
		},{
			textclass:"mdTxt mT2",
		},{
			textclass:"sgn2sec",
			textdata:data.string.equal
		},{
			textclass:"mdTxt mT3",
		}
	],
	extratextblock:[{
		textclass: "grenEqn",
		// textdata: data.string.p3s1qn
	}]
},
//slide 4
{
	contentblockadditionalclass:"creambg",
		extratextblock:[
		{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnTxt",
			textclass: "toptxt",
			textdata: data.string.p3s1qn
		},{
			textclass: "submit",
			textdata: data.string.clk
		}
	],
	imagecontainer:[{
		imagecontainerclass:"fruitsHolder"
	},{
		imagecontainerclass:"basketsHOlder"
	}]
},
//slide 5
{
	contentblockadditionalclass:"creambg",
		extratextblock:[
		{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnTxt",
			textclass: "toptxt",
			textdata: data.string.p3s2qn
		}
	],
	exetype1: [{
		optionsdivclass:"optionsdiv",
    exeoptions:[
      {
        optaddclass: "correct",
      },
      {
    	}
		]
	}],
	imagecontainer:[{
		imagecontainerclass:"basketsHOlder"
	}]
},
//slide 6
{
	contentblockadditionalclass:"creambg",
	imagecontainer:[{
		imagecontainerclass:"fruitsHolder t14"
	},{
		imagecontainerclass:"basketsHOlder t39"
	}],
	midtxt:[
		{
			textclass:"mdTxt mT1",
		},{
			textclass:"sgn1sec",
			// textdata:data.string.divide
		},{
			textclass:"mdTxt mT2",
		},{
			textclass:"sgn2sec",
			textdata:data.string.equal
		},{
			textclass:"mdTxt mT3",
		}
	],
	eqnblock:[{
		textclass: "grnEqFst",
	},{
		textclass: "grnEqFst",
	}]
},
//slide 7
{
	contentblockadditionalclass:"creambg",
		extratextblock:[
		{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnTxt",
			textclass: "toptxt",
			textdata: data.string.p3s1qn
		},{
			textclass: "submit",
			textdata: data.string.clk
		}
	],
	imagecontainer:[{
		imagecontainerclass:"fruitsHolder"
	},{
		imagecontainerclass:"basketsHOlder"
	}]
},
//slide 8
{
	contentblockadditionalclass:"creambg",
		extratextblock:[
		{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnTxt",
			textclass: "toptxt",
			textdata: data.string.p3s2qn
		}
	],
	exetype1: [{
		optionsdivclass:"optionsdiv",
    exeoptions:[
      {
        optaddclass: "correct",
      },
      {
    	}
		]
	}],
	imagecontainer:[{
		imagecontainerclass:"basketsHOlder"
	}]
},
//slide 9
{
	contentblockadditionalclass:"creambg",
		extratextblock:[
		{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnTxt",
			textclass: "toptxt",
			textdata: data.string.p3s1qn
		}
	],
	imagecontainer:[{
		imagecontainerclass:"fruitsHolder t14"
	},{
		imagecontainerclass:"basketsHOlder t39"
	}],
	midtxt:[
		{
			textclass:"mdTxt mT1",
		},{
			textclass:"sgn1sec",
			// textdata:data.string.divide
		},{
			textclass:"mdTxt mT2",
		},{
			textclass:"sgn2sec",
			textdata:data.string.equal
		},{
			textclass:"mdTxt mT3",
		}
	],
	eqnblock:[{
		textclass: "grnEqFst",
	},{
		textclass: "grnEqFst",
	},{
		textclass: "grnEqFst",
	}],
	extratextblock:[{
		textclass: "submit t90",
		textdata:data.string.practise
	}]
},

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	// var myvar, myvar2, myvar3, fruitName;
	var randNum = 0;
	var randItemNum = 0;
	var fruitName;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var timeout1, timeout2, timeout3;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: 'images/right.png', type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+'new/apple_new.png', type: createjs.AbstractLoader.IMAGE},
			{id: "basket", src: imgpath+'new/basket.png', type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+'new/orange_new.png', type: createjs.AbstractLoader.IMAGE},
			{id: "pear", src: imgpath+'new/pear_new.png', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			// {id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			// {id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			// {id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			// {id: "sound_5", src: soundAsset+"p1_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*==========================================randItemNum============
	 =            Navigation Controller Function            =
	 ======================================================*/
function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================randItemNum================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var fruitsArray=[[[20,2],[20,4],[20,5]],
										 [[12,3],[12,4],[12,6]],
										 [[18,3],[18,6],[18,9]]
									 ];

		var cpInccCount = 0;
		var trayCount = 0;
		var cInTrCount = 0;
		var itnCount = 0;
	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 0 +1)) + 0;
	    return randNum;
	  }
		function imageGenerators(randItemNum, randNum, fruitsArray, imageId, fruitName){
				$(".qnTxt:eq(0)").html(fruitsArray[randNum][randItemNum][0]+" "+fruitName);
				$(".qnTxt:eq(1)").html(fruitsArray[randNum][randItemNum][1]);
				// for(var i=0; i<fruitsArray[randNum][randItemNum][0]; i++){
				// 	$(".fruitsHolder").append("<img class='fruits frt"+i+"' src='"+preload.getResult(imageId).src+"' />");
				// }
				for(var i=(fruitsArray[randNum][randItemNum][0]-1); i>=0; i--){
						$(".fruitsHolder").append("<img class='fruits frt"+i+"' src='"+preload.getResult(imageId).src+"' />");
				}
				for(var i=0; i<fruitsArray[randNum][randItemNum][1]; i++){
					$(".basketsHOlder").append("<div class='basketFrtCntr bfc"+i+"'/>");
				}
				for(var i=0; i<fruitsArray[randNum][randItemNum][1]; i++){
					$(".bfc"+i).append("<div class='basketsCntr bskCnt"+i+"'/>");
					$(".bfc"+i).append("<div class='bsktImgCntr bic"+i+"'/>");
					$(".bskCnt"+i).append("<img class='baskets' src='"+preload.getResult("basket").src+"' />");
				}
				for(var i=0; i<fruitsArray[randNum][randItemNum][1]; i++){
					for(var j=0; j<fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]; j++){
						$(".bic"+i).append("<img class='fruitsInBskt fib"+j+"' src='"+preload.getResult(imageId).src+"' />")
					}
				}
			}
		function showHIde(cpInccCount,trayCount, cInTrCount, itnCount ,fruitsArray, randItemNum, randNum){
			console.log(cpInccCount+"--"+trayCount+"--"+cInTrCount+"--"+fruitsArray[randNum][randItemNum][0])

			$(".frt"+cpInccCount).css("opacity","0");
			$(".bic"+trayCount).find("img:eq("+cInTrCount+")").delay(1000).css("opacity","1");
			cpInccCount+=1;
			trayCount+=1;
			trayCount==(fruitsArray[randNum][randItemNum][1])?trayCount=0:'';
			timeout2 = setTimeout(function(){
				if(cpInccCount<fruitsArray[randNum][randItemNum][0]){
					itnCount++;
					if(itnCount==fruitsArray[randNum][randItemNum][1]){
						itnCount = 0;
						cInTrCount+=1;
					}
					showHIde(cpInccCount,trayCount, cInTrCount, itnCount,fruitsArray, randItemNum, randNum);
				}
				else{
					nav_button_controls(1000);
					cpInccCount = 0;
					trayCount = 0;
					cInTrCount = 0;
					itnCount = 0;
				}
			},500);
		}
		function showHIdeSec(cpInccCount,trayCount, cInTrCount, itnCount ,fruitsArray, randItemNum, randNum){
			console.log(cpInccCount+"--"+trayCount+"--"+cInTrCount+"--"+fruitsArray[randNum][randItemNum][0])
			$(".frt"+cpInccCount).css("opacity","0");
			$(".bic"+trayCount).find("img:eq("+cInTrCount+")").delay(1000).css("opacity","1");
			cpInccCount+=1;
			trayCount+=1;
			trayCount==(fruitsArray[randNum][randItemNum][1])?trayCount=0:'';
			timeout3=setTimeout(function(){
				if(cpInccCount<fruitsArray[randNum][randItemNum][0]){
					itnCount++;
					if(itnCount==fruitsArray[randNum][randItemNum][1]){
						itnCount = 0;
						cInTrCount+=1;
					}
					showHIdeSec(cpInccCount,trayCount, cInTrCount, itnCount,fruitsArray, randItemNum, randNum);
				}
			},500);
		}
		function imageCssController(randNum, randItemNum, fruitName){
				switch (randNum) {
					case 0:
						imageGenerators(randItemNum, randNum, fruitsArray, "apple", fruitName);
						$(".basketFrtCntr").css("width","25%");
						$(".fruitsInBskt").css({"width":"18%","top":"15%"});
						countNext==0?$(".bsktImgCntr").children().css("opacity","0"):'';
					break;
					case 1:
						imageGenerators(randItemNum, randNum, fruitsArray, "orange", fruitName);
						$(".basketFrtCntr").css("width","27%");
						$(".fruitsInBskt").css({"width":"25%","top":"18%"});
						countNext==0?$(".bsktImgCntr").children().css("opacity","0"):'';
					break;
					case 2:
						imageGenerators(randItemNum, randNum, fruitsArray, "pear", fruitName);
						$(".basketFrtCntr").css("width","27%");
						$(".fruitsInBskt").css({"width":"25%","top":"15%"});
						countNext==0?$(".bsktImgCntr").children().css("opacity","0"):'';
					break;
				}
		}
		function imageCssControllerSec(randNum, randItemNum, fruitName){
			switch (randNum) {
				case 0:
					imageGenerators(randItemNum, randNum, fruitsArray, "apple", fruitName);
					$(".basketFrtCntr").css("width","24%");
					$(".fruitsInBskt").css({"width":"25%","top":"15%"});
					countNext==3?$(".bsktImgCntr").children().css("opacity","0"):'';
				break;
				case 1:
					imageGenerators(randItemNum, randNum, fruitsArray, "orange", fruitName);
					$(".basketFrtCntr").css("width","24%");
					$(".fruitsInBskt").css({"width":"30%","top":"15%"});
					countNext==3?$(".bsktImgCntr").children().css("opacity","0"):'';
				break;
				case 2:
					imageGenerators(randItemNum, randNum, fruitsArray, "pear", fruitName);
					$(".basketFrtCntr").css("width","15%");
					$(".fruitsInBskt").css({"width":"36%","top":"15%"});
					countNext==3?$(".bsktImgCntr").children().css("opacity","0"):'';
				break;
			}
		}
		function imageCssControllerthird(randNum, randItemNum, fruitName){
			switch (randNum) {
				case 0:
					imageGenerators(randItemNum, randNum, fruitsArray, "apple", fruitName);
					$(".basketFrtCntr").css("width","15%");
					$(".fruitsInBskt").css({"width":"27%","top":"7%"});
					countNext==6?$(".bsktImgCntr").children().css("opacity","0"):'';
				break;
				case 1:
					imageGenerators(randItemNum, randNum, fruitsArray, "orange", fruitName);
					$(".basketFrtCntr").css("width","15%");
					$(".fruitsInBskt").css({"width":"35%","top":"7%","height":"100%"});
					countNext==6?$(".bsktImgCntr").children().css("opacity","0"):'';
				break;
				case 2:
					imageGenerators(randItemNum, randNum, fruitsArray, "pear", fruitName);
					$(".basketFrtCntr").css("width","15%");
					$(".fruitsInBskt").css({"width":"36%","top":"7%"});
					countNext==6?$(".bsktImgCntr").children().css("opacity","0"):'';
				break;
			}
		}
		function nameGenerator(randNum){
				switch (randNum) {
				case 0:
					fruitName = data.string.apple;
				break;
				case 1:
					fruitName = data.string.orange;
				break;
				case 2:
					fruitName = data.string.pear;
				break;
			}
			return(fruitName);
		}
		function autoAnim(cpInccCount,trayCount, cInTrCount, itnCount, fruitsArray,randItemNum,randNum){
			$(".fruitsHolder").delay(1000).animate({
				opacity:"1"
			},1000, function(){
				$(".mT1").css("opacity","1");
				$(".sgn1sec").delay(500).animate({
					opacity:"1"
				},200, function(){
					$(".basketFrtCntr").delay(1000).show(0);
						$(".mT2").delay(1000).animate({
							opacity:"1"
						},10,function(){
						 timeout1= setTimeout(function(){
							showHIdeSec(cpInccCount,trayCount, cInTrCount, itnCount, fruitsArray,randItemNum,randNum);
								$(".sgn2sec").delay(10000).animate({
									opacity:"1"
								},300, function(){
								$(".mT3").delay(500).animate({
									opacity:"1"
								},300, function(){
									$(".belowTxt").delay(500).animate({
										opacity:"1"
									},300, function(){
										$(".grenEqn").animate({
											opacity:"1"
										},300, function(){
												nav_button_controls(1000);
											 $(".submit").delay(500).show(0);
										});
									});
								});
							});
						},2000);
					});
				});
			});
		}
		switch(countNext){
			case 0:
				randNum = rand_generator(2);
				nameGenerator(randNum);
				randItemNum = 0;
				var navtime = (fruitsArray[randNum][0][0]/fruitsArray[randNum][0][1])*1450;
				console.log(navtime);
				imageCssController(randNum, randItemNum, fruitName);
				$(".submit").on("click", function(){
					showHIde(cpInccCount,trayCount, cInTrCount, itnCount, fruitsArray,randItemNum,randNum);
					$(".submit").css("pointer-events","none");
					nav_button_controls(navtime);
				});
			break;
			case 1:
				imageCssController(randNum, randItemNum, fruitName);
				$(".qnTxt:eq(0)").html(fruitName);
				$(".optionscontainer:eq(0)").find('p').html(fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]);
				$(".optionscontainer:eq(1)").find('p').html((fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1])*2);
				randomize(".optionsdiv");
				btnSlclk();
			break;
			case 2:
				var fname= nameGenerator(randNum);
				var navtime = (fruitsArray[randNum][0][0]/fruitsArray[randNum][0][1])*1800;
				imageCssController(randNum, randItemNum, fruitName);
				$(".basketFrtCntr").hide(0);
				$(".fruitsHolder").css("opacity","0");
				$(".midtxt,.bsktImgCntr").children().css("opacity","0");
				$(".mT1").html(fruitsArray[randNum][randItemNum][0]+fname);
				$(".mT1").append("<span class='belowTxt'>"+fruitsArray[randNum][randItemNum][0]+"</span>");
				$(".mT2").html(fruitsArray[randNum][randItemNum][1]+data.string.basket);
				$(".mT2").append("<span class='belowTxt'>"+fruitsArray[randNum][randItemNum][1]+"</span>");
				$(".mT3").html(fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]+data.string.inbskt);
				$(".mT3").append("<span class='belowTxt'>"+fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]+"</span>");
				$(".sgn1sec").html(data.string.dvdin);
				$(".sgn1sec").append("<span class='belowTxt'>"+data.string.divide+"</span>");
				$(".sgn2sec").append("<span class='belowTxt'>"+data.string.equal+"</span>");
				$(".grenEqn").html(fruitsArray[randNum][randItemNum][0]+"&#247;"+fruitsArray[randNum][randItemNum][1]+" = "+fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]);
				autoAnim(cpInccCount,trayCount, cInTrCount, itnCount, fruitsArray,randItemNum,randNum);
				// nav_button_controls(navtime);
			break;
			case 3:
				randItemNum = 1;
				nameGenerator(randNum);
				var navtime = (fruitsArray[randNum][0][0]/fruitsArray[randNum][0][1])*1450;
				imageCssControllerSec(randNum, randItemNum, fruitName);

				$(".submit").on("click", function(){
					showHIde(cpInccCount,trayCount, cInTrCount, itnCount, fruitsArray,randItemNum,randNum);
					$(".submit").css("pointer-events","none");
				});
				// nav_button_controls(navtime);
			break;
			case 4:
				imageCssControllerSec(randNum, randItemNum, fruitName);
				$(".qnTxt:eq(0)").html(fruitName);
				$(".optionscontainer:eq(0)").find('p').html(fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]);
				$(".optionscontainer:eq(1)").find('p').html((fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1])*2);

				randomize(".optionsdiv");
				btnSlclk();
			break;
			case 5:
				var fname= nameGenerator(randNum);
				var navtime = (fruitsArray[randNum][0][0]/fruitsArray[randNum][0][1])*1800;
				imageCssControllerSec(randNum, randItemNum, fruitName);
				$(".basketFrtCntr").hide(0);
				$(".fruitsHolder").css("opacity","0");
				$(".midtxt,.bsktImgCntr").children().css("opacity","0");
				$(".mT1").html(fruitsArray[randNum][randItemNum][0]+fname);
				$(".mT1").append("<span class='belowTxt'>"+fruitsArray[randNum][randItemNum][0]+"</span>");
				$(".mT2").html(fruitsArray[randNum][randItemNum][1]+data.string.basket);
				$(".mT2").append("<span class='belowTxt'>"+fruitsArray[randNum][randItemNum][1]+"</span>");
				$(".mT3").html(fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]+data.string.inbskt);
				$(".mT3").append("<span class='belowTxt'>"+fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]+"</span>");
				$(".sgn1sec").html(data.string.dvdin);
				$(".sgn1sec").append("<span class='belowTxt'>"+data.string.divide+"</span>");
				$(".sgn2sec").append("<span class='belowTxt'>"+data.string.equal+"</span>");
				$(".grnEqFst:eq(0)").html(fruitsArray[randNum][0][0]+"&#247;"+fruitsArray[randNum][0][1]+" = "+fruitsArray[randNum][0][0]/fruitsArray[randNum][0][1]);
				$(".grnEqFst:eq(1)").html(fruitsArray[randNum][1][0]+"&#247;"+fruitsArray[randNum][1][1]+" = "+fruitsArray[randNum][1][0]/fruitsArray[randNum][1][1]);
				autoAnim(cpInccCount,trayCount, cInTrCount, itnCount, fruitsArray,randItemNum,randNum);
				// nav_button_controls(navtime);
			break;
			case 6:
			 	randItemNum = 2;
				nameGenerator(randNum);
				imageCssControllerthird(randNum, randItemNum, fruitName);
				var navtime = (fruitsArray[randNum][0][0]/fruitsArray[randNum][0][1])*1450;

				$(".submit").on("click", function(){
					showHIde(cpInccCount,trayCount, cInTrCount, itnCount, fruitsArray,randItemNum,randNum);
					$(".submit").css("pointer-events","none");
				});
				nav_button_controls(navtime);
			break;
			case 7:
				imageCssControllerthird(randNum, randItemNum, fruitName);
				$(".qnTxt:eq(0)").html(fruitName);
				$(".optionscontainer:eq(0)").find('p').html(fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]);
				$(".optionscontainer:eq(1)").find('p').html((fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1])*2);

				randomize(".optionsdiv");
				btnSlclk();
			break;
			case 8:
				var fname= nameGenerator(randNum);
				var navtime = (fruitsArray[randNum][0][0]/fruitsArray[randNum][0][1])*1800;
				imageCssControllerthird(randNum, randItemNum, fruitName);
				$(".submit").hide(0);
				$(".basketFrtCntr").hide(0);
				$(".fruitsHolder").css("opacity","0");
				$(".midtxt,.bsktImgCntr").children().css("opacity","0");
				$(".mT1").html(fruitsArray[randNum][randItemNum][0]+fname);
				$(".mT1").append("<span class='belowTxt'></span>");
				$(".mT2").html(fruitsArray[randNum][randItemNum][1]+data.string.basket);
				$(".mT3").html(fruitsArray[randNum][randItemNum][0]/fruitsArray[randNum][randItemNum][1]+data.string.inbskt);
				$(".sgn1sec").html(data.string.dvdin);
				$(".grnEqFst:eq(0)").html(fruitsArray[randNum][0][0]+"&#247;"+fruitsArray[randNum][0][1]+" = "+fruitsArray[randNum][0][0]/fruitsArray[randNum][0][1]);
				$(".grnEqFst:eq(1)").html(fruitsArray[randNum][1][0]+"&#247;"+fruitsArray[randNum][1][1]+" = "+fruitsArray[randNum][1][0]/fruitsArray[randNum][1][1]);
				$(".grnEqFst:eq(2)").html(fruitsArray[randNum][2][0]+"&#247;"+fruitsArray[randNum][2][1]+" = "+fruitsArray[randNum][2][0]/fruitsArray[randNum][2][1]);
				autoAnim(cpInccCount,trayCount, cInTrCount, itnCount, fruitsArray,randItemNum,randNum);
				$(".submit").click(function(){
					location.reload();
				});
				// nav_button_controls(navtime);
			break;
		}


		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
	}
	function btnSlclk(){
		$(".buttonsel").click(function(){
			if($(this).hasClass("correct")){
				$(this).css({"background":"#98c02eff","border":"4px solid #deef3c"});
				$(".buttonsel").css("pointer-events","none");
				$(this).siblings(".corctopt").show(0);
				nav_button_controls(100);
				play_correct_incorrect_sound(1);
			}
			else{
				play_correct_incorrect_sound(0);
				$(this).css({"background":"#f00","border":"4px solid #980000"});
				$(this).css("pointer-events","none");
				$(this).siblings(".wrngopt").show(0);
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		clearTimeout(timeout1);
		clearTimeout(timeout2);
		clearTimeout(timeout3);
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		clearTimeout(timeout1);
		clearTimeout(timeout2);
		clearTimeout(timeout3);
		templatecaller();
			// clearTimeout(myvar2);
			// clearTimeout(myvar3);
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeout1);
		clearTimeout(timeout2);
		clearTimeout(timeout3);
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

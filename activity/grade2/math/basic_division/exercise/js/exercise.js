var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[
//exercise 1
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.exq1,
	    exeoptions:[
	      {
	        optaddclass: "correct",
	      },
	      {
	    	},
	      {
	    	},
	      {
	    	}
			]
		}]
	},
//exercise 2
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.exq2,
	    exeoptions:[
	      {
	        optaddclass: "correct",
	      },
	      {
	    	},
	      {
	    	},
	      {
	    	}
			]
		}]
	},
//exercise 3
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.exq3,
	    exeoptions:[
	      {
	        optaddclass: "correct",
	      },
	      {
	    	},
	      {
	    	},
	      {
	    	}
			]
		}]
	},
//exercise 4
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			textdata:data.string.exq4,
	    exeoptions:[
	      {
	        optaddclass: "correct",
	      },
	      {
	    	},
	      {
	    	},
	      {
	    	}
			]
		}]
	},
//exercise 5  -->left for imgOptions
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			textdata:data.string.exq5,
		}],
		imagecontainer:[{
			imagecontainerclass:"Container leftCont correct"
		},{
			imagecontainerclass:"Container rightCont"
		}]
	},
//exercise 6  -->left for imgOptions
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			textdata:data.string.exq6,
		}],
		imagecontainer:[{
			imagecontainerclass:"Container leftCont short correct"
		},{
			imagecontainerclass:"Container rightCont short"
		}]
	},
//exercise 7
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:'optionsdiv',
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			textdata:data.string.exq7,
	    exeoptions:[
	      {
					optcontainerextra:"shortCont",
	        optaddclass: "correct",
					startposextra:"top57"
	      },
	      {
					optcontainerextra:"shortCont",
					startposextra:"top57"
	    	}
			]
		}]
	},
//exercise 8
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			textdata:data.string.exq8,
	    exeoptions:[
	      {
					optcontainerextra:"shortCont",
	        optaddclass: "correct",
					startposextra:"top57"
	      },
	      {
					optcontainerextra:"shortCont",
					startposextra:"top57"
	    	}
			]
		}]
	},
//exercise 9
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			textdata:data.string.exq8,
	    exeoptions:[
	      {
	        optaddclass: "correct",
	      },
	      {
	    	},
	      {
	    	},
	      {
	    	}
			]
		}]
	},
//exercise 10
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			textdata:data.string.exq11,
	    exeoptions:[
	      {
	        optaddclass: "correct",
	      },
	      {
	    	},
	      {
	    	},
	      {
	    	}
			]
		}]
	},
//exercise 11
	{
		contentblockadditionalclass:"greenbg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			textdata:data.string.exq11,
	    exeoptions:[
	      {
	        optaddclass: "correct",
	      },
	      {
	    	},
	      {
	    	},
	      {
	    	}
			]
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 11;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "car", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "iron", src: imgpath+"iron.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+"mango.png", type: createjs.AbstractLoader.IMAGE},
			{id: "button", src:imgpath+'button.png', type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src:imgpath+'plus.png', type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src:imgpath+'equal.png', type: createjs.AbstractLoader.IMAGE},
			{id: "five-benches", src:imgpath+'new/five-benches.png', type: createjs.AbstractLoader.IMAGE},
			{id: "four-benches", src:imgpath+'new/four-benches.png', type: createjs.AbstractLoader.IMAGE},
			{id: "four_student", src:imgpath+'new/four_student.png', type: createjs.AbstractLoader.IMAGE},
			{id: "two_student", src:imgpath+'new/two_student.png', type: createjs.AbstractLoader.IMAGE},
			{id: "multiply", src:imgpath+'multiple.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(11);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		var updateScore = 0;
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parent){
			// alert(parent);
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
}

	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;


	$(".question").prepend(countNext+1+". ");
  function rand_generator(limit){
    var randNum = Math.floor(Math.random() * (limit - 0 +1)) + 0;
    return randNum;
  }
	function optionsGenerator(opnArray){
		for(var i=0; i<=opnArray.length; i++){
			$(".optionscontainer:eq("+i+")").find("p").html(opnArray[i]);
		}
	}
	 	switch(countNext){
	 		case 0:
				var qno = rand_generator(3);
				var qnArray = [[12,3],[16,8],[18,6],[20,4]]
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				var opnArray = [qnArray[qno][0] +"&#247;"+ qnArray[qno][1],
												qnArray[qno][0] +"+"+ qnArray[qno][1],
											  qnArray[qno][0] +"-"+ qnArray[qno][1],
												qnArray[qno][0] +"&#215;"+ qnArray[qno][1]];
				optionsGenerator(opnArray);
				randomize(".optionsdiv");
			break;
	 		case 1:
				var qno = rand_generator(3);
				var qnArray = [[12,3],[16,8],[18,6],[20,4]]
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				var opnArray = [qnArray[qno][0]/qnArray[qno][1],
												qnArray[qno][0]+qnArray[qno][1],
											  qnArray[qno][0]-qnArray[qno][1],
												qnArray[qno][0]*qnArray[qno][1]];
				optionsGenerator(opnArray);
				randomize(".optionsdiv");
			break;
	 		case 2:
				var qno = rand_generator(2);
				var qnArray = [[16,4],[12,2],[9,3]];
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				var opnArray = [["16 &#247;  4 = 4","16 &#247;  4 = 2","16 &#247;  4 = 3","16 &#247; 4 = 1"],
												["12 &#247;  2 = 6","12 &#247;  2 = 3","12 &#247;  2 = 5","12 &#247; 2 = 4"],
												["9 &#247;  3 = 3","9 &#247;  3 = 1","9 &#247;  3 = 2","9 &#247; 3 = 4"]];
				optionsGenerator(opnArray[qno]);
				randomize(".optionsdiv");
			break;
	 		case 3:
				var qno = rand_generator(1);
				var qnArray = [[24,8,3],[30,6,5]];
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				$(".qn:eq(2)").html(qnArray[qno][2]);
				var opnArray = [["24 &#247;  8 = 3","8 &#247;  24 = 3","3 &#247;  24 = 3","3 &#247; 8 = 24"],
												["30 &#247;  6 = 5","6 &#247;  30 = 5","5 &#247;  30 = 6","5 &#247; 6 = 30"]];
				optionsGenerator(opnArray[qno]);
				randomize(".optionsdiv");
			break;
			case 4:
				$(".leftCont").append("<img class='boxImg' src='"+preload.getResult("four-benches").src+"'/>");
				$(".rightCont").append("<img class='boxImg' src='"+preload.getResult("five-benches").src+"'/>");
				$(".leftCont").append("<p class='toptxt'>"+data.string.fbnch+"</p>");
				$(".rightCont").append("<p class='toptxt'>"+data.string.fvbnch+"</p>");
				containerClick();
			break;
			case 5:
				$(".leftCont").append("<img class='boxImg' src='"+preload.getResult("four_student").src+"'/>");
				$(".rightCont").append("<img class='boxImg' src='"+preload.getResult("two_student").src+"'/>");
				$(".leftCont").append("<p class='toptxt'>"+data.string.fstdnt+"</p>");
				$(".rightCont").append("<p class='toptxt'>"+data.string.fvstdnt+"</p>");
				containerClick();
			break;
	 		case 6:
				var qno = rand_generator(1);
				var qnArray = [[18,3,6],[18,6,3]];
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				$(".qn:eq(2)").html(qnArray[qno][2]);
				var opnArray = [["18 &#247;  3 = 6"+data.string.each_txt_1, "5 &#247;  15 = 3"+data.string.each_txt_2],
												["18 &#247;  6 = 3"+data.string.each_txt_2, "6 &#247;  18 = 3"+data.string.each_txt_1]];
				optionsGenerator(opnArray[qno]);
				randomize(".optionsdiv");
			break;
	 		case 7:
				var qno = rand_generator(1);
				var qnArray = [[24,8,3],[24,3,8]];
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				var opnArray = [["24 &#247;  8 = 3"+data.string.each_txt_2, "5 &#247;  15 = 3"+data.string.each_txt_2],
												["24 &#247;  3 = 8"+data.string.each_txt_3, "8 &#247;  24 = 6"+data.string.each_txt_1]];
				optionsGenerator(opnArray[qno]);
				randomize(".optionsdiv");
			break;
	 		case 8:
				var qno = rand_generator(2);
				var qnArray = [[15,5],[15,3],[21,3]];
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				var opnArray = [qnArray[qno][0]/qnArray[qno][1],
												(qnArray[qno][0]/qnArray[qno][1])+1,
												(qnArray[qno][0]/qnArray[qno][1])+2,
												(qnArray[qno][0]/qnArray[qno][1])+3];

				optionsGenerator(opnArray);
				randomize(".optionsdiv");
			break;
	 		case 9:
				var qno = rand_generator(2);
				var qnArray = [[55,5],[55,11],[21,7]];
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				var opnArray = [qnArray[qno][0]/qnArray[qno][1],
												(qnArray[qno][0]/qnArray[qno][1])+1,
												(qnArray[qno][0]/qnArray[qno][1])+2,
												(qnArray[qno][0]/qnArray[qno][1])+3];

				optionsGenerator(opnArray);
				randomize(".optionsdiv");
			break;
	 		case 10:
				var qno = rand_generator(2);
				var qnArray = [[15,5],[21,3],[24,12]];
				$(".qn:eq(0)").html(qnArray[qno][0]);
				$(".qn:eq(1)").html(qnArray[qno][1]);
				var opnArray = [qnArray[qno][0]/qnArray[qno][1],
												(qnArray[qno][0]/qnArray[qno][1])+1,
												(qnArray[qno][0]/qnArray[qno][1])+2,
												(qnArray[qno][0]/qnArray[qno][1])+3];

				optionsGenerator(opnArray);
				randomize(".optionsdiv");
			break;
	 		case 11:
				var qno = rand_generator(2);
				var qnArray = [[24,2],[24,3],[21,7]];
				$(".qn:eq(0)").html(qnArray[qno][1]);
				$(".qn:eq(1)").html(qnArray[qno][0]);
				var opnArray = [qnArray[qno][0]/qnArray[qno][1],
												(qnArray[qno][0]/qnArray[qno][1])+1,
												(qnArray[qno][0]/qnArray[qno][1])+2,
												(qnArray[qno][0]/qnArray[qno][1])+3];

				optionsGenerator(opnArray);
				randomize(".optionsdiv");
			break;
	 	}


	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
 			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
	//handle mcq
	$(".buttonsel").click(function(){
		$(this).removeClass('forhover');
			if(ansClicked == false){
				/*class 1 is always for the right answer. updates scoreboard and disables other click if
				right answer is clicked*/
				if($(this).hasClass("correct")){
					if(wrngClicked == false){
						testin.update(true);
					}
					play_correct_incorrect_sound(1);
					$(this).css({
						"background": "#bed62fff",
						"border":"5px solid #deef3c",
						"color":"#fff",
						'pointer-events': 'none'
					});
					$(this).siblings(".corctopt").show(0);
					$('.buttonsel').removeClass('forhover forhoverimg');
					$('.buttonsel').removeClass('clock-hover');
					$nextBtn.show(0);
					// navigationcontroller();
					ansClicked = true;
					// if(countNext != $total_page)
					// $nextBtn.show(0);

				}
				else{
					testin.update(false);
					play_correct_incorrect_sound(0);
					$(this).css({
						"background":"#FF0000",
						"border":"5px solid #980000",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(this).siblings(".wrngopt").show(0);
					wrngClicked = true;
				}
			}
		});

		function containerClick(){
			$(".Container").click(function(){
				if($(this).hasClass("correct")){
						testin.update(true);
						play_correct_incorrect_sound(1);
						$(this).css({
							"background": "#bed62fff",
							"border":"5px solid #deef3c",
							"color":"#000",
							'pointer-events': 'none'
						});
						$(this).children(".correctopt").show(0);
						$(".Container").css("pointer-events","none");
						$nextBtn.show(0);
				}else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css({
							"background":"#FF0000",
							"border":"5px solid #980000",
							"color":"#000",
							'pointer-events': 'none'
						});
						$(this).children(".incoropt").show(0);
						// wrngClicked = true;
				}
			});
		};
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
 	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function loadimage(imgrep,imgtext,imgsrc,quesNo){
		imgrep.attr("src",imgsrc);
		imgtext.text(quesNo);
	}

/*=====  End of Templates Controller Block  ======*/
});

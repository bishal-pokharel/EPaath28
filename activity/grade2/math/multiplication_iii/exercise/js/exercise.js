var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
//ex 1
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			},{
				textclass: "submit",
				textdata: data.string.check
			}
		],
		sidetextdiv:[{
			textdivclass:"leftTextBox",
			boxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_1",
				textclass:"bxTxt bxt_1",
				textdata:data.string.exchelp_txt_1
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_2",
				textclass:"bxTxt bxt_2",
				textdata:data.string.exchelp_txt_2
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_3",
				textclass:"bxTxt bxt_3",
				textdata:data.string.exchelp_txt_3
			}]
		}],
		eqex:[
			{
				eqexboxclass:"eqexBox box1",
				tbleblk:[{
					tableblkclass:"box1_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.tens
						},{
							// textclass:""
							tabledata:data.string.ones
						}]
					},{
						rowcontent:[{
							// textclass:""
							// tabledata:data.string.one
						},{
							// textclass:""
							// tabledata:data.string.five
						}]
					},{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.mul_sgn
						},{
							// textclass:""
							// tabledata:data.string.one
						}]
					}]
				}]
			},{
				eqexboxclass:"eqexBox box2",
				tbleblk:[{
					tableblkclass:"box2_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.blank
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					},{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.blank
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					}]
				}],
				qnbox:[{
					qnboxclass:"questionBox qb1",
					textclass:"arrowBox ab1",
					arrowimgclass:"arrowclass arw1",
				},{
					qnboxclass:"questionBox qb2",
					textclass:"arrowBox ab2",
					arrowimgclass:"arrowclass arw2"
				}]
			},{
				eqexboxclass:"eqexBox box3",
				tbleblk:[{
					tableblkclass:"box3_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.one
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					}]
				}],
				qnbox:[{
					qnboxclass:"questionBox qb3",
					textclass:"arrowBox ab3",
					arrowimgclass:"arrowclass arw3"
				}]
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"sideBoard",
				imgid:"black_board",
				imgsrc:''
			},{
				imgclass:"arrow arrow1",
				imgid:"arrow",
				imgsrc:''
			},{
				imgclass:"arrow arrow2",
				imgid:"arrow",
				imgsrc:''
			},{
				imgclass:"arrow arrow3",
				imgid:"arrow",
				imgsrc:''
			}]
		}]

	},
//ex 2
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			},{
				textclass: "submit",
				textdata: data.string.check
			}
		],
		sidetextdiv:[{
			textdivclass:"leftTextBox",
			boxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_1",
				textclass:"bxTxt bxt_1",
				textdata:data.string.exchelp_txt_1
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_2",
				textclass:"bxTxt bxt_2",
				textdata:data.string.exchelp_txt_2
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_3",
				textclass:"bxTxt bxt_3",
				textdata:data.string.exchelp_txt_3
			}]
		}],
		eqex:[
			{
				eqexboxclass:"eqexBox box1",
				tbleblk:[{
					tableblkclass:"box1_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.tens
						},{
							// textclass:""
							tabledata:data.string.ones
						}]
					},{
						rowcontent:[{
							// textclass:""
							// tabledata:data.string.one
						},{
							// textclass:""
							// tabledata:data.string.five
						}]
					},{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.mul_sgn
						},{
							// textclass:""
							// tabledata:data.string.one
						}]
					}]
				}]
			},{
				eqexboxclass:"eqexBox box2",
				tbleblk:[{
					tableblkclass:"box2_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.blank
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					},{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.blank
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					}]
				}],
				qnbox:[{
					qnboxclass:"questionBox qb1",
					textclass:"arrowBox ab1",
					arrowimgclass:"arrowclass arw1",
				},{
					qnboxclass:"questionBox qb2",
					textclass:"arrowBox ab2",
					arrowimgclass:"arrowclass arw2"
				}]
			},{
				eqexboxclass:"eqexBox box3",
				tbleblk:[{
					tableblkclass:"box3_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.one
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					}]
				}],
				qnbox:[{
					qnboxclass:"questionBox qb3",
					textclass:"arrowBox ab3",
					arrowimgclass:"arrowclass arw3"
				}]
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"sideBoard",
				imgid:"black_board",
				imgsrc:''
			},{
				imgclass:"arrow arrow1",
				imgid:"arrow",
				imgsrc:''
			},{
				imgclass:"arrow arrow2",
				imgid:"arrow",
				imgsrc:''
			},{
				imgclass:"arrow arrow3",
				imgid:"arrow",
				imgsrc:''
			}]
		}]

	},
//ex 3
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			},{
				textclass: "submit",
				textdata: data.string.check
			}
		],
		sidetextdiv:[{
			textdivclass:"leftTextBox",
			boxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_1",
				textclass:"bxTxt bxt_1",
				textdata:data.string.exchelp_txt_1
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_2",
				textclass:"bxTxt bxt_2",
				textdata:data.string.exchelp_txt_2
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_3",
				textclass:"bxTxt bxt_3",
				textdata:data.string.exchelp_txt_3
			}]
		}],
		eqex:[
			{
				eqexboxclass:"eqexBox box1",
				tbleblk:[{
					tableblkclass:"box1_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.tens
						},{
							// textclass:""
							tabledata:data.string.ones
						}]
					},{
						rowcontent:[{
							// textclass:""
							// tabledata:data.string.one
						},{
							// textclass:""
							// tabledata:data.string.five
						}]
					},{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.mul_sgn
						},{
							// textclass:""
							// tabledata:data.string.one
						}]
					}]
				}]
			},{
				eqexboxclass:"eqexBox box2",
				tbleblk:[{
					tableblkclass:"box2_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.blank
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					},{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.blank
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					}]
				}],
				qnbox:[{
					qnboxclass:"questionBox qb1",
					textclass:"arrowBox ab1",
					arrowimgclass:"arrowclass arw1",
				},{
					qnboxclass:"questionBox qb2",
					textclass:"arrowBox ab2",
					arrowimgclass:"arrowclass arw2"
				}]
			},{
				eqexboxclass:"eqexBox box3",
				tbleblk:[{
					tableblkclass:"box3_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.one
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					}]
				}],
				qnbox:[{
					qnboxclass:"questionBox qb3",
					textclass:"arrowBox ab3",
					arrowimgclass:"arrowclass arw3"
				}]
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"sideBoard",
				imgid:"black_board",
				imgsrc:''
			},{
				imgclass:"arrow arrow1",
				imgid:"arrow",
				imgsrc:''
			},{
				imgclass:"arrow arrow2",
				imgid:"arrow",
				imgsrc:''
			},{
				imgclass:"arrow arrow3",
				imgid:"arrow",
				imgsrc:''
			}]
		}]

	},
//ex 4
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			},{
				textclass: "submit",
				textdata: data.string.check
			}
		],
		sidetextdiv:[{
			textdivclass:"leftTextBox",
			boxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_1",
				textclass:"bxTxt bxt_1",
				textdata:data.string.exchelp_txt_1
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_2",
				textclass:"bxTxt bxt_2",
				textdata:data.string.exchelp_txt_2
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"eqnTxt_3",
				textclass:"bxTxt bxt_3",
				textdata:data.string.exchelp_txt_3
			}]
		}],
		eqex:[
			{
				eqexboxclass:"eqexBox box1",
				tbleblk:[{
					tableblkclass:"box1_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.tens
						},{
							// textclass:""
							tabledata:data.string.ones
						}]
					},{
						rowcontent:[{
							// textclass:""
							// tabledata:data.string.one
						},{
							// textclass:""
							// tabledata:data.string.five
						}]
					},{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.mul_sgn
						},{
							// textclass:""
							// tabledata:data.string.one
						}]
					}]
				}]
			},{
				eqexboxclass:"eqexBox box2",
				tbleblk:[{
					tableblkclass:"box2_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.blank
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					},{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.blank
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					}]
				}],
				qnbox:[{
					qnboxclass:"questionBox qb1",
					textclass:"arrowBox ab1",
					arrowimgclass:"arrowclass arw1",
				},{
					qnboxclass:"questionBox qb2",
					textclass:"arrowBox ab2",
					arrowimgclass:"arrowclass arw2"
				}]
			},{
				eqexboxclass:"eqexBox box3",
				tbleblk:[{
					tableblkclass:"box3_table",
					tablerow:[{
						rowcontent:[{
							// textclass:""
							tabledata:data.string.one
						},{
							// textclass:""
							tabledata:data.string.five
						}]
					}]
				}],
				qnbox:[{
					qnboxclass:"questionBox qb3",
					textclass:"arrowBox ab3",
					arrowimgclass:"arrowclass arw3"
				}]
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"sideBoard",
				imgid:"black_board",
				imgsrc:''
			},{
				imgclass:"arrow arrow1",
				imgid:"arrow",
				imgsrc:''
			},{
				imgclass:"arrow arrow2",
				imgid:"arrow",
				imgsrc:''
			},{
				imgclass:"arrow arrow3",
				imgid:"arrow",
				imgsrc:''
			}]
		}]

	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;
	var boxNum = 0;
	var qnArray=[2,3,4,5];
	var marksFlag = true;

	/*for limiting the questions to 10*/
	var $total_page = 4;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "car", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "iron", src: imgpath+"iron.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+"mango.png", type: createjs.AbstractLoader.IMAGE},
			{id: "button", src:imgpath+'button.png', type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src:imgpath+'plus.png', type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src:imgpath+'equal.png', type: createjs.AbstractLoader.IMAGE},
			{id: "multiply", src:imgpath+'multiple.png', type: createjs.AbstractLoader.IMAGE},
			{id: "buton", src:imgpath+'up.png', type: createjs.AbstractLoader.IMAGE},
			{id: "black_board", src:imgpath+'black_board01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src:imgpath+'arrow02.png', type: createjs.AbstractLoader.IMAGE},

//textboxes


			// sounds
			// sounds
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p2_1", src: soundAsset+"s3_p2_1.ogg"},
			{id: "s3_p2_2", src: soundAsset+"s3_p2_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(4);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		var updateScore = 0;
		// put_image_sec(content, countNext);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parent){
			// alert(parent);
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
}

	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		$(".arrowclass").attr('src',preload.getResult("buton").src);
		var fstAnsCount  = 0;
		var secAnsCount  = 0;
		var thirdAnsCount  = 0;
		var marksCount = 0;
	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 0 +1)) + 0;
	    return randNum;
	  }
	 	switch(countNext){
			case 0:
			case 1:
			case 2:
			case 3:
				countNext==0?sound_player("s3_p2"):"";
				boxNum=0;
				marksFlag = true;
				var randArNumPlace = rand_generator((qnArray.length)-1);
				var multiplier = qnArray[randArNumPlace];
			 	qnArray.splice(randArNumPlace, 1);
				// console.log(qnArray);
				$(".practice").hide(0);
				$(".bxt_2, .bxt_3").addClass("hideThis");
				$(".arrow1").addClass("showThis");
				var qnPosibArray = [[1,2,3,4],
														[1,2,3],
														[1,2],
														[1]
													];
				var firstNum =  multiplier==2?qnPosibArray[0][rand_generator((qnPosibArray[0].length)-1)]:
											  multiplier==3?qnPosibArray[1][rand_generator((qnPosibArray[1].length)-1)]:
		 									  multiplier==4?qnPosibArray[2][rand_generator((qnPosibArray[2].length)-1)]:1;

				var secNum = multiplier==2?qnPosibArray[0][rand_generator((qnPosibArray[0].length)-1)]:
										 multiplier==3?qnPosibArray[1][rand_generator((qnPosibArray[1].length)-1)]:
	 									 multiplier==4?qnPosibArray[2][rand_generator((qnPosibArray[2].length)-1)]:1;
 				var a = Number(""+firstNum+secNum);
				var thirdNum = multiplier;
				secAnsCount = Math.floor((a/10)*thirdNum)*10;
				var countb = secAnsCount-5;
				thirdAnsCount = a*thirdNum;
				var countc = thirdAnsCount-5;
				$(".eqnTxt_1:eq(0)").html(thirdNum);
				$(".eqnTxt_1:eq(1)").html(secNum);
				$(".eqnTxt_1:eq(2)").html(secNum*thirdNum);
				$(".eqnTxt_2:eq(0)").html(thirdNum);
				$(".eqnTxt_2:eq(1)").html(firstNum);
				// $(".eqnTxt_2:eq(2)").html(firstNum*10*thirdNum);
				$(".eqnTxt_3:eq(0)").html(firstNum*10*thirdNum);
				$(".eqnTxt_3:eq(1)").html(secNum*thirdNum);
				// $(".eqnTxt_3:eq(2)").html((firstNum*10*thirdNum)+(secNum*thirdNum));

				$(".box2_table td").addClass("hideThis");
				$(".box3_table td").addClass("hideThis");
				$(".box1_table tr:nth-of-type(2) td:eq(0)").html(firstNum);
				$(".box1_table tr:nth-of-type(2) td:eq(1)").html(secNum);
				$(".box1_table tr:nth-of-type(3) td:eq(1)").html(thirdNum);

				$(".box2_table tr:nth-of-type(1) td:eq(1)").html(secNum*thirdNum);
				$(".box2_table tr:nth-of-type(2) td:eq(0)").html(Math.floor((a/10)*thirdNum));
				$(".box2_table tr:nth-of-type(2) td:eq(1)").html(0);

				$(".box3_table tr:nth-of-type(1) td:eq(0)").html(Math.floor((a/10)*thirdNum));
				$(".box3_table tr:nth-of-type(1) td:eq(1)").html((a%10)*thirdNum);

				$(".qb1").addClass("showThis");
				arwClk(fstAnsCount, secAnsCount, thirdAnsCount, countb, countc);
				$(".submit").click(function(){
					submitFunction(secNum*thirdNum,Math.floor((a/10)*thirdNum)*10, a*thirdNum);
				});
		 break;
	 	}

		function sound_player(sound_id, next){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				if(next)
				navigationcontroller();
			});
		}
	function arwClk(fstAnsCount, secAnsCount, thirdAnsCount, countb, countc){
				$(".arw1").click(function(){
					fstAnsCount+=1;
					$(".ab1").html(fstAnsCount);
					fstAnsCount==10?fstAnsCount=0:fstAnsCount;
				});
				$(".arw2").click(function(){
					countb+=1;
					$(".ab2").html(countb);
					countb==(secAnsCount+5)?countb=(secAnsCount-6):countb;
				});
				$(".arw3").click(function(){
					countc+=1;
					$(".ab3").html(countc);
					countc==(thirdAnsCount+5)?countc=(thirdAnsCount-6):countc;
				});
			}
	function submitFunction(cor1, cor2, cor3){
		if(boxNum == 0){
			if ($(".ab1").text()==cor1){
				$(".qb1, .arrow1").removeClass("showThis");
				$(".qb2, .bxt_2, .arrow2").addClass("showThis");
				$(".box2_table tr:nth-of-type(1) td").addClass("showThis");
				// play_correct_incorrect_sound(1);
				marksCount+=1;
				boxNum+=1;
					countNext==0?sound_player("s3_p2_1"):"";
			}else{
				// marksFlag = false;
				marksCount-=1;
				incorrect($(".ab1"));
			}
		}
		else if(boxNum == 1){
			if ($(".ab2").text()==cor2){
				$(".qb2, .arrow2").removeClass("showThis");
				$(".qb3, .bxt_3, .arrow3").addClass("showThis");
				$(".box2_table tr:nth-of-type(2) td").addClass("showThis brdbtm");
				// play_correct_incorrect_sound(1);
				marksCount+=1;
				boxNum+=1;
					countNext==0?sound_player("s3_p2_2"):"";
			}else{
				// marksFlag = false;
				marksCount-=1;
				incorrect($(".ab2"));
			}
		}
		else if(boxNum == 2){
			if ($(".ab3").text()==cor3){
				$(".qb3, .arrow3").removeClass("showThis");
				$(".box3_table tr:nth-of-type(1) td").addClass("showThis");
				$(".eqnNum span").css("background","transparent");
				play_correct_incorrect_sound(1);
				$(".submit").css("pointer-events","none");
				marksCount+=1;
					$nextBtn.show(0);
				// navigationcontroller();
				boxNum+=1;
			}else{
				// marksFlag = false;
				marksCount-=1;
				incorrect($(".ab3"));
			}
			marksCount==3?testin.update(true):testin.update(false);
			// if(marksCount==3)
		}
	}
	function incorrect(bxClass){
		bxClass.css({
			"background":"#f00",
			"border":"2px solid #990000"
		});
		play_correct_incorrect_sound(0);
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });
 	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function loadimage(imgrep,imgtext,imgsrc,quesNo){
		imgrep.attr("src",imgsrc);
		imgtext.text(quesNo);
	}

/*=====  End of Templates Controller Block  ======*/
});

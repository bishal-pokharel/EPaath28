var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// coverpage
	{
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'covertxt'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'cover_page',
				imgclass:'bg_full'
			}]
		}]
	},
	// slide 2
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'magician_mid',
				imgid:'magician-nepali',
				imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p1s2txt
		}]
	},
	// slide 3
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"handContainer",
			imagestoshow:[{
				imgwithtext:true,
				imgTxtContainerclass:'oneHandContainer',
				imgclass:'handInCont',
				imgid:'hand_right',
				imgsrc:"",
				textclass:"midHandTxt",
				textdata:data.string.five
			}]
		}],
		extratextblock:[{
			textclass:"Btm-whiteBox-txt",
			textdata:data.string.p1s3txt
		}]
	},
	// slide 4
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'plus',
				imgid:'plus',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"handContainer",
			imagestoshow:[{
				imgwithtext:true,
				imgTxtContainerclass:'twoHandCont',
				imgclass:'handInCont',
				imgid:'hand_right',
				imgsrc:"",
				textclass:"midHandTxt",
				textdata:data.string.five
			},{
				imgwithtext:true,
				imgTxtContainerclass:'twoHandCont',
				imgclass:'handInCont',
				imgid:'hand_right',
				imgsrc:"",
				textclass:"midHandTxt",
				textdata:data.string.five
			}]
		}],
		extratextblock:[{
			textclass:"Btm-whiteBox-txt",
			textdata:data.string.p1s4txt
		}]
	},
	// slide 5
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"handContainer flxWrap",
			imagestoshow:[{
				imgwithtext:true,
				imgTxtContainerclass:'fvHndCont',
				imgclass:'handInCont',
				imgid:'hand_right',
				imgsrc:"",
				textclass:"midHandTxt",
				textdata:data.string.five
			},{
				imgwithtext:true,
				imgTxtContainerclass:'fvHndCont',
				imgclass:'handInCont',
				imgid:'hand_right',
				imgsrc:"",
				textclass:"midHandTxt",
				textdata:data.string.five
			},{
				imgwithtext:true,
				imgTxtContainerclass:'fvHndCont',
				imgclass:'handInCont',
				imgid:'hand_right',
				imgsrc:"",
				textclass:"midHandTxt",
				textdata:data.string.five
			},{
				imgwithtext:true,
				imgTxtContainerclass:'fvHndCont',
				imgclass:'handInCont',
				imgid:'hand_right',
				imgsrc:"",
				textclass:"midHandTxt",
				textdata:data.string.five
			},{
				imgwithtext:true,
				imgTxtContainerclass:'fvHndCont',
				imgclass:'handInCont',
				imgid:'hand_right',
				imgsrc:"",
				textclass:"midHandTxt",
				textdata:data.string.five
			}]
		}],
		extratextblock:[{
			textclass:"Btm-whiteBox-txt",
			textdata:data.string.p1s5txt
		}]
	},
	// slide 6
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			}]
		}],
		extratextblock:[{
			textclass:"Btm-whiteBox-txt",
			textdata:data.string.p1s6txt
		}]
	},
	// slide 7
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				}]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			datahighlightflag:"true",
			datahighlightcustomclass:"blueTxt",
			textclass:"boxClass",
			textdata:data.string.p1s7txt
		}]
	},
	// slide 8
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				}]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			datahighlightflag:"true",
			datahighlightcustomclass:"blueTxt",
			textclass:"boxClass",
			textdata:data.string.p1s8txt
		}]
	},
	// slide 9
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			}]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			datahighlightflag:"true",
			datahighlightcustomclass:"blueTxt",
			textclass:"boxClass",
			textdata:data.string.p1s9txt
		}]
	},
	// slide 10
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'magician_mid',
				imgid:'magician-nepali',
				imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p1s10txt
		}]
	},
	// slide 11
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'magician_mid',
				imgid:'magician-nepali',
				imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p1s11txt
		}]
	},
	// slide 12
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				},{
					imgclass:'elvHands',
					imgid:'hand_right',
					imgsrc:"",
				}]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			datahighlightflag:"true",
			datahighlightcustomclass:"blueTxt",
			textclass:"boxClass fntAbz",
			textdata:data.string.p1s12txt
		}]
	},
	// slide 13
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			}]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			datahighlightflag:"true",
			datahighlightcustomclass:"blueTxt",
			textclass:"boxClass fntAbz",
			textdata:data.string.p1s13txt
		}]
	},
	// slide 14
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont just",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			}]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			datahighlightflag:"true",
			datahighlightcustomclass:"blueTxt",
			textclass:"boxClass fntAbz",
			textdata:data.string.p1s14txt
		}]
	},
	// slide 15
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont just",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			}]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			datahighlightflag:"true",
			datahighlightcustomclass:"blueTxt",
			textclass:"boxClass fntAbz",
			textdata:data.string.p1s14txt
		}]
	},
	// slide 16
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			}]
		},
		{
			handtext:true,
			imageblockaddnalclass:"manyHandCont just",
			imagestoshow:[{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			},{
				imgclass:'elvHands',
				imgid:'hand_right',
				imgsrc:"",
			}]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			datahighlightflag:"true",
			datahighlightcustomclass:"blueTxt",
			textclass:"boxClass fntAbz",
			textdata:data.string.p1s15txt
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circus-new", src: imgpath+"circus-new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "magician-nepali", src: imgpath+"magician-nepali.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand_right", src: imgpath+"hand01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src: imgpath+"plus01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p7_1", src: soundAsset+"s1_p7_1.ogg"},
			{id: "s1_p7_2", src: soundAsset+"s1_p7_2.ogg"},
			{id: "s1_p7_3", src: soundAsset+"s1_p7_3.ogg"},
			{id: "s1_p7_4", src: soundAsset+"s1_p7_4.ogg"},
			{id: "s1_p7_5", src: soundAsset+"s1_p7_5.ogg"},
			{id: "s1_p7_6", src: soundAsset+"s1_p7_6.ogg"},
			{id: "s1_p7_7", src: soundAsset+"s1_p7_7.ogg"},
			{id: "s1_p7_8", src: soundAsset+"s1_p7_8.ogg"},
			{id: "s1_p7_9", src: soundAsset+"s1_p7_9.ogg"},
			{id: "s1_p7_10", src: soundAsset+"s1_p7_10.ogg"},
			{id: "s1_p7_11", src: soundAsset+"s1_p7_11.ogg"},
			{id: "s1_p7_12", src: soundAsset+"s1_p7_12.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p8_1", src: soundAsset+"s1_p8_1.ogg"},
			{id: "s1_p8_2", src: soundAsset+"s1_p8_2.ogg"},
			{id: "s1_p8_3", src: soundAsset+"s1_p8_3.ogg"},
			{id: "s1_p8_4", src: soundAsset+"s1_p8_4.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p9_1", src: soundAsset+"s1_p9_1.ogg"},
			{id: "s1_p9_2", src: soundAsset+"s1_p9_2.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "s1_p12", src: soundAsset+"s1_p12.ogg"},
			{id: "s1_p12_1", src: soundAsset+"s1_p12_1.ogg"},
			{id: "s1_p13", src: soundAsset+"s1_p13.ogg"},
			{id: "s1_p13_1", src: soundAsset+"s1_p13_1.ogg"},
			{id: "s1_p13_2", src: soundAsset+"s1_p13_2.ogg"},
			{id: "s1_p13_3", src: soundAsset+"s1_p13_3.ogg"},
			{id: "s1_p14", src: soundAsset+"s1_p14.ogg"},
			{id: "s1_p14_1", src: soundAsset+"s1_p14_1.ogg"},
			{id: "s1_p14_2", src: soundAsset+"s1_p14_2.ogg"},
			{id: "s1_p14_3", src: soundAsset+"s1_p14_3.ogg"},
			{id: "s1_p14_4", src: soundAsset+"s1_p14_4.ogg"},
			{id: "s1_p14_5", src: soundAsset+"s1_p14_5.ogg"},
			{id: "s1_p14_6", src: soundAsset+"s1_p14_6.ogg"},
			{id: "s1_p15", src: soundAsset+"s1_p15.ogg"},
			{id: "s1_p16", src: soundAsset+"s1_p16.ogg"},
			{id: "s1_p16_1", src: soundAsset+"s1_p16_1.ogg"},
			{id: "s1_p16_2", src: soundAsset+"s1_p16_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var numcount = 0;
		var handcount = 1;
		var soundCount = 0;


		function appearOneByOne(soundArray,numcount){
			$(".blueTxt:eq("+handcount+")").css("opacity","1");
			$(".elvHands:eq("+numcount+")").addClass("sclImg");
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundArray[numcount]);
			current_sound.play();
			current_sound.on('complete', function(){
				handcount+=1;
				numcount+=1;
				if(numcount<=10){
					appearOneByOne(soundArray,numcount)
				}else{
					$(".blueTxt:eq(12)").css("opacity","1");
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play("s1_p7_12");
					current_sound_1.play();
					current_sound_1.on('complete', function(){
						nav_button_controls(100);
					});
				}
			});
		}
		switch(countNext) {
			case 1:
				sound_player("s1_p"+(countNext+1),1);
			break;
			case 6:
				var soundArray=["s1_p7_1","s1_p7_2","s1_p7_3","s1_p7_4"
												,"s1_p7_5","s1_p7_6","s1_p7_7","s1_p7_8"
												,"s1_p7_9","s1_p7_10","s1_p7_11"
											];
				for(var i=1;i<=11;i++){
					$(".blueTxt:eq("+i+")").css("color","#1f6399");
				}

				$(".blueTxt").css("opacity","0");
					$(".blueTxt:eq(0)").css("opacity","1");
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p7");
					current_sound.play();
					current_sound.on('complete', function(){
						appearOneByOne(soundArray,numcount);
					});
			break;
			case 7:
				var soundArray = ["s1_p8","s1_p8_1","s1_p8_2",
													"s1_p8_3","s1_p8_4"
												];
				$(".blueTxt:eq(1), .blueTxt:eq(3)").css("color","#1f6399");
				$(".blueTxt").css("opacity","0");
			 	soundTxtAnim(soundArray, soundCount,".blueTxt");
			break;
			case 8:
				$(".blueTxt:eq(1), .blueTxt:eq(3)").css("color","#1f6399");
				$(".blueTxt").hide(0);
					$(".blueTxt:eq(0), .blueTxt:eq(1)").fadeIn(500);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p9");
					current_sound.play();
					current_sound.on('complete', function(){
						$(".blueTxt:eq(2), .blueTxt:eq(3)").fadeIn(500);
						createjs.Sound.stop();
						current_sound_1 = createjs.Sound.play("s1_p9_1");
						current_sound_1.play();
						current_sound_1.on('complete', function(){
							$(".blueTxt:eq(4)").fadeIn(500);
						createjs.Sound.stop();
						current_sound_2 = createjs.Sound.play("s1_p9_2");
						current_sound_2.play();
						current_sound_2.on('complete', function(){
							nav_button_controls(100);
						});
					});
				});
			break;
			case 11:
				$(".blueTxt").css({"color":"#1f6399","opacity":"0"});
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p12");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".blueTxt").animate({opacity:"1"},500);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p12_1");
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls(100);
						});
				});
			break;
			case 12:
				var soundArray = ["s1_p13","s1_p13_1","s1_p13_2",
													"s1_p13_3"];
				$(".blueTxt:eq(1),.blueTxt:eq(3)").css("color","#1f6399");
				$(".blueTxt").css("opacity","0");
				$(".elvHands").animate({
					width:"20%"
				},1000,function(){
					$(".imageblockclass").css("justify-content","space-around");
				});
			 soundTxtAnim(soundArray, soundCount,".blueTxt");
			break;
			case 13:
				var soundArray = ["s1_p14_4","s1_p14_5",
													"s1_p14_6"
												];
				$(".imageblockclass").append("<div class='upperdivBox'></div>");
				$(".imageblockclass").append("<div class='lowerdivBox'></div>");
				$(".upperdivBox").append("<p class='boxTxt'>"+data.string.tn_hnd+"</p>");
				$(".lowerdivBox").append("<p class='boxTxt wdth95'>"+data.string.one_hnd+"</p>");
				$(".blueTxt:eq(1),.blueTxt:eq(3),.blueTxt:eq(5),.blueTxt:eq(6)").css("color","#1f6399");
				$(".blueTxt").css("opacity","0");
				$(".blueTxt:eq(0),.blueTxt:eq(1),.blueTxt:eq(2),.blueTxt:eq(3),.blueTxt:eq(4)").css("opacity","1");
				$(".elvHands").css("width","20%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p14_4");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".blueTxt:eq(5)").animate({opacity:"1"},500);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p14_5");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".blueTxt:eq(6)").animate({opacity:"1"},500);
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s1_p14_6");
								current_sound.play();
								current_sound.on('complete', function(){
									nav_button_controls(100);
								});
						});
					});
			break;
			case 14:
				$(".imageblockclass").append("<div class='upperdivBox'></div>");
				$(".imageblockclass").append("<div class='lowerdivBox'></div>");
				$(".upperdivBox").append("<p class='boxTxt'>"+data.string.tn_hnd_fng+"</p>");
				$(".lowerdivBox").append("<p class='boxTxt wdth95'>"+data.string.one_hnd_fng+"</p>");
				$(".elvHands").css("width","20%");
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p15");
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(100);
					});
			break;
			case 15:
				var soundArray = ["s1_p16","s1_p16_1","s1_p16_2"];
				$(".imageblockclass").append("<div class='upperdivBox'></div>");
				$(".imageblockclass").append("<div class='lowerdivBox'></div>");
				$(".upperdivBox").append("<p class='boxTxt'>"+data.string.tn_hnd_fng+"</p>");
				$(".lowerdivBox").append("<p class='boxTxt wdth95'>"+data.string.one_hnd_fng+"</p>");
				$(".blueTxt:eq(1)").css("color","#1f6399");
				$(".elvHands").css("width","20%");
				$(".blueTxt").css("opacity","0");
				 soundTxtAnim(soundArray, soundCount,".blueTxt");
			break;
			default:
				sound_player("s1_p"+(countNext+1),1);
			break;

		}
		function soundTxtAnim(soundArray, soundCount, textClass){
			// var count = 0;
			var soundLen = soundArray.length-1;
			$(textClass+":eq("+soundCount+")").css("opacity","1");;
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundArray[soundCount]);
			current_sound.play();
			current_sound.on('complete', function(){
				soundCount+=1;
				soundCount<=soundLen?soundTxtAnim(soundArray, soundCount, textClass):nav_button_controls(1000);;
			});
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

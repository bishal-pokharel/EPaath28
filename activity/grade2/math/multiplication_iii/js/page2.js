var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'magician_mid',
				imgid:'magician-nepali',
				imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s1txt
		}]
	},
	// slide 2
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'magician_mid',
				imgid:'magician-nepali',
				imgsrc:""
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"text_box",
			imgsrc:'',
			textclass:"textInSp",
			textdata:data.string.p2s2txt
		}]
	},
	// slide 3
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'bb_fst',
				imgid:'black_board',
				imgsrc:""
			}
		]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			bxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"blueTxt",
				textclass:"boxClass bx1",
				textdata:data.string.p2s3_rghtfst
			},{
				textclass:"boxClass bx2",
				textdata:data.string.p2s3_rghtsec
			},
			{
				tableinbox:true,
				tableinbox:[{
					tableblkclass:"rbtbl",
					textclass:"btmLine",
					tablerow:[{
						rowcontent:[{
							textclass:"blkfnt t1",
							tabledata:data.string.one
						},{
							textclass:"blkfnt t2",
							tabledata:data.string.one
						}]
					},{
						rowcontent:[{
							textclass:"blkfnt t3",
							tabledata:data.string.mul_sgn
						},{
							textclass:"blkfnt t4",
							tabledata:data.string.five
						}]
					}]

				}]
			},
			{
				textclass:"boxClass bx3",
				textdata:data.string.p2s3_rght_third
			}]
		}],
		tbleblk:[
			{
				textclass:"tableTopTXt",
				textdata:data.string.p2s3_inst_txt,
				tableblkclass:"midTableBlock",
				tablerow:[
					{
						rowcontent:[
							{
								tabledata:data.string.p2s3_eq_1_rght
							},{
								tabledata:data.string.p2s3_eq_1_left
						}]
						},{
						rowcontent:[{
								// tabledata:data.string.tn_hnd
							},{
								tabledata:data.string.p2s3_eq_2_left
						}]
						},{
						rowcontent:[{
								// tabledata:data.string.tn_hnd
							},{
								tabledata:data.string.p2s3_eq_3_left
						}]
				}]
			}
		]
	},
	// slide 4
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'bb_fst',
				imgid:'black_board',
				imgsrc:""
			}
		]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			bxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"blueTxt",
				textclass:"boxClass fntAbz",
				textdata:data.string.p2s4_right
			}]
		}],
		tbleblk:[
			{
				textclass:"btmLine creamBg",
				tableblkclass:"shortEqnBlk",
				tablerow:[
					{
						rowcontent:[
							{
								textclass:"tens",
								tabledata:data.string.tens
							},{
								textclass:"ones",
								tabledata:data.string.ones
						}]
						},{
						rowcontent:[{
								tabledata:data.string.one
							},{
								tabledata:data.string.one
						}]
						},{
						rowcontent:[{
								tabledata:data.string.mul_sgn
							},{
								tabledata:data.string.five
						}]
				}]
			}
		]
	},
	// slide 5
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'bb_fst',
				imgid:'black_board',
				imgsrc:""
			},{
				imgclass:'arrow ar-5',
				imgid:'arrow',
				imgsrc:""
			}
		]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			bxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"blueTxt",
				textclass:"boxClass fntAbz",
				textdata:data.string.p2s5_right
			}]
		}],
		tbleblk:[
			{
				// textclass:"btmLine creamBg",
				tableblkclass:"shortEqnBlk",
				tablerow:[
					{
						btmline:true,
						rowcontent:[
							{
								tabledata:data.string.tens
							},{
								tabledata:data.string.ones
						}]
					},{
						rowcontent:[{
								tabledata:data.string.one
							},{
								tabledata:data.string.one
						}]
					},{
						rowcontent:[{
								tabledata:data.string.mul_sgn
							},{
								tabledata:data.string.five
						}]
					},{
						rowcontent:[{
								// tabledata:data.string.mul_sgn
							},{
								tabledata:data.string.five
						}]
					},{
						rowcontent:[{
								tabledata:data.string.five
							},{
								tabledata:data.string.zero
						}]
					},{
						rowcontent:[{
								tabledata:data.string.five
							},{
								tabledata:data.string.five
						}]
					}]
			}
		]
	},
	// slide 6
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'bb_fst',
				imgid:'black_board',
				imgsrc:""
			},{
				imgclass:'arrow ar-6',
				imgid:'arrow',
				imgsrc:""
			}
		]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			bxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"blueTxt",
				textclass:"boxClass fntAbz",
				textdata:data.string.p2s6_right
			}]
		}],
		tbleblk:[
			{
				// textclass:"btmLine creamBg",
				tableblkclass:"shortEqnBlk",
				tablerow:[
					{
						btmline:true,
						rowcontent:[
							{
								tabledata:data.string.tens
							},{
								tabledata:data.string.ones
						}]
					},{
						rowcontent:[{
								tabledata:data.string.one
							},{
								tabledata:data.string.one
						}]
					},{
						rowcontent:[{
								tabledata:data.string.mul_sgn
							},{
								tabledata:data.string.five
						}]
					},{
						rowcontent:[{
								// tabledata:data.string.mul_sgn
							},{
								tabledata:data.string.five
						}]
					},{
						rowcontent:[{
								tabledata:data.string.five
							},{
								tabledata:data.string.zero
						}]
					},{
						rowcontent:[{
								tabledata:data.string.five
							},{
								tabledata:data.string.five
						}]
					}]
			}
		]
	},
	// slide 7
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'circus-new',
				imgsrc:""
			},{
				imgclass:'bb_fst',
				imgid:'black_board',
				imgsrc:""
			},{
				imgclass:'arrow ar-7',
				imgid:'arrow',
				imgsrc:""
			}
		]
		}],
		righttextdiv:[{
			righttextboxclass:"rightTextBox",
			bxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"blueTxt",
				textclass:"boxClass fntAbz",
				textdata:data.string.p2s7_right
			}]
		}],
		tbleblk:[
			{
				// textclass:"btmLine creamBg",
				tableblkclass:"shortEqnBlk",
				tablerow:[
					{
						btmline:true,
						rowcontent:[
							{
								tabledata:data.string.tens
							},{
								tabledata:data.string.ones
						}]
					},{
						rowcontent:[{
								tabledata:data.string.one
							},{
								tabledata:data.string.one
						}]
					},{
						rowcontent:[{
								tabledata:data.string.mul_sgn
							},{
								tabledata:data.string.five
						}]
					},{
						rowcontent:[{
								// tabledata:data.string.mul_sgn
							},{
								tabledata:data.string.five
						}]
					},{
						rowcontent:[{
								tabledata:data.string.five
							},{
								tabledata:data.string.zero
						}]
					},{
						rowcontent:[{
								tabledata:data.string.five
							},{
								tabledata:data.string.five
						}]
					}]
			}
		]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circus-new", src: imgpath+"circus-new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "magician-nepali", src: imgpath+"magician-nepali.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand_right", src: imgpath+"hand01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "black_board", src: imgpath+"black_board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p3_1", src: soundAsset+"s2_p3_1.ogg"},
			{id: "s2_p3_2", src: soundAsset+"s2_p3_2.ogg"},
			{id: "s2_p3_3", src: soundAsset+"s2_p3_3.ogg"},
			{id: "s2_p3_4", src: soundAsset+"s2_p3_4.ogg"},
			{id: "s2_p3_5", src: soundAsset+"s2_p3_5.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p5_1", src: soundAsset+"s2_p5_1.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s2_p6_1", src: soundAsset+"s2_p6_1.ogg"},
			{id: "s2_p7", src: soundAsset+"s2_p7.ogg"},
			{id: "s2_p7_1", src: soundAsset+"s2_p7_1.ogg"},
			{id: "s1_p7_2", src: soundAsset+"s1_p7_2.ogg"},
			{id: "s1_p16_1", src: soundAsset+"s1_p16_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);



		switch(countNext) {
			case 1:
				sound_player("s2_p2",1);
			break;
			case 2:
				$(".rbtbl").css("color","#000");
				$(".boxClass, .rbtbl").hide(0);
				$(".midTableBlock table tr").css("opacity","0");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p3");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".midTableBlock table tr:nth-of-type(1)").css("opacity","1");
					createjs.Sound.stop();
					// current_sound_1 = createjs.Sound.play("s1_p16_1");
					current_sound_1 = createjs.Sound.play("s2_p3_1");
					current_sound_1.play();
					current_sound_1.on('complete', function(){
						$(".midTableBlock table tr:nth-of-type(2)").css("opacity","1");
						createjs.Sound.stop();
						current_sound_2 = createjs.Sound.play("s2_p3_2");
						current_sound_2.play();
						current_sound_2.on('complete', function(){
							$(".midTableBlock table tr:nth-of-type(3)").css("opacity","1");
							current_sound_3 = createjs.Sound.play("s2_p3_3");
							current_sound_3.play();
							current_sound_3.on("complete", function(){
								$(".bx1").fadeIn(1000);
								$(".bx2, .rbtbl").fadeIn(2000);
								createjs.Sound.stop();
								current_sound_4 = createjs.Sound.play("s2_p3_4");
								current_sound_4.play();
								current_sound_4.on('complete', function(){
									$(".bx3").fadeIn(1000);
									createjs.Sound.stop();
									current_sound_5 = createjs.Sound.play("s2_p3_5");
									current_sound_5.play();
									current_sound_5.on("complete", function(){
											nav_button_controls(100);
									});
								});
							});
						});
					});
				});
			break;
			case 3:
			createjs.Sound.stop();
			current_sound_4 = createjs.Sound.play("s2_p4");
			current_sound_4.play();
			current_sound_4.on('complete', function(){
				nav_button_controls();
			});
				setTimeout(function(){
					$(".ones").addClass("blink");
				},1000);
				setTimeout(function(){
					$(".tens").addClass("blink");
				},2000);
			break;
			case 4:
				$(".shortEqnBlk").append("<div class='topHghDiv'></div>");
				$(".shortEqnBlk").append("<div class='btmHghDiv'></div>");
				$(".shortEqnBlk table tr:nth-of-type(5), .shortEqnBlk table tr:nth-of-type(6)").css("opacity","0");
				$(".yellowBg, .topHghDiv, .btmHghDiv, .arrow").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p5");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".yellowBg, .topHghDiv, .arrow").fadeIn(1000);
					$(".btmHghDiv").delay(1000).fadeIn(1000);
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play("s2_p5_1");
					current_sound_1.on('complete', function(){
						nav_button_controls(1000);
					});
				});
			break;
			case 5:
				$(".shortEqnBlk").append("<div class='s6CircDiv s6df'></div>");
				$(".shortEqnBlk").append("<div class='s6CircDiv s6ds'></div>");
				$(".yellowBg, .s6df, .s6ds, .arrow").hide(0);
				$(".shortEqnBlk table tr:nth-of-type(5), .shortEqnBlk table tr:nth-of-type(6)").css("opacity","0");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p6");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".yellowBg, .s6df").fadeIn(1000);
					$(".shortEqnBlk table tr:nth-of-type(5)").delay(800).animate({opacity:"1"},100);
					$(".s6ds, .arrow").delay(1000).fadeIn(1000);
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play("s2_p6_1");
					current_sound_1.on('complete', function(){
						nav_button_controls(1000);
					});
				});
			break;
			case 6:
				$(".shortEqnBlk").append("<div class='s6CircDiv s7df'></div>");
				$(".yellowBg, .s7df,.arrow").hide(0);
				$(".shortEqnBlk table tr:nth-of-type(6)").css("opacity","0");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p7");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".shortEqnBlk table tr:nth-of-type(6)").css("opacity","1");
					$(".yellowBg, .s7df, .arrow").fadeIn(1000);
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play("s2_p7_1");
					current_sound_1.on('complete', function(){
						nav_button_controls(1000);
					});
				});
			break;
			default:
				sound_player("s2_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():"";
		});
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var images = content[count].imageblock;
			for(var j=0; j<images.length; j++){
			var imageblock = content[count].imageblock[j];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

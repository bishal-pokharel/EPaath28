var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
// slide 1
	{
		contentblockadditionalclass:"creamBg",
		extratextblock:[{
			textclass: "diyTxt",
			textdata: data.string.diy
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"bg_diy",
				imgsrc:''
			}]
		}]
	},
//slide 2
	{
	contentblockadditionalclass:"creamBg",
		extratextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p3_instrn_1
		},{
			textclass: "submit",
			textdata: data.string.check
		},{
			textclass: "practice",
			textdata: data.string.practice
		}
	],
	sidetextdiv:[{
		textdivclass:"leftTextBox",
		boxtxt:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"eqnTxt_1",
			textclass:"bxTxt bxt_1",
			textdata:data.string.page3help_txt_1
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"eqnTxt_2",
			textclass:"bxTxt bxt_2",
			textdata:data.string.page3help_txt_2
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"eqnTxt_3",
			textclass:"bxTxt bxt_3",
			textdata:data.string.page3help_txt_3
		}]
	}],
	eqex:[
		{
			eqexboxclass:"eqexBox box1",
			tbleblk:[{
				tableblkclass:"box1_table",
				tablerow:[{
					rowcontent:[{
						// textclass:""
						tabledata:data.string.tens
					},{
						// textclass:""
						tabledata:data.string.ones
					}]
				},{
					rowcontent:[{
						// textclass:""
						// tabledata:data.string.one
					},{
						// textclass:""
						// tabledata:data.string.five
					}]
				},{
					rowcontent:[{
						// textclass:""
						tabledata:data.string.mul_sgn
					},{
						// textclass:""
						// tabledata:data.string.one
					}]
				}]
			}]
		},{
			eqexboxclass:"eqexBox box2",
			tbleblk:[{
				tableblkclass:"box2_table",
				tablerow:[{
					rowcontent:[{
						// textclass:""
						tabledata:data.string.blank
					},{
						// textclass:""
						tabledata:data.string.five
					}]
				},{
					rowcontent:[{
						// textclass:""
						tabledata:data.string.blank
					},{
						// textclass:""
						tabledata:data.string.five
					}]
				}]
			}],
			qnbox:[{
				qnboxclass:"questionBox qb1",
				textclass:"arrowBox ab1",
				arrowimgclass:"arrowclass arw1",
			},{
				qnboxclass:"questionBox qb2",
				textclass:"arrowBox ab2",
				arrowimgclass:"arrowclass arw2"
			}]
		},{
			eqexboxclass:"eqexBox box3",
			tbleblk:[{
				tableblkclass:"box3_table",
				tablerow:[{
					rowcontent:[{
						// textclass:""
						tabledata:data.string.one
					},{
						// textclass:""
						tabledata:data.string.five
					}]
				}]
			}],
			qnbox:[{
				qnboxclass:"questionBox qb3",
				textclass:"arrowBox ab3",
				arrowimgclass:"arrowclass arw3"
			}]
		}
	],
	imageblock:[{
		imagestoshow:[{
			imgclass:"sideBoard",
			imgid:"black_board",
			imgsrc:''
		},{
			imgclass:"arrow arrow1",
			imgid:"arrow",
			imgsrc:''
		},{
			imgclass:"arrow arrow2",
			imgid:"arrow",
			imgsrc:''
		},{
			imgclass:"arrow arrow3",
			imgid:"arrow",
			imgsrc:''
		}]
	}]

},

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var boxNum = 0;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: 'images/right.png', type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "button", src:imgpath+'button.png', type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src:imgpath+'plus.png', type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src:imgpath+'equal.png', type: createjs.AbstractLoader.IMAGE},
			{id: "multiply", src:imgpath+'multiple.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bird", src:imgpath+'six_bird.png', type: createjs.AbstractLoader.IMAGE},
			{id: "buton", src:imgpath+'up.png', type: createjs.AbstractLoader.IMAGE},
			{id: "black_board", src:imgpath+'black_board01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src:imgpath+'arrow02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bg_diy", src:imgpath+'bg_diy.png', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p2_1", src: soundAsset+"s3_p2_1.ogg"},
			{id: "s3_p2_2", src: soundAsset+"s3_p2_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		$(".arrowclass").attr('src',preload.getResult("buton").src);
		var fstAnsCount  = 0;
		var secAnsCount  = 0;
		var thirdAnsCount  = 0;
	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 0 +1)) + 0;
	    return randNum;
	  }
		switch(countNext){
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
				sound_player("s3_p2");
				$(".practice").hide(0);
				$(".bxt_2, .bxt_3").addClass("hideThis");
				$(".arrow1").addClass("showThis");
				var qnPosibArray = [[1,2,3,4],
														[1,2,3],
														[1,2],
														[1]
													];
				var multiplier = Math.floor(Math.random() * (5 - 2 +1)) + 2;
				var firstNum =  multiplier==2?qnPosibArray[0][rand_generator((qnPosibArray[0].length)-1)]:
											  multiplier==3?qnPosibArray[1][rand_generator((qnPosibArray[1].length)-1)]:
		 									  multiplier==4?qnPosibArray[2][rand_generator((qnPosibArray[2].length)-1)]:1;

				var secNum = multiplier==2?qnPosibArray[0][rand_generator((qnPosibArray[0].length)-1)]:
										 multiplier==3?qnPosibArray[1][rand_generator((qnPosibArray[1].length)-1)]:
	 									 multiplier==4?qnPosibArray[2][rand_generator((qnPosibArray[2].length)-1)]:1;
 				var a = Number(""+firstNum+secNum);
				var thirdNum = multiplier;
				secAnsCount = Math.floor((a/10)*thirdNum)*10;
				var countb = secAnsCount-5;
				thirdAnsCount = a*thirdNum;
				var countc = thirdAnsCount-5;
				$(".eqnTxt_1:eq(0)").html(thirdNum);
				$(".eqnTxt_1:eq(1)").html(secNum);
				$(".eqnTxt_1:eq(2), .eqnTxt_1:eq(3)").html(secNum*thirdNum);
				$(".eqnTxt_2:eq(0)").html(thirdNum);
				$(".eqnTxt_2:eq(1)").html(firstNum);
				$(".eqnTxt_2:eq(2)").html(firstNum*10*thirdNum);
				$(".eqnTxt_3:eq(0)").html(firstNum*10*thirdNum);
				$(".eqnTxt_3:eq(1)").html(secNum*thirdNum);
				$(".eqnTxt_3:eq(2)").html((firstNum*10*thirdNum)+(secNum*thirdNum));

				$(".box2_table td").addClass("hideThis");
				$(".box3_table td").addClass("hideThis");
				$(".box1_table tr:nth-of-type(2) td:eq(0)").html(firstNum);
				$(".box1_table tr:nth-of-type(2) td:eq(1)").html(secNum);
				$(".box1_table tr:nth-of-type(3) td:eq(1)").html(thirdNum);

				$(".box2_table tr:nth-of-type(1) td:eq(1)").html(secNum*thirdNum);
				$(".box2_table tr:nth-of-type(2) td:eq(0)").html(Math.floor((a/10)*thirdNum));
				$(".box2_table tr:nth-of-type(2) td:eq(1)").html(0);

				$(".box3_table tr:nth-of-type(1) td:eq(0)").html(Math.floor((a/10)*thirdNum));
				$(".box3_table tr:nth-of-type(1) td:eq(1)").html((a%10)*thirdNum);

				$(".qb1").addClass("showThis");
				arwClk(fstAnsCount, secAnsCount, thirdAnsCount, countb, countc);
				$(".submit").click(function(){
					submitFunction(secNum*thirdNum,Math.floor((a/10)*thirdNum)*10, a*thirdNum);
				});
				$(".practice").click(function(){
					boxNum = 0;
					templatecaller();
				});
			break;
			default:
				nav_button_controls();
		}
	}
	function arwClk(fstAnsCount, secAnsCount, thirdAnsCount, countb, countc){
		$(".arw1").click(function(){
			fstAnsCount+=1;
			$(".ab1").html(fstAnsCount);
			fstAnsCount==10?fstAnsCount=0:fstAnsCount;
		});
		$(".arw2").click(function(){
			countb+=1;
			$(".ab2").html(countb);
			countb==(secAnsCount+5)?countb=(secAnsCount-6):countb;
		});
		$(".arw3").click(function(){
			countc+=1;
			$(".ab3").html(countc);
			countc==(thirdAnsCount+5)?countc=(thirdAnsCount-6):countc;
		});
	}
	function submitFunction(cor1, cor2, cor3){
		if(boxNum == 0){
			if ($(".ab1").text()==cor1){
				$(".qb1, .arrow1").removeClass("showThis");
				$(".qb2, .bxt_2, .arrow2").addClass("showThis");
				$(".box2_table tr:nth-of-type(1) td").addClass("showThis");
				// play_correct_incorrect_sound(1);
				boxNum+=1;
					sound_player("s3_p2_1");
			}else{
				incorrect($(".ab1"));
			}
		}
		else if(boxNum == 1){
			if ($(".ab2").text()==cor2){
				$(".qb2, .arrow2").removeClass("showThis");
				$(".qb3, .bxt_3, .arrow3").addClass("showThis");
				$(".box2_table tr:nth-of-type(2) td").addClass("showThis brdbtm");
				boxNum+=1;
					sound_player("s3_p2_2");
			}else{
				incorrect($(".ab2"));
			}
		}
		else if(boxNum == 2){
			if ($(".ab3").text()==cor3){
				$(".qb3, .arrow3").removeClass("showThis");
				$(".box3_table tr:nth-of-type(1) td").addClass("showThis");
				$(".eqnNum span").css("background","transparent");
				$(".submit").css("display","none");
				$(".practice").show(0);
				navigationcontroller(true);
				boxNum+=1;
			}else{
				incorrect($(".ab3"));
			}
		}
}
	function incorrect(bxClass){
		bxClass.css({
			"background":"#f00",
			"border":"2px solid #990000"
		});
		// play_correct_incorrect_sound(0);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		boxNum = 0;
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

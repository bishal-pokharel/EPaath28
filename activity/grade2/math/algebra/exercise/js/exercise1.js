var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_0 = new buzz.sound((soundAsset + "ex.ogg"));

var content=[

	//ex1
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex2
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex3
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex4
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex5
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex6
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex7
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex8
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex9
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
	//ex10
	{
		contentblockadditionalclass: "grassbg",
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.excqn
		},{
			textclass:"submit",
			textdata:data.string.submit
		}],
		excblock:[{
			qntxt:[{
				qntxtblkclass:"fstblk",
				textclass:"qntxt qn1_blk",
				textdata:data.string.t_1
			},{
				qntxtblkclass:"lstblk",
				textclass:"qntxt qn2_blk",
				textdata:data.string.t_1
			}],
			imageblock:[{
				imagestoshow:[{
					imcblkcls:"imgcont plscnt",
					imgclass:"imgplus",
					imgsrc:imgpath+'plus01.png'
				},{
					imcblkcls:"imgcont eqcont",
					imgclass:"imgplus",
					imgsrc:imgpath+'equal01.png'
				}]
			}],
			inputbox:[{
				inputblkcls:"ipcont",
				inputclass:"input"
			}]
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
  var $refreshBtn= $("#activity-page-refresh-btn");
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;
	var current_sound = sound_0;
	/*for limiting the questions to 10*/
	var $total_page = 10;

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

		var qnArray = [];
		for(var i=6;i<=19;i++){
			qnArray.push(i);
		}
		qnArray.shufflearray();

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
	 	var parent = $(".optionsdiv");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}


	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		var ans;
	 	switch(countNext){
			case 0:
			sound_player(sound_0);
			var fstNum = Math.floor(Math.random()*(qnArray[0]-1)+0);
			$(".qn2_blk").html(qnArray[0]);
			$(".qn1_blk").html(fstNum);
			ans = qnArray[0] - fstNum;
			// console.log(fstNum+" +"+ans+"="+qnArray[0]);
			qnArray.splice(0,1);
			break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
	 		case 9:
				var fstNum = Math.floor(Math.random()*(qnArray[0]-1)+0);
				$(".qn2_blk").html(qnArray[0]);
				$(".qn1_blk").html(fstNum);
				ans = qnArray[0] - fstNum;
				// console.log(fstNum+" +"+ans+"="+qnArray[0]);
				qnArray.splice(0,1);

	 		break;
	 	}
		$(".submit").click(function(){
			if($(".input").val()== ans){
				play_correct_incorrect_sound(1);
				$(".input").removeClass("incorrectclass");
				$(".input").attr("disabled",'true').addClass("correctclass");
				!wrngClicked?testin.update(true):testin.update(false);
				$(".submit").css("pointer-events","none");
				$(".wrngopt").hide(0);
				$(".corctopt").show(0);
				$nextBtn.show(0);
			}else{
				play_correct_incorrect_sound(0);
				$(".input").addClass("incorrectclass");
				testin.update(false);
				$(".wrngopt").show(0);
				wrngClicked = true;
			}
		});


	 	/*======= SCOREBOARD SECTION ==============*/
	 }
	 function sound_player(sound_data){
		 current_sound.stop();
		 current_sound = sound_data;
		 current_sound.play();
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_diy',

		extratextblock : [{
			textdata : data.string.diytext,
			textclass : 'diy-text',
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [{
			textdata : data.string.p6text2,
			textclass : 'p6-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri",
					imgsrc : imgpath + "anushri.png",
				},{
					imgclass : "fitem fitem1",
					imgsrc : imgpath + "pineapple1.png",
				},{
					imgclass : "fitem fitem2",
					imgsrc : imgpath + "apple.png",
				},{
					imgclass : "fitem fitem3",
					imgsrc : imgpath + "banana1.png",
				},{
					imgclass : "fitem fitem4",
					imgsrc : imgpath + "orange1.png",
				},{
					imgclass : "fitem fitem5",
					imgsrc : imgpath + "cheese1.png",
				}
			],
		}],
		foodblock:[{
			containerclass: 'drop-container',
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [{
			textdata : data.string.p6text3,
			textclass : 'p6-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text4,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		foodblock:[{
			containerclass: 'drop-container',
		},{
			containerclass: 'drag-container',
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [
            {
                textdata : data.string.commonmsg,
                textclass : 'p6-text my_font_very_big sniglet',
                datahighlightflag : true,
                datahighlightcustomclass : 'ul',
            },
			{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text4a,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		equation:[{
			textdata1: data.string.t_3,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_15,
			btntext: data.string.check
		}],
		foodblock:[{
			containerclass: 'drop-container',
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [{
			textdata : data.string.p6text5,
			textclass : 'p6-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text6,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		foodblock:[{
			containerclass: 'drop-container',
		},{
			containerclass: 'drag-container',
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [
            {
                textdata : data.string.commonmsg,
                textclass : 'p6-text my_font_very_big sniglet',
                datahighlightflag : true,
                datahighlightcustomclass : 'ul',
            },{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text6a,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		equation:[{
			textdata1: data.string.t_6,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_12,
			btntext: data.string.check
		}],
		foodblock:[{
			containerclass: 'drop-container',
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [{
			textdata : data.string.p6text7,
			textclass : 'p6-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text8,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		foodblock:[{
			containerclass: 'drop-container',
		},{
			containerclass: 'drag-container',
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [
            {
                textdata : data.string.commonmsg,
                textclass : 'p6-text my_font_very_big sniglet',
                datahighlightflag : true,
                datahighlightcustomclass : 'ul',
            },{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text8a,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		equation:[{
			textdata1: data.string.t_5,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_8,
			btntext: data.string.check
		}],
		foodblock:[{
			containerclass: 'drop-container',
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [{
			textdata : data.string.p6text9,
			textclass : 'p6-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text10,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		foodblock:[{
			containerclass: 'drop-container',
		},{
			containerclass: 'drag-container',
		}]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [
            {
                textdata : data.string.commonmsg,
                textclass : 'p6-text my_font_very_big sniglet',
                datahighlightflag : true,
                datahighlightcustomclass : 'ul',
            },{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text10a,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		equation:[{
			textdata1: data.string.t_11,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_17,
			btntext: data.string.check
		}],
		foodblock:[{
			containerclass: 'drop-container',
		}]
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [{
			textdata : data.string.p6text11,
			textclass : 'p6-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text12,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		foodblock:[{
			containerclass: 'drop-container',
		},{
			containerclass: 'drag-container',
		}]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [
            {
                textdata : data.string.commonmsg,
                textclass : 'p6-text my_font_very_big sniglet',
                datahighlightflag : true,
                datahighlightcustomclass : 'ul',
            },{
			textdata : data.string.submit,
			textclass : 'submit-btn my_font_medium sniglet',
		}],
		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p6text12a,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "anushri-1",
					imgsrc : imgpath + "anushri1.png",
				},{
					imgclass : "corincor correct-icon",
					imgsrc : "images/correct.png",
				},{
					imgclass : "corincor incorrect-icon",
					imgsrc : "images/wrong.png",
				}
			],
		}],
		equation:[{
			textdata1: data.string.t_7,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_15,
			btntext: data.string.check
		}],
		foodblock:[{
			containerclass: 'drop-container',
		}]
	},
//	slide12
    {
        hasheaderblock : false,
        contentblocknocenteradjust : true,
        speechbox:[
            {
                speechbox: 'sp-12',
                textdata : data.string.p6text12b,
                textclass : '',
                imgsrc: 'images/textbox/white/tl-1.png',
                // audioicon: true,
            }],
        imageblock : [{
            imagestoshow : [
                {
                    imgclass : "backgroundImg",
                    imgsrc : imgpath + "bg04.png",
                }
            ],
        }],
    }
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	// var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;


	var item_count = 0;
	var drag_cont_available;
	var drop_cont_available;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	//to sort numbers
	function sortNumber(a,b) {
		return a - b;
	}
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_1", src: soundAsset+"s5_p2.ogg"},
			{id: "sound_2a", src: soundAsset+"s5_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s5_p3_2.ogg"},
			{id: "sound_3a", src: soundAsset+"s5_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s5_p4_2.ogg"},
			{id: "sound_4a", src: soundAsset+"s5_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s5_p5_2.ogg"},
			{id: "sound_5a", src: soundAsset+"s5_p6_1.ogg"},
			{id: "sound_5b", src: soundAsset+"s5_p6_2.ogg"},
			{id: "sound_6a", src: soundAsset+"s5_p7_1.ogg"},
			{id: "sound_6b", src: soundAsset+"s5_p7_2.ogg"},
			{id: "sound_7a", src: soundAsset+"s5_p8_1.ogg"},
			{id: "sound_7b", src: soundAsset+"s5_p8_2.ogg"},
			{id: "sound_8a", src: soundAsset+"s5_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s5_p9_2.ogg"},
			{id: "sound_9a", src: soundAsset+"s5_p10_1.ogg"},
			{id: "sound_9b", src: soundAsset+"s5_p10_2.ogg"},
			{id: "sound_10a", src: soundAsset+"s5_p11_1.ogg"},
			{id: "sound_10b", src: soundAsset+"s5_p11_2.ogg"},
			{id: "sound_11a", src: soundAsset+"s5_p12_1.ogg"},
			{id: "sound_11b", src: soundAsset+"s5_p12_2.ogg"},
			{id: "sound_12", src: soundAsset+"s5_p13.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		// countNext==0?play_diy_audio():"";

		switch (countNext) {
		case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
		case 1:
			sound_player("sound_"+countNext);
			item_count = 0;

			var drop_html_1 = '<div style="top:';
			var drop_html_2 = '%; left:';
			var drop_html_3 = '%;">';
			var drop_html_4 = '</div>';
			var drop_image =  '<img class="drop-fimg" src="'+imgpath + 'banana.png"' + '>';
			var drag_image =  '<img class="drag-fimg" src="'+imgpath + 'banana.png"' + '>';
			for(var j=0; j<6; j++){
				for(var i =0; i<4; i++){
					$('.drop-container').append(drop_html_1+j*16.667+ drop_html_2+ i*25+ drop_html_3 + drop_html_4);
				}
			}
			break;
		case 2:
		case 4:
		case 6:
		case 8:
		case 10:
			sound_nav("sound_"+(countNext)+("a"),"sound_"+(countNext)+("b"));
			var drop_image;
			var drag_image;
			var answer = 0;
			if(countNext==2){
				item_count = 3;
				answer = 15;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'banana.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'banana.png"' + '>';
			} else if(countNext==4){
				item_count = 6;
				answer = 12;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'apple01.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'apple01.png"' + '>';
			} else if(countNext==6){
				item_count = 5;
				answer = 8;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'pineapple.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'pineapple.png"' + '>';
			} else if(countNext==8){
				item_count = 11;
				answer = 17;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'orange.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'orange.png"' + '>';
			} else if(countNext==10){
				item_count = 7;
				answer = 15;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'cheese.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'cheese.png"' + '>';
			}
			drag_cont_available = [];
			drop_cont_available = [];
			var drop_html_1 = '<div style="top:';
			var drop_html_2 = '%; left:';
			var drop_html_3 = '%;">';
			var drop_html_4 = '</div>';
			for(var j=0; j<5; j++){
				for(var i =0; i<5; i++){
					$('.drag-container').append(drop_html_1+j*20+ drop_html_2+ i*20+ drop_html_3+ drag_image + drop_html_4);
				}
			}
			for(var k=0; k<$('.drag-container>div').length; k++){
				$('.drag-container>div').eq(k).addClass('dragitem-'+k);
			}
			for(var j=0; j<6; j++){
				for(var i =0; i<4; i++){
					$('.drop-container').append(drop_html_1+j*16.667+ drop_html_2+ i*25+ drop_html_3 + drop_html_4);
				}
			}
			for(var k=0; k<$('.drop-container>div').length; k++){
				$('.drop-container>div').eq(k).addClass('dropitem-'+k);
				if(k>item_count-1){
					drop_cont_available.push(k);
				} else{
					$('.drop-container>div').eq(k).addClass('fixedblock');
					$('.drop-container>div').eq(k).append(drop_image);
				}
			}
			$('.fimg').click(function(){
				var parent_num;
				if(!$(this).hasClass('selected-drag')){
					if(drop_cont_available.length>0){//check available space
						item_count++;
						parent_num = parseInt($(this).parent().attr('class').replace(/\D/g,''));
						$(this).addClass('selected-drag');
						$(this).detach().css({
							'top': '0%',
							'left': '0%',
						}).appendTo($('.drop-container>div').eq(drop_cont_available[0]));
						drop_cont_available.splice(0,1);
						drag_cont_available.push(parent_num);
						// console.log(drop_cont_available);
						// console.log(drag_cont_available);
					}
				} else{
					if(drag_cont_available.length>0){//check available space
						item_count--;
						parent_num = parseInt($(this).parent().attr('class').replace(/\D/g,''));
						$(this).removeClass('selected-drag');
						$(this).detach().css({
							'top': '',
							'left': '',
						}).appendTo($('.drag-container>div').eq(drag_cont_available[0]));
						drag_cont_available.splice(0,1);
						drop_cont_available.push(parent_num);
						// console.log(drop_cont_available);
						// console.log(drag_cont_available);
					}
				}
				drop_cont_available.sort(sortNumber);
				drag_cont_available.sort(sortNumber);
			});
			$('.submit-btn').click(function(){
				bt_click(answer);
			});
			break;
		case 3:
		case 5:
		case 7:
		case 9:
		case 11:
			sound_nav("sound_"+(countNext)+("a"),"sound_"+(countNext)+("b"));
			var drop_image;
			var drag_image;
			var answer = 0;
			if(countNext==3){
				item_count = 15;
				answer = 12;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'banana.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'banana.png"' + '>';
			} else if(countNext==5){
				item_count = 12;
				answer = 6;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'apple01.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'apple01.png"' + '>';
			} else if(countNext==7){
				item_count = 8;
				answer = 3;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'pineapple.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'pineapple.png"' + '>';
			} else if(countNext==9){
				item_count = 17;
				answer = 6;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'orange.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'orange.png"' + '>';
			} else if(countNext==11){
				item_count = 15;
				answer = 8;
				drop_image =  '<img class="fimg selected-drag" src="'+imgpath + 'cheese.png"' + '>';
				drag_image =  '<img class="fimg" src="'+imgpath + 'cheese.png"' + '>';
			}
			drag_cont_available = [];
			drop_cont_available = [];
			var drop_html_1 = '<div style="top:';
			var drop_html_2 = '%; left:';
			var drop_html_3 = '%;">';
			var drop_html_4 = '</div>';
			for(var j=0; j<6; j++){
				for(var i =0; i<4; i++){
					$('.drop-container').append(drop_html_1+j*16.667+ drop_html_2+ i*25+ drop_html_3 + drop_html_4);
				}
			}
			for(var k=0; k<item_count; k++){
				$('.drop-container>div').eq(k).append(drop_image);
			}
			$('.submit-btn').click(function(){
				bt_click2(answer);
			});
			break;
			case 12:
			sound_player("sound_"+countNext);
			break;

		default:
			$prevBtn.show(0);
			nav_button_controls(100);
			break;
		}
	}

	function bt_click(num){
		if(item_count==num){
			createjs.Sound.stop();
			play_correct_incorrect_sound(1);
			$('.fimg').css('pointer-events', 'none');
			$('.incorrect-icon').hide(0);
			$('.correct-icon').show(0);
			$('.submit-btn').css({
				'pointer-events': 'none',
				'background-color':'#4CAF50'
				});
			if( countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				nav_button_controls(0);
			}
		} else{
			createjs.Sound.stop();
			play_correct_incorrect_sound(0);
			$('.submit-btn').css({
				'background-color':'#D32F2F'
			});
			$('.incorrect-icon').show(0);
		}
	}
	function bt_click2(num){
		if($('.eqt-2').val()==num){
			createjs.Sound.stop();
			play_correct_incorrect_sound(1);
			$('.incorrect-icon').hide(0);
			$('.correct-icon').show(0);
			$('.submit-btn').css({
				'pointer-events': 'none',
				'background-color':'#4CAF50'
			});
			if( countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				nav_button_controls(0);
			}
		} else{
			createjs.Sound.stop();
			play_correct_incorrect_sound(0);
			$('.submit-btn').css({
				'background-color':'#D32F2F'
			});
			$('.incorrect-icon').show(0);
		}
	}
	function dropper(dropped, droppedOn){
		if(!droppedOn.has('img').length){
			dropped.draggable('option', 'revert', false);
			dropped.draggable('option', 'revert', true);
			if(dropped.parent().parent().hasClass('drag-container')){
				item_count++;
			}
			// dropped.draggable('disable');
			dropped.detach().css({
				'top': '0%',
				'left': '11%'
			}).appendTo(droppedOn);
		}
	}
	function dragger(dropped, droppedOn){
		if(!droppedOn.has('img').length){
			dropped.draggable('option', 'revert', false);
			dropped.draggable('option', 'revert', true);
			if(dropped.parent().parent().hasClass('drop-container')){
				item_count--;
			}
			// dropped.draggable('disable');
			dropped.detach().css({
				'top': '0%',
				'left': '11%'
			}).appendTo(droppedOn);
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_nav(sound_id, sound_id1){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id1);
		current_sound.play();
	});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	// templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound((soundAsset + "s4_p1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "s4_p2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "s4_p3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "s4_p4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "s4_p5_1.ogg"));
var sound_6 = new buzz.sound((soundAsset + "s4_p5_2.ogg"));
var sound_7 = new buzz.sound((soundAsset + "s4_p5_3.ogg"));
var sound_8 = new buzz.sound((soundAsset + "s4_p5_4.ogg"));
var sound_9 = new buzz.sound((soundAsset + "s4_p5_5.ogg"));
var sound_10 = new buzz.sound((soundAsset + "s4_p5_6.ogg"));
var sound_11 = new buzz.sound((soundAsset + "s4_p5_7.ogg"));
var sound_12 = new buzz.sound((soundAsset + "s4_p6.ogg"));

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-4',



		extratextblock: [{
			textclass: 'added-text-0',
			textdata: data.string.p5text0
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rumi",
					imgsrc : imgpath + "rumi.png",
				},
				{
					imgclass : "suraj",
					imgsrc : imgpath + "suraj.png",
				},
			],
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-4',

		speechbox:[
		{
			speechbox: 'sp-2',
			textdata : data.string.p5text1,
			textclass : '',
			imgsrc: imgpath + "textbox03.png",
			// audioicon: true,
		}],
		extratextblock: [{
			textclass: 'suraj-text sniglet my_font_ultra_big its_hidden',
			textdata: data.string.t_11
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rumi",
					imgsrc : imgpath + "rumi.png",
				},
				{
					imgclass : "suraj",
					imgsrc : imgpath + "suraj.png",
				},
				{
					imgclass : "rack-new rack-new-1",
					imgsrc : imgpath + "rack2.png",
				},
				{
					imgclass : "rack-new rack-new-2",
					imgsrc : imgpath + "rack2.png",
				}
			],
		}],
		blocklist:[{
			containerclass: 'ball-container container-1',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			}]
		},{
			containerclass: 'ball-container container-2',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			}]
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-4',

		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : data.string.p5text2,
			textclass : '',
			imgsrc: imgpath + "textbox04.png",
			// audioicon: true,
		}],
		extratextblock: [{
			textclass: 'suraj-text sniglet my_font_ultra_big',
			textdata: data.string.t_11
		},
		{
			textclass: 'rumi-text sniglet my_font_ultra_big its_hidden',
			textdata: data.string.t_4
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rumi",
					imgsrc : imgpath + "rumi.png",
				},
				{
					imgclass : "suraj",
					imgsrc : imgpath + "suraj.png",
				},
				{
					imgclass : "rack-new rack-new-1",
					imgsrc : imgpath + "rack2.png",
				},
				{
					imgclass : "rack-new rack-new-2",
					imgsrc : imgpath + "rack2.png",
				}
			],
		}],
		blocklist:[{
			containerclass: 'ball-container container-1',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			}]
		},{
			containerclass: 'ball-container container-2',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			}]
		}]
	},


	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-4',

		speechbox:[
		{
			speechbox: 'sp-3',
			textdata : data.string.p5text3,
			textclass : '',
			imgsrc: imgpath + "textbox04.png",
			// audioicon: true,
		}],
		extratextblock: [{
			textclass: 'suraj-text sniglet my_font_ultra_big',
			textdata: data.string.t_11
		},
		{
			textclass: 'rumi-text sniglet my_font_ultra_big',
			textdata: data.string.t_4
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rumi",
					imgsrc : imgpath + "rumi.png",
				},
				{
					imgclass : "suraj",
					imgsrc : imgpath + "suraj.png",
				},
				{
					imgclass : "rack-new rack-new-1",
					imgsrc : imgpath + "rack2.png",
				},
				{
					imgclass : "rack-new rack-new-2",
					imgsrc : imgpath + "rack2.png",
				}
			],
		}],
		blocklist:[{
			containerclass: 'ball-container container-1',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			},{
				blockclass: '',
			}]
		},{
			containerclass: 'ball-container container-2',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			}]
		}],

		equationblock:[{
			equationblockclass: '',
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textdata3: data.string.t_11,
		}],
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-4',

		speechbox:[
		{
			speechbox: 'sp-1',
			textdata : "",
			textclass : '',
			imgsrc: imgpath + "textbox04.png",
			// audioicon: true,
		}],
		extratextblock: [{
			textclass: 'suraj-text sniglet my_font_ultra_big',
			textdata: data.string.t_11
		},
		{
			textclass: 'rumi-text sniglet my_font_ultra_big',
			textdata: data.string.t_4
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rumi",
					imgsrc : imgpath + "rumi.png",
				},
				{
					imgclass : "suraj",
					imgsrc : imgpath + "suraj.png",
				},
				{
					imgclass : "rack-new rack-new-1",
					imgsrc : imgpath + "rack2.png",
				},
				{
					imgclass : "rack-new rack-new-2",
					imgsrc : imgpath + "rack2.png",
				}
			],
		}],
		blocklist:[{
			containerclass: 'ball-container container-1',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-1',
				imgclass: 'block-ball its_hidden',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-3',
				imgclass: 'block-ball its_hidden',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-5',
				imgclass: 'block-ball its_hidden',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-7',
				imgclass: 'block-ball its_hidden',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-2',
				imgclass: 'block-ball its_hidden',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-4',
				imgclass: 'block-ball its_hidden',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-6',
				imgclass: 'block-ball its_hidden',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			}]
		},{
			containerclass: 'ball-container container-2',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			}]
		}],

		equationblock:[{
			equationblockclass: '',
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textdata3: data.string.t_11,
		}],
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-4',

		speechbox:[
		{
			speechbox: 'sp-3',
			textdata : data.string.p5text6,
			textclass : '',
			imgsrc: imgpath + "textbox04.png",
			// audioicon: true,
		}],
		extratextblock: [{
			textclass: 'suraj-text sniglet my_font_ultra_big',
			textdata: data.string.t_11
		},
		{
			textclass: 'rumi-text sniglet my_font_ultra_big',
			textdata: data.string.t_11
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rumi",
					imgsrc : imgpath + "rumi.png",
				},
				{
					imgclass : "suraj",
					imgsrc : imgpath + "suraj.png",
				},
				{
					imgclass : "rack-new rack-new-1",
					imgsrc : imgpath + "rack2.png",
				},
				{
					imgclass : "rack-new rack-new-2",
					imgsrc : imgpath + "rack2.png",
				}
			],
		}],
		blocklist:[{
			containerclass: 'ball-container container-1',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-1 new_book_space',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-2 new_book_space',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-3 new_book_space',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-4 new_book_space',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-5 new_book_space',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-6 new_book_space',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: 'b-7 new_book_space',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			}]
		},{
			containerclass: 'ball-container container-2',
			block:[{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
				imgclass: 'block-ball',
				imgsrc: imgpath + "ball.png",
			},{
				blockclass: '',
			}]
		}],

		equationblock:[{
			equationblockclass: '',
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_last',

		extratextblock: [{
			textclass: 'text-3  last-text sniglet',
			textdata: data.string.t_4
		},
		{
			textclass: 'text-5 last-text sniglet',
			textdata: data.string.t_7
		},
		{
			textclass: 'text-8 last-text sniglet',
			textdata: data.string.t_11
		},{
			textclass: 'what-text sniglet',
			textdata: data.string.t_0
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-2",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-2",
					imgsrc : imgpath + "equal01.png",
				},
				{
					imgclass : "hoop",
					imgsrc : imgpath + "basketballhoop.png",
				},
				{
					imgclass : "hoop bball-net",
					imgsrc : imgpath + "basketballnet.png",
				}
			],
		}],

		imagetextblock : [{
			imagediv: 'bball nb-1 nb1-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_1
		},{
			imagediv: 'bball nb-2 nb2-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_2
		},{
			imagediv: 'bball nb-3  nb3-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_3
		},{
			imagediv: 'bball nb-4 nb4-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_4
		},{
			imagediv: 'bball nb-5 nb5-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_5
		},{
			imagediv: 'bball nb-6 nb6-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_6
		},{
			imagediv: 'bball nb-7 nb7-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_7
		},{
			imagediv: 'bball nb-8 nb8-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_8
		},{
			imagediv: 'bball nb-9 nb9-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_9
		},{
			imagediv: 'bball nb-10 nb10-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_10
		},{
			imagediv: 'bball nb-11 nb11-anim',
			imgclass : "",
			imgsrc : imgpath + "ball.png",
			textclass : "its_hidden",
			textdata: data.string.t_11
		}]
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	var timeouts = [];
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				sound_nav(sound_1);
				break;
			case 1:
				current_sound.stop();
				current_sound = sound_2;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					$('.suraj-text').show(0);
					nav_button_controls(1200);
				});
				break;
			case 2:
				current_sound.stop();
				current_sound = sound_3;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					$('.rumi-text').show(0);
					nav_button_controls(1200);
				});
				break;
			case 3:
				sound_nav(sound_4);
				break;
			case 4:
				var bttext = [data.string.t_5, data.string.t_6, data.string.t_7, data.string.t_8, data.string.t_9, data.string.t_10, data.string.t_11];
				var toptext = [data.string.p4text4, data.string.p4text5, data.string.p4text6, data.string.p4text7, data.string.p4text8, data.string.p5text4, data.string.p5text5];
				var soundtext = [sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11];
				function create_books(index){
					timeouts.push(setTimeout(function(){
						$('.rumi-text').html(bttext[index-1]);
						$('.sp-1>p').html(toptext[index-1]);
						sound_player(soundtext[index-1]);
						$('.b-'+index).addClass('new_book_space');
						$('.b-'+index+'>img').show(0);
						if(index==7){
							nav_button_controls(1200);
						}
					}, index*1000));
				}
				for(var i=1; i<8; i++){
					create_books(i);
				}
				break;
			case 5:
				current_sound.stop();
				current_sound = sound_12;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					$('.change-eq').fadeOut(1000, function(){
						$('.change-eq').html(data.string.t_7);
						$('.change-eq').fadeIn(1000);
						nav_button_controls(0);
					});
				});
				break;
			case 6:
				$('.nb-1>img').addClass('ball_rot');
				var ctext = [data.string.t_1, data.string.t_2, data.string.t_3, data.string.t_4, data.string.t_5, data.string.t_6, data.string.t_7, data.string.t_8, data.string.t_9, data.string.t_10, data.string.t_11];
				function increment_count(index){
					timeouts.push(setTimeout(function(){
						$('.nb-'+index+'>p').show(0);
						$('.nb-'+parseInt(index)+'>img').removeClass('ball_rot');
						$('.nb-'+parseInt(index+1)+'>img').addClass('ball_rot');
						$('.what-text').html(ctext[index-1]);
					}, index*2500));
				}
				for(var i=1; i<12; i++){
					increment_count(i);
				}
				nav_button_controls(27000);
				break;
			default:
				if(countNext>0){
					$prevBtn.show(0);
				}
				$nextBtn.show(0);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		for (var i=0; i<timeouts.length; i++) {
			clearTimeout(timeouts[i]);
		}
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		for (var i=0; i<timeouts.length; i++) {
			clearTimeout(timeouts[i]);
		}
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

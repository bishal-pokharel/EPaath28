var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "beep.ogg"));

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-apple',

		extratextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title'
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		extratextblock : [{
			textdata : data.string.p1text1,
			textclass : 'p1-text my_font_medium pangolin',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textclass: 'encircle-apple',
			textdata: ''
		},{
			textclass: 'app1-text my_font_big sniglet',
			textdata: data.string.t_1,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sundar",
					imgsrc : imgpath + "sunder.png",
				},
				{
					imgclass : "apple",
					imgsrc : imgpath + "apple.png",
				}
			],
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		extratextblock : [{
			textdata : data.string.p1text2,
			textclass : 'p1-text my_font_medium pangolin',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textclass: 'encircle-apple2',
			textdata: ''
		},{
			textclass: 'app3-text my_font_big sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sundar",
					imgsrc : imgpath + "sunder.png",
				},
				{
					imgclass : "apple",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "sundari",
					imgsrc : imgpath + "sundari-3.png",
				},
				{
					imgclass : "apple1",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple2",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple3 ",
					imgsrc : imgpath + "apple.png",
				}
			],
		}]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'p1-text my_font_medium pangolin',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sundar",
					imgsrc : imgpath + "sunder.png",
				},
				{
					imgclass : "apple apple-anim",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple1",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple2",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple3 ",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "sundari",
					imgsrc : imgpath + "sundari-3.png",
				}
			],
		}]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		extratextblock : [{
			textdata : data.string.p1text4,
			textclass : 'p1-text my_font_medium pangolin',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textdata : data.string.t_1,
			textclass : 'apple1-text my_font_big sniglet fade-in-1',
		},{
			textdata : data.string.t_2,
			textclass : 'apple2-text my_font_big sniglet fade-in-3',
		},{
			textdata : data.string.t_3,
			textclass : 'apple3-text my_font_big sniglet fade-in-5',
		},{
			textdata : data.string.t_4,
			textclass : 'apple4-text my_font_big sniglet fade-in-7',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "apple1a apple1-anim",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple2a apple2-anim",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple3a apple3-anim",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple4a apple4-anim",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "sundari-2",
					imgsrc : imgpath + "sundari-3.png",
				}
			],
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		extratextblock : [{
			textdata : data.string.p1text5,
			textclass : 'p1-sum sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "apple1a",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple2a",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple3a",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "apple4a",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "app1 fade-in-2",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "app2a fade-in-3",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "app2b fade-in-3",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "app2c fade-in-3",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "app3a fade-in-4",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "app3b fade-in-4",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "app3c fade-in-4",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "app3d fade-in-4",
					imgsrc : imgpath + "apple.png",
				},
				{
					imgclass : "sundari-2",
					imgsrc : imgpath + "sundari-3.png",
				}
			],
		}]
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [{
			textdata : data.string.p1text6,
			textclass : 'review-title pangolin'
		}],
	},

	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p1 my_font_very_big sniglet',
			textdata: data.string.p1text7,
		},{
			textclass: 'block-1a-text my_font_very_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_very_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'what-text what-1 sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},


	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'what-text what-1 sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
			textclass: 'fade-in-1'
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_2,
			textclass: 'fade-in-2'
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_3,
			textclass: 'fade-in-3'
		}],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p1 my_font_very_big sniglet',
			textdata: data.string.p1text8,
		},{
			textclass: 'block-1a-text my_font_very_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_very_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'block-3a-text my_font_very_big sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			nav_button_controls(100);
			break;
		case 3:
			$prevBtn.show(0);
			nav_button_controls(2000);
			break;
		case 4:
			$prevBtn.show(0);
			nav_button_controls(8000);
			break;
		case 8:
			$prevBtn.show(0);
			nav_button_controls(3000);
			break;
		default:
			$prevBtn.show(0);
			nav_button_controls(100);
			break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

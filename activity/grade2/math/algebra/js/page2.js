var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-apple',

		extratextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title pangolin'
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2add1,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'what-text what-2-add sniglet',
			textdata: data.string.t_q,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_2,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2add2,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'what-text what-2-add sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_2,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2add3,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_2,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-3a added-last',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b added-last',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c added-last',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2add4,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2a fade-out-add',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_2,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2b fade-out-add',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-3a added-last',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b added-last',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c added-last',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2text1,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'what-text what-1 sniglet',
			textdata: data.string.t_q,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2text1,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'what-text what-1 sniglet',
			textdata: data.string.t_q,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2add5,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-2a fade-in-1a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_2,
		},{
			blockclass: 'block-2b fade-in-2a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_3,
		}],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2text2,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'encircle encircle-1',
			textdata: '',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2text3,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'encircle encircle-2',
			textdata: '',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2add6,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_big sniglet',
			textdata: data.string.t_2,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_super_big sniglet',
			textdata: data.string.p2text4,
		},{
			textclass: 'block-1a-text my_font_big sniglet',
			textdata: data.string.t_1,
		},{
			textclass: 'block-2a-text my_font_big sniglet',
			textdata: data.string.t_2,
		},{
			textclass: 'what-text what-2 sniglet',
			textdata: data.string.t_3,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_2,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_ultra_big sniglet',
			textdata: data.string.p2add7,
		},{
			textclass: 'block-3a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'what-text what-1 sniglet',
			textdata: data.string.t_q,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-1a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_1,
		},{
			blockclass: 'block-3a',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-3b',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-3c',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		}],
	},
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock : [{
			textdata : data.string.p2text5,
			textclass : 'center-text-1 my_font_ultra_big sniglet'
		}],
	},
	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2text6,
		},{
			textclass: 'block-4a-text my_font_big sniglet',
			textdata: data.string.t_4,
		},{
			textclass: 'block-7a-text my_font_big sniglet',
			textdata: data.string.t_7,
		},{
			textclass: 'what-text what-1 sniglet',
			textdata: data.string.t_q,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-4 block-4a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-4 block-4b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-4 block-4c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-4 block-4d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7e',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7f',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7g',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		}],
	},
	//slide15
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2text6,
		},{
			textclass: 'block-4a-text my_font_big sniglet',
			textdata: data.string.t_4,
		},{
			textclass: 'block-7a-text my_font_big sniglet',
			textdata: data.string.t_7,
		},{
			textclass: 'what-text what-1 sniglet fade-out-6',
			textdata: data.string.t_q,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-4 block-4a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textclass: 'fade-in-1',
			textdata: data.string.t_1,
		},{
			blockclass: 'block-4 block-4b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textclass: 'fade-in-2',
			textdata: data.string.t_2,
		},{
			blockclass: 'block-4 block-4c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textclass: 'fade-in-3',
			textdata: data.string.t_3,
		},{
			blockclass: 'block-4 block-4d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textclass: 'fade-in-4',
			textdata: data.string.t_4,
		},{
			blockclass: 'block-5 block-5a fade-in-7',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textclass: '',
			textdata: data.string.t_5,
		},{
			blockclass: 'block-5 block-5b fade-in-8',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textclass: '',
			textdata: data.string.t_6,
		},{
			blockclass: 'block-5 block-5c fade-in-9',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textclass: '',
			textdata: data.string.t_7,
		},{
			blockclass: 'block-7 block-7a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7e',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7f',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7g',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		}],
	},
	//slide16
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2text7,
		},{
			textclass: 'block-4a-text my_font_big sniglet',
			textdata: data.string.t_4,
		},{
			textclass: 'block-5a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'block-7a-text my_font_big sniglet',
			textdata: data.string.t_7,
		},{
			textclass: 'encircle encircle-3',
			textdata: '',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-4 block-4a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-4 block-4b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-4 block-4c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-4 block-4d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-5 block-5a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-5 block-5b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-5 block-5c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7e',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7f',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7g',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		}],
	},
	//slide17
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2text8,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textclass: 'block-4a-text my_font_big sniglet',
			textdata: data.string.t_4,
		},{
			textclass: 'block-5a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'block-7a-text my_font_big sniglet',
			textdata: data.string.t_7,
		},{
			textclass: 'encircle encircle-4',
			textdata: '',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-4 block-4a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-4 block-4b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-4 block-4c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-4 block-4d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-5 block-5a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-5 block-5b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-5 block-5c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7e',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7f',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7g',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		}],
	},
	//slide18
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_very_big sniglet',
			textdata: data.string.p2add6,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textclass: 'block-4a-text my_font_big sniglet',
			textdata: data.string.t_4,
		},{
			textclass: 'block-5a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'block-7a-text my_font_big sniglet',
			textdata: data.string.t_7,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-4 block-4a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-4 block-4b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-4 block-4c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-4 block-4d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-5 block-5a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-5 block-5b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-5 block-5c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7e',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7f',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7g',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		}],
	},
	//slide19
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'top-text-p2 my_font_ultra_big sniglet',
			textdata: data.string.p2text9,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		},{
			textclass: 'block-4a-text my_font_big sniglet',
			textdata: data.string.t_4,
		},{
			textclass: 'block-5a-text my_font_big sniglet',
			textdata: data.string.t_3,
		},{
			textclass: 'what-text what-2 sniglet',
			textdata: data.string.t_7,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus01.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal01.png",
				}
			],
		}],
		block:[{
			blockclass: 'block-4 block-4a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_1,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-4 block-4b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_2,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-4 block-4c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-4 block-4d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_4,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-5 block-5a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_5,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-5 block-5b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
			textdata: data.string.t_6,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-5 block-5c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
			textdata: data.string.t_7,
			textclass: 'its_hidden'
		},{
			blockclass: 'block-7 block-7a',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7b',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7c',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7d',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7e',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		},{
			blockclass: 'block-7 block-7f',
			imgclass: '',
			imgsrc: imgpath + "block01.png",
		},{
			blockclass: 'block-7 block-7g',
			imgclass: '',
			imgsrc: imgpath + "block02.png",
		}],
	}
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound;
	var myTimeout =  null;
	var myTimeout2 =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2a", src: soundAsset+"s1_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s1_p3_2.ogg"},
			{id: "sound_2c", src: soundAsset+"s1_p3_3.ogg"},
			{id: "sound_2d", src: soundAsset+"s1_p3_4.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_7a", src: soundAsset+"s1_p8_1.ogg"},
			{id: "sound_7b", src: soundAsset+"s1_p8_2.ogg"},
			{id: "sound_7c", src: soundAsset+"s1_p8_3.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p14.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p15.ogg"},
			{id: "sound_15", src: soundAsset+"s1_p16.ogg"},
			{id: "sound_16", src: soundAsset+"s1_p17.ogg"},
			{id: "sound_17", src: soundAsset+"s1_p18.ogg"},
			{id: "sound_18", src: soundAsset+"s1_p19.ogg"},
			{id: "sound_19a", src: soundAsset+"s1_p20_1.ogg"},
			{id: "sound_19b", src: soundAsset+"s1_p20_2.ogg"},
			{id: "sound_19c", src: soundAsset+"s1_p20_3.ogg"},
			{id: "sound_19d", src: soundAsset+"s1_p20_4.ogg"},
			{id: "sound_19e", src: soundAsset+"s1_p20_5.ogg"},
			{id: "sound_19f", src: soundAsset+"s1_p20_6.ogg"},
			{id: "sound_19g", src: soundAsset+"s1_p20_7.ogg"},
			{id: "sound_19h", src: soundAsset+"s1_p20_8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 2:
				$('.what-2-add').html(data.string.t_q);
				$('.block-1a').clone().appendTo('.board').animate({
					'left': '85%',
					'bottom': '56%',
					'z-index': '6',
				}, 4000, function(){
					$('.what-2-add').html(data.string.t_1);
					$('.block-1a').eq(1).children('p').fadeIn(1000, function(){	});
					$('.block-2a').clone().appendTo('.board').animate({
						'left': '85%',
						'bottom': '42%',
						'z-index': '4'
					}, 1000, function(){
						$('.what-2-add').html(data.string.t_2);
						$('.block-2a').eq(1).children('p').fadeIn(1000, function(){});
						$('.block-2b').clone().appendTo('.board').animate({
								'left': '85%',
								'bottom': '28%',
								'z-index': '3'
							}, 1000, function(){
								$('.what-2-add').html(data.string.t_3);
								$('.block-2b').eq(1).children('p').fadeIn(1000, function(){
								});
						});
					});
				});
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_2a");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_2b");
					current_sound.play();
					current_sound.on('complete', function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_2c");
						current_sound.play();
						current_sound.on('complete', function(){
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("sound_2d");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(200);
							});
						});
					});
				});
				break;
			case 6:
			nav_button_controls(1000);
			break;
			case 7:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_7a");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_7b");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_7c");
			current_sound.play();
			current_sound.on('complete', function(){
							nav_button_controls(200);
						});
					});
				});
				break;
			case 11:
				$('.block-1a').clone().appendTo('.board').animate({
					'left': '70%',
					'bottom': '56%',
					'z-index': '6'
				}, 1000, function(){
					$('.block-1a').eq(1).children('p').fadeIn(1000, function(){	});
					$('.block-2a').clone().appendTo('.board').animate({
						'left': '70%',
						'bottom': '42%',
						'z-index': '4'
					}, 1000, function(){
						$('.block-2a').eq(1).children('p').fadeIn(1000, function(){});
						$('.block-2b').clone().appendTo('.board').animate({
								'left': '70%',
								'bottom': '28%',
								'z-index': '3'
							}, 1000, function(){
							$('.block-2b').eq(1).children('p').fadeIn(1000, function(){

							});
						});
					});
				});
					sound_player("sound_"+countNext);
				break;
			case 15:
				nav_button_controls(11000);
					sound_player("sound_"+countNext);
				break;
			case 19:
				myTimeout =  setTimeout(function(){
					$('.block-4c').clone().appendTo('.board').animate({
						'left': '70%',
						'bottom': '55%',
						'z-index': '4'
					}, 500, function(){
						$('.block-4c').eq(1).children('p').fadeIn(1000, function(){});
						$('.block-4d').clone().appendTo('.board').animate({
							'left': '70%',
							'bottom': '45%',
							'z-index': '3'
						}, 500, function(){
							$('.block-4d').eq(1).children('p').fadeIn(1000, function(){});
						});
					});
				}, 6000);
				myTimeout2 =  setTimeout(function(){
					$('.block-5a').clone().appendTo('.board').animate({
						'left': '70%',
						'bottom': '35%',
						'z-index': '2'
					}, 500, function(){
						$('.block-5a').eq(1).children('p').fadeIn(1000, function(){});
						$('.block-5b').clone().appendTo('.board').animate({
								'left': '70%',
								'bottom': '25%',
								'z-index': '1'
						}, 500, function(){
							$('.block-5b').eq(1).children('p').fadeIn(1000, function(){});
							$('.block-5c').clone().appendTo('.board').animate({
								'left': '70%',
								'bottom': '15%',
								'z-index': '0'
							}, 500, function(){
								$('.block-5c').eq(1).children('p').fadeIn(1000, function(){
								});
							});
						});
					});
				}, 8000);
				$('.block-4a').clone().appendTo('.board').animate({
					'left': '70%',
					'bottom': '75%',
					'z-index': '6'
				}, 5000, function(){
					$('.block-4a').eq(1).children('p').fadeIn(1000, function(){});
					$('.block-4b').clone().appendTo('.board').animate({
						'left': '70%',
						'bottom': '65%',
						'z-index': '5'
					}, 500, function(){
						$('.block-4b').eq(1).children('p').fadeIn(1000, function(){
						});
					});
				});
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_19a");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_19b");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_19c");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_19d");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_19e");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_19f");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_19g");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_19h");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(200);
				});});
			});
		});});
	});
});
});
				break;
			default:
			sound_player("sound_"+countNext);
			break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_nav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		clearTimeout(myTimeout2);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		clearTimeout(myTimeout2);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

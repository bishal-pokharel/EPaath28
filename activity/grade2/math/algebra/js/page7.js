var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "beep.ogg"));

var ans_arr = [
				[3,9,12],
				[4,11,15],
				[7,6,13],
				[8,6,14],
				[5,2,7],
				[6,3,9],
				[4,6,10],
				[1,15,16],
				[2,9,11],
				[5,5,10],
			];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',
		
		extratextblock:[{
			textclass: 'top-title my_font_big sniglet',
			textdata: data.string.p7text1,
		},{
			textclass: 'what-title sniglet',
			textdata: data.string.t_q,
		},{
			textclass: 'sum-text sniglet my_font_big',
			textdata: data.string.t_5,
		},{
			textclass: 'initial-text sniglet my_font_big',
			textdata: data.string.t_1,
		}],
		
		uppertextblockadditionalclass: 'final-choice-text my_font_big sniglet',
		uppertextblock:[{
			textclass: 'top-title my_font_big sniglet',
			textdata: data.string.t_3,
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal.png",
				}
			],
		}],
		blockcontainer:[{
			containerclass: 'block-container-1',
			block:[{
				blockclass: 'block-1',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-2',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-3',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-4',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-5',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-6',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-7',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-8',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			}],
		},{
			containerclass: 'block-container-2',
			block:[{
				blockclass: 'block-1',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-2',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-3',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-4',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-5',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-6',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-7',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-8',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			}],
		},{
			containerclass: 'block-container-3',
			block:[{
				blockclass: 'block-1',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-2',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-3',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-4',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-5',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-6',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-7',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-8',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			}],
		},{
			containerclass: 'block-container-4',
			block:[{
				blockclass: 'block-1',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-2',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-3',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-4',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-5',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-6',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-7',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-8',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			}],
		},{
			containerclass: 'block-container-5',
			block:[{
				blockclass: 'block-1',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-2',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-3',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-4',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-5',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-6',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			},{
				blockclass: 'block-7',
				imgclass: '',
				imgsrc: imgpath + "block01.png",
			},{
				blockclass: 'block-8',
				imgclass: '',
				imgsrc: imgpath + "block02.png",
			}],
		}],
		option:[{
			optionclass: 'option-1',
			textdata: data.string.t_5
		},{
			optionclass: 'option-2',
			textdata: data.string.t_5
		},{
			optionclass: 'option-3',
			textdata: data.string.t_5
		},{
			optionclass: 'option-4',
			textdata: data.string.t_5
		}]
	},
	
];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length+9;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	
	var ans_index = [0,1,2,3,4,5,6,7,8,9];
	ans_index.shufflearray();
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = null;
		if(countNext<11){
			html = template(content[0]);
		} else{
			html = template(content[countNext]);
		}
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			default:
				var var_1 = ans_arr[ans_index[countNext]][0];
				var var_3 = ans_arr[ans_index[countNext]][2];
				var var_2 = var_3 - var_1;
				
				$('.initial-text').html(var_1);
				$('.sum-text').html(var_3);
				
				var new_item_a = 0;
				var new_item_b = 0;
				var new_item_a2 = 0;
				var new_item_b2 = 0;
				
				
				//set options
				var opt_2 = var_2 + 1 + Math.floor(Math.random()*2);
				var opt_3 = var_2 - 1 - Math.floor(Math.random()*4);
				if(opt_3<0){
					opt_3 = 1;
				}
				var opt_4 = var_2 + 1 + Math.floor(Math.random()*4);
				while(opt_4 == opt_2){
					opt_4 = var_2 + 1 + Math.floor(Math.random()*4);
				}
				var option_vals = [var_2, opt_2, opt_3, opt_4];
				option_vals.shufflearray();
				for(var i=1; i<=4; i++){
					$('.option-'+i+'>p').html(option_vals[i-1]);
				}
				
				//set block
				for(var i=1; i<=var_1; i++){
					$('.block-container-1>.block-div').eq(i).show(0);
				}
				for(var i=0; i<var_3; i++){
					if(var_3>5){
						if(i%2==0){
							$('.block-container-4>.block-div').eq(new_item_a).show(0);
							new_item_a++;
						} else{
							$('.block-container-5>.block-div').eq(new_item_b).show(0);
							new_item_b++;
						}
					} else{
						$('.block-container-4>.block-div').eq(i).show(0);
					}
				}
				
				$('.option-div').click(function(){
					if(parseInt($(this).children('p').html()) == parseInt(var_2)){
						play_correct_incorrect_sound(1);
						$('.final-choice-text>p').html(var_2);
						$('.option-div').css('pointer-events', 'none');
						$('.option-container').fadeOut(500);
						$('.what-title').fadeOut(500, function(){
							for(var i=0; i<var_2; i++){
								if(var_2>5){
									if(i%2==0){
										$('.block-container-2>.block-div').eq(new_item_a2).fadeIn(1000);
										new_item_a2++;
									} else{
										$('.block-container-3>.block-div').eq(new_item_b2).fadeIn(1000);
										new_item_b2++;
									}
								} else{
									$('.block-container-2>.block-div').eq(i).fadeIn(1000);
								}
							}
							$('.final-choice-text').fadeIn(1000, function(){
								nav_button_controls(100);
							});
						});
					} else{
						play_correct_incorrect_sound(0);
						$(this).css('background-color', '#FF7770');
					}
				});
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_diy',

		extratextblock : [{
			textdata : data.string.diy,
			textclass : 'diy-title'
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_die',

		extratextblock : [{
			textdata : data.string.p3text1,
			textclass : 'roll-text'
		}],
		die:[{
			containerclass: 'die-1',
			wrapperclass: '',
			dieclass: '',
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_die',

		extratextblock : [{
			textdata : data.string.p3text2,
			textclass : 'roll-text'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "replay-btn",
					imgsrc : "images/replay.png",
				},
			],
		}],
		divtextblock:[{
			divclass: 'big-dice bd-1',
			textdata : data.string.t_5,
		}],

		equationblockclass: '',
		equation:[{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		},{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		},{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		},{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		},{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		}],

	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_die',

		extratextblock : [{
			textdata : data.string.p3text1,
			textclass : 'roll-text'
		}],
		die:[{
			containerclass: 'die-1',
			wrapperclass: '',
			dieclass: '',
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_die',

		extratextblock : [{
			textdata : data.string.p3text2,
			textclass : 'roll-text'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "replay-btn",
					imgsrc : "images/replay.png",
				},
			],
		}],
		divtextblock:[{
			divclass: 'big-dice bd-1',
			textdata : data.string.t_5,
		}],

		equationblockclass: '',
		equation:[{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		},{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		},{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		},{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		},{
			textdata1: data.string.t_4,
			textdata2: data.string.t_q,
			textclass2: 'change-eq',
			textdata3: data.string.t_11,
			textdata4: data.string.t_11,
			btntext: data.string.check
		}],

	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound;
	var myTimeout =  null;
	var myTimeout1 =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var dnum1 = 0;
	var dnum2 = 0;

	var num_sum = 0;
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_1", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			sound_player1("sound_1");
				var rand_dnum_arr = [6];

				$('.die-container').click(function(){
					var current_die = 1;
					if($(this).hasClass('die-1')){
						current_die = 1;
					};
					rand_dnum_arr.shufflearray();
					for(var i=2; i<7; i++){
						$('.die-'+current_die+' .dnum-'+i).html(rand_dnum_arr[i-2]);
					}
					$('.die-'+current_die).css("pointer-events", 'none');
					$('.die-'+current_die+'>.wrapper').removeClass('roll-die-anim');
					$('.die-'+current_die+' .dice').removeClass('spin-die-anim');

					num_sum = parseInt($('.die-1 .dnum-6').html());
					dnum1 = parseInt($('.die-1 .dnum-6').html());
					myTimeout = setTimeout(function(){
						$('.die-'+current_die+'>.wrapper').addClass('roll-die-anim');
						$('.die-'+current_die+' .dice').addClass('spin-die-anim');
					}, 10);
					myTimeout1 = setTimeout(function(){
						$('.die-'+current_die).css("pointer-events", 'all');
						nav_button_controls(200);
					}, 3050);
				});
				break;
			case 2:
			case 4:
			sound_player1("sound_"+countNext);
				correct_count = 0;
				input_box('.eq-2', 2, '');
				$('.bd-1>p').html(dnum1);
				$('.eq-3').html(num_sum);
				var i = num_sum-1;
				var num_arr = [];
				var new_arr = [];
				while(i>0){
					num_arr.push(i);
					i--;
				}
				if(num_arr.length>5){
					num_arr.shufflearray();
					new_arr = num_arr.slice(0, 5);
				}else{
					new_arr = num_arr;
					for(var j=4; j>num_arr.length-1; j--){
						$('.equationcontainer').eq(j).remove();
					}
				}
				function sortNumber(a,b) {
					return a - b;
				}
				new_arr.sort(sortNumber);
				for(var j=0; j<new_arr.length; j++){
					$('.equationcontainer').eq(j).children('.eq-textblock').children('.eq-1').html(new_arr[j]);
				}
				$('.equationcontainer').eq(0).css('display', 'flex');
				$('.submit-btn').click(function(){
					var curr_sumval = parseInt($(this).parent().find('.eq-1').html())+
						parseInt($(this).parent().find('.eq-2').val());
						console.log($(this).parent().find('.eq-1'));
					if( curr_sumval==  num_sum){
						$(this).parent().children('.corrincor').attr('src', 'images/correct.png');
						$(this).parent().children('.corrincor').css('display', 'block');
						$(this).hide(0);
						play_correct_incorrect_sound(1);
						correct_count++;
						if(correct_count==$('.equationcontainer').length){
							nav_button_controls(0);
						} else{
							$('.equationcontainer').eq(correct_count).css('display', 'flex');
						}
					} else {
						$(this).parent().children('.corrincor').css('display', 'block');
						play_correct_incorrect_sound(0);
					}
				});
				$('.replay-btn').click(function(){
					countNext=1;
					templateCaller();
				});
				break;
			case 3:
				var nums_arr = [10,11,12,13,14,15,16,17,18,19];
				nums_arr.shufflearray();
				for(var i=1; i<7; i++){
					$('.dnum-'+i).html(nums_arr[i]);
				}
				var rand_dnum_arr = [6];
				$('.die-container').click(function(){
					var current_die = 1;
					if($(this).hasClass('die-1')){
						current_die = 1;
					};
					rand_dnum_arr.shufflearray();
					for(var i=2; i<7; i++){
						$('.die-'+current_die+' .dnum-'+i).html(rand_dnum_arr[i-2]);
					}
					$('.die-'+current_die).css("pointer-events", 'none');
					$('.die-'+current_die+'>.wrapper').removeClass('roll-die-anim');
					$('.die-'+current_die+' .dice').removeClass('spin-die-anim');

					num_sum = parseInt($('.die-1 .dnum-6').html());
					dnum1 = parseInt($('.die-1 .dnum-6').html());
					$nextBtn.hide(0);
					myTimeout = setTimeout(function(){
						$('.die-'+current_die+'>.wrapper').addClass('roll-die-anim');
						$('.die-'+current_die+' .dice').addClass('spin-die-anim');
					}, 10);
					myTimeout1 = setTimeout(function(){
						$('.die-'+current_die).css("pointer-events", 'all');
						nav_button_controls(200);
					}, 3050);
				});
				break;
			default:
				if(countNext>0){
					$prevBtn.show(0);
				}
				$nextBtn.show(0);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		clearTimeout(myTimeout1);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		clearTimeout(myTimeout1);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

		/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if(charCode === 13 && button_class!=null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

// slide0
{
		contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p1text17
			},
			{
				textclass: "subjects",
				textdata: data.string.p1text16
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
					textclass:'number2',
					textdata:data.string.p1text8
			},
			{
					textclass:'number3',
					textdata:data.string.p1text9
			},
			{
					textclass:'number4',
					textdata:data.string.p1text10
			},
			{
					textclass:'number5',
					textdata:data.string.p1text11
			},
			{
					textclass:'number6',
					textdata:data.string.p1text15
			},
			{
					textclass:'dog_text',
					textdata:data.string.p1text3
			},
			{
					textclass:'goat_text',
					textdata:data.string.p1text4
			},
			{
					textclass:'hen_text',
					textdata:data.string.p1text5
			},
			{
					textclass:'cow_text',
					textdata:data.string.p1text6
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q01',
					imgsrc: ""
				},
				{
					imgclass: "dog",
					imgid : 'dog',
					imgsrc: ""
				},
				{
					imgclass: "goat",
					imgid : 'goat',
					imgsrc: ""
				},
				{
					imgclass: "hen",
					imgid : 'hen',
					imgsrc: ""
				},
				{
					imgclass: "cow",
					imgid : 'cow',
					imgsrc: ""
				}
			]
		}],
	question_div:[{
				textclass:'question',
				textdata:data.string.exques1,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q1ansc
		},
		{
				textclass:'option',
				textdata:data.string.q1ansa
		},
		{
				textclass:'option',
				textdata:data.string.q1ansb
		},
		{
				textclass:'option',
				textdata:data.string.q1ansd
		},
	]
},


// slide1
{
		contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
				textclass: "numbertag-1 numbertag",
				textdata: data.string.p3text5
			},
			{
				textclass: "numbertag-2 numbertag",
				textdata: data.string.p3text6
			},
			{
				textclass: "numbertag-3 numbertag",
				textdata: data.string.p3text7
			},
			{
				textclass: "numbertag-4 numbertag",
				textdata: data.string.p3text8
			},
			{
				textclass: "numbertag-5 numbertag",
				textdata: data.string.p3text9
			},
			{
				textclass: "numbertag-6 numbertag",
				textdata: data.string.p3text10
			},
			{
				textclass: "numbertag-7 numbertag",
				textdata: data.string.p3text11
			},
			{
				textclass: "numbertag-8 numbertag",
				textdata: data.string.p3text12
			},
			{
				textclass: "numbertag-9 numbertag",
				textdata: data.string.p3text13
			},
			{
					textclass:'text-1',
					textdata:data.string.p3text14
			},
			{
					textclass:'text-2',
					textdata:data.string.p3text15
			},
			{
					textclass:'text-3',
					textdata:data.string.p3text16
			},
			{
					textclass:'text-4',
					textdata:data.string.p3text17
			},
			{
					textclass:'text-5',
					textdata:data.string.p3text18
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q02',
					imgsrc: ""
				}
			]
		}],
	question_div_additional_class:'blue',
	question_div:[{
				textclass:'question',
				textdata:data.string.exques2,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q2ansd
		},
		{
				textclass:'option',
				textdata:data.string.q2ansa
		},
		{
				textclass:'option',
				textdata:data.string.q2ansb
		},
		{
				textclass:'option',
				textdata:data.string.q2ansc
		},
	]
},


// slide2
{
	contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.add1
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
				textclass: "numbertag-1 numbertag",
				textdata: data.string.p3text5
			},
			{
				textclass: "numbertag-2 numbertag",
				textdata: data.string.p3text6
			},
			{
				textclass: "numbertag-3 numbertag",
				textdata: data.string.p3text7
			},
			{
				textclass: "numbertag-4 numbertag",
				textdata: data.string.p3text8
			},
			{
				textclass: "numbertag-5 numbertag",
				textdata: data.string.p3text9
			},
			{
				textclass: "numbertag-6 numbertag",
				textdata: data.string.p3text10
			},
			{
				textclass: "numbertag-7 numbertag",
				textdata: data.string.p3text11
			},
			{
				textclass: "numbertag-8 numbertag",
				textdata: data.string.p3text12
			},
			{
				textclass: "numbertag-9 numbertag",
				textdata: data.string.p3text13
			},
			{
					textclass:'text-1',
					textdata:data.string.p3text14
			},
			{
					textclass:'text-2',
					textdata:data.string.p3text15
			},
			{
					textclass:'text-3',
					textdata:data.string.p3text16
			},
			{
					textclass:'text-4',
					textdata:data.string.p3text17
			},
			{
					textclass:'text-5',
					textdata:data.string.p3text18
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q03',
					imgsrc: ""
				}
			]
		}],
	question_div_additional_class:'blue',
	question_div:[{
				textclass:'question',
				textdata:data.string.exques3,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q3ansa
		},
		{
				textclass:'option',
				textdata:data.string.q3ansd
		},
		{
				textclass:'option',
				textdata:data.string.q3ansb
		},
		{
				textclass:'option',
				textdata:data.string.q3ansc
		},
	]
},
// slide4
{
		contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p1text17
			},
			{
				textclass: "subjects",
				textdata: data.string.p1text16
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
					textclass:'number2',
					textdata:data.string.p1text8
			},
			{
					textclass:'number3',
					textdata:data.string.p1text9
			},
			{
					textclass:'number4',
					textdata:data.string.p1text10
			},
			{
					textclass:'number5',
					textdata:data.string.p1text11
			},
			{
					textclass:'number6',
					textdata:data.string.p1text15
			},
			{
					textclass:'dog_text',
					textdata:data.string.p1text3
			},
			{
					textclass:'goat_text',
					textdata:data.string.p1text4
			},
			{
					textclass:'hen_text',
					textdata:data.string.p1text5
			},
			{
					textclass:'cow_text',
					textdata:data.string.p1text6
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q01',
					imgsrc: ""
				},
				{
					imgclass: "dog",
					imgid : 'dog',
					imgsrc: ""
				},
				{
					imgclass: "goat",
					imgid : 'goat',
					imgsrc: ""
				},
				{
					imgclass: "hen",
					imgid : 'hen',
					imgsrc: ""
				},
				{
					imgclass: "cow",
					imgid : 'cow',
					imgsrc: ""
				}
			]
		}],
	question_div:[{
				textclass:'question',
				textdata:data.string.exques4,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q4ansd
		},
		{
				textclass:'option',
				textdata:data.string.q4ansa
		},
		{
				textclass:'option',
				textdata:data.string.q4ansb
		},
		{
				textclass:'option',
				textdata:data.string.q4ansc
		},
	]
},


// slide5
{
	contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.add1
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
				textclass: "numbertag-1 numbertag",
				textdata: data.string.p3text5
			},
			{
				textclass: "numbertag-2 numbertag",
				textdata: data.string.p3text6
			},
			{
				textclass: "numbertag-3 numbertag",
				textdata: data.string.p3text7
			},
			{
				textclass: "numbertag-4 numbertag",
				textdata: data.string.p3text8
			},
			{
				textclass: "numbertag-5 numbertag",
				textdata: data.string.p3text9
			},
			{
				textclass: "numbertag-6 numbertag",
				textdata: data.string.p3text10
			},
			{
				textclass: "numbertag-7 numbertag",
				textdata: data.string.p3text11
			},
			{
				textclass: "numbertag-8 numbertag",
				textdata: data.string.p3text12
			},
			{
				textclass: "numbertag-9 numbertag",
				textdata: data.string.p3text13
			},
			{
					textclass:'text-1',
					textdata:data.string.p3text14
			},
			{
					textclass:'text-2',
					textdata:data.string.p3text15
			},
			{
					textclass:'text-3',
					textdata:data.string.p3text16
			},
			{
					textclass:'text-4',
					textdata:data.string.p3text17
			},
			{
					textclass:'text-5',
					textdata:data.string.p3text18
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q06',
					imgsrc: ""
				}
			]
		}],
	question_div_additional_class:'blue',
	question_div:[{
				textclass:'question',
				textdata:data.string.exques5,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q5ansa
		},
		{
				textclass:'option',
				textdata:data.string.q5ansd
		},
		{
				textclass:'option',
				textdata:data.string.q5ansb
		},
		{
				textclass:'option',
				textdata:data.string.q5ansc
		},
	]
},

// slide6
{
	contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.numberfruits
			},
			{
				textclass: "subjects",
				textdata: data.string.fruits
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
				textclass: "numbertag-1 numbertag",
				textdata: data.string.p3text5
			},
			{
				textclass: "numbertag-2 numbertag",
				textdata: data.string.p3text6
			},
			{
				textclass: "numbertag-3 numbertag",
				textdata: data.string.p3text7
			},
			{
				textclass: "numbertag-4 numbertag",
				textdata: data.string.p3text8
			},
			{
				textclass: "numbertag-5 numbertag",
				textdata: data.string.p3text9
			},
			{
				textclass: "numbertag-6 numbertag",
				textdata: data.string.p3text10
			},
			{
				textclass: "numbertag-7 numbertag",
				textdata: data.string.p3text11
			},
			{
				textclass: "numbertag-8 numbertag",
				textdata: data.string.p3text12
			},
			{
				textclass: "numbertag-9 numbertag",
				textdata: data.string.p3text13
			},
			{
					textclass:'text-1a',
					textdata:data.string.apple
			},
			{
					textclass:'text-2a',
					textdata:data.string.orange
			},
			{
					textclass:'text-3a',
					textdata:data.string.banana
			},
			{
					textclass:'text-4a',
					textdata:data.string.mango
			},

		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q07',
					imgsrc: ""
				}
			]
		}],
	question_div_additional_class:'blue',
	question_div:[{
				textclass:'question',
				textdata:data.string.exques6,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q6ansa
		},
		{
				textclass:'option',
				textdata:data.string.q6ansd
		},
		{
				textclass:'option',
				textdata:data.string.q6ansb
		},
		{
				textclass:'option',
				textdata:data.string.q6ansc
		},
	]
},


// slide7
{
	contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.numberfruits
			},
			{
				textclass: "subjects",
				textdata: data.string.fruits
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
				textclass: "numbertag-1 numbertag",
				textdata: data.string.p3text5
			},
			{
				textclass: "numbertag-2 numbertag",
				textdata: data.string.p3text6
			},
			{
				textclass: "numbertag-3 numbertag",
				textdata: data.string.p3text7
			},
			{
				textclass: "numbertag-4 numbertag",
				textdata: data.string.p3text8
			},
			{
				textclass: "numbertag-5 numbertag",
				textdata: data.string.p3text9
			},
			{
				textclass: "numbertag-6 numbertag",
				textdata: data.string.p3text10
			},
			{
				textclass: "numbertag-7 numbertag",
				textdata: data.string.p3text11
			},
			{
				textclass: "numbertag-8 numbertag",
				textdata: data.string.p3text12
			},
			{
				textclass: "numbertag-9 numbertag",
				textdata: data.string.p3text13
			},
			{
					textclass:'text-1a',
					textdata:data.string.apple
			},
			{
					textclass:'text-2a',
					textdata:data.string.orange
			},
			{
					textclass:'text-3a',
					textdata:data.string.banana
			},
			{
					textclass:'text-4a',
					textdata:data.string.mango
			},

		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q08',
					imgsrc: ""
				}
			]
		}],
	question_div_additional_class:'blue',
	question_div:[{
				textclass:'question',
				textdata:data.string.exques7,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q7ansa
		},
		{
				textclass:'option',
				textdata:data.string.q7ansd
		},
		{
				textclass:'option',
				textdata:data.string.q7ansb
		},
		{
				textclass:'option',
				textdata:data.string.q7ansc
		},
	]
},

// slide8
{
	contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.numberfruits
			},
			{
				textclass: "subjects",
				textdata: data.string.fruits
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
				textclass: "numbertag-1 numbertag",
				textdata: data.string.p3text5
			},
			{
				textclass: "numbertag-2 numbertag",
				textdata: data.string.p3text6
			},
			{
				textclass: "numbertag-3 numbertag",
				textdata: data.string.p3text7
			},
			{
				textclass: "numbertag-4 numbertag",
				textdata: data.string.p3text8
			},
			{
				textclass: "numbertag-5 numbertag",
				textdata: data.string.p3text9
			},
			{
				textclass: "numbertag-6 numbertag",
				textdata: data.string.p3text10
			},
			{
				textclass: "numbertag-7 numbertag",
				textdata: data.string.p3text11
			},
			{
				textclass: "numbertag-8 numbertag",
				textdata: data.string.p3text12
			},
			{
				textclass: "numbertag-9 numbertag",
				textdata: data.string.p3text13
			},
			{
					textclass:'text-1a',
					textdata:data.string.apple
			},
			{
					textclass:'text-2a',
					textdata:data.string.orange
			},
			{
					textclass:'text-3a',
					textdata:data.string.banana
			},
			{
					textclass:'text-4a',
					textdata:data.string.mango
			},

		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q07',
					imgsrc: ""
				}
			]
		}],
	question_div_additional_class:'blue',
	question_div:[{
				textclass:'question',
				textdata:data.string.exques8,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q8ansa
		},
		{
				textclass:'option',
				textdata:data.string.q8ansd
		},
		{
				textclass:'option',
				textdata:data.string.q8ansb
		},
		{
				textclass:'option',
				textdata:data.string.q8ansc
		},
	]
},


// slide9
{
	contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.add1
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
				textclass: "numbertag-1 numbertag",
				textdata: data.string.p3text5
			},
			{
				textclass: "numbertag-2 numbertag",
				textdata: data.string.p3text6
			},
			{
				textclass: "numbertag-3 numbertag",
				textdata: data.string.p3text7
			},
			{
				textclass: "numbertag-4 numbertag",
				textdata: data.string.p3text8
			},
			{
				textclass: "numbertag-5 numbertag",
				textdata: data.string.p3text9
			},
			{
				textclass: "numbertag-6 numbertag",
				textdata: data.string.p3text10
			},
			{
				textclass: "numbertag-7 numbertag",
				textdata: data.string.p3text11
			},
			{
				textclass: "numbertag-8 numbertag",
				textdata: data.string.p3text12
			},
			{
				textclass: "numbertag-9 numbertag",
				textdata: data.string.p3text13
			},
			{
					textclass:'text-1',
					textdata:data.string.p3text14
			},
			{
					textclass:'text-2',
					textdata:data.string.p3text15
			},
			{
					textclass:'text-3',
					textdata:data.string.p3text16
			},
			{
					textclass:'text-4',
					textdata:data.string.p3text17
			},
			{
					textclass:'text-5',
					textdata:data.string.p3text18
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q03',
					imgsrc: ""
				}
			]
		}],
	question_div_additional_class:'blue',
	question_div:[{
				textclass:'question',
				textdata:data.string.exques9,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q9ansa
		},
		{
				textclass:'option',
				textdata:data.string.q9ansd
		},
		{
				textclass:'option',
				textdata:data.string.q9ansb
		},
		{
				textclass:'option',
				textdata:data.string.q9ansc
		},
	]
},


// slide10
{
		contentblockadditionalclass:'brown_bg',
		extratextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p1text17
			},
			{
				textclass: "subjects",
				textdata: data.string.p1text16
			},
			{
				textclass: "top_text",
				textdata: data.string.p4text8
			},
			{
					textclass:'number2',
					textdata:data.string.p1text8
			},
			{
					textclass:'number3',
					textdata:data.string.p1text9
			},
			{
					textclass:'number4',
					textdata:data.string.p1text10
			},
			{
					textclass:'number5',
					textdata:data.string.p1text11
			},
			{
					textclass:'number6',
					textdata:data.string.p1text15
			},
			{
					textclass:'dog_text',
					textdata:data.string.p1text3
			},
			{
					textclass:'goat_text',
					textdata:data.string.p1text4
			},
			{
					textclass:'hen_text',
					textdata:data.string.p1text5
			},
			{
					textclass:'cow_text',
					textdata:data.string.p1text6
			},
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow_5",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "middle_image",
					imgid : 'q01',
					imgsrc: ""
				},
				{
					imgclass: "dog",
					imgid : 'dog',
					imgsrc: ""
				},
				{
					imgclass: "goat",
					imgid : 'goat',
					imgsrc: ""
				},
				{
					imgclass: "hen",
					imgid : 'hen',
					imgsrc: ""
				},
				{
					imgclass: "cow",
					imgid : 'cow',
					imgsrc: ""
				}
			]
		}],
	question_div:[{
				textclass:'question',
				textdata:data.string.exques10,
				datahighlightflag:true,
				datahighlightcustomclass:'instruction_highlight'
		},
		{
				textclass:'option correct',
				textdata:data.string.q10ansa
		},
		{
				textclass:'option',
				textdata:data.string.q10ansc
		},
		{
				textclass:'option',
				textdata:data.string.q10ansb
		},
		{
				textclass:'option',
				textdata:data.string.q10ansd
		},
	]
},

];
content.shufflearray();
$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var score = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//  
			//images
			{id: "arrow_l", src: imgpath+"arrow_l.png", type: createjs.AbstractLoader.IMAGE},
			{id: "up_arrow", src: imgpath+"up_arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q01", src: imgpath1+"q01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q02", src: imgpath1+"q02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q03", src: imgpath1+"q04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q06", src: imgpath1+"q06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q07", src: imgpath1+"q07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q08", src: imgpath1+"q08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "goat", src: imgpath+"goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	var rhino = new NumberTemplate();

	rhino.init($total_page);
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		vocabcontroller.findwords(countNext);
		/*generate question no at the beginning of question*/
		rhino.numberOfQuestions();
		$nextBtn.hide(0);
		$prevBtn.hide(0);


		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		if(countNext==0) sound_player1('sound_1');
		var pos_array=['pos1','pos2','pos3','pos4'];
		pos_array.shufflearray();
		for (var i = 0; i < 4; i++) {
			$('.option').eq(i).addClass(pos_array[i]);
		}
		var wrong_clicked = false;
		$('.option').click(function(){
			if($(this).hasClass('correct')){
				if(!wrong_clicked){
					rhino.update(true);
				}
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.question_div').width()+'%';
				var centerY = ((position.top + height)*100)/$('.question_div').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:10%;transform:translate(2%,-222%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$(this).css({"background": "#98C02E",
											"border-color": "#DEEF3C",
												"transition":'.1s'});
				$('.option').css('pointer-events','none');
				current_sound.stop();

				play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).css({"background": "#FF0000",
											"border-color": "#980000",
												"transition":'.1s',
												"pointer-events":"none"});
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.question_div').width()+'%';
				var centerY = ((position.top + height)*100)/$('.question_div').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:10%;transform:translate(2%,-222%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				current_sound.stop();
				
				play_correct_incorrect_sound(0);

				wrong_clicked = true;
			}
		});
		// $('.question_div').animate({'right':'0'},500);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			 ole.footerNotificationHandler.hideNotification() ;
			if(countNext==0){
				$nextBtn.show(0);
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next == null)
			nav_button_controls(500);
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		// current_sound.on('complete', function(){
		// 	if(next == null)
		// 	navigationcontroller();
		// });
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);

		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		rhino.gotoNext();
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		 countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

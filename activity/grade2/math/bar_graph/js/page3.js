var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
	    extratextblock:[
		{
			textclass: "top_text",
			textdata: data.string.p3text1
		}
		],

	imageblock:[{
		imagestoshow:[
			{
				imgclass: "bg_full",
				imgid : 'schoolbg',
				imgsrc: ""
			}
		]
	}]
},


// slide1
{
		extratextblock:[
	{
		textclass: "top_text",
		textdata: data.string.p3text1
	}
	],
	uppertextblockadditionalclass:'popup_block',
	uppertextblock:[
		{
			textclass: "text1",
			textdata: data.string.p3text2
		},
		{
			textclass: "text2",
			textdata: data.string.p3text3
		},
		{
			textclass: "textnew",
			textdata: data.string.p3text3b
		},
		{
			textclass: "text3",
			textdata: data.string.p3text4
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "bg_full",
			imgid : 'schoolbg',
			imgsrc: ""
		}
	]
}]
},

// slide2
{
		uppertextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "template-dialougebox2-top-flipped-yellow boy_dialogue",
				textdata: data.string.p3text21
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "pasang",
					imgid : 'pasang02',
					imgsrc: ""
				},
			]
		}],
	table:[
		{
			textclass: "numbertag-1 numbertag",
			textdata: data.string.p3text5
		},
		{
			textclass: "numbertag-2 numbertag",
			textdata: data.string.p3text6
		},
		{
			textclass: "numbertag-3 numbertag",
			textdata: data.string.p3text7
		},
		{
			textclass: "numbertag-4 numbertag",
			textdata: data.string.p3text8
		},
		{
			textclass: "numbertag-5 numbertag",
			textdata: data.string.p3text9
		},
		{
			textclass: "numbertag-6 numbertag",
			textdata: data.string.p3text10
		},
		{
			textclass: "numbertag-7 numbertag",
			textdata: data.string.p3text11
		},
		{
			textclass: "numbertag-8 numbertag",
			textdata: data.string.p3text12
		},
		{
			textclass: "numbertag-9 numbertag",
			textdata: data.string.p3text13
		},

		{
			textclass: "name-1 name",
			textdata: data.string.p3text14
		},
		{
			textclass: "name-2 name",
			textdata: data.string.p3text15
		},
		{
			textclass: "name-3 name",
			textdata: data.string.p3text16
		},
		{
			textclass: "name-4 name",
			textdata: data.string.p3text17
		},
		{
			textclass: "name-5 name",
			textdata: data.string.p3text18
		},
	]
},


// slide3
{
		uppertextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "template-dialougebox2-top-flipped-yellow boy_dialogue",
				textdata: data.string.p3text22
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "pasang",
					imgid : 'pasang02',
					imgsrc: ""
				},
			]
		}],
	table:[
		{
			textclass: "numbertag-1 numbertag",
			textdata: data.string.p3text5
		},
		{
			textclass: "numbertag-2 numbertag",
			textdata: data.string.p3text6
		},
		{
			textclass: "numbertag-3 numbertag",
			textdata: data.string.p3text7
		},
		{
			textclass: "numbertag-4 numbertag",
			textdata: data.string.p3text8
		},
		{
			textclass: "numbertag-5 numbertag",
			textdata: data.string.p3text9
		},
		{
			textclass: "numbertag-6 numbertag",
			textdata: data.string.p3text10
		},
		{
			textclass: "numbertag-7 numbertag",
			textdata: data.string.p3text11
		},
		{
			textclass: "numbertag-8 numbertag",
			textdata: data.string.p3text12
		},
		{
			textclass: "numbertag-9 numbertag",
			textdata: data.string.p3text13
		},

		{
			textclass: "name-1 name",
			textdata: data.string.p3text14
		},
		{
			textclass: "name-2 name",
			textdata: data.string.p3text15
		},
		{
			textclass: "name-3 name",
			textdata: data.string.p3text16
		},
		{
			textclass: "name-4 name",
			textdata: data.string.p3text17
		},
		{
			textclass: "name-5 name",
			textdata: data.string.p3text18
		},
	]
},

// slide3
{
		uppertextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "template-dialougebox2-top-flipped-yellow boy_dialogue",
				textdata: data.string.p3text24
			},
			{
				textclass: "top_text",
				textdata: data.string.p3text23
			},
			{
				textclass: "feedback_text",
				textdata: data.string.p3text25
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "pasang",
					imgid : 'pasang02',
					imgsrc: ""
				},
			]
		}],
	table:[
		{
			textclass: "numbertag-1 numbertag",
			textdata: data.string.p3text5
		},
		{
			textclass: "numbertag-2 numbertag",
			textdata: data.string.p3text6
		},
		{
			textclass: "numbertag-3 numbertag",
			textdata: data.string.p3text7
		},
		{
			textclass: "numbertag-4 numbertag",
			textdata: data.string.p3text8
		},
		{
			textclass: "numbertag-5 numbertag",
			textdata: data.string.p3text9
		},
		{
			textclass: "numbertag-6 numbertag",
			textdata: data.string.p3text10
		},
		{
			textclass: "numbertag-7 numbertag",
			textdata: data.string.p3text11
		},
		{
			textclass: "numbertag-8 numbertag",
			textdata: data.string.p3text12
		},
		{
			textclass: "numbertag-9 numbertag",
			textdata: data.string.p3text13
		},

		{
			textclass: "name-1 name",
			textdata: data.string.p3text14
		},
		{
			textclass: "name-2 name",
			textdata: data.string.p3text15
		},
		{
			textclass: "name-3 name",
			textdata: data.string.p3text16
		},
		{
			textclass: "name-4 name",
			textdata: data.string.p3text17
		},
		{
			textclass: "name-5 name",
			textdata: data.string.p3text18
		},
	]
},


// slide4
{
		uppertextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "template-dialougebox2-top-flipped-yellow boy_dialogue",
				textdata: data.string.p3text26
			},
			{
				textclass: "top_text",
				textdata: data.string.p3text23
			},
			{
				textclass: "feedback_text",
				textdata: data.string.p3text27
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "pasang",
					imgid : 'pasang02',
					imgsrc: ""
				},
			]
		}],
	table:[
		{
			textclass: "numbertag-1 numbertag",
			textdata: data.string.p3text5
		},
		{
			textclass: "numbertag-2 numbertag",
			textdata: data.string.p3text6
		},
		{
			textclass: "numbertag-3 numbertag",
			textdata: data.string.p3text7
		},
		{
			textclass: "numbertag-4 numbertag",
			textdata: data.string.p3text8
		},
		{
			textclass: "numbertag-5 numbertag",
			textdata: data.string.p3text9
		},
		{
			textclass: "numbertag-6 numbertag",
			textdata: data.string.p3text10
		},
		{
			textclass: "numbertag-7 numbertag",
			textdata: data.string.p3text11
		},
		{
			textclass: "numbertag-8 numbertag",
			textdata: data.string.p3text12
		},
		{
			textclass: "numbertag-9 numbertag",
			textdata: data.string.p3text13
		},

		{
			textclass: "name-1 name",
			textdata: data.string.p3text14
		},
		{
			textclass: "name-2 name",
			textdata: data.string.p3text15
		},
		{
			textclass: "name-3 name",
			textdata: data.string.p3text16
		},
		{
			textclass: "name-4 name",
			textdata: data.string.p3text17
		},
		{
			textclass: "name-5 name",
			textdata: data.string.p3text18
		},
	]
},


// slide6
{
		uppertextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "template-dialougebox2-top-flipped-yellow boy_dialogue",
				textdata: data.string.p3text28
			},
			{
				textclass: "top_text",
				textdata: data.string.p3text23
			},
			{
				textclass: "feedback_text",
				textdata: data.string.p3text29
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "pasang",
					imgid : 'pasang02',
					imgsrc: ""
				},
			]
		}],
	table:[
		{
			textclass: "numbertag-1 numbertag",
			textdata: data.string.p3text5
		},
		{
			textclass: "numbertag-2 numbertag",
			textdata: data.string.p3text6
		},
		{
			textclass: "numbertag-3 numbertag",
			textdata: data.string.p3text7
		},
		{
			textclass: "numbertag-4 numbertag",
			textdata: data.string.p3text8
		},
		{
			textclass: "numbertag-5 numbertag",
			textdata: data.string.p3text9
		},
		{
			textclass: "numbertag-6 numbertag",
			textdata: data.string.p3text10
		},
		{
			textclass: "numbertag-7 numbertag",
			textdata: data.string.p3text11
		},
		{
			textclass: "numbertag-8 numbertag",
			textdata: data.string.p3text12
		},
		{
			textclass: "numbertag-9 numbertag",
			textdata: data.string.p3text13
		},

		{
			textclass: "name-1 name",
			textdata: data.string.p3text14
		},
		{
			textclass: "name-2 name",
			textdata: data.string.p3text15
		},
		{
			textclass: "name-3 name",
			textdata: data.string.p3text16
		},
		{
			textclass: "name-4 name",
			textdata: data.string.p3text17
		},
		{
			textclass: "name-5 name",
			textdata: data.string.p3text18
		},
	]
},

// slide7
{
		uppertextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "template-dialougebox2-top-flipped-yellow boy_dialogue",
				textdata: data.string.p3text30
			},
			{
				textclass: "top_text",
				textdata: data.string.p3text23
			},
			{
				textclass: "feedback_text",
				textdata: data.string.p3text31
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "pasang",
					imgid : 'pasang02',
					imgsrc: ""
				},
			]
		}],
	table:[
		{
			textclass: "numbertag-1 numbertag",
			textdata: data.string.p3text5
		},
		{
			textclass: "numbertag-2 numbertag",
			textdata: data.string.p3text6
		},
		{
			textclass: "numbertag-3 numbertag",
			textdata: data.string.p3text7
		},
		{
			textclass: "numbertag-4 numbertag",
			textdata: data.string.p3text8
		},
		{
			textclass: "numbertag-5 numbertag",
			textdata: data.string.p3text9
		},
		{
			textclass: "numbertag-6 numbertag",
			textdata: data.string.p3text10
		},
		{
			textclass: "numbertag-7 numbertag",
			textdata: data.string.p3text11
		},
		{
			textclass: "numbertag-8 numbertag",
			textdata: data.string.p3text12
		},
		{
			textclass: "numbertag-9 numbertag",
			textdata: data.string.p3text13
		},

		{
			textclass: "name-1 name",
			textdata: data.string.p3text14
		},
		{
			textclass: "name-2 name",
			textdata: data.string.p3text15
		},
		{
			textclass: "name-3 name",
			textdata: data.string.p3text16
		},
		{
			textclass: "name-4 name",
			textdata: data.string.p3text17
		},
		{
			textclass: "name-5 name",
			textdata: data.string.p3text18
		},
	]
},


// slide8
{
		uppertextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "template-dialougebox2-top-flipped-yellow boy_dialogue",
				textdata: data.string.p3text32
			},
			{
				textclass: "top_text",
				textdata: data.string.p3text23
			},
			{
				textclass: "feedback_text",
				textdata: data.string.p3text33
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "pasang",
					imgid : 'pasang02',
					imgsrc: ""
				},
			]
		}],
	table:[
		{
			textclass: "numbertag-1 numbertag",
			textdata: data.string.p3text5
		},
		{
			textclass: "numbertag-2 numbertag",
			textdata: data.string.p3text6
		},
		{
			textclass: "numbertag-3 numbertag",
			textdata: data.string.p3text7
		},
		{
			textclass: "numbertag-4 numbertag",
			textdata: data.string.p3text8
		},
		{
			textclass: "numbertag-5 numbertag",
			textdata: data.string.p3text9
		},
		{
			textclass: "numbertag-6 numbertag",
			textdata: data.string.p3text10
		},
		{
			textclass: "numbertag-7 numbertag",
			textdata: data.string.p3text11
		},
		{
			textclass: "numbertag-8 numbertag",
			textdata: data.string.p3text12
		},
		{
			textclass: "numbertag-9 numbertag",
			textdata: data.string.p3text13
		},

		{
			textclass: "name-1 name",
			textdata: data.string.p3text14
		},
		{
			textclass: "name-2 name",
			textdata: data.string.p3text15
		},
		{
			textclass: "name-3 name",
			textdata: data.string.p3text16
		},
		{
			textclass: "name-4 name",
			textdata: data.string.p3text17
		},
		{
			textclass: "name-5 name",
			textdata: data.string.p3text18
		},
	]
},


// slide9
{
		uppertextblock:[
			{
				textclass: "numberofstudents",
				textdata: data.string.p3text20
			},
			{
				textclass: "subjects",
				textdata: data.string.p3text19
			},
			{
				textclass: "top_text1",
				textdata: data.string.p3text34
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "right_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "left_arrow",
					imgid : 'arrow_l',
					imgsrc: ""
				},
				{
					imgclass: "up_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				},
				{
					imgclass: "down_arrow",
					imgid : 'up_arrow',
					imgsrc: ""
				}
			]
		}],
	table:[
		{
			textclass: "numbertag-1 numbertag",
			textdata: data.string.p3text5
		},
		{
			textclass: "numbertag-2 numbertag",
			textdata: data.string.p3text6
		},
		{
			textclass: "numbertag-3 numbertag",
			textdata: data.string.p3text7
		},
		{
			textclass: "numbertag-4 numbertag",
			textdata: data.string.p3text8
		},
		{
			textclass: "numbertag-5 numbertag",
			textdata: data.string.p3text9
		},
		{
			textclass: "numbertag-6 numbertag",
			textdata: data.string.p3text10
		},
		{
			textclass: "numbertag-7 numbertag",
			textdata: data.string.p3text11
		},
		{
			textclass: "numbertag-8 numbertag",
			textdata: data.string.p3text12
		},
		{
			textclass: "numbertag-9 numbertag",
			textdata: data.string.p3text13
		},

		{
			textclass: "name-1 name",
			textdata: data.string.p3text14
		},
		{
			textclass: "name-2 name",
			textdata: data.string.p3text15
		},
		{
			textclass: "name-3 name",
			textdata: data.string.p3text16
		},
		{
			textclass: "name-4 name",
			textdata: data.string.p3text17
		},
		{
			textclass: "name-5 name",
			textdata: data.string.p3text18
		},
	]
}



];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "schoolbg", src: imgpath+"schoolbg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_l", src: imgpath+"arrow_l.png", type: createjs.AbstractLoader.IMAGE},
			{id: "up_arrow", src: imgpath+"up_arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pasang02", src: imgpath+"pasang02.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_4a", src: soundAsset+"s3_p5_correct.ogg"},
			{id: "sound_4b", src: soundAsset+"s3_p5_click on the graph.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_5a", src: soundAsset+"s3_p6_correct.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_6a", src: soundAsset+"s3_p7_correct.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_7a", src: soundAsset+"s3_p8_correct.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_8a", src: soundAsset+"s3_p9_correct.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_0');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			sound_player("sound_0");
			break;
			case 1:
			$('li').first().css({"text-decoration":"underline"});
			// $('li').eq(6).css({"text-decoration":"underline"});
			sound_player("sound_1");
			break;
			case 2:
			sound_player("sound_2");
			break;
			case 3:
			$('td:nth-child(2),td:nth-child(4),td:nth-child(6),td:nth-child(8),td:nth-child(10)').css('border','.5vmin solid #FFF216');
			sound_player("sound_3");
			break;
			case 4:
			sound_player1("sound_4");
			bar_graph_functioner('tr:nth-child(4) > td:nth-child(2)','number','math_section',5,'#93C47D',"sound_4a");
			break;
			case 5:
			// for (var i = 1; i <= 5; i++) {
				// $('.number'+i).html(i);
				$('tr:nth-child(4) > td:nth-child(2),tr:nth-child(5) > td:nth-child(2),tr:nth-child(6) > td:nth-child(2),tr:nth-child(7) > td:nth-child(2),tr:nth-child(8) > td:nth-child(2)').css({'background':'#93C47D'});
				bar_graph_functioner('tr:nth-child(1) > td:nth-child(4)','numbera','nepali_section',8,'#FFD966',"sound_5a");
				sound_player1("sound_5");
			// }
			break;

			case 6:
			$('tr:nth-child(4) > td:nth-child(2),tr:nth-child(5) > td:nth-child(2),tr:nth-child(6) > td:nth-child(2),tr:nth-child(7) > td:nth-child(2),tr:nth-child(8) > td:nth-child(2)').css({'background':'#93C47D'});
			$('tr:nth-child(1) > td:nth-child(4),tr:nth-child(2) > td:nth-child(4),tr:nth-child(3) > td:nth-child(4),tr:nth-child(4) > td:nth-child(4),tr:nth-child(5) > td:nth-child(4),tr:nth-child(6) > td:nth-child(4),tr:nth-child(7) > td:nth-child(4),tr:nth-child(8) > td:nth-child(4)').css({'background':'#FFD966'});
			bar_graph_functioner('tr:nth-child(6) > td:nth-child(6)','numberb','english_section',3,'#A4C2F4',"sound_6a");
			sound_player1("sound_6");
			break;

			case 7:
			$('tr:nth-child(4) > td:nth-child(2),tr:nth-child(5) > td:nth-child(2),tr:nth-child(6) > td:nth-child(2),tr:nth-child(7) > td:nth-child(2),tr:nth-child(8) > td:nth-child(2)').css({'background':'#93C47D'});
			$('tr:nth-child(1) > td:nth-child(4),tr:nth-child(2) > td:nth-child(4),tr:nth-child(3) > td:nth-child(4),tr:nth-child(4) > td:nth-child(4),tr:nth-child(5) > td:nth-child(4),tr:nth-child(6) > td:nth-child(4),tr:nth-child(7) > td:nth-child(4),tr:nth-child(8) > td:nth-child(4)').css({'background':'#FFD966'});
			$('tr:nth-child(6) > td:nth-child(6),tr:nth-child(7) > td:nth-child(6),tr:nth-child(8) > td:nth-child(6)').css({'background':'#A4C2F4'});
			bar_graph_functioner('tr:nth-child(5) > td:nth-child(8)','numberc','science_section',4,'#EA9999','sound_7a');
			sound_player1("sound_7");
			break;

			case 8:
			$('tr:nth-child(4) > td:nth-child(2),tr:nth-child(5) > td:nth-child(2),tr:nth-child(6) > td:nth-child(2),tr:nth-child(7) > td:nth-child(2),tr:nth-child(8) > td:nth-child(2)').css({'background':'#93C47D'});
			$('tr:nth-child(1) > td:nth-child(4),tr:nth-child(2) > td:nth-child(4),tr:nth-child(3) > td:nth-child(4),tr:nth-child(4) > td:nth-child(4),tr:nth-child(5) > td:nth-child(4),tr:nth-child(6) > td:nth-child(4),tr:nth-child(7) > td:nth-child(4),tr:nth-child(8) > td:nth-child(4)').css({'background':'#FFD966'});
			$('tr:nth-child(6) > td:nth-child(6),tr:nth-child(7) > td:nth-child(6),tr:nth-child(8) > td:nth-child(6)').css({'background':'#A4C2F4'});
			$('tr:nth-child(5) > td:nth-child(8),tr:nth-child(6) > td:nth-child(8),tr:nth-child(7) > td:nth-child(8),tr:nth-child(8) > td:nth-child(8)').css({'background':'#EA9999'});
			bar_graph_functioner('tr:nth-child(7) > td:nth-child(10)','numberd','health_section',2,'#B4A7D6','sound_8a');
			sound_player1("sound_8");
			break;

			case 9:
			sound_player1("sound_9");
			// $('.top_text1').css({"left":"34%"});
			$('tr:nth-child(4) > td:nth-child(2),tr:nth-child(5) > td:nth-child(2),tr:nth-child(6) > td:nth-child(2),tr:nth-child(7) > td:nth-child(2),tr:nth-child(8) > td:nth-child(2)').css({'background':'#93C47D'});
			$('tr:nth-child(1) > td:nth-child(4),tr:nth-child(2) > td:nth-child(4),tr:nth-child(3) > td:nth-child(4),tr:nth-child(4) > td:nth-child(4),tr:nth-child(5) > td:nth-child(4),tr:nth-child(6) > td:nth-child(4),tr:nth-child(7) > td:nth-child(4),tr:nth-child(8) > td:nth-child(4)').css({'background':'#FFD966'});
			$('tr:nth-child(6) > td:nth-child(6),tr:nth-child(7) > td:nth-child(6),tr:nth-child(8) > td:nth-child(6)').css({'background':'#A4C2F4'});
			$('tr:nth-child(5) > td:nth-child(8),tr:nth-child(6) > td:nth-child(8),tr:nth-child(7) > td:nth-child(8),tr:nth-child(8) > td:nth-child(8)').css({'background':'#EA9999'});
			$('tr:nth-child(7) > td:nth-child(10),tr:nth-child(8) > td:nth-child(10)').css({'background':'#B4A7D6'});
			$('.table_div').css('left',' 4%');
			$('.numberofstudents').css('left','16%');
			$('.subjects').css('left','47%');
			$('.up_arrow, .down_arrow').css('left','16%');
			$(' .right_arrow').css('left','60%');
			$(' .left_arrow').css('left','23%');
			nav_button_controls(2300);
			break;
			default:
			sound_player("sound_0");
			break;
		}
		function bar_graph_functioner(top_clicker,numberclass,section_name,max_number,colo,sounddata){
			$('.feedback_text').css('opacity','0');
			$(top_clicker).css({'border':'.5vmin solid #FFF216','cursor':'pointer'});
			$(top_clicker).mouseover(function(){
					$('.'+section_name).css({'background':'#CCCCCC','height':'100%','width':'100%','top':'0','left':'0','position':'absolute'});
			});
			$(top_clicker).mouseleave(function(){
					$('.'+section_name).css({'background':'#F2F2F2','height':'100%','width':'100%','top':'0','left':'0','position':'absolute'});
			});
			$(top_clicker).click(function(){

					sound_player1(sounddata);
					$(this).css('pointer-events','none');
					$('.table_div').css('pointer-events','none');
					$(top_clicker).css({'border':'.25vmin solid #434343','cursor':'pointer'});
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$('.main_table').width()+'%';
					var centerY = ((position.top + height)*100)/$('.main_table').height()+'%';
					$('.pasang,.boy_dialogue').fadeOut(1000);
					$('.feedback_text').animate({'opacity':'1'},1000);
					$('<img style="left:85%;top:65%;position:absolute;width:5%;transform:translate(-50%,-50%)" src="'+imgpath +'correct.png" />').insertAfter('.feedback_text');
					nav_button_controls(500*max_number);
					for (var i = 1; i <= max_number; i++) {
							number_writer(i);
					}
					function number_writer(i){
						setTimeout(function(){
							$('.'+numberclass+i).html(i);
							$('.'+numberclass+i).removeClass(section_name).css({'background':colo,'height':'100%','width':'100%','top':'0','left':'0','position':'absolute'});
						},500*i);

					}
			});
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next == null)
			nav_button_controls(500);
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		// current_sound.on('complete', function(){
		// 	if(next == null)
		// 	navigationcontroller();
		// });
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);

		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

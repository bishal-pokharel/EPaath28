var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
	    extratextblock:[
		{
			textclass: "chapter_title",
			textdata: data.lesson.chapter
		}
		],

	imageblock:[{
		imagestoshow:[
			{
				imgclass: "bg_full",
				imgid : 'coverpage',
				imgsrc: ""
			}
		]
	}]
},

// slide1
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text1
			}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "animals",
			imgid : 'animals',
			imgsrc: ""
		},
		{
			imgclass: "girl1",
			imgid : 'girl1',
			imgsrc: ""
		}
	]
}]
},


// slide2
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text2
			}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl1",
			imgid : 'girl3',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},],
},


// slide3
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text12
			}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl1",
			imgid : 'girl5',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},],
},

// slide4
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text13
			}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl2",
			imgid : 'girl5',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table2:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},],
},




// slide5
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text14
			}
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl2",
			imgid : 'girl5',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table2:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},
{
		textclass:'number6',
		textdata:data.string.p1text15
},],
},


// slide6
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text18
			},
			{
					textclass:'animal',
					textdata:data.string.p1text16
			},
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl2",
			imgid : 'girl5',
			imgsrc: ""
		},
		{
			imgclass: "left_arrow",
			imgid : 'left_arrow',
			imgsrc: ""
		},
		{
			imgclass: "right_arrow",
			imgid : 'right_arrow',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table2:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},
{
		textclass:'number6',
		textdata:data.string.p1text15
},
],
},

// slide7
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text19
			},
			{
					textclass:'animal',
					textdata:data.string.p1text16
			},
			{
					textclass:'numberofanimals',
					textdata:data.string.p1text17
			},
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl2",
			imgid : 'girl5',
			imgsrc: ""
		},
		{
			imgclass: "left_arrow",
			imgid : 'left_arrow',
			imgsrc: ""
		},
		{
			imgclass: "right_arrow",
			imgid : 'right_arrow',
			imgsrc: ""
		},
		{
			imgclass: "up_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		},
		{
			imgclass: "down_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table2:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},
{
		textclass:'number6',
		textdata:data.string.p1text15
}],
},


// slide8
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text20
			},
			{
					textclass:'animal',
					textdata:data.string.p1text16
			},
			{
					textclass:'numberofanimals',
					textdata:data.string.p1text17
			},
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl2",
			imgid : 'girl5',
			imgsrc: ""
		},
		{
			imgclass: "left_arrow",
			imgid : 'left_arrow',
			imgsrc: ""
		},
		{
			imgclass: "right_arrow",
			imgid : 'right_arrow',
			imgsrc: ""
		},
		{
			imgclass: "up_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		},
		{
			imgclass: "down_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		},
		{
			imgclass: "vertical_hand",
			imgid : 'handpointing',
			imgsrc: ""
		},
		{
			imgclass: "horizontal_hand",
			imgid : 'handpointing',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table2:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},
{
		textclass:'number6',
		textdata:data.string.p1text15
}],
},

// slide9
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text21
			},
			{
					textclass:'animal',
					textdata:data.string.p1text16
			},
			{
					textclass:'numberofanimals',
					textdata:data.string.p1text17
			},
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl2",
			imgid : 'girl5',
			imgsrc: ""
		},
		{
			imgclass: "left_arrow",
			imgid : 'left_arrow',
			imgsrc: ""
		},
		{
			imgclass: "right_arrow",
			imgid : 'right_arrow',
			imgsrc: ""
		},
		{
			imgclass: "up_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		},
		{
			imgclass: "down_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		},
		{
			imgclass: "horizontal_hand",
			imgid : 'handpointing',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table2:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},
{
		textclass:'number6',
		textdata:data.string.p1text15
}],
},

// slide10
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text22
			},
			{
					textclass:'animal',
					textdata:data.string.p1text16
			},
			{
					textclass:'numberofanimals',
					textdata:data.string.p1text17
			},
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "girl2",
			imgid : 'girl5',
			imgsrc: ""
		},
		{
			imgclass: "left_arrow",
			imgid : 'left_arrow',
			imgsrc: ""
		},
		{
			imgclass: "right_arrow",
			imgid : 'right_arrow',
			imgsrc: ""
		},
		{
			imgclass: "up_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		},
		{
			imgclass: "down_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		},
		{
			imgclass: "vertical_hand",
			imgid : 'handpointing',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
table2:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},
{
		textclass:'number6',
		textdata:data.string.p1text15
}],
},

// slide11
{
		contentblockadditionalclass:'background_brown',
		extratextblock:[
			{
				textclass: "top_text",
				textdata: data.string.p1text23
			},
			{
					textclass:'animal',
					textdata:data.string.p1text16
			},
			{
					textclass:'numberofanimals',
					textdata:data.string.p1text17
			},
			{
					textclass:'equalsone',
					textdata:data.string.p1text24
			},
	],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "left_arrow",
			imgid : 'left_arrow',
			imgsrc: ""
		},
		{
			imgclass: "right_arrow",
			imgid : 'right_arrow',
			imgsrc: ""
		},
		{
			imgclass: "up_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		},
		{
			imgclass: "down_arrow",
			imgid : 'up_arrow',
			imgsrc: ""
		}
	]
}],
dog:data.string.p1text3,
goat:data.string.p1text4,
hen:data.string.p1text5,
cow:data.string.p1text6,
bar:'bar',
table2:[
{
		textclass:'number2',
		textdata:data.string.p1text8
},
{
		textclass:'number3',
		textdata:data.string.p1text9
},
{
		textclass:'number4',
		textdata:data.string.p1text10
},
{
		textclass:'number5',
		textdata:data.string.p1text11
},
{
		textclass:'number6',
		textdata:data.string.p1text15
},
],
},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "coverpage", src: imgpath+"cover-page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "animals", src: imgpath+"animals.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl1", src: imgpath+"girl01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl3", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl5", src: imgpath+"girl05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "goat", src: imgpath+"goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "left_arrow", src: imgpath+"arrow_l.png", type: createjs.AbstractLoader.IMAGE},
			{id: "right_arrow", src: imgpath+"arrow_r.png", type: createjs.AbstractLoader.IMAGE},
			{id: "up_arrow", src: imgpath+"up_arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handpointing", src: imgpath+"handpointing.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			sound_player("sound_1");
			break;
			case 1:
			sound_player("sound_2");
			break;
			case 2:
			$('.goatya').attr('src',preload.getResult('goat').src);
			$('.doggya').attr('src',preload.getResult('dog').src);
			$('.hennya').attr('src',preload.getResult('hen').src);
			$('.cowya').attr('src',preload.getResult('cow').src);
			$('.dogbg').css({'background':'#6FA8DC','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.goatbg').css({'background':'#FFAB40','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.henbg').css({'background':'#8776BA','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.cowbg').css({'background':'#E8846A','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			sound_player("sound_3");
			break
			case 3:
			$('.goatya').attr('src',preload.getResult('goat').src);
			$('.doggya').attr('src',preload.getResult('dog').src);
			$('.hennya').attr('src',preload.getResult('hen').src);
			$('.cowya').attr('src',preload.getResult('cow').src);
			$('.dogbg').css({'background':'#6FA8DC','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.goatbg').css({'background':'#FFAB40','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.henbg').css({'background':'#8776BA','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.cowbg').css({'background':'#E8846A','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.dog_bot').attr('src',preload.getResult('dog').src);
			$('.goat_bot').attr('src',preload.getResult('goat').src);
			$('.hen_bot').attr('src',preload.getResult('hen').src);
			$('.cow_bot').attr('src',preload.getResult('cow').src);
			$('.title').css("top", "82%");
			$('.bot_row').css('height','27%');
			$('.insideimage_bot').hide(0);
			$('.insideimage').hide(500);
			$('.insideimage_bot').show(500);
			sound_player("sound_4");
			break;
			case 4:
			big_table_function("sound_5");
			break;
			case 5:
			big_table_function("sound_6");
			break;

			case 6:
			big_table_function("sound_7");
			break;

			case 7:
			big_table_function("sound_8");
			break;

			case 8:
			big_table_function("sound_9");
			break;
			case 9:
			big_table_function("sound_10");
			break;
			case 10:
			big_table_function("sound_11");
			break;
			case 11:
			big_table_function("sound_12");
			$('.main_table1').css('display','block');
			$('.dogbg').css('border','.25vmin solid #EEFF41');
			$('tr:nth-child(4) > td:nth-child(2)').css('border','.5vmin solid #EEFF41');
			setTimeout(function(){
				$('.bar_to_animate').addClass('animate_bar');
			},500);
			$('.equalsone').hide(0);
			// $('.equalsone').css('opacity','0');
			setTimeout(function(){
				$('.bar_to_animate').css('left','-63.8%').addClass('bounce');
			},2700);
			setTimeout(function(){
				$('.equalsone').fadeIn(1000);
				// $('.equalsone').animate({'opacity':'1'},500);
			},4500);
			break;

		}
		function big_table_function(sound_to_play){
			$('.goatya').attr('src',preload.getResult('goat').src);
			$('.doggya').attr('src',preload.getResult('dog').src);
			$('.hennya').attr('src',preload.getResult('hen').src);
			$('.cowya').attr('src',preload.getResult('cow').src);
			$('.dogbg').css({'background':'#6FA8DC','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.goatbg').css({'background':'#FFAB40','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.henbg').css({'background':'#8776BA','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.cowbg').css({'background':'#E8846A','width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			$('.dog_bot').attr('src',preload.getResult('dog').src);
			$('.goat_bot').attr('src',preload.getResult('goat').src);
			$('.hen_bot').attr('src',preload.getResult('hen').src);
			$('.cow_bot').attr('src',preload.getResult('cow').src);
			$('.title').css("top", "82%");
			$('.bot_row').css('height','27%');
			$('td').css('width','1.111111%');
				$('.table_div').css({'width': '75%',
			'top': '19%',
			'left': '23%'});
			$('.insideimage_bot').css('width','135%');
			$('.insideimage').hide(0);
			$('.bot_row > td').css({'border': '0vmin','background':'#FFF2CC'});
			sound_player(sound_to_play);
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next == null)
			navigationcontroller();
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		// current_sound.on('complete', function(){
		// 	if(next == null)
		// 	navigationcontroller();
		// });
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);

		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

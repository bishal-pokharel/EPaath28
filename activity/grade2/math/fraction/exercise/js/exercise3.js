var imgpath = $ref + "/exercise/images/ex3/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide1
	{
		val1: 3,
		val2: 4,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-1'
				},
			]
		}],
	},
	//slide2
	{
		val1: 1,
		val2: 4,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-2'
				},
			]
		}],
	},
	//slide3
	{
		val1: 1,
		val2: 3,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-3'
				},
			]
		}],
	},
	//slide4
	{
		val1: 2,
		val2: 3,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-4'
				},
			]
		}],
	},
	//slide5
	{
		val1: 1,
		val2: 2,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-5'
				},
			]
		}],
	},
	//slide6
	{
		val1: 1,
		val2: 3,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-6'
				},
			]
		}],
	},
	//slide7
	{
		val1: 2,
		val2: 3,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-7'
				},
			]
		}],
	},
	//slide8
	{
		val1: 1,
		val2: 4,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-8'
				},
			]
		}],
	},
	//slide9
	{
		val1: 3,
		val2: 4,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-9'
				},
			]
		}],
	},
	//slide10
	{
		val1: 1,
		val2: 2,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e3ins,
			textclass: "instruction",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'im-10'
				},
			]
		}],
	}
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new LampTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"im01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+"im02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+"im03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-4", src: imgpath+"im04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-5", src: imgpath+"im05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-6", src: imgpath+"im06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-7", src: imgpath+"im07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-8", src: imgpath+"im08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-9", src: imgpath+"im09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-10", src: imgpath+"im10.png", type: createjs.AbstractLoader.IMAGE},

			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"ex3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}

	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */


	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrong_clicked = false;

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		put_image(content, countNext);
		texthighlight($board);
		splitintofractions($board);
		input_box('.value-input', 1, '.check-btn');
		if(countNext==0){
			sound_player("sound_0");
		}

		wrong_clicked = false;
		$('.check-btn').click(function(){
			checker(content[countNext].val1, content[countNext].val2);
		});
	}
	function checker(ans1, ans2){
		if($('.input-1').val()=='' && $('.input-2').val()==''){
			return false;
		} else{
			if($('.input-1').val()==ans1 && $('.input-2').val()==ans2){
				$('.feedback-img').attr('src', preload.getResult('correct').src);
				$('.feedback-img').show(0);
				play_correct_incorrect_sound(1);
				$('.value-input, .check-btn').css({
					'background-color': '#92AF3B',
					'border-color': '#DDE27B',
					'color': 'white',
					'pointer-events': 'none'
				});
				$nextBtn.show(0);
				$('.check-btn').unbind('click');
				$('.value-input').trigger('blur');
				if(!wrong_clicked) scoring.update(true);
			} else{
				wrong_clicked = true;
				if(!wrong_clicked) scoring.update(false);
				$('.feedback-img').attr('src', preload.getResult('incorrect').src);
				$('.feedback-img').show(0);
				play_correct_incorrect_sound(0);
				$('.check-btn').css({
					'background-color': '#FF0000',
					'border-color': '#980000',
					'color': 'white'
				});
				if($('.input-1').val()==''){
					$('.input-1').css({
						'border-color': '#EEFF41',
						'background-color': 'white',
						'color': 'black',
					});
				} else if($('.input-1').val()==ans1){
					$('.input-1').css({
						'background-color': '#92AF3B',
						'border-color': '#DDE27B',
						'color': 'white',
						'pointer-events': 'none'
					});
					$('.input-1').trigger('blur');
				} else{
					$('.input-1').css({
						'background-color': '#FF0000',
						'border-color': '#980000',
						'color': 'white'
					});
				}

				if($('.input-2').val()==''){
					$('.input-2').css({
						'border-color': '#EEFF41',
						'background-color': 'white',
						'color': 'black',
					});
				} else if($('.input-2').val()==ans2){
					$('.input-2').css({
						'background-color': '#92AF3B',
						'border-color': '#DDE27B',
						'color': 'white',
						'pointer-events': 'none'
					});
					$('.input-2').trigger('blur');
				} else{
					$('.input-2').css({
						'background-color': '#FF0000',
						'border-color': '#980000',
						'color': 'white'
					});
				}
			}
		}
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

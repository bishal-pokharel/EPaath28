var imgpath = $ref+"/exercise/images/ex1/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 1
	{
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1ins,
				questiondata: data.string.e1text1,
				option: [
					{
						option_class: "class1",
						optionsrc: 'im-1',
					},
					{
						option_class: "class2",
						optionsrc: 'im-2',
					},
					{
						option_class: "class3",
						optionsrc: 'im-3',
					},
					{
						option_class: "class4",
						optionsrc: 'im-4',
					}],
			}
		]
	},
	//slide 2
	{
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1ins,
				questiondata: data.string.e1text2,
				option: [
					{
						option_class: "class3",
						optionsrc: 'im-1',
					},
					{
						option_class: "class2",
						optionsrc: 'im-2',
					},
					{
						option_class: "class1",
						optionsrc: 'im-3',
					},
					{
						option_class: "class4",
						optionsrc: 'im-4',
					}],
			}
		]
	},
	//slide 3
	{
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1ins,
				questiondata: data.string.e1text3,
				option: [
					{
						option_class: "class3",
						optionsrc: 'im-5',
					},
					{
						option_class: "class1",
						optionsrc: 'im-6',
					},
					{
						option_class: "class2",
						optionsrc: 'im-7',
					},
					{
						option_class: "class4",
						optionsrc: 'im-8',
					}],
			}
		]
	},
	//slide 4
	{
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1ins,
				questiondata: data.string.e1text4,
				option: [
					{
						option_class: "class1",
						optionsrc: 'im-5',
					},
					{
						option_class: "class2",
						optionsrc: 'im-6',
					},
					{
						option_class: "class3",
						optionsrc: 'im-7',
					},
					{
						option_class: "class4",
						optionsrc: 'im-8',
					}],
			}
		]
	},
	//slide 5
	{
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.e1ins,
				questiondata: data.string.e1text5,
				option: [
					{
						option_class: "class3",
						optionsrc: 'im-9',
					},
					{
						option_class: "class1",
						optionsrc: 'im-10',
					},
					{
						option_class: "class2",
						optionsrc: 'im-11',
					},
					{
						option_class: "class4",
						optionsrc: 'im-12',
					}],
			}
		]
	}
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new LampTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"q01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+"q02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+"q03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-4", src: imgpath+"q04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-5", src: imgpath+"q05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-6", src: imgpath+"q06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-7", src: imgpath+"q07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-8", src: imgpath+"q08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-9", src: imgpath+"q09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-10", src: imgpath+"q10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-11", src: imgpath+"q11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-12", src: imgpath+"q12.png", type: createjs.AbstractLoader.IMAGE},

			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sundar", src: "images/sundar/normal.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sundar-tr", src: "images/sundar/top-right.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundar-br", src: "images/sundar/bottom-right.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sundar-i1", src: "images/sundar/incorrect-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundar-i2", src: "images/sundar/incorrect-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundar-i3", src: "images/sundar/incorrect-3.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sundar-c1", src: "images/sundar/correct-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundar-c2", src: "images/sundar/correct-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundar-c3", src: "images/sundar/correct-3.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
				{id: "sound_0", src: soundAsset+"ex1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}

	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrong_clicked 	= false;

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		put_image(content, countNext);
		splitintofractions($board);

		scoring.numberOfQuestions();
		if(countNext==0){
			sound_player("sound_0");
		}

		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
		$('.center-sundar').attr('src', preload.getResult('sundar').src);

		/*for randomizing the options*/
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}
		//top-left
		$('.option-pos-1').hover(function(){
		$('.center-sundar').attr('src', preload.getResult('sundar-tr').src);
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){

		});
		//bottom-left
		$('.option-pos-3').hover(function(){
		$('.center-sundar').attr('src', preload.getResult('sundar-br').src);
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){

		});
		//top-right
		$('.option-pos-2').hover(function(){
		$('.center-sundar').attr('src', preload.getResult('sundar-tr').src);
			$('.center-sundar').css('transform','none');
		}, function(){

		});
		//bottom-right
		$('.option-pos-4').hover(function(){
		$('.center-sundar').attr('src', preload.getResult('sundar-br').src);
			$('.center-sundar').css('transform','none');
		}, function(){

		});

		var wrong_clicked = 0;
		var correct_images = ['sundar-c1', 'sundar-c2', 'sundar-c3'];
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.option-pos-1, .option-pos-2, .option-pos-3, .option-pos-4').off('mouseenter mouseleave');
				$('.center-sundar').attr('src', preload.getResult(correct_images[rand_img]).src);
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				var classname_monkey = $(this).parent().attr('class').replace(/main-container/, '');
				classname_monkey = classname_monkey.replace(/ /g, '');
				$('.'+classname_monkey).off('mouseenter mouseleave');
				if(wrong_clicked==0){
					$('.center-sundar').attr('src', preload.getResult('sundar-i1').src);
				} else if(wrong_clicked == 1){
					$('.center-sundar').attr('src', preload.getResult('sundar-i2').src);
				} else {
					$('.center-sundar').attr('src', preload.getResult('sundar-i3').src);
				}
				scoring.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	};
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('exerciseblock')){
			var imageblock = content[count].exerciseblock[0];
			if(imageblock.hasOwnProperty('option')){
				var imageClass = imageblock.option;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].optionsrc).src;
					//get list of classes
					var classes_list = imageClass[i].option_class.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]+'>img');
					$(selector).attr('src', image_src);
					console.log($(selector).attr('src'));
				}
			}
		}
	}
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generalTemplate();
		// for scoringg purpose
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

var imgpath = $ref + "/exercise/images/";

var content=[
	//slide1
	{
		qno: 1,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		svgblock: [{
			svgblock: 'center-svg',
		}],
		extratextblock:[{
			textdata: data.string.e2text1,
			textclass: "click-ins",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cor-incor",
					imgsrc : '',
					imgid : 'incorrect'
				}
			]
		}],
	},
	//slide2
	{
		qno: 2,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		svgblock: [{
			svgblock: 'center-svg',
		}],
		extratextblock:[{
			textdata: data.string.e2text2,
			textclass: "click-ins",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cor-incor",
					imgsrc : '',
					imgid : 'incorrect'
				}
			]
		}],
	},
	//slide3
	{
		qno: 3,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.e2text3,
			textclass: "click-ins",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cor-incor",
					imgsrc : '',
					imgid : 'incorrect'
				}
			]
		}],
	},
	//slide4
	{
		qno: 4,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		svgblock: [{
			svgblock: 'center-svg',
		}],
		extratextblock:[{
			textdata: data.string.e2text4,
			textclass: "click-ins",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cor-incor",
					imgsrc : '',
					imgid : 'incorrect'
				}
			]
		}],
	},
	//slide5
	{
		qno: 5,
		contentnocenteradjust: true,
		contentblockadditionalclass: 'default-bg',
		svgblock: [{
			svgblock: 'center-svg',
		}],
		extratextblock:[{
			textdata: data.string.e2text5,
			textclass: "click-ins",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cor-incor",
					imgsrc : '',
					imgid : 'incorrect'
				}
			]
		}],
	}
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	var scoring = new LampTemplate();
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "triangle", src: imgpath+"triangle.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "circle-1", src: imgpath+"circle-2part.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "circle-2", src: imgpath+"circle-2.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "circle-3", src: imgpath+"circle_part_8.svg", type: createjs.AbstractLoader.IMAGE},
			
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrong_clicked = false;
	var click_part;
	var click_number = 0;
	var click_arr;
	var wrclick = false;
	
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		put_image(content, countNext);
		texthighlight($board);
		splitintofractions($board);
		
		wrong_clicked = false;
		wrclick = false;
		switch(content[countNext].qno) {
			case 1:
			case 2:
				click_number = 0;
				//1 means off
				wrong_clicked = false;
				click_part = [1,1,1,1];
				var s = Snap('#center-svg');
				var svg = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#tfill-1');
					var fill_2 = Snap.select('#tfill-2');
					var fill_3 = Snap.select('#tfill-3');
					var fill_4 = Snap.select('#part-4');
					fill_1.attr('fill', 'transparent');
					fill_2.attr('fill', 'transparent');
					fill_3.attr('fill', 'transparent');
					fill_4.attr('fill', 'transparent');
					click_arr = [fill_1, fill_2, fill_3, fill_4];
					fill_1.hover(function(){
						fill_1.attr('fill', "CFE2F3");
					}, function(){
						(click_part[0]==1)?fill_1.attr('fill', "transparent"):fill_1.attr('fill', "#FFE599");
					});
					fill_1.click(function(){
						clicker(0, fill_1);
					});
					
					fill_2.hover(function(){
						fill_2.attr('fill', "CFE2F3");
					}, function(){
						(click_part[1]==1)?fill_2.attr('fill', "transparent"):fill_2.attr('fill', "#FFE599");
					});
					fill_2.click(function(){
						clicker(1, fill_2);
					});
					
					fill_3.hover(function(){
						fill_3.attr('fill', "CFE2F3");
					}, function(){
						(click_part[2]==1)?fill_3.attr('fill', "transparent"):fill_3.attr('fill', "#FFE599");
					});
					fill_3.click(function(){
						clicker(2, fill_3);
					});
					
					fill_4.hover(function(){
						fill_4.attr('fill', "CFE2F3");
					}, function(){
						(click_part[3]==1)?fill_4.attr('fill', "transparent"):fill_4.attr('fill', "#FFE599");
					});
					fill_4.click(function(){
						clicker(3, fill_4);
					});
				} );
				$('.check-btn').click(function(){
					if(content[countNext].qno==2){
						check_button_func(3);
					}else{
						check_button_func(1);
					}
				});
				break;
			case 3:
				$prevBtn.show(0);
				var rect_counter = 0;
				var rect_arr = [0,0,0];
				for(var i=1; i<4; i++){
					var str_div_1 = '<div class="rect-block"> </div>';
					$('#center-rect').append(str_div_1);
					$('.rect-block').eq(i-1).attr('id', 'm-rect-'+i);
				}
				$('.rect-block').on('click', function(){
					var rec_id = $(this).attr('id').replace(/\D/g, '');
					if(rect_arr[rec_id-1]==0){
						$(this).css('background-color', '#FFE599');
						rect_arr[rec_id-1] = 1;
						rect_counter++;
					} else{
						$(this).css('background-color', 'transparent');
						rect_arr[rec_id-1] = 0;
						rect_counter--;
					}
				});
				$('.check-btn').click(function(){
					if(rect_counter == 1){
						$('.check-btn').css({
							'background-color': '#92AF3B',
							'border-color': '#D4EF34',
							'color': 'white',
							'pointer-events': 'none'
						});
						play_correct_incorrect_sound(1);
						for(var i=0; i<10; i++){
							if(rect_arr[i]==1){
								$('#m-rect-'+(parseInt(i)+1)).css('background-color', '#BED62F');
							}
						}
						$('#center-rect').css('pointer-events', 'none');
						$nextBtn.show(0);
						$('.cor-incor').attr('src', preload.getResult('correct').src);
						$('.cor-incor').show(0);
						if(!wrclick){
							scoring.update(true);
						}
					} else {
						wrclick =  true;
						if(!wrclick){
							scoring.update(false);
						}
						$('.check-btn').css({
							'background-color': '#FF0000',
							'border-color': '#980000',
							'color':'black'
						});
						play_correct_incorrect_sound(0);
						for(var i=0; i<10; i++){
							if(rect_arr[i]==1){
								$('#m-rect-'+(parseInt(i)+1)).css('background-color', '#CC0000');
							}
						}
						$('.cor-incor').show(0);
					}
				});
				break;
			case 4:
				click_number = 0;
				//1 means off
				wrong_clicked = false;
				click_part = [1,1,1];
				var s = Snap('#center-svg');
				var svg = Snap.load(preload.getResult('circle-2').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#fill-1');
					var fill_2 = Snap.select('#fill-2');
					var fill_3 = Snap.select('#fill-3');
					click_arr = [fill_1, fill_2, fill_3];
					fill_1.hover(function(){
						fill_1.attr('fill', "CFE2F3");
					}, function(){
						(click_part[0]==1)?fill_1.attr('fill', "transparent"):fill_1.attr('fill', "#FFE599");
					});
					fill_1.click(function(){
						clicker(0, fill_1);
					});
					
					fill_2.hover(function(){
						fill_2.attr('fill', "CFE2F3");
					}, function(){
						(click_part[1]==1)?fill_2.attr('fill', "transparent"):fill_2.attr('fill', "#FFE599");
					});
					fill_2.click(function(){
						clicker(1, fill_2);
					});
					
					fill_3.hover(function(){
						fill_3.attr('fill', "CFE2F3");
					}, function(){
						(click_part[2]==1)?fill_3.attr('fill', "transparent"):fill_3.attr('fill', "#FFE599");
					});
					fill_3.click(function(){
						clicker(2, fill_3);
					});
				} );
				$('.check-btn').click(function(){
					check_button_func(2);
				});
				break;
			case 5:
				click_number = 0;
				//1 means off
				wrong_clicked = false;
				click_part = [1,1];
				var s = Snap('#center-svg');
				var svg = Snap.load(preload.getResult('circle-1').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#c-fill-1');
					var fill_2 = Snap.select('#c-fill-2');
					fill_1.attr('fill', 'transparent');
					fill_2.attr('fill', 'transparent');
					click_arr = [fill_1, fill_2];
					fill_1.hover(function(){
						fill_1.attr('fill', "CFE2F3");
					}, function(){
						(click_part[0]==1)?fill_1.attr('fill', "transparent"):fill_1.attr('fill', "#FFE599");
					});
					fill_1.click(function(){
						clicker(0, fill_1);
					});
					
					fill_2.hover(function(){
						fill_2.attr('fill', "CFE2F3");
					}, function(){
						(click_part[1]==1)?fill_2.attr('fill', "transparent"):fill_2.attr('fill', "#FFE599");
					});
					fill_2.click(function(){
						clicker(1, fill_2);
					});
				} );
				$('.check-btn').click(function(){
					check_button_func(1);
				});
				break;
		}
	}
	
	function clicker(index, class_snap){
		if(wrong_clicked == true){
			for(var i=0; i<click_part.length; i++){
				click_arr[i].attr('fill', 'transparent');
			}
		}
		if(click_part[index]==1){
			click_number++;
			click_part[index] = 0;
			class_snap.attr('fill', "#FFE599");
		} else {
			click_number--;
			click_part[index] = 1;
			class_snap.attr('fill', "transparent");
		}
		wrong_clicked = false;
	}
	function check_button_func(correct_num){
		if(click_number==correct_num){
			wrong_clicked = false;
			$('.check-btn').css({
				'background-color': '#92AF3B',
				'border-color': '#D4EF34',
				'color': 'white',
				'pointer-events': 'none'
			});
			play_correct_incorrect_sound(1);
			for(var i=0; i<click_part.length; i++){
				if(click_part[i]==0){
					click_arr[i].attr('fill', '#BED62F');
				}
			}
			$('.cor-incor').attr('src', preload.getResult('correct').src);
			$('.cor-incor').show(0);
			$('svg').css('pointer-events', 'none');
			$nextBtn.show(0);
			if(!wrclick){
				scoring.update(true);
			}
		} else {
			wrong_clicked = false;
			if(!wrclick){
				scoring.update(false);
			}
			wrclick	= true;
			$('.check-btn').css({
				'background-color': '#FF0000',
				'border-color': '#980000',
				'color':'black'
			});
			play_correct_incorrect_sound(0);
			for(var i=0; i<click_part.length; i++){
				if(click_part[i]==0){
					click_arr[i].attr('fill', '#CC0000');
				}
			}
			$('.cor-incor').show(0);
		}
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});
